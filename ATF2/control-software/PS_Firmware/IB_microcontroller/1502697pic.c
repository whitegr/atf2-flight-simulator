//******************************************************************************
//  OCEM Firmware for SLAC serial Board 150.2967
//  Target Processor: PIC18F2420-I/SO
//  Version 2.00 01/22/2008
//
//  The code performs the following functions
//  1) Communicates with SLAC power supply controller (EPSC) using internal UART
//  2) Generates Power Supply Ready and ON signals for EPSC
//  3) Disables individual power modules using SPI link to gate array
//  4) Digitizes 5 analog module output current signals, check for overload 
//
//  ******** Signals from Power Supply Modules ********
//  Imo1     PIN_B2  Module 1 output current, ADC Channel 8
//  Imo2     PIN_A1  Module 2 output current, ADC Channel 1
//  Imo3     PIN_A2  Module 3 output current, ADC Channel 2
//  Imo4     PIN_A0  Module 4 output current, ADC Channel 0
//  Imo5     PIN_A5  Module 5 output current, ADC Channel 4
//
//  Fault1N  PIN_B0  Fault signal from module 1 (high = fault)
//  Fault2N  PIN_A6  Fault signal from module 2 (high = fault)
//  Fault3N  PIN_B5  Fault signal from module 3 (high = fault)
//  Fault4N  PIN_B6  Fault signal from module 4 (low = fault)
//  Fault5N  PIN_B7  Fault signal from module 5 (low = fault)
//
//  **************** Signals to/from EPSC ******************
//  On_cmd   PIN_C0  Module enable signal from EPSC (low = ON)
//  PS_on    PIN_C1  Power Supply ON signal to EPSC (high = ON)
//  PS_ready PIN_C2  Power Supply Ready signal to EPSC (high = Ready)
//
//  ****************** Signals from FPGA *******************
//  IO_uc    PIN_B1  Power Supply Ready (no error) from FPGA (high = ready)
//
//  ******** Number of modules (2-5), from DIP switch ******
//  Nmod1    PIN_B4  Number of modules LSB
//  Nmod2    PIN_B3  Number of modules MSB
//
//  UART Interface to EPSC, 9600 Baud, no parity, 1 stop bit, no flow control
//  
//******************************************************************************

#include "1502697pic.h"

#define C_Return  0x0D //Command Return
#define C_Disable 'd'  //Command Disable
#define C_Enable  'e'  //Command Enable
#define C_Current 'i'  //Command Read Current
#define C_Status  's'  //Command Read Status

#ROM 0x3ff0 = {"12/22/2007"} // Write release date at end of code memory (8K)
#ROM 0x3ffe = {0x0200}       // Write version number 02.00 at end of code mem

#define WORD unsigned int16  // Define type WORD as unsigned 16 bit integer

WORD ADC_Value = 0;          // This variable is used to store the ADC reading
WORD Mod_Cur[5];             // Module current
WORD Avg_Cur[5];             // Average of module current       
byte Chan;                   // Module number for ADC
byte Overcurrent = 0;        // Module Overcurrent flag, 0 = no fault

char EPSC_Command[3] = "";   // UART Input buffers
char N_modules = 5;          // Number of modules
char F_modules = 0;          // Not Faulted modules
char D_modules = 0;          // Disabled modules

//*************************** void Send_SPI( void ) ****************************
//  This function sends the module disable register (D_modules) to the FPGA.
//  "spi_read" is a built in function that sends the argument (D_modules) using
//  the PIC hardware SPI. Data is read on the SPI_DI pin (char c) and discarded.
//  SPI_DI    PIN_C5  SPI data in line, not used
//  SPI_DO    PIN_C4  SPI data out line to FPGA
//  SPI_CLK   PIN_C3  SPI data clock to FPGA
//  SPI_en    PIN_A4  Clock for SPI data latch in FPGA, latch on rising edge
//******************************************************************************

char c;                        // return value from spi_read, not used
#inline void Send_SPI( void )
{
  OUTPUT_BIT (SPI_en, 1);
  c = spi_read( ~D_modules );  // Send SPI data using built in function
  OUTPUT_BIT (SPI_en, 0);      // Data latched in FPGA on falling edge
}

//**************************** void ReadADC( void ) ****************************
// Read current for modules 1-5 using internal 10 bit ADC.
// Filter ADC readings and save in Mod_Cur[i].
// Filter behaves like an RC with a time constant of 32 samples (6.4 mS)
// If module current is greater then 55 amps, set Overcurrent
// ADC scale = 1023 / 5 volts = 204.6 counts / volt
// Module output for 50 amps = 4.00 volts ( 818 counts, 16.368 counts / amp )
// ADC value for 55 amps = 16.368 * 55 = 900
//******************************************************************************

#inline void ReadADC( void )
{
  Overcurrent = 0;                           // Clear overcurrent flag

  for (Chan = 0; Chan < 5; ++Chan)           // Read module 1-5 current
  {
    switch ( Chan )                          // Select ADC input
    {  
      case 0: set_adc_channel( 8 ); break;   // set ADC to read module 1 current
      case 1: set_adc_channel( 1 ); break;   // set ADC to read module 2 current
      case 2: set_adc_channel( 2 ); break;   // set ADC to read module 3 current
      case 3: set_adc_channel( 0 ); break;   // set ADC to read module 4 current
      case 4: set_adc_channel( 4 ); break;   // set ADC to read module 5 current
    }

    ADC_Value = Read_ADC();                  // read ADC (built-in function)

    Avg_Cur[ Chan ] += ADC_Value;
    Mod_Cur[ Chan ] = Avg_Cur[ Chan ] / 32;
    Avg_Cur[ Chan ] -= Mod_Cur[ Chan ];    
  
    if ( Mod_Cur[ Chan ] >= 900 ) Overcurrent = 1;
  }
}

//***************************** void Status( void )*****************************
//  This functions generates the status reply message for the UART. The reply
//  is seven characters long. It is sent in response to all valid requests,
//  except for the module current ('i').
//  's' first character identifies response as status
//  'n' next character is value of N_Modules (number of modules from DIP switch)  
//  'nn' next 2 characters are hexadecimal value of F_Modules (faulted modules)
//  'nn' next 2 characters are hexadecimal value of D_Modules (disabled modules)
//  0x0D last character is a carriage return
//
//  Faulted or Disabled modules are represented by a single bit
//  0x01  Module 1        0x10  Module 5
//  0x02  Module 2        0x20  Module 6 (reserved for future use)
//  0x04  Module 3        0x40  Module 7 (reserved for future use)
//  0x08  Module 4        0x80  Module 8 (reserved for future use)
//******************************************************************************

#inline void Status( void )
{
  F_modules = 0;
  if (  INPUT( Fault1N ) ) F_modules |= 0x01;   // Check module 1 fault signal
  if (  INPUT( Fault2N ) ) F_modules |= 0x02;   // Check module 2 fault signal
  if (  INPUT( Fault3N ) ) F_modules |= 0x04;   // Check module 3 fault signal
  if ( !INPUT( Fault4N ) ) F_modules |= 0x08;   // Check module 4 fault signal
  if ( !INPUT( Fault5N ) ) F_modules |= 0x10;   // Check module 5 fault signal
  
  // send formatted response string to UART
  printf("s%1i%02X%02X\r", N_Modules, F_Modules, D_Modules);
}

//**************************** void Disable( void ) ****************************
//  Process Disable Command ('d'). Set the disable bit in D_modules for the 
//  module number specified by the first character ( EPSC_Command[2] ). Send a
//  status message in response. Do nothing if module number is invalid.
//******************************************************************************

#inline void Disable( void )
{
  switch ( EPSC_Command[2] )       // get module number from first character
  {
    case '1': D_modules |= 0b00001; break;    // Disable module 1
    case '2': D_modules |= 0b00010; break;    // Disable module 2
    case '3': D_modules |= 0b00100; break;    // Disable module 3
    case '4': D_modules |= 0b01000; break;    // Disable module 4
    case '5': D_modules |= 0b10000; break;    // Disable module 5
    return;                                   // Not valid module, do nothing
  }
  Status();                        // send status response
}

//**************************** void Enable( void ) *****************************
//  Process Enable Command ('e'). Clear the disable bit in D_modules for the 
//  module number specified by the first character ( EPSC_Command[2] ). Send a
//  status message in response. Do nothing if module number is invalid.
//******************************************************************************

#inline void Enable( void )
{
  switch ( EPSC_Command[2] )       // get module number from first character    
  {
    case '0': D_modules &= 0b00000; break;    // Enable all modules 
    case '1': D_modules &= 0b11110; break;    // Enable module 1
    case '2': D_modules &= 0b11101; break;    // Enable module 2
    case '3': D_modules &= 0b11011; break;    // Enable module 3
    case '4': D_modules &= 0b10111; break;    // Enable module 4
    case '5': D_modules &= 0b01111; break;    // Enable module 5
    return;                                   // Not valid module, do nothing
  }
  Status();                        // send status response
}

//**************************** void Current( void ) ****************************
//  Process module current request ('i'). Read the current for the module number
//  specified by the first character (EPSC_Command[2]). Send a response message
//  with the DAC data as a 3 character hexadecimal number. The response message
//  is 6 characters long. Do nothing if module number is invalid.
//  'i' first character identifies respone as module current
//  'n' second character is the module number
//  'nnn' three character module current in hexadecimal (0x000 to 0x3FF)
//  0x0D last character is a carriage return
//******************************************************************************

#inline void Current( void )
{
  // if valid module number (1-5), send formated (hex) module current to UART
  if (( EPSC_Command[2] > '0' ) &&  ( EPSC_Command[2] <= '5' ))
    printf( "i%c%03lx\r", EPSC_Command[2], Mod_Cur[ EPSC_Command[2] - '1' ]);
}

//********************** void EPSC_Communication( void ) ***********************
//  Process character received from UART. The last three characters received are
//  buffered. When a carriage return is received, the previous 2 characters are
//  processed as a request.
//  The character before the carriage return defines the command
//    'd' disable module
//    'e' enable module (all modules enabled when module number = 0)
//    'i' reads module output current
//    's' read status of all modules (2 character command, no module number)
//  The character before the command is the module number (1,2,3,4,5)
//  All invalid messages are discarded
//******************************************************************************

#inline void EPSC_Communication( void )
{
  EPSC_Command[2] = EPSC_Command[1];  // Buffer last 3 characters recieved
  EPSC_Command[1] = EPSC_Command[0];
  EPSC_Command[0] = getc();  

  if (EPSC_Command[0] == 0x0d )      // If last character is carriage return
  {
    switch( EPSC_Command[1] )        // find the message type in EPSC_Command[1] 
    {
      case C_Disable : Disable(); break;
      case C_Enable  : Enable();  break;
      case C_Current : Current(); break;
      case C_Status  : Status();  break;
    }
  }
}

//************************ void initialization( void ) *************************
//  Initialize PIC hardware configuration registers
//******************************************************************************

#inline void initialization( void )
{
  setup_spi(SPI_MASTER);
  setup_adc_ports( AN0_TO_AN8 | VSS_VREF );
  setup_adc(ADC_CLOCK_DIV_8 | ADC_TAD_MUL_4);
  setup_wdt(WDT_ON);
  setup_timer_0(RTCC_INTERNAL);
  setup_timer_1(T1_DISABLED);
  setup_timer_2(T2_DISABLED,0,1);
  setup_timer_3(T3_DISABLED|T3_DIV_BY_1);
  setup_comparator(NC_NC_NC_NC);
  setup_vref(FALSE);
   
  rs232_errors = rs232_errors;  // avoid "Variable never used" compiler error

  for (Chan = 0; Chan < 5; ++Chan)
  {
    Avg_Cur[ Chan ] = 0;             // Initialize average current registers
    Mod_Cur[ Chan ] = 0;             // Initialize module current registers
  }
}

//****************************** void main( void )******************************
//  The main program initializes the PIC hardware, reads the number of modules
//  from a DIP switch, and then loops forever checking for messages recieved
//  from the EPSC by the PIC UART. The main loop executes approximately 5000
//  times per second.
//
//  Note: the UART read process does not use blocking functions, such as "gets".
//  These are functions that block all others waiting for an event to happen.
//  This EPSC_Communication() function is only called after a character is
//  available (kbhit() is true), and reads one character.
//  The INPUT(pin) functions returns true or false. True is only defined as
//  non-zero and should not be assumed to have a value of 1.
//******************************************************************************

void main( void )
{
  initialization();              // Initialize PIC hardware registers

  printf( "VER 12/18/07\r" );    // send firmware identification string to UART
  
  N_Modules = 2;                 // Read number of modules (2-5) from DIP switch
  if ( !INPUT( Nmod1 ) ) N_Modules += 1;
  if ( !INPUT( Nmod2 ) ) N_Modules += 2;

  while (true)                      // loop forever
  {
    ReadADC();                      // read module currents

    if ( kbhit() )                  // If character available from UART
      EPSC_Communication();         // call EPSC_Communication() to process  

    Send_SPI();                     // write disable status (D_modules) to FPGA

    if ( INPUT( IO_uc ) && !Overcurrent )   // Set PS Ready signal to EPSC
      OUTPUT_BIT( PS_ready, 1 );
    else
      OUTPUT_BIT( PS_ready, 0 );

    if ( INPUT( On_cmd ) )          // Set PS ON signal to EPSC         
      OUTPUT_BIT( PS_on, 0 );
    else
      OUTPUT_BIT( PS_on, 1 );

    restart_wdt();                  // Restart PIC watchdog timer    
  }
}


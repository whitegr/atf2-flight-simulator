#include <18F2420.h>

#device ICD=TRUE
#device ADC=10         //ADC 10 bit 
#FUSES WDT             //No Watch Dog TimerWDT, 
#FUSES WDT4            //Watch Dog Timer uses 1:4 Postscale (16 mSec)
#FUSES EC_IO           //External clock with A6 as IO
#FUSES NOPROTECT       //Code not protected from reading
#FUSES BROWNOUT        //Reset when brownout detected
#FUSES BORV20          //Brownout reset at 2.0V
#FUSES PUT             //No Power Up Timer
#FUSES NOCPD           //No EE protection
#FUSES STVREN          //Stack full/underflow will cause reset
#FUSES DEBUG           //Debug mode for ICD //NODEBUG
#FUSES LVP             //Low Voltage Programming on B3(PIC16) or B5(PIC18)
#FUSES NOWRT           //Program memory not write protected
#FUSES NOWRTD          //Data EEPROM not write protected
#FUSES IESO            //Internal External Switch Over mode enabled
#FUSES FCMEN           //Fail-safe clock monitor enabled
#FUSES PBADEN          //PORTB pins are configured as analog input channels
#FUSES NOWRTC          //configuration not registers write protected
#FUSES NOWRTB          //Boot block not write protected
#FUSES NOEBTR          //Memory not protected from table reads
#FUSES NOEBTRB         //Boot block not protected from table reads
#FUSES NOCPB           //No Boot Block code protection
#FUSES LPT1OSC         //Timer1 configured for low-power operation
#FUSES MCLR            //Master Clear pin enabled
#FUSES NOXINST         //Legacy mode,Extended set/Indexed Addressing disabled 

#use delay(clock=10000000,RESTART_WDT)
#define Imo4      PIN_A0 //PCB scambio con RB4
#define Imo2      PIN_A1 //PCB scambio con RB3
#define Imo3      PIN_A2 //PCB scambio con RB1
#define Vr_ad     PIN_A3 //OK PCB
#define SPI_en    PIN_A4 //OK PCB
#define Imo5      PIN_A5 //PCB scambio con RB0
#define Fault2N   PIN_A6 //OK PCB
#define On_cmd    PIN_C0 //OK PCB
#define PS_on     PIN_C1 //OK PCB
#define PS_ready  PIN_C2 //OK PCB

#define Fault1N   PIN_B0 //PCB scambio con RA5
#define IO_uc     PIN_B1 //PCB scambio con RA2
#define Imo1      PIN_B2 //OK PCB
#define Nmod2     PIN_B3 //PCB scambio con RA1
#define Nmod1     PIN_B4 //PCB scambio con RA0
#define Fault3N   PIN_B5 //OK PCB
#define Fault4N   PIN_B6 //OK PCB
#define Fault5N   PIN_B7 //OK PCB

#use rs232(baud=9600,parity=N,xmit=PIN_C6,rcv=PIN_C7,bits=8,errors,STOP=1)

#define SPI_DI     PIN_C5
#define SPI_DO     PIN_C4
#define SPI_CLK    PIN_C3

#include <string.h>
#include <stdlib.h>



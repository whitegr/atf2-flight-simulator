//******************************************************************************
//  READ SETPOINTS (0xC3) (Ethernet Power Supply Controller)
//  Filename: cmd_C3.cpp
//  03/03/05
//
//  This message reads power supply setpoints. The message must request from 1 to
//  5 setpoints. Each setpoint contains a desired power supply current and a ramp
//  time.
//
//  For the command to be executed, the following conditions are checked;
//  1) The channel number must be 0
//  2) The number of setpoints must be from 1 to 5
//  3) The command length must be correct for the number of setpoints
//
//******************************************************************************

#include <C:\Nburn\EPSC\include\includes.h>    //Include all headers for project

typedef struct          // C3 command structure
{ BYTE   Ctype;         // 00 command type 0xC3
  BYTE   Rsp;           // 01 response code
  BYTE   Tid;           // 02 Task ID
  BYTE   Num;           // 03 Number of setpoints (1 to 5)
  BYTE   Chan;          // 04 Channel number (must be zero)
} __attribute__ ((packed))  Cmd_C3;

#define CMD_LEN_C3  5   // expected length of command message

typedef struct          // setpoint
{ float  Cur;           // Power supply current
  WORD   Time;          // time in 1/100 second or 1/20 second (slow ramp)
} __attribute__ ((packed))  setp;

typedef struct          // C3 response structure
{ BYTE   Ctype;         // 00 command type, returned unchanged
  BYTE   Rsp;           // 01 response code
  BYTE   Tid;           // 02 Task ID, returned unchanged
  BYTE   Chan;          // 03 Channel, returned unchanged
  WORD   Status01;      // 04-05 Status bytes 0 and 1
  setp   Setpoint[5];   // 06-11, 17, 23, 29, 35, Setpoint 1 to 5
} __attribute__ ((packed))  Rsp_C3;

#define RSP_LEN_C3  6   // length of response message without setpoints
#define LEN_SP      6   // length of each setpoint

//********************* void Process_C3(UDPPacket * pUDP) **********************
//  pUDP  pointer to object of class UDPPacket, passed to function
//  pCmd  pointer to command structure (type Cmd_C3) set to start of UDP data
//  pRsp  pointer to response structure (type Rsp_C3) set to start of UDP data
//******************************************************************************

void Process_C3(UDPPacket * pUDP)
{
  PBYTE pUdpData = pUDP->GetDataBuffer();      //get pointer to UDP data field
  Cmd_C3 * pCmd = (Cmd_C3 *) pUdpData;         //set pointer for command data
  Rsp_C3 * pRsp = (Rsp_C3 *) pUdpData;         //set pointer for response data
  int NumSetPnt = pCmd->Num;                   //number of setpoints

  if (pCmd->Chan !=0)                          //check for valid channel (0)
  { pRsp->Rsp = INVALID_CHAN_NUM;
  }
  if ((NumSetPnt < 1) | (NumSetPnt > 5))       //check for valid number of setpoints
  { pRsp->Rsp = INVALID_NUM_ENTRY;
  }                                            //check for valid command length
  else if (pUDP->GetDataSize() != CMD_LEN_C3)
  { pRsp->Rsp = INVALID_MSG_LENGTH;
  }
  else                                         //valid message structure
  {
    //transfer requested number of setpoint time and currents to response message
    for (WORD i=1; i <= pCmd->Num; ++i)
    {
      //convert times from 120 hertz to 100hz or 20hz, convert to little endian
      pRsp->Setpoint[i-1].Time = (WORD) Chg120To100( xil.Ramp_Times[i] );


      //current = (xduct volts per amp) * (adc counts) / (adc counts per volt)
      pRsp->Setpoint[i-1].Cur = RevPol( xil.db_eep.reg_iva
                                        * (float)xil.ADC_SetPnt[i]
                                        / (float)KADC );

      //convert setpoint data to little endian (Intel) by reversing byte order
      flip2((PBYTE) & pRsp->Setpoint[i-1].Time);
      flip4((PBYTE) & pRsp->Setpoint[i-1].Cur);
    }

    pRsp->Rsp = COMMAND_OK;                     //set response code
    pRsp->Chan = pCmd->Chan;                    //set response chan
    pRsp->Status01 = Status01(NO_ERR);          //set status bytes 0 and 1

    //data size = (header length) + (number of setpoints) * (setpoint length)
    pUDP->SetDataSize(RSP_LEN_C3 + NumSetPnt * LEN_SP);
  }
}


//******************************************************************************
//  SET PS CURRENT (0xC1, 0xC2) (Ethernet Power Supply Controller)
//  Filename: cmd_C1.cpp
//  03/03/05
//
//  This message contains power supply setpoints. The message must have from 1 to
//  5 setpoints. Each setpoint contains a desired power supply current and a ramp
//  time. The C1 command starts the ramp immediately. The C2 command waits until
//  a hardware ramp signal is received.
//
//  For the command to be executed, the following conditions are checked;
//  1) The channel number must be 0
//  2) The number of setpoints must be from 1 to 5
//  3) The command length must be correct for the number of setpoints
//  4) The power supply must be in remote mode (not local mode)
//  5) Power supply on be ON
//  6) All ramp times must be greater then 0
//  7) All currents must convert to valid ADC voltages
//     a) 0 to 10 volts if the power supply type is unipolar
//     b) 0 to -10 volts if the power supply is in reverse polarity
//     c) -10 to 10 volts if the power supply is bipolar
//  8) The power supply must not be ramping (OFF or HOLD)
//  9) Write setpoints to Xilinx memory
//  10) Set Ramp and Hold Enable flags
//
//  The C1 command always sets the ramp state to ON. The C2 command always sets
//  the ramp state to OFF, and enables the hardware Ramp/Hold signals.
//  The hardware Ramp/Hold signals are enabled in the C1 command if the HOLD Enable
//  flag is set in the power supply configuration (xil.db_eep.config).
//
//******************************************************************************

#include <C:\Nburn\EPSC\include\includes.h>    //Include all headers for project

typedef struct          // setpoint
{ float  Cur;           // Power supply current
  WORD   Time;          // time in 1/100 second or 1/20 second (slow ramp)
} __attribute__ ((packed))  setp;

typedef struct          // C1, C2 command structure
{ BYTE   Ctype;         // 00 command type 0xC1 or 0xC2
  BYTE   Rsp;           // 01 response code
  BYTE   Tid;           // 02 Task ID
  BYTE   Num;           // 03 Number of setpoints (1 to 5)
  BYTE   Chan;          // 04 Channel number (must be zero)
  setp   Setpoint[5];   // 05-10, 16, 22, 26, 32, Setpoint 1 to 5
} __attribute__ ((packed))  Cmd_C1;

#define CMD_LEN_C1  5   // expected length of command message without setpoints
#define CMD_LEN_SP  6   // length of each setpoint

typedef struct          // C1, C2 response structure
{ BYTE   Ctype;         // 00 command type, returned unchanged
  BYTE   Rsp;           // 01 response code
  BYTE   Tid;           // 02 Task ID, returned unchanged
  BYTE   Chan;          // 03 Channel, returned unchanged
  WORD   Status01;      // 04-05 Status bytes 0 and 1
} __attribute__ ((packed))  Rsp_C1;

#define RSP_LEN_C1  6   // length of response message

//********************* void Process_C1(UDPPacket * pUDP) **********************
//  pUDP  pointer to object of class UDPPacket, passed to function
//  pCmd  pointer to command structure (type Cmd_C1) set to start of UDP data
//  pRsp  pointer to response structure (type Rsp_C1) set to start of UDP data
//******************************************************************************

void Process_C1(UDPPacket * pUDP)
{ WORD i = 0;
  BYTE errflag = NO_ERR;     //error flag
  float AdcVlt[5];           //temporary storage for setpoints, converted to volts

  PBYTE pUdpData = pUDP->GetDataBuffer();        //get pointer to UDP data field
  Cmd_C1 * pCmd = (Cmd_C1 *) pUdpData;           //set pointer for command data
  Rsp_C1 * pRsp = (Rsp_C1 *) pUdpData;           //set pointer for response data

  if (pCmd->Chan !=0)                            //check for valid channel (0)
  { pRsp->Rsp = INVALID_CHAN_NUM;
  }
  else if ((pCmd->Num < 1) | (pCmd->Num > 5))    //check for valid num of setpoints
  { pRsp->Rsp = INVALID_NUM_ENTRY;
  }                                              //check for valid command length
  else if (pUDP->GetDataSize() != (CMD_LEN_C1 + pCmd->Num * CMD_LEN_SP))
  { pRsp->Rsp = INVALID_MSG_LENGTH;
  }
  else                                           //valid message structure
  { if (xil.ps_status & STATUS_LOCAL)            //check for local mode
    { errflag = ERR;
      WrErrMsg("C1 ERROR, PS IN LOCAL MODE");
    }
    else if (xil.ps_status & STATUS_PS_OFF)      //check for power supply off
    { errflag = ERR;
      WrErrMsg("C1 ERROR, PS NOT ON");
    }
    else
    {
      for (i = 0; i < pCmd->Num; ++i)            //check setpoint data
      {
        //change setpoints to big endian (Motorola) from little endian (Intel)
        flip2((PBYTE) & pCmd->Setpoint[i].Time);      //reverse bytes for time
        flip4((PBYTE) & pCmd->Setpoint[i].Cur);       //reverse bytes for current

        if ((pCmd->Setpoint[i].Time) == 0)            //check for zero timespan
        { WrErrMsg("C1 ERROR, ZERO TIMESPAN");
          errflag = ERR;
          break;
        }

        //convert setpoint current to ADC volts for range chaecking
        //negate if reverse polarity
        AdcVlt[i] = RevPol( pCmd->Setpoint[i].Cur ) / xil.db_eep.reg_iva;

        //if chassis is set to bipolar, range is -10V to +10V
        //else the normal range is limited to 0V to 10V
        if (xil.db_eep.Config & CONFIG_BIPOLAR)     //type = bipolar, -10 to 10
        {
          if ((AdcVlt[i] < -10.0) || (AdcVlt[i] > 10.0)) errflag = ERR;
        }
        else                                        //type = normal, 0 to 10
        {
          if ((AdcVlt[i] < 0.0) || (AdcVlt[i] > 10.0)) errflag = ERR;
        }

        if (errflag == ERR)
        {
          WrErrMsg("C1 ERROR, CURRENT OUT OF RANGE");
        }
      }

      if (errflag == NO_ERR)                     //setpoints ok, process command
      {
        //Check for either of two ramp bits set
        if (xil.ps_status & STATUS_RAMP_ON)      //check for power supply ramping
        { errflag = ERR;
          WrErrMsg("C1 ERROR, PS RAMPING");
        }
        else
        {
          xil.ps_cntl = PSCNTL_RAMP_OFF          //set Ramp status to OFF
                      + PSCNTL_HOLD_OFF;         //set Hold Enable to OFF
          xil.Cur_Ramp_Seg = 1;                  //set current ramp segment to 1
          xil.Cur_Ramp_time = 0;                 //set current ramp time to 0
          xil.Num_Ramp_Seg = pCmd->Num;          //set number of ramp segments
          xil.Ramp_Times[0] = 0;                 //set time remaining to 0
          xil.ADC_SetPnt[0] = xil.Des_ADC;       //set start ADC to current setpoint

          //transfer setpoint time and currents to memory
          //convert times to 120 hertz from 100hz or 20hz
          //add each segment time to time remaining (xil.Ramp_Times[0])
          //convert volts to adc counts

          for (i=1; i <= 5; ++i)
          { if (i <= pCmd->Num)
            { xil.Ramp_Times[i] = Chg100To120( pCmd->Setpoint[i-1].Time );
              xil.Ramp_Times[0] += xil.Ramp_Times[i];
              xil.ADC_SetPnt[i] = (DWORD)(AdcVlt[i -1] * (float)KADC);
            }
            else                                 //clear unused setpoints
            { xil.Ramp_Times[i] = 0;
              xil.ADC_SetPnt[i] = 0;
            }
          }

          //set ramp state to RAMP_ON if C1 command
          //enable hardware Ramp/Hold signals if C2 command or Hold Enable On

          if (pCmd->Ctype == 0xC1)
          { 
            xil.ps_cntl = PSCNTL_RAMP_ON;        //set Ramp status to ON
            xil.Ramp_State = RAMP_C1;            //set Ramp State to C1
            if (xil.db_eep.Config & CONFIG_HOLD_EN)
            {
              xil.ps_cntl = PSCNTL_HOLD_EN;      //set Hold Enable to ON
            }
          }
          else  //pCmd->Ctype == 0xC2
          { 
            xil.ps_cntl = PSCNTL_HOLD_EN;        //set Hold Enable to ON
            xil.Ramp_State = RAMP_C2;            //set Ramp State to C2  
          }
        }
      }
    }
    pRsp->Rsp = COMMAND_OK;                      //set response code
    pRsp->Chan = pCmd->Chan;                     //set response chan
    pRsp->Status01 = Status01(errflag);          //set status bytes 0 and 1
    pUDP->SetDataSize(RSP_LEN_C1);               //set response packet data size
  }
}


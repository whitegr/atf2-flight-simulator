//******************************************************************************
//  DIAGANOSTIC MESSAGE 1 COMMAND (0xCB) (Ethernet Power Supply Controller)
//  Filename: cmd_CB.cpp
//  03/03/05
//  This message returns diagnostic information.
//******************************************************************************

#include <C:\Nburn\EPSC\include\includes.h>    //Include all headers for project

typedef struct          // CB command structure
{ BYTE   Ctype;         // 00 command type 0xCB
  BYTE   Rsp;           // 01 response code
  BYTE   Tid;           // 02 Task ID
  BYTE   Chan;          // 03 Channel number (must be zero)
} __attribute__ ((packed))  Cmd_CB;

#define CMD_LEN_CB  4   // expected length of command message

typedef struct          // CB response structure
{ BYTE   Ctype;         // 00 command type, returned unchanged
  BYTE   Rsp;           // 01 response code
  BYTE   Tid;           // 02 Task ID, returned unchanged
  BYTE   Chan;          // 03 Channel, returned unchanged
  BYTE   Config;        // 04 Chassis Configuration
  WORD   SerialNum[4];  // 05-12 Serial number 8 byte string
  char   Version[8];    // 13-20 Firmware Version 8 byte string
  WORD   MagnetId[4];   // 21-28 Magnet ID 8 byte string
} __attribute__ ((packed))  Rsp_CB;

#define RSP_LEN_CB  29  // length of response message

//********************* void Process_CB(UDPPacket * pUDP) **********************
//  pUDP  pointer to object of class UDPPacket, passed to function
//  pCmd  pointer to command structure (type Cmd_CB) set to start of UDP data
//  pRsp  pointer to response structure (type Rsp_CB) set to start of UDP data
//******************************************************************************

void Process_CB(UDPPacket * pUDP)
{
  PBYTE pUdpData = pUDP->GetDataBuffer();      //get pointer to UDP data field
  Cmd_CB * pCmd = (Cmd_CB *) pUdpData;         //set pointer for command data
  Rsp_CB * pRsp = (Rsp_CB *) pUdpData;         //set pointer for response data

  if (pUDP->GetDataSize() != CMD_LEN_CB)       //check for valid command length
  { pRsp->Rsp = INVALID_MSG_LENGTH;
  }
  else if (pCmd->Chan !=0)                     //check for valid channel (0)
  { pRsp->Rsp = INVALID_CHAN_NUM;
  }
  else
  { short i;
    pRsp->Config = xil.db_eep.Config;          //Chassis Configuration
    for (i = 0; i < 4; ++i)                    //Magnet ID 8 byte string
      pRsp->SerialNum[i] = xil.mb_eep.SerialNum[i];

    for (i = 0; i < 8; ++i)                    //Firmware Version date 8 byte string
      pRsp->Version[i] = *(pFirmwareDate + i);

    for (i = 0; i < 4; ++i)                    //Magnet ID 8 byte string
      pRsp->MagnetId[i] = xil.db_eep.MagnetId[i];
    pRsp->Rsp = COMMAND_OK;                    //set response code
    pUDP->SetDataSize(RSP_LEN_CB);             //set data size for response packet
  }
}


//******************************************************************************
//  Ethernet Diagnostic task (Ethernet Power Supply Controller)
//  Filename: EthernetDiagTask.cpp
//  10/21/05
//
//  This task prints all received ethernet messages on the serial port
//
//******************************************************************************

#include <C:\Nburn\EPSC\include\includes.h>    //Include all headers for project

//***************************** StartUdpDiagTask() *****************************

extern WORD EthernetFilter( pool_buffer * pb, BYTE BroadcastFlag);
void ShowEnetMain(void * pd);                          //foward declaration

DWORD  ShowEnetStk[USER_TASK_STK_SIZE] __attribute__(( aligned(4) ));

void StartShowEnetTask()
{ OSTaskCreate( ShowEnetMain,                          //address of task code
                (void *) 0,                            //optional argument pointer
                & ShowEnetStk[USER_TASK_STK_SIZE],     //top of stack
                ShowEnetStk,                           //bottom of stack
                SHOW_ENET_PRIO);                       //task priority
}

//******************************************************************************
// Display all received Ethernet Packets
//******************************************************************************

void ShowIpPacket( PBYTE pPos );  //Foward declaration

void ShowEnetMain(void * pd)
{ pd=pd;

  OSFifoInit( & ShowFecFifo);

  while (1)
  {
    pool_buffer * pb = ( pool_buffer * ) OSFifoPend( & ShowFecFifo, 0);
    WORD PassPacket = EthernetFilter( pb, pb->bBufferFlag );

    WORD ShowPacket = 1;                      // Show all packets

    if ( ShowEthernet == 2 )                  // Show passed packets
    {
      if ( !PassPacket ) ShowPacket = 0;      // Do not show dropped packets
    }
    else if ( ShowEthernet == 3 )             // Show dropped packets
    {
      if ( PassPacket ) ShowPacket = 0;       // Do not show passed packets
    }

    if ( ShowPacket )
    {
      if ( !PassPacket ) iprintf( "** " );    //Mark as dropped 
      ShowIpPacket( ( PBYTE ) pb->pData );    //Show contents
    }

    if ( PassPacket )                                 //Drop packet if false
      OSFifoPost( &FecFifo, ( OS_FIFO_EL * ) pb );    //Foward Packet
    else
      FreeBuffer( pb );                               //Release buffer
  }
}

void ShowIpPacket( PBYTE pPos )
{
  iprintf( "%02X%02X%02X%02X%02X%02X ", pPos[0], pPos[1], pPos[2],
                                        pPos[3], pPos[4], pPos[5] );
  iprintf( "%02X%02X%02X%02X%02X%02X ", pPos[6], pPos[7], pPos[8],
                                        pPos[9], pPos[10], pPos[11]);
  iprintf( "%02X%02X\n",                pPos[12], pPos[13]);

  int Type = (256 * pPos[12])+ pPos[13];
  if (Type == 0x0800)               // IP packet
  {
   iprintf( "%02X %02X %02X%02X ",  pPos[14], pPos[15], pPos[16], pPos[17]);
   iprintf( "%02X%02X %02X%02X ",   pPos[18], pPos[19], pPos[20], pPos[21]);
   iprintf( "%02X %02X %02X%02X ",  pPos[22], pPos[23], pPos[24], pPos[24]);
   iprintf( "%02X%02X%02X%02X ",    pPos[26], pPos[27], pPos[28], pPos[29]);
   iprintf( "%02X%02X%02X%02X\n",   pPos[30], pPos[31], pPos[32], pPos[33]);

   if ( pPos[23] == 0x11 )            // UDP
   {
     iprintf( "%02X%02X %02X%02X ", pPos[34], pPos[35], pPos[36], pPos[37]);
     iprintf( "%02X%02X %02X%02X ", pPos[38], pPos[39], pPos[40], pPos[41]);

     int Len = (256 * pPos[38]) + pPos[39] - 8;
     iprintf( "( UDP %d bytes )\n", Len );

     int i = 0;
     if ( Len > 36 ) Len = 36;
     while ( i < Len )
     {
       iprintf( "%02X ", pPos[ i + 42 ]);
       if ( ++i == Len )
         iprintf( "\n\n" );
       else if( i == 16 )
         iprintf( "\n" );
       else if ( ( i % 4 ) == 0 )
         iprintf( " " );
     }
   }
   else if ( pPos[23] == 0x06 )       // TCP
   {
     iprintf( "%02X%02X %02X%02X ",     pPos[34], pPos[35], pPos[36], pPos[37]);
     iprintf( "%02X%02X%02X%02X ",      pPos[38], pPos[39], pPos[40], pPos[41]);
     iprintf( "%02X%02X%02X%02X ",      pPos[42], pPos[43], pPos[44], pPos[45]);
     iprintf( "%02X%02X %02X%02X ",     pPos[46], pPos[47], pPos[48], pPos[49]);
     iprintf( "%02X%02X %02X%02X\n",    pPos[50], pPos[51], pPos[52], pPos[53]);
 
     iprintf( "%02X %02X %02X %02X ",   pPos[54], pPos[55], pPos[56], pPos[57]);
     iprintf( "%02X %02X %02X %02X ",   pPos[58], pPos[59], pPos[60], pPos[61]);
     iprintf( "%02X %02X %02X %02X ",   pPos[62], pPos[63], pPos[64], pPos[65]);
     iprintf( "%02X %02X %02X %02X\n\n",pPos[66], pPos[67], pPos[68], pPos[69]);
   } 
   else                               //not UDP or TCP
   {
     iprintf( "%02X %02X %02X %02X ",   pPos[34], pPos[35], pPos[36], pPos[37]);
     iprintf( "%02X %02X %02X %02X ",   pPos[38], pPos[39], pPos[40], pPos[41]);
     iprintf( "%02X %02X %02X %02X ",   pPos[42], pPos[43], pPos[44], pPos[45]);
     iprintf( "%02X %02X% 02X %02X\n\n",pPos[46], pPos[47], pPos[48], pPos[49]);
   }
  }
  else if (Type == 0x0806)
  {
   iprintf( "%02X%02X %02X%02X ",       pPos[14], pPos[15], pPos[16], pPos[17]);
   iprintf( "%02X %02X %02X%02X\n",     pPos[18], pPos[19], pPos[20], pPos[21]);

   iprintf( "%02X%02X%02X%02X%02X%02X ",pPos[22], pPos[23], pPos[24],
                                        pPos[25], pPos[26], pPos[27]);
   iprintf( "%02X%02X%02X%02X ",        pPos[28], pPos[29], pPos[30], pPos[31]);
   iprintf( "%02X%02X%02X%02X%02X%02X ",pPos[32], pPos[33], pPos[34],
                                        pPos[35], pPos[36], pPos[37]);
   iprintf( "%02X%02X%02X%02X\n\n",     pPos[38], pPos[39], pPos[40], pPos[41]);
  }
  else
  {
   iprintf( "%02X %02X %02X %02X ",    pPos[14], pPos[15], pPos[16], pPos[17]);
   iprintf( "%02X %02X %02X %02X ",    pPos[18], pPos[19], pPos[20], pPos[21]);
   iprintf( "%02X %02X %02X %02X ",    pPos[22], pPos[23], pPos[24], pPos[25]);
   iprintf( "%02X %02X %02X %02X\n",   pPos[26], pPos[27], pPos[28], pPos[29]);

   iprintf( "%02X %02X %02X %02X ",    pPos[30], pPos[31], pPos[32], pPos[33]);
   iprintf( "%02X %02X %02X %02X ",    pPos[34], pPos[35], pPos[36], pPos[37]);
   iprintf( "%02X %02X %02X %02X ",    pPos[38], pPos[39], pPos[40], pPos[41]);
   iprintf( "%02X %02X %02X %02X\n\n", pPos[42], pPos[43], pPos[44], pPos[45]);
  }
}


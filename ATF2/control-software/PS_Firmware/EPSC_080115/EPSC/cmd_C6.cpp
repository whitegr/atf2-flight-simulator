//******************************************************************************
//  TURN ON POWER SUPPLY (0xC6 and 0xC7) (Ethernet Power Supply Controller)
//  Filename: cmd_C6.cpp
//  03/03/05
//  This command turns on the power supply. The 0xC7 command turn on the power
//  supply in reverse polarity.
//
//  1) Checks command for proper length and channel number
//  2) Checks that power supply if off
//  3) Checks for hardware faults from power-on test of calibration
//  4) Checks for remote mode
//  5) Performs DAC test, leaves DAC at zero
//  6) Checks for system ready
//  7) Clears ramp status
//  8) If 0xC6, turns on power supply
//     If 0xC7 and confiruation shows a reversing switch, turns on reverse polarity
//  9) Verifies that power supply is turned on
//
//******************************************************************************

#include <C:\Nburn\EPSC\include\includes.h>    //Include all headers for project

typedef struct          // C6, C7 command structure
{ BYTE   Ctype;         // 00 command type 0xC6 or 0XC7
  BYTE   Rsp;           // 01 response code
  BYTE   Tid;           // 02 Task ID
  BYTE   Chan;          // 03 Channel, must be zero
} __attribute__ ((packed))  Cmd_C6;

#define CMD_LEN_C6  4   // expected length of command message

typedef struct          // C6, C7 response structure
{ BYTE   Ctype;         // 00 command type, returned unchanged
  BYTE   Rsp;           // 01 response code
  BYTE   Tid;           // 02 Task ID, returned unchanged
  BYTE   Chan;          // 03 Channel, returned unchanged
  WORD   Status01;      // 04-05 Status bytes 0 and 1
} __attribute__ ((packed))  Rsp_C6;

#define RSP_LEN_C6  6   // length of response message

//********************* void Process_C6(UDPPacket * pUDP) **********************
//  pUDP  pointer to object of class UDPPacket, passed to function
//  pCmd  pointer to command structure (type Cmd_C4) set to start of UDP data
//  pRsp  pointer to response structure (type Rsp_C4) set to start of UDP data
//******************************************************************************

void Process_C6(UDPPacket * pUDP)
{ BYTE errflag = ERR;                            //error flag
  PBYTE pUdpData = pUDP->GetDataBuffer();        //get pointer to UDP data field
  Cmd_C6 * pCmd = (Cmd_C6 *) pUdpData;           //set pointer for command data
  Rsp_C6 * pRsp = (Rsp_C6 *) pUdpData;           //set pointer for response data

  if (pUDP->GetDataSize() != CMD_LEN_C6)         //check for valid command length
  { pRsp->Rsp = INVALID_MSG_LENGTH;
  }
  else if (pCmd->Chan !=0)                       //check for valid channel (0)
  { pRsp->Rsp = INVALID_CHAN_NUM;
  }
  else
  {
    if (xil.ps_status & STATUS_LOCAL)            //check for local mode
    {
      WrErrMsg("TURN ON ERROR, PS IN LOCAL MODE");
    }
    else if (~xil.ps_status & STATUS_PS_OFF)     //check for power supply ON
    {
      WrErrMsg("TURN ON ERROR, PS ALREADY ON");
    }
    //if turn on reverse polarity (0xC7), check if configured with reversing switch
    else if ((pCmd->Ctype == 0xC7) && (~xil.db_eep.Config & CONFIG_REV_SW))
    {
      WrErrMsg("C7 ERROR, NO REVERSING SWITCH");
    }
    else
    { xil.ps_cntl = PSCNTL_EN_OFF                //set power supply enable off
                  + PSCNTL_PS_ON_L               //set power supply on low
                  + PSCNTL_VLT_OFF               //set voltage mode off 06/19/06
                  + PSCNTL_ERAMP_OFF;            //set error amp enable off

      if (pCmd->Ctype == 0xC7)                   //if turn on reverse
        xil.ps_cntl = PSCNTL_REV_ON;             //set reverse polarity ON
      else                                       //else
        xil.ps_cntl = PSCNTL_REV_OFF;            //set reverse polarity OFF

      //***************** insert turn on test code here ************************
      //need check of hardware status register and calibration register
      //********************** initialize ramp registers ***********************
      xil.Ramp_State    = 0;                     //Clear Ramp State
      xil.Cur_Ramp_Seg  = 0;                     //Clear current segement number
      xil.Cur_Ramp_time = 0;                     //Clear current ramp segment time
      xil.Num_Ramp_Seg  = 0;                     //Clear number of segments
      for (short i = 0; i < 6; ++i)              //Clear all segment values
      {
        xil.Ramp_Times[i] = 0;                   //clear ramp segment[i] time
        xil.ADC_SetPnt[i] = 0;                   //clear ramp segment[i] setpoint
      }

      xil.Des_ADC = 0;                           //set DAC desired ADC to 0
      xil.Des_DAC = 0;                           //set desired DAC setpoint to 0N

      //******************* send reset pulse to power supply *******************
      xil.ps_cntl = PSCNTL_RESET_ON;             //turn ON remote reset
      OSTimeDly(TICKS_PER_SECOND / 4);           //delay 200-250 milliseconds
      xil.ps_cntl = PSCNTL_RESET_OFF;            //turn OFF remote reset

      // 01/15/08 If status not ready after reset removed, delay 2 cycles
      if (xil.ps_status & STATUS_SYS_NRDY) OSTimeDly( 2 );

      if (xil.ps_status & STATUS_SYS_NRDY)       //check for system NOT READY
      {
        WrErrMsg("TURN ON ERROR, SYS NOT READY");
      }
      else
      {
        if (DIGITAL_REG_ENABLED)
          xil.ps_cntl = PSCNTL_VLT_ON;           //set voltage mode ON 06/19/06

        xil.ps_cntl = PSCNTL_EN_ON               //set power supply enable high
                    + PSCNTL_PS_ON_H;            //set power supply ON high

        //check that power supply indicates ON within 5 seconds
        SHORT Dly = 5 * TICKS_PER_SECOND;        //set Dly to 5 seconds

        while ( ( errflag != NO_ERR )  )
        {
          if ( (xil.ser_stath & SSH_PS_ON_STAT)  //Check if PS indicates ON
           && !(xil.ps_status & STATUS_PS_OFF) ) //Check if PS status ON 04/26/06
          {
            if (DIGITAL_REG_ENABLED)
            {
              InitDigCoef();                     //initialize coefficients
              ADC1_DREG_FLAG = 0;                //clear ADC data flag
              xil.PsRegFlag = DIGITAL_REG;       //set regulation enable flag
            }
            else
            {
              xil.ps_cntl = PSCNTL_ERAMP_ON;     //set error amplifier enable ON
              xil.PsRegFlag = ANALOG_REG;        //set regulation enable flag
            }
            xil.PsOnFlag = PS_ON;                //set power supply ON flag
            errflag = NO_ERR;                    //set error flag to no error
          }
          else                                   //else
          {
            if (--Dly <= 0)                      //if 5 seconds have elasped 
            {
              xil.ps_cntl = PSCNTL_EN_OFF;       //turn off power supply
              WrErrMsg("TURN ON ERROR, PS NOT RESPONDING");   //write error message
              break;                             //exit wait loop
            }
            else                                 //else
              OSTimeDly(1);                      //continue to wait
          }
        }
      }
    }
    pRsp->Rsp = COMMAND_OK;                      //set response code
    pRsp->Status01 = Status01(errflag);          //set status bytes 0 and 1
    pUDP->SetDataSize(RSP_LEN_C6);               //set response packet data size
  }
}


//******************************************************************************
//  Ethernet Packet Filter (Ethernet Power Supply Controller)
//  Filename: EthernetFilter.cpp
//  06/05/06
//  08/09/07  Added TCP Telnet packets to filter
//
// This module allows Ethernet packets to be dropped before reaching the TCP/IP
// stack. This function is called by ShowEnetTask to flag packets to drop.
// The function returns 0 to drop packet, 1 to pass packet to TCP/IP stack
//
//  The following packets need to be passed for normal operation;
//  1) Unicast UDP packets to port 2000 (0x07D0), normal control packets
//  2) ARP request packet, need bu TCP/IP to find MAC address of controller
//  3) Ping requests (ICMP), for diagnostic purposes
//  4) Unicast UDP packets to port 0x4E42 for program code download
//  5) Broadcast UDP packets to port 0x4E42 for Netburner Auto Update
//
//  extern IPADDR EthernetIP   Netburner Ethernet IP address (32 bit)
//
//  pDat[6] = IP type, 0800 (IP) or 0806 (ARP)
//  *(IPADDR*) & pdat[15] = Destination IP address for IP packet
//  *(IPADDR*) & pdat[19] = Destination IP address for ARP packet
//  pb->bBufferFlag  set to 01 for broadcast, 0 for unicast
//
//  IP_20BYTE_HDR  (0x45) == pByte[14]
//  ETHER_TYPE_IP  (0x0800) == pDat[6]
//  ETHER_TYPE_ARP (0x0806) == pDat[6]
//
//  IP_ICMP   1    (0x01) == pByte[23]
//  IP_TCP    6    (0x06) == pByte[23]
//  IP_UDP   17    (0x11) == pByte[23]
//  IP_IGMP   2
//
//  INA_DISCARD_PORT    0x0009
//  INA_ECHO_PORT       0x0007
//  INA_TELNET_PORT     0x0017  ( 23 )
//  INA_TFTP_PORT       0x0045  ( 69 )
//
//  Known UDP ports
//  UDP_PORT            0x07D0 ( 2000 )   UDP receive port number
//  UDP_DIAG_PORT       0x07D1 ( 2001 )   UDP diagnostic port number
//  Netburner Download  0x4E42 ( 20034 )  Netburner Download
//
//  TCP_NETBURNERID_PORT (0x4E42)
//  UDP_NETBURNERID_PORT (0x4E42)
//  UDP_DHCP_CLIENT_PORT (68)
//  TFTP_RX_PORT  (1414)
//******************************************************************************

#include <C:\Nburn\TelnetEPSC\include\includes.h>    //Include all headers for project

WORD EthernetFilter( pool_buffer * pb, BYTE BroadcastFlag)
{

  PWORD pDat = ( PWORD ) pb->pData;                   // Pointer to data word
  PBYTE pByte = ( PBYTE ) pb->pData;                  // Pointer to data byte

  if ( ( pDat[6] == 0x0800 )                          // Packet type IP
    && ( pByte[14] == 0x45 ) )                        // 20 byte IP header
  {
    if ( pByte[23] == 0x11 )                          // UDP packet
    {
      if ( ( ( pDat[18] == 0x07D0 )                   // SLAC UDP port (2000)
          || ( pDat[18] == 0x07D1 ) )                 // SLAC diag UDP port
        && ( EthernetIP == *(IPADDR *) & pDat[15] )   // IP address matches
        && ( BroadcastFlag == 0 ) )                   // Not MAC broadcast
        return( 1 );
      else if ( pDat[18] == 0x4E42 )                  // Netburner UDP port
        return( 1 );
    }

    else if ( ( pByte[23] == 0x06 )                   // TCP packet
      && ( EthernetIP == *(IPADDR *) & pDat[15] )     // IP address matches
      && ( BroadcastFlag == 0 )                       // Not MAC broadcast
      && ( pDat[18] == 23 ) )                         // Telnet port (23)
      return( 1 );

    else if ( ( pByte[23] == 0x01 )                   // ICMP (PING) packet
      && ( EthernetIP == *(IPADDR *) & pDat[15] ) )   // IP address matches
      return( 1 );
  }

  else if ( ( pDat[6] == 0x0806 )                     // Packet type ARP
    && ( EthernetIP == *(IPADDR *) & pDat[19] ) )     // IP address matches
    return( 1 );

  return( 0 );                                        // No match, filter packet
}


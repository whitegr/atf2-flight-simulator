//******************************************************************************
//  CHASSIS TEST COMMAND (0xF2) (Ethernet Power Supply Controller)
//  Filename: cmd_F2.cpp
//  01/12/06
//  This message requests a new value from ADC2 for the channel specified
//
//  To request data from ADC2, the function sets ADC2_INST_REQ_FLAG, selects an
//  ADC channel (ADC2_INST_REQ_CHAN) and pends on a semaphore (ADC2_INST_SEM).
//  ADC2 reads the selected channel ,places the data in ADC2_INST_REQ_DATA, and
//  sets the semaphore to allow the function to continue.
//
//  ADC2_INST_SEM          Semaphore for instruction task
//  ADC2_INST_REQ_FLAG     ADC2 Instrustion read request flag
//  ADC2_INST_REQ_CHAN     ADC2 Instruction read request channel
//  ADC2_INST_REQ_DATA     ADC2 Instruction read request data
//
//  ADC2_INST_REQ_FLAG states
//  ADC2_REQ_OFF           No Request pending
//  ADC2_REQ_ON            Request single cycle conversion
//  ADC2_REQ4_ON           Request 4 cycle conversion 
//  ADC2_REQ_CONV          Conversion in progress
//  ADC2_REQ_DONE          Request done, data valid
//
//******************************************************************************

#include <C:\Nburn\EPSC\include\includes.h>    //Include all headers for project

typedef struct          // F2 command structure
{ BYTE   Ctype;         // 00 command type 0xF1
  BYTE   Rsp;           // 01 response code
  BYTE   Tid;           // 02 Task ID
  BYTE   Chan;          // 03 Channel number (0-3)
} __attribute__ ((packed))  Cmd_F2;

#define CMD_LEN_F2 4    //expected length of command message

typedef struct          // F2 response structure
{ BYTE   Ctype;         // 00 command type, returned unchanged
  BYTE   Rsp;           // 01 response code
  BYTE   Tid;           // 02 Task ID, returned unchanged
  BYTE   Chan;          // 03 Channel read
  float  AdcV;          // 04-07 ADC reading
} __attribute__ ((packed))  Rsp_F2;

#define RSP_LEN_F2 8   //length of response message

//********************* void Process_F2(UDPPacket * pUDP) **********************
//  pUDP  pointer to object of class UDPPacket, passed to function
//  pCmd  pointer to command structure (type Cmd_F2) set to start of UDP data
//  pRsp  pointer to response structure (type Rsp_F2) set to start of UDP data
//******************************************************************************

void Process_F2(UDPPacket * pUDP)
{
  PBYTE pUdpData = pUDP->GetDataBuffer();      //get pointer to UDP data field
  Cmd_F2 * pCmd = (Cmd_F2 *) pUdpData;         //set pointer for command data
  Rsp_F2 * pRsp = (Rsp_F2 *) pUdpData;         //set pointer for response data

  if (pUDP->GetDataSize() != CMD_LEN_F2)       //check for valid command length
  { pRsp->Rsp = INVALID_MSG_LENGTH;
  }
  else if (pCmd->Chan > 23)                    //check for valid channel (0-23)
  { pRsp->Rsp = INVALID_CHAN_NUM;
  }
  else
  {
    float V = 0.0;                             //default ADC reading
    pRsp->Rsp = COMMAND_ERR;                   //set error response code

    ADC2_INST_REQ_CHAN = pCmd->Chan;           //set channel
    ADC2_INST_REQ_FLAG = ADC2_REQ4_ON;         //set 4 cycle request flag ON

    //pend on instruction semaphore, check for timeout, request done flag
    if (OS_NO_ERR == OSSemPend(&ADC2_INST_SEM, TICKS_PER_SECOND))
    {
      if (ADC2_INST_REQ_FLAG == ADC2_REQ_DONE)
      {
        V = (float)ADC2_INST_REQ_DATA / (float)KADC;
        pRsp->Rsp = COMMAND_OK;
      }
    }
    ADC2_INST_REQ_FLAG = ADC2_REQ_OFF;         //set request state to off

    flip4((PBYTE) &V);                         //convert to little endian
    pRsp->AdcV = V;                            //set ADC Volts
    pUDP->SetDataSize(RSP_LEN_F2);             //set data size for response packet
  }
}


//******************************************************************************
//  Diagnostic Port Utilities
//  Filename: utilities.cpp
//  10/21/05
//  08/01/07  Moved Motherboard EEPROM to separate menu
//            Changed operation of backspace in spRdStr and spRdChar
//******************************************************************************

#include <C:\Nburn\EPSC\include\includes.h>    //Include all headers for project

//***************** int spRdStr(int fd, char* buf, int maxchar)*****************
// Get String from Serial Port pointed at by file descriptor fd
// This routine reads a string from the file descriptor passed to it. It reads
// characters until a carriage return is found. The function returns the number
// of characters read, or 0 for a timeout or escape character. A backspace erases
// the last character received. The function returns 0 if no data is received in
// 60 seconds.
// Note: buffer passed to function must have minimum length of maxchar + 1 to
// accomodate null termination of string.
//******************************************************************************

const char Space = ' ';
const char Backspace = 8;

int spRdStr(int fd, char* buf, int maxchar)
{
  int len = 0;                      //number of char in output buffer
  char Char = 0;                    //Character from serial port

  while ( dataavail(fd) )           //discard any characters in buffer
    ReadWithTimeout(fd, &Char, 1, 1);

  Char = 0;
  while (Char != 13)
  { 
    int numchar = ReadWithTimeout(fd, &Char, 1, 60 * TICKS_PER_SECOND);

    //if timeout (numchar < 1) or escape (Char == -1), return 0
    if ( (numchar < 1) || (Char == -1) )
    { Char = 13;
      len = 0;
    }
    else if ((Char == Backspace) && (len > 0))   //Backspace, erase last char
    { --len;
      write(fd, & Backspace, 1);                 //write backspace
      write(fd, & Space, 1);                     //write space character
      write(fd, & Backspace, 1);                 //write second backspace
    }
    else if ((Char > 31) && (len < maxchar))     //if printable char (>31)
    { buf[len++] = Char;                         //add character to buffer
      write(fd, & Char, 1);                      //echo character
    }
  }
  writestring(fd, "\r\n");                       //write carriage return
  buf[len] = 0;                                  //terminate string with null
  return (len);
}

//**************************** int spRdChar(int fd)*****************************
// Get a character from Serial Port pointed at by file descriptor fd
// This routine reads a character from the file descriptor passed to it. It does
// not return until a carriage return is found. The function returns the character,
// or 0 for a timeout or escape character. Each new character overwrites the
// last character entered. The function times out in 60 seconds.
//******************************************************************************

char spRdChar(int fd, int timeout)
{
  char Char = 0;                    //Character from serial port
  char Buf = 0;                     //Character buffer

  while ( dataavail(fd) )           //discard any characters in buffer
    ReadWithTimeout(fd, & Char, 1, 1);

  Char = 0;
  while (Char != 13)
  {
    int numchar = ReadWithTimeout(fd, &Char, 1, timeout * TICKS_PER_SECOND);

    //if timeout (numchar < 1) or escape (Char == -1), return 0
    if ( (numchar < 1) || (Char == -1) )
    {
      Char = 13;
      Buf = 0;
    }
    else if (Char > 31)                     //if printable char (>31)
    { 
      if ( Buf > 0 )                        //if character already entered
        write(fd, & Backspace, 1);          //send backspace to clear
      Buf = Char;                           //set buffer to character
      write(fd, & Buf, 1);                  //echo character
    }
  }
  writestring(fd, "\r\n");                  //write carriage return
  return (Buf);
}

//************************ void spShowTopHelp(int fd) **************************
//  Write Top Menu to serial port
//******************************************************************************

void spShowTopHelp(int fd)
{
  writestring(fd, "Top Menu, select sub menu\r\n");
  writestring(fd, "'1' Program Daughter Board EEPROM\r\n");
  writestring(fd, "'2' Chassis Diagnostics\r\n");
  writestring(fd, "'3' Network Diagnostics\r\n");
  writestring(fd, "'4' Chassis Calibration\r\n");
  writestring(fd, "'R' Reset\r\n");
}

//*********************** void spShowDbEEPHelp(int fd) *************************
//  Write Daughter Board EEP Configuration Help menu to serial port
//******************************************************************************

void spShowDbEEPHelp(int fd)
{
  writestring(fd, "Help Menu, Program DbEEPROM Commands\r\n");
  writestring(fd, "'A' Set IP Address\r\n");
  writestring(fd, "'B' Set Coefficients (IVA)\r\n");
  writestring(fd, "'C' Set Configuration\r\n");
  writestring(fd, "'D' Set Digital Reg Coef\r\n");
  writestring(fd, "'E' Set Ethernet Config\r\n");
  writestring(fd, "'M' Set Magnet Name\r\n");
  writestring(fd, "'W' Write Daughterbrd EEPROM\r\n");
  writestring(fd, "'Z' Return to Top Menu\r\n");
}

//************************ void spShowDiagHelp(int fd) *************************
//  Write diagnostic command help menu to serial port
//******************************************************************************

void spShowDiagHelp(int fd)
{
  writestring(fd, "Help Menu, Diagnostic Commands\r\n");
  writestring(fd, "'A' ADC2 Data\r\n");
  writestring(fd, "'C' Calibration Coefficients\r\n");
  writestring(fd, "'D' Daughterbrd EEPROM Data\r\n");
  writestring(fd, "'E' Error Messages\r\n");
  writestring(fd, "'H' Fault History Buffers\r\n");  
  writestring(fd, "'I' Interlock Status\r\n");
  writestring(fd, "'L' Show Local Control LEDs\r\n"); 
  writestring(fd, "'M' Motherbrd EEPROM Data\r\n");
  writestring(fd, "'P' Power Supply Module Data\r\n");
  writestring(fd, "'R' Show Ramp Data\r\n");
  writestring(fd, "'X' EEPROM Data Image\r\n");
  writestring(fd, "'V' Version\r\n");
  writestring(fd, "'Z' Return to Top Menu\r\n");
}

//************************ void spShowEEPHelp(int fd) **************************
//  Write Network Diagnostic Help menu to serial port
//******************************************************************************

void spShowNetHelp(int fd)
{
  writestring(fd, "Help Menu, Network Diagnostics\r\n");
  writestring(fd, "'A' Show ARP Table\r\n");
  writestring(fd, "'B' Show Buffer Status\r\n");
  writestring(fd, "'C' Show MAC Buffer Status\r\n");
  writestring(fd, "'E' Show Ethernet\r\n");
  writestring(fd, "'M' Show MII Registers\r\n");
  writestring(fd, "'N' Show Network Data\r\n");
  writestring(fd, "'P' Show Pool Buffer Data\r\n");
  writestring(fd, "'S' Dump UCOS Stacks\r\n");
  writestring(fd, "'T' Dump UCOS Tasks\r\n");
  writestring(fd, "'X' Show Stack Data\r\n");
  writestring(fd, "'Z' Return to Top Menu\r\n");
}

//************************ void spShowCalHelp(int fd) **************************
//  Write Calibration/Motherboard EEPROM Help menu to serial port
//******************************************************************************

void spShowCalHelp(int fd)
{
  writestring(fd, "Help Menu, Calibration Commands\r\n");
  writestring(fd, "'1' ADC1 gain pot adj\r\n");
  writestring(fd, "'2' ADC2 gain pot adj\r\n");
  writestring(fd, "'3' DAC offset pot adj\r\n");
  writestring(fd, "'4' DAC gain pot adj\r\n");
  writestring(fd, "'R' Set Reference\r\n");
  writestring(fd, "'S' Set Serial Number\r\n");
  writestring(fd, "'L' ADC Linearity Coef\r\n");
  writestring(fd, "'W' Write Motherbrd EEPROM\r\n");
  writestring(fd, "'Z' Return to Top Menu\r\n");
}

//*************************** void spReset(int fd) *****************************
//  Reset the processor
//  08/10/07 added reset strings and delays, disabled watchdog for local mode
//******************************************************************************

void spReset(int fd)
{
  writestring(fd,
    "Type 'H' for Hard Reset, 'S' for Soft reset, 'W' for Watchdog reset: ");

  char Char = (char) toupper( spRdChar(fd) );

  if (Char == 'S')
  {
    writestring(fd, "Soft Reset\r\n");
    OSTimeDly(TICKS_PER_SECOND / 4);     //delay 200 millisecond
    xil.ResetType = UART_RESET;
    xil.sys = SYS_SOFT_RESET;
    puts("This message should never be seen if reset works");
  }
  else if (Char == 'H')
  {
    writestring(fd, "Hard Reset\r\n");
    OSTimeDly(TICKS_PER_SECOND / 4);     //delay 200 millisecond
    xil.sys = SYS_HARD_RESET;
  }
  else if (Char == 'W')
  {
    if (xil.ps_status & STATUS_LOCAL)    //check for local mode
    {
      writestring(fd, "Watchdog Reset disabled in local mode\r\n");
    }
    else    // not local mode, force watchdog reset
    {
      writestring(fd, "Waiting for Watchdog Reset\r\n");
      xil.WatchdogFlag = ForceWatchdogFlag;
    }
  }
}

//************************* void spDiagnostic(int fd) **************************
//  Diagnostic Function Select Tree
//******************************************************************************

void spDiagnostic(int fd)
{
  OSTimeDly(TICKS_PER_SECOND / 2);   //delay 1/2 second
  WORD Menu = 0;                     //Menu index
  char prompt = '>';                 //prompt character, waiting for command
  char Char;                         //command character from serial port

  spShowTopHelp(fd);
  write(fd, &prompt, 1);             //send prompt character to serial port

  while (1)
  {
    Char = spRdChar(fd, 0);        //wait for character, no timeout

    if (Char > -1)
    {
      Char = (char) toupper((int) Char);   //convert lower case to upper

      if (Menu == 0)
      {
        if      (Char == '1') {spShowDbEEPHelp(fd);  Menu = 1;}
        else if (Char == '2') {spShowDiagHelp(fd);   Menu = 2;}
        else if (Char == '3') {spShowNetHelp(fd);    Menu = 3;}
        else if (Char == '4') {spShowCalHelp(fd);    Menu = 4;}
        else if (Char == 'R')  spReset(fd);
        else                   spShowTopHelp(fd);
      }
      else if (Menu == 1)
      {
        if      (Char == 'A')  spSetIPStk(fd);
        else if (Char == 'B')  spSetIVA(fd);
        else if (Char == 'C')  spSetConfig(fd);
        else if (Char == 'D')  spSetDigCoef(fd);
        else if (Char == 'E')  spSetEnetConfig(fd);
        else if (Char == 'M')  spSetMagID(fd);
        else if (Char == 'W')  spWrDbEEP(fd);
        else if (Char == 'Z') {spShowTopHelp(fd); Menu = 0;}
        else                   spShowDbEEPHelp(fd);
      }
     else if (Menu == 2)
      {
        if      (Char == 'A')  spShowADC2Dat(fd);
        else if (Char == 'C')  spShowCalCoef(fd);
        else if (Char == 'D')  spShowDbEEP(fd);
        else if (Char == 'E')  spShowErrMsg(fd);
        else if (Char == 'H')  spShowFltHistBuf(fd);
        else if (Char == 'I')  spShowInterlockStat(fd);
        else if (Char == 'L')  spShowLocCntlLEDs(fd);         
        else if (Char == 'M')  spShowMbEEP(fd);
        else if (Char == 'P')  spShowPSModDat(fd);
        else if (Char == 'R')  spShowRamp(fd);
        else if (Char == 'X')  spShowEEPHex(fd);
        else if (Char == 'V')  spShowVersion(fd);
        else if (Char == 'Z') {spShowTopHelp(fd); Menu = 0;}
        else                   spShowDiagHelp(fd);
      }
      else if (Menu == 3)
      {
        if      (Char == 'A')  ShowArp();
        else if (Char == 'B')  spShowBuffers(fd);
        else if (Char == 'C')  spShowEMACBuffers(fd);
        else if (Char == 'E')  spShowEthernet(fd);
        else if (Char == 'M')  spShowMII(fd);
        else if (Char == 'N')  spNetwork(fd);
        else if (Char == 'P')  spShowPoolBufData(fd);
        else if (Char == 'S')  spDumpTCBStacks(fd);    // uCOS OSDumpTCBStacks()
        else if (Char == 'T')  spShowTasks(fd);
        else if (Char == 'X')  spShowStack(fd);
        else if (Char == 'Z') {spShowTopHelp(fd); Menu =0;}
        else                   spShowNetHelp(fd);
      }
      else if (Menu == 4)
      {
        if      (Char == 'R')  spSetAdcRef(fd);
        else if (Char == 'S')  spSetSerNum(fd);
        else if (Char == 'L')  spAdcLinCoef(fd);
        else if (Char == 'W')  spWrMbEEP(fd);
        else if (Char == '1')  spShowAdc1Gain(fd);
        else if (Char == '2')  spShowAdc2Gain(fd);
        else if (Char == '3')  spShowDacOffset(fd);
        else if (Char == '4')  spShowDacGain(fd);
        else if (Char == 'Z') {spShowTopHelp(fd); Menu = 0;}
        else                   spShowCalHelp(fd);
      }

      if      (Menu == 1) writestring(fd, "DbEEP");
      else if (Menu == 2) writestring(fd, "Diag");
      else if (Menu == 3) writestring(fd, "Net");
      else if (Menu == 4) writestring(fd, "Cal");

      write(fd, &prompt, 1);         //send prompt character to serial port
    }
  }
}


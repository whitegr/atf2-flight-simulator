//********************** void spEnableSmartTrap(int fd) ************************
// Enable Programmable Interrupt Timer 3 (pit[3]) to call smart_trap using the
// non-maskable interrupt (7). The counter is reset by the ADC task.
// sim.pit[3].pcsr   control register
//                 = x5xx    set frequency to CPU clock divided by 64
//                 = xx1x    enable direct writing of count register (pmr)
//                 = xxx8    interrupt enable
//                 = xxx4    interrupt flag (write 1 to clear)
//                 = xxx1    timer enable bit
// sim.pit[3].pmr    count register, interrupt when count reaches zero
//******************************************************************************

extern "C"    //declaration of external functions
{
  void SetIntc( int intc, long func, int vector, int level, int prio );
  void smart_trap_asm();
}

void spEnableSmartTrap(int fd)
{
  if ( sim.pit[3].pcsr & 0x000F )      //if counter enabled, disable
  {
    sim.pit[3].pcsr = 0x0514;          //disable counter, clear interrupt flag
    writestring(fd, "Smart Trap Interrupt Disabled\r\n" );
  }
  else                                 //enable counter and interrupt
  {
    sim.pit[3].pcsr = 0x0514;          //disable counter, clear interrupt flag
    sim.pit[3].pmr = 0xFFFF;           //set count to maximum
    SetIntc( 0, ( long ) & smart_trap_asm, 58, 7, 1 );  //set interrupt vector
    sim.pit[3].pcsr = 0x0519;          //enable counter and interrupt
    writestring(fd, "Smart Trap Interrupt Enabled\r\n" );
  }
}

    //reset smart_trap interrupt timer (uSec) 07/08/06 DJM
    sim.pit[3].pmr = 5000;   //**** 5000 uSec delay to interrupt 7 and smart_trap ****


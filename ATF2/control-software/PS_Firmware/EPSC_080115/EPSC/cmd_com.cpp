//******************************************************************************
//  Command support functions (Ethernet Power Supply Controller)
//  Filename: cmd_com.cpp
//  02/18/05
//******************************************************************************

#include <C:\Nburn\EPSC\include\includes.h>    //Include all headers for project

//************************* SEND UDP RESPONSE MESSAGE **************************
//
//  This function sends a response UDP packet. The packet is returned to the
//  source IP address and port.
//
//******************************************************************************

void SendResponse(UDPPacket * pUDP)
{
  IPADDR IpDestAdr = pUDP->GetSourceAddress();      //save source IP address
  pUDP->SetDestinationPort(pUDP->GetSourcePort());  //set dest port = rcv src port

//** Temp Windows Kluge, windows does not send from the same port it receives from
//  pUDP->SetDestinationPort(UDP_PORT);

  pUDP->SetSourcePort(UDP_PORT);                    //set source port

//  PoolPtr pMsg = pUDP->GetPoolPtr();
//  puts("Response Packet");
//  ShowData(pMsg->pData, 48);

  pUDP->Send(IpDestAdr);                            //send and release buffer
}

//****************************** WORD Status01() *******************************
//
// Generate status bytes 0 and 1 and return in unsigned word. Bits D3,D4, and
// D10-15 are read directly from xil.status (marked with *).
//
//       Status byte 0                    Status byte 1
// *D15  Local Mode                   D7  Spare
// *D14  Reverse Polarity             D6  Spare
// *D13  Ramp Hold, state 1           D5  Spare
// *D12  Ramp On, state 2            *D4  System not ready
// *D11  Ramp On, state 3            *D3  Aux Transductor not ready
// *D10  Power Supply Off             D2  Calibration Failure
//  D9   Command Error                D1  ADC Failure
//  D8   Command OK                   D0  Error Message Flag
//
//  Ramp State 1, ramp hold, waiting for hardware ramp signal
//  Ramp State 2, ramp on, hardware ramp/hold lines active
//  Ramp State 3, ramp on, hardware ramp/hold lines disabled
//
//******************************************************************************

WORD Status01(BYTE CmdErr)
{ WORD stat = xil.ps_status;
  if ( CmdErr ) stat |= STATUS_CMD_ERR; else stat |= STATUS_CMD_OK;
  if ( CalErr ) stat |= STATUS_CAL_ERR;
  if ( HrdFlt ) stat |= STATUS_HRD_FLT;
  if ( ErrMsgWrPtr != ErrMsgRdPtr ) stat |= STATUS_ERR_MSG;
  return stat;
}

//****************************** WORD Status23() *******************************
//
// Generate status bytes 2 and 3 and return in unsigned word. This status is read
// directly from the Xilinx (xil.intl_stat).
//
//       Status byte 2                    Status byte 3
//  D15  Control Fault                D7  Spare
//  D14  Ground Fault                 D6  Voltage Mode
//  D13  Reg Transductor not ready    D5  Power Supply ON Status
//  D12  Power Supply not ready       D4  Power Supply Status 3
//  D11  Magnet Interlock 3           D3  Power Supply Status 2
//  D10  Magnet Interlock 2           D2  Power Supply Status 1
//  D9   Magnet Interlock 1           D1  Power Supply Status 0
//  D8   Magnet Interlock 0           D0  Fault Latch on
//
//******************************************************************************

WORD Status23()
{ return xil.intl_stat;
}

//************************ DWORD Chg100To120(WORD time) ************************
//
//  last revised: 02/08/05
//
//  Instructions use unsigned integers (0 to 65535) to set the ramp time. Each count
//  is 1/100 second for normal ramps or 1/20 second for slow ramps. The controller
//  uses 32 bit integers for time with each count being 1/120 second. This function
//  converts instruction time to controller time.
//
//  Tc = controller time (32 bit), Ti = instruction time (16 bit)
//
//  Tc = ((6 * Ti) + 2) / 5   for normal ramps (100 hz to 120 hz)
//  Tc = 6 * Ti               for slow ramps   (20 hz to 120 hz)
//
//  Ti = ((5 * Tc) + 3) / 6   for normal ramps (120 hz to 100 hz)
//  Ti = (Tc + 3) / 6         for slow ramps   (120 hz to 20 hz)
//
//******************************************************************************

DWORD Chg100To120(WORD time)
{ if (xil.db_eep.Config & CONFIG_SLOW_RAMP)
    return (6 * (DWORD) time);            //convert 20hz to 120 hz time
  else
    return (6 * (DWORD) time + 2) / 5;    //convert 100hz to 120 hz time
}

DWORD Chg120To100(DWORD time)
{ if (xil.db_eep.Config & CONFIG_SLOW_RAMP)
    return ((time + 3) / 6);        //convert 120hz to 20 hz time
  else
    return ((5 * time + 3) / 6);    //convert 120hz to 100 hz time
}

//******************************************************************************
//  Reverse order of bytes to convert between big endian (Motorola) and little
//  endian (Intel). The Bitbus message structure uses little endian and the
//  Netburner board uses a big endian (Motorola) processor.
//******************************************************************************

void flip2(PBYTE pDat)                           //reverse order of 2 bytes
{ char temp = pDat[0];
  pDat[0] = pDat[1];
  pDat[1] = temp;
}

void flip4(PBYTE pDat)                           //reverse order of 4 bytes
{
  char temp = pDat[0];                           //reverse bytes 0 and 3
  pDat[0] = pDat[3];
  pDat[3] = temp;
  temp = pDat[1];                                //reverse  bytes 1 and 2
  pDat[1] = pDat[2];
  pDat[2] = temp;
}

//******************************************************************************
//  To request data from ADC2, the function sets ADC2_INST_REQ_FLAG, selects an
//  ADC channel (ADC2_INST_REQ_CHAN) and pends on a semaphore (INST_SEM).
//  The ADC reads the selected channel and places the data in ADC2_INST_REQ_DATA.
//  The ADC then sets the semaphore to allow the function to continue.
//
//  ADC2_INST_SEM          Semaphore for instruction task
//  ADC2_INST_REQ_FLAG     ADC2 Instrustion read request flag
//  ADC2_INST_REQ_CHAN     ADC2 Instruction read request channel
//  ADC2_INST_REQ_DATA     ADC2 Instruction read request data
//
//  ADC2_INST_REQ_FLAG states
//  ADC2_REQ_OFF           ADC2 request off state
//  ADC2_REQ_ON            ADC2 request on state
//  ADC2_REQ_CONV          ADC2 request convert state
//  ADC2_REQ_DONE          ADC2 request done state, data valid
//
//  The function returns 1 if the semaphore returns without a timeout and the
//  request flag indicates request done, else function returns zero
//******************************************************************************
BYTE InstReadADC2(WORD chan)
{
  BYTE ErrFlag = ERR;
  if (ADC2_INST_REQ_FLAG == ADC2_REQ_OFF)  //check for request flag off
  {
    ADC2_INST_REQ_CHAN = chan;             //set channel
    ADC2_INST_REQ_FLAG = ADC2_REQ_ON;      //set request flag ON

    //pend on instruction semaphore, check for timeout, request done flag
    if (OS_NO_ERR == OSSemPend(&ADC2_INST_SEM, TICKS_PER_SECOND))
    {
      if (ADC2_INST_REQ_FLAG == ADC2_REQ_DONE) ErrFlag = NO_ERR;
    }
  }

  ADC2_INST_REQ_FLAG = ADC2_REQ_OFF;
  return (ErrFlag);
}

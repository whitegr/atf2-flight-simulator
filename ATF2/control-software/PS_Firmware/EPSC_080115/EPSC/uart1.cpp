//******************************************************************************
//  UART1 Power Suppy Serial Link (Ethernet Power Supply Controller)
//  Filename: uart1.cpp
//  07/18/07  Modifiaction for OCEM power modules for KEK ATF2 project
//
//  This task uses a serial port (UART 1) running at 9600 baud to communicate
//  with a OCEM power supply interface board. The interface allows individual
//  modules in a multi-module power supply to be enabled and disabled, and to
//  report their output current.
//
//  void Uart1Rcv(void * pd)   Uart1 recieve task
//  void Uart1Xmt(void * pd)   Uart1 transmit task
//  void StartUart1RcvTask()   Start Uart1 recieve task
//  void StartUart1XmtTask()   Stark Uart1 transmit task
//
//  ModDataValid is set true when 5 valid messages are recieved (RcvCnt > 5),
//  set false when 5 transmissions do not receive responses (RcvCnt < 0)
//******************************************************************************

#include <C:\Nburn\EPSC\include\includes.h>    //Include all headers for project

// Local static variables
int fdcom1 = 0;                       //Handle for UART1
short RcvCnt = 0;                     //Receive message counter

//***************** Convert ASCII Hexidecimal digit to number ******************
//  if character is hexidecimal, return value, else return 255
//******************************************************************************

BYTE AscHexToNum( char c )
{
  if (( c >= '0' ) && ( c <= '9' )) return ( (BYTE) ( c - '0' ) );
  if (( c >= 'a' ) && ( c <= 'f' )) return ( (BYTE) ( c - 'a' ) + 10 );
  if (( c >= 'A' ) && ( c <= 'F' )) return ( (BYTE) ( c - 'A' ) + 10 );
  return ( 255 );
}

//**************************** UART 1 Receive Task *****************************
// The UART 1 receive task receives the messages from the power supply interface
// board over a serial link. There are only two types of messages; status and
// module output current.
//
// The status messages is 7 ASCII characters in the following format;
// 's', n, fh, fl, dh, dl, 0x0D
// transmission must start with a lower case 's' (0x73)
// n = number of modules present ( <= MaxNumMod )
// fh, fl = '00' to '1F', hexidecimal representation of module fault bits
// dh, dl = '00' to '1F', hexidecimal representation of module disable bits
// 0x0D, transmission must end with a carriage return
//
// The module current message is 6 ASCII characters in the following format;
// 'i', n, ih, im, il, 0x0D
// transmission must start with a lower case 'i' (0x69)
// n = module number ( <= MaxNumMod )
// ih,im,il = '000' to 'FFF', hexidecimal representation of module current
// 0x0D, transmission must end with a carriage return
//
//******************************************************************************

void Uart1Rcv(void * pd)
{ pd=pd;
  // Initialize UART1
  fdcom1 = OpenSerial(1,               //serial port to use
                      9600,            //baudrate
                      1,               //number of stop bits
                      8,               //number of data bits
                      eParityNone );   //parity

  while (1)
  {
    // static variables to hold data between characters from UART1
    static WORD  RcvState = 0;         //Receive State
    static BYTE  TmpNumMod = 0;        //Temporary Number of modules
    static BYTE  TmpModFault = 0;      //Temporary Fault byte storage
    static BYTE  TmpModDisable = 0;    //Temporary Disable byte storage
    static BYTE  TmpModNum = 0;        //Temporary module number
    static SHORT TmpModCur = 0;        //Temporary module current storage
    static char  Char;                 //Buffer for UART1 character reads

    // Suspend task until data available from UART1, read 1 character max
    int NumChar = read(fdcom1, & Char, 1);

    if (NumChar > 0)
    {
      if      ( Char == 's' )   { RcvState = 0x10; }
      else if ( Char == 'i' )   { RcvState = 0x20; }
      else if ( RcvState > 0 )
      {
        BYTE HexDig = AscHexToNum( Char );
        if ( HexDig < 16 )
        {
          if ( RcvState == 0x10 )        //Status message, number of modules
          {
            if (( HexDig > 0) && ( HexDig <= MaxNumMod ))
            { RcvState = 0x11; TmpNumMod = HexDig; }
            else
            { RcvState = 0; }
          }
          else if ( RcvState == 0x11 )   //Status message, upper fault nibble
          { RcvState = 0x12; TmpModFault  = HexDig * 16; }
          else if ( RcvState == 0x12 )   //Status message, lower fault nibble
          { RcvState = 0x13; TmpModFault += HexDig; }
          else if ( RcvState == 0x13 )   //Status message, upper disable nibble
          { RcvState = 0x14; TmpModDisable  = HexDig * 16; }
          else if ( RcvState == 0x14 )   //Status message, lower disable nibble
          { RcvState = 0x15; TmpModDisable += HexDig; }

          else if ( RcvState == 0x20 )   //Current request, module number
          {
            if (( HexDig > 0) && ( HexDig <= MaxNumMod ))
            { RcvState = 0x21; TmpModNum = HexDig; }
            else
            { RcvState = 0; }
          }
          else if ( RcvState == 0x21 )   //Current request, high nibble
          { RcvState = 0x22; TmpModCur  = (SHORT) HexDig * 256; }
          else if ( RcvState == 0x22 )   //Current request, middle nibble
          { RcvState = 0x23; TmpModCur += (SHORT) HexDig * 16; }
          else if ( RcvState == 0x23 )   //Current request, low nibble
          { RcvState = 0x24; TmpModCur += (SHORT) HexDig; }
          else
          { RcvState = 0; }     //no valid state found, reset Receive State
        }

        else if ( 0x0D == (BYTE) Char)   //Carriage return, end of message 
        {
          if ( RcvState == 0x15 )        //End of module status response
          {
            NumMod     = TmpNumMod;
            ModFault   = TmpModFault;
            ModDisable = TmpModDisable;
            ++RcvCnt;
          }

          else if ( RcvState == 0x24 )   //End of module current request
          {
            ModCur[ TmpModNum - 1 ] = TmpModCur;
            ++RcvCnt;
          }

          if ( RcvCnt > 5 )              //If 5 messages received
          {
            ModDataValid = 1;            //Set Module Data Valid flag
            RcvCnt = 5;                  //reset receive count
          }

          RcvState = 0;        // message done, set receive state to 0
        }

        else
        { RcvState = 0; }      // invalid character, set receive state to 0
      }
    }
  }
}

//**************************** UART 1 Transmit Task ****************************
// The task waits for module enable or disable requests passed in a queue from
// Ethernet UDP commands. If none are received, the "pend on queue" times out
// 10 times per second and generates status and module current requests.
// A counter (Num) cycles from 0 to the number of modules (NumMod). A value of 0
// generates a status request and nonzero generate a module current request.
//
// For every 10 messages sent, the module data valid flag (ModDataValid) is set
// true if at least 8 replies were received.
//******************************************************************************

void Uart1Xmt(void * pd)
{ pd=pd;

  //Set auto transmission time ( OSQPend timeout for Uart1XmtQ )
  #define Timeout ( TICKS_PER_SECOND / 10 )   //Transmit timer

  OSQInit( & Uart1XmtQ, Uart1XmtQData, 8);    //initialize queue structure

  char Buf[8];                     //buffer for output string
  int Len;                         //number of char in buffer
  BYTE Num = 0;                    //Module count

  while (1)
  {
    BYTE Err;                     //Error byte required by function, not used
    //  void * OSQPend( OS_Q *pq, WORD timeout, BYTE *err );
    OSTimeDly( TICKS_PER_SECOND / 10 );       // 09/27/07 delay 0.1 second
    BYTE Cmd  = (BYTE)(DWORD) OSQPend( & Uart1XmtQ, Timeout, & Err );
    //Double cast (void * > DWORD > BYTE) avoids complier warnings

    if ( Cmd == 0 )                    //timeout, no command received
    {
      if ( Num == 0 )                  //send status request
      {
        Buf[0] = 's';                  //Set status command
        Buf[1] = 0x0D;                 //Add carriage return
        Len = 2;                       //Set string length
      }
      else                             //send module current request
      {
        Buf[0] = '0' + (char) Num;     //Set channel number
        Buf[1] = 'i';                  //Set current request command
        Buf[2] = 0x0D;                 //Add carriage return
        Len = 3;                       //Set string length
      }

//      if ( ++Num > NumMod )            //Increment module number
      if ( ++Num > MaxNumMod )         //Increment module number
        Num = 0;                       //Set to zero if greater then MaxNumMod
    }
    else                               //Command in queue
    {
      Len = 3;                                   //Set string length
      Buf[0] = '0' + (char)( Cmd & 0x0F );       //Set channel number
      Buf[2] = 0x0D;                             //Add carriage return
      Cmd = Cmd & 0xF0;
      if      ( Cmd == 0x10 ) Buf[1] = 'e';      //Set enable command
      else if ( Cmd == 0x20 ) Buf[1] = 'd';      //Set disable command
      else                    Len = 0;           //Disable transmission
    }

    if ( Len > 0 )
    {
      write( fdcom1, & Buf[0], Len );  //Send string to UART1
      if ( ModDataValid == 1 )         //set Module Data Valid flag
      { 
        if ( --RcvCnt < 0 )            //Decrement message count
        {
          ModDataValid = 0;            //clear Module Data Valid flag
          RcvCnt = 0;                  //reset receive count
        }
      }
    }

  }
}

//************************* Start UART1 Recieve Task() *************************

DWORD  Uart1RcvStk[USER_TASK_STK_SIZE] __attribute__(( aligned(16) ));

void StartUart1RcvTask()
{
  OSTaskCreate(Uart1Rcv,                          //address of task code
               (void *) 0,                        //optional argument pointer
               &Uart1RcvStk[USER_TASK_STK_SIZE],  //top of stack
               Uart1RcvStk,                       //bottom of stack
               UART1_RCV_PRIO);                   //task priority
}

//************************* Start UART1 Transmit Task() ************************

DWORD  Uart1XmtStk[USER_TASK_STK_SIZE] __attribute__(( aligned(16) ));

void StartUart1XmtTask()
{
  OSTaskCreate(Uart1Xmt,                          //address of task code
               (void *) 0,                        //optional argument pointer
               &Uart1XmtStk[USER_TASK_STK_SIZE],  //top of stack
               Uart1XmtStk,                       //bottom of stack
               UART1_XMT_PRIO);                   //task priority
}

//******************************************************************************
/*
Protocol for EPSC to Interface Board UART

The EPSC power supply interface communicates with a UART in the OCEM interface
board to read status information and disable individual power supply modules.

The Ethernet Power Supply Controller (EPSC) and OCEM interface board shall
communicate using a RS232 interface. The interface protocol shall follow be
designed with the following objectives;

1)	The EPSC will be the master device and the interface board  will act as a
    slave. The interface board will only transmit when a request is received
    from the EPSC.
2)	No RS232 communications will be required for normal operation of the power
    modules. All individual module disable bits will default to NOT disabled.
3)	All commands and responses will be lower case ASCII characters ('a' to 'z').
    All data with be hexadecimal characters ('0' to '9', 'A' to 'F ') . All
    commands and responses will end with carriage return (0x0D).

The interface board will buffer characters until a carriage return (0x0D) is
received. The character immediately preceding the carriage return will be the
command. When required, the module number will precede the command character.
All invalid commands should be discarded.

EPSC Command Messages

The EPSC will send four different command messages; module current request,
disable module request, enable module request, and status request.

The module current request will be 3 characters long. The interface board should
reply with the current for the requested module.

1	'n'   Module number ('1' to '5')
2	'i'   Module #n current request
3	0x0D  Carriage return

The disable module request will be 3 characters long. The interface board should
reply with a status message.

1	'n'   Module number ('1' to '5')
2	'd'   Disable module #n 
3	0x0D  Carriage return

The enable module request will be 3 characters long. Sending a zero for module
number should enable all modules. The interface board should reply with a status
message.

1	'n'   Module number ('0' to '5')
2	'e'   Enable module #n
3	0x0D  Carriage return

The status request will be 2 characters long. The interface board should reply
with a status message.

1	's'   Status request
2	0x0D  Carriage return

Interface Board Response Messages

The interface board will provide two types of responses; power module status and
individual module output current.

The interface board status message will be a 7 characters long.

1	's'	Status response 
2	'n'	Number of modules ('1' to '5')
3-4	"nn"	Faulted modules, two character hexidecimal
5-6	"nn"	Disabled modules, two character hexidecimal
7	0x0D	Carriage return

The interface board module current message will be 6 characters long.

1	'i'   Module current
2	'n'   Module number ('1' to '5')
3-5	"nnn" Module current, in hexadecimal ("000"  to "FFF")
6	0x0D  Carriage return 

The module fault and disable status will be encoded into two character hex
numbers, each fault or disable bit being assigned a hexadecimal value.

Module #1	0x01
Module #2	0x02
Module #3	0x04
Module #4	0x08
Module #5	0x10
Module #6	0x20	(reserved for future use)
Module #7	0x40	(reserved for future use)
Module #8	0x80	(reserved for future use)

Thus if modules #3 and #5 had faults, the fault status would be 0x14. 

RS232 Interface
9600 baud
8 data bits
No parity
1 stop bit
No hardware or software flow control

Commands
'd'	100	0x64	Disable module
'e'	101	0x65	Enable module
'i'	105	0x69	Module current
's'	115	0x73	Status

Command/Response Termination
'cr'	13	0x0D

Numbers (hexidecimal)
'0'	48	0x30
'1'	49	0x31
'2'	50	0x32
'3'	51	0x33
'4'	52	0x34
'5'	53	0x35
'6'	54	0x36
'7'	55	0x37
'8'	56	0x38
'9'	57	0x39
'A'	65	0x41
'B'	66	0x42
'C'	67	0x43
'D'	68	0x44
'E'	69	0x45
'F'	70	0x46
*/
//******************************************************************************

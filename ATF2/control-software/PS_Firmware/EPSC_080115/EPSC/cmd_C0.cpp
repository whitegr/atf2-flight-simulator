//******************************************************************************
//  SHORT STATUS COMMAND (0xC0) (Ethernet Power Supply Controller)
//  Filename: cmd_C0.cpp
//  03/03/05
//  This message requests a new value from ADC2 for the power supply current
//  (regulated transductor) and status bytes 1 and 2.
//
//******************************************************************************

#include <C:\Nburn\EPSC\include\includes.h>    //Include all headers for project

typedef struct          // C0 command structure
{ BYTE   Ctype;         // 00 command type 0xC0
  BYTE   Rsp;           // 01 response code
  BYTE   Tid;           // 02 Task ID
  BYTE   Chan;          // 03 Channel number (must be zero)
} __attribute__ ((packed))  Cmd_C0;

#define CMD_LEN_C0 4    //expected length of command message

typedef struct          // C0 response structure
{ BYTE   Ctype;         // 00 command type, returned unchanged
  BYTE   Rsp;           // 01 response code
  BYTE   Tid;           // 02 Task ID, returned unchanged
  BYTE   Chan;          // 03 Channel, returned unchanged
  WORD   Status01;      // 04-05 Status bytes 0 and 1
  float  PSCur;         // 06-09 PS output current read from reg transductor
} __attribute__ ((packed))  Rsp_C0;

#define RSP_LEN_C0 10   //length of response message

//********************* void Process_C0(UDPPacket * pUDP) **********************
//  pUDP  pointer to object of class UDPPacket, passed to function
//  pCmd  pointer to command structure (type Cmd_C0) set to start of UDP data
//  pRsp  pointer to response structure (type Rsp_C0) set to start of UDP data
//******************************************************************************

void Process_C0(UDPPacket * pUDP)
{
  PBYTE pUdpData = pUDP->GetDataBuffer();      //get pointer to UDP data field
  Cmd_C0 * pCmd = (Cmd_C0 *) pUdpData;         //set pointer for command data
  Rsp_C0 * pRsp = (Rsp_C0 *) pUdpData;         //set pointer for response data

  if (pUDP->GetDataSize() != CMD_LEN_C0)       //check for valid command length
  { pRsp->Rsp = INVALID_MSG_LENGTH;
  }
  else if (pCmd->Chan !=0)                     //check for valid channel (0)
  { pRsp->Rsp = INVALID_CHAN_NUM;
  }
  else
  {
    BYTE ErrFlag = ERR;                        //set default ErrFlag to error
    float I = 0.0;                             //set default current to zero

    ADC2_INST_REQ_CHAN = MUX2_REG;             //set channel to reg transductor
    ADC2_INST_REQ_FLAG = ADC2_REQ4_ON;         //set 4 cycle request flag ON

    //pend on instruction semaphore, check for timeout, request done flag
    if (OS_NO_ERR == OSSemPend(&ADC2_INST_SEM, TICKS_PER_SECOND))
    {
      if (ADC2_INST_REQ_FLAG == ADC2_REQ_DONE)
      {
        I = (float)ADC2_INST_REQ_DATA          //Reg xduct ADC2 reading
            * xil.db_eep.reg_iva               //times Reg xduct amps per volt (IVA)
            / (float)KADC;                     //divided by ADC2 counts per volt
        I = RevPol(I);                         //Negate I if reversing switch on
        ErrFlag = NO_ERR;
      }
    }
    ADC2_INST_REQ_FLAG = ADC2_REQ_OFF;         //set request state to off

    flip4((PBYTE) &I);                         //convert to little endian
    pRsp->Status01 = Status01(ErrFlag);        //set status bytes 0 and 1
    pRsp->PSCur = I;                           //set PS current
    pRsp->Rsp = COMMAND_OK;                    //set response code
    pUDP->SetDataSize(RSP_LEN_C0);             //set data size for response packet
  }
}


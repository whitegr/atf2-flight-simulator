//******************************************************************************
//  Miscellaneous functions (Ethernet Power Supply Controller)
//  Filename: misc.cpp
//  02/16/05
//
// This module has miscellaneous functions.
//
//******************************************************************************

#include <C:\Nburn\EPSC\include\includes.h>    //Include all headers for project

//*************************** WrErrMsg(char * pMsg) ****************************
//
// Write character string to error message array.
//
// ErrMsg[i][j]  array of message buffers, i=message index, j=character index
// ErrMsgTime[i] array of timestamps (1/10 seconds since last reset)
// ErrMsgWrPtr   points to next location to be written
// ErrMsgRdPtr   points to next location to be read
// NUM_MSG_BUF   Number of buffers
// MSG_BUF_SIZE  Size of message buffer
//
// The function writes the new error message. If the buffer is full, the oldest
// message is discarded by incrementing the read pointer.
// 
//******************************************************************************

void WrErrMsg(char * pMsg)
{
  int i = 0;
  while ((pMsg[i] != 0) && (i < (ErrMsgBufSize-1) ))  //check for end of string
  { 
    ErrMsg[ErrMsgWrPtr][i] = pMsg[i];                 //transfer string to buffer
    ++i;
  }
  ErrMsg[ErrMsgWrPtr][i] = 0;                         //set null at end of string
  ErrMsgTime[ErrMsgWrPtr] = TIMER;                    //set time stamp
  ErrMsgWrPtr = (ErrMsgWrPtr + 1) % NumErrMsgBuf;     //increment write pointer
  if( ErrMsgWrPtr == ErrMsgRdPtr)                     //check for buffer full
  {
    ErrMsgRdPtr = (ErrMsgRdPtr + 1) % NumErrMsgBuf;   //increment read pointer
  }
}

//****************************** RevPol(float i) *******************************
//
//  Negate value of regulated transductor if reversing switch is on. The state is
//  determined by the STATUS_REV_POL flag in the xil.status.
//
//******************************************************************************

float RevPol(float I)
{
  if (xil.ps_status & STATUS_REV_POL)
    return -I;
  else
    return I;
}

//*************************** float KlixRes(float V) ***************************
//  Calculate equivalent resistance (K ohm) on the output of the Klixon circuit.
//  Sense resistor = 0.5 (500 ohm)      load voltage = 14.50 - V
//  R = V / I = (14.5 - V) / (V / 0.500) = 0.500 * (14.50 - V) / V
//  R = 500 * (14.5 - V) / V
//******************************************************************************

float KlixRes(float V)
{
  if (V < 1E-6) V = 1E-6;
  return (0.500 * (14.5 -V) / V);
}

//***************************float KlixCond(float V) ***************************
//  Calculate equivalent conductance (1/Mohm) on the output of the Klixon circuit.
//  Sense resistor (Mohm) = 0.000500 (500 ohm)      load voltage = 14.50 - V
//  R = V / I = (14.5 - V) / (V / 0.000500) = 0.000500 * (14.50 - V) / V
//  C = 1/R = (1/0.0005)* V /(14.5 - V) = 2000 * V / (14.5 - V)
//  Subtract 400K resistance of AD629 ( 2.5 = 1e6 / 400K)
//******************************************************************************

float KlixCond(float V)
{
  if (V < 1E-6) return (0.0);
  if (V > 7.25) return (2000.0);
  return ((2000.0 * V / (14.5 -V)) - 2.5);
}

//***************** short GetStr(char * pStr, int maxlen = 32) *****************
//
//  Get a string from STDIN with length checking
//
//  Reads a string from STDIN and writes to the the buffer passed to the function.
//  The first non printable character , or when the maxlen numbers of chacters are
//  read, ends the input and a null is written to the buffer to end the string.
//  Non printable characters are values less then ' ' (ASCII space, 0x20).
//
//  The function returns the number of chacters written.
//
//  pStr    pointer to a string buffer passed to the function
//  maxlen  maximum number of characters written to the buffer, including the null
//
//******************************************************************************

SHORT GetStr(char * pStr, WORD maxlen = 32)
{ int len = 0;
  while ( (( pStr[len] = (char)getchar() ) >= ' ') && (len < (maxlen - 1)) ) ++len;
  pStr[len] = 0;
  return len;
}

//******************** void ShowStat(PBYTE pStat, WORD len) ********************
//
//  Print short (2 byte) or long (4 byte) status
//
//  Prints the value of each bit with a text description of the status bytes. If
//  len is zero, only the first two status bytes are printed. The function is
//  passed the address of the first status byte.
//
//  ShowStat((PBYTE) &status[0], 1)  Typical usage
//******************************************************************************

void ShowStat(PBYTE pStat, WORD len)
{ const char * pS01[8] = {"%1X  Command OK       %1X  Error Message    ",
                          "%1X  Command Err      %1X  Hardware Flt     ",
                          "%1X  PS OFF           %1X  Calibrate Flt    ",
                          "%1X  Ramp ON, Setp    %1X  Aux Xduct Flt    ",
                          "%1X  Ramp ON, Ramp    %1X  Sys Not Ready    ",
                          "%1X  Ramp HOLD        %1X  Spare            ",
                          "%1X  Reverse Pol      %1X  Spare            ",
                          "%1X  Local Mode       %1X  Spare            "};

  const char * pS23[8] = {"%1X  Magnet Intl 0    %1X  Fault Latch ON",
                          "%1X  Magnet Intl 1    %1X  PS Status 0",
                          "%1X  Magnet Intl 2    %1X  PS Status 1",
                          "%1X  Magnet Intl 3    %1X  PS Status 2",
                          "%1X  PS Not Ready     %1X  PS Status 3",
                          "%1X  Reg Xduxt Flt    %1X  PS ON Status",
                          "%1X  Ground Current   %1X  Voltage Mode",
                          "%1X  Controller Flt   %1X  Spare"};
  BYTE mask = 0x01;
  for (WORD i = 0; i < 8; ++i)
  { printf(pS01[i], (pStat[0] & mask) ? 1:0, (pStat[1] & mask) ? 1:0);
    if (len)
      printf(pS23[i], (pStat[2] & mask) ? 1:0, (pStat[3] & mask) ? 1:0);
    printf("\r\n");
    mask = mask << 1;
  }
}

//******************** void ShowHex(WORD * pDat, WORD len) *********************
//
//  Prints the hex and ASCII values of the data pointed to by pDat. The number 
//  of words to be printed is passed in len. The function uses words to allow the
//  Xilinx data to be read.
//
//  ShowHex((WORD *) &xil.mb_eep, 64 )  Typical usage
//******************************************************************************

void ShowHex(WORD * pDat, WORD len)
{ WORD i = 0, j = 0;
  char c;

  while (i < len)
  { for (j=0; j < 8; ++j)                        //print HEX data
    { 
      if ((i + j) < len)
        printf("%04X ", pDat[i + j]);
      else
        printf("     ");
    }

    printf("    ");                              //print ASCII data
    for (j=0; j < 8; ++j)
    { 
      if ((i + j) < len)
      {
        c = (char) (pDat[ i + j ] / 256);
        if (c < ' ') putchar('.'); else putchar(c);
        c = (char) (pDat[ i + j ] % 256);
        if (c < ' ') putchar('.'); else putchar(c);
      }
    }
    printf("\r\n");
    i = i + 8;
  }
}

//******************* WORD Limit(long * pData, long Limit) *********************
//
//  Limits the value pointed to by pData to the value passed in Limit. If the
//  magnitude of data is less then Limit, it is unchanged and the function returns
//  0. If the magnitude of data is equal or greater then Limit, the data is set
//  set equal to limit with the sign of data and the function returns 0.
//
//******************************************************************************

WORD Limit(long * pData, long Limit)
{ if (*pData > Limit)
  { *pData = Limit;
    return 1;
  }
  else if (*pData < -Limit)
  { *pData = -Limit;
    return 1;
  }
  else return 0;
}

//********************** WORD FloatToShortFloat(float x) ***********************
//
//  Convert a float (32 bit) to a short float (16 bit) by truncating the mantissa
//
//  Note: short float is a float with the 24 bit mantissa truncated to 8 bits
//
//******************************************************************************

WORD FloatToShortFloat(float x)
{
  WORD * pShortFloat = (WORD *) & x;   //set word pointer to address of x
  return (* pShortFloat);              //return first 16 bits (WORD) of float
}

//********************** float ShortFloatToFloat(WORD x) ***********************
//
//  Convert a short float (16 bit) to float by padding mantissa with 16 zeros
//
//******************************************************************************

float ShortFloatToFloat(WORD x)
{
  static float f;
  WORD * pWord = (WORD *) &f;      //set WORD pointer to address of float
  pWord[0] = x;                    //set upper 16 bits of float to short float
  pWord[1] = 0;                    //set lower 16 bits of float to zero
  return (f);
}

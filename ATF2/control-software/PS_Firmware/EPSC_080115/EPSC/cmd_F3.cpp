//******************************************************************************
//  CHASSIS TEST COMMAND (0xF3) (Ethernet Power Supply Controller)
//  Filename: cmd_F3.cpp
//  01/12/06
//  This message allows writing of hardware configuration for chassis testing.
//  0x01   1 Set voltage mode         0 Set current mode
//  0x02   1 Set reverse polarity     0 Set normal polarity
//
//******************************************************************************

#include <C:\Nburn\EPSC\include\includes.h>    //Include all headers for project

typedef struct          // F3 command structure
{ BYTE   Ctype;         // 00 command type 0xC0
  BYTE   Rsp;           // 01 response code
  BYTE   Tid;           // 02 Task ID
  BYTE   Data;          // 03 Data byte
} __attribute__ ((packed))  Cmd_F3;

#define CMD_LEN_F3 4    //expected length of command message

typedef struct          // F3 response structure
{ BYTE   Ctype;         // 00 command type, returned unchanged
  BYTE   Rsp;           // 01 response code
  BYTE   Tid;           // 02 Task ID, returned unchanged
  BYTE   Data;          // 03 Data byte, returned unchanged
} __attribute__ ((packed))  Rsp_F3;

#define RSP_LEN_F3 4    //length of response message

//********************* void Process_F3(UDPPacket * pUDP) **********************
//  pUDP  pointer to object of class UDPPacket, passed to function
//  pCmd  pointer to command structure (type Cmd_F3) set to start of UDP data
//  pRsp  pointer to response structure (type Rsp_F3) set to start of UDP data
//******************************************************************************

void Process_F3(UDPPacket * pUDP)
{
  PBYTE pUdpData = pUDP->GetDataBuffer();      //get pointer to UDP data field
  Cmd_F3 * pCmd = (Cmd_F3 *) pUdpData;         //set pointer for command data
  Rsp_F3 * pRsp = (Rsp_F3 *) pUdpData;         //set pointer for response data

  if (pUDP->GetDataSize() != CMD_LEN_F3)       //check for valid command length
  { pRsp->Rsp = INVALID_MSG_LENGTH;
  }
  else
  {
    if (xil.ps_status & STATUS_PS_OFF)
    {
      if (pCmd->Data & 0x01)                     //if turn on reverse
        xil.ps_cntl = PSCNTL_VLT_ON;             //set voltage mode
      else
        xil.ps_cntl = PSCNTL_ERAMP_ON;           //set error amplifier enable ON

      if (pCmd->Data & 0x02)                     //if turn on reverse
        xil.ps_cntl = PSCNTL_REV_ON;             //set reverse polarity ON
      else                                       //else
        xil.ps_cntl = PSCNTL_REV_OFF;            //set reverse polarity OFF
      pRsp->Rsp = COMMAND_OK;                    //set response code
    }
    else
    {
      pRsp->Rsp = COMMAND_ERR;                   //set response code
    }
    pUDP->SetDataSize(RSP_LEN_F3);             //set data size for response packet
  }
}


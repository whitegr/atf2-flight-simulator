//******************************************************************************
//  Diagnostic Port Configuration Functions
//  Filename: config.cpp
//  05/05/05
//  07/31/07 Added configuration bit for PS serial link (UART1)
//           Corrected CONFIG_HOLD_EN and CONFIG_REV_SW bits in configuration
//
//  These functions allow display and modification of controller configuration.
//
//******************************************************************************

#include <C:\Nburn\EPSC\include\includes.h>    //Include all headers for project

//******************** void spShowIP(int fd, IPADDR ipAdr) *********************
// Write an IP address in decimal dotted format to a serial port
//******************************************************************************

void spShowIP(int fd, IPADDR ipAdr)
{
  PBYTE pByte = ( PBYTE ) & ipAdr;
  char buf[32];
  int len = sprintf(buf, "%3d . %3d . %3d . %3d\r\n",
          ( int ) pByte[0],
          ( int ) pByte[1],
          ( int ) pByte[2],
          ( int ) pByte[3] );
  write(fd, buf, len);
}

//************************** void spShowIpStk(int fd) **************************
//  Write IP stack information to a serial port
//  IP address     xil.db_eep.ip_adr
//  IP mask        xil.db_eep.ip_mask
//  IP gateway     xil.db_eep.ip_gate
//  IP DNS         xil.db_eep.ip_dns
//******************************************************************************

void spShowIpStk(int fd)
{
  writestring(fd, "IP Address:    ");            //write IP address to serial port
  spShowIP(fd, xil.db_eep.ip_adr);
  writestring(fd, "IP Mask:       ");            //write IP mask to serial port
  spShowIP(fd, xil.db_eep.ip_mask);
  writestring(fd, "IP Gateway:    ");            //write IP gateway to serial port
  spShowIP(fd, xil.db_eep.ip_gate);
  writestring(fd, "IP DNS:        ");            //write IP DNS to serial port
  spShowIP(fd, xil.db_eep.ip_dns);
}

//************************** void spSetIPStk(int fd) ***************************
//  Set values for IP address, mask, gateway, and DNS in Xilinx memory. The
//  daughterboard EEPROM must be programmed to make these changes permanent.
//******************************************************************************

void spSetIPStk(int fd)
{
  char buf[32];
  int len = 0;

  writestring(fd, "Current IP Stack Values\r\n");
  spShowIpStk(fd);                         //show current settings

  writestring(fd, "\r\nEnter new values, 'cr' retains current value\r\n");

  writestring(fd, "IP Address: ");
  len = spRdStr(fd, buf, 31);
  if (len > 0) xil.db_eep.ip_adr = AsciiToIp(buf);

  writestring(fd, "IP Mask:    ");
  len = spRdStr(fd, buf, 31);
  if (len > 0) xil.db_eep.ip_mask = AsciiToIp(buf);

  writestring(fd, "IP Gateway: ");
  len = spRdStr(fd, buf, 31);
  if (len > 0) xil.db_eep.ip_gate = AsciiToIp(buf);

  writestring(fd, "IP DNS:     ");
  len = spRdStr(fd, buf, 31);
  if (len > 0) xil.db_eep.ip_dns = AsciiToIp(buf);

  writestring(fd, "\r\nNew Values (program DB EEPROM to save)\r\n");
  spShowIpStk(fd);                         //show new settings
}

//************************ void spShowEnetConfig(int fd) ***********************
//  Write Ethernet configuration information to a serial port
//******************************************************************************

const char * pEnetConfig[5] = {"Auto Negotiation" ,
                               "10 Mb Half Duplex" ,
                               "10 Mb Full Duplex" ,
                               "100 Mb Half Duplex" ,
                               "100 Mb Full Duplex" };

void spShowEnetConfig(int fd)
{ 
  writestring(fd, "Ethernet = ");
  WORD Config = xil.db_eep.EnetConfig;
  if ( Config <= 4 )
    writestring(fd, pEnetConfig[Config] );
  else
    writestring(fd, "?");
  writestring(fd, "\r\n");
}

//************************ void spSetEnetConfig(int fd) ************************
//  Set Ethernet configuration information to a serial port
//******************************************************************************

void spSetEnetConfig(int fd)
{
  int len;
  char buf[32];

  spShowEnetConfig(fd);
  for ( WORD i = 0; i < 5 ; ++i)
  {
    len = sprintf(buf, "%d = %s\r\n", i, pEnetConfig[i] );
    write(fd, buf, len);
  }
  writestring(fd, "Enter Configuration Number = ");
  len = spRdStr(fd, buf, 16);
  if (len > 0)
  {
     WORD Config = (WORD) atoi(buf);
     if (Config <= 4) xil.db_eep.EnetConfig = Config;
  }
  spShowEnetConfig(fd);
}

//*************************** void spShowIVA(int fd) ***************************
//  Write scaling constants to serial port
//  Regulated Transductor   Amps/Volt    xil.db_eep.reg_iva
//  Auxiliary Transductor   Amps/Volt    xil.db_eep.aux_iva
//  Ground Current          Amps/Volt    xil.db_eep.gnd_iva
//  PS Output Voltage       Volt/Volt    xil.db_eep.psv_iva
//******************************************************************************

void spShowIVA(int fd)
{
  int len;
  char buf[64];

  len = sprintf(buf, "Reg Transductor %6E (Amps/Volt)\r\n",
                xil.db_eep.reg_iva);
  write(fd, buf, len);

  len = sprintf(buf, "Aux Transductor %6E (Amps/Volt)\r\n",
                xil.db_eep.aux_iva);
  write(fd, buf, len);

  len = sprintf(buf, "Ground Current  %6E (Amps/Volt)\r\n",
                xil.db_eep.gnd_iva);
  write(fd, buf, len);

  len = sprintf(buf, "PS Output Volts %6E (Volt/Volt)\r\n",
                xil.db_eep.psv_iva);
  write(fd, buf, len);
}

//*************************** void spSetIVA(int fd) ****************************
//  Set values for IVA coefficients in Xilinx memory. The daughterboard EEPROM
//  must be programmed to make these changes permanent. The regulated transductor
//  IVA must be non zero to allow conversion from amps to volts (1 / iva).
//******************************************************************************

void spSetIVA(int fd)
{
  char buf[32];
  int len = 1;

  writestring(fd, "Current IVA Coefficients\r\n");
  spShowIVA(fd);                           //show current settings

  writestring(fd, "\r\nEnter new values, 'cr' retains current value\r\n");

  while (len != 0)
  {
    writestring(fd, "Reg Transductor = ");
    len = spRdStr(fd, buf, 31);
    if (len > 0)
    { float reg_iva = atoff(buf);
      if (fabsf(reg_iva) > 1e-6)
      { xil.db_eep.reg_iva = reg_iva;
        len = 0;
      }
      else
        writestring(fd, "***ERROR, VALUE MUST BE > 1E-6 ***\r\n");
    }
  }

  writestring(fd, "Aux Transductor = ");
  len = spRdStr(fd, buf, 31);
  if (len > 0) xil.db_eep.aux_iva = atoff(buf);

  writestring(fd, "Ground Current  = ");
  len = spRdStr(fd, buf, 31);
  if (len > 0) xil.db_eep.gnd_iva = atoff(buf);

  writestring(fd, "PS Output Volts = ");
  len = spRdStr(fd, buf, 31);
  if (len > 0) xil.db_eep.psv_iva = atoff(buf);

  writestring(fd, "\r\nNew Values (program DB EEPROM to save)\r\n");
  spShowIVA(fd);                         //show new settings
}

//*********************** void spShowXilStr(WORD * pxil) ***********************
// This function reads an 8 byte character string from the xilinx and writes it
// to a serial port.
//******************************************************************************

void spShowXilStr(int fd, VWORD * pxil)
{ char buf[8];
  WORD * pbuf = (WORD *) &buf;
  for (int i=0; i < 4; ++i)    //transfer four words to character buffer
    pbuf[i] = pxil[i];
  write(fd, buf, 8);           //write string to serial port
}

//************************** void spShowMagID(int fd) **************************
//  Write the Magnet ID string to the serial port
//******************************************************************************

void spShowMagID(int fd)
{
  writestring(fd, "Magnet Name = ");
  spShowXilStr(fd, & xil.db_eep.MagnetId[0]);
  writestring(fd, "\r\n");
}

//************************** void spSetMagID(int fd) ***************************
//  Set new value for Magnet Name, 8 character ASCII string
//******************************************************************************

void spSetMagID(int fd)
{
  char buf[9];                   //define character buffer for string
  PWORD pW = (PWORD) &buf;       //define pointer to transfer buffer as WORDs
  int i, len;

  spShowMagID(fd);                                //show current magnet name
  writestring(fd, "Enter new Magnet Name = ");    //prompt for new string
  len = spRdStr(fd, buf, 8);                      //get new Magnet name string

  if (len > 0)                   //enter new string if length > 0
  {
    for (i = len; i < 8; ++i) buf[i]=' ';         //add spaces to end of string
    for (i = 0; i < 4; ++i)                       //write string to Xilinx SRAM
      xil.db_eep.MagnetId[i] = pW[i];

    writestring(fd, "\r\nNew Values (program DB EEPROM to save)\r\n");
    spShowMagID(fd);                              //show new value
  }
}

//************************* void spShowSerNum(int fd) **************************
//  Write the chassis serial number string to the serial port
//******************************************************************************

void spShowSerNum(int fd)
{
  writestring(fd, "Serial Number = ");
  spShowXilStr(fd, & xil.mb_eep.SerialNum[0]);
  writestring(fd, "\r\n");
}

//************************* void spSetSerNum(int fd) ***************************
//  Set new value for controller serial number, 8 character ASCII string
//******************************************************************************

void spSetSerNum(int fd)
{
  char buf[9];                   //define character buffer for string
  PWORD pW = (PWORD) &buf;       //define pointer to transfer buffer as WORDs
  int i, len;

  spShowSerNum(fd);                                //show current magnet name
  writestring(fd, "Enter new Serial Number = ");  //prompt for new string
  len = spRdStr(fd, buf, 8);                      //get new serial number string

  if (len > 0)                   //enter new string if length > 0
  {
    for (i = len; i < 8; ++i) buf[i]=' ';         //add spaces to end of string
    for (i = 0; i < 4; ++i)                       //write string to Xilinx SRAM
      xil.mb_eep.SerialNum[i] = pW[i];
    writestring(fd, "\r\nNew Value (program MB EEPROM to save)\r\n");
    spShowSerNum(fd);                              //show new value
  }
}

//************************* void spShowAdcRef(int fd) **************************
//  Write the ADC reference voltage and calibration date to the serial port
//******************************************************************************

void spShowAdcRef(int fd)
{
  int len;
  char buf[32];
  len = sprintf(buf, "Reference = %9.6f\r\n", xil.mb_eep.ref);
  write(fd, buf, len);

  writestring(fd, "Cal Date = ");
  spShowXilStr(fd, & xil.mb_eep.CalDate[0]);
  writestring(fd, "\r\n");
}

//************************** void spSetAdcRef(int fd) **************************
//  Set new value for ADC reference and Calibration Date
//******************************************************************************

void spSetAdcRef(int fd)
{
  char buf[16];                                   //character buffer for string
  int len;

  spShowAdcRef(fd);                               //show current reference

  writestring(fd, "Enter new Reference = ");      //prompt for new value
  len = spRdStr(fd, buf, 15);
  if (len > 0)
  {
    xil.mb_eep.ref = atoff(buf);

    writestring(fd, "Enter Calibration Date = "); //prompt for new string
    len = spRdStr(fd, buf, 8);                    //get new Magnet name string
    if (len > 0)                                  //enter new string if length > 0
    {
      for (int i = len; i < 8; ++i) buf[i]=' ';   //add spaces to end of string
      PWORD pW = (PWORD) &buf;
      for (int i = 0; i < 4; ++i)                 //write string to Xilinx SRAM
        xil.mb_eep.CalDate[i] = pW[i];
    }
    writestring(fd, "\r\nNew Values (program MB EEPROM to save)\r\n");
    spShowAdcRef(fd);                             //show new value
  }
}

//*********************** void spShowAdcLinCoef(int fd) ************************
//  Write ADC linearity coefficents to the serial port.
//******************************************************************************

float ctouv( short k )
{
  return( (float)k * 1E6 / (float)KADC );
}

void spShowAdcLinCoef(int fd)
{
  int len;
  char buf[32];

  len = sprintf(buf, "ADC1 K1 =%4.0f uV",  ctouv( xil.mb_eep.a1k1 ) );
  write(fd, buf, len);
  len = sprintf(buf, ", K2 =%4.0f uV\r\n", ctouv( xil.mb_eep.a1k2 ) );
  write(fd, buf, len);
  len = sprintf(buf, "ADC2 K1 =%4.0f uV",  ctouv( xil.mb_eep.a2k1 ) );
  write(fd, buf, len);
  len = sprintf(buf, ", K2 =%4.0f uV\r\n", ctouv( xil.mb_eep.a2k2 ) );
  write(fd, buf, len);
}

//************************* void spAdcLinCoef(int fd) **************************
//  Set new value for ADC reference and Calibration Date.
//  Values are entered in microvolts.
//******************************************************************************

short StrToCoef( char * buf )
{
  float V = atoff( buf );
  if( V >  999.9 ) V =  999.9;
  if( V < -999.9 ) V = -999.9;
  return( (short)( V * (float)KADC / 1E6 ) );
}

void spAdcLinCoef(int fd)
{
  char buf[16];                                   //character buffer for string
  int len;

  writestring(fd, "ADC linearity coefficients\r\n" );
  spShowAdcLinCoef(fd);                           //show current coefficents

  writestring(fd, "\r\nEnter new coefficients, 'cr' retains current value\r\n" );
  writestring(fd, "ADC1 K1 = " );  //prompt for new value
  len = spRdStr(fd, buf, 15);
  if (len > 0) xil.mb_eep.a1k1 = StrToCoef( buf );

  writestring(fd, "ADC1 K2 = " );  //prompt for new value
  len = spRdStr(fd, buf, 15);
  if (len > 0) xil.mb_eep.a1k2 = StrToCoef( buf );

  writestring(fd, "ADC2 K1 = " );  //prompt for new value
  len = spRdStr(fd, buf, 15);
  if (len > 0) xil.mb_eep.a2k1 = StrToCoef( buf );

  writestring(fd, "ADC2 K2 = " );  //prompt for new value
  len = spRdStr(fd, buf, 15);
  if (len > 0) xil.mb_eep.a2k2 = StrToCoef( buf );

  writestring(fd, "\r\nNew Values (program MB EEPROM to save)\r\n");
  spShowAdcLinCoef(fd);                 //show new value
}

//************************* void spShowConfig(int fd) **************************
//  Write the configuration flags to the serial port (xil.db_eep.config)
//******************************************************************************

void spShowConfig(int fd)
{
  int len, flag;
  char buf[32];
  if (LIN_RAMP_ENABLED) flag = 1; else flag = 0;
  len = sprintf(buf, "Linear Ramp        %1d\r\n", flag);
  write(fd, buf, len);

  if (SLOW_RAMP_ENABLED) flag = 1; else flag = 0;
  len = sprintf(buf, "Slow Ramp          %1d\r\n", flag);
  write(fd, buf, len);

  if (RAMP_HOLD_ENABLED) flag = 1; else flag = 0;
  len = sprintf(buf, "Enable Ramp Hold   %1d\r\n", flag);
  write(fd, buf, len);

  if (DIGITAL_REG_ENABLED) flag = 1; else flag = 0;
  len = sprintf(buf, "Digital Regulation %1d\r\n", flag);
  write(fd, buf, len);

  if (PS_SERIAL_ENABLE) flag = 1; else flag = 0;
  len = sprintf(buf, "Enable PS Serial   %1d\r\n", flag);
  write(fd, buf, len);

  if (PS_HAS_REV_SW) flag = 1; else flag = 0;
  len = sprintf(buf, "Reversing Switch   %1d\r\n", flag);
  write(fd, buf, len);

  if (PS_IS_BIPOLAR) flag = 1; else flag = 0;
  len = sprintf(buf, "Bipolar Power Sup  %1d\r\n", flag);
  write(fd, buf, len);
}

//************************** void spSetConfig(int fd) **************************
// Set the controller configuration flags in xil.db_eep.config
// CONFIG_LIN_RAMP   0x0001    Enable linear ramps
// CONFIG_SLOW_RAMP  0x0002    Enable slow ramp (time in 1/20 seconds)
// CONFIG_HOLD_EN    0x0004    Enable hardware Hold signal for all ramp
// CONFIG_DIG_REG    0x0010    Digital Regulation Enabled
// CONFIG_PS_SERIAL  0x0020    Enable UART1 for OCEM multi-module PS
// CONFIG_REV_SW     0x0040    Power supply has reversing switch
// CONFIG_BIPOLAR    0x0080    Power supply is bipolar
//******************************************************************************

void spSetConfig(int fd)
{
  char Char;

  spShowConfig(fd);                              //show current configuration

  writestring(fd, "\r\nEnter new values, 'cr' retains current value\r\n");

  writestring(fd, "Enable linear ramp (1/0) ");
  Char = spRdChar(fd);
  if (Char == '1') xil.db_eep.Config |=  CONFIG_LIN_RAMP;
  if (Char == '0') xil.db_eep.Config &= ~CONFIG_LIN_RAMP;

  writestring(fd, "Enable slow ramp   (1/0) ");
  Char = spRdChar(fd);
  if (Char == '1') xil.db_eep.Config |=  CONFIG_SLOW_RAMP;
  if (Char == '0') xil.db_eep.Config &= ~CONFIG_SLOW_RAMP;

  writestring(fd, "Enable ramp hold   (1/0) ");
  Char = spRdChar(fd);
  if (Char == '1') xil.db_eep.Config |=  CONFIG_HOLD_EN;
  if (Char == '0') xil.db_eep.Config &= ~CONFIG_HOLD_EN;

  writestring(fd, "Enable Digital Reg (1/0) ");
  Char = spRdChar(fd);
  if (Char == '1') xil.db_eep.Config |=  CONFIG_DIG_REG;
  if (Char == '0') xil.db_eep.Config &= ~CONFIG_DIG_REG;

  writestring(fd, "Enable PS Serial   (1/0) ");
  Char = spRdChar(fd);
  if (Char == '1') xil.db_eep.Config |=  CONFIG_PS_SERIAL;
  if (Char == '0') xil.db_eep.Config &= ~CONFIG_PS_SERIAL;

  writestring(fd, "Reversing Switch   (1/0) ");
  Char = spRdChar(fd);
  if (Char == '1') xil.db_eep.Config |=  CONFIG_REV_SW;
  if (Char == '0') xil.db_eep.Config &= ~CONFIG_REV_SW;

  writestring(fd, "Bipolar Pwr Supply (1/0) ");
  Char = spRdChar(fd);
  if (Char == '1') xil.db_eep.Config |=  CONFIG_BIPOLAR;
  if (Char == '0') xil.db_eep.Config &= ~CONFIG_BIPOLAR;

  writestring(fd, "\r\nNew Values (program DB EEPROM to save)\r\n");
  spShowConfig(fd);                              //show new configuration
}

//************************* void spShowDigCoef(int fd) *************************
//  Write digital regulation coefficients to serial port
//  Digital Gain            Gain of digital loop (DAC volts verses ADC volts)
//  Digital Time Constant   Time constant of integrator in seconds
//  Digital Error Limit     Error voltage limit (desired - actual ADC, 10 VFS)
//******************************************************************************

void spShowDigCoef(int fd)
{
  int len;
  char buf[64];

  len = sprintf(buf, "Digital Gain  %4.2E (DAC Volt/ ADC Volt)\r\n",
                ShortFloatToFloat(xil.db_eep.DigitalGain));
  write(fd, buf, len);

  len = sprintf(buf, "Time Constant %4.2E (sec)\r\n",
                ShortFloatToFloat(xil.db_eep.DigitalTC));
  write(fd, buf, len);

  len = sprintf(buf, "Error Limit   %4.2E (Volt)\r\n",
                ShortFloatToFloat(xil.db_eep.DigitalErrLim));
  write(fd, buf, len);
}

//************************ void spSetDigCoef(int fd) ***************************
//  Set values for IVA coefficients in Xilinx memory. The daughterboard EEPROM
//  must be programmed to make these changes permanent. The regulated transductor
//  IVA must be non zero to allow conversion from amps to volts (1 / iva).
//******************************************************************************

void spSetDigCoef(int fd)
{
  char buf[32];
  int len = 1;

  writestring(fd, "Current Digital Regulation Coefficients\r\n");
  spShowDigCoef(fd);                       //show current settings

  writestring(fd, "\r\nEnter new values, 'cr' retains current value\r\n");

  while (len != 0)
  {
    writestring(fd, "Digital Gain = ");
    len = spRdStr(fd, buf, 31);
    if (len > 0)
    { float gain = atoff(buf);
      if ( (gain > 0.1) && (gain <= 30.0) )
      { xil.db_eep.DigitalGain = FloatToShortFloat(gain);
        len = 0;
      }
      else
        writestring(fd, "***ERROR, VALUE MUST BE 0.1 to 30***\r\n");
    }
  }

  len = 1;
  while (len != 0)
  {
    writestring(fd, "Time Constant = ");
    len = spRdStr(fd, buf, 31);
    if (len > 0)
    { float tc = atoff(buf);
      if ( (tc >= 0.001) && (tc <= 3600.0) )
      { xil.db_eep.DigitalTC = FloatToShortFloat(tc);
        len = 0;
      }
      else
        writestring(fd, "***ERROR, VALUE MUST BE 0.001 to 3600***\r\n");
    }
  }

  len = 1;
  while (len != 0)
  {
    writestring(fd, "Error Limit = ");
    len = spRdStr(fd, buf, 31);
    if (len > 0)
    { float lim = atoff(buf);
      if ( (lim >= 0.001) && (lim <= 10.0) )
      { xil.db_eep.DigitalErrLim = FloatToShortFloat(lim);
        len = 0;
      }
      else
        writestring(fd, "***ERROR, VALUE MUST BE 0.001 to 10***\r\n");
    }
  }

  writestring(fd, "\r\nNew Values (program DB EEPROM to save)\r\n");
  spShowDigCoef(fd);                      //show new settings
}

//************************** void spShowMbEEP(int fd) **************************
//  This function writes the Motherboard EEPROM data to the serial port
//******************************************************************************

void spShowMbEEP(int fd)
{
  writestring(fd, "Motherboard Configuration Data\r\n");

  spShowSerNum(fd);
  spShowAdcRef(fd);
  spShowAdcLinCoef(fd);
}

//*************************** void spWrMbEEP(int fd) ***************************
//  This function writes the configuration data to the motherboard EEPROM.
//******************************************************************************

void spWrMbEEP(int fd)
{
  spShowMbEEP(fd);
  writestring(fd, "Write to Motherboard EEPROM? (y/n) ");
  char Char = spRdChar(fd);
  if ((Char == 'Y') || (Char == 'y'))
  {
    WrEEPROM(MB_EEP, EEP_LOW);
    writestring(fd, "Data written to motherboard EEPROM\r\n");
  }
}

//************************** void spShowDbEEP(int fd) **************************
//  This function writes the Daughterboard EEPROM data to the serial port
//******************************************************************************

void spShowDbEEP(int fd)
{
  writestring(fd, "Daughterboard Configuration Data\r\n");
  spShowMagID(fd);
  spShowIpStk(fd);
  spShowEnetConfig(fd);
  spShowIVA(fd);
  spShowConfig(fd);
  spShowDigCoef(fd);
}

//*************************** void spWrDbEEP(int fd) ***************************
//  This function writes the configuration data to the Daughterboard EEPROM.
//******************************************************************************

void spWrDbEEP(int fd)
{
  spShowDbEEP(fd);
  writestring(fd, "Write to Daughterboard EEPROM? (y/n) ");
  char Char = spRdChar(fd);
  if ((Char == 'Y') || (Char == 'y'))
  {
    WrEEPROM(DB_EEP, EEP_LOW);
    writestring(fd, "\r\nData written to Daughterboard EEPROM\r\n");
  }
}


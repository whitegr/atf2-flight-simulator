//******************************************************************************
//  UDP interface task (Ethernet Power Supply Controller)
//  Filename: udp.cpp
//  02/14/05
//
// This module initializes UDP interface. This module recieves and process UDP
// packets. It generates a UDP reply to each valid packet. 
//
//******************************************************************************

#include <C:\Nburn\EPSC\include\includes.h>    //Include all headers for project

//******************************* StartUDPTask() *******************************

DWORD  UdpStk[USER_TASK_STK_SIZE] __attribute__(( aligned(4) ));

void StartUDPTask()
{ OSTaskCreate(UdpMain,                         //address of task code
               (void *) 0,                      //optional argument pointer
               &UdpStk[USER_TASK_STK_SIZE],     //top of stack
               UdpStk,                          //bottom of stack
               UDP_PRIO);                       //task priority
}

//******************************************************************************
//
// Receive and Process Ethernet UDP Packets
//
//******************************************************************************

void UdpMain(void * pd)
{ pd=pd;

  OS_FIFO fifo;
  OSFifoInit(&fifo);
  RegisterUDPFifo(UDP_PORT, &fifo);
//  printf("UDP task started at priority %2d, listening on port: %d\r\n",
//          UDP_PRIO, UDP_PORT);

  while (1)
  { UDPPacket upkt(&fifo, 2 * TICKS_PER_SECOND);

    if (upkt.Validate())
    {
      PBYTE pCode = upkt.GetDataBuffer();            //Get pointer to command code

//      printf("Recieved from IP address: ");
//      ShowIP(upkt.GetSourceAddress());
//      printf(" port %d", upkt.GetSourcePort());
//      printf(" TID %d\r\n", *(pCode + 2));
//      PoolPtr pMsg = upkt.GetPoolPtr();
//      puts("Command Packet");
//      ShowData(pMsg->pData, 42 + upkt.GetDataSize());
//      printf("*");

      if      (*pCode == 0xC0) Process_C0(&upkt);
      else if (*pCode == 0xC1) Process_C1(&upkt);
      else if (*pCode == 0xC2) Process_C1(&upkt);
      else if (*pCode == 0xC3) Process_C3(&upkt);
      else if (*pCode == 0xC4) Process_C4(&upkt);
      else if (*pCode == 0xC5) Process_C5(&upkt);
      else if (*pCode == 0xC6) Process_C6(&upkt);
      else if (*pCode == 0xC7) Process_C6(&upkt);
      else if (*pCode == 0xC8) Process_C8(&upkt);
      else if (*pCode == 0xC9) Process_C9(&upkt);
      else if (*pCode == 0xCA) Process_CA(&upkt);
      else if (*pCode == 0xCB) Process_CB(&upkt);
      else if (*pCode == 0xCC) Process_CC(&upkt);
      else if (*pCode == 0xCD) Process_CD(&upkt);
      else if (*pCode == 0xCE) Process_CE(&upkt);
      else if (*pCode == 0xCF) Process_CF(&upkt);
      else if (*pCode == 0xE1) Process_E1(&upkt);
      else if (*pCode == 0xE3) Process_E3(&upkt);
      else if (*pCode == 0xE8) Process_E8(&upkt);   //07/19/07 OCEM/ATF2 Modules
      else if (*pCode == 0xF0) Process_F0(&upkt);   //chassis test command
      else if (*pCode == 0xF1) Process_F1(&upkt);   //chassis test command
      else if (*pCode == 0xF2) Process_F2(&upkt);   //chassis test command
      else if (*pCode == 0xF3) Process_F3(&upkt);   //chassis test command
      else if (*pCode == 0xF4) Process_F4(&upkt);   //chassis test command
      else *pCode = UNKNOWN_COMMAND;

//      puts("Response Packet");
//      ShowData(pMsg->pData, 42 + upkt.GetDataSize());

      FlashYellow = 48;                //Turn on Yellow LED for 200 milliseconds

      if (*pCode == NO_RESPONSE)       //if NO_RESPONSE flag
        upkt.ReleaseBuffer();          //relaese memory
      else
        SendResponse(&upkt);           //else send message (and release memory)
    }
    xil.WatchdogFlag &= ~UDPWatchdogFlag;    //watchdog timer flag
  }
}


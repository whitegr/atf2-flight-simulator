//******************************************************************************
//  Semaphore Test
//  Filename: utilities.cpp
//  11/10/05
//******************************************************************************

#include <C:\Nburn\EPSC\include\includes.h>    //Include all headers for project

DWORD  SemTask1Stk[USER_TASK_STK_SIZE] __attribute__(( aligned(16) ));
DWORD  SemTask2Stk[USER_TASK_STK_SIZE] __attribute__(( aligned(16) ));
DWORD  SemTask3Stk[USER_TASK_STK_SIZE] __attribute__(( aligned(16) ));
DWORD  SemTask4Stk[USER_TASK_STK_SIZE] __attribute__(( aligned(16) ));

OS_SEM SemaphoreA;
OS_SEM SemaphoreB;
OS_SEM SemaphoreC;

void SemTask1(void * pd)
{ 
  pd=pd;
  while(1)
  {
    printf("Task 1 waiting for semaphore A\r\n");
    OSSemPend( & SemaphoreA, 0);
    printf("Task 1 waiting for semaphore B\r\n");   
    OSSemPend( & SemaphoreB, 0);
  }
}

void SemTask2(void * pd)
{ 
  pd=pd;
  while(1)
  {
    printf("Task 2 waiting for semaphore A\r\n");
    OSSemPend( & SemaphoreA, 0);
    printf("Task 2 waiting for semaphore B\r\n");   
    OSSemPend( & SemaphoreB, 0);
  }
}

void SemTask3(void * pd)
{ 
  pd=pd;
  while(1)
  {
    printf("Task 3 waiting for semaphore A\r\n");
    OSSemPend( & SemaphoreA, 0);
    printf("Task 3 waiting for semaphore B\r\n");   
    OSSemPend( & SemaphoreB, 0);
  }
}

//******************************* StartTasks() *********************************

void StartSemTask1()
{ 
  OSTaskCreate( SemTask1,                           //address of task code
                (void *) 0,                         //optional argument pointer
                & SemTask1Stk[USER_TASK_STK_SIZE],  //top of stack
                SemTask1Stk,                        //bottom of stack
                21 );                               //task priority
}

void StartSemTask2()
{
  OSTaskCreate( SemTask2,                           //address of task code
                (void *) 0,                         //optional argument pointer
                & SemTask2Stk[USER_TASK_STK_SIZE],  //top of stack
                SemTask2Stk,                        //bottom of stack
                22 );                               //task priority
}

void StartSemTask3()
{
  OSTaskCreate( SemTask3,                           //address of task code
                (void *) 0,                         //optional argument pointer
                & SemTask3Stk[USER_TASK_STK_SIZE],  //top of stack
                SemTask1Stk,                        //bottom of stack
                23 );                               //task priority
}

void SemTask4(void * pd)
{ 
  pd=pd;
  OSSemPend( & SemaphoreC, 0);
  StartSemTask1();
  OSSemPend( & SemaphoreC, 0);
  StartSemTask2();
  OSSemPend( & SemaphoreC, 0);
  StartSemTask3();
  OSSemPend( & SemaphoreC, 0);

  while(1)
  {
    printf("Task 4 waiting for semaphore A\r\n");
    OSSemPend( & SemaphoreA, 0);
    printf("Task 4 waiting for semaphore B\r\n");   
    OSSemPend( & SemaphoreB, 0);
  }
}

void StartSemTask4()
{ OSTaskCreate( SemTask4,                           //address of task code
                (void *) 0,                         //optional argument pointer
                & SemTask4Stk[USER_TASK_STK_SIZE],  //top of stack
                SemTask4Stk,                        //bottom of stack
                24 );                               //task priority
}

//******************************* Main Program *********************************

void SemaphoreTest()
{
  OSSemInit( & SemaphoreA , 0);
  OSSemInit( & SemaphoreB , 0);
  OSSemInit( & SemaphoreC , 0);
  StartSemTask4();

  printf("Sempahore Test\r\n");
  printf("Type 'A' to post semaphore A\r\n");
  printf("Type 'B' to post semaphore B\r\n");
  printf("Type 'C' to add task\r\n");
  printf("Type 'X' to add exit\r\n");

  WORD NotDone = 1;
  OS_SEM * pSemA = & SemaphoreA;
  OS_SEM * pSemB = & SemaphoreB;
  while( NotDone )
  { 
    printf("Semaphore A count = %ld, Semaphore B count = %ld\r\n",
            pSemA->OSSemCnt, pSemB->OSSemCnt);
    printf("Post Semaphore = ");
    char Char = (char) toupper( getchar() );
    printf("\r\n");
    if      (Char == 'A')  OSSemPost( & SemaphoreA );
    else if (Char == 'B')  OSSemPost( & SemaphoreB );
    else if (Char == 'C')  OSSemPost( & SemaphoreC );
    else if (Char == 'X')  NotDone = 0;
  }
}


//******************************************************************************
//  COMMUNICATION CHECK (0xE1) (Ethernet Power Supply Controller)
//  Filename: cmd_E1.cpp
//  03/03/05
//  This message returns a "echo" message. No action is taken
//******************************************************************************

#include <C:\Nburn\EPSC\include\includes.h>    //Include all headers for project

typedef struct          // E1 command structure
{ BYTE   Ctype;         // 00 command type 0xE1
  BYTE   Rsp;           // 01 response code
  BYTE   Tid;           // 02 Task ID
  BYTE   Data;          // 03 Data byte, (don't care)
} __attribute__ ((packed))  Cmd_E1;

#define CMD_LEN_E1  4   // expected length of command message

typedef struct          // E1 response structure
{ BYTE   Ctype;         // 00 command type, returned unchanged
  BYTE   Rsp;           // 01 response code
  BYTE   Tid;           // 02 Task ID, returned unchanged
  BYTE   Data;          // 03 Data byte, set to 0xFF
} __attribute__ ((packed))  Rsp_E1;

#define RSP_LEN_E1   4  // length of response message

//********************* void Process_E1(UDPPacket * pUDP) **********************
//  pUDP  pointer to object of class UDPPacket, passed to function
//  pCmd  pointer to command structure (type Cmd_E1) set to start of UDP data
//  pRsp  pointer to response structure (type Rsp_E1) set to start of UDP data
//******************************************************************************

void Process_E1(UDPPacket * pUDP)
{
  PBYTE pUdpData = pUDP->GetDataBuffer();      //get pointer to UDP data field
  Rsp_E1 * pRsp = (Rsp_E1 *) pUdpData;         //set pointer for response data

  if (pUDP->GetDataSize() != CMD_LEN_E1)       //check for valid command length
  { pRsp->Rsp = INVALID_MSG_LENGTH;
  }
  else
  { pRsp->Rsp = COMMAND_OK;                    //set response code
    pRsp->Data = 0xFF;                         //set response data to 0xFF
    pUDP->SetDataSize(RSP_LEN_E1);             //set response packet data size
  }
}


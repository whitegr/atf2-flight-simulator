//******************************************************************************
//  Telnet Test Program
//  Filename: Telnet.cpp
//  08/09/07  First version from Netburner example TelnetCmd
//
//******************************************************************************

#include <C:\Nburn\EPSC\include\includes.h>    //Include all headers for project

#include <iosys.h>
#include <utils.h>
#include <ip.h>
#include <tcp.h>
#include <iointernal.h>

#define TCP_PORT_TO_USE               (23)      // The Telnet port
#define TIMEOUT_LIMIT                 (60)      // 10 sec per count
#define OVERRIDE_TIMEOUT              (20)      // 10 sec per count

#define MaxTelnetBuf (2048)        //Size of Telnet buffer for response messages
char  TelnetMsgBuf[MaxTelnetBuf];  //Declare Telnet message buffer
WORD  TelnetMsgBufPtr = 0;         //Declare Telnet message buffer pointer
int   fdTelnet = 0;                //File descriptor for Telnet write buffer

//************************ void TelnetCmdMenu( int fd ) ************************
//  Write command menu to telnet buffer
//******************************************************************************

void TelnetCmdMenu( int fd )
{
  writestring( fd, "EPSC Telnet Commands\r\n");
  writestring( fd, "'A' ADC2 Data\r\n");
  writestring( fd, "'C' Calibration Coefficients\r\n");
  writestring( fd, "'D' Daughterbrd EEPROM Data\r\n");
  writestring( fd, "'E' Error Messages\r\n");
  writestring( fd, "'H' Fault History Buffers\r\n");  
  writestring( fd, "'I' Interlock Status\r\n");
  writestring( fd, "'L' Show Local Control LEDs\r\n"); 
  writestring( fd, "'M' Motherbrd EEPROM Data\r\n");
  writestring( fd, "'P' Power Supply Module Data\r\n");
  writestring( fd, "'R' Show Ramp Data\r\n");
  writestring( fd, "'V' Version\r\n");
  writestring( fd, "'X' Exit Telnet Session\r\n");
}

//********* int ProcessTelnetString( int fd, char * buf, int NumChar) **********
//  Process Telnet input string. If charriage return, processes last alpha
//  character recieved.  Returns number of characters sent
//******************************************************************************

int ProcessTelnetString( int fd, char * buf, int NumChar) 
{
  static char LastChar = 0;
  int i = 0;
  while ( i < NumChar )
  {
    if (buf[ i ] == 13 )
    {  
      TelnetMsgBufPtr = 0;                     //Clear Telnet message buffer

      if      ( LastChar == 'A' )  spShowADC2Dat( fdTelnet );
      else if ( LastChar == 'C' )  spShowCalCoef( fdTelnet );
      else if ( LastChar == 'D' )  spShowDbEEP( fdTelnet );
      else if ( LastChar == 'E' )  spShowErrMsg( fdTelnet );
      else if ( LastChar == 'H' )  spShowFltHistBuf( fdTelnet );
      else if ( LastChar == 'I' )  spShowInterlockStat( fdTelnet );
      else if ( LastChar == 'L' )  spShowLocCntlLEDs( fdTelnet );
      else if ( LastChar == 'M' )  spShowMbEEP( fdTelnet );
      else if ( LastChar == 'P' )  spShowPSModDat( fdTelnet );
      else if ( LastChar == 'R' )  spShowRamp( fdTelnet );
      else if ( LastChar == 'V' )  spShowVersion( fdTelnet );
      else if ( LastChar != 'X' )  TelnetCmdMenu( fdTelnet );   //Show menu
      else { LastChar = 0; return -1; }       //exit telnet session

      writestring( fdTelnet, "EPSC Telnet> ");
      write( fd, TelnetMsgBuf, TelnetMsgBufPtr );

      LastChar = 0;                           //Clear last character
      return TelnetMsgBufPtr;                 //Return number of characters sent
    }
    else
    {  
      if ( isalpha( buf[ i ] ) )             //If character is alpha 'a-z'
        LastChar = toupper( buf[ i ] );      //save in LastChar as upper case
      ++i;                                   //go to next character
    }
  }
  return 0;                                  //No carriage return found   
}

//************* ADDING EXTRA (USER DEFINED) FILE DESCRIPTORS (FD) ************** 
//  from iointernal.h, structures to allow user defined read and write
//
//  struct IoExpandStruct
//   {
//         int ( * read )  ( int fd, char *buf, int nbytes ); 
//         int ( * write ) ( int fd, const char *buf, int nbytes ); 
//         int ( * close ) ( int fd );            
//         void *extra;
//   };
//
//   int GetExtraFD( void *extra_data, struct IoExpandStruct *pFuncs );
//
//******************************************************************************

IoExpandStruct TelnetFD;

int TelnetBufWrite( int fd, const char * buf, int nbytes )
{
  int i = 0;
  while ( ( i < nbytes ) && ( TelnetMsgBufPtr < MaxTelnetBuf ) )
  {
    TelnetMsgBuf[ TelnetMsgBufPtr++ ] = buf[ i++ ]; 
  }
  return( i );  //Return number of bytes copied
}

int InitTelnetFD( void )
{
  TelnetFD.read  = 0;                 //Pointer to read function
  TelnetFD.write = TelnetBufWrite;    //Pointer to write function
  TelnetFD.close = 0;                 //Pointer to close function
  TelnetFD.extra = 0;                 //Pointer to extra data  

  return ( GetExtraFD( 0, & TelnetFD ) ); //Return FD for Telnet IO
}

//*************************** Start of Telnet Task *****************************

void TelnetTask(void * pd)
{ pd=pd;

  OSTimeDly(1 * TICKS_PER_SECOND);             //delay 1 second

  fdTelnet = InitTelnetFD( );
  int fdListen = listen( INADDR_ANY, TCP_PORT_TO_USE, 5 );


  if ( ( fdTelnet > 0 ) && ( fdListen > 0 ) )
  {
    IPADDR client_addr;
    WORD port;

    while ( 1 )
    {
      int fdnet = accept( fdListen, &client_addr, &port, 0 );
      writestring( fdnet, "Connection Opened \r\n" );
      int tcp_timeout = 0;
      
      char CarriageReturn = 13;
      ProcessTelnetString( fdnet, & CarriageReturn, 1 ); 
      
      while ( fdnet > 0 )
      {
        fd_set read_fds;
        fd_set error_fds;

        FD_ZERO( & read_fds );
        FD_SET( fdnet, & read_fds );

        //Check for a waiting socket after override time
        if ( tcp_timeout >= OVERRIDE_TIMEOUT )
          FD_SET( fdListen, &read_fds );

        FD_ZERO( & error_fds );
        FD_SET( fdnet, & error_fds );

        if ( select( FD_SETSIZE,
                     & read_fds,                     //read FD set
                     (fd_set *) 0,                   //write FD set
                     & error_fds,                    //error FD set
                     TCP_WRITE_TIMEOUT ) )           //TCP timeout (10 sec)
        {
          if ( FD_ISSET( fdnet, &read_fds ) )
          {
            char buffer[40];
            int n = read( fdnet, buffer, 40 );       //Get received string
            int rsp = ProcessTelnetString( fdnet, buffer, n);  //Process string 
            if ( rsp == -1 )  //Close connection if -1 was returned
            {
              writestring( fdnet, "Connection closed\r\n" );
              close( fdnet );
              fdnet = 0;
            }
            else
              tcp_timeout = 0;                       //Restart TCP timer
          }

          if ( FD_ISSET( fdnet, &error_fds ) )       //Error reported
          {
            close( fdnet );                          //Close connection
            fdnet = 0;                               //Clear network FD
           }

          if ( FD_ISSET( fdListen, &read_fds ) )     //New request on port 23
          {
            writestring( fdnet, "Connection was Overidden\r\n" );
            close( fdnet );
            fdnet = 0;
          }
        }
        else                                         //Select Timed out
        {
          tcp_timeout++;
          if ( tcp_timeout > TIMEOUT_LIMIT )
          {
            writestring( fdnet, "Connection Timed out and Closed\r\n" );
            close( fdnet );
            fdnet = 0;
          }
        }    // end if ( Select() )
      }      // end while ( fdnet > 0 )
    }        // end while ( 1 )
  }          // end if ( fdListen > 0 )
}

//****************************** StartUart2Task() ******************************

DWORD  TelnetStk[USER_TASK_STK_SIZE] __attribute__(( aligned(16) ));

void StartTelnetTask()
{ OSTaskCreate(TelnetTask,                      //address of task code
               (void *) 0,                      //optional argument pointer
               &TelnetStk[USER_TASK_STK_SIZE],  //top of stack
               TelnetStk,                       //bottom of stack
               TELNET_PRIO);                    //task priority
}



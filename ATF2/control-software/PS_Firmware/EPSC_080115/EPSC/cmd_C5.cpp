//******************************************************************************
//  TURN OFF POWER SUPPLY (0xC5) (Ethernet Power Supply Controller)
//  Filename: cmd_C5.cpp
//  03/03/05
//  This command turns off the power supply.
//
//  1) Checks for valid command length and channel number of zero.
//  2) Checks for power supply in remote mode
//  3) If power supply is ON
//     a) Sets the last PS turn off code to 0
//     b) Generates an warning message if the DAC is greater then 0.1V
//  4) Sets the PS enable and error amp enable low to turn off power supply.
//  5) Checks that the power supply is indicating OFF (waits up to 1 second)
//
//******************************************************************************

#include <C:\Nburn\EPSC\include\includes.h>    //Include all headers for project

typedef struct          // C5 command structure
{ BYTE   Ctype;         // 00 command type 0xC5
  BYTE   Rsp;           // 01 response code
  BYTE   Tid;           // 02 Task ID
  BYTE   Chan;          // 03 Channel, must be zero
} __attribute__ ((packed))  Cmd_C5;

#define CMD_LEN_C5  4   // expected length of command message

typedef struct          // C5 response structure
{ BYTE   Ctype;         // 00 command type, returned unchanged
  BYTE   Rsp;           // 01 response code
  BYTE   Tid;           // 02 Task ID, returned unchanged
  BYTE   Chan;          // 03 Channel, returned unchanged
  WORD   Status01;      // 04-05 Status bytes 0 and 1
} __attribute__ ((packed))  Rsp_C5;

#define RSP_LEN_C5  6   // length of response message

//********************* void Process_C5(UDPPacket * pUDP) **********************
//  pUDP  pointer to object of class UDPPacket, passed to function
//  pCmd  pointer to command structure (type Cmd_C5) set to start of UDP data
//  pRsp  pointer to response structure (type Rsp_C5) set to start of UDP data
//******************************************************************************

void Process_C5(UDPPacket * pUDP)
{ BYTE errflag = NO_ERR;                         //error flag
  PBYTE pUdpData = pUDP->GetDataBuffer();        //get pointer to UDP data field
  Cmd_C5 * pCmd = (Cmd_C5 *) pUdpData;           //set pointer for command data
  Rsp_C5 * pRsp = (Rsp_C5 *) pUdpData;           //set pointer for response data

  if (pUDP->GetDataSize() != CMD_LEN_C5)         //check for valid command length
  { pRsp->Rsp = INVALID_MSG_LENGTH;
  }
  else if (pCmd->Chan !=0)                       //check for valid channel (0)
  { pRsp->Rsp = INVALID_CHAN_NUM;
  }
  else
  { if (xil.ps_status & STATUS_LOCAL)            //check for local mode
    { errflag = ERR;
      WrErrMsg("C5 ERROR, PS IN LOCAL MODE");
    }
    else
    { 
      if (~xil.ps_status & STATUS_PS_OFF)        //check for power supply ON
      { xil.LastOff = 0;                         //set last PS turn off code to zero
        LONG dac = xil.Des_DAC;                  //get current DAC setpoint
        if (dac < 0) dac = -dac;                 //get absolute value setpoint
        if (dac > (KDAC / 10))                   //set warning if DAC > .1V
          WrErrMsg("C5 WARNING, DAC NOT ZERO");  //KDAC is DAC counts per volt
      }

      xil.PsOnFlag = PS_OFF;                     //clear power supply ON flag
      xil.PsRegFlag = REG_OFF;                   //clear regulation enable flag
      CalErr &= ~DIG_REG_ERR;                    //clear Digital Reg error flag

      //set power supply and error amplifier enables OFF to turn off supply
      xil.ps_cntl = PSCNTL_EN_OFF + PSCNTL_ERAMP_OFF + PSCNTL_VLT_OFF;

      //check that power supply indicates status as OFF within 1 second
      SHORT Dly = TICKS_PER_SECOND;              //set Dly to 1 second
      while ( xil.ser_stath & SSH_PS_ON_STAT )   //while returned PS status is ON
      { if (--Dly <= 0)                          //if Dly <=0, 1 second expired
        { errflag = ERR;                         //set error flag and write message
          WrErrMsg("C5 ERROR, PS STATUS REMAINS ON");
          break;                                 //exit loop
        }
        else                                     //else
          OSTimeDly(1);                          //delay one OS tick
      }
    }
    pRsp->Rsp = COMMAND_OK;                      //set response code
    pRsp->Status01 = Status01(errflag);          //set status bytes 0 and 1
    pUDP->SetDataSize(RSP_LEN_C5);               //set response packet data size
  }
}


//******************************************************************************
//  RESET CONTROLLER PROCESSOR COMMAND (0xE3) (Ethernet Power Supply Controller)
//  Filename: cmd_E3.cpp
//  03/03/05
//  This message resets the processor. It does not generate a response message.
//******************************************************************************

#include <C:\Nburn\EPSC\include\includes.h>    //Include all headers for project

typedef struct          // E3 command structure
{ BYTE   Ctype;         // 00 command type 0xE3
  BYTE   Rsp;           // 01 response code
  BYTE   Tid;           // 02 Task ID, returned unchanged
  BYTE   Type;          // 03 Reset type, 0x00 is soft reset, 0x01 is hard reset
} __attribute__ ((packed))  Cmd_E3;

#define CMD_LEN_E3 4    // expected length of command message

//********************* void Process_E3(UDPPacket * pUDP) **********************
//  pUDP  pointer to object of class UDPPacket, passed to function
//  pCmd  pointer to command structure (type Cmd_E3) set to start of UDP data
//******************************************************************************

void Process_E3(UDPPacket * pUDP)
{
  PBYTE pUdpData = pUDP->GetDataBuffer();      //get pointer to UDP data field
  Cmd_E3 * pCmd = (Cmd_E3 *) pUdpData;         //set pointer for command data


  if (pUDP->GetDataSize() != CMD_LEN_E3)       //check for valid command length
  {
    pCmd->Ctype = INVALID_MSG_LENGTH;
  }
  else if (pCmd->Type > 1)                     //check for invalid reset type
  {
    pCmd->Ctype = UNKNOWN_COMMAND;
  }
  else
  {
    pCmd->Ctype = NO_RESPONSE;
    if (pCmd->Type == 1)                      //check for hard reset
    {
      if (~xil.ps_status & STATUS_LOCAL)      //check for local mode 06/19/06
      {
        xil.sys = SYS_HARD_RESET;
      }
    }
    else                                      //Type == 0, soft reset
    {
      xil.ResetType = SOFTWARE_RESET;
      xil.sys = SYS_SOFT_RESET;
    }
  }
}


//******************************************************************************
//  DIAGANOSTIC MESSAGE 1 COMMAND (0xCA) (Ethernet Power Supply Controller)
//  Filename: cmd_CA.cpp
//  03/03/05
//  This message returns diagnostic information including status bytes 0-3.
//******************************************************************************

#include <C:\Nburn\EPSC\include\includes.h>    //Include all headers for project

typedef struct          // CA command structure
{ BYTE   Ctype;         // 00 command type 0xCA
  BYTE   Rsp;           // 01 response code
  BYTE   Tid;           // 02 Task ID, set by sender, returned unchanged
  BYTE   Chan;          // 03 Chan number (must be zero)
} __attribute__ ((packed))  Cmd_CA;

#define CMD_LEN_CA  4   // expected length of command message

typedef struct          // CA response structure
{ BYTE   Ctype;         // 00 command type, returned unchanged
  BYTE   Rsp;           // 01 response code
  BYTE   Tid;           // 02 Task ID, returned unchanged
  BYTE   Chan;          // 03 Channel, returned unchanged
  WORD   Status01;      // 04-05 Status bytes 0 and 1
  WORD   Status23;      // 06-07 Status bytes 2 and 3
  BYTE   RampState;     // 08 Ramp state
  float  CurSetPnt;     // 09-12 Current Setpoint of ramp (amps)
  float  StrSetPtn;     // 13-16 Starting setpoint of ramp (amps)
  DWORD  TimeRem;       // 17-20 remaining time in ramp, 0.01 sec / cnt
  SHORT  Adc2Off;       // 21-22 ADC2 offset correction +/-0x7FFF
  SHORT  Adc2Gain;      // 23-24 ADC2 gain factor +/-0x7FFF
  SHORT  DacOff;        // 25-26 DAC offset +/-7FFF
  SHORT  DacGain;       // 27-28 DAC Gain, NOT USED
  BYTE   LastRst;       // 29 last reset code
  BYTE   LastOff;       // 30 last PS turn off code
  BYTE   CalErr;        // 31 ADC and DAC Calibration error codes
  BYTE   HrdFlt;        // 32 Hardware fault code
} __attribute__ ((packed))  Rsp_CA;

#define RSP_LEN_CA  33  // length of response message

//********************* void Process_CA(UDPPacket * pUDP) **********************
//  pUDP  pointer to object of class UDPPacket, passed to function
//  pCmd  pointer to command structure (type Cmd_CA) set to start of UDP data
//  pRsp  pointer to response structure (type Rsp_CA) set to start of UDP data
//******************************************************************************

void Process_CA(UDPPacket * pUDP)
{
  PBYTE pUdpData = pUDP->GetDataBuffer();      //get pointer to UDP data field
  Cmd_CA * pCmd = (Cmd_CA *) pUdpData;         //set pointer for command data
  Rsp_CA * pRsp = (Rsp_CA *) pUdpData;         //set pointer for response data

  if (pUDP->GetDataSize() != CMD_LEN_CA)       //check for valid command length
  { pRsp->Rsp = INVALID_MSG_LENGTH;
  }
  else if (pCmd->Chan !=0)                     //check for valid channel (0)
  { pRsp->Rsp = INVALID_CHAN_NUM;
  }
  else
  { pRsp->Status01 = Status01(NO_ERR);         //set status bytes 0 and 1
    pRsp->Status23 = Status23();               //set status bytes 2 and 3
    pRsp->RampState = xil.Ramp_State;          //Ramp state


    pRsp->CurSetPnt = RevPol(                  //negate if reverse polarity
                    (float)xil.Des_ADC         //Current Setpoint of ramp (amps)
                    * xil.db_eep.reg_iva       //times Reg xduct amps per volt (IVA)
                    / (float)KADC );           //divided by ADC2 counts per volt

    pRsp->StrSetPtn = RevPol(                  //negate if reverse polarity
                    (float)xil.ADC_SetPnt[0]   //Starting setpoint of ramp (amps)
                    * xil.db_eep.reg_iva       //times Reg xduct amps per volt (IVA)
                    / (float)KADC );           //divided by ADC2 counts per volt

    //remaining time in ramp, converted to 0.01 sec/cnt
    pRsp->TimeRem = Chg120To100( xil.Ramp_Times[0] );

    //ADC2 offset factor scaled to short +/-0x7FFF
    pRsp->Adc2Off = (SHORT)LL_KMULT(ADC2_Offset, 0x7FFF, MAX_ADC_OFFSET);

    //ADC2 gain factor scaled to short +/-0x7FFF
    pRsp->Adc2Gain = (SHORT)LL_KMULT(ADC2_Gain, 0x7FFF, MAX_ADC_GAIN);

    //DAC offset scaled to short +/-7FFF
    pRsp->DacOff = (SHORT)LL_KMULT(xil.DAC_Offset, 0x7FFF, MAX_DAC_OFFSET);

    pRsp->DacGain = 0;                         //DAC Gain, NOT USED

    //convert data to little endian (Intel) by reversing byte order
    flip4((PBYTE) & pRsp->CurSetPnt);
    flip4((PBYTE) & pRsp->StrSetPtn);
    flip4((PBYTE) & pRsp->TimeRem);
    flip2((PBYTE) & pRsp->Adc2Off);
    flip2((PBYTE) & pRsp->Adc2Gain);
    flip2((PBYTE) & pRsp->DacOff);

    pRsp->LastRst = (BYTE) xil.LastReset;      //last reset code
    pRsp->LastOff = (BYTE) xil.LastOff;        //last PS turn off code
    pRsp->CalErr  = (BYTE) CalErr;             //ADC and DAC Calibration error codes
    pRsp->HrdFlt  = (BYTE) HrdFlt;             //Hardware Fault code

    pRsp->Rsp = COMMAND_OK;                    //set response code
    pUDP->SetDataSize(RSP_LEN_CA);             //set data size for response packet
  }
}


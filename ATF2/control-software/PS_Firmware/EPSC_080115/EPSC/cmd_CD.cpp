//******************************************************************************
//  LAST READ STATUS COMMAND (0xCD) (Ethernet Power Supply Controller)
//  Filename: cmd_CD.cpp
//  03/03/05
//  This message returns the last value read by ADC2 for the power supply current
//  (regulated transductor) and status bytes 1 and 2.
//******************************************************************************

#include <C:\Nburn\EPSC\include\includes.h>    //Include all headers for project

typedef struct          // CD command structure
{ BYTE   Ctype;         // 00 command type 0xCD
  BYTE   Rsp;           // 01 response code
  BYTE   Tid;           // 02 Task ID
  BYTE   Chan;          // 03 Channel number (must be zero)
} __attribute__ ((packed))  Cmd_CD;

#define CMD_LEN_CD 4    // length of command message

typedef struct          // CD response structure
{ BYTE   Ctype;         // 00 command type, returned unchanged
  BYTE   Rsp;           // 01 response code
  BYTE   Tid;           // 02 Task ID, returned unchanged
  BYTE   Chan;          // 03 Channel, returned unchanged
  WORD   Status01;      // 04-05 Status bytes 0 and 1
  float  PSCur;         // 06-09 PS output current read from reg transductor
} __attribute__ ((packed))  Rsp_CD;

#define RSP_LEN_CD 10   // length of response message

//********************* void Process_CD(UDPPacket * pUDP) **********************
//  pUDP  pointer to object of class UDPPacket, passed to function
//  pCmd  pointer to command structure (type Cmd_CD) set to start of UDP data
//  pRsp  pointer to response structure (type Rsp_CD) set to start of UDP data
//******************************************************************************

void Process_CD(UDPPacket * pUDP)
{
  float I;                                     //Power supply current
  PBYTE pUdpData = pUDP->GetDataBuffer();      //get pointer to UDP data field
  Cmd_CD * pCmd = (Cmd_CD *) pUdpData;         //set pointer for command data
  Rsp_CD * pRsp = (Rsp_CD *) pUdpData;         //set pointer for response data

  if (pUDP->GetDataSize() != CMD_LEN_CD)       //check for valid command length
  { pRsp->Rsp = INVALID_MSG_LENGTH;
  }
  else if (pCmd->Chan !=0)                     //check for valid channel (0)
  { pRsp->Rsp = INVALID_CHAN_NUM;
  }
  else
  { I = (float)ADC2_Data_Reg[MUX2_REG]         //Reg xduct ADC2 reading
        * xil.db_eep.reg_iva                   //times Reg xduct amps per volt (IVA)
        / (float)KADC;                         //divided by ADC2 counts per volt
    I = RevPol(I);                             //Negate I if reversing switch on
    flip4((PBYTE) &I);                         //convert to little endian
    pRsp->Status01 = Status01(NO_ERR);         //set status bytes 0 and 1
    pRsp->PSCur = I;                           //set PS current
    pRsp->Rsp = COMMAND_OK;                    //set response code
    pUDP->SetDataSize(RSP_LEN_CD);             //set data size for response packet
  }
}


//******************************************************************************
//  Hardware Initialization for Coldfire MFC5282 Processor
//  Filename: init5282.cpp
//  01/13/05
//******************************************************************************

#include <C:/Nburn/EPSC/include/includes.h>

//******************************************************************************
//  Initialize chip select 3 (CS3) for Xilinx FPGA to base address 0xA0000000
//  Auto acknowlege required for long word transfers (5282 does not increment
//  address lines in multi word transfers with auto acknowledge off).
//  See MCF5282 Coldfire User's Manual Chapter 12 for complete information
//  *NOTE* address of structure "xil" must be set in "sys.ld" to 0xA0000000
//******************************************************************************

void InitChipSelect3()
{
  sim.cs[3].csar = (0xA0000000 >> 16);     //Set base address for CS3
  sim.cs[3].cscr = 0x0980;                 //16 bit port, auto ack after 2 clks
  sim.cs[3].csmr = 0x00000001;             //Set valid bit
}

//******************************************************************************
//  Configure Interrupt
//  Simplified version of SetIntc, only supports interrupt vectors 1 to 31
//  intc   = interrupt controller, 0 or 1
//  func   = interrupt service routine starting address
//  vector = interrupt vector number, IRQ1=1, 3, 5
//  level  = interrupt level, 1 (lowest) to 6 (highest)
//  prio   = priority within a level, 1 (lowest) to 7 (highest)
//******************************************************************************

void SetIntrCntl(int intc, long func, int vector, int level, int prio )
{
  DWORD mask;
  mask=(1 << vector);
  sim.intc[intc].imrl |= mask;            //set mask to diasble interrupt

  sim.intc[0].icrn[vector] = ( ((level & 7) << 3) | (prio & 7) );
  vector_base.table[(intc * 64) + 64 + vector] = func;

  sim.intc[intc].imrl &= ~mask;           //clear interrupt mask bit
  sim.intc[intc].imrl &= 0xFFFFFFFE;      //clear global mask bit
}

//************************* INITIALIZE AND ENABLE IRQ1 *************************

extern "C" { void irq5_isr(); }

//***************** set to use IRQ3 as test  4 /1/05 ***************************

void Enable_IRQ5()  //use irq5
{ sim.eport.eppar = 0x0800;   //set irq5 to falling edge trigger
  sim.eport.epddr = 0x0000;   //set all edgeport pins as inputs
  sim.eport.epier = 0x0020;   //enable irq7

  SetIntrCntl(0, (long) & irq5_isr, 5, 5, 1);
}


//void Enable_IRQ1()
//{ sim.eport.eppar=0x0080;   //set irq3 to falling edge trigger
//  sim.eport.epddr=0x0000;   //set all edgeport pins as inputs
//  sim.eport.epier=0x0008;   //enable irq3
//
//  SetIntrCntl(0, (long)&irq1_isr, 3, 3, 1);
//}

//void Enable_IRQ1()
//{ sim.eport.eppar=0x0008;   //set irq1 to falling edge trigger
//  sim.eport.epddr=0x0000;   //set all edgeport pins as inputs
//  sim.eport.epier=0x0002;   //enable irq1
//
//  SetIntrCntl(0, (long)&irq1_isr, 1, 1, 1);
//}

//****************************** void SetCTSRTS() ******************************
//  Configure MCF5282 I/O pins for CTS, RTS for UARTS 0 and 1
//
//  URTS0 = TC3 (DTIN3), UCTS0 = TC1 (DTIN1)
//  URTS1 = TD3 (DTIN1), UCTS1 = TD1 (DTIN0)
//
//  "sim.gpio.ptcpar" configures bits TC0-3 for alternate functions
//  See MCF5282 Users manual, section 26.3.2.13
//        TC0 (D1-0)     TC1 (D3-2)     TC2 (D5-4)     TC3 (D7-6)
//  00    digital I/O    digital I/O    digital I/O    digital I/O
//  01    UCTS0         *UCTS0          URTS0         *URTS0
//  10    UCTS1          UCTS1          URTS1          URTS1
//  11    DTOUT2         DTIN2          DTOUT3         DTIN3
//
//  "sim.gpio.ptdpar" configures bits TD0-3 for alternate functions
//  See MCF5282 Users manual, section 26.3.2.14
//        TD0 (D1-0)     TD1 (D3-2)     TD2 (D5-4)     TD3 (D7-6)
//  00    digital I/O    digital I/O    digital I/O    digital I/O
//  01    UCTS0          UCTS0          URTS0          URTS0
//  10    UCTS1         *UCTS1          URTS1         *URTS1
//  11    DTOUT0         DTIN0          DTOUT1         DTIN1
//******************************************************************************

void SetCtsRts()
{
  //get congiuration register (ptcpar) and set TC1 = UCTS0, TC3= URTS0
  WORD tptcpar = (sim.gpio.ptcpar & 0x33) | 0x44;
  //if TC2 not set to DTOUT3 (11), set to digital I/O (00)
  if ((tptcpar & 0x30) == 0x30) tptcpar &= ~0x30;
  //if TC0 not set to DTOUT2 (11), set to digital I/O (00)
  if ((tptcpar & 0x03) == 0x03) tptcpar &= ~0x03;
  sim.gpio.ptcpar = tptcpar ;

  //get congiuration register (ptdpar) and set TD1 = UCTS1, TD3= URTS1
  WORD tptdpar = (sim.gpio.ptdpar & 0x33) | 0x88;
  //if TD2 not set to DTOUT1 (11), set to digital I/O (00)
  if ((tptdpar & 0x30) == 0x30) tptdpar &= ~0x30;
  //if TD0 not set to DTOUT0 (11), set to digital I/O (00)
  if ((tptdpar & 0x03) == 0x03) tptdpar &= ~0x03;
  sim.gpio.ptdpar = tptdpar ;
}

//*************************** void UartHwFlow(unum) ****************************
//  Enable hardware RTS/CTS flow control for UART0 and UART1
//  Requires previous call to SetCtsRts() to configure I/O lines. This function
//  should be called immediately following the UART open command
//
//  This function writes the UART mode registers (UMR1-2). The function must
//  first write the command register to point to UMR1. Reading or writing UMR1
//  advances the pointer to UMR2. The follwoing bits are set;
//
//  UMR1 D7 RxRTS  Set to 1, enables the receive FIFO to set RTS when full
//  UMR2 D5 TxRTS  Set to 0, disables transmitter control of RTS line
//  UMR2 D4 TxCTS  Set to 1, enables CTS line to control tranmission
//
//  See MCF5282 Users manual, section 23
//
//******************************************************************************
void UartHwFlow(int unum)
{ if ((unum < 0) | (unum > 1)) return ;

  //enable UART1 receiver to output RTS and transmitter to use CTS
  sim.uarts[unum].ucr = 0x10;           //reset pointer to umr1
  BYTE tmp1 = sim.uarts[unum].umr;      //read UART mode register umr1
  BYTE tmp2 = sim.uarts[unum].umr;      //read UART mode register umr2

  sim.uarts[unum].ucr = 0x10;           //reset pointer to umr1
  //set receiver RTS bit in UART mode register umr1
  sim.uarts[unum].umr = tmp1 | 0x80;
  //clear transmiter RTS bit (0x20) and set CTS bit (0x10) in register umr2
  sim.uarts[unum].umr = (tmp2 & 0xDF) | 0x10;
  //set UART1 UartState to hardware flow control
  UartData[unum].m_UartState |= UART_FLOW_TXRTSCTS;
}

//**************************** void SetUart2RxTx() *****************************
//  Temporary kluge to set UART2 to use CAN pins 03/17/05
//  UTDX2 = AS2 (CANTX), URDX2 = AS3 (CANRX)
//  This function needs to be called immediately after the open command. The
//  open command sets the I/O lines to the default UTXD2 = AS0, URXD2 = AS1
//
//  "sim.gpio.paspar" (16 bit) configures bits AS0-5 for alternate functions
//  See MCF5282 Users manual, section 26.3.2.10
//
//        AS0 (D1-0)     AS1 (D3-2)     AS2 (D5-4)     AS3 (D7-6)
//  0x    digital I/O    digital I/O    digital I/O    digital I/O
//  10    UTXD2          URXD2          UTXD2          URXD2
//  11    SCL            SDA            CANTX          CANRX
//******************************************************************************

void SetUart2RxTx()
{
  //get congiuration register (paspar) and set AS2 = UTXD2, AS3= URXD2
  WORD tpaspar = (sim.gpio.paspar & 0xFF0F) | 0x00A0;
  //if bit AS0 set to UTXD2 (D1-0 = 10B), set to IO (D1-0 = 00B)
  if ((tpaspar & 0x0003) == 0x0002) tpaspar &= ~0x0003;
  //if bit AS1 set to URXD2 (D3-2 = 10B), set to IO (D3-2 = 00B)
  if ((tpaspar & 0x000C) == 0x0008) tpaspar &= ~0x000C;
  //set bit AS2 to UTXD2 (D5-4 = 10B) and AS3 to URXD2 (D7-6 = 10B)
  sim.gpio.paspar = tpaspar ;
}

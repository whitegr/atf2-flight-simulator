//******************************************************************************
//  Diagnostic Port Display Functions
//  Filename: display.cpp
//  07/20/07 Added display of OCEM power supply module data from UART1
//  05/05/05 First Version of File
//
//  These functions allow display of controller internal data and states.
//
//******************************************************************************

#include <C:\Nburn\EPSC\include\includes.h>    //Include all headers for project

//*************** void spShowHex(int fd, WORD * pDat, WORD len) ****************
//
//  Prints the hex and ASCII values of the data pointed to by pDat. The number
//  of words to be printed is passed in len. The function uses words to allow the
//  Xilinx data to be read.
//
//  ShowHex(fd, (WORD *) &xil.mb_eep, 64 )  Typical usage
//******************************************************************************

void spShowHex(int fd, WORD * pDat, WORD len)
{ WORD i = 0, j = 0;
  char c;
  char buf[8];

  while (i < len)
  { for (j=0; j < 8; ++j)                      //print HEX data
    {
      if ((i + j) < len)
      {
        sprintf(buf, "%04X ", pDat[i + j]);
        write(fd, buf, 5);
      }
      else
        writestring(fd, "     ");
    }

    writestring(fd, "     ");                  //print ASCII data
    for (j=0; j < 8; ++j)
    {
      if ((i + j) < len)
      {
        c = (char) (pDat[ i + j ] / 256);     //write high byte of word
        if (c < ' ') c = '.';
        write(fd, &c, 1);

        c = (char) (pDat[ i + j ] % 256);     //write low byte of word
        if (c < ' ') c = '.';
        write(fd, &c, 1);
      }
    }
    writestring(fd, "\r\n");
    i = i + 8;
  }
}

//********************** void spShowInterlockStat(int fd) **********************
//  Write interlock status to serial port
//
//  current status                 xil.ser_statl
//  status latched at turn on      xil.ser_statl_lch0
//  status latched at trip         xil.ser_statl_lch1
//
//  DATA_FAULT    0x8000    serial data fault, data not valid
//  GND_CUR       0x4000    Ground Current Interlock, 1 = Fault
//  REG_XDUCT     0x2000    Regulated Transductor Interlock, 1 = Fault
//  PS_READY      0x1000    Power Supply Ready Interlock, 1 = Fault
//  MAG_INT3      0x0800    Magnet Interlock 3, 1 = Fault
//  MAG_INT2      0x0400    Magnet Interlock 2, 1 = Fault
//  MAG_INT1      0x0200    Magnet Interlock 1, 1 = Fault
//  MAG_INT0      0x0100    Magnet Interlock 0, 1 = Fault
//  KLX_INT1      0x0080    Klixon Interlock 1, 1 = Fault
//  KLX_INT0      0x0040    Klixon Interlock 0, 1 = Fault
//  50KHZ_CLK     0x0020    50KHz Serial Status Clock fault
//  PS_STAT3      0x0008    Power Supply Status 3, set by Power Supply
//  PS_STAT2      0x0004    Power Supply Status 2, set by Power Supply
//  PS_STAT1      0x0002    Power Supply Status 1, set by Power Supply
//  PS_STAT0      0x0001    Power Supply Status 0, set by Power Supply
//
//******************************************************************************

void spShowInterlockStat(int fd)
{
  const char * pFS[16] = {"Data Fault" ,
                          "Ground Current" ,
                          "Reg Transductor" ,
                          "Power Supply Ready" ,
                          "Magnet Interlock 3" ,
                          "Magnet Interlock 2" ,
                          "Magnet Interlock 1" ,
                          "Magnet Interlock 0" ,
                          "Klixon Interlock 1" ,
                          "Klixon Interlock 0" ,
                          "Serial Clock Fault" ,
                          "Not Used" ,
                          "Power Supply Status 3" ,
                          "Power Supply Status 2" ,
                          "Power Supply Status 1" ,
                          "Power Supply Status 0" };

  WORD d0 = xil.ser_statl_lch0;   //status latched at turn on
  WORD d1 = xil.ser_statl_lch1;   //status latched at trip
  WORD d2 = xil.ser_statl;        //current status
  WORD mask = 0x8000;             //status bit mask

  char buf[64];                   //output string buffer
  int len;                        //buffer length

  writestring(fd, "Interlock Status (Value at Turn ON / Trip / Current)\r\n");
  for (WORD i = 0; i < 16; ++i)
  {
    len = sprintf(buf, "%1d / %1d / %1d  %s\r\n",
                      ( (d0 & mask) ? 1 : 0),
                      ( (d1 & mask) ? 1 : 0),
                      ( (d2 & mask) ? 1 : 0),
                      pFS[i] );
    write(fd, buf, len);
    mask = mask >> 1;
  }
}

//************************* void spShowADC2Dat(int fd) *************************
//  Write ADC2 data to serial port
//
//  offset (ground),   chan 0         temperature,       chan 12
//  reference,         chan 1         klixon 0 cur,      chan 13
//  DAC voltage,       chan 2         klixon 1 cur,      chan 14
//  reg transductor,   chan 3         spare (TV10),      chan 15
//  aux transductor,   chan 4         unreg +15V,        chan 16
//  abs ripple,        chan 5         reg +15V,          chan 17
//  analog 10V ref,    chan 6         unreg -15V,        chan 18
//  analog 5V,         chan 7         reg -15V,          chan 19
//  PS volts,          chan 8         digital 5.0V,      chan 20
//  gnd current,       chan 9         digital 3.3V,      chan 21
//  abs gnd current,   chan 10        digital 2.5v,      chan 22
//  external input,    chan 11        digital 1.2V,      chan 23
//******************************************************************************

void spShowADC2Dat(int fd)
{
  char buf[64];
  int len;
  float Vlt, Amp, Cond;
  float KCV = 1 / (float) KADC;      //conversion factor, ADC counts to volts

  writestring( fd, "External Signals     Volts       Scaled Units\r\n");


  Vlt = (float) ADC2_Data_Reg[MUX2_REG] * KCV;
  Amp = Vlt * xil.db_eep.reg_iva;
  len = sprintf(buf, "Reg Transductor    %8.5f    %10.5f amp\r\n", Vlt, Amp);
  write( fd, buf, len );

  Vlt = (float) ADC2_Data_Reg[MUX2_AUX] * KCV;
  Amp = Vlt * xil.db_eep.aux_iva;
  len = sprintf(buf, "Aux Transductor    %8.5f    %10.5f amp\r\n", Vlt, Amp);
  write( fd, buf, len );

  Vlt = (float) ADC2_Data_Reg[MUX2_DAC] * KCV;
  Amp = Vlt * xil.db_eep.reg_iva;
  len = sprintf(buf, "DAC                %8.5f    %10.5f amp\r\n", Vlt, Amp);
  write( fd, buf, len );

  Vlt = (float) ADC2_Data_Reg[MUX2_ARIP] * KCV;
  Amp = Vlt * xil.db_eep.reg_iva * 0.01;
  len = sprintf(buf, "Abs Ripple Cur     %8.5f    %10.5f amp\r\n", Vlt, Amp);
  write( fd, buf, len );

  Vlt = (float) ADC2_Data_Reg[MUX2_GND] * KCV;
  Amp = Vlt * xil.db_eep.gnd_iva;
  len = sprintf(buf, "Ground Current     %8.5f    %10.5f amp\r\n", Vlt, Amp);
  write( fd, buf, len );

  Vlt = (float) ADC2_Data_Reg[MUX2_AGND] * KCV;
  Amp = Vlt * xil.db_eep.gnd_iva;
  len = sprintf(buf, "Abs Ground Cur     %8.5f    %10.5f amp\r\n", Vlt, Amp);
  write( fd, buf, len );

  Vlt = (float) ADC2_Data_Reg[MUX2_PSV] * KCV;
  Amp = Vlt * xil.db_eep.psv_iva;
  len = sprintf(buf, "Pwr Supply Volts   %8.5f    %10.5f vlt\r\n", Vlt, Amp);
  write( fd, buf, len );

  Vlt = (float) ADC2_Data_Reg[MUX2_KLIX0] * KCV;
  Cond = KlixCond(Vlt);
  len = sprintf(buf, "Klixon 0           %8.5f   %7.1f micro mho\r\n", Vlt, Cond);
  write( fd, buf, len );

  Vlt = (float) ADC2_Data_Reg[MUX2_KLIX1] * KCV;
  Cond = KlixCond(Vlt);
  len = sprintf(buf, "Klixon 1           %8.5f   %7.1f micro mho\r\n", Vlt, Cond);
  write( fd, buf, len );

  Vlt = (float) ADC2_Data_Reg[MUX2_EXT] * KCV;
  len = sprintf(buf, "External Input     %8.5f\r\n\n", Vlt);
  write( fd, buf, len );

  writestring( fd, "Controller Internal Voltages\r\n" );
  
  Vlt = (float) ADC2_Data_Reg[MUX2_VDD] * KCV;
  len = sprintf(buf, "Digital 5.0V =%6.3f     ", Vlt);
  write( fd, buf, len );
  Vlt = (float) ADC2_Data_Reg[MUX2_VP15U] * KCV * 2;
  len = sprintf(buf, "Analog +15.0V =%7.3f\r\n", Vlt);
  write( fd, buf, len );

  Vlt = (float) ADC2_Data_Reg[MUX2_V3D3] * KCV;
  len = sprintf(buf, "Digital 3.3V =%6.3f     ", Vlt);
  write( fd, buf, len );
  Vlt = (float) ADC2_Data_Reg[MUX2_VM15U] * KCV * 2;
  len = sprintf(buf, "Analog -15.0V =%7.3f\r\n", Vlt);
  write( fd, buf, len );

  Vlt = (float) ADC2_Data_Reg[MUX2_V2D5] * KCV;
  len = sprintf(buf, "Digital 2.5V =%6.3f     ", Vlt);
  write( fd, buf, len );
  Vlt = (float) ADC2_Data_Reg[MUX2_VP15R] * KCV * 2;
  len = sprintf(buf, "Analog +14.5V =%7.3f\r\n", Vlt);
  write( fd, buf, len );

  Vlt = (float) ADC2_Data_Reg[MUX2_V1D2] * KCV;
  len = sprintf(buf, "Digital 1.2V =%6.3f     ", Vlt);
  write( fd, buf, len );
  Vlt = (float) ADC2_Data_Reg[MUX2_VM15R] * KCV * 2;
  len = sprintf(buf, "Analog -14.5V =%7.3f\r\n", Vlt);
  write( fd, buf, len );

  Vlt = (float) ADC2_Data_Reg[MUX2_TEMP] * KCV * 100.0;
  len = sprintf(buf, "Temperature  =%6.1f     ", Vlt);
  write( fd, buf, len );
  Vlt = (float) ADC2_Data_Reg[MUX2_A10V] * KCV;
  len = sprintf(buf, "Analog +10.0V =%7.3f\r\n", Vlt);
  write( fd, buf, len );

  len = sprintf(buf, "Fan RPM      =%6d     ", FanRPM);
  write( fd, buf, len );
  Vlt = (float) ADC2_Data_Reg[MUX2_A5V] * KCV;
  len = sprintf(buf, "Analog  +5.0V =%7.3f\r\n", Vlt);
  write( fd, buf, len );
}

//************************* void spShowErrMsg(int fd) **************************
//   Sends the 16 error message buffers and pointers to the serial port
//
//   ErrMsg[NumErrMsgBuf][ErrMsgBufSize]   error message array
//
//   NumErrMsgBuf     Number of error message buffers
//   ErrMsgBufSize    Number of characters in buffer
//   ErrMsgRdPtr      Pointer to next message to be read
//   ErrMsgWrPtr      Pointer to next message to be written
//
//******************************************************************************

void spShowErrMsg(int fd)
{
  char buf[80];
  int len;

  len = sprintf(buf, "Read Pointer  = %d \r\n", ErrMsgRdPtr);
  write(fd, buf, len);
  len = sprintf(buf, "Write Pointer = %d \r\n", ErrMsgWrPtr);
  write(fd, buf, len);

  writestring(fd, "ddd hh:mm:ss  ## = Error Message\r\n");
  for (short n = 1; n <= NumErrMsgBuf; ++n)
  {
    short i = (NumErrMsgBuf + ErrMsgWrPtr - n) % NumErrMsgBuf;
    char * pMsg = &ErrMsg[i][0];
    if (pMsg[0] != 0)
    {
      DWORD dt = ( TIMER - ErrMsgTime[i] ) / 10;        // convert to seconds
      WORD ds = (WORD)(  dt % 60 );                     // get seconds
      WORD dm = (WORD)(( dt / 60 ) % 60);               // get minutes
      WORD dh = (WORD)(( dt / 3600 ) % 24 );            // get hours
      WORD dd = (WORD)(  dt / 86400 );                  // get days
      len = sprintf(buf, "%3d %2d:%02d:%02d  ", dd, dh, dm, ds);
      write(fd, buf, len);
      len = sprintf(buf, "%2d = ", i);
      write(fd, buf, len);
      write(fd, pMsg, strlen( pMsg ) );
      writestring(fd, "\r\n");
    }
  }
}

//************************* void spShowVersion(int fd) *************************
//  This function writes the version information to the serial port
//******************************************************************************

void spShowVersion(int fd)
{
  char buf[64];
  int len;

  len = sprintf(buf, "Application name %s\r\n", AppName);
  write(fd, buf, len);
  len = sprintf(buf, "Firmware version %s\r\n", pFirmwareDate);
  write(fd, buf, len);
  len = sprintf(buf, "Xilinx version   %04X\r\n", xil.ver_num);
  write(fd, buf, len);
}

//************************* void spShowEEPHex(int fd) **************************
//  This function writes the EEPROM hex data image to the serial port
//******************************************************************************

void spShowEEPHex(int fd)
{
  writestring(fd, "Motherboard EEPROM Data\r\n");
  spShowHex(fd, (WORD *) &xil.mb_eep, 32 );
  writestring(fd, "Daughterboard EEPROM Data\r\n");
  spShowHex(fd, (WORD *) &xil.db_eep, 32 );
}

//************************** void spShowRamp(int fd) ***************************
//  This function writes ramp status to the serial port
//******************************************************************************

void spShowRamp(int fd)
{
  char buf[64];
  int len;
  float KA = xil.db_eep.reg_iva / (float) KADC;
  float KT = 1.0 / 120.0;
  int n = xil.Num_Ramp_Seg;

  len = sprintf(buf, "Ramp State = %d  Number of Setpoints = %d\r\n",
                xil.Ramp_State, n );
  write(fd, buf, len);

  len = sprintf(buf, "I0 = %10.4f amp\r\n",
                KA * (float) xil.ADC_SetPnt[0] );
  write(fd, buf, len);

  for(int i = 1; i <= n; ++i)
  {
    len = sprintf( buf, "I%1d = %10.4f amp, %7.2f sec\r\n",
                   i,
                   KA * (float) xil.ADC_SetPnt[i],
                   KT * (float) xil.Ramp_Times[i] );
    write(fd, buf, len);
  }

  len = sprintf( buf, "I  = %10.4f amp, %7.2f sec remaining\r\n",
                 KA * (float) xil.Des_ADC,
                 KT * (float) xil.Ramp_Times[0] );
  write(fd, buf, len);
}

//************************* void spShowCalCoef(int fd) *************************
//  This function writes the calibration coefficients to the serial port
//******************************************************************************

void spShowCalCoef(int fd)
{
  char buf[64];
  int len;
  
  float KAO = 100.0 / (float) MAX_ADC_OFFSET;
  float KAG = 100.0 / (float) MAX_ADC_GAIN;
  float KDO = 100.0 / (float) MAX_DAC_OFFSET;

  writestring(fd, "Auto Calibration Coefficients (percent of full scale)\r\n");

  len = sprintf( buf, "ADC 1 Offset = %6.1f%%  Gain = %6.1f%%\r\n",
                 KAO * (float) ADC1_Offset,
                 KAG * (float) ADC1_Gain );
  write(fd, buf, len);

  len = sprintf( buf, "ADC 2 Offset = %6.1f%%  Gain = %6.1f%%\r\n",
                 KAO * (float) ADC2_Offset,
                 KAG * (float) ADC2_Gain );
  write(fd, buf, len);

  len = sprintf( buf, "DAC Offset   = %6.1f%%\r\n",
                 KDO * (float) xil.DAC_Offset );
  write(fd, buf, len);

  // write calibration error registers and flags to serial port

  writestring(fd, "\r\nCalibration Error Register and Flags\r\n");

  const char * pCalStr[9] =
  {
    "Cal Err Reg = %02X  0xNN\r\n",
    "ADC1 Offset  = %1X  0x01\r\n",
    "ADC1 Gain    = %1X  0x02\r\n",
    "ADC2 Offset  = %1X  0x04\r\n",
    "ADC2 Gain    = %1X  0x08\r\n",
    "DAC Offset   = %1X  0x10\r\n",
    "Digital Reg  = %1X  0x20\r\n",
    "Spare        = %1X  0x40\r\n",
    "Spare        = %1X  0x80\r\n"
  };

  WORD Mask = 0x01;               //bit mask
  
  len = sprintf(buf, pCalStr[0], CalErr );
  write(fd, buf, len);
  
  for (WORD i = 1; i <= 8 ; ++i)
  {
    len = sprintf(buf, pCalStr[i], ( ( CalErr & Mask ) ? 1 : 0) );
    write(fd, buf, len);
    Mask = Mask << 1;
  }
}

//************************ void spShowPSModDat(int fd) *************************
//  This function writes the UART1 PS module data to the serial port
//  07/20/07 Added for OCEM/ATF2 power supply serial interface data (UART1)
//******************************************************************************

void spShowPSModDat(int fd)
{
  char buf[64];
  int len;
  if (PS_SERIAL_ENABLE)
  {
    writestring(fd, "Power Supply Module Data\r\n");
    len = sprintf( buf, "Data Valid = %1d, Num of Modules = %1d\r\n",
                     ModDataValid,
                     NumMod );
    write(fd, buf, len);                 //send formatted string to UART

    writestring(fd, "Num Flt Dis   ADC   Amps\r\n");
    BYTE FltBits = ModFault;             //Module Fault bits (1-8)
    BYTE DisBits = ModDisable;           //Module Disable bits (1-8)

    for (WORD i = 0; i < MaxNumMod; ++i)
    {
      len = sprintf( buf, " %1d   %1d   %1d  0x%03X %6.1f\r\n",
                     i+1,
                     FltBits & 0x01,
                     DisBits & 0x01,
                     ModCur[i],
                     (float) ModCur[i] * ModCur_CntToAmp );
      write(fd, buf, len);               //send formatted string to UART
      FltBits = FltBits >> 1;            //shift fault flags right one bit
      DisBits = DisBits >> 1;            //shift disable flags right one bit
    }
  }
  else
  {
    writestring(fd, "*** Power Supply Module Data not available ***\r\n");
    writestring(fd, "Enable PS Serial link in Daughterboard EEPROM\r\n");
  }
}

//*********************** void spShowFltHistBuf(int fd) ************************
//  This function writes the history buffers to the serial port
//  08/06/07 Added to allow viewing status from serial port
//
// LastOff                          last PS turn off code from intl_stat
// MAG_INT0 && KLX_INT0      0x01   Magnet interlock 0 and Klixon interlock 0
// MAG_INT1 && KLX_INT1      0x02   Magnet interlock 1 and Klixon interlock 1
// MAG_INT2                  0x04   Magnet interlock 2
// MAG_INT3                  0x08   Magnet interlock 3
// PS_READY                  0x10   Power Supply Fault (not ready)
// REG_XDUCT                 0x20   Regulated Transductor Fault
// GND_CUR                   0x40   Ground Current Fault
// DATA_FAULT && 50KHZ_CLK   0x80   Controller hardware fault (serial status)
//
// HrdFlt                           Hardware fault codes
// ADC1_FLT                  0x01   ADC1 fault
// ADC2_FLT                  0x02   ADC2 fault
// MB_EEP_FLT                0x04   Motherboard EEPROM data error
// DB_EEP_FLT                0x08   Daughter board EEPROM data error
// XIL_FLT                   0x10   Xilinx error
// WDT_FLT                   0x20   Watchdog Timer Reset
//
// LastReset                        Last reset code
// POWER_ON_RESET            0x01   Power On reset
// SOFTWARE_RESET            0x02   Software reset
// UART_RESET                0x04   UART (Diagnostic) reset
// LOCAL_RESET               0x08   Local control reset
// WATCHDOG_RESET            0x10   Watchdog reset
//
//******************************************************************************

void spShowFltHistBuf( int fd )
{
  const char * pHstStr[9] =
  {
    "Last Trip  = %02X   Last Reset = %02X   Hrd Flt   = %02X   0xNN\r\n",
    "MagnetFlt 0 = %1X   Power On    = %1X   ADC1 Flt   = %1X   0x01\r\n",
    "MagnetFlt 1 = %1X   Software    = %1X   ADC2 Flt   = %1X   0x02\r\n",
    "MagnetFlt 2 = %1X   UART (diag) = %1X   MB EEP     = %1X   0x04\r\n",
    "MagnetFlt 3 = %1X   Local Cntl  = %1X   DB EEP     = %1X   0x08\r\n",
    "PS Not Rdy  = %1X   Watchdog    = %1X   Xil FPGA   = %1X   0x10\r\n",
    "Reg Xduxt   = %1X   Spare       = %1X   Watchdog   = %1X   0x20\r\n",
    "Gnd Current = %1X   Spare       = %1X   Spare      = %1X   0x40\r\n",
    "Cntr Hdwr   = %1X   Netburner?  = %1X   Spare      = %1X   0x80\r\n"
  };

  char buf[96];                   //output string buffer
  int len;                        //buffer length
  WORD Mask = 0x01;               //bit mask

  writestring(fd, "Fault History Registers and Flags\r\n");  
  len = sprintf(buf, pHstStr[0], xil.LastOff, xil.LastReset, HrdFlt );
  write(fd, buf, len);
  
  for (WORD i = 1; i <= 8 ; ++i)
  {
    len = sprintf(buf, pHstStr[i],
      ( ( xil.LastOff   & Mask ) ? 1 : 0),
      ( ( xil.LastReset & Mask ) ? 1 : 0),
      ( ( HrdFlt        & Mask ) ? 1 : 0) );  
    write(fd, buf, len);
    Mask = Mask << 1;
  }
}

//*********************** void spShowLocCntlLEDs(int fd) ***********************
//  This function writes the status of the local control board LEDs to the
//  serial port.
//******************************************************************************

void spShowLocCntlLEDs( int fd )
{
  const char * pSysStatStr[8] =
  {
    "System Ready  = %s  Magnet Flt 0  = %s   Flt Latch On  = %s\r\n",
    "Pwr Supply On = %s  Magnet Flt 1  = %s   PS Status 0   = %s\r\n",
    "Local Mode    = %s  Magnet Flt 2  = %s   PS Stauts 1   = %s\r\n",
    "Voltage Mode  = %s  Magnet Flt 3  = %s   PS Status 2   = %s\r\n",
    "Rev Polarity  = %s  PS Flt        = %s   PS Status 3   = %s\r\n",
    "Ramp Ready    = %s  Reg Xduct Flt = %s   PS On Status  = %s\r\n",
    "Ramp On       = %s  Ground Flt    = %s   Aux Xduct Flt = %s\r\n",
    "Cal Fault     = %s  Cntl Flt      = %s   Spare         = %s\r\n"
  };

  const char * pOnOffStr[2] = { "--", "ON" };

  char buf[96];                   //output string buffer
  int len;                        //buffer length
  WORD Mask = 0x01;               //bit mask

  WORD Status1 = 0;                       //Clear all flags in Status 1
  WORD Status2 = xil.intl_stat >> 8;      //Set Status2 to interlock bits
  WORD Status3 = xil.intl_stat & 0x00FF;  //Set Status3 to status flags

  if (~xil.ps_status & STATUS_SYS_NRDY ) Status1 |= 0x0001;  //Set system ready  
  if (~xil.ps_status & STATUS_PS_OFF   ) Status1 |= 0x0002;  //Set PS On
  if ( xil.ps_status & STATUS_LOCAL    ) Status1 |= 0x0004;  //Set Local Mode
  if ( xil.intl_stat & 0x0040          ) Status1 |= 0x0008;  //Set Voltage Mode
  if (~xil.ps_status & STATUS_PS_OFF   ) Status1 |= 0x0010;  //Set Rev Polarity
  if ( xil.ps_status & STATUS_HOLD     ) Status1 |= 0x0020;  //Set Ramp Ready
  if ( xil.ps_status & STATUS_RAMP_ON  ) Status1 |= 0x0040;  //Set Ramp On
  if ( xil.ps_status & STATUS_CAL_ERR  ) Status1 |= 0x0080;  //Set Cal Fault
 
  Status3 = Status3 & ~0x0040;                               //Clear flag     
  if ( xil.ps_status & STATUS_AUX_NRDY ) Status3 |= 0x0040;  //Set Aux Xduct Flt  
                   

  writestring(fd, "Local Control LEDs\r\n");  
  for (WORD i = 0; i < 8 ; ++i)
  {
    len = sprintf(buf, pSysStatStr[i],
      pOnOffStr[ (( Status1 & Mask ) ? 1 : 0) ],
      pOnOffStr[ (( Status2 & Mask ) ? 1 : 0) ],
      pOnOffStr[ (( Status3 & Mask ) ? 1 : 0) ] );  
    write(fd, buf, len);
    Mask = Mask << 1;
  }
}



 

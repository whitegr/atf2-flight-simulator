//******************************************************************************
//  READ MESSAGE BUFFER COMMAND (0xC9) (Ethernet Power Supply Controller)
//  Filename: cmd_C9.cpp
//  03/03/05
//  This message returns the first unread message from the error message buffer.
//******************************************************************************

#include <C:\Nburn\EPSC\include\includes.h>    //Include all headers for project

typedef struct          // C9 command structure
{ BYTE   Ctype;         // 00 command type 0xC9
  BYTE   Rsp;           // 01 response code
  BYTE   Tid;           // 02 Task ID
  BYTE   Chan;          // 03 Channel number (must be zero)
} __attribute__ ((packed))  Cmd_C9;

#define CMD_LEN_C9 4    // expected length of command message

typedef struct          // C9 response structure
{ BYTE   Ctype;         // 00 command type, returned unchanged
  BYTE   Rsp;           // 01 response code
  BYTE   Tid;           // 02 Task ID, returned unchanged
  BYTE   Chan;          // 03 Channel, returned unchanged
  char   ErrMsg;        // 04-35 Error message, 0 to 32 characters
} __attribute__ ((packed))  Rsp_C9;

#define RSP_LEN_C9   4  // length of response message without error message

#define MAX_LEN_C9  32  // maximum length of error message

//******************* SHORT MsgCpy(char * pDes, char * pSrc) *******************
//  This function copies a null terminated string pointed to by pSrc to memory
//  pointed to by pDes. No more then MAX_LEN_C9 characters are copied. The number
//  of characters transferred is returned.
//******************************************************************************

SHORT MsgCpy(char * pDes, char * pSrc)
{ SHORT i=0;
  while ((pSrc[i] != 0) && (i < (MAX_LEN_C9)))
  { pDes[i] = pSrc[i];
    ++i;
  }
  return i;
}

//********************* void Process_C9(UDPPacket * pUDP) **********************
//  pUDP  pointer to object of class UDPPacket, passed to function
//  pCmd  pointer to command structure (type Cmd_C9) set to start of UDP data
//  pRsp  pointer to response structure (type Rsp_C9) set to start of UDP data
//******************************************************************************

void Process_C9(UDPPacket * pUDP)
{
  PBYTE pUdpData = pUDP->GetDataBuffer();      //get pointer to UDP data field
  Cmd_C9 * pCmd = (Cmd_C9 *) pUdpData;         //set pointer for command data
  Rsp_C9 * pRsp = (Rsp_C9 *) pUdpData;         //set pointer for response data

  if (pUDP->GetDataSize() != CMD_LEN_C9)       //check for valid command length
  { pRsp->Rsp = INVALID_MSG_LENGTH;
  }
  else if (pCmd->Chan !=0)                     //check for valid channel (0)
  { pRsp->Rsp = INVALID_CHAN_NUM;
  }
  else
  { short len;                                 //length of error message
    if (ErrMsgWrPtr != ErrMsgRdPtr)
    { len = MsgCpy(&pRsp->ErrMsg, ErrMsg[ErrMsgRdPtr]);
      ErrMsgRdPtr = (ErrMsgRdPtr + 1) % NumErrMsgBuf;
    }
    else
    { len = MsgCpy(&pRsp->ErrMsg, "MESSAGE BUFFER EMPTY");
    }
    pRsp->Rsp = COMMAND_OK;                    //set response code
    pUDP->SetDataSize(RSP_LEN_C9 + len);       //set data size for response packet
  }
}


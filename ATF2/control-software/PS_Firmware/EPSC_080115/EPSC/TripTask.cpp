//******************************************************************************
//  diagnostic task (Ethernet Power Supply Controller)
//  Filename: TripTask.cpp
//  05/23/05
//
//  This task only runs after a power supply trip is detected by the interrupt
//  task. The task waits for a semaphore to be set by the interrupt. The task
//  generates an error message detailing the source of the trip. The current
//  controller state and the ADC history buffers are copied to a trip report
//  file that can be accessed through the diagnostic ports.
//
//  Variable PsOnFlag is set when the power supply is turned ON. It is cleared
//  when the power supply is turned off. If the interrupt task detects that the
//  power supply status is off when the flag is set, it sets the trip semaphore
//  to enable the trip task.
//
//******************************************************************************

#include <C:\Nburn\EPSC\include\includes.h>   //Include all headers for project
BYTE TripReadADC2(WORD chan);                 //forward declaration
void WrTripMsg();                              //forward declaration

//************************** void DiagMain(void * pd) **************************

void TripTask(void * pd)
{ pd=pd;

//  printf("Trip Task Started at Priority %d\r\n", TRIP_PRIO);

  while(1)
  {
    OSSemPend(&START_TRIP_SEM, 0);
    printf("Power Supply Trip Detected\r\n");
    WrTripMsg();
  }
}

//****************************** StartTripTask() *******************************

DWORD  TripStk[USER_TASK_STK_SIZE] __attribute__(( aligned(16) ));

void StartTripTask()
{ OSTaskCreate(TripTask,                        //address of task code
               (void *) 0,                      //optional argument pointer
               &TripStk[USER_TASK_STK_SIZE],    //top of stack
               TripStk,                         //bottom of stack
               TRIP_PRIO);                      //task priority
}

//******************************************************************************
//  To request data from ADC2, the function sets ADC2_INST_REQ_FLAG, selects an
//  ADC channel (ADC2_INST_REQ_CHAN) and pends on a semaphore (ADC2_TRIP_SEM).
//  The ADC reads the selected channel and places the data in ADC2_INST_REQ_DATA.
//  The ADC then sets the semaphore to allow the function to continue.
//
//  ADC2_TRIP_SEM          Semaphore for instruction task
//  ADC2_TRIP_REQ_FLAG     ADC2 Instrustion read request flag
//  ADC2_TRIP_REQ_CHAN     ADC2 Instruction read request channel
//  ADC2_INST_REQ_DATA     ADC2 Instruction read request data
//
//  ADC2_TRIP_REQ_FLAG states
//  ADC2_REQ_OFF           ADC2 request off state
//  ADC2_REQ_ON            ADC2 request on state
//  ADC2_REQ_CONV          ADC2 request convert state
//  ADC2_REQ_DONE          ADC2 request done state, data valid
//
//  The function returns 1 if the semaphore returns without a timeout and the
//  request flag indicates request done, else function returns zero
//******************************************************************************
BYTE TripReadADC2(WORD chan)
{
  BYTE ErrFlag = ERR;
  if (ADC2_TRIP_REQ_FLAG == ADC2_REQ_OFF)  //check for request flag off
  {
    ADC2_TRIP_REQ_CHAN = chan;             //set channel
    ADC2_TRIP_REQ_FLAG = ADC2_REQ_ON;      //set request flag ON

    //pend on instruction semaphore, check for timeout, request done flag
    if (OS_NO_ERR == OSSemPend(&ADC2_TRIP_SEM, TICKS_PER_SECOND))
    {
      if (ADC2_TRIP_REQ_FLAG == ADC2_REQ_DONE) ErrFlag = NO_ERR;
    }
  }

  ADC2_TRIP_REQ_FLAG = ADC2_REQ_OFF;
  return (ErrFlag);
}

//****************************** void WrTripMsg() ******************************
//  Determine source of trip and write error message
//
//  status latched at turn on      xil.ser_statl_lch0
//  status latched at trip         xil.ser_statl_lch1
//
//  SSL_DATA_FAULT    0x8000    serial data fault, data not valid
//  SSL_GND_CUR       0x4000    Ground Current Interlock, 1 = Fault
//  SSL_REG_XDUCT     0x2000    Regulated Transductor Interlock, 1 = Fault
//  SSL_PS_READY      0x1000    Power Supply Ready Interlock, 1 = Fault
//  SSL_MAG_INT3      0x0800    Magnet Interlock 3, 1 = Fault
//  SSL_MAG_INT2      0x0400    Magnet Interlock 2, 1 = Fault
//  SSL_MAG_INT1      0x0200    Magnet Interlock 1, 1 = Fault
//  SSL_MAG_INT0      0x0100    Magnet Interlock 0, 1 = Fault
//  SSL_KLX_INT1      0x0080    Klixon Interlock 1, 1 = Fault
//  SSL_KLX_INT0      0x0040    Klixon Interlock 0, 1 = Fault
//  SSL_50KHZ_CLK     0x0020    50KHz Serial Status Clock fault
//  SSL_PS_STAT3      0x0008    Power Supply Status 3, set by Power Supply
//  SSL_PS_STAT2      0x0004    Power Supply Status 2, set by Power Supply
//  SSL_PS_STAT1      0x0002    Power Supply Status 1, set by Power Supply
//  SSL_PS_STAT0      0x0001    Power Supply Status 0, set by Power Supply
//
//******************************************************************************
void WrTripMsg()
{
  char buf[64];         //character buffer for error message
  int len;              //length of error message

  //mask bits that were faulted at turn on (MAG_INT0-1 KLX_INT0-1)
  WORD TripStat = xil.ser_statl_lch1 & ~xil.ser_statl_lch0;

  if (TripStat & SSL_50KHZ_CLK)
  {
    WrErrMsg("PS Trip, 50KHZ Clock Fault");
  }

  else if (TripStat & SSL_DATA_FAULT)
  {
    WrErrMsg("PS Trip, Interlock Data Fault");
  }

  else if (TripStat & SSL_PS_READY)
  {
    len = sprintf(buf, "PS Trip, Pwr Supply Rdy Fault %1XH",
                       xil.ser_statl_lch1 & 0x000F );
    buf [len] = 0;
    WrErrMsg(buf);
  }

  else if (TripStat & SSL_GND_CUR)
  {
    WrErrMsg("PS Trip, Ground Current");
  }

  else if (TripStat & SSL_REG_XDUCT)
  {
    WrErrMsg("PS Trip, Reg Transductor Fault");
  }

  else if (TripStat & SSL_KLX_INT0)
  {
    if ( TripReadADC2(MUX2_KLIX0) == NO_ERR)
    {
      float Cond = KlixCond((float) ADC2_Data_Reg[MUX2_KLIX0] / (float) KADC);
      len = sprintf(buf, "PS Trip, Klixon 0, %6.1f umho", Cond);
      buf [len] = 0;
      WrErrMsg(buf);
    }
    else
      WrErrMsg("PS Trip, Klixon 0");
  }

  else if (TripStat & SSL_KLX_INT1)
  {
    if ( TripReadADC2(MUX2_KLIX1) == NO_ERR)
    {
      float Cond = KlixCond((float) ADC2_Data_Reg[MUX2_KLIX1] / (float) KADC);
      len = sprintf(buf, "PS Trip, Klixon 1, %6.1f umho", Cond);
      buf [len] = 0;
      WrErrMsg(buf);
    }
    else
      WrErrMsg("PS Trip, Klixon 1");
  }

  else if (TripStat & SSL_MAG_INT0)
  {
    WrErrMsg("PS Trip, Magnet Interlock 0");
  }

  else if (TripStat & SSL_MAG_INT1)
  {
    WrErrMsg("PS Trip, Magnet Interlock 1");
  }

  else if (TripStat & SSL_MAG_INT2)
  {
    WrErrMsg("PS Trip, Magnet Interlock 2");
  }

  else if (TripStat & SSL_MAG_INT3)
  {
    WrErrMsg("PS Trip, Magnet Interlock 3");
  }

  else  //no fault detected, should never happen
  {
    len = sprintf(buf, "PS Trip, No Flt Detect %04X %04X",
                       xil.ser_statl_lch0 ,
                       xil.ser_statl_lch1 );
    buf [len] = 0;
    WrErrMsg(buf);
  }
}

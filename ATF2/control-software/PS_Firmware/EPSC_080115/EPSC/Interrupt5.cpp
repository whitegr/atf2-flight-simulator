//******************************************************************************
//  Interupt 5 Service Routine (Ethernet Power Supply Controller)
//  Filename: adc.cpp
//  07/11/06
//
// This module contains the Interrupt 5 service routine called by the Xilinx
// 240 times per second.
//
//******************************************************************************

#include <C:\Nburn\EPSC\include\includes.h>    //Include all headers for project

//************************** void ADCTask(void * pd) **************************

void ADCTask(void * pd)
{ pd=pd;

  while(1)
  {
    OSSemPend(&ADC_TASK_SEM, 0);

    // If power supply triped, set semaphore for power supply trip task
    if ( xil.PsOnFlag == PS_ON )              //if PS status flag is ON
    { if (xil.ps_status & STATUS_PS_OFF)      //check for power supply trip
      {
        xil.PsOnFlag = PS_OFF;                //clear power supply ON flag
        xil.PsRegFlag = REG_OFF;              //clear regulation enable flag
        xil.LastOff = (xil.intl_stat >> 8);   //set last off to interlock status
        CalErr &= ~DIG_REG_ERR;               //clear Digital Reg error flag
        OSSemPost( &START_TRIP_SEM );         //set start trip task semaphore
      }
    }

    Process_ADC1();             //Call process ADC1
    if ((ADC1_Cyc % 2) == 1)
    {
      WriteDAC();               //Call process DAC (write DAC, ramp, digital reg)
    }
    Process_ADC2();             //Call process ADC2

    ADC2_Cyc = (ADC2_Cyc + 1) % 8;
    ADC1_Cyc =  ADC2_Cyc % 4;

    static WORD IrqCnt;                //Time since last processor reset
    if (++IrqCnt > 23)                 //Divide 240 hz by 24
    {
      IrqCnt = 0;
      ++TIMER;                         //Increment 10 times per second
    }

    if (CalErr | HrdFlt)               //Flash Green or Red LED if fault
      FlashLED = (FlashLED + 1) % 240;
    else
      FlashLED = 0;

    if (xil.ps_status & STATUS_PS_OFF)        //if PS off
    { 
      if (FlashLED < 120)
        xil.leds = RED_OFF + GREEN_ON;        //Green LED ON, Red OFF
      else                                    //Flash Green LED if fault
        xil.leds = RED_OFF + GREEN_OFF;       //Green LED OFF, Red OFF
    }
    else                                      //else PS ON
    {
      if (FlashLED < 120)
        xil.leds = RED_ON;                    //Red ON
      else                                    //Flash Red LED if fault
        xil.leds = RED_OFF;                   //Red OFF

      if (xil.ps_status & STATUS_RAMP_ON)     //if ramping
        xil.leds = GREEN_ON;                  //Green LED ON
      else                                    //else
        xil.leds = GREEN_OFF;                 //Green LED OFF
    }

    if (FlashYellow > 0)            //Turn on yellow LED if FlashYellow > 0
    {
      --FlashYellow;
      xil.leds = YELLOW_ON;
    }
    else
      xil.leds = YELLOW_OFF;

    if (xil.WatchdogFlag == 0)
    {
      xil.WatchdogFlag = UDPWatchdogFlag;
      xil.leds = RESET_WATCHDOG;              //reset watchdog timer
    }
  }
}

//****************************** StartADCTask() *******************************

DWORD  ADCStk[USER_TASK_STK_SIZE] __attribute__(( aligned(16) ));

void StartADCTask()
{ OSTaskCreate(ADCTask,                         //address of task code
               (void *) 0,                      //optional argument pointer
               &ADCStk[USER_TASK_STK_SIZE],     //top of stack
               ADCStk,                          //bottom of stack
               ADC_PRIO);                       //task priority
}

//******************************** INTERRUPT 5 *********************************
// INTERRUPT(adr_label,sr) is a Netburner supplied macro
// sr=0x2n00 disables all interrupts at level n and below
// sr=0x2700 disables all interrupts
// sr=0x2000 enables all interrupts
//******************************************************************************

INTERRUPT(irq5_isr, 0x2500)     //for irq5
{
  sim.eport.epfr = 0x20;        //clear irq5 interrupt flag
  OSSemPost( &ADC_TASK_SEM );   //set semaphore to start ADC task
}

//************************* Netburner Interrupt Macro **************************
// 
// #define INTERRUPT(x,y)
// extern "C" { void real_##x();  void x(); }
// void fake_##x()
// {
// __asm__  (".global "#x);
// __asm__  (#x":");
//
// __asm__  ("move.w #0x2700,%sr ");                  Mask Interrupts
//
// __asm__  ("lea      -60(%a7),%a7 ");               PUSH registers
// __asm__  ("movem.l  %d0-%d7/%a0-%a6,(%a7) ");
//
// __asm__  ("move.w (OSISRLevel),%d0 ");             PUSH OSISRLevel (OS_ISR_Level)
// __asm__  ("move.l %d0,-(%sp) ");
//
// __asm__  ("move.b (OSIntNesting),%d0");            Increment OSIntNesting
// __asm__  ("addq.l #1,%d0");
// __asm__  ("move.b %d0,(OSIntNesting)");
//
// __asm__  ("move.w #"#y",%d0 ");                    Set sr and OSISRLevel to #y
// __asm__  ("move.w %d0,%sr ");
// __asm__  ("move.w %d0,(OSISRLevel)");
//
// __asm__  ("jsr real_"#x );                         Call main body of ISR
//
// __asm__  ("move.l (%sp)+,%d0 ");                   POP OSISRLevel
// __asm__  ("move.w %d0,(OSISRLevel)");
// 
// __asm__  ("jsr      OSIntExit  ");                 Task Switch
//
// __asm__  ("movem.l  (%a7),%d0-%d7/%a0-%a6 ");      POP registers
// __asm__  ("lea    60(%a7),%a7 ");
//
// __asm__  ("rte");                                  Return from exception
// }
// void real_##x()                                    *** End of macro ***
//
// *** user interrupt service code here ***
//
// INTERRUPT(SER0_ISR, 0x2400)         // sample application from serial.cpp
// { DoSerIsr(0);
// }
//
// Notes: Exception or interrupt pushes PC and sr on stack. Return from exception 
// (RTE) enables interrupts by restoring the previous interrupt mask in the sr.
// 
// For nested interrupts, current interrupt level saved in OS ISR level (OSISRLevel).
// 
// #define  UCOS_ENTER_CRITICAL() asm (" move.w #0x2700,%sr ");
// 
// #define  UCOS_EXIT_CRITICAL()  asm (" move.w (OSISRLevel),%d0 ");
//                                asm (" move.w %d0,%sr ");
//
// sr=0x2n00 disables all interrupts at level n and below
// sr=0x2700 disables all interrupts
// sr=0x2000 enables all interrupts
// Note: sr=2xxx sets supervisor mode flag	
//
//******************************************************************************


//******************************************************************************
//  Ramp Control (0xE4) (Ethernet Power Supply Controller)
//  Filename: cmd_E4.cpp
//  09/10/07
//
//  This command allows software control of the ramp state. Once a ramp is set
//  by the 0xC1 or 0xC2 command, the ramp can be started, stopped, or aborted.
//  The command returns the ramp state, setpoint, and time remaining.
//
//  States for xil.Ramp_State, ramp state in xilinx SRAM
//  RAMP_OFF   0x00     Ramp not configured
//  RAMP_C1    0x01     Ramp configured by C1 command
//  RAMP_C2    0x02     Ramp configured by C2 command
//  RAMP_DONE  0x04     Current ramp request has completed
//  RAMP_STOP  0x08     Current ramp request was stopped (aborted)
//
//******************************************************************************

#include <C:\Nburn\EPSC\include\includes.h>    //Include all headers for project

typedef struct          // E4 command structure
{ BYTE   Ctype;         // 00 command type 0xC3
  BYTE   Rsp;           // 01 response code
  BYTE   Tid;           // 02 Task ID
  BYTE   Cmd;           // 03 Command
} __attribute__ ((packed))  Cmd_E4;

#define CMD_LEN_E4  4   // expected length of command message

typedef struct          // E4 response structure
{ BYTE   Ctype;         // 00 command type, returned unchanged
  BYTE   Rsp;           // 01 response code
  BYTE   Tid;           // 02 Task ID, returned unchanged
  BYTE   RampState;     // 03 Ramp state
  float  CurSetPnt;     // 04-07 Current Setpoint of ramp (amps)
  DWORD  TimeRem;       // 08-11 remaining time in ramp, 0.01 sec / cnt
} __attribute__ ((packed))  Rsp_E4;

#define RSP_LEN_E4  12  // length of response messages

//********************* void Process_E4(UDPPacket * pUDP) **********************
//  pUDP  pointer to object of class UDPPacket, passed to function
//  pCmd  pointer to command structure (type Cmd_E4) set to start of UDP data
//  pRsp  pointer to response structure (type Rsp_E4) set to start of UDP data
//******************************************************************************

void Process_E4(UDPPacket * pUDP)
{
  PBYTE pUdpData = pUDP->GetDataBuffer();      //get pointer to UDP data field
  Cmd_E4 * pCmd = (Cmd_E4 *) pUdpData;         //set pointer for command data
  Rsp_E4 * pRsp = (Rsp_E4 *) pUdpData;         //set pointer for response data
  int NumSetPnt = pCmd->Num;                   //number of setpoints

  if (pUDP->GetDataSize() != CMD_LEN_E4)
  {
    pRsp->Rsp = INVALID_MSG_LENGTH;
  }
  else                                         //valid message structure
  {
    WORD RampState = xil.Ramp_State;
    if ( ( RampState == RAMP_C1 ) || ( RampState == RAMP_C2 ) )
    {
      if      (pCmd->Cmd == 0x01)
        xil.ps_cntl = PSCNTL_RAMP_ON;          //set Ramp status to ON
      else if (pCmd->Cmd == 0x02)
        xil.ps_cntl = PSCNTL_RAMP_OFF          //set Ramp status to OFF
      else if (pCmd->Cmd == 0x04)
      {
        xil.ps_cntl = PSCNTL_RAMP_OFF          //set Ramp status to OFF
                    + PSCNTL_HOLD_OFF;         //set Hold Enable to OFF
        xil.Ramp_State = RAMP_STOP;            //set Ramp State to Stop
      }
    }

    //Ramp status
    pRsp->RampState = xil.Ramp_State;          //Ramp state

    //Current Setpoint in amps
    pRsp->CurSetPnt = RevPol(KCV * (float)xil.Des_ADC * xil.db_eep.reg_iva);
    flip4((PBYTE) & pRsp->CurSetPnt);

    //remaining time in ramp, converted to 0.01 sec/cnt
    pRsp->TimeRem = Chg120To100( xil.Ramp_Times[0] );
    flip4((PBYTE) & pRsp->TimeRem);

    pRsp->Rsp = COMMAND_OK;                    //set response code
    pUDP->SetDataSize( RSP_LEN_E4 );
  }
}


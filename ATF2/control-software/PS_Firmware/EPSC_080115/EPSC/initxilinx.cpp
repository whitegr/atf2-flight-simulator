//******************************************************************************
//  Initialize Xilinx (Ethernet Power Supply Controller)
//  Filename: initxilinx.cpp
//  02/14/05
//
//  Misc fuctions to initialize xilinx data structures
//
//******************************************************************************

#include <C:\Nburn\EPSC\include\includes.h>    //Include all headers for project

void SetStr(WORD * pxil, char * strdat);       //forward declaration

void InitXilinx()
{
//******************************* Check Data Bus *******************************
// Write a single data bit (D0-D15) in 16 different registers in the Xilinx.
// Read back and check data to confirm data and address lines.
//******************************************************************************

  char buf[64];                        //character buffer for error message
  int len;                             //length of error message
  WORD data = 0x0001;
  WORD badbits = 0x0000;

  for(WORD i = 0; i < 16; ++i)
  {
    xil.Test[i] = data;
    data = data << 1;
  }

  data = 0x0001;
  for(WORD i = 0; i < 16; ++i)
  {
    WORD XilData = xil.Test[i];        // 05/22/07 save data for error message
    if ( XilData != data )
    {
      badbits |= data;
      printf("XILINX WRITE %04X, READ %04X\r\n", data, XilData );
    }
    data = data << 1;
  }

  if ( badbits > 0 )                   //Check for bad data bits
  {
    HrdFlt |= XIL_FLT;
    len = sprintf(buf, "Xilinx Read/Write Fault %04XH", badbits );
    buf [len] = 0;
    WrErrMsg(buf);
  }

//******************************** InitXilinx() ********************************
// This function checks if the Xilinx status flag has been set to 0xA55A indicating
// that the device has already been initialized. If not, the motherboard and
// daughter board EE proms are read into the Xilinx SRAM. If the data checksums
// is not valid, default values are written to the xilinx.
//******************************************************************************

  xil.LastReset = xil.ResetType;
  xil.ResetType = 0;
  if (xil.xil_status != 0xA55A)        //determine reset type
  { xil.xil_status = 0xA55A;
    WrErrMsg("POWER ON RESET");
    xil.LastReset = POWER_ON_RESET;
  }
  else if (xil.ser_stath & SSH_WATCHDOGFLAG)
  {
    WrErrMsg("WATCHDOG TIMER RESET");
    xil.LastReset = WATCHDOG_RESET;
    HrdFlt |= WDT_FLT;
  }
  else if (xil.ser_stath & SSH_LOCALRSTFLAG)
  {
    WrErrMsg("LOCAL CONTROL RESET");
    xil.LastReset = LOCAL_RESET;
  }
  else if (xil.LastReset == SOFTWARE_RESET)
    WrErrMsg("SOFTWARE RESET");
  else if (xil.LastReset == UART_RESET)
    WrErrMsg("DIAGNOSTIC PORT (UART) RESET");
  else     // no indication of source, probably Netburner code download
  {
    WrErrMsg("NETBURNER RESET");
    xil.LastReset = NETBURNER_RESET;
  }

  if (RdEEPROM( MB_EEP, EEP_LOW) != 1)
  { SetDefMbEEP();
    HrdFlt |= MB_EEP_FLT;
    WrErrMsg("MOTHER BOARD EEPROM READ ERROR");
  }

  if (RdEEPROM( DB_EEP, EEP_LOW) != 1)
  { SetDefDbEEP();
    HrdFlt |= DB_EEP_FLT;
    WrErrMsg("DAUGHTER BOARD EEPROM READ ERROR");
  }

  //get reference voltage from xilinx and convert to ADC counts for self calibration
  AdcRef = (long) (xil.mb_eep.ref * (float)KADC);
}

//********************************* SetMbEEP() *********************************
// This function is used to fill Xilinx mother board EE prom data area in static
// RAM when the EE prom data checksum is not valid.
//******************************************************************************

void SetDefMbEEP()
{ WORD * pXil = (WORD *) & xil.mb_eep;           //clear memory
  for (WORD i=0; i < 32; ++i) pXil[i] = 0;
  
  SetStr((WORD *) &xil.mb_eep.SerialNum[0], "**VOID**");
  SetStr((WORD *) &xil.mb_eep.CalDate[0],   "**VOID**");
  xil.mb_eep.ref = 6.8;
  xil.mb_eep.a1k1 = 0;
  xil.mb_eep.a1k2 = 0;
  xil.mb_eep.a2k1 = 0;
  xil.mb_eep.a2k2 = 0;
}

//********************************* SetDbEEP() *********************************
// This function is used to fill Xilinx daughter board EE prom data area in static
// RAM when the EE prom data checksum is not valid.
//******************************************************************************

void SetDefDbEEP()
{ WORD * pXil = (WORD *) & xil.db_eep;           //clear memory
  for (WORD i=0; i < 32; ++i) pXil[i] = 0;

  xil.db_eep.ip_adr  = (DWORD) AsciiToIp(DEFAULT_IP_ADDRESS);
  xil.db_eep.ip_mask = (DWORD) AsciiToIp(DEFAULT_IP_MASK);
  xil.db_eep.ip_gate = (DWORD) AsciiToIp(DEFAULT_IP_GATE);
  xil.db_eep.ip_dns  = (DWORD) AsciiToIp(DEFAULT_IP_DNS);
  xil.db_eep.reg_iva = 1;
  xil.db_eep.aux_iva = 1;
  xil.db_eep.gnd_iva = 1;
  xil.db_eep.psv_iva = 1;
  xil.db_eep.Config  = 0;
  xil.db_eep.EnetConfig = 0;
  SetStr((WORD *) &xil.db_eep.MagnetId[0], "**VOID**");
  xil.db_eep.DigitalGain = FloatToShortFloat(1.0);
  xil.db_eep.DigitalTC = FloatToShortFloat(1.0);
  xil.db_eep.DigitalErrLim = FloatToShortFloat(1.0);
}

//********************************** SetStr() **********************************
// This function is used to copy 8 byte strings into the Xilinx static ram. The
// data must be written as words (16 bit).
//******************************************************************************

void SetStr(WORD * pxil, char * strdat)
{ WORD i;
  for (i=0; i < 4; ++i)
    pxil[i] = 256*((WORD) strdat[2 * i])+(WORD) strdat[2 * i + 1];
}


//******************************************************************************
//  CONFIGURATION SUMMARY COMMAND (0xCB) (Ethernet Power Supply Controller)
//  Filename: cmd_CE.cpp
//  01/19/06
//  04/12/06 Added Ethernet MAC address to end of data
//  This message returns static chassis information. This message consolidates
//  information from the CB, and CC commands, and adds xilinx version,
//  digital regulation, IP configuration and ADC linearity coefficients.
//******************************************************************************

#include <C:\Nburn\EPSC\include\includes.h>    //Include all headers for project

typedef struct          // CE command structure
{ BYTE   Ctype;         // 00 command type 0xCE
  BYTE   Rsp;           // 01 response code
  BYTE   Tid;           // 02 Task ID
  BYTE   Chan;          // 03 Channel number (must be zero)
} __attribute__ ((packed))  Cmd_CE;

#define CMD_LEN_CE  4   // expected length of command message

typedef struct          // CE response structure
{ BYTE   Ctype;         // 00 command type, returned unchanged
  BYTE   Rsp;           // 01 response code
  BYTE   Tid;           // 02 Task ID, returned unchanged
  BYTE   Chan;          // 03 Channel, returned unchanged
  //daughterboard EEPROM data
  WORD   MagnetId[4];   // 04-11 Magnet ID 8 byte string
  DWORD  ip_adr;        // 12-15 Ethernet IP address
  DWORD  ip_mask;       // 16-19 Ethernet IP mask
  DWORD  ip_gate;       // 20-23 Ethernet IP gateway
  DWORD  ip_dns;        // 24-27 Ethernet IP Domain Name Server
  WORD   EnetConfig;    // 28-29 Ethernet configuration
  WORD   Config;        // 30-31 Chassis Configuration
  float  RegIva;        // 32-35 Regulated transductor amp per ADC volt
  float  AuxIva;        // 36-39 Auxiliary transductor amp per ADC volt
  float  GndIva;        // 40-43 Ground current amp per ADC volt
  float  PsvIva;        // 44-47 PS output volt per ADC volt
  float  DigitalGain;   // 48-51 Digital Regulation Gain
  float  DigitalTC;     // 52-55 Digital Reg time constant
  float  DigitalErrLim; // 56-59 Digital Reg error voltage limit
  //motherboard EEPROM data
  WORD   SerialNum[4];  // 60-67 Serial number 8 byte string
  char   Version[8];    // 68-75 Firmware date 8 byte string
  WORD   XilinxVersion; // 76-77 Xilinx version
  float  RefVlt;        // 78-81 Reference voltage
  WORD   CalDate[4];    // 82-89 Calibration date ASCII string (8 char)
  float  a1k1;          // 90-93 ADC1 linearity coefficient K1
  float  a1k2;          // 94-97 ADC1 linearity coefficient K2
  float  a2k1;          // 98-101 ADC2 linearity coefficient K1
  float  a2k2;          // 102-105 ADC2 linearity coefficient K2
  //Netburner Ethernet Address
  WORD   EnetAddr[3];   // 106-111 Ethernet address (48 bit)
} __attribute__ ((packed))  Rsp_CE;

#define RSP_LEN_CE  112 // length of response message

//********************* void Process_CE(UDPPacket * pUDP) **********************
//  pUDP  pointer to object of class UDPPacket, passed to function
//  pCmd  pointer to command structure (type Cmd_CE) set to start of UDP data
//  pRsp  pointer to response structure (type Rsp_CE) set to start of UDP data
//******************************************************************************

void Process_CE(UDPPacket * pUDP)
{
  PBYTE pUdpData = pUDP->GetDataBuffer();      //get pointer to UDP data field
  Cmd_CE * pCmd = (Cmd_CE *) pUdpData;         //set pointer for command data
  Rsp_CE * pRsp = (Rsp_CE *) pUdpData;         //set pointer for response data

  if (pUDP->GetDataSize() != CMD_LEN_CE)       //check for valid command length
  { pRsp->Rsp = INVALID_MSG_LENGTH;
  }
  else if (pCmd->Chan !=0)                     //check for valid channel (0)
  { pRsp->Rsp = INVALID_CHAN_NUM;
  }
  else
  {
    //Daughterboard EEPROM configuration data
    short i;
    for (i = 0; i < 4; ++i)                    //Magnet ID 8 byte string
      pRsp->MagnetId[i] = xil.db_eep.MagnetId[i];

    pRsp->ip_adr = xil.db_eep.ip_adr;          //IP address
    flip4((PBYTE) & pRsp->ip_adr);             //convert to little endian (Intel)

    pRsp->ip_mask = xil.db_eep.ip_mask;        //IP mask
    flip4((PBYTE) & pRsp->ip_mask);            //convert to little endian (Intel)

    pRsp->ip_gate = xil.db_eep.ip_gate;        //IP gateway
    flip4((PBYTE) & pRsp->ip_gate);            //convert to little endian (Intel)

    pRsp->ip_dns = xil.db_eep.ip_dns;          //IP domain name server (DNS)
    flip4((PBYTE) & pRsp->ip_dns);             //convert to little endian (Intel)

    pRsp->EnetConfig = xil.db_eep.EnetConfig;  //Ethernet Configuration
    flip2((PBYTE) & pRsp->EnetConfig);         //convert to little endian (Intel)

    pRsp->Config = xil.db_eep.Config;          //Chassis Configuration
    flip2((PBYTE) & pRsp->Config);             //convert to little endian (Intel)

    pRsp->RegIva = xil.db_eep.reg_iva;         //Reg transductor amp per ADC volt
    flip4((PBYTE) & pRsp->RegIva);             //convert to little endian (Intel)

    pRsp->AuxIva = xil.db_eep.aux_iva;         //Aux transductor amp per ADC volt
    flip4((PBYTE) & pRsp->AuxIva);             //convert to little endian (Intel)

    pRsp->GndIva = xil.db_eep.gnd_iva;         //Ground current amp per ADC volt
    flip4((PBYTE) & pRsp->GndIva);             //convert to little endian (Intel)

    pRsp->PsvIva = xil.db_eep.psv_iva;         //PS output volt per ADC volt
    flip4((PBYTE) & pRsp->PsvIva);             //convert to little endian (Intel)

    //convert digital regulation coefficients from short float (16 bit) to float
    pRsp->DigitalGain = ShortFloatToFloat(xil.db_eep.DigitalGain);
    flip4((PBYTE) & pRsp->DigitalGain);        //convert to little endian (Intel)

    pRsp->DigitalTC = ShortFloatToFloat(xil.db_eep.DigitalTC);
    flip4((PBYTE) & pRsp->DigitalTC);          //convert to little endian (Intel)

    pRsp->DigitalErrLim = ShortFloatToFloat(xil.db_eep.DigitalErrLim);
    flip4((PBYTE) & pRsp->DigitalErrLim);      //convert to little endian (Intel)

    //Motherboard EEPROM configuration data

    for (i = 0; i < 4; ++i)                    //Serial Number (8 byte) string
      pRsp->SerialNum[i] = xil.mb_eep.SerialNum[i];

    for (i = 0; i < 8; ++i)                    //Firmware Date 8 byte string
      pRsp->Version[i] = *(pFirmwareDate + i);

    pRsp->XilinxVersion = xil.ver_num;         //Xilinx Version
    flip2((PBYTE) & pRsp->XilinxVersion);      //convert to little endian (Intel)

    pRsp->RefVlt = xil.mb_eep.ref;             //Reference voltage
    flip4((PBYTE) & pRsp->RefVlt);             //convert to little endian (Intel)

    for (i = 0; i < 4; ++i)                   //Calibration date (8 byte) string
      pRsp->CalDate[i] = xil.mb_eep.CalDate[i];

    //convert ADC linearity coefficients from ADC counts to volts
    float atov = 1.0 / (float)KADC;            //conversion, ADC counts to volts

    pRsp->a1k1 = (float)xil.mb_eep.a1k1 * atov;  //ADC1 linearity coefficient K1
    flip4((PBYTE) & pRsp->a1k1);               //convert to little endian (Intel)

    pRsp->a1k2 = (float)xil.mb_eep.a1k2 * atov;  //ADC1 linearity coefficient K2
    flip4((PBYTE) & pRsp->a1k2);               //convert to little endian (Intel)

    pRsp->a2k1 = (float)xil.mb_eep.a2k1 * atov;  //ADC2 linearity coefficient K1
    flip4((PBYTE) & pRsp->a2k1);               //convert to little endian (Intel)

    pRsp->a2k2 = (float)xil.mb_eep.a2k2 * atov;  //ADC2 linearity coefficient K2
    flip4((PBYTE) & pRsp->a2k2);               //convert to little endian (Intel)

    //Read Ethernet MAC address (48 bit)
    //08/01/2007 Corrected word order in ethernet address, reversed 0 and 2

    pRsp->EnetAddr[2] = sim.fec.palr / 0x10000;    //most significant 16 bits
    pRsp->EnetAddr[1] = sim.fec.palr % 0x10000;    //middle 16 bits
    pRsp->EnetAddr[0] = sim.fec.paur / 0x10000;    //least significant 16 bits
    flip2((PBYTE) & pRsp->EnetAddr[0]);        //convert to little endian (Intel)
    flip2((PBYTE) & pRsp->EnetAddr[1]);        //convert to little endian (Intel)
    flip2((PBYTE) & pRsp->EnetAddr[2]);        //convert to little endian (Intel)

    pRsp->Rsp = COMMAND_OK;                    //set response code
    pUDP->SetDataSize(RSP_LEN_CE);             //set data size for response packet
  }
}


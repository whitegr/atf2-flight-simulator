//******************************************************************************
//  DIAGANOSTIC MESSAGE 1 COMMAND (0xCC) (Ethernet Power Supply Controller)
//  Filename: cmd_CC.cpp
//  03/03/05
//  This message returns diagnostic information.
//******************************************************************************

#include <C:\Nburn\EPSC\include\includes.h>    //Include all headers for project

typedef struct          // CC command structure
{ BYTE   Ctype;         // 00 command type 0xCC
  BYTE   Rsp;           // 01 response code
  BYTE   Tid;           // 02 Task ID
  BYTE   Chan;          // 03 Channel number (must be zero)
} __attribute__ ((packed))  Cmd_CC;

#define CMD_LEN_CC  4   // expected length of command message

typedef struct          // CC response structure
{ BYTE   Ctype;         // 00 command type, returned unchanged
  BYTE   Rsp;           // 01 response code
  BYTE   Tid;           // 02 Task ID, returned unchanged
  BYTE   Chan;          // 03 Channel, returned unchanged
  float  RegIva;        // 04-07 Regulated transductor amp per ADC volt
  float  AuxIva;        // 08-11 Auxiliary transductor amp per ADC volt
  float  GndIva;        // 12-15 Ground current amp per ADC volt
  float  PsvIva;        // 16-19 PS output volt per ADC volt
  float  RefVlt;        // 20-23 Reference voltage
  WORD   CalDate[4];    // 24-31 Calibration date ASCII string (8 char)
} __attribute__ ((packed))  Rsp_CC;

#define RSP_LEN_CC  32  // length of response message

//********************* void Process_CC(UDPPacket * pUDP) **********************
//  pUDP  pointer to object of class UDPPacket, passed to function
//  pCmd  pointer to command structure (type Cmd_CC) set to start of UDP data
//  pRsp  pointer to response structure (type Rsp_CC) set to start of UDP data
//******************************************************************************

void Process_CC(UDPPacket * pUDP)
{
  PBYTE pUdpData = pUDP->GetDataBuffer();      //get pointer to UDP data field
  Cmd_CC * pCmd = (Cmd_CC *) pUdpData;         //set pointer for command data
  Rsp_CC * pRsp = (Rsp_CC *) pUdpData;         //set pointer for response data

  if (pUDP->GetDataSize() != CMD_LEN_CC)       //check for valid command length
  { pRsp->Rsp = INVALID_MSG_LENGTH;
  }
  else if (pCmd->Chan !=0)                     //check for valid channel (0)
  { pRsp->Rsp = INVALID_CHAN_NUM;
  }
  else
  { pRsp->RegIva = xil.db_eep.reg_iva;         //Reg transductor amp per ADC volt
    pRsp->AuxIva = xil.db_eep.aux_iva;         //Aux transductor amp per ADC volt
    pRsp->GndIva = xil.db_eep.gnd_iva;         //Ground current amp per ADC volt
    pRsp->PsvIva = xil.db_eep.psv_iva;         //PS output volt per ADC volt
    pRsp->RefVlt = xil.mb_eep.ref;             //Reference voltage

    //convert data to little endian (Intel) by reversing byte order
    flip4((PBYTE) & pRsp->RegIva);
    flip4((PBYTE) & pRsp->AuxIva);
    flip4((PBYTE) & pRsp->GndIva);
    flip4((PBYTE) & pRsp->PsvIva);
    flip4((PBYTE) & pRsp->RefVlt);

    for (short i = 0; i < 4; ++i)              //Calibration date (8 byte) string
      pRsp->CalDate[i] = xil.mb_eep.CalDate[i];
    pRsp->Rsp = COMMAND_OK;                    //set response code
    pUDP->SetDataSize(RSP_LEN_CC);             //set response packet data size
  }
}


//******************************************************************************
//  COMMUNICATION CHECK (0xEF) (Ethernet Power Supply Controller)
//  Filename: cmd_EF.cpp
//  10/12/05
//  This is a test message. It returns the number of times the mesaage has been
//  sent. If the received TID is zero, the count is reset. Additional data can
//  be added after the header and will be returned.
//******************************************************************************

#include <C:\Nburn\EPSC\include\includes.h>    //Include all headers for project

typedef struct          // EF command structure
{ BYTE   Ctype;         // 00 command type 0xE1
  BYTE   Rsp;           // 01 response code
  BYTE   Tid;           // 02 Task ID
  DWORD  Tcnt;          // 03 - 07 Transmit packet count
} __attribute__ ((packed))  Cmd_EF;

#define CMD_LEN_EF  7   // expected length of command message

typedef struct          // EF response structure
{ BYTE   Ctype;         // 00 command type, returned unchanged
  BYTE   Rsp;           // 01 response code
  BYTE   Tid;           // 02 Task ID, returned unchanged
  DWORD  Tcnt;          // 03 - 07 Transmit packet count
  DWORD  Rcnt;          // 08 - 11 Received count
} __attribute__ ((packed))  Rsp_EF;

#define RSP_LEN_EF  11   // length of response message

//********************* void Process_E1(UDPPacket * pUDP) **********************
//  pUDP  pointer to object of class UDPPacket, passed to function
//  pCmd  pointer to command structure (type Cmd_EF) set to start of UDP data
//  pRsp  pointer to response structure (type Rsp_EF) set to start of UDP data
//******************************************************************************

void Process_EF(UDPPacket * pUDP)
{
  static DWORD Rcnt = 0;

  PBYTE pUdpData = pUDP->GetDataBuffer();      //get pointer to UDP data field
  Rsp_EF * pRsp = (Rsp_EF *) pUdpData;         //set pointer for response data

  WORD Len = pUDP->GetDataSize();
  if (Len < CMD_LEN_EF)                        //check for valid command length
  { pRsp->Rsp = INVALID_MSG_LENGTH;
  }
  else
  { pRsp->Rsp = COMMAND_OK;                    //set response code
    if (pRsp->Tcnt == 0)
      Rcnt = 0;
    else
      ++Rcnt;
    pRsp->Rcnt = Rcnt;                         //set receive count
    flip4((PBYTE) & pRsp->Rcnt);               //convert to Intel format
    if( Len < RSP_LEN_EF ) Len = RSP_LEN_EF;
    pUDP->SetDataSize( Len );                  //set response packet data size
  }
}


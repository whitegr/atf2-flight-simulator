//******************************************************************************
//  EEPROM interface functions (Ethernet Power Supply Controller)
//  Filename: eeprom.cpp
//  03/07/05
//
//  The EPSC uses two 93C46B EEPROMs. These are 64x16 bit serial devices.
//  There is one unit in the motherboard that is used to store the serial number
//  and calibration data. The second unit is in the daughter board and is used
//  to store all data configuration data for a magnet. This includes the IP
//  address, scaling factors (volts to amps), magnet name, and all other data
//  specific to the power supply.
//
//  There are 4 registers in the xilinx gate array for reading and writing data
//  to the EEPROMs. There are 16 bit registers for reading and for writing data
//  to the device. There is a 16 bit register to send instructions and a
//  register to read the status of the current instruction.
//
//  xil.eep_rddat  read only register for data read from the EEPROM (16 bit)
//  xil.eep_wrdat  write only register for data to be written to EEPROM (16 bit)
//  xil.eep_cnd    write only register for EEPROM commands
//                 D7-D0 EEPROM instruction
//                 D8    Set to enable transmisssion of instruction (D7-D0)
//                 D9    *READ/WRITE bit
//                 D10   command only (8 bit), low for command and data (24 bit)
//                 D12   *MB/DB, Chip select for mother (MB) or daughter (DB)
//                       (D11,13,14,15 not used)
//  xil.eep_stat   read only register for EEPROM interface status
//                 D0    EEPROM Busy, stays high during instr and write
//                 D1    TIMEOUT, aborted write after 12 mS
//                 D4    Chip select for mother board (CS0)
//                 D5    Chip select for daughter board (CS1)
//                 D6    EEPROM data out (low during write to signal BUSY)
//
//  **** Read data example ****
//  xil.eep_cmd = EEP_CMD_RD + EEP_CS_MB + adr;   send read 'adr' instruction
//  while (xil.eep_stat & EEP_BUSY);              wait for completion
//  data = xil.eep_rd_dat;                        read data from xilinx
//
//  **** Write data example ****
//  xil.eep_cmd = EEP_CMD_WREN + EEP_CS_MB;       send write enable
//  while(xil.eep_stat & EEP_BUSY) {delay;}       wait for completion of send
//  xil.eep_wrdat = data;                         write data to xilinx
//  xil.eep_cmd = EEP_CMD_WR + EEP_CS_MB + adr;   send write 'adr' instruction
//  while(xil.eep_stat & EEP_BUSY) {delay;}       wait for completion of write
//  if (xil.eep_stat & EEP_TIMEOUT) {abort;}      abort if write timeout
//  xil.eep_cmd = EEP_CMD_WRDS + EEP_CS_MB;       send write disable
//
//******************************************************************************

#include <C:/Nburn/EPSC/include/includes.h> 

#define EEP_CMD_RD     (0x0180)   //Read command (add address and chip select)
#define EEP_CMD_WR     (0x0340)   //Write command (add address and chip select)
#define EEP_CMD_WREN   (0x0530)   //Write enable command
#define EEP_CMD_WRDS   (0x0500)   //Write disable command

#define EEP_CS_MB      (0x0000)   //Mother board chip select, add to command
#define EEP_CS_DB      (0x1000)   //Daughter board chip select, add to command

#define EEP_BUSY       (0x0001)   //Status register bit mask, BUSY
#define EEP_TIMEOUT    (0x0002)   //Status register bit mask, TIMEOUT

//********************** WORD RdEEPROM(WORD cs ,WORD hl) ***********************
//  Read 32 words from the EEPROM referenced by cs into Xilinx memory image
//
//  cs selects the motherboard (cs == 0) or daughter board EEPROM
//  hl selects the lower 32 words (hl == 0) or the upper 32 words
//  '1' is returned if the 32 word checksum is 0x5AA5, else '0' is returned
//******************************************************************************

WORD RdEEPROM( WORD cs, WORD hl)
{ WORD * pDes;
  WORD CheckSum = 0;
  WORD DlyCnt;

  if (cs == 0)
  { pDes = (WORD *) & xil.mb_eep;
    cs = EEP_CS_MB;
  }
  else
  { pDes = (WORD *) & xil.db_eep;
    cs = EEP_CS_DB;
  }

  if (hl != 0) hl = 32;                          //if hl not zero, set to 32

  for (WORD i=0; i < 32; ++i)
  {
    xil.eep_cmd = EEP_CMD_RD + cs + hl + i;      //send read command to EEPROM

    //delay while EEPROM status shows busy, or loop count > 25 (hardware fault)
    DlyCnt = 0;
    do
    { for ( WORD Dly=0; Dly < 10; ++Dly);        //delay loop
      if (++DlyCnt > 25) return 0;               //increment delay loop count
    }
    while (xil.eep_stat & EEP_BUSY);

    pDes[i] = xil.eep_rddat;                     //move data to destination address
    CheckSum += xil.eep_rddat;                   //add data to checksum
  }
  if (CheckSum == 0x5AA5) return 1;
  else return 0;
}

//********************** WORD WrEEPROM(WORD cs ,WORD hl) ***********************
//  Writes 32 words to the EEPROM referenced by cs from Xilinx memory image
//
//  cs selects the motherboard (cs == 0) or daughter board EEPROM
//  hl selects the lower 32 words (hl == 0) or the upper 32 words
//  '0' is returned if the function times out, '1' otherwise
//******************************************************************************

WORD WrEEPROM(WORD cs, WORD hl)
{ WORD * pSrc;
  WORD CheckSum = 0;                             //data checksum
  WORD DlyCnt = 0;                               //delay loop counter

  //select mother board EEPROM if cs == 0, else select daughter board EEPROM
  if (cs == 0)
  { pSrc = (WORD *) & xil.mb_eep;
    cs = EEP_CS_MB;
  }
  else
  { pSrc = (WORD *) & xil.db_eep;
    cs = EEP_CS_DB;
  }

  //select lower 32 words of EEPROM if hl == 0, else select upper 32 words
  if (hl != 0) hl = 32;

  //send write enable command to EEPROM
  xil.eep_cmd = EEP_CMD_WREN + cs;               //send write enable
  do
  { for (WORD dly=0; dly < 10; ++dly);           //delay
    if (++DlyCnt > 25) return 0;
  }
  while (xil.eep_stat & EEP_BUSY);

  //write 31 words of data pointed at by pSrc to EEPROM
  for (WORD i = 0; i < 32; ++i)
  {
    if (i < 31)
    { CheckSum += pSrc[i];                       //add data to checksum
      xil.eep_wrdat = pSrc[i];                   //move data to write register
    }
    else
      xil.eep_wrdat = 0x5AA5 - CheckSum;         //last word, set checksum = 0x5AA5

    xil.eep_cmd = EEP_CMD_WR + cs + hl + i;      //send write command to EEPROM

    //Wait for EEPROM to finish writing data word (1 to 10 milliseconds)
    DlyCnt = 0;
    do
    { for (WORD dly=0; dly < 250; ++dly);          //delay
      if (++DlyCnt > 500) return 0;
    }
    while (xil.eep_stat & EEP_BUSY);
    
    if (xil.eep_stat & EEP_TIMEOUT) return 0;     //exit if hardware timeout
  }

  xil.eep_cmd = EEP_CMD_WRDS + EEP_CS_DB;        //send write disable
  DlyCnt = 0;
  do
  { for (WORD dly=0; dly < 10; ++dly);           //delay
    if (++DlyCnt > 25) return 0;
  }
  while (xil.eep_stat & EEP_BUSY);
  
  return 1;
}


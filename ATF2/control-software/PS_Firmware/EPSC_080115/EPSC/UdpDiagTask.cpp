//******************************************************************************
//  UDP Diagnostic task (Ethernet Power Supply Controller)
//  Filename: UdpDiagTask.cpp
//  05/26/05
//
// This module initializes a UDP interface for a text based diagnostic task
//
//******************************************************************************

#include <C:\Nburn\EPSC\include\includes.h>    //Include all headers for project

void udTest(UDPPacket * pUDP);  //temporary forward declaration

//***************************** StartUdpDiagTask() *****************************

void UdpDiagMain(void * pd);  //foward declaration

DWORD  UdpDiagStk[USER_TASK_STK_SIZE] __attribute__(( aligned(16) ));

void StartUdpDiagTask()
{ OSTaskCreate(UdpDiagMain,                       //address of task code
               (void *) 0,                        //optional argument pointer
               &UdpDiagStk[USER_TASK_STK_SIZE],   //top of stack
               UdpDiagStk,                        //bottom of stack
               UDP_DIAG_PRIO);                    //task priority
}

//******************************************************************************
//
// Receive and Process Ethernet UDP Packets
//
//******************************************************************************

void UdpDiagMain(void * pd)
{ pd=pd;

  OSFifoInit(&UdpDiagFifo);
  RegisterUDPFifo(UDP_DIAG_PORT, &UdpDiagFifo);
//  printf("UDP Diagnostic task started at priority %2d, listening on port: %d\r\n",
//          UDP_DIAG_PRIO, UDP_DIAG_PORT);

  while (1)
  { UDPPacket upkt(&UdpDiagFifo, 0 * TICKS_PER_SECOND);
    if (upkt.Validate())
    {
//      printf("Recieved from IP address: ");
//      ShowIP(upkt.GetSourceAddress());
//      printf(" port %d\n", upkt.GetSourcePort());
//      PoolPtr pMsg = upkt.GetPoolPtr();
//      puts("Command Packet");
//      ShowData(pMsg->pData, 64);

      PBYTE pCode = upkt.GetDataBuffer();            //Get pointer to command code
      char c = (char) toupper((int) *pCode);

      if      (c == 'A') udShowADC2Dat1(&upkt);
      else if (c == 'B') udShowADC2Dat2(&upkt);
      else if (c == 'Z') udTest(&upkt);
      else               udShowHelp(&upkt);
    }
  }
}

//******************* void udSendResponse(UDPPacket * pUDP) ********************
//  This function sends a response UDP packet using the memory pool buffer from
//  the source packet. The packet is sent to the source IP address and port.
//******************************************************************************

void udSendResponse(UDPPacket * pUDP)
{ IPADDR IpDestAdr = pUDP->GetSourceAddress();      //save source IP address
  pUDP->SetDestinationPort(pUDP->GetSourcePort());  //set dest port = rcv src port

  //Temp Windows Klugeto reply to fixed port number
  //pUDP->SetDestinationPort(UDP_DIAG_PORT);

  pUDP->SetSourcePort(UDP_DIAG_PORT);               //set source port
  pUDP->Send(IpDestAdr);                            //send and release buffer
}

//********* void udSendString(UDPPacket * pUDP, char * data, WORD len) *********
//  This function sends a string (data) of length (len) to the IP address and
//  port of the UDP packet (pUDP).
//******************************************************************************

void udSendString(UDPPacket * pUDP, char * data, WORD len)
{
  UDPPacket upkt;
  if (upkt.GetDataBuffer() != NULL)
  {
    upkt.AddData( (PBYTE) data, len );               //add data to packet
    //upkt.SetDestinationPort(pUDP->GetSourcePort());  //set dest port = rcv src port
    upkt.SetDestinationPort(UDP_DIAG_PORT);         //** Temp Windows Kluge **
    //windows does not send from the same port it receives from
    upkt.SetSourcePort(UDP_DIAG_PORT);             //set source port
    upkt.Send(pUDP->GetSourceAddress());           //send and release buffer
    //UDPPacket destructor releases memory
  }
}

//******** int udGetString(UDPPacket * pUDP, char * data, WORD MaxLen) *********
//  This function gets a string (data) from the IP address in pUDP, places it the
//  the buffer passed to the function, and returns the length of the string.
//  If the call times out or a packet is received from a different IP address,
//  -1 is returned.
//******************************************************************************

int udGetString(UDPPacket * pUDP, char * data, WORD MaxLen)
{
  int len = -1;
  UDPPacket upkt(&UdpDiagFifo, 10 * TICKS_PER_SECOND);
  if (upkt.Validate())
  {
    if (pUDP->GetSourceAddress() != upkt.GetSourceAddress()) return(len);
    if (pUDP->GetSourcePort() != upkt.GetSourcePort()) return(len);
    len = upkt.GetDataSize();
    if (len > MaxLen - 1) len = MaxLen - 1;
    char * pChar = (char*) upkt.GetDataBuffer();
    for (WORD i = 0; i < len; ++i) data[i] = *(pChar + i);
    data[len] = 0;
  }
  return(len);
  //UDPPacket destructor releases memory
}

//********************* void udShowHelp(UDPPacket * pUDP) **********************
//  This function sends a response UDP packet. The packet is returned to the
//  source IP address and port.
//******************************************************************************

void udShowHelp(UDPPacket * pUDP)
{
  pUDP->ResetData();
               //xxxxxxxxx1xxxxxxxxx2xxxxxxxxx3
  pUDP->AddData("Help Menu, UDP Diagnostic Task Commands\r\n");
  pUDP->AddData("A undefined      N undefined\r\n");
  pUDP->AddData("B undefined      O undefined\r\n");
  pUDP->AddData("C undefined      P undefined\r\n");
  pUDP->AddData("D undefined      Q undefined\r\n");
  pUDP->AddData("E undefined      R undefined\r\n");
  pUDP->AddData("F undefined      S undefined\r\n");
  pUDP->AddData("G undefined      T undefined\r\n");
  pUDP->AddData("H undefined      U undefined\r\n");
  pUDP->AddData("I undefined      V undefined\r\n");
  pUDP->AddData("J undefined      W undefined\r\n");
  pUDP->AddData("K undefined      X undefined\r\n");
  pUDP->AddData("L undefined      Y undefined\r\n");
  pUDP->AddData("M undefined      Z undefined\r\n>");
  udSendResponse(pUDP);
}

//******************* void udShowADC2Dat1(UDPPacket * pUDP) ********************
//  Write ADC2 data to UDP Diagnostic port
//
//  DAC voltage,       chan 2
//  reg transductor,   chan 3
//  aux transductor,   chan 4
//  abs ripple,        chan 5
//  PS volts,          chan 8
//  gnd current,       chan 9
//  abs gnd current,   chan 10
//  external input,    chan 11
//  klixon 0 cur,      chan 13
//  klixon 1 cur,      chan 14
//******************************************************************************

void udShowADC2Dat1(UDPPacket * pUDP)
{
  char buf[64];
  WORD len;
  float Vlt, Amp, Cond;
  float KCV = 1 / (float) KADC;      //conversion factor, ADC counts to volts

  pUDP->ResetData();

  Vlt = (float) ADC2_Data_Reg[MUX2_REG] * KCV;
  Amp = Vlt * xil.db_eep.reg_iva;
  len = sprintf(buf, "Reg Transductor    %8.5f   %10.5f amp\r\n", Vlt, Amp);
  pUDP->AddData((PBYTE) buf, len);

  Vlt = (float) ADC2_Data_Reg[MUX2_AUX] * KCV;
  Amp = Vlt * xil.db_eep.aux_iva;
  len = sprintf(buf, "Aux Transductor    %8.5f   %10.5f amp\r\n", Vlt, Amp);
  pUDP->AddData((PBYTE) buf, len);

  Vlt = (float) ADC2_Data_Reg[MUX2_DAC] * KCV;
  Amp = Vlt * xil.db_eep.reg_iva;
  len = sprintf(buf, "DAC                %8.5f   %10.5f amp\r\n", Vlt, Amp);
  pUDP->AddData((PBYTE) buf, len);

  Vlt = (float) ADC2_Data_Reg[MUX2_ARIP] * KCV;
  Amp = Vlt * xil.db_eep.reg_iva * 0.01;
  len = sprintf(buf, "Abs Ripple Cur     %8.5f   %10.5f amp\r\n", Vlt, Amp);
  pUDP->AddData((PBYTE) buf, len);

  Vlt = (float) ADC2_Data_Reg[MUX2_GND] * KCV;
  Amp = Vlt * xil.db_eep.gnd_iva;
  len = sprintf(buf, "Ground Current     %8.5f   %10.5f amp\r\n", Vlt, Amp);
  pUDP->AddData((PBYTE) buf, len);

  Vlt = (float) ADC2_Data_Reg[MUX2_AGND] * KCV;
  Amp = Vlt * xil.db_eep.gnd_iva;
  len = sprintf(buf, "Abs Ground Cur     %8.5f   %10.5f amp\r\n", Vlt, Amp);
  pUDP->AddData((PBYTE) buf, len);

  Vlt = (float) ADC2_Data_Reg[MUX2_PSV] * KCV;
  Amp = Vlt * xil.db_eep.psv_iva;
  len = sprintf(buf, "Pwr Supply Volts   %8.5f   %10.5f vlt\r\n", Vlt, Amp);
  pUDP->AddData((PBYTE) buf, len);

  Vlt = (float) ADC2_Data_Reg[MUX2_EXT] * KCV;
  len = sprintf(buf, "External Input     %8.5f\r\n", Vlt);
  pUDP->AddData((PBYTE) buf, len);

  Vlt = (float) ADC2_Data_Reg[MUX2_KLIX0] * KCV;
  Cond = KlixCond(Vlt);
  len = sprintf(buf, "Klixon 0           %8.5f  %7.1f micro mho\r\n", Vlt, Cond);
  pUDP->AddData((PBYTE) buf, len);

  Vlt = (float) ADC2_Data_Reg[MUX2_KLIX1] * KCV;
  Cond = KlixCond(Vlt);
  len = sprintf(buf, "Klixon 1           %8.5f  %7.1f micro mho\r\n>", Vlt, Cond);
  pUDP->AddData((PBYTE) buf, len);

  udSendResponse(pUDP);
}

//******************* void udShowADC2Dat2(UDPPacket * pUDP) ********************
//  Write ADC2 internal data to UDP Diagnostic port
//
//  analog 10V ref,    chan 6
//  analog 5V,         chan 7
//  unreg +15V,        chan 16
//  reg +15V,          chan 17
//  unreg -15V,        chan 18
//  reg -15V,          chan 19
//  digital 5.0V,      chan 20
//  digital 3.3V,      chan 21
//  digital 2.5v,      chan 22
//  digital 1.2V,      chan 23
//  temperature,       chan 12
//  fan speed
//******************************************************************************

void udShowADC2Dat2(UDPPacket * pUDP)
{
  char buf[64];
  WORD len;
  float Vlt, T;
  float KCV = 1 / (float) KADC;      //conversion factor, ADC counts to volts

  pUDP->ResetData();

  Vlt = (float) ADC2_Data_Reg[MUX2_A5V] * KCV;
  len = sprintf(buf, "5.0V analog       %7.3f\r\n", Vlt);
  pUDP->AddData((PBYTE) buf, len);

  Vlt = (float) ADC2_Data_Reg[MUX2_A10V] * KCV;
  len = sprintf(buf, "10.0V reference   %7.3f\r\n", Vlt);
  pUDP->AddData((PBYTE) buf, len);

  Vlt = (float) ADC2_Data_Reg[MUX2_VP15U] * KCV * 2;
  len = sprintf(buf, "+15V unregulated  %7.3f\r\n", Vlt);
  pUDP->AddData((PBYTE) buf, len);

  Vlt = (float) ADC2_Data_Reg[MUX2_VM15U] * KCV * 2;
  len = sprintf(buf, "-15V unregulated  %7.3f\r\n", Vlt);
  pUDP->AddData((PBYTE) buf, len);

  Vlt = (float) ADC2_Data_Reg[MUX2_VP15R] * KCV * 2;
  len = sprintf(buf, "+14.5V regulated  %7.3f\r\n", Vlt);
  pUDP->AddData((PBYTE) buf, len);

  Vlt = (float) ADC2_Data_Reg[MUX2_VM15R] * KCV * 2;
  len = sprintf(buf, "-14.5V regulated  %7.3f\r\n", Vlt);
  pUDP->AddData((PBYTE) buf, len);

  Vlt = (float) ADC2_Data_Reg[MUX2_VDD] * KCV;
  len = sprintf(buf, "5.0V digital      %7.3f\r\n", Vlt);
  pUDP->AddData((PBYTE) buf, len);

  Vlt = (float) ADC2_Data_Reg[MUX2_V3D3] * KCV;
  len = sprintf(buf, "3.3V digital      %7.3f\r\n", Vlt);
  pUDP->AddData((PBYTE) buf, len);

  Vlt = (float) ADC2_Data_Reg[MUX2_V2D5] * KCV;
  len = sprintf(buf, "2.5V digital      %7.3f\r\n", Vlt);
  pUDP->AddData((PBYTE) buf, len);

  Vlt = (float) ADC2_Data_Reg[MUX2_V1D2] * KCV;
  len = sprintf(buf, "1.2V digital      %7.3f\r\n", Vlt);
  pUDP->AddData((PBYTE) buf, len );
  
  Vlt = (float) ADC2_Data_Reg[MUX2_TEMP] * KCV;
  T = Vlt * 100.0;
  len = sprintf(buf, "Temperature       %7.3f  %7.1f deg F\r\n", Vlt, T);
  pUDP->AddData((PBYTE) buf, len);

  len = sprintf(buf, "Fan Speed                  %7d RPM\r\n>",FanRPM);
  pUDP->AddData((PBYTE) buf, len);

  udSendResponse(pUDP);
}

//******************* void udShowDigCoef(UDPPacket * pUDP) *********************
//  This function sends the digital regulation coefficients.
//
//  Digital Gain            Gain of digital loop (DAC volts verses ADC volts)
//  Digital Time Constant   Time constant of integrator in seconds
//  Digital Error Limit     Error voltage limit (desired - actual ADC, 10 VFS)
//******************************************************************************

void udTest(UDPPacket * pUDP)
{
  int len = 1;
  char buf[64];
  float x;

  while (len > 0)
  {
    udSendString(pUDP, "Enter Number: ", 14);
    len = udGetString(pUDP, buf, 64);
    if (len > 0)
    {
      x = strtodf(buf, 0);
      len = sprintf(buf, "%8.4f\r\n", x);
      udSendString(pUDP, buf, len);
    }
  }
  pUDP->ResetData();
  len = sprintf(buf, "Test Complete\r\n>");
  pUDP->AddData((PBYTE) buf, len);
  udSendResponse(pUDP);
}
//  len = sprintf(buf, "Digital Gain  %4.2E (DAC Volt/ ADC Volt)\r\n",
//                ShortFloatToFloat(xil.db_eep.DigitalGain));
//  write(fd, buf, len);

//  len = sprintf(buf, "Time Constant %4.2E (sec)\r\n",
//                ShortFloatToFloat(xil.db_eep.DigitalTC));
//  write(fd, buf, len);

//  len = sprintf(buf, "Error Limit   %4.2E (Volt)\r\n",
//                ShortFloatToFloat(xil.db_eep.DigitalErrLim));
//  write(fd, buf, len);



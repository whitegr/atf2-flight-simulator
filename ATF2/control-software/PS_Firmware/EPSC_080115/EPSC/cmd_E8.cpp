//******************************************************************************
//  PS Module Status (0xE8) (Ethernet Power Supply Controller)
//  Filename: cmd_E8.cpp
//  07/19/07 Added serial link (UART1) to  OCEM power supply for ATF2
//  This message returns the status and output currents for the individual
//  modules in an OCEM power supply (ATF2). The command can also be used to
//  enable and disable individual modules.
//
//  The command byte can be set to read status, or enable or disable a module.
//  0x00  Read status
//  0x10  Enable all modules
//  0x1n  Enble module 'n', 1 <= n <= MaxNumMod
//  0x2n  Disble module 'n', 1 <= n <= MaxNumMod
//
//  Initial "MaxNumMod" is 5 for OCEM/ATF2 power supplies, RSP_LEN_E8 = 28
//  Module currents (ModCur) is 16 bit unsigned integer (bits/amp TBD)
//  Data is sent in Intel Little Endian format
//******************************************************************************

#include <C:\Nburn\EPSC\include\includes.h>    //Include all headers for project

typedef struct                    // E8 command structure
{ BYTE   Ctype;                   // 00 command type 0xE1
  BYTE   Rsp;                     // 01 response code
  BYTE   Tid;                     // 02 Task ID
  BYTE   Cmd;                     // 03 Command byte
} __attribute__ ((packed))  Cmd_E8;

#define CMD_LEN_E8  4             // expected length of command message

typedef struct                    // E8 response structure
{ BYTE   Ctype;                   // 00 command type, returned unchanged
  BYTE   Rsp;                     // 01 response code
  BYTE   Tid;                     // 02 Task ID, returned unchanged
  BYTE   Cmd;                     // 03 Command byte, returned unchanged
  BYTE   ModDataValid;            // 04 Data valid flag
  BYTE   NumMod;                  // 05 Number of Modules in power supply (1-8)
  BYTE   ModFault;                // 06 Module Fault Flags
  BYTE   ModDisable;              // 07 Module Disable flags
  SHORT  ModCur[MaxNumMod];       // 08-(7 + 2 * MaxNumMod) Module Currents
} __attribute__ ((packed))  Rsp_E8;

#define RSP_LEN_E8 (8 + 2 * MaxNumMod)  // length of response message

//********************* void Process_E8(UDPPacket * pUDP) **********************
//  pUDP  pointer to object of class UDPPacket, passed to function
//  pCmd  pointer to command structure (type Cmd_E8) set to start of UDP data
//  pRsp  pointer to response structure (type Rsp_E8) set to start of UDP data
//******************************************************************************

void Process_E8(UDPPacket * pUDP)
{
  PBYTE pUdpData = pUDP->GetDataBuffer();      //get pointer to UDP data field
  Cmd_E8 * pCmd = (Cmd_E8 *) pUdpData;         //set pointer for command data
  Rsp_E8 * pRsp = (Rsp_E8 *) pUdpData;         //set pointer for response data

  if (pUDP->GetDataSize() != CMD_LEN_E8)       //check for valid command length
  { pRsp->Rsp = INVALID_MSG_LENGTH;
  }
  else if ( PS_SERIAL_ENABLE )                 //check for PS serial enabled
  {
    BYTE Cmd = pCmd->Cmd;
    if ( Cmd == 0 ) pRsp->Rsp = COMMAND_OK;
    else if (( Cmd >= 0x10 ) && ( Cmd <= 0x10 + MaxNumMod ))
      pRsp->Rsp = COMMAND_OK;
    else if (( Cmd >= 0x21 ) && ( Cmd <= 0x20 + MaxNumMod ))
      pRsp->Rsp = COMMAND_OK;
    else
      pRsp->Rsp = UNKNOWN_COMMAND;

    // If command is OK, send to UART1 queue
    if (pRsp->Rsp == COMMAND_OK) OSQPost( & Uart1XmtQ, (void *)(DWORD) Cmd );

    // Add data to response structure
    pRsp->ModDataValid = ModDataValid;
    pRsp->NumMod       = NumMod;
    pRsp->ModFault     = ModFault;
    pRsp->ModDisable   = ModDisable;
    for (WORD i = 0; i < MaxNumMod; ++i)
    {
      pRsp->ModCur[i] = ModCur[i];
      flip2((PBYTE) & pRsp->ModCur[i] );
    }
    pUDP->SetDataSize(RSP_LEN_E8);             //set response packet data size
  }
  else  // serial link not enabled
  {
    pRsp->Rsp = UNKNOWN_COMMAND;
  }
}


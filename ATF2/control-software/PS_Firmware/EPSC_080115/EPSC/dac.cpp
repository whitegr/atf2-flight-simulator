//******************************************************************************
//  DAC interface functions (Ethernet Power Supply Controller)
//  Filename: dac.cpp
//  02/22/05
//
//  This module contains the functions to interface with the PWM DAC. This includes
//  ramping the DAC, writing the DAC, and the digital regulation function.
//
//  The DAC PWM is part of the Xilinx chip. It is comprised of a 8 bit PWM cycling
//  at 120 Khz. It has a three stage digital filter to provide 24 bit resolution.
//  The DAC has gain and offset calibration pots to set the absolute accuracy to
//  better then 0.01%. For analog regulation, the DAC output is regulated using
//  ADC1 to read the DAC and adjust the DAC offset.
//
//  All variables and status of the DAC are stored in the Xilinx static RAM to
//  allow the processor to be reset and resume operations without disturbing the
//  power supply. Since the processor controls ramping and digital regulation,
//  these functions will be suspended during a reset and resume after the processer
//  has initialized.
//
//  The DAC is set using two numbers. The first is the 24 bit signed number for the
//  calculated DAC setting. The DAC offset is added to this number before being
//  written to the DAC. The second number is the expected ADC reading when ADC1
//  reads the DAC. The expected reading is subtracted from the actual reading and
//  added to the DAC offset.
//
//  Ramping is controled by the ramp ON flag in xil.ps_cntl. Setting this flag on
//  allows the power supply to ramp. Setting the Hold enable flag allows the 
//  external Ramp/Hold signals to set the state of the ramp flag.
//
//******************************************************************************

#include <C:\Nburn\EPSC\include\includes.h>    //Include all headers for project

//********************************** wrDAC() ***********************************
//
// last revised: 02/08/05
//
// This function adds the DAC offset register to the DAC setpoint register and
// writes the result to the DAC PWM register (xil.dac) in the Xilinx.
//
// The DAC offset is calculated by ADC1 by reading the DAC and subtracting the
// expected ADC reading (xil.DAC_Des_ADC). The difference is added to the DAC
// offset register. The offset is divided by a constant DAC_OFFSET_DIV before
// being added. This constant sets the response of the DAC offset nulling loop.
//
// xil.Des_ADC         Desired ADC reading for regulated transductor
// xil.Des_DAC         Desired DAC setpoint for DES_ADC
// xil.DAC_Offset      DAC offset count
// DAC_OFFSET_DIV      loop constant, defined in dac.h
//
// REG_OFF         regulation off
// ANALOG_REG      analog regulation active
// DIGITAL_REG     digital regulation active
// TEST_MODE       digital regulation active
//
//******************************************************************************

void WriteDAC()
{
  //this function allows the diagnostic task to set the DAC in sync with the ADC
  if (NewDesFlag)
  {
    NewDesFlag = 0;
    xil.Des_ADC = TmpDesADC;                        //set desired ADC to new value
    xil.Des_DAC = LL_KMULT(TmpDesADC, KDAC, KADC);  //set desired DAC to new value
  }
  else
  {
    if ( xil.PsOnFlag ) //if power supply is ON 
    {
      //if ramp state = RAMP_ON, process ramp
      if ( xil.ps_status & STATUS_RAMP_ON ) Ramp();
      //if digital regulation enabled, call digital reg function
      if ( xil.PsRegFlag == DIGITAL_REG ) DigReg();
    }
  }

  LONG DAC = xil.Des_DAC - (xil.DAC_Offset /DAC_OFFSET_DIV);
  if (DAC > MAX_DAC) DAC = MAX_DAC;
  if (DAC < -MAX_DAC) DAC = -MAX_DAC;
  xil.dac = DAC;
}

//********************************** ramp() ************************************
//
// last revised: 02/08/05
//
// This function ramps the desired DAC and ADC setpoints. 
//
// Each ramp is comprised of one to five segements. Each segment has a time and an
// end value (in ADC counts). The ramp uses a 120 hz update rate where the Network
// command use a 100 hz timebase. The times are stored internally in units of
// 1/120 of a second. The command uses a 16 bit unsigned integer for the time
// giving a range of 0.01 to 655.35 seconds.
//
// To configure a ramp, the following variables in the Xilinx are written;
//
// xil.Num_Ramp_Seg    Number of segments in current ramp (1-5)
// xil.ADC_SetPnt[0]   Starting setpoint
// xil.ADC_SetPnt[5]   Ramp ADC Setpoints
// xil.Ramp_Times[0]   Total time for all segments (time remaining status)
// xil.Ramp_Times[5]   Ramp Times, 1 to 5 segments
// xil.Cur_Ramp_Seg    Current segement number, initialize to 1
// xil.Cur_Ramp_time   Current time for current ramp segment, initialize to 0
// xil.ps_cntl         Ramp ON flag and ramp Hold enable flag
//
// The desired setpoint is calculated as follows
//
// current ADC = ADC_SetPnt[n-1] + (k * (ADC_SetPnt[n] - ADC_SetPnt[n-1])) / 2**24
// k = ((2**24) * Cur_Ramp_Time) / Ramp_Times[n]
// if cosine ramping is enabled, k = (2**24) * (1-cos(pi * k / 2**24)) / 2
//
//******************************************************************************

void Ramp()
{ WORD i;
  DWORD t0, t1, k;
  LONG y0, y1, y;
  xil.Ramp_Times[0] -= 1;                 //decrement time remaining
  i = xil.Cur_Ramp_Seg;                   //set i to current segment number
  t0 = ++xil.Cur_Ramp_time;               //set t0 to current segment time
  t1 = xil.Ramp_Times[i];                 //set t1 to total segment time
  if (t0 < t1)                            //if current time < segment time 
  { k = LL_KMULT( t0, 1 << 24, t1 );      //set k = (t0 * 2**24) / t1
    if (~xil.db_eep.Config & CONFIG_LIN_RAMP)
    {
      k = LinToCosine(k);                 //if cosine ramp flag set, convert k
    }
    y1 = xil.ADC_SetPnt[i];               //set y1 to end setpoint
    y0 = xil.ADC_SetPnt[i-1];             //set y0 to start setpoint
    y = y0 + LL_KMULT(y1-y0, k, 1 << 24); //set y = y0 + (y1-y0) * k / 2**24
  }
  else                                    //if current time >= segment time
  { y = xil.ADC_SetPnt[i];                //set y to segment end setpoint
    if (i <  xil.Num_Ramp_Seg)            //if current segment num not last segment
    { ++xil.Cur_Ramp_Seg;                 //set cuurent segmnet num to next segment
      xil.Cur_Ramp_time = 0;              //set current time to zero
    }
    else
    { xil.ps_cntl = PSCNTL_RAMP_OFF       //set Ramp status to OFF
                  + PSCNTL_HOLD_OFF;      //set Hold Enable to OFF
      xil.Ramp_State = RAMP_DONE;         //set Ramp State to Done
    }
  }
  xil.Des_ADC = y;                        //set desired ADC to new value

  //if analog regulation enabled, set desired DAC to new value
  if (xil.PsRegFlag == ANALOG_REG) xil.Des_DAC = LL_KMULT(y, KDAC, KADC);
}

//***************************** InitCosineTable() ******************************
//
// last revised: 02/08/05
//
// This function initializes a 257 DWORD table of ramp coefficients. The table
// is used by the ramp function to approximate the (1-cos(t))/2 function. The
// coefficients have a range of 0 to 0x1000000 (24 bit).
//
// Cos_Table[i] = 0x1000000 * (1 - cos(pi * i / 256) ) / 2
//
// This function is called once during processor initialization.
// Note: measured 65 milliseconds to run
//******************************************************************************

DWORD CosineTable[257];              //Declare cosine ramp lookup table

void InitCosineTable()
{ WORD i;
  DWORD k;
  double x, y;
  for (i=0; i < 129; ++i)
  { x = 3.14159265 *(double) i / 256.0 ;
    y = cos(x);
    k =(DWORD) (0.5 + (double)0x1000000 * (1.0 - y) / 2.0);
    CosineTable[i] = k;
    CosineTable[256 - i] = 0x1000000 - k;
  }
}

//******************************* LinToCosine() ********************************
//
//  last revised: 02/08/05
//
//  This function converts a 24 bit linear coefficient to 24 bit cosine coefficient.
//  The upper 8 bits are used as an index into the lookup table. The lower 16 bits
// are used to interpolate between two values.
//
//  ku = k / 0x1000   ku is upper eight bits of k
//  kl = k % 0x1000   kl is lower eight bits of k
//  k = CosineTable[ku] + (CosineTable[ku+1]-CosineTable[ku])*kl/0x10000
//
//******************************************************************************

DWORD LinToCosine(DWORD k)
{ DWORD ku, kl, tu, tl, kc;
  if (k > 0xFFFFFF) k = 0xFFFFFF;          //Force k <= 0xFFFFFF
  if (k < 0) k = 0;                        //Force k => 0
  ku = k / 0x10000;                        //Set ku to upper 8 bits of k
  kl = k % 0x10000;                        //Set kl to lower 16 bits of k
  tu = CosineTable[ku+1];                  //Set tu to upper table value
  tl = CosineTable[ku];                    //Set tu to lower table value
  kc = tl + LL_KMULT(tu-tl, kl, 0x10000);  //Set kc = tl + (tu-tl)*kl/0x10000
  return(kc);
}


//******************************************************************************
//  DYNAMIC DATA COMMAND (0xCF) (Ethernet Power Supply Controller)
//  Filename: cmd_CF.cpp
//  09/07/05
//  This message returns all dynamic data in one message
//******************************************************************************

#include <C:\Nburn\EPSC\include\includes.h>    //Include all headers for project

typedef struct          // CF command structure
{ BYTE   Ctype;         // 00 command type 0xCF
  BYTE   Rsp;           // 01 response code
  BYTE   Tid;           // 02 Task ID
  BYTE   Chan;          // 03 Channel number (must be zero)
} __attribute__ ((packed))  Cmd_CF;

#define CMD_LEN_CF  4   // expected length of command message

typedef struct          // setpoint
{ float  Cur;           // Power supply current
  WORD   Time;          // time in 1/100 second or 1/20 second (slow ramp)
} __attribute__ ((packed))  setp;

typedef struct          // CF response structure
{ BYTE   Ctype;         // 00 command type, return unchanged
  BYTE   Rsp;           // 01 response code
  BYTE   Tid;           // 02 Task ID, return unchanged
  BYTE   Chan;          // 03 Channel, return unchanged
  WORD   Status01;      // 04-05 Status bytes 0 and 1
  WORD   Status23;      // 06-07 Status bytes 2 and 3

  float  RegI;          // 08-11 PS current read from regulated transductor
  float  AuxI;          // 12-15 PS current read from auxiliary transductor
  float  DacI;          // 16-19 DAC voltage (scaled to amps)
  float  RipI;          // 20-23 PS current (reg xduct) ripple
  float  GndI;          // 24-27 PS ground current
  float  Temp;          // 28-31 Controller temperature (degrees F)
  float  PsV;           // 32-35 Power supply volts
  float  Spare;         // 36-39 Spare channel voltage
  float  AGnd;          // 40-43 Absolute round current
  float  Klix0;         // 44-47 Klixon 0 conductance
  float  Klix1;         // 48-51 Klixon 1 conductance
  float  VP15U;         // 52-55 Unregulated +15V
  float  VM15U;         // 56-59 Unregulated -15V
  float  VP15R;         // 60-63 Regulated +14.5
  float  VM15R;         // 64-67 Regulated -14.5
  float  A10V;          // 68-71 Analog +10V reference
  float  A5V;           // 72-75 Analog +5V for ADCs
  float  VDD;           // 76-79 Digital 5.0 volts
  float  V3D3;          // 80-83 Digital 3.3 volts
  float  V2D5;          // 84-87 Digital 2.5 volts
  float  V1D2;          // 88-91 Digital 1.2 volts
  SHORT  FanRPM;        // 92-93 Fan RPM

  SHORT  Adc2Off;       // 94-95 ADC2 offset correction +/-0x7FFF
  SHORT  Adc2Gain;      // 96-97 ADC2 gain factor +/-0x7FFF
  SHORT  Adc1Off;       // 98-99 ADC1 offset correction +/-0x7FFF
  SHORT  Adc1Gain;      // 100-101 ADC1 gain factor +/-0x7FFF
  SHORT  DacOff;        // 102-103 DAC offset +/-7FFF
  SHORT  DacGain;       // 104-105 DAC Gain, NOT USED

  BYTE   LastRst;       // 106 last reset code
  BYTE   LastOff;       // 107 last PS turn off code
  BYTE   CalErr;        // 108 ADC and DAC Calibration error codes
  BYTE   SelfTst;       // 109 Self test code

  BYTE   RampState;     // 110 Ramp state
  BYTE   NumSetP;       // 111 Number of setpoints
  float  CurSetPnt;     // 112-115 Current Setpoint of ramp (amps)
  float  StrSetPtn;     // 116-119 Starting setpoint of ramp (amps)
  DWORD  TimeRem;       // 120-123 remaining time in ramp, 0.01 sec / cnt
  setp   Setpoint[5];   // 124-153 Setpoint 1 to 5

} __attribute__ ((packed))  Rsp_CF;

#define RSP_LEN_CF  154  // length of respone message

//********************* void Process_C8(UDPPacket * pUDP) **********************
//  pUDP  pointer to object of class UDPPacket, passed to function
//  pCmd  pointer to command structure (type Cmd_C8) set to start of UDP data
//  pRsp  pointer to response structure (type Rsp_C8) set to start of UDP data
//******************************************************************************

void Process_CF(UDPPacket * pUDP)
{
  PBYTE pUdpData = pUDP->GetDataBuffer();        //get pointer to UDP data field
  Cmd_CF * pCmd = (Cmd_CF *) pUdpData;           //set pointer for command data
  Rsp_CF * pRsp = (Rsp_CF *) pUdpData;           //set pointer for response data

  if (pUDP->GetDataSize() != CMD_LEN_CF)         //check for valid command length
  { pRsp->Rsp = INVALID_MSG_LENGTH;
  }
  else if (pCmd->Chan !=0)                       //check for valid channel (0)
  { pRsp->Rsp = INVALID_CHAN_NUM;
  }
  else
  {
    float KCV = 1 / (float) KADC;                  //volts per ADC count

    pRsp->Status01 = Status01(NO_ERR);         //set status bytes 0 and 1
    pRsp->Status23 = Status23();               //set status bytes 2 and 3

    //Regulated Transductor reading, converted to amps
    pRsp->RegI = KCV * RevPol((float)ADC2_Data_Reg[MUX2_REG]) * xil.db_eep.reg_iva;
    flip4((PBYTE) & pRsp->RegI);

    //Auxiliary Transductor reading, converted to amps
    pRsp->AuxI = KCV * (float)ADC2_Data_Reg[MUX2_AUX] * xil.db_eep.aux_iva;
    flip4((PBYTE) & pRsp->AuxI);

    //DAC reading, converted to amps
    pRsp->DacI = KCV * RevPol((float)ADC2_Data_Reg[MUX2_DAC]) * xil.db_eep.reg_iva;
    flip4((PBYTE) & pRsp->DacI);

    //Absolute ripple current, converted to amps, adjusted for gain = 100
    pRsp->RipI = KCV * (float)ADC2_Data_Reg[MUX2_ARIP] * xil.db_eep.reg_iva * 0.01;
    flip4((PBYTE) & pRsp->RipI);

    //Ground current, converted to amps
    pRsp->GndI = KCV * (float)ADC2_Data_Reg[MUX2_GND] * xil.db_eep.gnd_iva;
    flip4((PBYTE) & pRsp->GndI);

    //Chassis temperature in degrees F (sensor = 10mV/degree F)
    pRsp->Temp = KCV * (float)(100 * ADC2_Data_Reg[MUX2_TEMP]);
    flip4((PBYTE) & pRsp->Temp);

    //Power supply output voltage
    pRsp->PsV  = KCV * (float)ADC2_Data_Reg[MUX2_PSV] * xil.db_eep.psv_iva;
    flip4((PBYTE) & pRsp->PsV);

    //Chassis spare channel
    pRsp->Spare = KCV * (float)ADC2_Data_Reg[MUX2_SPARE];
    flip4((PBYTE) & pRsp->Spare);

    //Absolute Ground Current
    pRsp->AGnd = KCV * (float)ADC2_Data_Reg[MUX2_AGND] * xil.db_eep.gnd_iva;
    flip4((PBYTE) & pRsp->AGnd);

    //Klixon 0 conductance (micro mho)
    pRsp->Klix0 = KlixCond(KCV * (float) ADC2_Data_Reg[MUX2_KLIX0]);
    flip4((PBYTE) & pRsp->Klix0);

    //Klixon 1 conductance (micro mho)
    pRsp->Klix1 = KlixCond(KCV * (float) ADC2_Data_Reg[MUX2_KLIX1]);
    flip4((PBYTE) & pRsp->Klix1);

    //Unregulated +15V
    pRsp->VP15U = KCV * (float)(2 * ADC2_Data_Reg[MUX2_VP15U]);
    flip4((PBYTE) & pRsp->VP15U);

    //Unregulated -15V
    pRsp->VM15U = KCV * (float)(2 * ADC2_Data_Reg[MUX2_VM15U]);
    flip4((PBYTE) & pRsp->VM15U);

    //Regulated +14.5V
    pRsp->VP15R = KCV * (float)(2 * ADC2_Data_Reg[MUX2_VP15R]);
    flip4((PBYTE) & pRsp->VP15R);

    //Regulated -14.5V
    pRsp->VM15R = KCV * (float)(2 * ADC2_Data_Reg[MUX2_VM15R]);
    flip4((PBYTE) & pRsp->VM15R);

    //Digital 5.0V
    pRsp->VDD = KCV * (float) ADC2_Data_Reg[MUX2_VDD];
    flip4((PBYTE) & pRsp->VDD);

    //Digital 3.3V
    pRsp->V3D3 = KCV * (float) ADC2_Data_Reg[MUX2_V3D3];
    flip4((PBYTE) & pRsp->V3D3);

    //Digital 2.5V
    pRsp->V2D5 = KCV * (float) ADC2_Data_Reg[MUX2_V2D5];
    flip4((PBYTE) & pRsp->V2D5);

    //Digital 1.2V
    pRsp->V1D2 = KCV * (float) ADC2_Data_Reg[MUX2_V1D2];
    flip4((PBYTE) & pRsp->V1D2);

    //Analog 5.0V (ADC power)
    pRsp->A5V = KCV * (float) ADC2_Data_Reg[MUX2_A5V];
    flip4((PBYTE) & pRsp->A5V);

    //Analog 10.0V (Reference
    pRsp->A10V = KCV * (float) ADC2_Data_Reg[MUX2_A10V];
    flip4((PBYTE) & pRsp->A10V);

    //Fan RPM
    pRsp->FanRPM = FanRPM;
    flip2((PBYTE) & pRsp->FanRPM);

    //ADC1 offset factor scaled to short +/-0x7FFF
    pRsp->Adc1Off = (SHORT)LL_KMULT(ADC1_Offset, 0x7FFF, MAX_ADC_OFFSET);
    flip2((PBYTE) & pRsp->Adc1Off);

    //ADC1 gain factor scaled to short +/-0x7FFF
    pRsp->Adc1Gain = (SHORT)LL_KMULT(ADC1_Gain, 0x7FFF, MAX_ADC_GAIN);
    flip2((PBYTE) & pRsp->Adc1Gain);

    //ADC2 offset factor scaled to short +/-0x7FFF
    pRsp->Adc2Off = (SHORT)LL_KMULT(ADC2_Offset, 0x7FFF, MAX_ADC_OFFSET);
    flip2((PBYTE) & pRsp->Adc2Off);

    //ADC2 gain factor scaled to short +/-0x7FFF
    pRsp->Adc2Gain = (SHORT)LL_KMULT(ADC2_Gain, 0x7FFF, MAX_ADC_GAIN);
    flip2((PBYTE) & pRsp->Adc2Gain);
    
    //DAC offset scaled to short +/-7FFF
    pRsp->DacOff = (SHORT)LL_KMULT(xil.DAC_Offset, 0x7FFF, MAX_DAC_OFFSET);
    flip2((PBYTE) & pRsp->DacOff);

    //DAC gain scaled to short +/-7FFF (NOT USED IN EPSC, set to zero)
    pRsp->DacGain = 0;

    //Status code bytes
    pRsp->LastRst = (BYTE) xil.LastReset;      //last reset code
    pRsp->LastOff = (BYTE) xil.LastOff;        //last PS turn off code
    pRsp->CalErr  = (BYTE) CalErr;             //ADC and DAC Calibration error codes
    pRsp->SelfTst = (BYTE) HrdFlt;             //Hardware Fault code

    //Ramp status
    pRsp->RampState = xil.Ramp_State;          //Ramp state
    pRsp->NumSetP = xil.Num_Ramp_Seg;          //Number of setpoints

    //Current Setpoint in amps
    pRsp->CurSetPnt = RevPol(KCV * (float)xil.Des_ADC * xil.db_eep.reg_iva);
    flip4((PBYTE) & pRsp->CurSetPnt);

    //Sarting Setpoint of current ramp
    pRsp->StrSetPtn = RevPol(KCV * (float)xil.ADC_SetPnt[0] * xil.db_eep.reg_iva);
    flip4((PBYTE) & pRsp->StrSetPtn);

    //remaining time in ramp, converted to 0.01 sec/cnt
    pRsp->TimeRem = Chg120To100( xil.Ramp_Times[0] );
    flip4((PBYTE) & pRsp->TimeRem);

    //Setpoints 1 to 5
    for (WORD i=1; i <= 5; ++i)
    {
      pRsp->Setpoint[i-1].Time = Chg120To100( xil.Ramp_Times[i] );
      flip2((PBYTE) & pRsp->Setpoint[i-1].Time);

      pRsp->Setpoint[i-1].Cur = KCV * xil.db_eep.reg_iva
                              * RevPol( (float)xil.ADC_SetPnt[i] );
      flip4((PBYTE) & pRsp->Setpoint[i-1].Cur);
    }

    pRsp->Rsp = COMMAND_OK;                      //set response code
    pUDP->SetDataSize(RSP_LEN_CF);               //set data size for response packet
  }
}


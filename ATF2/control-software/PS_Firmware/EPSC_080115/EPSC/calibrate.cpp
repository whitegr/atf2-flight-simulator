//******************************************************************************
//  Diagnostic Port Calibration Functions
//  Filename: calibrate.cpp
//  05/05/05
//
//  These functions allow display and adjustment of ADC and DAC calibration.
//
//******************************************************************************

#include <C:\Nburn\EPSC\include\includes.h>    //Include all headers for project

//************************ void spShowAdc1Gain(int fd) *************************
//  This function displays the regulated ADC gain coefficient normalized to
//  +/- 100%
//******************************************************************************

void spShowAdc1Gain(int fd)
{
  int num = 0;       //number of times coefficient sent to serial port
  char buf[16];      //character buffer for output string
  int len;           //string length

  writestring(fd, "Regulated ADC gain coefficient (%), hit enter key to stop\r\n");
  while (( !dataavail(fd) ) && (num < 1200))
  {
    float GainCoef = 100.0 * (float)ADC1_Gain / (float) MAX_ADC_GAIN;
    len = sprintf(buf, "%6.1f ", GainCoef);
    write(fd, buf, len);
    OSTimeDly(TICKS_PER_SECOND / 4);
    if ((++num % 8) == 0) writestring(fd,"\r\n");
  }
  if ((num % 8) != 0) writestring(fd,"\r\n");
}

//************************ void spShowAdc2Gain(int fd) *************************
//  This function displays the readback ADC gain coefficient normalized to
//  +/- 100%
//******************************************************************************

void spShowAdc2Gain(int fd)
{
  int num = 0;       //number of times coefficient sent to serial port
  char buf[16];      //character buffer for output string
  int len;           //string length

  writestring(fd, "Readback ADC gain coefficient (%), hit enter key to stop\r\n");
  while (( !dataavail(fd) ) && (num < 1200))
  {
    float GainCoef = 100.0 * (float)ADC2_Gain / (float) MAX_ADC_GAIN;
    len = sprintf(buf, "%6.1f ", GainCoef);
    write(fd, buf, len);
    OSTimeDly(TICKS_PER_SECOND / 4);
    if ((++num % 8) == 0) writestring(fd,"\r\n");
  }
  if ((num % 8) != 0) writestring(fd,"\r\n");
}

//************************ void spShowDacOffset(int fd) ************************
//  This function displays the DAC offset coefficient normalized to +/- 100% with
//  the DAC set to zero volts to allow adjustment of the DAC ofsset pot
//******************************************************************************

void spShowDacOffset(int fd)
{
  int num = 0;       //number of times coefficient sent to serial port
  char buf[16];      //character buffer for output string
  int len;           //string length

  writestring(fd, "DAC offset coefficient (%), hit enter key to stop\r\n");

  if (~xil.ps_status & STATUS_PS_OFF)      //check for power supply ON
  {
    writestring(fd, "*** ABORTED, POWER SUPPLY ON ***\r\n");
  }
  else
  {
    TmpDesADC = 0;                  //set desired ADC (DAC setpoint) to zero
    NewDesFlag = 1;                 //set new data flag

    while (( !dataavail(fd) ) && (num < 1200))
    {
      float GainCoef = 100.0 * (float)xil.DAC_Offset / (float) MAX_DAC_OFFSET;
      len = sprintf(buf, "%6.1f ", GainCoef);
      write(fd, buf, len);
      OSTimeDly(TICKS_PER_SECOND / 4);
      if ((++num % 8) == 0) writestring(fd,"\r\n");
    }
    if ((num % 8) != 0) writestring(fd,"\r\n");
  }
}

//************************* void spShowDacGain(int fd) *************************
//  This function displays the DAC offset coefficient normalized to +/- 100% with
// the DAC set to 7 volts to allow adjustment of the DAC gain pot
//******************************************************************************

void spShowDacGain(int fd)
{
  int num = 0;       //number of times coefficient sent to serial port
  char buf[16];      //character buffer for output string
  int len;           //string length

  writestring(fd, "DAC gain coefficient (%), hit enter key to stop\r\n");

  if (~xil.ps_status & STATUS_PS_OFF)      //check for power supply ON
  {
    writestring(fd, "*** ABORTED, POWER SUPPLY ON ***\r\n");
  }
  else
  {
    TmpDesADC = 7 * KADC;           //set desired ADC (DAC setpoint) to 7 volts
    NewDesFlag = 1;                 //set new data flag
    while (( !dataavail(fd) ) && (num < 1200))
    {
      OSTimeDly(TICKS_PER_SECOND / 4);
      float GainCoef = 100.0 * (float)xil.DAC_Offset / (float) MAX_DAC_OFFSET;
      len = sprintf(buf, "%6.1f ", GainCoef);
      write(fd, buf, len);
      if ((++num % 8) == 0) writestring(fd,"\r\n");
    }
    if ((num % 8) != 0)writestring(fd,"\r\n");
  }
}


//******************************************************************************
//  Digital Regulation functions (Ethernet Power Supply Controller)
//  Filename: DigReg.cpp
//  05/19/05
//
//  This functions performs digital regulation by comparing the desired readback
//  to the actual readback and adjusting the DAC output until they are equal. The
//  readback uses the regulated transductor input to ADC1.
//
//  xil.Des_ADC       desired ADC value
//
//  xil.DigGain       Digital regulation gain * 200 * KDAC / KADC
//  xil.ErrLimit      error limit (ADC counts)
//  xil.ErrSum        error sum (integrated error ADC counts), 64 bit long long
//  xil.ErrSumGain    error sum gain coefficient, 64 bit long long
//  xil.ErrSumMax     maimum value of error sum upper word
//  xil.ErrSumMin     minimum value of error sum upper word
//
//******************************************************************************

#include <C:\Nburn\EPSC\include\includes.h>

//pointer to access upper 32 bits ErrSum as long
long * pErrSum = (long *) &xil.ErrSum;

void DigReg()
{
  if ( ADC1_DREG_FLAG != 0);
  {
    ADC1_DREG_FLAG = 0;

    WORD LimitFlag = 0;                  //flag set if any variable reaches limit

    long ErrVlt = xil.Des_ADC - ADC1_DREG_DATA;

    if ( Limit(&ErrVlt, xil.ErrLimit) ) LimitFlag = 1;

    xil.ErrSum = xil.ErrSum + xil.ErrSumGain * (long long) ErrVlt;
    if (pErrSum[0] > xil.ErrSumMax)
    { pErrSum[0] = xil.ErrSumMax;
      LimitFlag = 1;
    }
    else if ( pErrSum[0] < xil.ErrSumMin )
    {
      pErrSum[0] = xil.ErrSumMin;
      LimitFlag = 1;
    }

    xil.Des_DAC = pErrSum[0] + LL_KMULT( ErrVlt, xil.DigGain, 65536);

    if (LimitFlag != 0)                   //if LimitFlag not zero
      CalErr |= DIG_REG_ERR;              //set Digital Regulation error flag
    else                                  //else
      CalErr &= ~DIG_REG_ERR;             //clear Digital Regulation error flag
  }
}

//***************************** void InitDigCoef() *****************************
//  Initialize digital regulation coefficients
//******************************************************************************

void InitDigCoef()
{
  float fpKADC = (float) KADC;                   //convert volts to ADC counts
  float fpKDAC = (float) KDAC / (float) KADC;    //convert ADC units to DAC units
  float fpKTC =  (float)(4294967296e0) / 120.0;  //time constanst scale factor

  float gain = ShortFloatToFloat(xil.db_eep.DigitalGain) * fpKDAC;
  float tc   = ShortFloatToFloat(xil.db_eep.DigitalTC);
  float lim  = ShortFloatToFloat(xil.db_eep.DigitalErrLim);

  xil.DigGain  = (long)( gain * 65536.0);
  xil.ErrLimit = (long)( lim * fpKADC);
  xil.ErrSumGain = (long long) ( gain * fpKTC / tc );

  xil.ErrSum = 0;                    //clear digital integrator
  xil.ErrSumMax = MAX_DAC;           //set integrator limits
  if (PS_IS_BIPOLAR)
    xil.ErrSumMin = -MAX_DAC;
  else
    xil.ErrSumMin = -MAX_DAC/100;
}


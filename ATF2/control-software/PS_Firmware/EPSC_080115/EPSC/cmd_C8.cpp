//******************************************************************************
//  ANALOG READBACK COMMAND (0xC8) (Ethernet Power Supply Controller)
//  Filename: cmd_C8.cpp
//  03/03/05
//  This message returns 8 analog voltages previously read by ADC2.
//******************************************************************************

#include <C:\Nburn\EPSC\include\includes.h>    //Include all headers for project

typedef struct          // C8 command structure
{ BYTE   Ctype;         // 00 command type 0xC8
  BYTE   Rsp;           // 01 response code
  BYTE   Tid;           // 02 Task ID
  BYTE   Chan;          // 03 Channel number (must be zero)
} __attribute__ ((packed))  Cmd_C8;

#define CMD_LEN_C8  4   // expected length of command message

typedef struct          // C8 response structure
{ BYTE   Ctype;         // 00 command type, return unchanged
  BYTE   Rsp;           // 01 response code
  BYTE   Tid;           // 02 Task ID, return unchanged
  BYTE   Chan;          // 03 Channel, return unchanged
  float  RegI;          // 04-07 PS current read from regulated transductor
  float  AuxI;          // 08-11 PS current read from auxiliary transductor
  float  DacI;          // 12-15 DAC voltage (scaled to amps)
  float  RipI;          // 16-19 PS current (reg xduct) ripple
  float  GndI;          // 20-23 PS ground current
  float  Temp;          // 24-27 Controller temperature (degrees F)
  float  PsV;           // 28-31 Power supply volts
  float  Spare;         // 32-35 Spare channel voltage
} __attribute__ ((packed))  Rsp_C8;

#define RSP_LEN_C8  36  // length of respone message

//********************* void Process_C8(UDPPacket * pUDP) **********************
//  pUDP  pointer to object of class UDPPacket, passed to function
//  pCmd  pointer to command structure (type Cmd_C8) set to start of UDP data
//  pRsp  pointer to response structure (type Rsp_C8) set to start of UDP data
//******************************************************************************

void Process_C8(UDPPacket * pUDP)
{
  PBYTE pUdpData = pUDP->GetDataBuffer();        //get pointer to UDP data field
  Cmd_C8 * pCmd = (Cmd_C8 *) pUdpData;           //set pointer for command data
  Rsp_C8 * pRsp = (Rsp_C8 *) pUdpData;           //set pointer for response data

  if (pUDP->GetDataSize() != CMD_LEN_C8)         //check for valid command length
  { pRsp->Rsp = INVALID_MSG_LENGTH;
  }
  else if (pCmd->Chan !=0)                       //check for valid channel (0)
  { pRsp->Rsp = INVALID_CHAN_NUM;
  }
  else
  { pRsp->RegI = RevPol( (float)ADC2_Data_Reg[MUX2_REG] ) // Regulated Transductor
                 * xil.db_eep.reg_iva              //times reg xduct amps per volt
                 / (float)KADC;                    //divided by ADC2 counts per volt
    pRsp->AuxI = (float)ADC2_Data_Reg[MUX2_AUX]    //ADC2 Auxiliary Transductor
                 * xil.db_eep.aux_iva              //times aux xduct amps per volt
                 / (float)KADC;                    //divided by ADC2 counts per volt
    pRsp->DacI = RevPol( (float)ADC2_Data_Reg[MUX2_DAC] ) //ADC2 Dac voltage
                 * xil.db_eep.reg_iva              //times reg xduct amps per volt
                 / (float)KADC;                    //divided by ADC2 counts per volt
    pRsp->RipI = (float)ADC2_Data_Reg[MUX2_ARIP]   //ADC2 PS abs ripple current
                 * xil.db_eep.reg_iva              //times reg xduct amps per volt
                 * 0.01                            //times 1/ripple gain (1/100)
                 / (float)KADC;                    //divided by ADC2 counts per volt
    pRsp->GndI = (float)ADC2_Data_Reg[MUX2_GND]    //ADC2 PS ground current
                 * xil.db_eep.gnd_iva              //times ground amps per volt
                 / (float)KADC;                    //divided by ADC2 counts per volt
    pRsp->Temp = (float)ADC2_Data_Reg[MUX2_TEMP]   //ADC2 Controller Temperature
                 * 100.0                           //times 100 (sensor 10mV/deg f)
                 / (float)KADC;                    //divided by ADC2 counts per volt
    pRsp->PsV  = (float)ADC2_Data_Reg[MUX2_PSV]    //ADC2 PS output voltage
                 * xil.db_eep.psv_iva              //times PS volts per ADC volt
                 / (float)KADC;                    //divided by ADC2 counts per volt
    pRsp->Spare = (float)ADC2_Data_Reg[MUX2_SPARE] //ADC2 spare channel
                 / (float)KADC;                    //divided by ADC2 counts per volt

    //convert data to little endian (Intel) by reversing byte order
    flip4((PBYTE) & pRsp->RegI);
    flip4((PBYTE) & pRsp->AuxI);
    flip4((PBYTE) & pRsp->DacI);
    flip4((PBYTE) & pRsp->RipI);
    flip4((PBYTE) & pRsp->GndI);
    flip4((PBYTE) & pRsp->Temp);
    flip4((PBYTE) & pRsp->PsV);
    flip4((PBYTE) & pRsp->Spare);

    pRsp->Rsp = COMMAND_OK;                      //set response code
    pUDP->SetDataSize(RSP_LEN_C8);               //set data size for response packet
  }
}


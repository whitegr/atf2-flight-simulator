//******************************************************************************
//  CHASSIS TEST COMMAND (0xF1) (Ethernet Power Supply Controller)
//  Filename: cmd_F1.cpp
//  01/12/06
//  This message requests a new value from ADC1 for the channel specified
//
//  To request data from ADC1, the function sets ADC1_INST_REQ_FLAG, selects an
//  ADC channel (ADC1_INST_REQ_CHAN) and pends on a semaphore (ADC1_INST_SEM).
//  ADC1 reads the selected channel ,places the data in ADC1_INST_REQ_DATA, and
//  sets the semaphore to allow the function to continue.
//
//  ADC1_INST_SEM          Semaphore for instruction task
//  ADC1_INST_REQ_FLAG     ADC1 Instrustion read request flag
//  ADC1_INST_REQ_CHAN     ADC1 Instruction read request channel
//  ADC1_INST_REQ_DATA     ADC1 Instruction read request data
//
//  ADC1_INST_REQ_FLAG states
//  ADC1_REQ_OFF           No request pending
//  ADC1_REQ_ON            Request single cycle conversion
//  ADC1_REQ_CONV          Conversion in progress
//  ADC1_REQ_DONE          Request done, data valid
//
//******************************************************************************

#include <C:\Nburn\EPSC\include\includes.h>    //Include all headers for project

typedef struct          // F1 command structure
{ BYTE   Ctype;         // 00 command type 0xF1
  BYTE   Rsp;           // 01 response code
  BYTE   Tid;           // 02 Task ID
  BYTE   Chan;          // 03 Channel number (0-3)
} __attribute__ ((packed))  Cmd_F1;

#define CMD_LEN_F1 4    //expected length of command message

typedef struct          // F1 response structure
{ BYTE   Ctype;         // 00 command type, returned unchanged
  BYTE   Rsp;           // 01 response code
  BYTE   Tid;           // 02 Task ID, returned unchanged
  BYTE   Chan;          // 03 Channel read
  float  AdcV;          // 04-07 ADC reading
} __attribute__ ((packed))  Rsp_F1;

#define RSP_LEN_F1 8   //length of response message

//********************* void Process_F1(UDPPacket * pUDP) **********************
//  pUDP  pointer to object of class UDPPacket, passed to function
//  pCmd  pointer to command structure (type Cmd_F1) set to start of UDP data
//  pRsp  pointer to response structure (type Rsp_F1) set to start of UDP data
//******************************************************************************

void Process_F1(UDPPacket * pUDP)
{
  PBYTE pUdpData = pUDP->GetDataBuffer();      //get pointer to UDP data field
  Cmd_F1 * pCmd = (Cmd_F1 *) pUdpData;         //set pointer for command data
  Rsp_F1 * pRsp = (Rsp_F1 *) pUdpData;         //set pointer for response data

  if (pUDP->GetDataSize() != CMD_LEN_F1)       //check for valid command length
  { pRsp->Rsp = INVALID_MSG_LENGTH;
  }
  else if (pCmd->Chan > 3)                     //check for valid channel (0-3)
  { pRsp->Rsp = INVALID_CHAN_NUM;
  }
  else
  {
    float V = 0.0;                             //default ADC1 reading
    pRsp->Rsp = COMMAND_ERR;                   //set response code

    ADC1_INST_REQ_CHAN = pCmd->Chan;           //set channel
    ADC1_INST_REQ_FLAG = ADC1_REQ_ON;          //set request flag ON

    //pend on instruction semaphore, check for timeout, request done flag
    if (OS_NO_ERR == OSSemPend(&ADC1_INST_SEM, TICKS_PER_SECOND))
    {
      if (ADC1_INST_REQ_FLAG == ADC1_REQ_DONE)
      {
        V = (float)ADC1_INST_REQ_DATA / (float)KADC;
        pRsp->Rsp = COMMAND_OK;
      }
    }
    ADC1_INST_REQ_FLAG = ADC1_REQ_OFF;         //set request flag to off

    flip4((PBYTE) &V);                         //convert to little endian
    pRsp->AdcV = V;                            //set ADC Volts
    pUDP->SetDataSize(RSP_LEN_F1);             //set data size for response packet
  }
}


//******************************************************************************
//  WRITE DC FAN VOLTAGE (0xF4) (Ethernet Power Supply Controller)
//  Filename: cmd_F4.cpp
//  02/14/06
//  This message allows direct control of the DC Fan voltage. Writing a value
//  greater then or equal to 0 removes the fan from temperature control and
//  the voltage to the fan DAC. A negative value returns the fan to temperature
//  control.
//
//  The response has the current fan voltage and RPM.
//
//  Vlt < 0             Return fan to temperature control
//  Vlt = 0             Set Fan voltage to zero
//  Vlt < min           Set fan to minimum voltage
//  min < Vlt < max     Set fan voltage to "Vlt"
//  Vlt > max           Set fan to maximum volatage
//
//******************************************************************************

#include <C:\Nburn\EPSC\include\includes.h>    //Include all headers for project

typedef struct          // F4 command structure
{ BYTE   Ctype;         // 00 command type 0xC0
  BYTE   Rsp;           // 01 response code
  BYTE   Tid;           // 02 Task ID
  BYTE   Chan;          // 03 Channel number, ignored
  float  Vlt;           // 04 - 07 Fan Volts
} __attribute__ ((packed))  Cmd_F4;

#define CMD_LEN_F4 8    //expected length of command message

typedef struct          // F3 response structure
{ BYTE   Ctype;         // 00 command type, returned unchanged
  BYTE   Rsp;           // 01 response code
  BYTE   Tid;           // 02 Task ID, returned unchanged
  BYTE   Chan;          // 03 Channel number, returned unchanged
  float  Vlt;           // 04 - 07 Fan Volts
  float  Temp;          // 08 - 11 Chassis Temperature
  WORD   FanRPM;        // 12 - 13 Fan RPM
} __attribute__ ((packed))  Rsp_F4;

#define RSP_LEN_F4 14   //length of response message

//********************* void Process_F4(UDPPacket * pUDP) **********************
//  pUDP  pointer to object of class UDPPacket, passed to function
//  pCmd  pointer to command structure (type Cmd_F4) set to start of UDP data
//  pRsp  pointer to response structure (type Rsp_F4) set to start of UDP data
//******************************************************************************

void Process_F4(UDPPacket * pUDP)
{
  PBYTE pUdpData = pUDP->GetDataBuffer();      //get pointer to UDP data field
  Cmd_F4 * pCmd = (Cmd_F4 *) pUdpData;         //set pointer for command data
  Rsp_F4 * pRsp = (Rsp_F4 *) pUdpData;         //set pointer for response data

  if (pUDP->GetDataSize() != CMD_LEN_F4)       //check for valid command length
    pRsp->Rsp = INVALID_MSG_LENGTH;
  else
  {
    flip4((PBYTE) & pCmd->Vlt);

    pRsp->Vlt = WrFanVlt( pCmd->Vlt );         //set fan voltage
    flip4((PBYTE) & pRsp->Vlt);

    pRsp->Temp = (float)(100 * ADC2_Data_Reg[MUX2_TEMP] / (float) KADC );
    flip4((PBYTE) & pRsp->Temp);

    pRsp->FanRPM = FanRPM;
    flip2((PBYTE) & pRsp->FanRPM);

    pRsp->Rsp = COMMAND_OK;                    //set response code
    pUDP->SetDataSize(RSP_LEN_F4);             //set data size for response packet
  }
}


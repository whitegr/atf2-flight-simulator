//******************************************************************************
//  CHASSIS TEST COMMAND (0xF0) (Ethernet Power Supply Controller)
//  Filename: cmd_F0.cpp
//  01/12/05
//  This command directly writes the DAC value. The power supply must be off for
//  the command to be processed.
//
//******************************************************************************

#include <C:\Nburn\EPSC\include\includes.h>    //Include all headers for project

typedef struct          // F0 command structure
{ BYTE   Ctype;         // 00 command type 0xC0
  BYTE   Rsp;           // 01 response code
  BYTE   Tid;           // 02 Task ID
  BYTE   Chan;          // 03 Channel number (ignored)
  float  DacVlt;        // 06-09 DAC voltage
} __attribute__ ((packed))  Cmd_F0;

#define CMD_LEN_F0 8    //expected length of command message

typedef struct          // F0 response structure
{ BYTE   Ctype;         // 00 command type, returned unchanged
  BYTE   Rsp;           // 01 response code
  BYTE   Tid;           // 02 Task ID, returned unchanged
  BYTE   Chan;          // 03 Channel, returned unchanged
  float  DacVlt;        // 06-09 DAC voltage
} __attribute__ ((packed))  Rsp_F0;

#define RSP_LEN_F0 8    //length of response message

//********************* void Process_F0(UDPPacket * pUDP) **********************
//  pUDP  pointer to object of class UDPPacket, passed to function
//  pCmd  pointer to command structure (type Cmd_F0) set to start of UDP data
//  pRsp  pointer to response structure (type Rsp_F0) set to start of UDP data
//******************************************************************************

void Process_F0(UDPPacket * pUDP)
{
  PBYTE pUdpData = pUDP->GetDataBuffer();      //get pointer to UDP data field
  Cmd_F0 * pCmd = (Cmd_F0 *) pUdpData;         //set pointer for command data
  Rsp_F0 * pRsp = (Rsp_F0 *) pUdpData;         //set pointer for response data

  if (pUDP->GetDataSize() != CMD_LEN_F0)       //check for valid command length
  { pRsp->Rsp = INVALID_MSG_LENGTH;
  }
  else
  if (xil.ps_status & STATUS_PS_OFF)
  {
    float V = pCmd->DacVlt;                    //DAC voltage
    flip4((PBYTE) &V);                         //convert to big endian
    if (V < -10.0) V = -10.0;
    if (V >  10.0) V =  10.0;
    TmpDesADC = (DWORD)( V * (float)KADC);
    NewDesFlag = 1;
    flip4((PBYTE) &V);                         //convert to little endian
    pRsp->DacVlt = V;                          //set DAC voltage in response
    pRsp->Rsp = COMMAND_OK;                    //set response code
  }
  else
  {
    pRsp->Rsp = COMMAND_ERR;                   //set response code
  }
  pUDP->SetDataSize(RSP_LEN_F0);               //set size for response packet
}


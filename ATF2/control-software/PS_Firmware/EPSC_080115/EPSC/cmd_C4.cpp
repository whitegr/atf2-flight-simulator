//******************************************************************************
//  CLEAR FAULT LATCH (0xC4) (Ethernet Power Supply Controller)
//  Filename: cmd_C4.cpp
//  03/03/05
//  This command clears the fault latch set when the power supply is turned on.
//******************************************************************************

#include <C:\Nburn\EPSC\include\includes.h>    //Include all headers for project

typedef struct          // C4 command structure
{ BYTE   Ctype;         // 00 command type 0xC4
  BYTE   Rsp;           // 01 response code
  BYTE   Tid;           // 02 Task ID, set by sender, returned unchanged
  BYTE   Chan;          // 03 Channel, must be zero
} __attribute__ ((packed))  Cmd_C4;

#define CMD_LEN_C4  4   // expected length of command message

typedef struct          // C4 response structure
{ BYTE   Ctype;         // 00 command type 0xC4, returned unchanged
  BYTE   Rsp;           // 01 response code
  BYTE   Tid;           // 02 Task ID, returned unchanged
  BYTE   Chan;          // 03 Channel, returned unchanged
  WORD   Status01;      // 04-05 Status bytes 0 and 1
} __attribute__ ((packed))  Rsp_C4;

#define RSP_LEN_C4  6   // length of response message

//********************* void Process_C4(UDPPacket * pUDP) **********************
//  pUDP  pointer to object of class UDPPacket, passed to function
//  pCmd  pointer to command structure (type Cmd_C4) set to start of UDP data
//  pRsp  pointer to response structure (type Rsp_C4) set to start of UDP data
//******************************************************************************

void Process_C4(UDPPacket * pUDP)
{ BYTE errflag = NO_ERR;                       //error flag
  PBYTE pUdpData = pUDP->GetDataBuffer();      //get pointer to UDP data field
  Cmd_C4 * pCmd = (Cmd_C4 *) pUdpData;         //set pointer for command data
  Rsp_C4 * pRsp = (Rsp_C4 *) pUdpData;         //set pointer for response data

  if (pUDP->GetDataSize() != CMD_LEN_C4)       //check for valid command length
  { pRsp->Rsp = INVALID_MSG_LENGTH;
  }
  else if (pCmd->Chan !=0)                     //check for valid channel (0)
  { pRsp->Rsp = INVALID_CHAN_NUM;
  }
  else                                         //valid message structure
  {
    if (xil.ps_status & STATUS_LOCAL)          //check for local mode
    { errflag = ERR;
      WrErrMsg("C4 ERROR, PS IN LOCAL MODE");
    }
//  else if (xil.ps_status & STATUS_PS_OFF)    //inverted test 5/30/06
    else if (~xil.ps_status & STATUS_PS_OFF)   //check for power supply ON
    { errflag = ERR;
      WrErrMsg("C4 ERROR, PS ON");
    }
    else
    {
      xil.ps_cntl = PSCNTL_EN_OFF;             //set power supply enable off
      xil.ps_cntl = PSCNTL_RESET_ON;           //turn ON remote reset
      OSTimeDly(5);                            //delay 200-150 milliseconds
      xil.ps_cntl = PSCNTL_RESET_OFF;          //turn ON remote reset
    }
    pRsp->Rsp = COMMAND_OK;                    //set response code
    pRsp->Status01 = Status01(errflag);        //set status bytes 0 and 1
    pUDP->SetDataSize(RSP_LEN_C4);             //set response packet data size
  }
}


//******************************************************************************
//  Main Program (Ethernet Power Supply Controller)
//  Filename: main.cpp
//  Revisions
//  01/15/08 Changed current coefficient for PS modules (ModCur_CntToAmp)
//  11/20/07 Removed loop from soft reset, use with Xilinx 1.01
//  09/06/07 Test Version, no timeout on UART1 transmit
//  08/09/07 Added Telnet interface for diagnostics
//  07/31/07 Added PS serial port (UART1) for OCEM power supplies (KEK ATF2)
//  07/11/06 Release version, commented out UCOS_TASKLIST in predef.h
//  10/26/05 Version 198RC3 of Netburner Code
//******************************************************************************

#define _Globals_Flag                   //enable declaration of global variables 
#include <C:\Nburn\EPSC\include\includes.h>

const char * AppName =  "EPSC 01/15/08";      //Netburner program ID
const char * pFirmwareDate = "01/15/08";      //firmware version

extern "C" { void UserMain(void * pd); }

//********************************* START MAIN *********************************

void UserMain(void * pd)
{

  InitChipSelect3();
  xil.sys = SYS_IRQ5_OFF;                        //disable 240 hz interrupts

  EnableSmartTraps();                            //07/04/06 Added smarttraps

  HrdFlt = 0x0000;                               //clear hardware fault register
  InitXilinx();
  InitCosineTable();
  InitADCLinearityTables();

//****************************** INITIALIZE UART0 ******************************

  SetCtsRts();          //configure MFC5282 I/O for UART0/1 CTS/RTS lines
  SerialClose( 0 );

  fdcom0 = OpenSerial(0,                  //SERIALPORT_TO_USE
                      115200,             //BAUDRATE_TO_USE,
                      1,                  //STOP_BITS,
                      8,                  //DATA_BITS,
                      eParityNone );

//  UartHwFlow(0);      // Enable hardware flow control

  ReplaceStdio( 0, fdcom0 );
  ReplaceStdio( 1, fdcom0 );
  ReplaceStdio( 2, fdcom0 );

  printf("Program version %s", AppName);
  printf(", Xilinx version %04X\r\n", xil.ver_num);

  puts("********************************************************");
  puts("*                                                      *");
  puts("*  Includes OCEM PS serial link, new diagnostic menus  *");
  puts("*  and diagnostics over Telnet (TCP port 23)           *");
  puts("*                                                      *");
  puts("********************************************************");
 
//************************** Initialize TCP/IP stack ***************************

  ManEnetConfig = xil.db_eep.EnetConfig;         //set Ethernet configuration
  EnetFilterOff = 0;                             //enable ethernet filter
  ShowEthernet = 0;                              //disable show packet display

  InitializeStack((IPADDR) xil.db_eep.ip_adr,    //set IP address
                  (IPADDR) xil.db_eep.ip_mask,   //set IP mask
                  (IPADDR) xil.db_eep.ip_gate,   //set IP gateway
                  (IPADDR) xil.db_eep.ip_dns);   //set IP DNS

  if ( EthernetIP == 0 ) GetDHCPAddress();

  OSChangePrio(MAIN_PRIO);
  EnableAutoUpdate();

  OSSemInit(&ADC_TASK_SEM , 0);      //240hz interrupt semaphorefor ADC task
  OSSemInit(&ADC1_INST_SEM , 0);     //Command semaphore, ADC1 data request
  OSSemInit(&ADC2_INST_SEM , 0);     //Command semaphore, ADC2 data request
  OSSemInit(&ADC2_DIAG_SEM , 0);     //Diagnostic semaphore, ADC data request
  OSSemInit(&ADC2_TRIP_SEM , 0);     //PS Trip task semaphore, ADC data request
  OSSemInit(&START_TRIP_SEM , 0);    //Power Supply Trip Task semaphore

//  OSSemInit(&ADC2_UART1_SEM , 0);  //Diagnostic semaphore, ADC data request

  StartADCTask();
  StartTripTask();
  StartShowEnetTask();
  StartUDPTask();
  StartUdpDiagTask();
  StartFanTask();
  StartUart2Task();
  StartTelnetTask();                 // 08/10/07 Added Telnet Task

// If PS is OCEM multi-module, enable UART1 serial link.
// Requires ESPC PCB version 2.0 or higher

  ModCur_CntToAmp = 50.0 / 818.0;    //OCEM serial board ADC scale (amps/cnt)
                                     // 50A / ((4V / 5V) * 1023)
  if (PS_SERIAL_ENABLE)
  {
    StartUart1RcvTask();
    StartUart1XmtTask();
  }

  xil.WatchdogFlag = 0;
  Enable_IRQ5();
  xil.sys = SYS_IRQ5_ON;             //enable 240 hz interrupts

  OSTimeDly(1 * TICKS_PER_SECOND);   //delay 1 second

  spDiagnostic(fdcom0);              //diagnostic function, call never returns

  while (1)
  {
    OSTimeDly(1 * TICKS_PER_SECOND); //delay 1 second
  }
}


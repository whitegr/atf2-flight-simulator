//******************************************************************************
//  UART2 diagnostic task (Ethernet Power Supply Controller)
//  Filename: uart2.cpp
//  05/24/05
//
//  This task runs the same diagnostic code as UART0 but uses 9600 baud to be
//  comptable with the local control card. Only one UART at a time should be
//  used to prevent interaction in the code.
//  Functions that use standard I/O can only be seen on UART0.
//******************************************************************************

#include <C:\Nburn\EPSC\include\includes.h>    //Include all headers for project

//************************** void Uart2Main(void * pd **************************

void Uart2Main(void * pd)
{ pd=pd;

//  printf("UART 2 Diagnostic Task Started at Priority %d\r\n", UART2_PRIO);

  int fdcom2=OpenSerial(2,                  //SERIALPORT_TO_USE
                        9600,               //BAUDRATE_TO_USE,
                        1,                  //STOP_BITS,
                        8,                  //DATA_BITS,
                        eParityNone);

  SetUart2RxTx();            //set UART2 on CAN lines

  spDiagnostic(fdcom2);     //diagnostic function, call never returns
}

//****************************** StartUart2Task() ******************************

DWORD  Uart2Stk[USER_TASK_STK_SIZE] __attribute__(( aligned(16) ));

void StartUart2Task()
{ OSTaskCreate(Uart2Main,                       //address of task code
               (void *) 0,                      //optional argument pointer
               &Uart2Stk[USER_TASK_STK_SIZE],   //top of stack
               Uart2Stk,                        //bottom of stack
               UART2_PRIO);                     //task priority
}


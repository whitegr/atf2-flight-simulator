//******************************************************************************
//  Diagnostic Port Utilities
//  Filename: Network.cpp
//  10/28/05
//  08/10/07 Added spDumpTCBStacks (OSDumpTCBStacks()) and TaskIdStr(Prio)
//******************************************************************************

#include <C:\Nburn\EPSC\include\includes.h>    //Include all headers for project

//*********************** void spShowEthernet(int fd) **************************
//  Set flag for Ethernet packet display. Display done by ShowEnetTask.
//******************************************************************************

void spShowEthernet(int fd)
{
  char Char;
  char buf[64];
  int len;

  writestring(fd, "Enter Mode ( 1 = All, 2 = Passed, 3 = Dropped ), Mode = ");
  Char = spRdChar(fd, 0);                    //wait for character, no timeout
  if      ( Char < '1' ) Char = '1';
  else if ( Char > '3' ) Char = '1';

  len = sprintf(buf, "*** Ethernet Packet Display On, Mode = %c ***\r\n", Char);
  write(fd, buf, len);

  ShowEthernet = (WORD) (Char - '0');
  EnetFilterOff = 1;
  Char = 0;
  while (ShowEthernet != 0)
  {
    ReadWithTimeout(fd, & Char, 1, 20);
    if ( Char > 0 )
    {
      EnetFilterOff = 0;
      ShowEthernet = 0;
    }
  }
  writestring( fd, "*** Ethernet Packet Display Off ***\r\n" );
}

//************************** void spNetwork(int fd) ****************************
//  Read network status counters
//  defined in ethervars.h, put into includes.h 11/07/05 DJM
//  extern BOOL bEthLink;     // True for OK, False for no connection
//  extern BOOL bEthDuplex;   // True for full duplex, False for half duplex
//  extern BOOL bEth100Mb;    // True for 100Mb/sec, False for 10Mb/sec
//******************************************************************************

void spNetwork(int fd)
{
  char buf[64];
  int len;

  writestring(fd,"Ethernet Status:    ");
  if (!bEthLink)
    writestring(fd, "Link is Down" );
  else
  {
    if (bEth100Mb)
      writestring(fd, "100 Mb" );
    else
      writestring(fd, "10 Mb" );
    if (bEthDuplex )
      writestring(fd, " Full Duplex" );
    else
      writestring(fd, " Half Duplex" );
  }
  writestring(fd, "\r\n" );

  len = sprintf(buf, "0x%08lx%04lx      Ethernet MAC Address\r\n",
                     sim.fec.palr, sim.fec.paur/0x10000 );
  write(fd, buf, len);
  len = sprintf(buf, "0x%08lx%08lx  Individual Hash Table\r\n",
                     sim.fec.iaur, sim.fec.ialr );
  write(fd, buf, len);
  len = sprintf(buf, "0x%08lx%08lx  Group Hash Table\r\n",
                     sim.fec.gaur, sim.fec.galr );
  write(fd, buf, len);

  WORD MaxPack = (WORD)(sim.fec.rcr / 0x10000) & 0x07FF;
  WORD RecMode = (WORD)(sim.fec.rcr & 0x0000003F);
  len = sprintf(buf, "%4d  Max Size  %02x  Receive Mode\r\n", MaxPack, RecMode );
  write(fd, buf, len);

  writestring(fd,    "Network Managment Counters\r\n");
  writestring(fd,    " Transmit    Receive\r\n");
  len = sprintf(buf, "%9ld  %9ld  Total Packet Count\r\n", sim.fec_rmon_t.packets
                                                     , sim.fec_rmon_r.packets );
  write(fd, buf, len);
  len = sprintf(buf, "%9ld  %9ld  Broadcast\r\n", sim.fec_rmon_t.bc_pkt
                                              , sim.fec_rmon_r.bc_pkt );
  write(fd, buf, len);
  len = sprintf(buf, "%9ld  %9ld  Multicast\r\n", sim.fec_rmon_t.mc_pkt
                                              , sim.fec_rmon_r.mc_pkt );
  write(fd, buf, len);
  len = sprintf(buf, "%9ld  %9ld  64 Byte\r\n", sim.fec_rmon_t.p64
                                            , sim.fec_rmon_r.p64);
  write(fd, buf, len);
  len = sprintf(buf, "%9ld  %9ld  65-127 Byte\r\n", sim.fec_rmon_t.p65to127
                                                , sim.fec_rmon_r.p65to127 );
  write(fd, buf, len);
  len = sprintf(buf, "%9ld  %9ld  128-255 Byte\r\n", sim.fec_rmon_t.p128to255
                                                 , sim.fec_rmon_r.p128top255);
  write(fd, buf, len);
  len = sprintf(buf, "%9ld  %9ld  256-511 Byte\r\n", sim.fec_rmon_t.p256to511
                                                 , sim.fec_rmon_r.p256to511 );
  write(fd, buf, len);
  len = sprintf(buf, "%9ld  %9ld  512-1023 Byte\r\n", sim.fec_rmon_t.p512to1023
                                                  , sim.fec_rmon_r.p512to1023 );
  write(fd, buf, len);
  len = sprintf(buf, "%9ld  %9ld  1024-2047 Byte\r\n", sim.fec_rmon_t.p1024to2047
                                                   , sim.fec_rmon_r.p1024to2047 );
  write(fd, buf, len);
  len = sprintf(buf, "%9ld  %9ld  >2048 Byte\r\n", sim.fec_rmon_t.p_gte2048
                                               , sim.fec_rmon_r.p_gte2048 );
  write(fd, buf, len);
  len = sprintf(buf, "%9ld  %9ld  CRC/Align Error\r\n", sim.fec_rmon_t.crc_align
                                                    , sim.fec_rmon_r.crc_align );
  write(fd, buf, len);
  len = sprintf(buf, "%9ld  %9ld  Undersize Good CRC\r\n", sim.fec_rmon_t.undersize
                                                       , sim.fec_rmon_r.undersize );
  write(fd, buf, len);
  len = sprintf(buf, "%9ld  %9ld  Undersize Bad CRC\r\n", sim.fec_rmon_t.frag
                                                      , sim.fec_rmon_r.frag );
  write(fd, buf, len);
  len = sprintf(buf, "%9ld  %9ld  Oversize Good CRC\r\n", sim.fec_rmon_t.oversize
                                                      , sim.fec_rmon_r.oversize );
  write(fd, buf, len);
  len = sprintf(buf, "%9ld  %9ld  Oversize Bad CRC\r\n", sim.fec_rmon_t.jab
                                                     , sim.fec_rmon_r.jab );
  write(fd, buf, len);
  len = sprintf(buf, "%9ld  %9ld  Dropped\r\n", sim.fec_ieee_t.drop
                                            , sim.fec_ieee_r.drop );
  write(fd, buf, len);

  len = sprintf(buf, "%9ld  Collisions\r\n", sim.fec_rmon_t.col );
  write(fd, buf, len);

  len = sprintf(buf, "%9ld  Single Collisions\r\n", sim.fec_ieee_t.scol );
  write(fd, buf, len);

  len = sprintf(buf, "%9ld  Multiple Collisions\r\n", sim.fec_ieee_t.mcol );
  write(fd, buf, len);

  len = sprintf(buf, "%9ld  Late Collisions\r\n", sim.fec_ieee_t.lcol );
  write(fd, buf, len);
}

//*************************** void ShowStack(int fd) ***************************
// Show used portion of task stack. Note task fills from top down.
//******************************************************************************

void spShowStack(int fd)
{
  int i = 0, j = 0, TaskPriority = 0;
  char buf[128];
  int len = 1;

  writestring(fd, "Enter Task Priority = ");
  len = spRdStr(fd, buf, 31);
  if (len > 0) TaskPriority = atoi(buf);
  
  for ( i = 0; i < OS_MAX_TASKS; i++ )
  {
    if (( OSTCBTbl[i].OSTCBPrio ) == TaskPriority) break;
  }

  if ((i >= 0) && (i < OS_MAX_TASKS))
  {
    DWORD * pStack = (DWORD *) OSTCBTbl[i].OSTCBStkPtr;
    DWORD * pBot   = (DWORD *) OSTCBTbl[i].OSTCBStkBot;
    DWORD * pTop   = &pBot[USER_TASK_STK_SIZE];
    DWORD * pMin   = pBot;
    DWORD * pW     = pBot;

    int Cnt = 0;
    while (pMin < pTop)
    {
      if ( ( Cnt++ % 8 ) == 0 ) pW = pMin;
      if ( * pMin != 0xDEADBEEF ) break;
      ++pMin;
    }

    writestring(fd, "Use space key for more data, any other key to stop\r\n");
    len = sprintf(buf, " Bottom %08lx, Min %08lx, Pntr %08lx,  Top %08lx\r\n",
                        (long) pBot, (long) pMin, (long) pStack, (long) pTop);
    write(fd, buf, len);
    
    char Char = ' ';
    Cnt = 0;
    while ( ( pW < pTop ) && ( Char == ' ' ) )
    {
      len = sprintf(buf, "%07lx", (long) pW);
      write(fd, buf, len);
      {
        for (j=0; j < 8; ++j)
        {
          len = sprintf(buf, " %08lx", * pW++);
          write(fd, buf, len);
        }
        writestring(fd, "\r\n");
      }
      if ( (++Cnt % 32) == 0 )  Char = spRdChar(fd, 0);
    }
  }
}

//*********************** void spShowPoolBufData(int fd) ***********************
// Show the first 512 bytes of a pool buffer
//******************************************************************************

extern pool_buffer static_pool_buffers[BUFFER_POOL_SIZE];

void spShowPoolBufData(int fd)
{
  int i = 0, j = 0, Num = 0;
  char buf[128];
  int len = 0;

  writestring(fd, "Enter Pool Buffer Number = ");
  len = spRdStr(fd, buf, 31);
  if (len > 0) Num = atoi(buf);
  if (Num < 0) Num = 0;
  if (Num >= BUFFER_POOL_SIZE) Num = BUFFER_POOL_SIZE - 1;

  DWORD * pW = (DWORD *) & static_pool_buffers[Num];

  writestring(fd, "Use space key for more data, any other key to stop\r\n");
  char Char = ' ';
  while(Char == ' ')
  {
    for ( i = 0; i < 16; ++i)
    {
      len = sprintf(buf, "%07lX", (long) pW);
      write(fd, buf, len);
      for (j=0; j < 8; ++j)
      {
        len = sprintf(buf, " %08lX", * pW++);
        write(fd, buf, len);
      }
      writestring(fd, "\r\n");
    }
    Char = spRdChar(fd, 0);
  }
}

//************************* char * TaskIdStr(int Prio) *************************
//  for a task priority, return a pointer to a string (Task Name)
//******************************************************************************

char * TaskIdStr(int Prio)
{
  switch ( Prio )
  {
    case 0x3F:               return( "Idle   " ); break;
    case MAIN_PRIO:          return( "Main   " ); break;
    case HTTP_PRIO:          return( "HTTP   " ); break;
    case PPP_PRIO:           return( "PPP    " ); break;
    case TCP_PRIO:           return( "TCP    " ); break;
    case IP_PRIO:            return( "IP     " ); break;
    case ETHER_SEND_PRIO:    return( "Esend  " ); break;
    case ADC_PRIO:           return( "ADC    " ); break;
    case TRIP_PRIO:          return( "TRIP   " ); break;
    case UDP_PRIO:           return( "UDP    " ); break;
    case UDP_DIAG_PRIO:      return( "DIAG   " ); break;
    case FAN_PRIO:           return( "FAN    " ); break;
    case UART1_XMT_PRIO:     return( "UART1X " ); break;
    case UART1_RCV_PRIO:     return( "UART1R " ); break;
    case UART2_PRIO:         return( "UART2  " ); break;
    case SHOW_ENET_PRIO:     return( "ShowE  " ); break;
    case TELNET_PRIO:        return( "Telnet " ); break;
    default:                 return( "??     " );
  }
}
 
//************************* void spShowTasks( int fd ) *************************
//  Show Task Information
//  This task relies on the task having been suspended by UCOS. This leaves all
//  of the registers (16), flags, and program counter on the stack. Register A6
//  contains a the value of the stack pointer prior to the interrupt or call. By
//  using the value of A6, the functions works down the stack to find the return
//  value of the program counter for each call on the stack.
//  UCOSWAITS_HERE() is the return address for OSSchedule
//  OSTaskDelete should the last return address on the stack
//******************************************************************************

//extern void UCOSWAITS_HERE( void );

extern "C" {extern void UCOSWAITS_HERE(void);}

void ShowTCBState( int fd, OS_TCB *pTcb )
{
  char buf[128];
  int len;

  writestring(fd, TaskIdStr( pTcb->OSTCBPrio ) );
  len = sprintf(buf, "%02d ", pTcb->OSTCBPrio);
  write(fd, buf, len);

  if ( OSTCBCur->OSTCBPrio == pTcb->OSTCBPrio )
  {
    writestring(fd, "Run  ");
  }
  else
  {
    switch ( pTcb->OSTCBStat )
    {
      case OS_STAT_RDY:
        if ( pTcb->OSTCBDly == 0 )
        {
          writestring(fd, "Ready" );
        }
        else
        {
          writestring(fd, "Timer" );
        }  
        break;
      case OS_STAT_MBOX :
        writestring(fd, "Mbox " ); break;
      case OS_STAT_SEM  :
        writestring(fd, "Sema " ); break;
      case OS_STAT_Q    :
        writestring(fd, "Queue" ); break;
      case OS_STAT_FIFO :
        writestring(fd, "Fifo " ); break;
      case OS_STAT_CRIT :
        writestring(fd, "Crit " ); break;
      default:
        len = sprintf(buf, "%02d ", pTcb->OSTCBStat);
        write(fd, buf, len);
        break;
    }
  }

  if ( pTcb->OSTCBDly )
  {
    len = sprintf(buf, "%5i  ", pTcb->OSTCBDly );
    write(fd, buf, len);
  }
  else
  {
    writestring(fd, "   no  " );
  }

  DWORD Top = (4 * USER_TASK_STK_SIZE) + (DWORD) pTcb->OSTCBStkBot;
  DWORD Stack = (DWORD) pTcb->OSTCBStkPtr;
  DWORD OldA6 = *( ( DWORD * ) (Stack + 14 * 4) );
  DWORD NewPC = *( ( DWORD * ) (Stack + 16 * 4) );

  if ( ( OldA6 < (Stack + 14)) || ( OldA6 > (Top - 12)) ||
       ( NewPC < 0x02000000 ) || ( NewPC > 0x02100000 ) )
    writestring(fd, "???" );
  else
  {
    int n = 0;
    while ( n < 5 )
    {
      len = sprintf(buf, "%08lX", NewPC );
      write(fd, buf, len);

      if ( OldA6 == (Top - 12))
        break;
      else
        writestring(fd, "->" );

      Stack = OldA6;
      OldA6 = *( ( DWORD * ) ( OldA6 ) );
      if (( OldA6 < Stack ) || ( OldA6 > (Top - 12)) )
      {
        writestring(fd, "???" );
        break;
      }
      NewPC = *( ( DWORD * ) ( OldA6 + 4 ) );
      n++;
    }
  }
  writestring(fd, "\r\n" );
}

void spShowTasks( int fd )
{
  // Read all tasks into an index
  int index;
  int task[64];

  writestring(fd, "Task Prio State  Dly  Call Stack\r\n" );
  UCOS_ENTER_CRITICAL();
  for ( index = 0; index < 64; index++ )
  {
    task[index] = -1;
  }
  for ( index = 0; index < OS_MAX_TASKS; index++ )
  {
   int Prio = OSTCBTbl[index].OSTCBPrio;
   if (Prio > 0) task[Prio] = index;
  }
  for ( index = 0; index < 64; index++ )
  {
    if ( task[index] >= 0 ) 
    {
      ShowTCBState( fd, OSTCBTbl + task[index] );
    }
  }
  UCOS_EXIT_CRITICAL();

  char buf[128];
  int len = sprintf(buf, "\nTask Switch = %08lX  Task Delete = %08lX\r\n",
                         ( DWORD ) & UCOSWAITS_HERE,
                         ( DWORD ) & OSTaskDelete );
  write(fd, buf, len);
}

//************************* void spShowTasks( int fd ) *************************
//  Show Task Stack Usage Information
//******************************************************************************

void spDumpTCBStacks( int fd )
{

  char buf[128];
  int len;

  len = sprintf(buf, "Task Stacks, Size =%5d\r\n", 4 * USER_TASK_STK_SIZE);
  write(fd, buf, len);             
  writestring(fd, "Task     Prio  Stack Ptr    Bottom    Free     Min\r\n" );
 
  int index;
  int task[64];
  for ( index = 0; index < 64; index++ )
  {
    task[index] = -1;
  }
  for ( index = 0; index < OS_MAX_TASKS; index++ )
  {
   int Prio = OSTCBTbl[index].OSTCBPrio;
   if (Prio > 0) task[Prio] = index;
  }
  for ( int prio = 0; prio < 64; prio++ )
  {
    index = task[prio];
    if ( index >= 0 )
    {
      UCOS_ENTER_CRITICAL();
 
      int Prio     = OSTCBTbl[index].OSTCBPrio;
      DWORD StkPtr = (DWORD) OSTCBTbl[index].OSTCBStkPtr;
      DWORD StkBot = (DWORD) OSTCBTbl[index].OSTCBStkBot;
      DWORD FreeNow = StkPtr - StkBot;
 
      long n = 0;
      long * cp = OSTCBTbl[index].OSTCBStkBot;
      while ( ( * ( cp++ ) == (long) 0xDEADBEEF ) && ( n < 2500 ) ) n++;
      long FreeMin = 4 * n;

      UCOS_EXIT_CRITICAL();
      
      writestring(fd, TaskIdStr( Prio ) );
      len = sprintf(buf, "| %3i | %08lX | %08lX |%6ld |%6ld\r\n",
                        Prio, StkPtr, StkBot, FreeNow, FreeMin );
      write(fd, buf, len);             
    }
  }
}

//************************ void spShowBuffers( int fd ) ************************
// Show Buffer Information
//
// The buffer states that define the ownership of the buffer
// BO_UNUSED      0  The buffer is free
// BO_SOFTWARE    1  The buffer belongs to some software function
// BO_PRX         2  The buffer is to be used for the RX function
// BO_PTXF        3  The buffer is to be TXed and then freeded
// BO_PTXS        4  The buffer is to be TXed and then set to S/W owns
// BO_RDY4TX      5  The buffer belongs to the Ethernet transmit buffer
// BO_RXBUF       7  The buffer belongs to the Ethernet receive buffer
//
// The buffer state flags
// BS_PHY_BCAST    1  The packet is a physical layer broadcast address
// BS_IP_BCAST     2  The packet is a layer 3 broadcast address
// BS_IP_LOCAL_NET 4  The packet is to/from the local subnet
// BS_TCP_PUSH     8  The push flag was set on RX
// BS_PHY_802_3 0x10  The network wants 802_3 encapsulation
//
// Buffer state is set to point to the Ethernet Buffer Descriptor when the
//   buffer state is BO_RDY4TX or BO_RXBUF
//
// NUM_TX_BD (3)
// NUM_RX_BD (10)
//******************************************************************************

//Note: remove 'static' from declaration of static_pool_buffers in buffers.cpp
extern pool_buffer static_pool_buffers[BUFFER_POOL_SIZE];

void spShowBuffers( int fd )
{
  char buf[128];
  int len;

  for ( int i = 0; i < (BUFFER_POOL_SIZE / 2); i++ )
  {
    for ( int j = i; j < BUFFER_POOL_SIZE; j += (BUFFER_POOL_SIZE / 2))
    {
      len = sprintf(buf, "%2d %2d %2d",
                    j,
                    static_pool_buffers[j].bBuffer_state,
                    static_pool_buffers[j].bBufferFlag );
      write(fd, buf, len);
      len = sprintf(buf, " %08lX %08lX %08lX  ",
                    (DWORD) &static_pool_buffers[j],
                    (DWORD) static_pool_buffers[j].pPoolPrev,
                    (DWORD) static_pool_buffers[j].pPoolNext );
      write(fd, buf, len);
    }
    writestring(fd, "\r\n" );
  }
  len = sprintf(buf, "Free Buffer Count = %2d\r\n", GetFreeCount() );
  write(fd, buf, len);
}

//********************** void spShowEMACBuffers( int fd ) **********************
// Show Ethernet MAC Buffer Information
//
//  typedef struct
//  {
//    unsigned short flags;       Buffer status flags
//    unsigned short length;      Length of data
//    unsigned long  address;     Pointer to data area of buffer
//  } EtherBD;
//
//  NUM_TX_BD (3)
//  NUM_RX_BD (10)
//
//  EtherBD  TxBD[NUM_TX_BD]      Ethernet MAC transmit Buffer Descritors
//  PoolPtr  TxBDpp[NUM_TX_BD]    Transmit buffer pool pointers
//  BYTE     TxBDFlag[NUM_TX_BD]  Transmit buffer pool flags
//
//  EtherBD  RxBD[NUM_RX_BD]      Ethernet MAC receive Buffer Descritors
//  PoolPtr  RxBDpp[NUM_RX_BD]    Receive buffer pool pointers
//  BYTE     RxBDFlag[NUM_RX_BD]  Receive buffer pool flags
//
//******************************************************************************

void spShowEMACBuffers( int fd )
{
  char buf[128];
  int len;

  writestring(fd, "Ethernet MAC Transmit Buffers\r\n" );
  for ( int i = 0; i < NUM_TX_BD; i++ )
  {
    len = sprintf(buf, "%1d  %04x  %4d  %08lx  %04x  %08lX\r\n",
                  i,
                  TxBD[i].flags,
                  TxBD[i].length,
                  TxBD[i].address,
                  TxBDFlag[i],
                  (DWORD) TxBDpp[i] );
    write(fd, buf, len);
  }

  extern OS_SEM SemSend; //static removed from declaration ethernet.cpp 11/07/05
  OS_SEM * pSem = & SemSend;
  len = sprintf(buf, "Semaphore Count = %ld,  eir = %08lx,  eimr = %08lX\r\n",
                     pSem->OSSemCnt, sim.fec.eir, sim.fec.eimr );
  write(fd, buf, len);

  writestring(fd, "Ethernet MAC Receive Buffers\r\n" );
  for ( int i = 0; i < NUM_RX_BD; i++ )
  {
    len = sprintf(buf, "%1d  %04X  %4d  %08lX  %04X  %08lX\r\n",
                  i,
                  RxBD[i].flags,
                  RxBD[i].length,
                  RxBD[i].address,
                  RxBDFlag[i],
                  (DWORD) RxBDpp[i] );
    write(fd, buf, len);
  }
  writestring(fd, "Buf Num, Flags, Len, Data Ptr, Pool Flags, Buf Ptr\r\n" );
}

//************************** void spShowMII(int fd) ****************************
//  Read MII registers
//******************************************************************************

void spShowMII(int fd)
{
  const WORD Reg[16]= {0, 1, 2, 3, 4, 5, 6, 16, 17, 18, 19, 20, 21, 22, 23, 24};
  char buf[128];
  int len;

  writestring(fd, "MII REGISTERS\r\n" );

  for (int i = 0; i < 4; ++i)
  {
    for (int j = i; j < 16; j += 4)
    {
      UCOS_ENTER_CRITICAL();
      DWORD Dly = 0;
      sim.fec.eir = 0x00800000; 
      sim.fec.mdata = 0x60020000 + 0x00040000 * Reg[j];
      while (((sim.fec.eir & 0x00800000)==0) && (Dly < 5000000)) Dly++;
      len = sprintf(buf, "%2d = %04lX    ", Reg[j], sim.fec.mdata & 0x0000FFFF);
      write(fd, buf, len);
      UCOS_EXIT_CRITICAL();
    }
    writestring(fd, "\r\n");
  }
}

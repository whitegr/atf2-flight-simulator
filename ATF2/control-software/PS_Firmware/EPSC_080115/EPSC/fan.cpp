//******************************************************************************
//  Fan control task (Ethernet Power Supply Controller)
//  Filename: fan.cpp
//  09/07/05
//
//  This task uses the chassis temperature to regulate the chassis fan voltage.
//  The fan DAC is a 12 bit serial DAC that is set by writing xil.WrFanDac. It
//  varies the voltage to a 12VDC fan, with 0x0FFF equal to about 13 VDC.
//
//  The register xil.FanCnt counts the current pulses to the fan. There are four
//  pulses per revolution, so 
//  RPM = (pulse/sec) * 60(sec/min) / 4 (pulse/rev) = (pulse/sec) * 15
//
//  The fan operates from about 4VDC to 13.2VDC, so there is a minimum DAC setting.
//  The fan voltage is aproximately (DAC / 4095) * 13.24
//
//  The temperature readings come from a sensor with 10 mV/(deg F) sensitivity.
//  The ADC constant is (KADC) 1315860 counts per volt.
//
//  Temp < TLOW           Fan is set to MIN_FAN_DAC
//  TLOW < Temp < THIGH   DAC interpolated between MIN_FAN_DAC and MAX_FAN_DAC
//  THIGH < Temp          DAC set to MAX_FAN_DAC
//
//******************************************************************************

#include <C:\Nburn\EPSC\include\includes.h>    //Include all headers for project

#define  TLOW  (long)( 95 * KADC/100)  // Temperature where speed reaches minimum
#define  THIGH (long)(113 * KADC/100)  // Temperature where speed reaches maximum
// (THIGH - TLOW) must be less then 40 to prevent overflow

// DAC = (desired voltage) * (0xFFF) / 13.24 = (desired voltage) * 309.3

#define  VLT_TO_FAN_DAC      309.3     // conversion for volts to Fan DAC counts
#define  MAX_FAN_DAC  (WORD) 0x0FF3    // maximum value for fan DAC, 13.2 volts
#define  MIN_FAN_DAC  (WORD) 0x060A    // minimum value for fan DAC, 5.0 volts
#define  STALL_BOOST  (WORD) 0x0305    // boost voltage if fan stalls (2.5 volt)
#define  KFAN         (WORD) 15        // conversion pulse/sec to RPM

//******************************************************************************
//  Fan speed verses temperature control
//******************************************************************************

void SetFanDAC( void )
{
  LONG Temp = ADC2_Data_Reg[MUX2_TEMP];      //Get ADC2 temperature reading

  if (Temp >= THIGH)                         //Temp > THIGH
    FanDAC = MAX_FAN_DAC;                    //set DAC to maximum
  else                                       //Interpolation mode
  if (Temp > TLOW)                           //TLOW < TEMP < THIGH
  { FanDAC = MIN_FAN_DAC +                   //interpolate between MIN and MAX
     (WORD)(((Temp - TLOW) * (MAX_FAN_DAC - MIN_FAN_DAC)) / (THIGH - TLOW));
  }
  else                                       //Temp < TLOW
    FanDAC = MIN_FAN_DAC;

  if (FanRPM < 300)
  {
    FanDAC += STALL_BOOST;
    if (FanDAC > MAX_FAN_DAC) FanDAC = MAX_FAN_DAC;
  }

  xil.WrFanDac = FanDAC;                      //output value in FanDAC to DAC
}

//******************************************************************************
// Direct write of DC Fan Volts, for chassis test or diagnostic purposes
//******************************************************************************

float WrFanVlt( float Vlt )
{
  if ( Vlt < 0.0 )
  {
    FanTest = 0;                       //set to temperature control mode
    SetFanDAC( );
  }
  else
  {
    FanTest = 1;                       //set to constant voltage mode
    if ( Vlt > 15.0 ) Vlt = 15.0;
    FanDAC = (WORD)( Vlt * VLT_TO_FAN_DAC );

    if      ( FanDAC == 0 )          FanDAC = 0;
    else if ( FanDAC < MIN_FAN_DAC ) FanDAC = MIN_FAN_DAC;
    else if ( FanDAC > MAX_FAN_DAC ) FanDAC = MAX_FAN_DAC;
    xil.WrFanDac = FanDAC;
  }

  return ( (float) FanDAC / VLT_TO_FAN_DAC );
}

//*************************** void FanMain(void * pd ***************************
//  Execute once per second. Read Fan RPM and call fan speed control
//******************************************************************************

void FanMain(void * pd)
{ pd=pd;

//  printf("Chassis Fan Speed Control Task Started at Priority %d\r\n", FAN_PRIO);

  FanRPM = xil.FanCnt;                           //read count to reset
  FanDAC = MIN_FAN_DAC;                          //initialize FanDAC to minimum
  while (1)
  {
    OSTimeDly(TICKS_PER_SECOND);                 //delay one second
    FanRPM = xil.FanCnt * KFAN;
    if ( FanTest == 0 ) SetFanDAC();
  }
}

//******************************* StartFanTask() *******************************

DWORD  FanStk[USER_TASK_STK_SIZE] __attribute__(( aligned(4) ));

void StartFanTask()
{ OSTaskCreate(FanMain,                         //address of task code
               (void *) 0,                      //optional argument pointer
               &FanStk[USER_TASK_STK_SIZE],     //top of stack
               FanStk,                          //bottom of stack
               FAN_PRIO);                       //task priority
}


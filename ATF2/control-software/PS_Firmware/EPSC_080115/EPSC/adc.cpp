//******************************************************************************
//  ADC interface functions (Ethernet Power Supply Controller)
//  Filename: adc.cpp
//  09/08/05
//
// This module contains the functions to interface with the two ADCs. The 
// the Process_ADC1() and Process_ADC2() are called from the 240 hz interrupt.
//
//******************************************************************************

#include <C:\Nburn\EPSC\include\includes.h>    //Include all headers for project

LONG  ADC1_Data;              //ADC1 current data
WORD  ADC1_Data_Valid;        //ADC1 data valid flag, 0 = not valid
WORD  ADC1_Flt_Cnt;           //ADC1 fault count
WORD  ADC1_State;             //ADC1 state

LONG  ADC2_Data;              //ADC2 current data
LONG  ADC2_Data_Sum;          //ADC2 data, sum of first three conversions
WORD  ADC2_Data_Sum_Cnt;      //ADC2 data, count of conversions in sum
WORD  ADC2_Data_Valid;        //ADC2 data valid flag, 0 = not valid
WORD  ADC2_Flt_Cnt;           //ADC2 fault count
WORD  ADC2_State;             //ADC2 state

WORD  ADC1_PipeLineCnt = 0;   //Current ADC1 pipeline cycle (0-2)
WORD  ADC1_PipeLineProc[3];   //Current ADC1 pipeline data process select

WORD  ADC2_PipeLineCnt = 0;   //Current ADC2 pipeline cycle (0-2)
WORD  ADC2_PipeLineChan[3];   //Current ADC2 pipeline channel number
WORD  ADC2_PipeLineProc[3];   //Current ADC2 pipeline data process select

//**************************** Process_ADC1_Data() *****************************
//
// This function selects the process for the ADC data as set by Set_ADC1_Chan().
// The process is delayed by two cycles using the ADC1_PipelineProc array. If the
// data valid flag is false (0), no process is performed.
//
// The ADC data has already had the offset and gain compensation applied. The offset
// and gain states expect a null reading. Any error is added to ADC1_Offset and
// any gain error is added to ADC1_Gain. These variables are integrators which
// provide precise nulling of DC errors. The gain is set such that a one count
// adjustment in the gain or offset requires an integrated 16 counts of error.
// This minimizes the effect of ADC noise on the calibration variables.
//
// There are 8 different processes
//
// ADC1_NULL  Filling pipeline, do nothing
// ADC1_DAC   Data is from DAC. Compare reading to desired reading and add error
//            to xil.DAC_Offset. MAX_DAC_OFFSET defines the maximum +/- value.
// ADC1_REGX  Data is from regulated transductor to be used for digital regualtion.
// ADC1_IREQ  An instruction has requested a dedicated read cycle
// ADC1_OFF   The input of the ADC was grounded to read the offset voltage.
//            The reading is added to ADC1_Offset and range checked. 
// ADC1_GAIN  Data is from the reference. The reading is subtracted from the number
//            stored in the mother board EEPROM. The difference is added to the 
//            ADC1_Gain and range checked against ADC1_Gain.
// ADC1_FOFF  Same as ADC1_OFF except the error is muliplied by 8 to provide
//            faster response.
// ADC1_FGAIN Same as ADC1_GAIN except the error is muliplied by 8 to provide
//            faster response.
// 
//******************************************************************************

#define ADC1_NULL   0x0000  //Do nothing, data not valid
#define ADC1_OFF    0x0001  //Process as offset error nulling data
#define ADC1_GAIN   0x0002  //Process as gain error nulling data
#define ADC1_FOFF   0x0003  //Process as fast offset error nulling data
#define ADC1_FGAIN  0x0004  //Process as fast gain error nulling data
#define ADC1_IREQ   0x0005  //Process as instruction request
#define ADC1_DAC    0x0006  //Process as DAC data
#define ADC1_REGX   0x0007  //Process as regulated transductor data

void Process_ADC1_Data()
{ 
  if (ADC1_Data_Valid)
  { WORD ADC1_Process = ADC1_PipeLineProc[ADC1_PipeLineCnt];
    if (ADC1_Process >= ADC1_DAC)
    {
      ADC1_Hist_Ptr = (ADC1_Hist_Ptr + 1) % ADC1_HIST_SIZE;
      DAC_Hist_Data[ADC1_Hist_Ptr] = xil.Des_ADC;
      ADC1_Hist_Data[ADC1_Hist_Ptr] = ADC1_Data;

      if (ADC1_Process == ADC1_DAC)
      { long delta = (ADC1_Data - xil.Des_ADC);
        if (delta > 512) delta = 512;
        if (delta < -512) delta = -512;
        xil.DAC_Offset += delta;
        if ( Limit( (long *) &xil.DAC_Offset, MAX_DAC_OFFSET) )
          CalErr |= DAC_OFF_ERR;                 //set DAC offset error flag
        else
          CalErr &= ~DAC_OFF_ERR;                //clear DAC offset error flag
      }
      else if (ADC1_Process == ADC1_REGX)
      {
        ADC1_DREG_DATA = ADC1_Data;
        ADC1_DREG_FLAG = 1;
      }
    }
    else if (ADC1_Process == ADC1_IREQ)
    { if (ADC1_INST_REQ_FLAG == ADC1_REQ_CONV)
      { ADC1_INST_REQ_DATA = ADC1_Data;
        ADC1_INST_REQ_FLAG = ADC1_REQ_DONE;
        OSSemPost( & ADC1_INST_SEM );
      }
    }
    else if (ADC1_Process == ADC1_OFF)
    { ADC1_Offset += ADC1_Data;                  //add reading to ADC1 offset
      if (Limit(&ADC1_Offset, MAX_ADC_OFFSET))   //limit magnitude of ADC1_Offset
        CalErr |= ADC1_OFF_ERR;                  //set ADC1 offset error flag
      else
        CalErr &= ~ADC1_OFF_ERR;                 //clear ADC1 offset error flag
    }
    else if (ADC1_Process == ADC1_GAIN)
    { ADC1_Gain += AdcRef - ADC1_Data;
      if (Limit(&ADC1_Gain, MAX_ADC_GAIN))        //limit magnitude of ADC1_Gain
        CalErr |= ADC1_GAIN_ERR;                  //set ADC1 offset error flag
      else
        CalErr &= ~ADC1_GAIN_ERR;                 //clear ADC1 offset error flag
    }
    else if (ADC1_Process == ADC1_FOFF)
    { ADC1_Offset += 8 * ADC1_Data;
      if (Limit(&ADC1_Offset, MAX_ADC_OFFSET))   //limit magnitude of ADC1_Offset
        CalErr |= ADC1_OFF_ERR;                  //set ADC1 offset error flag
      else
        CalErr &= ~ADC1_OFF_ERR;                 //clear ADC1 offset error flag
    }
    else if (ADC1_Process == ADC1_FGAIN)
    { ADC1_Gain += 8 * (AdcRef - ADC1_Data);
      if (Limit(&ADC1_Gain, MAX_ADC_GAIN))       //limit magnitude of ADC1_Gain
        CalErr |= ADC1_GAIN_ERR;                 //set ADC1 offset error flag
      else
        CalErr &= ~ADC1_GAIN_ERR;                //clear ADC1 offset error flag
    }
  }
}

//**************************** Process_ADC2_Data() *****************************
//
// This function selects the process for the ADC data as set by Set_ADC2_Chan().
// The process is delayed by two cycles using the ADC1_PipelineProc array. If the
// data valid flag is false (0), no process is performed.
//
// The ADC data has already had the offset and gain compensation applied. The offset
// and gain states expect a null reading. Any error is added to ADC1_Offset and
// any gain error is added to ADC2_Gain. These variables are integrators which
// provide precise nulling of DC errors. The gain is set such that a one count
// adjustment in the gain or offset requires an integrated 16 counts of error.
// This minimizes the effect of ADC noise on the calibration variables.
//
// Calibration data uses only one reading. The data is used to null the gain and
// offset of the ADC circuit (ADC, differential amp, and muliplexer).

// All normal data (non calibration) data uses two readings averaged together
// to reduce noise and provide 60hz rejection. The channel number is used to
// write the data to one of 24 data registers. The data and channel number is also
// written circular 1024 reading history buffer.
//
// There are 12 different processes.
//
// ADC2_NULL  Filling pipeline, do nothing
// ADC2_OFF   The input of the ADC was grounded to read the offset voltage.
//            The reading is added to ADC1_Offset and range checked.
// ADC2_GAIN  Data is from the reference. The reading is subtracted from the number
//            stored in the mother board EEPROM. The difference is added to the
//            ADC1_Gain and range checked against ADC1_Gain.
// ADC2_FOFF  Same as ADC1_OFF except the error is muliplied by 8 to provide
//            faster response.
// ADC2_FGAIN Same as ADC1_GAIN except the error is muliplied by 8 to provide
//            faster response.
// ADC2_FRST  First of 4 samples, clear sum and store data
// ADC2_SUM   Second and third samples, add to sum
// ADC2_DAT4  Last of 4 samples, add to sum and divide by 4
// ADC2_DAT   Single sample data
// ADC2_IREQ  Instrustion task read request. Send ADC data to instruction task.
// ADC2_DREQ  Diagnostic task read request. Send ADC data to diagnostic task
// ADC2_TREQ  Trip task read request. Send ADC data to trip task
// ADC2_UREQ  UART1 task read request. Send ADC data to UART1 task
//
//******************************************************************************

#define ADC2_NULL   0x0000  //Do nothing, data not valid
#define ADC2_OFF    0x0001  //Process as offset error nulling data
#define ADC2_GAIN   0x0002  //Process as gain error nulling data
#define ADC2_FOFF   0x0003  //Process as fast offset error nulling data
#define ADC2_FGAIN  0x0004  //Process as fast gain error nulling data
#define ADC2_FRST   0x0005  //First of 4 samples, clear sum and store data
#define ADC2_SUM    0x0006  //Second and third samples, add to sum
#define ADC2_DAT    0x0007  //Process as ADC data
#define ADC2_IREQ   0x0008  //Process as instruction request
#define ADC2_DREQ   0x0009  //Process as UART0 or 2 diagnostic request
#define ADC2_TREQ   0x000A  //Process as trip task request
//#define ADC2_UREQ   0x000B  //Process as UART1 Task diagnostic request
#define ADC2_DAT4   0x0010  //4 samples flag, add to sum and divide by 4

void Process_ADC2_Data()
{
  WORD ADC2_Process = ADC2_PipeLineProc[ADC2_PipeLineCnt];
  WORD ADC2_Chan = ADC2_PipeLineChan[ADC2_PipeLineCnt];

  if (ADC2_Process & ADC2_DAT4)                  //check for 4 sample flag
  { if (++ADC2_Data_Sum_Cnt == 4)
      ADC2_Data = (ADC2_Data + ADC2_Data_Sum) / 4;
    else
    { ADC2_Data_Valid = 0;
      ADC2_Data_Sum_Cnt = 4;
    }
    ADC2_Process &= ~ADC2_DAT4;                  //clear 4 sample flag
  }

  if (ADC2_Data_Valid)
  {
    if (ADC2_Process >= ADC2_DAT)
    {
      ADC2_Data_Reg[ADC2_Chan] = ADC2_Data;

      ADC2_Hist_Ptr = (ADC2_Hist_Ptr + 1) % ADC2_HIST_SIZE;
      ADC2_Hist_Data[ADC2_Hist_Ptr] = ADC2_Data;
      ADC2_Hist_Chan[ADC2_Hist_Ptr] = ADC2_Chan;

      if (ADC2_Process == ADC2_IREQ)
      { if (ADC2_INST_REQ_FLAG == ADC2_REQ_CONV)
        { ADC2_INST_REQ_DATA = ADC2_Data;
          ADC2_INST_REQ_FLAG = ADC2_REQ_DONE;
          OSSemPost( &ADC2_INST_SEM );
        }
      }
      else if (ADC2_Process == ADC2_DREQ)
      { if (ADC2_DIAG_REQ_FLAG == ADC2_REQ_CONV)  
        { ADC2_DIAG_REQ_DATA = ADC2_Data;
          ADC2_DIAG_REQ_FLAG = ADC2_REQ_DONE;
          OSSemPost( &ADC2_DIAG_SEM );
        }
      }
      else if (ADC2_Process == ADC2_TREQ)
      { if (ADC2_TRIP_REQ_FLAG == ADC2_REQ_CONV)
        { ADC2_TRIP_REQ_DATA = ADC2_Data;
          ADC2_TRIP_REQ_FLAG = ADC2_REQ_DONE;
          OSSemPost( &ADC2_TRIP_SEM );
        }
      }
//      else if (ADC2_Process == ADC2_UREQ)
//      { if (ADC2_UART1_REQ_FLAG == ADC2_REQ_CONV)
//        { ADC2_UART1_REQ_DATA = ADC2_Data;
//          ADC2_UART1_REQ_FLAG = ADC2_REQ_DONE;
//          OSSemPost( &ADC2_UART1_SEM );
//        }
//      }
    }
    else if (ADC2_Process == ADC2_FRST)
    { ADC2_Data_Sum = ADC2_Data;
      ADC2_Data_Sum_Cnt = 1;
    }
    else if (ADC2_Process == ADC2_SUM)
    { ADC2_Data_Sum += ADC2_Data;
      ++ADC2_Data_Sum_Cnt;
    }
    else if (ADC2_Process == ADC2_OFF)
    { ADC2_Offset += ADC2_Data;
      if (Limit(&ADC2_Offset, MAX_ADC_OFFSET))   //limit magnitude of ADC1_Offset
        CalErr |= ADC2_OFF_ERR;                  //set ADC2 offset error flag
      else
        CalErr &= ~ADC2_OFF_ERR;                 //clear ADC2 offset error flag
    }
    else if (ADC2_Process == ADC2_GAIN)
    { ADC2_Gain += AdcRef - ADC2_Data;
      if (Limit(&ADC2_Gain, MAX_ADC_GAIN))       //limit magnitude of ADC2_Gain
        CalErr |= ADC2_GAIN_ERR;                 //set ADC2 offset error flag
      else
        CalErr &= ~ADC2_GAIN_ERR;                //clear ADC2 offset error flag
    }
    else if (ADC2_Process == ADC2_FOFF)
    { ADC2_Offset += 8 * ADC2_Data;
      if (Limit(&ADC2_Offset, MAX_ADC_OFFSET))   //limit magnitude of ADC1_Offset
        CalErr |= ADC2_OFF_ERR;                  //set ADC2 offset error flag
      else
        CalErr &= ~ADC2_OFF_ERR;                 //clear ADC2 offset error flag
    }
    else if (ADC2_Process == ADC2_FGAIN)
    { ADC2_Gain += 8 * (AdcRef - ADC2_Data);
      if (Limit(&ADC2_Gain, MAX_ADC_GAIN))       //limit magnitude of ADC2_Gain
        CalErr |= ADC2_GAIN_ERR;                 //set ADC2 offset error flag
      else
        CalErr &= ~ADC2_GAIN_ERR;                //clear ADC2 offset error flag
    }
  }
}

//************************ ResetADC1() and ResetADC2() *************************
//
// This function sets a bit in the xilinx that causes a special sequence to be 
// sent on the ADC serial clock line. When the ADC detects this sequence, it
// performs a self reset. This returns the device to the power on state. It must
// be initialized before it can be used in normal operation. The process requires
// 263 microseconds to complete. The "reset in process" bit is high during the
// sequence. No other commands should be sent ot the ADC during this period. 
//
//******************************************************************************

void ResetADC1()
{ ADC1_Offset = 0;                   //Set ADC1 offset to zero
  ADC1_Gain = 0;                     //Set ADC1 gain coefficent to zero
  ADC1_Flt_Cnt = 0;                  //Set ADC1 fault count to zero
  ADC1_Status = 0;                   //Set ADC1 status to not ready
  ADC1_PipeLineCnt = 0;              //Set ADC1 pipeline index to 0
  ADC1_PipeLineProc[0] = ADC1_NULL;  //Set ADC1 pipeline process[0] to NULL
  ADC1_PipeLineProc[1] = ADC1_NULL;  //Set ADC1 pipeline process[1] to NULL
  ADC1_PipeLineProc[2] = ADC1_NULL;  //Set ADC1 pipeline process[2] to NULL 
  xil.adc1_cntl = 0x8000 | 0x0090;   //set bit D15 (0x8000) to start reset
}                                    //set ADC1 mux to ground (0x0090) 

void ResetADC2()
{ ADC2_Offset = 0;                   //Set ADC1 offset to zero
  ADC2_Gain = 0;                     //Set ADC1 gain coefficent to zero
  ADC2_Flt_Cnt = 0;                  //Set ADC1 fault count to zero
  ADC2_Status = 0;                   //Set ADC1 status to not ready
  ADC2_PipeLineCnt = 0;              //Set ADC1 pipeline index to 0
  ADC2_PipeLineProc[0] = ADC2_NULL;  //Set ADC1 pipeline process[0] to NULL
  ADC2_PipeLineProc[1] = ADC2_NULL;  //Set ADC1 pipeline process[1] to NULL
  ADC2_PipeLineProc[2] = ADC2_NULL;  //Set ADC1 pipeline process[2] to NULL
  xil.adc2_cntl = 0x8000 | 0x0090;   //set bit D15 (0x8000) to start reset
}                                    //set ADC2 mux to ground (0x0090)

//******************* InitializeADC1() and InitializeADC2() ********************
//
// This function writes the configuration registers in the ADS1255 ADC. These
// control the conversion rate, data format, input selections, etc. See the Texas
// Instruments data sheet for device for detailed descriptions of the registers.
//
// Status register        (adr 0)     Reset value = x1h   Set to = 01h
// Input Multplexer       (adr 1)     Reset value = 01h   Set to = 10h
// A/D Control Register   (adr 2)     Reset value = 20h   Set to = 00h
// Data Rate              (adr 3)     Reset value = F0h   Set to = 92h 
// I/O Register           (adr 4)     Reset value = E0h
// Offset Calibration     (adr 7-5)   Set by ADC during self calibration
// Gain Calibration       (adr 10-8)  Set by ADC during self calibration
//
// All registers are byte wide and are read/write. The gain and offset calibration
// are 24 bit numbers stored in three registers, LSB (low adr) to MSB (high adr).
//
// The initialize function writes the first four registers in one transfer. The
// other registers are not changed during initialize.
//
// The write to register command is 0x50, address of first register is 0
// The second byte is 0x53, indicating 5+1 bytes total, 3+1 data bytes
//
//******************************************************************************

void InitializeADC1()
{ xil.adc1_wr34 = 0x0110;    //set the data to be written to registers 0 and 1
  xil.adc1_wr56 = 0x0092;    //set the data to be written to registers 2 and 3
  xil.adc1_wr12 = 0x5053;    //write command to start transmission
}

void InitializeADC2()
{ xil.adc2_wr34 = 0x0110;    //set the data to be written to registers 0 and 1
  xil.adc2_wr56 = 0x0092;    //set the data to be written to registers 2 and 3
  xil.adc2_wr12 = 0x5053;    //write command to start transmission
}

//******************** CalibrateADC1() and CalibrateADC1() *********************
//
// This function sens a self calibrate command (0xF0) to the ADC. The ADC then
// performs an internal gain and offset calibration procedure. The calibration
// requires 6.11 milliseconds. No other commands can be sent during this time.
// 
//******************************************************************************

void CalibrateADC1()
{ xil.adc1_wr12 = 0xF000;   //send self calibrate command to ADC1
}

void CalibrateADC2()
{ xil.adc2_wr12 = 0xF000;   //send self calibrate command to ADC2
}

//****************** SetADC1AutoMode() and SetADC2AutoMode() *******************
//
// The function sends the ADC a "read data continuously" command and puts the
// xilinx in auto mode. The ADC will automatically transmit data when the serial
// clock is applied after data valid is asserted. The xilinx will automatically 
// send 24 serial clocks to the ADC after it recieves a data valid signal from
// the ADC. The xilinx will read and sum two adc conversions and then wait for
// a new value to be written for the mulitplexer.
//
//******************************************************************************

void SetADC1AutoMode()
{ xil.adc1_wr12 = 0x0300;   //send read data continously command to ADC
  xil.adc1_cntl = 0x0100;   //set xilinx adc1 control to auto mode
}

void SetADC2AutoMode()
{ xil.adc2_wr12 = 0x0300;   //send read data continously command to ADC
  xil.adc2_cntl = 0x0100;   //set xilinx adc1 control to auto mode
}

//******************* SetADC1Mux(chan) and SetADC2Mux(chan) ********************
//
// The function sets the ADC muliplexer for the channel selected
//
// chan       channel number, 0-3 for ADC1 and 0-23 for ADC2
// D2-0       multiplexer address
// D4         multiplexer 0 enable (channels 0-7)
// D5         multiplexer 1 enable (channels 8-15)
// D6         multiplexer 2 enable (channels 16-23)
// D7         multiplexer write enable, must be set to 1
// D12        clear data register
//
//******************************************************************************

void SetADC1Mux(WORD chan)
{ if (chan > 3) chan = 0;                   //set chan to zero if out of range
  xil.adc1_cntl = 0x1090 + chan;            //set ADC1 muliplexer to "chan"
}

void SetADC2Mux(WORD chan)
{ WORD mux;
  if (chan > 23) chan = 0;                  //set chan to zero if out of range
  mux = chan % 8;                           //get multiplexer address (0-7)
  mux = mux + (0x0010 << (chan / 8) );      //add chip select to address
  xil.adc2_cntl = 0x1080 + mux;             //add write enable and write xilinx
}

//************************ Read_ADC1() and Read_ADC2() *************************
//
// This fuction reads the ADC data from the Xilinx register. It checks that two 
// conversions have been transfered. The data is adjusted for offset and gain and
// written to ADC1_Data.
//
// If two conversions stored ((xil.adcstat & 0x0003) = 0x0002)
//    read data from xilinx
//    subtract ADC offset error
//    add ADC gain compensation
//    save data
//    set data valid flag
//    if fault count is greater then zero, decrement fault count
//  else
//    set data valid flag to false
//    add 10 to fault count
//    if fault count is greater then 99, reset the ADC 
//
//******************************************************************************

void Read_ADC1()
{ if ((xil.adc_stat & 0x0003) == 0x0002)    //check that two conversions recieved
  {
    ADC1_Data = xil.adc1_data;              //read ADC1 data from Xilinx

    ADC1_Data -= ADC1_Offset                //subtract calibration offset
               / ADC_OFF_SCALE;             //divide offset by offset scale

    ADC1_Data += ADC1_LIN[ 512 + (ADC1_Data / 0x8000) ];

    //** use 64 bit (long long) math to calculate gain correction factor **
    ADC1_Data = ADC1_Data + LL_KMULT(ADC1_Data, ADC1_Gain, ADC_GAIN_SCALE);

    ADC1_Data_Valid = 1;                    //set data valid flag true
    if (ADC1_Flt_Cnt > 0) --ADC1_Flt_Cnt;   //if fault count > zero, subtract 1
  }
  else
  { ADC1_Data_Valid = 0;
    ADC1_Flt_Cnt += 10;                     //add 10 to ADC1 fault count
    if (ADC1_Flt_Cnt > 99) ADC1_State = 0;  //if fault count > 99, reset ADC
  }
}

void Read_ADC2()
{ if ((xil.adc_stat & 0x0300) == 0x0200)    //check that two conversions recieved
  { ADC2_Data = xil.adc2_data;              //read ADC2 data from Xilinx
    ADC2_Data -= ADC2_Offset                //subtract calibration offset
                 / ADC_OFF_SCALE;           //divide offset by offset scale

    ADC2_Data += ADC2_LIN[ 512 + (ADC2_Data / 0x8000) ];

    //** use 64 bit (long long) math to calculate gain correction factor **
    ADC2_Data = ADC2_Data + LL_KMULT(ADC2_Data, ADC2_Gain, ADC_GAIN_SCALE);

    ADC2_Data_Valid = 1;                    //set data valid flag true
    if (ADC2_Flt_Cnt > 0) --ADC2_Flt_Cnt;   //if fault count > zero, subtract 1
//    if (ADC1_Flt_Cnt > 0) --ADC1_Flt_Cnt;   //corrected 07/04/06 DJM
  }
  else
  { ADC2_Data_Valid = 0;
    ADC2_Flt_Cnt += 10;                     //add 10 to ADC1 fault count
    if (ADC2_Flt_Cnt > 99) ADC2_State = 0;  //if fault count > 99, reset ADC
  }
}

//****************************** Set_ADC1_Chan() *******************************
//
// This fuction schedules the channels to be read by ADC1. The channel is determined
// by the ADC cycle count. This continously counts from 0 to 3, changed at the end of
// each interrupt cycle. It is shared with ADC2 to synchronize the operation.
//
//   ADC1_Cyc == 0   set muliplexer to OFF (gnd), set process as offset error null
//   ADC1_Cyc == 1   set muliplexer to DAC, set process as DAC offset error null
//   ADC1_Cyc == 2   set muliplexer to REF, set process as gain error null
//   ADC1_Cyc == 3   set muliplexer to DAC, set process as DAC offset error null
//
// The three stage pipeline is required since the multiplexer channel set now is
// used by the ADC on the next cycle, and the data is read back in 2 cycles. 
// The process type written on this cycle will be used in 2 cycles to determine
// the function called to process the data.
//
// After the current channel and process are written, the pipeline index is
// incremented to point to the data from 2 cycles previous.
//
//******************************************************************************

void Set_ADC1_Chan()
{ if (ADC1_Cyc == 0)
  { ADC1_PipeLineProc[ADC1_PipeLineCnt] = ADC1_OFF;
    SetADC1Mux(MUX1_OFF);
  }
  else if (ADC1_Cyc == 2)
  { ADC1_PipeLineProc[ADC1_PipeLineCnt] = ADC1_GAIN;
    SetADC1Mux(MUX1_REF);
  }
  else   //ADC1_CYC is 1 or 3
  {
    if (ADC1_INST_REQ_FLAG == ADC1_REQ_ON)      //check for instruction request
    { ADC1_INST_REQ_FLAG = ADC1_REQ_CONV;
      ADC1_PipeLineProc[ADC1_PipeLineCnt] = ADC1_IREQ;
      SetADC1Mux(ADC1_INST_REQ_CHAN);
    }
    else if (xil.PsRegFlag == DIGITAL_REG)
    {
      ADC1_PipeLineProc[ADC1_PipeLineCnt] = ADC1_REGX;
      SetADC1Mux(MUX1_REG);
    }
    else  //read DAC voltage
    {
      ADC1_PipeLineProc[ADC1_PipeLineCnt] = ADC1_DAC;
      SetADC1Mux(MUX1_DAC);
    }
  }
  ADC1_PipeLineCnt = (ADC1_PipeLineCnt + 1) % 3;
}

//****************************** Set_ADC2_Chan() *******************************
//
// This fuction schedules the channels to be read by ADC2. There are three loops
// that determine the ADC channel. ADC2_Cyc runs at 240 hertz, looping through 8
// states at a 30 Hz rate
//
// ADC2_CYC continously counts from 0 to 7, changed at the end of each interrupt
// cycle. It is synchronized with ADC1_Cyc the operation.
//
//   ADC_Cyc == 0    set muliplexer to data, set process to ADC2_DAT
//   ADC_Cyc == 1    set muliplexer to gnd, set process as ADC2_OFF
//   ADC_Cyc == 2    set muliplexer to data, set process to ADC2_DAT
//   ADC_Cyc == 3    set muliplexer to REF, set process as ADC2_GAIN
//   ADC_Cyc == 4    set muliplexer to data, set process as ADC2_FRST
//   ADC_Cyc == 5    set muliplexer to data, set process as ADC2_SUM
//   ADC_Cyc == 6    set muliplexer to data, set process as ADC2_SUM
//   ADC_Cyc == 7    set muliplexer to data, set process as ADC2_DAT4
//
// The three stage pipeline is required since the multiplexer channel set now is
// used by the ADC on the next cycle, and the data is read back in 2 cycles. 
// The process type written on this cycle will be used in 2 cycles to determine
// the function called to process the data. After the current channel and process
// are written, the pipeline index is incremented to point to the data from 2
// cycles previous.  
// 
// The fast data uses two channel lookup tables, ADC2_FAST_TABLE and ADC_SLOW_TABLE.
// The fast table has 2 channels and the slow tables has 20 channels. Two channels
// from the fast table are read followed by one channel from the slow table. For
// a 60 hz data rate, the fast channels get 20 readings per second and the slow
// channels get 40 readings per second.
//
// The data is saved in 24 registers (ADC2_Data_Reg[24]). Each reading is also saved
// in a circular history buffer of the last 1024 readings (ADC2_Hist_Data[1024]).
// The last data written is pointed to by ADC2_Hist_Ptr.
//
//******************************************************************************

//******************** ADC2 4 SAMPLE CHANNEL NUMBER LOOKUP *********************
// Channels in this table are sampled 10 times per second (30 samples / sec) / 3

const WORD ADC2_4_SAMPLE_TABLE_SIZE = 3;
const WORD ADC2_4_SAMPLE_TABLE[3] = {MUX2_DAC,  MUX2_REG,  MUX2_AUX};


//******************* ADC2 FAST LOOP CHANNEL NUMBER LOOKUP  ********************
// Channels in this table are sampled 10 times per second (20 samples / sec) / 2

const WORD ADC2_FAST_TABLE_SIZE = 2;
const WORD ADC2_FAST_TABLE[2] = {MUX2_GND,  MUX2_PSV};

//******************* ADC2 SLOW LOOP CHANNEL NUMBER LOOKUP  ********************
// Channels in this table are sampled 2 times per second (40 samples / sec) / 20

const WORD ADC2_SLOW_TABLE_SIZE = 20;

const WORD ADC2_SLOW_TABLE[20]= {MUX2_OFF,   MUX2_REF,   MUX2_ARIP,  MUX2_TEMP, \
                                 MUX2_A10V,  MUX2_A5V,   MUX2_EXT,   MUX2_AGND, \
                                 MUX2_VP15U, MUX2_VM15U, MUX2_KLIX0, MUX2_KLIX1,\
                                 MUX2_VDD,   MUX2_V3D3,  MUX2_V2D5,  MUX2_V1D2, \
                                 MUX2_EXT,   MUX2_SPARE, MUX2_VP15R, MUX2_VM15R};

void Set_ADC2_Chan()
{ static WORD ADC2_Chan = MUX2_OFF;      //temp register for channel number
  static WORD ADC2_Proc = ADC2_NULL;     //temp register for process number
  static WORD ADC2_Proc4 = ADC2_NULL;    //temp register for 4 sample process number
  static WORD ADC2_Chan_Index = 0;       //channel index, 0 or 1 = fast, 2 = slow
  static WORD ADC2_4_Sample_Index = 0;   //4 sample index, 0 to 2
  static WORD ADC2_Fast_Index = 0;       //fast index, 0 to ADC2_FAST_TABLE_SIZE - 1
  static WORD ADC2_Slow_Index = 0;       //slow index, 0 to ADC2_SLOW_TABLE_SIZE - 1

  if ((ADC2_Cyc == 0) | (ADC2_Cyc == 2))
  { if (ADC2_INST_REQ_FLAG == ADC2_REQ_ON)        //check for instruction request
    { ADC2_INST_REQ_FLAG = ADC2_REQ_CONV;
      ADC2_Chan = ADC2_INST_REQ_CHAN;
      ADC2_Proc = ADC2_IREQ;
    }
    else if (ADC2_DIAG_REQ_FLAG == ADC2_REQ_ON)   //check for diagnostic request
    { ADC2_DIAG_REQ_FLAG = ADC2_REQ_CONV;
      ADC2_Chan = ADC2_DIAG_REQ_CHAN;
      ADC2_Proc = ADC2_DREQ;
    }
//    else if (ADC2_UART1_REQ_FLAG == ADC2_REQ_ON)   //check for diagnostic request
//    { ADC2_UART1_REQ_FLAG = ADC2_REQ_CONV;
//      ADC2_Chan = ADC2_UART1_REQ_CHAN;
//      ADC2_Proc = ADC2_UREQ;
//    }
    else if (ADC2_TRIP_REQ_FLAG == ADC2_REQ_ON)   //check for trip task request
    { ADC2_TRIP_REQ_FLAG = ADC2_REQ_CONV;
      ADC2_Chan = ADC2_TRIP_REQ_CHAN;
      ADC2_Proc = ADC2_TREQ;
    }
    else                                         //process fast and slow data
    { if (ADC2_Chan_Index == 0)
      { ADC2_Chan = ADC2_FAST_TABLE[ADC2_Fast_Index];
        ADC2_Fast_Index = (ADC2_Fast_Index + 1) % ADC2_FAST_TABLE_SIZE;
      }
      else
      { ADC2_Chan = ADC2_SLOW_TABLE[ADC2_Slow_Index];
        ADC2_Slow_Index = (ADC2_Slow_Index + 1) % ADC2_SLOW_TABLE_SIZE;
      }
      ADC2_Chan_Index = (ADC2_Chan_Index + 1) % 3;
      ADC2_Proc = ADC2_DAT;
    }
  }
  else if (ADC2_Cyc == 1)
  { ADC2_Proc = ADC2_GAIN;
    ADC2_Chan = MUX2_REF;
  }
  else if (ADC2_Cyc == 3)
  { ADC2_Proc = ADC2_OFF;
    ADC2_Chan = MUX2_OFF;
  }
  else if (ADC2_Cyc == 4)
  { if (ADC2_INST_REQ_FLAG == ADC2_REQ4_ON)       //check for instruction request
    { ADC2_INST_REQ_FLAG = ADC2_REQ_CONV;
      ADC2_Chan = ADC2_INST_REQ_CHAN;
      ADC2_Proc4 = ADC2_IREQ;
    }
    else if (ADC2_DIAG_REQ_FLAG == ADC2_REQ4_ON)  //check for diagnostic request
    { ADC2_DIAG_REQ_FLAG = ADC2_REQ_CONV;
      ADC2_Chan = ADC2_DIAG_REQ_CHAN;
      ADC2_Proc4 = ADC2_DREQ;
    }
//    else if (ADC2_UART1_REQ_FLAG == ADC2_REQ4_ON)  //check for diagnostic request
//    { ADC2_UART1_REQ_FLAG = ADC2_REQ_CONV;
//      ADC2_Chan = ADC2_UART1_REQ_CHAN;
//      ADC2_Proc4 = ADC2_UREQ;
//    }
    else if (ADC2_TRIP_REQ_FLAG == ADC2_REQ4_ON)  //check for trip task request
    { ADC2_TRIP_REQ_FLAG = ADC2_REQ_CONV;
      ADC2_Chan = ADC2_TRIP_REQ_CHAN;
      ADC2_Proc4 = ADC2_TREQ;
    }
    else                                         //process normal 4 sample data
    { ADC2_Chan = ADC2_4_SAMPLE_TABLE[ADC2_4_Sample_Index];
      ADC2_4_Sample_Index = (ADC2_4_Sample_Index + 1) % ADC2_4_SAMPLE_TABLE_SIZE;
      ADC2_Proc4 = ADC2_DAT;
    }
    ADC2_Proc = ADC2_FRST;
  }
  else if ((ADC2_Cyc == 5) | (ADC2_Cyc == 6))
    ADC2_Proc = ADC2_SUM;
  else if (ADC2_Cyc == 7)
    ADC2_Proc = ADC2_DAT4 + ADC2_Proc4;

  ADC2_PipeLineProc[ADC2_PipeLineCnt] = ADC2_Proc;
  ADC2_PipeLineChan[ADC2_PipeLineCnt] = ADC2_Chan;
  SetADC2Mux(ADC2_Chan);
  ADC2_PipeLineCnt = (ADC2_PipeLineCnt + 1) % 3;
}

//****************************** Process_ADC1() *******************************
//
// This function is called by the 240 hz interrupt.  
// This fuction initializes ADC1 using 10 states over 27 interrupt cycles.
//
// state 0   reset the ADC and initialize vraiables
// state 1   send initialization data to the ADC
// state 2   read initialization data from the ADC
// state 3   check initialization data, go to reset if not correct
// state 4   send self calibration request to the ADC
// state 5   delay state to allow calibration to complete
// state 6   the ADC and the Xilinx are set to auto read
// state 7   the ADC performs 10 fast offset null operations
// state 8   the ADC performs 10 fast gain error null operations
// state 9   normal operation 
//
// Normally the ADC is initialized and remains in state 9. The ADC will return to
// state 0 (reset) whenever the fault count reaches 100. Each read attempt that
// does not receive valid data adds 10 to the count. Valid data reads decrement
// the count until it reaches zero.
//
//******************************************************************************

void Process_ADC1()
{ static WORD cyc_cnt = 0;

  if (ADC1_State == 9)                    //Normal operation
  { Read_ADC1();
    Set_ADC1_Chan();
    Process_ADC1_Data();
  }
  else if (ADC1_State == 8)               //Fast gain error nulling
  { Read_ADC1();
    ADC1_PipeLineProc[ADC1_PipeLineCnt] = ADC1_FGAIN;
    SetADC1Mux(MUX1_REF);
    ADC1_PipeLineCnt = (ADC1_PipeLineCnt + 1) % 3;
    Process_ADC1_Data();
    if (++cyc_cnt > 10) ADC1_State = 9;
  }
  else if (ADC1_State == 7)               //Fast offset error nulling
  { Read_ADC1();
    ADC1_PipeLineProc[ADC1_PipeLineCnt] = ADC1_FOFF;
    SetADC1Mux(MUX1_OFF);
    ADC1_PipeLineCnt = (ADC1_PipeLineCnt + 1) % 3;
    Process_ADC1_Data();
    if (++cyc_cnt > 10)
    { ADC1_State = 8;
      cyc_cnt = 0;
    }
  }
  else if (ADC1_State == 6)                 //Set auto mode
  { SetADC1AutoMode();
    cyc_cnt = 0;
    ADC1_State = 7;
  }
  else if (ADC1_State == 5)                 //Delay one cycle
  { ADC1_State = 6;
  }
  else if (ADC1_State == 4)                 //Calibrate the ADC 
  { CalibrateADC1();
    ADC1_State = 5;
  }
  else if (ADC1_State == 3)            //Test if registers match configuration
  {
    static DWORD PrintErrFlag = 1;
    DWORD Reg123 = xil.adc1_data;
    if ( Reg123 == 0x100092 )
    {
      ADC1_State = 4;
      PrintErrFlag = 1;
    }
    else                               //Registers do not match configuration
    {
      if (PrintErrFlag == 1)           //If first failure, print error message
      {
        printf("ADC1 Failed Initialize, Reg 1 to 3 = %06lx\r\n", Reg123 );
        PrintErrFlag = 0;
      }
      ADC1_State = 0;                       //Restart ADC initialization
    }
  }
  else if (ADC1_State == 2)
  {
    xil.adc1_wr12 = 0x1192;                 //read registers 1,2,3
    ADC1_State = 3;
  }
  else if (ADC1_State == 1)                 //Initialize the ADC
  { InitializeADC1(); 
    ADC1_State = 2;
  }
  else
  { ResetADC1();                            //Reset the ADC
    ADC1_State = 1;
  }
}

//****************************** Process_ADC2() *******************************
//
// This function is called by the 240 hz interrupt.  
// This fuction initializes ADC2 using 10 states over 27 interrupt cycles.
//
// state 0   reset the ADC and initialize vraiables
// state 1   send initialization data to the ADC
// state 2   read initialization data from the ADC
// state 3   check initialization data, go to reset if not correct
// state 4   send self calibration request to the ADC
// state 5   delay state to allow calibration to complete
// state 6   the ADC and the Xilinx are set to auto read
// state 7   the ADC performs 10 fast offset null operations
// state 8   the ADC performs 10 fast gain error null operations
// state 9   normal operation 
//
// Normally the ADC is initialized and remains in state 9. The ADC will return to
// state 0 (reset) whenever the fault count reaches 100. Each read attempt that
// does not receive valid data adds 10 to the count. Valid data reads decrement
// the count until it reaches zero.
//
//******************************************************************************

void Process_ADC2()
{ static WORD cyc_cnt = 0;

  if (ADC2_State == 9)                    //Normal operation
  { Read_ADC2();
    Set_ADC2_Chan();
    Process_ADC2_Data();
  }
  else if (ADC2_State == 8)               //Fast gain error nulling
  { Read_ADC2();
    ADC2_PipeLineProc[ADC2_PipeLineCnt] = ADC2_FGAIN;
    SetADC2Mux(MUX2_REF);
    ADC2_PipeLineCnt = (ADC2_PipeLineCnt + 1) % 3;
    Process_ADC2_Data();
    if (++cyc_cnt > 10) ADC2_State = 9;
  }
  else if (ADC2_State == 7)               //Fast offset error nulling
  { Read_ADC2();
    ADC2_PipeLineProc[ADC2_PipeLineCnt] = ADC2_FOFF;
    SetADC2Mux(MUX2_OFF);
    ADC2_PipeLineCnt = (ADC2_PipeLineCnt + 1) % 3;
    Process_ADC2_Data();
    if (++cyc_cnt > 10)
    { ADC2_State = 8;
      cyc_cnt = 0;
    }
  }
  else if (ADC2_State == 6)                 //Set auto mode
  { SetADC2AutoMode();
    cyc_cnt = 0;
    ADC2_State = 7;
  }
  else if (ADC2_State == 5)                 //Delay one cycle
  { ADC2_State = 6;
  }
  else if (ADC2_State == 4)                 //Calibrate the ADC 
  { CalibrateADC2();
    ADC2_State = 5;
  }
  else if (ADC2_State == 3)            //Test if registers match configuration
  {
    static DWORD PrintErrFlag = 1;
    DWORD Reg123 = xil.adc2_data;
    if ( Reg123 == 0x100092 )
    {
      ADC2_State = 4;
      PrintErrFlag = 1;
    }
    else                               //Registers do not match configuration
    {
      if (PrintErrFlag == 1)           //If first failure, print error message
      {
        printf("ADC2 Failed Initialize, Reg 1 to 3 = %06lx\r\n", Reg123 );
        PrintErrFlag = 0;
      }
      ADC2_State = 0;                       //Restart ADC initialization
    }
  }
  else if (ADC2_State == 2)
  {
    xil.adc2_wr12 = 0x1192;                 //read registers 1,2,3
    ADC2_State = 3;
  }
  else if (ADC2_State == 1)                 //Initialize the ADC
  { InitializeADC2(); 
    ADC2_State = 2;
  }
  else
  { ResetADC2();                            //Reset the ADC
    ADC2_State = 1;
  }
}

//*********************** ADC LINEARITY CORRECTION DATA ************************
//  This is a lookup table for linearity ADC correction. The most significant 10
//  bits of the ADC reading are used as the offset into the looup table. The value
//  in the table is added to the ADC reading.
//  adc gain = 1383688 counts per volt. +/-12.125 volts full scale
//  0.0-3.0 volts, 512-639, ramp up to K1
//  3.0-6.6 volts, 639-791, ramp back to zero
//  6.6-7.0 volts, 791-807, zero for ADC gain calibration
//  7.0-10.0 volts, 807-934, ramp up to K2
//  10.0- 12.1 volts, 935-1023, continue slope of K2
//******************************************************************************

void InitADCLinearityTables()
{
  long a1k1 = (long)xil.mb_eep.a1k1;
  long a1k2 = (long)xil.mb_eep.a1k2;
  long a2k1 = (long)xil.mb_eep.a2k1;
  long a2k2 = (long)xil.mb_eep.a2k2;

  for(int i = 0; i <= 127; ++i)  //512 to 639, delta 127
  {
    ADC1_LIN[512 + i] = (a1k1 * i) / 127;
    ADC2_LIN[512 + i] = (a2k1 * i) / 127;
  }
  for(int i = 1; i <= 152; ++i)  //639 to 791, delta 152
  {
    ADC1_LIN[639 + i] = a1k1 - (a1k1 * i) / 152;
    ADC2_LIN[639 + i] = a2k1 - (a2k1 * i) / 152;
  }
  for(int i = 792; i <= 807; ++i)  //792 to 807
  {
    ADC1_LIN[i] = 0;
    ADC2_LIN[i] = 0;
  }
  for(int i = 1; i <= 216; ++i)  //807 to 1023, delta 216
  {
    ADC1_LIN[807 + i] = (a1k2 * i) / 127;
    ADC2_LIN[807 + i] = (a2k2 * i) / 127;
  }
  for(int i = 0; i <= 511; ++i)  //transfer negative image to 0 to 511
  {
    ADC1_LIN[i] = -ADC1_LIN[1023 - i];
    ADC2_LIN[i] = -ADC2_LIN[1023 - i];
  }
}

//******************************************************************************
// Overview of ADC operation
//
// The EPSC uses two TI ADS1255 24 bit delta-sigma ADCs. ADC1 is dedicated to
// regulation of either the DAC voltage or regulated transductor voltage. ADC2
// is used for reading the status of the controller. Both units are sychronized
// to the 240 hertz interrupt.
//
// The 240 hz interrupt signals that new data is available from the ADC and
// starts a new conversion. The ADC is programmed to provide 500 conversions per
// per second, or two conversions in each 240 Hz period. At the end of the
// first conversion, the data is read from the ADC into an accumulator. After
// the second conversion, the data is read from the ADC and added to the first
// conversion to provide a 25 bit signed result. The multiplexer is set to the
// next value (set at the last 240 hz interrupt) to allow the ADC input to settle
// before the start of the next conversion.
//
// There are registers for programming the ADC and for reading the conversion
// data. There are 8 registers used to configure the two ADCs. The four registers
// for ADC1 are listed below.
//
// xil.adc1_cntl   ADC1 control register
//         D2-0    Multiplexer address bits A2-A0
//         D6-4    Multiplexer chip enables EN2, EN1, EN0
//         D7      Write multiplexer data (no change if D7 low)
//         D8      Enable Auto Mode
//         D9      Disable Auto Mode
//         D12     Clear data register
//         D15     Reset ADC using special serial data clock pattern 
// xil.adc1_wr12   ADC1 serial data bytes 1 and 2
//         D15-8   ADC command byte (sent as first byte to ADC)
//         D6-4    Message byte count (1 to 6 bytes)
//         D1-0    Byte count for ADC (sent in second byte to ADC)
// xil.adc1_wr34   ADC1 serial data bytes 3 and 4 
// xil.adc1_wr56   ADC1 serial data bytes 5 and 6
//
// There is a duplicate set for ADC2
//
// xil.adc2_cntl   ADC2 control register
// xil.adc2_wr12   ADC2 serial data bytes 1 and 2
// xil.adc2_wr34   ADC2 serial data bytes 3 and 4 
// xil.adc2_wr56   ADC2 serial data bytes 5 and 6
//
// When the adc*_wr12 register is written, a command is sent to the ADC. The 
// number of bytes sent is specified by D6-4 plus one. Messages may be a single
// byte or include 1 to 4 data bytes with a length byte. The command is in the 
// upper 8 bits of wr12. It is always sent unaltered. If data is transferred,
// the second byte sent is set to the value of D1-0 with the upper 6 bits set
// to zero. This specifies to the ADC the number of data bytes (plus one) to
// follow. A value of zero for one data byte or three for four data bytes.
//
// D6-4   D1-0
//   0      *      Send a one byte command, (only byte 1 sent, D1-0 ignored)
//   2      0      Send a command with one byte of data (bytes 1 to 3 sent)
//   5      3      Send a command with four bytes of data (bytes 1 to 6 sent)
// 
// Bits D6-4 are used by the xilinx to set the number of bytes to send. Bits D1-0
// are used by the ADC to determine the number of data bytes to transfer.
//
// The data registers (wr34 and wr56) are sent unaltered to the ADC. Byte 3 are
// the upper 8 bits of the register and byte 4 are the lower 8 bits of the register.
// Zero to four bytes will be included depending on the length set by D6-4.
//
// Setting auto mode (D8) allows the xilinx to automatically transfer readings
// from the ADC whenever the ADC data ready is valid. Two readings are summed
// together to form a 25 bit signed result after every sync pulse (240 hz).
//
// The clear data bit (D12) clears the recieved data accumulator and the receive
// count flags.
//
// The reset bit (D15) sends a special sequence on the ADC serial clock to cause
// a reset of the ADC. It also clears auto mode.
//
// There are registers for reading the ADC status and data. There is one status
// register that provides the state of both ADC1 and ADC2. Each ADC has a 32 bit
// data register.
//
// xil.adc_stat    ADC status register
//         D1-0    ADC1 receive count
//         D3-2    Set to zero
//         D4      ADC1 auto mode enabled
//         D5      ADC1 data ready line (diagnostic)
//         D6      ADC1 transmission in progress
//         D7      ADC1 reset in progress
//         D9-8    ADC2 receive count
//         D11-10  Set to zero
//         D12     ADC2 auto mode enabled
//         D13     ADC2 data ready line (diagnostic)
//         D14     ADC2 transmission in progress
//         D15     ADC2 reset in progress
//
// xil.adc1_data   ADC1 data register, 32 bit
// xil.adc2_data   ADC2 data register, 32 bit
//
// On each 240 hz interrupt, the processor reads the ADC data. The receive count
// must equal 2 for valid data, indicating two ADC conversions were transferred
// and sumed. The processor then writes new multiplexer data and clears the receive
// count.
//
// The muliplexer data is not transferred to the muliplexer until the completion of
// the conversion that is already in progress. The data for this muliplexer setting
// is converted by the ADC after the next 240 hz interrupt and is read back on the
// following 240 hz interrupt. There is a two stage delay between setting the 
// muliplexer and reading the data assocciated with that setting.
//  
// The ADC clock is divided down from the 66.3552 Mhz clock from the 5282 processor
// board. Dividing by eight yields a 8.2944 Mhz clock. The ADS1255 command assume
// a 7.68 Mhz clock. Setting the ADC for 500 samples per second results in a rate
// of 540 samples per second, or 1.85 millecond conversion.
//
// The 240 hz interrupt provides a 4.17 millisecond period. The ADC performs two
// conversions in 3.70 milisecons, leaving 0.47 millisecons settling time after
// the muliplexer is changed.
//
// A complete conversion by the ADC requires four cycles of 240 hz, or a 60 hz
// overall reading rate. The second and forth cycles read data and average the
// result to provide 60 hz rejection. The first and third cycles read ground and
// the reference to allow nulling of offset and gain errors in the ADC.
//
//******************************************************************************


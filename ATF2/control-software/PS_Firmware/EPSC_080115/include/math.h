//******************************************************************************
//  INCLUDES FILES FOR EPSC (Ethernet Power Supply Controller)
//  Filename: math.h
//  02/08/05
//
//******************************************************************************

//******************************* LL_KMULT(a,b,c) ******************************
//
// last revised: 02/08/05
//
// This is a macro fuction to perform coefficient multiplies (a*b)/c on long
// (32 bit) numbers with no loss of accuracy. The numbers are cast to (64 bit)
// long long numbers to allow multiplication without overflow and the result is
// recast to a long (32 bit) number.
//
//******************************************************************************

#define LL_KMULT(a,b,c) (LONG)(((long long)(a) * (long long)(b)) / (long long)(c))


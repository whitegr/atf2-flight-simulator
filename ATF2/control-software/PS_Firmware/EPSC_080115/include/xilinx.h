//*****************************************************************************
//  XILINX FPGA HARDWARE MAP (Ethernet Power Supply Controller)
//  Filename: GLOBAL_VARIABLES.H
//  02/28/05
//  *NOTE* "InitChipSelect3()" in "init5282" sets Xilinx address to 0xA0000000
//  and address of structure "xil" must be set in "sys.ld" to match
//
// 02/25/05 Found __attribute__ ((packed)) caused read and write errors to the
// xilinx. Appears to be writing a byte at a time which is not supported.
//
//*****************************************************************************

//*****************************************************************************
//  XILINX MEMORY MAP
//  The Xilinx Spartan 3 FPGA is mapped to 1024 words (16 bit) in memory. The
//  first 32 word addresses are for reading and writing hardware registers. All
//  other address are for static RAM in the Xilinx.
//
//  Writes (WORD offset from base address read data is undefined)
//  0        Front panel LEDS and enable Interrupt
//  1        Power Supply Configuration and ON/OFF Control
//  2        Serial EEPROM command register
//  3        Serial EEPROM data register
//  4-5      DAC data (24 bit signed integer, bits 24-31 ignored)
//  6        System, interrupt enables, resets, and watchdog timer
//  7        Fan speed control DAC register
//  8        ADC1 control register
//  9-11     ADC1 serial data registers (6 bytes)
//  12       ADC2 control register
//  13-15    ADC2 serial data registers (6 bytes)
//
//  Read (WORD offset from base address, writes have no effect)
//  16       Power Supply Status
//  17       ADC1 and ADC2 status register
//  18-19    ADC1 data (32 bit signed)
//  20-21    ADC2 data (32 bit signed)
//  22       Serial Status high word (local control and hardware ramp)
//  23       Serial Status low word (interlocks)
//  24       Serial Status low word, latched after PS turn ON
//  25       Serial Status low word, latched after PS trip
//  26       Latched Interlock Status for Ethernet messages
//  27       Xilinx Version number
//  28       Serial EEPROM status register
//  29       Serial EEPROM read data register
//  30       Fan Pulse Count Register
//  31       Not used (reads RAM address 15, but cannot be written)
//
//  32-1023  Static RAM read/write
//
//*****************************************************************************

//************************ Xilinx EEPROM data image ***************************
// Data is transferred from mother board and daughter board EEPROMs at startup

typedef struct            // mother board EEPROM structure (32 words)
{ VWORD   SerialNum[4];   // 00->03 chassis serial number, 8 characters
  VWORD   CalDate[4];     // 04->07 calibration date, 8 characters
  float   ref;            // 08->09 reference voltage (LM399A)
  VSHORT  a1k1;           // 10     ADC1 linearity coefficient K1
  VSHORT  a1k2;           // 11     ADC1 linearity coefficient K2
  VSHORT  a2k1;           // 12     ADC2 linearity coefficient K1
  VSHORT  a2k2;           // 13     ADC2 linearity coefficient K2
  VWORD   pack00[17];     // 14->30 packing
  VWORD   CheckSum;       // 31     structure checksum, set for 0xA5A5
} mb_eep_struct;

typedef struct            // daughter board EEPROM structure (32 words)
{ VDWORD  ip_adr;         // 00->01 Ethernet IP address
  VDWORD  ip_mask;        // 02->03 Ethernet IP mask
  VDWORD  ip_gate;        // 04->05 Ethernet IP gateway
  VDWORD  ip_dns;         // 06->07 Ethernet IP Domain Name Server
  float   reg_iva;        // 08->09 regulated transductor volts/amp
  float   aux_iva;        // 10->11 auxiliary transductor volts/amp
  float   gnd_iva;        // 12->13 ground current readback volts/amp
  float   psv_iva;        // 14->15 power supply voltage readback volts/volt
  VWORD   MagnetId[4];    // 16->19 magnet identification, 8 characters
  VWORD   Config;         // 20     chassis configuration flags
  VWORD   EnetConfig;     // 21     Ethernet configuration
  VWORD   DigitalGain;    // 22     Digital Regulation Gain, 16 bit float
  VWORD   DigitalTC;      // 23     Digital Reg time constant, 16 bit float
  VWORD   DigitalErrLim;  // 24     Digital Reg error voltage limit, 16 bit float
  VWORD   pack00[6];      // 25->30 packing
  VWORD   CheckSum;       // 31     structure checksum, set for 0x5A5A
} db_eep_struct;

//**************************** Xilinx Structure *******************************
// This is a type define only. Structure declared in "sys.ld" 

typedef struct
{                         // 0x0000-0x001F *** write only registers ***
  VWORD   leds;           // 0x0000 Front panel LED register
  VWORD   ps_cntl;        // 0x0002 Power supply control register
  VWORD   eep_wrdat;      // 0x0004 EEPROM write data register
  VWORD   eep_cmd;        // 0x0006 EEPROM instruction register
  VLONG   dac;            // 0x0008 DAC data register (32 bit signed)
  VWORD   sys;            // 0x000C System register
  VWORD   WrFanDac;       // 0x000E Fan DAC register
  VWORD   adc1_cntl;      // 0x0010 ADC1 control register
  VWORD   adc1_wr12;      // 0x0012 ADC1 write data bytes 1 and 2
  VWORD   adc1_wr34;      // 0x0014 ADC1 write data bytes 3 and 4
  VWORD   adc1_wr56;      // 0x0016 ADC1 write data bytes 5 and 6
  VWORD   adc2_cntl;      // 0x0018 ADC2 control register
  VWORD   adc2_wr12;      // 0x001A ADC2 write data bytes 1 and 2
  VWORD   adc2_wr34;      // 0x001C ADC2 write data bytes 3 and 4
  VWORD   adc2_wr56;      // 0x001E ADC2 write data bytes 5 and 6

                          // 0x0020-0x003F *** read only registers ***
  VWORD   ps_status;      // 0x0020 power supply status
  VWORD   adc_stat;       // 0x0022 adc status register
  VLONG   adc1_data;      // 0x0024 adc1 data register (32 bit signed)
  VLONG   adc2_data;      // 0x0028 adc2 data register (32 bit signed)
  VWORD   ser_stath;      // 0x002C serial status high word register
  VWORD   ser_statl;      // 0x002E serial status low word register
  VWORD   ser_statl_lch0; // 0x0030 serial status low, latched at PS turn on
  VWORD   ser_statl_lch1; // 0x0032 serial status low, latched after PS trip
  VWORD   intl_stat;      // 0x0034 latched interlock status
  VWORD   ver_num;        // 0x0036 version number   
  VWORD   eep_stat;       // 0x0038 serial EEPROM status register
  VWORD   eep_rddat;      // 0x003A serial EEPROM read data register
  VWORD   FanCnt;         // 0x003C fan pulse count register
  VWORD   pack00;         // 0x003E not avaialable

                          // 0x0040-0x07FF Static RAM in Xilinx
  mb_eep_struct  mb_eep;  // 0x0040-0x007F mother board EEPROM data
  db_eep_struct  db_eep;  // 0x0080-0x00BF daughter board EEPROM data

  //Xilinx SRAM data, uneffected by processor reset, zeroed by Xilinx reset

  WORD    xil_status;     // 0x00C0 Status of Xilinx, set to 0 by xilinx reset

//watchdog timer flags

  WORD    WatchdogFlag;
#define   UDPWatchdogFlag     (0x0001)      //UDP Task flag
#define   ForceWatchdogFlag   (0x0002)      //Force Watchdog Reset flag

  //Current DAC setting and ramp status

  LONG    Des_ADC;        // Desired ADC reading for regulated transductor
  LONG    Des_DAC;        // Desired DAC setpoint for DES_ADC
  LONG    DAC_Offset;     // DAC offset count

  //Ramp Status, times, and setpoints

  WORD    Ramp_State;     // Ramp State(BYTE value)
  WORD    Cur_Ramp_Seg;   // Current segement number (BYTE value)
  DWORD   Cur_Ramp_time;  // Current time for current ramp segment
  WORD    Num_Ramp_Seg;   // Number of segments in current ramp (1-5)
  DWORD   Ramp_Times[6];  // Ramp Times, 1 to 5 segments, [0]=time remaining
  LONG    ADC_SetPnt[6];  // Ramp ADC Setpoints, [0]=start value

  //Special status registers, only lower byte used by 0xCA command

  WORD    ResetType;                 //Reset type register
  WORD    LastReset;                 //Last reset code
#define   POWER_ON_RESET  (0x0001)   //Power On reset
#define   SOFTWARE_RESET  (0x0002)   //Software reset
#define   UART_RESET      (0x0004)   //UART (Diagnostic) reset
#define   LOCAL_RESET     (0x0008)   //Local control reset
#define   WATCHDOG_RESET  (0x0010)   //Watchdog reset
#define   NETBURNER_RESET (0x0080)   //Probable Netburner reset 08/10/07
                                     //No source could be determined

  WORD    LastOff;            // last PS turn off code

  //Power supply ON status, used to detect trips
  WORD    PsOnFlag;

  //PS regulation status flag set by ON/OFF commands, enables regulation loops
  WORD    PsRegFlag;

  //Digital Regulation variables and coefficients

  long       DigGain;         //Digital regulation gain * 200 * KDAC / KADC
  long       ErrLimit;        //error limit (ADC counts)
  long long  ErrSum;          //error sum (integrated error ADC counts)
  long long  ErrSumGain;      //error sum gain coefficient
  long       ErrSumMax;       //maimum value of error sum (upper word)
  long       ErrSumMin;       //minimum value of error sum (upper word)

  //Test Registers

  WORD    Test[16];           //16 word Xilinx write/read back test area

} xil_struct;

extern volatile xil_struct  xil;

//************************** Regulation Enable Flags ***************************
//  PsOnFlag shows power supply ON status set by ON/OFF commands
//  Used to detect power supply trips
//******************************************************************************

#define PS_OFF          0x0000      //Power Supply OFF
#define PS_ON           0x0001      //Power Supply ON

//************************** Regulation Enable Flags ***************************
//  PsRegFlag shows regulation status set by ON/OFF commands
//******************************************************************************

#define REG_OFF         0x0000      //regulation off
#define ANALOG_REG      0x0001      //analog regulation active
#define DIGITAL_REG     0x0002      //digital regulation active

//****************************** STATUS REGISTER *******************************
//  The status register contains various status flags. The layout corresponds to
//  status bytes 0 and 1 in UDP response messages.The bits marked "**" are set
//  by software for UDP status.
//******************************************************************************

#define STATUS_LOCAL      0x8000    //local mode
#define STATUS_REV_POL    0x4000    //reverse polarity ON
#define STATUS_HOLD       0x2000    //ramp HOLD, waiting for harwdware ramp
#define STATUS_RAMP_ON    0x1800    //ramp ON, either ramp or setpoint
#define STATUS_PS_OFF     0x0400    //power supply off
#define STATUS_CMD_ERR    0x0200    //** command processed with error
#define STATUS_CMD_OK     0x0100    //** command processed without error
#define STATUS_SYS_NRDY   0x0010    //system not ready
#define STATUS_AUX_NRDY   0x0008    //auxiliary transductor not ready
#define STATUS_CAL_ERR    0x0004    //** controller calibration error
#define STATUS_HRD_FLT    0x0002    //** controller Hardware Fault
#define STATUS_ERR_MSG    0x0001    //** error message available

//************************ SERIAL STATUS HIGH REGISTER *************************
//  bit assignments for xil.ser_stath
//******************************************************************************

#define SSH_DATA_VALID    0x8000    //data valid bit, cleared if no serial clock
#define SSH_WATCHDOGFLAG  0x4000    //Watchdog timer reset flag
#define SSH_LOCALRSTFLAG  0x2000    //Local mode reset flag
#define SSH_RAMP          0x0200    //external hardware RAMP signal
#define SSH_HOLD          0x0100    //external hardware HOLD signal
#define SSH_LOCAL_MODE    0x0080    //Local Mode request from local control board
#define SSH_LOC_PS_EN     0x0040    //Power Supply Enable from local control board
#define SSH_LOC_PS_ON     0x0020    //Power Supply ON from local control board
#define SSH_LOC_VOLT      0x0010    //voltage mode from local control board
#define SSH_LOC_REV_POL   0x0008    //reverse polarity from local control board
#define SSH_LOC_RESET     0x0004    //reset signal from local control board
#define SSH_PS_ON_STAT    0x0002    //PS ON status returned from power supply

//******************* SERIAL STATUS LOW AND HISTORY REGISTER *******************
//  bit assignments for the following registers
//  xil.ser_statl           serial status low word
//  xil.ser_statl_lch0      serial status low word latched after PS turn ON
//  xil.ser_statl_lch1      serial status low word latched after PS trip
//******************************************************************************

#define SSL_DATA_FAULT    0x8000    //serial data fault, data not valid
#define SSL_GND_CUR       0x4000    //Ground Current Interlock, 1 = Fault
#define SSL_REG_XDUCT     0x2000    //Regulated Transductor Interlock, 1 = Fault
#define SSL_PS_READY      0x1000    //Power Supply Ready Interlock, 1 = Fault
#define SSL_MAG_INT3      0x0800    //Magnet Interlock 3, 1 = Fault
#define SSL_MAG_INT2      0x0400    //Magnet Interlock 2, 1 = Fault
#define SSL_MAG_INT1      0x0200    //Magnet Interlock 1, 1 = Fault
#define SSL_MAG_INT0      0x0100    //Magnet Interlock 0, 1 = Fault
#define SSL_KLX_INT1      0x0080    //Klixon Interlock 1, 1 = Fault
#define SSL_KLX_INT0      0x0040    //Klixon Interlock 0, 1 = Fault
#define SSL_50KHZ_CLK     0x0020    //50KHz Serial Status Clock fault
#define SSL_PS_STAT3      0x0008    //Power Supply Status 3, set by Power Supply
#define SSL_PS_STAT2      0x0004    //Power Supply Status 2, set by Power Supply
#define SSL_PS_STAT1      0x0002    //Power Supply Status 1, set by Power Supply
#define SSL_PS_STAT0      0x0001    //Power Supply Status 0, set by Power Supply

//*************************** CHASSIS CONFIGURATION ****************************
//  bit assignments for xil.db_eep.Config
//  upper byte is undefined, lower byte is used as chassis type in message CA
//******************************************************************************

#define CONFIG_BIPOLAR    0x0080    //Power supply is bipolar
#define CONFIG_REV_SW     0x0040    //Power supply has reversing switch
#define CONFIG_PS_SERIAL  0x0020    //Enable UART1 for OCEM multi-module PS
#define CONFIG_DIG_REG    0x0010    //Digital Regulation Enabled
#define CONFIG_HOLD_EN    0x0004    //Enable hardware Hold signal for all ramps
#define CONFIG_SLOW_RAMP  0x0002    //Enable slow ramp (time in 1/20 seconds)
#define CONFIG_LIN_RAMP   0x0001    //Enable linear ramps

#define PS_IS_BIPOLAR       (xil.db_eep.Config & CONFIG_BIPOLAR)
#define PS_HAS_REV_SW       (xil.db_eep.Config & CONFIG_REV_SW)
#define PS_SERIAL_ENABLE    (xil.db_eep.Config & CONFIG_PS_SERIAL)
#define DIGITAL_REG_ENABLED (xil.db_eep.Config & CONFIG_DIG_REG)
#define RAMP_HOLD_ENABLED   (xil.db_eep.Config & CONFIG_HOLD_EN)
#define SLOW_RAMP_ENABLED   (xil.db_eep.Config & CONFIG_SLOW_RAMP)
#define LIN_RAMP_ENABLED    (xil.db_eep.Config & CONFIG_LIN_RAMP)

//*********************** POWER SUPPLY CONTROL REGISTER ************************
//  bit assignments for xil.ps_cntl
//  To turn on the power supply, the PS Enable is set high and the PS ON line is
//  set high. To turn off the power supply, the Enable is set low.
//******************************************************************************

#define PSCNTL_PS_ON_H   0x0100    //Generates ON pulse on low to high transistion
#define PSCNTL_PS_ON_L   0x0001    //Returns to low state, no effect
#define PSCNTL_EN_ON     0x0200    //Set PS enable ON
#define PSCNTL_EN_OFF    0x0002    //Set PS enable OFF
#define PSCNTL_RESET_ON  0x0400    //Set Reset ON
#define PSCNTL_RESET_OFF 0x0004    //Set Reset OFF
#define PSCNTL_ERAMP_ON  0x0800    //Set Error Amp Enable ON
#define PSCNTL_ERAMP_OFF 0x0008    //Set Error Amp Enable OFF
#define PSCNTL_VLT_ON    0x1000    //Set Voltage Mode ON
#define PSCNTL_VLT_OFF   0x0010    //Set Voltage Mode OFF (Current Mode)
#define PSCNTL_REV_ON    0x2000    //Set Reverse Polarity ON
#define PSCNTL_REV_OFF   0x0020    //Set Reverse Polarity OFF
#define PSCNTL_RAMP_ON   0x4000    //Set Ramp ON
#define PSCNTL_RAMP_OFF  0x0040    //Set Ramp OFF
#define PSCNTL_HOLD_EN   0x8000    //Enable hardware Ramp/Hold signals
#define PSCNTL_HOLD_OFF  0x0080    //Disable hardware Ramp/Hold signals

//****************************** SYSTEM REGISTER *******************************
//  bit assignments for xil.sys
//******************************************************************************

#define SYS_IRQ5_ON      0x0001    //Set IRQ1 Enable ON
#define SYS_IRQ5_OFF     0x0002    //Set IRQ1 Enable OFF
#define SYS_IRQ1_ON      0x0004    //Set IRQ3 Enable ON
#define SYS_IRQ1_OFF     0x0008    //Set IRQ3 Enable OFF
#define SYS_SOFT_RESET   0x0100    //Reset Netburner Processor
#define SYS_HARD_RESET   0x0300    //Reset Xilinx and Netburner Processor


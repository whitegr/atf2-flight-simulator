//******************************************************************************
//  INCLUDES FILES FOR EPSC (Ethernet Power Supply Controller)
//  Filename: diagnostic.h
//  05/03/05
//
//  These functions are for the diagnostic tasks. There are three independent
//  UARTs in the MOD5082 processor. These functions must specify which UART is
//  being used for communication. This allows any of the three UARTs to be used
//  as the diagnostic port.
//
//  Each UART will have its own task, but share the program.
//
//  COM0    fd = 3
//  COM1    fd = 4
//  COM2    fd = 25
//
//  Standard I/O is assigned to UART0 by default. It can be reassigned to COM0, COM1
//  or COM2. The COM interfaces uses FIFOs and are non-blocking. The default code
//  accesses UART0 directly and blocks.
//
//  stdin   fd = 0
//  sdtout  fd = 1
//  stderr  fd = 2
//
//******************************************************************************

//  Include files for utilities.cpp

int  spRdStr(int fd, char* buf, int maxchar); //get string from serial port
char spRdChar(int fd, int timeout=60);        //get a character from serial port
void spShowTopHelp(int fd);                   //show top menu
void spShowEEPHelp(int fd);                   //show EEPROM menu
void spShowCalHelp(int fd);                   //show Calibration menu
void spShowDiagHelp(int fd);                  //show Chassis Diagnostic menu
void spShowNetHelp(int fd);                   //show Network Diagnostic menu
void spDiagnostic(int fd);                    //diagnostic command select
void spReset(int fd);                         //reset processor

//  Include files for config.cpp

void spShowIP(int fd, IPADDR ipAdr);          //show IP address
void spShowIpStk(int fd);                     //show IP stack
void spSetIPStk(int fd);                      //set IP stack
void spShowEnetConfig(int fd);                //show Ethernet Configuration
void spSetEnetConfig(int fd);                 //set Ethernet Configuration
void spShowIVA(int fd);                       //show IVA coefficients
void spSetIVA(int fd);                        //set IVA coefficients
void spShowXilStr(int fd, WORD * pxil);       //show Xilinx string data
void spShowMagID(int fd);                     //show for magnet ID
void spSetMagID(int fd);                      //set for magnet ID
void spShowSerNum(int fd);                    //show chassis serial number
void spSetSerNum(int fd);                     //set chassis serial number
void spShowAdcRef(int fd);                    //show adc reference value
void spSetAdcRef(int fd);                     //set adc reference value
void spShowAdcLinCoef(int fd);                //show ADC linearity coefficents
void spAdcLinCoef(int fd);                    //set ADC linearity coefficents
void spShowConfig(int fd);                    //show configuration flags
void spSetConfig(int fd);                     //set configuration flags
void spShowDigCoef(int fd);                   //show dig regulation coefficents
void spSetDigCoef(int fd);                    //set dig regulation coefficients
void spShowMbEEP(int fd);                     //show motherboardd EEPROM data
void spWrMbEEP(int fd);                       //write motherboard EEPROM
void spShowDbEEP(int fd);                     //show daughterboard EEPROM data
void spWrDbEEP(int fd);                       //write daughterboard EEPROM

//  Include files for calibrate.cpp

void spShowAdc1Gain(int fd);                  //show ADC1 gain coefficient
void spShowAdc2Gain(int fd);                  //show ADC2 gain coefficient
void spShowDacOffset(int fd);                 //show DAC offset coef, DAC = 0V
void spShowDacGain(int fd);                   //show DAC offset coef, DAC = 7V

//  Include files for display.cpp

void spShowADC2Dat(int fd);
void spShowHex(int fd, WORD* pDat, WORD len); //show hex data
void spShowInterlockStat(int fd);             //show interlock status
void spShowErrMsg(int fd);                    //show error messages
void spShowVersion(int fd);                   //show version
void spShowEEPHex(int fd);                    //show EEProm data image
void spShowRamp(int fd);                      //show ramp status
void spShowCalCoef(int fd);                   //show calibration coefficients
void spShowPSModDat(int fd);                  //show PS module data 07/20/07
void spShowFltHistBuf( int fd );              //show fault history bufs 08/06/07
void spShowLocCntlLEDs( int fd );             //show local control LEDS 08/06/07

//  Include files for Network.cpp

void spShowEthernet(int fd);                 //show Ethernet packet headers
void spNetwork(int fd);                      //show network status, MII counters
void spShowStack(int fd);                    //show task stack data
void spShowPoolBufData(int fd);              //show pool buffer data
void spShowTasks( int fd );                  //show task information
void spShowBuffers( int fd );                //show buffer information
void spDumpTCBStacks( int fd );              //show task stack information
void spShowEMACBuffers( int fd );            //show MAC buffer descriptors
void spShowMII(int fd);                      //show Ethernet MII registers

//******************************************************************************
//  INCLUDES FILES FOR EPSC (Ethernet Power Supply Controller)
//  Filename: leds.h
//  01/13/05
//
//  Hardware mapping of xilinx register for settinbg the three front panl LEDs.
//  LEDs can be turned on and off independent of each other.
//
//  Green   Red  Yellow
//   on     off     X     Standbye
//   off    on      X     Power supply ON
//   on     on      X     Power supply ramping
//    X      X     fsh    Network traffic
//   off    off    off    No power or hardware fault
//   on     on     on     Hardware fault or self test in progress
//   fsh    off     X     Power supply OFF, Hardware fault
//   off    fsh     X     Power supply ON, Hardware fault
//
//    X  = don't care
//   fsh = flashing
//
//  usage   xil.leds = GREEN_ON + RED_OFF
//
//******************************************************************************

#define  GREEN_ON         0x0001              //Turn on green LED
#define  GREEN_OFF        0x0002              //Turn off green LED
#define  RED_ON           0x0004              //Turn on red LED
#define  RED_OFF          0x0008              //Turn off red LED
#define  YELLOW_ON        0x0010              //Turn on yellow LED
#define  YELLOW_OFF       0x0020              //Turn off yellow LED

#define  CAL_LED_ON       0x0080              //Turn on calibration LED
#define  CAL_LED_OFF      0x0040              //Turn off calibration LED
#define  SPARE_LED_ON     0x0200              //Turn on spare LED (D24)
#define  SPARE_LED_OFF    0x0100              //Turn off spare LED (D24)

#define  RESET_WATCHDOG   0x8000              //Reset Watchdog timer


//******************************************************************************
//  INCLUDES FILES FOR EPSC (Ethernet Power Supply Controller)
//  Filename: eeprom.h
//  01/27/05
//
//******************************************************************************

#define MB_EEP    0          //select mother board EEPROM
#define DB_EEP    1          //select daughter board EEPROM
#define EEP_LOW   0          //select lower 32 words
#define EEP_HIGH  1          //select lower 32 words

WORD RdEEPROM(WORD cs, WORD hl);
WORD WrEEPROM(WORD cs, WORD hl);


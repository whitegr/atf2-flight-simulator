//******************************************************************************
//  INCLUDES FILES FOR EPSC (Ethernet Power Supply Controller)
//  Filename: DigReg.h
//  05/24/05
//
//******************************************************************************

void DigReg();           //digital regulation function

void InitDigCoef();      //initialize digital regulation coefficients

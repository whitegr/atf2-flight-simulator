//******************************************************************************
//  INCLUDES FILES FOR EPSC (Ethernet Power Supply Controller)
//  Filename: misc.h
//  03/10/05
//
//  Miscellaneous functions
//
//******************************************************************************

void WrErrMsg(char * pMsg);                 //writes a text string into msg buf
float RevPol(float i);                      //adjusts voltage for rev switch
float KlixRes(float V);                     //resistance on Klixon circuit
float KlixCond(float V);                    //conductance on Klixon circuit
SHORT GetStr(char * pStr, WORD maxlen);     //gets keyboard input with check
void ShowStat(PBYTE pStat, WORD len);       //print short or long status
void ShowHex(WORD * pDat, WORD len);        //prints hax and ASCII values
WORD Limit(long * pData, long Limit);       //limit value of *pData to Limit
WORD FloatToShortFloat(float x);            //convert float to short float (16 bit)
float ShortFloatToFloat(WORD x);            //convert short float to float

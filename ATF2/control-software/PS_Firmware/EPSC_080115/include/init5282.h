//******************************************************************************
//  Function Prototypes for init5282.cpp
//  Filename: init5282.h
//  03/17/05
//******************************************************************************

void InitChipSelect3();

void SetIntrCntl(int intc, long func, int vector, int level, int prio );

void Enable_IRQ5();

void SetCtsRts();

void UartHwFlow(int unum);

void SetUart2RxTx();

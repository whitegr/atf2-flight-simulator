//******************************************************************************
//  Filename:     mux.h
//  Last changed: 02/07/05
//
// This file defines the constants to map data to ADC channels
//
//******************************************************************************

//****************************** ADC1 Multiplerer ******************************

#define MUX1_OFF    0x0000  //offset (ground),  chan 0
#define MUX1_REF    0x0001  //reference,        chan 1
#define MUX1_DAC    0x0002  //DAC voltage,      chan 2
#define MUX1_REG    0x0003  //reg transductor,  chan 3

//****************************** ADC2 Multiplerer ******************************

#define MUX2_OFF    0x0000  //offset (ground),   chan 0
#define MUX2_REF    0x0001  //reference,         chan 1
#define MUX2_DAC    0x0002  //DAC voltage,       chan 2
#define MUX2_REG    0x0003  //reg transductor,   chan 3
#define MUX2_AUX    0x0004  //aux transductor,   chan 4
#define MUX2_ARIP   0x0005  //abs ripple,        chan 5
#define MUX2_A10V   0x0006  //analog 10V ref,    chan 6
#define MUX2_A5V    0x0007  //analog 5V,         chan 7
#define MUX2_PSV    0x0008  //PS volts,          chan 8
#define MUX2_GND    0x0009  //gnd current,       chan 9
#define MUX2_AGND   0x000A  //abs gnd current,   chan 10
#define MUX2_EXT    0x000B  //external input,    chan 11
#define MUX2_TEMP   0x000C  //temperature,       chan 12
#define MUX2_KLIX0  0x000D  //klixon 0 cur,      chan 13
#define MUX2_KLIX1  0x000E  //klixon 1 cur,      chan 14
#define MUX2_SPARE  0x000F  //spare (TV10),      chan 15
#define MUX2_VP15U  0x0010  //unreg +15V,        chan 16
#define MUX2_VP15R  0x0011  //reg +15V,          chan 17
#define MUX2_VM15U  0x0012  //unreg -15V,        chan 18
#define MUX2_VM15R  0x0013  //reg -15V,          chan 19
#define MUX2_VDD    0x0014  //digital 5.0V,      chan 20
#define MUX2_V3D3   0x0015  //digital 3.3V,      chan 21
#define MUX2_V2D5   0x0016  //digital 2.5v,      chan 22
#define MUX2_V1D2   0x0017  //digital 1.2V,      chan 23


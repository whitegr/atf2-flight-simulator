//******************************************************************************
//  INCLUDES FILES FOR EPSC (Ethernet Power Supply Controller)
//  Filename: UdpDiag.h
//  05/25/05
//
//******************************************************************************

void StartUdpDiagTask();                //start UDP diagnostic task
void UdpDiagMain(void * pd);            //main UDP diagnostic task
void udSendResponse(UDPPacket * pUDP);  //send response packet
void udSendString(UDPPacket * pUDP, char * data, WORD len);
int  udGetString(UDPPacket * pUDP, char * data, WORD MaxLen);
void udShowHelp(UDPPacket * pUDP);      //show help menu
void udShowADC2Dat1(UDPPacket * pUDP);  //show external ADC2 readings
void udShowADC2Dat2(UDPPacket * pUDP);  //show internal ADC2 readings


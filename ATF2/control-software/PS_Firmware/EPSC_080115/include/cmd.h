//******************************************************************************
//  Filename:     cmd.h
//  Last changed: 07/19/07
//
// This file defines the function prototypes for all command functions
//
//******************************************************************************

void Process_C0(UDPPacket * pUDP);    //Short status check
void Process_C1(UDPPacket * pUDP);    //Set current, C2 = hardware Ramp/Hold
void Process_C3(UDPPacket * pUDP);    //Setpoint readback
void Process_C4(UDPPacket * pUDP);    //Interlock Reset
void Process_C5(UDPPacket * pUDP);    //Power supply turn OFF
void Process_C6(UDPPacket * pUDP);    //Power supply turn ON, C7 = reverse polarity
void Process_C8(UDPPacket * pUDP);    //Analog readback
void Process_C9(UDPPacket * pUDP);    //Read message buffer
void Process_CA(UDPPacket * pUDP);    //Diagnostic message 1
void Process_CB(UDPPacket * pUDP);    //Diagnostic message 2
void Process_CC(UDPPacket * pUDP);    //Diagnostic message 3
void Process_CD(UDPPacket * pUDP);    //Last read status
void Process_CE(UDPPacket * pUDP);    //Diagnostic message, configuration
void Process_CF(UDPPacket * pUDP);    //Diagnostic message, summary
void Process_E1(UDPPacket * pUDP);    //Communications check message
void Process_E3(UDPPacket * pUDP);    //Reset controller message
void Process_E8(UDPPacket * pUDP);    //Read Power Module Status 07/19/07

void Process_F0(UDPPacket * pUDP);    //Test command, set DAC volts
void Process_F1(UDPPacket * pUDP);    //Test command, read ADC1 channel
void Process_F2(UDPPacket * pUDP);    //Test command, read ADC2 channel
void Process_F3(UDPPacket * pUDP);    //Test command, set hardware state
void Process_F4(UDPPacket * pUDP);    //Test command, set DC Fan voltage

void SendResponse(UDPPacket * pUDP);  //Sends response packet, releases buffer
WORD Status01(BYTE CmdErr);           //returns status bytes 0 and 1
WORD Status23();                      //returns status bytes 2 and 3
DWORD Chg100To120(WORD time);         //convert from 100 hz time to 120 hz time
DWORD Chg120To100(DWORD time);        //convert from 120 hz time to 100 hz time

// flip byte order to convert between big endian (Motorola) and little endian (Intel)
void flip2(PBYTE pDat);               //reverse order of 2 bytes, SHORT or WORD
void flip4(PBYTE pDat);               //reverse order of 4 bytes, DWORD or float

BYTE InstReadADC2(WORD chan);         //request reading from ADC2

//*************************** MESSAGE RESPONE CODES ****************************
//  The command byte code is replaced in the response message. If the messsage
//  is processed normally, the response code is 0 (Command OK). If there is a
//  problem with the command structure (illegal command) the message is sent back
//  with the command code changed to the error code.
//
//  0x00  Command OK
//  0x11  Unknown command
//  0x12  Invalid message length
//  0x13  Invalid channel number
//  0x14  Invalid number of entries
//  0xFF  No response message (used with reset commands)
//
//  Other errors, including out of range requests, return a normal response message
//  with the Command Error flag set.
//
//******************************************************************************

#define COMMAND_OK           0x00
#define COMMAND_ERR          0x01
#define UNKNOWN_COMMAND      0x11
#define INVALID_MSG_LENGTH   0x12
#define INVALID_CHAN_NUM     0x13
#define INVALID_NUM_ENTRY    0x14
#define NO_RESPONSE          0xFF

#define NO_ERR   0                     //pass to Status01() if no error
#define ERR      1                     //pass to Status01() if error


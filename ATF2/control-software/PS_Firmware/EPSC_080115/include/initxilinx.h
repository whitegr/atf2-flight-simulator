//******************************************************************************
//  INCLUDES FILES FOR EPSC (Ethernet Power Supply Controller)
//  Filename: initxilinx.h
//  02/14/05
//
//******************************************************************************

void InitXilinx();

//Internal functions, should not be called from outside of module

void SetDefMbEEP();
void SetDefDbEEP();


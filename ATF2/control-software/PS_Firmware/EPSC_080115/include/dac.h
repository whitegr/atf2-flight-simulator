//******************************************************************************
//  INCLUDES FILES FOR EPSC (Ethernet Power Supply Controller)
//  Filename: dac.h
//  02/08/05
//
//******************************************************************************

//dac gain = 836768.9 counts per volt. +/-10.025 volts full scale
//#define KDAC 836789

//dac gain = 819200 counts per volt. +/-10.240 volts full scale
#define KDAC (819200)

#define MAX_DAC_OFFSET    0x00080000   //maximum value (+/-) for DAC offset
#define DAC_OFFSET_DIV    0x00000010   //DAC offset divisor
#define MAX_DAC           0x007FFFFF   //maximum value (+/-) for DAC

//States for xil.Ramp_State, ramp state in xilinx SRAM

#define RAMP_OFF      0x00       //Ramp not configured
#define RAMP_C1       0x01       //Ramp configured by C1 command
#define RAMP_C2       0x02       //Ramp configured by C2 command
#define RAMP_DONE     0x04       //Current ramp request has completed
#define RAMP_STOP     0x08       //Current ramp was stopped by 0xE4 request

//functions in dac.h

void  WriteDAC();                //write offset compenstated data to DAC
void  Ramp();                    //ramp desired ADC value
void  InitCosineTable();         //initialize cosine lookup table
DWORD LinToCosine(DWORD k);      //convert linear coefficient to (1-cosine)/2


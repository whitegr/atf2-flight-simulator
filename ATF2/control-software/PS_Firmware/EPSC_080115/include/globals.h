//*****************************************************************************
//  filname: globals.cpp
//  03/08/05
//  07/18/07  Added UART1 Power Supply serial interface
//  Define global variables and constants for Ethernet Power Suppy Controller
//*****************************************************************************

#ifndef _Globals_Flag          //if _Globals_Flag not set, define all as extern
  #define _ex extern           //set "_ex" to extern
#else
  #undef _Globals_Flag         //undefine _Globals_Flag
  #define _ex                  //set "_ex" to null to allocate storage
#endif

extern const char * AppName;         //application name, set in main.cpp
extern const char * pFirmwareDate;   //firmware version, set in main.cpp

void StartShowEnetTask();      //**** temporary forward declaration ****
void StartADCTask();           //**** temporary forward declaration ****
void StartFanTask();           //**** temporary forward declaration ****
void StartTripTask();          //**** temporary forward declaration ****
void StartUart1RcvTask();      //**** temporary forward declaration ****
void StartUart1XmtTask();      //**** temporary forward declaration ****
void StartUart2Task();         //**** temporary forward declaration ****
void StartTelnetTask();        //**** temporary forward declaration ****
float WrFanVlt( float Vlt);    //**** temporary forward declaration ****

//************************* ETHERNET IP CONFIGURATION **************************

_ex  WORD  ManEnetConfig;            //manual Ethernet Configuration
#define AUTO_NEGOIATE    (0)         //auto negotiation of speed / duplex
#define MAN_10_HALF      (1)         //fixed 10 Mb/sec half duplex
#define MAN_10_FULL      (2)         //fixed 10 Mb/sec full duplex
#define MAN_100_HALF     (3)         //fixed 100 Mb/sec half duplex
#define MAN_100_FULL     (4)         //fixed 100 Mb/sec full duplex

//#define DEFAULT_IP_ADDRESS  "10.1.1.100"       //default IP address
#define DEFAULT_IP_ADDRESS  "192.168.1.99"     //default IP address
#define DEFAULT_IP_MASK     "255.255.255.0"    //default IP mask
#define DEFAULT_IP_GATE     "0.0.0.0"          //default IP gateway
#define DEFAULT_IP_DNS      "0.0.0.0"          //default IP DNS

#define UDP_PORT        ( 2000 )       //define UDP receive port number
#define UDP_DIAG_PORT   ( 2001 )       //define UDP diagnostic port number

// Task to display all received Ethernet packets
_ex  OS_FIFO  FecFifo;         //Ethernet receive FIFO, Moved from ethernet.cpp
_ex  OS_FIFO  ShowFecFifo;     //Diagnostic FIFO to display all packets
_ex  WORD     ShowEthernet;    //enable flag for Ethernet packet display
_ex  WORD     EnetFilterOff;   //Disable Ethernet Filter

//**************************** UCOS TASK PRIORITIES ****************************
// Task priorities, 0 is highest, 63 is lowest
// from costants.h
// #define MAIN_PRIO (50)      //starts at priority 10, changed to 50
// #define HTTP_PRIO (45)
// #define PPP_PRIO (44)
// #define TCP_PRIO (40)
// #define IP_PRIO (39)
// #define ETHER_SEND_PRIO (38)
// #define WIFI_TASK_PRIO (37)
//******************************************************************************

#define ADC_PRIO         (32)              //ADC task priority (240 Hz)
#define TRIP_PRIO        (33)              //Power Supply trip task priority
#define SHOW_ENET_PRIO   (34)              //Ethernet Packet Filter/Display
//#define SHOW_ENET_PRIO   (MAIN_PRIO - 4)   //Ethernet Packet Display
#define UDP_PRIO         (MAIN_PRIO - 4)   //Ethernet UDP task priority
#define UDP_DIAG_PRIO    (MAIN_PRIO - 3)   //Ethernet UDP Diagnostic task prio
#define TELNET_PRIO      (MAIN_PRIO - 2)   //Ethernet UDP task priority
#define FAN_PRIO         (MAIN_PRIO - 1)   //Fan speed control task priority
#define UART1_XMT_PRIO   (MAIN_PRIO + 1)   //UART1 transmit task priority
#define UART1_RCV_PRIO   (MAIN_PRIO + 2)   //UART1 receive task priority
#define UART2_PRIO       (MAIN_PRIO + 3)   //UART2 diagnostic task priority

_ex  OS_SEM   ADC_TASK_SEM;        //Semaphore for ADC task
_ex  OS_FIFO  UdpDiagFifo;         //UDP Diagnostic task FIFO

//****************************** GLOBAL VARIABLES ******************************

_ex  int   fdcom0;                 //file descriptor for UART0
_ex  DWORD TIMER;                  //Tenths second since last reset (13.6 years max)
_ex  WORD  FlashLED;               //Counter to flash LEDs for hardware faults
_ex  WORD  FlashYellow;            //Counter to flash yellow LED for UDP message

_ex  WORD  ADC1_Cyc;               //Current ADC1 240hz interrupt cycle (0-3)
_ex  WORD  ADC2_Cyc;               //Current ADC2 240hz interrupt cycle (0-7)
_ex  WORD  Reg_Mode;               //Regulation mode, 0=analog, 1=digital

_ex  WORD  ADC1_Status;            //ADC1 status, 0 = not ready 
_ex  WORD  ADC2_Status;            //ADC2 status, 0 = not ready  

_ex  long  AdcRef;                 //Reference voltage in ADC counts
_ex  long  ADC1_Offset;            //ADC1 offset correction coefficient
_ex  long  ADC2_Offset;            //ADC2 offset correction coefficient
_ex  long  ADC1_Gain;              //ADC1 gain correction coefficient
_ex  long  ADC2_Gain;              //ADC2 gain correction coefficient

_ex  long  ADC1_DREG_DATA;         //ADC1 data for regulated transductor
_ex  WORD  ADC1_DREG_FLAG;         //indicates valid ADC1 data for dig regulation

//ADC1 and DAC history, 120 samples per second

#define    ADC1_HIST_SIZE  2048             //ADC1 history buffer size
_ex  long  DAC_Hist_Data[ADC1_HIST_SIZE];   //DAC  data history, last 1024 readings
_ex  long  ADC1_Hist_Data[ADC1_HIST_SIZE];  //ADC1 data history, last 1024 readings
_ex  WORD  ADC1_Hist_Ptr;                   //History last written pointer

//ADC2 data registers, mapped to muliplexer channel numbers

_ex  long  ADC2_Data_Reg[24];               //ADC2 data registers for 24 channels

//ADC2 history buffer, 60 samples per second

#define    ADC2_HIST_SIZE  1024             //ADC2 history buffer size
_ex  LONG  ADC2_Hist_Data[ADC2_HIST_SIZE];  //ADC2 history, data
_ex  WORD  ADC2_Hist_Chan[ADC2_HIST_SIZE];  //ADC2 history, channel number
_ex  WORD  ADC2_Hist_Ptr;                   //ADC2 history, last written pointer

//***************************** ADC2 READ REQUEST ******************************
// These buffers allow the instruction processing task and the diagnostic task
// to request ADC2 to read a specfic data channel. The task sets the request flag
// to ON , sets the channel number, and pends on a semaphore. When the ADC starts
// the request, it changes the flag to convert. When the ADC finishes the conversion,
// it places the ADC data in the data register, changes the flag to done, and sets
// the semaphore. The requesting task receives the semaphore and returns the flag
// to off.
//******************************************************************************

//Flag state definitions for instruction and diagnostic read requests

#define    ADC1_REQ_OFF   0         //ADC1 request off state
#define    ADC1_REQ_ON    1         //ADC1 request conversion state
#define    ADC1_REQ_CONV  3         //ADC1 request convert state
#define    ADC1_REQ_DONE  4         //ADC1 request done state, data valid

#define    ADC2_REQ_OFF   0         //ADC2 request off state
#define    ADC2_REQ_ON    1         //ADC2 request short conversion state
#define    ADC2_REQ4_ON   2         //ADC2 request long conversion state
#define    ADC2_REQ_CONV  3         //ADC2 request convert state
#define    ADC2_REQ_DONE  4         //ADC2 request done state, data valid

//Instruction task read request flags for ADC1

_ex  OS_SEM  ADC1_INST_SEM;          //Semaphore for instruction task
_ex  WORD    ADC1_INST_REQ_FLAG;     //ADC1 Instrustion read request flag
_ex  WORD    ADC1_INST_REQ_CHAN;     //ADC1 Instruction read request channel
_ex  long    ADC1_INST_REQ_DATA;     //ADC1 Instruction read request data

//Instruction task read request flags for ADC2

_ex  OS_SEM  ADC2_INST_SEM;          //Semaphore for instruction task
_ex  WORD    ADC2_INST_REQ_FLAG;     //ADC2 Instrustion read request flag
_ex  WORD    ADC2_INST_REQ_CHAN;     //ADC2 Instruction read request channel
_ex  long    ADC2_INST_REQ_DATA;     //ADC2 Instruction read request data

//Diagnostic task read request flags for ADC2

_ex  OS_SEM  ADC2_DIAG_SEM;          //Semaphore for diagnostic task
_ex  WORD    ADC2_DIAG_REQ_FLAG;     //ADC2 Diagnostic read request flag
_ex  WORD    ADC2_DIAG_REQ_CHAN;     //ADC2 Diagnostic read request channel
_ex  long    ADC2_DIAG_REQ_DATA;     //ADC2 Diagnostic read request data

//UART1 Diagnostic Task Semaphore and data register
//*** 07/31/07 UART1 converted to PS serial link ***
//_ex  OS_SEM  ADC2_UART1_SEM;         //Semaphore for UART1 task
//_ex  WORD    ADC2_UART1_REQ_FLAG;    //UART1 request flag
//_ex  WORD    ADC2_UART1_REQ_CHAN;    //ADC2 Diagnostic read request channel
//_ex  long    ADC2_UART1_REQ_DATA;    //UART1 Diagnostic read request data

//Power Supply trip task read request flags for ADC2

_ex  OS_SEM  ADC2_TRIP_SEM;          //Semaphore for instruction task
_ex  WORD    ADC2_TRIP_REQ_FLAG;     //ADC2 Instrustion read request flag
_ex  WORD    ADC2_TRIP_REQ_CHAN;     //ADC2 Instruction read request channel
_ex  long    ADC2_TRIP_REQ_DATA;     //ADC2 Instruction read request data

//Power Supply Trip start task semaphore, set in interrupt task

_ex  OS_SEM  START_TRIP_SEM;

//variables to synchronize update of DAC data from diagnostic task

_ex  WORD    NewDesFlag;             //flag for new DAC data
_ex  LONG    TmpDesADC;              //new desired DAC in ADC readback

//**************************** ERROR MESSAGE BUFFER ****************************
// These 16 buffers store up to 15 error messages in a circular buffer. The write
// pointer points at the last message written. The read pointer points at the next
// message to be read. The buffer is empty when read and write pointers are equal.
//******************************************************************************

#define   NumErrMsgBuf  16          //Number of error message buffers
#define   ErrMsgBufSize 64          //Number of characters in buffer
_ex  SHORT ErrMsgRdPtr;             //Pointer to next message to be read
_ex  SHORT ErrMsgWrPtr;             //Pointer to next message to be written

_ex  char  ErrMsg[NumErrMsgBuf][ErrMsgBufSize];   //error message array
_ex  DWORD ErrMsgTime[NumErrMsgBuf];              //Time stamp

//***************** CALIBRATION ERROR REGISTER BIT ASSIGMENTS ******************
//  bit assignments for CalErr, only lower byte used
//******************************************************************************

_ex  WORD  CalErr;                    //ADC and DAC Calibration error codes

#define    ADC1_OFF_ERR     0x0001    //ADC1 offset Error
#define    ADC1_GAIN_ERR    0x0002    //ADC1 gain Error
#define    ADC2_OFF_ERR     0x0004    //ADC2 offset Error
#define    ADC2_GAIN_ERR    0x0008    //ADC2 gain Error
#define    DAC_OFF_ERR      0x0010    //DAC offset Error
#define    DIG_REG_ERR      0x0020    //Digital regulation error

//****************** HARDWARE FAULT REGISTER BIT ASSIGMENTS ********************
//  bit assignments for HrdFlt, only lower byte used
//******************************************************************************

_ex  WORD  HrdFlt;                    //Hardware fault codes

#define    ADC1_FLT         0x0001    //ADC1 fault
#define    ADC2_FLT         0x0002    //ADC2 fault
#define    MB_EEP_FLT       0x0004    //Motherboard EEPROM data error
#define    DB_EEP_FLT       0x0008    //Daughter board EEPROM data error
#define    XIL_FLT          0x0010    //Xilinx error
#define    WDT_FLT          0x0020    //Watchdog Timer Reset

_ex  WORD  FanRPM;         //Chassis Fan speed, revolutions per second
_ex  WORD  FanDAC;         //Fan DAC setpoint
_ex  WORD  FanTest;        //Fan test flag, non zero disables temperature cntl

//*********************** ADC LINEARITY CORRECTION DATA ************************
//  This is a lookup table for linearity ADC correction. The most significant 10
//  bits of the ADC reading are used as the offset into the looup table. The value
//  in the table is added to the ADC reading.
//******************************************************************************

_ex  long  ADC1_LIN[1024];            //ADC1 linearity correction data
_ex  long  ADC2_LIN[1024];            //ADC1 linearity correction data

//************************ UART1 Power Supply Interface ************************
//  07/19/07 Added UART1 interface for OCEM Power Supply Modules for KEK/ATF2
//******************************************************************************

#define MaxNumMod  (5)                //Max number of PS modules (1 <= n <= 8)

// Declare a queue to pass commands from Ethernet UDP(0xE8) to UART1 transmit
_ex  void * Uart1XmtQData[8];         //create array of void pointers for queue
_ex  OS_Q  Uart1XmtQ;                 //create queue control structure

_ex  float ModCur_CntToAmp;           //Module current scale factor

// Declare global variables used to hold power supply module data
_ex  BYTE  NumMod;                    //Number of Modules in Power Supply
_ex  BYTE  ModDataValid;              //PS Module Data Valid Flag
_ex  BYTE  ModFault;                  //PS Module Fault Register
_ex  BYTE  ModDisable;                //PS Module Disable Register
_ex  SHORT ModCur[MaxNumMod];         //PS Module current


//******************************************************************************
//  INCLUDES FILES FOR EPSC (Ethernet Power Supply Controller)
//  Filename: adc.h
//  02/03/05
//
//  Process_ADC()
//    Initialize
//      ResetADC()
//      InitializeADC()
//      CalibrateADC()
//      SetADCAutoMode()
//    Normal operation
//      Read_ADC()
//      Set_ADC_Chan()
//        SetADCMux(chan)
//      Process_ADC_Data()
//
//******************************************************************************

//*************************** CALIBRATION CONSTANTS ****************************
//  These coefficients give +/-0.1% adjustment of gain and offset.
//  For 24 bit (plus sign) conversions, 0.1% is 2**14
//  0x80000 is 2**19, divided by 0x20 (2**5), is 2**14
//  Data * 0x80000 (2**19) / 0x20000000 (2**29) = Data / 2**10 or Data / 1024
//  adc gain = 1383688 counts per volt. +/-12.125 volts full scale
//******************************************************************************

#define MAX_ADC_OFFSET   0x00080000   //(2**19) maximum value (+/-) for ADC offset
#define MAX_ADC_GAIN     0x00080000   //(2**19) maximum value (+/-) for ADC gain
#define ADC_OFF_SCALE    0x00000020   //offset correction divisor
#define ADC_GAIN_SCALE   0x20000000   //(2**29) gain correction divisor

#define KADC             1383688      //ADC counts per volt

void Process_ADC1();
void Process_ADC2();

//Internal functions, should not be called outside of ADC.cpp

void ResetADC1();
void ResetADC2();
void InitializeADC1();
void InitializeADC2();
void CalibrateADC1();
void CalibrateADC2();
void SetADC1AutoMode();
void SetADC2AutoMode();
void SetADC1Mux(WORD chan);
void SetADC2Mux(WORD chan);
void Read_ADC1();
void Read_ADC2();
void Set_ADC1_Chan();
void Set_ADC2_Chan();
void Process_ADC1_Data();
void Process_ADC2_Data();
void InitADCLinearityTables();

//*****************************************************************************
//   INCLUDES FILES FOR EPSC (Ethernet Power Supply Controller)
//   Filename: includes.h
//   02/16/05
//*****************************************************************************

#ifndef _Includes_Flag
  #define _Includes_Flag

  #include <predef.h>
  #include <basictypes.h>
  #include <stdio.h>
  #include <stdlib.h>
  #include <ctype.h>
  #include <string.h>
  #include <counters.h>
  #include <startnet.h>
  #include <autoupdate.h>
  #include <dhcpclient.h>
  #include <cfinter.h>
  #include <math.h>
  #include <ucos.h>
  #include <udp.h>
  #include <arp.h>
  #include <buffers.h>
  #include <serial.h>
  #include <serinternal.h>
  #include <smarttrap.h>
  #include <C:\Nburn\mod5282\system\sim5282.h>
  #include <C:\Nburn\mod5282\system\ethervars.h>

  #include <C:\Nburn\EPSC\include\globals.h>
  #include <C:\Nburn\EPSC\include\init5282.h>
  #include <C:\Nburn\EPSC\include\xilinx.h>
  #include <C:\Nburn\EPSC\include\initxilinx.h>
  #include <C:\Nburn\EPSC\include\udp.h>
  #include <C:\Nburn\EPSC\include\cmd.h>
  #include <C:\Nburn\EPSC\include\adc.h>
  #include <C:\Nburn\EPSC\include\dac.h>
  #include <C:\Nburn\EPSC\include\mux.h>
  #include <C:\Nburn\EPSC\include\leds.h>
  #include <C:\Nburn\EPSC\include\eeprom.h>
  #include <C:\Nburn\EPSC\include\math.h>
  #include <C:\Nburn\EPSC\include\misc.h>
  #include <C:\Nburn\EPSC\include\diagnostic.h>
  #include <C:\Nburn\EPSC\include\DigReg.h>
  #include <C:\Nburn\EPSC\include\UdpDiag.h>

#endif  //ifndef _Includes_Flag


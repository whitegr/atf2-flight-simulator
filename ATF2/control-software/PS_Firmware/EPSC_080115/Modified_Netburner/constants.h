/* Rev:$Revision: 1.1.1.1 $ */  

/******************************************************************************
 *
 * Copyright 1998-2004 NetBurner  ALL RIGHTS RESERVED
 *   Permission is hereby granted to purchasers of NetBurner Hardware
 *   to use or modify this computer program for any use as long as the
 *   resultant program is only executed on NetBurner provided hardware.
 *
 *   No other rights to use this program or it's derivitives in part or
 *   in whole are granted.
 *
 *   It may be possible to license this or other NetBurner software for
 *        use on non NetBurner Hardware.
 *   Please contact Licensing@Netburner.com for more infomation.
 *
 *   NetBurner makes no representation or warranties
 *   with respect to the performance of this computer program, and
 *   specifically disclaims any responsibility for any damages,
 *   special or consequential, connected with the use of this program.
 *
 *---------------------------------------------------------------------
 *
 * NetBurner
 * 5405 Morehouse Dr #350
 * San Diego Ca, 92121
 *
 * information available at:  http://www.netburner.com
 * E-Mail info@netburner.com
 *
 * Support is availible: E-Mail support@netburner.com
 *
 *****************************************************************************/
#ifndef _CONSTANTS_H
#define _CONSTANTS_H

#define TICK_IRQ_LEVEL (5)      /* System clock IRQ level */
#define SERIAL_IRQ_LEVEL (3)
#define SERIAL_VECTOR_BASE (64)

#define TICKS_PER_SECOND (20)   /* System clock tick */

/* Ethernet buffer defines */
#define ETHER_BUFFER_SIZE 1548
#define ETH_MAX_SIZE  (1500)
#define ETH_MIN_SIZE  (46)
#define MAX_UDPDATA (ETH_MAX_SIZE-(20+8+14))

#define SERIAL_TX_BUFFERS (2) /* ETHERN_BUFFER_SIZE = bytes of serial TX fifo */
#define SERIAL_RX_BUFFERS (2) /* ETHERN_BUFFER_SIZE = bytes of serial RX fifo */
#define stdin_buffer_size (200)

#define OS_MAX_TASKS    20 /* Max number of system tasks */

/* System task priorities */
/* IDLE task is set at lowest priority, 63 */
#define MAIN_PRIO (50)  /* used for UserMain */
#define HTTP_PRIO (45)
#define PPP_PRIO (44)
#define TCP_PRIO (40)
#define IP_PRIO (39)
#define ETHER_SEND_PRIO (38)
#define WIFI_TASK_PRIO (37)

/* Stack definitions */
#define IP_STK_SIZE (2048)
#define TCP_STK_SIZE (2048)
#define HTTP_STK_SIZE (2048)
#define IDLE_STK_SIZE  (2048)
#define ETHER_SEND_STK_SIZE (2048)
#define USER_TASK_STK_SIZE (2048)


/* TCP definitions */
#define DEFAULT_TCP_MSS (512)
#define DEFAULT_TCP_RTTVAR ((TICKS_PER_SECOND*3)/4)  /*See RFC 1122 for a 50msec tick 60 ticks=3 sec 4*15=60 (The 4 comes from stevens Vol1-300) */
#define TCP_CONN_TO (75 * TICKS_PER_SECOND)    /* 75 seconds Min  */
#define TCP_ACK_TICK_DLY (TICKS_PER_SECOND /5) /* 200 msec delayed ACK timer */
#define DEFAULT_INITAL_RTO (TICKS_PER_SECOND*3)
#define TCP_MAX_RTO (64 * TICKS_PER_SECOND)
#define TCP_MIN_RTO (TICKS_PER_SECOND/2)
#define TCP_2MSL_WAIT (60 * TICKS_PER_SECOND)
#define MAX_TCP_RETRY (12)
#define TCP_WRITE_TIMEOUT (TICKS_PER_SECOND*10)
#define TCP_BUFFER_SEGMENTS (3)               /* Store 3 segments max in tx and rx buffers */

#define MAX_MULTICAST_GROUPS  (32)

#define HTTP_TIMEOUT   (TICKS_PER_SECOND*10)  /* 10 idle Seconds and a partially recieved request is abandoned */
#define HTTP_READ_TIME_LIMIT (30) /* Seconds to allow reading to avoid denial of service*/
#define HTTP_RX_BUFFERSIZE  (10000)

#define SERIAL_SOCKET_OFFSET (3)
#define TCP_SOCKET_OFFSET (5)
#define TCP_SOCKET_STRUCTS (32) 

#define EXTRA_IO_OFFSET (TCP_SOCKET_OFFSET+TCP_SOCKET_STRUCTS)
#define MAX_IP_ERRS 3

//*****************************************************************************************************
#define BUFFER_POOL_SIZE  (64) /* 08/01/2007 DJM Returned to 64 from 256, was raised by last release */
//*****************************************************************************************************

#define UDP_DISPATCH_SIZE (10)
#define UDP_MIN_BUFFER_THRESHOLD (10)
#define ARP_ENTRY_SIZE    (256)

#define UDP_NETBURNERID_PORT (0x4E42) /* NB */
#define UDP_DHCP_CLIENT_PORT (68) 

#define TFTP_RX_PORT  (1414)

#define LINK_STATUS_CHECK_INTERVAL (2*TICKS_PER_SECOND)


#endif

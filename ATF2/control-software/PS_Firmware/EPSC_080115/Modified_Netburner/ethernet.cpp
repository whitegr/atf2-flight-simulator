/* Rev:$Revision: 1.1.1.1 $ */
#include "predef.h"
#include "includes.h"
#include "nettypes.h"
#include "constants.h"
#include "sim5282.h"
#include "buffers.h"
#include "netinterface.h"
#include "ethernet.h"
#include "cfinter.h"
#include "counters.h"
#include "utils.h"
#include "system.h"
#include "bsp.h"
#include "ip.h"
#include "ethervars.h"
#include "randseed.h"
#include <stdio.h>

#ifdef GATHER_RANDOM
inline void random_seed()
{
  rnd_collect[( rnd_cnt++ ) & 0xFF] = sim.pit[0].pcntr;
}
#endif

extern "C"
{
  void SetIntc( int intc, long func, int vector, int level, int prio );
}

#define  BO_RDY4TX      (5)
#define  BO_RXBUF       (7)

static int myInterfaceNumber;

/* Global PUBLIC Ethernet related variables */
MACADR EthernetMac;

//************** Changes to allow variables to be visable gobally **************
//static OS_SEM SemSend;        //****uncomment to return to normal*************
OS_SEM SemSend;                 //*******Temporary 11/07/05 DJM ****************

//static OS_FIFO FecFifo;       //****uncomment to return to normal*************
extern OS_FIFO FecFifo;         //FecFifo declared in gobals 10/21/05 DJM
extern OS_FIFO ShowFecFifo;     //Show Enet packet FIFO      10/21/05 DJM
extern WORD    ShowEthernet;    //Show packet enable flag    10/26/05 DJM


//Packet filter function added 06/12/06 DJM
extern WORD EnetFilterOff;      //Enable Ethernet Filter     06/12/06 DJM
extern WORD EthernetFilter( pool_buffer * pb, BYTE BroadcastFlag);
//******************************************************************************

#define IP_20BYTE_ID  (0x4500)
#define ETHER_TYPE_IP (0x0800)
#define ETHER_TYPE_ARP (0x0806)

volatile int nTx;
volatile int nRx;

volatile EtherBD TxBD[NUM_TX_BD] __attribute__( ( aligned( 16 ) ) );
volatile EtherBD RxBD[NUM_RX_BD] __attribute__( ( aligned( 16 ) ) );;

volatile PoolPtr RxBDpp[NUM_RX_BD];
volatile PoolPtr TxBDpp[NUM_TX_BD];

volatile BYTE RxBDFlag[NUM_RX_BD];
volatile BYTE TxBDFlag[NUM_TX_BD];    //added 11/08/05 DJM
//volatile BYTE TxBDFlag[NUM_RX_BD];  //Removed 11/08/05 DJM

int last_rxbd;

DWORD LastIsrStatus;
DWORD RxPosts;
DWORD TxcPosts;
DWORD RxIsr;
DWORD TxIsr;

BOOL bEthLink;
BOOL bEthDuplex;
BOOL bEth100Mb;
static BOOL bEverHadLink; /*Have we ever had link */

void DoRX( PoolPtr pp, WORD length );

void ResetEnetPart2();
void WarmEnetReset();

DWORD   EtherSendStk[ETHER_SEND_STK_SIZE] __attribute__( ( aligned( 4 ) ) );

/*******************************************************************
*               Read the stored ethenet physical address           *
*******************************************************************/
void read_eth_addr( MACADR *hwa )
{
  int i;
  for ( i = 0; i < 6; i++ )
    ( ( char * ) hwa )[i] = gConfigRec.mac_address[i];
}

void ShowBufferNum( PoolPtr pp );

// change test from auto negotiate done to link status good 11/09/05 DJM
void ProcessDuplexStatus()
{
  DWORD DONT_HANG = 0;
  sim.fec.eir=0x00800000;
  sim.fec.mdata = 0x60060000;
  while (((sim.fec.eir & 0x00800000) == 0) && (DONT_HANG < 5000000)) DONT_HANG++;
  DWORD phy_data = sim.fec.mdata;

  if ( ( phy_data & 0xFFFF0000 ) == 0x60060000 )
  {
    BOOL bNewLink = ( ( phy_data & 0x04 ) != 0 );
    bEthLink = bNewLink;
    if (bEthLink)
    {
      if ( !bEverHadLink )
      {
        ResetEnetPart2();
        return;
      }
      else
      {
        DONT_HANG = 0;
        sim.fec.eir=0x00800000;
        sim.fec.mdata = 0x60020000;
        while (((sim.fec.eir & 0x00800000) == 0) && (DONT_HANG < 5000000)) DONT_HANG++;
        DWORD phy_data = sim.fec.mdata;
        bEth100Mb = (( phy_data & 0x2000 )!=0);
        BOOL bNewDuplex = (( phy_data & 0x100 )!= 0);
        if (bNewDuplex != bEthDuplex)
        {
          WarmEnetReset();
          return;
        }
      }
    }
  }
}

/*******************************************************************************
void ProcessDuplexStatus()
{
  DWORD phy_data=sim.fec.mdata;
  if ((phy_data & 0xFFFF0000)==0x60060000)
  {
    BOOL bNewLink= ((phy_data & 0x020 )!=0);
    if (bNewLink!=bEthLink)
    {
      bEthLink=bNewLink;
      if (bEthLink)
      {
        if (!bEverHadLink)
        {
          bEverHadLink=TRUE;
          ResetEnetPart2(); 
          return;
        }
        else
        {
          DWORD DONT_HANG=0;
          sim.fec.eir=0x00800000;
          sim.fec.mdata = 0x60020000;
          while (((sim.fec.eir & 0x00800000)==0) && (DONT_HANG < 5000000)) DONT_HANG++;
          DWORD phy_data=sim.fec.mdata;
          BOOL bNewDuplex= (( phy_data & 0x100 )!=0); 
          bEth100Mb=(( phy_data & 0x2000 )!=0);
          if (bNewDuplex!=bEthDuplex)
          {
            bEthDuplex=bNewDuplex;
            WarmEnetReset(); 
            return;
          }
        }
      }
    }
  }
  sim.fec.mdata=0x60060000;	
}
*******************************************************************************/

/* Ether process Task must do four things */
/* Process Received Data */
/* Process Add Data to be transmitted */
/* Release used Tx buffers */    

void Ether_ProcessTask( void * p )
{
  WORD wait = LINK_STATUS_CHECK_INTERVAL;
  while ( 1 )
  {
    if ( !bEthLink ) 
      wait = TICKS_PER_SECOND / 2;
    else
      wait = LINK_STATUS_CHECK_INTERVAL;
    pool_buffer * pb = ( pool_buffer * ) OSFifoPend( & FecFifo, wait );
    if (pb == NULL)
    {
      ProcessDuplexStatus();
    }
    else
    {
//    WORD len = (WORD) ( DWORD ) pb->pPoolNext;
      WORD len = pb->usedsize;                     // 06/06/06 DJM
      DoRX( pb, len );
    }
    if ( !bEthLink ) ProcessDuplexStatus(); 
  }
}

/*******************************************************************************
void Ether_ProcessTask( void *p )
{
  WORD wait=LINK_STATUS_CHECK_INTERVAL;
  while ( 1 )
  {
    int i;
    WORD len;
    if (!bEthLink) 
      wait=TICKS_PER_SECOND/2;
    else
      wait=LINK_STATUS_CHECK_INTERVAL;
    pool_buffer *pb = ( pool_buffer * ) OSFifoPend( &FecFifo, wait );
    if (pb==NULL)
    {  //Status checking 
      ProcessDuplexStatus();
    }
    else
    {
      switch ( pb->bBuffer_state )
      {
        case BO_RXBUF:
          i = pb->bBufferFlag;
          if ( RxBD[i].flags & RXBD_Error_Mask )
          {
            frames_rx_err++;
            USER_ENTER_CRITICAL();
            RxBD[i].flags |= RXBD_Flag_Empty;
            RxBDFlag[i] = TRUE;
            USER_EXIT_CRITICAL();
            sim.fec.rdar = 0x00000001;
          }
          else
          {
            if ( RxBD[i].flags & RXBD_Flag_BroadCast )
              pb->bBufferFlag = BS_PHY_BCAST;
            else
              pb->bBufferFlag = 0;
            len = RxBD[i].length;
            PoolPtr pp = GetBuffer();
            if ( pp == NULL )
            {
              pp = pb;
              pb = NULL;
            }
            RxBD[i].length = ETHER_BUFFER_SIZE;
            RxBD[i].address = ( unsigned long ) ( pp->pData );
            RxBDpp[i] = pp;
            pp->bBuffer_state = BO_RXBUF; 
            ChangeOwner( pp );
            pp->bBufferFlag = ( BYTE ) i;
            USER_ENTER_CRITICAL();
            RxBD[i].flags |= RXBD_Flag_Empty;
            RxBDFlag[i] = TRUE;
            USER_EXIT_CRITICAL();
            sim.fec.rdar = 0x00000001;
            if ( pb ) DoRX( pb, len );
          }
          break;
      }
    if (!bEthLink) ProcessDuplexStatus(); 
    }//Else got buffer 
  }
}
*******************************************************************************/

WORD GetSum( PWORD addr, WORD count );

void DoRX( PoolPtr pp, WORD ocount )
{
  frames_rx++;
  PWORD cp;
  DWORD n;
  DWORD csum = 0;
  WORD count = ocount;
  pp->bInterfaceNumber = myInterfaceNumber; 
  cp = ( PWORD ) pp->pData;
  pp->usedsize = 0;
  if ( count < 14 )
  {
    frames_rx_err++;
    FreeBuffer( pp );
    return;
  }
  count -= 14;
  n = 7;
  if ( cp[6] == ETHER_TYPE_IP ) //Type is IP
  {
    //Process IP packet
    //Now we read the IP header.
    //We need to do the checksum stuff
    DBPRINT( DB_ETHER, "I" );
    WORD ip_count;
    if ( ( cp[7] & 0xFF00 ) != IP_20BYTE_ID )
    {
      unsigned char option_count = ( ( ( cp[7] & 0xF000 ) >> 8 ) - 5 ) << 2;
      if ( ( cp[7] & 0xF000 ) == ( IP_20BYTE_ID & 0xF000 ) )
      {
        csum = GetSum( cp + 7, 20 + option_count );
        count -= option_count + 20;
        ip_count = cp[8];
        ip_count -= option_count + 20;;
        n += ( 20 + option_count ) / 2;
      }
      else
      {
        ip_count = 0;
        csum = 0x1234; /* A Bogus value */
      }
    }
    else
    {
      csum = GetSum( cp + 7, 20 );
      count -= 20;
      ip_count = cp[8];
      ip_count -= 20;
      n += 10;
    }
    //Now we should have a 32 bit checksum value for the header.
    //See RFC1071 for information on calculating the checksum.
    if ( csum == 0 )
    {
      csum = 0;
      csum = GetSum( cp + n, ip_count );
      pp->usedsize = ( n * 2 + ip_count );
      count = 0;
    }
    if ( pp->usedsize > 0 )
    {
      ChangeOwner( pp );
      pPacketfunc( pp, ( PEFRAME ) ( cp ), ( WORD ) ( ~csum ) );
    }
    else
    {
      frames_rx_err++;
      enet_last_errhw |= 0xF000;
      FreeBuffer( pp );
    }
    return;
  }/* Was IP packet */
  if ( cp[6] == ETHER_TYPE_ARP )
  {
    DBPRINT( DB_ETHER, "A" );
    while ( count )
    {
      n++;
      if ( count == 1 )
        count = 0;
      else
        count -= 2;
    }
    pp->usedsize = ( n * 2 );
    //Fixup the next packet pointer and recieve buffer boundary
    ChangeOwner( pp );
    pArpFunc( pp, ( PEFRAME ) ( cp ) );
    return;
  }
  FreeBuffer( pp );
}

INTERRUPT( rxenetisr, 0x2400 ) /* IRQ 4 MAsk */
{
#ifdef  GATHER_RANDOM
  random_seed();
#endif
  enet_isr++;
  RxIsr++; 
  DWORD i_status = sim.fec.eir;
  LastIsrStatus = i_status;
  sim.fec.eir = ( FEC_ISR_MASK_RXF | FEC_ISR_MASK_RXB );
  register int last = last_rxbd;
  register int i = last;
  do
  {
    if ( ( ( RxBD[i].flags & RXBD_Flag_Empty ) == 0 ) && ( RxBDFlag[i] ) )
    {
      if ( RxBD[i].flags & RXBD_Error_Mask )
      {
        frames_rx_err++;
      }
      else
      {

//*************** Packet filter added to interrupt 06/12/06 DJM ****************
        BYTE BroadcastFlag;
        if ( RxBD[i].flags & RXBD_Flag_BroadCast )
          BroadcastFlag = BS_PHY_BCAST;
        else
          BroadcastFlag = 0;

        PoolPtr pp = NULL;
        if ( ( EnetFilterOff || EthernetFilter( RxBDpp[i], BroadcastFlag ) )
          && ( GetFreeCount() > 4 ) )
        {
          pp = GetBuffer();
          if ( pp == NULL ) frames_rx_err++;
        }
//******************************************************************************

        if ( pp != NULL )
        {
          RxBDpp[i]->bBufferFlag = BroadcastFlag;     // 06/12/06 DJM
          RxBDpp[i]->usedsize = RxBD[i].length;       // 06/06/06 DJM
          RxBDpp[i]->dwTime = TimeTick; 
          RxBDpp[i]->dwTimeFraction = sim.pit[0].pcntr;

          if (ShowEthernet)
            OSFifoPost( &ShowFecFifo, ( OS_FIFO_EL * ) ( RxBDpp[i] ) );
          else
            OSFifoPost( &FecFifo, ( OS_FIFO_EL * ) ( RxBDpp[i] ) );

          RxPosts++;
          RxBDpp[i] = pp;
          RxBD[i].address = ( unsigned long ) ( pp->pData );
          pp->bBuffer_state = BO_RXBUF;
          ChangeOwner( pp );
          pp->bBufferFlag = ( BYTE ) i;
        }
      }

      RxBD[i].length = ETHER_BUFFER_SIZE;
      RxBD[i].flags |= RXBD_Flag_Empty;
      last_rxbd = i;
    }
    if (++i >= NUM_RX_BD) i = 0;
  }
  while (i != last);
  sim.fec.rdar = 0x00000001;
}

/*******************************************************************************
INTERRUPT( rxenetisr, 0x2400 )     // IRQ 4 MAsk
{
#ifdef  GATHER_RANDOM
  random_seed();
#endif
  enet_isr++;
  RxIsr++; 
  DWORD i_status = sim.fec.eir;
  LastIsrStatus = i_status;
  if ( i_status & FEC_ISR_MASK_RXF )
  {
    sim.fec.eir = FEC_ISR_MASK_RXF;
    register int last = last_rxbd;
    register int i = last;
    do
    {
      if ( ( ( RxBD[i].flags & RXBD_Flag_Empty ) == 0 ) && ( RxBDFlag[i] ) )
      {
        RxBDFlag[i] = FALSE;
        RxBDpp[i]->dwTime = TimeTick; 
        RxBDpp[i]->dwTimeFraction = sim.pit[0].pcntr;

// **********************Temporary Modification 10/26/05 DJM ***************
        if (ShowEthernet)
          OSFifoPost( &ShowFecFifo, ( OS_FIFO_EL * ) ( RxBDpp[i] ) );
        else
          OSFifoPost( &FecFifo, ( OS_FIFO_EL * ) ( RxBDpp[i] ) );
// *************************************************************************
//        OSFifoPost( &FecFifo, ( OS_FIFO_EL * ) ( RxBDpp[i] ) );  //original

        RxPosts++;
//        for ( int i = 0; i < NUM_RX_BD; i++ )  //removed DJM 
        last_rxbd = i;
      }
      i++;
      if (i == NUM_RX_BD) i=0;
    }
    while (i != last);
  }
  else if ( i_status & FEC_ISR_MASK_RXB )
  {
    sim.fec.eir = FEC_ISR_MASK_RXB;
    register int last = last_rxbd;
    register int i = last;
    do
    {
      if ( ( ( RxBD[i].flags & RXBD_Flag_Empty ) == 0 ) && ( RxBDFlag[i] ) )
      {
        RxBDFlag[i] = FALSE;
        RxBDpp[i]->dwTime = TimeTick; 
        RxBDpp[i]->dwTimeFraction = sim.pit[0].pcntr;

// **********************Temporary Modification 10/26/05 DJM ***************
        if (ShowEthernet)
          OSFifoPost( &ShowFecFifo, ( OS_FIFO_EL * ) ( RxBDpp[i] ) );
        else
          OSFifoPost( &FecFifo, ( OS_FIFO_EL * ) ( RxBDpp[i] ) );
// *************************************************************************
//        OSFifoPost( &FecFifo, ( OS_FIFO_EL * ) ( RxBDpp[i] ) );  //original

        RxPosts++;
        last_rxbd = i;   //added DJM    
      }
      i++;
      if (i == NUM_RX_BD) i = 0;
    }
    while (i != last);
  }
}
***************************************************************************/

INTERRUPT( txenetisr, 0x2400 ) /* IRQ 4 MAsk */
{
#ifdef  GATHER_RANDOM
  random_seed();
#endif
  enet_isr++;
  TxIsr++;
  DWORD i_status = sim.fec.eir;
  LastIsrStatus = i_status;
  sim.fec.eir = FEC_ISR_MASK_TXF | FEC_ISR_MASK_TXB | FEC_ISR_MASK_RL;
  for ( int i = 0; i < NUM_TX_BD; i++ )
  {
    if ( ( ( TxBD[i].flags & TXBD_Flag_Ready ) == 0 ) && ( TxBDFlag[i] ) )
    {
      TxBDFlag[i] = FALSE;
      OSSemPost( &SemSend );
      frames_tx++;
      if ( ( TxBD[i].flags & TXBD_Error_Mask ) ||    // error flags
           ( i_status & ( FEC_ISR_MASK_TXB | FEC_ISR_MASK_RL ) ) )
      {
        frames_tx_err++;
      }
      TxBD[i].length = 0;
      TxBD[i].address = 0;
      TxBDpp[i]->dwTime = TimeTick;
      TxcPosts++;
      TxBDpp[i]->bBuffer_state = BO_SOFTWARE;
      FreeBuffer( TxBDpp[i] );
      TxBDpp[i] = NULL;;
    }
  }
}

/*******************************************************************************
INTERRUPT( txenetisr, 0x2400 ) // IRQ 4 MAsk
{
#ifdef  GATHER_RANDOM
  random_seed();
#endif
  enet_isr++;
  TxIsr++;
  DWORD i_status = sim.fec.eir;
  LastIsrStatus = i_status;
  sim.fec.eir = FEC_ISR_MASK_TXF | FEC_ISR_MASK_TXB | FEC_ISR_MASK_RL;
  if ( i_status & FEC_ISR_MASK_TXF )
  {
    for ( int i = 0; i < NUM_TX_BD; i++ )
    {
      if ( ( ( TxBD[i].flags & TXBD_Flag_Ready ) == 0 ) && ( TxBDFlag[i] ) )
      {
        sim.fec.eir = FEC_ISR_MASK_TXF;
        TxBDFlag[i] = FALSE;
        OSSemPost( &SemSend );
        frames_tx++;
        if ( TxBD[i].flags & TXBD_Error_Mask )
        {
          frames_tx_err++;
        }
        TxBD[i].length = 0;
        TxBD[i].address = 0;
        TxBDpp[i]->dwTime = TimeTick;
        TxcPosts++;
        TxBDpp[i]->bBuffer_state = BO_SOFTWARE;
        FreeBuffer( TxBDpp[i] );
        TxBDpp[i] = NULL;;
      }
    }
  }
  else if ( i_status & FEC_ISR_MASK_TXB )
  {
    sim.fec.eir = FEC_ISR_MASK_TXB;
    for ( int i = 0; i < NUM_TX_BD; i++ )
    {
      if ( ( ( TxBD[i].flags & TXBD_Flag_Ready ) == 0 ) && ( TxBDFlag[i] ) )
      {
        TxBDFlag[i] = FALSE;
        OSSemPost( &SemSend );
        frames_tx_err++;
        TxBD[i].length = 0;
        TxBD[i].address = 0;
        TxBDpp[i]->dwTime = TimeTick;
        TxcPosts++;
        TxBDpp[i]->bBuffer_state = BO_SOFTWARE;
        FreeBuffer( TxBDpp[i] );
        TxBDpp[i] = NULL;;
      }
    }
  }
}
*******************************************************************************/

/******************************************************************************
*  Transmit packets to the network. The data in the buffer poolptr
*  that is passed in is assumed to have a valid EFRAM structure
*  at the begining of the data area.
******************************************************************************/

void EthTransmitBuffer( PoolPtr pb ) //Transmits a buffer and frees it afterward
{
  pb->bBuffer_state = BO_RDY4TX; 
  ( ( PEFRAME ) pb->pData )->src_addr = EthernetMac; 
  OSSemPend( &SemSend, 0 );
  if (!bEthLink)
  {
    FreeBuffer(pb);
    OSSemPost(&SemSend);
    return;
  }
  int i;
  int j;
  WORD len;
  for ( j = 0; j < NUM_TX_BD ; j++ )
  {
    i = ( j + nTx ) % NUM_TX_BD;
    if ( ( TxBD[i].flags & TXBD_Flag_Ready ) == 0 )
    {
      len = pb->usedsize;
      if ( len < ETH_MIN_SIZE )
      {
         len = ETH_MIN_SIZE;
      }
      TxBD[i].length = len;
      TxBD[i].address = ( unsigned long ) ( pb->pData );
      TxBDpp[i] = pb;
      pb->bBufferFlag = ( BYTE ) i;
      USER_ENTER_CRITICAL();
      TxBDFlag[i] = TRUE;
      TxBD[i].flags |= TXBD_Flag_NormalSend;
      asm(" nop");
      sim.fec.tdar = 0x00000001;
      nTx = ( i + 1 ) % NUM_TX_BD;
      USER_EXIT_CRITICAL();
//      break;             **** removed 11/01/05 DJM ****
      return;
    }
  }
//  if ( i == NUM_TX_BD )  **** removed 11/01/05 DJM ****
  FreeBuffer( pb );
}

DWORD SetMII( BYTE addr, WORD value )
{
  DWORD v;
  v = 0x50020001;               /* For write value, 0x60020000 for read */
  v |= ( 31 << 23 );
  v |= ( addr ) << 18;
  v |= value;
  sim.fec.mdata = v;
  OSTimeDly( 1 );
  v = sim.fec.mdata;
  v &= 0xFFFF;
  return v;
}

void ResetEnet()
{
  sim.gpio.paspar |= 0x0F00;
  sim.fec.ecr = 0x01;
  while ( sim.fec.ecr & 0x01 );
  sim.fec.mscr = 0x0D;            // Set MII speeed
  sim.fec.eimr = 0;
  sim.fec.eir = 0xFFFFFFFF;       //clear all flags
// ****************** Manual Configuration added 11/09/05 DJM ******************
//  sim.fec.mdata = 0x50023101;   // replaced with manual configuration
  extern WORD ManEnetConfig;
  DWORD MiiConfig;
  if       (ManEnetConfig == 1) MiiConfig = 0x50020000;  // 10 Mb/sec half
  else if  (ManEnetConfig == 2) MiiConfig = 0x50020100;  // 10 Mb/sec full
  else if  (ManEnetConfig == 3) MiiConfig = 0x50022000;  // 100 Mb/sec hald
  else if  (ManEnetConfig == 4) MiiConfig = 0x50022100;  // 100 Mb/sec full
  else                          MiiConfig = 0x50023300;  // auto negotiate
  sim.fec.mdata = MiiConfig;
// ************************* End Manual Configuration **************************
  DWORD DONT_HANG=0;
  while (((sim.fec.eir & 0x00800000) == 0) && (DONT_HANG < 5000000)) DONT_HANG++;

  sim.fec.eir = 0xFFFFFFFF;      //clear all flags
  sim.fec.mdata = 0x60060000;    // Set to read PHY status register 1
  while (((sim.fec.eir & 0x00800000) == 0) && (DONT_HANG < 5000000)) DONT_HANG++;
  DWORD phy_data=sim.fec.mdata;
//  if (phy_data & 0x020 )   // Removed check for auto negotiate complete DJM
  if (phy_data & 0x04 )      // Check link status 11/9/05 DJM
  {
    bEthLink = TRUE;
    ResetEnetPart2();
  }
  else
  {
    bEthLink = FALSE;
  }
}

void ResetEnetPart2()
{
  bEverHadLink = TRUE;  //*** moved from ProcessDuplexStatus() 11/01/05 DJM ***

  unsigned long i;
  DWORD DONT_HANG = 0;
  sim.fec.eir = 0xFFFFFFFF; 
  sim.fec.mdata = 0x60020000;
  while (((sim.fec.eir & 0x00800000) == 0) && (DONT_HANG < 5000000)) DONT_HANG++;
  DWORD phy_data = sim.fec.mdata;

  sim.fec.iaur = 0;
  sim.fec.ialr = 0;
  sim.fec.gaur = 0;
  sim.fec.galr = 0;
  sim.fec.opd = 0x00011000;
  sim.fec.tfwr = 0x03;
  sim.fec.mibc = 0x80000000;
  PDWORD pdw;
  pdw = ( PDWORD ) & ( sim.fec_rmon_t );
  for ( DWORD i = 0; i < ( sizeof( sim.fec_rmon_t ) / 4 ); i++ )
  {
    pdw[i] = 0;
  }
  pdw = ( PDWORD ) & ( sim.fec_ieee_t );
  for ( DWORD i = 0; i < ( sizeof( sim.fec_ieee_t ) / 4 ); i++ )
  {
    pdw[i] = 0;
  }
  pdw = ( PDWORD ) & ( sim.fec_rmon_r );
  for ( DWORD i = 0; i < ( sizeof( sim.fec_rmon_r ) / 4 ); i++ )
  {
    pdw[i] = 0;
  }
  pdw = ( PDWORD ) & ( sim.fec_ieee_r );
  for ( DWORD i = 0; i < ( sizeof( sim.fec_ieee_r ) / 4 ); i++ )
  {
    pdw[i] = 0;
  }
  sim.fec.mibc = 0x00000000;
  sim.fec.palr = *( ( unsigned long * ) ( gConfigRec.mac_address ) );
  sim.fec.paur = *( ( unsigned long * ) ( &( gConfigRec.mac_address[4] ) ) );
  sim.fec.emrbr = ETH_MAX_SIZE + 18 + 16;
  sim.fec.erdsr = ( unsigned long ) RxBD;
  sim.fec.etdsr = ( unsigned long ) TxBD;

  bEthDuplex= (( phy_data & 0x100 ) != 0);
  bEth100Mb=(( phy_data & 0x2000 ) != 0);
  if (bEthDuplex)
  {
    sim.fec.tcr = 0x04;
    sim.fec.rcr=((ETH_MAX_SIZE+18)<<16)+0x04;
  }
  else
  {
    sim.fec.tcr = 0x00;
    sim.fec.rcr=((ETH_MAX_SIZE+18)<<16)+0x06;
  }

  for ( i = 0; i < NUM_RX_BD ; i++ )
  {
    PoolPtr pp = GetBuffer();
    RxBD[i].length = ETHER_BUFFER_SIZE;
    RxBD[i].address = ( unsigned long ) ( pp->pData );
    RxBDpp[i] = pp;
    pp->bBuffer_state = BO_RXBUF;
    ChangeOwner( pp );
    pp->bBufferFlag = ( BYTE ) i;
    RxBD[i].flags = RXBD_Flag_Empty;
    RxBDFlag[i] = TRUE;
  }
  last_rxbd = 0;
  RxBD[i - 1].flags = RXBD_Flag_Empty | RXBD_Flag_Wrap;
  for ( i = 0; i < NUM_TX_BD ; i++ )
  {
    TxBD[i].flags = 0;
    TxBD[i].length = 0;
    TxBDpp[i] = NULL;
  }
  TxBD[i - 1].flags = TXBD_Flag_Wrap;
  sim.fec.ecr = 0x02;
  sim.fec.rdar = 0x00000001;

//**************** Interrupt Level/Priority Changed 11/15/05 DJM **************
//  SetIntc( 0, ( unsigned long ) & rxenetisr, 27, 1, 7 ); 
//  SetIntc( 0, ( unsigned long ) & rxenetisr, 28, 1, 7 ); 
//  SetIntc( 0, ( unsigned long ) & txenetisr, 23, 1, 4 ); 
//  sim.fec.eimr = ( FEC_ISR_MASK_TXF | FEC_ISR_MASK_RXF | FEC_ISR_MASK_RXB );
  SetIntc( 0, ( unsigned long ) & rxenetisr, 27, 4, 7 );  //RXF Level 4 Prio 7
  SetIntc( 0, ( unsigned long ) & rxenetisr, 28, 4, 6 );  //RXB Level 4 Prio 6
  SetIntc( 0, ( unsigned long ) & txenetisr, 23, 4, 5 );  //TXF Level 4 Prio 5
  SetIntc( 0, ( unsigned long ) & txenetisr, 24, 4, 4 );  //TXB Level 4 Prio 4
  sim.fec.eimr = ( FEC_ISR_MASK_TXF | FEC_ISR_MASK_TXB |
                   FEC_ISR_MASK_RXF | FEC_ISR_MASK_RXB );
//******************************************************************************

  vector_base.table[0x5F] = ( long ) & rxenetisr;
  SetSR_IntLevel( 0 );
  sim.fec.mdata = 0x60060000;
  OSTimeDly( 10 );
}

void WarmEnetReset()
{
  int i;
  bEthLink=FALSE;
  bEverHadLink=FALSE;
  sim.fec.ecr = 0x00;      //disable ethernet MAC
  sim.fec.eimr = 0;        //clear interrupt mask
  for ( i = 0; i < NUM_RX_BD ; i++ )
  {
    if( RxBDpp[i] ) 
    {
      FreeBuffer(RxBDpp[i]);
      RxBDpp[i]=0;
    }
  }  
  for ( i = 0; i < NUM_TX_BD ; i++ )
  {
    if( TxBDpp[i] )
    {
      FreeBuffer(TxBDpp[i]);
      TxBDpp[i]=0;
    }
  }
  nTx = 0;
  nRx = 0;
  OSTimeDly(1);
  ResetEnet();
}

void StopEnet()
{
  sim.fec.eimr = 0;
  sim.fec.ecr = 0x01;
  while ( sim.fec.ecr & 0x01 );
  sim.fec.eimr = 0;
  sim.fec.eir = 0xFFFFFFFF;
}

BOOL InitializeEthernet()
{
  read_eth_addr( &EthernetMac );
//  OSSemInit( &SemSend, 1 );          //removed 11/08/05 DJM
  OSSemInit( &SemSend, NUM_TX_BD );    //added 11/08/05 DJM
  OSFifoInit( &FecFifo );
  OSTaskCreate( Ether_ProcessTask,
                ( void * ) 0,
                ( void * ) &EtherSendStk[ETHER_SEND_STK_SIZE],
                ( void * ) EtherSendStk,
                ETHER_SEND_PRIO );
  ResetEnet();
  return TRUE;
}

void EthEnableMulticast( MACADR adr );
void EthDisableMulticast( MACADR adr );

void EthMultifunc( MACADR adr, BOOL enab )
{
  if ( enab )
    EthEnableMulticast( adr );
  else
    EthDisableMulticast( adr );
}

InterfaceBlock EthernetBlock;

int AddEthernetInterface()
{
  read_eth_addr( &( EthernetBlock.theMac ) );
  EthernetBlock.netIP = gConfigRec.ip_Addr;
  EthernetBlock.netIpMask = gConfigRec.ip_Mask;
  EthernetBlock.netIpGate = gConfigRec.ip_GateWay;
  EthernetBlock.netDNS = gConfigRec.ip_DNS_server;
  EthernetBlock.send_func = &EthTransmitBuffer;
  EthernetBlock.kill_if = &StopEnet;
  EthernetBlock.enab_multicast = &EthMultifunc;
  EthernetBlock.InterfaceName = "Ethernet";
  EthernetBlock.config_num = 0;
  int ifv = RegisterInterface( &EthernetBlock );
  myInterfaceNumber = ifv;
  InitializeEthernet();
  return ifv;
}

Notes on changes to Netburner software

1) Required to set Xilinx variables to fixed hardware address

in MOD5282\lib\sys.ld add

xil = 0xA0000000;    //add hardware address of Xilinx


2) 10/30/05 Fix bug in Ethernet buffers that allow out of sequence UDP packets. Bug reported to Netburner.

in MOD5282\system\ethnet.cpp INTERRUPT( rxenetisr, 0x2400 )

for ( int i = 0; i < NUM_RX_BD; i++ )  //remove in FEC_ISR_MASK_RXF
last_rxbd = i;                         //add in FEC_ISR_MASK_RXB


3) 10/21/05 Patch to allow viewing of all received packets by routing all packets to FIFO TempFecFifo in place of FecFifo

in declarations of MOD5282\system\ethernet.cpp 

//static OS_FIFO FecFifo;       //uncomment to return to normal
extern OS_FIFO FecFifo;         //Temporary 10/21/05 DJM 
extern OS_FIFO TempFecFifo;     //Temporary 10/21/05 DJM 
extern WORD DisplayEthernet;    //Temporary 10/26/05 DJM 

in MOD5282\system\ethnet.cpp INTERRUPT( rxenetisr, 0x2400 )

in FEC_ISR_MASK_RXF and FEC_ISR_MASK_RXB 

//OSFifoPost( &FecFifo, ( OS_FIFO_EL * ) ( RxBDpp[i] ) );

if (DisplayEthernet)
  OSFifoPost( &TempFecFifo, ( OS_FIFO_EL * ) ( RxBDpp[i] ) );
else
  OSFifoPost( &FecFifo, ( OS_FIFO_EL * ) ( RxBDpp[i] ) );


4) 10/27/05 Set flag to turn on 

in declarations of \include\predef.h

#define UCOS_STACKCHECK (1)   //add
#define UCOS_TASKLIST (1)     //add


5) 10/30/05 Patch to allow viewing of pool buffers by diagnostics. Pool buffers are declared static to hide from outside programs

in declarations of \system\buffers.cpp
 
pool_buffer static_pool_buffers[BUFFER_POOL_SIZE];
//static pool_buffer static_pool_buffers[BUFFER_POOL_SIZE];





  
/* Rev:$Revision: 1.1.1.1 $ */  

#ifndef _ETHER_VARS_H
#define _EHTER_VARS_H

#define IP_20BYTE_ID  (0x4500)
#define ETHER_TYPE_IP (0x0800)
#define ETHER_TYPE_ARP (0x0806)



#define NUM_TX_BD (3)
#define NUM_RX_BD (10)

extern volatile int nTx;
extern volatile int nRx;

extern volatile EtherBD TxBD[NUM_TX_BD] __attribute__( ( aligned( 16 ) ) );
extern volatile EtherBD RxBD[NUM_RX_BD] __attribute__( ( aligned( 16 ) ) );;

extern volatile PoolPtr RxBDpp[NUM_RX_BD];
extern volatile PoolPtr TxBDpp[NUM_TX_BD];

extern volatile BYTE RxBDFlag[NUM_RX_BD];
extern volatile BYTE TxBDFlag[NUM_TX_BD];     //added 11/08/05 DJM
//extern volatile BYTE TxBDFlag[NUM_RX_BD];   //removed 11/08/05 DJM

extern int last_rxbd;

extern DWORD LastIsrStatus;
extern DWORD RxPosts;
extern DWORD TxcPosts;


extern BOOL bEthLink; /* do we have link? */          
extern BOOL bEthDuplex; /* Are we setup for duplex? */
extern BOOL bEth100Mb; /* Are we 100Mb? */ 


#endif



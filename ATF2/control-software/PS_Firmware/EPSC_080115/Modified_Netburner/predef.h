/* Rev:$Revision: 1.1.1.1 $ */  

/******************************************************************************
 *
 * Copyright 1998-2004 NetBurner  ALL RIGHTS RESERVED
 *   Permission is hereby granted to purchasers of NetBurner Hardware
 *   to use or modify this computer program for any use as long as the
 *   resultant program is only executed on NetBurner provided hardware.
 *
 *   No other rights to use this program or it's derivitives in part or
 *   in whole are granted.
 *
 *   It may be possible to license this or other NetBurner software for
 *        use on non NetBurner Hardware.
 *   Please contact Licensing@Netburner.com for more infomation.
 *
 *   NetBurner makes no representation or warranties
 *   with respect to the performance of this computer program, and
 *   specifically disclaims any responsibility for any damages,
 *   special or consequential, connected with the use of this program.
 *
 *---------------------------------------------------------------------
 * NetBurner
 * 5405 Morehouse Dr #350
 * San Diego Ca, 92121
 *
 * information available at:  http://www.netburner.com
 * E-Mail info@netburner.com
 *
 * Support is availible: E-Mail support@netburner.com
 *
 *****************************************************************************/

//****** Turned on 10/27/05 by DJM ******
#define UCOS_STACKCHECK (1)
//#define UCOS_TASKLIST (1)  //removed 07/11/06 DJM

/* Use these constants to turn debug features on and off */
/*
#define UCOS_STACKCHECK (1)
#define UCOS_TASKLIST (1)

#define _DEBUG
#define  BUFFER_DIAG (1)
*/

/*
#define  _FULL_DISCLOSURE (1)
#define  _DO_ETHER_DIAG (1)
*/

/* This release  Build on: $Date: 2008/03/17 20:16:13 $ */
/* This build revison tag: $Name:  $ */

/*
Uncomment to enable multi-home operation
#define MULTIHOME
*/

/* #define GATHER_RANDOM (1) Uncomment for SSL */
#define NB_VERSION_1_5
#define NB_VERSION_1_6
#define NB_VERSION_1_7
#define NB_VERSION_1_8
#define NB_VERSION_1_9
#define NB_MINOR_VERSION (8)
#define NB_VERSION_TEXT "1.98"


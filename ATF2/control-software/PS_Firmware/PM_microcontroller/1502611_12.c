// 1502966.C: Diagnostic software for card
// F091 POWER MODULE SLAC, code 150.2966  Vin(max)=40V  Iout(nom)=50A
// 12/18/07 Version 2.00 SLAC Modified code
// 06/04/08 Version 2.10 Changed lower limit of the -5V test from -5.0V to -5.5V
//                       and lower limit of the -15V test from -16.3V to -17.0V
//                       and upper limit of the -15V test from -13.0V to -12.5V

//******************  DESCRIPTION OF THE SOFTWARE   ****************************
//
//  1)  Reads ADC(AN0), input voltage (PWR_in)
//  2)  Reads ADC(AN1), voltage drop of input fuse (PWR_in - PWR_MOS) 
//  3)  Reads ADC(AN2), output over voltage (PWR_out) 
//  4)  Reads A5, thermoswitch chain
//  5)  Reads ADC(AN6), -15V supply
//  6)  Reads ADC(AN7), -5V supply
//  7)  Reads external reset line (C1), sets fault state
//  8)  Reads external enable (A4), sets module enable
//  9)  Flashes green LED if module not enabled (see table)
//  10) Repeat loop ( cycle time about 500uSec )
//
//  Red  Green LED    Module state
//  ----------------------------------------------------------
//  OFF  ON steady    power module ON
//  OFF  flash 1 hz   power module off, ready to enable
//  ON   flash 2 hz   module faulted, flash 1-6 times (fault #)
//
//  ANALOG CHANNELS:
//  AN0: +PWR_in    input DC voltage (TP54)
//  AN1: +PWR_MOS   voltage downstream of the input fuse (TP53)
//  AN2: +PWR_out   output DC voltage (TP51)
//  AN6: -15V       on board -15V supply (TP50)
//  AN7: -5V        on board -5V supply (TP49)
//
//  DIGITAL INPUTS:
//  PORT_A4:  /Ext_Enable   (low = Enable)
//  PORT_A5:  Th_sw         (high = fault)
//  PORT_C1:  /Reset_Fault  (low = Reset)
//
//  DIGITAL OUTPUTS:
//  PORT_C0:  LED_OK        (high = green LED ON)
//  PORT_C4:  MOS_Enable    (high = enable MOSFETs)
//  PORT_C5:  /FAULT        (low = fault, red LED ON)
//
//******************************************************************************

#include "1502611_12.h"

#include <stdlib.h>

#ROM 0x3f0 = {"12/18/2007"} // Write release date to the end of code memory
#ROM 0x3fe = {0x0200}       // Version number 02.00
#ROM 0x3ff = {0x3480}

// ****** registers for ADC readings ******
long vmis = 0;              //Variable of the measured voltage
long pwrin_mis = 0;         //Variable of the measured +PWR_in voltage
#DEFINE DELAY 10            // delay time (uS) for ADC multiplexer

// ****** Fault 1 to fault 6 registers, initialize to fault ******
unsigned int fault1 = 1;    // Vin out range
unsigned int fault2 = 1;    // fuse failed
unsigned int fault3 = 1;    // Vo overvoltage
unsigned int fault4 = 1;    // thermoswitches open
unsigned int fault5 = 1;    // -15V out range
unsigned int fault6 = 1;    // -5V out range

unsigned int fault  = 0;    // latched fault register

// ****** power on fault reset, number of cycles ******
unsigned long porcnt  = 20000;

//*********************  input voltage check (fault 1) *************************
// ADC Channel 0 (AN0) +PWR_in
// scale 1023 / 74.94V = 13.65 cnt/V
//****************************************************************************** 

#DEFINE PWRIN_H    819    // high limit 60V
#DEFINE PWRIN_L    300    // low limit 22V
#DEFINE PWRIN_DLY   20    // delay cycles
unsigned int delay1 = 0;  // delay counter

#inline void Check_PWR_IN( void )
{ 
  set_adc_channel( 0 );
  delay_us( DELAY );
  pwrin_mis = read_adc();

  //alarm if +PWR_in is out of functioning range
  if ( (pwrin_mis < PWRIN_L) || (pwrin_mis > PWRIN_H) )
  {
    if (delay1 > 0)  --delay1; else fault1 = 1;
  }
  else
  {
    if (delay1 < PWRIN_DLY) ++delay1; else fault1 = 0;
  }
}

//*****************  input fuse voltage drop check (fault 2) *******************
// ADC channel 1 (AN1) +PWR_MOS
// scale 1023 / 74.94V = 13.65 cnt/V
//******************************************************************************

#DEFINE FUSE2_DV    68    // delta volts 5V
#DEFINE FUSE2_DLY  100    // delay cycles
unsigned int delay2 = 0;  // delay counter

#inline void Check_fuse()
{
  set_adc_channel( 1 );
  delay_us( DELAY );
  vmis = read_adc();

  // alarm if (+PWR_in - +PWR_MOS) > FUSE2_DV
  if ( pwrin_mis > (vmis + FUSE2_DV) )
  {
    if (delay2 > 0)  --delay2; else fault2 = 1;
  }
  else
  {
    if (delay2 < FUSE2_DLY) ++delay2; else fault2 = 0;
  }
}

//*******************  output voltage check (fault3) ***************************
// ADC Channel 2 (AN2) Output voltage PWR_out
// scale 1023 / 51.49V = 19.86 cnt/V
//******************************************************************************

#DEFINE PWROUT_H    695   // high limit 35V
#DEFINE PWROUT_DLY  100   // delay cycles
unsigned int delay3 = 0;  // delay counter

#inline void Check_PWR_OUT()
{
  set_adc_channel( 2 );
  delay_us( DELAY );
  vmis = read_adc();

  // alarm if +PWR_out > PWROUT_H
  if ( vmis > PWROUT_H )
  {
    if (delay3 > 0)  --delay3; else fault3 = 1;
  }
  else
  {
    if (delay3 < PWROUT_DLY) ++delay3; else fault3 = 0;
  }
}

//*******************  thermoswitches check (fault 4) **************************
// PIN A5, alarm thermoswitch, high = fault
//******************************************************************************

#DEFINE TSW_DLY     25    // delay cycles
unsigned int delay4 = 0;  // delay counter

#inline void Check_Thm_Sw( void )
{
  if (input(PIN_A5))
  {
    if (delay4 > 0)  --delay4; else fault4 = 1;
  }
  else
  {
    if (delay4 < TSW_DLY) ++delay4; else fault4 = 0;
  }                   
}

//******************  Check -15V supply volts (fault5) *************************
// ADC channel 6 (AN6) -15V suplly voltage
// scale 1023 / 25.04V = 40.85 cnt/V  (0 = -20.04V, 1023 = +5.00V)
// threshold = 40.85 * (20.4V + V)
// hyst_v15   hysterisis reg, V15_HYST when fault, 0 when no fault
//******************************************************************************

#DEFINE V15_H      323    // high limit -12.5V - hyst_v15
#DEFINE V15_L      139    // low limit  -17.0V + hyst_v15
#DEFINE V15_HYST    12    // histerisis 0.3V
#DEFINE V15_DLY     25    // delay cycles

unsigned int delay5 = 0;  // delay counter
int hyst_v15 = V15_HYST;  // -15V hysterisis register

#inline void Check_V15( void )
{
  set_adc_channel( 6 );
  delay_us(DELAY);
  vmis = read_adc();

  if (vmis < (V15_L + hyst_v15) || vmis > (V15_H - hyst_v15) )
  {
    if (delay5 > 0) --delay5; else { fault5 = 1; hyst_v15 = V15_HYST; }
  }
  else 
  {
    if (delay5 < V15_DLY) ++delay5; else { fault5 = 0; hyst_v15 = 0; }
  }                   
}

//******************** Check -5V Supply volts (fault 6)*************************
// ADC channel 7 (AN7) -5V supply
// scale 1023 / 12.51V = 81.74 cnt/V (0 = -7.5V, 1023 = 5.0V)
// threshold = 81.74 * (7.5V + V)
// hyst_v5     hysterisis reg, V5_HYST when fault, 0 when no fault
//****************************************************************************** 

#DEFINE V5_H       286    // high limit -4.0V - hyst_v5
#DEFINE V5_L       163    // low limt   -5.5V + hyst_v5
#DEFINE V5_HYST     16    // histerisis 0.2V
#DEFINE V5_DLY      25    // delay cycles

unsigned int delay6 = 0;  // delay counter
int hyst_v5 = V5_HYST;    // -5V hysterisis register

#inline void Check_V5( void )
{
  set_adc_channel( 7 );
  delay_us( DELAY );
  vmis = read_adc();

  if ( vmis < (V5_L + hyst_v5) || vmis > (V5_H - hyst_v5) )
  {
    if (delay6 > 0) --delay6; else { fault6 = 1; hyst_v5 = V5_HYST; }
  }                                  
  else  // no fault
  {
    if (delay6 < V5_DLY) ++delay6; else { fault6 = 0; hyst_v5 = 0; }
  }                   
}

//***************************** Flash green LED ********************************
// Green LED flash times, in main loop cycles (500uS/cnt)
//******************************************************************************

#DEFINE LONG_ON    1000      // cycles for LED on in long flash
#DEFINE LONG_CYC   2000      // total cycles for long flash 
#DEFINE SHORT_ON    500      // cycles for LED on in fault flash
#DEFINE SHORT_CYC  1000      // total cycles for fault flash

unsigned int flashcnt = 1;   // green LED fault flash count
unsigned long cyccnt  = 0;   // cycle count for green led flash

#inline void Flash_Long( void )      // flash green LED at 1 Hz
{
  if (cyccnt < LONG_ON )
    output_high( PIN_C0 );           // green LED on
  else
    output_low( PIN_C0 );            // green LED off

  if ( ++cyccnt > LONG_CYC ) cyccnt = 0;
}

#inline void Flash_Flt( void )       // flash fault number on green LED 2 Hz
{
  if (cyccnt < SHORT_ON )            // short flash on green LED
    output_high( PIN_C0 );           // green LED on
  else
    output_low( PIN_C0 );            // green LED off

  // every 'fault' number of flashes, generate long off period

  if (flashcnt < fault )   // short off time if flashcnt < fault
  {
    if ( ++cyccnt > SHORT_CYC )
    {
      cyccnt = 0;
      ++flashcnt;
    }
  }
  else                     // else generate long off period
  { 
    if ( ++cyccnt > LONG_CYC )
    {
      cyccnt = 0;
      flashcnt = 1;
    }
  }
}

//************************* Initialize PIC Hardware **************************** 

#inline void Init_PIC( void )
{
  setup_adc_ports(sAN0|sAN1|sAN2|sAN6|sAN7|VSS_VDD);
  setup_adc(ADC_CLOCK_INTERNAL);
  setup_timer_0(RTCC_INTERNAL);
  setup_timer_1(T1_DISABLED);
  setup_comparator(NC_NC_NC_NC);
  setup_vref(FALSE);
  port_a_pullups (0x20);  // enable of internal pull-up of the port A5
                          // Don't enable the pull-up
                          // for A4 (Ext_Enable). Port C doesn't have pull-ups.
}

//*****************************************************************************
//*********************************   MAIN    *********************************
//*****************************************************************************

void main()
{
  Init_PIC();             // initialize PIC hardware
  setup_wdt(WDT_72MS);    // set timeout time of watchdog
  
  while (TRUE)            // repeat forever
  {

    Check_PWR_IN();       // check input voltage, set fault1
    Check_Fuse();         // check volatge drop on input fuse, set fault 2
    Check_PWR_OUT();      // check output voltage, set fault 3
    Check_Thm_Sw();       // check thermal switch, set fault 4
    Check_V15();          // check -15V supply voltage, set fault 5    
    Check_V5();           // check -5V supply voltage, set fault 6

    //******* Disable latch for first 10 seconds after power on ********
    
    if ( porcnt > 0 ) { fault = 0; --porcnt; }

    //*********** Clear latch if external reset and no enable ***********

    if ( !input( PIN_C1 ) && input( PIN_A4 ) ) fault = 0;

    //************** If no latched fault, check all faults **************

    if ( fault == 0 )
    {
      if      ( fault1 != 0 ) fault = 1;
      else if ( fault2 != 0 ) fault = 2;
      else if ( fault3 != 0 ) fault = 3;
      else if ( fault4 != 0 ) fault = 4;
      else if ( fault5 != 0 ) fault = 5;
      else if ( fault6 != 0 ) fault = 6;
    }
    
    //***************** Set Outputs and flash green LED *****************

    if ( !input( PIN_A4 ) && (fault == 0))  // if ext enable and no fault
    {
      output_high(PIN_C0);        // set green LED on
      output_high(PIN_C4);        // enable module
      output_high(PIN_C5);        // clear fault (red LED off)      
    }
    else
    {
      output_low(PIN_C4);         // disable module
 
      if ( fault == 0 )           // no fault
      {
        output_high(PIN_C5);      // clear fault (red LED OFF)
        Flash_Long();             // flash green LED at 1 Hz
      }
      else                        // latched fault
      {
        output_low(PIN_C5);       // set fault (red LED ON)
        Flash_Flt();              // flash fault code on green LED
      }       
    }
    
    restart_wdt();                // restart of watchdog timer
  }   // end while (TRUE)
}   // end main()
  

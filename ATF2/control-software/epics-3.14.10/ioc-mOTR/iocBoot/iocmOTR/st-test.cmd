#!/home/atfopr/ATF2/control-software/epics-3.14.10/ioc-mOTR/bin/linux-x86_64/mOTR

## You may have to change mOTR to something else
## everywhere it appears in this file


< /home/atfopr/ATF2/control-software/epics-3.14.10/ioc-mOTR/iocBoot/iocmOTR/envPaths

errlogInit(20000)

cd ${TOP}

## Register all support components
dbLoadDatabase "dbd/mOTR.dbd"
mOTR_registerRecordDeviceDriver pdbbase


# --- Motor driver config
dbLoadTemplate "db/motor-test.substitutions"
dbLoadTemplate "db/XPSAux.substitutions"
# cards (total controllers)
XPSSetup(2)
# card, IP, PORT, number of axes, active poll period (ms), idle poll period (ms)
XPSConfig(0, "169.254.0.6", 5001, 8, 10, 5000)
XPSConfig(1, "169.254.0.7", 5001, 8, 10, 5000)
#XPSConfig(2, "169.254.0.8", 5001, 8, 10, 5000)
# asynPort, IP address, IP port, poll period (ms)
XPSAuxConfig("XPS_AUX1", "169.254.0.6", 5001, 50)
XPSAuxConfig("XPS_AUX2", "169.254.0.7", 5001, 50)
# asyn port, driver name, controller index, max. axes)
drvAsynMotorConfigure("XPS1", "motorXPS", 0, 8)
XPSInterpose("XPS1")
drvAsynMotorConfigure("XPS2", "motorXPS", 1, 8)
XPSInterpose("XPS2")
# card,  axis, groupName.positionerName, units
XPSConfigAxis(0,0,"X1.Pos",1)
XPSConfigAxis(0,1,"Y1.Pos",1)
XPSConfigAxis(0,2,"F1.Pos",1)
XPSConfigAxis(0,3,"X2.Pos",1)
XPSConfigAxis(0,4,"Y2.Pos",1)
XPSConfigAxis(0,5,"F2.Pos",1)
XPSConfigAxis(0,6,"M1.Pos",1)
XPSConfigAxis(0,7,"M2.Pos",1)
XPSConfigAxis(1,0,"X3.Pos",1)
XPSConfigAxis(1,1,"Y3.Pos",1)
XPSConfigAxis(1,2,"F3.Pos",1)
XPSConfigAxis(1,3,"X4.Pos",1)
XPSConfigAxis(1,4,"Y4.Pos",1)
XPSConfigAxis(1,5,"F4.Pos",1)
XPSConfigAxis(1,6,"M3.Pos",1)
XPSConfigAxis(1,7,"M4.Pos",1)

# Processed data records
dbLoadRecords("db/iocAdminSoft.db","IOC=mOTR")

iocInit()


< iocBoot/iocmOTR/motorSetup


#!../../bin.linux-x86/mOTR
errlogInit(5000)
< envPaths
# Tell EPICS all about the record types, device-support modules, drivers,
# etc. in this build from CARS

cd ${TOP}

dbLoadDatabase "dbd/mOTR.dbd"
mOTR_registerRecordDeviceDriver pdbbase

### Motors
dbLoadTemplate "db/motor-xps.substitutions"
dbLoadTemplate "db/XPSAux.substitutions"

# cards (total controllers)
XPSSetup(1)

# card, IP, PORT, number of axes, active poll period (ms), idle poll period (ms)
XPSConfig(0, "192.168.0.160", 5001, 1, 10, 5000)

# asynPort, IP address, IP port, poll period (ms)
XPSAuxConfig("XPS_AUX1", "192.168.0.160", 5001, 50)
#asynSetTraceMask("XPS_AUX1", 0, 255)
#asynSetTraceIOMask("XPS_AUX1", 0, 2)

# asyn port, driver name, controller index, max. axes)
drvAsynMotorConfigure("XPS1", "motorXPS", 0, 1)
XPSInterpose("XPS1")

# card,  axis, groupName.positionerName, units
XPSConfigAxis(0,0,"X1.Pos",1)
#XPSConfigAxis(0,1,"Y1.Pos",20480)
#XPSConfigAxis(0,2,"F1.Pos",20480)

iocInit

dbpf mOTR:motor1:MAX_JERK_TIME 0.05
dbpf mOTR:motor1:MIN_JERK_TIME 0.005
dbpf mOTR:motor1.VMAX 500
# Use encoder / PV readback
dbpf mOTR:motor1.UEIP 1

#!../../bin/linux-x86/mOTR

## You may have to change mOTR to something else
## everywhere it appears in this file

< envPaths

errlogInit(20000)

cd ${TOP}

## Register all support components
dbLoadDatabase "dbd/mOTR.dbd"
mOTR_registerRecordDeviceDriver pdbbase


prosilicaConfig("PS1", 107634, 50, 200000000)
asynSetTraceIOMask("PS1",0,2)
#asynSetTraceMask("PS1",0,255)
dbLoadRecords("$(AREA_DETECTOR)/ADApp/Db/ADBase.template",   "P=mOTR:,R=cam1:,PORT=PS1,ADDR=0,TIMEOUT=1")
dbLoadRecords("$(AREA_DETECTOR)/ADApp/Db/NDFile.template",   "P=mOTR:,R=cam1:,PORT=PS1,ADDR=0,TIMEOUT=1")
dbLoadRecords("$(AREA_DETECTOR)/ADApp/Db/prosilica.template","P=mOTR:,R=cam1:,PORT=PS1,ADDR=0,TIMEOUT=1")

# Create a standard arrays plugin, set it to get data from first Prosilica driver.
NDStdArraysConfigure("PS1Image", 5, 0, "PS1", 0, -1)
dbLoadRecords("$(AREA_DETECTOR)/ADApp/Db/NDPluginBase.template","P=mOTR:,R=image1:,PORT=PS1Image,ADDR=0,TIMEOUT=1,NDARRAY_PORT=PS1,NDARRAY_ADDR=0")
dbLoadRecords("$(AREA_DETECTOR)/ADApp/Db/NDStdArrays.template", "P=mOTR:,R=image1:,PORT=PS1Image,ADDR=0,TIMEOUT=1,TYPE=Int16,FTVL=SHORT,NELEMENTS=1228800")
NDFileTIFFConfigure("PS1TIFF", 20, 0, "PS1", 0)
dbLoadRecords("$(AREA_DETECTOR)/ADApp/Db/NDPluginBase.template","P=mOTR:,R=TIFF1:,PORT=PS1TIFF,ADDR=0,TIMEOUT=1,NDARRAY_PORT=PS1,NDARRAY_ADDR=0")
dbLoadRecords("$(AREA_DETECTOR)/ADApp/Db/NDFile.template",      "P=mOTR:,R=TIFF1:,PORT=PS1TIFF,ADDR=0,TIMEOUT=1")
dbLoadRecords("$(AREA_DETECTOR)/ADApp/Db/NDFileTIFF.template",  "P=mOTR:,R=TIFF1:,PORT=PS1TIFF,ADDR=0,TIMEOUT=1")

iocInit()

cd ${TOP}/iocBoot/${IOC}
iocInit
#!/home/atfopr/ATF2/control-software/epics-3.14.10/ioc-mOTR/bin/linux-x86_64/mOTR

< envPaths

# setup environment variables for this IOC
epicsEnvSet("EPICS_CA_AUTO_ADDR_LIST","NO")
epicsEnvSet("EPICS_CA_SERVER_PORT","${FS_PORT}"
epicsEnvSet("EPICS_CAS_BEACON_ADDR_LIST","localhost")

cd ${TOP}

## Register all support components
dbLoadDatabase "dbd/mOTR.dbd"
mOTR_registerRecordDeviceDriver pdbbase

# --- Camera config
# -- Create simDetector drivers and records
# - CAM 1
simDetectorConfig("PS1", 1280, 960, 1, 500, -1)
dbLoadRecords("$(AREA_DETECTOR)/ADApp/Db/ADBase.template",     "P=mOTR:,R=cam1:,PORT=PS1,ADDR=0,TIMEOUT=1")
dbLoadRecords("$(AREA_DETECTOR)/ADApp/Db/simDetector.template","P=mOTR:,R=cam1:,PORT=PS1,ADDR=0,TIMEOUT=1")
NDStdArraysConfigure("PS1Image", 5, 0, "PS1", 0)
dbLoadRecords("$(AREA_DETECTOR)/ADApp/Db/NDPluginBase.template","P=mOTR:,R=image1:,PORT=PS1Image,ADDR=0,TIMEOUT=1,NDARRAY_PORT=PS1,NDARRAY_ADDR=0")
dbLoadRecords("$(AREA_DETECTOR)/ADApp/Db/NDStdArrays.template", "P=mOTR:,R=image1:,PORT=PS1Image,ADDR=0,TIMEOUT=1,TYPE=Int16,FTVL=SHORT,NELEMENTS=1228800")
# - CAM 2
simDetectorConfig("PS2", 1280, 960, 1, 500, -1)
dbLoadRecords("$(AREA_DETECTOR)/ADApp/Db/ADBase.template",     "P=mOTR:,R=cam2:,PORT=PS2,ADDR=0,TIMEOUT=1")
dbLoadRecords("$(AREA_DETECTOR)/ADApp/Db/simDetector.template","P=mOTR:,R=cam2:,PORT=PS2,ADDR=0,TIMEOUT=1")
NDStdArraysConfigure("PS2Image", 5, 0, "PS2", 0)
dbLoadRecords("$(AREA_DETECTOR)/ADApp/Db/NDPluginBase.template","P=mOTR:,R=image2:,PORT=PS2Image,ADDR=0,TIMEOUT=1,NDARRAY_PORT=PS2,NDARRAY_ADDR=0")
dbLoadRecords("$(AREA_DETECTOR)/ADApp/Db/NDStdArrays.template", "P=mOTR:,R=image2:,PORT=PS2Image,ADDR=0,TIMEOUT=1,TYPE=Int32,FTVL=LONG,NELEMENTS=1228800")
# - CAM 3
simDetectorConfig("PS3", 1280, 960, 1, 500, -1)
dbLoadRecords("$(AREA_DETECTOR)/ADApp/Db/ADBase.template",     "P=mOTR:,R=cam3:,PORT=PS3,ADDR=0,TIMEOUT=1")
dbLoadRecords("$(AREA_DETECTOR)/ADApp/Db/simDetector.template","P=mOTR:,R=cam3:,PORT=PS3,ADDR=0,TIMEOUT=1")
NDStdArraysConfigure("PS3Image", 5, 0, "PS3", 0)
dbLoadRecords("$(AREA_DETECTOR)/ADApp/Db/NDPluginBase.template","P=mOTR:,R=image3:,PORT=PS1Image,ADDR=0,TIMEOUT=1,NDARRAY_PORT=PS3,NDARRAY_ADDR=0")
dbLoadRecords("$(AREA_DETECTOR)/ADApp/Db/NDStdArrays.template", "P=mOTR:,R=image3:,PORT=PS1Image,ADDR=0,TIMEOUT=1,TYPE=Int32,FTVL=LONG,NELEMENTS=1228800")
# - CAM 4
simDetectorConfig("PS4", 1280, 960, 1, 500, -1)
dbLoadRecords("$(AREA_DETECTOR)/ADApp/Db/ADBase.template",     "P=mOTR:,R=cam4:,PORT=PS1,ADDR=0,TIMEOUT=1")
dbLoadRecords("$(AREA_DETECTOR)/ADApp/Db/simDetector.template","P=mOTR:,R=cam4:,PORT=PS1,ADDR=0,TIMEOUT=1")
NDStdArraysConfigure("PS4Image", 5, 0, "PS4", 0)
dbLoadRecords("$(AREA_DETECTOR)/ADApp/Db/NDPluginBase.template","P=mOTR:,R=image4:,PORT=PS1Image,ADDR=0,TIMEOUT=1,NDARRAY_PORT=PS4,NDARRAY_ADDR=0")
dbLoadRecords("$(AREA_DETECTOR)/ADApp/Db/NDStdArrays.template", "P=mOTR:,R=image4:,PORT=PS1Image,ADDR=0,TIMEOUT=1,TYPE=Int32,FTVL=LONG,NELEMENTS=1228800")

# --- Motor driver config
dbLoadTemplate("db/motor-xps-sim.substitutions")
# Create simulated motors: ( start card , start axis , low limit, high limit, home posn, # cards, # axes to setup)
motorSimCreate( 0, 0, -100000, 100000, 0, 2, 6 )
# Setup the Asyn layer (portname, low-level driver drvet name, card, number of axes on card)
drvAsynMotorConfigure("XPS1", "motorSim", 0, 6)
drvAsynMotorConfigure("XPS2", "motorSim", 1, 6)

# --- Simulated Newport XPS Aux & Binary IO's
dbLoadRecords("db/simAuxXPS.vdb", "X2V=0.001,Y2V=0.001,F2V=0.001")

# processed waveform data
dbLoadRecords("db/procData.vdb","N=1")
dbLoadRecords("db/procData.vdb","N=2")
dbLoadRecords("db/procData.vdb","N=3")
dbLoadRecords("db/procData.vdb","N=4")

iocInit()

# --- Put Image Data Arrays into sim mode
dbpf mOTR:image1:ArrayData.SIMM YES
dbpf mOTR:image2:ArrayData.SIMM YES
dbpf mOTR:image3:ArrayData.SIMM YES
dbpf mOTR:image4:ArrayData.SIMM YES
dbpf mOTR:image1:ArrayData.PROC 1
dbpf mOTR:image2:ArrayData.PROC 1
dbpf mOTR:image3:ArrayData.PROC 1
dbpf mOTR:image4:ArrayData.PROC 1

< iocBoot/iocmOTR/otrInsertTime
< iocBoot/iocmOTR/prosilicaSetup
< iocBoot/iocmOTR/motorSetup

# Dump db records into record file
dbl > dbList.txt


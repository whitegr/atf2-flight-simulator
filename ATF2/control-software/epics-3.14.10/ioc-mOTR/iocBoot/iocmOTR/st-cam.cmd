#!/home/atfopr/ATF2/control-software/epics-3.14.10/ioc-mOTR/bin/linux-x86_64/mOTR

## You may have to change mOTR to something else
## everywhere it appears in this file


< /home/atfopr/ATF2/control-software/epics-3.14.10/ioc-mOTR/iocBoot/iocmOTR/envPaths

errlogInit(20000)

#epicsEnvSet("EPICS_CA_AUTO_ADDR_LIST","NO")
#epicsEnvSet("EPICS_CA_SERVER_PORT","5064")

cd ${TOP}

## Register all support components
dbLoadDatabase "dbd/mOTR.dbd"
mOTR_registerRecordDeviceDriver pdbbase


# Prosilica Camera config
prosilicaConfig("PS1", 118345, 10, 50000000)
dbLoadRecords("$(AREA_DETECTOR)/ADApp/Db/ADBase.template",   "P=mOTR:,R=cam1:,PORT=PS1,ADDR=0,TIMEOUT=1")
dbLoadRecords("$(AREA_DETECTOR)/ADApp/Db/NDFile.template",   "P=mOTR:,R=cam1:,PORT=PS1,ADDR=0,TIMEOUT=1")
dbLoadRecords("$(AREA_DETECTOR)/ADApp/Db/prosilica.template","P=mOTR:,R=cam1:,PORT=PS1,ADDR=0,TIMEOUT=1")
NDStdArraysConfigure("PS1Image", 5, 0, "PS1", 0)
dbLoadRecords("$(AREA_DETECTOR)/ADApp/Db/NDPluginBase.template","P=mOTR:,R=image1:,PORT=PS1Image,ADDR=0,TIMEOUT=1,NDARRAY_PORT=PS1,NDARRAY_ADDR=0")
dbLoadRecords("$(AREA_DETECTOR)/ADApp/Db/NDStdArrays.template", "P=mOTR:,R=image1:,PORT=PS1Image,ADDR=0,TIMEOUT=1,TYPE=Int16,FTVL=SHORT,NELEMENTS=1228800")
prosilicaConfig("PS2", 118346, 10, 50000000)
dbLoadRecords("$(AREA_DETECTOR)/ADApp/Db/ADBase.template",   "P=mOTR:,R=cam2:,PORT=PS2,ADDR=0,TIMEOUT=1")
dbLoadRecords("$(AREA_DETECTOR)/ADApp/Db/NDFile.template",   "P=mOTR:,R=cam2:,PORT=PS2,ADDR=0,TIMEOUT=1")
dbLoadRecords("$(AREA_DETECTOR)/ADApp/Db/prosilica.template","P=mOTR:,R=cam2:,PORT=PS2,ADDR=0,TIMEOUT=1")
NDStdArraysConfigure("PS2Image", 5, 0, "PS2", 0)
dbLoadRecords("$(AREA_DETECTOR)/ADApp/Db/NDPluginBase.template","P=mOTR:,R=image2:,PORT=PS2Image,ADDR=0,TIMEOUT=1,NDARRAY_PORT=PS2,NDARRAY_ADDR=0")
dbLoadRecords("$(AREA_DETECTOR)/ADApp/Db/NDStdArrays.template", "P=mOTR:,R=image2:,PORT=PS2Image,ADDR=0,TIMEOUT=1,TYPE=Int16,FTVL=SHORT,NELEMENTS=1228800")
prosilicaConfig("PS3", 143520, 10, 50000000)
dbLoadRecords("$(AREA_DETECTOR)/ADApp/Db/ADBase.template",   "P=mOTR:,R=cam3:,PORT=PS3,ADDR=0,TIMEOUT=1")
dbLoadRecords("$(AREA_DETECTOR)/ADApp/Db/NDFile.template",   "P=mOTR:,R=cam3:,PORT=PS3,ADDR=0,TIMEOUT=1")
dbLoadRecords("$(AREA_DETECTOR)/ADApp/Db/prosilica.template","P=mOTR:,R=cam3:,PORT=PS3,ADDR=0,TIMEOUT=1")
NDStdArraysConfigure("PS3Image", 5, 0, "PS3", 0)
dbLoadRecords("$(AREA_DETECTOR)/ADApp/Db/NDPluginBase.template","P=mOTR:,R=image3:,PORT=PS3Image,ADDR=0,TIMEOUT=1,NDARRAY_PORT=PS3,NDARRAY_ADDR=0")
dbLoadRecords("$(AREA_DETECTOR)/ADApp/Db/NDStdArrays.template", "P=mOTR:,R=image3:,PORT=PS3Image,ADDR=0,TIMEOUT=1,TYPE=Int16,FTVL=SHORT,NELEMENTS=1228800")
prosilicaConfig("PS4", 118341, 10, 50000000)
dbLoadRecords("$(AREA_DETECTOR)/ADApp/Db/ADBase.template",   "P=mOTR:,R=cam4:,PORT=PS4,ADDR=0,TIMEOUT=1")
dbLoadRecords("$(AREA_DETECTOR)/ADApp/Db/NDFile.template",   "P=mOTR:,R=cam4:,PORT=PS4,ADDR=0,TIMEOUT=1")
dbLoadRecords("$(AREA_DETECTOR)/ADApp/Db/prosilica.template","P=mOTR:,R=cam4:,PORT=PS4,ADDR=0,TIMEOUT=1")
NDStdArraysConfigure("PS4Image", 5, 0, "PS4", 0)
dbLoadRecords("$(AREA_DETECTOR)/ADApp/Db/NDPluginBase.template","P=mOTR:,R=image4:,PORT=PS4Image,ADDR=0,TIMEOUT=1,NDARRAY_PORT=PS4,NDARRAY_ADDR=0")
dbLoadRecords("$(AREA_DETECTOR)/ADApp/Db/NDStdArrays.template", "P=mOTR:,R=image4:,PORT=PS4Image,ADDR=0,TIMEOUT=1,TYPE=Int16,FTVL=SHORT,NELEMENTS=1228800")

# Processed data records
dbLoadRecords("db/procData.vdb","N=1")
dbLoadRecords("db/procData.vdb","N=2")
dbLoadRecords("db/procData.vdb","N=3")
dbLoadRecords("db/procData.vdb","N=4")
dbLoadRecords("db/emitData.db")
dbLoadRecords("db/iocAdminSoft.db","IOC=mOTR")
#dbLoadRecords("db/otrStatus.db","N=1,TARGET=mOTR:XPS1Aux3Bo2.RVAL,XV=mOTR:XPS1AuxAi0,YV=mOTR:XPS1AuxAi1,VERR=0.2")
#dbLoadRecords("db/otrStatus.db","N=2,TARGET=mOTR:XPS1Aux3Bo3.RVAL,XV=mOTR:XPS1AuxAi2,YV=mOTR:XPS1AuxAi3,VERR=0.2")
#dbLoadRecords("db/otrStatus.db","N=3,TARGET=mOTR:XPS2Aux3Bo2.RVAL,XV=mOTR:XPS2AuxAi0,YV=mOTR:XPS1AuxAi1,VERR=0.2")
#dbLoadRecords("db/otrStatus.db","N=4,TARGET=mOTR:XPS2Aux3Bo3.RVAL,XV=mOTR:XPS2AuxAi2,YV=mOTR:XPS1AuxAi3,VERR=0.2")


# IOC save/restore setup
#set_requestfile_path("./")
#set_savefile_path("./autosave")
#set_requestfile_path("$(AREA_DETECTOR)/ADApp/Db")
#set_pass0_restoreFile("auto_settings.sav")
#set_pass1_restoreFile("auto_settings.sav")
#save_restoreSet_status_prefix("mOTR:")
#dbLoadRecords("$(AUTOSAVE)/asApp/Db/save_restoreStatus.db", "P=mOTR:")

iocInit()


< iocBoot/iocmOTR/prosilicaSetup


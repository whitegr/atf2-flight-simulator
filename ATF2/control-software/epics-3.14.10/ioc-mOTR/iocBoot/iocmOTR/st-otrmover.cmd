#!/home/atfopr/ATF2/control-software/epics-3.14.10/ioc-mOTR/bin/linux-x86_64/mOTR

## You may have to change mOTR to something else
## everywhere it appears in this file


< /home/atfopr/ATF2/control-software/epics-3.14.10/ioc-mOTR/iocBoot/iocmOTR/envPaths

errlogInit(20000)

epicsEnvSet("EPICS_CA_ADDR_LIST","flight-simulator:5066")
#epicsEnvSet("EPICS_CA_SERVER_PORT","5065")

cd ${TOP}

## Register all support components
dbLoadDatabase "dbd/mOTR.dbd"
mOTR_registerRecordDeviceDriver pdbbase

# --- Admin PVs (for restart command)
dbLoadRecords("db/iocAdminSoft.db","IOC=mOTR-motor")

# --- Motor driver config
dbLoadTemplate "db/motor-xps.substitutions"
dbLoadTemplate "db/XPSAux.substitutions"
# cards (total controllers)
XPSSetup(4)
# card, IP, PORT, number of axes, active poll period (ms), idle poll period (ms)
XPSConfig(0, "169.254.0.6", 5001, 8, 10, 5000)
XPSConfig(1, "169.254.0.7", 5001, 8, 10, 5000)
XPSConfig(2, "169.254.0.8", 5001, 8, 10, 5000)
XPSConfig(3, "169.254.0.9", 5001, 8, 10, 5000)
# asynPort, IP address, IP port, poll period (ms)
XPSAuxConfig("XPS_AUX1", "169.254.0.6", 5001, 50)
XPSAuxConfig("XPS_AUX2", "169.254.0.7", 5001, 50)
XPSAuxConfig("XPS_AUX3", "169.254.0.8", 5001, 50)
XPSAuxConfig("XPS_AUX4", "169.254.0.9", 5001, 50)
# asyn port, driver name, controller index, max. axes)
drvAsynMotorConfigure("XPS1", "motorXPS", 0, 8)
XPSInterpose("XPS1")
drvAsynMotorConfigure("XPS2", "motorXPS", 1, 8)
XPSInterpose("XPS2")
drvAsynMotorConfigure("XPS3", "motorXPS", 2, 8)
XPSInterpose("XPS3")
drvAsynMotorConfigure("XPS4", "motorXPS", 3, 8)
XPSInterpose("XPS4")
# card,  axis, groupName.positionerName, units
XPSConfigAxis(0,0,"X1.Pos",1)
XPSConfigAxis(0,1,"Y1.Pos",1)
XPSConfigAxis(0,2,"F1.Pos",1)
XPSConfigAxis(0,3,"X2.Pos",1)
XPSConfigAxis(0,4,"Y2.Pos",1)
XPSConfigAxis(0,5,"F2.Pos",1)
XPSConfigAxis(0,6,"M1.Pos",1)
XPSConfigAxis(0,7,"M2.Pos",1)
XPSConfigAxis(1,0,"X3.Pos",1)
XPSConfigAxis(1,1,"Y3.Pos",1)
XPSConfigAxis(1,2,"F3.Pos",1)
XPSConfigAxis(1,3,"X4.Pos",1)
XPSConfigAxis(1,4,"Y4.Pos",1)
XPSConfigAxis(1,5,"F4.Pos",1)
XPSConfigAxis(1,6,"M3.Pos",1)
XPSConfigAxis(1,7,"M4.Pos",1)
XPSConfigAxis(2,0,"M1.Pos",1)
XPSConfigAxis(2,1,"M2.Pos",1)
XPSConfigAxis(2,2,"M3.Pos",1)
XPSConfigAxis(2,3,"M4.Pos",1)
XPSConfigAxis(2,4,"M5.Pos",1)
XPSConfigAxis(2,5,"M6.Pos",1)
XPSConfigAxis(2,6,"M7.Pos",1)
XPSConfigAxis(2,7,"M8.Pos",1)
XPSConfigAxis(3,0,"M1.Pos",1)
XPSConfigAxis(3,1,"M2.Pos",1)
XPSConfigAxis(3,2,"M3.Pos",1)
XPSConfigAxis(3,3,"M4.Pos",1)
XPSConfigAxis(3,4,"M5.Pos",1)
XPSConfigAxis(3,5,"M6.Pos",1)
XPSConfigAxis(3,6,"M7.Pos",1)
XPSConfigAxis(3,7,"M8.Pos",1)

iocInit()

< iocBoot/iocmOTR/otrInsertTime
< iocBoot/iocmOTR/motorSetup

dbpf mOTR:XPS4AuxAo1.OMSL closed_loop
dbpf mOTR:XPS4AuxAo1.DOL EVR:driver
dbpf mOTR:XPS4AuxAo1.SCAN ".1 second"


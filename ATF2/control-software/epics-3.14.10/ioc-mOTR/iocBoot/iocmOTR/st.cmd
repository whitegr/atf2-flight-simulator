#!/home/atfopr/ATF2/control-software/epics-3.14.10/ioc-mOTR/bin/linux-x86_64/mOTR

## You may have to change mOTR to something else
## everywhere it appears in this file


< /home/atfopr/ATF2/control-software/epics-3.14.10/ioc-mOTR/iocBoot/iocmOTR/envPaths

errlogInit(20000)

cd ${TOP}

## Register all support components
dbLoadDatabase "dbd/mOTR.dbd"
mOTR_registerRecordDeviceDriver pdbbase


# Prosilica Camera config
prosilicaConfig("PS1", 118345, 10, 50000000)
dbLoadRecords("$(AREA_DETECTOR)/ADApp/Db/ADBase.template",   "P=mOTR:,R=cam1:,PORT=PS1,ADDR=0,TIMEOUT=1")
dbLoadRecords("$(AREA_DETECTOR)/ADApp/Db/NDFile.template",   "P=mOTR:,R=cam1:,PORT=PS1,ADDR=0,TIMEOUT=1")
dbLoadRecords("$(AREA_DETECTOR)/ADApp/Db/prosilica.template","P=mOTR:,R=cam1:,PORT=PS1,ADDR=0,TIMEOUT=1")
NDStdArraysConfigure("PS1Image", 5, 0, "PS1", 0)
dbLoadRecords("$(AREA_DETECTOR)/ADApp/Db/NDPluginBase.template","P=mOTR:,R=image1:,PORT=PS1Image,ADDR=0,TIMEOUT=1,NDARRAY_PORT=PS1,NDARRAY_ADDR=0")
dbLoadRecords("$(AREA_DETECTOR)/ADApp/Db/NDStdArrays.template", "P=mOTR:,R=image1:,PORT=PS1Image,ADDR=0,TIMEOUT=1,TYPE=Int16,FTVL=SHORT,NELEMENTS=1228800")
prosilicaConfig("PS2", 118346, 10, 50000000)
dbLoadRecords("$(AREA_DETECTOR)/ADApp/Db/ADBase.template",   "P=mOTR:,R=cam2:,PORT=PS2,ADDR=0,TIMEOUT=1")
dbLoadRecords("$(AREA_DETECTOR)/ADApp/Db/NDFile.template",   "P=mOTR:,R=cam2:,PORT=PS2,ADDR=0,TIMEOUT=1")
dbLoadRecords("$(AREA_DETECTOR)/ADApp/Db/prosilica.template","P=mOTR:,R=cam2:,PORT=PS2,ADDR=0,TIMEOUT=1")
NDStdArraysConfigure("PS2Image", 5, 0, "PS2", 0)
dbLoadRecords("$(AREA_DETECTOR)/ADApp/Db/NDPluginBase.template","P=mOTR:,R=image2:,PORT=PS2Image,ADDR=0,TIMEOUT=1,NDARRAY_PORT=PS2,NDARRAY_ADDR=0")
dbLoadRecords("$(AREA_DETECTOR)/ADApp/Db/NDStdArrays.template", "P=mOTR:,R=image2:,PORT=PS2Image,ADDR=0,TIMEOUT=1,TYPE=Int16,FTVL=SHORT,NELEMENTS=1228800")
prosilicaConfig("PS3", 118348, 10, 50000000)
dbLoadRecords("$(AREA_DETECTOR)/ADApp/Db/ADBase.template",   "P=mOTR:,R=cam3:,PORT=PS3,ADDR=0,TIMEOUT=1")
dbLoadRecords("$(AREA_DETECTOR)/ADApp/Db/NDFile.template",   "P=mOTR:,R=cam3:,PORT=PS3,ADDR=0,TIMEOUT=1")
dbLoadRecords("$(AREA_DETECTOR)/ADApp/Db/prosilica.template","P=mOTR:,R=cam3:,PORT=PS3,ADDR=0,TIMEOUT=1")
NDStdArraysConfigure("PS3Image", 5, 0, "PS3", 0)
dbLoadRecords("$(AREA_DETECTOR)/ADApp/Db/NDPluginBase.template","P=mOTR:,R=image3:,PORT=PS3Image,ADDR=0,TIMEOUT=1,NDARRAY_PORT=PS3,NDARRAY_ADDR=0")
dbLoadRecords("$(AREA_DETECTOR)/ADApp/Db/NDStdArrays.template", "P=mOTR:,R=image3:,PORT=PS3Image,ADDR=0,TIMEOUT=1,TYPE=Int16,FTVL=SHORT,NELEMENTS=1228800")
prosilicaConfig("PS4", 118341, 10, 50000000)
dbLoadRecords("$(AREA_DETECTOR)/ADApp/Db/ADBase.template",   "P=mOTR:,R=cam4:,PORT=PS4,ADDR=0,TIMEOUT=1")
dbLoadRecords("$(AREA_DETECTOR)/ADApp/Db/NDFile.template",   "P=mOTR:,R=cam4:,PORT=PS4,ADDR=0,TIMEOUT=1")
dbLoadRecords("$(AREA_DETECTOR)/ADApp/Db/prosilica.template","P=mOTR:,R=cam4:,PORT=PS4,ADDR=0,TIMEOUT=1")
NDStdArraysConfigure("PS4Image", 5, 0, "PS4", 0)
dbLoadRecords("$(AREA_DETECTOR)/ADApp/Db/NDPluginBase.template","P=mOTR:,R=image4:,PORT=PS4Image,ADDR=0,TIMEOUT=1,NDARRAY_PORT=PS4,NDARRAY_ADDR=0")
dbLoadRecords("$(AREA_DETECTOR)/ADApp/Db/NDStdArrays.template", "P=mOTR:,R=image4:,PORT=PS4Image,ADDR=0,TIMEOUT=1,TYPE=Int16,FTVL=SHORT,NELEMENTS=1228800")

# --- Motor driver config
dbLoadTemplate "db/motor-xps.substitutions"
dbLoadTemplate "db/XPSAux.substitutions"
# cards (total controllers)
XPSSetup(4)
# card, IP, PORT, number of axes, active poll period (ms), idle poll period (ms)
XPSConfig(0, "169.254.0.6", 5001, 8, 10, 5000)
XPSConfig(1, "169.254.0.7", 5001, 8, 10, 5000)
XPSConfig(2, "169.254.0.8", 5001, 8, 10, 5000)
XPSConfig(3, "169.254.0.9", 5001, 8, 10, 5000)
# asynPort, IP address, IP port, poll period (ms)
XPSAuxConfig("XPS_AUX1", "169.254.0.6", 5001, 50)
XPSAuxConfig("XPS_AUX2", "169.254.0.7", 5001, 50)
XPSAuxConfig("XPS_AUX3", "169.254.0.8", 5001, 50)
# asyn port, driver name, controller index, max. axes)
drvAsynMotorConfigure("XPS1", "motorXPS", 0, 8)
XPSInterpose("XPS1")
drvAsynMotorConfigure("XPS2", "motorXPS", 1, 8)
XPSInterpose("XPS2")
drvAsynMotorConfigure("XPS3", "motorXPS", 2, 8)
XPSInterpose("XPS3")
drvAsynMotorConfigure("XPS4", "motorXPS", 3, 8)
XPSInterpose("XPS4")
# card,  axis, groupName.positionerName, units
XPSConfigAxis(0,0,"X1.Pos",1)
XPSConfigAxis(0,1,"Y1.Pos",1)
XPSConfigAxis(0,2,"F1.Pos",1)
XPSConfigAxis(0,3,"X2.Pos",1)
XPSConfigAxis(0,4,"Y2.Pos",1)
XPSConfigAxis(0,5,"F2.Pos",1)
XPSConfigAxis(0,6,"M1.Pos",1)
XPSConfigAxis(0,7,"M2.Pos",1)
XPSConfigAxis(1,0,"X3.Pos",1)
XPSConfigAxis(1,1,"Y3.Pos",1)
XPSConfigAxis(1,2,"F3.Pos",1)
XPSConfigAxis(1,3,"X4.Pos",1)
XPSConfigAxis(1,4,"Y4.Pos",1)
XPSConfigAxis(1,5,"F4.Pos",1)
XPSConfigAxis(1,6,"M3.Pos",1)
XPSConfigAxis(1,7,"M4.Pos",1)
XPSConfigAxis(2,0,"M1.Pos",1)
XPSConfigAxis(2,1,"M2.Pos",1)
XPSConfigAxis(2,2,"M3.Pos",1)
XPSConfigAxis(2,3,"M4.Pos",1)
XPSConfigAxis(2,4,"M5.Pos",1)
XPSConfigAxis(2,5,"M6.Pos",1)
XPSConfigAxis(2,6,"M7.Pos",1)
XPSConfigAxis(2,7,"M8.Pos",1)
XPSConfigAxis(3,0,"M1.Pos",1)
XPSConfigAxis(3,1,"M2.Pos",1)
XPSConfigAxis(3,2,"M3.Pos",1)
XPSConfigAxis(3,3,"M4.Pos",1)
XPSConfigAxis(3,4,"M5.Pos",1)
XPSConfigAxis(3,5,"M6.Pos",1)
XPSConfigAxis(3,6,"M7.Pos",1)
XPSConfigAxis(3,7,"M8.Pos",1)

# Processed data records
dbLoadRecords("db/procData.vdb","N=1")
dbLoadRecords("db/procData.vdb","N=2")
dbLoadRecords("db/procData.vdb","N=3")
dbLoadRecords("db/procData.vdb","N=4")
dbLoadRecords("db/emitData.db")
dbLoadRecords("db/iocAdminSoft.db","IOC=mOTR")
#dbLoadRecords("db/otrStatus.db","N=1,TARGET=mOTR:XPS1Aux3Bo2.RVAL,XV=mOTR:XPS1AuxAi0,YV=mOTR:XPS1AuxAi1,VERR=0.2")
#dbLoadRecords("db/otrStatus.db","N=2,TARGET=mOTR:XPS1Aux3Bo3.RVAL,XV=mOTR:XPS1AuxAi2,YV=mOTR:XPS1AuxAi3,VERR=0.2")
#dbLoadRecords("db/otrStatus.db","N=3,TARGET=mOTR:XPS2Aux3Bo2.RVAL,XV=mOTR:XPS2AuxAi0,YV=mOTR:XPS1AuxAi1,VERR=0.2")
#dbLoadRecords("db/otrStatus.db","N=4,TARGET=mOTR:XPS2Aux3Bo3.RVAL,XV=mOTR:XPS2AuxAi2,YV=mOTR:XPS1AuxAi3,VERR=0.2")


# IOC save/restore setup
#set_requestfile_path("./")
#set_savefile_path("./autosave")
#set_requestfile_path("$(AREA_DETECTOR)/ADApp/Db")
#set_pass0_restoreFile("auto_settings.sav")
#set_pass1_restoreFile("auto_settings.sav")
#save_restoreSet_status_prefix("mOTR:")
#dbLoadRecords("$(AUTOSAVE)/asApp/Db/save_restoreStatus.db", "P=mOTR:")

iocInit()


< iocBoot/iocmOTR/otrInsertTime
< iocBoot/iocmOTR/prosilicaSetup
< iocBoot/iocmOTR/motorSetup


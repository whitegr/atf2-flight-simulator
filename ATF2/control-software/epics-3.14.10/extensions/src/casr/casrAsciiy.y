
%{

#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>

#include "common.h"
#include "casrAscii.h"
#include "log.h"

static ItemNode		*pCurrentItem;
static ItemNode		*pPathItem;

typedef struct
{
	ELLNODE	Node;
	FILE	*File;
	char 	*FileName;
	int		LineNumber;
} CurrentFileStruct;

static ELLLIST				CurrentFileList;
static CurrentFileStruct	*pCurrentFile;

#define	INPUT_BUFFER_SIZE	1024
static char					*pInputBuffer;

static int BurtStartInclude(char *FileName);
static int BurtYYInput(char *Buf, int BufSize);

%}

%union
{
	long  integer;
	char  *string;
}

%token OPEN_ATTRIBUTE CLOSE_ATTRIBUTE EQUALS
%token <string>STRING
%token GARBAGE

%%

Burtfile:		Items
	{
	}

Items: 			
				| Items Item
	{
	}

Item:			STRING 
	{ 
		BurtItemAddAttribute(pCurrentItem, $1);

		/* 
		ItemNode *pItem = BurtItemCreate();
		pItem->Value = $1;
		pItem->pParent = pCurrentItem;
		ellAdd(&pCurrentItem->AttributeList, &pItem->Node);
		*/
	}
				| ItemValueStart Items ItemValueEnd
	{ 
	}
				| STRING EQUALS STRING
	{
		ItemNode *pItem = BurtItemAddAttribute(pCurrentItem, $1);
		BurtItemAddAttribute(pItem, $3);
	}

ItemValueStart:	STRING OPEN_ATTRIBUTE
	{
		pCurrentItem = BurtItemAddAttribute(pCurrentItem, $1);
		/*
		ItemNode *pItem = BurtItemCreate();
		pItem->Value = $1;
		pItem->pParent = pCurrentItem;
		ellAdd(&pCurrentItem->AttributeList, &pItem->Node);
		pCurrentItem = pItem;
		*/
	}
ItemValueEnd:	CLOSE_ATTRIBUTE
	{
		pCurrentItem = pCurrentItem->pParent;
	}
%%

#include "casrAsciil.c"

/*****************************************************************************
*
* We groked something.
*
*****************************************************************************/
static yyerror(char *str)
{
	LogMsg(0, "%s while reading file '%s' line %d\n", str, pCurrentFile->FileName, pCurrentFile->LineNumber);
	return(-1);
}

/*****************************************************************************
*
* Process the the pecified file.
*
*****************************************************************************/
int BurtAsciiParseFile(ItemNode *pBurtFile, FILE *file, ItemNode *pPath)
{
	static int    	firsttime = 1;
	int				status;

	yyin = file;
	if (firsttime)
	{
		ellInit(&CurrentFileList);
		firsttime = 0;
	}
	else
		yyrestart(yyin);

	pCurrentItem = pBurtFile;
	pPathItem = pPath;

	pCurrentFile = (CurrentFileStruct*)BurtMalloc(sizeof(CurrentFileStruct));
	ellAdd(&CurrentFileList, &pCurrentFile->Node);
	pCurrentFile->LineNumber = 1;
	pCurrentFile->FileName = pBurtFile->Value;
	pCurrentFile->File = yyin;

	pInputBuffer = (char*)BurtMalloc(INPUT_BUFFER_SIZE);
	status = yyparse();
	BurtFree(pInputBuffer);

	ellDelete(&CurrentFileList, &pCurrentFile->Node);
	BurtFree(pCurrentFile);
	pCurrentFile = NULL;

	return(status);
}

/*****************************************************************************
*
*****************************************************************************/
static int BurtYYInput(char *Buf, int BufSize)
{
	while(fgets(Buf, BufSize, pCurrentFile->File) == NULL)
	{
		if (ellCount(&CurrentFileList) > 1)
		{
			ellDelete(&CurrentFileList, &pCurrentFile->Node);
			fclose(pCurrentFile->File);
			BurtFree(pCurrentFile->FileName);
			BurtFree(pCurrentFile);
			pCurrentFile = (CurrentFileStruct*)ellLast(&CurrentFileList);
		}
		else
			return(0);
	}

#ifdef CASR_DEBUG
	LogMsg(0, "reading: %s", Buf);
#endif
	return(strlen(Buf));
}

static int BurtStartInclude(char *FileName)
{
	char				FullFileName[1024];	/* XXX */
	CurrentFileStruct	*pNewFile = (CurrentFileStruct*)BurtMalloc(sizeof(CurrentFileStruct));
	ItemNode			*pCurrentPath;

#ifdef CASR_DEBUG
	LogMsg(0, "opening include file: %s", FileName);
#endif
	/* Check CWD first...  */
	if ((pNewFile->File = fopen(FileName, "r")) != NULL)
	{
		pNewFile->FileName = strdup(FileName);
		pNewFile->LineNumber = 1;
		ellAdd(&CurrentFileList, &pNewFile->Node);
/* XXX pCurrentFile = pNewFile; */
		pCurrentFile = pNewFile;
		return(0);
	}
	pCurrentPath = BurtFindAttribute(NULL, pPathItem);
	while (pNewFile->File == NULL)
	{
		if ((pCurrentPath == NULL)||(pCurrentPath->Value == NULL))
		{
			BurtFree(pNewFile);
			sprintf(FullFileName, "Can't open include file '%s'\n", FileName);
			return(yyerror(FullFileName));
		}
		sprintf(FullFileName, "%s/%s", pCurrentPath->Value, FileName);
		pNewFile->File = fopen(FullFileName, "r");
		pCurrentPath = BurtFindNext(NULL, pCurrentPath);
	}
	pNewFile->FileName = strdup(FullFileName);
	pNewFile->LineNumber = 1;
	ellAdd(&CurrentFileList, &pNewFile->Node);
	pCurrentFile = pNewFile;
	return(0);
}

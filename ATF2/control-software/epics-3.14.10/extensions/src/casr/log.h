/*************************************************************************\
* Copyright (c) 2002 The University of Chicago, as Operator of Argonne
* National Laboratory.
* Copyright (c) 2002 The Regents of the University of California, as
* Operator of Los Alamos National Laboratory.
* This file is distributed subject to a Software License Agreement found
* in the file LICENSE that is included with this distribution. 
\*************************************************************************/
/****************************************************************************
*
* Author:   John Winans
* Date:     95-11-20
*
****************************************************************************/

#ifndef CASR_LOG_H
#define CASR_LOG_H

#include "common.h"

FILE *LogGetFd(void);
int LogOpen(char *FileName);
int LogMsg(int i, char *Format, ...);
int LogCat(int i, char *Format, ...);
int LogFlush();
int LogClose();

#endif

/*************************************************************************\
* Copyright (c) 2002 The University of Chicago, as Operator of Argonne
* National Laboratory.
* Copyright (c) 2002 The Regents of the University of California, as
* Operator of Los Alamos National Laboratory.
* This file is distributed subject to a Software License Agreement found
* in the file LICENSE that is included with this distribution. 
\*************************************************************************/
/****************************************************************************
*
* Author:   John Winans
* Date:     95-11-20
*
*****************************************************************************/

#include <pwd.h>
#include <time.h>
#include <sys/types.h>
#include <SDDS.h>

#include "log.h"
#include "common.h"
#include "casrSdds.h"

/******************************************************************************
*
* Insert the given burt environemnt as paremeters in the specified SDDS
* table.
*
******************************************************************************/
static int BurtSddsInsertEnvParameters(ELLLIST *pGlobalContext, SDDS_TABLE *pTable)
{
	ItemNode *pItem;
	int		status;


	pItem=(ItemNode*) ellFirst(pGlobalContext);
	while(pItem != NULL)
	{
		char *p = BurtFindAttribute(NULL, pItem)->Value;
		if (strcmp(pItem->Value, "Time") == 0)
			status = (!SDDS_DefineParameter(pTable, pItem->Value, NULL, NULL, NULL, NULL, SDDS_LONG, p) == -1);
		else
			status = (!SDDS_DefineParameter(pTable, pItem->Value, NULL, NULL, NULL, NULL, SDDS_STRING, p) == -1);

		if (status)
			SDDS_PrintErrors(LogGetFd(), SDDS_VERBOSE_PrintErrors|SDDS_EXIT_PrintErrors);
		pItem=(ItemNode*) ellNext(&pItem->Node);
	}

	return(0);
}
/******************************************************************************
*
* Write the value of the specified PV item node into the SDDS file
* named <fPath>.<fName>
*
******************************************************************************/
static int WriteSddsVectorFile(char *fPath, char *fName, ItemNode *pPv)
{
	char			buf[1000];
	SDDS_TABLE		Table;
	ItemNode		*pData;
	ItemNode		*pDim;
	ItemNode		*pItem;
	long			RowIndex;
	long			NumRows;

	if (fPath != NULL)
		sprintf(buf, "%s.%s", fPath, fName);
	else
		strcpy(buf, fName);

	if (!SDDS_InitializeOutput(&Table, SDDS_ASCII, 1L, NULL, NULL, buf))
		SDDS_PrintErrors(LogGetFd(), SDDS_VERBOSE_PrintErrors|SDDS_EXIT_PrintErrors);

	if (SDDS_DefineColumn(&Table, VAL_COL, NULL, NULL, NULL, NULL, SDDS_STRING, 0) == -1)
		SDDS_PrintErrors(LogGetFd(), SDDS_VERBOSE_PrintErrors|SDDS_EXIT_PrintErrors);

	if (!SDDS_WriteLayout(&Table))
		SDDS_PrintErrors(LogGetFd(), SDDS_VERBOSE_PrintErrors|SDDS_EXIT_PrintErrors);

	pItem = BurtFindAttribute("value", pPv);
	pDim = BurtFindAttribute("dim", pItem);
	pDim = BurtFindAttribute(NULL, pDim);
	if (pDim == NULL)
		BurtCroak(21, "WriteSddsVectorFile() have valueless dimension attribute\n");
	NumRows = strtol(pDim->Value, NULL, 10);
	if (!SDDS_StartTable(&Table, NumRows))
		SDDS_PrintErrors(LogGetFd(), SDDS_VERBOSE_PrintErrors|SDDS_EXIT_PrintErrors);

	pData = BurtFindAttribute("data", pItem);
	pData = BurtFindAttribute(NULL, pData);
	RowIndex = 0;
	while ((pData != NULL)&&(RowIndex<NumRows))
	{
		if (SDDS_SetRowValues(&Table, SDDS_SET_BY_NAME|SDDS_PASS_BY_VALUE, RowIndex, VAL_COL, pData->Value, NULL) != 1)
			SDDS_PrintErrors(LogGetFd(), SDDS_VERBOSE_PrintErrors|SDDS_EXIT_PrintErrors);
		pData = BurtFindNext(NULL, pData);
		++RowIndex;
	}
	if (!SDDS_WriteTable(&Table))
		SDDS_PrintErrors(LogGetFd(), SDDS_VERBOSE_PrintErrors|SDDS_EXIT_PrintErrors);
	if (!SDDS_Terminate(&Table))
		SDDS_PrintErrors(LogGetFd(), SDDS_VERBOSE_PrintErrors|SDDS_EXIT_PrintErrors);
	return(0);
}

/******************************************************************************
*
******************************************************************************/
static int ReadSddsVectorFile(char *fPath, char *fName, ItemNode *pParentItem)
{
	char		buf[1000];
	SDDS_TABLE	SddsInTable;
	char		**ppChar;
	long		RowCounter;
	long		RowsOnPage;

	if (fPath != NULL)
		sprintf(buf, "%s.%s", fPath, fName);
	else
		strcpy(buf, fName);

	if (SDDS_InitializeInput(&SddsInTable, buf) != 1)
		SDDS_PrintErrors(LogGetFd(), SDDS_VERBOSE_PrintErrors|SDDS_EXIT_PrintErrors);
	if (SDDS_SetColumnsOfInterest(&SddsInTable, SDDS_NAME_STRINGS, VAL_COL, NULL) != 1)
		SDDS_PrintErrors(LogGetFd(), SDDS_VERBOSE_PrintErrors|SDDS_EXIT_PrintErrors);
	SDDS_SetColumnFlags(&SddsInTable, 1);
	SDDS_SetRowFlags(&SddsInTable, 1);

	if(SDDS_ReadPage(&SddsInTable) <= 0) 
		SDDS_PrintErrors(LogGetFd(), SDDS_VERBOSE_PrintErrors|SDDS_EXIT_PrintErrors);

	/* Grab the data out of this page */
	if ((RowsOnPage = SDDS_CountRowsOfInterest(&SddsInTable)) == -1)
		SDDS_PrintErrors(LogGetFd(), SDDS_VERBOSE_PrintErrors|SDDS_EXIT_PrintErrors);

	RowCounter = 0;
	pParentItem = BurtItemAddAttribute(pParentItem, "value");
	pParentItem = BurtItemAddAttribute(pParentItem, "dim");
	sprintf(buf, "%d", RowsOnPage);
	BurtItemAddAttribute(pParentItem, buf);
	pParentItem = pParentItem->pParent;
	pParentItem = BurtItemAddAttribute(pParentItem, "data");

	while (RowCounter < RowsOnPage)
	{
		if ((ppChar = SDDS_GetValue(&SddsInTable, VAL_COL, RowCounter, NULL)) == NULL)
			SDDS_PrintErrors(LogGetFd(), SDDS_VERBOSE_PrintErrors|SDDS_EXIT_PrintErrors);
	
		BurtItemAddAttribute(pParentItem, *ppChar);
		++RowCounter;
	}
	if (!SDDS_Terminate(&SddsInTable))
		SDDS_PrintErrors(LogGetFd(), SDDS_VERBOSE_PrintErrors|SDDS_EXIT_PrintErrors);
	return(0);
}
/******************************************************************************
*
*
******************************************************************************/
static int GetRowValueField(SDDS_TABLE *pTable, long RowIndex, ItemNode *pPv, char *fPath)
{
	ItemNode	*pParentItem;
	char		*pChar;
	char		**ppChar;

	if (((ppChar = SDDS_GetValue(pTable, INDIRECT_COL, RowIndex, NULL)) != NULL)&&
		(strcmp(*ppChar, "-")!=0))
	{
		ReadSddsVectorFile(fPath, *ppChar, pPv);
		return(0);
	}
	else
		SDDS_ClearErrors();

	if ((ppChar = SDDS_GetValue(pTable, VAL_COL, RowIndex, NULL)) == NULL)
	{
		SDDS_ClearErrors();
		return(-1);
	}

	pChar = *ppChar;
	if (!((pChar[0] == '-')&&(pChar[1] == '\0')))
	{	/* A '-' is a null value placeholder */

		/* nuke any quotes that may be around the value string */
		while ((pChar[0] == '"')&&(pChar[strlen(pChar)-1] == '"'))
		{
			++pChar;
			pChar[strlen(pChar)-1] = '\0';
		}
		pParentItem = BurtItemAddAttribute(pPv, "value");
		pParentItem = BurtItemAddAttribute(pParentItem, "data");
		BurtItemAddAttribute(pParentItem, pChar);
	}
	return(0);
}
/******************************************************************************
*
* NOTE: We unlink any possible vector file because there could be stale ones
* laying around and if our read failed, we won't know if the PV was at one
* time a vector value.  Annoying, isn't it!
*
******************************************************************************/
static int SetRowValueField(SDDS_TABLE *pTable, long RowIndex, ItemNode *pPv, char *fPath)
{
	char		*pIndirValue;
	char		*pValue;
	ItemNode	*pItem;
	ItemNode	*pData;
	char		buf[1000];

	if (pPv->MValue != NULL)
		pIndirValue = pPv->MValue;
	else
		pIndirValue = pPv->Value;

	if (fPath != NULL)
	{
		sprintf(buf, "%s.%s", fPath, pIndirValue);
		unlink(buf);
	}
	else
		unlink(pIndirValue);

	if (((pItem = BurtFindAttribute("value", pPv)) != NULL) &&
		((pData = BurtFindAttribute("data", pItem)) != NULL) &&
		((pData = BurtFindAttribute(NULL, pData)) != NULL))
	{
		if ((pItem = BurtFindAttribute("dim", pItem)) != NULL)
		{	/* We have a vector value.  Make an indirect... */

			pValue = "-";
			WriteSddsVectorFile(fPath, pIndirValue, pPv);
		}
		else
		{
			pValue = pData->Value;
			pIndirValue = "-";
		}
	}
	else
	{
		pValue = "-";
		pIndirValue = "-";
	}

	if (SDDS_SetRowValues(pTable, SDDS_SET_BY_NAME|SDDS_PASS_BY_VALUE, RowIndex, VAL_COL, pValue, INDIRECT_COL, pIndirValue, NULL) != 1)
		SDDS_PrintErrors(LogGetFd(), SDDS_VERBOSE_PrintErrors|SDDS_EXIT_PrintErrors);
	return(0);
}
/******************************************************************************
*
* Read an SDDS burt file and populate the PV structures.
*
* NOTES:
* pSddsPageTable[0] is primed to be used as the SDDS output file in the
* event that we want to save to an SDDS FILE.
*
******************************************************************************/
int BurtSddsReadFile(ItemNode *pBurtFile)
{
	SDDS_TABLE	SddsInTable;
	SDDS_TABLE	*pSddsPageTable;
	char		*fName;
	char		*fPath;
	char		buf[1000];
	long		SddsPageCount;

	int			RowsOnPage;
	int			RowCounter;
	char		*pChar;
	char		**ppChar;
	ItemNode	*pParentItem = pBurtFile;
	long		status;

	long		NameIndex;
	long		ModeIndex;
	long		IndirectIndex;
	long		ValueIndex;
	
	if ((pBurtFile->Value == NULL)||(strcmp("-", pBurtFile->Value) == 0))
	{
		fName = NULL;
		fPath = NULL;
	}
	else
	{
		fName = pBurtFile->Value;
		fPath = pBurtFile->Value;
	}

	if (SDDS_InitializeInput(&SddsInTable, fName) != 1)
		SDDS_PrintErrors(LogGetFd(), SDDS_VERBOSE_PrintErrors|SDDS_EXIT_PrintErrors);

	pSddsPageTable = NULL;
	SddsPageCount = 1;
	if (!(pSddsPageTable = SDDS_Realloc(pSddsPageTable, sizeof(*pSddsPageTable)*(SddsPageCount+10))))
		SDDS_PrintErrors(LogGetFd(), SDDS_VERBOSE_PrintErrors|SDDS_EXIT_PrintErrors);
	if (!SDDS_InitializeCopy(&(pSddsPageTable[0]), &SddsInTable, NULL, "m"))
		SDDS_PrintErrors(LogGetFd(), SDDS_VERBOSE_PrintErrors|SDDS_EXIT_PrintErrors);
	
#if 0
	if (SDDS_SetColumnsOfInterest(&SddsInTable, SDDS_NAME_STRINGS, NAME_COL, MODE_COL, VAL_COL, INDIRECT_COL, NULL) != 1)
		SDDS_PrintErrors(LogGetFd(), SDDS_VERBOSE_PrintErrors|SDDS_EXIT_PrintErrors);
#endif

	SDDS_SetColumnFlags(&SddsInTable, 1);	/* XXX -- ??? */
	SDDS_SetRowFlags(&SddsInTable, 1);

	NameIndex = SDDS_GetColumnIndex(&SddsInTable, NAME_COL);
	ModeIndex = SDDS_GetColumnIndex(&SddsInTable, MODE_COL);
	IndirectIndex = SDDS_GetColumnIndex(&SddsInTable, INDIRECT_COL);
	ValueIndex = SDDS_GetColumnIndex(&SddsInTable, VAL_COL);

	if (NameIndex < 0)
	{
		if (fName == NULL)
			LogMsg(0, "SDDS input file (stdin) has no %s field\n", NAME_COL);
		else
			LogMsg(0, "SDDS input file '%s' has no %s field\n", fName, NAME_COL);
		exit (-1);
	}

	while (SDDS_ReadPage(&SddsInTable) > 0) 
	{
		if ((SddsPageCount % 10) == 0)
    		if (!(pSddsPageTable = SDDS_Realloc(pSddsPageTable, sizeof(*pSddsPageTable)*(SddsPageCount+10))))
      			SDDS_PrintErrors(LogGetFd(), SDDS_VERBOSE_PrintErrors|SDDS_EXIT_PrintErrors);
    	if (!SDDS_InitializeCopy(pSddsPageTable+SddsPageCount, &SddsInTable, NULL, "m") ||
			!SDDS_CopyPage(pSddsPageTable+SddsPageCount, &SddsInTable))
      		SDDS_PrintErrors(LogGetFd(), SDDS_VERBOSE_PrintErrors|SDDS_EXIT_PrintErrors);

    	SddsPageCount++;

		/* Grab the data out of this page */
		if ((RowsOnPage = SDDS_CountRowsOfInterest(&SddsInTable)) == -1)
			SDDS_PrintErrors(LogGetFd(), SDDS_VERBOSE_PrintErrors|SDDS_EXIT_PrintErrors);

		RowCounter = 0;
		pParentItem = BurtItemAddAttribute(pParentItem, "data");
		while (RowCounter < RowsOnPage)
		{
			if ((ppChar = SDDS_GetValue(&SddsInTable, NAME_COL, RowCounter, NULL)) == NULL)
				SDDS_PrintErrors(LogGetFd(), SDDS_VERBOSE_PrintErrors|SDDS_EXIT_PrintErrors);
	
			pChar = *ppChar;
			pParentItem = BurtItemAddAttribute(pParentItem, pChar);
	
			/* The mode column is not mandatory */
			if ((ModeIndex >= 0)&&((ppChar = SDDS_GetValue(&SddsInTable, MODE_COL, RowCounter, NULL)) != NULL))
			{
				if (((*ppChar)[0] == 'R')&&((*ppChar)[1] == 'O'))
				{
					pParentItem = BurtItemAddAttribute(pParentItem, "readonly");
					if ((*ppChar)[2] == 'N')
						BurtItemAddAttribute(pParentItem, "notify");
					pParentItem = pParentItem->pParent;
				}
			}

			/* The value column is not mandatory */
			if ((IndirectIndex>=0)||(ValueIndex>=0))
			GetRowValueField(&SddsInTable, RowCounter, pParentItem, fPath);

			pParentItem = pParentItem->pParent;
			++RowCounter;
		}
		pParentItem = pParentItem->pParent;
	}
	if (status == 0)
		SDDS_PrintErrors(LogGetFd(), SDDS_VERBOSE_PrintErrors|SDDS_EXIT_PrintErrors);

	pBurtFile->pTranPvt= pSddsPageTable;
	if (!SDDS_Terminate(&SddsInTable))
		SDDS_PrintErrors(LogGetFd(), SDDS_VERBOSE_PrintErrors|SDDS_EXIT_PrintErrors);
	return(0);
}
/******************************************************************************
*
* Open an output BURT file that starts out as a copy of an SDDS input file. 
*
******************************************************************************/
static SDDS_TABLE *BurtSddsWriteOpenCopy(ELLLIST *pFileList, ELLLIST *pGlobalContext, char *fName, char *fPath)
{
	SDDS_TABLE	*pOutputTable;
	SDDS_TABLE	*pInputTable;
	ItemNode	*pBurtInputFile = (ItemNode*) ellFirst(pFileList);
	char		*InputFileName;
	char		buf[1000];
	int			PageNumber;

	pOutputTable = (SDDS_TABLE*)BurtMalloc(sizeof(SDDS_TABLE));
	pInputTable = (SDDS_TABLE*) pBurtInputFile->pTranPvt;

	if (!SDDS_InitializeCopy(pOutputTable, &(pInputTable[0]), fName, "w"))
		SDDS_PrintErrors(LogGetFd(), SDDS_VERBOSE_PrintErrors|SDDS_EXIT_PrintErrors);

	/* Make sure we have a value column */
	if (SDDS_GetColumnIndex(pOutputTable, VAL_COL) == -1)
	{
		if (SDDS_DefineColumn(pOutputTable, VAL_COL, NULL, NULL, NULL, NULL, SDDS_STRING, 0) == -1)
			SDDS_PrintErrors(LogGetFd(), SDDS_VERBOSE_PrintErrors|SDDS_EXIT_PrintErrors);
	}
	if (SDDS_GetColumnIndex(pOutputTable, INDIRECT_COL) == -1)
	{
		if (SDDS_DefineColumn(pOutputTable, INDIRECT_COL, NULL, NULL, NULL, NULL, SDDS_STRING, 0) == -1)
			SDDS_PrintErrors(LogGetFd(), SDDS_VERBOSE_PrintErrors|SDDS_EXIT_PrintErrors);
	}
	BurtSddsInsertEnvParameters(pGlobalContext, pOutputTable);

	if (!SDDS_SaveLayout(pOutputTable) || !SDDS_WriteLayout(pOutputTable))
		SDDS_PrintErrors(LogGetFd(), SDDS_VERBOSE_PrintErrors|SDDS_EXIT_PrintErrors);
	return(pOutputTable);
}
/******************************************************************************
*
******************************************************************************/
static void *BurtSddsWriteOpenNew(ELLLIST *pFileList, ELLLIST *pGlobalContext, char *fName, char *fPath)
{
	char			buff[1000];
	SDDS_TABLE		*pTable;
	long			l;
	ItemNode		*pItem;

	pTable = (SDDS_TABLE*) BurtMalloc(sizeof(SDDS_TABLE));
	if (!SDDS_InitializeOutput(pTable, SDDS_ASCII, 1L, NULL, NULL, fName))
		SDDS_PrintErrors(LogGetFd(), SDDS_VERBOSE_PrintErrors|SDDS_EXIT_PrintErrors);

	/* Insert the BURT parameter information. */
	BurtSddsInsertEnvParameters(pGlobalContext, pTable);

	/* We need to install and/or clear the columns that BURT writes to */

	if (SDDS_DefineColumn(pTable, NAME_COL, NULL, NULL, NULL, NULL, SDDS_STRING, 0) == -1)
		SDDS_PrintErrors(LogGetFd(), SDDS_VERBOSE_PrintErrors|SDDS_EXIT_PrintErrors);
	if (SDDS_DefineColumn(pTable, MODE_COL, NULL, NULL, NULL, NULL, SDDS_STRING, 0) == -1)
		SDDS_PrintErrors(LogGetFd(), SDDS_VERBOSE_PrintErrors|SDDS_EXIT_PrintErrors);

	if (SDDS_DefineColumn(pTable, VAL_COL, NULL, NULL, NULL, NULL, SDDS_STRING, 0) == -1)
		SDDS_PrintErrors(LogGetFd(), SDDS_VERBOSE_PrintErrors|SDDS_EXIT_PrintErrors);
	if (SDDS_DefineColumn(pTable, INDIRECT_COL, NULL, NULL, NULL, NULL, SDDS_STRING, 0) == -1)
		SDDS_PrintErrors(LogGetFd(), SDDS_VERBOSE_PrintErrors|SDDS_EXIT_PrintErrors);

	if (!SDDS_WriteLayout(pTable))
		SDDS_PrintErrors(LogGetFd(), SDDS_VERBOSE_PrintErrors|SDDS_EXIT_PrintErrors);

	/* sukky problem here... need to know how many things we are gonna write */
	l = BurtCountAllPvs(pFileList);
	if (!SDDS_StartTable(pTable, l))
		SDDS_PrintErrors(LogGetFd(), SDDS_VERBOSE_PrintErrors|SDDS_EXIT_PrintErrors);

	return(pTable);
}
/******************************************************************************
*
******************************************************************************/
int BurtSddsWriteCloseCopy(SDDS_TABLE *pTable)
{
	if (!SDDS_Terminate(pTable))
		SDDS_PrintErrors(LogGetFd(), SDDS_VERBOSE_PrintErrors|SDDS_EXIT_PrintErrors);
	return(0);
}
/******************************************************************************
*
******************************************************************************/
int BurtSddsWriteCloseNew(SDDS_TABLE *pTable)
{
	if (!SDDS_WriteTable(pTable))
		SDDS_PrintErrors(LogGetFd(), SDDS_VERBOSE_PrintErrors|SDDS_EXIT_PrintErrors);
	if (!SDDS_Terminate(pTable))
		SDDS_PrintErrors(LogGetFd(), SDDS_VERBOSE_PrintErrors|SDDS_EXIT_PrintErrors);
	return(0);
}
/******************************************************************************
*
* The SDDS mode column is ignored here as it is not relevant when writing
* using a copy template.
*
******************************************************************************/
static int BurtSddsWriteFileCopy(SDDS_TABLE *pTable, ELLLIST *pFileList, char *fName, char *fPath)
{
	char		buff[1000];
	ItemNode	*pInputFileItem;
	ItemNode	*pData;
	ItemNode	*pItem;
	ItemNode	*pPv;

	long		RowIndex;
	long		PageNumber;
	SDDS_TABLE	*pInputTable;

	pInputFileItem = (ItemNode*) ellFirst(pFileList);

	while(pInputFileItem != NULL)
	{
		pInputTable = (SDDS_TABLE*)pInputFileItem->pTranPvt;
		PageNumber = 1;

		pData = BurtFindAttribute("data", pInputFileItem);
		while(pData != NULL)
		{
			if (!SDDS_CopyPage(pTable, &(pInputTable[PageNumber])))
				SDDS_PrintErrors(LogGetFd(), SDDS_VERBOSE_PrintErrors|SDDS_EXIT_PrintErrors);

			RowIndex = 0;
			pPv = BurtFindAttribute(NULL, pData);	/* get pvname */
			while(pPv != NULL)
			{
				SetRowValueField(pTable, RowIndex, pPv, fName);
				++RowIndex;
				pPv = BurtFindNext(NULL, pPv);
			}
			pData = BurtFindNext("data", pData);
#if 0
			if (!SDDS_WritePage(pTable)||
				!SDDS_Terminate(&(pInputTable[PageNumber])))
				SDDS_PrintErrors(LogGetFd(), SDDS_VERBOSE_PrintErrors|SDDS_EXIT_PrintErrors);
#else
			if (!SDDS_WritePage(pTable))
				SDDS_PrintErrors(LogGetFd(), SDDS_VERBOSE_PrintErrors|SDDS_EXIT_PrintErrors);
#endif
			++PageNumber;
		}
		pInputFileItem = (ItemNode*)ellNext(&pInputFileItem->Node);
	}
	return(0);
}
/******************************************************************************
*
******************************************************************************/
static int BurtSddsWriteFileNew(SDDS_TABLE *pTable, ELLLIST *pFileList, char *fName, char *fPath)
{
	char		buff[1000];
	ItemNode	*pInputFileItem;
	ItemNode	*pData;
	ItemNode	*pItem;
	ItemNode	*pPv;
	char		*pName;
	char		*pMode;
	char		*pValue;
	long		RowIndex = 0;

	pInputFileItem = (ItemNode*) ellFirst(pFileList);
	while(pInputFileItem != NULL)
	{
		pData = BurtFindAttribute("data", pInputFileItem);
		while(pData != NULL)
		{
			pPv = BurtFindAttribute(NULL, pData);	/* get pvname */
			while(pPv != NULL)
			{
				if (pPv->MValue != NULL)
					pName = pPv->MValue;
				else
					pName = pPv->Value;

				if ((pItem = BurtFindAttribute("readonly", pPv)) != NULL)
				{
					if ((pItem = BurtFindAttribute("notify", pItem)) != NULL)
						pMode = READONLYNOTIFYSTRING;
					else
						pMode = READONLYSTRING;
				}
				else
					pMode = "-";

				if (SDDS_SetRowValues(pTable, SDDS_SET_BY_NAME|SDDS_PASS_BY_VALUE, RowIndex, NAME_COL, pName, MODE_COL, pMode, NULL) != 1)
					SDDS_PrintErrors(LogGetFd(), SDDS_VERBOSE_PrintErrors|SDDS_EXIT_PrintErrors);

				SetRowValueField(pTable, RowIndex, pPv, fName);
				++RowIndex;
				pPv = BurtFindNext(NULL, pPv);
			}
			pData = BurtFindNext("data", pData);
		}
		pInputFileItem = (ItemNode*)ellNext(&pInputFileItem->Node);
	}
	return(0);
}
/******************************************************************************
*
* Write all PVs from all files in the given file list.
*
******************************************************************************/
int BurtSddsWriteFileList(ELLLIST *pFileList, ELLLIST *pGlobalContext, char *fName, char *fPath)
{
	SDDS_TABLE	*pTable;
	ItemNode	*pInputFileItem;

	pInputFileItem = (ItemNode*) ellFirst(pFileList);

    if ((fName == NULL)||(strcmp("-", fName) == 0))
        fName = NULL;       /* opening a null name writes to stdout */
 
    if ((pInputFileItem != NULL)&&(pInputFileItem->MValue != NULL)&&(pInputFileItem->MValue[0] == 's'))
	{
        pTable = BurtSddsWriteOpenCopy(pFileList, pGlobalContext, fName, fPath);
		BurtSddsWriteFileCopy(pTable, pFileList, fName, fPath);
		BurtSddsWriteCloseCopy(pTable);
	}
	else
	{
    	pTable = BurtSddsWriteOpenNew(pFileList, pGlobalContext, fName, fPath);
		BurtSddsWriteFileNew(pTable, pFileList, fName, fPath);
		BurtSddsWriteCloseNew(pTable);
	}
	return(0);
}

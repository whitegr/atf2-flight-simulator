/*************************************************************************\
* Copyright (c) 2002 The University of Chicago, as Operator of Argonne
* National Laboratory.
* Copyright (c) 2002 The Regents of the University of California, as
* Operator of Los Alamos National Laboratory.
* This file is distributed subject to a Software License Agreement found
* in the file LICENSE that is included with this distribution. 
\*************************************************************************/
/****************************************************************************
*
* Author:   John Winans
* Date:     95-11-20
*
*****************************************************************************/

#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <limits.h>
#include <ctype.h>

#include "log.h"
#include "common.h"
#include "casrAscii.h"

void *BurtAsciiWriteOpen(char *fName)
{
	if ((fName == NULL)||(strcmp("-", fName) == 0))
		return(stdout);

	return(fopen(fName, "w"));
}
int BurtAsciiWriteClose(void *pOutFile)
{
	FILE	*OutFile = (FILE*)pOutFile;

	if (OutFile != NULL)
	{
		fflush(OutFile);
		fclose(OutFile);
	}
	return(0);
}
/*****************************************************************************
*
* Read an ascii burt file and populate the PV structures
*
*****************************************************************************/
int BurtAsciiReadFile(ItemNode *pBurtFile, ItemNode *pPath)
{
	FILE	*File;

	/* Open the file */
	if ((pBurtFile->Value == NULL) || (pBurtFile->Value[0] == '\0') || (strcmp("-", pBurtFile->Value) == 0))
	{	/* Read data from stdin */
		File = stdin;
	}
	else if ((File = fopen(pBurtFile->Value, "r")) == NULL)
	{
		LogMsg(0, "Can't open file %s\n", pBurtFile->Value);
		return(-1);
	}

	/* Process the file */
	BurtAsciiParseFile(pBurtFile, File, pPath);

	if (ferror(File))
	{
		LogMsg(0, "WARNING: Error encountered while reading file %s\n", pBurtFile->Value);
	}
	/* Close the file */
	fclose(File);

	return(0);
}

/*****************************************************************************
*
*
*****************************************************************************/
int BurtAsciiWriteFile(ItemNode *pBurtFile, void *pOutFile)
{
	FILE	*OutFile = (FILE*)pOutFile;

	if (OutFile == NULL)
		OutFile = stdout;

	BurtDumpItem((ItemNode*)(ellFirst(&pBurtFile->AttributeList)), OutFile, 2, 0);
	return(0);
}

/*****************************************************************************
*
*
*****************************************************************************/
int BurtAsciiWriteFileList(ELLLIST *pFileList, ELLLIST *pGlobalContext, char *OutFileName)
{
	ItemNode	*pFile;
	FILE		*pOutFile;
	ItemNode	*pItem;

	if ((pOutFile = BurtAsciiWriteOpen(OutFileName)) == NULL)
		BurtCroak(20, "Can not open ascii output file\n");

	fputs("env{\n", pOutFile);
	pItem = (ItemNode*)ellFirst(pGlobalContext);
	while(pItem != NULL)
	{
		BurtDumpItem(pItem, pOutFile, 1, 0);
		pItem = (ItemNode*)ellNext(&pItem->Node);
	}
	fputs("}\n", pOutFile);

    pFile = (ItemNode*) ellFirst(pFileList);
    while(pFile != NULL)
    {
		pItem = BurtFindAttribute(NULL, pFile);
		while (pItem != NULL)
		{
			if (strcmp(pItem->Value, "env") != 0)
				BurtDumpItem(pItem, pOutFile, 2, 0);
			pItem = BurtFindNext(NULL, pItem);
		}
        pFile = (ItemNode*)ellNext(&pFile->Node);		/* XXX this is sucky code */
    }

	BurtAsciiWriteClose(pOutFile);
    return(0);
}

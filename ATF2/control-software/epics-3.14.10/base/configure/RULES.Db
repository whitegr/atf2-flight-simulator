#*************************************************************************
# Copyright (c) 2002 The University of Chicago, as Operator of Argonne
#     National Laboratory.
# Copyright (c) 2002 The Regents of the University of California, as
#     Operator of Los Alamos National Laboratory.
# EPICS BASE Versions 3.13.7
# and higher are distributed subject to a Software License Agreement found
# in file LICENSE that is included with this distribution. 
#*************************************************************************
#RULES.Db

#####################################################  vpath

vpath %.dbd $(USR_VPATH) $(GENERIC_SRC_DIRS) $(dir $(DBD))
vpath %.db $(USR_VPATH) $(GENERIC_SRC_DIRS) $(dir $(DB))
vpath %.vdb $(USR_VPATH) $(GENERIC_SRC_DIRS) $(dir $(DB))
vpath %.substitutions $(USR_VPATH) $(GENERIC_SRC_DIRS) $(COMMON_DIR)
vpath %.template $(USR_VPATH) $(GENERIC_SRC_DIRS) $(COMMON_DIR)
vpath bpt%.data $(USR_VPATH) $(GENERIC_SRC_DIRS) $(COMMON_DIR)
vpath %.acf $(USR_VPATH) $(GENERIC_SRC_DIRS) $(COMMON_DIR)
vpath %.acs $(USR_VPATH) $(GENERIC_SRC_DIRS) $(COMMON_DIR)

##################################################### dbdflags

# dbExpand
INSTALL_DBDFLAGS += -I $(INSTALL_LOCATION)/dbd
INSTALL_DBFLAGS += -I $(INSTALL_LOCATION)/db
DBDFLAGS = $(USR_DBDFLAGS) -I . -I .. $(INSTALL_DBDFLAGS) $(RELEASE_DBDFLAGS)
DBFLAGS = $($*_DBFLAGS) $(USR_DBFLAGS) -I. -I.. $(INSTALL_DBFLAGS) $(RELEASE_DBFLAGS)

#####################################################  Targets

# Following line added for backward compatibilty
DBD += $(DBDNAME)

DBD += $(addsuffix .dbd,$(patsubst %.h,%,$(patsubst %.db,%,$(DBDINC))))
INC += $(addsuffix .h,$(patsubst %.h,%,$(patsubst %.db,%,$(DBDINC))))

INSTALL_DBDS += $(addprefix $(INSTALL_DBD)/,$(notdir $(DBD)))

COMMON_DBDS += $(filter $(COMMON_DIR)/%, $(foreach file, $(DBD), \
    $(firstword  $(SOURCE_DBD) $(COMMON_DIR)/$(file) ) ) )
SOURCE_DBD = $(wildcard $(file) $(SOURCE_DBD_bbb) )
SOURCE_DBD_bbb = $(foreach dir, $(GENERIC_SRC_DIRS), $(SOURCE_DBD_aaa)  )
SOURCE_DBD_aaa = $(addsuffix /$(file), $(dir) )

INSTALL_DBS += $(addprefix $(INSTALL_DB)/,$(notdir $(DB)))

COMMON_DBS += $(filter $(COMMON_DIR)/%, $(foreach file, $(DB), \
    $(firstword  $(SOURCE_DB) $(COMMON_DIR)/$(file) ) ) )
SOURCE_DB = $(wildcard $(file) $(SOURCE_DB_bbb) )
SOURCE_DB_bbb = $(foreach dir, $(GENERIC_SRC_DIRS), $(SOURCE_DB_aaa)  )
SOURCE_DB_aaa = $(addsuffix /$(file), $(dir) )

COMMONS = $(COMMON_DIR)/*.dbd $(COMMON_DIR)/*.db $(COMMON_DIR)/*.h \
          $(COMMON_DIR)/*.substitutions $(COMMON_DIR)/*.template

# Remove trailing numbers (to 99) on stem
TEMPLATE1=$(patsubst %0,%,$(patsubst %1,%,$(patsubst %2,%,$(patsubst %3,%,$(patsubst %4,%, \
          $(patsubst %5,%,$(patsubst %6,%,$(patsubst %7,%,$(patsubst %8,%,$(patsubst %9,%, \
          $*))))))))))
TEMPLATE2=$(patsubst %0,%,$(patsubst %1,%,$(patsubst %2,%,$(patsubst %3,%,$(patsubst %4,%, \
          $(patsubst %5,%,$(patsubst %6,%,$(patsubst %7,%,$(patsubst %8,%,$(patsubst %9,%, \
          $(TEMPLATE1)))))))))))
TEMPLATE3=$(addsuffix .template,$(addprefix ../,$(TEMPLATE2)))
TEMPLATE_FILENAME=$(firstword $(wildcard $($*_TEMPLATE) $(addprefix ../,$($*_TEMPLATE)) ../$*.template $(TEMPLATE3) ../template))

# dbst based database optimization
ifeq '$(DB_OPT)' 'YES'
RAW=.raw
COMMON_DBS = $(addprefix $(COMMON_DIR)/,$(filter %.db,$(DB)))
COMMON_DBS += $(addsuffix $(RAW),$(addprefix $(COMMON_DIR)/,$(filter %.db,$(DB))))
endif

INSTALL_DB_INSTALLS = $(addprefix $(INSTALL_DB)/,$(notdir $(DB_INSTALLS)))
INSTALL_DBD_INSTALLS = $(addprefix $(INSTALL_DBD)/,$(notdir $(DBD_INSTALLS)))

#####################################################  acf files
# An access security configuration file, *.acf, can be created from
# an *.acs file (has format of acf file plus #include "filename" lines)

# flags for GNU compiler
ACF_CPPFLAGS_YES = -undef -nostdinc
ACF_CPPFLAGS = $(ACF_CPPFLAGS_$(GNU))

ACF_INCLUDES = -I. $(TARGET_INCLUDES) $(USR_INCLUDES)\
                $(SRC_INCLUDES) -I$(INSTALL_DB)
ACFDEPENDS_CMD = -$(MKMF) -m $@$(DEP) $(subst -I,,$(ACF_INCLUDES)) $@ $<
ACF_CMD = $(CPP) $(ACF_CPPFLAGS) $(ACF_INCLUDES) $< > $@

#####################################################  dependancies

HINC += $(addsuffix .h,$(patsubst %.h,%,$(patsubst %.db,%,$(DBDINC))))
COMMON_DBDINC += $(addprefix $(COMMON_DIR),$(HINC))

DBDDEPENDS_FILES += $(addsuffix $(DEP),$(COMMON_DBDS) \
                    $(COMMON_DBDINC) $(COMMON_DBS))

DBDDEPENDS_FLAGS = $(subst -I,,$(filter-out -S%,$(DBDFLAGS)))
DBDDEPENDS_CMD = -$(MKMF) -m $@$(DEP) $(DBDDEPENDS_FLAGS) $@ $<

MAKEDBDEPENDS = $(PERL) $(TOOLS)/makeDbDepends.pl

##################################################### 

ifndef T_A

COMMON_DIR = .
INSTALL_DBDS =
INSTALL_DBS =
COMMON_DBDS = $(DBD)
COMMON_DBS = $(DB)
COMMONS = $(DBD) $(DB)

-include $(TOP)/configure/CONFIG_APP_INCLUDE

all:    install

install: buildInstall

buildInstall : build

rebuild: clean install

.PHONY: all inc build install clean rebuild buildInstall

endif # T_A defined

ifneq (,$(strip $(DBDDEPENDS_FILES)))
-include $(DBDDEPENDS_FILES)
endif

$(DBDDEPENDS_FILES):

#####################################################  build dependancies, clean rule

inc : $(COMMON_INC) $(INSTALL_INC)

build : $(COMMON_DBDS) $(COMMON_DBS) \
	$(INSTALL_DBDS) $(INSTALL_DBS) \
	$(DBDDEPENDS_FILES) $(TARGETS) \
	$(INSTALL_DB_INSTALLS) $(INSTALL_DBD_INSTALLS)

clean:: 
	@$(RM) $(COMMONS) $(DBDDEPENDS_FILES)
	@$(RM) *_registerRecordDeviceDriver.cpp
	@$(RM) $(TARGETS)

realclean: clean

##################################################### CapFast filter

$(COMMON_DIR)/%.edf: ../%.sch $(DEPSCHS) 
	@$(RM) $@
	@if [ ! -f cad.rc -a -r ../cad.rc ] ; then ln -s ../cad.rc ; fi
	$(SCH2EDIF) $(SCH2EDIF_SYSFLAGS) $(SCH2EDIF_FLAGS) -o $@  $<

##################################################### Substitution files

#  WARNING: CREATESUBSTITUTIONS script needs output dir on command line

ifdef CREATESUBSTITUTIONS
$(COMMON_DIR)/%.substitutions:
	@echo "Create substitutions"
	@$(RM) $@
	$(CREATESUBSTITUTIONS) $(COMMON_DIR)/$*
endif

$(INSTALL_DB)/%.substitutions: %.substitutions
	@echo "Installing db file $@"
	@$(INSTALL) -d -m $(INSTALL_PERMISSIONS) $< $(@D)

.PRECIOUS: $(COMMON_DIR)/%.substitutions

##################################################### Template files

$(COMMON_DIR)/%.template: $(COMMON_DIR)/%.edf 
	@$(RM) $@
	$(E2DB) $(E2DB_SYSFLAGS) $(E2DB_FLAGS) -n $@.VAR $<
	@$(REPLACEVAR) < $@.VAR > $@
	@$(RM) $@.VAR

$(INSTALL_DB)/%.template: %.template
	@echo "Installing db file $@"
	@$(INSTALL) -d -m $(INSTALL_PERMISSIONS) $< $(@D)

.PRECIOUS: $(COMMON_DIR)/%.template

##################################################### INC files

$(COMMON_DIR)/%Record.h: $(COMMON_DIR)/%Record.dbd
	@$(RM) $@
	$(DBTORECORDTYPEH) $(DBDFLAGS)  $< $@

$(COMMON_DIR)/%Record.h: %Record.dbd
	@$(RM) $@
	$(DBTORECORDTYPEH) $(DBDFLAGS)  $< $@

$(COMMON_DIR)/menu%.h: $(COMMON_DIR)/menu%.dbd
	@$(RM) $@
	$(DBTOMENUH) $< $@

$(COMMON_DIR)/menu%.h: menu%.dbd
	@$(RM) $@
	$(DBTOMENUH) $< $@

.PRECIOUS: $(COMMON_DIR)/%.h

##################################################### DBD files

$(COMMON_DIR)/bpt%.dbd: bpt%.data 
	@$(RM) $@
	$(MAKEBPT) $< $@

$(COMMON_DIR)/%.dbd: $(COMMON_DIR)/%Include.dbd 
	@$(RM) $@$(DEP)
	@$(DBDDEPENDS_CMD)
	@echo "$<:../Makefile" >> $@$(DEP)
	@echo "Expanding dbd"
	@$(RM) $@
	$(DBEXPAND) $(DBDFLAGS) -o $@ $<

$(COMMON_DIR)/%.dbd: %Include.dbd 
	@$(RM) $@$(DEP)
	@$(DBDDEPENDS_CMD)
	@echo "Expanding dbd"
	@$(RM) $@
	$(DBEXPAND) $(DBDFLAGS) -o $@ $<

$(COMMON_DIR)/%Include.dbd:
	@$(RM) $@
	$(PERL) $(TOOLS)/makeIncludeDbd.pl $($*_DBD) $@

$(INSTALL_DBD)/%: $(COMMON_DIR)/%
	@echo "Installing created dbd file $@"
	@$(INSTALL) -d -m $(INSTALL_PERMISSIONS) $< $(@D)

$(INSTALL_DBD)/%: %
	@echo "Installing dbd file $@"
	@$(INSTALL) -d -m $(INSTALL_PERMISSIONS) $< $(@D)

define  DBD_INSTALLS_template
$$(INSTALL_DBD)/$$(notdir $(1)) : $(1)
	@echo "Installing $$@"
	@$$(INSTALL) -d -m $$(INSTALL_PERMISSIONS) $$^ $$(INSTALL_DBD)
endef
$(foreach file, $(DBD_INSTALLS), $(eval $(call DBD_INSTALLS_template, $(file))))

.PRECIOUS: $(COMMON_DBDS) $(COMMON_DIR)/%Include.dbd

##################################################### DB files

$(COMMON_DIR)/%.db$(RAW): $(COMMON_DIR)/%.edf 
	$(E2DB) $(E2DB_SYSFLAGS) $(E2DB_FLAGS) -n $@.VAR $<
	@$(REPLACEVAR) < $@.VAR > $@
	@$(RM) $@.VAR

#$(COMMON_DIR)/%.db$(RAW): %.substitutions %.template
#	@$(RM) $@$(DEP)
#	@$(MAKEDBDEPENDS) $@ $^  >> $@$(DEP)
#	@echo "Inflating database from $^"
#	@$(RM) $@
#	$(MSI) $(DBFLAGS) -S$< $(firstword $(patsubst %.substitutions,,$^)) > msi.tmp
#	$(MV) msi.tmp $@

$(COMMON_DIR)/%.db$(RAW): %.substitutions
	@$(RM) $@$(DEP)
	$(MAKEDBDEPENDS) $@ $< $(TEMPLATE_FILENAME) >> $@$(DEP)
	@echo "$@:$(TEMPLATE_FILENAME)" >> $@$(DEP)
	@echo "Inflating database from $< $(TEMPLATE_FILENAME)"
	@$(RM) $@
	$(MSI) $(DBFLAGS) -S$< $(TEMPLATE_FILENAME) > msi.tmp
	$(MV) msi.tmp $@

$(COMMON_DIR)/%.db$(RAW): %.template
	@$(RM) $@$(DEP)
	@$(MAKEDBDEPENDS) $@ $^  >> $@$(DEP)
	@echo "Inflating database from $<"
	@$(RM) $@
	$(MSI) $(DBFLAGS)  $< > msi.tmp
	$(MV) msi.tmp $@

$(COMMON_DIR)/%.acf: %.acs
	@$(RM) $@$(DEP)
	@$(ACFDEPENDS_CMD)
	@echo "Creating acf file $@"
	@$(RM) $@
	$(ACF_CMD)

.PRECIOUS: $(COMMON_DIR)/%.acf

# dbst based database optimization
ifeq '$(DB_OPT)' 'YES'

$(COMMON_DIR)/%.db$(RAW): ../%.db
	@$(RM) $@
	$(CP) $< $@

$(COMMON_DIR)/%.db: $(COMMON_DIR)/%.db$(RAW)
	@echo "Optimizing database $@"
	@$(RM) $@
	$(DBST) . $< -d > $@

.PRECIOUS: $(COMMON_DIR)/%.db
.PRECIOUS: $(DB:%=$(COMMON_DIR)/%$(RAW))
else

$(INSTALL_DB)/%: %
	@echo "Installing db file $@"
	@$(INSTALL) -d -m $(INSTALL_PERMISSIONS) $< $(@D)
endif

$(INSTALL_DB)/%.db: $(COMMON_DIR)/%.db
	@echo "Installing created db file $@"
	@$(INSTALL) -d -m $(INSTALL_PERMISSIONS) $< $(@D)

define  DB_INSTALLS_template
$$(INSTALL_DB)/$$(notdir $(1)) : $(1)
	@echo "Installing $$@"
	@$$(INSTALL) -d -m $$(INSTALL_PERMISSIONS) $$^ $$(INSTALL_DB)
endef
$(foreach file, $(DB_INSTALLS), $(eval $(call DB_INSTALLS_template, $(file))))

.PRECIOUS: $(COMMON_DIR)/%.edf
.PRECIOUS: $(COMMON_DBS)

##################################################### register record,device,driver support

%_registerRecordDeviceDriver.cpp: $(COMMON_DIR)/%.dbd 
	@$(RM) $@ temp.cpp
	$(REGISTERRECORDDEVICEDRIVER) $< $(basename $@) > temp.cpp
	$(MV) temp.cpp $@

%_registerRecordDeviceDriver.cpp: %.dbd 
	@$(RM) $@ temp.cpp
	$(REGISTERRECORDDEVICEDRIVER) $< $(basename $@) > temp.cpp
	$(MV) temp.cpp $@

.PRECIOUS: %_registerRecordDeviceDriver.cpp

##################################################### END OF FILE


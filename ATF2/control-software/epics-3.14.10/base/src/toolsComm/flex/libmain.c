/*************************************************************************\
* Copyright (c) 2002 The University of Chicago, as Operator of Argonne
*     National Laboratory.
* Copyright (c) 2002 The Regents of the University of California, as
*     Operator of Los Alamos National Laboratory.
* EPICS BASE Versions 3.13.7
* and higher are distributed subject to a Software License Agreement found
* in file LICENSE that is included with this distribution. 
\*************************************************************************/
/* libmain - flex run-time support library "main" function */

/* $Header: /afs/slac/g/ilc/cvs/ATF2/control-software/epics-3.14.10/base/src/toolsComm/flex/libmain.c,v 1.1.1.1 2009/08/14 19:33:46 whitegr Exp $ */

extern int yylex();

int main( argc, argv )
int argc;
char *argv[];

    {
    return yylex();
    }

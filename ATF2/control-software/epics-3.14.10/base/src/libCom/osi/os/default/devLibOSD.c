/*************************************************************************\
* Copyright (c) 2006 The University of Chicago, as Operator of Argonne
*     National Laboratory.
* EPICS BASE Versions 3.13.7
* and higher are distributed subject to a Software License Agreement found
* in file LICENSE that is included with this distribution.
\*************************************************************************/

/* $Id: devLibOSD.c,v 1.1.1.1 2009/08/14 19:33:45 whitegr Exp $ */

#include <stdlib.h>

#include "devLib.h"

/* This file must contain no definitions other than the following: */

devLibVirtualOS *pdevLibVirtualOS;

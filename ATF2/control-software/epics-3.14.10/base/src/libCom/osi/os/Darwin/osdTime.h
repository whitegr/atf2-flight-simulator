/*************************************************************************\
* Copyright (c) 2002 The University of Saskatchewan
* EPICS BASE Versions 3.13.7
* and higher are distributed subject to a Software License Agreement found
* in file LICENSE that is included with this distribution. 
\*************************************************************************/
/*
 * $Id: osdTime.h,v 1.1.1.1 2009/08/14 19:33:45 whitegr Exp $
 *
 * Author: Eric Norum
 */

#ifndef osdTimeh
#define osdTimeh

#include <sys/time.h>

#endif /* ifndef osdTimeh */


/* apex2Detector.cpp
 *
 * This is a driver for an Apex2 ccd detector.
 *
 * Author: Jeff Gebhardt
 *         University of Chicago
 *
 * Created:  March 18, 2010
 *
 * Derived from pilatusDetector.cpp
 *
 * Author: Mark Rivers
 *         University of Chicago
 *
 * Created:  June 11, 2008
 */
 
#include <stddef.h>
#include <stdlib.h>
#include <stdarg.h>
#include <math.h>
#include <stdio.h>
#include <errno.h>
#include <string.h>
#include <ctype.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <unistd.h>
#include <iostream>
#include <fstream>

#include <epicsTime.h>
#include <epicsThread.h>
#include <epicsEvent.h>
#include <epicsMutex.h>
#include <epicsString.h>
#include <epicsStdio.h>
#include <epicsMutex.h>
#include <cantProceed.h>
#include <iocsh.h>
#include <epicsExport.h>

#include <asynOctetSyncIO.h>

#include "ADDriver.h"

/** Messages to/from BIS */
#define MAX_MESSAGE_SIZE 256 
#define MAX_FILENAME_LEN 256
/** Time to poll when reading from BIS */
#define ASYN_POLL_TIME .01 
#define BIS_DEFAULT_TIMEOUT 1.0 
/** Time between checking to see if .SFRM file is complete */
#define FILE_READ_DELAY .01

#define Apex2SFRMTimeoutString  "SFRM_TIMEOUT"
#define Apex2AcqCmdString       "ACQUIRE_COMMAND"

static const char *driverName = "apex2Detector";

/** Driver for Bruker Apex2 ccd detector using their BIS server over TCP/IP socket */
class apex2Detector : public ADDriver {
public:
    apex2Detector(const char *portName, const char *BISPort,
                    int maxSizeX, int maxSizeY,
                    int maxBuffers, size_t maxMemory,
                    int priority, int stackSize);
                 
    /* These are the methods that we override from ADDriver */
    virtual asynStatus writeInt32(asynUser *pasynUser, epicsInt32 value);
    virtual asynStatus writeOctet(asynUser *pasynUser, const char *value, 
                                    size_t nChars, size_t *nActual);
    void report(FILE *fp, int details);
    void apex2Task(); /* This should be private but is called from C so must be public */
 
 protected:
    int Apex2SFRMTimeout;
#define FIRST_APEX2_PARAM Apex2SFRMTimeout
    int Apex2AcqCmd;
#define LAST_APEX2_PARAM Apex2AcqCmd

 private:                                       
    /* These are the methods that are new to this class */
    asynStatus readSFRM(const char *fileName, epicsTimeStamp *pStartTime, double timeout, NDArray *pImage);
    asynStatus writeBIS(double timeout);
       
    /* Our data */
    epicsEventId startEventId;
    epicsEventId stopEventId;
    char toBIS[MAX_MESSAGE_SIZE];
    asynUser *pasynUserBIS;
};

#define NUM_APEX2_PARAMS (&LAST_APEX2_PARAM - &FIRST_APEX2_PARAM + 1)

/** This function reads .SFRM files.  It is not intended to be general,
 * it is intended to read the .SFRM files that apex2 creates.  It does not implement under/overflows.  It checks to make sure
 * that the creation time of the file is after a start time passed to it, to force it to
 * wait for a new file to be created.
 */
 
asynStatus apex2Detector::readSFRM(const char *fileName, epicsTimeStamp *pStartTime, double timeout, NDArray *pImage)
{
    int fd=-1;
    int fileExists=0;
    struct stat statBuff;
    epicsTimeStamp tStart, tCheck;
    time_t acqStartTime;
    double deltaTime;
    int status=-1;
    const char *functionName = "readSFRM";
    char *buffer;
    int hdrblk = 0;
    int nrows = 0;
    int ncols = 0;
    int nbpxl = 0;
    char data[80];
        
    deltaTime = 0.;
    if (pStartTime) epicsTimeToTime_t(&acqStartTime, pStartTime);
    epicsTimeGetCurrent(&tStart);
    
    while (deltaTime <= timeout) {
        fd = open(fileName, O_RDONLY, 0);
        if ((fd >= 0) && (timeout != 0.)) {
            fileExists = 1;
            /* The file exists.  Make sure it is a new file, not an old one.
             * We don't do this check if timeout==0 */
            status = fstat(fd, &statBuff);
            if (status){
                asynPrint(this->pasynUserSelf, ASYN_TRACE_ERROR,
                    "%s::%s error calling fstat, errno=%d %s\n",
                    driverName, functionName, errno, fileName);
                close(fd);
                return(asynError);
            }
            /* We allow up to 10 second clock skew between time on machine running this IOC
             * and the machine with the file system returning modification time */
            if (difftime(statBuff.st_mtime, acqStartTime) > -10) break;
            close(fd);
            fd = -1;
        }
        /* Sleep, but check for stop event, which can be used to abort a long acquisition */
        status = epicsEventWaitWithTimeout(this->stopEventId, FILE_READ_DELAY);
        if (status == epicsEventWaitOK) {
            return(asynError);
        }
        epicsTimeGetCurrent(&tCheck);
        deltaTime = epicsTimeDiffInSeconds(&tCheck, &tStart);
    }
    if (fd < 0) {
        asynPrint(this->pasynUserSelf, ASYN_TRACE_ERROR,
            "%s::%s timeout waiting for file to be created %s\n",
            driverName, functionName, fileName);
        if (fileExists) {
            asynPrint(this->pasynUserSelf, ASYN_TRACE_ERROR,
                "  file exists but is more than 10 seconds old, possible clock synchronization problem\n");
        } 
        return(asynError);
    }
    close(fd);

    deltaTime = 0.;
    while (deltaTime <= timeout) {
        /* At this point we know the file exists, but it may not be completely written yet.
         * If we get errors then try again */
	std::ifstream datafile(fileName, std::ios::in | std::ios::out | std::ios::binary);
	if (!datafile.is_open()) {
            status = asynError;
            goto retry;
      }
     	for (int i = 0; i < 71; i++){
            datafile.seekg((168+i)*sizeof(char), std::ios::beg);
            datafile.read(reinterpret_cast<char *>(&data[i]), sizeof(char));
    	}
   	hdrblk = atoi(data);
      for (int i = 0; i < 71; i++) {
            datafile.seekg((i+(3128))*sizeof(char), std::ios::beg);
            datafile.read(reinterpret_cast<char *>(&data[i]), sizeof(char));
    	}
    	nbpxl = atoi(data);
      for (int i = 0; i < 71; i++) {
            datafile.seekg((i+(3208))*sizeof(char), std::ios::beg);
            datafile.read(reinterpret_cast<char *>(&data[i]), sizeof(char));
    	}
    	nrows = atoi(data);
      for (int i = 0; i < 71; i++) {
            datafile.seekg((i+(3288))*sizeof(char), std::ios::beg);
            datafile.read(reinterpret_cast<char *>(&data[i]), sizeof(char));
    	}
    	ncols = atoi(data);
      buffer = (char *)pImage->pData;
     	for (int i = 0; i < nbpxl*nrows*ncols; i++) {
            datafile.seekg((i+(512*hdrblk))*nbpxl, std::ios::beg);
            datafile.read(reinterpret_cast<char *>(&data), nbpxl);
            *buffer = atoi(data);
            buffer += sizeof(char);
    	}
      /* Sucesss! */
      if (datafile.is_open()) datafile.close();
      break;
        
      retry:
        if (datafile.is_open()) datafile.close();
        /* Sleep, but check for stop event, which can be used to abort a long acquisition */
        status = epicsEventWaitWithTimeout(this->stopEventId, FILE_READ_DELAY);
        if (status == epicsEventWaitOK) {
            return(asynError);
        }
        epicsTimeGetCurrent(&tCheck);
        deltaTime = epicsTimeDiffInSeconds(&tCheck, &tStart);
    }
    return(asynSuccess);
}   

asynStatus apex2Detector::writeBIS(double timeout)
{
    size_t nwrite;
    asynStatus status;
    const char *functionName="writeBIS";

    status = pasynOctetSyncIO->write(this->pasynUserBIS, this->toBIS,
                                     strlen(this->toBIS), timeout,
                                     &nwrite);
                                        
    if (status) asynPrint(this->pasynUserSelf, ASYN_TRACE_ERROR,
                    "%s:%s, status=%d, sent\n%s\n",
                    driverName, functionName, status, this->toBIS);

    /* Set output string so it can get back to EPICS */
    setStringParam(ADStringToServer, this->toBIS);
    
    return(status);
}

static void apex2TaskC(void *drvPvt)
{
    apex2Detector *pPvt = (apex2Detector *)drvPvt;
    
    pPvt->apex2Task();
}

/** This thread controls acquisition, reads SFRM files to get the image data, and
  * does the callbacks to send it to higher layers */
void apex2Detector::apex2Task()
{
    int status = asynSuccess;
    int imageCounter;
    int acquire;
    ADStatus_t acquiring;
    NDArray *pImage;
    double acquireTime;
    double readSFRMTimeout;
    double deltaTime;
    epicsTimeStamp startTime, tStart, tCheck;
    const char *functionName = "apex2Task";
    char fullFileName[MAX_FILENAME_LEN];
    char statusMessage[MAX_MESSAGE_SIZE];
    int dims[2];
    int arrayCallbacks;
    
    this->lock();

    /* Loop forever */
    while (1) {
        /* Is acquisition active? */
        getIntegerParam(ADAcquire, &acquire);
        
        /* If we are not acquiring then wait for a semaphore that is given when acquisition is started */
        if (!acquire) {
            setStringParam(ADStatusMessage, "Waiting for acquire command");
            setIntegerParam(ADStatus, ADStatusIdle);
            callParamCallbacks();
            /* Release the lock while we wait for an event that says acquire has started, then lock again */
            this->unlock();
            asynPrint(this->pasynUserSelf, ASYN_TRACE_FLOW, 
                "%s:%s: waiting for acquire to start\n", driverName, functionName);
            status = epicsEventWait(this->startEventId);
            this->lock();
            getIntegerParam(ADAcquire, &acquire);
        }
        
        /* We are acquiring. */
        /* Get the current time */
        epicsTimeGetCurrent(&startTime);
        
        /* Get the exposure parameters */
        getDoubleParam(ADAcquireTime, &acquireTime);
        getDoubleParam(Apex2SFRMTimeout, &readSFRMTimeout);
        
        acquiring = ADStatusAcquire;
        setIntegerParam(ADStatus, acquiring);

        /* Create the full filename */
        createFileName(sizeof(fullFileName), fullFileName);
        
        setStringParam(ADStatusMessage, "Starting exposure");
        /* Call the callbacks to update any changes */
        setStringParam(NDFullFileName, fullFileName);
        callParamCallbacks();
        /* Send the acquire command to BIS */
        writeBIS(2.0);
        callParamCallbacks();

        while (acquire) {
            /* For single frame need to wait for acquisition to complete before trying to read file, 
             * else we get a recent but stale file. */
            setStringParam(ADStatusMessage, "Waiting for Acquistion");
            callParamCallbacks();
            /* We release the mutex when waiting for Acquistion because this takes a long time and
             * we need to allow abort operations to get through */
            this->unlock();
            deltaTime = 0.;
            epicsTimeGetCurrent(&tStart);
            while (deltaTime <= acquireTime + 1) {
                epicsTimeGetCurrent(&tCheck);
                deltaTime = epicsTimeDiffInSeconds(&tCheck, &tStart);
            }
            this->lock();
            /* If there was an error jump to bottom of loop */
            if (status) {
                acquire = 0;
                continue;
            }
            getIntegerParam(NDArrayCallbacks, &arrayCallbacks);
            getIntegerParam(NDArrayCounter, &imageCounter);
            imageCounter++;
            setIntegerParam(NDArrayCounter, imageCounter);
            /* Call the callbacks to update any changes */
            callParamCallbacks();

            if (arrayCallbacks) {
                /* Get an image buffer from the pool */
                getIntegerParam(ADMaxSizeX, &dims[0]);
                getIntegerParam(ADMaxSizeY, &dims[1]);
                pImage = this->pNDArrayPool->alloc(2, dims, NDInt32, 0, NULL);
                epicsSnprintf(statusMessage, sizeof(statusMessage), "Reading from File %s", fullFileName);
                setStringParam(ADStatusMessage, statusMessage);
                callParamCallbacks();
                /* We release the mutex when calling readSFRM, because this takes a long time and
                 * we need to allow abort operations to get through */
                this->unlock(); 
                status = readSFRM(fullFileName, &startTime, acquireTime + readSFRMTimeout, pImage); 
                this->lock(); 
                /* If there was an error jump to bottom of loop */
                if (status) {
                    acquire = 0;
                    pImage->release();
                    continue;
                } 

                /* Put the frame number and time stamp into the buffer */
                pImage->uniqueId = imageCounter;
                pImage->timeStamp = startTime.secPastEpoch + startTime.nsec / 1.e9;

                /* Get any attributes that have been defined for this driver */        
                this->getAttributes(pImage->pAttributeList);
                
                /* Call the NDArray callback */
                /* Must release the lock here, or we can get into a deadlock, because we can
                 * block on the plugin lock, and the plugin can be calling us */
                this->unlock();
                asynPrint(this->pasynUserSelf, ASYN_TRACE_FLOW, 
                     "%s:%s: calling NDArray callback\n", driverName, functionName);
                doCallbacksGenericPointer(pImage, NDArrayData, 0);
                this->lock();
                /* Free the image buffer */
                pImage->release();
            }
            acquire = 0;
        }
        /* We are done acquiring */
        /* Call the callbacks to update any changes */
        callParamCallbacks();
    }
}

/** Called when asyn clients call pasynInt32->write().
  * This function performs actions for some parameters, including ADAcquire, ADTriggerMode, etc.
  * For all parameters it sets the value in the parameter library and calls any registered callbacks..
  * \param[in] pasynUser pasynUser structure that encodes the reason and address.
  * \param[in] value Value to write. */
asynStatus apex2Detector::writeInt32(asynUser *pasynUser, epicsInt32 value)
{
    int function = pasynUser->reason;
    int adstatus;
    asynStatus status = asynSuccess;
    const char *functionName = "writeInt32";

    status = setIntegerParam(function, value);

    if (function == ADAcquire) {
        getIntegerParam(ADStatus, &adstatus);
        if (value && (adstatus == ADStatusIdle)) {
            /* Send an event to wake up the Apex2 task.  */
            epicsEventSignal(this->startEventId);
        } 
        if (!value && (adstatus != ADStatusIdle)) {
            /* This was a command to stop acquisition */
            epicsEventSignal(this->stopEventId);
        }
    } else {
        /* If this parameter belongs to a base class call its method */
        if (function < FIRST_APEX2_PARAM) status = ADDriver::writeInt32(pasynUser, value);
    }
            
    /* Do callbacks so higher layers see any changes */
    callParamCallbacks();
    
    if (status) 
        asynPrint(pasynUser, ASYN_TRACE_ERROR, 
              "%s:%s: error, status=%d function=%d, value=%d\n", 
              driverName, functionName, status, function, value);
    else        
        asynPrint(pasynUser, ASYN_TRACEIO_DRIVER, 
              "%s:%s: function=%d, value=%d\n", 
              driverName, functionName, function, value);
    return status;
}


/** Called when asyn clients call pasynOctet->write().
  * This function performs actions for some parameters, including Apex2BadPixelFile, ADFilePath, etc.
  * For all parameters it sets the value in the parameter library and calls any registered callbacks..
  * \param[in] pasynUser pasynUser structure that encodes the reason and address.
  * \param[in] value Address of the string to write.
  * \param[in] nChars Number of characters to write.
  * \param[out] nActual Number of characters actually written. */
asynStatus apex2Detector::writeOctet(asynUser *pasynUser, const char *value, 
                                    size_t nChars, size_t *nActual)
{
    int function = pasynUser->reason;
    asynStatus status = asynSuccess;
    const char *functionName = "writeOctet";

    /* Set the parameter in the parameter library. */
    /* status = (asynStatus)setStringParam(function, (char *)value); */

    char fullFileName[MAX_FILENAME_LEN];
    double acquireTime;

    if (function == Apex2AcqCmd) {
        createFileName(MAX_FILENAME_LEN, fullFileName);
        getDoubleParam(ADAcquireTime, &acquireTime);
        epicsSnprintf(this->toBIS, sizeof(this->toBIS), "[Scan /Filename=%s /scantime=%f /Repetitions=0]", "C:\\Frames\\jtest\\jtest1.sfrm", 1.0);
        writeBIS(BIS_DEFAULT_TIMEOUT);
    } else {
        /* If this parameter belongs to a base class call its method */
        if (function < FIRST_APEX2_PARAM) status = ADDriver::writeOctet(pasynUser, value, nChars, nActual);
    }
    
     /* Do callbacks so higher layers see any changes */
    status = (asynStatus)callParamCallbacks();

    if (status) 
        epicsSnprintf(pasynUser->errorMessage, pasynUser->errorMessageSize, 
                  "%s:%s: status=%d, function=%d, value=%s", 
                  driverName, functionName, status, function, value);
    else        
        asynPrint(pasynUser, ASYN_TRACEIO_DRIVER, 
              "%s:%s: function=%d, value=%s\n", 
              driverName, functionName, function, value);
    *nActual = nChars;
    return status;
}

/** Report status of the driver.
  * Prints details about the driver if details>0.
  * It then calls the ADDriver::report() method.
  * \param[in] fp File pointed passed by caller where the output is written to.
  * \param[in] details If >0 then driver details are printed.
  */
void apex2Detector::report(FILE *fp, int details)
{

    fprintf(fp, "Apex2 detector %s\n", this->portName);
    if (details > 0) {
        int nx, ny, dataType;
        getIntegerParam(ADSizeX, &nx);
        getIntegerParam(ADSizeY, &ny);
        getIntegerParam(NDDataType, &dataType);
        fprintf(fp, "  NX, NY:            %d  %d\n", nx, ny);
        fprintf(fp, "  Data type:         %d\n", dataType);
    }
    /* Invoke the base class method */
    ADDriver::report(fp, details);
}

extern "C" int apex2DetectorConfig(const char *portName, const char *BISPort, 
                                    int maxSizeX, int maxSizeY,
                                    int maxBuffers, size_t maxMemory,
                                    int priority, int stackSize)
{
    new apex2Detector(portName, BISPort, maxSizeX, maxSizeY, maxBuffers, maxMemory,
                        priority, stackSize);
    return(asynSuccess);
}

/** Constructor for Apex2 driver; most parameters are simply passed to ADDriver::ADDriver.
  * After calling the base class constructor this method creates a thread to collect the detector data, 
  * and sets reasonable default values for the parameters defined in this class, asynNDArrayDriver, and ADDriver.
  * \param[in] portName The name of the asyn port driver to be created.
  * \param[in] BISPort The name of the asyn port previously created with drvAsynIPPortConfigure to
  *            communicate with BIS.
  * \param[in] maxSizeX The size of the Apex2 detector in the X direction.
  * \param[in] maxSizeY The size of the Apex2 detector in the Y direction.
  * \param[in] portName The name of the asyn port driver to be created.
  * \param[in] maxBuffers The maximum number of NDArray buffers that the NDArrayPool for this driver is 
  *            allowed to allocate. Set this to -1 to allow an unlimited number of buffers.
  * \param[in] maxMemory The maximum amount of memory that the NDArrayPool for this driver is 
  *            allowed to allocate. Set this to -1 to allow an unlimited amount of memory.
  * \param[in] priority The thread priority for the asyn port driver thread if ASYN_CANBLOCK is set in asynFlags.
  * \param[in] stackSize The stack size for the asyn port driver thread if ASYN_CANBLOCK is set in asynFlags.
  */
apex2Detector::apex2Detector(const char *portName, const char *BISPort,
                                int maxSizeX, int maxSizeY,
                                int maxBuffers, size_t maxMemory,
                                int priority, int stackSize)

    : ADDriver(portName, 1, NUM_APEX2_PARAMS, maxBuffers, maxMemory,
               0, 0,             /* No interfaces beyond those set in ADDriver.cpp */
               ASYN_CANBLOCK, 1, /* ASYN_CANBLOCK=1, ASYN_MULTIDEVICE=0, autoConnect=1 */
               priority, stackSize)
{
    int status = asynSuccess;
    const char *functionName = "apex2Detector";
    int dims[2];

    createParam(Apex2SFRMTimeoutString,  asynParamFloat64, &Apex2SFRMTimeout);
    createParam(Apex2AcqCmdString,       asynParamOctet,   &Apex2AcqCmd);

    /* Create the epicsEvents for signaling to the apex2 task when acquisition starts and stops */
    this->startEventId = epicsEventCreate(epicsEventEmpty);
    if (!this->startEventId) {
        printf("%s:%s epicsEventCreate failure for start event\n", 
            driverName, functionName);
        return;
    }
    this->stopEventId = epicsEventCreate(epicsEventEmpty);
    if (!this->stopEventId) {
        printf("%s:%s epicsEventCreate failure for stop event\n", 
            driverName, functionName);
        return;
    }
    
    /* Allocate the raw buffer we use to readSFRM files.  Only do this once */
    dims[0] = maxSizeX;
    dims[1] = maxSizeY;
        
    /* Connect to BIS */
    status = pasynOctetSyncIO->connect(BISPort, 0, &this->pasynUserBIS, NULL);

    /* Set some default values for parameters */
    status =  setStringParam (ADManufacturer, "Bruker");
    status |= setStringParam (ADModel, "Apex2");
    status |= setIntegerParam(ADMaxSizeX, maxSizeX);
    status |= setIntegerParam(ADMaxSizeY, maxSizeY);
    status |= setIntegerParam(ADSizeX, maxSizeX);
    status |= setIntegerParam(ADSizeY, maxSizeY);
    status |= setIntegerParam(NDArraySizeX, maxSizeX);
    status |= setIntegerParam(NDArraySizeY, maxSizeY);
    status |= setIntegerParam(NDArraySize, 0);
    status |= setIntegerParam(NDDataType,  NDUInt32);
    status |= setIntegerParam(ADImageMode, ADImageContinuous);
    status |= setDoubleParam (ADAcquireTime, .001);

    status |= setDoubleParam (Apex2SFRMTimeout, 20.);
    status =  setStringParam (Apex2AcqCmd, " ");
       
    if (status) {
        printf("%s: unable to set camera parameters\n", functionName);
        return;
    }
    
    /* Create the thread that updates the images */
    status = (epicsThreadCreate("Apex2DetTask",
                                epicsThreadPriorityMedium,
                                epicsThreadGetStackSize(epicsThreadStackMedium),
                                (EPICSTHREADFUNC)apex2TaskC,
                                this) == NULL);
    if (status) {
        printf("%s:%s epicsThreadCreate failure for image task\n", 
            driverName, functionName);
        return;
    }
}

/* Code for iocsh registration */
static const iocshArg apex2DetectorConfigArg0 = {"Port name", iocshArgString};
static const iocshArg apex2DetectorConfigArg1 = {"BIS port name", iocshArgString};
static const iocshArg apex2DetectorConfigArg2 = {"maxSizeX", iocshArgInt};
static const iocshArg apex2DetectorConfigArg3 = {"maxSizeY", iocshArgInt};
static const iocshArg apex2DetectorConfigArg4 = {"maxBuffers", iocshArgInt};
static const iocshArg apex2DetectorConfigArg5 = {"maxMemory", iocshArgInt};
static const iocshArg apex2DetectorConfigArg6 = {"priority", iocshArgInt};
static const iocshArg apex2DetectorConfigArg7 = {"stackSize", iocshArgInt};
static const iocshArg * const apex2DetectorConfigArgs[] =    {&apex2DetectorConfigArg0,
                                                              &apex2DetectorConfigArg1,
                                                              &apex2DetectorConfigArg2,
                                                              &apex2DetectorConfigArg3,
                                                              &apex2DetectorConfigArg4,
                                                              &apex2DetectorConfigArg5,
                                                              &apex2DetectorConfigArg6,
                                                              &apex2DetectorConfigArg7};
static const iocshFuncDef configApex2Detector = {"apex2DetectorConfig", 8, apex2DetectorConfigArgs};
static void configApex2DetectorCallFunc(const iocshArgBuf *args)
{
    apex2DetectorConfig(args[0].sval, args[1].sval, args[2].ival,  args[3].ival,  
                          args[4].ival, args[5].ival, args[6].ival,  args[7].ival);
}

static void apex2DetectorRegister(void)
{

    iocshRegister(&configApex2Detector, configApex2DetectorCallFunc);
}

extern "C" {
epicsExportRegistrar(apex2DetectorRegister);
}


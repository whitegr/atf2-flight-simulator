<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xml:lang="en" xmlns="http://www.w3.org/1999/xhtml">
<head>
  <title>areaDetector Plugin NDPluginFile</title>
  <meta content="text/html; charset=ISO-8859-1" http-equiv="Content-Type" />
</head>
<body>
  <div style="text-align: center">
    <h1>
      areaDetector Plugin NDPluginFile</h1>
    <h2>
      May 19, 2010</h2>
    <h2>
      Mark Rivers</h2>
    <h2>
      University of Chicago</h2>
  </div>
  <h2>
    Contents</h2>
  <ul>
    <li><a href="#Overview">Overview</a></li>
    <li><a href="#JPEG">JPEG file plugin</a></li>
    <li><a href="#TIFF">TIFF file plugin</a></li>
    <li><a href="#netCDF">netCDF file plugin</a></li>
    <li><a href="#NeXus">NeXus (HDF) file plugin</a></li>
    <li><a href="#Screens">Screen shots</a></li>
  </ul>
  <h2 id="Overview">
    Overview
  </h2>
  <p>
    NDPluginFile is a base class from which actual file plugins are derived. There are
    currently file plugins for JPEG, TIFF, netCDF, and Nexus (HDF) file formats.
  </p>
  <p>
    NDPluginFile inherits from NDPluginDriver. The <a href="areaDetectorDoxygenHTML/class_n_d_plugin_file.html">
      NDPluginFile class documentation</a> describes this class in detail. This class
    is designed to simplify the task of supporting a new file format. A derived class
    to support a new file format will typically need to implement only the pure virtual
    functions openFile(), readFile(), writeFile(), and closeFile(). Note that none of
    the current file plugins actually support the readFile() function yet, but this
    is planned for future releases.
  </p>
  <p>
    The NDArray callback data can be written to disk in 1 of 3 modes:
  </p>
  <ol>
    <li>Single mode. In this mode each NDArray callback results in a separate disk file.</li>
    <li>Capture mode. In this mode a memory buffer is allocated before saving begins.
      Callback arrays are placed into this buffer, and when capture stops the file is
      written to disk. This mode limits the number of frames that can be saved, because
      they all must fit in a memory buffer. It is the fastest mode, with the least probability
      of dropping arrays, because no disk I/O is required while capture is in progress.</li>
    <li>Stream mode. In this mode the data are written to a single disk file for those
      file formats that support multiple arrays per file (currently only netCDF; NeXus/HDF
      support for multiple arrays per file is planned for a future release). Each frame
      is appended to the file without closing it. It is intermediate in speed between
      single mode and capture mode, but unlike capture mode it is not limited by available
      memory in the number of arrays that can be saved. For file formats that do not support
      multiple arrays per file (e.g. JPEG, TIFF and currently NeXus/HDF) this mode is
      really the same as Single mode, except that one can specify a total number of files
      to save before stopping.</li>
  </ol>
  <p>
    At least one array with the same datatype, array size, and attributes must have
    been collected by the driver (and/or plugins) from which the file saving plugin
    is getting its data <b>before</b> capture or stream mode file saving is started.
    This is required so that the openFile() function can know the dimensions and datatype
    of the arrays.
  </p>
  <p>
    NDPluginFile supports all of the file saving parameters defined in <a href="areaDetectorDoc.html#asynNDArrayDriver">
      asynNDArrayDriver</a>, e.g. NDFilePath, NDFileName, etc. Thus, the same interface
    that is used for saving files directly in a driver is used for this plugin.
  </p>
  <h2 id="JPEG">
    JPEG file plugin
  </h2>
  <p>
    NDFileJPEG inherits from NDPluginFile. This plugin saves data in the <a href="http://en.wikipedia.org/wiki/JPEG">
      JPEG</a> file format, which is a compressed file format for storing images. There
    is JPEG support for almost all languages and programs such as IDL and Matlab.
  </p>
  <p>
    The JPEG plugin is limited to 8-bit arrays. It supports all color modes (Mono, RGB1,
    RGB2, and RGB3). It is limited to a single array per file, but capture and stream
    mode are supported by writing multiple JPEG files.
  </p>
  <p>
    The JPEG plugin supports the Int32 parameter NDFileJPEGQuality to control the amount
    of compression in the file. This parameter varies from 0 (maximum compression, lowest
    quality) to 100 (least compression, best quality). NDFileJPEG.template defines 2
    records to support this: $(P)$(R)JPEGQuality (longout) and $(P)$(R)JPEGQuality_RBV
    (longin).
  </p>
  <p>
    The <a href="areaDetectorDoxygenHTML/class_n_d_file_j_p_e_g.html">NDFileJPEG class
      documentation </a>describes this class in detail.
  </p>
  <p>
    The NDFileJPEG plugin is created with the NDFileJPEGConfigure command, either from
    C/C++ or from the EPICS IOC shell.</p>
  <pre>NDFileJPEGConfigure (const char *portName, int queueSize, int blockingCallbacks, 
                     const char *NDArrayPort, int NDArrayAddr, size_t maxMemory, 
                     int priority, int stackSize)
  </pre>
  <p>
    For details on the meaning of the parameters to this function refer to the detailed
    documentation on the NDFileJPEGConfigure function in the <a href="areaDetectorDoxygenHTML/_n_d_file_j_p_e_g_8cpp.html">
      NDFileJPEG.cpp documentation</a> and in the documentation for the constructor
    for the <a href="areaDetectorDoxygenHTML/class_n_d_file_j_p_e_g.html">NDFileJPEG class</a>.
  </p>
  <h2 id="TIFF">
    TIFF file plugin
  </h2>
  <p>
    NDFileTIFF inherits from NDPluginFile. This plugin saves data in the <a href="http://en.wikipedia.org/wiki/Tagged_Image_File_Format">
      TIFF</a> file format, which is a popular file format for storing images. There
    is TIFF support for almost all languages and programs such as IDL and Matlab.
  </p>
  <p>
    The TIFF plugin is limited to 8, 16 and 32-bit integer arrays. It supports all color
    modes (Mono, RGB1, RGB2, and RGB3). Note that some TIFF readers do not support 16
    or 32 bit TIFF files, and many do not support 16 or 32 bit color files. NDFileTIFF
    is limited to a single array per file, but capture and stream mode are supported
    by writing multiple TIFF files.
  </p>
  <p>
    The <a href="areaDetectorDoxygenHTML/class_n_d_file_t_i_f_f.html">NDFileNetTIFF class
      documentation </a>describes this class in detail.
  </p>
  <p>
    The NDFileTIFF plugin is created with the NDFileTIFFConfigure command, either from
    C/C++ or from the EPICS IOC shell.</p>
  <pre>NDFileTIFFConfigure (const char *portName, int queueSize, int blockingCallbacks, 
                     const char *NDArrayPort, int NDArrayAddr, size_t maxMemory, 
                     int priority, int stackSize)
  </pre>
  <p>
    For details on the meaning of the parameters to this function refer to the detailed
    documentation on the NDFileTIFFConfigure function in the <a href="areaDetectorDoxygenHTML/_n_d_file_t_i_f_f_8cpp.html">
      NDFileTIFF.cpp documentation</a> and in the documentation for the constructor
    for the <a href="areaDetectorDoxygenHTML/class_n_d_file_t_i_f_f.html">NDFileTIFF class</a>.
  </p>
  <h2 id="netCDF">
    netCDF file plugin
  </h2>
  <p>
    NDFileNetCDF inherits from NDPluginFile. This plugin saves data in the <a href="http://www.unidata.ucar.edu/software/netcdf/">
      netCDF</a> file format, which is a portable self-describing binary file format
    supported by <a href="http://www.unidata.ucar.edu/">UniData</a> at <a href="http://www.ucar.edu/">
      UCAR (University Corporation for Atmospheric Research).</a> There are netCDF libraries
    for C, C++, Fortran, and Java. Other languages, including Matlab and IDL have built-in
    support for netCDF. There are also add-on interfaces available for Python, Ruby
    and other languages.
  </p>
  <p>
    The netCDF plugin supports all NDArray data types and any number of array dimensions.
    It also has full support for NDArray attributes. It will write all attributes associated
    with the NDArray to the file. If multiple arrays are written to a single netCDF
    file (stream or capture mode) then each attribute will be an array, with the attribute
    value for each NDArray in the file being stored. Note that the number and data types
    of attributes must not be changed while file capture or file streaming are in progress
    because that would change the structure of the attribute array. Also the colorMode
    attribute must not be changed while capture or streaming is in progress, because
    that would change the structure of the NDArray data.</p>
  <p>
    The <a href="areaDetectorDoxygenHTML/class_n_d_file_net_c_d_f.html">NDFileNetCDF class
      documentation </a>describes this class in detail.
  </p>
  <p>
    The NDFileNetCDF plugin is created with the NDFileNetCDFConfigure command, either
    from C/C++ or from the EPICS IOC shell.</p>
  <pre>NDFileNetCDFConfigure (const char *portName, int queueSize, int blockingCallbacks, 
                       const char *NDArrayPort, int NDArrayAddr, size_t maxMemory, 
                       int priority, int stackSize)
  </pre>
  <p>
    For details on the meaning of the parameters to this function refer to the detailed
    documentation on the NDFileNetCDFConfigure function in the <a href="areaDetectorDoxygenHTML/_n_d_file_net_c_d_f_8cpp.html">
      NDFileNetCDF.cpp documentation</a> and in the documentation for the constructor
    for the <a href="areaDetectorDoxygenHTML/class_n_d_file_net_c_d_f.html">NDFileNetCDF
      class</a>.
  </p>
  <p>
    The following is the header contents of a netCDF file produced by this plugin. This
    information was produced with the following command:</p>
  <pre>ncdump -h test_netCDF_68.nc

netcdf test_netCDF_68 {
dimensions:
	numArrays = UNLIMITED ; // (10 currently)
	dim0 = 240 ;
	dim1 = 320 ;
	dim2 = 1 ;
	attrStringSize = 256 ;
variables:
	int uniqueId(numArrays) ;
	double timeStamp(numArrays) ;
	float array_data(numArrays, dim0, dim1, dim2) ;
	int Attr_colorMode(numArrays) ;
	double Attr_AcquireTime(numArrays) ;
	double Attr_RingCurrent(numArrays) ;
	int Attr_ImageCounter(numArrays) ;
	char Attr_CameraModel(numArrays, attrStringSize) ;
	int Attr_BinX(numArrays) ;
	int Attr_BinY(numArrays) ;
	double Attr_AttrTimeStamp(numArrays) ;
	double Attr_ROI0Mean(numArrays) ;
	double Attr_ROI1Mean(numArrays) ;
	char Attr_FilePath(numArrays, attrStringSize) ;
	char Attr_FileName(numArrays, attrStringSize) ;

// global attributes:
		:dataType = 6 ;
		:NDNetCDFFileVersion = 3. ;
		:numArrayDims = 3 ;
		:dimSize = 1, 320, 240 ;
		:dimOffset = 0, 0, 0 ;
		:dimBinning = 1, 2, 2 ;
		:dimReverse = 0, 0, 0 ;
		:Attr_colorMode_DataType = "Int32" ;
		:Attr_colorMode_Description = "Color mode" ;
		:Attr_colorMode_Source =  ;
		:Attr_colorMode_SourceType = "Driver" ;
		:Attr_AcquireTime_DataType = "Float64" ;
		:Attr_AcquireTime_Description = "Camera acquire time" ;
		:Attr_AcquireTime_Source = "13SIM1:cam1:AcquireTime" ;
		:Attr_AcquireTime_SourceType = "EPICS_PV" ;
		:Attr_RingCurrent_DataType = "Float64" ;
		:Attr_RingCurrent_Description = "Storage ring current" ;
		:Attr_RingCurrent_Source = "S:SRcurrentAI" ;
		:Attr_RingCurrent_SourceType = "EPICS_PV" ;
		:Attr_ImageCounter_DataType = "Int32" ;
		:Attr_ImageCounter_Description = "Image counter" ;
		:Attr_ImageCounter_Source = "ARRAY_COUNTER" ;
		:Attr_ImageCounter_SourceType = "Param" ;
		:Attr_CameraModel_DataType = "String" ;
		:Attr_CameraModel_Description = "Camera model" ;
		:Attr_CameraModel_Source = "MODEL" ;
		:Attr_CameraModel_SourceType = "Param" ;
		:Attr_BinX_DataType = "Int32" ;
		:Attr_BinX_Description = "X binning" ;
		:Attr_BinX_Source = "13SIM1:ROI1:0:BinX_RBV" ;
		:Attr_BinX_SourceType = "EPICS_PV" ;
		:Attr_BinY_DataType = "Int32" ;
		:Attr_BinY_Description = "Y binning" ;
		:Attr_BinY_Source = "13SIM1:ROI1:0:BinY_RBV" ;
		:Attr_BinY_SourceType = "EPICS_PV" ;
		:Attr_AttrTimeStamp_DataType = "Float64" ;
		:Attr_AttrTimeStamp_Description = "Time stamp" ;
		:Attr_AttrTimeStamp_Source = "TIME_STAMP" ;
		:Attr_AttrTimeStamp_SourceType = "Param" ;
		:Attr_ROI0Mean_DataType = "Float64" ;
		:Attr_ROI0Mean_Description = "Mean value ROI 0" ;
		:Attr_ROI0Mean_Source = "MEAN_VALUE" ;
		:Attr_ROI0Mean_SourceType = "Param" ;
		:Attr_ROI1Mean_DataType = "Float64" ;
		:Attr_ROI1Mean_Description = "Mean value ROI 0" ;
		:Attr_ROI1Mean_Source = "MEAN_VALUE" ;
		:Attr_ROI1Mean_SourceType = "Param" ;
		:Attr_FilePath_DataType = "String" ;
		:Attr_FilePath_Description = "File path" ;
		:Attr_FilePath_Source = "13SIM1:netCDF1:FilePath_RBV" ;
		:Attr_FilePath_SourceType = "EPICS_PV" ;
		:Attr_FileName_DataType = "String" ;
		:Attr_FileName_Description = "File name" ;
		:Attr_FileName_Source = "13SIM1:netCDF1:FileName_RBV" ;
		:Attr_FileName_SourceType = "EPICS_PV" ;
}  </pre>
  <p>
    ncdump is one of a number of very useful command line utilities that come with the
    netCDF package. The -h option to ncdump means to dump only the header information,
    not the variable data. This is an explanation of this output:
  </p>
  <ul>
    <li>dimensions: numArrays is the number of arrays in the file. It will be 1 for files
      collected in Single mode, and is normally &gt;1 for files collected in Capture or
      Stream mode. For each array dim0 is the slowest varying dimension, dim1 the next
      slowest, etc. attrStringSize is the maximum string length for string attributes.</li>
    <li>variables: There are 15 variables in this netCDF file. uniqueId is the unique
      ID number of each array. timeStamp is the timestamp in seconds for each array. array_data
      is the array data. Its data type depends on the data type of the NDArray data passed
      in the callbacks. It dimensions are [numArrays, dim0, dim1, ...dimN]. This notation
      is in the Fortran syntax where the slowest varying dimension comes first in the
      list. The remaining variables all have the prefix Attr_ and are the attributes for
      the arrays. Each can have its own data type, and all have the numArrays elements.</li>
    <li>global attributes. dataType is the NDDataType_t enum value for the array data
      type. numArrayDims is the number of dimensions in each array. array_data has 1 more
      dimension than this, numArrays, because it contains all of the array callback data.
      dimSize is an array[numArrayDims] containing the size of each dimension, with the
      fastest varying dimension first. dimOffset, dimBinning, and dimReverse are the values
      of the offset, binning and reverse fields in the NDDimension_t structure for each
      dimension. The remaining netCDF global attributes all have the prefix Attr_, and
      describe the NDArray attribute values. For each NDArray attribute there is information
      on the data type, a description, source string and source type. </li>
  </ul>
  <p>
    There is an IDL function, <a href="http://cars.uchicago.edu/software/idl/detector_routines.html#read_nd_netcdf">
      read_nd_netcdf</a> that can be used to read the netCDF files created by this plugin.
    This routine is contained in the <a href="http://cars.uchicago.edu/software/idl/detectors.html#read_nd_netcdf">
      CARS IDL detector package</a>. This function is also contained in the areaDetector
    distribution in the Viewers/IDL directory.
  </p>
  <p>
    There is a plugin for the popular <a href="http://rsbweb.nih.gov/ij/">ImageJ</a>
    program that can be used to read the netCDF files created by this plugin. This ImageJ
    plugin can be downloaded <a href="http://lmb.informatik.uni-freiburg.de/lmbsoft/imagej_plugins/netcdf.en.html">
      here</a>. This plugin is also contained in the areaDetector distribution in the
    Viewers/ImageJ/EPICS_areaDetector directory.
  </p>
  <h2 id="NeXus">
    NeXus (HDF) file plugin
  </h2>
  <p>
    A plugin to write <a href="http://www.nexusformat.org/Main_Page">NeXus</a> files
    was written by John Hammonds from the APS. NeXus is a standard format for x-ray
    and neutron data based on <a href="http://www.hdfgroup.org">HDF</a>. This is a very
    general file format, capable of storing any type of array data and meta-data.</p>
  <p>
    The <a href="areaDetectorDoxygenHTML/class_n_d_file_nexus.html">NDFileNexus class
      documentation </a>describes this class in detail.
  </p>
  <p>
    The NDFileNexus plugin is created with the NDFileNexusConfigure command, either
    from C/C++ or from the EPICS IOC shell.</p>
  <pre>NDFileNexusConfigure (const char *portName, int queueSize, int blockingCallbacks, 
                      const char *NDArrayPort, int NDArrayAddr, size_t maxMemory, 
                      int priority, int stackSize)
  </pre>
  <p>
    For details on the meaning of the parameters to this function refer to the detailed
    documentation on the NDFileNexusConfigure function in the <a href="areaDetectorDoxygenHTML/_n_d_file_nexus_8cpp.html">
      NDFileNexus.cpp documentation</a> and in the documentation for the constructor
    for the <a href="areaDetectorDoxygenHTML/class_n_d_file_nexus.html">NDFileNexus class</a>.</p>
  <p>
    NDFileNeXus uses 2 additional parameters to define the location of an XML file that
    is read to determine the contents of the NeXus files written by this plugin. These
    are described in the following table.</p>
  <table border="1" cellpadding="2" cellspacing="2" style="text-align: left">
    <tbody>
      <tr>
        <td align="center" colspan="7,">
          <b>Parameter Definitions in NDFileNexus.h and EPICS Record Definitions in NDFileNexus.template</b></td>
      </tr>
      <tr>
        <th>
          Parameter index variable</th>
        <th>
          asyn interface</th>
        <th>
          Access</th>
        <th>
          Description</th>
        <th>
          drvInfo string</th>
        <th>
          EPICS record name</th>
        <th>
          EPICS record type</th>
      </tr>
      <tr>
        <td align="center" colspan="7,">
          <b>Location of XML file to configure NeXus file contents</b></td>
      </tr>
      <tr>
        <td>
          NDFileNexusTemplatePath</td>
        <td>
          asynOctet</td>
        <td>
          r/w</td>
        <td>
          Path to XML template file</td>
        <td>
          TEMPLATE_FILE_PATH</td>
        <td>
          $(P)$(R)TemplateFilePath<br />
          $(P)$(R)TemplateFilePath_RBV</td>
        <td>
          waveform<br />
          waveform</td>
      </tr>
      <tr>
        <td>
          NDFileNexusTemplateFile</td>
        <td>
          asynOctet</td>
        <td>
          r/w</td>
        <td>
          Name of XML template file</td>
        <td>
          TEMPLATE_FILE_NAME</td>
        <td>
          $(P)$(R)TemplateFileName<br />
          $(P)$(R)TemplateFileName_RBV</td>
        <td>
          waveform<br />
          waveform</td>
      </tr>
    </tbody>
  </table>
  <p>
    There is currently no documentation on the contents of the XML template file. However,
    there are example XML template files in the iocSimDetector and iocPerkinElmer directories.
    Documentation on the XML file contents will be written ASAP.</p>
  <p>
    The prebuilt Linux libraries libhdf5. and libNeXus.a are built with HDF5 1.6.9.
    When they are built with the latest version, 1.8.2, they require GLIBC version 2.7
    or higher, i.e. /lib/libc-2.7.so or higher. Since users may want to install areaDetector
    on older Linux systems (which predate Fedora Core 8 for example), it was decided
    to use this older version of HDF5. Future releases of areaDetector may use HDF5
    1.8.2 or later, and hence not work with older Linux systems.</p>
  <h2 id="Screens">
    Screen shots</h2>
  <p>
    The following is the MEDM screen that provides access to the parameters in NDPluginDriver.h
    and NDPluginFile.h through records in NDPluginBase.template and NDFileNetCDF.template.
    This is the MEDM screen that is used to control the saving of images to disk in
    netCDF format.</p>
  <div style="text-align: center">
    <h3>
      NDPluginFileNetCDF.adl</h3>
    <p>
      <img alt="NDFileNetCDF.png" src="NDFileNetCDF.png" /></p>
  </div>
</body>
</html>

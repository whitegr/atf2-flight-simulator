< envPaths
errlogInit(20000)

dbLoadDatabase("$(AREA_DETECTOR)/dbd/apex2DetectorApp.dbd")
apex2DetectorApp_registerRecordDeviceDriver(pdbbase) 

###
# Create the asyn port to talk to the APEX2 on command port 49153.
drvAsynIPPortConfigure("BIS1","chemmat21:49153")
# Create the asyn port to talk to the APEX2 on status port 49155.
drvAsynIPPortConfigure("BIS2","chemmat21:49155")

# Set the output terminator.
asynOctetSetOutputEos("BIS1", 0, "\n")

apex2DetectorConfig("APX", "BIS1", 512, 512, 50, 200000000)
dbLoadRecords("$(AREA_DETECTOR)/ADApp/Db/ADBase.template", "P=APEX2:,R=bis1:,PORT=APX,ADDR=0,TIMEOUT=1")
dbLoadRecords("$(AREA_DETECTOR)/ADApp/Db/NDFile.template", "P=APEX2:,R=bis1:,PORT=APX,ADDR=0,TIMEOUT=1")
dbLoadRecords("$(AREA_DETECTOR)/ADApp/Db/apex2.template","P=APEX2:,R=bis1:,PORT=APX,ADDR=0,TIMEOUT=1,BIS_PORT=BIS1")

# Create a standard arrays plugin
NDStdArraysConfigure("apxImage", 5, 0, "APX", 0, -1)
dbLoadRecords("$(AREA_DETECTOR)/ADApp/Db/NDPluginBase.template","P=APEX2:,R=image1:,PORT=apxImage,ADDR=0,TIMEOUT=1,NDARRAY_PORT=APX,NDARRAY_ADDR=0")
dbLoadRecords("$(AREA_DETECTOR)/ADApp/Db/NDStdArrays.template", "P=APEX2:,R=image1:,PORT=apxImage,ADDR=0,TIMEOUT=1,TYPE=Int32,FTVL=LONG,NELEMENTS=262144")

# Create an ROI plugin
NDROIConfigure("apxROI", 5, 0, "APX", 0, 10, 20, -1)
dbLoadRecords("$(AREA_DETECTOR)/ADApp/Db/NDPluginBase.template","P=APEX2:,R=ROI1:,  PORT=apxROI,ADDR=0,TIMEOUT=1,NDARRAY_PORT=APX,NDARRAY_ADDR=0")
dbLoadRecords("$(AREA_DETECTOR)/ADApp/Db/NDROI.template",       "P=APEX2:,R=ROI1:,  PORT=apxROI,ADDR=0,TIMEOUT=1")
dbLoadRecords("$(AREA_DETECTOR)/ADApp/Db/NDROIN.template",      "P=APEX2:,R=ROI1:0:,PORT=apxROI,ADDR=0,TIMEOUT=1,HIST_SIZE=256")
dbLoadRecords("$(AREA_DETECTOR)/ADApp/Db/NDROIN.template",      "P=APEX2:,R=ROI1:1:,PORT=apxROI,ADDR=1,TIMEOUT=1,HIST_SIZE=256")
dbLoadRecords("$(AREA_DETECTOR)/ADApp/Db/NDROIN.template",      "P=APEX2:,R=ROI1:2:,PORT=apxROI,ADDR=2,TIMEOUT=1,HIST_SIZE=256")
dbLoadRecords("$(AREA_DETECTOR)/ADApp/Db/NDROIN.template",      "P=APEX2:,R=ROI1:3:,PORT=apxROI,ADDR=3,TIMEOUT=1,HIST_SIZE=256")
dbLoadRecords("$(AREA_DETECTOR)/ADApp/Db/NDROIN.template",      "P=APEX2:,R=ROI1:4:,PORT=apxROI,ADDR=3,TIMEOUT=1,HIST_SIZE=256")
dbLoadRecords("$(AREA_DETECTOR)/ADApp/Db/NDROIN.template",      "P=APEX2:,R=ROI1:5:,PORT=apxROI,ADDR=3,TIMEOUT=1,HIST_SIZE=256")
dbLoadRecords("$(AREA_DETECTOR)/ADApp/Db/NDROIN.template",      "P=APEX2:,R=ROI1:6:,PORT=apxROI,ADDR=3,TIMEOUT=1,HIST_SIZE=256")
dbLoadRecords("$(AREA_DETECTOR)/ADApp/Db/NDROIN.template",      "P=APEX2:,R=ROI1:7:,PORT=apxROI,ADDR=3,TIMEOUT=1,HIST_SIZE=256")

# Create "fastSweep" drivers for the MCA record to do on-the-fly scanning of ROI data
initFastSweep("apxSweepTotal", "apxROI", 8, 2048, "TOTAL_ARRAY", "CALLBACK_PERIOD")
initFastSweep("apxSweepNet", "apxROI", 8, 2048, "NET_ARRAY", "CALLBACK_PERIOD")

# Load MCA records for the fast sweep drivers
dbLoadRecords("$(MCA)/mcaApp/Db/mca.db", "P=APEX2:,M=ROI1:0:TotalArray,DTYP=asynMCA,NCHAN=2048,INP=@asyn(apxSweepTotal 0)")
dbLoadRecords("$(MCA)/mcaApp/Db/mca.db", "P=APEX2:,M=ROI1:1:TotalArray,DTYP=asynMCA,NCHAN=2048,INP=@asyn(apxSweepTotal 1)")
dbLoadRecords("$(MCA)/mcaApp/Db/mca.db", "P=APEX2:,M=ROI1:2:TotalArray,DTYP=asynMCA,NCHAN=2048,INP=@asyn(apxSweepTotal 2)")
dbLoadRecords("$(MCA)/mcaApp/Db/mca.db", "P=APEX2:,M=ROI1:3:TotalArray,DTYP=asynMCA,NCHAN=2048,INP=@asyn(apxSweepTotal 3)")
dbLoadRecords("$(MCA)/mcaApp/Db/mca.db", "P=APEX2:,M=ROI1:4:TotalArray,DTYP=asynMCA,NCHAN=2048,INP=@asyn(apxSweepTotal 4)")
dbLoadRecords("$(MCA)/mcaApp/Db/mca.db", "P=APEX2:,M=ROI1:5:TotalArray,DTYP=asynMCA,NCHAN=2048,INP=@asyn(apxSweepTotal 5)")
dbLoadRecords("$(MCA)/mcaApp/Db/mca.db", "P=APEX2:,M=ROI1:6:TotalArray,DTYP=asynMCA,NCHAN=2048,INP=@asyn(apxSweepTotal 6)")
dbLoadRecords("$(MCA)/mcaApp/Db/mca.db", "P=APEX2:,M=ROI1:7:TotalArray,DTYP=asynMCA,NCHAN=2048,INP=@asyn(apxSweepTotal 7)")

dbLoadRecords("$(MCA)/mcaApp/Db/mca.db", "P=APEX2:,M=ROI1:0:NetArray,DTYP=asynMCA,NCHAN=2048,INP=@asyn(apxSweepNet 0)")
dbLoadRecords("$(MCA)/mcaApp/Db/mca.db", "P=APEX2:,M=ROI1:1:NetArray,DTYP=asynMCA,NCHAN=2048,INP=@asyn(apxSweepNet 1)")
dbLoadRecords("$(MCA)/mcaApp/Db/mca.db", "P=APEX2:,M=ROI1:2:NetArray,DTYP=asynMCA,NCHAN=2048,INP=@asyn(apxSweepNet 2)")
dbLoadRecords("$(MCA)/mcaApp/Db/mca.db", "P=APEX2:,M=ROI1:3:NetArray,DTYP=asynMCA,NCHAN=2048,INP=@asyn(apxSweepNet 3)")
dbLoadRecords("$(MCA)/mcaApp/Db/mca.db", "P=APEX2:,M=ROI1:4:NetArray,DTYP=asynMCA,NCHAN=2048,INP=@asyn(apxSweepNet 4)")
dbLoadRecords("$(MCA)/mcaApp/Db/mca.db", "P=APEX2:,M=ROI1:5:NetArray,DTYP=asynMCA,NCHAN=2048,INP=@asyn(apxSweepNet 5)")
dbLoadRecords("$(MCA)/mcaApp/Db/mca.db", "P=APEX2:,M=ROI1:6:NetArray,DTYP=asynMCA,NCHAN=2048,INP=@asyn(apxSweepNet 6)")
dbLoadRecords("$(MCA)/mcaApp/Db/mca.db", "P=APEX2:,M=ROI1:7:NetArray,DTYP=asynMCA,NCHAN=2048,INP=@asyn(apxSweepNet 7)")


#asynSetTraceMask("APX",0,255)
#asynSetTraceMask("apxROI",0,3)
#asynSetTraceIOMask("apxROI",0,4)

# Load scan records for scanning energy threshold
dbLoadRecords("$(SSCAN)/sscanApp/Db/scan.db", "P=APEX2:,MAXPTS1=2000,MAXPTS2=200,MAXPTS3=20,MAXPTS4=10,MAXPTSH=10")

set_requestfile_path("./")
set_savefile_path("./autosave")
set_requestfile_path("$(AREA_DETECTOR)/ADApp/Db")
set_requestfile_path("$(SSCAN)/sscanApp/Db")
set_pass0_restoreFile("auto_settings.sav")
set_pass1_restoreFile("auto_settings.sav")
save_restoreSet_status_prefix("APEX2:")
dbLoadRecords("$(AUTOSAVE)/asApp/Db/save_restoreStatus.db", "P=APEX2:")

iocInit()

# save things every thirty seconds
create_monitor_set("auto_settings.req", 30,"P=APEX2:,D=bis1:")

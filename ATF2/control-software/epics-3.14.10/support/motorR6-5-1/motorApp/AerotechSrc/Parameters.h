// File contains parameter numbers and header for Parameters.cpp
// NOTE: this file is controller firmware version dependant and needs to match it

#ifndef __PARAMETERS_H__
#define __PARAMETERS_H__

#define NUMBER_OF_PARAMETERS 340

typedef enum {
	ParameterNumber_PosScaleFactor = 3,
	ParameterNumber_ServoUpdateRate = 4,
	ParameterNumber_ServoMask = 5,
	ParameterNumber_GainKpos = 6,
	ParameterNumber_GainKi = 7,
	ParameterNumber_GainKp = 8,
	ParameterNumber_GainVff = 9,
	ParameterNumber_GainAff = 10,
	ParameterNumber_GainKv = 11,
	ParameterNumber_GainKpi = 12,
	ParameterNumber_FiltTorq1N0 = 13,
	ParameterNumber_FiltTorq1N1 = 14,
	ParameterNumber_FiltTorq1N2 = 15,
	ParameterNumber_FiltTorq1D1 = 16,
	ParameterNumber_FiltTorq1D2 = 17,
	ParameterNumber_FiltTorq2N0 = 18,
	ParameterNumber_FiltTorq2N1 = 19,
	ParameterNumber_FiltTorq2N2 = 20,
	ParameterNumber_FiltTorq2D1 = 21,
	ParameterNumber_FiltTorq2D2 = 22,
	ParameterNumber_IGainDeadTimeUsec = 23,
	ParameterNumber_IGainK = 25,
	ParameterNumber_AxisRolloverDistCnts = 26,
	ParameterNumber_IGainKi = 27,
	ParameterNumber_IGainKp = 28,
	ParameterNumber_IGainIaOffset = 29,
	ParameterNumber_IGainIbOffset = 30,
	ParameterNumber_IGainVaOffset = 31,
	ParameterNumber_IGainVbOffset = 32,
	ParameterNumber_IGainVcOffset = 33,
	ParameterNumber_FaultMaskGlobal = 34,
	ParameterNumber_FaultMaskDisable = 35,
	ParameterNumber_FaultMaskDecel = 36,
	ParameterNumber_FaultMaskStop = 37,
	ParameterNumber_BrakeOnDriveDisable = 39,
	ParameterNumber_FaultMaskAux = 40,
	ParameterNumber_FaultEstopInput = 41,
	ParameterNumber_ThresholdPosErr = 42,
	ParameterNumber_ThresholdAvgIAmp = 43,
	ParameterNumber_ThresholdAvgITimeMsec = 44,
	ParameterNumber_ThresholdVelCmd = 45,
	ParameterNumber_ThresholdVelError = 46,
	ParameterNumber_ThresholdSoftCCW = 47,
	ParameterNumber_ThresholdSoftCW = 48,
	ParameterNumber_ThresholdClampIAmp = 49,
	ParameterNumber_ThresholdInPosDist = 50,
	ParameterNumber_CfgMotType = 51,
	ParameterNumber_CfgMotCyclesRev = 52,
	ParameterNumber_CfgMotCntsRev = 53,
	ParameterNumber_CfgMotOffsetAng = 55,
	ParameterNumber_CfgMotMSetTimeMsec = 56,
	ParameterNumber_CfgMotMSetIAmps = 57,
	ParameterNumber_CfgFbkPosType = 58,
	ParameterNumber_CfgFbkPosChan = 59,
	ParameterNumber_CfgFbkPosMultiplier = 60,
	ParameterNumber_CfgFbkVelType = 61,
	ParameterNumber_CfgFbkVelChan = 62,
	ParameterNumber_CfgFbkVelMultiplier = 63,
	ParameterNumber_CfgFbkMultFactorMX = 66,
	ParameterNumber_CfgFbkEncSineGain = 67,
	ParameterNumber_CfgFbkEncSineOffset = 68,
	ParameterNumber_CfgFbkEncCosineGain = 69,
	ParameterNumber_CfgFbkEncCosineOffset = 70,
	ParameterNumber_CfgFbkEncPhase = 71,
	ParameterNumber_GantryMasterAxis = 72,
	ParameterNumber_GantrySlaveAxis = 73,
	ParameterNumber_MBusMasterSlaveIPAddress = 74,
	ParameterNumber_MBusMasterSlaveSubnetMask = 75,
	ParameterNumber_MBusMasterSlaveGateway = 76,
	ParameterNumber_MBusMasterInputWords = 77,
	ParameterNumber_MBusMasterOutputWords = 78,
	ParameterNumber_MBusMasterInputBits = 79,
	ParameterNumber_MBusMasterOutputBits = 80,
	ParameterNumber_MBusMasterVirtualInputs = 81,
	ParameterNumber_MBusMasterVirtualOutputs = 82,
	ParameterNumber_LimitDecelDistCnts = 84,
	ParameterNumber_LimitDebounceTimeMsec = 85,
	ParameterNumber_LimitLevelMask = 86,
	ParameterNumber_BacklashDistCnts = 87,
	ParameterNumber_AuxOutBitMask = 88,
	ParameterNumber_AuxOutBitLevel = 89,
	ParameterNumber_DriveIOConfig = 90,
	ParameterNumber_BrakeOutput = 91,
	ParameterNumber_MBusMasterOutputWordsSections = 93,
	ParameterNumber_MBusMasterOutputBitsSections = 94,
	ParameterNumber_MBusMasterRWReadOffset = 95,
	ParameterNumber_AdcScaleFactor = 96,
	ParameterNumber_EncoderDivider = 97,
	ParameterNumber_FaultAuxInput = 98,
	ParameterNumber_BrakeDisableDelay = 99,
	ParameterNumber_DacScaleFactor = 100,
	ParameterNumber_MaxJogDistance = 101,
	ParameterNumber_DefaultSpeed = 102,
	ParameterNumber_DefaultRampRate = 103,
	ParameterNumber_DecelRateMoveAbort = 104,
	ParameterNumber_HomeType = 105,
	ParameterNumber_HomeDirection = 106,
	ParameterNumber_HomeFeedRate = 107,
	ParameterNumber_HomeOffset = 108,
	ParameterNumber_HomeAccelDecelRate = 109,
	ParameterNumber_DefaultWaitMode = 110,
	ParameterNumber_DefaultSCurve = 111,
	ParameterNumber_MotionBuffer = 112,
	ParameterNumber_DataCollectBufferSize = 113,
	ParameterNumber_CfgMotStepperRes = 114,
	ParameterNumber_CfgMotStepperHighCur = 115,
	ParameterNumber_CfgMotStepperLowCur = 116,
	ParameterNumber_CfgMotStepperDGain = 117,
	ParameterNumber_CfgMotStepperVerVel = 118,
	ParameterNumber_LimitDebounceDistCnts = 119,
	ParameterNumber_FiltTorq3N0 = 120,
	ParameterNumber_FiltTorq3N1 = 121,
	ParameterNumber_FiltTorq3N2 = 122,
	ParameterNumber_FiltTorq3D1 = 123,
	ParameterNumber_FiltTorq3D2 = 124,
	ParameterNumber_FiltTorq4N0 = 125,
	ParameterNumber_FiltTorq4N1 = 126,
	ParameterNumber_FiltTorq4N2 = 127,
	ParameterNumber_FiltTorq4D1 = 128,
	ParameterNumber_FiltTorq4D2 = 129,
	ParameterNumber_PrintStatementBufferSize = 138,
	ParameterNumber_AnalogInputOffset0 = 139,
	ParameterNumber_AnalogInputOffset1 = 140,
	ParameterNumber_ComPortXonChar = 146,
	ParameterNumber_ComPortXoffChar = 147,
	ParameterNumber_ComPortBaudRate = 148,
	ParameterNumber_ComPortSettings = 149,
	ParameterNumber_CfgTask0Slot = 151,
	ParameterNumber_CfgTask1Slot = 152,
	ParameterNumber_CfgTask2Slot = 153,
	ParameterNumber_CfgTask3Slot = 154,
	ParameterNumber_CfgTask4Slot = 155,
	ParameterNumber_CfgTask0Code = 156,
	ParameterNumber_CfgTask1Code = 157,
	ParameterNumber_CfgTask2Code = 158,
	ParameterNumber_CfgTask3Code = 159,
	ParameterNumber_CfgTask4Code = 160,
	ParameterNumber_CfgTask0Data = 161,
	ParameterNumber_CfgTask1Data = 162,
	ParameterNumber_CfgTask2Data = 163,
	ParameterNumber_CfgTask3Data = 164,
	ParameterNumber_CfgTask4Data = 165,
	ParameterNumber_CfgTask0Stack = 166,
	ParameterNumber_CfgTask1Stack = 167,
	ParameterNumber_CfgTask2Stack = 168,
	ParameterNumber_CfgTask3Stack = 169,
	ParameterNumber_CfgTask4Stack = 170,
	ParameterNumber_CfgTask0SysStack = 171,
	ParameterNumber_CfgTask1SysStack = 172,
	ParameterNumber_CfgTask2SysStack = 173,
	ParameterNumber_CfgTask3SysStack = 174,
	ParameterNumber_CfgTask4SysStack = 175,
	ParameterNumber_CfgTask0AutoProgram = 176,
	ParameterNumber_CfgTask1AutoProgram = 180,
	ParameterNumber_CfgTask2AutoProgram = 184,
	ParameterNumber_CfgTask3AutoProgram = 188,
	ParameterNumber_CfgTask4AutoProgram = 192,
	ParameterNumber_MaxJogSpeed = 201,
	ParameterNumber_CfgIntegerRegisters = 202,
	ParameterNumber_CfgDoubleRegisters = 203,
	ParameterNumber_PositionDecimalPlaces = 204,
	ParameterNumber_CfgTask0TaskFaultAxisMask = 205,
	ParameterNumber_CfgTask1TaskFaultAxisMask = 206,
	ParameterNumber_CfgTask2TaskFaultAxisMask = 207,
	ParameterNumber_CfgTask3TaskFaultAxisMask = 208,
	ParameterNumber_CfgTask4TaskFaultAxisMask = 209,
	ParameterNumber_AxisCalibrationFile = 210,
	ParameterNumber_InetGlbIntStartReg = 214,
	ParameterNumber_InetGlbIntNumReg = 215,
	ParameterNumber_InetGlbDblStartReg = 216,
	ParameterNumber_InetGlbDblNumReg = 217,
	ParameterNumber_InetGlbRegBcastRate = 218,
	ParameterNumber_InetGlbRegEnableFlags = 219,
	ParameterNumber_MBusSlaveUnitID = 220,
	ParameterNumber_MBusSlaveInputWords = 221,
	ParameterNumber_MBusSlaveOutputWords = 222,
	ParameterNumber_MBusSlaveInputBits = 223,
	ParameterNumber_MBusSlaveOutputBits = 224,
	ParameterNumber_MBusSlaveInputWordsOffset = 225,
	ParameterNumber_MBusSlaveOutputWordsOffset = 226,
	ParameterNumber_MBusSlaveInputBitsOffset = 227,
	ParameterNumber_MBusSlaveOutputBitsOffset = 228,
	ParameterNumber_MBusSlaveRWReadOffset = 229,
	ParameterNumber_MBusSlaveRWWriteOffset = 230,
	ParameterNumber_MBusMasterInputWordsOffset = 231,
	ParameterNumber_MBusMasterOutputWordsOffset = 232,
	ParameterNumber_MBusMasterInputBitsOffset = 233,
	ParameterNumber_MBusMasterOutputBitsOffset = 234,
	ParameterNumber_MBusMasterStatusWordsOffset = 235,
	ParameterNumber_MBusMasterStatusBitsOffset = 236,
	ParameterNumber_MBusMasterVirtualInputsOffset = 237,
	ParameterNumber_MBusMasterVirtualOutputsOffset = 238,
	ParameterNumber_MBusMasterRWWriteOffset = 239,
	ParameterNumber_MBusMasterFunctions = 240,
	ParameterNumber_MBusConfigFlags = 241,
	ParameterNumber_MBusSlaveType = 242,
	ParameterNumber_PosUnitsDisplay = 243,
	ParameterNumber_InetSock1IPAddress = 247,
	ParameterNumber_InetSock1Port = 248,
	ParameterNumber_InetSock1Flags = 249,
	ParameterNumber_InetSock1OutThreshBytes = 250,
	ParameterNumber_InetSock2IPAddress = 251,
	ParameterNumber_InetSock2Port = 252,
	ParameterNumber_InetSock2Flags = 253,
	ParameterNumber_InetSock2OutThreshBytes = 254,
	ParameterNumber_InetSock1ActiveTimeSec = 255,
	ParameterNumber_InetSock2ActiveTimeSec = 256,
	ParameterNumber_UserInteger1 = 261,
	ParameterNumber_UserInteger2 = 262,
	ParameterNumber_UserDouble1 = 263,
	ParameterNumber_UserDouble2 = 264,
	ParameterNumber_UserString1 = 265,
	ParameterNumber_UserString2 = 269,
	ParameterNumber_CfgFbkEncAbsSetup = 273,
	ParameterNumber_CfgFbkEncAbsPosBits = 274,
	ParameterNumber_CfgFbkEncAbsRevBits = 275,
	ParameterNumber_AsciiCmdEnable = 281,
	ParameterNumber_ComPortAuxXonChar = 283,
	ParameterNumber_ComPortAuxXoffChar = 284,
	ParameterNumber_ComPortAuxBaudRate = 285,
	ParameterNumber_ComPortAuxSettings = 286,
	ParameterNumber_JoystickVertCenter = 288,
	ParameterNumber_JoystickVertDeadband = 289,
	ParameterNumber_JoystickHorzCenter = 290,
	ParameterNumber_JoystickHorzDeadband = 291,
	ParameterNumber_JoystickFeedrateLow = 292,
	ParameterNumber_JoystickFeedrateHigh = 293,
	ParameterNumber_JoystickAxisPair1 = 294,
	ParameterNumber_JoystickAxisPair2 = 295,
	ParameterNumber_JoystickAxisPair3 = 296,
	ParameterNumber_JoystickAxisPair4 = 297,
	ParameterNumber_JoystickConfig = 298,
	ParameterNumber_HomePositionSet = 299,
	ParameterNumber_CfgTask0AxisMask = 300,
	ParameterNumber_CfgTask1AxisMask = 301,
	ParameterNumber_CfgTask2AxisMask = 302,
	ParameterNumber_CfgTask3AxisMask = 303,
	ParameterNumber_CfgTask4AxisMask = 304,
	ParameterNumber_AxisCalibrationFile2D = 305,
	ParameterNumber_FaultMaskDisableDelay = 309,
	ParameterNumber_DefaultVectorSpeed = 310,
	ParameterNumber_DefaultVectorRampRate = 311,
	ParameterNumber_CfgTask0StopOnAxisFault = 312,
	ParameterNumber_CfgTask1StopOnAxisFault = 313,
	ParameterNumber_CfgTask2StopOnAxisFault = 314,
	ParameterNumber_CfgTask3StopOnAxisFault = 315,
	ParameterNumber_CfgTask4StopOnAxisFault = 316,
	ParameterNumber_GpibEOSChar = 317,
	ParameterNumber_GpibPrimaryAddress = 318,
	ParameterNumber_GpibParallelResponse = 319,
	ParameterNumber_AsciiCmdEOSChar = 320,
	ParameterNumber_AsciiCmdAckChar = 321,
	ParameterNumber_AsciiCmdNakChar = 322,
	ParameterNumber_AsciiCmdFaultChar = 323,
	ParameterNumber_CfgTask0StopFaultMask = 324,
	ParameterNumber_CfgTask1StopFaultMask = 325,
	ParameterNumber_CfgTask2StopFaultMask = 326,
	ParameterNumber_CfgTask3StopFaultMask = 327,
	ParameterNumber_CfgTask4StopFaultMask = 328,
	ParameterNumber_FaultAxisGroup = 330,
	ParameterNumber_HarmonicCancellation0Period = 336,
	ParameterNumber_HarmonicCancellation0Type = 337,
	ParameterNumber_HarmonicCancellation0Channel = 338,
	ParameterNumber_HarmonicCancellation0Gain = 339,
	ParameterNumber_HarmonicCancellation0Phase = 340,
	ParameterNumber_HarmonicCancellation1Period = 341,
	ParameterNumber_HarmonicCancellation1Type = 342,
	ParameterNumber_HarmonicCancellation1Channel = 343,
	ParameterNumber_HarmonicCancellation1Gain = 344,
	ParameterNumber_HarmonicCancellation1Phase = 345,
	ParameterNumber_HarmonicCancellation2Period = 346,
	ParameterNumber_HarmonicCancellation2Type = 347,
	ParameterNumber_HarmonicCancellation2Channel = 348,
	ParameterNumber_HarmonicCancellation2Gain = 349,
	ParameterNumber_HarmonicCancellation2Phase = 350,
	ParameterNumber_AsciiCmdTimeout = 356,
	ParameterNumber_AsciiCmdTimeoutChar = 357,
	ParameterNumber_CfgFbkRDGain = 358,
	ParameterNumber_CfgFbkRDConfig = 359,
	ParameterNumber_CfgFbkRDCosPhase = 360,
	ParameterNumber_SoftLimitMode = 367,
	ParameterNumber_PsoSSI1Config = 368,
	ParameterNumber_PsoSSI2Config = 369,
	ParameterNumber_CfgFbkEncQuadDivider = 370,
	ParameterNumber_HarmonicCancellation3Period = 371,
	ParameterNumber_HarmonicCancellation3Type = 372,
	ParameterNumber_HarmonicCancellation3Channel = 373,
	ParameterNumber_HarmonicCancellation3Gain = 374,
	ParameterNumber_HarmonicCancellation3Phase = 375,
	ParameterNumber_HarmonicCancellation4Period = 376,
	ParameterNumber_HarmonicCancellation4Type = 377,
	ParameterNumber_HarmonicCancellation4Channel = 378,
	ParameterNumber_HarmonicCancellation4Gain = 379,
	ParameterNumber_HarmonicCancellation4Phase = 380,
	ParameterNumber_EnhancedThroughputChannel = 381,
	ParameterNumber_EnhancedThroughputGain = 382,
	ParameterNumber_HarmonicCancellationSetup = 383,
	ParameterNumber_EnhancedThroughputCurrentClamp = 384,
	ParameterNumber_Analog0Filter0CoeffN0 = 385,
	ParameterNumber_Analog0Filter0CoeffN1 = 386,
	ParameterNumber_Analog0Filter0CoeffN2 = 387,
	ParameterNumber_Analog0Filter0CoeffD1 = 388,
	ParameterNumber_Analog0Filter0CoeffD2 = 389,
	ParameterNumber_Analog0Filter1CoeffN0 = 390,
	ParameterNumber_Analog0Filter1CoeffN1 = 391,
	ParameterNumber_Analog0Filter1CoeffN2 = 392,
	ParameterNumber_Analog0Filter1CoeffD1 = 393,
	ParameterNumber_Analog0Filter1CoeffD2 = 394,
	ParameterNumber_Analog1Filter0CoeffN0 = 395,
	ParameterNumber_Analog1Filter0CoeffN1 = 396,
	ParameterNumber_Analog1Filter0CoeffN2 = 397,
	ParameterNumber_Analog1Filter0CoeffD1 = 398,
	ParameterNumber_Analog1Filter0CoeffD2 = 399,
	ParameterNumber_Analog1Filter1CoeffN0 = 400,
	ParameterNumber_Analog1Filter1CoeffN1 = 401,
	ParameterNumber_Analog1Filter1CoeffN2 = 402,
	ParameterNumber_Analog1Filter1CoeffD1 = 403,
	ParameterNumber_Analog1Filter1CoeffD2 = 404,
	ParameterNumber_ThresholdScheduleSetup = 405,
	ParameterNumber_ThresholdRegion2High = 406,
	ParameterNumber_ThresholdRegion2Low = 407,
	ParameterNumber_ThresholdRegion3GainKpos = 408,
	ParameterNumber_ThresholdRegion3GainKp = 409,
	ParameterNumber_ThresholdRegion3GainKi = 410,
	ParameterNumber_ThresholdRegion3GainKpi = 411,
	ParameterNumber_ThresholdRegion4High = 412,
	ParameterNumber_ThresholdRegion4Low = 413,
	ParameterNumber_ThresholdRegion5GainKpos = 414,
	ParameterNumber_ThresholdRegion5GainKp = 415,
	ParameterNumber_ThresholdRegion5GainKi = 416,
	ParameterNumber_ThresholdRegion5GainKpi = 417,
	ParameterNumber_DynamicScheduleSetup = 418,
	ParameterNumber_DynamicGainKposScale = 419,
	ParameterNumber_DynamicGainKpScale = 420,
	ParameterNumber_DynamicGainKiScale = 421,
	ParameterNumber_LinearAmpMaxPower = 422,
	ParameterNumber_LinearAmpDeratingFactor = 423,
	ParameterNumber_LinearAmpBusVoltage = 424,
	ParameterNumber_MotorResistance = 425,
	ParameterNumber_MotorBackEMFConstant = 426
} ParameterNumber;

typedef struct {
	const char* Name;
	int Number;
} ParameterInformation;

// returns information about parameters, array size is NUMBER_OF_PARAMETERS
ParameterInformation* GetParameterInformation();

#endif

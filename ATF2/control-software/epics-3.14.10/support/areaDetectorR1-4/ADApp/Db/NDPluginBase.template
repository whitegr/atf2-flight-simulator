# Database for NDPluginBase, i.e. records common to all plugins
# Mark Rivers
# April 25, 2008

###################################################################
#  This record contains the asyn port name of this plugin         #
###################################################################

record(stringin, "$(P)$(R)PortName_RBV")
{
   field(PINI, "1")
   field(DTYP, "asynOctetRead")
   field(INP,  "@asyn($(PORT),$(ADDR),$(TIMEOUT))PORT_NAME_SELF")
   field(VAL,  "Unknown")
   field(SCAN, "I/O Intr")
}

###################################################################
#  These records control the connection of the server to          #
#  an NDArray driver port and address                             #
###################################################################
# Array port name
record(stringout, "$(P)$(R)NDArrayPort")
{
    field(PINI, "1")
    field(DTYP, "asynOctetWrite")
    field(OUT,  "@asyn($(PORT),$(ADDR),$(TIMEOUT))NDARRAY_PORT")
    field(VAL,  "$(NDARRAY_PORT)")
}

record(stringin, "$(P)$(R)NDArrayPort_RBV")
{
    field(PINI, "1")
    field(DTYP, "asynOctetRead")
    field(INP,  "@asyn($(PORT),$(ADDR),$(TIMEOUT))NDARRAY_PORT")
    field(SCAN, "I/O Intr")
}

record(longout, "$(P)$(R)NDArrayAddress")
{
    field(PINI, "1")
    field(DTYP, "asynInt32")
    field(OUT,  "@asyn($(PORT),$(ADDR),$(TIMEOUT))NDARRAY_ADDR")
    field(VAL,  "$(NDARRAY_ADDR)")
}

record(longin, "$(P)$(R)NDArrayAddress_RBV")
{
    field(PINI, "1")
    field(DTYP, "asynInt32")
    field(INP,  "@asyn($(PORT),$(ADDR),$(TIMEOUT))NDARRAY_ADDR")
    field(SCAN, "I/O Intr")
}

###################################################################
#  These records control whether callbacks are enabled and        #
#  minimum time between callbacks                                 #
###################################################################
record(bo, "$(P)$(R)EnableCallbacks")
{
    field(PINI, "1")
    field(VAL,  "1")
    field(DTYP, "asynInt32")
    field(OUT,  "@asyn($(PORT),$(ADDR),$(TIMEOUT))ENABLE_CALLBACKS")
    field(ZNAM, "No")
    field(ONAM, "Yes")
}

record(bi, "$(P)$(R)EnableCallbacks_RBV")
{
    field(PINI, "1")
    field(DTYP, "asynInt32")
    field(INP,  "@asyn($(PORT),$(ADDR),$(TIMEOUT))ENABLE_CALLBACKS")
    field(ZNAM, "No")
    field(ONAM, "Yes")
    field(SCAN, "I/O Intr")
}

record(ao, "$(P)$(R)MinCallbackTime")
{
    field(PINI, "1")
    field(DTYP, "asynFloat64")
    field(OUT,  "@asyn($(PORT),$(ADDR),$(TIMEOUT))MIN_CALLBACK_TIME")
    field(VAL,  "0.1")
    field(EGU,  "s")
    field(PREC, "3")
}

record(ai, "$(P)$(R)MinCallbackTime_RBV")
{
    field(PINI, "1")
    field(DTYP, "asynFloat64")
    field(INP,  "@asyn($(PORT),$(ADDR),$(TIMEOUT))MIN_CALLBACK_TIME")
    field(EGU,  "s")
    field(PREC, "3")
    field(SCAN, "I/O Intr")
}

###################################################################
#  These records control whether callbacks block or not           #
###################################################################
record(bo, "$(P)$(R)BlockingCallbacks")
{
    field(PINI, "1")
    field(DTYP, "asynInt32")
    field(OUT,  "@asyn($(PORT),$(ADDR),$(TIMEOUT))BLOCKING_CALLBACKS")
    field(VAL,  "0")
    field(ZNAM, "No")
    field(ONAM, "Yes")
}

record(bi, "$(P)$(R)BlockingCallbacks_RBV")
{
    field(PINI, "1")
    field(DTYP, "asynInt32")
    field(INP,  "@asyn($(PORT),$(ADDR),$(TIMEOUT))BLOCKING_CALLBACKS")
    field(ZNAM, "No")
    field(ONAM, "Yes")
    field(SCAN, "I/O Intr")
}


###################################################################
#  These records provide statistics on array callbacks and        #
#  array callback rates                                           # 
###################################################################
record(longout, "$(P)$(R)ArrayCounter")
{
    field(PINI, "1")
    field(DTYP, "asynInt32")
    field(OUT,  "@asyn($(PORT),$(ADDR),$(TIMEOUT))ARRAY_COUNTER")
    field(VAL,  "0")
}

record(longin, "$(P)$(R)ArrayCounter_RBV")
{
    field(PINI, "1")
    field(DTYP, "asynInt32")
    field(INP,  "@asyn($(PORT),$(ADDR),$(TIMEOUT))ARRAY_COUNTER")
    field(SCAN, "I/O Intr")
}

# This record needs work, because B should reflect the time since last processed
record(calc, "$(P)$(R)ArrayRate_RBV")
{
    field(INPA, "$(P)$(R)ArrayRate_RBV.B NPP NMS")  # Previous counter value
    field(INPB, "$(P)$(R)ArrayCounter_RBV NPP NMS") # Current counter value
    field(INPC, "1.0")                              # Delta time, needs work
    field(CALC, "(B-A)/C")
    field(PREC, "1")
    field(SCAN, "1 second")
}

record(longout, "$(P)$(R)DroppedArrays")
{
    field(PINI, "1")
    field(DTYP, "asynInt32")
    field(OUT,  "@asyn($(PORT),$(ADDR),$(TIMEOUT))DROPPED_ARRAYS")
    field(VAL,  "0")
}

record(longin, "$(P)$(R)DroppedArrays_RBV")
{
    field(PINI, "1")
    field(DTYP, "asynInt32")
    field(INP,  "@asyn($(PORT),$(ADDR),$(TIMEOUT))DROPPED_ARRAYS")
    field(SCAN, "I/O Intr")
}

###################################################################
#  These records are the array size and data type                 #
###################################################################
# The number of dimensions
record(longin, "$(P)$(R)NDimensions_RBV")
{
    field(PINI, "1")
    field(DTYP, "asynInt32")
    field(INP,  "@asyn($(PORT),$(ADDR),$(TIMEOUT))ARRAY_NDIMENSIONS")
    field(SCAN, "I/O Intr")
}

# The array dimensions waveform record
record(waveform, "$(P)$(R)Dimensions_RBV")
{
    field(PINI, "1")
    field(DTYP, "asynInt32ArrayIn")
    field(INP,  "@asyn($(PORT),$(ADDR),$(TIMEOUT))ARRAY_DIMENSIONS")
    field(FTVL, "LONG")
    field(NELM, "10")
    field(FLNK, "$(P)$(R)Dim0SA")
    field(SCAN, "I/O Intr")
}

# Note, we only extract the first 2 dimensions here, but this
# can easily be extended up to the maximum of 10 dimensions
record(subArray, "$(P)$(R)Dim0SA")
{
    field(INP,  "$(P)$(R)Dimensions_RBV NPP NMS")
    field(FTVL, "LONG")
    field(MALM, "10")
    field(NELM, "1")
    field(INDX, "0")
    field(FLNK, "$(P)$(R)ArraySize0_RBV")
}

record(longin, "$(P)$(R)ArraySize0_RBV")
{
    field(INP,  "$(P)$(R)Dim0SA")
    field(FLNK, "$(P)$(R)Dim1SA")
}

record(subArray, "$(P)$(R)Dim1SA")
{
    field(INP,  "$(P)$(R)Dimensions_RBV NPP NMS")
    field(FTVL, "LONG")
    field(MALM, "10")
    field(NELM, "1")
    field(INDX, "1")
    field(FLNK, "$(P)$(R)ArraySize1_RBV")
}

record(longin, "$(P)$(R)ArraySize1_RBV")
{
    field(INP,  "$(P)$(R)Dim1SA")
    field(FLNK, "$(P)$(R)Dim2SA")
}

record(subArray, "$(P)$(R)Dim2SA")
{
    field(INP,  "$(P)$(R)Dimensions_RBV NPP NMS")
    field(FTVL, "LONG")
    field(MALM, "10")
    field(NELM, "1")
    field(INDX, "2")
    field(FLNK, "$(P)$(R)ArraySize2_RBV")
}

record(longin, "$(P)$(R)ArraySize2_RBV")
{
    field(INP,  "$(P)$(R)Dim2SA")
#    field(FLNK, "$(P)$(R)Dim3SA")
}

record(mbbi, "$(P)$(R)DataType_RBV")
{
   field(PINI, "1")
   field(DTYP, "asynInt32")
   field(INP,  "@asyn($(PORT),$(ADDR),$(TIMEOUT))DATA_TYPE")
   field(ZRST, "Int8")
   field(ZRVL, "0")
   field(ONST, "UInt8")
   field(ONVL, "1")
   field(TWST, "Int16")
   field(TWVL, "2")
   field(THST, "UInt16")
   field(THVL, "3")
   field(FRST, "Int32")
   field(FRVL, "4")
   field(FVST, "UInt32")
   field(FVVL, "5")
   field(SXST, "Float32")
   field(SXVL, "6")
   field(SVST, "Float64")
   field(SVVL, "7")
   field(SCAN, "I/O Intr")
}

record(mbbi, "$(P)$(R)ColorMode_RBV")
{
   field(PINI, "1")
   field(DTYP, "asynInt32")
   field(INP,  "@asyn($(PORT),$(ADDR),$(TIMEOUT))COLOR_MODE")
   field(ZRST, "Mono")
   field(ZRVL, "0")
   field(ONST, "Bayer")
   field(ONVL, "1")
   field(TWST, "RGB1")
   field(TWVL, "2")
   field(THST, "RGB2")
   field(THVL, "3")
   field(FRST, "RGB3")
   field(FRVL, "4")
   field(FVST, "YUV444")
   field(FVVL, "5")
   field(SXST, "YUV422")
   field(SXVL, "6")
   field(SVST, "YUV421")
   field(SVVL, "7")
   field(SCAN, "I/O Intr")
}

record(mbbi, "$(P)$(R)BayerPattern_RBV")
{
   field(PINI, "1")
   field(DTYP, "asynInt32")
   field(INP,  "@asyn($(PORT),$(ADDR),$(TIMEOUT))BAYER_PATTERN")
   field(ZRST, "RGGB")
   field(ZRVL, "0")
   field(ONST, "GBRG")
   field(ONVL, "1")
   field(TWST, "GRBG")
   field(TWVL, "2")
   field(THST, "BGGR")
   field(THVL, "3")
   field(SCAN, "I/O Intr")
}

###################################################################
#  These records are the uniqueId and time stamp of the array     #
###################################################################
record(longin, "$(P)$(R)UniqueId_RBV")
{
    field(PINI, "1")
    field(DTYP, "asynInt32")
    field(INP,  "@asyn($(PORT),$(ADDR),$(TIMEOUT))UNIQUE_ID")
    field(SCAN, "I/O Intr")
}

record(ai, "$(P)$(R)TimeStamp_RBV")
{
    field(PINI, "1")
    field(DTYP, "asynFloat64")
    field(INP,  "@asyn($(PORT),$(ADDR),$(TIMEOUT))TIME_STAMP")
    field(PREC, "3")
    field(SCAN, "I/O Intr")
}


###################################################################
#  The asynRecord is used for mainly for trace mask               # 
###################################################################
 
# Set ASYN_TRACEIO_HEX bit by default
record(asyn,"$(P)$(R)AsynIO")
{
    field(PORT, $(PORT))
    field(TIB2,"1")
}


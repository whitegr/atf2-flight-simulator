# Database for area detector control using asyn driver and standard asyn device support
# Mark Rivers
# March 9, 2008

###################################################################
#  This record contains the asyn port name                        #
#  about the detector                                             # 
###################################################################

record(stringin, "$(P)$(R)PortName_RBV")
{
   field(VAL,  "$(PORT)")
}

###################################################################
#  These records control basic information                        #
#  about the detector                                             # 
###################################################################

record(stringin, "$(P)$(R)Manufacturer_RBV")
{
   field(PINI, "1")
   field(DTYP, "asynOctetRead")
   field(INP,  "@asyn($(PORT),$(ADDR),$(TIMEOUT))MANUFACTURER")
   field(VAL,  "Unknown")
   field(SCAN, "I/O Intr")
}

record(stringin, "$(P)$(R)Model_RBV")
{
   field(PINI, "1")
   field(DTYP, "asynOctetRead")
   field(INP,  "@asyn($(PORT),$(ADDR),$(TIMEOUT))MODEL")
   field(VAL,  "Unknown")
   field(SCAN, "I/O Intr")
}

record(longin, "$(P)$(R)MaxSizeX_RBV")
{
   field(PINI, "1")
   field(DTYP, "asynInt32")
   field(INP,  "@asyn($(PORT),$(ADDR),$(TIMEOUT))MAX_SIZE_X")
   field(SCAN, "I/O Intr")
}

record(longin, "$(P)$(R)MaxSizeY_RBV")
{
   field(PINI, "1")
   field(DTYP, "asynInt32")
   field(INP,  "@asyn($(PORT),$(ADDR),$(TIMEOUT))MAX_SIZE_Y")
   field(SCAN, "I/O Intr")
}

###################################################################
#  These records control the data type                            # 
###################################################################

record(mbbo, "$(P)$(R)DataType")
{
   field(PINI, "1")
   field(DTYP, "asynInt32")
   field(OUT,  "@asyn($(PORT),$(ADDR),$(TIMEOUT))DATA_TYPE")
   field(ZRST, "Int8")
   field(ZRVL, "0")
   field(ONST, "UInt8")
   field(ONVL, "1")
   field(TWST, "Int16")
   field(TWVL, "2")
   field(THST, "UInt16")
   field(THVL, "3")
   field(FRST, "Int32")
   field(FRVL, "4")
   field(FVST, "UInt32")
   field(FVVL, "5")
   field(SXST, "Float32")
   field(SXVL, "6")
   field(SVST, "Float64")
   field(SVVL, "7")
}

record(mbbi, "$(P)$(R)DataType_RBV")
{
   field(PINI, "1")
   field(DTYP, "asynInt32")
   field(INP,  "@asyn($(PORT),$(ADDR),$(TIMEOUT))DATA_TYPE")
   field(ZRST, "Int8")
   field(ZRVL, "0")
   field(ONST, "UInt8")
   field(ONVL, "1")
   field(TWST, "Int16")
   field(TWVL, "2")
   field(THST, "UInt16")
   field(THVL, "3")
   field(FRST, "Int32")
   field(FRVL, "4")
   field(FVST, "UInt32")
   field(FVVL, "5")
   field(SXST, "Float32")
   field(SXVL, "6")
   field(SVST, "Float64")
   field(SVVL, "7")
   field(SCAN, "I/O Intr")
}

###################################################################
#  These records control the detector readout                     #
#  including binning, region start and size                       # 
###################################################################

record(longout, "$(P)$(R)BinX")
{
   field(PINI, "1")
   field(DTYP, "asynInt32")
   field(OUT,  "@asyn($(PORT),$(ADDR),$(TIMEOUT))BIN_X")
}

record(longin, "$(P)$(R)BinX_RBV")
{
   field(PINI, "1")
   field(DTYP, "asynInt32")
   field(INP,  "@asyn($(PORT),$(ADDR),$(TIMEOUT))BIN_X")
   field(SCAN, "I/O Intr")
}

record(longout, "$(P)$(R)BinY")
{
   field(PINI, "1")
   field(DTYP, "asynInt32")
   field(OUT,  "@asyn($(PORT),$(ADDR),$(TIMEOUT))BIN_Y")
}

record(longin, "$(P)$(R)BinY_RBV")
{
   field(PINI, "1")
   field(DTYP, "asynInt32")
   field(INP,  "@asyn($(PORT),$(ADDR),$(TIMEOUT))BIN_Y")
   field(SCAN, "I/O Intr")
}

record(longout, "$(P)$(R)MinX")
{
   field(PINI, "1")
   field(DTYP, "asynInt32")
   field(OUT,  "@asyn($(PORT),$(ADDR),$(TIMEOUT))MIN_X")
}

record(longin, "$(P)$(R)MinX_RBV")
{
   field(PINI, "1")
   field(DTYP, "asynInt32")
   field(INP,  "@asyn($(PORT),$(ADDR),$(TIMEOUT))MIN_X")
   field(SCAN, "I/O Intr")
}

record(longout, "$(P)$(R)MinY")
{
   field(PINI, "1")
   field(DTYP, "asynInt32")
   field(OUT,  "@asyn($(PORT),$(ADDR),$(TIMEOUT))MIN_Y")
}

record(longin, "$(P)$(R)MinY_RBV")
{
   field(PINI, "1")
   field(DTYP, "asynInt32")
   field(INP,  "@asyn($(PORT),$(ADDR),$(TIMEOUT))MIN_Y")
   field(SCAN, "I/O Intr")
}

record(longout, "$(P)$(R)SizeX")
{
   field(PINI, "1")
   field(DTYP, "asynInt32")
   field(OUT,  "@asyn($(PORT),$(ADDR),$(TIMEOUT))SIZE_X")
}

record(longin, "$(P)$(R)SizeX_RBV")
{
   field(PINI, "1")
   field(DTYP, "asynInt32")
   field(INP,  "@asyn($(PORT),$(ADDR),$(TIMEOUT))SIZE_X")
   field(SCAN, "I/O Intr")
}

record(longout, "$(P)$(R)SizeY")
{
   field(PINI, "1")
   field(DTYP, "asynInt32")
   field(OUT,  "@asyn($(PORT),$(ADDR),$(TIMEOUT))SIZE_Y")
}

record(longin, "$(P)$(R)SizeY_RBV")
{
   field(PINI, "1")
   field(DTYP, "asynInt32")
   field(INP,  "@asyn($(PORT),$(ADDR),$(TIMEOUT))SIZE_Y")
   field(SCAN, "I/O Intr")
}

record(bo, "$(P)$(R)ReverseX")
{
   field(PINI, "1")
   field(DTYP, "asynInt32")
   field(OUT,  "@asyn($(PORT),$(ADDR),$(TIMEOUT))REVERSE_X")
   field(ZNAM, "No")
   field(ONAM, "Yes")
}

record(bi, "$(P)$(R)ReverseX_RBV")
{
   field(PINI, "1")
   field(DTYP, "asynInt32")
   field(INP,  "@asyn($(PORT),$(ADDR),$(TIMEOUT))REVERSE_X")
   field(ZNAM, "No")
   field(ONAM, "Yes")
   field(SCAN, "I/O Intr")
}

record(bo, "$(P)$(R)ReverseY")
{
   field(PINI, "1")
   field(DTYP, "asynInt32")
   field(OUT,  "@asyn($(PORT),$(ADDR),$(TIMEOUT))REVERSE_Y")
   field(ZNAM, "No")
   field(ONAM, "Yes")
}

record(bi, "$(P)$(R)ReverseY_RBV")
{
   field(PINI, "1")
   field(DTYP, "asynInt32")
   field(INP,  "@asyn($(PORT),$(ADDR),$(TIMEOUT))REVERSE_Y")
   field(ZNAM, "No")
   field(ONAM, "Yes")
   field(SCAN, "I/O Intr")
}

record(longin, "$(P)$(R)ImageSizeX_RBV")
{
   field(PINI, "1")
   field(DTYP, "asynInt32")
   field(INP,  "@asyn($(PORT),$(ADDR),$(TIMEOUT))IMAGE_SIZE_X")
   field(SCAN, "I/O Intr")
}

record(longin, "$(P)$(R)ImageSizeY_RBV")
{
   field(PINI, "1")
   field(DTYP, "asynInt32")
   field(INP,  "@asyn($(PORT),$(ADDR),$(TIMEOUT))IMAGE_SIZE_Y")
   field(SCAN, "I/O Intr")
}

record(longin, "$(P)$(R)ImageSize_RBV")
{
   field(PINI, "1")
   field(DTYP, "asynInt32")
   field(INP,  "@asyn($(PORT),$(ADDR),$(TIMEOUT))IMAGE_SIZE")
   field(SCAN, "I/O Intr")
}

###################################################################
#  These records control the acquisition time and                 #
#  period                                                         # 
###################################################################
record(ao, "$(P)$(R)AcquireTime")
{
   field(PINI, "1")
   field(DTYP, "asynFloat64")
   field(OUT,  "@asyn($(PORT),$(ADDR),$(TIMEOUT))ACQ_TIME")
   field(PREC, "3")
}

record(ai, "$(P)$(R)AcquireTime_RBV")
{
   field(PINI, "1")
   field(DTYP, "asynFloat64")
   field(INP,  "@asyn($(PORT),$(ADDR),$(TIMEOUT))ACQ_TIME")
   field(PREC, "3")
   field(SCAN, "I/O Intr")
}

record(ao, "$(P)$(R)AcquirePeriod")
{
   field(PINI, "1")
   field(DTYP, "asynFloat64")
   field(OUT,  "@asyn($(PORT),$(ADDR),$(TIMEOUT))ACQ_PERIOD")
   field(PREC, "3")
}

record(ai, "$(P)$(R)AcquirePeriod_RBV")
{
   field(PINI, "1")
   field(DTYP, "asynFloat64")
   field(INP,  "@asyn($(PORT),$(ADDR),$(TIMEOUT))ACQ_PERIOD")
   field(PREC, "3")
   field(SCAN, "I/O Intr")
}



###################################################################
#  These records control the gain                                 # 
###################################################################
record(ao, "$(P)$(R)Gain")
{
   field(PINI, "1")
   field(DTYP, "asynFloat64")
   field(OUT,  "@asyn($(PORT),$(ADDR),$(TIMEOUT))GAIN")
   field(PREC, "3")
}

record(ai, "$(P)$(R)Gain_RBV")
{
   field(PINI, "1")
   field(DTYP, "asynFloat64")
   field(INP,  "@asyn($(PORT),$(ADDR),$(TIMEOUT))GAIN")
   field(PREC, "3")
   field(SCAN, "I/O Intr")
}

###################################################################
#  These records control the acquisition mode                     # 
###################################################################
record(mbbo, "$(P)$(R)ImageMode")
{
   field(PINI, "1")
   field(DTYP, "asynInt32")
   field(OUT,  "@asyn($(PORT),$(ADDR),$(TIMEOUT))IMAGE_MODE")
   field(ZRST, "Single")
   field(ZRVL, "0")
   field(ONST, "Multiple")
   field(ONVL, "1")
   field(TWST, "Continuous")
   field(TWVL, "2")
}

record(mbbi, "$(P)$(R)ImageMode_RBV")
{
   field(PINI, "1")
   field(DTYP, "asynInt32")
   field(INP,  "@asyn($(PORT),$(ADDR),$(TIMEOUT))IMAGE_MODE")
   field(ZRST, "Single")
   field(ZRVL, "0")
   field(ONST, "Multiple")
   field(ONVL, "1")
   field(TWST, "Continuous")
   field(TWVL, "2")
   field(SCAN, "I/O Intr")
}

###################################################################
#  These records control the trigger mode                         # 
###################################################################
record(mbbo, "$(P)$(R)TriggerMode")
{
   field(PINI, "1")
   field(DTYP, "asynInt32")
   field(OUT,  "@asyn($(PORT),$(ADDR),$(TIMEOUT))TRIGGER_MODE")
   field(ZRST, "Internal")
   field(ZRVL, "0")
   field(ONST, "External")
   field(ONVL, "1")
}

record(mbbi, "$(P)$(R)TriggerMode_RBV")
{
   field(PINI, "1")
   field(DTYP, "asynInt32")
   field(INP,  "@asyn($(PORT),$(ADDR),$(TIMEOUT))TRIGGER_MODE")
   field(ZRST, "Internal")
   field(ZRVL, "0")
   field(ONST, "External")
   field(ONVL, "1")
   field(SCAN, "I/O Intr")
}

###################################################################
#  These records control the number of exposures and              #
#  number of images                                               # 
###################################################################
record(longout, "$(P)$(R)NumExposures")
{
   field(PINI, "1")
   field(DTYP, "asynInt32")
   field(OUT,  "@asyn($(PORT),$(ADDR),$(TIMEOUT))NEXPOSURES")
}

record(longin, "$(P)$(R)NumExposures_RBV")
{
   field(PINI, "1")
   field(DTYP, "asynInt32")
   field(INP,  "@asyn($(PORT),$(ADDR),$(TIMEOUT))NEXPOSURES")
   field(SCAN, "I/O Intr")
}

record(longout, "$(P)$(R)NumImages")
{
   field(PINI, "1")
   field(DTYP, "asynInt32")
   field(OUT,  "@asyn($(PORT),$(ADDR),$(TIMEOUT))NIMAGES")
}

record(longin, "$(P)$(R)NumImages_RBV")
{
   field(PINI, "1")
   field(DTYP, "asynInt32")
   field(INP,  "@asyn($(PORT),$(ADDR),$(TIMEOUT))NIMAGES")
   field(SCAN, "I/O Intr")
}

###################################################################
#  These records control  acquisition start and                   # 
#  and stop                                                       #
###################################################################

record(bo, "$(P)$(R)Acquire") {
   field(ZNAM, "Done")
   field(ONAM, "Acquire")
   field(OUT, "$(P)$(R)DoAcquire PP")
}

record(calcout, "$(P)$(R)StartAcquireCalc") {
   field(INPA, "$(P)$(R)Acquire")
   field(CALC, "A")
   field(OUT, "$(P)$(R)AcquireBusy PP MS")
   field(OOPT, "Every Time")
}

record(busy, "$(P)$(R)AcquireBusy") {
}

record(longout, "$(P)$(R)DoAcquire") {
   field(DTYP, "asynInt32")
   field(OUT, "@asyn($(PORT),$(ADDR),$(TIMEOUT))ACQUIRE")
   field(FLNK, "$(P)$(R)StartAcquireCalc") }

record(longin, "$(P)$(R)Acquire_RBV") {
   field(PINI, "1")
   field(DTYP, "asynInt32")
   field(INP, "@asyn($(PORT),$(ADDR),$(TIMEOUT))ACQUIRE")
   field(SCAN, "I/O Intr")
   field(FLNK, "$(P)$(R)AcquireDoneCalc") }

record(calcout, "$(P)$(R)AcquireDoneCalc") {
   field(INPA, "$(P)$(R)Acquire_RBV")
   field(CALC, "A")
   field(OUT, "$(P)$(R)Acquire CA")
   field(OOPT, "Transition To Zero")
}

###################################################################
#  These records provide statistics on image callbacks and        #
#  image callback rates                                           # 
###################################################################
record(longout, "$(P)$(R)ImageCounter")
{
    field(DTYP, "asynInt32")
    field(OUT,  "@asyn($(PORT),$(ADDR),$(TIMEOUT))IMAGE_COUNTER")
}

record(longin, "$(P)$(R)ImageCounter_RBV")
{
   field(PINI, "1")
    field(DTYP, "asynInt32")
    field(INP,  "@asyn($(PORT),$(ADDR),$(TIMEOUT))IMAGE_COUNTER")
    field(SCAN, "I/O Intr")
}

# This record needs work, because .B should reflect the time since last processed
record(calc, "$(P)$(R)ImageRate_RBV")
{
    field(INPA, "$(P)$(R)ImageRate_RBV.B NPP NMS")  # Previous counter value
    field(INPB, "$(P)$(R)ImageCounter_RBV NPP NMS") # Current counter value
    field(INPC, "1.0")                              # Delta time, needs work
    field(CALC, "(B-A)/C")
    field(PREC, "1")
    field(SCAN, "1 second")
}

###################################################################
#  These records are for the detector state                       # 
###################################################################

record(mbbi, "$(P)$(R)DetectorState_RBV")
{
    field(PINI, "1")
    field(DTYP, "asynInt32")
    field(INP,  "@asyn($(PORT),$(ADDR),$(TIMEOUT))STATUS")
    field(ZRST, "Idle")
    field(ZRVL, "0")
    field(ZRSV, "NO_ALARM")
    field(ONST, "Acquire")
    field(ONVL, "1")
    field(ONSV, "MINOR")
    field(TWST, "Readout")
    field(TWVL, "2")
    field(TWSV, "MINOR")
    field(THST, "Correct")
    field(THVL, "3")
    field(THSV, "MINOR")
    field(FRST, "Saving")
    field(FRVL, "4")
    field(FRSV, "MINOR")
    field(FVST, "Aborting")
    field(FVVL, "5")
    field(FVSV, "MINOR")
    field(SXST, "Error")
    field(SXVL, "6")
    field(SXSV, "MAJOR")
    field(SCAN, "I/O Intr")
}


###################################################################
#  These records control file I/O                                 # 
###################################################################

# File path.
record(waveform, "$(P)$(R)FilePath")
{
    field(PINI, "1")
    field(DTYP, "asynOctetWrite")
    field(INP,  "@asyn($(PORT),$(ADDR),$(TIMEOUT))FILE_PATH")
    field(FTVL, "UCHAR")
    field(NELM, "256")
}

record(waveform, "$(P)$(R)FilePath_RBV")
{
    field(PINI, "1")
    field(DTYP, "asynOctetRead")
    field(INP,  "@asyn($(PORT),$(ADDR),$(TIMEOUT))FILE_PATH")
    field(FTVL, "UCHAR")
    field(NELM, "256")
    field(SCAN, "I/O Intr")
}

# Filename
record(waveform, "$(P)$(R)FileName")
{
    field(PINI, "1")
    field(DTYP, "asynOctetWrite")
    field(INP,  "@asyn($(PORT),$(ADDR),$(TIMEOUT))FILE_NAME")
    field(FTVL, "UCHAR")
    field(NELM, "256")
}

record(waveform, "$(P)$(R)FileName_RBV")
{
    field(PINI, "1")
    field(DTYP, "asynOctetRead")
    field(INP,  "@asyn($(PORT),$(ADDR),$(TIMEOUT))FILE_NAME")
    field(FTVL, "UCHAR")
    field(NELM, "256")
    field(SCAN, "I/O Intr")
}

# File number
record(longout, "$(P)$(R)FileNumber")
{
    field(PINI, "1")
    field(VAL,  "1")
    field(DTYP, "asynInt32")
    field(OUT,  "@asyn($(PORT),$(ADDR),$(TIMEOUT))FILE_NUMBER")
}

record(longin, "$(P)$(R)FileNumber_RBV")
{
    field(PINI, "1")
    field(DTYP, "asynInt32")
    field(INP,  "@asyn($(PORT),$(ADDR),$(TIMEOUT))FILE_NUMBER")
    field(SCAN, "I/O Intr")
}

record(calcout, "$(P)$(R)FileNumber_Sync")
{
    field(INPA, "$(P)$(R)FileNumber_RBV CP")
    field(CALC, "A")
    field(OUT,  "$(P)$(R)FileNumber PP")
}

# Autoincrement flag
record(bo, "$(P)$(R)AutoIncrement")
{
    field(PINI,  "1")
    field(DTYP, "asynInt32")
    field(OUT,  "@asyn($(PORT),$(ADDR),$(TIMEOUT))AUTO_INCREMENT")
    field(ZNAM, "No")
    field(ONAM, "Yes")
}

record(bi, "$(P)$(R)AutoIncrement_RBV")
{
    field(PINI, "1")
    field(DTYP, "asynInt32")
    field(INP,  "@asyn($(PORT),$(ADDR),$(TIMEOUT))AUTO_INCREMENT")
    field(ZNAM, "No")
    field(ONAM, "Yes")
    field(SCAN, "I/O Intr")
}

# File template
record(waveform, "$(P)$(R)FileTemplate")
{
    field(PINI,  "1")
    field(DTYP, "asynOctetWrite")
    field(INP,   "@asyn($(PORT),$(ADDR),$(TIMEOUT))FILE_TEMPLATE")
    field(FTVL, "UCHAR")
    field(NELM, "256")
}

record(waveform, "$(P)$(R)FileTemplate_RBV")
{
    field(PINI, "1")
    field(DTYP, "asynOctetRead")
    field(INP,  "@asyn($(PORT),$(ADDR),$(TIMEOUT))FILE_TEMPLATE")
    field(FTVL, "UCHAR")
    field(NELM, "256")
    field(SCAN, "I/O Intr")
}

# Full filename, including path
record(waveform, "$(P)$(R)FullFileName_RBV")
{
    field(PINI, "1")
    field(DTYP, "asynOctetRead")
    field(INP,  "@asyn($(PORT),$(ADDR),$(TIMEOUT))FULL_FILE_NAME")
    field(FTVL, "UCHAR")
    field(NELM, "256")
    field(SCAN, "I/O Intr")
}

# Autosave flag
record(bo, "$(P)$(R)AutoSave")
{
    field(PINI,  "1")
    field(DTYP, "asynInt32")
    field(OUT,  "@asyn($(PORT),$(ADDR),$(TIMEOUT))AUTO_SAVE")
    field(ZNAM, "No")
    field(ONAM, "Yes")
}

record(bi, "$(P)$(R)AutoSave_RBV")
{
    field(PINI, "1")
    field(DTYP, "asynInt32")
    field(INP,  "@asyn($(PORT),$(ADDR),$(TIMEOUT))AUTO_SAVE")
    field(ZNAM, "No")
    field(ONAM, "Yes")
    field(SCAN, "I/O Intr")
}

# NOTE: These should be busy records, but wait till we get asynBusy record
# Write file
record(longout, "$(P)$(R)WriteFile")
{
    field(DTYP, "asynInt32")
    field(OUT,  "@asyn($(PORT),$(ADDR),$(TIMEOUT))WRITE_FILE")
}

# Read file
record(longout, "$(P)$(R)ReadFile")
{
    field(DTYP, "asynInt32")
    field(OUT,  "@asyn($(PORT),$(ADDR),$(TIMEOUT))READ_FILE")
}

# File data format 
record(mbbo, "$(P)$(R)FileFormat")
{
    field(PINI, "1")
    field(DTYP, "asynInt32")
    field(OUT,  "@asyn($(PORT),$(ADDR),$(TIMEOUT))FILE_FORMAT")
    field(ZRST, "Default")
    field(ZRVL, "0")
    field(ONST, "Invalid")
    field(ONVL, "1")
}

record(mbbi, "$(P)$(R)FileFormat_RBV")
{
    field(PINI, "1")
    field(DTYP, "asynInt32")
    field(INP,  "@asyn($(PORT),$(ADDR),$(TIMEOUT))FILE_FORMAT")
    field(ZRST, "Default")
    field(ZRVL, "0")
    field(ONST, "Invalid")
    field(ONVL, "1")
    field(SCAN, "I/O Intr")
}

###################################################################
#  These records provide status information                       # 
###################################################################

# Status message.
record(waveform, "$(P)$(R)StatusMessage_RBV")
{
    field(DTYP, "asynOctetRead")
    field(INP,  "@asyn($(PORT),$(ADDR),$(TIMEOUT))STATUS_MESSAGE")
    field(FTVL, "UCHAR")
    field(NELM, "256")
    field(SCAN, "I/O Intr")
}

record(waveform, "$(P)$(R)StringToServer_RBV")
{
    field(DTYP, "asynOctetRead")
    field(INP,  "@asyn($(PORT),$(ADDR),$(TIMEOUT))STRING_TO_SERVER")
    field(FTVL, "UCHAR")
    field(NELM, "256")
    field(SCAN, "I/O Intr")
}

record(waveform, "$(P)$(R)StringFromServer_RBV")
{
    field(DTYP, "asynOctetRead")
    field(INP,  "@asyn($(PORT),$(ADDR),$(TIMEOUT))STRING_FROM_SERVER")
    field(FTVL, "UCHAR")
    field(NELM, "256")
    field(SCAN, "I/O Intr")
}

###################################################################
#  The asynRecord is used for mainly for trace mask               # 
###################################################################
 
# Set ASYN_TRACEIO_HEX bit by default
record(asyn,"$(P)$(R)AsynIO")
{
    field(PORT, $(PORT))
    field(TIB2,"1")
}

###################################################################
#  Records past here need work!!!!                                # 
###################################################################

# Needs works past here
record(mbbo, "$(P)$(R)Compression")
{
   field(ZRST,  "None")
   field(ONST,  "LZW")
   field(TWST, "Skip-Huff")
   field(THST, "RL Encoding")
}

record(bi, "$(P)$(R)CorrectBackground")
{
   field(VAL, "0")
   field(ZNAM, "No")
   field(ONAM, "Yes")
}

record(bi, "$(P)$(R)CorrectFlatfield")
{
   field(VAL, "0")
   field(ZNAM, "No")
   field(ONAM, "Yes")
}

record(bi, "$(P)$(R)CorrectSpatial")
{
   field(VAL, "0")
   field(ZNAM, "No")
   field(ONAM, "Yes")
}

record(mbbo, "$(P)$(R)ImageType")
{
   field(VAL, "0")
   field(ZRST, "Normal")
   field(ONST, "Dbl correlation")
   field(TWST, "Background")
   field(THST, "Flatfield")
}

record(ao, "$(P)$(R)TimeRemaining")
{
   field(VAL, "0")
   field(PREC, "1")
}

record(stringout, "$(P)$(R)OpenShutterStr")
{
   field(VAL, "1")
}

record(bo, "$(P)$(R)OpenShutter")
{
}

record(stringout, "$(P)$(R)CloseShutterStr")
{
   field(VAL, "0")
}

record(bo, "$(P)$(R)CloseShutter")
{
}

record(ao, "$(P)$(R)OpenShutterDly")
{
   field(VAL, ".05")
   field(PREC, "3")
}

record(ao, "$(P)$(R)CloseShutterDly")
{
   field(VAL, ".05")
   field(PREC, "3")
}

record(mbbo, "$(P)$(R)ShutterMode")
{
   field(VAL, "2")
   field(ZRST, "None")
   field(ONST, "Camera output")
   field(TWST, "EPICS PV")
}

record(bo, "$(P)$(R)Shutter")
{
   field(PINI, "YES")
   field(VAL, "0")
   field(ZNAM, "Closed")
   field(ZSV, "MAJOR")
   field(ONAM, "Open")
   field(OSV, "NO_ALARM")
}

record(stringin, "$(P)$(R)ShutterStatus")
{
   field(PINI, "YES")
   field(INP, "$(P)$(R)Shutter CP MS")
}

record(stringout, "$(P)$(R)DetInStr")
{
}

record(stringout, "$(P)$(R)DetOutStr")
{
}

record(stringin, "$(P)$(R)Comment1")
{
   field(DESC, "Comment 1")
}

record(stringin, "$(P)$(R)Comment2")
{
   field(DESC, "Comment 2")
}

record(stringin, "$(P)$(R)Comment3")
{
   field(DESC, "Comment 3")
}

record(stringin, "$(P)$(R)Comment4")
{
   field(DESC, "Comment 4")
}

record(stringin, "$(P)$(R)Comment5")
{
   field(DESC, "Comment 5")
}

record(stringin, "$(P)$(R)Comment6")
{
   field(DESC, "Comment 6")
}


In current version of office on my laptop
Use File/Save As/ to save as a PNG file.

In older version of office:

To make the PNG file from the PPT file, select the entire page (^A), right click, Save as Picture, select PNG.

Saving the slide as PNG with Save As works, but it is lower resolution so the text is ugly.

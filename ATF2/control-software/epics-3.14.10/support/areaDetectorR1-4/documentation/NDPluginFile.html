a<html xmlns="http://www.w3.org/1999/xhtml">
<head>
  <title>areaDetector Plugin NDPluginFile</title>
</head>
<body>
  <div style="text-align: center">
    <h1>
      areaDetector Plugin NDPluginFile</h1>
    <h2>
      January 30, 2009</h2>
    <h2>
      Mark Rivers</h2>
    <h2>
      University of Chicago</h2>
  </div>
  <h2>
    Contents</h2>
  <ul>
    <li><a href="#Overview">Overview</a></li>
    <li><a href="#Configuration">Configuration</a></li>
    <li><a href="#Screens">Screen shots</a></li>
    <li><a href="#netCDF">netCDF file contents</a></li>
    <li><a href="#IDL">IDL file reading function</a></li>
    <li><a href="#Future">Future plans</a></li>
  </ul>
  <h2 id="Overview">
    Overview
  </h2>
  <p>
    NDPluginFile saves the NDArray data from a callback to a disk file.
  </p>
  <p>
    NDPluginFile inherits from NDPluginDriver. This plugin currently saves data in the
    <a href="http://www.unidata.ucar.edu/software/netcdf">netCDF</a> file format, which
    is a portable self-describing binary file format supported by <a href="http://www.unidata.ucar.edu/">
      UniData</a> at <a href="http://www.ucar.edu/">UCAR (University Corporation for Atmospheric
        Research).</a> There are netCDF libraries for C, C++, Fortran, and Java. Other
    languages, including Matlab and IDL have built-in support for netCDF. There are
    also add-on interfaces available for Python, Ruby and other languages.
  </p>
  <p>
    The NDArray callback data can be written to disk in 1 of 3 modes:
  </p>
  <ol>
    <li>Single mode. In this mode each NDArray callback results in a separate disk file.</li>
    <li>Capture mode. In this mode a memory buffer is allocated before saving begins.
      Callback arrays are placed into this buffer, and when capture stops the file is
      written to disk. This mode limits the number of frames that can be saved, because
      they all must fit in a memory buffer. It is the fastest mode, with the least probability
      of dropping arrays, because no disk I/O is required while capture is in progress.</li>
    <li>Stream mode. In this mode the data are written to a single disk file, with each
      frame being appended to the file without closing it. It is intermediate in speed
      between single mode and capture mode, but unlike capture mode it is not limited
      by available memory in the number of arrays that can be saved.</li>
  </ol>
  <p>
    Note that the colorMode and bayerFormat must not be changed while file capture or
    file streaming are in progress because that would change the structure of the data
    array.</p>
  <p>
    The NDPluginFile public interface is defined in NDPluginFile.h as follows:
  </p>
  <pre>/* Note that the file format enum must agree with the mbbo/mbbi records in the NDFile.template file */
typedef enum {
    NDFileFormatNetCDF
} NDPluginFileFormat_t;

...
class NDPluginFile : public NDPluginDriver {
public:
    NDPluginFile(const char *portName, int queueSize, int blockingCallbacks, 
                 const char *NDArrayPort, int NDArrayAddr);
                 
    /* These methods override those in the base class */
    void processCallbacks(NDArray *pArray);
    asynStatus writeInt32(asynUser *pasynUser, epicsInt32 value);
    asynStatus writeNDArray(asynUser *pasynUser, void *genericPointer);
    asynStatus drvUserCreate(asynUser *pasynUser, const char *drvInfo, 
                             const char **pptypeName, size_t *psize);

...
}
</pre>
  <p>
    NDPluginFile supports all of the file saving parameters defined in <a href="areaDetectorDoc.html#ADStdDriverParams">
      ADStdDriverParams.h</a>, e.g. ADFilePath, ADFileName, etc. Thus, the same interface
    that is used for saving files directly in a driver is used for this plugin.
  </p>
  <h2 id="Configuration">
    Configuration</h2>
  <p>
    The NDPluginFile plugin is created with the following command, either from C/C++
    or from the EPICS IOC shell.
  </p>
  <pre>drvNDFileConfigure(const char *portName, int queueSize, int blockingCallbacks, 
                   const char *NDArrayPort, int NDArrayAddr)
  </pre>
  <table border="1" cellpadding="2" cellspacing="2" style="text-align: left">
    <tr>
      <th>
        Argument</th>
      <th>
        Description</th>
    </tr>
    <tr>
      <td>
        <code>portName</code></td>
      <td>
        The name of the asyn port for this plugin.
      </td>
    </tr>
    <tr>
      <td>
        <code>queueSize</code></td>
      <td>
        The maximum number of NDArray objects that can be queued for processing. Passed
        to the NDPluginDriver base class constructor.
      </td>
    </tr>
    <tr>
      <td>
        <code>blockingCallbacks</code></td>
      <td>
        Flag controlling whether callbacks block. Passed to the NDPluginDriver base class
        constructor.
      </td>
    </tr>
    <tr>
      <td>
        <code>NDArrayPort</code></td>
      <td>
        The name of the asyn port of the driver that will provide the NDArray data. Passed
        to the NDPluginDriver base class constructor.
      </td>
    </tr>
    <tr>
      <td>
        <code>NDArrayAddr</code></td>
      <td>
        The asyn addr of the asyn port of the driver that will provide the NDArray data.
        Passed to the NDPluginDriver base class constructor.
      </td>
    </tr>
  </table>
  <h2 id="Screens">
    Screen shots</h2>
  <p>
    The following is the MEDM screen that provides access to the parameters in NDPluginDriver.h
    and NDPluginFile.h through records in NDPluginBase.template and NDFile.template.
    This is the MEDM screen that is used to control the saving of images to disk.
  </p>
  <div style="text-align: center">
    <h3>
      NDPluginFile.adl</h3>
    <p>
      <img alt="NDPluginFile.png" src="NDPluginFile.png" /></p>
  </div>
  <h2 id="netCDF">
    netCDF file contents</h2>
  <p>
    The following is the header contents of a netCDF file produced by this plugin. This
    information was produced with the following command:</p>
  <pre>ncdump -h testB_3.nc

netcdf testB_3 {
dimensions:
        numArrays = UNLIMITED ; // (10 currently)
        dim0 = 1024 ;
        dim1 = 1360 ;
        dim2 = 3 ;
variables:
        int uniqueId(numArrays) ;
        double timeStamp(numArrays) ;
        byte array_data(numArrays, dim0, dim1, dim2) ;

// global attributes:
                :dataType = 1 ;
                :numArrayDims = 3 ;
                :dimSize = 3, 1360, 1024 ;
                :dimOffset = 0, 0, 0 ;
                :dimBinning = 1, 1, 1 ;
                :dimReverse = 0, 0, 0 ;
                :colorMode = "RGB1" ;
                :bayerPattern = "RGGB" ;
}
  </pre>
  <p>
    This is an explanation of this output:
  </p>
  <ul>
    <li>dimensions: numArrays is the number of arrays in the file. It will be 1 for files
      collected in Single mode, and is normally &gt 1 for files collected in Capture or
      Stream mode. For each array dim0 is the slowest varying dimension, dim1 the next
      slowest, etc.</li>
    <li>variables: There are 3 variables in the netCDF file. uniqueId is the unique ID
      number of each array. timeStamp is the timestamp in seconds for each array. array_data
      is the array data. Its data type depends on the data type of the NDArray data passed
      in the callbacks. It dimensions are [numArrays, dim0, dim1, ...dimN]. This notation
      is in the Fortran syntax where the slowest varying dimension comes first in the
      list.</li>
    <li>global attributes. dataType is the NDDataType_t enum value for the array data
      type. numArrayDims is the number of dimensions in each array. array_data has 1 more
      dimension than this, numArrays, because it contains all of the array callback data.
      dimSize is an array[numArrayDims] containing the size of each dimension, with the
      fastest varying dimension first. dimOffset, dimBinning, and dimReverse are the values
      of the offset, binning and reverse fields in the NDDimension_t structure for each
      dimension. colorMode is the color mode of the data. It is a string rather than the
      NDColorMode_T value, so that it is easy to understand and independent of changes
      to the enum definition. bayerPattern is the Bayer pattern of the data, also as a
      string rather than an enum. </li>
  </ul>
  <h2 id="IDL">
    IDL file reading function</h2>
  <p>
    There is an IDL function, <a href="http://cars.uchicago.edu/software/idl/detector_routines.html#read_nd_netcdf">
      read_nd_netcdf</a> that can be used to read the netCDF files created by this plugin.
    This routine is contained in the <a href="http://cars.uchicago.edu/software/idl/detectors.html#read_nd_netcdf">
      CARS IDL detector package</a>. This function is also contained in the areaDetector
    distribution in the Viewers/IDL directory.
  </p>
  <h2 id="Future">
    Future plans</h2>
  <p>
    Additional file formats, such as TIFF and HDF may be supported in the future.
  </p>
</body>
</html>

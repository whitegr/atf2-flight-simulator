<html xmlns="http://www.w3.org/1999/xhtml">
<head>
  <title>areaDetector Simulation driver</title>
</head>
<body>
  <div style="text-align: center">
    <h1 style="text-align: center">
      areaDetector Simulation driver</h1>
    <h2>
      January 30, 2009</h2>
    <h2>
      Mark Rivers</h2>
    <h2>
      University of Chicago</h2>
  </div>
  <p>
    &nbsp;</p>
  <h2>
    Table of Contents</h2>
  <ul>
    <li><a href="#Introduction">Introduction</a></li>
    <li><a href="#Driver_parameters">Simulation driver specific parameters</a></li>
    <li><a href="#Unsupported">Unsupported standard driver parameters</a></li>
    <li><a href="#Screenshots">Screenshots</a></li>
    <li><a href="#Configuration">Configuration</a></li>
  </ul>
  <h2 id="Introduction">
    Introduction</h2>
  <p>
    simDetector is a driver for a simulated area detector. It inherits from ADDriver.
    The simulation detector implements nearly all of the parameters defined in ADStdDriverParams.h,
    with the exception of the file saving parameters, which it does not implement. It
    also implements a few parameters that are specific to the simulation detector. The
    simulation detector is useful as a model for writing real detector drivers. It is
    also very useful for testing plugins and channel access clients. This is part of
    the definition of the simDetector class:
  </p>
  <pre>class simDetector : public ADDriver {
public:
    simDetector(const char *portName, int maxSizeX, int maxSizeY, NDDataType_t dataType,
                int maxBuffers, size_t maxMemory);
                 
    /* These are the methods that we override from ADDriver */
    virtual asynStatus writeInt32(asynUser *pasynUser, epicsInt32 value);
    virtual asynStatus writeFloat64(asynUser *pasynUser, epicsFloat64 value);
    virtual asynStatus drvUserCreate(asynUser *pasynUser, const char *drvInfo, 
                                     const char **pptypeName, size_t *psize);
    void report(FILE *fp, int details);
</pre>
  <p>
    In the constructor <code>simDetector</code> the portName, maxBuffers, and maxMemory
    arguments are passed to the ADDriver base class constructor. The maxSizeX, maxSizeY,
    and dataType arguments are specific to the simulation driver, controlling the maximum
    image size and initial data type of the computed images. The writeInt32 and writeFloat64
    methods override those in the base class. The driver takes action when new parameters
    are passed via those interfaces. For example, the ADAcquire parameter (on the asynInt32
    interface) is used to turn acquisition (i.e. computing new images) on and off.
  </p>
  <p>
    The simulation driver initially sets the image[i, j] = i*gainX + j*gainY * gain
    * exposureTime * 1000. Thus the image is a linear ramp in the X and Y directions,
    with the gains in each direction being detector-specific parameters. Each subsquent
    acquisition increments each pixel value by gain*exposureTime*1000. Thus if gain=1
    and exposureTime=.001 second then the pixels are incremented by 1. If the array
    is an unsigned 8 or 16 bit integer then the pixels will overflow and wrap around
    to 0 after some period of time. This gives the appearance of bands that appear to
    move with time. The slope of the bands and their periodicity can be adjusted by
    changing the gains and exposure times.
  </p>
  <p>
    The driver creates a thread that waits for a signal to start acquisition. When acquisition
    is started that thread computes new images and then calls back any registered plugins
    as follows:
  </p>
  <pre>        /* Put the frame number and time stamp into the buffer */
        pImage->uniqueId = imageCounter;
        pImage->timeStamp = startTime.secPastEpoch + startTime.nsec / 1.e9;
        
        /* Call the NDArray callback */
        /* Must release the lock here, or we can get into a deadlock, because we can
         * block on the plugin lock, and the plugin can be calling us */
        epicsMutexUnlock(this->mutexId);
        asynPrint(this->pasynUser, ASYN_TRACE_FLOW, 
             "%s:%s: calling imageData callback\n", driverName, functionName);
        doCallbacksGenericPointer(pImage, NDArrayData, addr);
        epicsMutexLock(this->mutexId);
</pre>
  <h2 id="Driver_parameters">
    Simulation driver specific parameters</h2>
  <p>
    The simulation driver-specific parameters are the following:
  </p>
  <table border="1" cellpadding="2" cellspacing="2" style="text-align: left">
    <tbody>
      <tr>
        <td align="center" colspan="7">
          <b>Parameter Definitions in simDetector.cpp and EPICS Record Definitions in simDetector.template</b></td>
      </tr>
      <tr>
        <th>
          Enum name</th>
        <th>
          asyn interface</th>
        <th>
          Access</th>
        <th>
          Description</th>
        <th>
          drvUser string</th>
        <th>
          EPICS record name</th>
        <th>
          EPICS record type</th>
      </tr>
      <tr>
        <td>
          SimGainX</td>
        <td>
          asynFloat64</td>
        <td>
          r/w</td>
        <td>
          Gain in the X direction</td>
        <td>
          SIM_GAINX</td>
        <td>
          $(P)$(R)GainX<br />
          $(P)$(R)GainX_RBV</td>
        <td>
          ao<br />
          ai</td>
      </tr>
      <tr>
        <td>
          SimGainY</td>
        <td>
          asynFloat64</td>
        <td>
          r/w</td>
        <td>
          Gain in the Y direction</td>
        <td>
          SIM_GAINY</td>
        <td>
          $(P)$(R)GainY<br />
          $(P)$(R)GainY_RBV</td>
        <td>
          ao<br />
          ai</td>
      </tr>
      <tr>
        <td>
          SimResetImage</td>
        <td>
          asynInt32</td>
        <td>
          r/w</td>
        <td>
          Reset image back to initial conditions when 1.</td>
        <td>
          RESET_IMAGE</td>
        <td>
          $(P)$(R)Reset<br />
          $(P)$(R)Reset_RBV</td>
        <td>
          longout<br />
          longin</td>
      </tr>
    </tbody>
  </table>
  <h2 id="Unsupported">
    Unsupported standard driver parameters</h2>
  <ul>
    <li>Collect: Number of exposures per image (ADNumExposures)</li>
    <li>Collect: Trigger mode (ADTriggerMode)</li>
    <li>File control: No file I/O is supported</li>
  </ul>
  <h2 id="Screenshots">
    Screenshots</h2>
  <p>
    The following is the MEDM screen ADBase.adl connected to a simulation detector.
  </p>
  <div style="text-align: center">
    <h3>
      ADBase.adl</h3>
    <img alt="ADBase_sim.png" src="ADBase_sim.png" />
  </div>
  <p>
    The following is the MEDM screen that provides access to the specific parameters
    for the simulation detector.
  </p>
  <div style="text-align: center">
    <h3>
      simDetector.adl</h3>
    <img alt="simDetector.png" src="simDetector.png" />
  </div>
  <p>
    The following is an IDL <a href="http://cars.uchicago.edu/software/idl/imaging_routines.html#epics_ad_display">
      epics_ad_display</a> screen using <a href="http://cars.uchicago.edu/software/idl/imaging_routines.html#image_display">
        image_display</a> to display the simulation detector images.
  </p>
  <div style="text-align: center">
    <h3>
      epics_ad_display.pro</h3>
    <img alt="simDetector_image_display.png" src="simDetector_image_display.png" />
  </div>
  <h2 id="Configuration">
    Configuration</h2>
  <p>
    This driver is configured via the <tt>simDetectorConfig()</tt> function. If this
    is to be used in an IOC, it must be called before <tt>iocInit()</tt>. It has the
    following syntax:
  </p>
  <pre>simDetectorConfig(const char *portName, int maxSizeX, int maxSizeY, 
                  int dataType, int maxBuffers, size_t maxMemory)
  </pre>
  <table border="1" cellpadding="2" cellspacing="2" style="text-align: left">
    <tbody>
      <tr>
        <th>
          Argument</th>
        <th>
          Description</th>
      </tr>
      <tr>
        <td>
          <code>portName</code></td>
        <td>
          The name of the asyn port for this detector.
        </td>
      </tr>
      <tr>
        <td>
          <code>maxSizeX</code></td>
        <td>
          Maximum number of pixels in the X direction for the simulated detector.
        </td>
      </tr>
      <tr>
        <td>
          <code>maxSizeY</code></td>
        <td>
          Maximum number of pixels in the Y direction for the simulated detector.
        </td>
      </tr>
      <tr>
        <td>
          <code>dataType</code></td>
        <td>
          Initial data type of the detector data. These are the enum values for NDDataType_t,
          i.e.
          <ul>
            <li>0=NDInt8</li>
            <li>1=NDUInt8</li>
            <li>2=NDInt16</li>
            <li>3=NDUInt16</li>
            <li>4=NDInt32</li>
            <li>5=NDUInt32</li>
            <li>6=NDFloat32</li>
            <li>7=NDFloat64</li>
          </ul>
        </td>
      </tr>
      <tr>
        <td>
          <code>maxBuffers</code></td>
        <td>
          Maxiumum number of NDArray objects (image buffers) this driver is allowed to allocate.
          The driver itself requires 2 buffers, and each queue element in a plugin can require
          one buffer. So, for example, if 3 plugins are connected to this driver, and each
          has a queue size of 10, then maxBuffers should be at least 32.
        </td>
      </tr>
      <tr>
        <td>
          <code>maxMemory</code></td>
        <td>
          Maxiumum number of bytes of memory for all NDArray objects (image buffers) allocated
          by this driver. If maxSizeX=maxSizeY=1024, and maxBuffers=32, then maxMemory should
          be at least 33554432 (32MB).
        </td>
      </tr>
    </tbody>
  </table>
  <p>
    If being used in an IOC, and an EPICS PV interface with the driver is desired, the
    <tt>ADBase.template</tt> and <tt>simDetector.template</tt> databases should also
    be loaded for the driver instance.
  </p>
  <p>
    The areaDetector software comes with an example IOC for the simulation driver, <tt>
      iocBoot/iocSimDetector</tt>.
  </p>
</body>
</html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
  <title>areaDetector Plugin NDPluginColorConvert</title>
</head>
<body>
  <div style="text-align: center">
    <h1>
      areaDetector Plugin NDPluginColorConvert</h1>
    <h2>
      January 30, 2009</h2>
    <h2>
      Mark Rivers</h2>
    <h2>
      University of Chicago</h2>
  </div>
  <h2>
    Contents</h2>
  <ul>
    <li><a href="#Overview">Overview</a></li>
    <li><a href="#Configuration">Configuration</a></li>
    <li><a href="#Screens">Screen shots</a></li>
    <li><a href="#Restrictions">Restrictions</a></li>
  </ul>
  <h2 id="Overview">
    Overview
  </h2>
  <p>
    This plugin is a tool for converting the color mode of NDArray data.
  </p>
  <p>
    NDPluginColorConvert inherits from NDPluginDriver. NDPluginColorConvert receives
    an input NDArray with one color mode and outputs another NDArray with a (potentially)
    different color mode. All other attributes of the array are preserved. The NDPluginColorConvert
    public interface is defined in NDPluginColorConvert.h as follows:</p>
  <pre>class NDPluginColorConvert : public NDPluginDriver {
public:
    NDPluginColorConvert(const char *portName, int queueSize, int blockingCallbacks,
                         const char *NDArrayPort, int NDArrayAddr,
                         size_t maxMemory);

    /* These methods override the virtual methods in the base class */
    void processCallbacks(NDArray *pArray);
    asynStatus drvUserCreate(asynUser *pasynUser, const char *drvInfo,
                             const char **pptypeName, size_t *psize);

    /* These methods are just for this class */
    template &lt typename epicstype &gt void convertColor(NDArray *pArray);
};
...
}
</pre>
  <p>
    NDPluginColorConvert defines the following parameters. It also implements all of
    the standard plugin parameters from <a href="pluginDoc.html#NDPluginDriver">NDPluginDriver</a>
    . The EPICS database NDColorConvert.template provides access to these parameters,
    listed in the following table.
  </p>
  <table border="1" cellpadding="2" cellspacing="2" style="text-align: left">
    <tbody>
      <tr>
        <td align="center" colspan="7,">
          <b>Parameter Definitions in NDPluginColorConvert.h and EPICS Record Definitions in
            NDColorConvert.template</b></td>
      </tr>
      <tr>
        <th>
          Enum name</th>
        <th>
          asyn interface</th>
        <th>
          Access</th>
        <th>
          Description</th>
        <th>
          drvUser string</th>
        <th>
          EPICS record name</th>
        <th>
          EPICS record type</th>
      </tr>
      <tr>
        <td>
          NDPluginColorConvertColorModeOut</td>
        <td>
          asynInt32</td>
        <td>
          r/w</td>
        <td>
          The output color mode (NDColorMode_t).</td>
        <td>
          COLOR_MODE_OUT</td>
        <td>
          $(P)$(R)ColorModeOut
          <br />
          $(P)$(R)ColorModeOut_RBV
        </td>
        <td>
          mbbo
          <br />
          mbbi</td>
      </tr>
    </tbody>
  </table>
  <p>
    NDPluginColorConvert currently supports the following conversions:</p>
  <ul>
    <li>Bayer to RGB1, RGB2, or RGB3 </li>
    <li>RGB1 to RGB2 or RGB3</li>
    <li>RGB2 to RGB1 or RGB3</li>
    <li>RGB3 to RGB1 or RGB2</li>
  </ul>
  <p>
    The Bayer color conversion supports the 4 Bayer formats (NDBayerRGGB, NDBayerGBRG,
    NDBayerGRBG, NDBayerBGGR) defined in NDArray.h. If the input color mode and output
    color mode are not one of these supported conversion combinations then the output
    array is simply a copy of the input array and no conversion is performed.</p>
  <h2 id="Configuration">
    Configuration</h2>
  <p>
    The NDPluginColorConvert plugin is created with the following command, either from
    C/C++ or from the EPICS IOC shell.
  </p>
  <pre>int drvNDColorConvertConfigure(const char *portName, int queueSize, int blockingCallbacks, 
                               const char *NDArrayPort, int NDArrayAddr, 
                               int maxBuffers, size_t maxMemory);
  </pre>
  <table border="1" cellpadding="2" cellspacing="2" style="text-align: left">
    <tr>
      <th>
        Argument</th>
      <th>
        Description</th>
    </tr>
    <tr>
      <td>
        <code>portName</code></td>
      <td>
        The name of the asyn port for this plugin.
      </td>
    </tr>
    <tr>
      <td>
        <code>queueSize</code></td>
      <td>
        The maximum number of NDArray objects that can be queued for processing. Passed
        to the NDPluginDriver base class constructor.
      </td>
    </tr>
    <tr>
      <td>
        <code>blockingCallbacks</code></td>
      <td>
        Flag controlling whether callbacks block. Passed to the NDPluginDriver base class
        constructor.
      </td>
    </tr>
    <tr>
      <td>
        <code>NDArrayPort</code></td>
      <td>
        The name of the asyn port of the driver that will provide the NDArray data. Passed
        to the NDPluginDriver base class constructor.
      </td>
    </tr>
    <tr>
      <td>
        <code>NDArrayAddr</code></td>
      <td>
        The asyn addr of the asyn port of the driver that will provide the NDArray data.
        Passed to the NDPluginDriver base class constructor.
      </td>
    </tr>
    <tr>
      <td>
        <code>maxBuffers</code></td>
      <td>
        Maximum number of NDArray buffers to be created for plugin callbacks, i.e. for plugins
        that will be getting called from this plugin. Passed to the constructor for the
        NDPluginDriver base class.</td>
    </tr>
    <tr>
      <td>
        <code>maxMemory</code></td>
      <td>
        Maximum number of bytes of memory to be allocated from the NDArrayPool. Passed to
        the constructor for the NDPluginDriver base class.</td>
    </tr>
  </table>
  <h2 id="Screens">
    Screen shots</h2>
  <p>
    The following is the MEDM screen that provides access to the parameters in NDPluginDriver.h
    and NDPluginColorConvert.h through records in NDPluginBase.template and NDColorConvert.template.
  </p>
  <div style="text-align: center">
    <h3>
      NDColorConvert.adl</h3>
    <p>
      <img alt="NDColorConvert.png" src="NDColorConvert.png" /></p>
  </div>
  <h2 id="Restrictions">
    Restrictions</h2>
  <ul>
    <li>The Bayer color conversion is done using a library function provided in the Prosilica
      library. The source code for this function is not provided, and the binaries are
      only available on Linux and Windows.</li>
    <li>YUV color conversion is not supported. This may be added in a future release.</li>
  </ul>
</body>
</html>

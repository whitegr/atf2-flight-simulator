<html xmlns="http://www.w3.org/1999/xhtml">
<head>
  <title>areaDetector Plugins</title>
</head>
<body>
  <div style="text-align: center">
    <h1>
      areaDetector Plugins</h1>
    <h2>
      January 30, 2009</h2>
    <h2>
      Mark Rivers</h2>
    <h2>
      University of Chicago</h2>
  </div>
  <h2>
    Contents</h2>
  <ul>
    <li><a href="#Overview">Overview</a></li>
    <li><a href="#NDPluginDriver">NDPluginDriver</a></li>
    <li><a href="NDPluginStdArrays.html">NDPluginStdArrays</a></li>
    <li><a href="NDPluginFile.html">NDPluginFile</a></li>
    <li><a href="NDPluginROI.html">NDPluginROI</a></li>
    <li><a href="NDPluginColorConvert.html">NDPluginColorConvert</a></li>
  </ul>
  <h2 id="Overview">
    Overview</h2>
  <p>
    A powerful feature of the <a href="areaDetectorDoc.html">areaDetector</a> module
    is the concept of plugins. A plugin is code that is called by a driver that passes
    NDArray data in a callback. Plugins can be used to process array data in real time.
    Existing plugins convert data to standard asyn arrays (NDPluginStdArrays), save
    data to disk (NDPluginFile), select regions-of-interest (NDPluginROI), and convert
    color modes (NDPluginColorConvert). New plugins could be written to perform functions
    like finding the centroid of a beam, etc. Once a plugin is written it will work
    with any areaDetector driver. Plugins have the the following properties:
  </p>
  <ul>
    <li>They can execute either in a blocking mode or a non-blocking mode. In the blocking
      mode the callback is executed by the driver callback thread. In this mode the callback
      is guaranteed to execute for each NDArray callback. However, it can slow down the
      driver, and does not utilize the multi-core capability of modern CPUs. In the non-blocking
      mode the driver callback simply places the NDArray data in a queue that is part
      of the plugin. The plugin then executes the callback code in it own thread. It removes
      NDArray data from the queue, processes it, and releases the data back to the NDArrayPool
      when it is done. In the non-blocking mode some additional memory is required for
      the NDArray objects that are in the queue. It is also possible to drop NDArray data
      if the queue is full when a callback occurs, i.e. some callback data will not be
      processed. The non-blocking mode can utilize the multi-core capabilities of modern
      CPUs because each plugin is executing in its own thread. The operation of the queue
      and the NDArrayPool class means that data never needs to be copied, each plugin
      has a pointer to the data which will continue to be valid until the last plugin
      is done with it.</li>
    <li>They can be enabled or disabled at run time.</li>
    <li>They can be throttled to only execute at a limited rate. This means, for example,
      that a detector can be saving data to disk at full speed, but images can be posted
      to EPICS at a reduced rate.</li>
    <li>They can be unplugged from one driver, and plugged into another driver at run
      time. For example, the NDPluginROI driver is itself a source of NDArray data callbacks,
      so a file saving plugin could be unplugged from a detector driver (where it would
      be saving the entire detector), and plugged into a particular ROI, where it would
      just save a portion of the detector. Similarly the NDPluginColorConvert plugin is
      also a source of NDArray data callbacks. A pipeline of plugins can be constructed,
      for example NDPluginColorConvert -&gt NDPluginROI -&gt NDPluginStdArrays. Each stage
      of this pipeline can be executing in its own thread, and on modern multi-core processors
      each can be executing on its own core.</li>
  </ul>
  <h2 id="NDPluginDriver">
    NDPluginDriver</h2>
  <p>
    NDPluginDriver inherits from <a href="areaDetectorDoc.html#asynNDArrayDriver">asynNDArrayDriver</a>.
    NDPluginDriver is the class from which actual plugins are directly derived. The
    NDPluginDriver class handles most of the details of processing NDArray callbacks
    from the driver. Plugins derived from this class typically need to implement the
    processCallbacks method, the drvUser method, and one or more of the write(Int32,
    Float64, Octet) methods. The NDPluginDriver public interface is defined in NDPluginDriver.h
    as follows:
  </p>
  <pre>class NDPluginDriver : public asynNDArrayDriver {
public:
    NDPluginDriver(const char *portName, int queueSize, int blockingCallbacks, 
                 const char *NDArrayPort, int NDArrayAddr, int maxAddr, int paramTableSize,
                 int maxBuffers, size_t maxMemory, int interfaceMask, int interruptMask);
                 
    /* These are the methods that we override from asynNDArrayDriver */
    virtual asynStatus writeInt32(asynUser *pasynUser, epicsInt32 value);
    virtual asynStatus writeOctet(asynUser *pasynUser, const char *value, size_t maxChars,
                          size_t *nActual);
    virtual asynStatus readInt32Array(asynUser *pasynUser, epicsInt32 *value,
                                        size_t nElements, size_t *nIn);
    virtual asynStatus drvUserCreate(asynUser *pasynUser, const char *drvInfo, 
                                     const char **pptypeName, size_t *psize);
                                     
    /* These are the methods that are new to this class */
    virtual void processCallbacks(NDArray *pArray);
    virtual int createFileName(int maxChars, char *fullFileName);
    virtual int createFileName(int maxChars, char *filePath, char *fileName);

...
}
</pre>
  <p>
    The methods of the NDPluginDriver class are:
  </p>
  <ul>
    <li><code>NDPluginDriver</code> This is the constructor for the class. All parameters
      except queueSize, blockingCallbacks, NDArrayPort, and NDArrayAddr are just passed
      to the constructor for asynNDArrayDriver. NDArrayPort and NDArrayAddr, and blockingCallbacks
      are the initial values for the NDPluginDriverArrayPort, NDPluginDriverArrayAddr
      and NDPluginDriverBlockingCallbacks parameters described in the table below.</li>
    <li><code>drvUserCreate</code> This method returns one of the enum values for the
      parameters defined in NDPluginDriver.h if the driverInfo field matches one the strings
      defined in that file. Derived classes will typically provide an implementation of
      drvUserCreate() that searches for parameters that are unique to that plugin. If
      a parameter is not matched, then NDPluginDriver->drvUserCreate() will be called
      to see if it is a standard plugin parameter (defined in NDPluginDriver.h).</li>
    <li><code>processCallbacks</code> This method is called each time a driver calls the
      plugin with a new NDArray object. Derived classes typically provide an implementation
      of this method that calls this base class method to perform some bookkeeping chores,
      and then they perform whatever operations are specific to that plugin.</li>
    <li><code>createFileName</code> This is a convenience function that constructs a complete
      file name in the ADFullFileName parameter from the ADFilePath, ADFileName, ADFileNumber,
      and ADFileTemplate parameters. There are two variants: the first returns a complete
      file name, including the path. The second returns the path and file name separately.</li>
  </ul>
  <p>
    NDPluginDriver defines parameters that all plugin drivers should implement if possible.
    These parameters are defined by enum values with an associated asyn interface, and
    access (read-only or read-write). The EPICS database NDPluginBase.template provides
    access to these standard plugin parameters, listed in the following table. Note
    that to reduce the width of this table the enum names have been split into 2 lines,
    but these are just a single name, for example <code>NDPluginDriverArrayPort</code>.
  </p>
  <table border="1" cellpadding="2" cellspacing="2" style="text-align: left">
    <tbody>
      <tr>
        <td align="CENTER" colspan="7,">
          <b>Parameter Definitions in NDPluginDriver.h and EPICS Record Definitions in NDPluginBase.template</b></td>
      </tr>
      <tr>
        <th>
          Enum name</th>
        <th>
          asyn interface</th>
        <th>
          Access</th>
        <th>
          Description</th>
        <th>
          drvUser string</th>
        <th>
          EPICS record name</th>
        <th>
          EPICS record type</th>
      </tr>
      <tr>
        <td align="CENTER" colspan="7,">
          <b>asyn NDArray driver doing callbacks</b></td>
      </tr>
      <tr>
        <td>
          NDPluginDriver<br />
          ArrayPort</td>
        <td>
          asynOctet</td>
        <td>
          r/w</td>
        <td>
          asyn port name for NDArray driver that will make callbacks to this plugin. This
          port can be changed at run time, connecting the plugin to a different NDArray driver.</td>
        <td>
          NDARRAY_PORT</td>
        <td>
          $(P)$(R)NDArrayPort<br />
          (P)$(R)NDArrayPort_RBV</td>
        <td>
          stringout<br />
          stringin</td>
      </tr>
      <tr>
        <td>
          NDPluginDriver<br />
          ArrayAddr</td>
        <td>
          asynInt32</td>
        <td>
          r/w</td>
        <td>
          asyn port address for NDArray driver that will make callbacks to this plugin. This
          address can be changed at run time, connecting the plugin to a different address
          in the NDArray driver.</td>
        <td>
          NDARRAY_ADDR</td>
        <td>
          $(P)$(R)NDArrayAddress<br />
          $(P)$(R)NDArrayAddress_RBV</td>
        <td>
          longout<br />
          longin</td>
      </tr>
      <tr>
        <td align="CENTER" colspan="7,">
          <b>Callback enable, minimum time, and statistics</b></td>
      </tr>
      <tr>
        <td>
          NDPluginDriver<br />
          EnableCallbacks</td>
        <td>
          asynInt32</td>
        <td>
          r/w</td>
        <td>
          Enable (1) or disable (0) callbacks from the driver to this plugin. If callbacks
          are disabled then the plugin will normally be idle and consume no CPU resources.</td>
        <td>
          ENABLE_CALLBACKS</td>
        <td>
          $(P)$(R)EnableCallbacks<br />
          $(P)$(R)EnableCallbacks_RBV</td>
        <td>
          bo<br />
          bi</td>
      </tr>
      <tr>
        <td>
          NDPluginDriver<br />
          BlockingCallbacks</td>
        <td>
          asynInt32</td>
        <td>
          r/w</td>
        <td>
          0 = callbacks from the driver do not block; the NDArray data is put on a queue and
          the callback processes in its own thread.
          <br />
          1 = callbacks from the driver block; the callback processes in the driver callback
          thread.</td>
        <td>
          BLOCKING_CALLBACKS</td>
        <td>
          $(P)$(R)CallbacksBlock<br />
          $(P)$(R)CallbacksBlock_RBV</td>
        <td>
          bo<br />
          bi</td>
      </tr>
      <tr>
        <td>
          NDPluginDriver<br />
          MinCallbackTime</td>
        <td>
          asynFloat64</td>
        <td>
          r/w</td>
        <td>
          The minimum time in seconds between calls to processCallbacks. Any callbacks occuring
          before this minimum time has elapsed will be ignored. 0 means no minimum time, i.e.
          process all callbacks.</td>
        <td>
          MIN_CALLBACK_TIME</td>
        <td>
          $(P)$(R)MinCallbackTime<br />
          $(P)$(R)MinCallbackTime_RBV</td>
        <td>
          ao<br />
          ai</td>
      </tr>
      <tr>
        <td>
          NDPluginDriver<br />
          ArrayCounter</td>
        <td>
          asynInt32</td>
        <td>
          r/w</td>
        <td>
          Counter that increments by 1 each time an NDArray callback is processed</td>
        <td>
          ARRAY_COUNTER</td>
        <td>
          $(P)$(R)ArrayCounter<br />
          $(P)$(R)ArrayCounter_RBV</td>
        <td>
          longout<br />
          longin</td>
      </tr>
      <tr>
        <td>
          N/A</td>
        <td>
          N/A</td>
        <td>
          r/o</td>
        <td>
          Rate (Hz) at which ArrayCounter is incrementing. Computed in database.</td>
        <td>
          N/A</td>
        <td>
          $(P)$(R)ArrayRate_RBV</td>
        <td>
          calc</td>
      </tr>
      <tr>
        <td>
          NDPluginDriver<br />
          DroppedArrays</td>
        <td>
          asynInt32</td>
        <td>
          r/w</td>
        <td>
          Counter that increments by 1 each time an NDArray callback occurs when NDPluginDriverBlockingCallbacks=0
          and the plugin driver queue is full, so the callback cannot be processed.</td>
        <td>
          DROPPED_ARRAYS</td>
        <td>
          $(P)$(R)DroppedArrays<br />
          $(P)$(R)DroppedArrays_RBV</td>
        <td>
          longout<br />
          longin</td>
      </tr>
      <tr>
        <td align="CENTER" colspan="7,">
          <b>Information about last NDArray callback data</b></td>
      </tr>
      <tr>
        <td>
          NDPluginDriver<br />
          NDimensions</td>
        <td>
          asynInt32</td>
        <td>
          r/o</td>
        <td>
          Number of dimensions in last NDArray callback data</td>
        <td>
          ARRAY_NDIMENSIONS</td>
        <td>
          $(P)$(R)NDimensions_RBV</td>
        <td>
          longin</td>
      </tr>
      <tr>
        <td>
          NDPluginDriver<br />
          Dimensions</td>
        <td>
          asynInt32Array</td>
        <td>
          r/o</td>
        <td>
          Dimensions in last NDArray callback data</td>
        <td>
          ARRAY_DIMENSIONS</td>
        <td>
          $(P)$(R)Dimensions_RBV</td>
        <td>
          waveform</td>
      </tr>
      <tr>
        <td>
          N/A</td>
        <td>
          N/A</td>
        <td>
          r/o</td>
        <td>
          First dimension of NDArray callback data</td>
        <td>
          N/A</td>
        <td>
          $(P)$(R)ArraySize0_RBV</td>
        <td>
          longin</td>
      </tr>
      <tr>
        <td>
          N/A</td>
        <td>
          N/A</td>
        <td>
          r/o</td>
        <td>
          Second dimension of NDArray callback data</td>
        <td>
          N/A</td>
        <td>
          $(P)$(R)ArraySize1_RBV</td>
        <td>
          longin</td>
      </tr>
      <tr>
        <td>
          NDPluginDriver<br />
          DataType</td>
        <td>
          asynInt32</td>
        <td>
          r/o</td>
        <td>
          Data type of last NDArray callback data (NDDataType_t).</td>
        <td>
          DATA_TYPE</td>
        <td>
          $(P)$(R)DataType_RBV</td>
        <td>
          mbbi</td>
      </tr>
      <tr>
        <td>
          NDPluginDriver<br />
          ColorMode</td>
        <td>
          asynInt32</td>
        <td>
          r/o</td>
        <td>
          Color mode of last NDArray callback data (NDColorMode_t).</td>
        <td>
          COLOR_MODE</td>
        <td>
          $(P)$(R)ColorMode_RBV</td>
        <td>
          mbbi</td>
      </tr>
      <tr>
        <td>
          NDPluginDriver<br />
          BayerPattern</td>
        <td>
          asynInt32</td>
        <td>
          r/o</td>
        <td>
          BayerPattern of last NDArray callback data (NDBayerPattern_t).</td>
        <td>
          BAYER_PATTERN</td>
        <td>
          $(P)$(R)BayerPattern_RBV</td>
        <td>
          mbbi</td>
      </tr>
      <tr>
        <td>
          NDPluginDriver<br />
          UniqueId</td>
        <td>
          asynInt32</td>
        <td>
          r/o</td>
        <td>
          Unique ID number of last NDArray callback data</td>
        <td>
          UNIQUE_ID</td>
        <td>
          $(P)$(R)UniqueId_RBV</td>
        <td>
          longin</td>
      </tr>
      <tr>
        <td>
          NDPluginDriver<br />
          TimeStamp</td>
        <td>
          asynFloat64</td>
        <td>
          r/o</td>
        <td>
          Time stamp number of last NDArray callback data</td>
        <td>
          TIME_STAMP</td>
        <td>
          $(P)$(R)TimeStamp_RBV</td>
        <td>
          ai</td>
      </tr>
      <tr>
        <td align="CENTER" colspan="7,">
          <b>Debugging control</b></td>
      </tr>
      <tr>
        <td>
          N/A</td>
        <td>
          N/A</td>
        <td>
          N/A</td>
        <td>
          asyn record to control debugging (asynTrace)</td>
        <td>
          N/A</td>
        <td>
          $(P)$(R)AsynIO</td>
        <td>
          asyn</td>
      </tr>
    </tbody>
  </table>
</body>
</html>

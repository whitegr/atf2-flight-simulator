<html xmlns="http://www.w3.org/1999/xhtml">
<head>
  <title>areaDetector MarCCD driver</title>
</head>
<body>
  <div style="text-align: center">
    <h1>
      areaDetector MarCCD driver</h1>
    <h2>
      January 26, 2009</h2>
    <h2>
      Mark Rivers</h2>
    <h2>
      University of Chicago</h2>
  </div>
  <h2>
    Table of Contents</h2>
  <ul>
    <li><a href="#Introduction">Introduction</a></li>
    <li><a href="#StandardNotes">Implementation of standard driver parameters</a></li>
    <li><a href="#Driver_parameters">MarCCD specific parameters</a></li>
    <li><a href="#Unsupported">Unsupported standard driver parameters</a></li>
    <li><a href="#Configuration">Configuration</a></li>
    <li><a href="#MEDM_screens">MEDM screens</a></li>
    <li><a href="#Performance_measurements">Performance measurements</a> </li>
    <li><a href="#Restrictions">Restrictions</a> </li>
  </ul>
  <h2 id="Introduction" style="text-align: left">
    Introduction</h2>
  <p>
    This is a driver for the MarCCD detectors from <a href="http://www.mar-usa.com/">Rayonix/MarUSA</a>.
    It inherits from ADDriver and implements many of the parameters in ADStdDriverParams.h.
    It also implements a number of parameters that are specific to the MarCCD detectors.</p>
  <p>
    The interface to the detector is via a TCP/IP socket interface to the <b>marccd_server_socket</b>
    server that MarUSA provides. The marccd_server_socket program must be started before
    the areaDetector software is started, by running the marccd program and executing
    Acquire/Remote Control/Start.
  </p>
  <p>
    marccd must be using Version 1 of the remote protocol. This is normally done done
    by editing the file marccd/configuration/marccd.conf and replacing the line</p>
  <pre>include marccd_server_v0.conf
    </pre>
  <p>
    with</p>
  <pre>include marccd_server_v1.conf
    </pre>
  <p>
    The file marccd_server_v1.conf should contain the lines:</p>
  <pre>remote_mode_server_command /home/marccd/contrib/marccd_server/marccd_server_socket
remote_mode_server_arguments 2222
    </pre>
  <p>
    The first line points to the location of the marccd_server_socket program that is
    used to implement remote control. In order to work with the areaDetector driver
    this must be a version of this program created after November 11, 2008 when support
    for the <code>get_frameshift</code> command was added. A recent version of this
    program can be downloaded from the <a href="ftp://ftp.rayonix.com/pub/marccd/example_marccd_server.tgz">
      Rayonix FTP site</a>.
  </p>
  <p>
    The marccd program saves the data to disk as TIFF files. The areaDetector software
    reads these disk files in order to read the data, because marccd does not provide
    another mechanism to access the data.
  </p>
  <h2 id="StandardNotes" style="text-align: left">
    Implementation of standard driver parameters</h2>
  <p>
    The following table describes how the MarCCD driver implements some of the standard
    driver parameters.
  </p>
  <table border="1" cellpadding="2" cellspacing="2" style="text-align: left">
    <tbody>
      <tr>
        <td align="center" colspan="3">
          <b>Implementation of Parameters in ADStdDriverParams.h and EPICS Record Definitions
            in ADBase.template and NDFile.template</b></td>
      </tr>
      <tr>
        <th>
          Enum name</th>
        <th>
          EPICS record name</th>
        <th>
          Description</th>
      </tr>
      <tr>
        <td>
          ADFrameType</td>
        <td>
          $(P$(R)FrameType</td>
        <td>
          The driver redefines the choices for the ADFrameType parameter (record $(P)$(R)FrameType)
          from ADStdDriverParams.h. The choices for the MarCCD are:
          <ul>
            <li>Normal (corrected data frame without double correlation)</li>
            <li>Background (background frame with 0 exposure time, done with double correlation
              to remove zingers)</li>
            <li>Raw (data frame without correction for background or spatial distortion)</li>
            <li>DblCorrelation (two images each collected for half the nominal acquisition time,
              zingers removed by double correlation)</li>
          </ul>
        </td>
      </tr>
      <tr>
        <td>
          ADNumImages</td>
        <td>
          $(P$(R)NumImages</td>
        <td>
          Controls the number of images to acquire when ADImageMode is ADImageMultiple.</td>
      </tr>
      <tr>
        <td>
          ADAcquirePeriod</td>
        <td>
          $(P$(R)AcquirePeriod</td>
        <td>
          Controls the period between images when ADImageMode is ADImageMultiple or ADImageContinuous.
          If this is greater than the acquisition time plus readout overhead then the driver
          will wait until the period has elapsed before starting the next acquisition.</td>
      </tr>
      <tr>
        <td>
          ADReadStatus</td>
        <td>
          $(P)$(R)ReadStatus</td>
        <td>
          Writing 1 to this parameter causes the status to be read from the marccd server.
          By processing or periodically scanning this record the status information can be
          refreshed. This is normally not necessary, but if ADArrayCallbacks is 0 and marCCDOverlap
          is 1 then the status will not indicate that the system is idle when acquisition
          is complete, because the driver polling stops before the file is written. This record
          can be used to eliminate the confusion that might cause.</td>
      </tr>
      <tr>
        <td>
          ADFilePath</td>
        <td>
          $(P$(R)FilePath</td>
        <td>
          Controls the path for saving images. It must be a valid path for marccd <i>and</i>
          for the areaDetector driver, which is normally running in an EPICS IOC. If marccd
          and the EPICS IOC are not running on the same machine then soft links will typically
          be used to make the paths look identical.</td>
      </tr>
      <tr>
        <td>
          ADFileFormat</td>
        <td>
          $(P)$(R)FileFormat</td>
        <td>
          marccd only supports TIFF files.
        </td>
      </tr>
    </tbody>
  </table>
  <p>
    It is useful to use NDPluginROI to define an ROI containing the entire marccd detector.
    The MaxValue_RBV PV in this ROI can be monitored to make sure that the 16-bit limit
    of 65,535 is not being approached in any pixel.
  </p>
  <h2 id="Driver_parameters" style="text-align: left">
    MarCCD specific parameters</h2>
  <p>
    The MarCCD driver implements the following parameters in addition to those in ADStdDriverParams.h.
    Note that to reduce the width of this table the enum names have been split into
    2 lines, but these are just a single name, for example <code>marCCDState</code>.
  </p>
  <table border="1" cellpadding="2" cellspacing="2" style="text-align: left">
    <tbody>
      <tr>
        <td align="center" colspan="7">
          <b>Parameter Definitions in marccd.cpp and EPICS Record Definitions in marccd.template</b></td>
      </tr>
      <tr>
        <th>
          Enum name</th>
        <th>
          asyn interface</th>
        <th>
          Access</th>
        <th>
          Description</th>
        <th>
          drvUser string</th>
        <th>
          EPICS record name</th>
        <th>
          EPICS record type</th>
      </tr>
      <tr>
        <td align="center" colspan="7">
          <b>Status parameters</b></td>
      </tr>
      <tr>
        <td>
          marCCD<br />
          State</td>
        <td>
          asynInt32</td>
        <td>
          r/o</td>
        <td>
          State word returned by marccd server. The low-order 4-bits of this word are the
          state of the marccd server, and will be Idle (0x0), Error (0x7), or Busy (0x8).
          The next 20 bits encode the state of the 5 server tasks (Acquire, Readout, Correct,
          Save, Dezinger) with 4-bits per task. Each task can be in the state Idle (0x0),
          Queued (0x1), Executing (0x2), Error (0x4), or Reserved (0x8).</td>
        <td>
          MAR_STATE</td>
        <td>
          $(P)$(R)MarState_RBV</td>
        <td>
          longin</td>
      </tr>
      <tr>
        <td>
          marCCD<br />
          Status</td>
        <td>
          asynInt32</td>
        <td>
          r/o</td>
        <td>
          Status of the marccd server task (Idle, Error, or Busy)</td>
        <td>
          MAR_STATUS</td>
        <td>
          $(P)$(R)MarStatus_RBV</td>
        <td>
          mbbi</td>
      </tr>
      <tr>
        <td>
          marCCDTask<br />
          AcquireStatus</td>
        <td>
          asynInt32</td>
        <td>
          r/o</td>
        <td>
          Status of the marccd server acquire task (Idle, Queued, Executing, Error, or Reserved)</td>
        <td>
          MAR_ACQUIRE_STATUS</td>
        <td>
          $(P)$(R)MarAcquireStatus_RBV</td>
        <td>
          mbbi</td>
      </tr>
      <tr>
        <td>
          marCCDTask<br />
          ReadoutStatus</td>
        <td>
          asynInt32</td>
        <td>
          r/o</td>
        <td>
          Status of the marccd server readout task (Idle, Queued, Executing, Error, or Reserved)</td>
        <td>
          MAR_READOUT_STATUS</td>
        <td>
          $(P)$(R)MarReadoutStatus_RBV</td>
        <td>
          mbbi</td>
      </tr>
      <tr>
        <td>
          marCCDTask<br />
          CorrectStatus</td>
        <td>
          asynInt32</td>
        <td>
          r/o</td>
        <td>
          Status of the marccd server correct task (Idle, Queued, Executing, Error, or Reserved)</td>
        <td>
          MAR_CORRECT_STATUS</td>
        <td>
          $(P)$(R)MarCorrectStatus_RBV</td>
        <td>
          mbbi</td>
      </tr>
      <tr>
        <td>
          marCCDTask<br />
          WritingStatus</td>
        <td>
          asynInt32</td>
        <td>
          r/o</td>
        <td>
          Status of the marccd server file writing task (Idle, Queued, Executing, Error, or
          Reserved)</td>
        <td>
          MAR_WRITING_STATUS</td>
        <td>
          $(P)$(R)MarWritingStatus_RBV</td>
        <td>
          mbbi</td>
      </tr>
      <tr>
        <td>
          marCCDTask<br />
          DezingerStatus</td>
        <td>
          asynInt32</td>
        <td>
          r/o</td>
        <td>
          Status of the marccd server dezinger task (Idle, Queued, Executing, Error, or Reserved)</td>
        <td>
          MAR_DEZINGER_STATUS</td>
        <td>
          $(P)$(R)MarDezingerStatus_RBV</td>
        <td>
          mbbi</td>
      </tr>
      <tr>
        <td align="center" colspan="7">
          <b>Optimization parameters</b></td>
      </tr>
      <tr>
        <td>
          marCCD<br />
          Overlap</td>
        <td>
          asynInt32</td>
        <td>
          r/w</td>
        <td>
          The marccd server has 5 tasks (Acquire, Readout, Correct, Write, Dezinger) that
          can overlap their operation. The areaDetector driver can exploit this to improve
          performance in some circumstances. If this parameter is set to 1 (Overlap) then
          the ADAcquire parameter will go to 0 (Done) when the Readout task is done executing,
          but before the Correct and Write tasks have finished correcting and saving the file
          to disk. This improves performance because the next image can begin as soon as ADAcquire
          goes to done, and hence before the previous image is written to disk. Note, however
          that this parameter must be set to 0 (Sequential) if callbacks are being used to
          compute ROIs that are being used in data collection, e.g. in a scan. If this is
          not done then the ROI information will be grabbed before it is updated and incorrect
          scan data will result.</td>
        <td>
          MAR_OVERLAP</td>
        <td>
          $(P)$(R)OverlapMode
          <br />
          $(P)$(R)OverlapMode_RBV</td>
        <td>
          bo
          <br />
          bi</td>
      </tr>
      <tr>
        <td align="center" colspan="7">
          <b>Frameshift parameters</b></td>
      </tr>
      <tr>
        <td>
          marCCD<br />
          Frameshift</td>
        <td>
          asynInt32</td>
        <td>
          r/w</td>
        <td>
          marccd can be used for time-resolved studies by collecting multiple data sets before
          reading out the detector. This is done by placing a mask in front of the detector
          that restricts the x-rays to horizontal stripe. An exposure is made, and then an
          external signal causes the detector to shift the image by the number of lines given
          by this parameter. A number of images separated by times of a few milliseconds can
          be collected, and then the detector is read out. Set this parameter to 0 to disable
          frameshift mode.</td>
        <td>
          MAR_FRAME_SHIFT</td>
        <td>
          $(P)$(R)FrameShift
          <br />
          $(P)$(R)FrameShift_RBV</td>
        <td>
          longout
          <br />
          longin</td>
      </tr>
      <tr>
        <td align="center" colspan="7">
          <b>Timeout parameters</b></td>
      </tr>
      <tr>
        <td>
          marCCD<br />
          TiffTimeout</td>
        <td>
          asynFloat64</td>
        <td>
          r/w</td>
        <td>
          Timeout in seconds when reading a TIFF file. It should be set to several seconds,
          because there it can take some time for the marccd server to write the file.</td>
        <td>
          MAR_TIFF_TIMEOUT</td>
        <td>
          $(P)$(R)ReadTiffTimeout</td>
        <td>
          ao</td>
      </tr>
      <tr>
        <td align="center" colspan="7">
          <b>Ancillary parameters. These parameters are written to the header of the marccd
            TIFF file.</b></td>
      </tr>
      <tr>
        <td>
          marCCD<br />
          DetectorDistance</td>
        <td>
          asynFloat64</td>
        <td>
          r/w</td>
        <td>
          Distance from the sample to the detector (mm)</td>
        <td>
          MAR_DETECTOR_DISTANCE</td>
        <td>
          $(P)$(R)DetectorDistance</td>
        <td>
          ao</td>
      </tr>
      <tr>
        <td>
          marCCD<br />
          BeamX</td>
        <td>
          asynFloat64</td>
        <td>
          r/w</td>
        <td>
          X position of the direct beam on the detector (mm)</td>
        <td>
          MAR_BEAM_X</td>
        <td>
          $(P)$(R)BeamX</td>
        <td>
          ao</td>
      </tr>
      <tr>
        <td>
          marCCD<br />
          BeamY</td>
        <td>
          asynFloat64</td>
        <td>
          r/w</td>
        <td>
          Y position of the direct beam on the detector (mm)</td>
        <td>
          MAR_BEAM_Y</td>
        <td>
          $(P)$(R)BeamY</td>
        <td>
          ao</td>
      </tr>
      <tr>
        <td>
          marCCD<br />
          StartPhi</td>
        <td>
          asynFloat64</td>
        <td>
          r/w</td>
        <td>
          Starting value of phi rotation (deg)</td>
        <td>
          MAR_START_PHI</td>
        <td>
          $(P)$(R)StartPhi</td>
        <td>
          ao</td>
      </tr>
      <tr>
        <td>
          marCCD<br />
          RotationAxis</td>
        <td>
          asynOctet</td>
        <td>
          r/w</td>
        <td>
          Rotation axis being used (phi, omega, etc.)</td>
        <td>
          MAR_ROTATION_AXIS</td>
        <td>
          $(P)$(R)RotationAxis</td>
        <td>
          ao</td>
      </tr>
      <tr>
        <td>
          marCCD<br />
          RotationRange</td>
        <td>
          asynFloat64</td>
        <td>
          r/w</td>
        <td>
          Rotation range of the rotation axis.</td>
        <td>
          MAR_ROTATION_RANGE</td>
        <td>
          $(P)$(R)RotationRange</td>
        <td>
          ao</td>
      </tr>
      <tr>
        <td>
          marCCD<br />
          Wavelength</td>
        <td>
          asynFloat64</td>
        <td>
          r/w</td>
        <td>
          Wavelength in Angstroms.</td>
        <td>
          MAR_WAVELENGTH</td>
        <td>
          $(P)$(R)Wavelength</td>
        <td>
          ao</td>
      </tr>
      <tr>
        <td>
          marCCD<br />
          FileComments</td>
        <td>
          asynOctet</td>
        <td>
          r/w</td>
        <td>
          Comments for this file.</td>
        <td>
          MAR_FILE_COMMENTS</td>
        <td>
          $(P)$(R)FileComments</td>
        <td>
          waveform</td>
      </tr>
      <tr>
        <td>
          marCCD<br />
          DatasetComments</td>
        <td>
          waveform</td>
        <td>
          r/w</td>
        <td>
          Comments for this dataset.</td>
        <td>
          MAR_DATASET_COMMENTS</td>
        <td>
          $(P)$(R)DatasetComments</td>
        <td>
          waveform</td>
      </tr>
      <tr>
        <td align="center" colspan="7">
          <b>Debugging</b></td>
      </tr>
      <tr>
        <td>
          N/A</td>
        <td>
          N/A</td>
        <td>
          N/A</td>
        <td>
          asyn record to control debugging communication with marccd_server_socket program</td>
        <td>
          N/A</td>
        <td>
          $(P)$(R)marSserverAsyn</td>
        <td>
          asyn</td>
      </tr>
    </tbody>
  </table>
  <h2 id="Unsupported">
    Unsupported standard driver parameters</h2>
  <p>
    The MarCCD driver does not support the following standard driver parameters because
    they are not supported in the marccd program:</p>
  <ul>
    <li>Number of exposures per image (ADNumExposures)</li>
    <li>Trigger mode (ADTriggerMode)</li>
    <li>Gain (ADGain)</li>
    <li>Region to read out (ADMinX, ADMinY, ADSizeX, ADSizeY, ADReverseX, ADReverseY)</li>
    <li>Data type (ADDataType)</li>
    <li>Reading previous files (ADReadFile)</li>
    <li>Capture or stream file saving (ADFileWriteMode, ADFileCapture, ADNumCapture, ADNumCaptured)</li>
  </ul>
  <h2 id="Configuration" style="text-align: left">
    Configuration</h2>
  <p>
    The MarCCD driver is created with the following command, either from C/C++ or from
    the EPICS IOC shell.
  </p>
  <pre>   
marCCDConfig(const char *portName, const char *marCCDPort, 
             int maxBuffers, size_t maxMemory);
  </pre>
  <table border="1" cellpadding="2" cellspacing="2" style="text-align: left">
    <tbody>
      <tr>
        <th>
          Argument</th>
        <th>
          Description</th>
      </tr>
      <tr>
        <td>
          <code>portName</code></td>
        <td>
          The name of the asyn port for this detector.
        </td>
      </tr>
      <tr>
        <td>
          <code>marCCDPort</code></td>
        <td>
          The name of the asyn TCP/IP port to communicate with marccd_server_socket. This
          must have been previously created with <code>drvAsynIPPortConfig()</code>,
        </td>
      </tr>
      <tr>
        <td>
          <code>maxBuffers</code></td>
        <td>
          Maximum number of buffers to be created for plugin callbacks. Passed to the constructor
          for the ADDriver base class.</td>
      </tr>
      <tr>
        <td>
          <code>maxMemory</code></td>
        <td>
          Maximum number of bytes of memory to be allocated for plugin callbacks. Passed to
          the constructor for the ADDriver base class.</td>
      </tr>
    </tbody>
  </table>
  <p>
    The following is an example st.cmd startup script:
  </p>
  <pre>< envPaths
errlogInit(20000)

dbLoadDatabase("$(AREA_DETECTOR)/dbd/marCCDApp.dbd")
marCCDApp_registerRecordDeviceDriver(pdbbase) 

###
# Create the asyn port to talk to the MAR on port 2222
drvAsynIPPortConfigure("marServer","gse-marccd1.cars.aps.anl.gov:2222")
# Set the input and output terminators.
asynOctetSetInputEos("marServer", 0, "\n")
asynOctetSetOutputEos("marServer", 0, "\n")
#asynSetTraceMask("marServer",0,9)
asynSetTraceIOMask("marServer",0,2)

marCCDConfig("MAR", "marServer", 20, 200000000)
dbLoadRecords("$(AREA_DETECTOR)/ADApp/Db/ADBase.template",  "P=13MARCCD1:,R=cam1:,PORT=MAR,ADDR=0,TIMEOUT=1")
dbLoadRecords("$(AREA_DETECTOR)/ADApp/Db/marCCD.template","P=13MARCCD1:,R=cam1:,PORT=MAR,ADDR=0,TIMEOUT=1,MARSERVER_PORT=marServer")
dbLoadRecords("$(AREA_DETECTOR)/ADApp/Db/NDFile.template","P=13MARCCD1:,R=cam1:,PORT=MAR,ADDR=0,TIMEOUT=1")

# Create a standard arrays plugin
drvNDStdArraysConfigure("MARImage", 5, 0, "MAR", 0, -1)
dbLoadRecords("$(AREA_DETECTOR)/ADApp/Db/NDPluginBase.template","P=13MARCCD1:,R=image1:,PORT=MARImage,ADDR=0,TIMEOUT=1,NDARRAY_PORT=MAR,NDARRAY_ADDR=0")
dbLoadRecords("$(AREA_DETECTOR)/ADApp/Db/NDStdArrays.template", "P=13MARCCD1:,R=image1:,PORT=MARImage,ADDR=0,TIMEOUT=1,SIZE=16,FTVL=SHORT,NELEMENTS=1200000")

# Create an ROI plugin
drvNDROIConfigure("MARROI", 5, 0, "MAR", 0, 10, 20, -1)
dbLoadRecords("$(AREA_DETECTOR)/ADApp/Db/NDPluginBase.template","P=13MARCCD1:,R=ROI1:,  PORT=MARROI,ADDR=0,TIMEOUT=1,NDARRAY_PORT=MAR,NDARRAY_ADDR=0")
dbLoadRecords("$(AREA_DETECTOR)/ADApp/Db/NDROI.template",       "P=13MARCCD1:,R=ROI1:,  PORT=MARROI,ADDR=0,TIMEOUT=1")
dbLoadRecords("$(AREA_DETECTOR)/ADApp/Db/NDROIN.template",      "P=13MARCCD1:,R=ROI1:0:,PORT=MARROI,ADDR=0,TIMEOUT=1,HIST_SIZE=256")
dbLoadRecords("$(AREA_DETECTOR)/ADApp/Db/NDROIN.template",      "P=13MARCCD1:,R=ROI1:1:,PORT=MARROI,ADDR=1,TIMEOUT=1,HIST_SIZE=256")
dbLoadRecords("$(AREA_DETECTOR)/ADApp/Db/NDROIN.template",      "P=13MARCCD1:,R=ROI1:2:,PORT=MARROI,ADDR=2,TIMEOUT=1,HIST_SIZE=256")
dbLoadRecords("$(AREA_DETECTOR)/ADApp/Db/NDROIN.template",      "P=13MARCCD1:,R=ROI1:3:,PORT=MARROI,ADDR=3,TIMEOUT=1,HIST_SIZE=256")
dbLoadRecords("$(AREA_DETECTOR)/ADApp/Db/NDROIN.template",      "P=13MARCCD1:,R=ROI1:4:,PORT=MARROI,ADDR=3,TIMEOUT=1,HIST_SIZE=256")
dbLoadRecords("$(AREA_DETECTOR)/ADApp/Db/NDROIN.template",      "P=13MARCCD1:,R=ROI1:5:,PORT=MARROI,ADDR=3,TIMEOUT=1,HIST_SIZE=256")
dbLoadRecords("$(AREA_DETECTOR)/ADApp/Db/NDROIN.template",      "P=13MARCCD1:,R=ROI1:6:,PORT=MARROI,ADDR=3,TIMEOUT=1,HIST_SIZE=256")
dbLoadRecords("$(AREA_DETECTOR)/ADApp/Db/NDROIN.template",      "P=13MARCCD1:,R=ROI1:7:,PORT=MARROI,ADDR=3,TIMEOUT=1,HIST_SIZE=256")

# Create "fastSweep" drivers for the MCA record to do on-the-fly scanning of ROI data
initFastSweep("MARSweepTotal", "MARROI", 8, 2048, "TOTAL_ARRAY", "CALLBACK_PERIOD")
initFastSweep("MARSweepNet", "MARROI", 8, 2048, "NET_ARRAY", "CALLBACK_PERIOD")

# Load MCA records for the fast sweep drivers
dbLoadRecords("$(MCA)/mcaApp/Db/mca.db", "P=13MARCCD1:,M=ROI1:0:TotalArray,DTYP=asynMCA,NCHAN=2048,INP=@asyn(MARSweepTotal 0)")
dbLoadRecords("$(MCA)/mcaApp/Db/mca.db", "P=13MARCCD1:,M=ROI1:1:TotalArray,DTYP=asynMCA,NCHAN=2048,INP=@asyn(MARSweepTotal 1)")
dbLoadRecords("$(MCA)/mcaApp/Db/mca.db", "P=13MARCCD1:,M=ROI1:2:TotalArray,DTYP=asynMCA,NCHAN=2048,INP=@asyn(MARSweepTotal 2)")
dbLoadRecords("$(MCA)/mcaApp/Db/mca.db", "P=13MARCCD1:,M=ROI1:3:TotalArray,DTYP=asynMCA,NCHAN=2048,INP=@asyn(MARSweepTotal 3)")
dbLoadRecords("$(MCA)/mcaApp/Db/mca.db", "P=13MARCCD1:,M=ROI1:4:TotalArray,DTYP=asynMCA,NCHAN=2048,INP=@asyn(MARSweepTotal 4)")
dbLoadRecords("$(MCA)/mcaApp/Db/mca.db", "P=13MARCCD1:,M=ROI1:5:TotalArray,DTYP=asynMCA,NCHAN=2048,INP=@asyn(MARSweepTotal 5)")
dbLoadRecords("$(MCA)/mcaApp/Db/mca.db", "P=13MARCCD1:,M=ROI1:6:TotalArray,DTYP=asynMCA,NCHAN=2048,INP=@asyn(MARSweepTotal 6)")
dbLoadRecords("$(MCA)/mcaApp/Db/mca.db", "P=13MARCCD1:,M=ROI1:7:TotalArray,DTYP=asynMCA,NCHAN=2048,INP=@asyn(MARSweepTotal 7)")

dbLoadRecords("$(MCA)/mcaApp/Db/mca.db", "P=13MARCCD1:,M=ROI1:0:NetArray,DTYP=asynMCA,NCHAN=2048,INP=@asyn(MARSweepNet 0)")
dbLoadRecords("$(MCA)/mcaApp/Db/mca.db", "P=13MARCCD1:,M=ROI1:1:NetArray,DTYP=asynMCA,NCHAN=2048,INP=@asyn(MARSweepNet 1)")
dbLoadRecords("$(MCA)/mcaApp/Db/mca.db", "P=13MARCCD1:,M=ROI1:2:NetArray,DTYP=asynMCA,NCHAN=2048,INP=@asyn(MARSweepNet 2)")
dbLoadRecords("$(MCA)/mcaApp/Db/mca.db", "P=13MARCCD1:,M=ROI1:3:NetArray,DTYP=asynMCA,NCHAN=2048,INP=@asyn(MARSweepNet 3)")
dbLoadRecords("$(MCA)/mcaApp/Db/mca.db", "P=13MARCCD1:,M=ROI1:4:NetArray,DTYP=asynMCA,NCHAN=2048,INP=@asyn(MARSweepNet 4)")
dbLoadRecords("$(MCA)/mcaApp/Db/mca.db", "P=13MARCCD1:,M=ROI1:5:NetArray,DTYP=asynMCA,NCHAN=2048,INP=@asyn(MARSweepNet 5)")
dbLoadRecords("$(MCA)/mcaApp/Db/mca.db", "P=13MARCCD1:,M=ROI1:6:NetArray,DTYP=asynMCA,NCHAN=2048,INP=@asyn(MARSweepNet 6)")
dbLoadRecords("$(MCA)/mcaApp/Db/mca.db", "P=13MARCCD1:,M=ROI1:7:NetArray,DTYP=asynMCA,NCHAN=2048,INP=@asyn(MARSweepNet 7)")


#asynSetTraceMask("MARROI",0,3)
#asynSetTraceIOMask("MARROI",0,4)

# Load scan records
dbLoadRecords("$(SSCAN)/sscanApp/Db/scan.db", "P=13MARCCD1:,MAXPTS1=2000,MAXPTS2=200,MAXPTS3=20,MAXPTS4=10,MAXPTSH=10")

set_requestfile_path("./")
set_savefile_path("./autosave")
set_requestfile_path("$(AREA_DETECTOR)/ADApp/Db")
set_requestfile_path("$(SSCAN)/sscanApp/Db")
set_pass0_restoreFile("auto_settings.sav")
set_pass1_restoreFile("auto_settings.sav")
save_restoreSet_status_prefix("13MARCCD1:")
dbLoadRecords("$(AUTOSAVE)/asApp/Db/save_restoreStatus.db", "P=13MARCCD1:")

iocInit()

# save things every thirty seconds
create_monitor_set("auto_settings.req", 30,"P=13MARCCD1:,D=cam1:")

</pre>
  <h2 id="MEDM_screens" style="text-align: left">
    MEDM screens</h2>
  <p>
    The following show the MEDM screens that are used to control the MarCCD detector.
    Note that the general purpose screen ADBase.adl can be used, but it exposes many
    controls that are not applicable to the MarCCD, and lacks some fields that are important
    for the MarCCD.</p>
  <p>
    <code>marccd.adl</code> is the main screen used to control the MarCCD driver.
  </p>
  <div style="text-align: center">
    <h3 style="text-align: center">
      marccd.adl</h3>
    <img alt="marCCD.png" src="marCCD.png" /></div>
  <p>
    <code>marccdAncillary.adl</code> is the screen used to input ancillary information
    that is written to the MarCCD TIFF files.
  </p>
  <div style="text-align: center">
    <h3 style="text-align: center">
      marccdAncillary.adl</h3>
    <img alt="MarCCDAncillary.png" src="MarCCDAncillary.png" /></div>
  <p>
    <code>asynRecord.adl</code> is used to control the debugging information printed
    by the asyn TCP/IP driver (asynTraceIODriver) and the EPICS device support (asynTraceIODevice).</p>
  <div style="text-align: center">
    <h3>
      asynRecord.adl</h3>
    <img alt="MarCCDAsynRecord.png" src="MarCCDAsynRecord.png" /></div>
  <p>
    <code>asynOctet.adl</code> can be used to send any command to the marccd remote
    server and display the response. It can be loaded from the More menu in asynRecord.adl
    above.</p>
  <div style="text-align: center">
    <h3>
      asynOctet.adl</h3>
    <img alt="MarCCDAsynOctet.png" src="MarCCDAsynOctet.png" /></div>
  <h2 id="Performance_measurements">
    Performance measurements</h2>
  <p>
    The following measurements were done to demonstrate the performance that can be
    obtained with the areaDetector MarCCD driver. These measurements were made with
    a MAR-165 CCD. The EPICS IOC was running on the same Linux machine as the marccd
    program. The acquisition time was 1 second.</p>
  <table border="1" cellpadding="2" cellspacing="2" style="text-align: left">
    <tbody>
      <tr>
        <th>
          Binning</th>
        <th>
          Image size</th>
        <th>
          marCCDOverlap</th>
        <th>
          Time for 10 images</th>
        <th>
          Overhead per image</th>
        <th>
          Time per task</th>
      </tr>
      <tr>
        <td>
          2x2
        </td>
        <td>
          2048x2048
        </td>
        <td>
          Sequential
        </td>
        <td>
          50.0
        </td>
        <td>
          4.00
        </td>
        <td>
          Readout: 3.02
          <br />
          Correct: 0.56
          <br />
          Save: 0.20
        </td>
      </tr>
      <tr>
        <td>
          2x2
        </td>
        <td>
          2048x2048
        </td>
        <td>
          Overlap
        </td>
        <td>
          46.2
        </td>
        <td>
          3.62
        </td>
        <td>
          Same
        </td>
      </tr>
      <tr>
        <td>
          4x4
        </td>
        <td>
          1024x1024
        </td>
        <td>
          Sequential
        </td>
        <td>
          29.0
        </td>
        <td>
          1.90
        </td>
        <td>
          Readout: 1.30
          <br />
          Correct: 0.28
          <br />
          Save: 0.06
        </td>
      </tr>
      <tr>
        <td>
          4x4
        </td>
        <td>
          1024x1024
        </td>
        <td>
          Overlap
        </td>
        <td>
          28.7
        </td>
        <td>
          1.87
        </td>
        <td>
          Same
        </td>
      </tr>
      <tr>
        <td>
          8x8
        </td>
        <td>
          512x512
        </td>
        <td>
          Sequential
        </td>
        <td>
          24.0
        </td>
        <td>
          1.40
        </td>
        <td>
          Readout: 0.78
          <br />
          Correct: 0.29
          <br />
          Save: 0.06
        </td>
      </tr>
      <tr>
        <td>
          8x8
        </td>
        <td>
          512x512
        </td>
        <td>
          Overlap
        </td>
        <td>
          23.6
        </td>
        <td>
          1.36
        </td>
        <td>
          Same
        </td>
      </tr>
    </tbody>
  </table>
  <h2 id="Restrictions">
    Restrictions</h2>
  <p>
    The following are some current restrictions of the MarCCD driver:</p>
  <ul>
    <li>The marccd program can save files in many different formats, with TIFF being the
      default. The areaDetector driver can only read TIFF files, not other formats. </li>
    <li>The areaDetector driver can in principle be run on machines other than the Linux
      machine running marccd, since the connection is via a socket. It has only been tested
      on Linux, because it requires libtiff and this is not normally installed, for example,
      on Windows.</li>
    <li>The MarCCD driver keeps retrying to read each TIFF file until the modification
      date of the TIFF file is <i>after</i> the time that the exposure command was issued.
      If it did not do this check then it could be reading and displaying old files that
      happen to have the same name as the current files being collected. This check requires
      that the computer that is running the soft IOC must have its clock well synchronized
      with the clock on the computer on which the files are being written (i.e. the computer
      generating the file modification time). If the clocks are not synchronized then
      the files may appear to be stale when they are not, and the driver will time out.
      The driver actually tolerates up to 10 second clock skew betweeen the computers
      but any more than this may lead to problems.</li>
    <li>The following items are hardcoded in the driver. They can be changed by recompiling
      compiling if necessary.
      <ul>
        <li>MAX_MESSAGE_SIZE=256 The maximum size of message to/from marccd_socket_server.</li>
        <li>MAX_FILENAME_LEN=256 The maximum size of a complete file name including path and
          extension.</li>
        <li>MARCCD_SERVER_TIMEOUT=1.0 Timeout when communicating with marccd_socket_server.</li>
        <li>FILE_READ_DELAY=.01 seconds. The time between polling to see if the TIFF file
          exists or if it is the expected size.</li>
        <li>MARCCD_POLL_DELAY=.01 seconds. The time between polling the marccd_socket_server
          status to see when a task has completed.</li>
      </ul>
    </li>
  </ul>
</body>
</html>

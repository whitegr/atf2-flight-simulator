function varargout = ps_control(varargin)
% PS_CONTROL M-file for ps_control.fig
%      PS_CONTROL, by itself, creates a new PS_CONTROL or raises the existing
%      singleton*.
%
%      H = PS_CONTROL returns the handle to a new PS_CONTROL or the handle to
%      the existing singleton*.
%
%      PS_CONTROL('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in PS_CONTROL.M with the given input arguments.
%
%      PS_CONTROL('Property','Value',...) creates a new PS_CONTROL or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before ps_control_OpeningFunction gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to ps_control_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help ps_control

% Last Modified by GUIDE v2.5 22-Oct-2007 14:51:01

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @ps_control_OpeningFcn, ...
                   'gui_OutputFcn',  @ps_control_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before ps_control is made visible.
function ps_control_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to ps_control (see VARARGIN)
global PS

% Choose default command line output for ps_control
% handles.output = hObject;

% Set layout of PS push buttons
handles.layout = {'1' '2' '3' '4' '5' '6' '7' '8' '38' '39' '40' '41' '42' '43' '44' '45' '70' '71' '78' '79' '80' '81' '82' '83' '84' '85' '110' '111' '112' '113' '114' '115' '116' '117' '142' '143' '150' '151';
                  '12' '13' '14' '15' '16' '17' '18' '19' '46' '47' '48' '49' '50' '51' '52' '53' '72' '73' '86' '87' '88' '89' '90' '91' '92' '93' '118' '119' '120' '121' '122' '123' '124' '125' '144' '145' '152' '153'};
handles.bulkLayout = {'22' '23' '24' '25' '26' '27' '28' '29' '54' '55' '56' '57' '58' '59' '60' '61' '74' '75' '94' '95' '96' '97' '98' '99' '100' '101' '126' '127' '128' '129' '130' '131' '132' '133' '146' '147' '154' '155';
                      '30' '31' '32' '33' '34' '35' '36' '37' '62' '63' '64' '65' '66' '67' '68' '69' '76' '77' '102' '103' '104' '105' '106' '107' '108' '109' '134' '135' '136' '137' '138' '139' '140' '141' '148' '149' '156' '157'};

% Only make available controls for requested PS's
for iPS=1:length(handles.layout)
  if ismember(iPS,PS.inuse)
    set(handles.(['pushbutton',handles.layout{1,iPS}]),'Visible','on');
    set(handles.(['pushbutton',handles.layout{2,iPS}]),'Visible','on');
    set(handles.(['pushbutton',handles.bulkLayout{1,iPS}]),'Visible','on');
    set(handles.(['pushbutton',handles.bulkLayout{2,iPS}]),'Visible','on');
    fprintf('%d on\n',iPS)
  else
    set(handles.(['pushbutton',handles.layout{1,iPS}]),'Visible','off');
    set(handles.(['pushbutton',handles.layout{2,iPS}]),'Visible','off');
    set(handles.(['pushbutton',handles.bulkLayout{1,iPS}]),'Visible','off');
    set(handles.(['pushbutton',handles.bulkLayout{2,iPS}]),'Visible','off');
    fprintf('%d off\n',iPS)
  end % PS vis
end % for iPS

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes ps_control wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = ps_control_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles;


% --- Executes on button press in pushbutton1.
function pushbutton1_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
launch_psPanel(1);

% Update handles structure
guidata(hObject, handles);

% --- Executes on button press in pushbutton2.
function pushbutton2_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
launch_psPanel(2);

% Update handles structure
guidata(hObject, handles);

% --- Executes on button press in pushbutton3.
function pushbutton3_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
launch_psPanel(3);

% Update handles structure
guidata(hObject, handles);

% --- Executes on button press in pushbutton4.
function pushbutton4_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
launch_psPanel(4);

% Update handles structure
guidata(hObject, handles);

% --- Executes on button press in pushbutton5.
function pushbutton5_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton5 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
launch_psPanel(5);

% Update handles structure
guidata(hObject, handles);

% --- Executes on button press in pushbutton6.
function pushbutton6_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton6 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
launch_psPanel(6);

% Update handles structure
guidata(hObject, handles);

% --- Executes on button press in pushbutton7.
function pushbutton7_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton7 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
launch_psPanel(7);

% Update handles structure
guidata(hObject, handles);

% --- Executes on button press in pushbutton8.
function pushbutton8_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton8 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
launch_psPanel(8);

% Update handles structure
guidata(hObject, handles);

% --- Executes on button press in pushbutton10.
function pushbutton10_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton10 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global PS

for iPS=PS.inuse
  if strcmp(get(handles.(['pushbutton',handles.layout{2,iPS}]),'String'),'On')
    evalc(['pushbutton',handles.layout{2,iPS},'_Callback(handles.pushbutton',handles.layout{2,iPS},',eventdata,handles)']);
  end % if PS on
end % for iPS

% Update handles structure
guidata(hObject, handles);

% --- Executes on button press in pushbutton11.
function pushbutton11_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton11 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global PS

for iPS=PS.inuse
  if strcmp(get(handles.(['pushbutton',handles.layout{2,iPS}]),'String'),'Off')
    evalc(['pushbutton',handles.layout{2,iPS},'_Callback(handles.pushbutton',handles.layout{2,iPS},',eventdata,handles)']);
  end % if PS on
end % for iPS

% Update handles structure
guidata(hObject, handles);

% --- Executes on button press in pushbutton12.
function pushbutton12_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton12 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
if strcmp(get(hObject,'String'),'Off')
  powerToggle('On',1,hObject,handles);
else
  powerToggle('Off',1,hObject,handles);
end % is on or off?
  
% Update handles structure
guidata(hObject, handles);

% --- Executes on button press in pushbutton13.
function pushbutton13_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton13 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
if strcmp(get(hObject,'String'),'Off')
  powerToggle('On',2,hObject,handles);
else
  powerToggle('Off',2,hObject,handles);
end % is on or off?

% Update handles structure
guidata(hObject, handles);

% --- Executes on button press in pushbutton14.
function pushbutton14_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton14 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
if strcmp(get(hObject,'String'),'Off')
  powerToggle('On',3,hObject,handles);
else
  powerToggle('Off',3,hObject,handles);
end % is on or off?

% Update handles structure
guidata(hObject, handles);

% --- Executes on button press in pushbutton15.
function pushbutton15_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton15 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
if strcmp(get(hObject,'String'),'Off')
  powerToggle('On',4,hObject,handles);
else
  powerToggle('Off',4,hObject,handles);
end % is on or off?

% Update handles structure
guidata(hObject, handles);

% --- Executes on button press in pushbutton16.
function pushbutton16_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton16 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
if strcmp(get(hObject,'String'),'Off')
  powerToggle('On',5,hObject,handles);
else
  powerToggle('Off',5,hObject,handles);
end % is on or off?

% Update handles structure
guidata(hObject, handles);

% --- Executes on button press in pushbutton17.
function pushbutton17_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton17 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
if strcmp(get(hObject,'String'),'Off')
  powerToggle('On',6,hObject,handles);
else
  powerToggle('Off',6,hObject,handles);
end % is on or off?

% Update handles structure
guidata(hObject, handles);

% --- Executes on button press in pushbutton18.
function pushbutton18_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton18 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
if strcmp(get(hObject,'String'),'Off')
  powerToggle('On',7,hObject,handles);
else
  powerToggle('Off',7,hObject,handles);
end % is on or off?

% Update handles structure
guidata(hObject, handles);

% --- Executes on button press in pushbutton19.
function pushbutton19_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton19 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
if strcmp(get(hObject,'String'),'Off')
  powerToggle('On',8,hObject,handles);
else
  powerToggle('Off',8,hObject,handles);
end % is on or off?

% Update handles structure
guidata(hObject, handles);

% --- Executes on button press in pushbutton20.
function pushbutton20_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton20 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global PS
if strcmp(get(hObject,'String'),'Monitor Off')
  PS.doMonitor=true;
  set(handles.pushbutton20,'BackgroundColor',PS.guiCol.green);
  set(handles.pushbutton20,'String','Monitor On');
else
  PS.doMonitor=false;
  set(handles.pushbutton20,'BackgroundColor',PS.guiCol.red);
  set(handles.pushbutton20,'String','Monitor Off');
end % if monitor button off

% Update handles structure
guidata(hObject, handles);

% --- Executes on button press in pushbutton21.
function pushbutton21_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton21 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global PS

% Close this and any open gui associated with this
try
  openGui=fieldnames(PS.gui);
  for iField=1:length(openGui)
    if ~strcmp(openGui{iField},'main')
      delete(PS.gui.(openGui{iField}).figure1);
      PS.gui = rmfield(PS.gui,openGui{iField});
    end % if not main gui
  end % for iField
  delete(PS.gui.main.figure1);
  PS.gui = rmfield(PS.gui,'main');
  exit
catch
  delete(gcf)
  rethrow(lasterror)
end % try/catch

function powerToggle(onoff, iPS, hObject, handles)
global PS
if ~PS.stat.lmode(PS.ptr==iPS)
  switch onoff
    case 'Off'
      % First set current -> 0 if not already
      lcaput(PS.cmd.curSet{PS.ptr==iPS},0);
      lcaput(PS.cmd.ramp{PS.ptr==iPS},PS.cmd.rampVal(PS.ptr==iPS));
      % Wait for ramp to finish
      pause(lcaget(PS.cmd.tRamp(PS.ptr==iPS))*0.01);
      pause(0.1);
      % Update PS panel if open
      psGui=['ps_panel',num2str(iPS)];
      if isfield(PS.gui,psGui)
        % Fix case where gui window killed but gui flag still there
        isReallyThere=true;
        try
          findobj(PS.gui.(psGui).figure1);
        catch
          PS.gui=rmfield(PS.gui,psGui);
          isReallyThere=false;
        end % try/catch
        if isReallyThere
          % Set current box -> 0
          set(PS.gui.(['ps_panel',num2str(iPS)]).edit1,'String','0.000');
        end % if isReallyThere
      end % if PS gui panel open
      % Switch off command
      lcaput(PS.cmd.pwrOff{PS.ptr==iPS},PS.cmd.pwrOffVal(PS.ptr==iPS));
      if PS.simMode.val
        set(hObject,'BackgroundColor',PS.guiCol.red);
        set(hObject,'String','Off');
      end % if simMode
      % Update PS panel if open
      psGui=['ps_panel',num2str(iPS)];
      if isfield(PS.gui,psGui)
        % Fix case where gui window killed but gui flag still there
        isReallyThere=true;
        try
          findobj(PS.gui.(psGui).figure1);
        catch
          PS.gui=rmfield(PS.gui,psGui);
          isReallyThere=false;
        end % try/catch
        if isReallyThere && PS.simMode.val
          set(PS.gui.(psGui).pushbutton10,'BackgroundColor',PS.guiCol.red);
          set(PS.gui.(psGui).pushbutton10,'String','Power Off');
        end % if isReallyThere
      end % if PS gui panel open
    case 'On'
      lcaput(PS.cmd.pwrOn{PS.ptr==iPS},PS.cmd.pwrOnVal(PS.ptr==iPS));
      if PS.simMode.val
        set(hObject,'BackgroundColor',PS.guiCol.green);
        set(hObject,'String','On');
      end % if simMode
      % Update PS panel if open
      psGui=['ps_panel',num2str(iPS)];
      if isfield(PS.gui,psGui)
        % Fix case where gui window killed but gui flag still there
        isReallyThere=true;
        try
          findobj(PS.gui.(psGui).figure1);
        catch
          PS.gui=rmfield(PS.gui,psGui);
          isReallyThere=false;
        end % try/catch
        if isReallyThere && PS.simMode.val
          set(PS.gui.(psGui).pushbutton10,'BackgroundColor',PS.guiCol.green);
          set(PS.gui.(psGui).pushbutton10,'String','Power On');
        end % if isReallyThere
      end % if PS gui panel open
  end % switch onoff
end % if ~ local mode
% Update handles structure
guidata(hObject, handles);

function launch_psPanel(iPS)
global PS

% Lauch PS Panel Gui
if ~isfield(PS.gui,['ps_panel',num2str(iPS)])
  PS.gui.(['ps_panel',num2str(iPS)]) = ps_panel(iPS);
else
  figure(PS.gui.(['ps_panel',num2str(iPS)]).figure1);
end % if panel already exists

% Update fields
ps_stat(find(PS.ptr==iPS),1);


% --- Executes on button press in pushbutton22.
function pushbutton22_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton22 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
launch_psPanel(1);

% --- Executes on button press in pushbutton23.
function pushbutton23_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton23 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
launch_psPanel(2);

% --- Executes on button press in pushbutton24.
function pushbutton24_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton24 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
launch_psPanel(3);

% --- Executes on button press in pushbutton25.
function pushbutton25_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton25 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
launch_psPanel(4);

% --- Executes on button press in pushbutton26.
function pushbutton26_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton26 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
launch_psPanel(5);

% --- Executes on button press in pushbutton27.
function pushbutton27_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton27 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
launch_psPanel(6);

% --- Executes on button press in pushbutton28.
function pushbutton28_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton28 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
launch_psPanel(7);

% --- Executes on button press in pushbutton29.
function pushbutton29_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton29 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
launch_psPanel(8);

% --- Executes on button press in pushbutton30.
function pushbutton30_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton30 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
if strcmp(get(hObject,'String'),'Off')
  powerToggleBulk('On',1,hObject,handles);
else
  powerToggleBulk('Off',1,hObject,handles);
end % is on or off?

% --- Executes on button press in pushbutton31.
function pushbutton31_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton31 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
if strcmp(get(hObject,'String'),'Off')
  powerToggleBulk('On',2,hObject,handles);
else
  powerToggleBulk('Off',2,hObject,handles);
end % is on or off?

% --- Executes on button press in pushbutton32.
function pushbutton32_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton32 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
if strcmp(get(hObject,'String'),'Off')
  powerToggleBulk('On',3,hObject,handles);
else
  powerToggleBulk('Off',3,hObject,handles);
end % is on or off?

% --- Executes on button press in pushbutton33.
function pushbutton33_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton33 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
if strcmp(get(hObject,'String'),'Off')
  powerToggleBulk('On',4,hObject,handles);
else
  powerToggleBulk('Off',4,hObject,handles);
end % is on or off?

% --- Executes on button press in pushbutton34.
function pushbutton34_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton34 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
if strcmp(get(hObject,'String'),'Off')
  powerToggleBulk('On',5,hObject,handles);
else
  powerToggleBulk('Off',5,hObject,handles);
end % is on or off?

% --- Executes on button press in pushbutton35.
function pushbutton35_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton35 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
if strcmp(get(hObject,'String'),'Off')
  powerToggleBulk('On',6,hObject,handles);
else
  powerToggleBulk('Off',6,hObject,handles);
end % is on or off?

% --- Executes on button press in pushbutton36.
function pushbutton36_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton36 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
if strcmp(get(hObject,'String'),'Off')
  powerToggleBulk('On',7,hObject,handles);
else
  powerToggleBulk('Off',7,hObject,handles);
end % is on or off?

% --- Executes on button press in pushbutton37.
function pushbutton37_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton37 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
if strcmp(get(hObject,'String'),'Off')
  powerToggleBulk('On',8,hObject,handles);
else
  powerToggleBulk('Off',8,hObject,handles);
end % is on or off?

function powerToggleBulk(onOff,iPS,hObject,handles)
global PS
switch onOff
  case 'On'
    lcaput(PS.cmd.bulkPwr{find(PS.ptr==iPS)},1);
    if PS.simMode.val
      set(hObject,'BackgroundColor',PS.guiCol.green);
      set(hObject,'String','On');
    end % if simMode
  case 'Off'
    lcaput(PS.cmd.bulkPwr{find(PS.ptr==iPS)},0);
    if PS.simMode.val
      set(hObject,'BackgroundColor',PS.guiCol.red);
      set(hObject,'String','Off');
    end % if simMode
end % switch onOff


% --- Executes on button press in pushbutton38.
function pushbutton38_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton38 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
launch_psPanel(9);

% --- Executes on button press in pushbutton39.
function pushbutton39_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton39 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
launch_psPanel(10);

% --- Executes on button press in pushbutton40.
function pushbutton40_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton40 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
launch_psPanel(11);

% --- Executes on button press in pushbutton41.
function pushbutton41_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton41 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
launch_psPanel(12);

% --- Executes on button press in pushbutton42.
function pushbutton42_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton42 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
launch_psPanel(13);

% --- Executes on button press in pushbutton43.
function pushbutton43_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton43 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
launch_psPanel(14);

% --- Executes on button press in pushbutton44.
function pushbutton44_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton44 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
launch_psPanel(15);

% --- Executes on button press in pushbutton45.
function pushbutton45_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton45 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
launch_psPanel(16);

% --- Executes on button press in pushbutton46.
function pushbutton46_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton46 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
if strcmp(get(hObject,'String'),'Off')
  powerToggle('On',9,hObject,handles);
else
  powerToggle('Off',9,hObject,handles);
end % is on or off?

% --- Executes on button press in pushbutton47.
function pushbutton47_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton47 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
if strcmp(get(hObject,'String'),'Off')
  powerToggle('On',10,hObject,handles);
else
  powerToggle('Off',10,hObject,handles);
end % is on or off?

% --- Executes on button press in pushbutton48.
function pushbutton48_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton48 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
if strcmp(get(hObject,'String'),'Off')
  powerToggle('On',11,hObject,handles);
else
  powerToggle('Off',11,hObject,handles);
end % is on or off?

% --- Executes on button press in pushbutton49.
function pushbutton49_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton49 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
if strcmp(get(hObject,'String'),'Off')
  powerToggle('On',12,hObject,handles);
else
  powerToggle('Off',12,hObject,handles);
end % is on or off?

% --- Executes on button press in pushbutton50.
function pushbutton50_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton50 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
if strcmp(get(hObject,'String'),'Off')
  powerToggle('On',13,hObject,handles);
else
  powerToggle('Off',13,hObject,handles);
end % is on or off?

% --- Executes on button press in pushbutton51.
function pushbutton51_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton51 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
if strcmp(get(hObject,'String'),'Off')
  powerToggle('On',14,hObject,handles);
else
  powerToggle('Off',14,hObject,handles);
end % is on or off?

% --- Executes on button press in pushbutton52.
function pushbutton52_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton52 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
if strcmp(get(hObject,'String'),'Off')
  powerToggle('On',15,hObject,handles);
else
  powerToggle('Off',15,hObject,handles);
end % is on or off?

% --- Executes on button press in pushbutton53.
function pushbutton53_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton53 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
if strcmp(get(hObject,'String'),'Off')
  powerToggle('On',16,hObject,handles);
else
  powerToggle('Off',16,hObject,handles);
end % is on or off?

% --- Executes on button press in pushbutton54.
function pushbutton54_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton54 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
launch_psPanel(9);

% --- Executes on button press in pushbutton55.
function pushbutton55_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton55 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
launch_psPanel(10);

% --- Executes on button press in pushbutton56.
function pushbutton56_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton56 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
launch_psPanel(11);

% --- Executes on button press in pushbutton57.
function pushbutton57_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton57 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
launch_psPanel(12);

% --- Executes on button press in pushbutton58.
function pushbutton58_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton58 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
launch_psPanel(13);

% --- Executes on button press in pushbutton59.
function pushbutton59_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton59 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
launch_psPanel(14);

% --- Executes on button press in pushbutton60.
function pushbutton60_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton60 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
launch_psPanel(15);

% --- Executes on button press in pushbutton61.
function pushbutton61_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton61 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
launch_psPanel(16);

% --- Executes on button press in pushbutton62.
function pushbutton62_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton62 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
if strcmp(get(hObject,'String'),'Off')
  powerToggleBulk('On',9,hObject,handles);
else
  powerToggleBulk('Off',9,hObject,handles);
end % is on or off?

% --- Executes on button press in pushbutton63.
function pushbutton63_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton63 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
if strcmp(get(hObject,'String'),'Off')
  powerToggleBulk('On',10,hObject,handles);
else
  powerToggleBulk('Off',10,hObject,handles);
end % is on or off?

% --- Executes on button press in pushbutton64.
function pushbutton64_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton64 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
if strcmp(get(hObject,'String'),'Off')
  powerToggleBulk('On',11,hObject,handles);
else
  powerToggleBulk('Off',11,hObject,handles);
end % is on or off?

% --- Executes on button press in pushbutton65.
function pushbutton65_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton65 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
if strcmp(get(hObject,'String'),'Off')
  powerToggleBulk('On',12,hObject,handles);
else
  powerToggleBulk('Off',12,hObject,handles);
end % is on or off?

% --- Executes on button press in pushbutton66.
function pushbutton66_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton66 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
if strcmp(get(hObject,'String'),'Off')
  powerToggleBulk('On',13,hObject,handles);
else
  powerToggleBulk('Off',13,hObject,handles);
end % is on or off?

% --- Executes on button press in pushbutton67.
function pushbutton67_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton67 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
if strcmp(get(hObject,'String'),'Off')
  powerToggleBulk('On',14,hObject,handles);
else
  powerToggleBulk('Off',14,hObject,handles);
end % is on or off?

% --- Executes on button press in pushbutton68.
function pushbutton68_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton68 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
if strcmp(get(hObject,'String'),'Off')
  powerToggleBulk('On',15,hObject,handles);
else
  powerToggleBulk('Off',15,hObject,handles);
end % is on or off?

% --- Executes on button press in pushbutton69.
function pushbutton69_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton69 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
if strcmp(get(hObject,'String'),'Off')
  powerToggleBulk('On',16,hObject,handles);
else
  powerToggleBulk('Off',16,hObject,handles);
end % is on or off?

% --- Executes on button press in pushbutton70.
function pushbutton70_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton70 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
launch_psPanel(17);

% --- Executes on button press in pushbutton71.
function pushbutton71_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton71 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
launch_psPanel(18);

% --- Executes on button press in pushbutton72.
function pushbutton72_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton72 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
if strcmp(get(hObject,'String'),'Off')
  powerToggle('On',17,hObject,handles);
else
  powerToggle('Off',17,hObject,handles);
end % is on or off?

% --- Executes on button press in pushbutton73.
function pushbutton73_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton73 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
if strcmp(get(hObject,'String'),'Off')
  powerToggle('On',18,hObject,handles);
else
  powerToggle('Off',18,hObject,handles);
end % is on or off?

% --- Executes on button press in pushbutton74.
function pushbutton74_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton74 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
launch_psPanel(17);

% --- Executes on button press in pushbutton75.
function pushbutton75_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton75 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
launch_psPanel(18);

% --- Executes on button press in pushbutton76.
function pushbutton76_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton76 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
if strcmp(get(hObject,'String'),'Off')
  powerToggleBulk('On',17,hObject,handles);
else
  powerToggleBulk('Off',17,hObject,handles);
end % is on or off?

% --- Executes on button press in pushbutton77.
function pushbutton77_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton77 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
if strcmp(get(hObject,'String'),'Off')
  powerToggleBulk('On',18,hObject,handles);
else
  powerToggleBulk('Off',18,hObject,handles);
end % is on or off?

% --- Executes on button press in pushbutton78.
function pushbutton78_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton78 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
launch_psPanel(19);

% --- Executes on button press in pushbutton79.
function pushbutton79_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton79 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
launch_psPanel(20);

% --- Executes on button press in pushbutton80.
function pushbutton80_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton80 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
launch_psPanel(21);

% --- Executes on button press in pushbutton81.
function pushbutton81_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton81 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
launch_psPanel(22);

% --- Executes on button press in pushbutton82.
function pushbutton82_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton82 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
launch_psPanel(23);

% --- Executes on button press in pushbutton83.
function pushbutton83_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton83 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
launch_psPanel(24);

% --- Executes on button press in pushbutton84.
function pushbutton84_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton84 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
launch_psPanel(25);

% --- Executes on button press in pushbutton85.
function pushbutton85_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton85 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
launch_psPanel(26);

% --- Executes on button press in pushbutton86.
function pushbutton86_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton86 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
if strcmp(get(hObject,'String'),'Off')
  powerToggle('On',19,hObject,handles);
else
  powerToggle('Off',19,hObject,handles);
end % is on or off?

% --- Executes on button press in pushbutton87.
function pushbutton87_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton87 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
if strcmp(get(hObject,'String'),'Off')
  powerToggle('On',20,hObject,handles);
else
  powerToggle('Off',20,hObject,handles);
end % is on or off?

% --- Executes on button press in pushbutton88.
function pushbutton88_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton88 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
if strcmp(get(hObject,'String'),'Off')
  powerToggle('On',21,hObject,handles);
else
  powerToggle('Off',21,hObject,handles);
end % is on or off?

% --- Executes on button press in pushbutton89.
function pushbutton89_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton89 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
if strcmp(get(hObject,'String'),'Off')
  powerToggle('On',22,hObject,handles);
else
  powerToggle('Off',22,hObject,handles);
end % is on or off?

% --- Executes on button press in pushbutton90.
function pushbutton90_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton90 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
if strcmp(get(hObject,'String'),'Off')
  powerToggle('On',23,hObject,handles);
else
  powerToggle('Off',23,hObject,handles);
end % is on or off?

% --- Executes on button press in pushbutton91.
function pushbutton91_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton91 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
if strcmp(get(hObject,'String'),'Off')
  powerToggle('On',24,hObject,handles);
else
  powerToggle('Off',24,hObject,handles);
end % is on or off?

% --- Executes on button press in pushbutton92.
function pushbutton92_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton92 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
if strcmp(get(hObject,'String'),'Off')
  powerToggle('On',25,hObject,handles);
else
  powerToggle('Off',25,hObject,handles);
end % is on or off?

% --- Executes on button press in pushbutton93.
function pushbutton93_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton93 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
if strcmp(get(hObject,'String'),'Off')
  powerToggle('On',26,hObject,handles);
else
  powerToggle('Off',26,hObject,handles);
end % is on or off?

% --- Executes on button press in pushbutton94.
function pushbutton94_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton94 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
launch_psPanel(19);

% --- Executes on button press in pushbutton95.
function pushbutton95_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton95 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
launch_psPanel(20);

% --- Executes on button press in pushbutton96.
function pushbutton96_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton96 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
launch_psPanel(21);

% --- Executes on button press in pushbutton97.
function pushbutton97_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton97 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
launch_psPanel(22);

% --- Executes on button press in pushbutton98.
function pushbutton98_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton98 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
launch_psPanel(23);

% --- Executes on button press in pushbutton99.
function pushbutton99_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton99 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
launch_psPanel(24);

% --- Executes on button press in pushbutton100.
function pushbutton100_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton100 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
launch_psPanel(25);

% --- Executes on button press in pushbutton101.
function pushbutton101_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton101 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
launch_psPanel(26);

% --- Executes on button press in pushbutton102.
function pushbutton102_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton102 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
if strcmp(get(hObject,'String'),'Off')
  powerToggleBulk('On',19,hObject,handles);
else
  powerToggleBulk('Off',19,hObject,handles);
end % is on or off?

% --- Executes on button press in pushbutton103.
function pushbutton103_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton103 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
if strcmp(get(hObject,'String'),'Off')
  powerToggleBulk('On',20,hObject,handles);
else
  powerToggleBulk('Off',20,hObject,handles);
end % is on or off?

% --- Executes on button press in pushbutton104.
function pushbutton104_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton104 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
if strcmp(get(hObject,'String'),'Off')
  powerToggleBulk('On',21,hObject,handles);
else
  powerToggleBulk('Off',21,hObject,handles);
end % is on or off?

% --- Executes on button press in pushbutton105.
function pushbutton105_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton105 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
if strcmp(get(hObject,'String'),'Off')
  powerToggleBulk('On',22,hObject,handles);
else
  powerToggleBulk('Off',22,hObject,handles);
end % is on or off?

% --- Executes on button press in pushbutton106.
function pushbutton106_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton106 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
if strcmp(get(hObject,'String'),'Off')
  powerToggleBulk('On',23,hObject,handles);
else
  powerToggleBulk('Off',23,hObject,handles);
end % is on or off?

% --- Executes on button press in pushbutton107.
function pushbutton107_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton107 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
if strcmp(get(hObject,'String'),'Off')
  powerToggleBulk('On',24,hObject,handles);
else
  powerToggleBulk('Off',24,hObject,handles);
end % is on or off?

% --- Executes on button press in pushbutton108.
function pushbutton108_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton108 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
if strcmp(get(hObject,'String'),'Off')
  powerToggleBulk('On',25,hObject,handles);
else
  powerToggleBulk('Off',25,hObject,handles);
end % is on or off?

% --- Executes on button press in pushbutton109.
function pushbutton109_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton109 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
if strcmp(get(hObject,'String'),'Off')
  powerToggleBulk('On',26,hObject,handles);
else
  powerToggleBulk('Off',26,hObject,handles);
end % is on or off?

% --- Executes on button press in pushbutton110.
function pushbutton110_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton110 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
launch_psPanel(27);

% --- Executes on button press in pushbutton111.
function pushbutton111_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton111 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
launch_psPanel(28);

% --- Executes on button press in pushbutton112.
function pushbutton112_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton112 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
launch_psPanel(29);

% --- Executes on button press in pushbutton113.
function pushbutton113_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton113 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
launch_psPanel(30);

% --- Executes on button press in pushbutton114.
function pushbutton114_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton114 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
launch_psPanel(31);

% --- Executes on button press in pushbutton115.
function pushbutton115_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton115 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
launch_psPanel(32);

% --- Executes on button press in pushbutton116.
function pushbutton116_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton116 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
launch_psPanel(33);

% --- Executes on button press in pushbutton117.
function pushbutton117_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton117 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
launch_psPanel(34);

% --- Executes on button press in pushbutton118.
function pushbutton118_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton118 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
if strcmp(get(hObject,'String'),'Off')
  powerToggle('On',27,hObject,handles);
else
  powerToggle('Off',27,hObject,handles);
end % is on or off?

% --- Executes on button press in pushbutton119.
function pushbutton119_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton119 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
if strcmp(get(hObject,'String'),'Off')
  powerToggle('On',28,hObject,handles);
else
  powerToggle('Off',28,hObject,handles);
end % is on or off?

% --- Executes on button press in pushbutton120.
function pushbutton120_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton120 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
if strcmp(get(hObject,'String'),'Off')
  powerToggle('On',29,hObject,handles);
else
  powerToggle('Off',29,hObject,handles);
end % is on or off?

% --- Executes on button press in pushbutton121.
function pushbutton121_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton121 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
if strcmp(get(hObject,'String'),'Off')
  powerToggle('On',30,hObject,handles);
else
  powerToggle('Off',30,hObject,handles);
end % is on or off?

% --- Executes on button press in pushbutton122.
function pushbutton122_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton122 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
if strcmp(get(hObject,'String'),'Off')
  powerToggle('On',31,hObject,handles);
else
  powerToggle('Off',31,hObject,handles);
end % is on or off?

% --- Executes on button press in pushbutton123.
function pushbutton123_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton123 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
if strcmp(get(hObject,'String'),'Off')
  powerToggle('On',32,hObject,handles);
else
  powerToggle('Off',32,hObject,handles);
end % is on or off?

% --- Executes on button press in pushbutton124.
function pushbutton124_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton124 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
if strcmp(get(hObject,'String'),'Off')
  powerToggle('On',33,hObject,handles);
else
  powerToggle('Off',33,hObject,handles);
end % is on or off?

% --- Executes on button press in pushbutton125.
function pushbutton125_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton125 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
if strcmp(get(hObject,'String'),'Off')
  powerToggle('On',34,hObject,handles);
else
  powerToggle('Off',34,hObject,handles);
end % is on or off?

% --- Executes on button press in pushbutton126.
function pushbutton126_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton126 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
launch_psPanel(27);

% --- Executes on button press in pushbutton127.
function pushbutton127_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton127 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
launch_psPanel(28);

% --- Executes on button press in pushbutton128.
function pushbutton128_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton128 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
launch_psPanel(29);

% --- Executes on button press in pushbutton129.
function pushbutton129_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton129 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
launch_psPanel(30);

% --- Executes on button press in pushbutton130.
function pushbutton130_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton130 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
launch_psPanel(31);

% --- Executes on button press in pushbutton131.
function pushbutton131_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton131 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
launch_psPanel(32);

% --- Executes on button press in pushbutton132.
function pushbutton132_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton132 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
launch_psPanel(33);

% --- Executes on button press in pushbutton133.
function pushbutton133_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton133 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
launch_psPanel(34);

% --- Executes on button press in pushbutton134.
function pushbutton134_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton134 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
if strcmp(get(hObject,'String'),'Off')
  powerToggleBulk('On',27,hObject,handles);
else
  powerToggleBulk('Off',27,hObject,handles);
end % is on or off?

% --- Executes on button press in pushbutton135.
function pushbutton135_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton135 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
if strcmp(get(hObject,'String'),'Off')
  powerToggleBulk('On',28,hObject,handles);
else
  powerToggleBulk('Off',28,hObject,handles);
end % is on or off?

% --- Executes on button press in pushbutton136.
function pushbutton136_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton136 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
if strcmp(get(hObject,'String'),'Off')
  powerToggleBulk('On',29,hObject,handles);
else
  powerToggleBulk('Off',29,hObject,handles);
end % is on or off?

% --- Executes on button press in pushbutton137.
function pushbutton137_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton137 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
if strcmp(get(hObject,'String'),'Off')
  powerToggleBulk('On',30,hObject,handles);
else
  powerToggleBulk('Off',30,hObject,handles);
end % is on or off?

% --- Executes on button press in pushbutton138.
function pushbutton138_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton138 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
if strcmp(get(hObject,'String'),'Off')
  powerToggleBulk('On',31,hObject,handles);
else
  powerToggleBulk('Off',31,hObject,handles);
end % is on or off?

% --- Executes on button press in pushbutton139.
function pushbutton139_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton139 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
if strcmp(get(hObject,'String'),'Off')
  powerToggleBulk('On',32,hObject,handles);
else
  powerToggleBulk('Off',32,hObject,handles);
end % is on or off?

% --- Executes on button press in pushbutton140.
function pushbutton140_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton140 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
if strcmp(get(hObject,'String'),'Off')
  powerToggleBulk('On',33,hObject,handles);
else
  powerToggleBulk('Off',33,hObject,handles);
end % is on or off?

% --- Executes on button press in pushbutton141.
function pushbutton141_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton141 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
if strcmp(get(hObject,'String'),'Off')
  powerToggleBulk('On',34,hObject,handles);
else
  powerToggleBulk('Off',34,hObject,handles);
end % is on or off?

% --- Executes on button press in pushbutton142.
function pushbutton142_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton142 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
launch_psPanel(35);

% --- Executes on button press in pushbutton143.
function pushbutton143_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton143 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
launch_psPanel(36);

% --- Executes on button press in pushbutton144.
function pushbutton144_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton144 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
if strcmp(get(hObject,'String'),'Off')
  powerToggle('On',35,hObject,handles);
else
  powerToggle('Off',35,hObject,handles);
end % is on or off?

% --- Executes on button press in pushbutton145.
function pushbutton145_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton145 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
if strcmp(get(hObject,'String'),'Off')
  powerToggle('On',36,hObject,handles);
else
  powerToggle('Off',36,hObject,handles);
end % is on or off?

% --- Executes on button press in pushbutton146.
function pushbutton146_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton146 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
launch_psPanel(35);

% --- Executes on button press in pushbutton147.
function pushbutton147_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton147 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
launch_psPanel(36);

% --- Executes on button press in pushbutton148.
function pushbutton148_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton148 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
if strcmp(get(hObject,'String'),'Off')
  powerToggleBulk('On',35,hObject,handles);
else
  powerToggleBulk('Off',35,hObject,handles);
end % is on or off?

% --- Executes on button press in pushbutton149.
function pushbutton149_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton149 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
if strcmp(get(hObject,'String'),'Off')
  powerToggleBulk('On',36,hObject,handles);
else
  powerToggleBulk('Off',36,hObject,handles);
end % is on or off?

% --- Executes on button press in pushbutton150.
function pushbutton150_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton150 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
launch_psPanel(37);

% --- Executes on button press in pushbutton151.
function pushbutton151_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton151 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
launch_psPanel(38);

% --- Executes on button press in pushbutton152.
function pushbutton152_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton152 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
if strcmp(get(hObject,'String'),'Off')
  powerToggle('On',37,hObject,handles);
else
  powerToggle('Off',37,hObject,handles);
end % is on or off?

% --- Executes on button press in pushbutton153.
function pushbutton153_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton153 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
if strcmp(get(hObject,'String'),'Off')
  powerToggle('On',38,hObject,handles);
else
  powerToggle('Off',38,hObject,handles);
end % is on or off?

% --- Executes on button press in pushbutton154.
function pushbutton154_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton154 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
launch_psPanel(37);

% --- Executes on button press in pushbutton155.
function pushbutton155_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton155 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
launch_psPanel(38);

% --- Executes on button press in pushbutton156.
function pushbutton156_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton156 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
if strcmp(get(hObject,'String'),'Off')
  powerToggleBulk('On',37,hObject,handles);
else
  powerToggleBulk('Off',37,hObject,handles);
end % is on or off?

% --- Executes on button press in pushbutton157.
function pushbutton157_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton157 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
if strcmp(get(hObject,'String'),'Off')
  powerToggleBulk('On',38,hObject,handles);
else
  powerToggleBulk('Off',38,hObject,handles);
end % is on or off?


curDir=pwd;
atfDir=fullfile(curDir,'..','..','..','..','ATF2');
epicsHost=getenv('EPICS_HOST_ARCH');
if ispc
  labcaDir=fullfile(atfDir,'control-software','epics-3.14.8','support','labca_3_0_beta','bin','win32-x86','labca');
else
  labcaDir=fullfile(atfDir,'control-software','epics-3.14.8','support','labca_3_1','bin',epicsHost,'labca');
end
addpath(labcaDir)
function varargout = ps_panel(varargin)
% PS_PANEL M-file for ps_panel.fig
%      PS_PANEL, by itself, creates a new PS_PANEL or raises the existing
%      singleton*.
%
%      H = PS_PANEL returns the handle to a new PS_PANEL or the handle to
%      the existing singleton*.
%
%      PS_PANEL('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in PS_PANEL.M with the given input arguments.
%
%      PS_PANEL('Property','Value',...) creates a new PS_PANEL or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before ps_panel_OpeningFunction gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to ps_panel_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help ps_panel

% Last Modified by GUIDE v2.5 25-Sep-2007 15:28:22

% Begin initialization code - DO NOT EDIT
gui_Singleton = 0;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @ps_panel_OpeningFcn, ...
                   'gui_OutputFcn',  @ps_panel_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before ps_panel is made visible.
function ps_panel_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to ps_panel (see VARARGIN)
global PS %#ok<NUSED>

% Choose default command line output for ps_panel
% handles.output = hObject;

% Flag PS number
handles.thisPS = varargin{1};

% Set PS label in gui
set(handles.text1,'String',['PS ',num2str(handles.thisPS)]);

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes ps_panel wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = ps_panel_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles;



function edit1_Callback(hObject, eventdata, handles)
% hObject    handle to edit1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit1 as text
%        str2double(get(hObject,'String')) returns contents of edit1 as a double
global PS

% Write value to EPICS DB
lcaput(PS.cmd.curSet{PS.ptr==handles.thisPS},str2double(get(hObject,'String')));

% Update handles structure
guidata(hObject, handles);

% Renew status
ps_stat(find(PS.ptr==handles.thisPS));


% --- Executes during object creation, after setting all properties.
function edit1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit2_Callback(hObject, eventdata, handles)
% hObject    handle to edit2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit2 as text
%        str2double(get(hObject,'String')) returns contents of edit2 as a double
global PS

% Write value to EPICS DB
lcaput(PS.cmd.curStore(PS.ptr==handles.thisPS),str2double(get(hObject,'String')));

% Update handles structure
guidata(hObject, handles);

% Renew status
ps_stat(find(PS.ptr==handles.thisPS));


% --- Executes during object creation, after setting all properties.
function edit2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in pushbutton1.
function pushbutton1_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global PS

% Send set current command to EPSC
lcaput(PS.cmd.ramp{PS.ptr==handles.thisPS},PS.cmd.rampVal(PS.ptr==handles.thisPS));

% Update handles structure
guidata(hObject, handles);

% Renew status
ps_stat(find(PS.ptr==handles.thisPS));

% --- Executes on button press in pushbutton2.
function pushbutton2_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global PS

% Standardisation procedure
iLow=lcaget(PS.cmd.sd_iLow{PS.ptr==handles.thisPS});
iHigh=lcaget(PS.cmd.sd_iHigh{PS.ptr==handles.thisPS});
waitLow=lcaget(PS.cmd.sd_waitLow{PS.ptr==handles.thisPS});
waitHigh=lcaget(PS.cmd.sd_waitHigh{PS.ptr==handles.thisPS});
nCycles=lcaget(PS.cmd.sd_nCycles{PS.ptr==handles.thisPS});
currents1 = [ iHigh iLow];
waitTimes1 = [waitHigh waitLow];
currents=[]; waitTimes=[];
for iCycle=1:nCycles
  currents=[currents currents1]; %#ok<AGROW>
  waitTimes=[waitTimes waitTimes1]; %#ok<AGROW>
end % for iCycle
% put at low value
setCurrent(iLow,handles);
pause(waitLow);
for iCycle=1:nCycles
  setCurrent(iHigh,handles);
  pause(waitHigh);
  setCurrent(iLow,handles);
  pause(waitLow);
end

function setCurrent(I,handles)
global PS

% Set current value on database
lcaput(PS.cmd.curSet{PS.ptr==handles.thisPS},I);
% Ramp
lcaput(PS.cmd.ramp{PS.ptr==handles.thisPS},PS.cmd.rampVal(PS.ptr==handles.thisPS));
% Get current readback value and wait for the required time
readI=-1e10;
while readI<I*0.9 || readI>I*1.1
  readI=lcaget(PS.cmd.curRead{PS.ptr==handles.thisPS});
  set(handles.text2,'String',sprintf('%.3f',readI));
  % Update GUI
  drawnow;
  pause(0.5);
end % while still ramping

% --- Executes on button press in pushbutton3.
function pushbutton3_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global PS

% Set Store value to set value
lcaput(PS.cmd.curSet{PS.ptr==handles.thisPS},str2double(get(handles.edit2,'String')));
set(handles.edit1,'String',get(handles.edit2,'String'));

% Send set current command to EPSC
lcaput(PS.cmd.ramp{PS.ptr==handles.thisPS},PS.cmd.rampVal(PS.ptr==handles.thisPS));

% Update handles structure
guidata(hObject, handles);

% Renew status
ps_stat(find(PS.ptr==handles.thisPS));

% --- Executes on button press in pushbutton4.
function pushbutton4_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global PS

% Launch Standardisation edit GUI
PS.gui.(['psStandardisation_panel',num2str(handles.thisPS)]) = ps_standardisation(handles.thisPS);



function edit3_Callback(hObject, eventdata, handles)
% hObject    handle to edit3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit3 as text
%        str2double(get(hObject,'String')) returns contents of edit3 as a double
global PS

% Write value to EPICS DB
lcaput(PS.cmd.tRamp(PS.ptr==handles.thisPS),str2double(get(hObject,'String')));

% Update handles structure
guidata(hObject, handles);

% Renew status
ps_stat(find(PS.ptr==handles.thisPS));

% --- Executes during object creation, after setting all properties.
function edit3_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in radiobutton1.
function radiobutton1_Callback(hObject, eventdata, handles)
% hObject    handle to radiobutton1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of radiobutton1


% --- Executes on button press in radiobutton2.
function radiobutton2_Callback(hObject, eventdata, handles)
% hObject    handle to radiobutton2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of radiobutton2


% --- Executes on button press in radiobutton3.
function radiobutton3_Callback(hObject, eventdata, handles)
% hObject    handle to radiobutton3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of radiobutton3


% --- Executes on button press in radiobutton4.
function radiobutton4_Callback(hObject, eventdata, handles)
% hObject    handle to radiobutton4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of radiobutton4


% --- Executes on button press in radiobutton5.
function radiobutton5_Callback(hObject, eventdata, handles)
% hObject    handle to radiobutton5 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of radiobutton5


% --- Executes on button press in radiobutton6.
function radiobutton6_Callback(hObject, eventdata, handles)
% hObject    handle to radiobutton6 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of radiobutton6


% --- Executes on button press in radiobutton7.
function radiobutton7_Callback(hObject, eventdata, handles)
% hObject    handle to radiobutton7 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of radiobutton7


% --- Executes on button press in radiobutton8.
function radiobutton8_Callback(hObject, eventdata, handles)
% hObject    handle to radiobutton8 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of radiobutton8


% --- Executes on button press in radiobutton9.
function radiobutton9_Callback(hObject, eventdata, handles)
% hObject    handle to radiobutton9 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of radiobutton9


% --- Executes on button press in radiobutton10.
function radiobutton10_Callback(hObject, eventdata, handles)
% hObject    handle to radiobutton10 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of radiobutton10


% --- Executes on button press in pushbutton5.
function pushbutton5_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton5 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global PS

% Send interlock reset command
lcaput(PS.cmd.resetInterlocks{PS.ptr==handles.thisPS},PS.cmd.resetInterlocksVal(PS.ptr==handles.thisPS));

% Update handles structure
guidata(hObject, handles);

% Renew status
ps_stat(find(PS.ptr==handles.thisPS));


% --- Executes on button press in pushbutton6.
function pushbutton6_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton6 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global PS

% Send controller reset command
lcaput(PS.cmd.resetController{PS.ptr==handles.thisPS},PS.cmd.resetControllerVal(PS.ptr==handles.thisPS));

% Update handles structure
guidata(hObject, handles);

% Renew status
ps_stat(find(PS.ptr==handles.thisPS));

% --- Executes on button press in pushbutton7.
function pushbutton7_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton7 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global PS

% Renew status
ps_stat(find(PS.ptr==handles.thisPS));

% --- Executes on button press in pushbutton8.
function pushbutton8_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton8 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on button press in pushbutton10.
function pushbutton10_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton10 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global PS

% Push Power button on main gui panel for this PS
ps_control(['pushbutton',PS.gui.main.layout{2,handles.thisPS},'_Callback'], ...
  PS.gui.main.(['pushbutton',PS.gui.main.layout{2,handles.thisPS}]), [], PS.gui.main) ;

% Update handles structure
guidata(hObject, handles);

% --- Executes on button press in pushbutton11.
function pushbutton11_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton11 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global PS
try
   while PS.isupdating
    disp('Waiting for panel to stop processing before shutting down...');
    pause(1);
  end % while updating
  delete(PS.gui.(['ps_panel',num2str(handles.thisPS)]).figure1);
  PS.gui=rmfield(PS.gui,['ps_panel',num2str(handles.thisPS)]);
catch
  delete(gcf);
end

% --- Executes on button press in pushbutton12.
function pushbutton12_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton12 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global PS

% Launch Module GUI
PS.gui.(['psModule_panel',num2str(handles.thisPS)]) = psModule_panel(handles.thisPS,2);

% Update stats
ps_stat(find(PS.ptr==handles.thisPS))

% Update handles structure
guidata(hObject, handles);



% --- Executes on button press in pushbutton13.
function pushbutton13_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton13 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global PS

% Push Bulk Power button on main gui panel for this PS
ps_control(['pushbutton',PS.gui.main.bulkLayout{2,handles.thisPS},'_Callback'], ...
  PS.gui.main.(['pushbutton',PS.gui.main.bulkLayout{2,handles.thisPS}]), [], PS.gui.main) ;



% --- Executes on button press in pushbutton14.
function pushbutton14_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton14 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global PS

% Bulk PS Fault Reset Command
lcaput(PS.cmd.bulkFaultReset{PS.ptr==handles.thisPS},1);
pause(0.5)
lcaput(PS.cmd.bulkFaultReset{PS.ptr==handles.thisPS},0);


% --- Executes on button press in pushbutton15.
function pushbutton15_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton15 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global PS

% Launch Expert GUI panel
PS.gui.(['psExpert_panel',num2str(handles.thisPS)]) = ps_expert(handles.thisPS);

% Update stats
ps_stat(find(PS.ptr==handles.thisPS))

% Update handles structure
guidata(hObject, handles);


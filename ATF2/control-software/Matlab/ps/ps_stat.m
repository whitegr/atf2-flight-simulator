function [] = ps_stat(ps_choice,newPsWindow)
% Get PS status and handle alarms
global PS
persistent bulkErrorLast bulkErrMessages modErrorLast

% Init
if isempty(bulkErrorLast); bulkErrorLast=zeros(length(PS.inuse),1); end
if isempty(modErrorLast); modErrorLast=zeros(length(PS.inuse),1); end

% Get list of PS's required
if length(ps_choice)>1
  psList=ps_choice;
elseif ps_choice
  psList=ps_choice;
else
  psList=1:length(PS.cmd.stat1Read);
end % if ps_choice
% Make sure psList is row-wise
[rw,col]=size(psList); if rw<col; psList=psList'; end;

% Get PS status, alarm states etc.
% EPSC status
lcaput({PS.cmd.stat{psList}}',PS.cmd.statVal(psList));
pause(0.1);
[PS.rdb.stat1.val(psList), PS.rdb.stat1.time(psList)]=lcaget({PS.cmd.stat1Read{psList}}');
[PS.rdb.stat2.val(psList), PS.rdb.stat2.time(psList)]=lcaget({PS.cmd.stat2Read{psList}}');
[PS.rdb.stat3.val(psList), PS.rdb.stat3.time(psList)]=lcaget({PS.cmd.stat3Read{psList}}');
[PS.rdb.stat4.val(psList), PS.rdb.stat4.time(psList)]=lcaget({PS.cmd.stat4Read{psList}}');
[PS.rdb.current.val(psList), PS.rdb.current.time(psList)]=lcaget({PS.cmd.curRead{psList}}');
% Module status
if PS.doModule
  [PS.rdb.modDataValid.val(psList), PS.rdb.modDataValid.time(psList)]=lcaget({PS.cmd.modDataValid{psList}}');
  [PS.rdb.modNum.val(psList), PS.rdb.modNum.time(psList)]=lcaget({PS.cmd.modNum{psList}}');
  [PS.rdb.modFaultFlags.val(psList), PS.rdb.modFaultFlags.time(psList)]=lcaget({PS.cmd.modFaultFlags{psList}}');
  [PS.rdb.modDisableFlags.val(psList), PS.rdb.modDisableFlags.time(psList)]=lcaget({PS.cmd.modDisableFlags{psList}}');
  for iPS=psList'
    [PS.rdb.modCurrents.val{iPS}, PS.rdb.modCurrents.time(iPS)]=lcaget({PS.cmd.modCurrents{iPS}}');
  end % for iPS
end % if doModule
% Bulk PS status
if PS.doBulk
  [PS.rdb.bulkPwr.val(psList), PS.rdb.bulkPwr.time(psList)]=lcaget({PS.cmd.bulkPwr{psList}}');
  [PS.rdb.bulkStat.val(psList), PS.rdb.bulkStat.time(psList)]=lcaget({PS.cmd.bulkStatus{psList}}');
  if isempty(bulkErrMessages); bulkErrMessages={'Bulk PS Over Voltage!' 'Bulk PS Under Voltage!' 'Bulk PS Control in Local Mode!'}; end;
end % if doBulk
if exist('newPsWindow','var') && newPsWindow==1
  [PS.rdb.tRamp.val(psList), PS.rdb.tRamp.time(psList)]=lcaget({PS.cmd.tRamp{psList}}');
  [PS.rdb.curSet.val(psList), PS.rdb.curSet.time(psList)]=lcaget({PS.cmd.curSet{psList}}');
  [PS.rdb.curStore.val(psList), PS.rdb.curStore.time(psList)]=lcaget({PS.cmd.curStore{psList}}');
end % if new EPSC window
for iPS=psList'
  if PS.doBulk
    PS.bulkStat.pwr(iPS) = bitget(uint8(PS.rdb.bulkStat.val(iPS)),1);
    PS.bulkStat.ovolt(iPS) = bitget(uint8(PS.rdb.bulkStat.val(iPS)),2);
    PS.bulkStat.uvolt(iPS) = bitget(uint8(PS.rdb.bulkStat.val(iPS)),3);
    PS.bulkStat.lmode(iPS) = uint8(~bitget(uint8(PS.rdb.bulkStat.val(iPS)),4));
    bulkErrorStat(iPS)=PS.bulkStat.ovolt(iPS)+PS.bulkStat.uvolt(iPS)*2+PS.bulkStat.lmode(iPS)*4; %#ok<AGROW>
  end % if doBulk
  PS.stat.cmdError(iPS)=bitget(uint8(PS.rdb.stat1.val(iPS)),2);
  PS.stat.psOff(iPS)=bitget(uint8(PS.rdb.stat1.val(iPS)),3);
  PS.stat.rampOn(iPS)=bitget(uint8(PS.rdb.stat1.val(iPS)),4);
  PS.stat.rampOnSync(iPS)=bitget(uint8(PS.rdb.stat1.val(iPS)),5);
  PS.stat.rampReady(iPS)=bitget(uint8(PS.rdb.stat1.val(iPS)),6);
  PS.stat.revPol(iPS)=bitget(uint8(PS.rdb.stat1.val(iPS)),7);
  PS.stat.lmode(iPS)=bitget(uint8(PS.rdb.stat1.val(iPS)),8);
  PS.stat.errMess(iPS)=bitget(uint8(PS.rdb.stat2.val(iPS)),1);
  PS.stat.adcFail(iPS)=bitget(uint8(PS.rdb.stat2.val(iPS)),2);
  PS.stat.calFault(iPS)=bitget(uint8(PS.rdb.stat2.val(iPS)),3);
  PS.stat.auxTransFault(iPS)=bitget(uint8(PS.rdb.stat2.val(iPS)),4);
  PS.stat.interlock(iPS)=bitget(uint8(PS.rdb.stat2.val(iPS)),5);
  % EPSC alarm states
  PS.alarm(iPS)=0;
  minor_alarm=PS.stat.lmode(iPS);
  major_alarm=any([PS.stat.cmdError(iPS)>0 PS.rdb.stat2.val(iPS)>1 PS.rdb.stat3.val(iPS)>0]);
  if minor_alarm; PS.alarm(iPS)=1; end;
  if major_alarm; PS.alarm(iPS)=2; end;
  % Module alarms
  PS.modAlarm(iPS)=0;
  if PS.doModule
    if ~isequal(PS.rdb.modDataValid.val(iPS),1) || (5-sum(bitget(PS.rdb.modFaultFlags.val(iPS),1:5)))<PS.rdb.modNum.val(iPS) %#ok<BDLOG>
      PS.alarm(iPS)=2;
      PS.modAlarm(iPS)=1; PS.modAlarmTime = PS.rdb.modDataValid.time(iPS);
      PS.modError(iPS)=uint8(~isequal(PS.rdb.modDataValid.val(iPS),1)) + ...
        uint8((5-sum(bitget(PS.rdb.modFaultFlags.val(iPS),1:5)))<PS.rdb.modNum.val(iPS))*2;
    end % if module alarm state
  end % if doModule
  % Bulk PS alarm state
  if PS.doBulk
    PS.bulkAlarm(iPS)=0;
    if PS.bulkStat.lmode(iPS); PS.bulkAlarm(iPS)=1; end;
    if PS.bulkStat.ovolt(iPS) || PS.bulkStat.uvolt(iPS); PS.bulkAlarm(iPS)=2; end;
  end % if doBulk
  % --------- Update main GUI ---------
  if isfield(PS,'gui') && isfield(PS.gui,'main')
    alarmCol={PS.guiCol.green PS.guiCol.orange PS.guiCol.red};
    % Set PS button colour to indicate alarm status
    set(PS.gui.main.(['pushbutton',PS.gui.main.layout{1,PS.ptr(iPS)}]),'BackgroundColor',alarmCol{PS.alarm(iPS)+1});
    if PS.doBulk
      set(PS.gui.main.(['pushbutton',PS.gui.main.bulkLayout{1,PS.ptr(iPS)}]),'BackgroundColor',alarmCol{PS.bulkAlarm(iPS)+1});
    end % if doBulk
    % Set Power on/off status (EPSC)
    if PS.stat.psOff(iPS)
      set(PS.gui.main.(['pushbutton',PS.gui.main.layout{2,PS.ptr(iPS)}]),'BackgroundColor',PS.guiCol.red);
      set(PS.gui.main.(['pushbutton',PS.gui.main.layout{2,PS.ptr(iPS)}]),'String','Off');
    else
      set(PS.gui.main.(['pushbutton',PS.gui.main.layout{2,PS.ptr(iPS)}]),'BackgroundColor',PS.guiCol.green);
      set(PS.gui.main.(['pushbutton',PS.gui.main.layout{2,PS.ptr(iPS)}]),'String','On');
    end % if PS off
    % Set Power on/off status (bulk PS)
    if PS.doBulk
      if PS.bulkStat.pwr(iPS)
        set(PS.gui.main.(['pushbutton',PS.gui.main.bulkLayout{2,PS.ptr(iPS)}]),'BackgroundColor',PS.guiCol.green);
        set(PS.gui.main.(['pushbutton',PS.gui.main.bulkLayout{2,PS.ptr(iPS)}]),'String','On');
      else
        set(PS.gui.main.(['pushbutton',PS.gui.main.bulkLayout{2,PS.ptr(iPS)}]),'BackgroundColor',PS.guiCol.red);
        set(PS.gui.main.(['pushbutton',PS.gui.main.bulkLayout{2,PS.ptr(iPS)}]),'String','Off');
      end % if PS off
    end % if doBulk
  end % if main gui there
  % -------- Update EPSC panel if open ---------
  psGui=['ps_panel',num2str(PS.ptr(iPS))];
  if isfield(PS,'gui') && (isfield(PS.gui,psGui) || (exist('newPsWindow','var')&&newPsWindow==1))
    isReallyThere=true;
    if ~(exist('newPsWindow','var')&&newPsWindow==1)
      % Fix case where gui window killed but gui flag still there
      try
        findobj(PS.gui.(psGui).figure1);
      catch
        PS.gui=rmfield(PS.gui,psGui);
        isReallyThere=false;
      end % try/catch
    end % if new window
    if isReallyThere
      PS.isupdating=true;
      % Is local mode active?
%       if PS.stat.lmode(iPS)
%         set(PS.gui.(psGui).text9,'Visible','on');
%       else
%         set(PS.gui.(psGui).text9,'Visible','off');
%       end % is local mode active?
      % Set power status button
      if PS.stat.psOff(iPS)
        set(PS.gui.(psGui).pushbutton10,'BackgroundColor',PS.guiCol.red);
        set(PS.gui.(psGui).pushbutton10,'String','Power Off');
      else
        set(PS.gui.(psGui).pushbutton10,'BackgroundColor',PS.guiCol.green);
        set(PS.gui.(psGui).pushbutton10,'String','Power On');
      end % if PS power off
      % Set Power on/off status (bulk PS)
      if PS.doBulk
        if PS.bulkStat.pwr(iPS)
          set(PS.gui.(psGui).pushbutton13,'BackgroundColor',PS.guiCol.green);
          set(PS.gui.(psGui).pushbutton13,'String','Bulk Pwr On');
        else
          set(PS.gui.(psGui).pushbutton13,'BackgroundColor',PS.guiCol.red);
          set(PS.gui.(psGui).pushbutton13,'String','Bulk Pwr Off');
        end % if PS off
        % Bulk error status indication
        if ~PS.bulkAlarm(iPS)
          set(PS.gui.(psGui).pushbutton14,'BackgroundColor',PS.guiCol.green);
        elseif PS.bulkAlarm(iPS)==1
          set(PS.gui.(psGui).pushbutton14,'BackgroundColor',PS.guiCol.orange);
        else
          set(PS.gui.(psGui).pushbutton14,'BackgroundColor',PS.guiCol.red);
        end % bulk alarm state
      end % if doBulk
      % Set current readback value
      set(PS.gui.(psGui).text2,'String',sprintf('%.3f',PS.rdb.current.val(iPS)));
      % Set edit boxes if just opened window
      if exist('newPsWindow','var') && newPsWindow==1
        set(PS.gui.(psGui).edit3,'String',sprintf('%d',PS.rdb.tRamp.val(iPS)));
        set(PS.gui.(psGui).edit1,'String',sprintf('%.3f',PS.rdb.curSet.val(iPS)));
        set(PS.gui.(psGui).edit2,'String',sprintf('%.3f',PS.rdb.curStore.val(iPS)));
      end % if new window
      % Deal with EPSC error messages
      errMessCount=0;
      while PS.stat.errMess(iPS)
        errMessCount=errMessCount+1;
        if errMessCount>100; error('Error message read runaway!'); end;
        % Get and display current error message
        lcaput({PS.cmd.errMess{iPS}}',PS.cmd.errMessVal(iPS));
        [PS.rdb.errMess.val{iPS}, PS.rdb.errMess.time(iPS)]=lcaget(PS.cmd.errMessRead{iPS});
        % Message buffer = 15 messages, push oldest out after that
        permitErrMess=false;
        for iMess=1:length(PS.allowedErrMess)
          if length(findstr(PS.allowedErrMess{iMess},PS.rdb.errMess.val{iPS}{1})) %#ok<ISMT>
            permitErrMess=true;
            break;
          end % if permitted error message
        end % permitted error message list
        if length(PS.errMessages(iPS).str)==15 && ~permitErrMess
          for iMess=1:14
            PS.errMessages{iPS}.str(iPS).str{iMess}=PS.errMessages(iPS).str{iMess+1};
          end % for iMess
          PS.errMessages(iPS).str{15}=...
            [datestr(datenum('1/1/1970')+real(PS.rdb.errMess.time(iPS)/(3600*24)),'local'),':',PS.rdb.errMess.val{iPS}{1}];
        elseif ~permitErrMess
          PS.errMessages(iPS).str{length(PS.errMessages(iPS).str)+1}=...
            [datestr(datenum('1/1/1970')+real(PS.rdb.errMess.time(iPS)/(3600*24)),'local'),':',PS.rdb.errMess.val{iPS}{1}];
        end % if error message buffer full
        % Check to see if any more messages in buffer
        lcaput({PS.cmd.statOval{iPS}}',PS.cmd.statOvalVal(iPS));
        [PS.rdb.stat2.val(iPS), PS.rdb.stat2.time(iPS)]=lcaget(PS.cmd.stat2Read{iPS});
        PS.stat.errMess(iPS)=bitget(uint8(PS.rdb.stat2.val(iPS)),1);
      end % if error message waiting
      % Module alarm error message handling
      if PS.doModule && PS.modAlarm(iPS)
        if ~isequal(PS.modAlarm(iPS),modErrorLast(iPS))
          if length(PS.errMessages(iPS).str)==15
            for iMess=1:14
              PS.errMessages(iPS).str{iMess}=PS.errMessages(iPS).str{iMess+1};
            end % for iMess
            PS.errMessages(iPS).str{15}=...
              [datestr(datenum('1/1/1970')+real(PS.modAlarmTime/(3600*24)),'local'),': Module data not valid or fault detected- check module panel'];
          else
            PS.errMessages(iPS).str{length(PS.errMessages(iPS).str)+1}=...
                [datestr(datenum('1/1/1970')+real(PS.modAlarmTime/(3600*24)),'local'),': Module data not valid or fault detected- check module panel'];
          end % if error message buffer full
          modErrorLast(iPS)=PS.modAlarm(iPS);
        end % if mod alarm
      end % if doModule
      % Bulk PS error message handling
      if PS.doBulk
        if bulkErrorStat(iPS) && length(bulkErrorStat)==length(bulkErrorLast) && ~isequal(bulkErrorStat(iPS),bulkErrorLast(iPS))
          estat=[PS.bulkStat.ovolt(iPS) PS.bulkStat.uvolt(iPS) PS.bulkStat.lmode(iPS)];
          time=PS.rdb.bulkStat.time(iPS);
          for iStat=find(estat)
            if length(PS.errMessages(iPS).str)==15
              for iMess=1:14
                PS.errMessages(iPS).str{iMess}=PS.errMessages(iPS).str{iMess+1};
              end % for iMess
              PS.errMessages(iPS).str{15}=...
                [datestr(datenum('1/1/1970')+real(time/(3600*24)),'local'),': ',bulkErrMessages{iStat}];
            else
              PS.errMessages(iPS).str{length(PS.errMessages(iPS).str)+1}=...
                [datestr(datenum('1/1/1970')+real(time/(3600*24)),'local'),': ',bulkErrMessages{iStat}];
            end % if error message buffer full
          end % for iStat
          bulkErrorLast(iPS)=bulkErrorStat(iPS);
        end % if Bulk PS error state
      end % if doBulk
      % Write message buffer
      set(PS.gui.(psGui).text7,'String',PS.errMessages(iPS).str);
      % Set interlock status
      if PS.stat.interlock(iPS)
        set(PS.gui.(psGui).pushbutton5,'BackgroundColor',PS.guiCol.red);
      else
        set(PS.gui.(psGui).pushbutton5,'BackgroundColor',PS.guiCol.green);
      end % if interlocked
      uiresume(PS.gui.(psGui).figure1);
      PS.isupdating=false;
    end % if isReallyThere
  end % if PS gui open
  % ---------- Deal with Module panel GUI if open ------------
  if PS.doModule
    modGui=['psModule_panel',num2str(PS.ptr(iPS))];
    if isfield(PS,'gui') && isfield(PS.gui,modGui)
      isReallyThere=true;
      % Fix case where gui window killed but gui flag still there
      try
        findobj(PS.gui.(modGui).figure1);
      catch
        PS.gui=rmfield(PS.gui,modGui);
        isReallyThere=false;
      end % try/catch
      if isReallyThere
        PS.isupdating=true;
        for iMod=1:PS.rdb.modNum.val(iPS)
          % Set enabled/disabled status
          if bitget(uint8(PS.rdb.modDisableFlags.val(iPS)),iMod)
            set(PS.gui.(modGui).(['text',PS.gui.(modGui).statusText{1,iMod}]),'BackgroundColor',PS.guiCol.red);
            set(PS.gui.(modGui).(['text',PS.gui.(modGui).statusText{1,iMod}]),'String','DISABLED');
          else
            set(PS.gui.(modGui).(['text',PS.gui.(modGui).statusText{1,iMod}]),'BackgroundColor',PS.guiCol.green);
            set(PS.gui.(modGui).(['text',PS.gui.(modGui).statusText{1,iMod}]),'String','ENABLED');
          end % if disabled
          % Set current
          set(PS.gui.(modGui).(['text',PS.gui.(modGui).statusText{2,iMod}]),'String',[num2str(PS.rdb.modCurrents.val{iPS}(iMod)*PS.modcurConv),' A']);
          % Set Fault Status
          cols={'black' 'red'}; set(PS.gui.(modGui).(['radiobutton',PS.gui.(modGui).faultButton{iMod}]),'ForegroundColor',...
            PS.guiCol.(cols{sign(uint8(~isequal(PS.rdb.modDataValid.val(iPS),1)) + bitget(uint8(PS.rdb.modFaultFlags.val(iPS)),iMod))+1}));
          set(PS.gui.(modGui).(['radiobutton',PS.gui.(modGui).faultButton{iMod}]),'Value',...
            sign(uint8(~isequal(PS.rdb.modDataValid.val(iPS),1)) + bitget(uint8(PS.rdb.modFaultFlags.val(iPS)),iMod)));
          uiresume(PS.gui.(modGui).figure1);
        end % for iMod
        PS.isupdating=false;
      end % if isreallythere
    end % if isthere
  end % if doMopdule
  % ---------- Deal with Expert panel GUI if open ------------
  expGui=['psExpert_panel',num2str(PS.ptr(iPS))];
  if isfield(PS,'gui') && isfield(PS.gui,expGui)
    isReallyThere=true;
    % Fix case where gui window killed but gui flag still there
    try
      findobj(PS.gui.(expGui).figure1);
    catch
      PS.gui=rmfield(PS.gui,expGui);
      isReallyThere=false;
    end % try/catch
    if isReallyThere
      PS.isupdating=true;
      % Deal with status lights
      statusLight={PS.guiCol.green PS.guiCol.red};
      set(PS.gui.(expGui).pushbutton2,'BackgroundColor',statusLight{PS.stat.lmode(iPS)+1});
      set(PS.gui.(expGui).pushbutton3,'BackgroundColor',...
        statusLight{bitget(uint8(PS.rdb.stat3.val(iPS)),1)+1});
      set(PS.gui.(expGui).pushbutton4,'BackgroundColor',...
        statusLight{bitget(uint8(PS.rdb.stat3.val(iPS)),2)+1});
      set(PS.gui.(expGui).pushbutton5,'BackgroundColor',...
        statusLight{bitget(uint8(PS.rdb.stat3.val(iPS)),3)+1});
      set(PS.gui.(expGui).pushbutton6,'BackgroundColor',...
        statusLight{bitget(uint8(PS.rdb.stat3.val(iPS)),4)+1});
      set(PS.gui.(expGui).pushbutton7,'BackgroundColor',...
        statusLight{bitget(uint8(PS.rdb.stat3.val(iPS)),6)+1});
      set(PS.gui.(expGui).pushbutton8,'BackgroundColor',...
        statusLight{bitget(uint8(PS.rdb.stat2.val(iPS)),4)+1});
      set(PS.gui.(expGui).pushbutton9,'BackgroundColor',...
        statusLight{bitget(uint8(PS.rdb.stat3.val(iPS)),7)+1});
      set(PS.gui.(expGui).pushbutton10,'BackgroundColor',...
        statusLight{bitget(uint8(PS.rdb.stat3.val(iPS)),1)+1});
      set(PS.gui.(expGui).pushbutton11,'BackgroundColor',...
        statusLight{bitget(uint8(PS.rdb.stat2.val(iPS)),3)+1});
      set(PS.gui.(expGui).pushbutton12,'BackgroundColor',...
        statusLight{bitget(uint8(PS.rdb.stat3.val(iPS)),8)+1});
      if PS.stat.interlock(iPS)
        set(PS.gui.(expGui).pushbutton13,'BackgroundColor',PS.guiCol.red);
      else
        set(PS.gui.(expGui).pushbutton13,'BackgroundColor',PS.guiCol.green);
      end % if interlocked
        
      % other error status codes
      strCommand={ PS.cmd.lastRst{iPS}; PS.cmd.lastOff{iPS}; PS.cmd.selfTest{iPS};
        PS.cmd.calErr{iPS}; PS.cmd.ipAddr{iPS}; PS.cmd.chassisConfig{iPS}; PS.cmd.serNum{iPS};
        PS.cmd.magnetID{iPS}; PS.cmd.firmVer{iPS}; PS.cmd.calDate{iPS}; PS.cmd.regAtoV{iPS};
        PS.cmd.auxAtoV{iPS}; PS.cmd.gndAtoV{iPS}; PS.cmd.psVtoV{iPS}; PS.cmd.refV{iPS}; PS.cmd.digErrLim{iPS}};
      dCommand = { PS.cmd.auxI{iPS}; PS.cmd.dacI{iPS}; PS.cmd.ripI{iPS};
        PS.cmd.gndI{iPS}; PS.cmd.psV{iPS}; PS.cmd.chassisTemp{iPS}; PS.cmd.spareV{iPS};
        PS.cmd.adcOff{iPS}; PS.cmd.adcGain{iPS}; PS.cmd.dacOff{iPS}; PS.cmd.dacGain{iPS};
        PS.cmd.bulkV{iPS}};
      cstr=lcaget(strCommand,0,'char');
      dvals=lcaGet(dCommand);
      lastRst = cstr{1,1};
      lastOff = cstr{2,1};
      selfTest = cstr{3,1};
      calErr = cstr{4,1};
      set(PS.gui.(expGui).text6,'String',lastRst);
      set(PS.gui.(expGui).text7,'String',lastOff);
      set(PS.gui.(expGui).text8,'String',selfTest);
      set(PS.gui.(expGui).text9,'String',calErr);
      % Deal with status readbacks
      auxI = dvals(1);
      dacI = dvals(2);
      ripI = dvals(3);
      gndI = dvals(4);
      psV = dvals(5);
      cTemp = dvals(6);
      spareV = dvals(7);
      adcOff = dvals(8);
      adcG = dvals(9);
      dacOff = dvals(10);
      dacG = dvals(11);
      bulkV = dvals(12);
      set(PS.gui.(expGui).text10,'String',sprintf('%.3f',PS.rdb.current.val(iPS)));
      set(PS.gui.(expGui).text11,'String',sprintf('%.3f',auxI));
      set(PS.gui.(expGui).text12,'String',sprintf('%.3f',dacI));
      set(PS.gui.(expGui).text13,'String',sprintf('%.3f',ripI));
      set(PS.gui.(expGui).text14,'String',sprintf('%.3f',gndI));
      set(PS.gui.(expGui).text15,'String',sprintf('%.3f',psV));
      set(PS.gui.(expGui).text16,'String',sprintf('%.3f',cTemp));
      set(PS.gui.(expGui).text17,'String',sprintf('%.3f',spareV));
      set(PS.gui.(expGui).text18,'String',sprintf('%4d',adcOff));
      set(PS.gui.(expGui).text19,'String',sprintf('%4d',adcG));
      set(PS.gui.(expGui).text20,'String',sprintf('%4d',dacOff));
      set(PS.gui.(expGui).text21,'String',sprintf('%4d',dacG));
      set(PS.gui.(expGui).text48,'String',sprintf('%.3f',bulkV));
      % Deal with config summary section fv date
      ip = {cstr{5,1:4}};
      cType = cstr{6,1};
      sNum = cstr{7,1}; if length(sNum)>8; sNum=sNum(1:8); end;
      mID = cstr{8,1}; if length(mID)>8; mID=mID(1:8); end;
      firmVer = cstr{9,1}; if length(firmVer)>8; firmVer=firmVer(1:8); end;
      cDate = cstr{10,1}; if length(cDate)>8; cDate=cDate(1:8); end;
      regAtoV = cstr{11,1};
      auxAtoV = cstr{12,1};
      gndAtoV = cstr{13,1};
      psVtoV = cstr{14,1};
      refV = cstr{15,1};
      digErrLim = cstr{16,1};
      set(PS.gui.(expGui).text35,'String',sprintf('%d.%d.%d.%d',str2double(ip)));
      set(PS.gui.(expGui).text36,'String',cType);
      set(PS.gui.(expGui).text37,'String',sNum);
      set(PS.gui.(expGui).text38,'String',mID);
      set(PS.gui.(expGui).text39,'String',firmVer);
      set(PS.gui.(expGui).text40,'String',cDate);
      set(PS.gui.(expGui).text42,'String',regAtoV);
      set(PS.gui.(expGui).text43,'String',auxAtoV);
      set(PS.gui.(expGui).text44,'String',gndAtoV); 
      set(PS.gui.(expGui).text45,'String',psVtoV);
      set(PS.gui.(expGui).text46,'String',refV);
      set(PS.gui.(expGui).text47,'String',digErrLim);
      drawnow;
      PS.isupdating=false;
      if isfield(PS,'commandToRun')
        eval(PS.commandToRun{1});
        eval(PS.commandToRun{2});
        PS=rmfield(PS,'commandToRun');
      end % if commands waiting
    end % if Module panel open
  end % if isReallyThere
end % for iPS
function varargout = ps_standardisation(varargin)
% PS_STANDARDISATION M-file for ps_standardisation.fig
%      PS_STANDARDISATION, by itself, creates a new PS_STANDARDISATION or raises the existing
%      singleton*.
%
%      H = PS_STANDARDISATION returns the handle to a new PS_STANDARDISATION or the handle to
%      the existing singleton*.
%
%      PS_STANDARDISATION('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in PS_STANDARDISATION.M with the given input arguments.
%
%      PS_STANDARDISATION('Property','Value',...) creates a new PS_STANDARDISATION or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before ps_standardisation_OpeningFunction gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to ps_standardisation_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help ps_standardisation

% Last Modified by GUIDE v2.5 27-Sep-2007 11:18:49

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @ps_standardisation_OpeningFcn, ...
                   'gui_OutputFcn',  @ps_standardisation_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before ps_standardisation is made visible.
function ps_standardisation_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to ps_standardisation (see VARARGIN)
global PS

% Choose default command line output for ps_standardisation
handles.output = hObject;

% Flag PS number
handles.thisPS = varargin{1};

% Set PS label in gui
set(handles.text1,'String',['PS ',num2str(handles.thisPS)]);

% Update fields from EPICS DB
set(handles.edit1,'String',num2str(lcaget(PS.cmd.sd_iLow{PS.ptr==handles.thisPS})));
set(handles.edit2,'String',num2str(lcaget(PS.cmd.sd_iHigh{PS.ptr==handles.thisPS})));
set(handles.edit3,'String',num2str(lcaget(PS.cmd.sd_nCycles{PS.ptr==handles.thisPS})));
set(handles.edit4,'String',num2str(lcaget(PS.cmd.sd_waitLow{PS.ptr==handles.thisPS})));
set(handles.edit5,'String',num2str(lcaget(PS.cmd.sd_waitHigh{PS.ptr==handles.thisPS})));

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes ps_standardisation wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = ps_standardisation_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles;


% --- Executes on button press in pushbutton1.
function pushbutton1_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global PS
try
  delete(PS.gui.(['psStandardisation_panel',num2str(handles.thisPS)]).figure1);
  PS.gui=rmfield(PS.gui,['psStandardisation_panel',num2str(handles.thisPS)]);
catch
  delete(gcf)
  rethrow(lasterror)
end % try/catch


function edit1_Callback(hObject, eventdata, handles)
% hObject    handle to edit1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit1 as text
%        str2double(get(hObject,'String')) returns contents of edit1 as a double
global PS
lcaput(PS.cmd.sd_iLow{PS.ptr==handles.thisPS},str2double(get(hObject,'String')))

% --- Executes during object creation, after setting all properties.
function edit1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit2_Callback(hObject, eventdata, handles)
% hObject    handle to edit2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit2 as text
%        str2double(get(hObject,'String')) returns contents of edit2 as a double
global PS
lcaput(PS.cmd.sd_iHigh{PS.ptr==handles.thisPS},str2double(get(hObject,'String')))

% --- Executes during object creation, after setting all properties.
function edit2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit3_Callback(hObject, eventdata, handles)
% hObject    handle to edit3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit3 as text
%        str2double(get(hObject,'String')) returns contents of edit3 as a double
global PS
lcaput(PS.cmd.sd_nCycle{PS.ptr==handles.thisPS},str2double(get(hObject,'String')))

% --- Executes during object creation, after setting all properties.
function edit3_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit4_Callback(hObject, eventdata, handles)
% hObject    handle to edit4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit4 as text
%        str2double(get(hObject,'String')) returns contents of edit4 as a double
global PS
lcaput(PS.cmd.sd_waitLow{PS.ptr==handles.thisPS},str2double(get(hObject,'String')))

% --- Executes during object creation, after setting all properties.
function edit4_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit5_Callback(hObject, eventdata, handles)
% hObject    handle to edit5 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit5 as text
%        str2double(get(hObject,'String')) returns contents of edit5 as a double
global PS
lcaput(PS.cmd.sd_waitHigh{PS.ptr==handles.thisPS},str2double(get(hObject,'String')))

% --- Executes during object creation, after setting all properties.
function edit5_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit5 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end





function ps_guistart(psList)
warning('off','MATLAB:dispatcher:InexactMatch')
global PS

% Initialisation
PS.errMessages={};
PS.allowedErrMess={'MESSAGE BUFFER EMPTY'};
PS.doMonitor=true;
PS.doModule=true; % Ensure module:status.SCAN to 1 sec periodic if true
PS.doBulk=true;
PS.modcurConv=50/410;

% Power supplies in use
if isdeployed
  if isempty(psList) || ~isnumeric(str2double(psList))
    fid=fopen([getenv('PSIOC'),'/st.cmd']);
    fline=fgetl(fid);
    psList=[];
    while ~isnumeric(fline)
      t=regexp(fline,'^epscDriverInit("PS(\d+)','tokens');
      if ~isempty(t); psList=[psList str2double(t{1}{1})]; end; %#ok<AGROW>
      fline=fgetl(fid);
    end
    fclose(fid);
    PS.inuse = psList;
  else
    PS.inuse = str2num(psList); %#ok<ST2NM>
  end % get PS list
else
  if exist('psList','var')
    PS.inuse = psList;
  else
    fid=fopen([getenv('PSIOC'),'/st.cmd']);
    fline=fgetl(fid);
    psList=[];
    while ~isnumeric(fline)
      t=regexp(fline,'^epscDriverInit("PS(\d+)','tokens');
      if ~isempty(t); psList=[psList str2double(t{1}{1})]; end; %#ok<AGROW>
      fline=fgetl(fid);
    end
    fclose(fid);
    PS.inuse = psList;
  end % if psList passed
end % isdeployed?

% Error message buffer init
for iPS=1:length(psList)
  PS.errMessages(iPS).str={};
end % for iPS

% Form EPICS command strings
PS.cmd.stat={};
PS.pointer=zeros(length(PS.inuse),1);
% Get sim mode
[PS.simMode.val PS.simMode.time] = lcaget(['PS',num2str(psList(1)),':simMode']);
for iPS=PS.inuse
  ind=length(PS.cmd.stat)+1;
  PS.cmd.stat{ind,1}=['PS',num2str(iPS),':readADC.PROC']; PS.cmd.statVal(ind,1)=1;
  PS.cmd.statOval{ind,1}=['PS',num2str(iPS),':stat.PROC']; PS.cmd.statOvalVal(ind,1)=1;
  PS.cmd.stat1Read{ind,1}=['PS',num2str(iPS),':status1.VAL'];
  PS.cmd.stat2Read{ind,1}=['PS',num2str(iPS),':status2.VAL'];
  PS.cmd.stat3Read{ind,1}=['PS',num2str(iPS),':status3.VAL'];
  PS.cmd.stat4Read{ind,1}=['PS',num2str(iPS),':status4.VAL'];
  PS.cmd.curRead{ind,1}=['PS',num2str(iPS),':current.VAL'];
  PS.cmd.tRamp{ind,1}=['PS',num2str(iPS),':rampTimeConv.VAL'];
  PS.cmd.curSet{ind,1}=['PS',num2str(iPS),':currentDES.VAL'];
  PS.cmd.curStore{ind,1}=['PS',num2str(iPS),':currentSTORE.VAL'];
  PS.cmd.errMess{ind,1}=['PS',num2str(iPS),':readErr.PROC']; PS.cmd.errMessVal(ind,1)=1;
  PS.cmd.errMessRead{ind,1}=['PS',num2str(iPS),':errorString.VAL'];
  PS.cmd.pwrOn{ind,1}=['PS',num2str(iPS),':pwrOn.PROC']; PS.cmd.pwrOnVal(ind,1)=1;
  PS.cmd.pwrOff{ind,1}=['PS',num2str(iPS),':pwrOff.PROC']; PS.cmd.pwrOffVal(ind,1)=1;
  PS.cmd.ramp{ind,1}=['PS',num2str(iPS),':setCurrent.PROC']; PS.cmd.rampVal(ind,1)=1;
  PS.cmd.rampHold{ind,1}=['PS',num2str(iPS),':setCurrentHold.PROC']; PS.cmd.rampHoldVal(ind,1)=1;
  PS.cmd.resetInterlocks{ind,1}=['PS',num2str(iPS),':resetInterlocks.PROC']; PS.cmd.resetInterlocksVal(ind,1)=1;
  PS.cmd.resetController{ind,1}=['PS',num2str(iPS),':resetController.PROC']; PS.cmd.resetControllerVal(ind,1)=1;
  PS.ptr(ind,1)=iPS;
  PS.alarm(ind,1)=0;
  % Expert panel stuff
  PS.cmd.chassisConfig{ind,1}=['PS',num2str(iPS),':chassisConfig.VAL'];
  PS.cmd.magnetID{ind,1}=['PS',num2str(iPS),':magnetID.VAL'];
  PS.cmd.ipAddr{ind,1}=['PS',num2str(iPS),':ipAddr.VAL'];
  PS.cmd.regAtoV{ind,1}=['PS',num2str(iPS),':regTransA_per_V.VAL'];
  PS.cmd.auxAtoV{ind,1}=['PS',num2str(iPS),':auxTransA_per_V.VAL'];
  PS.cmd.gndAtoV{ind,1}=['PS',num2str(iPS),':gndCurA_per_V.VAL'];
  PS.cmd.psVtoV{ind,1}=['PS',num2str(iPS),':psV_per_V.VAL'];
  PS.cmd.serNum{ind,1}=['PS',num2str(iPS),':chassisSerialNum.VAL'];
  PS.cmd.firmVer{ind,1}=['PS',num2str(iPS),':firmwareVer.VAL'];
  PS.cmd.adcOff{ind,1}=['PS',num2str(iPS),':adcOff.VAL'];
  PS.cmd.adcGain{ind,1}=['PS',num2str(iPS),':adcGain.VAL'];
  PS.cmd.dacOff{ind,1}=['PS',num2str(iPS),':dacOff.VAL'];
  PS.cmd.dacGain{ind,1}=['PS',num2str(iPS),':dacGain.VAL'];
  PS.cmd.refV{ind,1}=['PS',num2str(iPS),':refV.VAL'];
  PS.cmd.calDate{ind,1}=['PS',num2str(iPS),':calDate.VAL'];
  PS.cmd.auxI{ind,1}=['PS',num2str(iPS),':auxI.VAL'];
  PS.cmd.dacI{ind,1}=['PS',num2str(iPS),':dacI.VAL'];
  PS.cmd.ripI{ind,1}=['PS',num2str(iPS),':ripI.VAL'];
  PS.cmd.gndI{ind,1}=['PS',num2str(iPS),':gndI.VAL'];
  PS.cmd.chassisTemp{ind,1}=['PS',num2str(iPS),':chassisTemp.VAL'];
  PS.cmd.psV{ind,1}=['PS',num2str(iPS),':psV.VAL'];
  PS.cmd.spareV{ind,1}=['PS',num2str(iPS),':spareV.VAL'];
  PS.cmd.lastRst{ind,1}=['PS',num2str(iPS),':lastRst.VAL'];
  PS.cmd.lastOff{ind,1}=['PS',num2str(iPS),':lastOff.VAL'];
  PS.cmd.calErr{ind,1}=['PS',num2str(iPS),':calErr.VAL'];
  PS.cmd.selfTest{ind,1}=['PS',num2str(iPS),':selfTest.VAL'];
  PS.cmd.digErrLim{ind,1}=['PS',num2str(iPS),':digErrLim.VAL'];
  PS.cmd.sd_iLow{ind,1}=['PS',num2str(iPS),':standardise:iLow.VAL'];
  PS.cmd.sd_iHigh{ind,1}=['PS',num2str(iPS),':standardise:iHigh.VAL'];
  PS.cmd.sd_waitLow{ind,1}=['PS',num2str(iPS),':standardise:waitLow.VAL'];
  PS.cmd.sd_waitHigh{ind,1}=['PS',num2str(iPS),':standardise:waitHigh.VAL'];
  PS.cmd.sd_nCycles{ind,1}=['PS',num2str(iPS),':standardise:nCycles.VAL'];
  % Module Commands
  PS.cmd.modStat{ind,1}=['PS',num2str(iPS),':module:status.PROC']; % Command to fill Module status records (SCAN=periodic)
  PS.cmd.modEnable{ind,1}=['PS',num2str(iPS),':module:enable.VAL']; % Enable Module command (VAL=module #, 0=all)
  PS.cmd.modDisable{ind,1}=['PS',num2str(iPS),':module:disable.VAL']; % Enable Disable command (VAL=module #)
  PS.cmd.modDataValid{ind,1}=['PS',num2str(iPS),':module:dataValid.RVAL']; % Data valid flag
  PS.cmd.modNum{ind,1}=['PS',num2str(iPS),':module:number.VAL']; % Number of modules reported
  PS.cmd.modFaultFlags{ind,1}=['PS',num2str(iPS),':module:faultFlags.VAL']; % Flags reporting faulty modules
  PS.cmd.modDisableFlags{ind,1}=['PS',num2str(iPS),':module:disableFlags.VAL']; % Flags reporting disabled modules
  PS.cmd.modCurrents{ind,1}=['PS',num2str(iPS),':module:currents.VAL']; % Array of reported module currents
  % Bulk PS command strings
  if PS.simMode.val
    field='SVAL';
  else
    field='RVAL';
  end % if simMode
  PS.cmd.bulkPwr{ind,1}=['BPS',num2str(iPS),':pwrOnOff'];
  PS.cmd.bulkFaultReset{ind,1}=['BPS',num2str(iPS),':faultReset'];
  PS.cmd.bulkStatus{ind,1}=['BPS',num2str(iPS),':status.',field];
  PS.cmd.bulkV{ind,1}=['BPS',num2str(iPS),':outputVoltage.VAL'];
  % Get data for which BPS drives which PS (need to give commands to last
  % allocated BPS record)
  t=regexp(lcaGet(['BPS',num2str(iPS),':pwrOnOff.OUT']),'REMOTE_CONTROL\[(-?\d+)\]','tokens');
  bulkList(ind,1)=abs(str2double(t{1}{1})); %#ok<AGROW>
end % for iPS
for ibulk=1:max(bulkList)
  bm{ibulk}=find(bulkList==ibulk); %#ok<AGROW>
  for ib=1:length(bm{ibulk})
    bulkMatch{ibulk}{ib}=num2str(bm{ibulk}(ib)); %#ok<AGROW>
  end % for ib
end % for ibulk
for ind=1:length(PS.cmd.bulkPwr)
  for ibulk=1:length(bulkMatch)
    if ismember(ind,str2double(bulkMatch{ibulk}))
      sm=str2double(sort(bulkMatch{ibulk}));
      whichPS(ind)=sm(end); %#ok<AGROW>
      break;
    end % if member bulkMatch
  end % for ibulk
  PS.cmd.bulkPwr{ind,1}=['BPS',num2str(whichPS(ind)),':pwrOnOff'];
  PS.cmd.bulkFaultReset{ind,1}=['BPS',num2str(whichPS(ind)),':faultReset'];
  PS.cmd.bulkStatus{ind,1}=['BPS',num2str(whichPS(ind)),':status.',field];
  PS.cmd.bulkV{ind,1}=['BPS',num2str(whichPS(ind)),':outputVoltage.VAL'];
end % for ips

% GUI parameters
PS.guiCol.green = [0 1 0.501961];
PS.guiCol.red = [1 0 0];
PS.guiCol.orange = [1 0.501961 0];
PS.guiCol.black = [0 0 0];

% Get PS status (all PS's)
ps_stat(0);

% Launch main GUI
PS.gui.main = ps_control;
ps_stat(0);

% Start monitor process
monitorSetup=[];
controlMonitorPVs={PS.cmd.stat1Read{:};
                   PS.cmd.stat2Read{:};
                   PS.cmd.stat3Read{:};
                   PS.cmd.stat4Read{:}};
if PS.doModule
  controlMonitorPVs={controlMonitorPVs{:},...
                     PS.cmd.modDisableFlags{:},...
                     PS.cmd.modDataValid{:},...
                     PS.cmd.modFaultFlags{:}}';
end % if doModule
bulkList=unique(whichPS);
if PS.doBulk
  for ibulk=1:length(bulkList)
    controlMonitorPVs={controlMonitorPVs{:},...
                       PS.cmd.bulkStatus{bulkList(ibulk)}}';
  end % for ibulk
end % if doBulk
while 1
  % Clear monitor channels and exit if main GUI closed
  if ~isfield(PS,'gui') || ~isfield(PS.gui,'main')
    lcaclear();
    break
  end % if gui still open
  % Fix case where main gui window killed but gui flag still there
  try
    findobj(PS.gui.main.figure1);
  catch
    lcaclear();
    break
  end % try/catch
  % If monitor requested, setup monitors and update status entries
  if isfield(PS,'doMonitor') && PS.doMonitor
    if ~isfield(monitorSetup,'main')
      % Main control panel monitors
      lcaSetMonitor({controlMonitorPVs{:}}');
      monitorSetup.main=true;
      % EPSC panel monitors
      for iPS=PS.inuse
        if isfield(PS,'gui') && ~isfield(monitorSetup,['ps_panel',num2str(iPS)])
          lcaSetMonitor(PS.cmd.curRead{PS.ptr==iPS}); % Current readback monitor
          monitorSetup.(['ps_panel',num2str(iPS)])=true;
        end % is panel open for iPS?
      end % for iPS
      % Module panel monitors
      if PS.doModule
        for iPS=PS.inuse
          if isfield(PS,'gui') && ~isfield(monitorSetup,['psModule_panel',num2str(iPS)])
            lcaSetMonitor(PS.cmd.modCurrents{PS.ptr==iPS}); % Currents readback monitor
            monitorSetup.(['psModule_panel',num2str(iPS)])=true;
          end % is panel open for iPS?
        end % for iPS
      end % if doModule
    end % if monitor setup
    % Check for any status report changes
    flags=lcaNewMonitorValue({controlMonitorPVs{:}}');
%     if sum(flags);
%     ps_stat(unique(ceil(mod((find(flags>0))/length(controlMonitorPVs),length(controlMonitorPVs))))); end;
    if sum(flags); ps_stat(0); end;
    % Check for any current readback value changes
    panelsActive=fieldnames(PS.gui);
    for iPanel=1:length(panelsActive)
      % Update EPSC panel gui's
      psPanelNum=regexp(panelsActive{iPanel},'ps_panel(\d+)','tokens');
      if ~isempty(psPanelNum)
        if lcaNewMonitorValue(PS.cmd.curRead{PS.ptr==str2double(psPanelNum{1})})>0; ps_stat(find(PS.ptr==str2double(psPanelNum{1}))); end;
      end % if ps_panel
      % Update at refresh rate if expert panel open
      expPanelNum=regexp(panelsActive{iPanel},'psExpert_panel(\d+)','tokens');
      if ~isempty(expPanelNum)
        ps_stat(find(PS.ptr==str2double(expPanelNum{1})));
      end % if expert panel open
      % Update EPSC Module panel gui's
      if PS.doModule
        modPanelNum=regexp(panelsActive{iPanel},'psModule_panel(\d+)','tokens');
        if ~isempty(modPanelNum)
          if lcaNewMonitorValue(PS.cmd.modCurrents{PS.ptr==str2double(modPanelNum{1})})>0; ps_stat(find(PS.ptr==str2double(modPanelNum{1}))); end;
        end % if ps_panel
      end % if doModule
    end % for iPanel
  end % if monitor
  pause(0.2);
  drawnow;
end % monitor loop

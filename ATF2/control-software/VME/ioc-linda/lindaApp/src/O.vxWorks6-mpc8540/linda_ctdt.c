/* C++ static constructor and destructor lists */
/* This is a generated file, do not edit! */
#include <vxWorks.h>

/* Declarations */
void _GLOBAL__I__ZN14repeaterClientC2ERK11osiSockAddr();
void _GLOBAL__I__ZN18epicsThreadRunableD2Ev();
void _GLOBAL__I__ZN21epicsTimeLoadTimeInitC2Ev();
void _GLOBAL__I__ZN9dbService13contextCreateER10epicsMutexS1_R16cacContextNotify();
void _GLOBAL__I_atRebootExtern();
void _GLOBAL__I_iocshPpdbbase();
void _GLOBAL__I_linda_registerRecordDeviceDriver();
void _GLOBAL__I_localHostNameAtLoadTime();
void _GLOBAL__I_logMsgToErrlog();
void _GLOBAL__I_noopIIU();
void _GLOBAL__I_osdNTPGet();
void _GLOBAL__I_timerQueueMgrEPICS();
void _GLOBAL__D__ZN9dbService13contextCreateER10epicsMutexS1_R16cacContextNotify();
void _GLOBAL__D_localHostNameAtLoadTime();
void _GLOBAL__D_noopIIU();
void _GLOBAL__D_timerQueueMgrEPICS();

/* Constructors */
void (*_ctors[])() = {
    _GLOBAL__I__ZN14repeaterClientC2ERK11osiSockAddr,
    _GLOBAL__I__ZN18epicsThreadRunableD2Ev,
    _GLOBAL__I__ZN21epicsTimeLoadTimeInitC2Ev,
    _GLOBAL__I__ZN9dbService13contextCreateER10epicsMutexS1_R16cacContextNotify,
    _GLOBAL__I_atRebootExtern,
    _GLOBAL__I_iocshPpdbbase,
    _GLOBAL__I_linda_registerRecordDeviceDriver,
    _GLOBAL__I_localHostNameAtLoadTime,
    _GLOBAL__I_logMsgToErrlog,
    _GLOBAL__I_noopIIU,
    _GLOBAL__I_osdNTPGet,
    _GLOBAL__I_timerQueueMgrEPICS,
    NULL
};

/* Destructors */
void (*_dtors[])() = {
    _GLOBAL__D_timerQueueMgrEPICS,
    _GLOBAL__D_noopIIU,
    _GLOBAL__D_localHostNameAtLoadTime,
    _GLOBAL__D__ZN9dbService13contextCreateER10epicsMutexS1_R16cacContextNotify,
    NULL
};


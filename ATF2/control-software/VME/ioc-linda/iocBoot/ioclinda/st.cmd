## Example vxWorks startup file

## The following is needed if your board support package doesn't at boot time
## automatically cd to the directory containing its startup script
cd "/home/whitegr/ioc-linda/iocBoot/ioclinda"
#hostAdd("lindaHost","192.168.0.1")
#nfsMountAll("lindaHost")
< cdCommands

cd topbin
## You may have to change linda to something else
## everywhere it appears in this file

ld < linda.munch

## This drvTS initializer is needed if the IOC has a hardware event system
#TSinit

## Register all support components
cd top
dbLoadDatabase("dbd/linda.dbd",0,0)
linda_registerRecordDeviceDriver(pdbbase)

## Load record instances
#dbLoadRecords("db/motor.db")
#dbLoadRecords("db/v3122.db","P=ESB:ADC,C=1")
dbLoadRecords("db/gtr.db","name=slacvme1:sis1:,card=1,size=1000")
dbLoadRecords("db/bpm.vdb","bpmname=MQF1X,wfname=slacvme1:sis1:,wfsize=1000,card=1,lwf=0,rwf=1,uwf=2,dwf=3,anal=MADV");

## Oms58 Driver Setup
#oms58Setup(2,0x0000,190,5,10)

## vmic3122 setup
#this is from Zen lband/lband/ioc-esb-.../itab/itab/st.cmd
#drvV3122Config( 1,0x10000,32,32,10,0xc5,3,"VMIC 3122")
#             (card,adrs,buf,nch,rate,ivec,ilev,name)
#drvV3122Config( 1,0x00000,32, 32,  10,0xc5,   3,"VMIC 3122")

## Set this to see messages from mySub
#mySubDebug = 1

# SIS setup (card,clockspeed,a32offset,intVec,intLev,useDma)
sisfadcConfig(1,105,0x20000000,0xcc,3,0)
#sisfadcConfig(2,105,0x21000000,0xcd,3,0)
#sisfadcConfig(3,105,0x22000000,0xce,3,0)
#sisfadcConfig(4,105,0x23000000,0xcf,3,0)
#sisfadcConfig(5,105,0x24000000,0xd0,3,0)
#sisfadcConfig(6,105,0x25000000,0xd1,3,0)
#sisfadcConfig(7,105,0x26000000,0xd2,3,0)

cd startup
iocInit()

# Need to hit the soft trigger once to get the cards triggering for some reason
dbpf "slacvme1:sis1:softTrigger", "1"
#dbpf "slacvme1:sis2:softTrigger", "1"
#dbpf "slacvme1:sis3:softTrigger", "1"
#dbpf "slacvme1:sis4:softTrigger", "1"

## Start any sequence programs
#seq &sncExample,"user=whitegr"

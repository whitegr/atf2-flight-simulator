#!/atf/op/epics/slacepics/ioc-ps/bin/linux-x86/ps

< /atf/op/epics/slacepics/ioc-ps/iocBoot/iocps/envPaths

## Set Debug level
epicsEnvSet(PS_DEBUG_LEVEL,"0")

## Simulation Flag - Set to 1 and comment out UDP connection lines + drvEtherIP_define_PLC for sim mode
epicsEnvSet(PS_SIM_MODE,"0")

cd ${TOP}

## Register all support components
dbLoadDatabase("dbd/ps.dbd")
ps_registerRecordDeviceDriver(pdbbase)

## EtherIP setup (comment drvEtherIP_define_PLC lines for sim mode)
drvEtherIP_init()
drvEtherIP_define_PLC("plc1", "192.168.1.200", 0)
#drvEtherIP_define_PLC("plc2", "192.168.1.101", 0)
EIP_verbosity(0)

## UDP connection to EPSC (comment out for sim mode)
drvAsynIPPortConfigure("PS1conn","192.168.1.98:2000 UDP*",0,0,1)
drvAsynIPPortConfigure("PS2conn","192.168.1.97:2000 UDP*",0,0,1)
drvAsynIPPortConfigure("PS3conn","192.168.1.96:2000 UDP*",0,0,1)
drvAsynIPPortConfigure("PS4conn","192.168.1.95:2000 UDP*",0,0,1)
drvAsynIPPortConfigure("PS5conn","192.168.1.94:2000 UDP*",0,0,1)
drvAsynIPPortConfigure("PS6conn","192.168.1.93:2000 UDP*",0,0,1)
drvAsynIPPortConfigure("PS7conn","192.168.1.92:2000 UDP*",0,0,1)
drvAsynIPPortConfigure("PS8conn","192.168.1.91:2000 UDP*",0,0,1)
drvAsynIPPortConfigure("PS9conn","192.168.1.90:2000 UDP*",0,0,1)
drvAsynIPPortConfigure("PS10conn","192.168.1.89:2000 UDP*",0,0,1)
drvAsynIPPortConfigure("PS11conn","192.168.1.88:2000 UDP*",0,0,1)
drvAsynIPPortConfigure("PS12conn","192.168.1.87:2000 UDP*",0,0,1)
drvAsynIPPortConfigure("PS13conn","192.168.1.86:2000 UDP*",0,0,1)
drvAsynIPPortConfigure("PS14conn","192.168.1.85:2000 UDP*",0,0,1)
drvAsynIPPortConfigure("PS15conn","192.168.1.84:2000 UDP*",0,0,1)
drvAsynIPPortConfigure("PS16conn","192.168.1.83:2000 UDP*",0,0,1)
drvAsynIPPortConfigure("PS17conn","192.168.1.82:2000 UDP*",0,0,1)
drvAsynIPPortConfigure("PS18conn","192.168.1.81:2000 UDP*",0,0,1)
drvAsynIPPortConfigure("PS19conn","192.168.1.80:2000 UDP*",0,0,1)
drvAsynIPPortConfigure("PS20conn","192.168.1.79:2000 UDP*",0,0,1)
drvAsynIPPortConfigure("PS21conn","192.168.1.78:2000 UDP*",0,0,1)
drvAsynIPPortConfigure("PS22conn","192.168.1.77:2000 UDP*",0,0,1)
drvAsynIPPortConfigure("PS23conn","192.168.1.76:2000 UDP*",0,0,1)
drvAsynIPPortConfigure("PS24conn","192.168.1.75:2000 UDP*",0,0,1)
drvAsynIPPortConfigure("PS25conn","192.168.1.74:2000 UDP*",0,0,1)
drvAsynIPPortConfigure("PS26conn","192.168.1.73:2000 UDP*",0,0,1)
drvAsynIPPortConfigure("PS27conn","192.168.1.72:2000 UDP*",0,0,1)
drvAsynIPPortConfigure("PS28conn","192.168.1.71:2000 UDP*",0,0,1)
drvAsynIPPortConfigure("PS29conn","192.168.1.70:2000 UDP*",0,0,1)
drvAsynIPPortConfigure("PS30conn","192.168.1.69:2000 UDP*",0,0,1)
drvAsynIPPortConfigure("PS31conn","192.168.1.68:2000 UDP*",0,0,1)
drvAsynIPPortConfigure("PS32conn","192.168.1.67:2000 UDP*",0,0,1)
drvAsynIPPortConfigure("PS33conn","192.168.1.66:2000 UDP*",0,0,1)
drvAsynIPPortConfigure("PS34conn","192.168.1.65:2000 UDP*",0,0,1)
drvAsynIPPortConfigure("PS35conn","192.168.1.64:2000 UDP*",0,0,1)
drvAsynIPPortConfigure("PS36conn","192.168.1.63:2000 UDP*",0,0,1)
drvAsynIPPortConfigure("PS37conn","192.168.1.62:2000 UDP*",0,0,1)
drvAsynIPPortConfigure("PS38conn","192.168.1.61:2000 UDP*",0,0,1)

## Initialise drivers for EPSC
epscDriverInit("PS1",0,0,0,"PS1conn")
epscDriverInit("PS2",0,0,0,"PS2conn")
epscDriverInit("PS3",0,0,0,"PS3conn")
epscDriverInit("PS4",0,0,0,"PS4conn")
epscDriverInit("PS5",0,0,0,"PS5conn")
epscDriverInit("PS6",0,0,0,"PS6conn")
epscDriverInit("PS7",0,0,0,"PS7conn")
epscDriverInit("PS8",0,0,0,"PS8conn")
epscDriverInit("PS9",0,0,0,"PS9conn")
epscDriverInit("PS10",0,0,0,"PS10conn")
epscDriverInit("PS11",0,0,0,"PS11conn")
epscDriverInit("PS12",0,0,0,"PS12conn")
epscDriverInit("PS13",0,0,0,"PS13conn")
epscDriverInit("PS14",0,0,0,"PS14conn")
epscDriverInit("PS15",0,0,0,"PS15conn")
epscDriverInit("PS16",0,0,0,"PS16conn")
epscDriverInit("PS17",0,0,0,"PS17conn")
epscDriverInit("PS18",0,0,0,"PS18conn")
epscDriverInit("PS19",0,0,0,"PS19conn")
epscDriverInit("PS20",0,0,0,"PS20conn")
epscDriverInit("PS21",0,0,0,"PS21conn")
epscDriverInit("PS22",0,0,0,"PS22conn")
epscDriverInit("PS23",0,0,0,"PS23conn")
epscDriverInit("PS24",0,0,0,"PS24conn")
epscDriverInit("PS25",0,0,0,"PS25conn")
epscDriverInit("PS26",0,0,0,"PS26conn")
epscDriverInit("PS27",0,0,0,"PS27conn")
epscDriverInit("PS28",0,0,0,"PS28conn")
epscDriverInit("PS29",0,0,0,"PS29conn")
epscDriverInit("PS30",0,0,0,"PS30conn")
epscDriverInit("PS31",0,0,0,"PS31conn")
epscDriverInit("PS32",0,0,0,"PS32conn")
epscDriverInit("PS33",0,0,0,"PS33conn")
epscDriverInit("PS34",0,0,0,"PS34conn")
epscDriverInit("PS35",0,0,0,"PS35conn")
epscDriverInit("PS36",0,0,0,"PS36conn")
epscDriverInit("PS37",0,0,0,"PS37conn")
epscDriverInit("PS38",0,0,0,"PS38conn")


## Load EPSC+EtherIP databases
dbLoadRecords("db/ps.vdb","PS=1,PSconn=PS1conn,SIM_MODE=${PS_SIM_MODE},PLC=plc1,PLC_PS=1,MINCUR=0,MAXCUR=150,RATE=7.5,SRATE=7.5,SCYC=5,SMIN=0.5,SMAX=150,SWAITMIN=10,SWAITMAX=10,NAME=QD10X")
dbLoadRecords("db/ps.vdb","PS=2,PSconn=PS2conn,SIM_MODE=${PS_SIM_MODE},PLC=plc1,PLC_PS=1,MINCUR=0,MAXCUR=150,RATE=7.5,SRATE=7.5,SCYC=5,SMIN=0.5,SMAX=150,SWAITMIN=10,SWAITMAX=10,NAME=QF11X")
dbLoadRecords("db/ps.vdb","PS=3,PSconn=PS3conn,SIM_MODE=${PS_SIM_MODE},PLC=plc1,PLC_PS=1,MINCUR=0,MAXCUR=150,RATE=7.5,SRATE=7.5,SCYC=5,SMIN=0.5,SMAX=150,SWAITMIN=10,SWAITMAX=10,NAME=QD12X")
dbLoadRecords("db/ps.vdb","PS=4,PSconn=PS4conn,SIM_MODE=${PS_SIM_MODE},PLC=plc1,PLC_PS=1,MINCUR=0,MAXCUR=50,RATE=7.5,SRATE=7.5,SCYC=5,SMIN=0.5,SMAX=50,SWAITMIN=10,SWAITMAX=10,NAME=QD10BFF")
dbLoadRecords("db/ps.vdb","PS=5,PSconn=PS5conn,SIM_MODE=${PS_SIM_MODE},PLC=plc1,PLC_PS=1,MINCUR=0,MAXCUR=50,RATE=7.5,SRATE=7.5,SCYC=5,SMIN=0.5,SMAX=50,SWAITMIN=10,SWAITMAX=10,NAME=QD10AFF")
dbLoadRecords("db/ps.vdb","PS=6,PSconn=PS6conn,SIM_MODE=${PS_SIM_MODE},PLC=plc1,PLC_PS=2,MINCUR=0,MAXCUR=150,RATE=7.5,SRATE=7.5,SCYC=5,SMIN=0.5,SMAX=150,SWAITMIN=10,SWAITMAX=10,NAME=QF15X")
dbLoadRecords("db/ps.vdb","PS=7,PSconn=PS7conn,SIM_MODE=${PS_SIM_MODE},PLC=plc1,PLC_PS=2,MINCUR=0,MAXCUR=150,RATE=7.5,SRATE=7.5,SCYC=5,SMIN=0.5,SMAX=150,SWAITMIN=10,SWAITMAX=10,NAME=QD16X")
dbLoadRecords("db/ps.vdb","PS=8,PSconn=PS8conn,SIM_MODE=${PS_SIM_MODE},PLC=plc1,PLC_PS=2,MINCUR=0,MAXCUR=150,RATE=7.5,SRATE=7.5,SCYC=5,SMIN=0.5,SMAX=150,SWAITMIN=10,SWAITMAX=10,NAME=QF17X")
dbLoadRecords("db/ps.vdb","PS=9,PSconn=PS9conn,SIM_MODE=${PS_SIM_MODE},PLC=plc1,PLC_PS=2,MINCUR=0,MAXCUR=50,RATE=7.5,SRATE=7.5,SCYC=5,SMIN=0.5,SMAX=50,SWAITMIN=10,SWAITMAX=10,NAME=QF9BFF")
dbLoadRecords("db/ps.vdb","PS=10,PSconn=PS10conn,SIM_MODE=${PS_SIM_MODE},PLC=plc1,PLC_PS=2,MINCUR=0,MAXCUR=50,RATE=7.5,SRATE=7.5,SCYC=5,SMIN=0.5,SMAX=50,SWAITMIN=10,SWAITMAX=10,NAME=QF9AFF")
dbLoadRecords("db/ps.vdb","PS=11,PSconn=PS11conn,SIM_MODE=${PS_SIM_MODE},PLC=plc1,PLC_PS=2,MINCUR=0,MAXCUR=50,RATE=3,SRATE=8,SCYC=3,SMIN=0.5,SMAX=50,SWAITMIN=30,SWAITMAX=30,NAME=SF6FF")
dbLoadRecords("db/ps.vdb","PS=12,PSconn=PS12conn,SIM_MODE=${PS_SIM_MODE},PLC=plc1,PLC_PS=3,MINCUR=0,MAXCUR=200,RATE=3,SRATE=8,SCYC=4,SMIN=0.5,SMAX=175,SWAITMIN=30,SWAITMAX=30,NAME=B5FF")
dbLoadRecords("db/ps.vdb","PS=13,PSconn=PS13conn,SIM_MODE=${PS_SIM_MODE},PLC=plc1,PLC_PS=3,MINCUR=0,MAXCUR=150,RATE=7.5,SRATE=7.5,SCYC=5,SMIN=0.5,SMAX=150,SWAITMIN=10,SWAITMAX=10,NAME=QD18X")
dbLoadRecords("db/ps.vdb","PS=14,PSconn=PS14conn,SIM_MODE=${PS_SIM_MODE},PLC=plc1,PLC_PS=3,MINCUR=0,MAXCUR=150,RATE=7.5,SRATE=7.5,SCYC=5,SMIN=0.5,SMAX=150,SWAITMIN=10,SWAITMAX=10,NAME=QF19X")
dbLoadRecords("db/ps.vdb","PS=15,PSconn=PS15conn,SIM_MODE=${PS_SIM_MODE},PLC=plc1,PLC_PS=3,MINCUR=0,MAXCUR=50,RATE=7.5,SRATE=7.5,SCYC=5,SMIN=0.5,SMAX=50,SWAITMIN=10,SWAITMAX=10,NAME=QD8FF")
dbLoadRecords("db/ps.vdb","PS=16,PSconn=PS16conn,SIM_MODE=${PS_SIM_MODE},PLC=plc1,PLC_PS=3,MINCUR=0,MAXCUR=50,RATE=7.5,SRATE=7.5,SCYC=5,SMIN=0.5,SMAX=50,SWAITMIN=10,SWAITMAX=10,NAME=QF7FF")
dbLoadRecords("db/ps.vdb","PS=17,PSconn=PS17conn,SIM_MODE=${PS_SIM_MODE},PLC=plc1,PLC_PS=3,MINCUR=0,MAXCUR=50,RATE=7.5,SRATE=7.5,SCYC=5,SMIN=0.5,SMAX=50,SWAITMIN=10,SWAITMAX=10,NAME=QD6FF")
dbLoadRecords("db/ps.vdb","PS=18,PSconn=PS18conn,SIM_MODE=${PS_SIM_MODE},PLC=plc1,PLC_PS=3,MINCUR=0,MAXCUR=25,RATE=3,SRATE=8,SCYC=3,SMIN=0.5,SMAX=50,SWAITMIN=30,SWAITMAX=30,NAME=SF5FF")
dbLoadRecords("db/ps.vdb","PS=19,PSconn=PS19conn,SIM_MODE=${PS_SIM_MODE},PLC=plc1,PLC_PS=4,MINCUR=-150,MAXCUR=150,RATE=7.5,SRATE=7.5,SCYC=5,SMIN=0.5,SMAX=150,SWAITMIN=10,SWAITMAX=10,NAME=QM16FF")
dbLoadRecords("db/ps.vdb","PS=20,PSconn=PS20conn,SIM_MODE=${PS_SIM_MODE},PLC=plc1,PLC_PS=4,MINCUR=-150,MAXCUR=150,RATE=7.5,SRATE=7.5,SCYC=5,SMIN=0.5,SMAX=150,SWAITMIN=10,SWAITMAX=10,NAME=QM15FF")
dbLoadRecords("db/ps.vdb","PS=21,PSconn=PS21conn,SIM_MODE=${PS_SIM_MODE},PLC=plc1,PLC_PS=4,MINCUR=-150,MAXCUR=150,RATE=7.5,SRATE=7.5,SCYC=5,SMIN=0.5,SMAX=150,SWAITMIN=10,SWAITMAX=10,NAME=QM14FF")
dbLoadRecords("db/ps.vdb","PS=22,PSconn=PS22conn,SIM_MODE=${PS_SIM_MODE},PLC=plc1,PLC_PS=4,MINCUR=0,MAXCUR=50,RATE=7.5,SRATE=7.5,SCYC=5,SMIN=0.5,SMAX=50,SWAITMIN=10,SWAITMAX=10,NAME=QF5BFF")
dbLoadRecords("db/ps.vdb","PS=23,PSconn=PS23conn,SIM_MODE=${PS_SIM_MODE},PLC=plc1,PLC_PS=4,MINCUR=0,MAXCUR=50,RATE=7.5,SRATE=7.5,SCYC=5,SMIN=0.5,SMAX=50,SWAITMIN=10,SWAITMAX=10,NAME=QF5AFF")
dbLoadRecords("db/ps.vdb","PS=24,PSconn=PS24conn,SIM_MODE=${PS_SIM_MODE},PLC=plc1,PLC_PS=4,MINCUR=0,MAXCUR=50,RATE=7.5,SRATE=7.5,SCYC=5,SMIN=0.5,SMAX=50,SWAITMIN=10,SWAITMAX=10,NAME=QD4BFF")
dbLoadRecords("db/ps.vdb","PS=25,PSconn=PS25conn,SIM_MODE=${PS_SIM_MODE},PLC=plc1,PLC_PS=4,MINCUR=0,MAXCUR=50,RATE=7.5,SRATE=7.5,SCYC=5,SMIN=0.5,SMAX=50,SWAITMIN=10,SWAITMAX=10,NAME=QD4AFF")
dbLoadRecords("db/ps.vdb","PS=26,PSconn=PS26conn,SIM_MODE=${PS_SIM_MODE},PLC=plc1,PLC_PS=5,MINCUR=-150,MAXCUR=150,RATE=7.5,SRATE=7.5,SCYC=5,SMIN=0.5,SMAX=150,SWAITMIN=10,SWAITMAX=10,NAME=QM13FF")
dbLoadRecords("db/ps.vdb","PS=27,PSconn=PS27conn,SIM_MODE=${PS_SIM_MODE},PLC=plc1,PLC_PS=5,MINCUR=-150,MAXCUR=150,RATE=7.5,SRATE=7.5,SCYC=5,SMIN=0.5,SMAX=150,SWAITMIN=10,SWAITMAX=10,NAME=QM12FF")
dbLoadRecords("db/ps.vdb","PS=28,PSconn=PS28conn,SIM_MODE=${PS_SIM_MODE},PLC=plc1,PLC_PS=5,MINCUR=-150,MAXCUR=150,RATE=7.5,SRATE=7.5,SCYC=5,SMIN=0.5,SMAX=150,SWAITMIN=10,SWAITMAX=10,NAME=QM11FF")
dbLoadRecords("db/ps.vdb","PS=29,PSconn=PS29conn,SIM_MODE=${PS_SIM_MODE},PLC=plc1,PLC_PS=5,MINCUR=0,MAXCUR=50,RATE=3,SRATE=8,SCYC=3,SMIN=0.5,SMAX=50,SWAITMIN=30,SWAITMAX=30,NAME=SD4FF")
dbLoadRecords("db/ps.vdb","PS=30,PSconn=PS30conn,SIM_MODE=${PS_SIM_MODE},PLC=plc1,PLC_PS=5,MINCUR=0,MAXCUR=50,RATE=7.5,SRATE=7.5,SCYC=5,SMIN=0.5,SMAX=50,SWAITMIN=10,SWAITMAX=10,NAME=QF3FF")
dbLoadRecords("db/ps.vdb","PS=31,PSconn=PS31conn,SIM_MODE=${PS_SIM_MODE},PLC=plc1,PLC_PS=5,MINCUR=0,MAXCUR=50,RATE=7.5,SRATE=7.5,SCYC=5,SMIN=0.5,SMAX=50,SWAITMIN=10,SWAITMAX=10,NAME=QD2BFF")
dbLoadRecords("db/ps.vdb","PS=32,PSconn=PS32conn,SIM_MODE=${PS_SIM_MODE},PLC=plc1,PLC_PS=5,MINCUR=0,MAXCUR=50,RATE=7.5,SRATE=7.5,SCYC=5,SMIN=0.5,SMAX=50,SWAITMIN=10,SWAITMAX=10,NAME=QD2AFF")
dbLoadRecords("db/ps.vdb","PS=33,PSconn=PS33conn,SIM_MODE=${PS_SIM_MODE},PLC=plc1,PLC_PS=6,MINCUR=0,MAXCUR=200,RATE=3,SRATE=8,SCYC=4,SMIN=0.5,SMAX=175,SWAITMIN=30,SWAITMAX=30,NAME=B2FF")
dbLoadRecords("db/ps.vdb","PS=34,PSconn=PS34conn,SIM_MODE=${PS_SIM_MODE},PLC=plc1,PLC_PS=6,MINCUR=0,MAXCUR=200,RATE=3,SRATE=8,SCYC=4,SMIN=0.5,SMAX=175,SWAITMIN=30,SWAITMAX=30,NAME=B1FF")
dbLoadRecords("db/ps.vdb","PS=35,PSconn=PS35conn,SIM_MODE=${PS_SIM_MODE},PLC=plc1,PLC_PS=6,MINCUR=0,MAXCUR=220,RATE=4,SRATE=10,SCYC=4,SMIN=5,SMAX=150,SWAITMIN=30,SWAITMAX=30,NAME=QF1FF")
dbLoadRecords("db/ps.vdb","PS=36,PSconn=PS36conn,SIM_MODE=${PS_SIM_MODE},PLC=plc1,PLC_PS=6,MINCUR=0,MAXCUR=220,RATE=4,SRATE=10,SCYC=4,SMIN=5,SMAX=150,SWAITMIN=30,SWAITMAX=30,NAME=QD0FF")
dbLoadRecords("db/ps.vdb","PS=37,PSconn=PS37conn,SIM_MODE=${PS_SIM_MODE},PLC=plc1,PLC_PS=6,MINCUR=0,MAXCUR=10,RATE=1,SRATE=2,SCYC=3,SMIN=0.5,SMAX=10,SWAITMIN=30,SWAITMAX=30,NAME=SF1FF")
dbLoadRecords("db/ps.vdb","PS=38,PSconn=PS38conn,SIM_MODE=${PS_SIM_MODE},PLC=plc1,PLC_PS=6,MINCUR=0,MAXCUR=10,RATE=1,SRATE=2,SCYC=3,SMIN=0.5,SMAX=10,SWAITMIN=30,SWAITMAX=30,NAME=SD0FF")



cd ${TOP}/iocBoot/${IOC}

## Debug info
#asynSetTraceMask("PS1",0,0xff)
#asynSetTraceIOMask("PS1",0,0x2)

iocInit()

# Dump db records into record file
dbl > dbList.txt

## Start any sequence programs
#seq sncExample,"user=whitegrHost"

/*epscDriver.c */
/***********************************************************************
* EPICS Driver for SLAC Ethernet Power Supply Controller
* Intended fo use with accompanying ps.vdb database
* Requires AsynDriver
* Built on AsynDriver echoDriver template, see below
* Glen White, SLAC: August 17 2007
**********************************************************************/

/***********************************************************************
* Copyright (c) 2002 The University of Chicago, as Operator of Argonne
* National Laboratory, and the Regents of the University of
* California, as Operator of Los Alamos National Laboratory, and
* Berliner Elektronenspeicherring-Gesellschaft m.b.H. (BESSY).
* asynDriver is distributed subject to a Software License Agreement
* found in file LICENSE that is included with this distribution.
***********************************************************************/

/*test driver for asyn support*/
/* 
 * Author: Marty Kraimer
 */

#include <stddef.h>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <errno.h>

#include <cantProceed.h>
#include <epicsStdio.h>
#include <epicsThread.h>
#include <iocsh.h>
#include <dbFldTypes.h>
#include <dbAccess.h>
#include <dbDefs.h>
#include <link.h>
#include <menuScan.h>

#include <asynDriver.h>
#include <asynOctet.h>
#include <asynOctetSyncIO.h>

#include <epicsExport.h>
#define BUFFERSIZE 4096
#define NUM_DEVICES 2

typedef struct deviceBuffer {
    char buffer[BUFFERSIZE];
    int  nchars;
}deviceBuffer;

typedef struct deviceInfo {
    deviceBuffer buffer;
    int          connected;
}deviceInfo;

typedef struct epscPvt {
  deviceInfo    device[NUM_DEVICES];
  const char    *portName;
  char          *udpPort;
  int           connected;
  int           multiDevice;
  double        delay;
  asynInterface common;
  asynInterface octet;
  char          eos[2];
  int           eoslen;
  int           simMode;
  int           simPsState;
  int           simError;
  long int      debugLevel;
  struct dbAddr pStrDB, pStatOneDB, pStatTwoDB, pCurrentDB, pCurrentDESDB, pRampTimeDB, pModEnableDB, pModDisableDB;
  struct dbAddr pModValidDB, pModNumDB, pModFaultDB, pModDisableStatDB, pModCurDB,pStatThreeDB,pStatFourDB;
  struct dbAddr pChassisConfigDB,pMagnetIDDB,pIpAddrDB,pRegTransA_per_VDB,pAuxTransA_per_VDB,pGndCurA_per_VDB;
  struct dbAddr pPsV_per_VDB,pSerialNumDB,pVersionDB,pRefVDB,pCalDateDB,pAuxIDB,pDacIDB,pRipIDB,pGndIDB;
  struct dbAddr pTempDB,pPsVDB,pSpareVDB,pAdcOffDB,pAdcGainDB,pDacOffDB,pDacGainDB,pLastRstDB,pLastOffDB;
  struct dbAddr pCalErrDB,pSelfTestDB,pDigErrLimDB,pPwrOnRev;
  void          *pasynPvt;   /*For registerInterruptSource*/
}epscPvt;
    
/* init routine */
static int epscDriverInit(const char *dn, double delay,
			  int noAutoConnect,int multiDevice, const char *PSconn);

/* asynCommon methods */
static void report(void *drvPvt,FILE *fp,int details);
static asynStatus connect(void *drvPvt,asynUser *pasynUser);
static asynStatus disconnect(void *drvPvt,asynUser *pasynUser);
static asynCommon asyn = { report, connect, disconnect };

/* asynOctet methods */
static asynStatus epscWrite(void *drvPvt,asynUser *pasynUser,
    const char *data,size_t numchars,size_t *nbytesTransfered);
static asynStatus epscRead(void *drvPvt,asynUser *pasynUser,
    char *data,size_t maxchars,size_t *nbytesTransfered,int *eomReason);
static asynStatus epscFlush(void *drvPvt,asynUser *pasynUser);
static asynStatus setEos(void *drvPvt,asynUser *pasynUser,
    const char *eos,int eoslen);
static asynStatus getEos(void *drvPvt,asynUser *pasynUser,
    char *eos, int eossize, int *eoslen);

epicsShareExtern asynOctetSyncIO *pasynOctetSyncIO;

static int epscDriverInit(const char *dn, double delay,
			  int noAutoConnect,int multiDevice, const char *PSudp)
{
    epscPvt    *pepscPvt;
    char       *portName;
    char       *udpPortName;
    asynStatus status;
    int        nbytes;
    int        attributes;
    asynOctet  *pasynOctet;
/* need to check this */
    nbytes = sizeof(epscPvt) + sizeof(asynOctet) + strlen(dn) + strlen(PSudp) + 1;
    pepscPvt = callocMustSucceed(nbytes,sizeof(char),"epscDriverInit");
    pasynOctet = (asynOctet *)(pepscPvt + 1);
    portName = (char *)(pasynOctet + 1);
    strcpy(portName,dn);
    udpPortName = (char *)(portName + strlen(portName) + 1);
    strcpy(udpPortName,PSudp);
    pepscPvt->portName = portName;
    pepscPvt->udpPort = udpPortName;
    pepscPvt->delay = delay;
    pepscPvt->multiDevice = multiDevice;
    pepscPvt->common.interfaceType = asynCommonType;
    pepscPvt->common.pinterface  = (void *)&asyn;
    pepscPvt->common.drvPvt = pepscPvt;
    attributes = 0;
    pepscPvt->debugLevel = strtol(getenv("PS_DEBUG_LEVEL"),NULL,0);
    /* Simulation parameters */
    pepscPvt->simMode = strtol(getenv("PS_SIM_MODE"),NULL,0);
    pepscPvt->simPsState = 0;
    pepscPvt->simError = 0;
    if(multiDevice) attributes |= ASYN_MULTIDEVICE;
    if(delay>0.0) attributes|=ASYN_CANBLOCK;
    status = pasynManager->registerPort(portName,attributes,!noAutoConnect,0,0);
    if(status!=asynSuccess) {
        printf("epscDriverInit registerDriver failed\n");
        return 0;
    }
    status = pasynManager->registerInterface(portName,&pepscPvt->common);
    if(status!=asynSuccess){
        printf("epscDriverInit registerInterface failed\n");
        return 0;
    }

    pasynOctet->writeRaw = epscWrite;
    pasynOctet->readRaw = epscRead;
    pasynOctet->flush = epscFlush;
    pasynOctet->setInputEos = setEos;
    pasynOctet->getInputEos = getEos;
    pepscPvt->octet.interfaceType = asynOctetType;
    pepscPvt->octet.pinterface  = pasynOctet;
    pepscPvt->octet.drvPvt = pepscPvt;
    if(multiDevice) {
        status = pasynOctetBase->initialize(portName,&pepscPvt->octet,0,0,0);
    } else {
        status = pasynOctetBase->initialize(portName,&pepscPvt->octet,1,1,0);
    }
    if(status==asynSuccess)
        status = pasynManager->registerInterruptSource(
            portName,&pepscPvt->octet,&pepscPvt->pasynPvt);
    if(status!=asynSuccess){
        printf("epscDriverInit registerInterface failed\n");
        return 0;
    }
    return(0);
}

/* asynCommon methods */
static void report(void *drvPvt,FILE *fp,int details)
{
    epscPvt *pepscPvt = (epscPvt *)drvPvt;
    int i,n;

    fprintf(fp,"    epscDriver. "
        "multiDevice:%s connected:%s delay = %f\n",
        (pepscPvt->multiDevice ? "Yes" : "No"),
        (pepscPvt->connected ? "Yes" : "No"),
        pepscPvt->delay);
    n = (pepscPvt->multiDevice) ? NUM_DEVICES : 1;
    for(i=0;i<n;i++) {
       fprintf(fp,"        device %d connected:%s nchars = %d\n",
            i,
            (pepscPvt->device[i].connected ? "Yes" : "No"),
            pepscPvt->device[i].buffer.nchars);
    }
}

static asynStatus connect(void *drvPvt,asynUser *pasynUser)
{
    epscPvt    *pepscPvt = (epscPvt *)drvPvt;
    deviceInfo *pdeviceInfo;
    int        addr;
    asynStatus status;

    status = pasynManager->getAddr(pasynUser,&addr);
    if(status!=asynSuccess) return status;
    asynPrint(pasynUser, ASYN_TRACE_FLOW,
        "%s epscDriver:connect addr %d\n",pepscPvt->portName,addr);
    if(!pepscPvt->multiDevice) {
        if(pepscPvt->connected) {
            asynPrint(pasynUser,ASYN_TRACE_ERROR,
               "%s epscDriver:connect port already connected\n",
               pepscPvt->portName);
            return asynError;
        }
        pepscPvt->connected = 1;
        pepscPvt->device[0].connected = 1;
        pasynManager->exceptionConnect(pasynUser);
        return asynSuccess;
    }
    if(addr<=-1) {
        if(pepscPvt->connected) {
            asynPrint(pasynUser,ASYN_TRACE_ERROR,
               "%s epscDriver:connect port already connected\n",
               pepscPvt->portName);
            return asynError;
        }
        pepscPvt->connected = 1;
        pasynManager->exceptionConnect(pasynUser);
        return asynSuccess;
    }
    if(addr>=NUM_DEVICES) {
        asynPrint(pasynUser,ASYN_TRACE_ERROR,
            "%s epscDriver:connect illegal addr %d\n",pepscPvt->portName,addr);
        return asynError;
    }
    pdeviceInfo = &pepscPvt->device[addr];
    if(pdeviceInfo->connected) {
        asynPrint(pasynUser,ASYN_TRACE_ERROR,
            "%s epscDriver:connect device %d already connected\n",
            pepscPvt->portName,addr);
        return asynError;
    }
    pdeviceInfo->connected = 1;
    pasynManager->exceptionConnect(pasynUser);

    return(asynSuccess);
}

static asynStatus disconnect(void *drvPvt,asynUser *pasynUser)
{
    epscPvt    *pepscPvt = (epscPvt *)drvPvt;
    deviceInfo *pdeviceInfo;
    int        addr;
    asynStatus status;

    status = pasynManager->getAddr(pasynUser,&addr);
    if(status!=asynSuccess) return status;
    asynPrint(pasynUser, ASYN_TRACE_FLOW,
        "%s epscDriver:disconnect addr %d\n",pepscPvt->portName,addr);
    if(!pepscPvt->multiDevice) {
        if(!pepscPvt->connected) {
            asynPrint(pasynUser,ASYN_TRACE_ERROR,
               "%s epscDriver:disconnect port not connected\n",
               pepscPvt->portName);
            return asynError;
        }
        pepscPvt->connected = 0;
        pepscPvt->device[0].connected = 0;
        pasynManager->exceptionDisconnect(pasynUser);
        return asynSuccess;
    }
    if(addr<=-1) {
        if(!pepscPvt->connected) {
            asynPrint(pasynUser,ASYN_TRACE_ERROR,
               "%s epscDriver:disconnect port not connected\n",
               pepscPvt->portName);
            return asynError;
        }
        pepscPvt->connected = 0;
        pasynManager->exceptionDisconnect(pasynUser);
        return asynSuccess;
    }
    if(addr>=NUM_DEVICES) {
        asynPrint(pasynUser,ASYN_TRACE_ERROR,
            "%s epscDriver:disconnect illegal addr %d\n",pepscPvt->portName,addr);
        return asynError;
    }
    pdeviceInfo = &pepscPvt->device[addr];
    if(!pdeviceInfo->connected) {
        asynPrint(pasynUser,ASYN_TRACE_ERROR,
            "%s epscDriver:disconnect device %d not connected\n",
            pepscPvt->portName,addr);
        return asynError;
    }
    pdeviceInfo->connected = 0;
    pasynManager->exceptionDisconnect(pasynUser);
    return(asynSuccess);
}

/* asynOctet methods */
static asynStatus epscWrite(void *drvPvt,asynUser *pasynUser,
    const char *data,size_t nchars,size_t *nbytesTransfered)
{
    epscPvt      *pepscPvt = (epscPvt *)drvPvt;
    deviceInfo   *pdeviceInfo;
    deviceBuffer *pdeviceBuffer;
    int          addr,sp;
    asynStatus   status;
    char         pvStr[100];
    long         dbStat, rampTime;
    float        currentDES, ftemp;
    unsigned char dataOut[35];
    short int modInfo;
    char forceReverse[10];

    union f_type {
      float f;
      unsigned char fchar[4];
    } Cur[5];
    union i_type {
      short unsigned int i;
      unsigned char ichar[2];
    } charConv,charConv2,Ctype,Rsp,Tid,Num,Chan,Time[5];

    forceReverse[0]=0xffffffc7;

    if(pepscPvt->debugLevel) printf("PORT: %s\n",pepscPvt->udpPort);

    status = pasynManager->getAddr(pasynUser,&addr);
    if(status!=asynSuccess) return status;
    if(!pepscPvt->multiDevice) addr = 0;
    asynPrint(pasynUser, ASYN_TRACE_FLOW,
        "%s epscDriver:write addr %d\n",pepscPvt->portName,addr);
    if(addr<0 || addr>=NUM_DEVICES) {
        epicsSnprintf(pasynUser->errorMessage,pasynUser->errorMessageSize,
            "addr %d is illegal. Must be 0 or 1\n",addr);
        return asynError;
    }
    pdeviceInfo = &pepscPvt->device[addr];
    if(!pdeviceInfo->connected) {
        asynPrint(pasynUser,ASYN_TRACE_ERROR,
            "%s epscDriver:write device %d not connected\n",
            pepscPvt->portName,addr);
        epicsSnprintf(pasynUser->errorMessage,pasynUser->errorMessageSize,
            "%s epscDriver:write device %d not connected\n",
            pepscPvt->portName,addr);
        return asynError;
    }
    if(pepscPvt->delay>pasynUser->timeout) {
        if(pasynUser->timeout>0.0) epicsThreadSleep(pasynUser->timeout);
        asynPrint(pasynUser, ASYN_TRACE_ERROR,
            "%s epscDriver write timeout\n",pepscPvt->portName);
        epicsSnprintf(pasynUser->errorMessage,pasynUser->errorMessageSize,
            "%s epscDriver write timeout\n",pepscPvt->portName);
        return asynTimeout;
    }
    pdeviceBuffer = &pdeviceInfo->buffer;
    if(nchars>BUFFERSIZE) nchars = BUFFERSIZE;


    if (!pepscPvt->pRampTimeDB.pfield) { /* Get cuurentDES record pointer */
      strcpy(pvStr, pepscPvt->portName);
      strcat(pvStr, ":rampTime.VAL");
      dbStat = dbNameToAddr( pvStr, &pepscPvt->pRampTimeDB );
      if (dbStat) {
        printf("%d: ******NAME ERROR!!!!*******",dbStat);
        return asynError;
      }
    }
    /* Get command data string */
    /* Add zero byte to end of data (EPSC channel number - server expects to be 0x00)*/
    nchars++;
    if(nchars>0) memcpy(pdeviceBuffer->buffer,data,nchars);
    charConv.ichar[0]=data[0];
    charConv.ichar[1]='\0';
    switch (charConv.i) {
    case 0xc1: /* Set PS Current (and optionally ramp points) */
    case 0xc2: /* As above but additionally puts hardware into wait state */
      if (!pepscPvt->pCurrentDESDB.pfield) { /* Get cuurentDES record pointer */
        strcpy(pvStr, pepscPvt->portName);
        strcat(pvStr, ":currentDES.VAL");
        dbStat = dbNameToAddr( pvStr, &pepscPvt->pCurrentDESDB );
	if (dbStat) {
	  printf("%d: ******NAME ERROR!!!!*******",dbStat);
	  return asynError;
	}
      }
      /* Get currentDES VAL field */
      dbStat = dbGetField(&pepscPvt->pCurrentDESDB,DBR_FLOAT,&currentDES,NULL,NULL,NULL);
      if (dbStat) {
      printf("%d: *******Error getting current VAL!*******\n",dbStat);
      return asynError;
      }
      /* Get rampTime VAL field */
      dbStat = dbGetField(&pepscPvt->pRampTimeDB,DBR_LONG,&rampTime,NULL,NULL,NULL);
      if (dbStat) {
	printf("%d: *******Error getting rampTime VAL!*******\n",dbStat);
	return asynError;
      }
      if(pepscPvt->debugLevel) printf("CurrentDES: %f rampTime: %ld\n",currentDES,rampTime);
      Ctype.i=charConv.i;
      dataOut[0]=Ctype.ichar[0];
      Rsp.i='\0';
      dataOut[1]=Rsp.ichar[0];
      Tid.i='\1';
      dataOut[2]=Tid.ichar[0];
      Num.i=1;
      dataOut[3]=Num.ichar[0];
      Chan.i=0x00;
      dataOut[4]=Chan.ichar[0];
      Cur[0].f=currentDES;
      Time[0].i=rampTime*100;
      for (sp=0;sp<Num.i;sp++) {
        dataOut[5+sp*6]=Cur[sp].fchar[0];
        dataOut[6+sp*6]=Cur[sp].fchar[1];
        dataOut[7+sp*6]=Cur[sp].fchar[2];
        dataOut[8+sp*6]=Cur[sp].fchar[3];
        dataOut[9+sp*6]=Time[sp].ichar[0];
        dataOut[10+sp*6]=Time[sp].ichar[1];
      }
      status = pasynOctetSyncIO->flushOnce(pepscPvt->portName,0,"");    
      if (!pepscPvt->simMode) {
	status = pasynOctetSyncIO->flushOnce(pepscPvt->udpPort,0,"");    
        status = pasynOctetSyncIO->writeRawOnce(pepscPvt->udpPort,0,dataOut,5+Num.i*6,1,nbytesTransfered,"") ;
        if (status!=asynSuccess) printf("******Write Error for set current command!*******\n");
      }
      else {
	pdeviceBuffer = &pdeviceInfo->buffer;
        if(nchars>BUFFERSIZE) nchars = BUFFERSIZE;
        if(nchars>0) memcpy(pdeviceBuffer->buffer,data,nchars);
        asynPrintIO(pasynUser,ASYN_TRACEIO_DRIVER,data,nchars,
            "epscWrite nchars %d\n",nchars);
        pdeviceBuffer->nchars = nchars;
        *nbytesTransfered = nchars;
      }
      break;
    case 0xe8: /* Module commands (SHOULD BE NO BREAK BETWEEN HERE AND DEFAULT:) */
      if (!pepscPvt->pModEnableDB.pfield) { /* Get enable/disable module record pointer */
        strcpy(pvStr, pepscPvt->portName);
        strcat(pvStr, ":module:enable.VAL");
        dbStat = dbNameToAddr( pvStr, &pepscPvt->pModEnableDB );
	if (dbStat) {
	  printf("%d: ******NAME ERROR!!!!*******",dbStat);
	  return asynError;
	}
	strcpy(pvStr, pepscPvt->portName);
        strcat(pvStr, ":module:disable.VAL");
        dbStat = dbNameToAddr( pvStr, &pepscPvt->pModDisableDB );
	if (dbStat) {
	  printf("%d: ******NAME ERROR!!!!*******",dbStat);
	  return asynError;
	}
      }
      charConv.ichar[0]=data[2];
      charConv.ichar[1]='\0';
      if (charConv.i==0x01) { /* enable command */
	/* Get module to enable (0 = all) */
	dbStat = dbGetField(&pepscPvt->pModEnableDB,DBR_SHORT,&modInfo,NULL,NULL,NULL);
	charConv2.i=0x10 + (short unsigned int)modInfo;
      }
      else if(charConv.i==0x02) { /*disable command*/
	/* Get module to disable */
	dbStat = dbGetField(&pepscPvt->pModDisableDB,DBR_SHORT,&modInfo,NULL,NULL,NULL);
	charConv2.i=0x20 + (short unsigned int)modInfo;
      }
      else charConv2.i=0x00;
      pdeviceBuffer->buffer[3] = charConv2.ichar[0];
    case 0xc5: /* pwr off */
    case 0xc6: /* pwr on */
      if (!pepscPvt->pCurrentDESDB.pfield) { /* Get cuurentDES record pointer */
        strcpy(pvStr, pepscPvt->portName);
        strcat(pvStr, ":currentDES.VAL");
        dbStat = dbNameToAddr( pvStr, &pepscPvt->pCurrentDESDB );
	if (dbStat) {
	  printf("%d: ******NAME ERROR!!!!*******",dbStat);
	  return asynError;
	}
        strcpy(pvStr, pepscPvt->portName);
        strcat(pvStr, ":pwrOnRev.PROC");
        dbStat = dbNameToAddr( pvStr, &pepscPvt->pPwrOnRev );
        if (dbStat) {
          printf("%d: ******NAME ERROR!!!!*******",dbStat);
          return asynError;
        }
      }
      /* Get currentDES VAL field */
      dbStat = dbGetField(&pepscPvt->pCurrentDESDB,DBR_FLOAT,&currentDES,NULL,NULL,NULL);
      if ( currentDES<0 & charConv.i==0xc6 & !pepscPvt->simMode) { /* use pwr on rev instead */
        memcpy(pdeviceBuffer->buffer,forceReverse,1);
        pdeviceBuffer->nchars = nchars;
        status = pasynOctetSyncIO->flushOnce(pepscPvt->portName,0,"");
        status = pasynOctetSyncIO->flushOnce(pepscPvt->udpPort,0,"");
        status = pasynOctetSyncIO->writeRawOnce(pepscPvt->udpPort,0,pdeviceBuffer->buffer,nchars,0.1,nbytesTransfered,"") ;
        /*printf("command: %x\n",data[0]);
        printf("buffer: %x\n",pdeviceBuffer->buffer[0]);
        printf("nchars: %d strlen: %d\n",nchars,strlen(forceReverse));*/
        break;
        /*printf("command: %x\n",data[0]);*/
      }
    case 0xc7: /* pwr on rev*/
      if (!pepscPvt->pCurrentDB.pfield) { /* Get current record pointer */
        strcpy(pvStr, pepscPvt->portName);
        strcat(pvStr, ":current.VAL");
        dbStat = dbNameToAddr( pvStr, &pepscPvt->pCurrentDB );
	if (dbStat) {
	  printf("%d: ******NAME ERROR!!!!*******",dbStat);
	  return asynError;
	}
      }
      if(pepscPvt->simMode) {
        if(charConv.i==0xc5) {
	  pepscPvt->simPsState=0;
	  ftemp=0;dbPutField(&pepscPvt->pCurrentDB,DBR_FLOAT,&ftemp,1);
	}
        if(charConv.i==0xc6) pepscPvt->simPsState=1;
      }
    default:
      asynPrintIO(pasynUser,ASYN_TRACEIO_DRIVER,data,nchars,
              "epscWrite nchars %d\n",nchars);
      pdeviceBuffer->nchars = nchars;
      /* Write string to EPSC (flush buffers first)*/
      status = pasynOctetSyncIO->flushOnce(pepscPvt->portName,0,"");    
      if (!pepscPvt->simMode) {
        status = pasynOctetSyncIO->flushOnce(pepscPvt->udpPort,0,"");    
        status = pasynOctetSyncIO->writeRawOnce(pepscPvt->udpPort,0,pdeviceBuffer->buffer,nchars,0.1,nbytesTransfered,"") ;
      }
      else {
	pdeviceBuffer = &pdeviceInfo->buffer;
        if(nchars>BUFFERSIZE) nchars = BUFFERSIZE;
        if(nchars>0) memcpy(pdeviceBuffer->buffer,data,nchars);
        asynPrintIO(pasynUser,ASYN_TRACEIO_DRIVER,data,nchars,
            "epscWrite nchars %d\n",nchars);
        pdeviceBuffer->nchars = nchars;
        *nbytesTransfered = nchars;
      }
    }
    return status;
}

static asynStatus epscRead(void *drvPvt,asynUser *pasynUser,
    char *data,size_t maxchars,size_t *nbytesTransfered,int *eomReason)
{
    epscPvt      *pepscPvt = (epscPvt *)drvPvt;
    deviceInfo   *pdeviceInfo;
    deviceBuffer *pdeviceBuffer;
    char         udpReadBuffer[200];
    char         errorStr[32],magnetID[8],serialNum[8],version[8],calDate[8];
    char         *pfrom,*pto;
    char         thisChar;
    int          nremaining;
    int          nout = 0;
    int          addr;
    int          c,itemp;
    long         dbStat;
    char         pvStr[100];
    asynStatus   status;
    short unsigned int statOne, statTwo, numMod, cmdType, ipAddr[4];
    float        modCurrents[8],currentDES, ftemp, current;

    union i_type {
      short unsigned int i;
      char ichar[2];
    } charConv;
     union f_type {
      float f;
      unsigned char fchar[4];
    } fConv;

    if(eomReason) *eomReason=0;
    if(nbytesTransfered) *nbytesTransfered = 0;
    status = pasynManager->getAddr(pasynUser,&addr);
    if(status!=asynSuccess) return status;
    if(!pepscPvt->multiDevice) addr = 0;
    asynPrint(pasynUser, ASYN_TRACE_FLOW,
        "%s epscDriver:read addr %d\n",pepscPvt->portName,addr);
    if(addr<0 || addr>=NUM_DEVICES) {
        epicsSnprintf(pasynUser->errorMessage,pasynUser->errorMessageSize,
            "addr %d is illegal. Must be 0 or 1\n",addr);
        return(0);
    }
    
    pdeviceInfo = &pepscPvt->device[addr];
    if(!pdeviceInfo->connected) {
        asynPrint(pasynUser,ASYN_TRACE_ERROR,
            "%s epscDriver:read device %d not connected\n",
            pepscPvt->portName,addr);
        epicsSnprintf(pasynUser->errorMessage,pasynUser->errorMessageSize,
            "%s epscDriver:read device %d not connected\n",
            pepscPvt->portName,addr);
        return asynError;
    }
    if(pepscPvt->delay>pasynUser->timeout) {
        if(pasynUser->timeout>0.0) epicsThreadSleep(pasynUser->timeout);
        asynPrint(pasynUser, ASYN_TRACE_ERROR,
            "%s epscDriver read timeout\n",pepscPvt->portName);
        epicsSnprintf(pasynUser->errorMessage,pasynUser->errorMessageSize,
            "%s epscDriver read timeout\n",pepscPvt->portName);
        return asynTimeout;
    }

    pdeviceBuffer = &pdeviceInfo->buffer;
    nremaining = pdeviceBuffer->nchars;
    pdeviceBuffer->nchars = 0;
    pfrom = pdeviceBuffer->buffer;
    pto = data;

    /* Read from EPSC port buffer */
    strcpy(data, pdeviceBuffer->buffer);
    charConv.ichar[0]=data[0];
    charConv.ichar[1]='\0';
    if(pepscPvt->debugLevel) printf("Raw Data: %s\n",data);
    if(pepscPvt->debugLevel) printf("Data: %x\n",charConv.i);
    if(pepscPvt->debugLevel) printf("Reading From EPSC device.....\n");
    /* Get pointer structures to required DB records */
    if (!pepscPvt->pCurrentDESDB.pfield) { /* Get cuurentDES record pointer */
      strcpy(pvStr, pepscPvt->portName);
      strcat(pvStr, ":currentDES.VAL");
      dbStat = dbNameToAddr( pvStr, &pepscPvt->pCurrentDESDB );
      if (dbStat) {
	printf("%d: ******NAME ERROR!!!!*******",dbStat);
	return asynError;
      }
    }
    if (!pepscPvt->pStrDB.pfield) {
      strcpy(pvStr, pepscPvt->portName);
      strcat(pvStr, ":errorString.VAL");
      dbStat = dbNameToAddr( pvStr, &pepscPvt->pStrDB );
      strcpy(pvStr, pepscPvt->portName);
      strcat(pvStr, ":status1.VAL");
      dbStat = dbNameToAddr( pvStr, &pepscPvt->pStatOneDB );
      strcpy(pvStr, pepscPvt->portName);
      strcat(pvStr, ":status2.VAL");
      dbStat = dbNameToAddr( pvStr, &pepscPvt->pStatTwoDB );
      strcpy(pvStr, pepscPvt->portName);
      strcat(pvStr, ":status3.VAL");
      dbStat = dbNameToAddr( pvStr, &pepscPvt->pStatThreeDB );
      strcpy(pvStr, pepscPvt->portName);
      strcat(pvStr, ":status4.VAL");
      dbStat = dbNameToAddr( pvStr, &pepscPvt->pStatFourDB );
      strcpy(pvStr, pepscPvt->portName);
      strcat(pvStr, ":current.VAL");
      dbStat = dbNameToAddr( pvStr, &pepscPvt->pCurrentDB );
      strcpy(pvStr, pepscPvt->portName);
      strcat(pvStr, ":module:dataValid.VAL");
      dbStat = dbNameToAddr( pvStr, &pepscPvt->pModValidDB );
      strcpy(pvStr, pepscPvt->portName);
      strcat(pvStr, ":module:number.VAL");
      dbStat = dbNameToAddr( pvStr, &pepscPvt->pModNumDB );
      strcpy(pvStr, pepscPvt->portName);
      strcat(pvStr, ":module:faultFlags.VAL");
      dbStat = dbNameToAddr( pvStr, &pepscPvt->pModFaultDB );
      strcpy(pvStr, pepscPvt->portName);
      strcat(pvStr, ":module:disableFlags.VAL");
      dbStat = dbNameToAddr( pvStr, &pepscPvt->pModDisableStatDB );
      strcpy(pvStr, pepscPvt->portName);
      strcat(pvStr, ":module:currents.VAL");
      dbStat = dbNameToAddr( pvStr, &pepscPvt->pModCurDB );
      strcpy(pvStr, pepscPvt->portName);
      strcat(pvStr, ":chassisConfig.VAL");
      dbStat = dbNameToAddr( pvStr, &pepscPvt->pChassisConfigDB );
      strcpy(pvStr, pepscPvt->portName);
      strcat(pvStr, ":magnetID.VAL");
      dbStat = dbNameToAddr( pvStr, &pepscPvt->pMagnetIDDB );
      strcpy(pvStr, pepscPvt->portName);
      strcat(pvStr, ":ipAddr.VAL");
      dbStat = dbNameToAddr( pvStr, &pepscPvt->pIpAddrDB );
      strcpy(pvStr, pepscPvt->portName);
      strcat(pvStr, ":regTransA_per_V.VAL");
      dbStat = dbNameToAddr( pvStr, &pepscPvt->pRegTransA_per_VDB );
      strcpy(pvStr, pepscPvt->portName);
      strcat(pvStr, ":auxTransA_per_V.VAL");
      dbStat = dbNameToAddr( pvStr, &pepscPvt->pAuxTransA_per_VDB );
      strcpy(pvStr, pepscPvt->portName);
      strcat(pvStr, ":gndCurA_per_V.VAL");
      dbStat = dbNameToAddr( pvStr, &pepscPvt->pGndCurA_per_VDB );
      strcpy(pvStr, pepscPvt->portName);
      strcat(pvStr, ":psV_per_V.VAL");
      dbStat = dbNameToAddr( pvStr, &pepscPvt->pPsV_per_VDB );
      strcpy(pvStr, pepscPvt->portName);
      strcat(pvStr, ":chassisSerialNum.VAL");
      dbStat = dbNameToAddr( pvStr, &pepscPvt->pSerialNumDB );
      strcpy(pvStr, pepscPvt->portName);
      strcat(pvStr, ":firmwareVer.VAL");
      dbStat = dbNameToAddr( pvStr, &pepscPvt->pVersionDB );
      strcpy(pvStr, pepscPvt->portName);
      strcat(pvStr, ":refV.VAL");
      dbStat = dbNameToAddr( pvStr, &pepscPvt->pRefVDB );
      strcpy(pvStr, pepscPvt->portName);
      strcat(pvStr, ":calDate.VAL");
      dbStat = dbNameToAddr( pvStr, &pepscPvt->pCalDateDB );
      strcpy(pvStr, pepscPvt->portName);
      strcat(pvStr, ":auxI.VAL");
      dbStat = dbNameToAddr( pvStr, &pepscPvt->pAuxIDB );
      strcpy(pvStr, pepscPvt->portName);
      strcat(pvStr, ":dacI.VAL");
      dbStat = dbNameToAddr( pvStr, &pepscPvt->pDacIDB );
      strcpy(pvStr, pepscPvt->portName);
      strcat(pvStr, ":ripI.VAL");
      dbStat = dbNameToAddr( pvStr, &pepscPvt->pRipIDB );
      strcpy(pvStr, pepscPvt->portName);
      strcat(pvStr, ":gndI.VAL");
      dbStat = dbNameToAddr( pvStr, &pepscPvt->pGndIDB );
      strcpy(pvStr, pepscPvt->portName);
      strcat(pvStr, ":chassisTemp.VAL");
      dbStat = dbNameToAddr( pvStr, &pepscPvt->pTempDB );
      strcpy(pvStr, pepscPvt->portName);
      strcat(pvStr, ":psV.VAL");
      dbStat = dbNameToAddr( pvStr, &pepscPvt->pPsVDB );
      strcpy(pvStr, pepscPvt->portName);
      strcat(pvStr, ":spareV.VAL");
      dbStat = dbNameToAddr( pvStr, &pepscPvt->pSpareVDB );
      strcpy(pvStr, pepscPvt->portName);
      strcat(pvStr, ":adcOff.VAL");
      dbStat = dbNameToAddr( pvStr, &pepscPvt->pAdcOffDB );
      strcpy(pvStr, pepscPvt->portName);
      strcat(pvStr, ":adcGain.VAL");
      dbStat = dbNameToAddr( pvStr, &pepscPvt->pAdcGainDB );
      strcpy(pvStr, pepscPvt->portName);
      strcat(pvStr, ":dacOff.VAL");
      dbStat = dbNameToAddr( pvStr, &pepscPvt->pDacOffDB );
      strcpy(pvStr, pepscPvt->portName);
      strcat(pvStr, ":dacGain.VAL");
      dbStat = dbNameToAddr( pvStr, &pepscPvt->pDacGainDB );
      strcpy(pvStr, pepscPvt->portName);
      strcat(pvStr, ":lastRst.VAL");
      dbStat = dbNameToAddr( pvStr, &pepscPvt->pLastRstDB );
      strcpy(pvStr, pepscPvt->portName);
      strcat(pvStr, ":lastOff.VAL");
      dbStat = dbNameToAddr( pvStr, &pepscPvt->pLastOffDB );
      strcpy(pvStr, pepscPvt->portName);
      strcat(pvStr, ":calErr.VAL");
      dbStat = dbNameToAddr( pvStr, &pepscPvt->pCalErrDB );
      strcpy(pvStr, pepscPvt->portName);
      strcat(pvStr, ":selfTest.VAL");
      dbStat = dbNameToAddr( pvStr, &pepscPvt->pSelfTestDB );
      strcpy(pvStr, pepscPvt->portName);
      strcat(pvStr, ":digErrLim.VAL");
      dbStat = dbNameToAddr( pvStr, &pepscPvt->pDigErrLimDB );
    }
    /* Get data from EPSC buffer */
    if (charConv.i==0xc0 || charConv.i==0xcf) epicsThreadSleep(0.1); /* wait 100ms if requesting new ADC vals */
    if(!pepscPvt->simMode) {
      status = pasynOctetSyncIO->readRawOnce(pepscPvt->udpPort,0,udpReadBuffer,200,0.9,nbytesTransfered,eomReason,"") ;
    }
    else {
      strcpy(udpReadBuffer,data);
    }
    /* Deal with data read dependent on command type */
    charConv.ichar[0]=udpReadBuffer[0];
    charConv.ichar[1]='\0';
    cmdType = charConv.i;
    switch (charConv.i) {
    case 0xcf: /* Diagnostic Message */
    if(!pepscPvt->simMode) {
       charConv.ichar[0]=udpReadBuffer[4]; charConv.ichar[1]='\0';
       dbPutField(&pepscPvt->pStatOneDB,DBR_SHORT,&charConv.i,1);
       charConv.ichar[0]=udpReadBuffer[5]; charConv.ichar[1]='\0';
       dbPutField(&pepscPvt->pStatTwoDB,DBR_SHORT,&charConv.i,1);
       charConv.ichar[0]=udpReadBuffer[6]; charConv.ichar[1]='\0';
       dbPutField(&pepscPvt->pStatThreeDB,DBR_SHORT,&charConv.i,1);
       charConv.ichar[0]=udpReadBuffer[7]; charConv.ichar[1]='\0';
       dbPutField(&pepscPvt->pStatFourDB,DBR_SHORT,&charConv.i,1);
       for(c=8;c<=11;c++) {
	 fConv.fchar[c-8]=udpReadBuffer[c];
       }
       dbPutField(&pepscPvt->pCurrentDB,DBR_FLOAT,&fConv.f,1);
       for(c=12;c<=15;c++) {
	 fConv.fchar[c-12]=udpReadBuffer[c];
       }
       dbPutField(&pepscPvt->pAuxIDB,DBR_FLOAT,&fConv.f,1);
       for(c=16;c<=19;c++) {
	 fConv.fchar[c-16]=udpReadBuffer[c];
       }
       dbPutField(&pepscPvt->pDacIDB,DBR_FLOAT,&fConv.f,1);
       for(c=20;c<=23;c++) {
	 fConv.fchar[c-20]=udpReadBuffer[c];
       }
       dbPutField(&pepscPvt->pRipIDB,DBR_FLOAT,&fConv.f,1);
       for(c=24;c<=27;c++) {
	 fConv.fchar[c-24]=udpReadBuffer[c];
       }
       dbPutField(&pepscPvt->pGndIDB,DBR_FLOAT,&fConv.f,1);
       for(c=28;c<=31;c++) {
	 fConv.fchar[c-28]=udpReadBuffer[c];
       }
       dbPutField(&pepscPvt->pTempDB,DBR_FLOAT,&fConv.f,1);
       for(c=32;c<=35;c++) {
	 fConv.fchar[c-32]=udpReadBuffer[c];
       }
       dbPutField(&pepscPvt->pPsVDB,DBR_FLOAT,&fConv.f,1);
       for(c=36;c<=39;c++) {
	 fConv.fchar[c-36]=udpReadBuffer[c];
       }
       dbPutField(&pepscPvt->pSpareVDB,DBR_FLOAT,&fConv.f,1);
       charConv.ichar[0]=udpReadBuffer[98]; charConv.ichar[1]=udpReadBuffer[99];
       dbPutField(&pepscPvt->pAdcOffDB,DBR_SHORT,&charConv.i,1);
       charConv.ichar[0]=udpReadBuffer[100]; charConv.ichar[1]=udpReadBuffer[101];
       dbPutField(&pepscPvt->pAdcGainDB,DBR_SHORT,&charConv.i,1);
       charConv.ichar[0]=udpReadBuffer[102]; charConv.ichar[1]=udpReadBuffer[103];
       dbPutField(&pepscPvt->pDacOffDB,DBR_SHORT,&charConv.i,1);
       charConv.ichar[0]=udpReadBuffer[104]; charConv.ichar[1]=udpReadBuffer[105];
       dbPutField(&pepscPvt->pDacGainDB,DBR_SHORT,&charConv.i,1);
       charConv.ichar[0]=udpReadBuffer[106]; charConv.ichar[1]='\0';
       dbPutField(&pepscPvt->pLastRstDB,DBR_SHORT,&charConv.i,1);
       charConv.ichar[0]=udpReadBuffer[107]; charConv.ichar[1]='\0';
       dbPutField(&pepscPvt->pLastOffDB,DBR_SHORT,&charConv.i,1);
       charConv.ichar[0]=udpReadBuffer[108]; charConv.ichar[1]='\0';
       dbPutField(&pepscPvt->pCalErrDB,DBR_SHORT,&charConv.i,1);
       charConv.ichar[0]=udpReadBuffer[109]; charConv.ichar[1]='\0';
       dbPutField(&pepscPvt->pSelfTestDB,DBR_SHORT,&charConv.i,1);
     }
     else { /* Sim Mode */
       charConv.i=1;
       dbPutField(&pepscPvt->pStatOneDB,DBR_SHORT,&charConv.i,1);
       charConv.i=0;
       dbPutField(&pepscPvt->pStatTwoDB,DBR_SHORT,&charConv.i,1);
       dbPutField(&pepscPvt->pStatThreeDB,DBR_SHORT,&charConv.i,1);
       dbPutField(&pepscPvt->pStatFourDB,DBR_SHORT,&charConv.i,1);
       dbStat = dbGetField(&pepscPvt->pCurrentDESDB,DBR_FLOAT,&currentDES,NULL,NULL,NULL);
       dbPutField(&pepscPvt->pStatTwoDB,DBR_SHORT,&pepscPvt->simError,1);
       dbPutField(&pepscPvt->pCurrentDB,DBR_FLOAT,&currentDES,1);
       dbPutField(&pepscPvt->pAuxIDB,DBR_FLOAT,&currentDES,1);
       dbPutField(&pepscPvt->pDacIDB,DBR_FLOAT,&currentDES,1);
       fConv.f=0;
       dbPutField(&pepscPvt->pRipIDB,DBR_FLOAT,&fConv.f,1);
       dbPutField(&pepscPvt->pGndIDB,DBR_FLOAT,&fConv.f,1);
       fConv.f=70;
       dbPutField(&pepscPvt->pTempDB,DBR_FLOAT,&fConv.f,1);
       fConv.f=1;
       dbPutField(&pepscPvt->pPsVDB,DBR_FLOAT,&fConv.f,1);
       dbPutField(&pepscPvt->pSpareVDB,DBR_FLOAT,&fConv.f,1);
       charConv.i=1;
       dbPutField(&pepscPvt->pAdcOffDB,DBR_SHORT,&charConv.i,1);
       dbPutField(&pepscPvt->pAdcGainDB,DBR_SHORT,&charConv.i,1);
       dbPutField(&pepscPvt->pDacOffDB,DBR_SHORT,&charConv.i,1);
       dbPutField(&pepscPvt->pDacGainDB,DBR_SHORT,&charConv.i,1);
       charConv.i=0;
       dbPutField(&pepscPvt->pLastRstDB,DBR_SHORT,&charConv.i,1);
       dbPutField(&pepscPvt->pLastOffDB,DBR_SHORT,&charConv.i,1);
       dbPutField(&pepscPvt->pCalErrDB,DBR_SHORT,&charConv.i,1);
       dbPutField(&pepscPvt->pSelfTestDB,DBR_SHORT,&charConv.i,1);
     }
    break;
    case 0xce: /* Config summary */
      if(!pepscPvt->simMode) {
	for(c=4;c<=11;c++) {
	  magnetID[c-4]=udpReadBuffer[c];
	}
	dbPutField(&pepscPvt->pMagnetIDDB,DBR_STRING,magnetID,1);
	for(c=12;c<=15;c++) {
	  charConv.ichar[0]=udpReadBuffer[c]; charConv.ichar[1]='\0';
	  ipAddr[3-(c-12)]=charConv.i;
	}
	dbPutField(&pepscPvt->pIpAddrDB,DBR_SHORT,ipAddr,4);
	charConv.ichar[0]=udpReadBuffer[30]; charConv.ichar[1]=udpReadBuffer[31];
	dbPutField(&pepscPvt->pChassisConfigDB,DBR_SHORT,&charConv.i,1);
	for(c=32;c<=35;c++) {
	  fConv.fchar[c-32]=udpReadBuffer[c];
	}
	dbPutField(&pepscPvt->pRegTransA_per_VDB,DBR_FLOAT,&fConv.f,1);
	for(c=36;c<=39;c++) {
	  fConv.fchar[c-36]=udpReadBuffer[c];
	}
	dbPutField(&pepscPvt->pAuxTransA_per_VDB,DBR_FLOAT,&fConv.f,1);
	for(c=40;c<=43;c++) {
	  fConv.fchar[c-40]=udpReadBuffer[c];
	}
	dbPutField(&pepscPvt->pGndCurA_per_VDB,DBR_FLOAT,&fConv.f,1);
 	for(c=44;c<=47;c++) {
	  fConv.fchar[c-44]=udpReadBuffer[c];
	}
	dbPutField(&pepscPvt->pPsV_per_VDB,DBR_FLOAT,&fConv.f,1);
	for(c=56;c<=59;c++) {
	  fConv.fchar[c-56]=udpReadBuffer[c];
	}
	dbPutField(&pepscPvt->pDigErrLimDB,DBR_FLOAT,&fConv.f,1);
	for(c=60;c<=67;c++) {
	  serialNum[c-60]=udpReadBuffer[c];
	}
	dbPutField(&pepscPvt->pSerialNumDB,DBR_STRING,serialNum,1);
	for(c=68;c<=75;c++) {
	  version[c-68]=udpReadBuffer[c];
	}
	dbPutField(&pepscPvt->pVersionDB,DBR_STRING,version,1);
	for(c=78;c<=81;c++) {
	  fConv.fchar[c-78]=udpReadBuffer[c];
	}
	dbPutField(&pepscPvt->pRefVDB,DBR_FLOAT,&fConv.f,1);
	for(c=82;c<=89;c++) {
	  calDate[c-82]=udpReadBuffer[c];
	}
	dbPutField(&pepscPvt->pCalDateDB,DBR_STRING,calDate,1);
     }
     else { /* Simulation mode */
	dbPutField(&pepscPvt->pMagnetIDDB,DBR_STRING,"SimMag",1);
	ipAddr[0]=0; ipAddr[1]=0; ipAddr[2]=0; ipAddr[3]=0;
	dbPutField(&pepscPvt->pIpAddrDB,DBR_SHORT,ipAddr,4);
	charConv.i=1;
	dbPutField(&pepscPvt->pChassisConfigDB,DBR_SHORT,&charConv.i,1);
	fConv.f=1;
	dbPutField(&pepscPvt->pRegTransA_per_VDB,DBR_FLOAT,&fConv.f,1);
	dbPutField(&pepscPvt->pAuxTransA_per_VDB,DBR_FLOAT,&fConv.f,1);
	dbPutField(&pepscPvt->pGndCurA_per_VDB,DBR_FLOAT,&fConv.f,1);
	dbPutField(&pepscPvt->pPsV_per_VDB,DBR_FLOAT,&fConv.f,1);
	dbPutField(&pepscPvt->pSerialNumDB,DBR_STRING,"Sim0",1);
	dbPutField(&pepscPvt->pVersionDB,DBR_STRING,"Sim0",1);
	dbPutField(&pepscPvt->pRefVDB,DBR_FLOAT,&fConv.f,1);
	dbPutField(&pepscPvt->pCalDateDB,DBR_STRING,"01011970",1);
	dbPutField(&pepscPvt->pDigErrLimDB,DBR_FLOAT,&fConv.f,1);
      }
      break;
    case 0xe8: /* Module access */
      if(!pepscPvt->simMode) {
        charConv.ichar[0]=udpReadBuffer[4]; charConv.ichar[1]='\0';
        dbPutField(&pepscPvt->pModValidDB,DBR_SHORT,&charConv.i,1);
        charConv.ichar[0]=udpReadBuffer[5]; charConv.ichar[1]='\0';
	numMod=charConv.i;
	dbPutField(&pepscPvt->pModNumDB,DBR_SHORT,&charConv.i,1);
	charConv.ichar[0]=udpReadBuffer[6]; charConv.ichar[1]='\0';
	dbPutField(&pepscPvt->pModFaultDB,DBR_SHORT,&charConv.i,1);
	charConv.ichar[0]=udpReadBuffer[7]; charConv.ichar[1]='\0'; 
	dbPutField(&pepscPvt->pModDisableStatDB,DBR_SHORT,&charConv.i,1);
	for (c=1;c<=numMod;c++) {
	  charConv.ichar[0]=udpReadBuffer[8+(c-1)*2];
	  charConv.ichar[1]=udpReadBuffer[9+(c-1)*2];
	  /*printf("Mod %d: cur: %x\n",c,charConv.i);*/
	  modCurrents[c-1]=(float)charConv.i;
	  modCurrents[c-1]/=2;
	}
        for (c=c;c<=8;c++) {
	  modCurrents[c-1]=0;
        }
        dbPutField(&pepscPvt->pModCurDB,DBR_FLOAT,modCurrents,8);
	}
      else { /* Simulation behaviour */
	itemp=1;dbPutField(&pepscPvt->pModValidDB,DBR_SHORT,&itemp,1);
	itemp=4;dbPutField(&pepscPvt->pModNumDB,DBR_SHORT,&itemp,1);
	itemp=0;dbPutField(&pepscPvt->pModFaultDB,DBR_SHORT,&itemp,1);
	itemp=0;dbPutField(&pepscPvt->pModDisableStatDB,DBR_SHORT,&itemp,1);
	dbStat = dbGetField(&pepscPvt->pCurrentDESDB,DBR_FLOAT,&currentDES,NULL,NULL,NULL);
	dbPutField(&pepscPvt->pModDisableStatDB,DBR_SHORT,&charConv.i,1);
	for (c=1;c<=4;c++) modCurrents[c-1]=currentDES;
        for (c=c;c<=8;c++) modCurrents[c-1]=0;
	dbPutField(&pepscPvt->pModValidDB,DBR_FLOAT,modCurrents,8);
      }
      break;
    case 0xc0: /* Short status check (Get new ADC values) */
    case 0xcd: /* Short status check (Old ADC values) */
    case 0xc1: /* Set PS current */
    case 0xc2: /* Set PS current and put h/w in hold state */
      if(!pepscPvt->simMode) {
        /* Fill Status Bit Fields */
        charConv.ichar[0]=udpReadBuffer[4]; charConv.ichar[1]='\0';
        statOne=charConv.i;
        charConv.ichar[0]=udpReadBuffer[5]; charConv.ichar[1]='\0';
        statTwo=charConv.i;
        if(pepscPvt->debugLevel) printf("status bytes 1 and 2: %d %d\n",statOne,statTwo);
        dbPutField(&pepscPvt->pStatOneDB,DBR_SHORT,&statOne,1);
        dbPutField(&pepscPvt->pStatTwoDB,DBR_SHORT,&statTwo,1);
        /* Get Current Readback */
        for (c=0;c<=3;c++) {
	  fConv.fchar[c] = udpReadBuffer[6+c];
        }
        dbPutField(&pepscPvt->pCurrentDB,DBR_FLOAT,&fConv.f,1);
      }
      else { /* Simulation behaviour */
	if(pepscPvt->simPsState) {
	  itemp=1;dbPutField(&pepscPvt->pStatOneDB,DBR_SHORT,&itemp,1);
	}
	else {
	  itemp=5;dbPutField(&pepscPvt->pStatOneDB,DBR_SHORT,&itemp,1);
	}
	dbStat = dbGetField(&pepscPvt->pCurrentDESDB,DBR_FLOAT,&currentDES,NULL,NULL,NULL);
        dbPutField(&pepscPvt->pStatTwoDB,DBR_SHORT,&pepscPvt->simError,1);
	if(pepscPvt->simPsState&&cmdType==0xc1) dbPutField(&pepscPvt->pCurrentDB,DBR_FLOAT,&currentDES,1);
      }
      break;
    case 0xc9: /* Read error message */
      if(!pepscPvt->simMode) {
	for (c=4;c<=35;c++) {
	  errorStr[c-4]=udpReadBuffer[c];
	}
	if(pepscPvt->debugLevel) printf("Error String: %s\n",errorStr);
	/* Put Error String into errorString record */
	dbPutField(&pepscPvt->pStrDB,DBR_STRING,errorStr,1);
      }
      else { /* Simulation behaviour */
	if(pepscPvt->simError==1) dbPutField(&pepscPvt->pStrDB,DBR_STRING,"Warning: DAC not zero",1);
	else dbPutField(&pepscPvt->pStrDB,DBR_STRING,"MESSAGE BUFFER EMPTY",1);
	pepscPvt->simError=0;
      }
      break;
    case 0xc5: /* Power off */
    case 0xc6: /* Power on */
    case 0xc4: /* Interlock reset */
      if(!pepscPvt->simMode) {
	/* Fill Status Bit Fields */
	charConv.ichar[0]=udpReadBuffer[4]; charConv.ichar[1]='\0';
	statOne=charConv.i;
	charConv.ichar[0]=udpReadBuffer[5]; charConv.ichar[1]='\0';
	statTwo=charConv.i;
	if(pepscPvt->debugLevel) printf("status bytes 1 and 2: %d %d\n",statOne,statTwo);
	dbPutField(&pepscPvt->pStatOneDB,DBR_SHORT,&statOne,1);
	dbPutField(&pepscPvt->pStatTwoDB,DBR_SHORT,&statTwo,1);
      }
      else { /* Simulation behaviour */
	if(pepscPvt->simPsState) {
	  itemp=1;dbPutField(&pepscPvt->pStatOneDB,DBR_SHORT,&itemp,1);
	}
	else {
	  itemp=5; dbPutField(&pepscPvt->pStatOneDB,DBR_SHORT,&itemp,1);
	}
	dbStat = dbGetField(&pepscPvt->pCurrentDESDB,DBR_FLOAT,&currentDES,NULL,NULL,NULL);
        if(currentDES>0 && cmdType==0xc5) pepscPvt->simError=1; /* warning if power off while current set >0 */
	dbPutField(&pepscPvt->pStatTwoDB,DBR_SHORT,&pepscPvt->simError,1);
      }
      break;
    }

    pasynOctetBase->callInterruptUsers(pasynUser,pepscPvt->pasynPvt,
        data,nbytesTransfered,eomReason);
    asynPrintIO(pasynUser,ASYN_TRACEIO_DRIVER,data,nout,
        "epscRead nbytesTransfered %d\n",*nbytesTransfered);

    return status;
}

static asynStatus epscFlush(void *drvPvt,asynUser *pasynUser)
{
    epscPvt *pepscPvt = (epscPvt *)drvPvt;
    deviceInfo *pdeviceInfo;
    deviceBuffer *pdeviceBuffer;
    int          addr;
    asynStatus   status;

    status = pasynManager->getAddr(pasynUser,&addr);
    if(status!=asynSuccess) return status;
    if(!pepscPvt->multiDevice) addr = 0;
    asynPrint(pasynUser, ASYN_TRACE_FLOW,
        "%s epscDriver:flush addr %d\n",pepscPvt->portName,addr);
    if(addr<0 || addr>=NUM_DEVICES) {
        epicsSnprintf(pasynUser->errorMessage,pasynUser->errorMessageSize,
            "addr %d is illegal. Must be 0 or 1\n",addr);
        return(0);
    }
    pdeviceInfo = &pepscPvt->device[addr];
    if(!pdeviceInfo->connected) {
        asynPrint(pasynUser,ASYN_TRACE_ERROR,
            "%s epscDriver:flush device %d not connected\n",
            pepscPvt->portName,addr);
        return -1;
    }
    pdeviceBuffer = &pdeviceInfo->buffer;
    asynPrint(pasynUser,ASYN_TRACE_FLOW,
        "%s epscFlush\n",pepscPvt->portName);
    pdeviceBuffer->nchars = 0;
    return(asynSuccess);
}

static asynStatus setEos(void *drvPvt,asynUser *pasynUser,
     const char *eos,int eoslen)
{
    epscPvt *pepscPvt = (epscPvt *)drvPvt;
    int     i;

    if(eoslen>2 || eoslen<0) {
        epicsSnprintf(pasynUser->errorMessage,pasynUser->errorMessageSize,
            "setEos illegal eoslen %d\n",eoslen);
        return(asynError);
    }
    pepscPvt->eoslen = eoslen;
    for(i=0; i<eoslen; i++) pepscPvt->eos[i] = eos[i];
    asynPrint(pasynUser,ASYN_TRACE_FLOW, "%s setEos\n",pepscPvt->portName);
    return(asynSuccess);
}

static asynStatus getEos(void *drvPvt,asynUser *pasynUser,
    char *eos, int eossize, int *eoslen)
{
    epscPvt *pepscPvt = (epscPvt *)drvPvt;
    int     i;

    *eoslen = pepscPvt->eoslen;
    for(i=0; i<*eoslen; i++) eos[i] = pepscPvt->eos[i];
    asynPrint(pasynUser,ASYN_TRACE_FLOW, "%s setEos\n",pepscPvt->portName);
    return(asynSuccess);
}

/* register epscDriverInit*/
static const iocshArg epscDriverInitArg0 = { "portName", iocshArgString };
static const iocshArg epscDriverInitArg1 = { "delay", iocshArgDouble };
static const iocshArg epscDriverInitArg2 = { "disable auto-connect", iocshArgInt };
static const iocshArg epscDriverInitArg3 = { "multiDevice", iocshArgInt };
static const iocshArg epscDriverInitArg4 = { "PSconn", iocshArgString };
static const iocshArg *epscDriverInitArgs[] = {
    &epscDriverInitArg0,&epscDriverInitArg1,
    &epscDriverInitArg2,&epscDriverInitArg3,
    &epscDriverInitArg4};
static const iocshFuncDef epscDriverInitFuncDef = {
    "epscDriverInit", 5, epscDriverInitArgs};
static void epscDriverInitCallFunc(const iocshArgBuf *args)
{
  epscDriverInit(args[0].sval,args[1].dval,args[2].ival,args[3].ival,args[4].sval);
}

static void epscDriverRegister(void)
{
    static int firstTime = 1;
    if (firstTime) {
        firstTime = 0;
        iocshRegister(&epscDriverInitFuncDef, epscDriverInitCallFunc);
    }
}
epicsExportRegistrar(epscDriverRegister);

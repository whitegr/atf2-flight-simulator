#!/home/atf2-fs/ATF2/control-software/epics-3.14.8/ioc-atf/bin/linux-x86_64/atf

< /home/atf2-fs/ATF2/control-software/epics-3.14.8/ioc-atf/iocBoot/iocatf/envPaths

epicsEnvSet("EPICS_CA_CONN_TMO","60")
epicsEnvSet("EPICS_CA_AUTO_ADDR_LIST","NO")
epicsEnvSet("EPICS_CA_SERVER_PORT","5066")
epicsEnvSet("EPICS_CAS_BEACON_ADDR_LIST","20.10.67.177 slacvme1.atf-local")
epicsEnvSet("EPICS_CA_ADDR_LIST","cbpm-lxs.atf-local:5064 atfcc1.atf-local:5068 atf-lxs2.atf-local:5066 atfcc1.atf-local:5066 atfsv1.atf-local:40000 atf-lxs8.atf-local:5070 20.10.66.9:5064 20.10.67.177:5066 atf-lxs7.atf-local:5064 ccnet17.atf-local slacvme1.atf-local:5064 lxs-motr.atf-local:5064 slacvme1.atf-local:5064 slacvme2.atf-local:5064 slacvme3.atf-local:5064")
epicsEnvSet("EPICS_CA_MAX_ARRAY_BYTES","50000000")

cd ${TOP}

## Register all support components
dbLoadDatabase("dbd/atf.dbd")
atf_registerRecordDeviceDriver(pdbbase)

# IOC ADMIN
dbLoadRecords("db/iocAdminSoft.db","IOC=IOC-ATF")

## - Initialise asyn ports
# atfDriverInit(PORT_NAME,SERVER_IP_ADDR,SERVER_PORT,SIM_MODE=0|1)
# atfDriverInit("atf","20.10.71.44",50101,0)
# atfDriverInit("atf_arr","20.10.71.44",50101,0)
< iocBoot/iocatf/extBpm_asynPorts

## Load record instances
dbLoadRecords("db/atfepics.vdb","SIM_MODE=0")
dbLoadRecords("db/fsecs.vdb")
#dbLoadRecords("db/atf.vdb","SIM_MODE=0")
dbLoadRecords("db/atf2.vdb","SIM_MODE=0")
dbLoadRecords("db/bkgmon.db","caprefix=BCKMON:AGILSCOPE1")
dbLoadRecords("db/bkgmon.db","caprefix=BCKMON:AGILSCOPE2")

< iocBoot/iocatf/bpmDefs2
< iocBoot/iocatf/extBpmDb
#< iocBoot/iocatf/magDefsTemp
< iocBoot/iocatf/profDefs
#dbLoadRecords("db/drbpm.vdb","BPMNAME=MB1R,XINDX=3,YINDX=5,TMITINDX=1,cratenum=1")

# Access Security
#asSetFilename("${TOP}/../AccessSecurity.acf")

# EXT Stripline BPM processing
#dbLoadRecords("db/extStriplineProc.db","bpmname=MQF1X,wfsize=3000,lwf=slacvme1:sis1:waveform0,rwf=slacvme1:sis1:waveform1,uwf=slacvme1:sis1:waveform2,dwf=slacvme1:sis1:waveform3,anal=SDEV")
dbLoadRecords("db/extStriplineProc.db","bpmname=MQF1X,wfsize=1024,lwf=slacvme1:sdigi2:Cavity_BPM:CH1,rwf=slacvme1:sdigi2:Cavity_BPM:CH2,uwf=slacvme1:sdigi2:Cavity_BPM:CH3,dwf=slacvme1:sdigi2:Cavity_BPM:CH4,anal=SDEV")
#dbLoadRecords("db/extStriplineProc.db","bpmname=MQD2X,wfsize=3000,lwf=slacvme1:sis1:waveform4,rwf=slacvme1:sis1:waveform5,uwf=slacvme1:sis1:waveform6,dwf=slacvme1:sis1:waveform7,anal=SDEV")
dbLoadRecords("db/extStriplineProc.db","bpmname=MQD2X,wfsize=1024,lwf=slacvme1:sdigi3:Cavity_BPM:CH1,rwf=slacvme1:sdigi3:Cavity_BPM:CH2,uwf=slacvme1:sdigi3:Cavity_BPM:CH3,dwf=slacvme1:sdigi3:Cavity_BPM:CH4,anal=SDEV")
#dbLoadRecords("db/extStriplineProc.db","bpmname=MQF3X,wfsize=3000,lwf=slacvme1:sis2:waveform0,rwf=slacvme1:sis2:waveform1,uwf=slacvme1:sis2:waveform2,dwf=slacvme1:sis2:waveform3,anal=SDEV")
dbLoadRecords("db/extStriplineProc.db","bpmname=MQF3X,wfsize=1024,lwf=slacvme1:sdigi4:Cavity_BPM:CH1,rwf=slacvme1:sdigi4:Cavity_BPM:CH2,uwf=slacvme1:sdigi4:Cavity_BPM:CH3,dwf=slacvme1:sdigi4:Cavity_BPM:CH4,anal=SDEV")
#dbLoadRecords("db/extStriplineProc.db","bpmname=MQF4X,wfsize=3000,lwf=slacvme1:sis2:waveform4,rwf=slacvme1:sis2:waveform5,uwf=slacvme1:sis2:waveform6,dwf=slacvme1:sis2:waveform7,anal=SDEV")
dbLoadRecords("db/extStriplineProc.db","bpmname=MQF4X,wfsize=1024,lwf=slacvme1:sdigi5:Cavity_BPM:CH1,rwf=slacvme1:sdigi5:Cavity_BPM:CH2,uwf=slacvme1:sdigi5:Cavity_BPM:CH3,dwf=slacvme1:sdigi5:Cavity_BPM:CH4,anal=SDEV")
#dbLoadRecords("db/extStriplineProc.db","bpmname=MQD5X,wfsize=3000,lwf=slacvme1:sis3:waveform0,rwf=slacvme1:sis3:waveform1,uwf=slacvme1:sis3:waveform2,dwf=slacvme1:sis3:waveform3,anal=SDEV")
dbLoadRecords("db/extStriplineProc.db","bpmname=MQD5X,wfsize=1024,lwf=slacvme1:sdigi6:Cavity_BPM:CH1,rwf=slacvme1:sdigi6:Cavity_BPM:CH2,uwf=slacvme1:sdigi6:Cavity_BPM:CH3,dwf=slacvme1:sdigi6:Cavity_BPM:CH4,anal=SDEV")
#dbLoadRecords("db/extStriplineProc.db","bpmname=MQF6X,wfsize=3000,lwf=slacvme1:sis3:waveform4,rwf=slacvme1:sis3:waveform5,uwf=slacvme1:sis3:waveform6,dwf=slacvme1:sis3:waveform7,anal=SDEV")
dbLoadRecords("db/extStriplineProc.db","bpmname=MQF6X,wfsize=1024,lwf=slacvme1:sdigi7:Cavity_BPM:CH1,rwf=slacvme1:sdigi7:Cavity_BPM:CH2,uwf=slacvme1:sdigi7:Cavity_BPM:CH3,dwf=slacvme1:sdigi7:Cavity_BPM:CH4,anal=SDEV")
#dbLoadRecords("db/extStriplineProc.db","bpmname=MQF7X,wfsize=3000,lwf=slacvme1:sis4:waveform0,rwf=slacvme1:sis4:waveform1,uwf=slacvme1:sis4:waveform2,dwf=slacvme1:sis4:waveform3,anal=SDEV")
dbLoadRecords("db/extStriplineProc.db","bpmname=MQF7X,wfsize=1024,lwf=slacvme1:sdigi8:Cavity_BPM:CH1,rwf=slacvme1:sdigi8:Cavity_BPM:CH2,uwf=slacvme1:sdigi8:Cavity_BPM:CH3,dwf=slacvme1:sdigi8:Cavity_BPM:CH4,anal=SDEV")
#dbLoadRecords("db/extStriplineProc.db","bpmname=MQD8X,wfsize=3000,lwf=slacvme1:sis4:waveform4,rwf=slacvme1:sis4:waveform5,uwf=slacvme1:sis4:waveform6,dwf=slacvme1:sis4:waveform7,anal=SDEV")
dbLoadRecords("db/extStriplineProc.db","bpmname=MQD8X,wfsize=1024,lwf=slacvme1:sdigi9:Cavity_BPM:CH1,rwf=slacvme1:sdigi9:Cavity_BPM:CH2,uwf=slacvme1:sdigi9:Cavity_BPM:CH3,dwf=slacvme1:sdigi9:Cavity_BPM:CH4,anal=SDEV")
#dbLoadRecords("db/extStriplineProc.db","bpmname=MQF9X,wfsize=3000,lwf=slacvme1:sis5:waveform0,rwf=slacvme1:sis5:waveform1,uwf=slacvme1:sis5:waveform2,dwf=slacvme1:sis5:waveform3,anal=SDEV")
dbLoadRecords("db/extStriplineProc.db","bpmname=MQF9X,wfsize=1024,lwf=slacvme1:sdigi10:Cavity_BPM:CH1,rwf=slacvme1:sdigi10:Cavity_BPM:CH2,uwf=slacvme1:sdigi10:Cavity_BPM:CH3,dwf=slacvme1:sdigi10:Cavity_BPM:CH4,anal=SDEV")
#dbLoadRecords("db/extStriplineProc.db","bpmname=MQF13X,wfsize=3000,lwf=slacvme1:sis5:waveform4,rwf=slacvme1:sis5:waveform5,uwf=slacvme1:sis5:waveform6,dwf=slacvme1:sis5:waveform7,anal=SDEV")
dbLoadRecords("db/extStriplineProc.db","bpmname=MQF13X,wfsize=1024,lwf=slacvme1:sdigi11:Cavity_BPM:CH1,rwf=slacvme1:sdigi11:Cavity_BPM:CH2,uwf=slacvme1:sdigi11:Cavity_BPM:CH3,dwf=slacvme1:sdigi11:Cavity_BPM:CH4,anal=SDEV")
#dbLoadRecords("db/extStriplineProc.db","bpmname=MQD14X,wfsize=3000,lwf=slacvme1:sis6:waveform0,rwf=slacvme1:sis6:waveform1,uwf=slacvme1:sis6:waveform2,dwf=slacvme1:sis6:waveform3,anal=SDEV")
dbLoadRecords("db/extStriplineProc.db","bpmname=MQD14X,wfsize=1024,lwf=slacvme1:sdigi12:Cavity_BPM:CH1,rwf=slacvme1:sdigi12:Cavity_BPM:CH2,uwf=slacvme1:sdigi12:Cavity_BPM:CH3,dwf=slacvme1:sdigi12:Cavity_BPM:CH4,anal=SDEV")
#dbLoadRecords("db/extStriplineProc.db","bpmname=MQF15X,wfsize=3000,lwf=slacvme1:sis6:waveform4,rwf=slacvme1:sis6:waveform5,uwf=slacvme1:sis6:waveform6,dwf=slacvme1:sis6:waveform7,anal=SDEV")
dbLoadRecords("db/extStriplineProc.db","bpmname=MQF15X,wfsize=1024,lwf=slacvme1:sdigi13:Cavity_BPM:CH1,rwf=slacvme1:sdigi13:Cavity_BPM:CH2,uwf=slacvme1:sdigi13:Cavity_BPM:CH3,dwf=slacvme1:sdigi13:Cavity_BPM:CH4,anal=SDEV")
dbLoadRecords("db/extStriplineProc.db","bpmname=MFB1FF,wfsize=1024,lwf=slacvme1:sdigi14:Cavity_BPM:CH1,rwf=slacvme1:sdigi14:Cavity_BPM:CH2,uwf=slacvme1:sdigi14:Cavity_BPM:CH3,dwf=slacvme1:sdigi14:Cavity_BPM:CH4,anal=SDEV")
#dbLoadRecords("db/drBpmProc.db","bpmname=MB2X,wfsize=1024,brwf=slacvme1:sdigi14:Cavity_BPM:CH1,blwf=slacvme1:sdigi14:Cavity_BPM:CH2,tlwf=slacvme1:sdigi14:Cavity_BPM:CH3,trwf=slacvme1:sdigi14:Cavity_BPM:CH4,anal=SDEV")
#dbLoadRecords("db/extStriplineProc.db","bpmname=MFB1FF,wfsize=1024,lwf=slacvme1:sis7:waveform0,rwf=slacvme1:sis7:waveform1,uwf=slacvme1:sis7:waveform2,dwf=slacvme1:sis7:waveform3,anal=SDEV")

# OTR status records
dbLoadRecords("db/otrStatus.db","N=1,TARGET=mOTR:XPS1Aux3Bo2.RVAL,XV=mOTR:XPS1AuxAi0,YV=mOTR:XPS1AuxAi1,VERR=0.1,REFX=5.5,REFY=7.268")
dbLoadRecords("db/otrStatus.db","N=2,TARGET=mOTR:XPS1Aux3Bo3.RVAL,XV=mOTR:XPS1AuxAi2,YV=mOTR:XPS1AuxAi3,VERR=0.1,REFX=5.12,REFY=7.2")
dbLoadRecords("db/otrStatus.db","N=3,TARGET=mOTR:XPS2Aux3Bo2.RVAL,XV=mOTR:XPS2AuxAi0,YV=mOTR:XPS2AuxAi1,VERR=0.1,REFX=6.48,REFY=7.18")
dbLoadRecords("db/otrStatus.db","N=4,TARGET=mOTR:XPS2Aux3Bo3.RVAL,XV=mOTR:XPS2AuxAi2,YV=mOTR:XPS2AuxAi3,VERR=0.1,REFX=6.97,REFY=7.2")

cd ${TOP}/iocBoot/${IOC}

## Debug info
#asynSetTraceMask("atf",0,0xff)
#asynSetTraceIOMask("atf",0,0x2)

iocInit()

# Dump db records into record file
dbl > dbList.txt

## Start any sequence programs
#seq sncExample,"user=whitegrHost"

#!../../bin/linux-x86/atf

< envPaths


# setup environment variables for this IOC
epicsEnvSet("EPICS_CA_AUTO_ADDR_LIST","NO")
epicsEnvSet("EPICS_CA_SERVER_PORT","${FS_PORT}"
epicsEnvSet("EPICS_CAS_BEACON_ADDR_LIST","localhost")

cd ${TOP}


## Register all support components
dbLoadDatabase("dbd/atf.dbd")
atf_registerRecordDeviceDriver(pdbbase)

## - Initialise asyn ports
## atfDriverInit(PORT_NAME,SERVER_IP_ADDR,SERVER_PORT,SIM_MODE=0|1)
atfDriverInit("atf","20.10.71.44",50101,1)
atfDriverInit("atf_arr","20.10.71.44",50101,1)

## Load record instances
dbLoadRecords("db/atf.vdb","SIM_MODE=1")
dbLoadRecords("db/fsecs.vdb")
< iocBoot/iocatf/cbpmsimDefs
< iocBoot/iocatf/bpmDefs
< iocBoot/iocatf/magDefs
< iocBoot/iocatf/magsimDefs
< iocBoot/iocatf/profDefs
dbLoadRecords("db/atfcbpm_ref.vdb")

# Access Security
#asSetFilename("${TOP}/../AccessSecurity.acf")

cd ${TOP}/iocBoot/${IOC}

## Debug info
#asynSetTraceMask("atf",0,0xff)
#asynSetTraceIOMask("atf",0,0x2)

iocInit()

# Dump db records into record file
dbl > dbList.txt

## Start any sequence programs
#seq sncExample,"user=whitegrHost"

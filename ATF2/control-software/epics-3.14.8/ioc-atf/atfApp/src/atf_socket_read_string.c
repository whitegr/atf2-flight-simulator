#include <stdio.h>
#include <string.h>
#include "atf_socket.h"
/*
!==============================================================================

    Possible return code

        IATF_SOCKET_NORMAL
        IATF_SOCKET_ERROR
        IATF_SOCKET_TIMEOUT
        IATF_SOCKET_DISCONNECTED
        IATF_SOCKET_INVALID_DATA
	IATF_SOCKET_INVALID_TYPE

!==============================================================================
*/
int atf_socket_read_string( int 	sock,
			    char 	*string,
			    int 	*string_length,
			    int 	timeout )
{
	#define		WRITE	1
	static char	buffer[MX_NANOBPM_SIZE*4];
	int			status, err, truncated, receipt_state;

/* Receive buffer from socket.
   The value of status is sent if atf_socket_read() is error.*/

	status = atf_socket_read( sock, &buffer[0], timeout, &err );
	if( status != IATF_SOCKET_NORMAL ){

	    return( status );
	}

/* Copy buffer data field into string.
   +1 for the NULL character.*/

	memcpy( string, &buffer[32*4], sizeof(MX_NANOBPM_SIZE-32)*4 );


	return( IATF_SOCKET_NORMAL );

}


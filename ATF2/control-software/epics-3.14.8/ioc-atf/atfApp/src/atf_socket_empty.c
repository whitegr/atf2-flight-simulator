#include <stdio.h>
#include <netdb.h>
#include <string.h>
#include <unistd.h>
#include <errno.h>
#include "atf_socket.h"
/*
!==============================================================================

    Possible return code

        IATF_SOCKET_NORMAL
        IATF_SOCKET_ERROR
        IATF_SOCKET_TIMEOUT
        IATF_SOCKET_DISCONNECTED
        IATF_SOCKET_INVALID_DATA

!==============================================================================
*/
int atf_socket_empty( int sock, int *err )
{
	#define		READ		0
	#define		TIMEOUT0	0
	#define		REMAINING_SIZE	65535
	int	status, rc;
	int	remaining_size;
	char	dummy[REMAINING_SIZE];
	int	timeout;

	while(1){

	    status = atf_socket_ready( sock, READ, TIMEOUT0, err );

	    if( status != IATF_SOCKET_NORMAL ){

		if( status == IATF_SOCKET_TIMEOUT ){
		    return( IATF_SOCKET_NORMAL );
		} else {
		    return( IATF_SOCKET_ERROR );
		}

	    }	

/* When connection is cut, ECONNRESET may be set to errno by read().
   When connection is cut, EINVAL may be set to errno by read().*/

	    rc = read( sock, dummy, REMAINING_SIZE );

	    if( rc <= 0 ){
		if( rc == 0 ){
		    return( IATF_SOCKET_DISCONNECTED );
		} else if( rc < 0 ){
		    *err = errno;
		    if( *err == ECONNRESET ){
			return( IATF_SOCKET_DISCONNECTED );
		    } else if( *err == EINVAL ){
			return( IATF_SOCKET_DISCONNECTED );
		    } else {
			return( IATF_SOCKET_ERROR );
		    }
		}       
	    }
	
	}

}

#include <stdio.h>
#include "atf_socket.h"

int atf_socket_close( int sock )
{
	if( shutdown( sock, 2 ) < 0 ) return(IATF_SOCKET_ERROR);

	close( sock );

	return( IATF_SOCKET_NORMAL );

}


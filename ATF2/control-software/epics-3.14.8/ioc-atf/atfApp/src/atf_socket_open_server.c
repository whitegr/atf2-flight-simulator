#include <stdio.h>
#include <netdb.h>
#include <errno.h>

int atf_socket_open_server( int *sock, int port )
{
	struct hostent	*shost;
	char shostname[40];
	struct sockaddr_in	sv_addr;

	// サーバーのホスト名取得
	if( gethostname( shostname, sizeof(shostname) ) < 0 ){
	    atf_err("atf_socket_open_server","gethostname");
	    return(0);
	}

	//ホスト名からIPアドレスを取得
	shost = gethostbyname( shostname );
	if( shost == NULL ){
	    atf_err("atf_socket_open_server","gethostbyname");
	    return(0);
	}

	// ソケット作成(TCP)
	*sock = socket( AF_INET, SOCK_STREAM, 0 );
	if( *sock < 0 ){
	    atf_err("atf_socket_open_server","socket");
	    return(0);
	}

	// ソケットに名前をつける
	bzero( (char *)&sv_addr, sizeof(sv_addr) );
	sv_addr.sin_family = AF_INET;
	sv_addr.sin_port   = htons(port);
	memcpy( (char *)&sv_addr.sin_addr,
		(char *)shost->h_addr, shost->h_length );

	if( bind( *sock, (struct sockaddr *)&sv_addr, sizeof(sv_addr))<0){
	    atf_err("atf_socket_open_server", "bind");
	    perror("bind");
	    printf("errno=%d\n",errno);
	    return(0);
	}

	// コネクション要求の最大待ち数を設定
	if( listen( *sock, SOMAXCONN ) == -1 ){
	    atf_err("atf_socket_open_server", "listen");
	    return(0);
	}

	return(1);
}

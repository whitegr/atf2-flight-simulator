#include <stdio.h>
#include <string.h>
#include "atf_socket.h"
/*
!==============================================================================

    Possible return code

        IATF_SOCKET_NORMAL
        IATF_SOCKET_ERROR
        IATF_SOCKET_TIMEOUT
        IATF_SOCKET_DISCONNECTED
	IATF_SOCKET_INVALID_TYPE

!==============================================================================
*/
int atf_socket_write_int( int 	sock,
			     int 	*data,
			     int 	*ndata,
			     int 	timeout )
{
	#define		READ	0
	static SOCKET_BUFFER	buffer;
	int			status, err,
				confirm_header_id, receipt_state;

/* Copy data into buffer data field.*/

        memcpy( &buffer.data.i, data, *ndata * sizeof( int ) );

/* Set buffer code into header field.*/

	buffer.header.code = htonl( HEADER_CODE );

/* Set buffer id into header field.*/

	buffer.header.id += 1;
	if( buffer.header.id> MX_BUFFER_ID ) buffer.header.id = 1;

/* Set buffer type into header field.*/

	buffer.header.type  = DATA_TYPE_INT;

/* Set buffer size into header field.*/

	buffer.header.nbyte = *ndata * sizeof( int ) + MX_HEADER_BYTE;

/* Send buffer to socket.*/

	status = atf_socket_write( sock, &buffer, timeout, &err );
	if( status != IATF_SOCKET_NORMAL ) return( status );

/* Receive header_id from socket.*/

	status = atf_socket_confirm( sock, READ, &confirm_header_id,
		&receipt_state, &err );
	if( status != IATF_SOCKET_NORMAL ) return( status );

/* header_id is compared with sent confirm_header_id.*/

	if( buffer.header.id != confirm_header_id ) return( IATF_SOCKET_ERROR );

/* When receipt_state is not IATF_SOCKET_NORMAL, communication is not
   performed normally.*/

	if( receipt_state != IATF_SOCKET_NORMAL ){

/* If receipt_state is IATF_SOCKET_INVALID_TYPE, the data types by the side
   of read differ.*/

	    if( receipt_state == IATF_SOCKET_INVALID_TYPE ){
		return( IATF_SOCKET_INVALID_TYPE );
	    } else {
		return( IATF_SOCKET_ERROR );
	    }
	}

	return( IATF_SOCKET_NORMAL );

}

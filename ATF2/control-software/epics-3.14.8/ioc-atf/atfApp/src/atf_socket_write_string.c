#include <stdio.h>
#include <string.h>
#include "atf_socket.h"
/*
!==============================================================================

    Possible return code

        IATF_SOCKET_NORMAL
        IATF_SOCKET_ERROR
        IATF_SOCKET_TIMEOUT
        IATF_SOCKET_DISCONNECTED
	IATF_SOCKET_INVALID_TYPE

!==============================================================================
*/
int atf_socket_write_string( int 	sock,
			     char 	*string,
			     int 	*string_length,
			     int 	timeout )
{
	#define		READ	0
	static char	buffer[MX_NANOBPM_SIZE*4];
	int			status, err, buffer_byte, truncated,
				confirm_header_id, receipt_state;


/* Copy string into buffer data field.
 +1 for the NULL character.
*/
	memcpy( &buffer[32*4], string, *string_length + 1 );

/* Send buffer to socket.
*/
	status = atf_socket_write( sock, &buffer[0], timeout, &err );
	if( status != IATF_SOCKET_NORMAL ) return( status );

	return( IATF_SOCKET_NORMAL );
}

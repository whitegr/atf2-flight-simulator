#include <stdio.h>
#include <string.h>
#include <netdb.h>
#include <errno.h>
#include <netinet/in.h>
#include "atf_socket.h"
/*

!===============================================================================

    Server makes the preparations which open a socket.

    Socket description is stored in the 1nd argument.
    Port number is specified to be the 2st argument.
    
    Possible return code

	IATF_SOCKET_NORMAL
	IATF_SOCKET_ERROR

!===============================================================================
*/
int atf_socket_server_open( int *sock, int port )
{
	int		    ON = 1;
	struct hostent	    *shost;
	struct sockaddr_in  sv_addr;
	int		    status;
	char		    hostname[40];

	errno = 0;

/* Read the fully qualified name of the local host.*/

	status = gethostname( hostname, sizeof(hostname) );
	if( status < 0 ) return( IATF_SOCKET_ERROR );

/* Searches the hosts database for a host record with a given name or alias.*/

	shost = gethostbyname( hostname );
	if( shost == NULL ) return( IATF_SOCKET_ERROR );

/* Creates an end point for communication by returning a soket descriptor.*/

	*sock = socket( AF_INET, SOCK_STREAM, 0 );
	if( *sock < 0 ) return( IATF_SOCKET_ERROR );

	bzero( &sv_addr, sizeof( sv_addr ) );
	sv_addr.sin_family = AF_INET;
	sv_addr.sin_port = htons( port );
	memcpy( &sv_addr.sin_addr, shost->h_addr, shost->h_length );

/* Sets options on a socket.
!-------------------------------------------------------------------------------
   SOL_SOCKET   ... Set the options at the socket level.

   SO_REUSEADDR ... Specifies that the rules used in validating addresses
                    supplied by a bind() function should allow reuse of
                    local address.
!-------------------------------------------------------------------------------
*/
	status = setsockopt( *sock, SOL_SOCKET, SO_REUSEADDR, &ON, sizeof(ON));
	if( status < 0 ) return( IATF_SOCKET_ERROR );

/* Binds a name to a socket.*/

	status = bind( *sock, (struct sockaddr *)&sv_addr, sizeof(sv_addr) );
	if( status < 0 ) return( IATF_SOCKET_ERROR );

/* Sets the maximum limit of outstanding connection requests for a TCP socket.*/

	status = listen( *sock, SOMAXCONN );
	if( status < 0 ) return( IATF_SOCKET_ERROR );

	return( IATF_SOCKET_NORMAL );

}


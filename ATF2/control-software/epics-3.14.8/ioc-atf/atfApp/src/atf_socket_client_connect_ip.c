#include <netdb.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <netinet/in.h>
#include "atf_socket.h"
/*
!===============================================================================

    It is connectable with a server by specifying an IP address.

    Ip-address is specified to be the 1st argument.
    Socket description is stored in the 2nd argument.
    Port number is specified to be the 3st argument.

    Possible return code

	IATF_SOCKET_NORMAL
	IATF_SOCKET_ERROR
	IATF_SOCKET_TIMEOUT
	IATF_SOCKET_ECONNREFUSED

!===============================================================================
*/

int atf_socket_client_connect_ip( char *ip_addr, int *sock, int port )
{
        struct	sockaddr_in	sv_addr;
	struct 	hostent		*shost;
	int			status;

	errno = 0;

/* Socket is created.
*/
	*sock = socket( AF_INET, SOCK_STREAM, 0 );
	if( *sock < 0 ) return( IATF_SOCKET_ERROR );

	bzero( &sv_addr, sizeof( sv_addr ) );
	sv_addr.sin_family	   = AF_INET;
        sv_addr.sin_port	   = htons( port );
        sv_addr.sin_addr.s_addr = inet_addr( ip_addr );

/* The connection demand of a socket.
*/
	status = connect( *sock, (struct sockaddr *)&sv_addr, sizeof(sv_addr));
	if( status != 0 ){
	    if( errno == ECONNREFUSED ){
		return( IATF_SOCKET_ECONNREFUSED );
	    } else {
		return( IATF_SOCKET_ERROR );
	    }
	}

	return( IATF_SOCKET_NORMAL );
}

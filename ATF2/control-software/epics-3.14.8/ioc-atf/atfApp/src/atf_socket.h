/*!===============================================================================
    The parameter currently used by socket function
!===============================================================================*/

#define DATA_TYPE_STRING        0
#define DATA_TYPE_INT           1
#define DATA_TYPE_FLOAT         2
#define DATA_TYPE_SHORT         3

/*!===============================================================================
    socket data structure.
!===============================================================================*/

#define HEADER_CODE             123456
#define MX_BUFFER_ID            10000

#define MX_BUFFER_BYTE          65535
#define MX_HEADER_BYTE          128
#define MX_DATA_BYTE            MX_BUFFER_BYTE - MX_HEADER_BYTE

#define MX_I_ARRAY_SIZE         16351   
#define MX_F_ARRAY_SIZE         16351  
#define MX_S_ARRAY_SIZE         32703   
#define MX_C_ARRAY_SIZE         65407  

typedef struct {

  struct {
    int		code;
    unsigned int	id;
    int		receipt_state;
    int		type;
    int		nbyte;
    char		reserve[108];
  } header;
  
  union {
    int             i[MX_I_ARRAY_SIZE];
    float           f[MX_F_ARRAY_SIZE];
    short           s[MX_S_ARRAY_SIZE];
    char            c[MX_C_ARRAY_SIZE];
  } data;
  
} SOCKET_BUFFER;

#define MX_NANOBPM_SIZE 1032
/*!===============================================================================
    socket function
!===============================================================================*/

int atf_socket_server_open( int *sock, int port );
int atf_socket_server_accept( int sock, int *nsock );

int atf_socket_client_connection( char *server, int *sock, int port);

int atf_socket_ready( int sock, int readwrite, int timeout, int *err );
int atf_socket_empty( int sock, int *err );
int atf_socket_confirm( int sock, int readwrite, int *confirm_id, int *receipt_state,int *err );
int atf_socket_close( int sock );

int atf_socket_read( int sock, char *buffer, int timeout, int *err );
int atf_socket_read_int( int sock, int *data, int *ndata, int timeout );
int atf_socket_read_float( int sock, float *data, int *ndata, int timeout );
int atf_socket_read_short( int sock, short *data, int *ndata, int timeout );
int atf_socket_read_string( int sock, char *string, int *string_length, int timeout );

int atf_socket_write( int sock, char *write_data, int timeout, int *err );
int atf_socket_write_int( int sock, int *data, int *ndata, int timeout );
int atf_socket_write_float( int sock, float *data, int *ndata, int timeout );
int atf_socket_write_short( int sock, short *data, int *ndata, int timeout );
int atf_socket_write_string( int sock, char *string, int *string_length, int timeout );

int atf_socket_client_connect_ip( char *ip_addr, int *sock, int port);

/*!===============================================================================
    socket function return code.
!===============================================================================*/

#define     IATF_SOCKET_NORMAL          0x00000001

#define     IATF_SOCKET_ERROR           0x00000000
#define     IATF_SOCKET_TIMEOUT         0x00000002
#define     IATF_SOCKET_DISCONNECTED    0x00000004
#define     IATF_SOCKET_INVALID_DATA    0x00000008
#define     IATF_SOCKET_INVALID_TYPE    0x00000010
#define     IATF_SOCKET_ECONNREFUSED    0X00000020

#define     IATF_SOCKET_INVALID_ARG1    0x00000040
#define     IATF_SOCKET_INVALID_ARG2    0x00000080
#define     IATF_SOCKET_INVALID_ARG3    0x00000100
#define     IATF_SOCKET_INVALID_ARG4    0x00000200
#define     IATF_SOCKET_INVALID_ARG5    0x00000400

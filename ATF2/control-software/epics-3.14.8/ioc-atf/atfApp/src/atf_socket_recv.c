#include <stdio.h>
#include <netdb.h>
#include "atf_socket.h"

#define DEBUG 1

int atf_socket_recv( int sock, void *data, int type, int *ndata, int timeout )
{
	SOCKET_DATA read_data;
	int sc, rc, status, read_status;
	int data_size;
	int read_size;
	char buf[128];

	status = atf_socket_wait_read( sock, timeout );
	if( status != 1 ){
		if( status == 2 ){
			return(2);
		}
		else{
			atf_err("atf_socket_recv","atf_socket_wait_read");
			return(0);
		}
	}

	rc = read( sock, &read_data.header, sizeof(read_data.header) );
	if( rc <= 0 ){
		atf_err("atf_socket_recv","header read");
		return(0);
	}

	if( read_data.header.type != type ){
		atf_err("atf_socket_recv","data type");
		return(0);
	}

	if( type == DATA_TYPE_STRING ){
		data_size = sizeof(char) * read_data.header.ndata;
	}
	else if( type == DATA_TYPE_INT ){
		data_size = sizeof(int) * read_data.header.ndata;
	}
	else if( type == DATA_TYPE_FLOAT ){
		data_size = sizeof(float) * read_data.header.ndata;
	}
	else if( type == DATA_TYPE_SHORT ){
		data_size = sizeof(short) * read_data.header.ndata;
	}
	else{
		atf_err("atf_socket_recv","data type");
		return(0);
	}
	*ndata = read_data.header.ndata;

	read_size = 0;
	read_status = 0;
	while(1){
		status = atf_socket_wait_read( sock, 10 );
		if( status != 1 ){
			atf_err("atf_socket_recv","atf_socket_wait_read");
			return(0);
		}

		rc = read( sock, &read_data.data[0]+read_size, data_size-read_size );
		if( rc <= 0 ){
			atf_err("atf_socket_recv","data read");
			return(0);
		}


		read_size += rc;

		if( read_size >= data_size ){
			read_status = 1;
			break;
		}
	}

	memset( &buf[0], 0, sizeof(buf) );
	if( read_status == 1 )
		strcpy( buf, "ACCEPT" );
	else
		strcpy( buf, "ERROR " );

	sc = write( sock, buf, strlen(buf) );
	if( sc < 0 ){
		atf_err("atf_socket_recv","write");
		return(0);
	}

	memcpy( data, &read_data.data[0], data_size );

	return(1);
}

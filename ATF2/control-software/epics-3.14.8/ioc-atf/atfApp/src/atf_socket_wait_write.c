#include <stdio.h>
#include <netdb.h>

#include "atf_socket.h"

int atf_socket_wait_write( int sock, int time_out )
{
	struct timeval to;
	fd_set write_fds;
	int status;

	FD_ZERO( &write_fds );
	FD_SET( sock, &write_fds );

	if( time_out == NO_TIMEOUT ){
		status = select( sock+1, NULL, &write_fds, NULL, NULL );
	}
	else{
		to.tv_sec  = time_out;
		to.tv_usec = 0;
		status = select( sock+1, NULL, &write_fds, NULL, &to );
	}

	if( status == 0 ){
		return(2); //time out
	}
	else if( status == -1 ){
		atf_err("atf_socket_wait_write","select");
		return(0);
	}

	if( !( FD_ISSET(sock,&write_fds) ) ){
		atf_err("atf_socket_wait_write","select");
		return(0);
	}

	return(1);
}

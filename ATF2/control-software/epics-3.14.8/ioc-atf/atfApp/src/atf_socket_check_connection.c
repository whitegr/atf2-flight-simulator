#include <stdio.h>
#include <sys/poll.h>
#include <unistd.h>

int atf_socket_check_connection( int sock )
{
	struct pollfd pfd;

	pfd.fd = sock;
	pfd.events = POLLHUP;

	if( poll( &pfd, 1, 0 ) < 0 ){
		atf_err("atf_scoket_check_connection","poll");
		return(0);
	}

	if( (pfd.revents & POLLHUP) != 0 ){
		atf_err("atf_socket_check_connection","Hung up.");
		return(0);
	}

	return(1);
}

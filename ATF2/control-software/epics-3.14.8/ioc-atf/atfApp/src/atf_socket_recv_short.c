#include <stdio.h>
#include <netdb.h>
#include "atf_socket.h"

int atf_socket_recv_short( int sock, short *data, int *ndata, int timeout)
{
	int status;

	status = atf_socket_recv( sock, data, DATA_TYPE_SHORT, ndata, timeout );
	if( status != 1 ){
		if( status == 2 ){
			return(2);
		}
		else{
			atf_err("atf_socket_recv_short","atf_socket_recv");
			return(0);
		}
	}

	return(1);
}

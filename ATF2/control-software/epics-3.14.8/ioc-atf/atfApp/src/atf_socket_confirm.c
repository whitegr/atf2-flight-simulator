#include <stdio.h>
#include <unistd.h>
#include <errno.h>
#include "atf_socket.h"
/*
!==============================================================================

    Possible return code

	IATF_SOCKET_NORMAL
	IATF_SOCKET_ERROR
	IATF_SOCKET_TIMEOUT
	IATF_SOCKET_INVALID_ARG2
	IATF_SOCKET_INVALID_ARG3
	IATF_SOCKET_DISCONNECTED

!==============================================================================
*/
int atf_socket_confirm( int 	sock,
			int 	readwrite,
			int 	*confirm_id,
			int	*receipt_state,
		     	int 	*err )
{
	#define		READ	0
	#define		WRITE	1
	#define 	TIMEOUT60 60	/* 1 minute */

	int		buffer[2];
	int		status, rc, sc;

/* Clear error code.*/

	*err = 0;

/* Ready ?*/

	status = atf_socket_ready( sock, readwrite, TIMEOUT60, err );
	if( status != IATF_SOCKET_NORMAL ) return(status);

/* Ready to read data on the socket.*/
/* When connection is cut, EINVAL may be set to errno by read().*/

	if( readwrite == READ ){

	    rc = read( sock, buffer, sizeof( buffer ) );

	    if( rc <= 0 ){
		if( rc == 0 ){
		    return( IATF_SOCKET_DISCONNECTED );
		} else {
		    *err = errno;
		    if( *err == ECONNRESET ){
			return( IATF_SOCKET_DISCONNECTED );
		    } else if( *err == EINVAL ){
			return( IATF_SOCKET_DISCONNECTED );
		    } else {
			return(IATF_SOCKET_ERROR);
		    }
		}           
	    }

	    *confirm_id    = buffer[0];
	    *receipt_state = buffer[1];

/* Ready to write data on the socket.*/

	} else if( readwrite == WRITE ){

	    buffer[0] = *confirm_id;
	    buffer[1] = *receipt_state;	

	    sc = write( sock, buffer, sizeof( buffer ) );

	    if( sc < 0 ){
		*err = errno;
		if( *err == EPIPE ){
		    return( IATF_SOCKET_DISCONNECTED );
		} else {
		    return(IATF_SOCKET_ERROR);
		}
	    }

	} else {

	    return( IATF_SOCKET_INVALID_ARG2 );

	}

	return( IATF_SOCKET_NORMAL );
}

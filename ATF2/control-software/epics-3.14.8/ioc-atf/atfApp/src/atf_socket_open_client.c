#include <stdio.h>
#include <netdb.h>

int atf_socket_open_client( int *sock, int port, char *sv_name )
{
        struct hostent		*shost;
        struct sockaddr_in	sv_addr;
	int status;

	shost = gethostbyname(sv_name);
        if( shost == NULL ){
                atf_err("atf_socket_open_client", "gethostbyname");
		return(0);
	}

	*sock = socket( AF_INET, SOCK_STREAM, 0);
        if( *sock < 0 ){
                atf_err("atf_socket_open_client","socket");
		return(0);
        }

	sv_addr.sin_family = AF_INET;
	sv_addr.sin_port = htons(port);
	memcpy(&sv_addr.sin_addr, shost->h_addr, shost->h_length);

        status = connect( *sock, (struct sockaddr *)&sv_addr, sizeof(sv_addr) );
        if( status < 0 ){
                atf_err("atf_socket_open_client","connect");
                return(0);
        }

	return(1);
}

#include <stdio.h>
#include <netdb.h>
#include <string.h>
#include <unistd.h>
#include <errno.h>
#include <netinet/in.h>
#include "atf_socket.h"
/*
!===============================================================================

    ---------------------------------------------------------------------------
    int atf_socket_ready( int  sock, int readwrite, int  timeout, int *err )
    ---------------------------------------------------------------------------

    This function cheks the ready status for reading or writting.

    Arguments

	sock	    A discriptor that must refer to a socket.
	readwrite   Code to select the I/O status for read(0) or write(1).
	timeout	    Seconds for timeout.
	err	    Error code of internal socket functions.

    Possible return code

	IATF_SOCKET_NORMAL
	IATF_SOCKET_ERROR
	IATF_SOCKET_TIMEOUT
	IATF_SOCKET_INVALID_ARG2
	IATF_SOCKET_INVALID_ARG3

!===============================================================================
*/
int atf_socket_ready( int  sock, int readwrite, int  timeout, int *err )
{
	#define		READ	0
	#define		WRITE	1
	struct timeval	to;
	fd_set		read_fds, write_fds;
	int		status, read_flg, write_flg;

	*err = 0;

/* Check the range of third argument for timeout.*/

	if( timeout < 0 ){
	    return( IATF_SOCKET_INVALID_ARG3 );
	}

/* Ready status for Read action.*/

	if( readwrite == READ ){

/*	    Clear a pointer to an array of bits that should be examined for 
 	    read readness.*/

	    FD_ZERO( &read_fds );
	    FD_SET( sock, &read_fds );

/*	    Check the I/O status of the sockets to read data. */

	    to.tv_sec  = timeout;
	    to.tv_usec = 0;
	    status = select( sock+1, &read_fds, NULL, NULL, &to );

/* Ready status for Write action.*/

	} else if( readwrite == WRITE ){

/*	    Clear a pointer to an array of bits that should be examined for 
  	    write readness.*/

	    FD_ZERO( &write_fds );
	    FD_SET( sock, &write_fds );

/*	    Check the I/O status of the sockets to write data.*/

	    to.tv_sec  = timeout;
	    to.tv_usec = 0;
	    status = select( sock+1, NULL, &write_fds, NULL, &to );

	} else {

	    return( IATF_SOCKET_INVALID_ARG2 );

	}

/* The select() routine timed out before any socket became ready for I/O.*/

	if( status == 0 ){

	    return( IATF_SOCKET_TIMEOUT );

/* The select() routine returns with an error.*/

	} else if( status == -1 ) {

	    *err = errno;
	    return( IATF_SOCKET_ERROR );

	}

/* Ready to read data on the socket.*/

	return( IATF_SOCKET_NORMAL );

}


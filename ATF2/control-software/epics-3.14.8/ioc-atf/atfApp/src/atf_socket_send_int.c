#include <stdio.h>
#include <netdb.h>
#include "atf_socket.h"

int atf_socket_send_int( int sock, int *data, int ndata )
{
	if( atf_socket_send( sock, data, DATA_TYPE_INT, ndata ) != 1 ){
		atf_err("atf_socket_send_int","atf_socket_send");
		return(0);
	}

	return(1);
}

#include <stdio.h>
#include <string.h>
#include "atf_socket.h"
/*
!==============================================================================

    Possible return code

        IATF_SOCKET_NORMAL
        IATF_SOCKET_ERROR
        IATF_SOCKET_TIMEOUT
        IATF_SOCKET_DISCONNECTED
        IATF_SOCKET_INVALID_DATA
	IATF_SOCKET_INVALID_TYPE

!==============================================================================
*/
int atf_socket_read_float( int	    sock,
			   float    *data,
			   int	    *ndata,
			   int	    timeout)
{
	static SOCKET_BUFFER    buffer;
        char *buffer_in;
	int             status, err, i, receipt_state,ival,ia,byteSwap=0;
        union v {float ni; unsigned char d[sizeof(float)];};
        union v vn; 
        float *testFloat;


	status = atf_socket_read( sock, &buffer, timeout, &err );
	if( status != IATF_SOCKET_NORMAL ){

	    return( status );
	}
        /*memcpy(&buffer,buffer_in,sizeof(buffer));*/
        /* If on little-endian machine, byte swap bpm_data array*/
        if (byteSwap==1) {
          for ( ival=0; ival<MX_F_ARRAY_SIZE; ival+=sizeof(float) ) {
            for (ia=0; ia<sizeof(float); ia++) {
              vn.d[ia]=buffer.data.c[sizeof(float)-ia];
            }
            data[ival/sizeof(float)]=vn.ni;
          }
        } 
        else {
	  testFloat=buffer.data.f;
        }
        *data=testFloat[400];
        printf("first fdata: %f\n",*data);
        /*for (ival=400;ival<401;ival++) printf("ival: %d val: %f\n",ival,fdata[ival]);*/
	return( IATF_SOCKET_NORMAL );

}


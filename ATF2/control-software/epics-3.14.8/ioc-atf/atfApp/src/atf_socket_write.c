#include <stdio.h>
#include <unistd.h>
#include <errno.h>
#include "atf_socket.h"
/*
!==============================================================================

    This function reads data on a specified socket. 
    At the first, it checks an arrival of data on a socket and waits until
    past the timeout interval, reads them
    
    Possible return code

	IATF_SOCKET_NORMAL
	IATF_SOCKET_ERROR
	IATF_SOCKET_TIMEOUT
	IATF_SOCKET_DISCONNECTED

!==============================================================================
*/
int atf_socket_write( int	    sock,
		      char	    *write_data,
		      int	    timeout,
		      int	    *err )
{
	#define		WRITE	1
	int	status, rc;

/* Clear error code.*/

	*err = 0;

/* Ready ?*/

	status = atf_socket_ready( sock, WRITE, timeout, err );

	if( status != IATF_SOCKET_NORMAL ){

	    if( status == IATF_SOCKET_TIMEOUT ){
		return( IATF_SOCKET_TIMEOUT );
	    } else {
		return( IATF_SOCKET_ERROR );
	    }

	}

/* Ready to write data on the socket.*/

	rc = write( sock, write_data, MX_NANOBPM_SIZE*4 );

	if( rc < 0 ){
	    *err = errno;
	    if( *err == EPIPE ){
		return( IATF_SOCKET_DISCONNECTED );
	    } else {
		return( IATF_SOCKET_ERROR );
	    }
	}

	return( IATF_SOCKET_NORMAL );

}


#include <stdio.h>
#include <netdb.h>
#include "atf_socket.h"

int atf_socket_send( int sock, void *data, int type, int ndata )
{
	SOCKET_DATA write_data;
	int sc, rc;
	int size;
	char buf[128];
	
	if( type == DATA_TYPE_STRING ){
		size = sizeof(char) * ndata;
	}
	else if( type == DATA_TYPE_INT ){
		size = sizeof(int) * ndata;
	}
	else if( type == DATA_TYPE_FLOAT ){
		size = sizeof(float) * ndata;
	}
	else if( type == DATA_TYPE_SHORT ){
		size = sizeof(short) * ndata;
	}
	else{
		atf_err("atf_socket_send","data type");
		return(0);
	}

	write_data.header.type  = type;
	write_data.header.ndata = ndata;
	memcpy( &write_data.data[0], data, size );

	if( atf_socket_wait_write( sock, 10 ) != 1 ){
		atf_err("atf_socket_send","atf_socket_wait_write");
		return(0);
	}

	if( atf_socket_check_connection( sock ) != 1 ){
		atf_err("atf_socket_send","atf_socket_check_connection");
		return(0);
	}

	sc = write( sock, &write_data, sizeof(write_data.header)+size );
	if( sc < 0 ){
		atf_err("atf_socket_send","write");
		return(0);
	}

	if( atf_socket_check_connection( sock ) != 1 ){
		atf_err("atf_socket_send","atf_socket_check_connection");
		return(0);
	}


	if( atf_socket_wait_read( sock, 10 ) != 1 ){
		atf_err("atf_socket_send","atf_socket_wait_read");
		return(0);
	}

//	rc = read( sock, buf, sizeof(buf) );
	rc = read( sock, buf, 6 );
	if( rc < 0 ){
		atf_err("atf_socket_send","read");
		return(0);
	}
	buf[rc]='\0';

	if( strcmp( buf, "ACCEPT" ) != 0 ){
		atf_err("atf_socket_send","recv data");
	}

	return(1);
}

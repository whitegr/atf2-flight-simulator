#include <stdio.h>
#include <string.h>
#include "atf_socket.h"
/*
!==============================================================================

    Possible return code

        IATF_SOCKET_NORMAL
        IATF_SOCKET_ERROR
        IATF_SOCKET_TIMEOUT
        IATF_SOCKET_DISCONNECTED
        IATF_SOCKET_INVALID_DATA
	IATF_SOCKET_INVALID_TYPE

!==============================================================================
*/
int atf_socket_read_short( int 	sock,
	                short 	*data,
			int 	*ndata,
			int 	timeout )
{
	#define		WRITE	1
	static SOCKET_BUFFER	buffer;
	int			status, err, receipt_state;

/* Receive buffer from socket.
   The value of status is sent if atf_socket_read() is error.*/

	status = atf_socket_read( sock, &buffer, timeout, &err );
	if( status != IATF_SOCKET_NORMAL ){

	    atf_socket_confirm( sock, WRITE, &buffer.header.id,
	   	    &status, &err );

	    return( status );
	}

/* Check buffer type from header field.
   The value of IATF_SOCKET_INVALID_TYPE is sent when data type differs.*/

	if( buffer.header.type != DATA_TYPE_SHORT ){

	    receipt_state = IATF_SOCKET_INVALID_TYPE;
	    atf_socket_confirm( sock, WRITE, &buffer.header.id,
	   	    &receipt_state, &err );

	    return( IATF_SOCKET_INVALID_TYPE );
	}

/* Check string_length from header field.*/

	*ndata = ( buffer.header.nbyte - MX_HEADER_BYTE ) / sizeof( short );

/* Copy buffer data field into string.*/

	memcpy( data, &buffer.data.s, *ndata * sizeof( short ) );

/* Send buffer_id and receipt_state from header field.*/

	receipt_state = IATF_SOCKET_NORMAL;
	status = atf_socket_confirm( sock, WRITE, &buffer.header.id,
		&receipt_state, &err );
	if( status != IATF_SOCKET_NORMAL ) return( status );

	return( IATF_SOCKET_NORMAL );

}


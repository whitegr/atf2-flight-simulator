/*atfDriver.c */
/***********************************************************************
* Copyright (c) 2002 The University of Chicago, as Operator of Argonne
* National Laboratory, and the Regents of the University of
* California, as Operator of Los Alamos National Laboratory, and
* Berliner Elektronenspeicherring-Gesellschaft m.b.H. (BESSY).
* asynDriver is distributed subject to a Software License Agreement
* found in file LICENSE that is included with this distribution.
***********************************************************************/

/* Author: Marty Kraimer */

#include <stddef.h>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <errno.h>
#include <ctype.h>

#include <cantProceed.h>
#include <epicsStdio.h>
#include <epicsMutex.h>
#include <epicsEvent.h>
#include <epicsThread.h>
#include <iocsh.h>
#include <dbFldTypes.h>
#include <dbAccess.h>
#include <dbDefs.h>
#include <cadef.h>

#include <asynDriver.h>
#include <asynDrvUser.h>
#include <asynFloat64Array.h>
#include <asynFloat64.h>

#include <epicsExport.h>
#define NCHANNELS 523
#define MAGARRAY_SIZE 523

/* ATF Socket stuff*/
#include "atf_socket.h"
#define TIMEOUT 20

typedef struct chanPvt {
    epicsInt32 value;
    void       *asynPvt;
    char       magstr[20];
    int        magindx;
}chanPvt;

typedef struct drvPvt {
    const char    *portName;
    epicsMutexId  lock;
    epicsEventId  waitWork;
    int           connected;
    double        interruptDelay;
    asynInterface common;
    asynInterface asynDrvUser;
    asynInterface asynFloat64Array;
    asynInterface asynFloat64;
    char          *serverIP;
    epicsInt32    serverPort;
    int           serverSocket;
    short int     littleEndian;
    void          *asynFloat64ArrayPvt; /* For registerInterruptSource*/
    void          *asynFloat64Pvt; /* For registerInterruptSource*/
    chanPvt       channel[NCHANNELS];    
    int           simMode;
}drvPvt;

static int atfDriverInit(const char *dn,char *serverIP, int serverPort, int simMode);
static asynStatus getAddr(drvPvt *pdrvPvt,asynUser *pasynUser,
    int *paddr,int portOK);
static void interruptThread(drvPvt *pdrvPvt);

/* asynCommon methods */
static void report(void *drvPvt,FILE *fp,int details);
static asynStatus connect(void *drvPvt,asynUser *pasynUser);
static asynStatus disconnect(void *drvPvt,asynUser *pasynUser);
static asynCommon common = { report, connect, disconnect };

/* asynDrvUser */
static asynStatus create(void *drvPvt,asynUser *pasynUser,
    const char *drvInfo, const char **pptypeName,size_t *psize);
static asynStatus getType(void *drvPvt,asynUser *pasynUser,
    const char **pptypeName,size_t *psize);
static asynStatus destroy(void *drvPvt,asynUser *pasynUser);
static asynDrvUser drvUser = {create,getType,destroy};

/* asynFloat64(Array) methods */
static asynStatus float64Write(void *drvPvt,asynUser *pasynUser,
				    epicsFloat64 value);
static asynStatus float64ArrayRead(void *drvPvt,asynUser *pasynUser,
				   epicsFloat64 *value, size_t nelements, size_t *nIn);

static int atfDriverInit(const char *dn,char *serverIP,int serverPort,int simMode)
{
    drvPvt    *pdrvPvt;
    char       *portName, *testbyte, *pserverIP;
    asynStatus status;
    int        nbytes;
    asynFloat64Array *pasynFloat64Array;
    asynFloat64 *pasynFloat64;
    short int  testword;
   
    nbytes = sizeof(drvPvt) + sizeof(asynFloat64Array) +sizeof(asynFloat64);
    nbytes += strlen(dn) + strlen(serverIP) + 2;
    pdrvPvt = callocMustSucceed(nbytes,sizeof(char),"atfDriverInit");
    pasynFloat64Array = (asynFloat64Array *)(pdrvPvt + 1);
    pasynFloat64 = (asynFloat64 *)(pasynFloat64Array + 1);
    portName = (char *)(pasynFloat64 + 1);
    pserverIP = (char *)(portName +1);
    strcpy(pserverIP,serverIP);
    strcpy(portName,dn);
    pdrvPvt->serverIP=pserverIP;
    pdrvPvt->portName = portName;
    pdrvPvt->lock = epicsMutexMustCreate();
    pdrvPvt->waitWork = epicsEventCreate(epicsEventEmpty);
    pdrvPvt->common.interfaceType = asynCommonType;
    pdrvPvt->common.pinterface  = (void *)&common;
    pdrvPvt->common.drvPvt = pdrvPvt;
    pdrvPvt->asynDrvUser.interfaceType = asynDrvUserType;
    pdrvPvt->asynDrvUser.pinterface = (void *)&drvUser;
    pdrvPvt->asynDrvUser.drvPvt = pdrvPvt;
    pdrvPvt->serverPort = serverPort;
    pdrvPvt->simMode = simMode;
    /* Get endianness of machine */
    testword = 0x0001;
    testbyte = (char *) &testword;
    if (testbyte[0]) pdrvPvt->littleEndian=1;
    else pdrvPvt->littleEndian=0;
    status = pasynManager->registerPort(portName,ASYN_MULTIDEVICE,1,0,0);
    if(status!=asynSuccess) {
        printf("atfDriverInit registerDriver failed\n");
        return 0;
    }
    status = pasynManager->registerInterface(portName,&pdrvPvt->common);
    if(status!=asynSuccess){
        printf("atfDriverInit registerInterface failed\n");
        return 0;
    }
    status = pasynManager->registerInterface(portName,&pdrvPvt->asynDrvUser);
    if(status!=asynSuccess){
        printf("atfDriverInit registerInterface failed\n");
        return 0;
    }
    pasynFloat64->write = float64Write;
    pasynFloat64Array->read = float64ArrayRead;
    pdrvPvt->asynFloat64Array.interfaceType = asynFloat64ArrayType;
    pdrvPvt->asynFloat64Array.pinterface  = pasynFloat64Array;
    pdrvPvt->asynFloat64Array.drvPvt = pdrvPvt;
    status = pasynFloat64ArrayBase->initialize(pdrvPvt->portName,
        &pdrvPvt->asynFloat64Array);
    
    if(status!=asynSuccess) return 0;
    pdrvPvt->interruptDelay = 0.0;
    status = pasynManager->registerInterruptSource(portName,&pdrvPvt->asynFloat64Array,
        &pdrvPvt->asynFloat64ArrayPvt);
    if(status!=asynSuccess) {
        printf("atfDriverInit registerInterruptSource failed\n");
    }
    pdrvPvt->asynFloat64.interfaceType = asynFloat64Type;
    pdrvPvt->asynFloat64.pinterface  = pasynFloat64;
    pdrvPvt->asynFloat64.drvPvt = pdrvPvt;
    status = pasynFloat64Base->initialize(pdrvPvt->portName,
        &pdrvPvt->asynFloat64);
    if(status!=asynSuccess) return 0;
    
    return(0);
}

static asynStatus getAddr(drvPvt *pdrvPvt,asynUser *pasynUser,
    int *paddr,int portOK)
{
    asynStatus status;  

    status = pasynManager->getAddr(pasynUser,paddr);
    if(status!=asynSuccess) return status;
    if(*paddr>=-1 && *paddr<NCHANNELS) return asynSuccess;
    if(!portOK && *paddr>=0) return asynSuccess;
    epicsSnprintf(pasynUser->errorMessage,pasynUser->errorMessageSize,
        "%s addr %d is illegal; Must be >= %d and < %d\n",
        pdrvPvt->portName,*paddr,
        (portOK ? -1 : 0),NCHANNELS);
    printf("getaddr Error!\n");
    return asynError;
}

/* asynCommon methods */
static void report(void *pvt,FILE *fp,int details)
{
    drvPvt *pdrvPvt = (drvPvt *)pvt;

    fprintf(fp,"    atfDriver: connected:%s interruptDelay = %f\n",
        (pdrvPvt->connected ? "Yes" : "No"),
        pdrvPvt->interruptDelay);
}

static asynStatus connect(void *pvt,asynUser *pasynUser)
{
    drvPvt   *pdrvPvt = (drvPvt *)pvt;
    int        addr;
    asynStatus status;  


    status = getAddr(pdrvPvt,pasynUser,&addr,1);
    if(status!=asynSuccess) return status;
    asynPrint(pasynUser, ASYN_TRACE_FLOW,
        "%s atfDriver:connect addr %d\n",pdrvPvt->portName,addr);
    if(addr>=0) {
        pasynManager->exceptionConnect(pasynUser);
        return asynSuccess;
    }
    if(pdrvPvt->connected) {
        asynPrint(pasynUser,ASYN_TRACE_ERROR,
           "%s atfDriver:connect port already connected\n",
           pdrvPvt->portName);
        return asynError;
    }
    
    pdrvPvt->connected = 1;
    pasynManager->exceptionConnect(pasynUser);

    return asynSuccess;
}

static asynStatus disconnect(void *pvt,asynUser *pasynUser)
{
    drvPvt    *pdrvPvt = (drvPvt *)pvt;
    int        addr;
    asynStatus status;


    status = getAddr(pdrvPvt,pasynUser,&addr,1);
    if(status!=asynSuccess) return status;
    asynPrint(pasynUser, ASYN_TRACE_FLOW,
        "%s atfDriver:disconnect addr %d\n",pdrvPvt->portName,addr);
    if(addr>=0) {
        pasynManager->exceptionDisconnect(pasynUser);
        return asynSuccess;
    }
    if(!pdrvPvt->connected) {
        asynPrint(pasynUser,ASYN_TRACE_ERROR,
           "%s atfDriver:disconnect port not connected\n",
           pdrvPvt->portName);
        return asynError;
    }

    pdrvPvt->connected = 0;
    pasynManager->exceptionDisconnect(pasynUser);
    return asynSuccess;
}

static asynStatus float64Write(void *pvt,asynUser *pasynUser,
				    epicsFloat64 value)
{
    drvPvt   *pdrvPvt = (drvPvt *)pvt;
    int        addr, atf_status, command_length, atf_socket, ichar;
    int digitTest, splitInd, clength;
    long dbStat, nele=MAGARRAY_SIZE;
    asynStatus status;
    char command[256],newcommand[256],pvStr[256];
    epicsFloat64      magArray[MAGARRAY_SIZE];
    struct dbAddr dbMagArray;

    status = getAddr(pdrvPvt,pasynUser,&addr,0);
    if(status!=asynSuccess) return status;
    if(!pdrvPvt->connected) {
        asynPrint(pasynUser,ASYN_TRACE_ERROR,
            "%s : read  not connected\n",pdrvPvt->portName);
        epicsSnprintf(pasynUser->errorMessage,pasynUser->errorMessageSize,
            "%s : read not connected\n",pdrvPvt->portName);
        return asynError;
    }
    if(pdrvPvt->simMode==0) {
      /* ATF socket connection */
      atf_status=atf_socket_client_connect_ip( "20.10.71.44", &atf_socket, pdrvPvt->serverPort );
      if( atf_status != IATF_SOCKET_NORMAL ){
         printf("%s: could not connect to ATF server\n",pdrvPvt->portName);
         return asynError;
      }
      
      /* Set current command */
      command_length=sprintf( command, "SET_CURRENT %s %f\0", pdrvPvt->channel[addr].magstr, value );
      /* Fix name format */
      if (isdigit(pdrvPvt->channel[addr].magstr[strlen(pdrvPvt->channel[addr].magstr)])) {
        digitTest=0;splitInd=0;ichar=0;
        do {
	  ichar++;
	  if (isdigit(command[ichar])) digitTest++;
	  if ((digitTest>0)&(!strncmp(&command[ichar],"R",1)|!strncmp(&command[ichar],"X",1))) splitInd=ichar;
        } while((splitInd==0)|(ichar==command_length));
        if (splitInd>0) {
	  sprintf( newcommand, "SET_CURRENT \0");
          strncat( newcommand, &command[12],splitInd-11);
	  strncat( newcommand, ".",1);
	  strcat( newcommand, &command[splitInd+1]);
        }
      }
      else {
        strncpy(newcommand,command,command_length);
	clength=strlen(newcommand);
      }
      /* Send request command to ATF server
      atf_status = atf_socket_write_string( atf_socket, newcommand, &clength,TIMEOUT );
      if( atf_status != IATF_SOCKET_NORMAL ) {
        asynPrint(pasynUser,ASYN_TRACE_ERROR,
            "%s : error writing to ATF server socket\n",pdrvPvt->portName);
        return asynError;
      }
      */printf("command to send: %s\n",newcommand);

      /* ATF socket disconnect */
      if (pdrvPvt->simMode==0) {
        sprintf( command, "QUIT" );
        command_length = 4;
        atf_status = atf_socket_write_string( atf_socket, command, &command_length,TIMEOUT );
        atf_socket_close( atf_socket );
      }
    }
    else {
      strcpy(pvStr, "ATF:readMAGS.VAL");
      dbStat = dbNameToAddr( pvStr, &dbMagArray );
      dbStat = dbGetField(&dbMagArray,DBR_DOUBLE,magArray,NULL,&nele,NULL);
      magArray[pdrvPvt->channel[addr].magindx]=value;
      dbPutField(&dbMagArray,DBR_DOUBLE,magArray,MAGARRAY_SIZE);
    }
    return asynSuccess;
}

static asynStatus float64ArrayRead(void *pvt,asynUser *pasynUser,
				   epicsFloat64 *value, size_t nelements, size_t *nIn )
{
    drvPvt *pdrvPvt = (drvPvt *)pvt;
    int        addr, ival, command_length, atf_status, atf_socket, err ;
    static SOCKET_BUFFER atfbuffer;
    asynStatus status;
    char command[256];

    /* ATF socket connection */
    if (pdrvPvt->simMode==0) {
      atf_status=atf_socket_client_connect_ip( "20.10.71.44", &atf_socket, pdrvPvt->serverPort );
      /*atf_status = atf_socket_client_connect_ip( pdrvPvt->serverIP, &atf_socket, pdrvPvt->serverPort );*/
      if( atf_status != IATF_SOCKET_NORMAL ){
         printf("%s: could not connect to ATF server\n",pdrvPvt->portName);
         return asynError;
       }
    }
    status = getAddr(pdrvPvt,pasynUser,&addr,0);
    if(status!=asynSuccess) return status;
    asynPrint(pasynUser, ASYN_TRACE_FLOW,
        "%s %d float64ArrayRead\n",pdrvPvt->portName,addr);
    if(!pdrvPvt->connected) {
        asynPrint(pasynUser,ASYN_TRACE_ERROR,
            "%s atfDriver:read  not connected\n",pdrvPvt->portName);
        epicsSnprintf(pasynUser->errorMessage,pasynUser->errorMessageSize,
            "%s atfDriver:read not connected\n",pdrvPvt->portName);
        return asynError;
    }
    if (pdrvPvt->simMode==0) {
      if (addr==0) {
        /* Set command to get ATF BPM data */
        command_length=sprintf( command, "READ_BPM" );
      }
      else if (addr==1) {
        /* Set command to get ATF magnet data */
        command_length=sprintf( command, "READ_MAGNET" );
      }

      /* Send request command to ATF server */
      atf_status = atf_socket_write_string( atf_socket, command, &command_length,TIMEOUT );
      if( atf_status != IATF_SOCKET_NORMAL ) {
        asynPrint(pasynUser,ASYN_TRACE_ERROR,
            "%s atfDriver: error writing to ATF server socket\n",pdrvPvt->portName);
        return asynError;
      }

      /* Recieve data */
      status = atf_socket_read( atf_socket, &atfbuffer, TIMEOUT, &err );
      if( status != IATF_SOCKET_NORMAL ){
        return( status );
      }
      for (ival=0;ival<nelements;ival++) value[ival]=(epicsFloat64) atfbuffer.data.f[ival];
      if( atf_status != IATF_SOCKET_NORMAL ) {
        asynPrint(pasynUser,ASYN_TRACE_ERROR,
            "%s atfDriver: error reading from ATF server socket: %d \n",pdrvPvt->portName, atf_status);
        return asynError;
      }
      *nIn = nelements;
    }
    /* ATF socket disconnect */
    if (pdrvPvt->simMode==0) {
      sprintf( command, "QUIT" );
      command_length = 4;
      atf_status = atf_socket_write_string( atf_socket, command, &command_length,TIMEOUT );
      atf_socket_close( atf_socket );
    }

    return asynSuccess;
}

static const char *testDriverReason = "testDriverReason";
static const char *skipWhite(const char *pstart)
{
    const char *p = pstart;
    while(*p && isspace(*p)) p++;
    return p;
}

static asynStatus create(void *pvt,asynUser *pasynUser,
    const char *drvInfo, const char **pptypeName,size_t *psize)
{
    const char *p1, *p;
    int addr;
    drvPvt *pdrvPvt = (drvPvt *)pvt;
    asynStatus status;

    status = getAddr(pdrvPvt,pasynUser,&addr,1);
    if(status!=asynSuccess) return status;
    if((addr>=0)&(strcmp("atf",pdrvPvt->portName)==0)) {
      p1 = skipWhite(drvInfo);
      if(strlen(p1)==0) {
        asynPrint(pasynUser,ASYN_TRACE_ERROR,
            "%s: Incorrect ASYN USER declaration\n",pdrvPvt->portName);
        epicsSnprintf(pasynUser->errorMessage,pasynUser->errorMessageSize,
            "%s: Incorrect ASYN USER declaration\n",pdrvPvt->portName);
        printf("adde: %d\n",addr);
        return asynSuccess;
      }
      p = strchr(p1, ':');
      if(!p) {
        asynPrint(pasynUser,ASYN_TRACE_ERROR,
	    "%s: Incorrect ASYN USER declaration\n",pdrvPvt->portName);
        epicsSnprintf(pasynUser->errorMessage,pasynUser->errorMessageSize,
            "%s: Incorrect ASYN USER declaration\n",pdrvPvt->portName);
        printf("adde: %d\n",addr);
        return asynSuccess;
      }
      pdrvPvt->channel[addr].magindx=atoi(p+1);
      memcpy(pdrvPvt->channel[addr].magstr,p1,(p-p1));
    }
    if(pptypeName) *pptypeName = drvInfo;
    if(psize) *psize = sizeof(int);
    return asynSuccess;
}


static asynStatus getType(void *drvPvt,asynUser *pasynUser,
    const char **pptypeName,size_t *psize)
{
    *pptypeName = testDriverReason;
    *psize = sizeof(int);
    return asynSuccess;
}

static asynStatus destroy(void *drvPvt,asynUser *pasynUser)
{ return asynSuccess;}

/* register atfDriverInit*/
static const iocshArg atfDriverInitArg0 = { "portName", iocshArgString };
static const iocshArg atfDriverInitArg1 = { "serverIP", iocshArgString };
static const iocshArg atfDriverInitArg2 = { "serverPort", iocshArgInt };
static const iocshArg atfDriverInitArg3 = { "simMode", iocshArgInt };
static const iocshArg *atfDriverInitArgs[] = {
  &atfDriverInitArg0,&atfDriverInitArg1,&atfDriverInitArg2,&atfDriverInitArg3};
static const iocshFuncDef atfDriverInitFuncDef = {
    "atfDriverInit", 4, atfDriverInitArgs};
static void atfDriverInitCallFunc(const iocshArgBuf *args)
{
  atfDriverInit(args[0].sval,args[1].sval,args[2].ival,args[3].ival);
}

static void atfDriverRegister(void)
{
    static int firstTime = 1;
    if (firstTime) {
        firstTime = 0;
        iocshRegister(&atfDriverInitFuncDef, atfDriverInitCallFunc);
    }
}
epicsExportRegistrar(atfDriverRegister);

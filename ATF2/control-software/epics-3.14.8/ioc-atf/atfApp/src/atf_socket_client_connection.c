#include <netdb.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <netinet/in.h>
#include "atf_socket.h"
/*
!===============================================================================

    It is used in order to connection with server.

    Socket description is stored in the 1nd argument.
    Port number is specified to be the 2st argument.
    Host information is specified to be the 3rd argument.

    Possible return code

	IATF_SOCKET_NORMAL
	IATF_SOCKET_ERROR
	IATF_SOCKET_TIMEOUT
	IATF_SOCKET_ECONNREFUSED

!===============================================================================
*/

int atf_socket_client_connection( char *server,
				  int *sock,
				  int port )
{
        struct	sockaddr_in	sv_addr;
	struct 	hostent		*shost;
	int			status;

	errno = 0;

/* Server information is acquired. */

	shost = gethostbyname( server );
	if( shost == NULL ) return( IATF_SOCKET_ERROR );

/* Socket is created.*/

	*sock = socket( AF_INET, SOCK_STREAM, 0 );
	if( *sock < 0 ) return( IATF_SOCKET_ERROR );

	bzero( &sv_addr, sizeof( sv_addr ) );
	sv_addr.sin_family = AF_INET;
	sv_addr.sin_port = htons( port );
	memcpy( &sv_addr.sin_addr, shost->h_addr, shost->h_length );

/* The connection demand of a socket.*/

	status = connect( *sock, (struct sockaddr *)&sv_addr, sizeof(sv_addr));
	if( status != 0 ){
	    if( errno == ECONNREFUSED ){
		return( IATF_SOCKET_ECONNREFUSED );
	    } else {
		return( IATF_SOCKET_ERROR );
	    }
	}

	return( IATF_SOCKET_NORMAL );

}


#include <stdio.h>
#include <netdb.h>

int atf_socket_accept_new_client( int sock, int *nsock, int timeout )
{
	int len, status;
	struct sockaddr_in	cl_addr;

	status = atf_socket_wait_read( sock, timeout );
	if( status != 1 ){
		if( status == 2 ){
			return(2); //time out
		}
		else{
			atf_err("atf_socket_accept_new_client","atf_socket_wait_read");
			return(0);
		}
	}

	// 接続要求を待ち別ソケットを作成
	len = sizeof( cl_addr );
	*nsock = accept( sock, (struct sockaddr *)&cl_addr, &len);
	if( *nsock == -1 ){
	    atf_err("atf_socket_accept_new_client","accept");
	    return(0);
	}

	return(1);
}

#include <stdio.h>
#include <unistd.h>
#include <errno.h>
#include "atf_socket.h"
/*
!==============================================================================

    This function reads data on a specified socket. 
    At the first, it checks an arrival of data on a socket and waits until
    past the timeout interval, reads them
    
    Possible return code

	IATF_SOCKET_NORMAL
	IATF_SOCKET_ERROR
	IATF_SOCKET_TIMEOUT
	IATF_SOCKET_DISCONNECTED
	IATF_SOCKET_INVALID_DATA

    When connection is cut, EINVAL may be set to errno by read().

!==============================================================================
*/
int atf_socket_read( int	    sock,
		     char	    *buffer,
		     int	    timeout,
		     int	    *err )
{

	#define		READ	0
	int	status, rc, read_size, remainder_size;
	int	i;

/* Clear error code. */

	*err = 0;

	read_size   = 0;
	while(1){

	    status = atf_socket_ready( sock, READ, timeout, err );
	    if( status != IATF_SOCKET_NORMAL ){

		if( status == IATF_SOCKET_TIMEOUT ){
		    return( IATF_SOCKET_TIMEOUT );
		} else {
		    return( IATF_SOCKET_ERROR );
		}

	    }

	    remainder_size = MX_NANOBPM_SIZE*4 - read_size;

	    rc = read( sock, &buffer[0]+read_size, remainder_size);

	    /*for( i=read_size; i<read_size+rc; i++ ){
		printf("[%d][%d] %x\n", i/4, i%4, buffer[i] );
	    } */

	    if( rc <= 0 ){
		if( rc == 0 ){
		    return( IATF_SOCKET_DISCONNECTED );
		} else {
		    *err = errno;
		    if( *err == ECONNRESET ){
			return( IATF_SOCKET_DISCONNECTED );
		    } else if( *err == EINVAL ){
			return( IATF_SOCKET_DISCONNECTED );
		    } else {
			return( IATF_SOCKET_ERROR );
		    }
		}		
	    }

	    read_size += rc;

	    if( read_size >= MX_NANOBPM_SIZE*4 ) break;

	}

	return( IATF_SOCKET_NORMAL );
}


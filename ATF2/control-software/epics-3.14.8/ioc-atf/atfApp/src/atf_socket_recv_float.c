#include <stdio.h>
#include <netdb.h>
#include "atf_socket.h"

int atf_socket_recv_float( int sock, float *data, int *ndata, int timeout)
{
	int status;

	status = atf_socket_recv( sock, data, DATA_TYPE_FLOAT, ndata, timeout );
	if( status != 1 ){
		if( status == 2 ){
			return(2);
		}
		else{
			atf_err("atf_socket_recv_float","atf_socket_recv");
			return(0);
		}
	}

	return(1);
}

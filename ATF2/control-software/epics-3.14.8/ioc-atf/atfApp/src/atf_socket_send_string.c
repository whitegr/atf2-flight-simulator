#include <stdio.h>
#include <netdb.h>
#include "atf_socket.h"

int atf_socket_send_string( int sock, char *data, int length )
{
	length = length + 1;
	if( atf_socket_send( sock, data, DATA_TYPE_STRING, length ) != 1 ){
		atf_err("atf_socket_send_string","atf_socket_send");
		return(0);
	}

	return(1);
}


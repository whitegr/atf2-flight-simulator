#include <stdio.h>
#include <netdb.h>
#include <string.h>
#include <netinet/in.h>
#include "atf_socket.h"
/*
!===============================================================================

    The socket description with the connection demand is looked for.

    Socket description is stored in the 1nd argument.
    Socket description connected to the 2nd argument by 'accept()'.

    Possible return code

	IATF_SOCKET_NORMAL
	IATF_SOCKET_ERROR
	IATF_SOCKET_TIMEOUT

!===============================================================================
*/
int atf_socket_server_accept( int sock, int *nsock )
{
	#define		READ		0
	#define		TIMEOUT0	0	
	size_t		    len;
	struct sockaddr_in  cl_addr;
	int		    status, err;

/* Connection demand is checked.  */ 

	status = atf_socket_ready( sock, READ, TIMEOUT0, &err );
	if( status != IATF_SOCKET_NORMAL ){
	    if( status == IATF_SOCKET_TIMEOUT ){
		return( IATF_SOCKET_TIMEOUT );
	    } else {
		return( IATF_SOCKET_ERROR );
	    }
	}

/* Accepts a connection on a passive socket.*/

	len = sizeof( cl_addr );
	*nsock = accept( sock, (struct sockaddr *)&cl_addr, &len );
	if( *nsock == -1 ) return( IATF_SOCKET_ERROR );
 
	return( IATF_SOCKET_NORMAL );

}


/* C++ static constructor and destructor lists */
/* This is a generated file, do not edit! */
#include <vxWorks.h>

/* Declarations */
void _GLOBAL__I_pvar_func_spectrumRecordSizeOffset();
void _GLOBAL__I_spectrumRecord_registerRecordDeviceDriver();
void _GLOBAL__D_pvar_func_spectrumRecordSizeOffset();

/* Constructors */
void (*_ctors[])() = {
    _GLOBAL__I_pvar_func_spectrumRecordSizeOffset,
    _GLOBAL__I_spectrumRecord_registerRecordDeviceDriver,
    NULL
};

/* Destructors */
void (*_dtors[])() = {
    _GLOBAL__D_pvar_func_spectrumRecordSizeOffset,
    NULL
};


#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/ioctl.h>
#include <unistd.h>
#include <stdio.h>
#include "pcc.h"

#define FRAME_LEN 17*1024
#define BUF_LEN FRAME_LEN*2

int e_time(first, second)
     struct timeval *first, *second; {

  if (first->tv_usec > second->tv_usec) {
    second->tv_usec += 1000000;
    second->tv_sec--;
  }
#ifdef DEBUG
  printf("elapsed time %d sec, %d microsec\n",
         second->tv_sec - first->tv_sec, second->tv_usec - first->tv_usec);
#endif
  return( (int)(second->tv_sec - first->tv_sec)*1000000
	  + (int)(second->tv_usec - first->tv_usec));
}

main(int argc, char **argv) {

  int cc_fd;
  int i, j, len;
  int status, loop = 1;
  int cmdbuf[BUF_LEN+2], rplybuf[BUF_LEN+2];
  int databuf[FRAME_LEN];
  int actual_length, q, x;
  int num_frame, pattern, fast_flag, process;
  int data;
  int overhead;
  struct timeval time1, time2;

  fast_flag = 0;
  if( argc > 1 )
    fast_flag = 1;

  if((cc_fd = cam_open( )) == -1) {
    printf("cam_open error\n");
    exit(0);
  }

  // set fast cycle...
  if( fast_flag ) {
    if( (status = cam_single_cc(cc_fd, 25, 2, 26, &data, &q, &x )) == -1 ) {
      printf("CAMAC: error...\n");
      exit(1);
    }
    printf("Fast cycle mode...\n");
  } else {
    if( (status = cam_single_cc(cc_fd, 25, 2, 24, &data, &q, &x )) == -1 ) {
      printf("CAMAC: error...\n");
      exit(1);
    }
    printf("Normal cycle mode...\n");
  }
  pattern = 1;

  printf("frame size    execution time in usec\n");
  // PIO
  printf("PIO\n"); 
  // measure overhead of loop
  gettimeofday(&time1,NULL);
  for(i=0; i < 1000; i++) {
    cam_gen_init( len, rplybuf );
  }
  gettimeofday(&time2,NULL);
  overhead = e_time(&time1, &time2)/1000;

  num_frame = 1;
  cam_gen_init( num_frame, cmdbuf );
  gen_data(pattern, num_frame, cmdbuf );
  len = cmdbuf[1];
  gettimeofday(&time1,NULL);
  printf("start measuring overhead\n");
  for(i=0; i < 1000; i++) {
    cam_gen_init( len, rplybuf );
    cam_exec_pio( cc_fd, cmdbuf, rplybuf );
    //    printf("cam_exec_pio:loop = %d\n", i);
  }
  gettimeofday(&time2,NULL);
  printf("%d  %d\n", 
	 num_frame, e_time(&time1, &time2)/1000 - overhead);

  for (num_frame = 10; num_frame < 101; num_frame += 5 ) {
    cam_gen_init( num_frame, cmdbuf );
    gettimeofday(&time1,NULL);
    for(i=0; i < 1000; i++) {
      cam_gen_init( num_frame, cmdbuf );
    }
    gettimeofday(&time2,NULL);
    overhead = e_time(&time1, &time2)/1000;

    gen_data(pattern, num_frame, cmdbuf );
    len = cmdbuf[1];
    gettimeofday(&time1,NULL);
    for(i=0; i < 1000; i++) {
      cam_gen_init( len, rplybuf );
      cam_exec_pio( cc_fd, cmdbuf, rplybuf );
    }
    gettimeofday(&time2,NULL);
    printf("%d  %d\n", 
	   num_frame, e_time(&time1, &time2)/1000 -  overhead);
  }

  // DMA
  printf("DMA\n"); 
  // measure overhead of loop
  gettimeofday(&time1,NULL);
  for(i=0; i < 1000; i++) {
    cam_gen_init( len, rplybuf );
  }
  gettimeofday(&time2,NULL);
  overhead = e_time(&time1, &time2)/1000;

  pattern = 1;

  num_frame = 1;
  cam_gen_init( num_frame, cmdbuf );
  gen_data(pattern, num_frame, cmdbuf );
  len = cmdbuf[1];
  gettimeofday(&time1,NULL);
  for(i=0; i < 1000; i++) {
    cam_gen_init( len, rplybuf );
    cam_exec_dma( cc_fd, cmdbuf, rplybuf );
  }
  gettimeofday(&time2,NULL);
  printf("%d  %d\n", 
	 num_frame, e_time(&time1, &time2)/1000 - overhead);
  
  for (num_frame = 10; num_frame < 101; num_frame += 5 ) {
    cam_gen_init( num_frame, cmdbuf );
    gettimeofday(&time1,NULL);
    for(i=0; i < 1000; i++) {
      cam_gen_init( num_frame, cmdbuf );
    }
    gettimeofday(&time2,NULL);
    overhead = e_time(&time1, &time2)/1000;

    gen_data(pattern, num_frame, cmdbuf );
    len = cmdbuf[1];
    gettimeofday(&time1,NULL);
    for(i=0; i < 1000; i++) {
      cam_gen_init( len, rplybuf );
      cam_exec_dma( cc_fd, cmdbuf, rplybuf );
    }
    gettimeofday(&time2,NULL);
    printf("%d %d\n", 
	   num_frame, e_time(&time1, &time2)/1000 -  overhead);
  }

  for (num_frame = 2; num_frame < 17000; num_frame *= 2 ) {
    loop = 1000;
    if (num_frame > 256 )
      loop = 100;
    if (num_frame > 1000 )
      loop = 10;
    if (num_frame > 16000 ) {
      loop = 1;
      num_frame = 16380;
    }
    cam_gen_init( num_frame, cmdbuf );
    gettimeofday(&time1,NULL);
    for(i=0; i < 1000; i++) {
      cam_gen_init( num_frame, cmdbuf );
    }
    gettimeofday(&time2,NULL);
    overhead = e_time(&time1, &time2)/1000;

    gen_data(pattern, num_frame, cmdbuf );
    len = cmdbuf[1];

    gettimeofday(&time1,NULL);
    for(i=0; i < loop; i++) {
      cam_gen_init( len, rplybuf );
      cam_exec_dma( cc_fd, cmdbuf, rplybuf );
    }
    gettimeofday(&time2,NULL);
    printf("%d %d\n", 
	   num_frame, e_time(&time1, &time2)/loop -  overhead);
  }

  // reset fast cycle 
  if( fast_flag ) {
    if( (status = cam_single_cc(cc_fd, 25, 2, 24, &data, &q, &x )) == -1 ) {
      printf("CAMAC: error...\n");
      exit(1);
    }
    printf("Normal cycle mode...\n");
  }
  
  cam_close( cc_fd );
}

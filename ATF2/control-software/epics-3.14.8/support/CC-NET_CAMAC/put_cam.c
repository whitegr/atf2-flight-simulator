#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/ioctl.h>
#include <unistd.h>
#include <stdio.h>

main(int argc, char **argv) {

  int fd;
  int status;
  int wdata, cmd;
  
  if( argc != 3 ) {
    printf("usage : ./put_cam data(hex) cmd(hex)\n");
    exit(0);
  }
  wdata = strtoul(argv[1], NULL, 0);
  cmd = strtoul(argv[2], NULL, 0);

  if((fd = cam_open( )) == -1) {
    printf("cam_open error\n");
    exit(0);
  }
  if( (status = cam_put( fd, wdata, cmd )) < 0 ) {
      printf("cam_put error\n");
      exit(0);
  }
  cam_close( fd );
}

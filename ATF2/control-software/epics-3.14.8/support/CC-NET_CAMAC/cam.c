#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/ioctl.h>
#include <unistd.h>
#include <stdio.h>
#include "pcc.h"

#define PACKET_HEADER 0xE0000000

main(int argc, char **argv) {

  int fd, i;
  int status;
  int n, a, f, QX;
  int wdata, rdata, cmd, reply;
  
  if( argc !=4 && argc !=5 ) {
    printf("usage : ./cam n a f [data]\n");
    exit(0);
  }
  if( argc >= 4 ) {
    n = atoi(argv[1]);
    a = atoi(argv[2]);
    f = atoi(argv[3]);
  }
  
  cmd = PACKET_HEADER | cam_naf(n, a, f);

  if( argc == 5 )
    wdata = strtoul(argv[4],NULL,0);

  if((fd = cam_open( )) == -1) {
    printf("cam_open error\n");
    exit(0);
  }
  if( (status = cam_put( fd, wdata, cmd )) < 0 ) {
      printf("cam_put error\n");
      exit(0);
  }
  if( (status = cam_get( fd, &rdata, &reply )) < 0 ) {
      printf("cam_get error\n");
      exit(0);
    }
  QX = (rdata >> 24) & 3;
  printf("Q = %d : X = %d ", QX&1, (QX>>1)&1);
  if( f >=0 && f <= 7 )
    printf(": data = %x", (rdata&0xFFFFFF));
  printf("\n");
  cam_close( fd );
}

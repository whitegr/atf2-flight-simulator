#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/ioctl.h>
#include <unistd.h>
#include <stdio.h>
#include "pcc.h"

#define SWREG 4
#define CC_READ 0
#define CC_WRITE 16
#define FRAME_LEN 100
#define BUF_LEN FRAME_LEN*2

main(int argc, char **argv) {

  int fd, i, len;
  int status, loop = 1;
  int length = FRAME_LEN;
  int cmdbuf[BUF_LEN+2], rplybuf[BUF_LEN+2];
  int* cbuf = cmdbuf+2;
  int* rbuf = rplybuf+2;
  int event_count;
  int data;

  if( argc > 2 ) {
    printf("usage : ./exam2\n");
    exit(0);
  }
  if( (status = cam_gen_init( length, cmdbuf )) == -1 ) {
    printf("cam_gen_init error...\n");
    exit(0);
  }
  if( (status = cam_gen_init( length, rplybuf )) == -1 ) {
    printf("cam_gen_init error...\n");
    exit(0);
  }
  data = 0;
  if( (status = cam_gen_daq(cmdbuf, DAQEXE_CTRL_READ, data)) == -1 ) { // read event count 
    printf("cam_gen_daq DAQEXE_CTRL_READ error...\n");
    exit(0);
  }
  if( (status = cam_gen_daq(cmdbuf, DAQEXE_CTRL_CLRBSY, data)) == -1 ) { // clear busy out
    printf("cam_gen_daq DAQEXE_CTRL_CLRBSY error...\n");
    exit(0);
  }
  if( (status = cam_gen_daq(cmdbuf, DAQEXE_CTRL_CLRCNT, data)) == -1 ) { // clear evnet counter
    printf("cam_gen_daq DAQEXE_CTRL_CLRCNT error...\n");
    exit(0);
  }
  if( (status = cam_gen_daq(cmdbuf, DAQEXE_CTRL_ENABLE, data)) == -1 ) { // enable trigger input
    printf("cam_gen_daq DAQEXE_CTRL_ENABLE error...\n");
    exit(0);
  }
  if( (status = cam_gen_daq(cmdbuf, DAQEXE_CTRL_DISABLE, data)) == -1 ) { // disable trigger input
    printf("cam_gen_daq DAQEXE_CTRL_DISABLE error...\n");
    exit(0);
  }
  if( (status = cam_gen_daq(cmdbuf, DAQEXE_CTRL_PLSOUT, data)) == -1 ) { // set pulse out
    printf("cam_gen_daq DAQEXE_CTRL_PLSOUT error...\n");
    exit(0);
  }
  if( (status = cam_gen_daq(cmdbuf, DAQEXE_CTRL_NOPLSOUT, data)) == -1 ) { // reset pulse out (default)
    printf("cam_gen_daq DAQEXE_CTRL_NOPLSOUT error...\n");
    exit(0);
  }

  if((fd = cam_open( )) == -1) {
    printf("cam_open error\n");
    exit(0);
  }
  if( (status = cam_exec_pio( fd, cmdbuf, rplybuf )) < 0 ) {
      printf("cam_exec_pio error\n");
      exit(0);
  }

  printf("number of reply frames : %d\n", rplybuf[1]);

  printf("read event count     : data(       0) = %8x rply(d0000000) = %8x\n",
	 rbuf[0], rbuf[1]);
  printf("clear busy out       : data(       0) = %8x rply(90010000) = %8x\n",
	 rbuf[2], rbuf[3]);
  printf("clear event counter  : data(       0) = %8x rply(90020000) = %8x\n",
	 rbuf[4], rbuf[5]);
  printf("enable trigger input : data(       0  = %8x rply(90030002) = %8x\n",
	 rbuf[6], rbuf[7]);
  printf("disable trigger input: data(       0) = %8x rply(90040000) = %8x\n",
	 rbuf[8], rbuf[9]);
  printf("set pulse out        : data(       0) = %8x rply(90050008) = %8x\n",
	 rbuf[10], rbuf[11]);
  printf("reset pulse out      : data(       0) = %8x rply(b0060000) = %8x\n",
	 rbuf[12], rbuf[13]);

  cam_close( fd );
}

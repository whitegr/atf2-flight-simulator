#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/ioctl.h>
#include <unistd.h>
#include <stdio.h>
#include "pcc.h"

#define TIMEOUT 0 // default is used

main(int argc, char **argv) {

  int fd, i, len;
  int status, loop = 1;
  int event_count;
  int data;
  int daq_status;

  if( argc > 2 ) {
    printf("usage : ./exam4 [loop:1]\n");
    exit(0);
  }
  if( argc == 2 )
    loop = atoi(argv[1]);

  if((fd = cam_open( )) == -1) {
    printf("cam_open error\n");
    exit(0);
  }

  // disalbe interrupt
  status = cam_disable_trig(fd);
  if ( status < 0 ) {
    printf("cam_disable_trig : error...\n");
    exit(0);
  }

  // clear event counter
  status = cam_single_daq( fd, DAQEXE_CTRL_CLRCNT, &data, &daq_status);
  if( status < 0 ) {
    printf("cam_single_daq: DAQEXE_CTRL_CLRCNT error...\n");
    exit(0);
  }

  // clear busy out
  status = cam_single_daq( fd, DAQEXE_CTRL_CLRBSY, &data, &daq_status);
  if( status < 0 ) {
    printf("cam_single_daq: DAQEXE_CTRL_CLRBSY error...\n");
    exit(0);
  }

  for( i = 0; i < loop; i++) {

    status = cam_enable_trig(fd);
    if ( status < 0 ) {
      printf("cam_enable_trig : error...\n");
      exit(0);
    }

    if( (status = cam_wait_trig( fd, &event_count, TIMEOUT )) < 0 ) {
      printf("cam_wait_trig error cod = %d\n", status);
      // disalbe interrupt
      status = cam_disable_trig(fd);
      if ( status < 0 ) {
	printf("cam_disable_trig : error...\n");
	exit(0);
      }
      printf("loop count = %d\n", i+1);
      exit(0);
    }

    // disalbe interrupt
    status = cam_disable_trig(fd);
    if ( status < 0 ) {
      printf("cam_disable_trig : error...\n");
      exit(0);
    }

    // clear busy out
    status = cam_single_daq( fd, DAQEXE_CTRL_CLRBSY, &data, &daq_status);
    if( status < 0 ) {
      printf("cam_single_daq: DAQEXE_CTRL_CLRBSY error...\n");
      exit(0);
    }
    //    if(!((i+1)%1000))
      printf("Loop count = %d : Event count = %d\n", i+1, event_count);
  }
 

  cam_close( fd );
}

/* exam0.c :
 *
 * written by : Y.Yasu (KEK online group)
 *
 * description :
 *    This program checks basic CAMAC functions.
 *    It includes Z, C, SetInhibit, RemoteInhibit, 
 *    DisableInt, EnableInt, WriteEnableBit, ReadEnableBit,
 *    SetFastCycle, ResetFastCylce and Read/Write/NDT to
 *    a Switch register.
 */

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/ioctl.h>
#include <unistd.h>
#include <stdio.h>
#include "pcc.h"

#define SWREG 4
#define CC_READ 0
#define CC_WRITE 16
#define FRAME_LEN 100
#define BUF_LEN FRAME_LEN*2

main(int argc, char **argv) {

  int fd, i, len;
  int status, loop = 1;
  int length = FRAME_LEN;
  int cmdbuf[BUF_LEN+2], rplybuf[BUF_LEN+2];
  int* cbuf = cmdbuf+2;
  int* rbuf = rplybuf+2;

  if( argc > 1 ) {
    printf("usage : ./exam0\n");
    exit(0);
  }

  if( (status = cam_gen_init( length, cmdbuf )) == -1 ) {
    printf("cam_gen_init error...\n");
    exit(0);
  }
  if( (status = cam_gen_init( length, rplybuf )) == -1 ) {
    printf("cam_gen_init error...\n");
    exit(0);
  }

  if( (status = cam_gen_cc( cmdbuf, 25, 0, 17, 0 )) == -1 ) { // Z
    printf("cam_gen_cc error ...\n");
    exit(0);
  }
  if( (status = cam_gen_cc(cmdbuf, 25, 0, 16, 0)) == -1 ) { // C
    printf("cam_gen_cc error...\n");
    exit(0);
  }
  if( (status = cam_gen_cc(cmdbuf, 25, 0, 26, 0)) == -1 ) { // set Inhibit
    printf("cam_gen_cc error...\n");
    exit(0);
  }
  if( (status = cam_gen_cc(cmdbuf, 25, 0, 24, 0)) == -1 ) { // remove Inhibit
    printf("cam_gen_cc error...\n");
    exit(0);
  }
  if( (status = cam_gen_cc(cmdbuf, 25, 1, 24, 0)) == -1 ) { // disable Interrupt
    printf("cam_gen_cc error...\n");
    exit(0);
  }
  if( (status = cam_gen_cc(cmdbuf, 25, 1, 26, 0)) == -1 ) { // enable Interrupt
    printf("cam_gen_cc error...\n");
    exit(0);
  }
  if( (status = cam_gen_cc(cmdbuf, 25, 1, 16, 0xFFFFFF)) == -1 ) { // write interrupt enable bits
    printf("cam_gen_cc error...\n");
    exit(0);
  }
  if( (status = cam_gen_cc(cmdbuf, 25, 1, 26, 0)) == -1 ) { // read interrupt enable bits
    printf("cam_gen_cc error...\n");
    exit(0);
  }
  if( (status = cam_gen_cc(cmdbuf, 25, 2, 26, 0)) == -1 ) { // set fast cycle
    printf("cam_gen_cc error...\n");
    exit(0);
  }
  if( (status = cam_gen_cc(cmdbuf, 25, 2, 24, 0)) == -1 ) { // reset fast clcle
    printf("cam_gen_cc error...\n");
    exit(0);
  }
  if( (status = cam_gen_cc(cmdbuf, SWREG, 0, 16, 0xAAAAAA)) == -1 ) { // write data to Switch register.
    printf("cam_gen_cc error...\n");
    exit(0);
  }
  if( (status = cam_gen_cc(cmdbuf, SWREG, 0, 0, 0)) == -1 ) { // read data from Switch register.
    printf("cam_gen_cc error...\n");
    exit(0);
  }
  if( (status = cam_gen_cc(cmdbuf, SWREG, 0, 10, 0xAAAAAA)) == -1 ) { // clear data in Switch register.
    printf("cam_gen_cc error...\n");
    exit(0);
  }

  if((fd = cam_open( )) == -1) {
    printf("cam_open error\n");
    exit(0);
  }
  if( (status = cam_exec_pio( fd, cmdbuf, rplybuf )) < 0 ) {
      printf("cam_exec_pio error\n");
      exit(0);
  }

  printf("number of reply frames : %d\n", rplybuf[1]);

  printf("( 1) Z :                 data( 4000000) = %8x rply(c0190011) = %8x\n",
	 rbuf[0], rbuf[1]);
  printf("( 2) C :                 data( 4000000) = %8x rply(80190010) = %8x\n",
	 rbuf[2], rbuf[3]);
  printf("( 3) set Inhibit :       data( 4000000) = %8x rply(8019001a) = %8x\n",
	 rbuf[4], rbuf[5]);
  printf("( 4) remove Inhibit :    data(       0) = %8x rply(80190018) = %8x\n",
	 rbuf[6], rbuf[7]);
  printf("( 5) disable interrupt : data(  ffffff) = %8x rply(80190118) = %8x\n",
	 rbuf[8], rbuf[9]);
  printf("( 6) enable interrupt :  data(10ffffff) = %8x rply(8019011a) = %8x\n", 
	 rbuf[10], rbuf[11]);
  printf("( 7) write enable bits : data(10ffffff) = %8x rply(80190110) = %8x\n", 
	 rbuf[12], rbuf[13]);
  printf("( 8) read enable bits :  data(10ffffff) = %8x rply(8019011a) = %8x\n", 
	 rbuf[14], rbuf[15]);
  printf("( 9) set fast cycle :    data(90000000) = %8x rply(8019021a) = %8x\n", 
	 rbuf[16], rbuf[17]);
  printf("(10) reset fast cyclc :  data(10000000) = %8x rply(80190218) = %8x\n", 
	 rbuf[18], rbuf[19]);
  printf("(11) write data to SW :  data(13000000) = %8x rply(80040010) = %8x\n", 
	 rbuf[20], rbuf[21]);
  printf("(12) read data from SW : data(13aaaaaa) = %8x rply(80040000) = %8x\n", 
	 rbuf[22], rbuf[23]);
  printf("(13) clear data in  SW : data(12000000) = %8x rply(a004000a) = %8x\n", 
	 rbuf[24], rbuf[25]);

  cam_close( fd );
}

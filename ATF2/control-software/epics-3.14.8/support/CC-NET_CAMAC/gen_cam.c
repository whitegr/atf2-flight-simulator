#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <stdio.h>

main(int argc, char **argv) {
  int n, a, f, data, flag;
  int packet1, packet2;

  if( argc != 6 ) {
    printf("usage : ./gen_cam n a f data flag(1:start,2:end,0:normal,packet:others)\n");
    exit(0);
  }

  n =  atoi(argv[1]);
  a =  atoi(argv[2]);
  f =  atoi(argv[3]);
  data = strtoul(argv[4],NULL,0);
  flag = atoi(argv[5]);

  if( gen_cam_command(&packet1, &packet2, n, a, f, data, flag) ) {
    printf("command error...\n");
    exit(0);
  }
  printf("Data1 = %x(hex)\n", packet1);
  printf("Data2 = %x(hex)\n", packet2);
}

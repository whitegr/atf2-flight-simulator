#define CC_READ 0
#define CC_WRITE 16
#define SWREG 4

int gen_data(int pattern, int num_frame, int *cmdbuf ) {
  int i, seed;

  //  printf("data pattern %1d is selected...\n", pattern);
  switch (pattern) {
  case 0:
    for ( i = 0; i < num_frame; i += 4 ) {
      cam_gen_cc(cmdbuf, SWREG, 0, CC_WRITE, 0xFFFFFF);
      cam_gen_cc(cmdbuf, SWREG, 0, CC_READ, 0);
      cam_gen_cc(cmdbuf, SWREG, 0, CC_WRITE, 0);
      cam_gen_cc(cmdbuf, SWREG, 0, CC_READ, 0);
    }
    break;
  case 1:
    for ( i = 0; i < num_frame; i += 10 ) {
      cam_gen_cc(cmdbuf, SWREG, 0, CC_WRITE, 0xFFFFFF);
      cam_gen_cc(cmdbuf, SWREG, 0, CC_READ, 0);
      cam_gen_cc(cmdbuf, SWREG, 0, CC_WRITE, 0);
      cam_gen_cc(cmdbuf, SWREG, 0, CC_READ, 0);
      cam_gen_cc(cmdbuf, SWREG, 0, CC_WRITE, 0x555555);
      cam_gen_cc(cmdbuf, SWREG, 0, CC_READ, 0);
      cam_gen_cc(cmdbuf, SWREG, 0, CC_WRITE, 0xAAAAAA);
      cam_gen_cc(cmdbuf, SWREG, 0, CC_READ, 0);
      cam_gen_cc(cmdbuf, SWREG, 0, CC_WRITE, 0);
      cam_gen_cc(cmdbuf, SWREG, 0, CC_READ, 0);
    }
    break;
  case 2:
    for ( i = 0; i < num_frame; i += 2 ) {
      cam_gen_cc(cmdbuf, SWREG, 0, CC_WRITE, i);
      cam_gen_cc(cmdbuf, SWREG, 0, CC_READ, 0);
    }break;
  case 3:
    seed = 987654321;
    for ( i = 0; i < num_frame; i += 2 ) {
      seed *= 663608941;
      cam_gen_cc(cmdbuf, SWREG, 0, CC_WRITE, seed);
      cam_gen_cc(cmdbuf, SWREG, 0, CC_READ, 0);
    }
    break;
  default:
    return -1;
  }
  return 0;
}

int check_data(int pattern, int num_frame, int *data ) {
  int i, seed;

  switch (pattern) {
  case 0:
    for(i = 0; i < num_frame; i += 4 ) {
      if( data[i+1] != 0xFFFFFF ) {
	printf("Error at number of frame = %d : written data = 0xFFFFFF : read data = %6x\n", i, data[i+1] );
	exit(1);
      }
      if( data[i+3] != 0 ) {
	printf("Error at number of frame = %d : written data =      0 : read data = %6x\n", i, data[i+3] );
	exit(1);
      }
    }
    break;
  case 1:
    for(i = 0; i < num_frame; i += 10 ) {
      if( data[i+1] != 0xFFFFFF ) {
	printf("Error at number of frame = %d : written data = 0xFFFFFF : read data = %6x\n", i, data[i+1] );
	exit(1);
      }
      if( data[i+3] != 0 ) {
	printf("Error at number of frame = %d : written data =      0 : read data = %6x\n", i, data[i+3] );
	exit(1);
      }
      if( data[i+5] != 0x555555 ) {
	printf("Error at number of frame = %d : written data = 0x555555 : read data = %6x\n", i, data[i+5] );
	exit(1);
      }
      if( data[i+7] != 0xAAAAAA ) {
	printf("Error at number of frame = %d : written data = 0xAAAAAA : read data = %6x\n", i, data[i+7] );
	exit(1);
      }
      if( data[i+9] != 0 ) {
	printf("Error at number of frame = %d : written data = 0xFFFFFF : read data = %6x\n", i, data[i+9] );
	exit(1);
      }
    }
    break;
  case 2:
    for(i = 0; i < num_frame; i += 2 ) {
      if( data[i+1] != i ) {
	printf("Error at number of frame = %d : written data = %6x : read data = %6x\n", i, i, data[i+1] );
	exit(1);
      }
    }
    break;
  case 3:
    seed = 987654321;
    for ( i = 0; i < num_frame; i += 2 ) {
      seed *= 663608941;
      if( data[i+1] != (seed&0xFFFFFF) ) {
	printf("Error at number of frame = %d : written data = %6x : read data = %6x\n", i, seed, data[i+1] );
	exit(1);
      }
    }
    break;
  }
}

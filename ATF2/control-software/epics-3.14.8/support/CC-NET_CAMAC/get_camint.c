#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/ioctl.h>
#include <unistd.h>
#include <stdio.h>

main(int argc, char **argv) {

  int fd;
  int status;
  int rdata, reply;
  
  if((fd = cam_open( )) == -1) {
    printf("cam_open error\n");
    exit(0);
  }
  if( (status = cam_getint( fd, &rdata, &reply )) < 0 ) {
      printf("cam_getint error\n");
      exit(0);
    }
  printf("data = 0x%x : reply = 0x%x\n", rdata, reply);
  cam_close( fd );
}

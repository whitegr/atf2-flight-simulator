#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/ioctl.h>
#include <unistd.h>
#include <stdio.h>
#include "pcc.h"

main(int argc, char **argv) {

  int fd;
  int status;
  struct pccreg pccreg;
  
  if((fd = cam_open( )) == -1) {
    printf("cam_open error\n");
    exit(0);
  }

  if( (status = cam_dump_pccreg( fd, &pccreg ) ) < 0 ) {
    printf("cam_dump_pccreg error\n");
    exit(0);
  }
  dump_pccreg(&pccreg);

  cam_close( fd );
}

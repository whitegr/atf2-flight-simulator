/* File name     : camtest1c.c
 * Creation date : April 1992
 * Author        : Y.Yasu, Online group, Physics department, KEK
 * Modified date : July 1992, Y.Y
 * Modified      : August 1994, Y.Yasu
 *                 For HP-RT
 *               : July 2003, Y.Yasu for pipeline camac controller
 */

/* System headers */
#include <errno.h>
#include <fcntl.h>
#include <sys/types.h>

/* Local headers */
#include "camlib.h"

/* Macros */
#define  SWREG  4
#define  MAXLEN 0x100000

/* File scope variable */

/* External variables */
#ifdef HP_RT
extern char *sys_errlist[];
#endif

/* External functions */

/* Structure and unions */

/* Signal catching functions */

char *strerror();

/* Functions */
error_exit(status)
     int status; {
#ifdef HP_RT
  perror(sys_errlist[status]);
#else
  perror(strerror(status));
#endif
  exit(status);
}
/* Main */
main() {
  int status, i, q, x, ii;
  short j, jj;

  printf("camtest1c : test starts\n");
  status = CAMOPEN( 0 );
  if( status ) error_exit(status);
  printf("camopen : OK\n");
  status = CGENZ( 0 );
  if( status ) error_exit(status);
  printf("CGENZ    : OK\n");
  status = CGENC( 0 );
  if( status ) error_exit(status);
  printf("CGENC    : OK\n");
  status = CSETI( 0 );
  if( status ) error_exit(status);
  printf("CSETI    : OK\n");
  status = CREMI( 0 );
  if( status ) error_exit(status);
  printf("CREMI    : OK\n");

  while (i < MAXLEN ) {
    status = CAMAC(0, SWREG, 0, 16, &i, &q, &x );
    if( status ) error_exit(status);
    status = CAMAC(0, SWREG, 0, 0, &ii, &q, &x );
    if( status ) error_exit(status);
    if( i != ii ) {
      printf("Data Comparison Error\n");
      printf("Written data are %x(h)\n", i);
      printf("Read data are    %x(h)\n", ii);
      exit(1);
    }
    i++;
  }
  printf("CAMAC   : OK\n");

  status = CAMCLS();
  if( status ) error_exit(status);
  printf("CAMCLS: OK\n");
}


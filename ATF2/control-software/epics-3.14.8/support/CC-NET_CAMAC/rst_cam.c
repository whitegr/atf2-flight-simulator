#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/ioctl.h>
#include <unistd.h>
#include <stdio.h>

main(int argc, char **argv) {

  int fd;
  int status;
  int rdata, reply;
  
  if((fd = cam_open( )) == -1) {
    printf("cam_open error\n");
    exit(0);
  }

  if( (status = cam_reset( fd ) ) < 0 ) {
    printf("cam_reset error\n");
    exit(0);
  }
  cam_close( fd );
}

#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <stdio.h>

main(int argc, char **argv) {
  int n, a, f, data, status;
  int data1, data2;

  if( argc != 3 ) {
    printf("usage : ./dec_cam data1(hex) data2(hex)\n");
    exit(0);
  }

  data1 = strtoul(argv[1],NULL,0);
  data2 = strtoul(argv[2],NULL,0);

  cam_decode_cc_frame(data1, data2, &n, &a, &f, &data, &status);
  printf("n(%d) a(%d) f(%d) data(0x%x) status(0x%x)\n",
	 n, a, f, data, status);
}

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/ioctl.h>
#include <unistd.h>
#include <stdio.h>
#include "pcc.h"

#define FRAME_LEN 16*1000
#define BUF_LEN FRAME_LEN*2
//#define DEBUG

main(int argc, char **argv) {

  int cc_fd;
  int i, j, len;
  int status, loop = 1;
  int cmdbuf[BUF_LEN+2], rplybuf[BUF_LEN+2];
  int databuf[FRAME_LEN];
  int actual_length, q, x;
  int num_frame, pattern, fast_flag, process;
  int data;

  pattern = 1;
  loop = 1;
  num_frame = 10;
  fast_flag = 0;

  if( (argc < 2) || (argc > 6) ) {
    printf("usage : ./exam1 process:0 [pattern:1] [loop:1] [num_frame] [fast]\n");
    printf("        process 0: cam_exec_pio (default)\n");
    printf("        process 1: cam_exec_dma\n");
    printf("        process 2: cam_exec_dma_seq\n");
    printf("        process 3: cam_exec\n");
    printf("        pattern 0: a series of data (0xFFFFFF, 0)\n");
    printf("        pattern 1: a series of data (0xFFFFFF, 0, 0x555555, 0, 0xAAAAAA) (default)\n");
    printf("        pattern 2: a series of data (0,1,2,3,4,5...)\n");
    printf("        pattern 3: a series of random data\n");
    printf("        loop     : iteration count to be executed (default = 1)\n");
    printf("        num_frame: number of frame to be executed (default = 10)\n");
    printf("        fast     : fast cycle:1 normal cycle:0 (default = 0)\n");
    printf("For an example\n");
    printf("        %% ./exam1 0 1 1 10 0\n");
    exit(0);
  }
  if( argc > 5 )
    fast_flag = atoi(argv[5]);
  if( argc > 4 )
    num_frame = atoi(argv[4]);
  if( argc > 3 )
    loop = atoi(argv[3]);
  if( argc > 2 )
    pattern = atoi(argv[2]);
  process = atoi(argv[1]);

  if( (num_frame < 1) || (num_frame > MAX_FRAME_LENGTH) )
    num_frame = 10;
  if( loop < 1 )
    loop = 1;
  if( (pattern < 0) || (pattern > 3) )
      pattern = 1;
  if( (process < 0) || (process > 3) )
    process = 1;

  if((cc_fd = cam_open( )) == -1) {
    printf("cam_open error\n");
    exit(0);
  }

  // set fast cycle...
  if( fast_flag ) {
    if( (status = cam_single_cc(cc_fd, 25, 2, 26, &data, &q, &x )) == -1 ) {
      printf("CAMAC: error...\n");
      exit(1);
    }
    printf("Fast cycle mode...\n");
  }
  
  if( (status = cam_gen_init( num_frame, cmdbuf )) == -1 ) {
    printf("cam_gen_init error...\n");
    exit(0);
  }

  status = gen_data(pattern, num_frame, cmdbuf );
  len = cmdbuf[1];

  printf("%d command frames...\n", len);
  /*  for(i = 0; i < len; i++)
    printf("data = %x  cmd = %x\n", cmdbuf[2*i+2], cmdbuf[2*i+3]);
  */
  printf("Execution with loop = %d\n", loop);

    switch (process) {
    case 0: 
      printf("cam_exec_pio process is selected...\n");
      break;
    case 1:
      printf("cam_exec_dma process is selected...\n");
      break;
    case 2:
      printf("cam_exec_dma_seq process is selected...\n");
      break;
    case 3:
      printf("cam_exec process is selected...\n");
      break;
    default:
      printf("no process is selected...\n");
      cam_close(cc_fd);
      exit(1);
    }

  printf("Start CAMAC access to Switch register...\n");

  for (i = 0; i < loop; i++) {
    cam_gen_init( len, rplybuf );
    switch (process) {
    case 0: 
      cam_exec_pio( cc_fd, cmdbuf, rplybuf );
      break;
    case 1:
      cam_exec_dma( cc_fd, cmdbuf, rplybuf );
      break;
    case 2:
      cam_exec_dma_seq( cc_fd, cmdbuf, rplybuf );
      break;
    case 3:
      cam_exec( cc_fd, cmdbuf, rplybuf );
      break;
      //    default:
    }
    cam_extract_cc_data(rplybuf, len, 
				      &actual_length, databuf );
    status = check_data( pattern, actual_length, databuf );

    if( (i != 0) && ((i%10000)==0) )
      printf("loop = %d\n", i);
  }
  printf("exam1 has been done successfully\n");

  // reset fast cycle...
  if( fast_flag ) {
    if( (status = cam_single_cc(cc_fd, 25, 2, 24, &data, &q, &x )) == -1 ) {
      printf("CAMAC: error...\n");
      exit(1);
    }
    printf("Normal cycle mode...\n");
  }
  
  cam_close( cc_fd );
}

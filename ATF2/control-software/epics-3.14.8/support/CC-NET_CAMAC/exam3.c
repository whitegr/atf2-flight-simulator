#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/ioctl.h>
#include <unistd.h>
#include <stdio.h>
#include "pcc.h"

#define INTREG 2
#define CC_READ 0
#define CC_WRITE 16
#define TIMEOUT 0    // default is used

main(int argc, char **argv) {

  int fd, i, len;
  int status, loop = 1;
  int lam_pattern;
  int data, q, x;
  struct pccreg pccreg;
  
  if( argc > 2 ) {
    printf("usage : ./exam3 [loop:1]\n");
    exit(0);
  }
  if( argc == 2 )loop = atoi(argv[1]);

  if((fd = cam_open( )) == -1) {
    printf("cam_open error\n");
    exit(0);
  }

  // Z
  if( (status = cam_single_cc(fd, 25, 0, 17, &data, &q, &x)) < 0 ) {
    printf("cam_single_cc(fd, 25, 0, 17...) error...\n");
    exit(0);
  }
  // C
  if( (status = cam_single_cc(fd, 25, 0, 16, &data, &q, &x)) < 0 ) { 
    printf("cam_single_cc(fd, 25, 0, 16...) error...\n");
    exit(0);
  }
  // remove inhibit
  if( (status = cam_single_cc(fd, 25, 0, 24, &data, &q, &x)) < 0 ) {
    printf("cam_single_cc(fd, 25, 0, 24...) error...\n");
    exit(0);
  }

  // disable interrupt at Interrupt register
  if( (status = cam_single_cc(fd, INTREG, 0, 24, &data, &q, &x)) < 0 ) { 
    printf("cam_single_cc(fd, INTREG, 0, 24...) error...\n");
    exit(0);
  }

  for( i = 0; i < loop; i++ ){


    // clear interrupt at Interrupt register
    status = cam_single_cc(fd, INTREG, 0, 9, &data, &q, &x);
    if( status < 0) {
      printf("cam_single_cc(fd, INTREG, 0, 9...) error...\n");
      exit(0);
    }

    // enable interrupt at the controller
    if( (status = cam_enable_lam(fd, 0xFFFFFF)) < 0 ) {
      printf("cam_enable_lam error...\n");
      exit(0);
    }

    // enable interrupt at Interrupt register
    if( (status = cam_single_cc(fd, INTREG, 0, 26, &data, &q, &x)) < 0 ) {
      printf("cam_single_cc(fd, INTREG, 0, 26...) error...\n");
      exit(0);
    }

    if( (status = cam_wait_lam( fd, &lam_pattern, TIMEOUT )) < 0 ) {
      // disable interrupt at the controller
      if( (status = cam_disable_lam(fd)) < 0 ) {
	printf("cam_disable_lam error...\n");
	exit(0);
      }
      printf("cam_wait_lam error = %d : loop count = %d\n", status, i );
      exit(0);
    }

    // disable interrupt at Interrupt register
    if( (status = cam_single_cc(fd, INTREG, 0, 24, &data, &q, &x)) < 0 ) { 
      printf("cam_single_cc(fd, INTREG, 0, 24...) error...\n");
      exit(0);
    }

    //    if(!((i+1)%1000))
      printf("Loop count = %d : LAM pattern = %x\n", i+1, lam_pattern);

  }
  cam_close( fd );
}


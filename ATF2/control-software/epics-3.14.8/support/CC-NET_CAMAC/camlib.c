#include <sys/types.h>
#include <unistd.h>
#include <sys/file.h>
#include <sys/errno.h>
#include <sys/ioctl.h>
#include <sys/stat.h>
#include <fcntl.h>
#include "camlib.h"

/* Local Storage */
static int cc_fd;                  /* camac file descriptor */
static int cc_x;        /* camac x status : true = 1 */
static int cc_q;         /* camac q status : true = 1 */
static int cc_camac_err;       /* camac error code of */
static int cc_status;
static int cc_data;
static int cc_n;
static int cc_a;
static int cc_f;

static union swaps {
  long longs;
  short shorts[2];
} swaps;

/* CAMOPN or camopen */
int CAMOPEN( int crate ) {
    int i;

    cc_fd = cam_open();
    if ( cc_fd < 0 )
      return -1;
    return 0;
}

/* CAMCLS or camcls */
int CAMCLS( int crate ) {

    return cam_close(cc_fd);
}

int NAF( n, a, f )
int n, a, f; {
    return  ((((n<<8) + a) << 8) + f);
}

void dec_naf( int naf, int *n, int *a, int *f) {
  *f = 0xFF & naf;
  *a = 0xFF & (naf>>8);
  *n = 0xFF & (naf>>16);
}

/* CRESET or creset */
int CRESET( int crate ) {
  if( (cc_status = cam_reset( cc_fd ) ) < 0 )
    return -1;
  return 0;
}

/* CGENZ or cgenz */
int CGENZ( int crate ) {
  cc_status = cam_single_cc(cc_fd, 25, 0, 17, &cc_data, &cc_q, &cc_x);
  if( cc_status)
    return -1;
  return 0;
}

/* CGENC or cgenc */
int CGENC( int crate ) {
  cc_status = cam_single_cc(cc_fd, 25, 0, 16, &cc_data, &cc_q, &cc_x);
  if( cc_status)
    return -1;
  return 0;
}

/* CSETI or cseti */
int CSETI( int crate ) {
  cc_status = cam_single_cc(cc_fd, 25, 0, 26, &cc_data, &cc_q, &cc_x);
  if( cc_status)
    return -1;
  return 0;
}

/* CREMI or cremi */
int CREMI( int crate ) {
  cc_status = cam_single_cc(cc_fd, 25, 0, 24, &cc_data, &cc_q, &cc_x);
  if( cc_status)
    return -1;
  return 0;
}

/* CAMAC or camac */
int CAMAC( int c, int n, int a, int f, int *data, int *q, int *x )
{
  cc_status = cam_single_cc(cc_fd, n, a, f, data, q, x);
  if( cc_status)
    return -1;
  return 0;
}

/* CENLAM or cenlam */
int CENLAM( int crate, int mask)
{

  cc_status = cam_single_cc(cc_fd, 25, 1, 16, &mask, &cc_q, &cc_x);
  if( cc_status)
    return -1;
  cc_status = cam_single_cc(cc_fd, 25, 1, 26, &cc_data, &cc_q, &cc_x);
  if( cc_status)
    return -1;
  return 0;
}

/* CDSLAM or cdslam */
int CDSLAM( int crate ) {

  cc_data = 0;
  cc_status = cam_single_cc(cc_fd, 25, 1, 16, &cc_data, &cc_q, &cc_x);
  if( cc_status)
    return -1;
  cc_status = cam_single_cc(cc_fd, 25, 1, 24, &cc_data, &cc_q, &cc_x);
  if( cc_status)
    return -1;
  return 0;
}

/* CWTLAM or cwtlam */
int CWTLAM( int crate, int timeout)
{
  cc_status = cam_wait_lam( cc_fd, &cc_data, timeout );
  if( cc_status)
    return -1;
  return 0;
}

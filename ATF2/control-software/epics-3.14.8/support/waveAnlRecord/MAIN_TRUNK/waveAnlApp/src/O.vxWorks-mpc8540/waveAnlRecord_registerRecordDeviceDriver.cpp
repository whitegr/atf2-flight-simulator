/* THIS IS A GENERATED FILE. DO NOT EDIT! */
/* Generated from ../waveAnlRecord.dbd */

#include "registryCommon.h"
#include "iocsh.h"
#include "iocshRegisterCommon.h"

extern "C" {
epicsShareExtern rset *pvar_rset_waveAnlRSET;
epicsShareExtern int (*pvar_func_waveAnlRecordSizeOffset)(dbRecordType *pdbRecordType);

static const char * const recordTypeNames[1] = {
    "waveAnl"
};

static const recordTypeLocation rtl[1] = {
    {pvar_rset_waveAnlRSET, pvar_func_waveAnlRecordSizeOffset}
};

int waveAnlRecord_registerRecordDeviceDriver(DBBASE *pbase)
{
    if (!pbase) {
        printf("pdbbase is NULL; you must load a DBD file first.\n");
        return -1;
    }
    registerRecordTypes(pbase, 1, recordTypeNames, rtl);
    return 0;
}

/* registerRecordDeviceDriver */
static const iocshArg registerRecordDeviceDriverArg0 =
                                            {"pdbbase",iocshArgPdbbase};
static const iocshArg *registerRecordDeviceDriverArgs[1] =
                                            {&registerRecordDeviceDriverArg0};
static const iocshFuncDef registerRecordDeviceDriverFuncDef =
                {"waveAnlRecord_registerRecordDeviceDriver",1,registerRecordDeviceDriverArgs};
static void registerRecordDeviceDriverCallFunc(const iocshArgBuf *)
{
    waveAnlRecord_registerRecordDeviceDriver(*iocshPpdbbase);
}

} // extern "C"
/*
 * Register commands on application startup
 */
class IoccrfReg {
  public:
    IoccrfReg() {
        iocshRegisterCommon();
        iocshRegister(&registerRecordDeviceDriverFuncDef,registerRecordDeviceDriverCallFunc);
    }
};
#if !defined(__GNUC__) || !(__GNUC__<2 || (__GNUC__==2 && __GNUC_MINOR__<=95))
namespace { IoccrfReg iocshReg; }
#else
IoccrfReg iocshReg;
#endif

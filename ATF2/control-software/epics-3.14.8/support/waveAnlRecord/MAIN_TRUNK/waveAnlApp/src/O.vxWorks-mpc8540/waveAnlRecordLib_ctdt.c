/* C++ static constructor and destructor lists */
/* This is a generated file, do not edit! */
#include <vxWorks.h>

/* Declarations */
void _GLOBAL__I_waveAnlRecord_registerRecordDeviceDriver();

/* Constructors */
void (*_ctors[])() = {
    _GLOBAL__I_waveAnlRecord_registerRecordDeviceDriver,
    NULL
};

/* Destructors */
void (*_dtors[])() = {
    NULL
};


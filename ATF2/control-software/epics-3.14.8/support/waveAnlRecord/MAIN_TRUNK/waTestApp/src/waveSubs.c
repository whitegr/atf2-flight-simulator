/* waveSubs.c */  
/* waveSubs.c - Test stub subroutines for waveform analysis record */
/*
 *      Original Author: John Maclean
 */

#include <stdio.h>
#include <registryFunction.h>
#include <epicsExport.h>
#include "waveAnlRecord.h"

long initTest(waveAnlRecord *pwvanl) {
  printf("initTest routine is executing.\n");
  pwvanl->vala = 1;
  pwvanl->valb = 2; 
  pwvanl->valh = 3; 
  return (0);
}

long procTest1(waveAnlRecord *pwvanl) { 
  printf("User routine 1 is executing pass %d\n",pwvanl->pass);
  pwvanl->vala += 1;
  pwvanl->valb  = pwvanl->a + pwvanl->b;
  pwvanl->valh += pwvanl->h;
  return (0);
}

long procTest2(waveAnlRecord *pwvanl) { 
  printf("User routine 2 is executing pass %d\n",pwvanl->pass);
  pwvanl->vala += 1;
  pwvanl->valb  = pwvanl->a - pwvanl->b;
  pwvanl->valh  = pwvanl->a + pwvanl->h;
  return (0);
}

static registryFunctionRef testSubRef[] = {
    { "initTest", (REGISTRYFUNCTION)initTest},
    { "procTest1", (REGISTRYFUNCTION)procTest1},
    { "procTest2", (REGISTRYFUNCTION)procTest2},
};

static void testSubRegistrar(void)
{
    registryFunctionRefAdd(testSubRef,NELEMENTS(testSubRef));
}
epicsExportRegistrar(testSubRegistrar);

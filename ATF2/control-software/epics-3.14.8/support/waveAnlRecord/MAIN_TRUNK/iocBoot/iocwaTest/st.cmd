#!../../bin/linux-x86/waTest

## You may have to change test to something else
## everywhere it appears in this file

< envPaths

cd ${TOP}

## Register all support components
dbLoadDatabase("dbd/waTest.dbd")
waTest_registerRecordDeviceDriver(pdbbase)

## Load record instances
dbLoadRecords("db/waTest.db","user=playWave")


cd ${TOP}/iocBoot/${IOC}
iocInit()


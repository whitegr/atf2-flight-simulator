#!/bin/sh

#
# Start the IOC then run this script
#
case "$#" in
    1)  ;;
    *)  echo "Usage: $0 user" >&2 ; exit 1 ;;
esac

caput -m "$1:data1" 0,1,5,8,10,10,8,5,1,0 
caput "$1:wa.BGRI" 0
caput "$1:wa.ENRI" 9
caput "$1:wa.PROC" 1
a=`caget "$1:wa.MEAN"`
echo "$a -- expect 4.8"
a=`caget "$1:wa.VAR"`
echo "$a -- expect 16.6222"
a=`caget "$1:wa.SDEV"`
echo "$a -- expect 4.07704"
a=`caget "$1:wa.MADV"`
echo "$a -- expect 3.44"
a=`caget "$1:wa.MIN"`
echo "$a -- expect 0"
a=`caget "$1:wa.MAX"`
echo "$a -- expect 10"
a=`caget "$1:wa.FWHM"`
echo "$a -- expect 4"
a=`caget "$1:wa.PKPK"`
echo "$a -- expect 10"

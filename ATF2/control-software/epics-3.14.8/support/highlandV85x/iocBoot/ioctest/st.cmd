## Example vxWorks startup file

## The following is needed if your board support package doesn't at boot time
## automatically cd to the directory containing its startup script
#cd "/afs/slac.stanford.edu/g/lcls/vol8/epics/TestStand/modules/highlandV85x/Development/iocBoot/ioctest"

< cdCommands
#< ../nfsCommands

cd topbin

## You may have to change test to something else
## everywhere it appears in this file
ld(0,0, "test.munch")

## Register all support components
cd top
dbLoadDatabase("dbd/test.dbd")
test_registerRecordDeviceDriver(pdbbase)

# ===================================================
# Specify number of hiddg85x in the system:
# Also, specify the VME base address
# Max number of cards can only be three
#
# hiddg85x_config(int ncards, int base_addr)
# ===================================================
hiddg85x_config(1,0xc000)

# ======================================================================
# Specify more configuration:
# hiddg85x_config_card configures a single card.
# The arguments are: vlo, vhi for the low and high voltage values
# of the output signal (in Volts)
# trg_lvl is, well, the trigger level for the input signal.
# The defaults for these values you can look up in the manual.
#
# hiddg85x_config_card(int card, double vlo, double vhi, double trg_lvl)
# ========================================================================
#hiddg85x_config_card(1, 0.0, 5.0, 1.25)




## Load record instances
#dbLoadTemplate("db/user.substitutions")
dbLoadRecords("db/DDG_4Chan.db", "NAME=DDG1:ATF,CARD=0")

cd startup
iocInit


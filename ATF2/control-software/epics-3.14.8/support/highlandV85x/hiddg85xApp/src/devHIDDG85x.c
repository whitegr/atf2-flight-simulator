/*
 * Device support for Highland Digital Delay Generator V85x
 *
 * $Log: devHIDDG85x.c,v $
 * Revision 1.1.1.1  2010/11/01 00:27:11  ernesto
 * initial import of the Highland Technologies V850/851 Delay Generator Driver
 *
 * Revision 1.6.2.1  2007/01/17 10:50:51  rcsadm
 * added support for bo (control register bits) and longout (intrate register)
 *
 * Revision 1.6  2006/03/30 13:17:04  franksen
 * do NOT consider insertion delay (so that RVAL==0 => delay==INS_DELAY)
 *
 * Revision 1.5  2004/03/22 16:01:18  luchini
 * Mod error definition, removed spaces before ##
 *
 * Revision 1.4  2003/12/16 13:12:55  franksen
 * some enhancements + general cleanup
 *
 * Revision 1.3  2003/09/25 11:21:51  franksen
 * expand all tabs
 *
 * Revision 1.2  1999/09/09 13:15:35  franksen
 * return 2 from init_record
 *
 * Revision 1.1.1.1  1999/07/13 17:58:04  franksen
 * Imported using tkCVS
 */
 
 /* Ernest Williams at SLAC changes for EPICS R3.14.10 and higher 
 epicsIndex is no longer in EPICS BASE
 Also, use the following for format specifiers to avoid compile
 time warnings:
%hhu is for unsigned char.
%hu is for unsigned short.
%u is for unsigned int.
%lu is for unsigned long int.
%llu is for unsigned long long int.
*/

#include <epicsStdlib.h>
#include <epicsThread.h>
#include <iocsh.h>
#include <epicsTypes.h>
#include <errlog.h>
#include <epicsString.h>
#include <drvSup.h>
#include <devLib.h>
#include <dbDefs.h>
#include <dbFldTypes.h>
#include <dbAccess.h>
#include <dbScan.h>
#include <devSup.h>
#include <aoRecord.h>
#include <aiRecord.h>
#include <biRecord.h>
#include <boRecord.h>
#include <longoutRecord.h>
#include <menuConvert.h>
#include <recGbl.h>
#include <alarm.h>

#include <limits.h>


/* local includes */
#include <drvHIDDG85x.h>

/* NOTE: This must be the last include ?le in the source modul */
#include <epicsExport.h>

/* card specific stuff */
#if 0
#define INS_DELAY       640     /* 640 digits = 25 ns insertion delay */
#endif

#if 0
#define MIN_DELAY       INS_DELAY
#define MAX_DELAY       LONG_MAX
#endif

#if 0
#define MIN_DELAYNAN    0.0390625   /* minimum delay nanoseconds */
#define MAX_DELAYNAN    167772200.0 /* maximum width nanoseconds */
#endif

/* configuration defaults */
#if 0
#define VOUT_HI 5.0             /* voltage out high value */
#define VOUT_LO 0.0             /* voltage out low value */
#define TRG_LVL 1.25            /* external trigger level value */
#endif

/* additional channels for bi support */
#define TIMING_CYCLE    NUM_CHANNELS
#define EOD_INTERVAL    (NUM_CHANNELS+1)

/* error reporting */
#define error(format, args...)\
    ( errlogSevPrintf(errlogFatal,  "%s: " format, __FUNCTION__ , ## args ), -1 )

#define warning(format, args...) \
    ( errlogSevPrintf(errlogMinor, "%s: " format, __FUNCTION__ , ## args ), 0 )

typedef struct hiddg85x_card hiddg85x_card;
typedef struct hiddg85x_chan hiddg85x_chan;

/* per card information */
struct hiddg85x_card
{
    hiddg85x_card *next;        /* link to next card structure */
    hiddg85x_chan *channels[NUM_CHANNELS+2];    /* last two channels are for bi */
    int timing_cycle;           /* last sample of the timing_cycle status bit */
    int eod_interval;           /* last sample of the eod_interval status bit */
    IOSCANPVT ioscanpvt;        /* i/o interrupt source */
    int card;                   /* card number */
};

/* per channel information */
struct hiddg85x_chan
{
    hiddg85x_card *pcard;       /* link to next card structure */
    int force_eod;              /* should eod be forced when activating? */
    unsigned mode;              /* waveform mode, see drvHiDDGV85x.h */
    int chan;                   /* channel/signal number */
};

/* the list of card structures */
hiddg85x_card *hiddg85x_list = NULL;

/******************************************************************************/
/*                              common routines                               */
/******************************************************************************/

static hiddg85x_card *find_card(int card)
{
    hiddg85x_card *pcard;

    for(pcard = hiddg85x_list; pcard; pcard = pcard->next)
        if (pcard->card == card)
            break;
    return pcard;
}

static hiddg85x_card *init_card(char *rec_name, int card)
{
    epicsStatus status;
    hiddg85x_card *pcard = find_card(card);

    if(!pcard) {
        status = phiddg85xDrv->getStatusBits(card, 0, 0);
        if (status)
            return error("%s: bad card number %d\n", rec_name, card), pcard;

        /* allocate new structure */
        pcard = calloc(1,sizeof(hiddg85x_card));
        if(!pcard)
            return error("%s: out of memory\n", rec_name), pcard;

        /* initialize */
        pcard->card = card;
        scanIoInit(&pcard->ioscanpvt);
        /* other fields stay 0 */

        /* link into list */
        pcard->next = hiddg85x_list;
        hiddg85x_list = pcard;
    }
    return pcard;
}

/******************************************************************************/
/*                                  ao support                                */
/******************************************************************************/

static long init_record_ao();
static long write_ao();
static long special_linconv_ao();

struct
{
    long number;
    DEVSUPFUN report;
    DEVSUPFUN init;
    DEVSUPFUN init_record;
    DEVSUPFUN get_ioint_info;
    DEVSUPFUN write;
    DEVSUPFUN special_linconv;
}
devAoHIDDG85x =
{
    6,
    NULL,
    NULL,
    init_record_ao,
    NULL,
    write_ao,
    special_linconv_ao
};
epicsExportAddress(dset, devAoHIDDG85x);

static long init_record_ao(aoRecord *prec)
{
    long status;
    struct vmeio *pvmeio;       /* pointer to vmeio portion of OUT field */
    unsigned mode;
    char feod_str[MAX_STRING_SIZE];
    hiddg85x_chan *pchan;
    hiddg85x_card *pcard;
    epicsUInt32     card;

    prec->pact = TRUE;

    if(prec->out.type != VME_IO)
        return error("%s: link type must be VME_IO\n", prec->name);

    /* check the channel number */
    pvmeio = &(prec->out.value.vmeio);
    if(pvmeio->signal < 0 || pvmeio->signal >= NUM_CHANNELS)
        return error("%s: signal number %d out of range [0..%d[\n", 
            prec->name, pvmeio->signal, NUM_CHANNELS);

    card =  pvmeio->card;
    pcard = init_card(prec->name, pvmeio->card);
    if (!pcard)
    {
        recGblSetSevr(prec,epicsAlarmDisable, epicsSevInvalid);
        return -1;
    }
    if ( HIDDG85X_ABSENT == phiddg85xDrv->getPresence(pvmeio->card) )
    {
        recGblSetSevr(prec,epicsAlarmDisable, epicsSevInvalid);
        return error("%s: no card %u\n", prec->name, card);
    }

    pchan = (hiddg85x_chan *)calloc(1,sizeof(hiddg85x_chan));
    if (!pchan)
        return error("%s: out of memory\n", prec->name);

    if (pcard->channels[pvmeio->signal])
        warning("%s: channel %d already used\n", prec->name, pvmeio->signal);
    pcard->channels[pvmeio->signal] = pchan;
    pchan->pcard = pcard;

    /* analyse the parm string */
    status = sscanf(pvmeio->parm, " %d %s", &mode, feod_str);
    if (status < 1)
        mode = DELAYPLUS;   /* default if parm == "" */
    else if (status == 2 && epicsStrCaseCmp(feod_str,"force_eod") == 0)
        pchan->force_eod = TRUE;
    if (mode > OR_WIDTHS && mode != ONE_SHOT)
        return error("%s: invalid mode %d\n", prec->name, mode);
    pchan->mode = mode;

    pchan->chan = pvmeio->signal;

    /* save pointer to channel structure in the record */
    prec->dpvt = pchan;

    /* set up the card */
    status = phiddg85xDrv->setMode(pvmeio->card, pvmeio->signal, mode);
    if (status)
        return error("%s: setMode failed\n", prec->name);

    /* calculate  value for the linear conversion slope */
    if(prec->linr == menuConvertLINEAR)
        special_linconv_ao(prec, 1);

    prec->pact = FALSE;
    return 2; /* no warm reboot, registers are write-only :( */
}

static long write_ao(aoRecord *prec)
{
    long status;
    hiddg85x_chan *pchan = prec->dpvt;
    hiddg85x_card *pcard = pchan->pcard;
    long delay = prec->rval /* - INS_DELAY */;

    if(delay < 0.0)
        delay = 0.0;

    /* set the delay */
    status = phiddg85xDrv->setDelayRaw(pcard->card, pchan->chan, delay);
    if (status)
        return error("%s: setDelayRaw failed\n", prec->name);

    /* ask the card to activate settings */
    if(status == 0)
        status = phiddg85xDrv->activate(pcard->card, pchan->force_eod);

    if(status == 0)
        prec->rbv = delay;

    if(status != 0)
        recGblSetSevr(prec, WRITE_ALARM, INVALID_ALARM);
    return status;
}

static long special_linconv_ao(aoRecord *prec, int after)
{
    double rawf = (double)ULONG_MAX+1.0;
    double rawl = 0;

    if(!after) return 0;

    if(prec->linr == menuConvertLINEAR) {
        prec->eoff = (rawf * prec->egul - rawl * prec->eguf) / (rawf - rawl);
        prec->eslo = (prec->eguf - prec->egul) / (rawf - rawl);
    }
    else {
        prec->eoff = 0;
        prec->eslo = 1;
    }

    return 0;
}

/******************************************************************************/
/*                                  bi support                                */
/******************************************************************************/

static long init_record_bi();
static long read_bi();
static long get_ioint_info_bi();

struct
{
    long number;
    DEVSUPFUN report;
    DEVSUPFUN init;
    DEVSUPFUN init_record;
    DEVSUPFUN get_ioint_info;
    DEVSUPFUN read;
}
devBiHIDDG85x =
{
    5,
    NULL,
    NULL,
    init_record_bi,
    get_ioint_info_bi,
    read_bi
};
epicsExportAddress(dset, devBiHIDDG85x);

void poll_task(void *not_used)
{
    hiddg85x_card *pcard;
    int timing_cycle, eod_interval;
    epicsStatus status;

    while (TRUE) {
        for(pcard = hiddg85x_list; pcard; pcard = pcard->next) {
            status = phiddg85xDrv->getStatusBits(pcard->card, 
                &timing_cycle, &eod_interval);
            if (status) continue;
            if (timing_cycle != pcard->timing_cycle || eod_interval != pcard->eod_interval) {
                pcard->timing_cycle = timing_cycle;
                pcard->eod_interval = eod_interval;
                scanIoRequest(pcard->ioscanpvt);
            }
        }
        epicsThreadSleep(0.001);
    }
}

static long init_record_bi(biRecord *prec)
{
    struct vmeio *pvmeio;
    static int first_time = TRUE;
    hiddg85x_chan *pchan;
    hiddg85x_card *pcard;
    epicsUInt32     card;

    prec->pact = TRUE;

    if(prec->inp.type != VME_IO)
        error("%s: link type must be VME_IO\n", prec->name);

    pvmeio = &(prec->inp.value.vmeio);
    switch (pvmeio->signal) {
    case TIMING_CYCLE:
    case EOD_INTERVAL:
        break;
    default:
        return error("%s: signal number %d must be %d or %d\n", 
            prec->name, pvmeio->signal, TIMING_CYCLE, EOD_INTERVAL);
    }

    card = pvmeio->card;
    pcard = init_card(prec->name, pvmeio->card);
    if (!pcard)
    {
        recGblSetSevr(prec,epicsAlarmDisable, epicsSevInvalid);
        return -1;
    }
    if ( HIDDG85X_ABSENT == phiddg85xDrv->getPresence(pvmeio->card) )
    {
        recGblSetSevr(prec,epicsAlarmDisable, epicsSevInvalid);
        return error("%s: no card %u\n", prec->name, card);
    }

    pchan = (hiddg85x_chan *)calloc(1,sizeof(hiddg85x_chan));
    if (!pchan)
        return error("%s: out of memory\n", prec->name);

    if (pcard->channels[pvmeio->signal])
        warning("%s: channel %d already used\n", prec->name, pvmeio->signal);
    pcard->channels[pvmeio->signal] = pchan;
    pchan->pcard = pcard;

    pchan->chan = pvmeio->signal;

    /* save pointer to channel structure in the record */
    prec->dpvt = pchan;

    if (first_time) {
        epicsThreadId tid;
        tid = epicsThreadMustCreate("hiddgPollReg", epicsThreadPriorityMax,
                  epicsThreadGetStackSize(epicsThreadStackSmall), poll_task, NULL);
        first_time = FALSE;
    }

    prec->pact = FALSE;
    return 0;
}

static long read_bi(biRecord *prec)
{
    hiddg85x_chan *pchan = (hiddg85x_chan *)prec->dpvt;
    hiddg85x_card *pcard = pchan->pcard;

    if (pchan->chan == TIMING_CYCLE)
        prec->rval = pcard->timing_cycle;
    else if (pchan->chan == EOD_INTERVAL)
        prec->rval = pcard->eod_interval;
    else
        return error("%s: invalid channel number %d\n", prec->name, pchan->chan);
    return 0;
}

static long get_ioint_info_bi(int cmd, biRecord *prec, IOSCANPVT * pioscanpvt)
{
    hiddg85x_card *pcard = find_card(prec->inp.value.vmeio.card);
    if (cmd == 0) {
        pcard = find_card(prec->inp.value.vmeio.card);
        *pioscanpvt = pcard->ioscanpvt;
    }
    else {
        *pioscanpvt = 0;
    }
    return 0;
}

/******************************************************************************/
/*                                  bo support                                */
/* (Ralf Hartmann)                                                            */
/******************************************************************************/

static long init_record_bo();
static long write_bo();

struct
{
    long number;
    DEVSUPFUN report;
    DEVSUPFUN init;
    DEVSUPFUN init_record;
    DEVSUPFUN get_ioint_info;
    DEVSUPFUN read;
}
devBoHIDDG85x =
{
    5,
    NULL,
    NULL,
    init_record_bo,
    NULL,
    write_bo
};
epicsExportAddress(dset, devBoHIDDG85x);

static long init_record_bo(boRecord *prec)
{
    struct vmeio  *pvmeio = &(prec->out.value.vmeio);
    epicsUInt32     card = prec->out.value.vmeio.card;
    char          *parm = prec->out.value.vmeio.parm;
    long           status = 0;
    hiddg85x_card *pcard = NULL;
    epicsUInt16    mask;

	prec->pact = TRUE;
	
	if(prec->out.type != VME_IO)
		return error("%s: link type must be VME_IO\n", prec->name);
	
	pcard = init_card(prec->name, card);
	if (!pcard)
    {
        recGblSetSevr(prec,epicsAlarmDisable, epicsSevInvalid);
		return error("%s: could not initialize card %u\n", prec->name, card);
    }
    if ( HIDDG85X_ABSENT == phiddg85xDrv->getPresence(pvmeio->card) )
    {
        recGblSetSevr(prec,epicsAlarmDisable, epicsSevInvalid);
        return error("%s: no card %u\n", prec->name, card);
    }

	prec->dpvt = (void*)pcard;
	
	status = phiddg85xDrv->getControlValueFromParm(parm, &mask);
	if (0 != status)
		return error("%s: wrong parm (%s)\n", prec->name, parm);	

	prec->mask = mask;
	
    	prec->pact = FALSE;
    	return 2;
}

static long write_bo(boRecord *prec)
{
hiddg85x_card *pcard = (hiddg85x_card *)prec->dpvt;
epicsUInt16 cr;
epicsUInt16 mask;
epicsStatus status;

	status = phiddg85xDrv->getControlRegister(pcard->card, &cr);
	if (0 != status)
        return error("%s: %s() getControlRegister()\n", prec->name, __FUNCTION__);

	mask = (epicsUInt16)prec->mask;
	if (prec->val)	
		cr |= mask;
	else
		cr &= (~mask);
	
	status =  phiddg85xDrv->setControlRegister(pcard->card, cr);
	if (0 != status)
        return error("%s: %s() setControlRegister()\n", prec->name, __FUNCTION__);
	
	return 0;
}
/******************************************************************************/
/*                                  lo support                                */
/* (Ralf Hartmann)                                                            */
/******************************************************************************/
static long init_record_lo();
static long write_longout();
struct {
	long		number;
	DEVSUPFUN	report;
	DEVSUPFUN	init;
	DEVSUPFUN	init_record;
	DEVSUPFUN	get_ioint_info;
	DEVSUPFUN	write_longout;
	DEVSUPFUN	special_linconv;
}devLoHIDDG85x={
	6,
	NULL,
	NULL,
	init_record_lo,
	NULL,
	write_longout,
        NULL
};
epicsExportAddress(dset, devLoHIDDG85x);

static long init_record_lo(longoutRecord* prec)
{
    struct vmeio  *pvmeio = &(prec->out.value.vmeio);
    epicsUInt32     card = prec->out.value.vmeio.card;
    hiddg85x_card *pcard = NULL;


    prec->pact = TRUE;
    if(prec->out.type != VME_IO)
        return error("%s: link type must be VME_IO\n", prec->name);

    pcard = init_card(prec->name, card);
    if (!pcard)
    {
        recGblSetSevr(prec, epicsAlarmDisable, epicsSevInvalid);
		return error("%s: could not initialize card %u\n", prec->name, card);
    }
    if ( HIDDG85X_ABSENT == phiddg85xDrv->getPresence(pvmeio->card) )
    {
        recGblSetSevr(prec, epicsAlarmDisable, epicsSevInvalid);
        return error("%s: no card %u\n", prec->name, card);
    }
    prec->dpvt = (void*)pcard;

    prec->pact = FALSE;
    return 0;

}

static long write_longout(longoutRecord* prec)
{
    hiddg85x_card *pcard  = (hiddg85x_card *)prec->dpvt;
    epicsStatus    status;

    status =  phiddg85xDrv->setIntrateRegister(pcard->card, prec->val);
    if (0 != status)
        return error("%s: %s() setIntrateRegister()\n", prec->name, __FUNCTION__);

    return 0;
}

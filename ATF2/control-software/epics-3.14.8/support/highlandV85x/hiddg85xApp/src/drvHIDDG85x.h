/* 
 * Header file for the Highland digital delay generator driver
 * This defines interface between driver and device support
 *
 * Author:  Rozelle Wright
 * Date:    04-10-96
 *
 *  Experimental Physics and Industrial Control System (EPICS)
 *
 *  Copyright 1991, the Regents of the University of California,
 *  and the University of Chicago Board of Governors.
 *
 *  This software was produced under  U.S. Government contracts:
 *  (W-7405-ENG-36) at the Los Alamos National Laboratory,
 *  and (W-31-109-ENG-38) at Argonne National Laboratory.
 *
 *  Initial development by:
 *      The Controls and Automation Group (AT-8)
 *      Ground Test Accelerator
 *      Accelerator Technology Division
 *      Los Alamos National Laboratory
 *
 *  Co-developed with
 *      The Controls and Computing Group
 *      Accelerator Systems Division
 *      Advanced Photon Source
 *      Argonne National Laboratory
 *
 * $Log: drvHIDDG85x.h,v $
 * Revision 1.1.1.1  2010/11/01 00:27:11  ernesto
 * initial import of the Highland Technologies V850/851 Delay Generator Driver
 *
 * Revision 1.3.2.1  2007/01/17 10:47:30  rcsadm
 * added routines to set/get control register, int(ernal) rate register;
 * added routine to retrieve control values by name (string)
 *
 * Revision 1.3  2003/12/16 13:12:56  franksen
 * some enhancements + general cleanup
 *
 *
 * Modification Log:
 * -----------------
 * 01   03-06-95    mrk Moved all driver specific code to drvAb.c
 */

 /* Ernest Williams at SLAC changes for EPICS R3.14.10 and higher 
 epicsIndex is no longer in EPICS BASE
*/
 
 
 
#ifndef INCDRVHIDDGV85X_H
#define INCDRVHIDDGV85X_H

#include <epicsTypes.h>

/* waveform modes */
#define DELAYPLUS       0x0
#define DELAYMINUS      0x1
#define WIDTHPLUS       0x2
#define WIDTHMINUS      0x3
#define TODELAYPLUS     0x4
#define TODELAYMINUS    0x5
#define OR_ALL          0x6
#define OR_WIDTHS       0x7
#define ONE_SHOT        0x10

/* number of channels per card */
#define NUM_CHANNELS    6

/* configuration defaults */
#define DEF_DEBUG_LEVEL     0
#define DEF_VHI             5.0
#define DEF_VLO             0.0
#define DEF_TRG_LVL         (DEF_VLO + (DEF_VHI-DEF_VLO)/4)
/*
#define DEF_BASE_ADDR       0xc040
*/

/*
The V850-series modules are register-based VME slave devices capable of performing 8 or 16-bit data transactions, formally 'A16:D16:D08(EO)'.
*/
/*
A DIPswitch is provided on the DDG module for selection of the VME bus module base address; bits are clearly labeled on the switch. The switch is accessible without removing the module cover. Note: In accordance with VXI standards, the module occupies 32 words (64 bytes) in the high 4K of the 16-bit addressing space, namely the address range C000 - FFC0. Modules should not be switched to have base address FFC0 (all switches set to '1') as that might interfere with automatic module address assignment in certain VXI systems.
The V850 responds to hex address modifier codes 29 and 2D.
V850 modules are normally shipped with all address switches set to '0', establishing a base address of C000 hex.
*/

/* Let's try factory default base address */
/* All rockers set to off or zero */
/* A13, A12, A11, A10, A9, A8, A7, A6 */
#define DEF_BASE_ADDR       0xc000
#define DEF_MAX_CARDS       3

typedef enum {HIDDG85X_ABSENT, HIDDG85X_PRESENT} hiddg85x_presence;

/*entry table for dev to drv routines */
typedef struct
{
    epicsStatus (*setVout) (int card, double vlo, double vhi);
    epicsStatus (*setTrigLevel) (int card, double trg_lvl);
    epicsStatus (*setMode) (int card, int channel, unsigned int mode);
    epicsStatus (*setDelay) (int card, int channel,double *delay, epicsUInt32 * rdelay, int roffset);
    epicsStatus (*setDelayRaw) (int card, int channel, epicsUInt32 rdelay);
    epicsStatus (*activate) (int card, int force_eod);
    epicsStatus (*getStatusBits) (int card, int *ptiming_cycle, int *peod_interval);
    epicsStatus (*setControlRegister) (int card, epicsUInt16 bits);
    epicsStatus (*getControlRegister) (int card, epicsUInt16* bits);
    epicsStatus (*getControlValueFromParm)(const char* parm, epicsUInt16* cv);
    epicsStatus (*setIntrateRegister) (int card, epicsUInt16 rate);
    epicsStatus (*getIntrateRegister) (int card, epicsUInt16* rate);
    hiddg85x_presence (*getPresence) (epicsUInt32 card); 
}
hiddgv85xDrv;
extern hiddgv85xDrv *phiddg85xDrv;

#endif

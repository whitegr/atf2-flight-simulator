/*
 * Driver Support Routines for Highland DDGV851
 *
 *  Author: Rozelle Wright
 *  Date:   3-25-96
 *
 *  Experimental Physics and Industrial Control System (EPICS)
 *
 *  Copyright 1991, the Regents of the University of California,
 *  and the University of Chicago Board of Governors.
 *
 *  This software was produced under  U.S. Government contracts:
 *  (W-7405-ENG-36) at the Los Alamos National Laboratory,
 *  and (W-31-109-ENG-38) at Argonne National Laboratory.
 *
 *  Initial development by:
 *      The Controls and Automation Group (AT-8)
 *      Ground Test Accelerator
 *      Accelerator Technology Division
 *      Los Alamos National Laboratory
 *
 *  Co-developed with
 *      The Controls and Computing Group
 *      Accelerator Systems Division
 *      Advanced Photon Source
 *      Argonne National Laboratory
 *
 */

#include <math.h>

#include <epicsStdlib.h>
#include <iocsh.h>
#include <epicsTypes.h>
#include <errlog.h>
#include <epicsString.h>
#include <drvSup.h>
#include <devLib.h>
#include <dbDefs.h>
#include <registryFunction.h>

#include <drvHIDDG85x.h>

#include <epicsExport.h>

/* Ernest Williams at SLAC changes for EPICS R3.14.10 and higher 
 epicsIndex is no longer in EPICS BASE
 Also, use the following for format specifiers to avoid compile
 time warnings:
%hhu is for unsigned char.
%hu is for unsigned short.
%u is for unsigned int.
%lu is for unsigned long int.
%llu is for unsigned long long int.
*/




/* module IDs */
#define VXI_MFR             0xFEEE
#define VXITYPE             0x5943

/* status register bits */
#define TIMING_CYCLE_BIT    0x100   /* BIT is HIGH during Timing cycle */
#define EOD_INTERVAL_BIT    0x200   /* BIT is HIGH during EOD interval */

/* channel output */
#define VLOWMIN             -2.0
#define VHIGHMAX            5.0
#define VLOWFACTOR          32640   /* R = (- VLOW ) * VLOWFACTOR */
#define VHIGHFACTOR         13056   /* R = ( VHIGH ) * VHIGHFACTOR */

/* trigger input */
#define TRG_LVLMIN          -2.5
#define TRG_LVLMAX          2.5
#define TRG_LVLFACTOR       13107

/* action register */
#define FEOD    0x1             /* forces end-of-delay */
#define CAL     0x8             /* initiaties a phase-locked-loop recalibration */
#define XFR     0x80            /* causes newly written timing values to be
                                   queued for loading at the next EOD */
#define FIRE    0x8000          /* triggers DDR if module is in VME trigger mode. */

/* arrays for use of ioreport */
LOCAL epicsUInt16 action[] =
{
    FEOD, 
    CAL, 
    XFR, 
    FIRE
};
LOCAL char *action_str[] =
{
    "FEOD", 
    "CAL", 
    "XFR", 
    "FIRE"
};

#define AFLAGNUM (sizeof action)/sizeof(short)

/* CONTROL register Definitions */
#define SLOPE   0x100           /* selects negative slope */
#define VTRIG   0x200           /* module is triggerable by the
                                   FIRE action bit */
#define ITRIG   0x400           /* internal rate generator triggers the DDG;
                                   if neither VTRIG or ITRIG are set, the trigger 
                                   is external */
#define PRESCL  0x800           /* when set, prescales the internal rate by
                                   100 */
#define LDNOW   0x1000          /* writes to the INTRATE register will load the
                                   internal rate immediately.  If this is not set
                                   it will be loaded at the next internal trig */
#define CCAL    0x2000          /* forces module into CAL mode
                                   for maintenance only */
#define EXTB    0x4000          /* front-panel CLOCK is an input and the module will 
                                   phase-lock to external 10 MHz source
                                   When not set CLOCK is a 10 MHz output */
#define DISARM 0x8000           /* when set, the DDG will not trigger */
LOCAL epicsUInt16 control[] =
{
    SLOPE, 
    VTRIG, 
    ITRIG, 
    PRESCL, 
    LDNOW, 
    CCAL, 
    EXTB, 
    DISARM
};
LOCAL char *control_str[] =
{
    "SLOPE", 
    "VTRIG", 
    "ITRIG", 
    "PRESCL",
    "LDNOW", 
    "CCAL",
    "EXTB", 
    "DISARM",
    NULL
};

LOCAL epicsStatus getControlValueFromParm(const char* parm, epicsUInt16* cv)
{
	char** val = control_str;
		
	while (NULL != *val)
		if (0 != epicsStrCaseCmp(parm, *val))
			val++;
		else break;

	if (NULL != *val)
	{
		*cv = control[val-control_str];
		return 0;
	}
	else return -1;
}

#define CFLAGNUM (sizeof control)/sizeof(short)

/* WAVEFORM MODE MASKS and SHIFTS */
#define LOWMASK 0xf
#define HIMASK 0xf0
#define NIBBLESHIFT 4

LOCAL epicsUInt16 wavemode[] =
{
    DELAYPLUS, 
    DELAYMINUS, 
    WIDTHPLUS, 
    TODELAYPLUS,
    TODELAYMINUS,
    OR_ALL, 
    OR_WIDTHS, 
    ONE_SHOT
};
LOCAL char *wavemode_str[] =
{
    "DELAYPLUS", 
    "DELAYMINUS", 
    "WIDTHPLUS",
    "TODELAYPLUS", 
    "TODELAYMINUS",
    "OR_ALL", 
    "OR_WIDTHS"
};

#define WAVEMODENUM (sizeof wavemode)/sizeof(short)

/* Delay Factor */
/* 
 * Bit resolution is 39.0625 pico seconds. This means that to transform
 * nanoseconds into bits one must divide the number of nanoseconds
 * by 0.0390625, which is the equivalent of multiplying by 25.6.
 */
#define NSECFACTOR 25.6

/* Register Area Definition */
/*
 * The first three registers are read only and except for the
 * unused registers all the rest are write only. Only the status
 * register will be modified by the hardware.
 */
typedef struct
{                               /* hex offset -- description */
    volatile epicsUInt16 vxi_mfr;  /* 00 vxi manufacturer id  */
    volatile epicsUInt16 vxitype;  /* 02 module type */
    volatile epicsUInt16 vxi_sts;  /* 04 VXI status register */
    epicsUInt16 unused0;           /* 06 unused */
    epicsUInt16 vout_lo;           /* 08 low voltage level for T0 and channnels */
    epicsUInt16 vout_hi;           /* 0A high voltage level for T0 and channnels */
    epicsUInt16 actions;           /* 0C module action command bits */
    epicsUInt16 trg_lvl;           /* 0E external trigger level set */
    epicsUInt16 control;           /* 10 module control bits */
    epicsUInt16 intrate;           /* 12 internal trig reprate divisor */
    epicsUInt16 unused[6];         /* 14 - 1E unused registers */
    epicsUInt16 delays[12];        /* 20 - 36 even indices are high 16 bits of delay
                                   odd indices are low 16 bits of delay for
                                   channel 1 -- delays [0-1]
                                   through channel 6 -- delays [10-11] */
    epicsUInt16 waves[3];          /* 38 - 3C registers determining waveform modes 
                                   for 1 and 2, 3 and 4, 5 and 6 mode
                                   nibble for odd channels are in
                                   lowest 4 bits for even channels in next
                                   4 bits. high bytes are unused */
    epicsUInt16 unused3;           /* 3E */
}
hiddg85x_regs;

/* values written to write-only registers */
typedef struct
{
    epicsUInt16 st_vout_lo;        /* low voltage level for T0 and channnels */
    epicsUInt16 st_vout_hi;        /* high voltage level for T0 and channnels */
    epicsUInt16 st_actions;        /* module action command bits */
    epicsUInt16 st_trg_lvl;        /* level for external trigger */
    epicsUInt16 st_control;        /* module control bits */
    epicsUInt16 st_intrate;        /* internal trig reprate divisor */
    epicsUInt16 st_delays[12];     /* lo and hi delay values for each channel */
    epicsUInt16 st_waves[3];       /* waveform modes for each wave */
}
hiddg85x_store;

/* everything we need to know for a single card */
typedef struct
{
    hiddg85x_regs     *pregs;       /* pointer to vme memory register location */
    hiddg85x_store     store;       /* record of values written to registers */
    hiddg85x_presence  presence;
}
hiddg85x_info;

/* globals */
typedef struct
{
    int debug_level;            /* debug level */
    int max_cards;              /* max number of cards */
    size_t base_addr;           /* start address of cards */
    hiddg85x_info *pinfo;       /* info for each card */
    hiddg85x_regs *pregs;       /* registers for each card */
}
hiddg85x_driver;

/* defaults for the globals */
LOCAL hiddg85x_driver hiddg85x = {
    DEF_DEBUG_LEVEL,
    DEF_MAX_CARDS,
    DEF_BASE_ADDR,
    NULL,
    NULL
};

/* error reporting & debugging */
#define error(format, args...) \
    errlogSevPrintf(errlogFatal, "drvHIDDG85x: " format , ## args), -1
#define warning(format, args...) \
    errlogSevPrintf(errlogMinor, "drvHIDDG85x: " format , ## args), 0
#define debug(level, format, args...) \
    if ((level) < hiddg85x.debug_level) { \
        errlogSevPrintf(errlogInfo, "drvHIDDG85x: " format , ## args); \
    }
#define info(format, args...) debug(-1, format , ## args)

/* forward routine declarations */
LOCAL epicsStatus hiddg85x_init_info(void);
LOCAL epicsStatus hiddg85x_init_card(int card);
LOCAL epicsStatus hiddg85x_init(void);
LOCAL epicsStatus hiddg85x_io_report(int level);

LOCAL epicsStatus setVout(int card, double vlo, double vhi);
LOCAL epicsStatus setTrigLevel(int card, double trg_lvl);
LOCAL epicsStatus setMode(int card, int channel, unsigned int mode);
LOCAL epicsStatus setDelay(int card, int channel, double *delay,
    epicsUInt32 * rdelay, int roffset);
LOCAL epicsStatus activate(int card, int force_eod);
LOCAL hiddg85x_presence getPresence(epicsUInt32 card);

/* non-static routines (to be called from startup file) */

/* 
 * calling hiddg85x_debug with a level greater than 0 will turn
 * ever increasing levels of verbose debugging messages on
 */
static const iocshArg            set_debugArg     = {"debug_level", iocshArgInt};
static const iocshArg     *const set_debugArgs[1] = {&set_debugArg};
static const iocshFuncDef        set_debugDef     = {"hiddg85x_set_debug", 1, set_debugArgs};
epicsStatus hiddg85x_set_debug(int level)
{
    hiddg85x.debug_level = level;
    return 0;
}

static const iocshArg            set_base_addrArg     = {"base_addr", iocshArgInt};
static const iocshArg     *const set_base_addrArgs[1] = {&set_base_addrArg};
static const iocshFuncDef        set_base_addrDef     = {"hiddg85x_set_base_addr", 1, set_base_addrArgs};
epicsStatus hiddg85x_set_base_addr(int base_addr)
{
    if (hiddg85x.pinfo) {
        return error("base_addr already fixed\n");
    }
    if (base_addr < 0) {
        return error("base_addr must be positive\n");
    }
    hiddg85x.base_addr = base_addr;
    return 0;
}

static const iocshArg            set_max_cardsArg     = {"max_cards", iocshArgInt};
static const iocshArg     *const set_max_cardsArgs[1] = {&set_max_cardsArg};
static const iocshFuncDef        set_max_cardsDef     = {"hiddg85x_set_max_cards", 1, set_max_cardsArgs};
epicsStatus hiddg85x_set_max_cards(int ncards)
{
    if (hiddg85x.pinfo) {
        return error("number of cards already fixed\n");
    }
    if (ncards < 0)  {
        return error("number of cards must be positive\n");
    }
    hiddg85x.max_cards = ncards;
    return 0;
}

static const iocshArg            configArg0    = {"cards",     iocshArgInt};
static const iocshArg            configArg1    = {"base_addr", iocshArgInt};
static const iocshArg     *const configArgs[2] = {&configArg0, &configArg1};
static const iocshFuncDef        configDef     = {"hiddg85x_config", 1, configArgs};
epicsStatus hiddg85x_config(int ncards, int base_addr)
{
    epicsStatus status;

    if (hiddg85x.pinfo) {
        return error("number of cards and base_addr already fixed\n");
    }
    status = hiddg85x_set_max_cards(ncards);
    if (status) return status;
    status = hiddg85x_set_base_addr(base_addr);
    return status;
}

/*
 * this routine should be called only *after* base_addr and max_cards
 * have been defined
 */
static const iocshArg            config_cardArg0     = {"card",    iocshArgInt};
static const iocshArg            config_cardArg1     = {"vlo",     iocshArgDouble};
static const iocshArg            config_cardArg2     = {"vhi",     iocshArgDouble};
static const iocshArg            config_cardArg3     = {"trg_lvl", iocshArgDouble};
static const iocshArg     *const cconfig_cardArgs[4] = {&config_cardArg0, &config_cardArg1, &config_cardArg2, &config_cardArg3};
static const iocshFuncDef        config_cardDef      = {"hiddg85x_config_card", 1, cconfig_cardArgs};
epicsStatus hiddg85x_config_card(int card, double vlo, double vhi, double trg_lvl)
{
    hiddg85x_info *pinfo;
    if (card < 0 || card >= hiddg85x.max_cards) {
        return error("invalid argument: card %d (arg 1) out of range [0..%d[\n", card, hiddg85x.max_cards);
    }
    if (hiddg85x_init_info())
        return -1;
    pinfo = hiddg85x.pinfo + card;
    if (vlo < VLOWMIN || vlo >= vhi || vhi > VHIGHMAX) {
        return error("invalid argument: must have %f <= vlo (arg 2) < vhi (arg 3) <= %f\n", VLOWMIN, VHIGHMAX);
    }
    pinfo->store.st_vout_lo = vlo * VLOWFACTOR;
    pinfo->store.st_vout_hi = vhi * VHIGHFACTOR;
    if (trg_lvl < TRG_LVLMIN || trg_lvl > TRG_LVLMAX) {
        return error("invalid argument: trg_lvl %f (arg 4) out of range [%f,%f]\n", trg_lvl, TRG_LVLMIN, TRG_LVLMAX);
    }
    pinfo->store.st_trg_lvl = (TRG_LVLMAX - trg_lvl) * VHIGHFACTOR;
    return 0;
}

/* end global routines */


struct
{
    long number;
    DRVSUPFUN report;
    DRVSUPFUN init;
}
drvHIDDG85x =
{
    2,
    (DRVSUPFUN)hiddg85x_io_report,
    (DRVSUPFUN)hiddg85x_init
};
epicsExportAddress(drvet, drvHIDDG85x);

LOCAL epicsStatus hiddg85x_dump(int card, int level)
{
    hiddg85x_info *pinfo = hiddg85x.pinfo + card;
    hiddg85x_store *pstore;
    epicsUInt16 *ptemp;
    epicsUInt32 temp;
    int i, j;
    double ftemp;

    printf("VME_ADDR 0x%p\n", pinfo->pregs);

    /* print current card status */
    temp = pinfo->pregs->vxi_sts;
    printf("status register 0x%x ", temp);
    if (temp & TIMING_CYCLE_BIT)
        printf("TIMING ");
    if (temp & EOD_INTERVAL_BIT)
        printf("EOD");
    printf("\n");

    /* print stored card settings */
    pstore = &(pinfo->store);

    /* print voltage output levels */
    temp = pstore->st_vout_lo;
    ftemp = temp;
    ftemp = -(ftemp / VLOWFACTOR);
    printf("VOUT LO: %0x %fV;", temp, ftemp);
    temp = pstore->st_vout_hi;
    ftemp = temp;
    ftemp = ftemp / VHIGHFACTOR;
    printf(" HI: %0x %fV \n", temp, ftemp);

    /* print trigger level */
    temp = pstore->st_trg_lvl;
    ftemp = temp;
    ftemp = TRG_LVLMAX - ftemp / TRG_LVLFACTOR;
    printf("TRG_LVL: %0x %fV \n", temp, ftemp);

    /* print actions */
    temp = pstore->st_actions;
    printf("ACTIONS:0x%x ", temp);
    for (i = 0; i < AFLAGNUM; i++) {
        if (temp & action[i])
            printf("%s ", action_str[i]);
    }
    printf("\n");

    /* print control */
    temp = pstore->st_control;
    printf("CONTROL:0x%x ", temp);
    for (i = 0; i < CFLAGNUM; i++) {
        if (temp & control[i])
            printf("%s ", control_str[i]);
    }
    printf("\n");

    /* print internal level rates */
    temp = pstore->st_intrate;
    printf("INTRATE:0x%x = %d \n", temp, temp);

    /* print channel delays */
    ptemp = pstore->st_delays;
    for (i = 1; i <= 6; i++) {
        temp = *ptemp++;
        printf("delay[%d] = 0x%xhi << 16 + 0x%xlo = ", i, temp,
            *ptemp);
        temp = (temp << 16) + *ptemp++;
        printf(" 0x%x = %d = ", temp, temp);
        ftemp = temp;
        ftemp = ftemp / NSECFACTOR;
        printf("%f\n", ftemp);
    }

    /* print waveform modes */
    ptemp = pstore->st_waves;
    for (i = 1; i <= 3; i++) {
        printf("WAVES[%d] = 0x%x = 0x%x\n", i, *ptemp,
            pstore->st_waves[i]);
        temp = (*ptemp) & LOWMASK;
        printf("WAVEMODES[%d]: 0x%x ", 2 * i - 1, temp);
        for (j = 0; j < WAVEMODENUM; j++)
            if (temp == wavemode[j])
                printf(" %s ", wavemode_str[j]);
        printf("\n");
        temp = ((*ptemp) & HIMASK) >> NIBBLESHIFT;
        printf("WAVEMODES[%d]: 0x%x ", 2 * i, temp);
        for (j = 0; j < WAVEMODENUM; j++)
            if (temp == wavemode[j])
                printf(" %s ", wavemode_str[j]);
        printf("\n");
        ptemp++;
    }

    return 0;
}

LOCAL epicsStatus hiddg85x_init_card(int card)
{
    hiddg85x_regs *pregs = hiddg85x.pregs + card;
    hiddg85x_info *pinfo = hiddg85x.pinfo + card;
    epicsUInt16    cok;
#if 0
    int            i;
#endif

    if ( devReadProbe(1, pregs, &cok) ) {
        return error("no card found at address %p\n", pregs);
    }
    pinfo->pregs = pregs;
    debug(1, "found card at address %p\n", pregs);

    /* check that this is really the right type of card */
    if (pregs->vxi_mfr != VXI_MFR || pregs->vxitype != VXITYPE) {
        return error("card MFR 0x%x of wrong type 0x%x at address %p\n",
            pregs->vxi_mfr, pregs->vxitype, pregs);
        return -1;
    }

    /*
     * If we found a valid card, put it in it's initial state
     */
    pregs->vout_lo = pinfo->store.st_vout_lo;
    pregs->vout_hi = pinfo->store.st_vout_hi;
    pregs->trg_lvl = pinfo->store.st_trg_lvl;
#if 0
    pregs->control = DISARM;    /* Disable signals */
    for (i = 0; i < 12; i++)
        pregs->delays[i] = 0;   /* Set all delays to 0 */
    for (i = 0; i < 3; i++)
        pregs->waves[i] = 0;    /* Set all channels to mode 0 */
    pregs->actions = XFR;       /* Load all values */
    pregs->control = 0;         /* Re-enable signals */
#endif
    pinfo->presence = HIDDG85X_PRESENT;
    return 0;
}

LOCAL epicsStatus hiddg85x_init_info(void)
{
    int card;

    /* initialize the card information array (if not yet done) */
    if (hiddg85x.pinfo)
        return 0;

    hiddg85x.pinfo = (hiddg85x_info *)calloc(hiddg85x.max_cards, sizeof(hiddg85x_info));
    if (!hiddg85x.pinfo) {
        return error("out of memory\n");
    }
    /* set defaults */
    for (card = 0; card < hiddg85x.max_cards; card ++) {
        hiddg85x.pinfo[card].store.st_vout_lo = DEF_VLO * VLOWFACTOR;
        hiddg85x.pinfo[card].store.st_vout_hi = DEF_VHI * VHIGHFACTOR;
        hiddg85x.pinfo[card].store.st_trg_lvl = (TRG_LVLMAX - DEF_TRG_LVL) * VHIGHFACTOR;
        hiddg85x.pinfo[card].presence         = HIDDG85X_ABSENT;
    }
    return 0;
}

LOCAL epicsStatus hiddg85x_init(void)
{
    void        *plocaladdr;
    int          card;
    epicsStatus  status     = 0;
    int          ncards     = 0;
    long         size;

    if (hiddg85x_init_info())
        return -1;
    /* register space for all possible cards */
    size = hiddg85x.max_cards * sizeof(hiddg85x_regs);
    debug(1, "registering 0x%x of size %ld\n", hiddg85x.base_addr, size);
    status = devRegisterAddress("drvHIDDG85x", atVMEA16,
        hiddg85x.base_addr, size, (volatile void**)&plocaladdr);

    if (status) {
        /* why unregister if register went wrong? */
        /* devUnregisterAddress(atVMEA16, (void*)hiddg85x.base_addr, "drvHIDDG85x"); */
        return error("unable to register %d cards at 0x%x\n",
            hiddg85x.max_cards, hiddg85x.base_addr);
    }

    hiddg85x.pregs = (hiddg85x_regs *) plocaladdr;
    for (card = 0; card < hiddg85x.max_cards; card++) {
        status = hiddg85x_init_card(card);
        if (0 == status) ncards++;
    }

    info("%d of %d configured cards found\n", ncards, hiddg85x.max_cards);
    return 0;
}

LOCAL epicsStatus hiddg85x_io_report(int level)
{
    int card;
    hiddg85x_info *pinfo;

    for (card = 0; card < hiddg85x.max_cards; card++) {
        pinfo = hiddg85x.pinfo + card;
        if (!pinfo->pregs)
            continue;
        printf("card%d:VXI MFR 0x%x VXITYPE 0X%X; ",
            card, pinfo->pregs->vxi_mfr, pinfo->pregs->vxitype);
        if (level > 0) {
            printf("vxAddr: %p  control: 0x%x\n",
                pinfo->pregs, pinfo->store.st_control);
            hiddg85x_dump(card, level);
        }
    }
    return 0;
}

LOCAL hiddg85x_info *checkCard(int card)
{
    hiddg85x_info *pinfo;

    if (card < 0 || card >= hiddg85x.max_cards) {
        error("bad card number:%d\n", card);
        return NULL;
    }
    pinfo = hiddg85x.pinfo + card;
    if (pinfo->pregs == NULL) {
        error("no card %d found\n", card);
    }
    return pinfo;
}

LOCAL epicsStatus checkChannel(int channel)
{
    epicsStatus status = 0;

    if (channel < 0 || channel >= NUM_CHANNELS) {
        return error("bad channel number:%d\n", channel);
    }
    return status;
}

LOCAL epicsStatus setVout(int card, double vlo, double vhi)
{
    epicsStatus status = 0;
    hiddg85x_info *pinfo;

    if (!(pinfo = checkCard(card)))
        return -1;

    if (vhi > VHIGHMAX || vhi <= vlo || vlo < VLOWMIN) {
        return error("bad parameters card:%d,vhi:%f,vlo:%f\n", 
            card, vhi, vlo);
    }
    pinfo->store.st_vout_lo = vlo * VLOWFACTOR;
    pinfo->store.st_vout_hi = vhi * VHIGHFACTOR;
    pinfo->pregs->vout_lo = pinfo->store.st_vout_lo;
    pinfo->pregs->vout_hi = pinfo->store.st_vout_hi;

    return status;
}

LOCAL epicsStatus setTrigLevel(int card, double trg_lvl)
{
    hiddg85x_info *pinfo;
    epicsStatus status = 0;

    if (!(pinfo = checkCard(card)))
        return -1;

    if (trg_lvl < TRG_LVLMIN || trg_lvl > TRG_LVLMAX) {
        return error("bad parameters card:%d,trg_lvl:%f\n", card, trg_lvl);
    }
    pinfo->store.st_trg_lvl = (TRG_LVLMAX - trg_lvl) * VHIGHFACTOR;
    pinfo->pregs->trg_lvl = pinfo->store.st_trg_lvl;
    return status;
}

LOCAL epicsStatus setMode(int card, int channel, unsigned int mode)
{
    epicsStatus status = 0;
    epicsUInt16 mask = 0xf;
    epicsUInt16 temp;
    hiddg85x_info *pinfo;
    epicsUInt16 *pwave;
    int waveindex = channel / 2;
    int iseven;

    iseven = ((2 * waveindex) == channel);
    if (!(pinfo = checkCard(card)))
        return -1;
    if ( (status = checkChannel(channel)) )
        return status;
    if (mode > ONE_SHOT)
        return -1;
    debug(0, "set card %d channel %d to mode %d\n", card, channel, mode);
    temp = mode;
    if (iseven)
        mask <<= 4;
    else
        temp <<= 4;
    pwave = &(pinfo->store.st_waves[waveindex]);
    *pwave = (*pwave & mask) | temp;
    pinfo->pregs->waves[waveindex] = *pwave;
    debug(0, "set card %d channel %d to mode %d (pwave %p, *pwave 0x%x)\n",
        card, channel, mode, pwave, *pwave);

    return status;
}

LOCAL epicsStatus setDelay(int card, int channel, double *delay, epicsUInt32 * rdelay,
    int roffset)
{
    epicsStatus status = 0;
    double dbl_temp;
    epicsUInt32 temp;
    hiddg85x_info *pinfo;
    epicsUInt16 *pldelbits;        /* least significant 16bits for delay */
    epicsUInt16 *pmdelbits;        /* most significant 16bits for delay */

    return error("Not supported\n");

    if (!(pinfo = checkCard(card)))
        return -1;
    if ( (status = checkChannel(channel)) )
        return status;
    debug(0, "setDelay card %d channel %d delay %f rdelay %d roffset %d\n",
        card, channel, *delay, *rdelay, roffset);

    /* round delay to nearest bit value */
    dbl_temp = floor(*delay * NSECFACTOR + 0.5) - roffset;

    /* reset delay to value obtained from rounding */
    *delay = (dbl_temp + roffset) / NSECFACTOR;
    debug(1, "*delay %g nanoseconds dbl_temp= %g\n", *delay, dbl_temp);

    /* set rdelay and split it into two 16 bit values */
    *rdelay = dbl_temp;
    if ((int) *rdelay < roffset)
        *rdelay = 0;
    debug(0, "*rdelay %d 0x%x\n", *rdelay, *rdelay);

    pmdelbits = &(pinfo->store.st_delays[2 * channel]);
    pldelbits = &(pinfo->store.st_delays[2 * channel + 1]);
    *pmdelbits = (*rdelay & 0xffff0000) >> 16;
    temp = *pmdelbits;
    debug(0, "*pmdelbits %d 0x%x\n", temp, temp);
    *pldelbits = (*rdelay) & 0xffff;
    temp = *pldelbits;
    debug(0, "*pldelbits %d 0x%x\n", temp, temp);
    pinfo->pregs->delays[2 * channel] = *pmdelbits;
    pinfo->pregs->delays[2 * channel + 1] = *pldelbits;

    return status;
}

LOCAL epicsStatus setDelayRaw(int card, int channel, epicsUInt32 rdelay)
{
    epicsStatus status = 0;
    hiddg85x_info *pinfo;
    epicsUInt16 *pldelbits;        /* least significant 16bits for delay */
    epicsUInt16 *pmdelbits;        /* most significant 16bits for delay */

    if (!(pinfo = checkCard(card)))
        return -1;
    if ( (status = checkChannel(channel)) )
        return status;
    debug(0, "setDelay card %d channel %d rdelay %u\n", card, channel, rdelay);

    pmdelbits = &(pinfo->store.st_delays[2 * channel]);
    pldelbits = &(pinfo->store.st_delays[2 * channel + 1]);

    *pmdelbits = (rdelay & 0xffff0000) >> 16;
    *pldelbits = rdelay & 0xffff;

    pinfo->pregs->delays[2 * channel] = *pmdelbits;
    pinfo->pregs->delays[2 * channel + 1] = *pldelbits;

    return status;
}

LOCAL epicsStatus activate(int card, int force_eod)
{
    epicsStatus status = 0;
    hiddg85x_info *pinfo;
    epicsUInt16 *pactions;

    if (!(pinfo = checkCard(card)))
        return -1;
    pactions = &(pinfo->store.st_actions);
    *pactions |= XFR;
    if (force_eod)
        *pactions |= FEOD;
    pinfo->pregs->actions = *pactions;

    return status;
}

LOCAL epicsStatus getStatusBits(int card, int *ptiming_cycle, int *peod_interval)
{
    epicsUInt16 vxi_sts;
    hiddg85x_info *pinfo;

    if (!(pinfo = checkCard(card)))
        return -1;
    vxi_sts = pinfo->pregs->vxi_sts;
    if (ptiming_cycle) *ptiming_cycle = (vxi_sts & TIMING_CYCLE_BIT)?1:0;
    if (peod_interval) *peod_interval = (vxi_sts & EOD_INTERVAL_BIT)?1:0;
    return 0;
}
LOCAL epicsStatus setControlRegister(int card, epicsUInt16 bits)
{    
hiddg85x_info *pinfo;
epicsUInt16 *control;
	
	if (!(pinfo = checkCard(card)))
        	return -1;
	control = &(pinfo->store.st_control);

	*control = bits;
	pinfo->pregs->control = bits;
	return 0;
}

LOCAL epicsStatus getControlRegister(int card, epicsUInt16* bits)
{    
hiddg85x_info *pinfo;
	
	if (!(pinfo = checkCard(card)))
        	return -1;

	*bits = pinfo->store.st_control;

	return 0;
}

LOCAL epicsStatus setIntrateRegister(int card, epicsUInt16 rate)
{    
hiddg85x_info *pinfo;
epicsUInt16 *intrate;
	
	if (!(pinfo = checkCard(card)))
        	return -1;
	intrate = &(pinfo->store.st_intrate);

	*intrate = rate;
	pinfo->pregs->intrate = rate;
	return 0;
}

LOCAL epicsStatus getIntrateRegister(int card, epicsUInt16* rate)
{    
hiddg85x_info *pinfo;
	
	if (!(pinfo = checkCard(card)))
        	return -1;

	*rate = pinfo->store.st_intrate;

	return 0;
}

LOCAL hiddg85x_presence getPresence(epicsUInt32 card)
{
    hiddg85x_info *pinfo;

    if (!(pinfo = checkCard(card)))
            return HIDDG85X_ABSENT;

    return pinfo->presence;
}

/* initialize entry table for dev to drv routines */
LOCAL hiddgv85xDrv hiddgv85xDrvTable =
{
    setVout, setTrigLevel, setMode, setDelay, setDelayRaw,
    activate, getStatusBits, setControlRegister,
    getControlRegister, getControlValueFromParm,
    setIntrateRegister, getIntrateRegister,
    getPresence
};

hiddgv85xDrv *phiddg85xDrv = &hiddgv85xDrvTable;

/* registrar for ioshell */

static registryFunctionRef mySubRef[] = {
    {"hiddg85x_set_debug",     (REGISTRYFUNCTION)hiddg85x_set_debug},
    {"hiddg85x_set_base_addr", (REGISTRYFUNCTION)hiddg85x_set_base_addr},
    {"hiddg85x_set_max_cards", (REGISTRYFUNCTION)hiddg85x_set_max_cards},
    {"hiddg85x_config",        (REGISTRYFUNCTION)hiddg85x_config},
    {"hiddg85x_config_card",   (REGISTRYFUNCTION)hiddg85x_config_card}
};

void drvHIDDG85x_registrar(void)
{
    registryFunctionRefAdd(mySubRef, NELEMENTS(mySubRef));

    iocshRegister(&set_debugDef,     (iocshCallFunc)hiddg85x_set_debug);
    iocshRegister(&set_base_addrDef, (iocshCallFunc)hiddg85x_set_base_addr);
    iocshRegister(&set_max_cardsDef, (iocshCallFunc)hiddg85x_set_max_cards);
    iocshRegister(&configDef,        (iocshCallFunc)hiddg85x_config);
    iocshRegister(&config_cardDef,   (iocshCallFunc)hiddg85x_config_card);
}

epicsExportRegistrar(drvHIDDG85x_registrar);

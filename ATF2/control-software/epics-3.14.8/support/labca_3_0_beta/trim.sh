#!/bin/bash
# script to trim base distribution:
#  - remove '.o' files
#  - remove runpaths
#  - move shared libs to 'shared' subdir because we
#    want to link labca against static version
GFIND=gfind
CHRPATH=/afs/slac/g/lcls/users/strauman/tools/chrpath-0.13/build-$EPICS_HOST_ARCH/chrpath

if [ -z "$EPICS_HOST_ARCH" ] ; then
	echo EPICS_HOST_ARCH not set
	exit 1
fi

if [ ! -d bin/$EPICS_HOST_ARCH ] ; then
	echo "Hmm; no bin/$EPICS_HOST_ARCH directory"
	exit 1
fi

if ! $CHRPATH -v ; then
	echo chrpath tool not found
	exit 1
fi

$GFIND bin/$EPICS_HOST_ARCH -type f -perm +111 -exec sh -c "if [ ! -w '{}' ] ; then chmod u+w '{}'; $CHRPATH -d '{}'; chmod u-w '{}'; else $CHRPATH -d '{}'; fi" \;
$GFIND lib/$EPICS_HOST_ARCH -type f -perm +111 -exec sh -c "if [ ! -w '{}' ] ; then chmod u+w '{}'; $CHRPATH -d '{}'; chmod u-w '{}'; else $CHRPATH -d '{}'; fi" \;

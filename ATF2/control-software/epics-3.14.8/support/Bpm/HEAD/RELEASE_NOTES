Release: Bpm-R5-3-1
  configure/RELEASE: use fcomUtil-R1-0-0, fcom-R1-3-2, udpComm-R1-3-1, event-R3-2-1
  Use iocAdminRTEMS.db (instead of iocAdmin.db)
  Add :CHK PV per BPM to inform that BPM needs calib/ironing; add to save/restore; add to alarm summary
  Remove Cavity BPM raw BSA substitutions (URER, UIMR, VRER, VIMR, REFR)
  vmeBpmApp/srcDisplay/bpm_*_diagnostic.edl: add text based on :CHK PV; add expert related display;
  remove :FBCK PV control
  See Changelog for more detail  
Release: Bpm-R5-3-0
  Fixed a few minor bugs in vectorized stripline code and enabled
  this vectorized code by default.
Release: Bpm-R5-2-0
  Merged changes in the <release>_lowI <release>_calGlitchTrap variants.
  Added a few diagnostic/statistics counters.
  Use udpComm-R1-3-0 (which contains some more statistics, too).
Release: Bpm-R5-1-0
  Bugfix: Make sure cavity BPM data packet is vector-aligned. 
    (Do not use Bpm-R5-0-0 or Bpm-R5-0-1 with cavity BPM IOCs.)
    Changes to vmeDigi/vmeDigiComm.c, vmeBpmApp/src/drvBpmCavity.c 
  padBpmApp/Db/bpmPadAfe.template
    Change cal attenuation CALA.DRVL from 0 to 6 dB
  configure/RELEASE: use fcom-R1-3-0, udpComm-R1-2-0, fcomUtil-R0-4-1
  vmeBpmApp/srcDisplay/bpm_stripline_diagnostic: remove UOFF/VOFF
Release: Bpm-R5-0-1
  Additional X/Y offsets per undulator request
    Changes to vmeBpmApp/Db/bpm.db.part, vmeBpmApp/srcRestore/BPM_*_VME.cwConfig
  iocBoot/st.vmegeneric.cmd, st.padgeneric.cmd
    Remove incorrect optional argument for ld(lanIpBasic*.obj)
Release: Bpm-R5-0-0
  Use rtems-4.9.4 and base-R3-14-8-2-lcls6
  Many changes associated with fast feedback project to vmeBpmApp, padBpmApp, iocBoot directories. 
  For detail, see Changelog. 
    configure/RELEASE: add udpComm, fcom, fcomUtil
    Add multicast support (use if PADMCGRP envvar defined)
    Stripline VME-PAD communication changed from broadcast/UDP to multicast/UDP (for fnet systems only)
    Add support for IOCs to stream X,Y,TMIT (+ timestamp/status) data on new fast network
    Use new padSupport and udpComm modules (moved from ssrlApps to new EPICS module udpComm)   
    Bitmask for channels using FCOM - can remove undesired BPMs    
    First phase of fast feedback using all IN20, LI21, BSY0, LTU0, LTU1, UND1, DMP1 iocs/eiocs 
  padBpmApp/srcdrvPadAfeQspi.c: reject 0xff AFE version number (value if no AFE present)
  vmeBpmApp/src/drvBpm.c, vmeBpmApp/src/drvBpm.h,vmeBpmApp/src/drvBpmStripl.c, vmeBpmApp/src/drvBpmCavity.c:
     Add TimeStamp* argument to drvBpmCalcPos() so that the timestamp from the PAD reply can be passed on and 
     used to tag the X/Y/TMIT records. (TSE on these records needs to be set to make use of the feature.)
  padBpmApp/src/devWfPadBpm.c: rtems 4.9.1, 4.10 API changes in uC5282 BSP
  Use new ao records to hold MPS thresholds (instead of X,Y,TMIT .LOLO,.HIHI)
    vmeBpmApp/Db/bpm.db/part: new records with ASG set; changed X,Y,TMIT alarm severities = NO_ALARM
    vmeBpmApp/src/: new device support to access new MPS thresholds   
    vmeBpmApp/srcRestore/ stripline and cavity templates: add threshold PVs   
  New device suppport to deposit waveform into digitizer memory
    Changes in vmeDigi
  Move vmeBpmApp/Db/bpmQMaster.db to BpmSoft app to reduce number of CA conn for ioc-in20-bp01;
    remove PVs from vmeBpmApp/srcRestore/IOC-IN20-BP01.cwConfig
  vmeBpmApp/Db/bpm.db/part: new calcout records to allow additional X/Y offsets
    Add .B and .C fields to vmeBpmApp/srcRestore/ strip and cavity templates
  vmeBpmApp/Db remove unused mpsLimits.db
  vmeBpmApp/bpm.db.part: Set X_SLOW.SCAN (for cavity bpms, was not being processed)
  vmeBpmApp/db/bpmMps.acf: remove iocegr/lcls-builder from allowed user/host: eiocdesk only

Release: Bpm-R4-1-1
  Use event-R3-0-5

Release: Bpm-R4-1-0
  Change MPS algorithm to include BYKICK status, add new PVs MPSFLT_T, MPSLTBYP
    Changes to vmeBpmApp/src/drvBpm.c, vmeBpmApp/Db/bpm.db.part
  vmeBpmApp/Db/IOC-LTU1-BP01bpm.substitutions
    Add MPS record substitutions
  iocBoot/ioc-ltu1-bp01/st.cmd
    For MPS signal, add IP470A, XY9660 init; define mpsPetCallback
  Fix flagging of X, Y, TMIT when TMIT over/underrange (was being reset)
    Changes to vmeBpmApp/src/drvBpm.c
  vmeBpmApp/Db/bpm.db.part: 
    Replace ASG=BPM_MPS with ASG=$(asg) for X, Y, TMIT and MPS PVs. Populate 
      asg macros in ioc*bpm.subsitutions files: most will be set to ""; 
      units in MPS will be set to BPM_MPS
    Set MPSBYP default db value to 0 (bypassed)
  Use event-R3-0-4
    vmeBpmApp/src/fidProcess.c
      Add additional argument (0) to fidProcess.c call to evrTimeRegister  
  iocBoot/st.vmegeneric.cmd
    Add rtems-4.9.1 VME bus timing change to fix EVR errors 

Release: Bpm-R4-0-0
  Add remaining Linac BPMs to
    vmeBpmApp/Db and /srcRestore, iocBoot
  vmeBpmApp/Db/bpmCavity.db.part,bpmStripl.db.part
    Change STA_ALH from sel to calc; sel doesn't work for alh acknowledge
  padBpmApp/Db/bpmPadAfe.template
    To support new PAD design with 6dB less atten, 
    optionally add 6dB to total att based on fw version
  vmeBpmApp/srcRestore
    Add :Z to BPMS_STRIP_VME.cwConfig and BPMS_CAVITY_VME.cwConfig 
    BPMS_STRIP_VME.cwConfig, replace X.AOFF, Y.AOFF with XAOFF, YAOFF
  Remove all SLC-aware code
    Changes to vmeBpmApp/Db and iocBoot directories
  Add evrInitialize() to all stripline st.cmd files
  New file vmeBpmApp/Db/bpmIocMps.db
    Move MPS PVs from bpmIocStripl to bpmIocMps
    Update IOC-UND1-BP0*.sub accordingly
    Add MPS substitutions to IOC-LTU1-BP01.substitutions
  vmeBpmApp/Db/bpmPattern.substitutions
    Comment out LNK5 to FIDP record; FIDP record is usually
    commented out in bpmIocStripl.db, so both will need to be 
    uncommented to use event simulation mode
  vmeBpmApp/srcRestore/
    Add X,Y,TMIT .HHSV, .LLSV to strip and cavity cw configs
      This is to avoid unwanted alarms due to MPS-related limits
  vmeBpmApp/Db/bpm.db.part
    Set RSTA.SCAN to 1 second; remove RSTA from diagnostic processing chain
  vmeBpmApp/Db/bpmCavity.db.part
    Remove URMS,VRMS,RRMS fields that were intended to be used by MPS;
      since then we have decided to use X,Y,TMIT for MPS

Release: Bpm-R3-0-0
  Use RTEMS 4.9.1
    configure/RELEASE
      Change base and module versions
    padBpmApp/src/devWfPadBpm.c
      Redefine RTEMS symbols Timer_initialize,Read_timer
    iocBoot/ioc-und1-bp0*/st.cmd
      Change ipac carrier declaration

Release: Bpm-R2-0-5
  vmeBpmApp/Db/bpm.db.part
    Set Z.PINI=YES, change Z ai to ao 
  vmeBpmApp/srcRestore/BPMS_CAVITY_VME.cwConfig
     Remove unused RCVR_SELFTST, RCVR_LO_CTL

Release: Bpm-R2-0-4
  Changes to vmeBpmApp/src/drvBpmStripl.c, drvBpm.h, drvBpmCavity.c
    To fix bug reported by elog id 284523
    Changes to setting overrange bits, setting status/severity,
      flagging underflow/overflow
  Move most MPS support from databases to low-level code
    Many changes in:
      vmeBpmApp/Db and vmeBpmApp/src
      (Details in ChangeLog)
    New file:
      vmeBpmApp/src/mpsSupport.c
    Add CAS to X, Y PVs
  Use event-R2-8-2 
  Use iocAdmin-R2-1-4

Release: Bpm-R2-0-3
  Changes to:
    vmeBpmApp/src/vp/vp.c
    vmeBpmApp/src/drvBpm.c,drvBpmCavity.c
    for:
    -Fixed problem where AI record status/severity
    (X/Y/TMIT) update was delayed by 1 pulse.
    -Other code cleanup
   vmeBpmApp/Db/mps_stat.db
     Set :MPSSIG to 1 on startup (1=FAULT)

Release: Bpm-R2-0-2
  vmeBpmApp/srcRestore/BPMS_STRIP_VME.cwConfig
    Change U/VOFF to U/VOFF_SEL.A
    Change PCMM to U/VSCL_SEL.A
  vmeBpmApp/Db/bpm.db.part
    Remove PCMM
  Use iocAdmin-R2-1-2 to pick up new feedback PV
    Add FBCK, FBCK.RVAL to vmeBpmApp/srcRestore BPMS*.cwConfig
    Add FBCK to vmeBpmApp/srcDisplay/bpm*diagnostic.edl
  vmeBpmApp/srcDisplay/
    bpm_cavity_diagnostics.edl - added a new tab giving access to X/Y ASLO/AOFF.    
    bpm_stripline_diagnostic.edl - remove PCMM, add USCL,VSCL
  
Release: Bpm-R2-0-1
  Use iocAdmin-R2-1-1
  Use event-R2-8-1
  vmeBpmApp/Db/ 
     Changes to mps_stat.db for non-heartbeat signal for MPS
     Add bpmMps.acf for MPS CAS
     Add CAS for all MPS-related records
     Add mpsLimits.db 
  vmeBpmApp/Db/bpmEvrCavity.substitutions
     Remove IRQ enable for event codes 20, 30, 40
  iocBoot/ioc-und1-bp01/st.cmd
    Add mpsLimits.db
  iocBoot/ioc-und1-bp01,-bp02,-bp03,-bp04/st.cmd
    Add bpmMps.acf
  vmeBpmApp/src/
    drvBpmCavity.c - scale u_re/v_re/u_im/v_im by 128/nsamples
    drvBpm.c, drvBpm.h - must always try to convert; even if callback returns error

Release: Bpm-R2-0-0
  Added support for cavity BPMs:
    New directories: fftw,vmeDigi
    Many new/changed vmeBpmApp/Db and /src files
    Support for signal to MPS: vmeBpmApp/Db/mps_stat.db
    New vmeBpmApp/srcDisplay and /srcRestore files
    Support for ioc-und1-bp01,-bp02,-bp03,-bp04
    Support for new BSA attributes: URER,UIMR,VRER,VIMR,REFR (not in use)
  Commented out simulation records in vmeBpmApp/Db/bpmIocStripl.db
  When no beam, :TMIT no longer invalid

Release: Bpm-cavity-branch-merged
  Merged Bpm-cavity-branch back onto main trunk.

Branch:  Bpm-R1-branch
  Branched-off before merging the major changes that
  add support for cavity BPMs (and modified a lot of
  stuff under the hood) back onto the main trunk.

  Further fixes of Bpm-R1-x-y should be done on the
  Bpm-R1-branch!

Release: Bpm-R1-branchpoint
  CVS HEAD just before creating Bpm-R1-branch and before
  merging the Bpm-cavity-branch back onto the HEAD.
  Further fixes of Bpm-R1-x-y should be done on the
  Bpm-R1-branch!

Release: Bpm-R1-0-10

  vmeBpmApp/Db/IOC-LTU1-BP03trig.substitutions and
               IOC-LTU1-BP04trig.substitutions
    Correct typos (change ltu to ltu1)

  iocBoot/ioc-ltu1-bp02,-bp03,-bp04/st.cmd
    Correct EVR declarations and names (Add second
    evr declaration to -bp02 and increment subsequent
    evr unit numbers)

Release: Bpm-R1-0-9

  vmeBpmApp/Db/IOC-BSY0-BP01bpm.substitutions, 
         ""/""/IOC-BSY0-BP01trig.substitutions,
         ""/""/IOC-BSY0-BP02bpm.substitutions, and
         ""/""/IOC-BSY0-BP02trig.substitutions -
    Change the following MAD names:
    BPM460029T to BPMBSY29
    BPM460039T to BPMBSY39
    BPM920020T to BPMBSY61
    BPM920030T to BPMBSY63
    BPM920050T to BPMBSY83        	
    BPM921010T to BPMBSY85        	
    BPM921020T to BPMBSY88        	
    BPM921030T to BPMBSY92 		
 
  vmeBpmApp/Db/bpmStripl.db - 
    Add $(bpm):Z PV for modelling/database software to 
    identify BPM.

  padBpmApp/src/subAtt.c -
    When calculating attenuator settings we must not assume that
    ATTO is a positive offset. Treat all values as signed quantities.
    (T.S. 20090923)

Release: Bpm-R1-0-8

  Change ioc-li25-bp01 save/restore location from:
  /data/restore/ to /data 
  to match other bpm iocs.

  Add new stripline ioc and bpm instances.
  New units:
    ioc-bsy0-bp02, eioc-bsy0-bp61, eioc-bsy0-bp63, eioc-bsy0-bp83,
   eioc-bsy0-bp85, eioc-bsy0-bp88, eioc-bsy0-bp92
   and
   all ltu0, ltu1, dmp1 iocs and eiocs.

  Add simulation mode and link for each BPM X,Y,TMIT on the VME IOC.
  The simulation link uses the pulse ID from the EVR.  Database changes
  only.  To simulate, set <ioc>:PATTERNSIM to "Raw" or "2".  To go back
  to data from the PADs, set <ioc>:PATTERNSIM to "Off" or "0" or reboot.

Release: Bpm-R1-0-7

  Add new ioc and bpm instances.
  New units:
    ioc-li21-bp02, 
    eioc-li21-bp401, eioc-li21-bp501
    eioc-li21-bp601, eioc-li21-bp701
    eioc-li21-bp801, eioc-li21-bp901

  No changes to software.

Release: Bpm-R1-0-6

  Change ioc-li24-bp02 and ioc-li25-bp01 EVRs from 
  PMC to VME. Only software change for this is to 
  ErConfigure() in st.cmd.

  Use new version of event module event-R2-6-3.

  bpm_stripline_diagnostic.edl
    Added related display to tabular bpm values
    display.

Branch:  Bpm-cavity-branch
  New branch where development of cavity BPM support
  is done.

Branch:  Bpm-cavity-branch-before
  Branchpoint on the main trunk where
  Bpm-cavity-branch was forked.

Release: Bpm-R1-0-5

  Generate alarm limits for IOC:IN20:BP01:QANN 
  automatically based on charge feedback setpoint
  FBCK:BCI0:1:CHRGSP.

Release: Bpm-R1-0-4

  Add new ioc and bpm instances.
  New units:
    ioc-li27-bp01, eioc-li27-bp301, eioc-li27-bp401
    ioc-li27-bp02, eioc-li27-bp701, eioc-li27-bp801
    ioc-li28-bp01, eioc-li27-bp301, eioc-li27-bp401
    ioc-li28-bp02, eioc-li27-bp701, eioc-li27-bp801
    ioc-bsy0-bp01, eioc-bsy0-bp29,  eioc-bsy0-bp39
                   eioc-bsy0-bp52

  Use new version of micro module micro-R1-1-13

Release: Bpm-R1-0-3

  Use new version of event module: event-R2-6-2

Release: Bpm-R1-0-2

  Two files changed to accomodate new AFE firmware:

  bpmPadAfe.template - add $(dev):ATT2H, $(dev):ATT2HFO,
		      	   $(dev):ATTH,  $(dev):ATTHFO
    Together, these records use the firmware version :VERS to determine 
    the values of :ATT2.HOPR, :ATT2.DRVH, :ATT.HOPR, :ATT.DRVH, and :ATTC.E
    and then write the values to those fields.

  padBpmApp/src/subAtt.c - use :ATTC.E input instead of hardcoded
  value for max. attenuation of ATT2.

Head:

  vmeBpmApp/Db/bpmIocStripl.db:
    - read MODIFIER5.R (EVR bit 'this timeslot carries beam' info)
      into record that is processed at 120Hz
  vmeBpmApp/src/fidProcess.c, vmeBpmApp/src/fidProcess.h
    - add routine 'fidHasBeam()' to access info w/o need to
      go through EPICS db every time the driver needs to know.
  vmeBpmApp/src/drvPadUdpComm.c:
    - support continuous triggering; beam data packets acquired
      when there is no beam (according to MODIFIER5.R) are ignored
      (cal-data is always used).


Release: Bpm-R1-0-1

  bpmStripl.db - add slow_x/y/tmit/r/b/g/y to use for slow update display
    change sevr of RSTA from INVALID to MAJOR

Release: Bpm-R1-0-0

  Till's latest and greatest finally tagged



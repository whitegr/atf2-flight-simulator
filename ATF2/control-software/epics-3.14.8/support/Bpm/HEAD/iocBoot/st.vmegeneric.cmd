# Generic piece of startup script
# for BPM IOCs

ld("watchdog.obj")
wdStart(20)

ld("rtems-gdb-stub.obj")

ld("vec.obj")
vec_install_extension()

ld("lanIpBasic_mve.obj")

# If IPADDR1/NETMASK1 not overridden then set it now
# to the old (pre-FNET) values
setenv("IPADDR1","192.168.1.1",0)
setenv("NETMASK1","255.255.255.0",0)
setenv("PADMCGRP","192.168.1.255:0000",0)

chdir("../../")

# 4.7.1 cexp cannot deal with weak undefined symbols
# need to load a helper module
ld("bin/RTEMS-beatnik/bpmWeakSymdefsWkarnd.obj")

ld("bin/RTEMS-beatnik/bpmApp.obj")

getenv("NEW_LANIP") && fcomUtilSetIPADDR1("-fnet")

lanIpSetup(getenv("IPADDR1"),getenv("NETMASK1"),0,0)
lanIpDebug=0
padProtoDebug=0

lsmod()

epicsEnvSet("EPICS_CA_MAX_ARRAY_BYTES","1000000")

#mRouteAdd(getenv("SLC_PROXY_IP"),getenv("SLC_PROXY_GATEWAY_IP"),0xfffffc00,0,0x3)

dbLoadDatabase("dbd/bpmApp.dbd")
bpmApp_registerRecordDeviceDriver(pdbbase)

drvPadUdpCommSetup(getenv("PADMCGRP"),drvBpmStriplCallbacks)
drvPadUdpCommTimeout=1000

(getenv("NEW_LANIP") && fcomInit(fcomUtilGethostbyname(getenv("FCOMMCGRP"),0),1000))

# hack around the EPICS memory tester
free(malloc(1024*1024*32))

# From Till Straumann (for RTEMS 4.9.1 upgrade):
# This should set the VME chip into a mode where it 
# surrenders the VME bus after every transaction.
# This means that the master has to rearbitrate for the bus for every cycle 
# (which slows things down).
#
# The faster setting I had enabled would let the master hold on to the bus 
# until some other master requests it.
*(long*)(vmeTsi148RegBase + 0x234) &= ~ 0x18

# Set value of 'FNET' PV
substr=malloc(500)
getenv("NEW_LANIP") && sprintf(substr,"FN=1") || sprintf(substr,"FN=0")


## Example vxWorks startup file

## The following is needed if your board support package doesn't at boot time
## automatically cd to the directory containing its startup script
#cd "/afs/slac.stanford.edu/u/cd/julia-33/sandbox/Bpm/iocBoot/iocvmeDigi"

< cdCommands
#< ../nfsCommands

cd topbin

## You may have to change vmeDigi to something else
## everywhere it appears in this file
ld 0,0, "vmeDigi.munch"

## Register all support components
cd top
dbLoadDatabase "dbd/vmeDigi.dbd"
vmeDigi_registerRecordDeviceDriver pdbbase

## Load record instances
dbLoadTemplate "db/user.substitutions"
dbLoadRecords "db/dbSubExample.db", "user=julia-33"

## Set this to see messages from mySub
#mySubDebug = 1

## Run this to trace the stages of iocInit
#traceIocInit

cd startup
iocInit

## Start any sequence programs
#seq &sncExample, "user=julia-33"

#addpath("/boot/g/lcls/users/strauman/epics/dev/app/bin/RTEMS-uC5282/:",1)

# Generic piece of Stripline BPM PAD initialization
wdStart(20)

# Debugger stub daemon
ld("rtems-gdb-stub.obj")

# Dedicated ethernet + network stack driver
ld("lanIpBasic_ln9.obj")

# Load IOC
chdir("../..")
ld("bin/RTEMS-uC5282/bpmPadApp.obj")

getenv("NEW_LANIP") && fcomUtilSetIPADDR1("-fnet")

lanIpSetup(getenv("IPADDR1"),getenv("NETMASK1"),0,0)
lanIpDebug    = 0
padProtoDebug = 0

lsmod()

# For iocAdmin
epicsEnvSet("ENGINEER","Sonya Hoobler")
epicsEnvSet("LOCATION",getenv("LOCN"))

epicsEnvSet("EPICS_CA_MAX_ARRAY_BYTES","1000000")

devWfPadBpmDebug=0
dbLoadDatabase("dbd/bpmPadApp.dbd")

bpmPadApp_registerRecordDeviceDriver(pdbbase)

substr=malloc(500)

sprintf(substr,"dev=BPMS:%s:%s,chrg=IOC:IN20:BP01:ATTR",getenv("LOCAs")+1,getenv("UNITs")+1)
dbLoadRecords("db/bpmPadRawWaveform.db",substr)
dbLoadRecords("db/bpmPadAfe.db",substr)

sprintf(substr,"IOC=EIOC:%s:BP%s",getenv("LOCAs")+1,getenv("UNITs")+1,0)
dbLoadRecords("db/iocAdminRTEMS.db",substr)
free(substr)

iocInit()

/* $Id: devWfVmeDigiSup.c,v 1.1 2010/08/30 17:22:01 strauman Exp $ */

#include <stdlib.h>
#include <string.h>

/* Begin EPICS Includes */
#include <epicsVersion.h>
#include <dbAccess.h>
/*
#include <devSup.h>
#include <recSup.h>
#include <dbEvent.h>
*/
#if (EPICS_REVISION == 14 && EPICS_MODIFICATION >= 11)
#include <errlog.h>
#endif

#include <waveformRecord.h>
/* End EPICS Includes */

#include <devVmeDigiSupport.h>

#include <devWfVmeDigiSup.h>

int
devWfVmeDigiSupArgcheck(struct waveformRecord *prec, const char *prefix, int needs_config)
{
int          idx;

	if ( VME_IO != prec->inp.type ) {
		epicsPrintf("%s.init_record(): expect VME_IO link\n",prefix);
		return -1;
	}

	idx = prec->inp.value.vmeio.card;

	if ( idx < 1 || idx > MAX_DIGIS ) {
		epicsPrintf("%s.init_record(): card # %i out of range\n", prefix, idx);
		return -1;
	}

	if ( needs_config && ! devVmeDigis[idx-1].digi ) {
		epicsPrintf("%s.init_record(): card # %i not configured\n", prefix, idx);
		return -1;
	}

	if ( DBR_SHORT != prec->ftvl ) {
		epicsPrintf("%s.init_record(): FTVL must be SHORT\n", prefix);
		return -1;
	}

	if ( prec->nelm % NCHAN != 0 || 0 == prec->nelm ) {
		epicsPrintf("%s.init_record(): NELM must be a (nonzero) multible of %i\n", prefix, NCHAN);
		return -1;
	}

	return idx;
}

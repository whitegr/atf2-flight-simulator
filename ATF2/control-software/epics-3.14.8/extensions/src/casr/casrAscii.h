/*************************************************************************\
* Copyright (c) 2002 The University of Chicago, as Operator of Argonne
* National Laboratory.
* Copyright (c) 2002 The Regents of the University of California, as
* Operator of Los Alamos National Laboratory.
* This file is distributed subject to a Software License Agreement found
* in the file LICENSE that is included with this distribution. 
\*************************************************************************/
/****************************************************************************
*
* Author:   John Winans
* Date:     95-11-20
*
*****************************************************************************/

#ifndef CASR_CASRASCII_H
#define CASR_CASRASCII_H

int BurtAsciiWriteFileList(ELLLIST *pFileList, ELLLIST *pGlobalContext, char *OutFileName);
int BurtAsciiReadFile(ItemNode *pBurtFile, ItemNode *pPath);
int BurtAsciiWriteFile(ItemNode *pBurtFile, void *pOutFile);
int BurtAsciiParseFile(ItemNode *pBurtFile, FILE *file, ItemNode *pPath);
void *BurtAsciiWriteOpen(char *fName);
int BurtAsciiWriteClose(void *pOutFile);

#endif


%{
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

#undef YY_INPUT
#define YY_INPUT(b,r,ms) (r=BurtYYInput(b,ms))

%}

string [0-9A-Za-z\~\!\@\#\$\%\^\&\*\_\-\+\|\\\[\]\:\;\'\<\,\>\(\)\.\?\/]+

%%

\{		{ return(OPEN_ATTRIBUTE); }
\}		{ return(CLOSE_ATTRIBUTE); }
\=		{ return(EQUALS); }

^#include[ \t]{string} { 
			char *p = &yytext[yyleng];
			while(!isspace(*--p));
			++p;
			BurtStartInclude(p);
		}
^#include[ \t]\"{string}\" {
			char *p = &yytext[yyleng];
			--p;
			*p = '\0';
			while(!isspace(*--p));
			p += 2;
			BurtStartInclude(p);
		}

\"[^"\n]*["\n]	{
			if (yytext[yyleng-1] != '\"')
				return(GARBAGE);
			if (yytext[yyleng-2] == '\\')
			{
				yyless(yyleng-1);		/* return the last double quote */
				yymore();				/* keep reading more characters */
			}
			else
			{
                yytext[yyleng-1] = '\0';
                yylval.string = strdup(&yytext[1]);
                return(STRING);
			}
		}
{string} {
			yylval.string = strdup(yytext);
			return(STRING);
		}
\/\/.*$	;
[ \t]	;
\n		{ pCurrentFile->LineNumber++; }
.		{ return(GARBAGE); }

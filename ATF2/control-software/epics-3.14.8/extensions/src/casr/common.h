/*************************************************************************\
* Copyright (c) 2002 The University of Chicago, as Operator of Argonne
* National Laboratory.
* Copyright (c) 2002 The Regents of the University of California, as
* Operator of Los Alamos National Laboratory.
* This file is distributed subject to a Software License Agreement found
* in the file LICENSE that is included with this distribution. 
\*************************************************************************/
/****************************************************************************
*
* Author:	John Winans
* Date:		95-11-20
*
*****************************************************************************/

#ifndef CASR_COMMON_H
#define CASR_COMMON_H

#include <stddef.h>
#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>

#include <string.h>
#include <limits.h>

#include "ellLib.h"

#define DEFAULT_CAWAIT 10.0

/*****************************************************************************
*
* Name-value pairs are stored in lists of AttributeNode.  
* Each atribute can have a name, a set of values and a set of attributes.
*
******************************************************************************/
typedef struct ItemNode
{
	ELLNODE			Node;
	struct ItemNode	*pParent;

	char			*Value;				/* original value */
	char			*MValue;			/* post-processed value */
	void			*pTranPvt;			/* private used by transport layer */
	ELLLIST			AttributeList;		/* list of ItemNode */
} ItemNode;

/*****************************************************************************
*
* This is filled in by the startup code when it parses the command line
* parameters.
*
* FileList holds a list of file names.
* GlobalItemList holds comments, keywords, includes, and defines
*
******************************************************************************/

typedef struct
{
	char			*logfile;
	char			*asciioutfile;
	char			*sddsoutfile;
	char			*sddsoutpath;
	float			cawait;
	int				debugFlag;
	int				generationMod;
	int				daemonMode;

	ELLLIST			FileList;		/* list of ItemNode */
	ELLLIST			GlobalItemList;	/* list of ItemNode */
	void			*TransportPrivate;
} BurtParmStruct;

void BurtCroak(int status, char *Message);
void *BurtMalloc(size_t size);
void BurtFree(void *p);
void *BurtRealloc(void *p, size_t size);

ItemNode *BurtItemCreate(void);
int BurtItemDelete(ItemNode *pItem);
long BurtCountAllPvs(ELLLIST *pFileList);
int BurtFileRead(ELLLIST *pFileList, ELLLIST *pGlobalContext, char *OptMacs);

int BurtPvInitialize(void **p, float cawait);
int BurtPvCleanup(void **p);
int BurtPvWasteTime(void *pTransportPrivate, float cawait);
int BurtPvConnect(void *pTransportPrivate, ELLLIST *pFileList, float cawait, int UseRon);
int BurtPvFileConnect(void *pTransportPrivate, ItemNode *pFileItem, int UseRon);
int BurtPvDisconnect(void *pTransportPrivate, ELLLIST *pFileList, float cawait);
int BurtPvFileDisconnect(void *pTransportPrivate, ItemNode *pFileItem);
int BurtPvGetType(void *pTransportPrivate, ELLLIST *pFileList, float cawait);
int BurtTrimPvItemValueInfo(ItemNode *pPv);
int BurtTrimEnvSection(ELLLIST *pFileList);
int BurtSetGlobalEnv(ELLLIST *pGlobalContext, ELLLIST *pFileList);
int BurtSetGlobalEnvTime(ELLLIST *pGlobalContext);

int BurtPvRead(void *pTransportPrivate, ELLLIST *pFileList, float cawait);
int BurtPvFileRead(void *pTransportPrivate, ItemNode *pFileItem);

int BurtPvWrite(void *pTransportPrivate, ELLLIST *pFileList, float cawait);
int BurtPvFileWrite(void *pTransportPrivate, ItemNode *pFileItem);

void BurtDumpItem(ItemNode *pItem, FILE *OutFile, int BreakLevel, int macroFlag);
ItemNode *BurtFindAttribute(char *Attribute, ItemNode *pSection);
ItemNode *BurtFindNext(char *Value, ItemNode *pCurrent);
ItemNode *BurtItemAddAttribute(ItemNode *pParentItem, const char *pStr);
char  *BurtGetInputSnapType(ItemNode *pFileItem);


#endif

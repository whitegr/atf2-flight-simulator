/*************************************************************************\
* Copyright (c) 2002 The University of Chicago, as Operator of Argonne
* National Laboratory.
* Copyright (c) 2002 The Regents of the University of California, as
* Operator of Los Alamos National Laboratory.
* This file is distributed subject to a Software License Agreement found
* in the file LICENSE that is included with this distribution. 
\*************************************************************************/
/****************************************************************************
*
* Author:   John Winans
* Date:     95-11-20
*
*****************************************************************************/

#ifndef CASR_CASRSDDS_H
#define CASR_CASRSDDS_H

/* SDDS Snapshot Parameter names */
#define KEYWORDSHEADERSTRING "BurtKeywords"
#define COMMENTSHEADERSTRING "BurtComments"
#define TYPEHEADERSTRING     "SnapType"

#define HEADER_UID				"LoginID"
#define	HEADER_JDATE			"JulianDate"
#define HEADER_TIMEOFDAY		"TimeOfDay"

/* SDDS column names */
#define NAME_COL		"ControlName"
#define MODE_COL		"ControlMode"
#define VAL_COL			"ValueString"
#define INDIRECT_COL	"IndirectName"
 
#define READONLYSTRING       "RO"
#define READONLYNOTIFYSTRING "RON"
 
#define ABSOLUTESTRING "Absolute"
#define RELATIVESTRING "Relative"
#define NOWRITESTRING  "Nowrite"

#define SDDS_FILE_NAME	"casr.snap"

int BurtSddsReadFile(ItemNode *pBurtFile);
int BurtSddsWriteFileList(ELLLIST *pFileList, ELLLIST *pGlobalContext, char *OutFileName, char *OutFilePath);

#endif

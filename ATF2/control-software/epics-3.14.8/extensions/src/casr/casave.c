/*************************************************************************\
* Copyright (c) 2002 The University of Chicago, as Operator of Argonne
* National Laboratory.
* Copyright (c) 2002 The Regents of the University of California, as
* Operator of Los Alamos National Laboratory.
* This file is distributed subject to a Software License Agreement found
* in the file LICENSE that is included with this distribution. 
\*************************************************************************/
/****************************************************************************
*
* Author:   John Winans
* Date:     95-11-20
*
*****************************************************************************/

#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <limits.h>
#include <signal.h>

#include "common.h"
#include "debug.h"
#include "casrAscii.h"
#include "log.h"
#ifdef CASR_INCLUDE_SDDS
#include "casrSdds.h"
#endif

#define STATIC static 

STATIC int SignalEventArrived;

static void usage(void);
static int procArgs(BurtParmStruct *parms, int argc, char **argv);
static char *getArgString(int *argc, char ***argv);
STATIC int BurtRead(BurtParmStruct *parms);

/*****************************************************************************
*
*
*****************************************************************************/
main(int argc, char **argv)
{
	BurtParmStruct	Parms;
	int				status;

	++argv;
	--argc;

	/* Build internal data structures from command-line args */
	if (procArgs(&Parms, argc, argv) != 0)
	{
		usage();
		return(-1);
	}
	LogOpen(Parms.logfile);
	status = BurtRead(&Parms);
	LogClose();

	return(status);
}

/*****************************************************************************
*
*****************************************************************************/
void SignalEventHandler(int i)
{
	signal(SIGUSR1, SignalEventHandler);
	SignalEventArrived = 1;
}
/*****************************************************************************
*
* Main routine for casave and carestore
*
*	Process the command line args
*	Read the PV-related information from the input files
*	Connect to the named PVs
*	Figure out the data types of all the PVs
*	Read the value of all the PVs
*	Write the data to the oputput file(s)
*	Disconnect from the PVs
*
*****************************************************************************/
STATIC BurtRead(BurtParmStruct *pParms)
{
	char		*pAsciiOutFileName;
	char		*pSddsOutFileName;
	int			GenerationCounter = 0;
	ItemNode	*pItem;

	/* Build the in-core list of PV names */
#ifdef CARESTORE
	if (BurtFileRead(&(pParms->FileList), &(pParms->GlobalItemList), "writemacros") != 0)
		return(-1);
#else
	if (BurtFileRead(&(pParms->FileList), &(pParms->GlobalItemList), "readmacros") != 0)
		return(-1);
#endif

	BurtSetGlobalEnv(&(pParms->GlobalItemList), &(pParms->FileList));

	if (pParms->debugFlag)
	{
		LogMsg(0, "After reading the request files...\n");
		BurtDebugPrintParms(pParms);
	}

	/* Initialize the PV connection system (CA, CDEV, ...) */
	if (BurtPvInitialize(&(pParms->TransportPrivate), pParms->cawait) != 0)
		return(-6);

#ifdef CARESTORE
	/* Bind to PVs */
	if (BurtPvConnect(pParms->TransportPrivate, &(pParms->FileList), pParms->cawait, 1) != 0)
		return(-2);

	if (BurtPvWrite(pParms->TransportPrivate, &(pParms->FileList), pParms->cawait) != 0)
		return(-3);

	/* If necessary, we could create a RON snapshot file here... */

#else
	/* Bind to PVs */
	if (BurtPvConnect(pParms->TransportPrivate, &(pParms->FileList), pParms->cawait, 0) != 0)
		return(-2);

	if (pParms->daemonMode)
	{	
		char	*p;

		/* Set up a signal handler to let us grab snapshots on demand */
		signal(SIGUSR1, SignalEventHandler);

		/* Set up a filename buffer to hold the generation names */
		if (pParms->asciioutfile != NULL)
			pAsciiOutFileName = (char *)malloc(strlen(pParms->asciioutfile)+10);
		else
			pAsciiOutFileName = NULL;

		if (pParms->sddsoutfile != NULL)
			pSddsOutFileName = (char *)malloc(strlen(pParms->sddsoutfile)+10);
		else
			pSddsOutFileName = NULL;
	}
	else
	{
		pAsciiOutFileName = pParms->asciioutfile;
		pSddsOutFileName = pParms->sddsoutfile;
	}

	do
	{
		if (pParms->daemonMode)
		{	/* Wait for a kill signal to let us know to take a snapshot */
			SignalEventArrived = 0;
			if (pSddsOutFileName != NULL)
				sprintf(pSddsOutFileName, "%s%02d", pParms->sddsoutfile, GenerationCounter);
			if (pAsciiOutFileName != NULL)
				sprintf(pAsciiOutFileName, "%s%02d", pParms->asciioutfile, GenerationCounter);
			++GenerationCounter;
			GenerationCounter %= pParms->generationMod;

			while(SignalEventArrived == 0)
				BurtPvWasteTime(pParms->TransportPrivate, 0.5);
		}

		/* Take a snapshot */
		if (BurtPvRead(pParms->TransportPrivate, &(pParms->FileList),pParms->cawait) != 0)
			return(-3);
	
		/* Write data to output file(s) */
		if (pAsciiOutFileName != NULL)
		{
			if (BurtAsciiWriteFileList(&(pParms->FileList), &(pParms->GlobalItemList), pAsciiOutFileName) != 0)
				return(-4);
		}
#ifdef CASR_INCLUDE_SDDS
		if (pSddsOutFileName != NULL)
		{
			if (BurtSddsWriteFileList(&(pParms->FileList), &(pParms->GlobalItemList), pSddsOutFileName, pParms->sddsoutpath) != 0)
				return(-4);
		}
#endif
	}
	while (pParms->daemonMode);

#endif
	
	/* Disconnect from the PVs */
	if (BurtPvDisconnect(pParms->TransportPrivate, &(pParms->FileList),pParms->cawait) != 0)
		return(-5);

	/* Initialize the PV connection system (CA, CDEV, ...) */
	if (BurtPvCleanup(&(pParms->TransportPrivate)) != 0)
		return(-7);

	return(0);
}

/*****************************************************************************
*
* Display a usage message.
*
*****************************************************************************/
static void usage(void)
{
	fprintf(stderr, "Usage:\n");
	fprintf(stderr, "[-asciiin <file1> [<file2> [...]]]   specify ASCII input files\n");
#ifndef CARESTORE
	fprintf(stderr, "[-asciiout <outputfile>]             specify ASCII output file\n");
#endif
	fprintf(stderr, "[-sddsin <file>]                     declare SDDS input file\n");
#ifndef CARESTORE
	fprintf(stderr, "[-sddsout <outputfile>]              declare SDDS output file\n");
	fprintf(stderr, "[-sddsoutpath <dirname>]             declare SDDS output path for vector files\n");
#endif
	fprintf(stderr, "[-log <logfile>]                     specify the log filename\n");
#ifndef CARESTORE
	fprintf(stderr, "[-comments <comments>]               specify file comments\n");
	fprintf(stderr, "[-keywords <keywords>]               specify file keywords\n");
#endif
	fprintf(stderr, "[-cawait <ca idle wait time>]        set min CA idle wait time\n");
#ifndef CARESTORE
	fprintf(stderr, "[-daemon <generation count>]         operate in daemon mode\n");
#endif
	fprintf(stderr, "[-I<path>]                           specify an include directory (ascii files only)\n");
	fprintf(stderr, "[-D<name=value>]                     specify a global macro\n (ascii files only)");
	fprintf(stderr, "[-d]                                 enable debugging output\n");
}


/*****************************************************************************
*
* Read and process all the command line args.
* Generally this simply means that we read them and set the corresponding 
* flags and stuff in the BurtParmStruct.
*
* The following are supported, but have been demoted:
*	[-f <file1> [[<file2>]...]]				specify ASCII input files
*	[-o <outputfile>] 						specify ASCII output file
*	[-l <logfile>]							specify the log filename
*	[-c <comments>]							specify file comments
*	[-k <keywords>]							specify file keywords
*	[-r <retry time>]						set max CA idle wait time
*
*****************************************************************************/
static int procArgs(BurtParmStruct *parms, int argc, char **argv)
{
	ItemNode	*pItem;
	char		*pChar;

	parms->logfile = NULL;
	parms->asciioutfile = NULL;
	parms->sddsoutfile = NULL;
	parms->sddsoutpath = NULL;
	parms->cawait = DEFAULT_CAWAIT;
	parms->debugFlag = 0;
	parms->TransportPrivate = NULL;
	parms->daemonMode = 0;
	parms->generationMod = 0;

	ellInit(&parms->FileList);
	ellInit(&parms->GlobalItemList);

	while(argc > 0)
	{
		if ((strcmp(*argv, "-log") == 0)||(strcmp(*argv, "-l") == 0))
		{ /* Set logfile name */
			--argc; ++argv;
			parms->logfile = *argv;
			--argc; ++argv;
		}
		else if ((strcmp(*argv, "-asciiout") == 0)||(strcmp(*argv, "-o") == 0))
		{ /* Set output file name */
			--argc; ++argv;
			parms->asciioutfile = *argv;
			--argc; ++argv;
		}
		else if (strcmp(*argv, "-sddsoutpath") == 0)
		{ /* Set output file name */
			--argc; ++argv;
			parms->sddsoutpath = *argv;
			--argc; ++argv;
		}
		else if (strcmp(*argv, "-sddsout") == 0)
		{ /* Set output file name */
			--argc; ++argv;
			parms->sddsoutfile = *argv;
			--argc; ++argv;
		}
		else if ((strcmp(*argv, "-comments") == 0)||(strcmp(*argv, "-c") == 0))
		{ /* Set comments from all next non-switch args */
			ItemNode *pItemAttrib;

			--argc; ++argv;
			pItem = BurtItemCreate();
			pItem->Value = strdup("comments");
			ellAdd(&parms->GlobalItemList, &pItem->Node);
			
			pItemAttrib = BurtItemCreate();
			pItemAttrib->Value = getArgString(&argc, &argv);
			ellAdd(&pItem->AttributeList, &pItemAttrib->Node);
		}
		else if ((strcmp(*argv, "-keywords") == 0)||(strcmp(*argv, "-k") == 0))
		{ /* Set keywords from all next non-switch args */
			ItemNode *pItemAttrib;

			--argc; ++argv;
			pItem = BurtItemCreate();
			pItem->Value = strdup("keywords");
			ellAdd(&parms->GlobalItemList, &pItem->Node);

			pItemAttrib = BurtItemCreate();
			pItemAttrib->Value = getArgString(&argc, &argv);
			ellAdd(&pItem->AttributeList, &pItemAttrib->Node);
		}
		else if ((strcmp(*argv, "-cawait") == 0)||(strcmp(*argv, "-r") == 0))
		{
			char	*p;

			--argc; ++argv;
			parms->cawait = strtod(*argv, &p);
			if (*p != '\0')
				return(-1);
			--argc; ++argv;
		}
		else if (strcmp(*argv, "-daemon") == 0)
		{
			char	*p;
			--argc; ++argv;
			parms->daemonMode = 1;
			parms->generationMod = strtol(*argv, &p, 10);
			if (*p != '\0')
				return(-1);
			--argc; ++argv;
		}
		else if (strcmp(*argv, "-d") == 0)
		{
			--argc; ++argv;
			parms->debugFlag = 1;
		}
		else if (strcmp(*argv, "-v") == 0)
		{
			--argc; ++argv;
		}
		else if ((strcmp(*argv, "-asciiin") == 0)||(strcmp(*argv, "-f") == 0))
		{
			--argc; ++argv;
			while((argc > 0) && (**argv != '-'))
			{
				if (strcmp(*argv, "-") == 0)
					pChar = "-";
				else if (**argv == '-')
					break;
				else
					pChar = *argv;

				pItem = BurtItemCreate();
				pItem->MValue = "ascii";
				pItem->Value = strdup(pChar);
				ellAdd(&(parms->FileList), &pItem->Node);
#ifdef CASR_DEBUG
				LogMsg(0, "asciiin '%s'\n", pItem->Value);
#endif
				--argc; ++argv;
			}
		}
		else if (strcmp(*argv, "-sddsin") == 0)
		{
			--argc; ++argv;
			while(argc > 0)
			{
				if (strcmp(*argv, "-") == 0)
					pChar = "-";
				else if (**argv == '-')
					break;
				else
					pChar = strdup(*argv);

				pItem = BurtItemCreate();
				pItem->MValue = "sdds";
				pItem->Value = pChar;
				ellAdd(&(parms->FileList), &pItem->Node);
				--argc; ++argv;
			}
		}
		else if (strncmp(*argv, "-I", 2) == 0)
		{
			ItemNode	*pPath;
			ItemNode	*pPathItem;

			pPath = (ItemNode*)ellFirst(&parms->GlobalItemList);
			if ((pPath!=NULL)&&(strcmp(pPath->Value, "path") != 0))
				pPath = BurtFindNext("path", pPath);
			if (pPath==NULL)
			{
				pPath = BurtItemCreate();
				pPath->Value = strdup("path");
				ellAdd(&parms->GlobalItemList, &pPath->Node);
			}
			BurtItemAddAttribute(pPath, &((*argv)[2]));
			--argc; ++argv;
		}
		else if (strncmp(*argv, "-D", 2) == 0)
		{
			ItemNode	*pGlobMacs;
			ItemNode	*pMacItem;
			char		MacName[500];
			char		*MacValue;

			pGlobMacs = (ItemNode*)ellFirst(&parms->GlobalItemList);
			if ((pGlobMacs!=NULL)&&(strcmp(pGlobMacs->Value, "macros") != 0))
				BurtFindNext("macros", pGlobMacs);
			if (pGlobMacs==NULL)
			{
				pGlobMacs = BurtItemCreate();
				pGlobMacs->Value = strdup("macros");
				ellAdd(&parms->GlobalItemList, &pGlobMacs->Node);
			}
			strcpy(MacName, &((*argv)[2]));
			MacValue = MacName;
			while ((*MacValue!='\0')&&(*MacValue!='='))
				++MacValue;
			if (*MacValue=='\0')
			{
				LogMsg(0, "Bogus macro definition '%s'\n", *argv);
				return(-1);
			}
			*MacValue = '\0';
			++MacValue;
			pMacItem = BurtItemAddAttribute(pGlobMacs, MacName);
			BurtItemAddAttribute(pMacItem, MacValue);
			--argc; ++argv;
		}
		else
			return(-1);
	}
	return(0);
}

/*****************************************************************************
*
* A utility function for procArgs().
*
* Builds a string using the the next non-switch args.
* Returns the number of args placed into the new list.
*
******************************************************************************/
static char *getArgString(int *argc, char ***argv)
{
	int		Length;
	char	*Buf;
	int		FirstFlag;
	int		c;
	char	**v;

	/* Calculate buffer size */
	c = *argc;
	v = *argv;
	FirstFlag = 1;
	Length = 0;
	while((c > 0) && (**v != '-'))
	{
		if (!FirstFlag)
			++Length;
		else
			FirstFlag = 0;
		Length += strlen(*v);
		--c;
		++v;
	}

	Buf = (char*)BurtMalloc(Length+1);
	Buf[0] = '\0';
	FirstFlag = 1;

	while((*argc > 0) && (***argv != '-'))
	{
		if (!FirstFlag)
			strcat(Buf, " ");
		else
			FirstFlag = 0;

		strcat(Buf, **argv);
		--(*argc);
		++(*argv);
	}
	return(Buf);
}

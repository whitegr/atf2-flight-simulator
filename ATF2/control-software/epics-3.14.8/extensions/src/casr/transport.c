/*************************************************************************\
* Copyright (c) 2002 The University of Chicago, as Operator of Argonne
* National Laboratory.
* Copyright (c) 2002 The Regents of the University of California, as
* Operator of Los Alamos National Laboratory.
* This file is distributed subject to a Software License Agreement found
* in the file LICENSE that is included with this distribution. 
\*************************************************************************/
#define STATIC
#define STUPID_CA_SPEED_HACKS

/****************************************************************************
*
* Author:	John Winans
* Date:		95-11-20
*
*****************************************************************************/

#include <stddef.h>
#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <limits.h>
#include <math.h>

#include "alarm.h"
#include "alarmString.h"
#include "db_access.h"
#include "tsDefs.h"
#include "cadef.h"
#include "dbDefs.h"
#include "ellLib.h"

#include "log.h"
#include "common.h"
#include "transport.h"

#ifndef STATIC
#define STATIC static
#endif

/*
* This structure contains all the information for a single PV connection.
* There is one allocated for each PV.
*/
struct TranStruct;

typedef struct CaPvStruct
{
	ELLNODE				Node;
	struct TranStruct	*pTransport;
    chid				MyChid;			/* CA chid */
	ItemNode			*pPv;			/* PV item reference */
	ELLLIST				*pStateList;	/* What state list we are a member of */
} CaPvStruct;

/*
* This structure is used to hold the transport layer operating environment.
* There is one allocated for the entire burt process.
*/
typedef struct TranStruct
{
	unsigned long	ConnectCounter;
	ELLLIST			StateDisconnected;
	ELLLIST			StateConnectPend;
	ELLLIST			StateConnectIdle;
	ELLLIST			StateReadPend;
	ELLLIST			StateWritePend;
} TranStruct;

STATIC void BurtCaConnectionCallback(struct connection_handler_args cha);
STATIC CaPvStruct *BurtCaPvCreate(TranStruct *pTransport);
STATIC void BurtCaPvDelete(CaPvStruct *pCaPv);
STATIC void BurtCaReadEventCallback(struct event_handler_args eha);
STATIC int BurtCaPvSetState(CaPvStruct *pCaPv, ELLLIST *pStateList);
STATIC int BurtTransportServiceWaitState(ELLLIST *pStateList, float PollTime);

static void floatToString(float value,char *preturn);
static void doubleToString(double value,char *preturn);

/*****************************************************************************
*
* Called one single time when BURT starts up.
*
*****************************************************************************/
int BurtTransportServiceInit(void **ppTransportPrivate)
{
	TranStruct	*pTransport;
	int	status;

	status = ca_task_initialize();
	if (status != ECA_NORMAL)
	{
		LogMsg(0, "Channel access initialization failure: %s\n", ca_message(status));
		return(-1);
	}
	pTransport = (TranStruct*)BurtMalloc(sizeof(TranStruct));

	ellInit(&pTransport->StateDisconnected);
	ellInit(&pTransport->StateConnectPend);
	ellInit(&pTransport->StateConnectIdle);
	ellInit(&pTransport->StateReadPend);
	ellInit(&pTransport->StateWritePend);

	*ppTransportPrivate = pTransport;

	return(0);
}
/*****************************************************************************
*
* Called one single time when BURT is finished.
*
* For now, we ignore cleaning up since the task is gonna exit right away 
* anyway.
*
*****************************************************************************/
int BurtTransportServiceCleanup(void **ppTransportPrivate)
{
	/* Disconnect from any open PVs */

	/* Free all the PV connection structures */

	/* Free the TransportPrivate structure */

	/* Tell CA we are thru */
	ca_task_exit();

	return(0);
}
/*****************************************************************************
*
* Establish a connection to a single PV item.
*
*****************************************************************************/
int BurtTransportServicePvConnect(void *pTransportPrivate, ItemNode *pPvItem)
{
	TranStruct  *pTransport = (TranStruct*)pTransportPrivate;
	char		*PvName;
	CaPvStruct	*pCaPv;
	int			status;

#ifdef	STUPID_CA_SPEED_HACKS
	{ /* This insane fragment cuts half the time off connecting */
		static int ntimes = 0;
		if (ntimes++ == 50) {ntimes=0; ca_pend_event(.1);}
	}
#endif

	/* Use the macro-substituted name if available */
	if (pPvItem->MValue != NULL)
		PvName = pPvItem->MValue;
	else if (pPvItem->Value != NULL)
		PvName = pPvItem->Value;
	else
	{
		LogMsg(0, "WARNING: PV encountered with a NULL name\n");
		return(-1);		
	}

	pCaPv = BurtCaPvCreate(pTransport);
	pCaPv->pPv = pPvItem;
	pPvItem->pTranPvt = pCaPv;
	BurtCaPvSetState(pCaPv, &pTransport->StateConnectPend);
	status = ca_search_and_connect(PvName, &pCaPv->MyChid, BurtCaConnectionCallback, pCaPv);
	if (status != ECA_NORMAL)
	{
		pPvItem->pTranPvt = NULL;
		BurtCaPvDelete(pCaPv);
		LogMsg(0, "Channel access PV search failure: %s\n", ca_message(status));
		return(-1);
	}

	return(0);
}

/*****************************************************************************
*
* Wait as long as we are seeing connection activity.
*
*****************************************************************************/
int BurtTransportServiceWaitConnect(void *pTransportPrivate, float interval )
{
	TranStruct  *pTransport = (TranStruct*)pTransportPrivate;
	int			status;
	int			count;

	status = BurtTransportServiceWaitState(&pTransport->StateConnectPend, interval);
	if ((status != ECA_TIMEOUT)&&(status != ECA_NORMAL))
	{
		LogMsg(0, "Channel access failure while waiting for connections: %s\n", ca_message(status));
		return(-1);
	}
	count = ellCount(&pTransport->StateConnectPend);
	if (count != 0)
	{
		CaPvStruct *pCaPv = (CaPvStruct*)ellFirst(&pTransport->StateConnectPend);
		LogMsg(0, "Warning, failed to connect to %d process variables\n", count);
		while(pCaPv != NULL)
		{
			LogMsg(0, "  %s\n", ca_name(pCaPv->MyChid));
			pCaPv = (CaPvStruct*)ellNext(&pCaPv->Node);
		}
	}

	return(0);
}

/*****************************************************************************
*
* Callback for the CA connection handler.
*
*****************************************************************************/
STATIC void BurtCaConnectionCallback(struct connection_handler_args cha)
{
	chid		chid=cha.chid;
    CaPvStruct	*pCaPv = (CaPvStruct*)ca_puser(chid);

	if (ca_state(pCaPv->MyChid) != cs_conn)
		BurtCaPvSetState(pCaPv, &pCaPv->pTransport->StateConnectPend);
	else
		BurtCaPvSetState(pCaPv, &pCaPv->pTransport->StateConnectIdle);

	return;
}

/*****************************************************************************
*
* This disconnection logic is not currently used.
*
*****************************************************************************/
int BurtTransportServicePvDisconnect(void *pTransportPrivate, ItemNode *pPvItem)
{
	return(0);
}
int BurtTransportServiceWaitDisconnect(void *pTransportPrivate, float interval)
{
	return(0);
}
STATIC void BurtCaDisconnectCallback(struct connection_handler_args cha)
{
	return;
}

/*****************************************************************************
*
* For read, use get_callback and use same fallback logic logic as the connect.
*
*****************************************************************************/
int BurtTransportServicePvRead(void *pTransportPrivate, ItemNode *pPvItem)
{
	TranStruct  *pTransport = (TranStruct*)pTransportPrivate;
	CaPvStruct	*pCaPv = (CaPvStruct*)pPvItem->pTranPvt;
	int			status;

	if (pCaPv == NULL)
	{
		LogMsg(0, "ERROR: Corrupt internal data structure found in BurtTransportServicePvRead()\n");
		return(-1);
	}

	if (ca_state(pCaPv->MyChid) != cs_conn)
		return(0);

	BurtCaPvSetState(pCaPv, &pTransport->StateReadPend);

	/* Logic manure due to the lack of organization in the CA interface */
	switch(ca_field_type(pCaPv->MyChid))
	{
	case DBF_DOUBLE:
		status = ca_array_get_callback(DBR_STS_DOUBLE, 
					ca_element_count(pCaPv->MyChid), 
					pCaPv->MyChid, 
					BurtCaReadEventCallback, 
					pCaPv);
		break;

	case DBF_FLOAT:
		status = ca_array_get_callback(DBR_STS_FLOAT, 
					ca_element_count(pCaPv->MyChid), 
					pCaPv->MyChid, BurtCaReadEventCallback, pCaPv);
		break;

	case DBF_INT:
	/* bogus decision in db_access.h...  DBF_SHORT == DBF_INT */
	case DBF_CHAR:
	case DBF_LONG:
		status = ca_array_get_callback(DBR_STS_LONG, 
					ca_element_count(pCaPv->MyChid),
					pCaPv->MyChid, BurtCaReadEventCallback, pCaPv);
		break;

	case DBF_STRING:
	case DBF_ENUM:
	default:
		status = ca_array_get_callback(DBR_STS_STRING, 
					ca_element_count(pCaPv->MyChid),
					pCaPv->MyChid, BurtCaReadEventCallback, pCaPv);
		break;
	}

	if (status != ECA_NORMAL)
	{
		BurtCaPvSetState(pCaPv, &pTransport->StateConnectIdle);
		LogMsg(0, "Channel access PV read failure: %s\n", ca_message(status));
		return(-1);
	}
	return(0);
}

/*****************************************************************************
*
*****************************************************************************/
int BurtTransportServiceWaitRead(void *pTransportPrivate, float interval)
{
	TranStruct  *pTransport = (TranStruct*)pTransportPrivate;
	int			status;
	int			count;

	status = BurtTransportServiceWaitState(&pTransport->StateReadPend, interval);
	if ((status != ECA_TIMEOUT)&&(status != ECA_NORMAL))
	{
		LogMsg(0, "Channel access failure while waiting for read completion %s\n", ca_message(status));
		return(-1);
	}
	count = ellCount(&pTransport->StateReadPend);
	if (count != 0)
		LogMsg(0, "Warning, failed to read %d process variables\n", count);

	return(0);
}

/*****************************************************************************
*
*****************************************************************************/
STATIC void BurtCaReadEventCallback(struct event_handler_args eha)
{
	ItemNode	*pParentItem;
    CaPvStruct	*pPvStruct = (CaPvStruct*)eha.usr;
	char		str[100];
	long		AlmSeverity;
	long		AlmStatus;
	double		*pDouble;
	float		*pFloat;
	long		*pLong;
	char		*pChar;
	long		Dim;

	BurtCaPvSetState(pPvStruct, &pPvStruct->pTransport->StateConnectIdle);
	if (eha.status != ECA_NORMAL)
	{
		LogMsg(0, "Channel access PV read failure on PV %s\n", ca_name(eha.chid));
		return;
	}

	Dim = eha.count;
	pParentItem = pPvStruct->pPv;
	pParentItem = BurtItemAddAttribute(pParentItem, "value");
	if (Dim > 1)
	{
		sprintf(str, "%d", eha.count);
		pParentItem = BurtItemAddAttribute(pParentItem, "dim");
		BurtItemAddAttribute(pParentItem, str);
		pParentItem = pParentItem->pParent;
	}
	pParentItem = BurtItemAddAttribute(pParentItem, "data");
	/* Add the value info to the PV item */

	switch (eha.type)
	{
	case DBR_STS_DOUBLE:
		pDouble = (double*)dbr_value_ptr(eha.dbr, DBR_STS_DOUBLE);
		while (Dim--)
		{
			doubleToString(*pDouble, str);
			BurtItemAddAttribute(pParentItem, str);
			++pDouble;
		}
		AlmStatus = ((struct dbr_sts_double*)(eha.dbr))->status;
		AlmSeverity = ((struct dbr_sts_double*)(eha.dbr))->severity;
		break;

	case DBR_STS_FLOAT:
		pFloat = (float*)dbr_value_ptr(eha.dbr, DBR_STS_FLOAT);
		while (Dim--)
		{
			floatToString(*pFloat, str);
			BurtItemAddAttribute(pParentItem, str);
			++pFloat;
		}
		AlmStatus = ((struct dbr_sts_float*)(eha.dbr))->status;
		AlmSeverity = ((struct dbr_sts_float*)(eha.dbr))->severity;
		break;

	case DBR_STS_LONG:
		pLong = (long*)dbr_value_ptr(eha.dbr, DBR_STS_LONG);
		while(Dim--)
		{
			sprintf(str, "%ld", *pLong);
			BurtItemAddAttribute(pParentItem, str);
			++pLong;
		}
		AlmStatus = ((struct dbr_sts_long*)(eha.dbr))->status;
		AlmSeverity = ((struct dbr_sts_long*)(eha.dbr))->severity;
		break;

	case DBR_STS_STRING:
		pChar = (char *)dbr_value_ptr(eha.dbr, DBR_STS_STRING);
		while(Dim--)
		{
			BurtItemAddAttribute(pParentItem, pChar);
			pChar += MAX_STRING_SIZE;
		}
		AlmStatus = ((struct dbr_sts_string*)(eha.dbr))->status;
		AlmSeverity = ((struct dbr_sts_string*)(eha.dbr))->severity;
		break;
	
	default:
		LogMsg(0, "BurtCaReadEventCallback(%s) received bogus message from channel access.\n", ca_name(eha.chid));
	}

	pParentItem = pParentItem->pParent;
	pParentItem = pParentItem->pParent;

	pParentItem = BurtItemAddAttribute(pParentItem, "sevr");
	BurtItemAddAttribute(pParentItem, alarmSeverityString[AlmSeverity]);
	pParentItem = pParentItem->pParent;
	if (AlmSeverity != NO_ALARM)
	{
		pParentItem = BurtItemAddAttribute(pParentItem, "stat");
		BurtItemAddAttribute(pParentItem, alarmStatusString[AlmStatus]);
		pParentItem = pParentItem->pParent;
	}
	return;
}

/*****************************************************************************
*
*****************************************************************************/
int BurtTransportServicePvWrite(void *pTransportPrivate, ItemNode *pPvItem)
{
	TranStruct  *pTransport = (TranStruct*)pTransportPrivate;
	CaPvStruct	*pCaPv = (CaPvStruct*)pPvItem->pTranPvt;
	int			status;
	ItemNode	*pParentItem;
	ItemNode	*pItem;
	long		Dim;
	double		*pDouble;
	float		*pFloat;
	long		*pLong;
	char		*pChar;
	void		*pVoid;
	int			i;

	if (pCaPv == NULL)
	{
		LogMsg(0, "ERROR: Corrupt internal data structure found in BurtTransportServicePvWrite()\n");
		return(-1);
	}

	if (ca_state(pCaPv->MyChid) != cs_conn)
		return(0);

	pItem = BurtFindAttribute("readonly", pPvItem);
	if (pItem != NULL)
	{
		if (pPvItem->MValue != NULL)
			pChar = pPvItem->MValue;
		else
			pChar = pPvItem->Value;

		LogMsg(0, "INTERNAL CASR ERROR! Got a RO and tried to write it for PV '%s'!!\n", pChar);
		return(0);
	}

	pParentItem = BurtFindAttribute("value", pPvItem);
	if (pParentItem == NULL)
		return(0);			/* No value to write for this PV */


	Dim = 1;
	pItem = BurtFindAttribute("dim", pParentItem);
	pItem = BurtFindAttribute(NULL, pItem);
	if ((pItem != NULL) && (pItem->Value != NULL))
		sscanf(pItem->Value, "%d", &Dim);

	pItem = BurtFindAttribute("data", pParentItem);
	pItem = BurtFindAttribute(NULL, pItem);

	if (pItem == NULL)
		return(0);

	status = ECA_NORMAL;
	switch(ca_field_type(pCaPv->MyChid))
	{
	case DBF_DOUBLE:
		pDouble = (double *)BurtMalloc(dbr_size_n(DBR_DOUBLE, Dim));
		i = 0;
		while((i < Dim) && (pItem != NULL) && (pItem->Value != NULL))
		{
			pDouble[i] = 0.0;
			sscanf(pItem->Value, "%lg", &(pDouble[i]));
			pItem = BurtFindNext(NULL, pItem);
			++i;
		}
		if (i > 0)
		{
			status = ca_array_put(DBF_DOUBLE, i, pCaPv->MyChid, pDouble);
#if 0
			fprintf(stderr, "BurtTransportServicePvWrite() wrote %d doubles to %s\n", i, ca_name(pCaPv->MyChid));
			fprintf(stderr, "%lg, ...\n", pDouble[0]);
#endif
		}

		BurtFree(pDouble);
		break;
	case DBF_FLOAT:
		pFloat = (float*)BurtMalloc(dbr_size_n(DBR_FLOAT, Dim));
		i = 0;
		while((i < Dim) && (pItem != NULL) && (pItem->Value != NULL))
		{
			pFloat[i] = 0.0;
			sscanf(pItem->Value, "%g", &(pFloat[i]));
			pItem = BurtFindNext(NULL, pItem);
			++i;
		}
		if (i > 0)
			status = ca_array_put(DBF_FLOAT, i, pCaPv->MyChid, pFloat);

		BurtFree(pFloat);
		break;

	case DBF_INT:
	/* moronic decision in db_access.h...  case DBF_SHORT: */
	case DBF_CHAR:
	case DBF_LONG:
		pLong = (long*)BurtMalloc(dbr_size_n(DBR_LONG, Dim));
		i = 0;
		while((i < Dim) && (pItem != NULL) && (pItem->Value != NULL))
		{
			pLong[i] = strtol(pItem->Value, (char **)NULL, 10);
			pItem = BurtFindNext(NULL, pItem);
			++i;
		}
		if (i > 0)
			status = ca_array_put(DBF_LONG, i, pCaPv->MyChid, pLong);

		BurtFree(pLong);
		break;

    case DBF_STRING:
	case DBF_ENUM:
	default:
#ifdef XXX
		fprintf(stderr, "writing a string to '%s' (%d)\n", ca_name(pCaPv->MyChid), dbr_size_n(DBR_STRING, 1));
#endif
		pVoid = BurtMalloc(dbr_size_n(DBR_STRING, Dim));
		pChar = (char*)pVoid;
		i = 0;
		while((i < Dim) && (pItem != NULL) && (pItem->Value != NULL))
		{
			strncpy(pChar, pItem->Value, dbr_size_n(DBR_STRING, 1));
#ifdef XXX
			fprintf(stderr, "'%s', ", pChar);
#endif
			pChar += dbr_size_n(DBR_STRING, 1);
			pItem = BurtFindNext(NULL, pItem);
			++i;
		}
#ifdef XXX
			fprintf(stderr, "\n");
#endif
		if (i > 0)
			status = ca_array_put(DBF_STRING, i, pCaPv->MyChid, pVoid);

		BurtFree(pVoid);
	}
	if (status != ECA_NORMAL)
	{
		BurtCaPvSetState(pCaPv, &pTransport->StateConnectIdle);
		LogMsg(0, "Channel access PV write failure: %s\n", ca_message(status));
		return(-1);
	}
	return(0);
}

/*****************************************************************************
*
* This is not used at this time.
* 
*****************************************************************************/
int BurtTransportServiceWaitWrite(void *pTransportPrivate, float interval)
{
	ca_flush_io();
	return(0);
}

/*****************************************************************************
*
*****************************************************************************/
STATIC int BurtCaPvSetState(CaPvStruct *pCaPv, ELLLIST *pStateList)
{
	ellDelete(pCaPv->pStateList, &pCaPv->Node);
	pCaPv->pStateList = pStateList;
	ellAdd(pCaPv->pStateList, &pCaPv->Node);
	return(0);
}

/*****************************************************************************
*
* Return an empty CaPvStruct.
*
*****************************************************************************/
STATIC CaPvStruct *BurtCaPvCreate(TranStruct *pTransport)
{
	CaPvStruct	*pCaPv = (CaPvStruct*)BurtMalloc(sizeof(CaPvStruct));

	pCaPv->pPv = NULL;
	pCaPv->pTransport = pTransport;
	pCaPv->pStateList = &pTransport->StateDisconnected;
	ellAdd(&pTransport->StateDisconnected, &pCaPv->Node);
	return(pCaPv);
}

/*****************************************************************************
*
*****************************************************************************/
STATIC void BurtCaPvDelete(CaPvStruct *pCaPv)
{
	ellDelete(pCaPv->pStateList, &pCaPv->Node);
	BurtFree(pCaPv);
	return;
}

/*****************************************************************************
*
* Returns:
*	ECA_TIMEOUT:	Normal completion, check ellCount(pStateList) to see if
*					all events completed or not.
*
*	otherwise:		CA error of some kind.
*
*****************************************************************************/
#define CA_PEND_TIME	.1	/* compensate for the oddites of ca_pend_event */
STATIC int BurtTransportServiceWaitState(ELLLIST *pStateList, float PollTime)
{
	int			status = ECA_TIMEOUT;
	int			inactivity = 0;
	int			lastcount;
	int			PendLoops;

#if 0
fprintf(stderr, "pending operation count = %d idle limit = %f\n", ellCount(pStateList), PollTime);
#endif

	PendLoops = PollTime / CA_PEND_TIME;

	/* Loop/wait as long as we are making progress */
	while ((lastcount = ellCount(pStateList)) > 0)
	{
		status = ca_pend_event(CA_PEND_TIME);
		if ((status != ECA_NORMAL) && (status != ECA_TIMEOUT))
			break;

		if (lastcount > ellCount(pStateList))
			inactivity = 0;
		else
			++inactivity;

		if (inactivity > PendLoops)
			break;
	}
	return(status);
}
/*****************************************************************************
*
* Kill some time --> wait for cawait seconds and return.
*
*****************************************************************************/
int BurtTransportServiceWasteTime(void *p, float cawait)
{
	ca_pend_event(cawait);
	return(0);
}

/*****************************************************************************
*
* Yuccy routines to deal with the printing of floating point numbers.
*
*****************************************************************************/
static double delta[2]={1e-6,1e-15};
static int precision[2]={6,14};
static void realToString(double value,char *preturn,int isdouble)
{
    long	intval;
    double	diff,absvalue;
    int		logval,prec,end;
    char	tstr[30];
    char	*ptstr=&tstr[0];
    int		round;
    int		ise=FALSE;
    char	*loce=NULL;

    if(value==0.0) {strcpy(preturn,"0"); return;};
    intval=value;
    diff = value - intval;
    if(diff<0.0) diff =-diff;
    absvalue = (value<0.0? -value: value);
    if(diff < absvalue*delta[isdouble]) {
	cvtLongToString(intval,preturn);
	return;
    }
    /*Now starts the hard cases*/
    if(value<0.0) {*preturn++ = '-'; value = -value;}
    logval = (int)log10(value);
    if(logval>6 || logval<-2 ) {
	ise=TRUE;
	prec = precision[isdouble];
	sprintf(ptstr,"%.*e",prec,value);
	loce = strchr(ptstr,'e');
	if(!loce) {errMessage(-1,"logic error in real to string"); return;}
	*loce++ = 0;
    } else {
	prec = precision[isdouble]-logval;
	if(prec<0)prec=0;
	sprintf(ptstr,"%.*f",prec,value);
    }
    if(prec>0) {
	end = strlen(ptstr) -1;
	round=FALSE;
	while(TRUE) {
	    if(end<=0)break;
	    if(tstr[end]=='.'){end--; break;}
	    if(tstr[end]=='0'){end--; continue;}
	    if(!round && end<precision[isdouble]) break;
	    if(!round && tstr[end]<'8') break;
	    if(tstr[end-1]=='.') {
		if(round)end = end-2;
		break;
	    }
	    if(tstr[end-1]!='9') break;
	    round=TRUE;
	    end--;
	}
	tstr[end+1]=0;
	while (round) {
	    if(tstr[end]<'9') {tstr[end]++; break;}
	    if(end==0) { *preturn++='1'; tstr[end]='0'; break;}
	    tstr[end--]='0';
	}
    }
    strcpy(preturn,&tstr[0]);
    if(ise) {
	if(!(strchr(preturn,'.'))) strcat(preturn,".0");
	strcat(preturn,"e");
	strcat(preturn,loce);
    }
    return;
}

static void floatToString(float value,char *preturn)
{
    realToString((double)value,preturn,0);
    return;
}

static void doubleToString(double value,char *preturn)
{
    realToString(value,preturn,1);
    return;
}


/*************************************************************************\
* Copyright (c) 2002 The University of Chicago, as Operator of Argonne
* National Laboratory.
* Copyright (c) 2002 The Regents of the University of California, as
* Operator of Los Alamos National Laboratory.
* This file is distributed subject to a Software License Agreement found
* in the file LICENSE that is included with this distribution. 
\*************************************************************************/

/**************************************************************************
 *	dbVarSub.h,v 1.2 2002/08/02 15:38:24 jba Exp
 *     Author:	Jim Kowalkowski
 *
 *      Experimental Physics and Industrial Control System (EPICS)
 *
 ***********************************************************************/

#define VAR_LEFT_IND '('
#define VAR_RIGHT_IND ')'
#define VAR_START_IND '$'
#ifdef vxWorks
#define VAR_MAX_SUB_SIZE 200
#define VAR_MAX_VAR_STRING 1500
#define VAR_MAX_VARS 100
#else
#define VAR_MAX_SUB_SIZE 300
#define VAR_MAX_VAR_STRING 50000
#define VAR_MAX_VARS 700
#endif
 
struct var_sub {
    char *var;
    char sub[VAR_MAX_SUB_SIZE];
};
           
struct var_tree {
    int me;
    struct var_tree* parent;
};

long dbDoSubst(char* replace, int replace_size, struct var_tree* par);
long dbInitSubst(char* parm_pattern);
void dbFreeSubst();


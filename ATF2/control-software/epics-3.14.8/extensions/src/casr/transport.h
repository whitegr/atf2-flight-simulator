/*************************************************************************\
* Copyright (c) 2002 The University of Chicago, as Operator of Argonne
* National Laboratory.
* Copyright (c) 2002 The Regents of the University of California, as
* Operator of Los Alamos National Laboratory.
* This file is distributed subject to a Software License Agreement found
* in the file LICENSE that is included with this distribution. 
\*************************************************************************/
/****************************************************************************
*
* Author:	John Winans
* Date:		95-11-20
*
*****************************************************************************/

#ifndef CASR_TRANSPORT_H
#define CASR_TRANSPORT_H

#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <limits.h>

#include "common.h"

int BurtTransportServiceInit(void **ppTransportPrivate);
int BurtTransportServiceCleanup(void **ppTransportPrivate);

int BurtTransportServicePvConnect(void *pTransportPrivate, ItemNode *pPvItem);
int BurtTransportServiceWaitConnect(void *pTransportPrivate, float interval);

int BurtTransportServicePvRead(void *pTransportPrivate, ItemNode *pPvItem);
int BurtTransportServiceWaitRead(void *pTransportPrivate, float interval);

int BurtTransportServicePvWrite(void *pTransportPrivate, ItemNode *pPvItem);
int BurtTransportServiceWaitWrite(void *pTransportPrivate, float interval);

int BurtTransportServiceWasteTime(void *pTransportPrivate, float cawait);

int BurtTransportServicePvDisconnect(void *pTransportPrivate, ItemNode *pPvItem);
int BurtTransportServiceWaitDisconnect(void *pTransportPrivate, float interval);
#endif

/*************************************************************************\
* Copyright (c) 2002 The University of Chicago, as Operator of Argonne
* National Laboratory.
* Copyright (c) 2002 The Regents of the University of California, as
* Operator of Los Alamos National Laboratory.
* This file is distributed subject to a Software License Agreement found
* in the file LICENSE that is included with this distribution. 
\*************************************************************************/
/****************************************************************************
*
* Author:   John Winans
* Date:     95-11-20
*
****************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <time.h>

#include "log.h"

static FILE	*LogFile = NULL;
static char *LogFileName = NULL;

/******************************************************************************
*
******************************************************************************/
static void LogPrime()
{
	if (LogFileName == NULL)
	{
		LogFile = stderr;
		return;
	}

	LogFile = fopen(LogFileName, "w");
	if (LogFile == NULL)
	{
		fprintf(stderr, "Can not open log file '%s'\n", LogFileName);
		LogFile = stderr;
		return;
	}

	LogMsg(0, "********************************** Start of system log\n");
}
/******************************************************************************
*
* Bozo function for SDDS.
*
******************************************************************************/
FILE *LogGetFd()
{
	if (LogFile == NULL)
		LogPrime();
	return(LogFile);
}
/******************************************************************************
*
******************************************************************************/
int LogOpen(char *FileName)
{
	if (FileName != NULL)
		LogFileName = strdup(FileName);
	return(0);
}
/******************************************************************************
*
******************************************************************************/
int LogCat(int ScreenFlag, char *Format, ...)
{
	va_list	ap;
	int		st;

	if (LogFile == NULL)
		LogPrime();

	va_start(ap, Format);
	st = vfprintf(LogFile, Format, ap);
	va_end(ap);
	if (ScreenFlag && (LogFile != stderr))
	{
		va_start(ap, Format);
		vfprintf(stderr, Format, ap);
		va_end(ap);
	}
	return(st);
}
/******************************************************************************
*
******************************************************************************/
int LogMsg(int ScreenFlag, char *Format, ...)
{
	int			st;
	time_t		Tod;
	struct tm	*ptm;
	va_list 	ap;

	if (LogFile == NULL)
		LogPrime();

	Tod = time(NULL);
	ptm = localtime(&Tod);

	if (LogFile != stderr)
		fprintf(LogFile, "%02d:%02d:%02d %02d-%02d-%02d ", ptm->tm_hour, ptm->tm_min, ptm->tm_sec, ptm->tm_year, ptm->tm_mon+1, ptm->tm_mday);

	va_start(ap, Format);
	st = vfprintf(LogFile, Format, ap);
	va_end(ap);

	if (ScreenFlag && (LogFile != stderr))
	{
		va_start(ap, Format);
		vfprintf(stderr, Format, ap);
		va_end(ap);
	}
	return(st);
}
/******************************************************************************
*
******************************************************************************/
int LogFlush()
{
	if (LogFile == NULL)
		return(0);

	fflush(LogFile);
	return(0);
}
/******************************************************************************
*
******************************************************************************/
int LogClose()
{
	if (LogFile == NULL)
		return(0);

	if (LogFile == stderr)
		return(0);

	LogMsg(0, "********************************** End of system log\n");
	fflush(LogFile);
	fclose(LogFile);
	return(0);
}

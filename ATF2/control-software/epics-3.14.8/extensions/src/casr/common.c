/*************************************************************************\
* Copyright (c) 2002 The University of Chicago, as Operator of Argonne
* National Laboratory.
* Copyright (c) 2002 The Regents of the University of California, as
* Operator of Los Alamos National Laboratory.
* This file is distributed subject to a Software License Agreement found
* in the file LICENSE that is included with this distribution. 
\*************************************************************************/
/****************************************************************************
*
* Author:   John Winans
* Date:     95-11-20
*
*****************************************************************************/

#include <unistd.h>
#include <sys/types.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <limits.h>
#include <time.h>
#include <pwd.h>
#include <ctype.h>

#include "common.h"
#include "transport.h"
#include "casrAscii.h"
#ifdef CASR_INCLUDE_SDDS
#include "casrSdds.h"
#endif
#include "dbVarSub.h"
#include "log.h"

static int BurtApplyMacros(ItemNode *pFile, ELLLIST *pGlobalContext, char *OptMacs);
static int BurtFileVerify(ItemNode *pFile);

/*****************************************************************************
*
* Error exit handler for burt programs.  There should be no other
* exit statement in a burt program other than this, and one at the end
* of main().
*
*****************************************************************************/
void BurtCroak(int status, char *Message)
{
	LogMsg(0, "Burt failure, status %d, can not proceed... %s\n", status, Message);
	exit(status);
}

/*****************************************************************************
*
* A malloc for burt that should be considered return-value safe.
*
*****************************************************************************/
void *BurtMalloc(size_t size)
{
	void *p;
	
	p = malloc(size);
	if (p == NULL)
		BurtCroak(1, "Out of memory");

	return(p);
}

void *BurtRealloc(void *buf, size_t size)
{
	void *p = realloc(buf, size);
	if (p == NULL)
		BurtCroak(2, "Out of Memory");

	return(p);
}

/*****************************************************************************
*
* Provided for symmetry reasons.
*
*****************************************************************************/
void BurtFree(void *p)
{
	if (p != NULL)
		free(p);
	return;
}

/*****************************************************************************
*
* Allocates and initializes an empty ItemNode structure.
*
*****************************************************************************/
ItemNode *BurtItemCreate(void)
{
	ItemNode	*pItem;

	pItem = (ItemNode*) BurtMalloc(sizeof(ItemNode));

	ellInit(&(pItem->AttributeList));
	pItem->Value = NULL;
	pItem->MValue = NULL;
	pItem->pParent = NULL;
	pItem->pTranPvt = NULL;
	return(pItem);
}

/*****************************************************************************
*
* Nuke the given item and all its attributes...
*
*****************************************************************************/
int BurtItemDelete(ItemNode *pItem)
{
	ItemNode	*pChild;

	if (pItem == NULL)
		return(0);

	/* Children */
	pChild = BurtFindAttribute(NULL, pItem);
	while (pChild != NULL)
	{
		BurtItemDelete(pChild);
		pChild = BurtFindNext(NULL, pChild);
	}

	/* Self */
	BurtFree(pItem->Value);
	BurtFree(pItem->MValue);

	if (pItem->pParent != NULL)
		ellDelete(&pItem->pParent->AttributeList, &pItem->Node);

	BurtFree(pItem);
	return(0);
}

/*****************************************************************************
*
* Read the PV information out of the given list of burt files.
*
*****************************************************************************/
int BurtFileRead(ELLLIST *pFileList, ELLLIST *pGlobalContext, char *OptMacs)
{
	ItemNode		*pFile;
	ItemNode		*pPath;

	pPath = (ItemNode*)ellFirst(pGlobalContext);
	if ((pPath!=NULL)&&(strcmp(pPath->Value, "path") != 0))
		pPath = BurtFindNext("path", pPath);
#ifdef XXX
	if (pPath==NULL)
		return(0);
#endif

	pFile = (ItemNode*) ellFirst(pFileList);
	while(pFile != NULL)
	{
#ifdef CASR_INCLUDE_SDDS
		if (((char*)(pFile->MValue))[0] == 's')
		{
			if (BurtSddsReadFile(pFile) != 0)
				return(-1);
		}
		else
#endif
		{
			if (BurtAsciiReadFile(pFile, pPath) != 0)
				return(-1);
		}
		BurtFileVerify(pFile);
		BurtApplyMacros(pFile, pGlobalContext, OptMacs);
		pFile = (ItemNode*)ellNext(&pFile->Node);
	}
	return(0);
}
/*****************************************************************************
*
* Wait for cawait seconds to pass.
*
*****************************************************************************/
int BurtPvWasteTime(void *p, float cawait)
{
	return(BurtTransportServiceWasteTime(p, cawait));
}
/*****************************************************************************
*
* Called one time during initialization to initialize the transport layer.
*
*****************************************************************************/
int BurtPvInitialize(void **p, float cawait)
{
	return(BurtTransportServiceInit(p));
}
int BurtPvCleanup(void **p)
{
	return(BurtTransportServiceCleanup(p));
}
/*****************************************************************************
*
* Open a channel access connection to each of the PVs in each of the files
* in the file list.
*
*****************************************************************************/
int BurtPvConnect(void *pTransportPrivate, ELLLIST *pFileList, float cawait, int UseRon)
{
	ItemNode		*pFile;

	pFile = (ItemNode*) ellFirst(pFileList);
	while(pFile != NULL)
	{
		if (BurtPvFileConnect(pTransportPrivate, pFile, UseRon) != 0)
			return(-1);

		pFile = (ItemNode*)ellNext(&pFile->Node);
	}

	/* Wait for all PV connections to complete */
	BurtTransportServiceWaitConnect(pTransportPrivate, cawait);

	return(0);
}

/*****************************************************************************
*
* Given a file item reference, connect to all the PVs in the file.
*
*****************************************************************************/
int BurtPvFileConnect(void *pTransportPrivate, ItemNode *pFileItem, int UseRon)
{
	ItemNode	*pDataItem;
	ItemNode	*pPv;

	pDataItem = BurtFindAttribute("data", pFileItem);
	while (pDataItem != NULL)
	{
		pPv = BurtFindAttribute(NULL, pDataItem);

		while(pPv != NULL)
		{
			/* Don't do it if it is a RO or RON */
			if ((UseRon == 0) || (BurtFindAttribute("readonly", pPv) == NULL))
				BurtTransportServicePvConnect(pTransportPrivate, pPv);
			pPv = BurtFindNext(NULL, pPv);
		}
		pDataItem = BurtFindNext("data", pDataItem);
	}

	return(0);
}

/*****************************************************************************
*
* Closes the channel access connection to each of the PVs in each of the files
* in the file list.
*
*****************************************************************************/
int BurtPvDisconnect(void *pTransportPrivate, ELLLIST *pFileList, float cawait)
{
	ItemNode		*pFile;

	pFile = (ItemNode*) ellFirst(pFileList);
	while(pFile != NULL)
	{
		if (BurtPvFileDisconnect(pTransportPrivate, pFile) != 0)
			return(-1);

		pFile = (ItemNode*)ellNext(&pFile->Node);
	}

	/* Wait for all PV connections to complete */
	BurtTransportServiceWaitDisconnect(pTransportPrivate, cawait);

	return(0);
}
/*****************************************************************************
*
*****************************************************************************/
int BurtPvFileDisconnect(void *pTransportPrivate, ItemNode *pFileItem)
{
	ItemNode	*pDataItem;
	ItemNode	*pPv;

	pDataItem = BurtFindAttribute("data", pFileItem);
	while (pDataItem != NULL)
	{
		pPv = BurtFindAttribute(NULL, pDataItem);

		while(pPv != NULL)
		{
			BurtTransportServicePvDisconnect(pTransportPrivate, pPv);
			pPv = BurtFindNext(NULL, pPv);
		}
		pDataItem = BurtFindNext("data", pDataItem);
	}

	return(0);
}

/*****************************************************************************
*
* Reads the value of each PV in each of the files that are in the file list.
*
*****************************************************************************/
int BurtPvRead(void *pTransportPrivate, ELLLIST *pFileList, float cawait)
{
	ItemNode		*pFile;

	pFile = (ItemNode*) ellFirst(pFileList);
	while(pFile != NULL)
	{
		if (BurtPvFileRead(pTransportPrivate, pFile) != 0)
			return(-1);

		pFile = (ItemNode*)ellNext(&pFile->Node);
	}

	/* Wait for all PV connections to complete */
	BurtTransportServiceWaitRead(pTransportPrivate, cawait);

	return(0);
}
/*****************************************************************************
*
* Request that the transport layer add the following items to the PV Item
* attribute list:
*
*	value(dim() data()) stat() sevr() time()
*
*****************************************************************************/
int BurtPvFileRead(void *pTransportPrivate, ItemNode *pFileItem)
{
	ItemNode	*pDataItem;
	ItemNode	*pPv;

	pDataItem = BurtFindAttribute("data", pFileItem);
	while (pDataItem != NULL)
	{
		pPv = BurtFindAttribute(NULL, pDataItem);

		while(pPv != NULL)
		{
			if (BurtFindAttribute("writeonly", pPv) == NULL)
			{
				BurtTrimPvItemValueInfo(pPv);
				BurtTransportServicePvRead(pTransportPrivate, pPv);
			}
#ifdef XXX
			else
			{
				LogMsg(0, "'%s' is writeonly, not reading...\n", pPv->Value);
			}
#endif
			pPv = BurtFindNext(NULL, pPv);
		}
		pDataItem = BurtFindNext("data", pDataItem);
	}
	return(0);
}

/*****************************************************************************
*
* Writes the value of each PV in each of the files that are in the file list.
*
*****************************************************************************/
int BurtPvWrite(void *pTransportPrivate, ELLLIST *pFileList, float cawait)
{
	ItemNode		*pFile;
	char			*SnapType;

	pFile = (ItemNode*) ellFirst(pFileList);
	while(pFile != NULL)
	{
		/* XXX check if the filetype is a nowrite */
		SnapType = BurtGetInputSnapType(pFile);

		if ((SnapType==NULL)||(strcmp("Nowrite", SnapType)!=0))
		{
			if (BurtPvFileWrite(pTransportPrivate, pFile) != 0)
				return(-1);
		}

		pFile = (ItemNode*)ellNext(&pFile->Node);
	}

	/* Wait for all PV network operations to complete */
	BurtTransportServiceWaitWrite(pTransportPrivate, cawait);

	return(0);
}

/*****************************************************************************
*
*****************************************************************************/
int BurtPvFileWrite(void *pTransportPrivate, ItemNode *pFileItem)
{
	ItemNode	*pDataItem;
	ItemNode	*pPv;

	pDataItem = BurtFindAttribute("data", pFileItem);
	while (pDataItem != NULL)
	{
		pPv = BurtFindAttribute(NULL, pDataItem);

		while(pPv != NULL)
		{
			ItemNode	*pRO;
			/* Don't do it if it is a RO or RON */
			if ((pRO = BurtFindAttribute("readonly", pPv)) != NULL)
			{
				if (BurtFindAttribute("notify", pRO) != NULL)
				{
					ItemNode	*pValue;
					ItemNode	*pDim;
					pValue = BurtFindAttribute("value", pPv);
					pDim = BurtFindAttribute("dim", pValue);
					pDim = BurtFindAttribute(NULL, pDim);
					pValue = BurtFindAttribute("data", pValue);
					pValue = BurtFindAttribute(NULL, pValue);

					if ((pValue != NULL)&&(pValue->Value != NULL))
						LogMsg(0, "RON %s %s\n", pPv->Value, pValue->Value);
					else
						LogMsg(0, "RON %s\n", pPv->Value);
				}
			}
			else
				BurtTransportServicePvWrite(pTransportPrivate, pPv);

			pPv = BurtFindNext(NULL, pPv);
		}
		pDataItem = BurtFindNext("data", pDataItem);
	}
	return(0);
}
/*****************************************************************************
*
* Delete the 'env' section from each of the input files.
*
*****************************************************************************/
int BurtTrimEnvSection(ELLLIST *pFileList)
{
	ItemNode		*pFile;
	ItemNode		*pEnv;
	ItemNode		*pSnapType;
	ItemNode		*pItem;

	pFile = (ItemNode*) ellFirst(pFileList);
	while(pFile != NULL)
	{
		pEnv = BurtFindAttribute("env", pFile);		
#ifdef XXX
		pItem = BurtFindAttribute(NULL, pEnv);
		while(pItem != NULL)
		{
			if (strcmp(pItem->Value, "SnapType") != 0)
				BurtItemDelete(pItem);

			pItem = BurtFindNext(NULL, pItem);
		}
		/* If it is now empty, nuke the entire thing */
		if ((pItem = BurtFindAttribute(NULL, pEnv)) == NULL)
			BurtItemDelete(pEnv);
#else
		BurtItemDelete(pEnv);
#endif

		pFile = (ItemNode*)ellNext(&pFile->Node);
	}
	return(0);
}
/*****************************************************************************
*
* Remove the following attributes from the given ItemNode:
*
*	value() stat() sevr() time()
*
*****************************************************************************/
int BurtTrimPvItemValueInfo(ItemNode *pPv)
{
	BurtItemDelete(BurtFindAttribute("value", pPv));
	BurtItemDelete(BurtFindAttribute("stat", pPv));
	BurtItemDelete(BurtFindAttribute("sevr", pPv));
	BurtItemDelete(BurtFindAttribute("time", pPv));
		
	return(0);
}



static int NewLine(int Level, FILE *OutFile);
static void DumpItemLogic(ItemNode *pItem, FILE *OutFile, int Level, int BreakLevel, int macroFlag);
static int LinePos;

/*****************************************************************************
*
* This is used to print out the contents of an item list in burt ascii file
* format.
*
*****************************************************************************/
static void PrintItemValue(char *Value, FILE *file)
{
    char    *p = Value;

    while(*p)
    {
		if (!isalnum(*p) && (*p != '.') && (*p != ':') && (*p != '+') && (*p != '-') && (*p != '_'))
        {
            fputc('"', file);
            fputs(Value, file);
            fputc('"', file);
            return;
        }
        ++p;        
    }   
    fputs(Value, file);
    return;
}

void BurtDumpItem(ItemNode *pItem, FILE *OutFile, int BreakLevel, int macroFlag)
{
	LinePos = 0;
	DumpItemLogic(pItem, OutFile, 0, BreakLevel, macroFlag);
	fputc('\n', OutFile);
	return;
}

static void DumpItemLogic(ItemNode *pItem, FILE *OutFile, int Level, int BreakLevel, int macroFlag)
{
	int	FirstInList;

	if (Level > 0)
	{
		fputc('{', OutFile);
		++LinePos;
		if (Level < BreakLevel)
			LinePos = NewLine(Level, OutFile);
	}

	FirstInList = 1;
	while(pItem != NULL)
	{
		if (pItem->Value != NULL)
		{
			if (LinePos > 70)
			{
				LinePos = NewLine(Level, OutFile);
			}
			if (FirstInList)
				FirstInList = 0;
			else
			{
				if (Level < BreakLevel)
					LinePos = NewLine(Level, OutFile);
				else
				{
					fputc(' ', OutFile);
					LinePos += 1;
				}
			}
	
			if ((macroFlag != 0)&&(pItem->MValue != NULL))
				PrintItemValue(pItem->MValue, OutFile);
			else
				PrintItemValue(pItem->Value, OutFile);
			LinePos += strlen(pItem->Value);
	
			if (ellCount(&pItem->AttributeList) > 0)
			{
				DumpItemLogic((ItemNode*)ellFirst(&(pItem->AttributeList)), OutFile, Level+1, BreakLevel, macroFlag);
			}
		}
		if (Level > 0)
			pItem = (ItemNode*)ellNext(&(pItem->Node));
		else
			break;
	}

	if (Level > 0)
	{
		if (Level < BreakLevel)
			LinePos = NewLine(Level-1, OutFile);
		fputc('}', OutFile);
		++LinePos;
	}

	return;
}

static int NewLine(int Level, FILE *OutFile)
{
	int	Pos;

	if (Level > 15)
		Level = 15;

	Pos = Level*2;

	fputc('\n', OutFile);
	while(Level--)
		fputs("  ", OutFile);

	return(Pos);
}


/*****************************************************************************
*
* Check the Item tree created by a file read to make sure it is legal.
*
* At the moment we accept all parsable trees and pass thru any unused data
* in tact. XXX
*
*****************************************************************************/
static int BurtFileVerify(ItemNode *pFile)
{
	return(0);
}
/*****************************************************************************
*
* How many PVs are there in the entire request file set?
*
*****************************************************************************/
long BurtCountAllPvs(ELLLIST *pFileList)
{
	ItemNode	*pFileItem;
	ItemNode	*pDataItem;
	long		count = 0;

	if (pFileList == NULL)
		return(0);
	pFileItem = (ItemNode*) ellFirst(pFileList);
	while(pFileItem != NULL)
	{
		pDataItem = BurtFindAttribute("data", pFileItem);
		while (pDataItem != NULL)
		{
			count += ellCount(&pDataItem->AttributeList);
			pDataItem = BurtFindNext("data", pDataItem);
		}
		pFileItem = (ItemNode*) ellNext(&pFileItem->Node);
	}
	return(count);
}
/*****************************************************************************
*
* Apply all macros to the PV names in the tree.
*
*****************************************************************************/
typedef struct MacroStruct 
{
	char		*Name;
	char		*Value;
} MacroStruct;
/*****************************************************************************
*
* Walk thru the input file and delete any macros that have been overridden 
* from the command line environment.
*
*****************************************************************************/
static int BurtCleanOverriddenMacros(ItemNode *pFile, ELLLIST *pGlobalContext, char *OptMacs)
{
	ItemNode	*pSection;
	ItemNode	*pGlobMacs;

	/* Boy, don't these next few lines represent a hose-job on my list design */
	pGlobMacs = (ItemNode*)ellFirst(pGlobalContext);
	if ((pGlobMacs!=NULL)&&(strcmp(pGlobMacs->Value, "macros") != 0))
		BurtFindNext("macros", pGlobMacs);
	if (pGlobMacs==NULL)
	{
#ifdef CASR_DEBUG
		LogMsg(0, "No command line macros found\n");
#endif
		return(0);
	}

	pSection = BurtFindAttribute(NULL, pFile);
	while(pSection != NULL)
	{
		if ((strcmp("macros", pSection->Value)==0)||((OptMacs!=NULL)&&(strcmp(OptMacs, pSection->Value)==0)))
		{
			ItemNode	*pMacName;
			ItemNode	*pMacValue;
			pMacName = BurtFindAttribute(NULL, pSection);
			while(pMacName != NULL)
			{
				ItemNode	*pItem = BurtFindNext(NULL, pMacName);

				if (BurtFindAttribute(pMacName->Value, pGlobMacs) != NULL)
				{
#ifdef CASR_DEBUG
					LogMsg(0, "Removing redefined macro '%s'\n", pMacName->Value);
#endif
					BurtItemDelete(pMacName);
				}
				pMacName = pItem;
			}
		}
		pSection = BurtFindNext(NULL, pSection);
	}
	/* Now move the command line macros into its own section */
	ellDelete(pGlobalContext, &pGlobMacs->Node);
	ellInsert(&pFile->AttributeList, NULL, &pGlobMacs->Node);

	return(0);
}
/*****************************************************************************
*
*****************************************************************************/
static int BurtMacroFind(MacroStruct *pMacros, int MacroCount, char *Name)
{
	int	i = 0;
	while(i < MacroCount)
	{
		if (strcmp(pMacros[i].Name, Name) == 0)
			return(i);
		++i;
	}
	return(-1);
}
/*****************************************************************************
*
*****************************************************************************/
static int BurtApplyMacros(ItemNode *pFile, ELLLIST *pGlobalContext, char *OptMacs)
{
	MacroStruct	*pMacros;
	int			MacroCount;
	int			MacroSize;
	ItemNode	*pSection;

	BurtCleanOverriddenMacros(pFile, pGlobalContext, OptMacs);

	MacroCount = 0;
	MacroSize = 1000;
	pMacros = (MacroStruct*)BurtMalloc(MacroSize * sizeof(MacroStruct));

	pSection = BurtFindAttribute(NULL, pFile);
	while(pSection != NULL)
	{
		if ((strcmp("macros", pSection->Value)==0)||((OptMacs!=NULL)&&(strcmp(OptMacs, pSection->Value)==0)))
		{
			ItemNode	*pMacName;
			ItemNode	*pMacValue;
			pMacName = BurtFindAttribute(NULL, pSection);
			while(pMacName != NULL)
			{
				if (pMacName->Value != NULL)
				{
					int i;
					i = BurtMacroFind(pMacros, MacroCount, pMacName->Value);
					if (i < 0)
					{
						if (MacroCount == MacroSize)
						{
							MacroSize += 1000;
							pMacros = (MacroStruct*)BurtRealloc(pMacros, MacroSize * sizeof(MacroStruct));
						}
						i = MacroCount++;
					}
					pMacros[i].Name = pMacName->Value;

					pMacValue = BurtFindAttribute(NULL, pMacName);
					if ((pMacValue==NULL)||(pMacValue->Value==NULL))
						pMacros[i].Value = "";
					else
						pMacros[i].Value = pMacValue->Value;
				}
				pMacName = BurtFindNext(NULL, pMacName);
			}
		}
		else if (strcmp("data", pSection->Value)==0)
		{
			int i = 0;
			ItemNode	*pPv;
			char		*MacBuf;
			size_t		MacMax=1000;
			size_t		MacLen=0;

			MacBuf = (char*)BurtMalloc(MacMax);	/* Prime it */
			MacBuf[0] = '\0';

			while (i < MacroCount)
			{
				MacLen += strlen(pMacros[i].Name) + strlen(pMacros[i].Value) + 2;
				if (MacLen >= MacMax)
					MacBuf = (char *)BurtRealloc(MacBuf, MacLen+4000);
				MacMax += 4000;
				strcat(MacBuf, pMacros[i].Name);
				strcat(MacBuf, "=");
				strcat(MacBuf, pMacros[i].Value);
				strcat(MacBuf, ",");
				++i;
			}
			if (dbInitSubst(MacBuf) != 0)
				BurtCroak(4, "Bogus macro information encountered");
		
			pPv = BurtFindAttribute(NULL, pSection);
			while(pPv != NULL)
			{
				char	tmp[500];
				char	*pString = tmp;
				int		len = strlen(pPv->Value);
				if (len < sizeof(tmp))
				{
					strncpy(tmp, pPv->Value, sizeof(tmp));
					if (tmp[0] == '"')
					{
						tmp[len-1] = '\0';
						pString = &tmp[1];
					}
					if (dbDoSubst(pString, sizeof(tmp)-1, NULL) == 0)
						pPv->MValue = strdup(pString);
					else
						BurtCroak(6, "Burt macro substitution failed");
				}
				else
				{
					LogMsg(0, "Invalid pvName encountered '%s'\n", pPv->Value);
					BurtCroak(3, "PV name too long");
				}
				pPv = BurtFindNext(NULL, pPv);
			}
			dbFreeSubst();
			BurtFree(MacBuf);
		}
		pSection = BurtFindNext(NULL, pSection);
	}
	BurtFree(pMacros);
	return(0);
}
/*****************************************************************************
* 
* Given an item node, find the first attribute node with the specified
* value
*
*****************************************************************************/
ItemNode *BurtFindAttribute(char *Attribute, ItemNode *pSection)
{
	ItemNode	*pItem;

	if (pSection == NULL)
		return(NULL);

	pItem = (ItemNode*)ellFirst(&pSection->AttributeList);

	if (Attribute == NULL)
		return(pItem);

	while(pItem != NULL)
	{
		if (strcmp(pItem->Value, Attribute) == 0)
			return(pItem);
		pItem = (ItemNode*)ellNext(&pItem->Node);
	}
	return(NULL);
}

/*****************************************************************************
* 
* Find next item in current attribute list with the specified value
*
*****************************************************************************/
ItemNode *BurtFindNext(char *Value, ItemNode *pCurrent)
{
	ItemNode	*pItem;

	if (pCurrent == NULL)
		return(NULL);

	pItem = (ItemNode*)ellNext(&pCurrent->Node);

	if (Value == NULL)
		return(pItem);

	while(pItem != NULL)
	{
		if (strcmp(pItem->Value, Value) == 0)
			return(pItem);
		pItem = (ItemNode*)ellNext(&pItem->Node);
	}
	return(NULL);
}

/*****************************************************************************
*
*****************************************************************************/
ItemNode *BurtItemAddAttribute(ItemNode *pParentItem, const char *pStr)
{
    ItemNode *pItem;

	pItem = BurtItemCreate();
    pItem->Value = strdup(pStr);
    pItem->pParent = pParentItem;
    ellAdd(&pParentItem->AttributeList, &pItem->Node);
	return(pItem);
}

/*****************************************************************************
*
* This function adds the following items to the given list:
*	Logonid
*	LocalTime
*	LocalDTime
*	LocalJDate
*
*	GmtTime
*	GmtDTime
*	GmtJDate
*
*****************************************************************************/
int BurtSetGlobalEnv(ELLLIST *pGlobalContext, ELLLIST *pFileList)
{
	ItemNode		*pItem;
	ItemNode		*pFile;
	time_t			t;
	struct tm		*pTmStruct;
	struct passwd	*pPasswd;
	uid_t			uid;
	char			buff[1000];
	char			cwd[1000];

	uid = geteuid();
	if ((pPasswd = getpwuid(uid)) == NULL)
	{
		LogMsg(1, "WARNING: failed to get user information for %d\n", uid);
		sprintf(buff, "uid-%d", uid);
	}
	else
		sprintf(buff, "%s (%s)", pPasswd->pw_name, pPasswd->pw_gecos);

	pItem = BurtItemCreate();
	pItem->Value = strdup("Logonid");
	ellAdd(pGlobalContext, &pItem->Node);
	BurtItemAddAttribute(pItem, buff);

	pItem = BurtItemCreate();
	pItem->Value = strdup("SnapType");
	ellAdd(pGlobalContext, &pItem->Node);
	BurtItemAddAttribute(pItem, "Absolute");	/* XXX */

	BurtSetGlobalEnvTime(pGlobalContext);
	
	if (getcwd(cwd, sizeof(cwd)) == NULL)
		strcpy(cwd, ".");

	pFile = (ItemNode*) ellFirst(pFileList);
	while(pFile != NULL)
	{
		pItem = BurtItemCreate();
		pItem->Value = strdup("InputFile");
		ellAdd(pGlobalContext, &pItem->Node);

		if (pFile->Value[0] != '/')
			sprintf(buff, "%s/%s", cwd, pFile->Value);
		else
			strcpy(buff, pFile->Value);
		BurtItemAddAttribute(pItem, buff);
#ifdef CASR_DEBUG
		LogMsg(0, "BurtSetGlobalEnv: InputFile{\"%s\"}\n", buff);
#endif

		pFile = (ItemNode*)ellNext(&pFile->Node);
	}

	return(0);
}
int BurtSetGlobalEnvTime(ELLLIST *pGlobalContext)
{
	ItemNode		*pItem;
	time_t			t;
	struct tm		*pTmStruct;
	struct passwd	*pPasswd;
	uid_t			uid;
	char			buff[1000];

	/* Delete any stale time info */
	/* This is a hack... we KNOW that the time stuff is NEVER first */
	pItem=(ItemNode*) ellFirst(pGlobalContext);
	BurtItemDelete(BurtFindNext("LocalTime", pItem));
	pItem=(ItemNode*) ellFirst(pGlobalContext);
	BurtItemDelete(BurtFindNext("LocalDTime", pItem));
	pItem=(ItemNode*) ellFirst(pGlobalContext);
	BurtItemDelete(BurtFindNext("LocalJDate", pItem));
	pItem=(ItemNode*) ellFirst(pGlobalContext);
	BurtItemDelete(BurtFindNext("GmtDTime", pItem));
	pItem=(ItemNode*) ellFirst(pGlobalContext);
	BurtItemDelete(BurtFindNext("GmtJDate", pItem));
	pItem=(ItemNode*) ellFirst(pGlobalContext);
	BurtItemDelete(BurtFindNext("Time", pItem));
	pItem=(ItemNode*) ellFirst(pGlobalContext);
	BurtItemDelete(BurtFindNext("TimeStamp", pItem));

	t = time(NULL);
	pTmStruct = localtime(&t);
	
	strcpy(buff, ctime(&t));
	buff[strlen(buff)-1] = ' ';
	pItem = BurtItemCreate();
	pItem->Value = strdup("TimeStamp");
	ellAdd(pGlobalContext, &pItem->Node);
	BurtItemAddAttribute(pItem, buff);

	sprintf(buff, "%ld", t);
	pItem = BurtItemCreate();
	pItem->Value = strdup("Time");
	ellAdd(pGlobalContext, &pItem->Node);
	BurtItemAddAttribute(pItem, buff);

#if 0
	strftime(buff, sizeof(buff), "%C", pTmStruct);
	pItem = BurtItemCreate();
	pItem->Value = strdup("LocalTime");
	ellAdd(pGlobalContext, &pItem->Node);
	BurtItemAddAttribute(pItem, buff);
#endif

	strftime(buff, sizeof(buff), "%H%M%S", pTmStruct);
	pItem = BurtItemCreate();
	pItem->Value = strdup("LocalDTime");
	ellAdd(pGlobalContext, &pItem->Node);
	BurtItemAddAttribute(pItem, buff);

	strftime(buff, sizeof(buff), "%y%j", pTmStruct);
	pItem = BurtItemCreate();
	pItem->Value = strdup("LocalJDate");
	ellAdd(pGlobalContext, &pItem->Node);
	BurtItemAddAttribute(pItem, buff);

	pTmStruct = gmtime(&t);
	strftime(buff, sizeof(buff), "%H%M%S", pTmStruct);
	pItem = BurtItemCreate();
	pItem->Value = strdup("GmtDTime");
	ellAdd(pGlobalContext, &pItem->Node);
	BurtItemAddAttribute(pItem, buff);

	strftime(buff, sizeof(buff), "%y%j", pTmStruct);
	pItem = BurtItemCreate();
	pItem->Value = strdup("GmtJDate");
	ellAdd(pGlobalContext, &pItem->Node);
	BurtItemAddAttribute(pItem, buff);

	return(0);
}
/*****************************************************************************
*
*****************************************************************************/
char  *BurtGetInputSnapType(ItemNode *pFileItem)
{
	ItemNode	*pItem;
	pItem = BurtFindAttribute("env", pFileItem);
	pItem = BurtFindAttribute("SnapType", pItem);
	pItem = BurtFindAttribute(NULL, pItem);
	if (pItem == NULL)
		return(NULL);
	return(pItem->Value);
}

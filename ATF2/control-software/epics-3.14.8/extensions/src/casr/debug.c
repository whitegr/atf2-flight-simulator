/*************************************************************************\
* Copyright (c) 2002 The University of Chicago, as Operator of Argonne
* National Laboratory.
* Copyright (c) 2002 The Regents of the University of California, as
* Operator of Los Alamos National Laboratory.
* This file is distributed subject to a Software License Agreement found
* in the file LICENSE that is included with this distribution. 
\*************************************************************************/
/****************************************************************************
*
* Author:   John Winans
* Date:     95-11-20
*
*****************************************************************************/

#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <limits.h>

#include "common.h"
#include "debug.h"

/*****************************************************************************
*
* This prints out state information about burt.
*
*****************************************************************************/
void BurtDebugPrintParms(BurtParmStruct *parms)
{
	ItemNode	*pItem;

	if (parms->logfile != NULL)
		fprintf(stderr, "LogFile '%s'\n", parms->logfile);
	else
		fprintf(stderr, "LogFile (null)\n");
	if (parms->asciioutfile != NULL)
		fprintf(stderr, "Ascii Outfile '%s'\n", parms->asciioutfile);
	else
		fprintf(stderr, "Ascii Outfile (null)\n");
	if (parms->sddsoutfile != NULL)
		fprintf(stderr, "SDDS Outfile '%s'\n", parms->sddsoutfile);
	else
		fprintf(stderr, "SDDS Outfile (null)\n");
	fprintf(stderr, "cawait %f\n", parms->cawait);
	fprintf(stderr, "debugFlag = %d\n", parms->debugFlag);

	fprintf(stderr, "\nGlobal environment:\n");
	pItem = (ItemNode*)ellFirst(&(parms->GlobalItemList));
	while(pItem != NULL)
	{
		BurtDumpItem(pItem, stderr, 1, 1);
		pItem = (ItemNode*)ellNext(&pItem->Node);
	}

	fprintf(stderr, "\nFile Contents:\n");
	pItem = (ItemNode*)ellFirst(&(parms->FileList));
	while(pItem != NULL)
	{
		BurtDumpItem(pItem, stderr, 3, 1);
		pItem = (ItemNode*)ellNext(&pItem->Node);
	}
	return;
}

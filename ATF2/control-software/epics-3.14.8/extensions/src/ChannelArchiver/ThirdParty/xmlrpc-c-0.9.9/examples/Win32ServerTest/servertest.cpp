/* A simple standalone XML-RPC server written in C. */

#include <stdio.h>
#ifndef HAVE_WIN32_CONFIG_H
#include "xmlrpc_config.h"
#else
#include "xmlrpc_win32_config.h"
#endif

#include "xmlrpc.h"
#include "xmlrpc_client.h"
#include "xmlrpc_abyss.h"

xmlrpc_value *
echo (xmlrpc_env *env, xmlrpc_value *param_array, void *user_data)
{
    char * String1 = (char*) malloc (1024);
	memset (String1, 0, 1024);

    /* Parse our argument array. */
    xmlrpc_parse_value(env, param_array, (char*)"(s)", &String1);
    if (env->fault_occurred)
	return NULL;

	printf ("ECHO: %s\r\n", String1);
    /* Return our echo result. */
    return xmlrpc_build_value(env, "s", String1);
}

void main (int argc, char **argv)
{
    if (argc != 2) {
	fprintf(stderr, "Usage: servertest abyss.conf\n");
	exit(1);
    }

    xmlrpc_server_abyss_init(XMLRPC_SERVER_ABYSS_NO_FLAGS, argv[1]);
    xmlrpc_server_abyss_add_method("examples.echo", &echo, NULL);

    printf("server: switching to background.\n");
    xmlrpc_server_abyss_run();
}
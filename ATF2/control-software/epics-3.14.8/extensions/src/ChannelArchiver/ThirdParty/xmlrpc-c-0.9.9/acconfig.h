/* Define if va_list is actually an array. */
#undef VA_LIST_IS_ARRAY

/* Define if we're using a copy of libwww with built-in SSL support. */
#undef HAVE_LIBWWW_SSL

/*

					W3C Sample Code Library libwww HTTP SERVER




!HTTP Server Module!

*/

/*
**	(c) COPYRIGHT MIT 1995.
**	Please first read the full copyright statement in the file COPYRIGH.
*/

/*

This is actually a very small definition file as almost everything is
set up elsewhere. 

This module is implemented by HTTPServ.c, and
it is a part of the W3C
Sample Code Library. 

*/

#ifndef HTTPSERV_H
#define HTTPSERV_H

#include "HTProt.h"

extern HTProtCallback HTServHTTP;

#endif /* HTTPSERV_H */

/*



@(#) $Id: HTTPServ.h,v 1.1.1.1 2009/03/14 06:44:11 whitegr Exp $


*/

/*
 * Created on Feb 7, 2005
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package epics.archiveviewer.xal.view.export;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.GridLayout;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import epics.archiveviewer.xal.view.components.AVAbstractPanel;

/**
 * @author serge
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class ExportOptionsPanel extends AVAbstractPanel
{
	private JLabel methodLabel;
	private JComboBox methodBox;
	private JLabel maxCountPerAVELabel;
	private JTextField countField;
	private JLabel periodLabel;
	private JTextField periodField;
	private JLabel tsFormatLabel;
	private JTextField tsFormatField;
	private JCheckBox exportStatusBox;
	private JButton resetAndCloseButton;

	public ExportOptionsPanel()
	{
		init();
	}
	
	protected void createComponents() {	
		this.methodLabel = new JLabel("Method");
		this.methodBox = new JComboBox();
		this.maxCountPerAVELabel = new JLabel("Max Count Per AVE");
		this.countField = new JTextField(10);
		this.periodLabel = new JLabel("Period [secs]");
		this.periodField = new JTextField(10);
		this.tsFormatLabel = new JLabel("Timestamp Format");
		this.tsFormatField = new JTextField(15);
		this.exportStatusBox = new JCheckBox("Export Status");
		this.resetAndCloseButton = new JButton("Reset & Close");
	}

	protected void addComponents() {
		JPanel labelsPanel = new JPanel(new GridLayout(0,1));
		labelsPanel.add(this.methodLabel);
		labelsPanel.add(this.maxCountPerAVELabel);
		labelsPanel.add(this.periodLabel);
		labelsPanel.add(this.tsFormatLabel);
		labelsPanel.add(new JLabel());
		
		JPanel methodBoxPanel = new JPanel(new BorderLayout());
		methodBoxPanel.add(this.methodBox, BorderLayout.WEST);
		
		JPanel checkBoxPanel = new JPanel(new BorderLayout());
		checkBoxPanel.add(this.exportStatusBox, BorderLayout.WEST);
		
		JPanel inputPanel = new JPanel(new GridLayout(0,1));
		inputPanel.add(methodBoxPanel);
		inputPanel.add(this.countField);
		inputPanel.add(this.periodField);
		inputPanel.add(this.tsFormatField);
		inputPanel.add(checkBoxPanel);
		
		JPanel p = new JPanel(new BorderLayout(5,0));
		p.add(labelsPanel, BorderLayout.WEST);
		p.add(inputPanel, BorderLayout.CENTER);
		
		JPanel resetAndCloseButtonPanel = new JPanel(new FlowLayout());
		resetAndCloseButtonPanel.add(this.resetAndCloseButton);
		
		setLayout(new BorderLayout());
		add(p, BorderLayout.NORTH);
		add(resetAndCloseButtonPanel, BorderLayout.CENTER);
	}
	
	public JComboBox getMethodBox()
	{
		return this.methodBox;
	}
	public JTextField getCountField()
	{
		return this.countField;
	}
	
	public JTextField getPeriodField()
	{
		return this.periodField;
	}
	
	public JTextField getTSFormatField()
	{
		return this.tsFormatField;
	}
	public JCheckBox getExportStatusBox()
	{
		return this.exportStatusBox;
	}
	
	public JButton getResetAndCloseButton()
	{
		return this.resetAndCloseButton;
	}
}

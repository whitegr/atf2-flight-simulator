## Example vxWorks startup file

## The following is needed if your board support package doesn't at boot time
## automatically cd to the directory containing its startup script
cd "/home/tftp/ioc-linda/iocBoot/ioclinda"
#hostAdd("lindaHost","192.168.0.1")
#nfsMountAll("lindaHost")
< cdCommands-atf
putenv("EPICS_CA_MAX_ARRAY_BYTES=10000000")
putenv("NWFM=5000")
#putenv("EPICS_TS_NTP_INET=20.10.64.21")
#putenv("EPICS_CA_AUTO_ADDR_LIST=NO")
#putenv("EPICS_CA_SERVER_PORT=4400")
#putenv("EPICS_CAS_BEACON_ADDR_LIST","localhost")

# =========================================================
# Load Main EPICS Application and Libraries
# =========================================================
cd topbin

## You may have to change linda to something else
## everywhere it appears in this file
ld(0,0, "linda.munch")

# =========================================================
## Register all support components
# =========================================================
cd top
dbLoadDatabase("dbd/linda.dbd")
linda_registerRecordDeviceDriver(pdbbase)
# =========================================================
# ======= Application and Libraries loaded ================ 
# =========================================================


# ========================================================================
# Initialize Hardware
# ========================================================================
# Configure VME digitizers
#
#   devVmeDigiConfig(card_no, slot_no, vme_a32_addr, int_vec, int_lvl)
#
# card_no:
#     'unit number' associated with a particular card (1..8).
#
# slot_no:
#     VME slot the card is sitting in (1-based, i.e., a number from 1..21).
#
# vme_a32_addr:
#     Address in VME A32 space where card's sample memory is to be mapped.
#
# int_vec:
#     Unique VME interupt vector.
#
# int_lvl:
#     VME interrupt level (may be shared among cards).
#
# NOTE: 'slot_no' may be zero. In this case the VME bus is scanned
#       for instance 'card_no'. E.g., if digitizers are in slot 5 + 7
#       and 'card_no==2', 'slot_no==0' then the card in slot 7 is
#       be configured and associated with card/unit number '2'.
#
#       If both, 'card_no' and 'slot_no' are zero then the VME bus
#       is scanned and all cards that are found are configured and
#       assigned unique card numbers (from 1..N).
#       The sample memories of the cards are mapped at consecutive
#       blocks starting at vme_a32_addr. Interrupt vectors are assigned
#       starting with 'int_vec'. E.g., if 'int_vec==0xC0' then cards
#       1..3 will use vectors 0xC0, 0xC1 and 0xC2. The same 'int_lvl'
#       is shared by all cards.
#


# ==========================================
# Initialize V850 Digital Delay Generator
# ==========================================
# ==========================================
# Specify number of hiddg85x in the system:
# Also, specify the VME base A16 address
# Max number of cards can only be three
#
# hiddg85x_config(int ncards, int base_addr)
# ===========================================
#hiddg85x_config(1,0x0000)

# ======================================================================
# Specify more configuration:
# hiddg85x_config_card configures a single card.
# The arguments are: vlo, vhi for the low and high voltage values
# of the output signal (in Volts)
# trg_lvl is, well, the trigger level for the input signal.
# The defaults for these values you can look up in the manual.
#
# hiddg85x_config_card(int card, double vlo, double vhi, double trg_lvl)
# ========================================================================
#hiddg85x_config_card(1, 0.0, 5.0, 1.25)

# ========================================================================
# Initialize Hardware
# ========================================================================
# Configure VME digitizers
#
#   devVmeDigiConfig(card_no, slot_no, vme_a32_addr, int_vec, int_lvl)
#
# card_no:
#     'unit number' associated with a particular card (1..8).
#
# slot_no:
#     VME slot the card is sitting in (1-based, i.e., a number from 1..21).
#
# vme_a32_addr:
#     Address in VME A32 space where card's sample memory is to be mapped.
#
# int_vec:
#     Unique VME interupt vector.
#
# int_lvl:
#     VME interrupt level (may be shared among cards).
#
# NOTE: 'slot_no' may be zero. In this case the VME bus is scanned
#       for instance 'card_no'. E.g., if digitizers are in slot 5 + 7
#       and 'card_no==2', 'slot_no==0' then the card in slot 7 is
#       be configured and associated with card/unit number '2'.
#
#       If both, 'card_no' and 'slot_no' are zero then the VME bus
#       is scanned and all cards that are found are configured and
#       assigned unique card numbers (from 1..N).
#       The sample memories of the cards are mapped at consecutive
#       blocks starting at vme_a32_addr. Interrupt vectors are assigned
#       starting with 'int_vec'. E.g., if 'int_vec==0xC0' then cards
#       1..3 will use vectors 0xC0, 0xC1 and 0xC2. The same 'int_lvl'
#       is shared by all cards.
devVmeDigiConfig(1,12,0x27000000,0xC0,3)
devVmeDigiConfig(2,13,0x27040000,0xC1,3)
devVmeDigiConfig(3,14,0x27080000,0xC2,3)
devVmeDigiConfig(4,15,0x270C0000,0xC3,3)
devVmeDigiConfig(5,16,0x27140000,0xC4,3)
devVmeDigiConfig(6,17,0x27180000,0xC5,3)

# ==============================================================
# Load vmeDigi EPICS Database records. 
# ==============================================================
# This command is to be repeated
# for all cards. This associates a name (the value of
# the 'digi=<name>' assignment) with a particular card
# instance (number given in the config step above).
#
# E.g., if card #2 in slot 7 was configured as shown above
# then
#
#   dbLoadRecords("db/vmeDigiApp.db","digi=Hugo,card=2,nelm=4096")
#
# makes the PVs of the digitizer in slot 7 available as
#
#   Hugo:WAV, Hugo:CLK, Hugo:ARM etc.
#
# The value given to the 'nelm' parameter defines the size of
# the waveforms to be acquired (cannot be changed dynamically
# at run-time, sorry).
# 'nelm' defines the total number of samples and must be
# a multiple of four (the number of digitizer channels).
# ==========================================================================
dbLoadRecords("db/vmeDigiApp.db","digi=slacvme1:sdigi1,card=1,nelm=4096")
dbLoadRecords("db/vmeDigiApp.db","digi=slacvme1:sdigi2,card=2,nelm=4096")
dbLoadRecords("db/vmeDigiApp.db","digi=slacvme1:sdigi3,card=3,nelm=4096")
dbLoadRecords("db/vmeDigiApp.db","digi=slacvme1:sdigi4,card=4,nelm=4096")
dbLoadRecords("db/vmeDigiApp.db","digi=slacvme1:sdigi5,card=5,nelm=4096")
dbLoadRecords("db/vmeDigiApp.db","digi=slacvme1:sdigi6,card=6,nelm=4096")

# ===============================================================================================
# Setup/Initialize GTR SIS3301 Fast ADC:
# Better Known as a waveform digitizer
# ===============================================================================================
# The SIS3301 sits in A32 VME Address Space (Don't overlap with Cavity BPM)
# for SIS3301-80, clock is 80 MHz
# sisfadcConfig(int card,int clockSpeed, unsigned int a32offset,int intVec,int intLev, int useDma)
sisfadcConfig(1,105,0x20000000,0xcc,4,0)
sisfadcConfig(2,105,0x21000000,0xcd,4,0)
sisfadcConfig(3,105,0x22000000,0xce,4,0)
sisfadcConfig(4,105,0x23000000,0xcf,4,0)
sisfadcConfig(5,105,0x24000000,0xd0,4,0)
sisfadcConfig(6,105,0x25000000,0xd1,4,0)
sisfadcConfig(7,105,0x26000000,0xd2,4,0)
dbLoadRecords("db/gtr.db","name=slacvme1:sis1:,card=1,size=3000")
dbLoadRecords("db/gtr.db","name=slacvme1:sis2:,card=2,size=3000")
dbLoadRecords("db/gtr.db","name=slacvme1:sis3:,card=3,size=3000")
dbLoadRecords("db/gtr.db","name=slacvme1:sis4:,card=4,size=3000")
dbLoadRecords("db/gtr.db","name=slacvme1:sis5:,card=5,size=3000")
dbLoadRecords("db/gtr.db","name=slacvme1:sis6:,card=6,size=3000")
dbLoadRecords("db/gtr.db","name=slacvme1:sis7:,card=7,size=3000")

#====================================
# BPM records
#====================================
#dbLoadRecords("db/bpm.vdb","bpmname=MQF1X,wfsize=3000,card=1,lwf=0,rwf=1,uwf=2,dwf=3,anal=SDEV")
#dbLoadRecords("db/bpm.vdb","bpmname=MQD2X,wfsize=3000,card=1,lwf=4,rwf=5,uwf=6,dwf=7,anal=SDEV")
#dbLoadRecords("db/bpm.vdb","bpmname=MQF3X,wfsize=3000,card=2,lwf=0,rwf=1,uwf=2,dwf=3,anal=SDEV")
#dbLoadRecords("db/bpm.vdb","bpmname=MQF4X,wfsize=3000,card=2,lwf=4,rwf=5,uwf=6,dwf=7,anal=SDEV")
#dbLoadRecords("db/bpm.vdb","bpmname=MQD5X,wfsize=3000,card=3,lwf=0,rwf=1,uwf=2,dwf=3,anal=SDEV")
#dbLoadRecords("db/bpm.vdb","bpmname=MQF6X,wfsize=3000,card=3,lwf=4,rwf=5,uwf=6,dwf=7,anal=SDEV")
#dbLoadRecords("db/bpm.vdb","bpmname=MQF7X,wfsize=3000,card=4,lwf=0,rwf=1,uwf=2,dwf=3,anal=SDEV")
#dbLoadRecords("db/bpm.vdb","bpmname=MQD8X,wfsize=3000,card=4,lwf=4,rwf=5,uwf=6,dwf=7,anal=SDEV")
#dbLoadRecords("db/bpm.vdb","bpmname=MQF9X,wfsize=3000,card=5,lwf=0,rwf=1,uwf=2,dwf=3,anal=SDEV")
#dbLoadRecords("db/bpm.vdb","bpmname=MQF13X,wfsize=3000,card=5,lwf=4,rwf=5,uwf=6,dwf=7,anal=SDEV")
#dbLoadRecords("db/bpm.vdb","bpmname=MQD14X,wfsize=3000,card=6,lwf=0,rwf=1,uwf=2,dwf=3,anal=SDEV")
#dbLoadRecords("db/bpm.vdb","bpmname=MQF15X,wfsize=3000,card=6,lwf=4,rwf=5,uwf=6,dwf=7,anal=SDEV")
#dbLoadRecords("db/bpm.vdb","bpmname=MFB1FF,wfsize=3000,card=7,lwf=0,rwf=1,uwf=2,dwf=3,anal=SDEV")
< iocBoot/ioclinda/extwf

# ========================================================================
# All Hardware Initialization is done now 
# ========================================================================

# ==============================================================
# Let's load some EPICS Databases
# ==============================================================
# ==============================================================
# Load vmeDigi EPICS Database records. 
# ==============================================================
# This command is to be repeated
# for all cards. This associates a name (the value of
# the 'digi=<name>' assignment) with a particular card
# instance (number given in the config step above).
#
# E.g., if card #2 in slot 7 was configured as shown above
# then
#
#   dbLoadRecords("db/vmeDigiApp.db","digi=Hugo,card=2,nelm=4096")
#
# makes the PVs of the digitizer in slot 7 available as
#
#   Hugo:WAV, Hugo:CLK, Hugo:ARM etc.
#
# The value given to the 'nelm' parameter defines the size of
# the waveforms to be acquired (cannot be changed dynamically
# at run-time, sorry).
# 'nelm' defines the total number of samples and must be
# a multiple of four (the number of digitizer channels).
# ==========================================================================
#dbLoadRecords("db/vmeDigiApp.db","digi=slacvme1:sdigi1,card=1,nelm=4096")

# ==============================================================
# Load Database Records for the Digital Delay Generator:
# ==============================================================
#dbLoadRecords("db/DDG_4Chan.db", "NAME=slacvme1:delay1,CARD=0")

# ==========================================================================
# Start EPICS Kernel
# ==========================================================================
cd startup
iocInit

dbpf("slacvme1:sdigi1:ARM","2")
dbpf("slacvme1:sdigi2:ARM","2")
dbpf("slacvme1:sdigi3:ARM","2")
dbpf("slacvme1:sdigi4:ARM","2")
dbpf("slacvme1:sdigi5:ARM","2")
dbpf("slacvme1:sdigi6:ARM","2")

# Dump db records into record file
dbl > dbList.txt

# ==================================================================================
## vxWorks StartUp File
====================================================

< cdCommands

# =========================================================
# Load Main EPICS Application and Libraries
# =========================================================
cd topbin

## You may have to change linda to something else
## everywhere it appears in this file
ld(0,0, "linda.munch")

# =========================================================
## Register all support components
# =========================================================
cd top
dbLoadDatabase("dbd/linda.dbd")
linda_registerRecordDeviceDriver(pdbbase)
# =========================================================
# ======= Application and Libraries loaded ================ 
# =========================================================


# ========================================================================
# Initialize Hardware
# ========================================================================
# Configure VME digitizers
#
#   devVmeDigiConfig(card_no, slot_no, vme_a32_addr, int_vec, int_lvl)
#
# card_no:
#     'unit number' associated with a particular card (1..8).
#
# slot_no:
#     VME slot the card is sitting in (1-based, i.e., a number from 1..21).
#
# vme_a32_addr:
#     Address in VME A32 space where card's sample memory is to be mapped.
#
# int_vec:
#     Unique VME interupt vector.
#
# int_lvl:
#     VME interrupt level (may be shared among cards).
#
# NOTE: 'slot_no' may be zero. In this case the VME bus is scanned
#       for instance 'card_no'. E.g., if digitizers are in slot 5 + 7
#       and 'card_no==2', 'slot_no==0' then the card in slot 7 is
#       be configured and associated with card/unit number '2'.
#
#       If both, 'card_no' and 'slot_no' are zero then the VME bus
#       is scanned and all cards that are found are configured and
#       assigned unique card numbers (from 1..N).
#       The sample memories of the cards are mapped at consecutive
#       blocks starting at vme_a32_addr. Interrupt vectors are assigned
#       starting with 'int_vec'. E.g., if 'int_vec==0xC0' then cards
#       1..3 will use vectors 0xC0, 0xC1 and 0xC2. The same 'int_lvl'
#       is shared by all cards.
#

devVmeDigiConfig(1,4,0x20000000,0xC0,3)
devVmeDigiConfig(2,5,0x20040000,0xC2,3)
devVmeDigiConfig(3,6,0x20080000,0xC4,3)
devVmeDigiConfig(4,7,0x200C0000,0xC6,3)
devVmeDigiConfig(5,8,0x20140000,0xC8,3)
devVmeDigiConfig(6,9, 0x20180000,0xCa,3)

# ==========================================
# Initialize V850 Digital Delay Generator
# ==========================================
# ==========================================
# Specify number of hiddg85x in the system:
# Also, specify the VME base A16 address
# Max number of cards can only be three
#
# hiddg85x_config(int ncards, int base_addr)
# ===========================================
hiddg85x_config(1,0xc000)

# ======================================================================
# Specify more configuration:
# hiddg85x_config_card configures a single card.
# The arguments are: vlo, vhi for the low and high voltage values
# of the output signal (in Volts)
# trg_lvl is, well, the trigger level for the input signal.
# The defaults for these values you can look up in the manual.
#
# hiddg85x_config_card(int card, double vlo, double vhi, double trg_lvl)
# ========================================================================
hiddg85x_config_card(1, 0.0, 5.0, 1.25)

# ===============================================================================================
# Setup/Initialize GTR SIS3301 Fast ADC:
# Better Known as a waveform digitizer
# ===============================================================================================
# The SIS3301 sits in A32 VME Address Space (Don't overlap with Cavity BPM)
# for SIS3301-80, clock is 80 MHz
# sisfadcConfig(int card,int clockSpeed, unsigned int a32offset,int intVec,int intLev, int useDma)
sisfadcConfig( 1,100,0x21000000,0xCC,4,0 )

# ===============================================================================================
# VMIC3122 Initialization
# ===============================================================================================
# drvVmic3122Init(card number, base address, interrupt vector, interrupt level, scan rate in Hz)
# Note: the base address must be set by jumpers E5,E6,E7
# Eg. 0x10000 = A16 out
# Eg. 0x30000 = A12 and A16 out

# Windriver vxWorks does not to pass floating point arguments
# via the target shell. Register it with iocsh and invoke with iocshCmd
# Note that args now look like command line args, not subroutine CSVs
#iocshCmd("drvVmic3122Init 0 0x30000 0xCC 3 400.0")

# Let's go for 100 kHz (IOC is CPU Bound when using 100 kHz)
iocshCmd("drvVmic3122Init 0 0x30000 0xCC 3 100000.0")

# ========================================================================
# All Hardware Initialization is done now 
# ========================================================================

# ==============================================================
# Let's load some EPICS Databases
# ==============================================================
# ==============================================================
# Load vmeDigi EPICS Database records. 
# ==============================================================
# This command is to be repeated
# for all cards. This associates a name (the value of
# the 'digi=<name>' assignment) with a particular card
# instance (number given in the config step above).
#
# E.g., if card #2 in slot 7 was configured as shown above
# then
#
#   dbLoadRecords("db/vmeDigiApp.db","digi=Hugo,card=2,nelm=4096")
#
# makes the PVs of the digitizer in slot 7 available as
#
#   Hugo:WAV, Hugo:CLK, Hugo:ARM etc.
#
# The value given to the 'nelm' parameter defines the size of
# the waveforms to be acquired (cannot be changed dynamically
# at run-time, sorry).
# 'nelm' defines the total number of samples and must be
# a multiple of four (the number of digitizer channels).
# ==========================================================================
dbLoadRecords("db/vmeDigiApp.db","digi=BPMS:B34:ATF2-CD01,card=1,nelm=4096")
dbLoadRecords("db/vmeDigiApp.db","digi=BPMS:B34:ATF2-CD02,card=2,nelm=4096")
dbLoadRecords("db/vmeDigiApp.db","digi=BPMS:B34:ATF2-CD03,card=3,nelm=4096")
dbLoadRecords("db/vmeDigiApp.db","digi=BPMS:B34:ATF2-CD04,card=4,nelm=4096")
dbLoadRecords("db/vmeDigiApp.db","digi=BPMS:B34:ATF2-CD05,card=5,nelm=4096")
dbLoadRecords("db/vmeDigiApp.db","digi=BPMS:B34:ATF2-CD06,card=6,nelm=4096")

# ==============================================================
# Load Database Records for the Digital Delay Generator:
# ==============================================================
dbLoadRecords("db/DDG_4Chan.db", "NAME=DDG1:ATF,CARD=0")

# =======================================================================================
# Load Database Records for the  v3122
# =======================================================================================
dbLoadRecords("db/vmic3122_signals.db")



# ==========================================================================
# Start EPICS Kernel
# ==========================================================================
cd startup
iocInit

## Start any sequence programs
#seq &sncExample, "user=cavityBPM_CD01"

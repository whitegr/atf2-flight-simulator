## Example vxWorks startup file

nfsServer="whitegr-laptop"
netDevHandle=iosDevFind( "whitegr-laptop:")
#nfsAuthUnixSet(nfsServer, 1000, 1000, 0)
putenv("EPICS_TS_NTP_INET=192.168.0.10")

## The following is needed if your board support package doesn't at boot time
## automatically cd to the directory containing its startup script
cd "/home/whitegr/ATF2/control-software/epics-3.14.10/ioc-linda/iocBoot/ioclinda"

< cdCommands
#< ../nfsCommands

cd topbin
## You may have to change linda to something else
## everywhere it appears in this file

ld < linda.munch

## This drvTS initializer is needed if the IOC has a hardware event system
#TSinit

## Register all support components
cd top
dbLoadDatabase("dbd/linda.dbd",0,0)
linda_registerRecordDeviceDriver(pdbbase)

## Load record instances
dbLoadTemplate "db/user.substitutions"
#dbLoadRecords("db/dbSubExample.db","user=whitegr")
dbLoadRecords("db/motor.db")

# OMS58 driver setup
oms58Setup(2,0x3000,64,5,10)

## Set this to see messages from mySub
#mySubDebug = 1

cd startup
iocInit()

## Start any sequence programs
#seq &sncExample,"user=whitegr"

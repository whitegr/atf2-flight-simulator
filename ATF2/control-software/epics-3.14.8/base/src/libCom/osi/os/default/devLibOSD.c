/*************************************************************************\
* Copyright (c) 2006 The University of Chicago, as Operator of Argonne
*     National Laboratory.
* EPICS BASE Versions 3.13.7
* and higher are distributed subject to a Software License Agreement found
* in file LICENSE that is included with this distribution.
\*************************************************************************/

/* devLibOSD.c,v 1.1.2.1 2006/02/17 22:51:24 anj Exp */

#include <stdlib.h>

#include "devLib.h"

/* This file must contain no definitions other than the following: */

devLibVirtualOS *pdevLibVirtualOS;

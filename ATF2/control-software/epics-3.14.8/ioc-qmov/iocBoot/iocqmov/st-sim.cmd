#!../../bin/linux-x86_64/qmov

< envPaths

epicsEnvSet("EPICS_CA_AUTO_ADDR_LIST","NO")
epicsEnvSet("EPICS_CA_SERVER_PORT","${FS_PORT}")
epicsEnvSet("EPICS_CAS_BEACON_ADDR_LIST","localhost")

cd ${TOP}

## Register all support components
dbLoadDatabase("dbd/qmov.dbd")
qmov_registerRecordDeviceDriver(pdbbase)

## Number of motors installed in MMC (number of quad movers *3)
epicsEnvSet(QMOV_NMOTORS,"90")
epicsEnvSet(SIMMODE,"1")

## Initialise drivers (change qmovApp/src/qmoverDriver.c-camac_only to .c extension
## when deploying for real on CAMAC controller
## - These for actual deployment on CAMAC controller (comment out for elsewhere)
#qmoverDriverInit("qmovWrite",-32768,32767,${QMOV_NMOTORS})
#qmoverDriverInit("qmovReadStepsRemain",-32768,32767,${QMOV_NMOTORS})
#qmoverDriverInit("qmovReadPot",-32768,32767,${QMOV_NMOTORS})
#qmoverDriverInit("qmovReadLVDT",-32768,32767,${QMOV_NMOTORS})
## - These for testing/simulation elsewhere
#motorDriverInit("qmovWrite",-32768,32767)
#motorDriverInit("qmovReadStepsRemain",-32768,32767)
#motorDriverInit("qmovReadPot",-32768,32767)
#motorDriverInit("qmovReadLVDT",-32768,32767)

## Load record instances
## - 3 motors per quad mover (starting with addr1=1)
dbLoadRecords("db/dbQmovCommand.vdb","user=c1,T=0,SIMMODE=${SIMMODE}")
dbLoadRecords("db/dbQmov.vdb","user=c1,addr1=0,addr2=1,addr3=2,nm=1,T=0")
dbLoadRecords("db/dbQmov.vdb","user=c1,addr1=3,addr2=4,addr3=5,nm=2,T=0")
dbLoadRecords("db/dbQmov.vdb","user=c1,addr1=6,addr2=7,addr3=8,nm=3,T=0")
dbLoadRecords("db/dbQmov.vdb","user=c1,addr1=9,addr2=10,addr3=11,nm=4,T=0")
dbLoadRecords("db/dbQmov.vdb","user=c1,addr1=12,addr2=13,addr3=14,nm=5,T=0")
dbLoadRecords("db/dbQmov.vdb","user=c1,addr1=15,addr2=16,addr3=17,nm=6,T=0")
dbLoadRecords("db/dbQmov.vdb","user=c1,addr1=18,addr2=19,addr3=20,nm=7,T=0")
dbLoadRecords("db/dbQmov.vdb","user=c1,addr1=21,addr2=22,addr3=23,nm=8,T=0")
dbLoadRecords("db/dbQmov.vdb","user=c1,addr1=24,addr2=25,addr3=26,nm=9,T=0")
dbLoadRecords("db/dbQmov.vdb","user=c1,addr1=27,addr2=28,addr3=29,nm=10,T=0")
dbLoadRecords("db/dbQmov.vdb","user=c1,addr1=30,addr2=31,addr3=32,nm=11,T=0")
dbLoadRecords("db/dbQmov.vdb","user=c1,addr1=33,addr2=34,addr3=35,nm=12,T=0")
dbLoadRecords("db/dbQmov.vdb","user=c1,addr1=36,addr2=37,addr3=38,nm=13,T=0")
dbLoadRecords("db/dbQmov.vdb","user=c1,addr1=39,addr2=40,addr3=41,nm=14,T=0")
dbLoadRecords("db/dbQmov.vdb","user=c1,addr1=42,addr2=43,addr3=44,nm=15,T=0")
dbLoadRecords("db/dbQmov.vdb","user=c1,addr1=45,addr2=46,addr3=47,nm=16,T=0")
dbLoadRecords("db/dbQmov.vdb","user=c1,addr1=48,addr2=49,addr3=50,nm=17,T=0")
dbLoadRecords("db/dbQmov.vdb","user=c1,addr1=51,addr2=52,addr3=53,nm=18,T=0")
dbLoadRecords("db/dbQmov.vdb","user=c1,addr1=54,addr2=55,addr3=56,nm=19,T=0")
dbLoadRecords("db/dbQmov.vdb","user=c1,addr1=57,addr2=58,addr3=59,nm=20,T=0")
dbLoadRecords("db/dbQmov.vdb","user=c1,addr1=60,addr2=61,addr3=62,nm=21,T=0")
dbLoadRecords("db/dbQmov.vdb","user=c1,addr1=63,addr2=64,addr3=65,nm=22,T=0")
dbLoadRecords("db/dbQmov.vdb","user=c1,addr1=66,addr2=67,addr3=68,nm=23,T=0")
dbLoadRecords("db/dbQmov.vdb","user=c1,addr1=69,addr2=70,addr3=71,nm=24,T=0")
dbLoadRecords("db/dbQmov.vdb","user=c1,addr1=72,addr2=73,addr3=74,nm=25,T=0")
dbLoadRecords("db/dbQmov.vdb","user=c1,addr1=75,addr2=76,addr3=77,nm=26,T=0")
dbLoadRecords("db/dbQmov.vdb","user=c1,addr1=78,addr2=79,addr3=80,nm=27,T=0")
dbLoadRecords("db/dbQmov.vdb","user=c1,addr1=81,addr2=82,addr3=83,nm=28,T=0")
dbLoadRecords("db/dbQmovConstants.db","cr=c1")

# Access Security
#asSetFilename("${TOP}/../AccessSecurity.acf")

cd ${TOP}/iocBoot/${IOC}
## Debug info
#asynSetTraceMask("qmovWrite",0,0xff)
#asynSetTraceIOMask("qmovWrite",0,0x2)
iocInit()

# Dump db records into record file
dbl > dbList.txt

## Start any sequence programs
#seq sncExample,"user=whitegrHost"
## Start any sequence programs
seq trim,"CC=1,MM=1,name=1-1"
seq trim,"CC=1,MM=2,name=1-2"
seq trim,"CC=1,MM=3,name=1-3"
seq trim,"CC=1,MM=4,name=1-4"
seq trim,"CC=1,MM=5,name=1-5"
seq trim,"CC=1,MM=6,name=1-6"
seq trim,"CC=1,MM=7,name=1-7"
seq trim,"CC=1,MM=8,name=1-8"
seq trim,"CC=1,MM=9,name=1-9"
seq trim,"CC=1,MM=10,name=1-10"
seq trim,"CC=1,MM=11,name=1-11"
seq trim,"CC=1,MM=12,name=1-12"
seq trim,"CC=1,MM=13,name=1-13"
seq trim,"CC=1,MM=14,name=1-14"
seq trim,"CC=1,MM=15,name=1-15"
seq trim,"CC=1,MM=16,name=1-16"
seq trim,"CC=1,MM=17,name=1-17"
seq trim,"CC=1,MM=18,name=1-18"
seq trim,"CC=1,MM=19,name=1-19"
seq trim,"CC=1,MM=20,name=1-20"
seq trim,"CC=1,MM=21,name=1-21"
seq trim,"CC=1,MM=22,name=1-22"
seq trim,"CC=1,MM=23,name=1-23"
seq trim,"CC=1,MM=24,name=1-24"
seq trim,"CC=1,MM=25,name=1-25"
seq trim,"CC=1,MM=26,name=1-26"
seq trim,"CC=1,MM=27,name=1-27"
seq trim,"CC=1,MM=28,name=1-28"

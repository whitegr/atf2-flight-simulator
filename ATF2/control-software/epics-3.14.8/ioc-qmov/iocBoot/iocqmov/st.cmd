#!/home/toyo/epics-3.14.8/ioc-qmov/bin/linux-x86/qmov
#
# 110210 JLN add qmov_sam variable - for 90 motors + 32 diag sam channels
# 081031 JLN add loadrecords for motor 28, qd0
#
< /home/toyo/epics-3.14.8/ioc-qmov/iocBoot/iocqmov/envPaths

cd ${TOP}

## Register all support components
dbLoadDatabase("dbd/qmov.dbd")
qmov_registerRecordDeviceDriver(pdbbase)

## Number of motors installed in MMC (number of quad movers *3)
epicsEnvSet(QMOV_NMOTORS,"90")
epicsEnvSet(SIMMODE,"0")
## 90 motors + 32 spare sam channels
epicsEnvSet(QMOV_SAMS,"122")

## Initialise drivers (change qmovApp/src/qmoverDriver.c-camac_only to .c extension
## when deploying for real on CAMAC controller
## - These for actual deployment on CAMAC controller (comment out for elsewhere)
qmoverDriverInit("qmovWrite",-32768,32767,${QMOV_NMOTORS})
qmoverDriverInit("qmovReadStepsRemain",-32768,32767,${QMOV_NMOTORS})
qmoverDriverInit("qmovReadSams",-32768,32767,$(QMOV_SAMS))
qmoverDriverInit("qmovReadLvdts",-32768,32767,$(QMOV_NMOTORS))
## Added this line 090120 JLN
qmoverDriverInit("qmovResetSams",-32768,32767,$(QMOV_NMOTORS))
## - These for testing/simulation elsewhere
#motorDriverInit("qmovWrite",-32768,32767)
#motorDriverInit("qmovReadStepsRemain",-32768,32767)
#motorDriverInit("qmovReadPot",-32768,32767)
#motorDriverInit("qmovReadLVDT",-32768,32767)

## Load record instances
## - 3 motors per quad mover (starting with addr1=1)
dbLoadTemplate("db/dbQmov.template")
dbLoadRecords("db/dbQmovConstants.db","cr=c1")
dbLoadRecords("db/dbQmovCommand.vdb","user=c1,nm=1,T=0,SIMMODE=${SIMMODE}")
dbLoadRecords("db/qmov.db","user=c1,nchan=1000")

# Access Security
#asSetFilename("${TOP}/../AccessSecurity.acf")

cd ${TOP}/iocBoot/${IOC}
## Debug info
asynSetTraceMask("qmovWrite",0,0xff)
asynSetTraceIOMask("qmovWrite",0,0x2)
iocInit()

# Dump db records into record file
dbl > dbList.txt


#dbpf("c1:qmov:m11:subVals.INPG","c1:qmov:m11:motor1:indians PP NMS")
#dbpf("c1:qmov:m11:subVals.INPH","c1:qmov:m11:motor2:indians PP NMS")
#dbpf("c1:qmov:m11:subVals.INPI","c1:qmov:m11:motor3:indians PP NMS")
dbpf("c1:qmov:m11:potxps.VAL",1)
dbpf("c1:qmov:m11:ntry.VAL",1)
#dbpf("c1:qmov:m17:subVals.INPG","c1:qmov:m17:motor1:indians PP NMS")
#dbpf("c1:qmov:m17:subVals.INPH","c1:qmov:m17:motor2:indians PP NMS")
#dbpf("c1:qmov:m17:subVals.INPI","c1:qmov:m17:motor3:indians PP NMS")
dbpf("c1:qmov:m17:potxps.VAL",1)
dbpf("c1:qmov:m17:ntry.VAL",1)
#dbpf("c1:qmov:m20:subVals.INPG","c1:qmov:m20:motor1:indians PP NMS")
#dbpf("c1:qmov:m20:subVals.INPH","c1:qmov:m20:motor2:indians PP NMS")
#dbpf("c1:qmov:m20:subVals.INPI","c1:qmov:m20:motor3:indians PP NMS")
dbpf("c1:qmov:m20:potxps.VAL",1)
dbpf("c1:qmov:m20:ntry.VAL",1)
#dbpf("c1:qmov:m25:subVals.INPG","c1:qmov:m25:motor1:indians PP NMS")
#dbpf("c1:qmov:m25:subVals.INPH","c1:qmov:m25:motor2:indians PP NMS")
#dbpf("c1:qmov:m25:subVals.INPI","c1:qmov:m25:motor3:indians PP NMS")
dbpf("c1:qmov:m25:potxps.VAL",1)
dbpf("c1:qmov:m25:ntry.VAL",1)
#dbpf("c1:qmov:m27:subVals.INPG","c1:qmov:m27:motor1:indians PP NMS")
#dbpf("c1:qmov:m27:subVals.INPH","c1:qmov:m27:motor2:indians PP NMS")
#dbpf("c1:qmov:m27:subVals.INPI","c1:qmov:m27:motor3:indians PP NMS")
dbpf("c1:qmov:m27:potxps.VAL",1)
dbpf("c1:qmov:m27:ntry.VAL",1)
#dbpf("c1:qmov:m28:subVals.INPG","c1:qmov:m28:motor1:indians PP NMS")
#dbpf("c1:qmov:m28:subVals.INPH","c1:qmov:m28:motor2:indians PP NMS")
#dbpf("c1:qmov:m28:subVals.INPI","c1:qmov:m28:motor3:indians PP NMS")
dbpf("c1:qmov:m28:potxps.VAL",1)
dbpf("c1:qmov:m28:ntry.VAL",1)

## Start any sequence programs
seq trim,"CC=1,MM=1,name=1-1"
seq trim,"CC=1,MM=2,name=1-2"
seq trim,"CC=1,MM=3,name=1-3"
seq trim,"CC=1,MM=4,name=1-4"
seq trim,"CC=1,MM=5,name=1-5"
seq trim,"CC=1,MM=6,name=1-6"
seq trim,"CC=1,MM=7,name=1-7"
seq trim,"CC=1,MM=8,name=1-8"
seq trim,"CC=1,MM=9,name=1-9"
seq trim,"CC=1,MM=10,name=1-10"
seq trim,"CC=1,MM=11,name=1-11"
seq trim,"CC=1,MM=12,name=1-12"
seq trim,"CC=1,MM=13,name=1-13"
seq trim,"CC=1,MM=14,name=1-14"
seq trim,"CC=1,MM=15,name=1-15"
seq trim,"CC=1,MM=16,name=1-16"
seq trim,"CC=1,MM=17,name=1-17"
seq trim,"CC=1,MM=18,name=1-18"
seq trim,"CC=1,MM=19,name=1-19"
seq trim,"CC=1,MM=20,name=1-20"
seq trim,"CC=1,MM=21,name=1-21"
seq trim,"CC=1,MM=22,name=1-22"
seq trim,"CC=1,MM=23,name=1-23"
seq trim,"CC=1,MM=24,name=1-24"
seq trim,"CC=1,MM=25,name=1-25"
seq trim,"CC=1,MM=26,name=1-26"
seq trim,"CC=1,MM=27,name=1-27"
seq trim,"CC=1,MM=28,name=1-28"
seq trim,"CC=1,MM=29,name=1-29"

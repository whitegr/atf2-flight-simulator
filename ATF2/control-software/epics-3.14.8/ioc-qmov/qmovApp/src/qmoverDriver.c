/* qmoverDriver.c */
/* Glen White, Apr 12 2006 */
/* CAMAC based driver for ATF2 Quadrupole mover system */
/* Requires KEK/TOYO Pipeline CAMAC controller with PC/104 cpu */
/* uses asynInt32 and asynFloat64 interfaces, requiring the ASYN package */
/* Motor drive provided by SLAC MMC CAMAC module, pot and LVDT readback through SLAC SAM module */
/* Required asyn port names: qmovWrite (asynInt32) for move commands
 *                           qmovReadStepsRemain (asynInt32) to get remaining steps
 *                           qmovReadPot (asynFloat64) to read back motor pot's
 *                           qmovReadLVDT (asynFloat64) to read back LVDT's */
/* Driver makes use of controller pipeline communication; addr values 1-N provide access to motors,
 * addr 0 commands write/read operation */
/* */
/* 090702 JLN - add out of tolerance checks to readsam */
/* Big change: */
/* get rid of qmovReadPot and qmovReadLVDT and make readSams and readLvdts instead */
/* float64arrays instead of float64s */
/* 081114 JLN - though Glen did all of the hard work.*/
/* */

/* ------ Original Template from Asyn ------ */
/*asynInt32Driver.c */
/***********************************************************************
* Copyright (c) 2002 The University of Chicago, as Operator of Argonne
* National Laboratory, and the Regents of the University of
* California, as Operator of Los Alamos National Laboratory, and
* Berliner Elektronenspeicherring-Gesellschaft m.b.H. (BESSY).
* asynDriver is distributed subject to a Software License Agreement
* found in file LICENSE that is included with this distribution.
***********************************************************************/
/* Author: Marty Kraimer */

#include <stddef.h>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <errno.h>
#include <ctype.h>
#include <math.h>
#include <time.h>

#include <cantProceed.h>
#include <epicsStdio.h>
#include <epicsMutex.h>
#include <epicsEvent.h>
#include <epicsThread.h>
#include <iocsh.h>

#include <dbDefs.h>
#include <dbAccess.h>
#include <dbFldTypes.h>

#include <asynDriver.h>
#include <asynDrvUser.h>
#include <asynInt32.h>
#include <asynFloat64Array.h>
#include <epicsExport.h>
#include <epicsTime.h>
/* Max number of motors dealt with by this driver */
#define NCHANNELS 1000
#define DEBUG 0 /* 1 = Print goofy messages */
#define VERBOSE 0 /*1 = print LOTS of goofy messages */
#define READSAM 800000 /* pause in us between sam reads */
#define DEBSAM 0 /* debug messages for readSAM*/
#define DEBSAMS 1 /* debug messages for readSAM*/
#define DEBOOT 1 /* debug messages for out of tol stuff */

/* header for KEK/TOYO Pipeline CAMAC controller with PC/104 cpu */
#include <pcc.h>
#include <unistd.h> /* for usleep compliments of JEM */

typedef struct chanPvt {
    epicsInt32 value;
    void       *asynPvt;
    int        N;
    int        A;
    int        F;
}chanPvt;

typedef struct drvPvt {
    const char    *portName;
    epicsMutexId  lock;
    epicsEventId  waitWork;
    int           connected;
    double        interruptDelay;
    asynInterface common;
    asynInterface asynDrvUser;
    asynInterface asynInt32;
    asynInterface asynFloat64Array;
    epicsInt32    low;
    epicsInt32    high;
    void          *asynInt32Pvt; /* For registerInterruptSource*/
    void          *asynFloat64ArrayPvt; /* For registerInterruptSource*/
    chanPvt       channel[NCHANNELS];
    int           *camBuf;
    int           nChan;
    int           camFD;
    epicsInt32    intData[NCHANNELS];
}drvPvt;

static int qmoverDriverInit(const char *dn,int low,int high,int nChan);
static asynStatus getAddr(drvPvt *pdrvPvt,asynUser *pasynUser,
    int *paddr,int portOK);

/* asynCommon methods */
static void report(void *drvPvt,FILE *fp,int details);
static asynStatus connect(void *drvPvt,asynUser *pasynUser);
static asynStatus disconnect(void *drvPvt,asynUser *pasynUser);
static asynCommon common = { report, connect, disconnect };

/* asynDrvUser */
static asynStatus create(void *drvPvt,asynUser *pasynUser,
    const char *drvInfo, const char **pptypeName,size_t *psize);
static asynStatus getType(void *drvPvt,asynUser *pasynUser,
    const char **pptypeName,size_t *psize);
static asynStatus destroy(void *drvPvt,asynUser *pasynUser);
static asynDrvUser drvUser = {create,getType,destroy};

/* asynInt32 methods */
static asynStatus int32Write(void *drvPvt,asynUser *pasynUser,epicsInt32 value);
static asynStatus int32Read(void *drvPvt,asynUser *pasynUser,epicsInt32 *value);
static asynStatus int32GetBounds(void *drvPvt, asynUser *pasynUser,
                                epicsInt32 *low, epicsInt32 *high);

/* asynFloat64Array methods */
static asynStatus float64ArrayRead(void *drvPvt,asynUser *pasynUser,
  epicsFloat64 *avalue, size_t nelements, size_t *nIn);

/* CAMAC functions */
int cam_gen_init( int length, int* buf );
int cam_gen_cc( int *buf, int n, int a, int f, int data );
int cam_open( void );
int cam_close( int fd );
int cam_reset( int fd );
int cam_clear_fifo( int fd );
int cam_exec( int fd, int* cmdbuf, int* rplybuf );
int cam_exec_pio( int fd, int* cmdbuf, int* rplybuf );
int cam_exec_dma( int fd, int* cmdbuf, int* rplybuf );
int cam_extract_cc_data( int* rplybuf, int len, int* actuallen, int* data );
int cam_single_cc( int fd, int n, int a, int f, int *data, int *q, int *x);

static int qmoverDriverInit(const char *dn,int low,int high,int nChan)
{
    drvPvt    *pdrvPvt;
    char       *portName;
    asynStatus status;
    int        nbytes;
    int        addr, camStat, cc, nCards, iCard, funcList[3];
    int        *camBuf;

    asynInt32  *pasynInt32;
    asynFloat64Array *pasynFloat64Array;
    
    nbytes = sizeof(drvPvt) + sizeof(asynInt32) + sizeof(asynFloat64Array);
    nbytes += strlen(dn) + 1 + sizeof(camBuf);
    pdrvPvt = callocMustSucceed(nbytes,sizeof(char),"qmoverDriverInit");
    pasynInt32 = (asynInt32 *)(pdrvPvt + 1);
    pasynFloat64Array = (asynFloat64Array *)(pasynInt32 + 1);
    portName = (char *)(pasynFloat64Array + 1);
    strcpy(portName,dn);
    camBuf = (int *)(portName + 1 + strlen(portName));
    pdrvPvt->nChan = nChan; /* Number of motors installed */
    pdrvPvt->portName = portName;
    pdrvPvt->camBuf = camBuf;
    pdrvPvt->lock = epicsMutexMustCreate();
    pdrvPvt->waitWork = epicsEventCreate(epicsEventEmpty);
    pdrvPvt->common.interfaceType = asynCommonType;
    pdrvPvt->common.pinterface  = (void *)&common;
    pdrvPvt->common.drvPvt = pdrvPvt;
    pdrvPvt->asynDrvUser.interfaceType = asynDrvUserType;
    pdrvPvt->asynDrvUser.pinterface = (void *)&drvUser;
    pdrvPvt->asynDrvUser.drvPvt = pdrvPvt;
    pdrvPvt->low = low;
    pdrvPvt->high = high;
    status = pasynManager->registerPort(portName,ASYN_MULTIDEVICE,1,0,0);
    if(status!=asynSuccess) {
        printf("qmoverDriverInit registerDriver failed\n");
        return 0;
    }
    status = pasynManager->registerInterface(portName,&pdrvPvt->common);
    if(status!=asynSuccess){
        printf("qmoverDriverInit registerInterface common failed\n");
        return 0;
    }
    status = pasynManager->registerInterface(portName,&pdrvPvt->asynDrvUser);
    if(status!=asynSuccess){
        printf("qmoverDriverInit registerInterface drvuser failed\n");
        return 0;
    }
    pasynInt32->write = int32Write;
    pasynInt32->read = int32Read;
    pasynInt32->getBounds = int32GetBounds;
    pdrvPvt->asynInt32.interfaceType = asynInt32Type;
    pdrvPvt->asynInt32.pinterface  = pasynInt32;
    pdrvPvt->asynInt32.drvPvt = pdrvPvt;
    status = pasynInt32Base->initialize(pdrvPvt->portName, &pdrvPvt->asynInt32);
    if(status!=asynSuccess) return 0;

    pasynFloat64Array->read = float64ArrayRead;
    pdrvPvt->asynFloat64Array.interfaceType = asynFloat64ArrayType;
    pdrvPvt->asynFloat64Array.pinterface = pasynFloat64Array;
    pdrvPvt->asynFloat64Array.drvPvt = pdrvPvt;
    status = pasynFloat64ArrayBase->initialize(pdrvPvt->portName,
      &pdrvPvt->asynFloat64Array);
    if (status!=asynSuccess) return 0;

    pdrvPvt->interruptDelay = 0.0;
    for(addr=0; addr<nChan+1; addr++) {
        pdrvPvt->channel[addr].value = pdrvPvt->low;
    }
    
    /* Generate space for CAMAC buffer frames */
    camStat = cam_gen_init( nChan, pdrvPvt->camBuf);
    /* SAMS have 32 channels
    	 LVDTs have 8 channels, and MMCs have 48. */
    if ((strcmp(pdrvPvt->portName,"qmovWrite")==0)|
        (strcmp(pdrvPvt->portName,"qmovReadStepsRemain")==0)) {
      funcList[0]=16;funcList[1]=17;funcList[2]=18;
      if (strcmp(pdrvPvt->portName,"qmovReadStepsRemain")==0) {
      	funcList[0]=0;funcList[1]=1;funcList[2]=2;
      }
      nCards=(int)ceil((float)nChan/48.0);
      if (DEBUG) printf("qmovWrite nCards %d\n",nCards);
      addr=-1;
      for (iCard=2; iCard<=nCards+1;iCard++) {
       for (cc=0;cc<48;cc++) {
       	addr++;
       	pdrvPvt->channel[addr].N=iCard;
       	/* 081031 JLN changed <= to <, channels are numbered 0-15 */
       	if (cc<16) {
       		pdrvPvt->channel[addr].A=cc;
       		pdrvPvt->channel[addr].F=funcList[0];
       	}
       	else if (cc<32) {
       		pdrvPvt->channel[addr].A=cc-16;
       		pdrvPvt->channel[addr].F=funcList[1];
       	}
       	else {
       		pdrvPvt->channel[addr].A=cc-32;
       		pdrvPvt->channel[addr].F=funcList[2];
				}
       }
    	}
    }
    else if ((strcmp(pdrvPvt->portName,"qmovReadSams")==0)|
             (strcmp(pdrvPvt->portName,"qmovResetSams")==0)) {
      if (strcmp(pdrvPvt->portName,"qmovResetSams")==0) {
        funcList[0]=9;
      } else {
        funcList[0]=0; /*read command is F=0 */
      }
      nCards = (int) ceil((float)nChan/32);
      if (DEBSAMS) printf("qmovReadSams nCards %d\n",nCards);
      addr=-1;
      for (iCard=4; iCard<=nCards+3;iCard++) {
        for (cc=0;cc<32;cc++) {
          addr=addr+1;
          pdrvPvt->channel[addr].N=iCard;
          pdrvPvt->channel[addr].A=cc;
          pdrvPvt->channel[addr].F=nCards;
        }
      }
    }
    else if (strcmp(pdrvPvt->portName,"qmovReadLvdts")==0) {
      funcList[0]=0; /*read command is F=0 */
      nCards = (int) ceil((float)nChan/8);
      if (DEBUG) printf("qmovReadLvdts nCards %d\n",nCards);
      addr=-1;
      for (iCard=9; iCard<=nCards+8;iCard++) {
        for (cc=0;cc<8;cc++) {
          addr=addr+1;
          pdrvPvt->channel[addr].N=iCard;
          pdrvPvt->channel[addr].A=cc;
          pdrvPvt->channel[addr].F=nCards;
        }
      }
    }
    else {
      printf("Port name is: %s\n",pdrvPvt->portName);
      printf("qmoverDriverInit registerInterface failed: port name must be:\nqmovWrite, qmovReadSams, qmovReadLvdts, or qmovReadStepsRemain\n");
            return 0;
    }    
    return(0);
}

static asynStatus getAddr(drvPvt *pdrvPvt,asynUser *pasynUser,
    int *paddr,int portOK)
{
    asynStatus status;  

    status = pasynManager->getAddr(pasynUser,paddr);
    if(status!=asynSuccess) return status;
    if(*paddr>=-1 && *paddr<pdrvPvt->nChan+1) return asynSuccess;
    if(!portOK && *paddr>=0) return asynSuccess;
    epicsSnprintf(pasynUser->errorMessage,pasynUser->errorMessageSize,
        "%s addr %d is illegal; Must be >= %d and < %d\n",
        pdrvPvt->portName,*paddr,
        (portOK ? -1 : 0),pdrvPvt->nChan+1);
    return asynError;
}

/* asynCommon methods */
static void report(void *pvt,FILE *fp,int details)
{
    drvPvt *pdrvPvt = (drvPvt *)pvt;

    fprintf(fp,"    qmoverDriver: connected:%s interruptDelay = %f\n",
        (pdrvPvt->connected ? "Yes" : "No"),
        pdrvPvt->interruptDelay);
}

static asynStatus connect(void *pvt,asynUser *pasynUser)
{
    drvPvt   *pdrvPvt = (drvPvt *)pvt;
    int        addr;
    asynStatus status;  

    status = getAddr(pdrvPvt,pasynUser,&addr,1);
    if(status!=asynSuccess) return status;
    asynPrint(pasynUser, ASYN_TRACE_FLOW,
      "%s qmoverDriver:connect addr %d\n",pdrvPvt->portName,addr);
    if(addr>=0) {
        pasynManager->exceptionConnect(pasynUser);
        return asynSuccess;
    }
    asynPrint(pasynUser,ASYN_TRACE_ERROR,"connect status: %d addr: %d name: %s\n",
	      pdrvPvt->connected,addr,pdrvPvt->portName);
    if(pdrvPvt->connected) {
        asynPrint(pasynUser,ASYN_TRACE_ERROR,
           "%s qmoverDriver:connect port already connected\n",
           pdrvPvt->portName);
        return asynError;
    }
    /* Open CAMAC device and get file descriptor */
    epicsMutexMustLock(pdrvPvt->lock);
    pdrvPvt->camFD = cam_open();
    pdrvPvt->connected = 1;
    epicsMutexUnlock(pdrvPvt->lock);
    pasynManager->exceptionConnect(pasynUser);
    return asynSuccess;
}

static asynStatus disconnect(void *pvt,asynUser *pasynUser)
{
    drvPvt    *pdrvPvt = (drvPvt *)pvt;
    int        addr,camStat;
    asynStatus status;  

    status = getAddr(pdrvPvt,pasynUser,&addr,1);
    if(status!=asynSuccess) return status;
    asynPrint(pasynUser, ASYN_TRACE_FLOW,
        "%s qmoverDriver:disconnect addr %d\n",pdrvPvt->portName,addr);
    if(addr>=0) {
        pasynManager->exceptionDisconnect(pasynUser);
        return asynSuccess;
    }
    if(!pdrvPvt->connected) {
        asynPrint(pasynUser,ASYN_TRACE_ERROR,
           "%s qmoverDriver:disconnect port not connected\n",
           pdrvPvt->portName);
        return asynError;
    }
    /* Close CAMAC device */
    epicsMutexMustLock(pdrvPvt->lock);
    camStat = cam_close( pdrvPvt->camFD );
    pdrvPvt->connected = 0;
    epicsMutexUnlock(pdrvPvt->lock);
    pasynManager->exceptionDisconnect(pasynUser);
    return asynSuccess;
}

static asynStatus int32Write(void *pvt,asynUser *pasynUser,
                                 epicsInt32 value)
{
    drvPvt	*pdrvPvt = (drvPvt *)pvt;
    int			addr;/*, camStat, rplyBuf;*/
    asynStatus status;  
		int data,Q,X,nn;
		
		if (VERBOSE) printf("in int32Write\n");
    status = getAddr(pdrvPvt,pasynUser,&addr,0);
    if(status!=asynSuccess) return status;
    asynPrint(pasynUser, ASYN_TRACE_FLOW,
        "%s qmoverDriver:writeInt32 value %d\n",pdrvPvt->portName,value);
    if(!pdrvPvt->connected) {
        asynPrint(pasynUser,ASYN_TRACE_ERROR,
            "%s qmoverDriver:read not connected\n",pdrvPvt->portName);
        epicsSnprintf(pasynUser->errorMessage,pasynUser->errorMessageSize,
            "%s qmoverDriver:read not connected\n",pdrvPvt->portName);
        return asynError;
    }
    asynPrint(pasynUser,ASYN_TRACE_FLOW,
        "%s addr %d write %d\n",pdrvPvt->portName,addr,value);
    
		if (VERBOSE) printf("%s addr %d value %d\n",pdrvPvt->portName,addr,value);
		if (VERBOSE) {
			printf("N %d A %d F %d V %d\n",
				pdrvPvt->channel[addr].N,pdrvPvt->channel[addr].A,
				pdrvPvt->channel[addr].F,value);
		}
    /*  --- Command Motor Steps --- */
    if (addr==-1) {
			if (DEBSAMS) printf("addr=-1 in int32write qmovResetSams\n");
/* 20120611 JLN change nn<7 to nn<8 to reset diag sam */
  			for (nn=4;nn<8;nn++) {
  			 if (VERBOSE) printf("nn=%d\n",nn);
			  if (cam_single_cc(pdrvPvt->camFD,nn,
			    0,9,&data,&Q,&X)==-1)
			    { printf("cam call error: sam reset");
			      return(-1);
			    }
			}
		} else { 
      if ((pdrvPvt->channel[addr].N<25)&(abs(value)>0)) {
			  if(cam_single_cc(pdrvPvt->camFD,pdrvPvt->channel[addr].N,
				  1,24,&data,&Q,&X)==-1) 
			  {
				  printf("cam call error: mover unpause\n");
				  return(-1);
			  }
			  if (VERBOSE) printf("unpause data %d Q %d X %d\n",data,Q,X);

			  if(cam_single_cc(pdrvPvt->camFD,pdrvPvt->channel[addr].N,
			  	pdrvPvt->channel[addr].A,
			  	pdrvPvt->channel[addr].F,&value,&Q,&X)==-1){
			  	printf("cam call error: set mover steps\n");
			  	return(-1);
			  }
    		if (DEBUG) 
		    	printf("N %d A %d F %d V %d Q %d X %d\n",
				    pdrvPvt->channel[addr].N,pdrvPvt->channel[addr].A,
				    pdrvPvt->channel[addr].F,value,Q,X);
			  if (VERBOSE) printf("set steps data %d Q %d X %d\n",value,Q,X);
      }
		}
    return asynSuccess;
}

static asynStatus int32Read(void *pvt,asynUser *pasynUser,
                                 epicsInt32 *value)
{
    drvPvt *pdrvPvt = (drvPvt *)pvt;
    int        addr, out,Q,X; /*camStat, rplyBuf, actuallen;*/
    asynStatus status;  
    status = getAddr(pdrvPvt,pasynUser,&addr,0);
    if(status!=asynSuccess) return status;
    asynPrint(pasynUser, ASYN_TRACE_FLOW,
        "%s qmoverDriver:readInt32 value %d\n",pdrvPvt->portName,value);
    if(!pdrvPvt->connected) {
        asynPrint(pasynUser,ASYN_TRACE_ERROR,
            "%s qmoverDriver:read  not connected\n",pdrvPvt->portName);
        epicsSnprintf(pasynUser->errorMessage,pasynUser->errorMessageSize,
            "%s qmoverDriver:read not connected\n",pdrvPvt->portName);
        return asynError;
    }
    /*  --- Read Number of Motor Steps Remaining --- */
    if (addr==-1) {
      /* Execute stored command frames and get data */
/*      camStat = cam_exec(pdrvPvt->camFD, pdrvPvt->camBuf, &rplyBuf) ;
      camStat = cam_extract_cc_data(&rplyBuf, NCHANNELS, &actuallen, pdrvPvt->intData) ;
*/
			if (VERBOSE) printf("addr=-1 int32read\n");
		}
    else {
				epicsMutexMustLock(pdrvPvt->lock);
				if (VERBOSE) printf("in qmovReadStepsRemain\n");
				if (VERBOSE) printf("FD %d N %d A %d addr %d F %d\n",
										pdrvPvt->camFD,pdrvPvt->channel[addr].N,
										pdrvPvt->channel[addr].A, 
										addr, pdrvPvt->channel[addr].F);
				out=0;
				if (cam_single_cc(pdrvPvt->camFD, pdrvPvt->channel[addr].N, 
					pdrvPvt->channel[addr].A, 
					pdrvPvt->channel[addr].F, &out, &Q, &X)) {
					printf("readsteps: camcall error read NAF %d %d %d ch %d\n",
						pdrvPvt->channel[addr].N, pdrvPvt->channel[addr].A, 
						pdrvPvt->channel[addr].F, addr);
				}
				if (VERBOSE) printf("out %d Q %d X %d\n",out,Q,X);
				*value=out;
				
				epicsMutexUnlock(pdrvPvt->lock);
      /* Read Steps remaining from intData for this address (motor) */
/*      *value = pdrvPvt->intData[addr-1];
*/
    }
    return asynSuccess;
}
static asynStatus int32GetBounds(void *pvt, asynUser *pasynUser,
                                epicsInt32 *low, epicsInt32 *high)
{
    drvPvt *pdrvPvt = (drvPvt *)pvt;

    *low = pdrvPvt->low; *high = pdrvPvt->high;
    asynPrint(pasynUser,ASYN_TRACE_FLOW,
        "%s int32GetBounds low %d high %d\n",pdrvPvt->portName,*low,*high);
    return asynSuccess;
}

static asynStatus float64ArrayRead(void *pvt,asynUser *pasynUser,
  epicsFloat64 *avalue, size_t nelements, size_t *nIn)
{
  drvPvt *pdrvPvt = (drvPvt *)pvt;
  int addr,Q, X, out,ii=0,jj=0,kk=0,cc,nCards,nn=0;
  int camStat,actuallen,rplyBuf;
  int array[NCHANNELS];
  long int arraytoo[NCHANNELS];
  float final[NCHANNELS];
  char *test;
  char pvStr1[100],pvStr2[100],pvStr3[100];
  char ootStr[100],potHiStr[100],potLoStr[100];
  double oot,potHi,potLo;
  asynStatus status;
  long dbStat,thirtytwo=32,one=1;
  struct dbAddr pv1,pv2,pv3;
  struct dbAddr ootpv,potHipv,potLopv;
  double wf1[32],wf2[32],wf3[32];
  double zero=0.0;
  epicsTimeStamp timeAtStart,timeAtStop;
  double startt,stopt;
  time_t now;
  struct tm ts;
  char buf[80];
                
  if (VERBOSE) printf("in float64arrayread\n");
  status=getAddr(pdrvPvt,pasynUser,&addr,0);
  if (status!=asynSuccess){
    printf("getAddr failed\n");
    return status;
  }
  asynPrint(pasynUser, ASYN_TRACE_FLOW,
    "%s %d float64ArrayRead\n",pdrvPvt->portName,addr);
  if (DEBUG) printf("%s %d float64ArrayRead\n",pdrvPvt->portName,addr);
                                  
  if(!pdrvPvt->connected) {
    asynPrint(pasynUser,ASYN_TRACE_ERROR,
     "%s qmoverDriver:readArray not connected\n",pdrvPvt->portName);
    epicsSnprintf(pasynUser->errorMessage,pasynUser->errorMessageSize,
      "%s qmoverDriver:readArray not connected\n",pdrvPvt->portName);
    return asynError;
  }
  epicsMutexMustLock(pdrvPvt->lock);

  if (strcmp(pdrvPvt->portName,"qmovReadSams")==0) {
    if (DEBSAM) printf("readsams: ch0 NAF %d %d %d addr %d\n",
      pdrvPvt->channel[0].N, pdrvPvt->channel[0].A,
      pdrvPvt->channel[0].F,addr);
    if (addr==-1){
/*smoosh 3 other pvs together into this waveform, yes it's ugly */
      epicsTimeGetCurrent(&timeAtStart);
      startt=timeAtStart.secPastEpoch + (timeAtStart.nsec/1.0e9);
      strcpy(pvStr1,"c1:qmov:readSams1");
      strcpy(pvStr2,"c1:qmov:readSams2");
      strcpy(pvStr3,"c1:qmov:readSams3");
      dbStat=dbNameToAddr(pvStr1, &pv1);
      dbStat=dbNameToAddr(pvStr2, &pv2);
      dbStat=dbNameToAddr(pvStr3, &pv3);
      dbStat=dbGetField(&pv1,DBR_DOUBLE,&wf1,NULL,&thirtytwo,NULL);
      dbStat=dbGetField(&pv2,DBR_DOUBLE,&wf2,NULL,&thirtytwo,NULL);
      dbStat=dbGetField(&pv3,DBR_DOUBLE,&wf3,NULL,&thirtytwo,NULL);
      if (DEBUG) printf("addr=-1,wf1[1] %g wf2[1] %g wf3[1] %g\n", 
      wf1[1], wf2[1], wf3[1]);

      strcpy(ootStr,"c1:qmov:readSams:oot");
      dbStat=dbNameToAddr(ootStr,&ootpv);
      oot=0.0;
      strcpy(potHiStr,"c1:qmov:m1:pot1.HIHI");
      dbStat=dbNameToAddr(potHiStr,&potHipv);
      dbStat=dbGetField(&potHipv,DBR_DOUBLE,&potHi,NULL,&one,NULL);
      strcpy(potLoStr,"c1:qmov:m1:pot1.LOLO");
      dbStat=dbNameToAddr(potLoStr,&potLopv);
      dbStat=dbGetField(&potLopv,DBR_DOUBLE,&potLo,NULL,&one,NULL);

      for(ii=0;ii<32;ii++) avalue[ii]=(epicsFloat64) wf1[ii];
      for(ii=32;ii<64;ii++)avalue[ii]=(epicsFloat64) wf2[ii-32];
      for(ii=64;ii<96;ii++)avalue[ii]=(epicsFloat64) wf3[ii-64];

      for(ii=0;ii<96;ii++) {
        if (ii<pdrvPvt->nChan) {
          if (avalue[ii]<(potLo-0.1) || avalue[ii]>potHi) {
            oot=oot+1.0;
            time(&now);
            ts = *localtime(&now);
            strftime(buf,sizeof(buf),"%a %d %H:%M:%S",&ts);
            if (DEBOOT) {
              printf("%s %d wf[%d] %g %4.1f %4.1f\n",
                buf,(int)oot,ii,avalue[ii],potLo,potHi);
            }
          }
        } else {
          avalue[ii]=(epicsFloat64) zero;
        }
      }
      dbStat=dbPutField(&ootpv,DBR_DOUBLE,&oot,one);

      epicsMutexUnlock(pdrvPvt->lock);
      *nIn=(int) 96;
      
      epicsTimeGetCurrent(&timeAtStop);
      stopt=timeAtStop.secPastEpoch + (timeAtStop.nsec/1.0e9);
      if (VERBOSE) printf("sams-1: %g sec\n",stopt-startt);
      return(asynSuccess);
    } else {
               
      epicsTimeGetCurrent(&timeAtStart);
      startt=timeAtStart.secPastEpoch + (timeAtStart.nsec/1.0e9);
      ii=addr;
      out=100;/* f17,f16,+64 reads + round up */
      camStat = cam_gen_init( out, pdrvPvt->camBuf);            
      camStat = cam_gen_init( out, &rplyBuf);            
       out=4;           
      camStat = cam_gen_cc(pdrvPvt->camBuf,ii,0,16,out);
      out=0;             
      camStat = cam_gen_cc(pdrvPvt->camBuf,ii,0,17,out);
      for(cc=0;cc<64;cc++) {
        camStat = cam_gen_cc(pdrvPvt->camBuf,ii,0,0,out);
      }
      camStat = cam_exec(pdrvPvt->camFD, pdrvPvt->camBuf, &rplyBuf) ;
      if (DEBSAM) printf("ii %d camStat %d\n",ii,camStat);
      out=100;
      camStat = 
        cam_extract_cc_data(&rplyBuf, out, &actuallen, pdrvPvt->intData) ;
      if (DEBSAM) printf("actuallen %d camStat %d\n",actuallen, camStat);
      for (cc=2;cc<66;cc++) {
        jj=cc-2;
        array[jj]=pdrvPvt->intData[cc] & 0xFFFF;
      }

      epicsTimeGetCurrent(&timeAtStop);
      stopt=timeAtStop.secPastEpoch + (timeAtStop.nsec/1.0e9);
      if (VERBOSE) printf("sams%d: end ccgets %g sec\n",addr,stopt-startt);
      if (DEBSAM) printf("readSams: array 0-3 %d %d %d JJ %d kk %d\n",
        array[0],array[1],array[2],jj,kk);             
      if (VERBOSE) printf("readSams: ");
      for(kk=0;kk<32;kk++) {
        arraytoo[kk]=array[2*kk+1] << 16 | array[2*kk];
        arraytoo[kk]=arraytoo[kk] & 0xFFFFFF00;                
        test=memcpy(&final[kk],&arraytoo[kk],8);              
        if (VERBOSE) printf("%d:%d:%g ",kk,(int)arraytoo[kk],final[kk]);
      }      
      if (VERBOSE) printf("\n");
      
      epicsMutexUnlock(pdrvPvt->lock);
     
      *nIn=(int)(jj+1)/2;
      for (ii=0;ii<*nIn;ii++) avalue[ii]=(epicsFloat64) final[ii];

      epicsTimeGetCurrent(&timeAtStop);
      stopt=timeAtStop.secPastEpoch + (timeAtStop.nsec/1.0e9);
      if (DEBSAM) printf("sams%d: %g sec\n",addr,stopt-startt);
            
      return(asynSuccess);
    }      
  } else if (strcmp(pdrvPvt->portName,"qmovReadLvdts")==0) {
    if (DEBUG) printf("readLvdts: ch0 NAF %d %d %d\n",
      pdrvPvt->channel[0].N, pdrvPvt->channel[0].A,
      pdrvPvt->channel[0].F);
      epicsTimeGetCurrent(&timeAtStart);
      startt=timeAtStart.secPastEpoch + (timeAtStart.nsec/1.0e9);
                         
    nCards = pdrvPvt->channel[0].F;                         
    out=0;
    nn=pdrvPvt->channel[0].N;
    if (VERBOSE) printf("readLvdts: ");
    for(ii=nn;ii<(nn+nCards);ii++) {
      for(cc=0;cc<8;cc++) {
        jj=cc+(ii-nn)*8;
        if (cam_single_cc(pdrvPvt->camFD,ii,cc,0,&out,&Q,&X)) {
          printf("readlvdt: camcall error read NAF %d %d 0\n",ii,cc);
          return(asynError);
        }
        out=out & 0xFFFF;
        if (out>0x8000) {
          avalue[jj]=(epicsFloat64) (-1*(0xFFFF-out+1));
        } else {
          avalue[jj]=(epicsFloat64) out;
        }
        if (VERBOSE) printf("%d: %d; ",jj,(int)avalue[jj]);
      } /*for cc*/
    } /*for ii*/
    if (VERBOSE) printf ("*%d*\n",jj);
    epicsMutexUnlock(pdrvPvt->lock);
    *nIn=jj;
      epicsTimeGetCurrent(&timeAtStop);
      stopt=timeAtStop.secPastEpoch + (timeAtStop.nsec/1.0e9);
      if (VERBOSE) printf("lvdts: %g sec\n",stopt-startt);
    return(asynSuccess);
  } else {
    return(asynSuccess);
  }
}  

static const char *testDriverReason = "testDriverReason";
static const char *skipWhite(const char *pstart)
{
    const char *p = pstart;
    while(*p && isspace(*p)) p++;
    return p;
}

static asynStatus create(void *drvPvt,asynUser *pasynUser,
    const char *drvInfo, const char **pptypeName,size_t *psize)
{
    const char *pnext;
    long  reason = 0;

    if(!drvInfo) {
        reason = 0;
    } else {
        char *endp;

        pnext = skipWhite(drvInfo);
        if(strlen(pnext)==0) {
            reason = 0;
        } else {
            pnext = strstr(pnext,"reason");
            if(!pnext) goto error;
            pnext += strlen("reason");
            pnext = skipWhite(pnext);
            if(*pnext!='(') goto error;
            pnext++;
            pnext = skipWhite(pnext);
            errno = 0;
            reason = strtol(pnext,&endp,0);
            if(errno) {
                printf("strtol failed %s\n",strerror(errno));
                goto error;
            }
        }
    }
    pasynUser->reason = reason;
    if(pptypeName) *pptypeName = testDriverReason;
    if(psize) *psize = sizeof(int);
    return asynSuccess;
error:
    printf("asynDrvUser failed. got |%s| expecting reason(<int>)\n",drvInfo);
    epicsSnprintf(pasynUser->errorMessage,pasynUser->errorMessageSize,
        "asynDrvUser failed. got |%s| expecting reason(<int>)\n",drvInfo);
    return asynError;
}

static asynStatus getType(void *drvPvt,asynUser *pasynUser,
    const char **pptypeName,size_t *psize)
{
    *pptypeName = testDriverReason;
    *psize = sizeof(int);
    return asynSuccess;
}

static asynStatus destroy(void *drvPvt,asynUser *pasynUser)
{ return asynSuccess;}

/* register qmoverDriverInit*/
static const iocshArg qmoverDriverInitArg0 = { "portName", iocshArgString };
static const iocshArg qmoverDriverInitArg1 = { "low", iocshArgInt };
static const iocshArg qmoverDriverInitArg2 = { "high", iocshArgInt };
static const iocshArg qmoverDriverInitArg3 = { "nChan", iocshArgInt };
static const iocshArg *qmoverDriverInitArgs[] = {
    &qmoverDriverInitArg0,&qmoverDriverInitArg1,&qmoverDriverInitArg2, &qmoverDriverInitArg3};
static const iocshFuncDef qmoverDriverInitFuncDef = {
    "qmoverDriverInit", 4, qmoverDriverInitArgs};
static void qmoverDriverInitCallFunc(const iocshArgBuf *args)
{
    qmoverDriverInit(args[0].sval,args[1].ival,args[2].ival,args[3].ival);
}


static void qmoverDriverRegister(void)
{
    static int firstTime = 1;
    if (firstTime) {
        firstTime = 0;
        iocshRegister(&qmoverDriverInitFuncDef, qmoverDriverInitCallFunc);
    }
}

epicsExportRegistrar(qmoverDriverRegister);

/*==========================================================================
  Abs:  Magnet Mover Calculations

	Name: subQmov.c      
		subQmovInit - Generic Initialization routine
		subQmovDumb - For processing the dumbie funnel subroutines
		subreadback2Val  - Calculate x, y, and tilt from pot/lvdt readouts
		subcmd2MotorStep - Calculate motor steps from change in x,y,tilt
		submot2pot, submot2lvdt - calculate desired pot,lvdt from motor 
			posn? Coming later.

      Subroutines registered at end of file.
			
  Proto: not required and not used.  All functions called by the
         subroutine record get passed one argument:

         psub                       Pointer to the subroutine record data.
          Use:  pointer               
          Type: struct subRecord *    
          Acc:  read/write            
          Mech: reference

         All functions return a long integer.  0 = OK, -1 = ERROR.
         The subroutine record ignores the status returned by the Init
         routines.  
				 
  Auth: 1-May-2008, Janice Nelson
  Rev:  DD-MMM-YYYY, Reviewer's Name (.NE. Author's Name) 
  tweak
			Blatently stolen from peakPowerMeter subroutine file in 
			/afs/slac/g/cd/soft/epics/R3.14.6/ioc/lbdApp/src/subPPM.c
-------------------------------------------------------------------------------

  Mod: 
       3Jun11 JLN Add calculation of cam angles to cmd2ms & calc
         of xyt from cam angles
       6Dec10 JLN Add pot2lvdt subroutine to set lvdt ped such that
         xyt calculated from lvdts equals xyt calced from pots
       4Jun09 JLN Add -1* to tilt readins and spitouts
       9Feb09 JLN Add limit calculations to readback2val

=============================================================================*/
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>

#include <menuFtype.h>
#include <dbCommon.h>
#include <subRecord.h>
#include <dbAccess.h>
#include <registryFunction.h> /*for epicsExport */
#include <epicsExport.h> /*for epicsRegisterFunction */
#include <recSup.h> /*for struct rset */

#define ERROR -1
#define OK 0
/* 0 = off 1 = on*/
#define DEBUG 0
#define DEBUGL 0 /*for readback2lim*/
#define DEBUGMS 0 /*for cmd2motorstep*/
#define DEBUGPL 0 /*for pot2lvdt*/
#define DEBUGRV 0 /*for readback2val*/
#define DEBUGPX 0 /*for potxps*/
#define VERBOSE 0
#define PI 3.141592653589793238462643 /* more digits are better */
#define NCHANNELS 1000

long subQmovInit(struct subRecord *psub) {
		if (VERBOSE) printf("subQmovInit\n");
    return OK;
}

long subQmovDumb(struct subRecord *psub) {
		if (VERBOSE) printf("subQmovDumb\n");
		psub->val = (unsigned long)psub;
		if (VERBOSE) printf("dumbval %e %e %e %e %e %e\n",
			(float)psub->a,
			(float)psub->b,
			(float)psub->c,
			(float)psub->d,
			(float)psub->e,
			(float)psub->f);
		if (VERBOSE) printf("        %e %e %e %e %e %e\n",
			(float)psub->g,
			(float)psub->h,
			(float)psub->i,
			(float)psub->j,
			(float)psub->k,
			(float)psub->l);
		return OK;
}

long subreadback2Val(struct subRecord *psub) {
  /*
   * Calculate x,y,tilt from pot/lvdt readback
   *
   *  Blatantly stolen from Justin's MagMove_coordinates.c:
   *		coord_camPotRB_to_camRotations & coord_lvdt_to_positions
   *
   * Inputs:
   *  INPA = Current Values subroutine "funnel"
   *    INPA= LVDT1
   *       B = LVDT2
   *       C = LVDT3
   *       D = POT1
   *       E = POT2
   *       F = POT3
   *       G = motor1:radians
   *       H = motor2:radians
   *       I = motor3:radians
   *       J = backwards? 1=no, -1=yes
   *       K = potlvdt
   *       L = potxps
   *  INPB = cam 1 constants
   *    INPA=offset X
   *       B=offset Y
   *       C=offset Rotation - hardcode 18Jul11 JLN
   *       D=potMin
   *       E=potMax
   *       F=potZAngle
   *       G=camAngle = calc from des xyt
   *       H=camAlpha = calc from pot
   *  INPC = cam2 constants
   *  INPD = cam3 constants
   *       IJK=motor#:sumsteps
   *  INPE = cam constants
   *    INPA=cam step size
   *       B=cam radius
   *       C=cam lift
   *       D=mag bore height
   *       E=bore offset X
   *       F=lvdt1 X
   *       G=lvdt2 X
   *       H=lvdt3 y
   *       J=ped:pitch
   *       K=ped:roll
   * Outputs:
   *       INPF = xyt from steps, predicted pot values, and steps from pot volts
   *       INPG = x:pot (c#:qmov:m#:x:pot)
   *       INPH = y:pot (c#:qmov:m#:y)
   *       INPI = tilt:pot (c#:qmov:m#:tilt)
   *       INPJ = x:lvdt (c#:qmov:m#:x:lvdt)
   *       INPK = y:lvdt (c#:qmov:m#:y:lvdt)
   *       INPL = tilt:lvdt (c#:qmov:m#:tilt:lvdt)
   *
   * Returns -1 on error, 0 if OK.
   *
   * Mods:
   *   21-Oct-2011 J Nelson
   *     add pitch/roll to xyt calcs in cmd2ms & r2v
   *   18-Jul-2011 J Nelson
   *     calculate desired cam angles for xps
   *   17-Feb-2011 J Nelson
   *     Added capability for calculating xyt and pot volts from step sum
   *       and step count from pot volts
   */  
 
	if (DEBUGRV) printf("inside readback2Val sub\n");
/* Innies */
	struct subRecord *psubVals = 
		(struct subRecord *)(unsigned long)psub->a;
	struct subRecord *psubCam1 = 
		(struct subRecord *)(unsigned long)psub->b;
	struct subRecord *psubCam2 = 
		(struct subRecord *)(unsigned long)psub->c;
	struct subRecord *psubCam3 = 
		(struct subRecord *)(unsigned long)psub->d;
	struct subRecord *psubCams =
		(struct subRecord *)(unsigned long)psub->e;

	float lvdt[3],pot[3],bw,stepsum[3],diagsteps[3],xpsrad[3];
	float xoffs[3],yoffs[3],toffs[3];
	float potMin[3],potMax[3],potZAngle[3];
	float cam_step,cam_radius,cam_lift,bore_height;
	float boffx,lvdt1x,lvdt2x,lvdt3y;
  float pitch, roll,oldx,oldy,oldt;
	int	potlvdt, potxps, no_elementsF;
  DBADDR *pdbLinkF, *pdbLinkA;
  char * pvnam;

  struct rset *pdbRset;
  float *stepwf; 

/* Outies */
	float xytpot[3],xytlvdt[3];
	
/* local vars */
	int ii;
	float camAngle[3], modBoreH,scamAngle[3];
	float co[3],si[3], x1,y1,alpha;
	float x1_new,y1_new,alpha_new;
	int Niter;
	float xoffsRot[3]; /* hard code pi/2 3pi/4 & -3pi/4 */
  float poffsRot[3]; /* hard code 0,3pi/4,pi/4*/ 
/*	float dx,dy;
*/
	float th1_check,th2_check,th3_check;
	
    pdbLinkA = dbGetPdbAddrFromLink(&psub->inpa);
    pvnam=pdbLinkA->precord->name;
	
	if (DEBUGRV) printf("%s got through var defs\n",pvnam);
  
  pdbLinkF=dbGetPdbAddrFromLink(&psub->inpf);
  no_elementsF=pdbLinkF->no_elements;
  pdbRset=dbGetRset(pdbLinkF);
  (*pdbRset->put_array_info)(pdbLinkF, no_elementsF);
  stepwf = (float *)pdbLinkF->pfield;
	if (DEBUGRV) printf("no_elementsF %d\n",no_elementsF);

	lvdt[0]=(float)psubVals->a;
	lvdt[1]=(float)psubVals->b;
	lvdt[2]=(float)psubVals->c;
	pot[0] =(float)psubVals->d;
	pot[1] =(float)psubVals->e;
	pot[2] =(float)psubVals->f;
	xpsrad[0] =(float)psubVals->g;
	xpsrad[1] =(float)psubVals->h;
	xpsrad[2] =(float)psubVals->i;
  bw=(float)psubVals->j;
	potlvdt=(int)psubVals->k;
	potxps=(int)psubVals->l;

	if (DEBUGRV) 
		printf("lvdt %e %e %e pot %e %e %e bw %g\n",
		lvdt[0],lvdt[1],lvdt[2],pot[0],pot[1],pot[2],bw);
			
	xoffs[0]=(float)psubCam1->a;
	yoffs[0]=(float)psubCam1->b;
	toffs[0]=(float)psubCam1->c; /* next line hardcode*/
  poffsRot[0]=0.0;
	potMin[0]=(float)psubCam1->d;
	potMax[0]=(float)psubCam1->e;
	potZAngle[0]=(float)psubCam1->f;
	if (DEBUGRV)
		printf("offs[0] %e %e %e min/ax/zang %e %e %e\n",
		xoffs[0],yoffs[0],toffs[0],potMin[0],potMax[0],potZAngle[0]);

	xoffs[1]=(float)psubCam2->a;
	yoffs[1]=(float)psubCam2->b;
	toffs[1]=(float)psubCam2->c; /* next line hard code */
  poffsRot[1]=3.0*PI/4.0;
	potMin[1]=(float)psubCam2->d;
	potMax[1]=(float)psubCam2->e;
	potZAngle[1]=(float)psubCam2->f;
	if (DEBUGRV)
		printf("offs[1] %e %e %e min/ax/zang %e %e %e\n",
		xoffs[1],yoffs[1],toffs[1],potMin[1],potMax[1],potZAngle[1]);

	xoffs[2]=(float)psubCam3->a;
	yoffs[2]=(float)psubCam3->b;
	toffs[2]=(float)psubCam3->c; /* next line hardcode */
  poffsRot[2]=PI/4.0;
	potMin[2]=(float)psubCam3->d;
	potMax[2]=(float)psubCam3->e;
	potZAngle[2]=(float)psubCam3->f;
	if (DEBUGRV)
		printf("offs[2] %e %e %e min/ax/zang %e %e %e\n",
		xoffs[2],yoffs[2],toffs[2],potMin[2],potMax[2],potZAngle[2]);
  stepsum[0]=(float)psubCam3->i;
  stepsum[1]=(float)psubCam3->j;
  stepsum[2]=(float)psubCam3->k;
  if (DEBUGRV)
    printf("stepsum %g %g %g\n", stepsum[0],stepsum[1],stepsum[2]);

	cam_step=(float)psubCams->a;
	cam_radius=(float)psubCams->b;
	cam_lift=(float)psubCams->c;
	bore_height=(float)psubCams->d;
	boffx=(float)psubCams->e;
	lvdt1x=(float)psubCams->f;
	lvdt2x=(float)psubCams->g;
	lvdt3y=(float)psubCams->h;
  pitch=(float)psubCams->j;
  roll=(float)psubCams->k;
  roll=roll*1.0e-6; /* ur to r */
		
	if (DEBUGRV)
		printf("cam step %e rad %e lift %e bore_height %e\n",
		cam_step,cam_radius,cam_lift,bore_height);
	if (DEBUGRV)
		printf("boffx %e lvdt1x %e 2x %e 3y %e potlvdt %d\n",
		boffx,lvdt1x,lvdt2x,lvdt3y,potlvdt);
	if (DEBUGRV)
		printf("pitch %e roll %e\n",
	    pitch, roll);

/* for xps */
	xoffsRot[0]=PI/2.0;
	xoffsRot[1]=-3.0*PI/4.0;
	xoffsRot[2]=3.0*PI/4.0;

/* potRB->camAngle */
	for(ii=0;ii<3;ii++){
		camAngle[ii]=((pot[ii]-5.0-potZAngle[ii])*cam_step /
			((potMax[ii]-potMin[ii])/40000.0)) + poffsRot[ii];/*toffs[ii];i*/
    scamAngle[ii]=poffsRot[ii]+cam_step*stepsum[ii];
    diagsteps[ii]=(camAngle[ii]-poffsRot[ii])/cam_step;
	}
/* these were toffs jln 111011 */
	psubCam1->h=camAngle[0]+xoffsRot[0]-poffsRot[0];
	psubCam2->h=camAngle[1]+xoffsRot[1]-poffsRot[1];
	psubCam3->h=camAngle[2]+xoffsRot[2]-poffsRot[2];

	if (DEBUGRV) printf("camAngles %e %e %e\n",
		camAngle[0],camAngle[1],camAngle[2]);
	if (DEBUGRV) printf("scamAngles %e %e %e diagsteps %g %g %g\n",
		scamAngle[0],scamAngle[1],scamAngle[2],
    diagsteps[0],diagsteps[1],diagsteps[2]);

/* cam Angles -> magCoords */
	modBoreH=bore_height+xoffs[2]-cam_radius*sqrtf(2.0);
	
  for(ii=0;ii<3;ii++){
	  co[ii]=cosf(camAngle[ii]-poffsRot[ii]);
	  si[ii]=sinf(camAngle[ii]-poffsRot[ii]);
  }
  if (DEBUGRV) printf("%s pot co %g %g %g si %g %g %g\n",
    pvnam, co[0], co[1], co[2], si[0],si[1],si[2]);

	Niter=10;
	x1=0.0;
	y1=bore_height-modBoreH;
	alpha=0.0;
	for(ii=0;ii<Niter;ii++){
		alpha_new=((y1+modBoreH-bore_height)+cam_lift*(alpha*co[0]-si[0])) 
			/	(xoffs[0]+x1);
		x1_new=(alpha*(xoffs[2]-y1))+(cam_lift/sqrtf(2.0) 
			* (alpha*co[1]-si[1]+alpha*co[2]-si[2]));
		y1_new=sqrtf(2.0)*cam_radius-xoffs[2]+alpha*x1+ cam_lift/sqrtf(2.0) 
			* (alpha*co[1]-si[1]-alpha*co[2]+si[2]);
		alpha=alpha_new;
		x1=x1_new;
		y1=y1_new;
		if (DEBUGRV) printf("x1 %g y1 %g alpha%g\n",x1,y1,alpha);
	}
	
		xytpot[0]=x1+boffx - boffx*cosf(alpha) - modBoreH*sinf(alpha);
		xytpot[1]=y1-bore_height + modBoreH*cosf(alpha) - boffx*sinf(alpha);
		xytpot[2]=alpha;
   if (DEBUGRV) {
     printf("rb2v: old pot x %g y %g t%g \n",xytpot[0],xytpot[1],xytpot[2]);
   }
    oldx=xytpot[0];oldy=xytpot[1];oldt=xytpot[2];
/* convert magCoords to real space coords to correct for adjuster box */
    xytpot[0]=oldx*cosf(roll)+oldy*sinf(roll)*cosf(pitch);
    xytpot[1]=-1*oldx*sinf(roll)+oldy*cosf(roll)*cosf(pitch);
    xytpot[2]=oldt-roll;
/*  z' = -y'*sin(pitch) */
   if (DEBUGRV) {
     printf("rb2v: new pot x %g y %g t%g \n",xytpot[0],xytpot[1],xytpot[2]);
   }
	
	if (DEBUGRV) {
	 th1_check=alpha-asinf(1.0 / cam_lift * ((x1+xoffs[0])*sinf(alpha) - 
	 y1*cosf(alpha) + (bore_height-modBoreH)));
	 th2_check=alpha-asinf(1.0 / cam_lift * ((x1+xoffs[2]) * 
	 	sinf(PI/4.0-alpha) + y1*cosf(PI/4.0-alpha) - cam_radius));
	 th3_check=alpha-asinf(1.0 / cam_lift * ((x1-xoffs[2]) 
	 	*sinf(PI/4.0-alpha) - y1*cosf(PI/4.0+alpha) + cam_radius));
	 printf("th1=%f\tth2=%f\tth3=%f\n",th1_check,th2_check,th3_check);
	}

/* use angles calced from steps to calc xyt */
  for(ii=0;ii<3;ii++){
	  co[ii]=cosf(scamAngle[ii]-poffsRot[ii]);
	  si[ii]=sinf(scamAngle[ii]-poffsRot[ii]);/* these were toffs 111011 */
  }

	Niter=10;
	x1=0.0;
	y1=bore_height-modBoreH;
	alpha=0.0;
	for(ii=0;ii<Niter;ii++){
		alpha_new=((y1+modBoreH-bore_height)+cam_lift*(alpha*co[0]-si[0])) 
			/	(xoffs[0]+x1);
		x1_new=(alpha*(xoffs[2]-y1))+(cam_lift/sqrtf(2.0) 
			* (alpha*co[1]-si[1]+alpha*co[2]-si[2]));
		y1_new=sqrtf(2.0)*cam_radius-xoffs[2]+alpha*x1+ cam_lift/sqrtf(2.0) 
			* (alpha*co[1]-si[1]-alpha*co[2]+si[2]);
		alpha=alpha_new;
		x1=x1_new;
		y1=y1_new;
		if (DEBUGRV) printf("steps: x1 %g y1 %g alpha%g\n",x1,y1,alpha);
	}
	
  stepwf[0]=x1+boffx - boffx*cosf(alpha) - modBoreH*sinf(alpha);
	stepwf[1]=y1-bore_height+modBoreH*cosf(alpha)-boffx*sinf(alpha);
	stepwf[2]=alpha;
    oldx=stepwf[0];oldy=stepwf[1];oldt=stepwf[2];
/* convert magCoords to real space coords to correct for adjuster box */
    stepwf[0]=oldx*cosf(roll)+oldy*sinf(roll)*cosf(pitch);
    stepwf[1]=-1*oldx*sinf(roll)+oldy*cosf(roll)*cosf(pitch);
    stepwf[2]=oldt-roll;
  stepwf[0]=stepwf[0]*bw;
  stepwf[2]=stepwf[2]*1.0e6*bw;
  for(ii=0;ii<3;ii++){ /* was toffs 111011 */
    stepwf[ii+3]=(scamAngle[ii]-poffsRot[ii])*(potMax[ii]-potMin[ii]) /
      40000.0 / cam_step + 5 + potZAngle[ii];
    stepwf[ii+6]=diagsteps[ii];
  }
  if (DEBUGRV) printf("stepwf %g %g %g %g %g %g %g %g %g\n",
    stepwf[0],stepwf[1],stepwf[2],stepwf[3],stepwf[4],stepwf[5],
    stepwf[6],stepwf[7],stepwf[8]);

	
/* use angles from xps to calc xyt */
  if (potxps) {

  if (VERBOSE) printf("%s scam co %g %g %g si %g %g %g stepwf %g %g %g \n",
    pvnam, co[0],co[1],co[2],si[0],si[1],si[2],stepwf[0],stepwf[1],stepwf[2]);

/* add xoffsRot stuff 18Jul11 */
  for(ii=0;ii<3;ii++){
	  co[ii]=cosf(xpsrad[ii] - xoffsRot[ii]-toffs[ii]); /* no toffs before111011*/
	  si[ii]=sinf(xpsrad[ii] - xoffsRot[ii]-toffs[ii]);
/*	  co[ii]=cosf(xpsrad[ii]-toffs[ii]);
	  si[ii]=sinf(xpsrad[ii]-toffs[ii]);
*/
  }
  if (VERBOSE) printf("%s xps co %g %g %g si %g %g %g\n",
    pvnam, co[0], co[1], co[2], si[0],si[1],si[2]);

	Niter=10;
	x1=0.0;
	y1=bore_height-modBoreH;
	alpha=0.0;
	for(ii=0;ii<Niter;ii++){
		alpha_new=((y1+modBoreH-bore_height)+cam_lift*(alpha*co[0]-si[0])) 
			/	(xoffs[0]+x1);
		x1_new=(alpha*(xoffs[2]-y1))+(cam_lift/sqrtf(2.0) 
			* (alpha*co[1]-si[1]+alpha*co[2]-si[2]));
		y1_new=sqrtf(2.0)*cam_radius-xoffs[2]+alpha*x1+ cam_lift/sqrtf(2.0) 
			* (alpha*co[1]-si[1]-alpha*co[2]+si[2]);
		alpha=alpha_new;
		x1=x1_new;
		y1=y1_new;
		if (DEBUGRV) printf("steps: x1 %g y1 %g alpha%g\n",x1,y1,alpha);
	}
	
  stepwf[9]=x1+boffx - boffx*cosf(alpha) - modBoreH*sinf(alpha);
	stepwf[10]=y1-bore_height+modBoreH*cosf(alpha)-boffx*sinf(alpha);
	stepwf[11]=alpha;
    oldx=stepwf[9];oldy=stepwf[10];oldt=stepwf[11];
/* convert magCoords to real space coords to correct for adjuster box */
    stepwf[9]=oldx*cosf(roll)+oldy*sinf(roll)*cosf(pitch);
    stepwf[10]=-1*oldx*sinf(roll)+oldy*cosf(roll)*cosf(pitch);
    stepwf[11]=oldt-roll;

  stepwf[9]=stepwf[9]*bw;
  stepwf[11]=stepwf[11]*1.0e6*bw;
  if (VERBOSE) printf("xpsrad %g %g %g xytxps %g %g %g\n",
    xpsrad[0],xpsrad[1],xpsrad[2],stepwf[9],stepwf[10],stepwf[11]);
  } else {
  stepwf[9]=0.0;stepwf[10]=0.0;stepwf[11]=0.0;
  }

	
/* lvdt -> MagCoords */
		xytlvdt[1]=(lvdt[0] + lvdt[1])/2.0;
		xytlvdt[2]=atanf((lvdt[0] - lvdt[1])/(lvdt1x+lvdt2x));
		xytlvdt[0]=lvdt[2]-(xytlvdt[1]+lvdt3y)*tanf(xytlvdt[2]);

    oldx=xytlvdt[0];oldy=xytlvdt[1];oldt=xytlvdt[2];
/* convert magCoords to real space coords to correct for adjuster box */
    xytlvdt[0]=oldx*cosf(roll)+oldy*sinf(roll)*cosf(pitch);
    xytlvdt[1]=-1*oldx*sinf(roll)+oldy*cosf(roll)*cosf(pitch);
    xytlvdt[2]=oldt-roll;
	
	psub->g=xytpot[0]*bw;
	psub->h=xytpot[1];
	psub->i=xytpot[2]*1.0e6*bw;
	psub->j=xytlvdt[0]*bw;
	psub->k=xytlvdt[1];
	psub->l=xytlvdt[2]*1.0e6*bw;
	if (DEBUGRV) {
		printf("xytpot  %e %e %e\n",xytpot[0],xytpot[1],xytpot[2]);
		printf("xytlvdt %e %e %e\n",xytlvdt[0],xytlvdt[1],xytlvdt[2]);
	}	
	
	if (DEBUGRV) printf("leaving readback2Val sub\n");
	return OK;
}
/* =================================================================== */
long subcmd2MotorStep(struct subRecord *psub) {
  /*
   * Calculate motor steps and pot readback from x,y,tilt 
   *
   *  Blatantly stolen from Justin's MagMove_coordinates.c:
   *	coord_MagCoords_to_rotations & camRotations_to_camMoveData
   *
   * Inputs:
   *			INPA = Input Values subroutine "funnel"
   *				INPA= desired relative change in um: x c#:qmov:m#:moveCMD:x
   *      		B = y
   *					C = tilt
   *					D = current x value
   *					E = current y value
   *					F = current tilt value
   *					G = current pot 1 value
   *					H = current pot 2 value
   *					I = current pot 3 value
   *					J = transform: 0 = slc method, 1 = 5 motor scheme
   *          K = backward: 1=no, -1 =yes
   *			INPB = cam 1 constants
   *				INPA=offset X
   *					 B=offset Y
   *					 C=offset Rotation
   *					 D=potMin
   *					 E=potMax
   *					 F=potZAngle
   *           G=calculated desired cam angle from xyt
   *				INPC = cam2 constants
   *				INPD = cam3 constants
   *				INPE = cam constants
   *					INPA=cam step size
   *						 B=cam radius
   *						 C=cam lift
   *						 D=mag bore height
   *						 E=bore offset X
   *						 F=lvdt1 X (not used here)
   *						 G=lvdt2 X (not used here)
   *						 H=lvdt3 y (not used here)
   *             I=potxps toggle
   *						 L=bore offset Y
   * Outputs:
   *			 INPF = badCoord - 0 = OKOK, 1 = sad :(
   *       INPG = pot 1 desired RB (c#:qmov:m#:pot#:des)
   *       INPH = pot 2 desired RB (c#:qmov:m#:pot#:des)
   *       INPI = pot 3 desired RB (c#:qmov:m#:pot#:des)
   *       INPJ = step command (c#:qmov:m#:motor1:stepCMD)
   *       INPK = step command (c#:qmov:m#:motor2:stepCMD)
   *       INPL = step command (c#:qmov:m#:motor3:stepCMD)
   *
   * Returns -1 on error, 0 if OK.
   */  
 
	if (DEBUGMS) printf("inside cam2motorstep sub\n");
/* Innies */
	struct subRecord *psubMovs = 
		(struct subRecord *)(unsigned long)psub->a;
	struct subRecord *psubCam1 = 
		(struct subRecord *)(unsigned long)psub->b;
	struct subRecord *psubCam2 = 
		(struct subRecord *)(unsigned long)psub->c;
	struct subRecord *psubCam3 = 
		(struct subRecord *)(unsigned long)psub->d;
	struct subRecord *psubCams =
		(struct subRecord *)(unsigned long)psub->e;

	float desxyt[3],absxyt[3],actPot[3],bw;
	int transform,potxps;
	float cam_step,cam_radius,cam_lift,bore_height;
	float xoffs[3],toffs[3];
	float potMin[3],potMax[3],potZAngle[3];
	float boffx;/*,boffy,stwo,sthree,vdepth;*/
  float pitch,roll; 
	
/* Outies */
	long absSteps[3];
	float absPotDes[3];
	int badCoords=0;

/* local vars */
	int ii;
	float mod_bore_height,sinbm,cosbm;
	float absCamAngle[3],actCamAngle[3],actPotDes[3];
	float dx_int,dy_int,guts;
	long actSteps[3];
  float xoffsRot[3],poffsRot[3]; /*hard code xps offs & pot offs */
/*	long stepData; Does it break without these?
	float rbData;	
*/
  float desx,desy;
	char * pvnam;
  DBADDR *pdbLinkA;

	if (DEBUGMS) printf("got through var defs cmd2motorstep\n");
  xoffsRot[0]=PI/2.0;
  xoffsRot[1]=-3.0*PI/4.0;
  xoffsRot[2]=3.0*PI/4.0;

  bw=(float)psubMovs->k;
	desxyt[0]=(float)psubMovs->a*bw;
	desxyt[1]=(float)psubMovs->b;
	desxyt[2]=(float)psubMovs->c*1.0e-6*bw;
	absxyt[0]=(float)psubMovs->d*bw;
	absxyt[1]=(float)psubMovs->e;
	absxyt[2]=(float)psubMovs->f*1.0e-6*bw;
	actPot[0]=(float)psubMovs->g;
	actPot[1]=(float)psubMovs->h;
	actPot[2]=(float)psubMovs->i;
	transform=(int)psubMovs->j;
	if (DEBUGMS) printf("c2ms: absxyt %g %g %g absxyt %g %g %g trans %d bw %g\n",
		desxyt[0],desxyt[1],desxyt[2],absxyt[0],absxyt[1],absxyt[2],
		transform,bw);
	
	xoffs[0]=(float)psubCam1->a;
	toffs[0]=(float)psubCam1->c; /* next line hardcode 11Oct11*/
  poffsRot[0]=0.0;
	potMin[0]=(float)psubCam1->d;
	potMax[0]=(float)psubCam1->e;
	potZAngle[0]=(float)psubCam1->f;
	if (DEBUGMS)
		printf("c2ms: offs[0] %e %e min/ax/zang %e %e %e\n",
		xoffs[0],toffs[0],potMin[0],potMax[0],potZAngle[0]);

	xoffs[1]=(float)psubCam2->a;
	toffs[1]=(float)psubCam2->c; /* next line hard code 11Octl11 */
  poffsRot[1]=3.0*PI/4.0;
	potMin[1]=(float)psubCam2->d;
	potMax[1]=(float)psubCam2->e;
	potZAngle[1]=(float)psubCam2->f;
	if (DEBUGMS)
		printf("c2ms: offs[1] %e %e min/ax/zang %e %e %e\n",
		xoffs[1],toffs[1],potMin[1],potMax[1],potZAngle[1]);

	xoffs[2]=(float)psubCam3->a;
	toffs[2]=(float)psubCam3->c; /* next line hard code 18Jul11 */
  poffsRot[2]=PI/4.0;
	potMin[2]=(float)psubCam3->d;
	potMax[2]=(float)psubCam3->e;
	potZAngle[2]=(float)psubCam3->f;
	if (DEBUGMS)
		printf("c2ms: offs[2] %e %e min/ax/zang %e %e %e\n",
		xoffs[2],toffs[2],potMin[2],potMax[2],potZAngle[2]);

	cam_step=(float)psubCams->a;
	cam_radius=(float)psubCams->b;
	cam_lift=(float)psubCams->c;
	bore_height=(float)psubCams->d;
	boffx=(float)psubCams->e;
	potxps=(int)psubCams->i;
  pitch=(float)psubCams->j;
  roll=(float)psubCams->k;
  roll=roll*1.0e-6; /* ur to r */

	if (DEBUGMS) {
		printf("c2ms: cstep %g crad %g clift%g bheight %g pitch %g roll %g\n",
			cam_step,cam_radius,cam_lift,bore_height,pitch,roll);
		printf("c2ms: boffx %g\n",boffx);
	}
	badCoords=0; /* innocent until proven guilty? */
	for(ii=0;ii<3;ii++){
		absSteps[ii]=0;
		absPotDes[ii]=5.0;
	}
	psub->f=badCoords;
	psub->g=absPotDes[0];
	psub->h=absPotDes[1];
	psub->i=absPotDes[2];
	psub->j=absSteps[0];
	psub->k=absSteps[1];
	psub->l=absSteps[2]; /* just in case */


    pdbLinkA = dbGetPdbAddrFromLink(&psub->inpa);
    pvnam=pdbLinkA->precord->name;
/* =====================================================*/
/* Part 0: calculate des xyt in mover coords from abs des xyt */
/*                                                            */
   if (fabs(roll)>0) {
     printf("c2ms: old x %g y %g t%g \n",desxyt[0],desxyt[1],desxyt[2]);
   }
   desx=desxyt[0]*cosf(roll)-desxyt[1]*sinf(roll);
   desy=cosf(pitch)*(desxyt[1]*cosf(roll)+desxyt[0]*sinf(roll));
   desxyt[2]=desxyt[2]+roll;
/* desz=-y'*sin(pitch) */
   desxyt[0]=desx;
   desxyt[1]=desy;
   if (fabs(roll)>0) {
     printf("c2ms: new x %g y %g t%g \n",desxyt[0],desxyt[1],desxyt[2]);
   }

/*   ======================================================*/
/* Part I: calculate required camAngles to achieve given absxyt*/
/* Always use "SLC Method" not 5 roller */
		mod_bore_height=bore_height+fabsf(xoffs[2])-sqrtf(2.0L) * 
			cam_radius;
		sinbm=(cosf(desxyt[2])-sinf(desxyt[2]))/sqrt(2.0);
		cosbm=(cosf(desxyt[2])+sinf(desxyt[2]))/sqrt(2.0);
		dx_int=desxyt[0]+boffx*cosf(desxyt[2]) + mod_bore_height * 
			sinf(desxyt[2])-boffx;
		dy_int=desxyt[1]-(mod_bore_height*cosf(desxyt[2])) + (boffx * 
			sinf(desxyt[2]))+bore_height;
		if (DEBUGMS) printf("c2ms: boffx %e cosA %e sinA %e bh %e ",
		    boffx,cosf(desxyt[2]),sin(desxyt[2]),bore_height);
		if (DEBUGMS) printf("c2msmodbh %e sinbm %e cosbm %e dx_int %e dy_int %e\n",
			mod_bore_height,sinbm,cosbm,dx_int,dy_int);
		
		/* check for bad coordinates */
		guts=(((xoffs[0]+dx_int) * sinf(desxyt[2])) 
			- (dy_int * cosf(desxyt[2])) + bore_height - 
			mod_bore_height)/cam_lift;
		absCamAngle[0]=asinf(guts);
		if (DEBUGMS) printf("c2ms: absCamAngle[0] guts %e before %g", guts, 
			absCamAngle[0]);

/*		if (fabsf(absCamAngle[0])>1 || isnanf(absCamAngle[0])) {
20120611 JLN change check for <pi/2
*/
		if (fabsf(absCamAngle[0])>1.5708 || isnanf(absCamAngle[0])) {
			badCoords=1;
			absCamAngle[0]=0;
      if (DEBUGMS) printf("c2ms: badCoords=1 absCamAngle[0]\n");
		} else {
			absCamAngle[0]=desxyt[2] - absCamAngle[0];
		}
		if (DEBUGMS) printf(" after %g\n",absCamAngle[0]);
		
		if (DEBUGMS) 
			printf("c2ms: xoffs[2] %g dx_int %g sinbm %g dy_int %g cosbm %g\n",
			 xoffs[2],dx_int,sinbm,dy_int,cosbm);

		
guts=((((xoffs[2]+dx_int)*sinbm)+(dy_int*cosbm)-cam_radius)/cam_lift);	

/*	  
guts=((((xoffs[2]+dx_int)*sinbm)+(dy_int*sinbm)-cam_radius)/cam_lift);	
*/
		absCamAngle[1]=asinf(guts);
    printf("c2ms: sinbm cosbm ");
		if (DEBUGMS) printf("c2ms: absCamAngle[1] guts %e before %g", 
		guts, absCamAngle[1]);
		
/*		if (fabsf(absCamAngle[1])>1 || isnanf(absCamAngle[1])) {
*/
		if (fabsf(absCamAngle[1])>1.5708 || isnanf(absCamAngle[1])) {
			badCoords=1;
			absCamAngle[1]=0;
      if (DEBUGMS) printf(" badCoords=1 absCamAngle[1]\n");
		} else {
/*			absCamAngle[1]=desxyt[2]+ toffs[1] - absCamAngle[1];*/
			absCamAngle[1]=desxyt[2]+ poffsRot[1] - absCamAngle[1];
		}
		if (DEBUGMS) printf(" after %g\n",absCamAngle[1]);

		if (DEBUGMS)
			printf("c2ms: dx_int %g xoffs[2] %g cosbm %g dy_int %g sinbm %g\n",
				dx_int,xoffs[2],cosbm,dy_int,sinbm);
				
		guts=(((dx_int-xoffs[2])*cosbm) - (dy_int*sinbm) + 

/*		guts=(((dx_int-xoffs[2])*cosbm) - (dy_int*cosbm) + 
*/
			cam_radius) / cam_lift;		
			printf("c2ms: cosbm sinbm\n");
		absCamAngle[2]=asinf(guts);		
		if (DEBUGMS) printf("c2ms: absCamAngle[2] guts %e before %g",
		 guts, absCamAngle[2]);

/*		if (fabsf(absCamAngle[2])>1||isnanf(absCamAngle[2])) {
*/
		if (fabsf(absCamAngle[2])>1.5708||isnanf(absCamAngle[2])) {
			badCoords=1;
			absCamAngle[2]=0;
      if (DEBUGMS) printf(" badCoords=1 absCamAngle[2]\n");
		} else {
/*			absCamAngle[2]=desxyt[2]+ toffs[2] - absCamAngle[2];*/
			absCamAngle[2]=desxyt[2]+ poffsRot[2] - absCamAngle[2];
		}
		if (DEBUGMS) printf(" after %g\n",absCamAngle[2]);
		
		if (DEBUG) printf("c2ms: absCamAngles %g %g %g\n",absCamAngle[0], 
			absCamAngle[1], absCamAngle[2]);

		if (badCoords==1) {
			psub->f=badCoords;
			if (DEBUGMS) printf("bad coords\n");
			if (DEBUGMS) printf("%g potDes %g %g %g absSteps %g %g %g\n",
			psub->f,psub->g,psub->h,psub->i,psub->j,psub->k,psub->l);
			return(-1);
		}
	if (DEBUGPX) printf("%s abscamangles %e %e %e\n",
   pvnam, absCamAngle[0], absCamAngle[1], absCamAngle[2]);	
/* end of MagCoords to rotations */

/* ======================================================*/
/* Part II: Calculate desired steps (total steps needed to get to desired 
		xyt) and what the pots should read when we're done.
*/
/* begin camRotations to camMoveData */
/* 110211 JLN change "- potZangle" to "+potZangle" */

/*
 Pot voltage should be 5V at +10000, -15000, +15000 turns, with some 
 correction term
 zero steps is set as the cam rotation angle where the machined flats 
 are oriented such that the normal to each flat is vertical
*/
/* 	number of cam steps for each cam */
/* 	corrected pot readback (ideal case) */

	for (ii=0;ii<3;ii++) {
/*		absSteps[ii] = (long)floorf((absCamAngle[ii] - toffs[ii])/cam_step); */
		absSteps[ii] = (long)floorf((absCamAngle[ii] - poffsRot[ii])/cam_step);
		absPotDes[ii] = ((float)absSteps[ii] * ((potMax[ii]-potMin[ii])/40000)) 
			+ 5.0 +  potZAngle[ii];
	}
	badCoords=0; /*must be if we've gotten this far */

/* end camRotations to camMoveData */
/* ======================================================*/

/* Part III: Calculate current angles from current pot readback.*/
/* potRB->camAngle */
	for(ii=0;ii<3;ii++){
		actCamAngle[ii]=((actPot[ii]-5.0-potZAngle[ii])*cam_step /
			((potMax[ii]-potMin[ii])/40000.0)) + poffsRot[ii];/*toffs[ii];i*/
	}
  if (DEBUGMS) printf("actCamAngles %g %g %g\n", actCamAngle[0],
    actCamAngle[1], actCamAngle[2]);
/* end potRB to cam Rot */	
/* ======================================================*/

/* Part IV: calc current "steps" from current rotations */
/* begin camRotations to camMoveData for current values */
/* 110211 JLN Change "- potz" to "+ potz"*/
	for (ii=0;ii<3;ii++) {
/*		actSteps[ii] = (long)floorf((actCamAngle[ii] - toffs[ii])/cam_step);*/
		actSteps[ii] = (long)floorf((actCamAngle[ii] - poffsRot[ii])/cam_step);
		actPotDes[ii] = ((float)actSteps[ii] * ((potMax[ii]-potMin[ii])/40000))  
			+ 5.0 + potZAngle[ii];
	}
	if (DEBUGMS) {
		printf("act Steps: %d %d %d\n",(int)actSteps[0], (int)actSteps[1], 
		(int)actSteps[2]);
		printf("act PotDes: %g %g %g\n",actPotDes[0], actPotDes[1], 
			actPotDes[2]);
	}
/* end camRot to camMove for act values */
/* ======================================================*/

/* Part V: Wrap it up */
	
/* load up output vars */
	psub->f=badCoords;
	psub->g=absPotDes[0];
	psub->h=absPotDes[1];
	psub->i=absPotDes[2];
 /*if xps, send 0 steps to MMC*/
  if (potxps) { /*if xps, send 0 steps to MMC*/
  	psub->j=0;
	  psub->k=0;
	  psub->l=0;
  } else {
  	psub->j=absSteps[0]-actSteps[0];
	  psub->k=absSteps[1]-actSteps[1];
	  psub->l=absSteps[2]-actSteps[2];
	}
  if (DEBUGPX) printf("%s xoffsR %e %e %e poffsR %e %e %e\n",
    pvnam, xoffsRot[0], xoffsRot[1], xoffsRot[2], 
    poffsRot[0], poffsRot[1], poffsRot[2]);
/*  psubCam1->g=absCamAngle[0]+PI/2.0-toffs[0];*/  /* add xps offset 18Jul11 */ 
  psubCam1->g=absCamAngle[0]+xoffsRot[0]-poffsRot[0]+toffs[0];  /* add xps offset 18Jul11 */ 
  psubCam2->g=absCamAngle[1]+xoffsRot[1]-poffsRot[1]+toffs[1];
  psubCam3->g=absCamAngle[2]+xoffsRot[2]-poffsRot[2]+toffs[2];
/* wave good bye */
	if (DEBUGMS) printf("potxps %d steps sent back to sub %d %d %d\n",
	potxps, (int)absSteps[0]-(int)actSteps[0], 
	(int)absSteps[1]-(int)actSteps[1], (int)absSteps[2]-(int)actSteps[2]);
	

	return(0);
}
long subreadback2Lim(struct subRecord *psub) {
  /*
   * Calculate x,y,tilt limits from xyt
   *
   * It's not very pretty and I'm not very proud
   *
   * Inputs:
   *			INPA = cam 1 constants
   *				INPA=offset X
   *					 B=offset Y
   *					 C=offset Rotation
   *					 D=potMin
   *					 E=potMax
   *					 F=potZAngle
   *				INPB = cam2 constants
   *				INPC = cam3 constants
   *				INPD = cam constants
   *					INPA=cam step size
   *						 B=cam radius
   *						 C=cam lift
   *						 D=mag bore height
   *						 E=bore offset X
   *						 F=lvdt1 X
   *						 G=lvdt2 X
   *						 H=lvdt3 y
   *        INPE = xyt values
   *             A = x
   *             B = y
   *             C = t
   * Outputs:
   *       INPG = x min
   *       INPH = x max
   *       INPI = y min
   *       INPJ = y max
   *       INPK = tilt min
   *       INPL = tilt max
   *
   * Returns -1 on error, 0 if OK.
   */  
 
	if (DEBUGL) printf("inside readback2Lim sub\n");
/* Innies */
	struct subRecord *psubCam1 = 
		(struct subRecord *)(unsigned long)psub->a;
	struct subRecord *psubCam2 = 
		(struct subRecord *)(unsigned long)psub->b;
	struct subRecord *psubCam3 = 
		(struct subRecord *)(unsigned long)psub->c;
	struct subRecord *psubCams =
		(struct subRecord *)(unsigned long)psub->d;
	struct subRecord *psubxyt =
		(struct subRecord *)(unsigned long)psub->e;
	float xoffs[3],yoffs[3],toffs[3];
	float potMin[3],potMax[3],potZAngle[3];
	float cam_step,cam_radius,cam_lift,bore_height;
	float boffx,lvdt1x,lvdt2x,lvdt3y;
	float absxyt[3];
	float minxyt[3],maxxyt[3];
  float minx[3],miny[3],mina[3];
  float maxx[3],maxy[3],maxa[3];
/* Outies */
	
/* local vars */
	int ii,nn,foundmin[3],foundmax[3];
	float mod_bore_height,sinbm,cosbm,dx_int,dy_int;
  float guts_lim, alfa, dx_inta, dy_inta, sinbma, cosbma;
  float term1[3],term2[3],term3[3],guts[3];
  float term1a[3],term2a[3],term3a[3],gutsa[3];
	float xlim,ylim,alim;
  double dumfrac,dumint; /* for modf */	
  
  xlim=5.0e3;ylim=5.0e3;alim=7.0e-3;
	xoffs[0]=(float)psubCam1->a;
	yoffs[0]=(float)psubCam1->b;
	toffs[0]=(float)psubCam1->c;
	potMin[0]=(float)psubCam1->d;
	potMax[0]=(float)psubCam1->e;
	potZAngle[0]=(float)psubCam1->f;
	if (DEBUGL)
		printf("offs[0] %e %e %e min/ax/zang %e %e %e\n",
		xoffs[0],yoffs[0],toffs[0],potMin[0],potMax[0],potZAngle[0]);

	xoffs[1]=(float)psubCam2->a;
	yoffs[1]=(float)psubCam2->b;
	toffs[1]=(float)psubCam2->c;
	potMin[1]=(float)psubCam2->d;
	potMax[1]=(float)psubCam2->e;
	potZAngle[1]=(float)psubCam2->f;
	if (DEBUGL)
		printf("offs[1] %e %e %e min/ax/zang %e %e %e\n",
		xoffs[1],yoffs[1],toffs[1],potMin[1],potMax[1],potZAngle[1]);

	xoffs[2]=(float)psubCam3->a;
	yoffs[2]=(float)psubCam3->b;
	toffs[2]=(float)psubCam3->c;
	potMin[2]=(float)psubCam3->d;
	potMax[2]=(float)psubCam3->e;
	potZAngle[2]=(float)psubCam3->f;
	if (DEBUGL)
		printf("offs[2] %e %e %e min/ax/zang %e %e %e\n",
		xoffs[2],yoffs[2],toffs[2],potMin[2],potMax[2],potZAngle[2]);

	cam_step=(float)psubCams->a;
	cam_radius=(float)psubCams->b;
	cam_lift=(float)psubCams->c;
	bore_height=(float)psubCams->d;
	boffx=(float)psubCams->e;
	lvdt1x=(float)psubCams->f;
	lvdt2x=(float)psubCams->g;
	lvdt3y=(float)psubCams->h;
		
	if (DEBUGL)
		printf("cam step %e rad %e lift %e bore_height %e\n",
		cam_step,cam_radius,cam_lift,bore_height);
	if (DEBUGL)
		printf("boffx %e lvdt1x %e 2x %e 3y %e\n",
		boffx,lvdt1x,lvdt2x,lvdt3y);

  absxyt[0]=(float)psubxyt->a;
  absxyt[1]=(float)psubxyt->b;
  absxyt[2]=(float)psubxyt->c;
  absxyt[2]=absxyt[2]*1.0e-6;
  
	if (DEBUGL)
		printf("absxyt[0] %e absxyt[1] %e absxyt[2] %e\n",
		absxyt[0],absxyt[1],absxyt[2]);

/* cam Angles limits -> xyt limits */

    guts_lim=sin(1);
    mod_bore_height=bore_height+fabs(xoffs[2])-cam_radius*sqrtf(2.0);
		sinbm=(cosf(absxyt[2])-sinf(absxyt[2]))/sqrt(2.0);
		cosbm=(cosf(absxyt[2])+sinf(absxyt[2]))/sqrt(2.0);
		dx_int=absxyt[0]+boffx*cosf(absxyt[2]) + (mod_bore_height * 
			sinf(absxyt[2]))-boffx;
    if (DEBUGL)
      printf("sinbm %e cosbm %e absxyt[2] %e\n",
        sinbm, cosbm, absxyt[2]);
		if (DEBUGL)
		  printf("dx: x %e boffx %e cosa %e mbh %e sina %e\n",
		    absxyt[0],boffx,cosf(absxyt[2]),
		    mod_bore_height,sinf(absxyt[2]));
		dy_int=absxyt[1]-(mod_bore_height*cosf(absxyt[2])) + (boffx * 
			sinf(absxyt[2]))+bore_height;
    if (DEBUGL)
      printf("mbh %e dxi %e dyi %e\n",mod_bore_height,dx_int,dy_int);

    term1[0]=(xoffs[0]+dx_int)*sin(absxyt[2]);
    term2[0]=dy_int*cosf(absxyt[2]);
    term3[0]=bore_height-mod_bore_height;    
		guts[0]=(term1[0]-term2[0]+term3[0])/cam_lift;
	
    term1[1]=(xoffs[2]+dx_int)*sinbm;
    term2[1]=dy_int*cosbm;
    term3[1]=cam_radius;
		guts[1]=(term1[1]+term2[1]-term3[1])/cam_lift;	
		
		term1[2]=(dx_int-xoffs[2])*cosbm;
		term2[2]=dy_int*sinbm;
		term3[2]=cam_radius;
		guts[2]=(term1[2]-term2[2]+term3[2]) / cam_lift;		

    if (DEBUGL) {
      printf("t1[0] %e t2 %e t3 %e guts %e\n",
      term1[0],term2[0],term3[0],guts[0]);
      printf("t1[1] %e t2 %e t3 %e guts %e\n",
      term1[1],term2[1],term3[1],guts[1]);
      printf("t1[2] %e t2 %e t3 %e guts %e\n",
      term1[2],term2[2],term3[2],guts[2]);
    }
/* x limits */
    if (fabsf(sinf(absxyt[2]))<1.0e-15) {
      maxx[0]=1.0e9;
      minx[0]=-1.0e9;
    } else {
      minx[0]=(term2[0]-term3[0]-guts_lim*cam_lift)/sinf(absxyt[2])
        -mod_bore_height*sinf(absxyt[2])-boffx*cosf(absxyt[2])-bore_height;
      maxx[0]=(term2[0]-term3[0]+guts_lim*cam_lift)/sin(absxyt[2])
        -mod_bore_height*sinf(absxyt[2])-boffx*cosf(absxyt[2])-bore_height;
    }
    if (fabsf(sinbm)<1.0e-15) {
      maxx[1]=1.0e9;
      minx[1]=-1.0e9;
    } else {
      minx[1]=(term3[1]-term2[1]-guts_lim*cam_lift)/sinbm
        -xoffs[2]-boffx*(cosf(absxyt[2])-1)-mod_bore_height*sinf(absxyt[2]);
      maxx[1]=(term3[1]-term2[1]+guts_lim*cam_lift)/sinbm
        -xoffs[2]-boffx*(cosf(absxyt[2])-1)-mod_bore_height*sinf(absxyt[2]);
    }
    if (fabsf(cosbm)<1e-15) {
      maxx[2]=1.0e9;
      minx[2]=-1.0e9;
    } else {
      minx[2]=(term2[2]-term3[2]-guts_lim*cam_lift)/cosbm
        +xoffs[2]-boffx*(cosf(absxyt[2])-1)-mod_bore_height*sinf(absxyt[2]);
      maxx[2]=(term2[2]-term3[2]+guts_lim*cam_lift)/cosbm
        +xoffs[2]-boffx*(cosf(absxyt[2])-1)-mod_bore_height*sinf(absxyt[2]);
    }
/* ylimits */    
    if (fabsf(cosf(absxyt[2]))<1.0e-15) {
      maxy[0]=1e9;
      miny[0]=-1e9;
    } else {
      miny[0]=(term1[0]+term3[0]-guts_lim*cam_lift)/cosf(absxyt[3])
        +mod_bore_height*cos(absxyt[2])-boffx*sinf(absxyt[2])-bore_height;
      maxy[0]=(term1[0]+term3[0]+guts_lim*cam_lift)/cosf(absxyt[3])
        +mod_bore_height*cos(absxyt[2])-boffx*sinf(absxyt[2])-bore_height;
    }
    if (fabsf(cosbm)<1e-15) {
      maxy[1]=1.0e9;
      miny[1]=-1.0e9;
    } else {
      miny[1]=(term3[1]-term1[1]-guts_lim*cam_lift)/cosbm
        +mod_bore_height*cosf(absxyt[2])-boffx*sinf(absxyt[2])-bore_height;
      maxy[1]=(term3[1]-term1[1]+guts_lim*cam_lift)/cosbm
        +mod_bore_height*cosf(absxyt[2])-boffx*sinf(absxyt[2])-bore_height;
    }
    if (fabsf(sinbm)<1e-15) {
      maxy[2]=1.0e9;
      miny[2]=-1.0e9;
    } else {
      miny[2]=(term1[2]+term3[2]-guts_lim*cam_lift)/sinbm
        +mod_bore_height*cosf(absxyt[2])-boffx*sinf(absxyt[2])-bore_height;
      maxy[2]=(term1[2]+term3[2]+guts_lim*cam_lift)/sinbm
        +mod_bore_height*cosf(absxyt[2])-boffx*sinf(absxyt[2])-bore_height;
    }
/* tilt limits */
  foundmin[0]=0;foundmin[1]=0;foundmin[2]=0;
  foundmax[0]=0;foundmax[1]=0;foundmax[2]=0;
  for(ii=-10000;ii<10000;ii+=5) {
    alfa=ii*1.0e-6;
		sinbma=(cosf(alfa)-sinf(alfa))/sqrt(2.0);
		cosbma=(cosf(alfa)+sinf(alfa))/sqrt(2.0);
		dx_inta=absxyt[0]+boffx*cosf(alfa)+mod_bore_height*sinf(alfa)-boffx;
		dy_inta=absxyt[1]-mod_bore_height*cosf(alfa)+boffx*sinf(alfa)+bore_height;

    term1a[0]=(xoffs[0]+dx_inta)*sinf(alfa);
    term2a[0]=dy_inta*cosf(alfa);
    term3a[0]=bore_height-mod_bore_height;    
		gutsa[0]=(term1a[0]-term2a[0]+term3a[0])/cam_lift;
	
    term1a[1]=(xoffs[2]+dx_inta)*sinbma;
    term2a[1]=dy_inta*cosbma;
    term3a[1]=cam_radius;
		gutsa[1]=(term1a[1]+term2a[1]-term3a[1])/cam_lift;	
		
		term1a[2]=(dx_inta-xoffs[2])*cosbma;
		term2a[2]=dy_inta*sinbma;
		term3a[2]=cam_radius;
		gutsa[2]=(term1a[2]-term2a[2]+term3a[2]) / cam_lift;		
    
    for (nn=0;nn<3;nn++) {
      if(fabsf(gutsa[nn])<guts_lim && !foundmin[nn]) {
        mina[nn]=alfa;
        foundmin[nn]=1;
      }
      if (!foundmax[nn] && gutsa[nn]>guts_lim) {
        maxa[nn]=alfa-5e-6;
        foundmax[nn]=1;
      }
    }

  }  
  /* need to check for wacky values in all 6 array - values >10^4 for 
  example */
  
  if (DEBUGL) {
    printf("minx %10.3e %10.3e %10.3e ",minx[0], minx[1], minx[2]);
    printf("maxx %10.3e %10.3e %10.3e\n",maxx[0], maxx[1], maxx[2]);
    printf("miny %10.3e %10.3e %10.3e ",miny[0], miny[1], miny[2]);
    printf("maxy %10.3e %10.3e %10.3e\n",maxy[0], maxy[1], maxy[2]);
    printf("mina %10.3e %10.3e %10.3e ",mina[0], mina[1], mina[2]);
    printf("maxa %10.3e %10.3e %10.3e\n",maxa[0], maxa[1], maxa[2]);
  }    
  for(nn=0;nn<3;nn++) {
    minx[nn]=minx[nn]-absxyt[0];
    maxx[nn]=maxx[nn]-absxyt[0];
    miny[nn]=miny[nn]-absxyt[1];
    maxy[nn]=maxy[nn]-absxyt[1];
    mina[nn]=mina[nn]-absxyt[2];
    maxa[nn]=maxa[nn]-absxyt[2];
    if (fabsf(minx[nn])>xlim) minx[nn]=-1e9;
    if (fabsf(maxx[nn])>xlim) maxx[nn]=1e9;
    if (fabsf(miny[nn])>ylim) miny[nn]=-1e9;
    if (fabsf(maxy[nn])>ylim) maxy[nn]=1e9;
    if (fabsf(mina[nn])>alim) mina[nn]=-1e9;
    if (fabsf(maxa[nn])>alim) maxa[nn]=1e9;
  }
  if (DEBUGL) {
    printf("minx %e %e %e ",minx[0], minx[1], minx[2]);
    printf("maxx %e %e %e\n",maxx[0], maxx[1], maxx[2]);
    printf("miny %e %e %e ",miny[0], miny[1], miny[2]);
    printf("maxy %e %e %e\n",maxy[0], maxy[1], maxy[2]);
    printf("mina %e %e %e ",mina[0], mina[1], mina[2]);
    printf("maxa %e %e %e\n",maxa[0], maxa[1], maxa[2]);
  }    
  minxyt[0]=(minx[0]>minx[1])? ((minx[2]>minx[0])? minx[2] : minx[0]) 
                             : ((minx[2]>minx[1])? minx[2] : minx[1]);
  minxyt[1]=(miny[0]>miny[1])? ((miny[2]>miny[0])? miny[2] : miny[0]) 
                             : ((miny[2]>miny[1])? miny[2] : miny[1]);  
  minxyt[2]=(mina[0]>mina[1])? ((mina[2]>mina[0])? mina[2] : mina[0]) 
                             : ((mina[2]>mina[1])? mina[2] : mina[1]);  
  maxxyt[0]=(maxx[0]<maxx[1])? ((maxx[2]<maxx[0])? maxx[2] : maxx[0]) 
                             : ((maxx[2]<maxx[1])? maxx[2] : maxx[1]);
  maxxyt[1]=(maxy[0]<maxy[1])? ((maxy[2]<maxy[0])? maxy[2] : maxy[0]) 
                             : ((maxy[2]<maxy[1])? maxy[2] : maxy[1]);
  maxxyt[2]=(maxa[0]<maxa[1])? ((maxa[2]<maxa[0])? maxa[2] : maxa[0]) 
                             : ((maxa[2]<maxa[1])? maxa[2] : maxa[1]);
      
/*===========*/
/* let's do some rounding */
  dumfrac=modf((double)(minxyt[0]+10.0)/10.0,&dumint);
  minxyt[0]=dumint*10;
  dumfrac=modf((double)(maxxyt[0]-10.0)/10.0,&dumint);
  maxxyt[0]=dumint*10;
  dumfrac=modf((double)(minxyt[1]+10.0)/10.0,&dumint);
  minxyt[1]=dumint*10;
  dumfrac=modf((double)(maxxyt[1]-10.0)/10.0,&dumint);
  maxxyt[1]=dumint*10;
  dumfrac=modf((double)(minxyt[2]*1.0e6+10.0)/10.0,&dumint);
  minxyt[2]=dumint*10*1.0e-6;
  dumfrac=modf((double)(maxxyt[2]*1.0e6-10.0)/10.0,&dumint);
  maxxyt[2]=dumint*10*1.0e-6;
  
	psub->g=minxyt[0];
	psub->h=maxxyt[0];
	psub->i=minxyt[1];
	psub->j=maxxyt[1];
	psub->k=minxyt[2]*1.0e6;
	psub->l=maxxyt[2]*1.0e6;
	if (DEBUGL) {
		printf("mins %e %e %e\n",minxyt[0],minxyt[1],minxyt[2]);
		printf("maxx %e %e %e\n",maxxyt[0],maxxyt[1],maxxyt[2]);
		printf("psubs %e %e %e\n",psub->g,psub->h,psub->i);
	}	
	
	if (VERBOSE) printf("leaving readback2Lim sub\n");
	return OK;
}
/* =================================================================== */

long subPot2Lvdt(struct subRecord *psub) {
  /*
   * Calculate lvdt peds such that LVDT xyt equal pot xyt
   *
   *
   * Inputs:
   *   INPA = current positions subroutine "funnel"
   *      A=x:pot
   *      B=y:pot
   *      C=tilt:pot
   *      D=x:lvdt
   *      E=y:lvdt
   *      F=tilt:lvdt
   *      G=potlvdt (0=pot)
   *      H=lvdt1:X (constant)
   *      I=lvdt2:X
   *      J=lvdt3:Y
   *      K=bw (backwards? 1=no, -1=yes)
   *   INPB = subLvdt1 (constants for raw->mm lvdts)
   *      A=lvdt1:raw
   *      B=lvdt1:n1
   *      C=lvdt1:n2
   *      D=lvdt1:n3
   *      E=lvdt1:n4
   *      F=lvdt1:cal
   *      G=lvdt1:ped
   *   INPC = sublvdt2
   *   INPD = sublvdt3
   *
   * Outputs:
   *   INPE = lvdt1:ped
   *   INPF = lvdt2:ped
   *   INPG = lvdt3:ped
   *
   * Returns -1 on error, 0 if OK.
   */  
 
	if (DEBUGPL) printf("Welcome to pot2lvdt sub!\n");
/* outside functions */
   int solve_cubic(double a, double  b, double c, 
      double *x0,double *x1,double *x2);
/* Innies */
	struct subRecord *psubPos = 
		(struct subRecord *)(unsigned long)psub->a;
	struct subRecord *psubLvdt1 = 
		(struct subRecord *)(unsigned long)psub->b;
	struct subRecord *psubLvdt2 = 
		(struct subRecord *)(unsigned long)psub->c;
	struct subRecord *psubLvdt3 = 
		(struct subRecord *)(unsigned long)psub->d;
        double xytpot[3],xytlvdt[3],lvdt1x,lvdt2x,lvdt3y;
        double raw[3],n1[3],n2[3],n3[3],n4[3],cal[3],oldped[3],bw;
	int	potlvdt; 

/* Outies */
	double newped[3];
	
/* local vars */
      double potxyt[3],lvdt[3],testval[3];
      double A[3],B[3],C[3],root1[3],root2[3],root3[3];
      int ii,nroots[3];
	
	
   xytpot[0]=(double)psubPos->a;
   xytpot[1]=(double)psubPos->b;
   xytpot[2]=(double)psubPos->c;
   if (DEBUGPL) printf("pot2lvdt: xytpot %g %g %g\n",xytpot[0],xytpot[1],xytpot[2]);
   xytlvdt[0]=(double)psubPos->d;
   xytlvdt[1]=(double)psubPos->e;
   xytlvdt[2]=(double)psubPos->f;
   if (DEBUGPL) printf("pot2lvdt: xytlvdt %g %g %g\n",xytlvdt[0],xytlvdt[1],xytlvdt[2]);
   potlvdt=(int)psubPos->g;
   lvdt1x=(double)psubPos->h;
   lvdt2x=(double)psubPos->i;
   lvdt3y=(double)psubPos->j;
   bw=(double)psubPos->k;
   if (DEBUGPL) printf("pot2lvdt:potlvdt %d lvdtoffs %g %g %g bw %g\n",
      potlvdt,lvdt1x,lvdt2x,lvdt3y,bw);

   raw[0]=(double)psubLvdt1->a;
   n1[0]=(double)psubLvdt1->b;
   n2[0]=(double)psubLvdt1->c;
   n3[0]=(double)psubLvdt1->d;
   n4[0]=(double)psubLvdt1->e;
   cal[0]=(double)psubLvdt1->f;
   oldped[0]=(double)psubLvdt1->g;

   raw[1]=(double)psubLvdt2->a;
   n1[1]=(double)psubLvdt2->b;
   n2[1]=(double)psubLvdt2->c;
   n3[1]=(double)psubLvdt2->d;
   n4[1]=(double)psubLvdt2->e;
   cal[1]=(double)psubLvdt2->f;
   oldped[1]=(double)psubLvdt2->g;

   raw[2]=(double)psubLvdt3->a;
   n1[2]=(double)psubLvdt3->b;
   n2[2]=(double)psubLvdt3->c;
   n3[2]=(double)psubLvdt3->d;
   n4[2]=(double)psubLvdt3->e;
   cal[2]=(double)psubLvdt3->f;
   oldped[2]=(double)psubLvdt3->g;

/* step 0: Wish you had never been born */
/* step 1: use potlvdt to figure out pot xyts */
  for(ii=0;ii<3;ii++){
     potxyt[ii]=xytpot[ii];
  }
  potxyt[0]=potxyt[0]/bw;
  potxyt[2]=potxyt[2]*1.0e-6/bw;

   if (DEBUGPL) printf("pot2lvdt: potxyt[0] %e potxyt[1] %e potxyt[2] %e\n", 
      potxyt[0],potxyt[1],potxyt[2]);

/* step 2: calculate lvdt 1-3 readings from potxyt 
   lvdt1=y+1/2*tan(th)*(xoffs1+xoffs2)
   lvdt2=y-1/2*tan(th)*(xoffs1+xoffs2)
   lvdt3=x+tan(th)*(y+yoffs3)
*/

  lvdt[0]=potxyt[1]+(tan(potxyt[2])*(lvdt1x+lvdt2x))/2.0;   
  lvdt[1]=potxyt[1]-(tan(potxyt[2])*(lvdt1x+lvdt2x))/2.0;
  lvdt[2]=potxyt[0]+(tan(potxyt[2])*(potxyt[1]+lvdt3y));

   if (DEBUGPL) printf("pot2lvdt: lvdt[0] %e lvdt[1] %e lvdt[2] %e\n", 
      lvdt[0],lvdt[1],lvdt[2]);

/* step 3: calculate coefficients for cubic poly to solve 
 from db: lvdt=cal+n1+n2*(raw-ped)+n3*(raw-ped)^2+n4*(raw-ped)^3;
 x^3+n3/n4*x^2+n2/n4*x+(n1+cal-lvdt)/n4=0, where x=(raw-ped)
 do this 3 times
*/
   for(ii=0;ii<3;ii++){
      A[ii]=n3[ii]/n4[ii];
      B[ii]=n2[ii]/n4[ii];
      C[ii]=(n1[ii]+cal[ii]-lvdt[ii])/n4[ii];
      nroots[ii]=
         solve_cubic(A[ii],B[ii],C[ii],&root1[ii],&root2[ii],&root3[ii]);
   }
   if (DEBUGPL) printf("pot2lvdt: nroots %d %d %d\n",
      nroots[0],nroots[1],nroots[2]);

   for(ii=0;ii<3;ii++){
      if (nroots[ii]==1)
         newped[ii]=root1[ii]-raw[ii];
      else {
         if (nroots[ii]==3) {
            testval[0]=root1[ii]-raw[ii];
            testval[1]=root2[ii]-raw[ii];
            if (abs(testval[1])<abs(testval[0])) {
              testval[0]=testval[1];
            }
            testval[2]=root3[ii]-raw[ii];
            if (abs(testval[2])<abs(testval[0])) {
              testval[0]=testval[2];
            }
            newped[ii]=testval[0]; /*this was root1-raw*/
         }
         else {
            printf("nroots(not 1 or 3)=%d\n",nroots[ii]);
            return ERROR;
         }
      }
      if (DEBUGPL) printf("pot2lvdt: LVDT%d nroots=%d %8.4f %8.4f %8.4f \n",
        ii,nroots[ii],root1[ii],root2[ii],root3[ii]);
   }

   if (DEBUGPL) printf("pot2lvdt: newped %e %e %e\n",
      -newped[0],-newped[1],-newped[2]);

   psub->e=-newped[0];
   psub->f=-newped[1];
   psub->g=-newped[2];	

   if (DEBUGPL) printf("Leaving pot2lvdt, TTFN\n");
   return OK;
}
/*==============================================================================*/

epicsRegisterFunction(subQmovInit);
epicsRegisterFunction(subQmovDumb);
/* epicsRegisterFunction(subQmovDumb2); */
epicsRegisterFunction(subreadback2Val);
epicsRegisterFunction(subcmd2MotorStep);
epicsRegisterFunction(subreadback2Lim);
epicsRegisterFunction(subPot2Lvdt);


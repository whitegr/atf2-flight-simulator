/*
 * pcc.c : Linux driver for PCI / pipeline CAMAC controller
 * Copyright 2003 (C) 2003 Yoshiji Yasu <Yoshiji.YASU@kek.jp>.
 *
 * version: 0.1  04-DEC-2002, born
 *          0.9  11-AUG-2003  release for beta version
 *                              LAM & TRIG handling are not available.
 *          0.91 25-SEP-2003  bug fixed
 *          0.95 03-OCT-2003  modification of LAM & TRIG handler
 *          0.96 07-NOV-2003  bug fix for PCCIOC_EXEC_PIO 
 *          0.97 19-DEC-2003  bug fix for PCCIOC_ENABLE_INTERRUPT 
*/

#include <linux/module.h>
#include <linux/version.h>
#include <linux/pci.h>
#include <asm/uaccess.h>
#include <linux/mm.h>
#include <linux/poll.h>
#include <asm/io.h>
#include "pcc.h"

struct pcc_device {
  char *io_base;
  unsigned int irq;
  char *name;
#if LINUX_VERSION_CODE >= KERNEL_VERSION(2,4,0)
  wait_queue_head_t waitqTx;
  wait_queue_head_t waitqRx;
  wait_queue_head_t waitqInt;
#else
  struct wait_queue *waitqTx;
  struct wait_queue *waitqRx;
  struct wait_queue *waitqInt;
#endif
  struct pccreg pccreg;
  int statusRxInt, statusTxInt, statusIntInt;
  int int_write_counter;
  int int_read_counter;
  int read_flag;
  int dma_flag;
};

static int needed_pages;
static struct pcc_device pccdev; // Driver database for P-CC
static char *read_data_buf;
static char *write_data_buf;
static int *cmdbuf;
static int *rplybuf;
static struct pci_dev *pcidev_save;
#ifdef MEASUREMENT
static struct timeval  tv[4];
#endif
static int timeout, timeout_flag;

static inline int __get_order(unsigned long size)
{
  int order;

  size = (size-1) >> (PAGE_SHIFT-1);
  order = -1;
  do {
    size >>= 1;
    order++;
  } while (size);
  return order;
}

void dump_reg(void) {
  printk("Tx Control      = %x\n", inl(pccdev.pccreg.TxControl));
  printk("Tx Status       = %x\n", inl(pccdev.pccreg.TxStatus));
  printk("Tx Address      = %x\n", inl(pccdev.pccreg.TxAddress));
  printk("Tx Preset Count = %x\n", inl(pccdev.pccreg.TxPresetCount));
  printk("Tx Actual Count = %x\n", inl(pccdev.pccreg.TxActualCount));
  printk("Tx FIFO Count   = %x\n", inl(pccdev.pccreg.TxFifoCount));
  printk("Rx Control      = %x\n", inl(pccdev.pccreg.RxControl));
  printk("Rx Status       = %x\n", inl(pccdev.pccreg.RxStatus));
  printk("Rx Address      = %x\n", inl(pccdev.pccreg.RxAddress));
  printk("Rx Preset Count = %x\n", inl(pccdev.pccreg.RxPresetCount));
  printk("Rx Actual Count = %x\n", inl(pccdev.pccreg.RxActualCount));
  printk("Rx FIFO Count   = %x\n", inl(pccdev.pccreg.RxFifoCount));
  printk("System          = %x\n", inl(pccdev.pccreg.System));
  printk("Int Control      = %x\n", inl(pccdev.pccreg.IntControl));
  printk("Int Status       = %x\n", inl(pccdev.pccreg.IntStatus));
  printk("Int FIFO Count   = %x\n", inl(pccdev.pccreg.IntFifoCount));
}


void clearIntRxAll(struct pcc_device *own_pccdev) {
  int data;

  data = inl(own_pccdev->pccreg.RxControl);
  data |= RC_INT_CLR;
  outl(data, own_pccdev->pccreg.RxControl);
}

void clearIntTxAll(struct pcc_device *own_pccdev) {
  int data;

  data = inl(own_pccdev->pccreg.TxControl);
  data |= TC_INT_CLR;
  outl(data, own_pccdev->pccreg.TxControl);
}


/* This is an interrupt method registered at loading this device driver.
 */ 
static void pcc_interrupt(int irq, struct pcc_device *own_pccdev, struct pt_regs *regs)
{
  int statusTx, statusRx, statusInt;

  statusTx = inl(own_pccdev->pccreg.TxStatus);
  statusRx = inl(own_pccdev->pccreg.RxStatus);
  statusInt = inl(own_pccdev->pccreg.IntStatus);
#ifdef DEBUG
  printk("pcc_interrupt: statusTx = %x statsuRx = %x statusInt = %x\n",
	 statusTx, statusRx, statusInt);
#endif
  if( irq != own_pccdev->irq ) { // unexpected interrupt
    outl(TC_INT_CLR, own_pccdev->pccreg.TxControl);
    outl(RC_INT_CLR, own_pccdev->pccreg.RxControl);
    outl(IC_INT_CLR, own_pccdev->pccreg.IntControl);
    own_pccdev->statusTxInt = statusTx;
    own_pccdev->statusRxInt = statusRx;
    own_pccdev->statusIntInt = statusInt;
    printk("unexpected interrupt...\n");
    return;
  }
  if (  pccdev.dma_flag ) { // DMA is on going
    if( statusTx & TS_INT ) {
#ifdef MEASUREMENT
      do_gettimeofday(&tv[1]);
#endif
      if( statusTx & TS_INT_PKT_END ) {
	own_pccdev->int_write_counter = 1;
	//	printk("pcc_interrupt:tx TS_INT_PKT_END\n");
#ifdef DEBUG
	printk("pcc_interrupt:tx TS_INT_PKT_END\n");
#endif
	wake_up_interruptible(&own_pccdev->waitqTx);
      } else if( statusTx & TS_INT_FORCE_END ) {
	own_pccdev->int_write_counter = 2;
#ifdef DEBUG
	printk("pcc_interrupt:tx TS_INT_FORCE_END\n");
#endif
	wake_up_interruptible(&own_pccdev->waitqTx);
      }
      outl(TC_INT_CLR, own_pccdev->pccreg.TxControl);
    } 
    if (statusRx & RS_INT) {
      if( statusRx & RS_INT_PKT_END ) {
	own_pccdev->int_read_counter = 1;
#ifdef DEBUG
	printk("pcc_interrupt:rx RS_INT_PKT_END\n");
#endif
	wake_up_interruptible(&own_pccdev->waitqRx);
      } else if( statusRx & RS_INT_FORCE_END ) {
	own_pccdev->int_read_counter = 2;
#ifdef DEBUG
	printk("pcc_interrupt:rx RS_INT_FORCE_END\n");
#endif
	wake_up_interruptible(&own_pccdev->waitqRx);
      }
      outl(RC_INT_CLR, own_pccdev->pccreg.RxControl);
    }
  } else {
    if( statusInt & IS_INT ) {
      own_pccdev->int_read_counter = 3;
      
      outl(IC_INT_CLR, own_pccdev->pccreg.IntControl);
      //      outl(IC_INT_ENABLE | IC_INT_CLR, own_pccdev->pccreg.IntControl);
#ifdef DEBUG
      printk("pcc_interrupt:rx IS_INT\n");
#endif
      wake_up_interruptible(&own_pccdev->waitqInt);
    }
  }
  if ( !((statusRx & RS_INT) | (statusTx & TS_INT) | (statusInt & IS_INT)) ) {
    printk("No interrupt from PCC\n");
    own_pccdev->statusTxInt = statusTx;
    own_pccdev->statusRxInt = statusRx;
    own_pccdev->statusIntInt = statusInt;
    return ;
  }
  own_pccdev->statusTxInt = statusTx;
  own_pccdev->statusRxInt = statusRx;
  own_pccdev->statusIntInt = statusInt;

#ifdef DEBUG
  printk("pcc_interrupt: write interrupt counter = %x read interrupt counter = %x\n",
	 own_pccdev->int_write_counter, own_pccdev->int_read_counter);
#endif

#ifdef MEASUREMENT
  do_gettimeofday(&tv[2]);
#endif
  return;
}

/*
 * This is "open" sytem call interface.
 */ 
static int pcc_open(struct inode *inode, struct file * file)
{
  //  MOD_INC_USE_COUNT;
  return 0;
}

/*
 * This is "close" sytem call interface.
 */ 
static int pcc_release(struct inode * inode, struct file * file)
{
  //  MOD_DEC_USE_COUNT;
  return 0;
}


/*
 * This is "ioctl" system call interface.
 */
static int pcc_ioctl(struct inode *inode, struct file *file,
                    unsigned int cmd, unsigned long arg)
{
  int retval = 0;
  int status, i, j;
  int data, count;
  int buf[8];
  struct pccreg pccreg;
  unsigned long flags;
  int total_len, actual_len, cmd_len;

#ifdef DEBUG
  printk("ioctl:enter\n");
#endif
  switch ( cmd ) {
  case PCCIOC_TMP :
    data = 0xffffffff;
    outl(data, pccdev.pccreg.TxStatus);
    break;
  case PCCIOC_RESET :
    data = inl(pccdev.pccreg.System);
    data &= ~SYS_READY;
    outl(data, pccdev.pccreg.System);
    data |= SYS_READY;
    outl(data, pccdev.pccreg.System);
    break;
  case PCCIOC_CLEAR_FIFO:
    data = inl(pccdev.pccreg.TxControl);
    data |= TC_CLR_FIFO;
    outl(data, pccdev.pccreg.TxControl);
    data = inl(pccdev.pccreg.RxControl);
    data |= RC_CLR_FIFO;
    outl(data, pccdev.pccreg.RxControl);
    data = inl(pccdev.pccreg.IntControl);
    data |= IC_CLR_FIFO;
    outl(data, pccdev.pccreg.IntControl);
    break;
  case PCCIOC_PUT_DATA:
    if (copy_from_user(buf, (int *)arg, sizeof(int)*2)) {
      printk("ioctl:put_data:copy_from_user:error...\n");
      return -EFAULT;
    }
#ifdef DEBUG
    printk("ioctl:put data cmd = %x %x \n", buf[0], buf[1]);
#endif
    timeout = 0;
    do {
      status = inl(pccdev.pccreg.TxFifoCount);
      timeout++;
    } while ( (status > MAX_FIFO_FRAME-2) && (timeout != PCC_TIMEOUT_PIO ) );
    if( timeout == PCC_TIMEOUT_PIO ) {
      printk("ioctl:put_dat:timeout_pio:error...\n");
      return -ETIME;
    }
    outl(buf[0], pccdev.pccreg.TxData1);
    outl(buf[1], pccdev.pccreg.TxData2);
    break;
  case PCCIOC_GET_DATA:
    timeout = 0;
    do {
      status = inl(pccdev.pccreg.RxFifoCount);
      timeout++;
    } while ( (status < 2) && (timeout < PCC_TIMEOUT_PIO ));
    if( timeout == PCC_TIMEOUT_PIO ) {
      printk("ioctl:get_dat:timeout_pio:error...\n");
      return -ENODATA;
    }
    buf[0] = inl(pccdev.pccreg.RxData1);
    buf[1] = inl(pccdev.pccreg.RxData2);
#ifdef DEBUG
    printk("ioctl:get data reply = %x %x\n", buf[0], buf[1]);
#endif
    outl(RC_INT_CLR, pccdev.pccreg.RxControl);
    if ( copy_to_user((int *)arg, buf, sizeof(int)*2) ) {
      printk("ioctl:get_data:copy_to_user:error...\n");
      return -EFAULT;
    }
    break;
  case PCCIOC_GET_INTDATA:
    timeout = 0;
    do {
      status = inl(pccdev.pccreg.IntFifoCount);
      timeout++;
    } while ( (status < 2) && (timeout < PCC_TIMEOUT_PIO ));
    if( timeout == PCC_TIMEOUT_PIO ) {
      printk("ioctl:get_intdat:timeout_pio:error...\n");
      return -ENODATA;
    }
    buf[0] = inl(pccdev.pccreg.IntData1);
    buf[1] = inl(pccdev.pccreg.IntData2);
#ifdef DEBUG
    printk("ioctl:get intdata:reply = %x %x\n", buf[0], buf[1]);
#endif
    outl(IC_INT_CLR, pccdev.pccreg.RxControl);
    if ( copy_to_user((int *)arg, buf, sizeof(int)*2) ) {
      printk("ioctl:get_intdata:copy_to_user:error...\n");
      return -EFAULT;
    }
    break;
  case PCCIOC_EXEC_PIO:
    if ( copy_from_user(buf, (int *)arg, sizeof(int)*2) )
      return -EFAULT;
    if ( copy_from_user(cmdbuf, (int *)buf[0], sizeof(int)*2) )
      return -EFAULT;
    if ( copy_from_user(rplybuf, (int *)buf[1], sizeof(int)) )
      return -EFAULT;
    cmd_len = cmdbuf[1];
    total_len = cmdbuf[1];
    if( (cmd_len > total_len) || (cmd_len <= 0) || (cmd_len > MAX_BUFFER_SIZE/8) || (total_len <=0) )
      return -EINVAL;
    if ( copy_from_user(cmdbuf+2, (int *)(buf[0]+8), cmd_len*8) )
      return -EFAULT;
    i = j = 2;
    timeout = 0;
    do {
      timeout_flag = 1;
      if( j < cmd_len*2+2 ) {
	status = inl(pccdev.pccreg.TxFifoCount);
	status &= 0xFFFF;
	if( status < MAX_FIFO_FRAME - 2 ) {
	  outl(cmdbuf[j], pccdev.pccreg.TxData1);
	  outl(cmdbuf[j+1], pccdev.pccreg.TxData2);
	  j += 2;
	  timeout_flag = 0;
#ifdef DEBUG
	  printk("ioctl:PCCIOC_EXEC_PIO:write TX\n");
#endif
	}
      }
      status = inl(pccdev.pccreg.RxFifoCount);
      status &= 0xFFFF;
      if( status >= 2 ) {
	rplybuf[i] = inl(pccdev.pccreg.RxData1);
	rplybuf[i+1] = inl(pccdev.pccreg.RxData2);
	i += 2;
	timeout_flag = 0;
#ifdef DEBUG
	printk("ioctl:PCCIOC_EXEC_PIO:read RX\n");
#endif
      }
      if(timeout_flag==0)
	timeout = 0;
      else
	timeout++;
    } while ( (i < total_len*2+2) && (timeout != PCC_TIMEOUT_PIO*100 ) );
    if( timeout ==  PCC_TIMEOUT_PIO*100) {
#ifdef DEBUG
      printk("ioctl:PCCIOC_EXEC_PIO:timeout:total_len=%d,actual_len=%d\n",
	     total_len, i-2);
#endif
      return -ETIME;
    }
    actual_len = (i-2)/2;
    rplybuf[1] = actual_len;
    outl(RC_INT_CLR, pccdev.pccreg.RxControl);
    if( actual_len != 0 )
      if (copy_to_user((int *)buf[1], rplybuf, actual_len*8+8 ))
	return -EFAULT;
    break;
  case PCCIOC_KICK_READ:
    if ( copy_from_user(&count, (int *)arg, sizeof(int)) )
      return -EFAULT;
    if( count > MAX_BUFFER_SIZE || count < MIN_SIZE )
      return -EINVAL;
    pccdev.read_flag = 1;
    pccdev.dma_flag = 1;
    outl((RC_INT_ENABLE_FORCE_END|RC_INT_ENABLE_PKT_END), pccdev.pccreg.RxControl);
    outl( virt_to_phys(read_data_buf), pccdev.pccreg.RxAddress );
    outl( count/8, pccdev.pccreg.RxPresetCount);
    data = inl(pccdev.pccreg.RxControl);
    pccdev.int_read_counter = 0;
#ifdef POLLING
    data = RC_SRT_DMA;
#else
    data = RC_INT_ENABLE_FORCE_END|RC_INT_ENABLE_PKT_END|RC_SRT_DMA;
#endif
    outl(data, pccdev.pccreg.RxControl);
    break;
  case PCCIOC_DISABLE_INTERRUPT:
    outl(0, pccdev.pccreg.IntControl);
    pccdev.int_read_counter = 0;
    break;
  case PCCIOC_ENABLE_INTERRUPT:
    outl(IC_INT_CLR, pccdev.pccreg.IntControl);
    pccdev.int_read_counter = 0;
    outl(IC_INT_ENABLE, pccdev.pccreg.IntControl);
    break;
  case PCCIOC_WAIT_INTERRUPT:
    if ( copy_from_user(&data, (int *)arg, sizeof(int)) ) {
      printk("ioctl:wait_interrupt:copy_from_user error...\n");
      return -EFAULT;
    }
    if( data < 1 ) 
      timeout = PCC_TIMEOUT;
    else
      timeout = data;

    save_flags(flags);
    cli();
    if( pccdev.int_read_counter == 0 ) {
#ifdef DEBUG
      printk("ioctl:wait_interrupt:wait...\n");
#endif
      interruptible_sleep_on_timeout(&pccdev.waitqInt, timeout);
      if( pccdev.int_read_counter != 3 ) {
	outl(0, pccdev.pccreg.IntControl); // disable interrupt
	printk("ioctl:wait_interrupt:timeout occurred (%d ticks)...\n", timeout);
	restore_flags(flags);
	return -ETIME;
      }
    }
#ifdef DEBUG
    else {
      data = inl(pccdev.pccreg.RxFifoCount);
      printk("ioctl:wait_interrupt:Interrupt already occurred and rxfifocnt = %d\n", data);
    }
#endif
    data = inl(pccdev.pccreg.IntFifoCount);
    if( data >= 2 ) {
      buf[0] = inl(pccdev.pccreg.IntData1);
      buf[1] = inl(pccdev.pccreg.IntData2);
#ifdef DEBUG
      printk("ioctl:wait_interrupt:buf[0] = %x buf[1] = %x\n", buf[0], buf[1]);
#endif
      if ( copy_to_user((int *)arg, buf, sizeof(int)*2) ) {
	printk("ioctl:wait_interrupt:copy_to_user error...\n");
	restore_flags(flags);
	return -EFAULT;
      }
    } else {
      printk("ioctl:wait_interrupt:interrupt frame is not ready : read_counter = %d\n",
	     pccdev.int_read_counter );
      restore_flags(flags);
      return -EFAULT;
    }
    pccdev.int_read_counter = 0;
    restore_flags(flags);
    break;
  case PCCIOC_DUMP_REGISTERS:
    pccreg.TxControl  = inl(pccdev.pccreg.TxControl);
    pccreg.TxStatus  = inl(pccdev.pccreg.TxStatus);
    pccreg.TxAddress   = inl(pccdev.pccreg.TxAddress);
    pccreg.TxPresetCount = inl(pccdev.pccreg.TxPresetCount);
    pccreg.TxActualCount = inl(pccdev.pccreg.TxActualCount);
    pccreg.TxFifoCount = inl(pccdev.pccreg.TxFifoCount);
    pccreg.RxControl  = inl(pccdev.pccreg.RxControl);
    pccreg.RxStatus  = inl(pccdev.pccreg.RxStatus);
    pccreg.RxAddress   = inl(pccdev.pccreg.RxAddress);
    pccreg.RxPresetCount = inl(pccdev.pccreg.RxPresetCount);
    pccreg.RxActualCount = inl(pccdev.pccreg.RxActualCount);
    pccreg.RxFifoCount = inl(pccdev.pccreg.RxFifoCount);
    pccreg.System = inl(pccdev.pccreg.System);
    pccreg.IntControl  = inl(pccdev.pccreg.IntControl);
    pccreg.IntStatus  = inl(pccdev.pccreg.IntStatus);
    pccreg.IntFifoCount = inl(pccdev.pccreg.IntFifoCount);
    if (copy_to_user((int *)arg, &pccreg, sizeof(struct pccreg)))
      return -EFAULT;
    break;
  default:
    retval = -EINVAL;
  };
  return retval;
}


/*
 * This is "read" system call interface.
 * The return value is the actually transferred count in bytes.
 * FIFO size for Rx is 120 CAMAC transfers.
 */
static ssize_t pcc_read (struct file *file, char *buf, size_t count, 
			  loff_t *ppos)
{
  int length, data;
  unsigned long flags;

  if( count > MAX_BUFFER_SIZE || count < MIN_SIZE )
    return -EINVAL;
  save_flags(flags);
  cli();
  pccdev.dma_flag = 1;
  if (!pccdev.read_flag ) {
    pccdev.int_read_counter = 0;
    outl( virt_to_phys(read_data_buf), pccdev.pccreg.RxAddress );
    outl( count/8, pccdev.pccreg.RxPresetCount);
#ifdef POLLING
    data = RC_SRT_DMA;
#else
    data = RC_INT_ENABLE_FORCE_END|RC_INT_ENABLE_PKT_END|RC_SRT_DMA;
#endif
    outl(data, pccdev.pccreg.RxControl);
  }
  pccdev.read_flag = 0;

#ifndef POLLING
  if ( !pccdev.int_read_counter ) {
    interruptible_sleep_on_timeout(&pccdev.waitqRx, PCC_TIMEOUT);
    if ( pccdev.int_read_counter == 0 ) {
      printk("read:read count = 0\n");
      restore_flags(flags);
      return -ETIME;
    }
  }
  restore_flags(flags);
#else
  restore_flags(flags);
  i = 0;
  do {
    i++;
    data=inl(pccdev.pccreg.RxControl);
    if(!(data& RC_SRT_DMA)) {
	break;
    }
  } while(!(i==1000000 ) );
  if (i==1000000) {
    restore_flags(flags);
    return -ETIME;
  }
#endif

  pccdev.dma_flag = 0;
  pccdev.int_read_counter = 0;
  length = inl(pccdev.pccreg.RxActualCount);
  if ( copy_to_user(buf, read_data_buf, length*8) )
     return -EFAULT;
  return length*8;
}

/*
 * This is "write" system call interface.
 * The return value is the actually transferred count in bytes.
 * FIFO size for Tx is 120 CAMAC transfers.
 */
static ssize_t pcc_write (struct file *file, const char *buf, size_t count, loff_t *ppos)
{
  int length, data;
  unsigned long flags;

  if( count > MAX_BUFFER_SIZE || count < MIN_SIZE )
    return -EINVAL;
  data = inl(pccdev.pccreg.TxFifoCount);
  if( data != 0 ) // if TxFifoCount is not empty, tray again.
    return -EAGAIN;
  if ( copy_from_user(write_data_buf, buf, count) )
      return -EFAULT;
  outl( virt_to_phys(write_data_buf), pccdev.pccreg.TxAddress );
  outl( count/8, pccdev.pccreg.TxPresetCount);
  data = inl(pccdev.pccreg.TxControl);
  pccdev.int_write_counter = 0;
  pccdev.dma_flag = 1;
#ifdef POLLING
  data = TC_SRT_DMA;
#else
  data = TC_INT_ENABLE_FORCE_END|TC_INT_ENABLE_PKT_END|TC_SRT_DMA;
#endif
  save_flags(flags);
  cli();
  outl(data, pccdev.pccreg.TxControl);
#ifdef MEASUREMENT
  do_gettimeofday(&tv[0]);
#endif

#ifndef POLLING
  interruptible_sleep_on_timeout(&pccdev.waitqTx, PCC_TIMEOUT);
  if ( pccdev.int_write_counter == 0 ) {
    restore_flags(flags);
    return -ETIME;
  }
  restore_flags(flags);
#else
  restore_flags(flags);
  i = 0;
    do {
    i++;
    data=inl(pccdev.pccreg.TxControl);
    if(!(data& TC_SRT_DMA)) {
      data=inl(pccdev.pccreg.TxFifoCount);
      if(data == 0)
	break;
    }
  } while(!(i==1000000 ) );
  if (i==1000000) {
    restore_flags(flags);
    return -ETIME;
  }
#endif

#ifdef MEASUREMENT
  do_gettimeofday(&tv[3]);
#endif

  pccdev.dma_flag = 0;
  pccdev.int_write_counter = 0;
  length = inl(pccdev.pccreg.TxActualCount);
  return length*8;
}

int pcc_mmap( struct file *filp, struct vm_area_struct *vma ) {

  return 0;
}

static struct file_operations pcc_fops = {
ioctl:    pcc_ioctl,
open:     pcc_open,
release:  pcc_release,
read:	  pcc_read,
write:    pcc_write,
mmap:     pcc_mmap,
};

/*
 * This is called at loading this device driver by insmod command.
 */
int init_module(void)
{
  struct pci_dev *pcidev=NULL;
  int retval;

  if(!pci_present())
    return -ENODEV;

    pcidev = pci_find_device(PCI_VENDOR_ID_PCC,  PCI_DEVICE_ID_PCC, pcidev);
    if(pcidev == NULL)
      return -ENODEV;

#if LINUX_VERSION_CODE >= KERNEL_VERSION(2,4,0)
    pci_enable_device(pcidev);
    pccdev.io_base = (char *)pci_resource_start(pcidev, 0);
#else
    pccdev.io_base = (char *)(pcidev->base_address[0] & PCI_BASE_ADDRESS_MEM_MASK);
#endif
    pcidev_save = pcidev;
    printk("init_module:IO_BASE = %x\n", (int)pccdev.io_base);
    pccdev.irq = pcidev->irq;
    printk("init_module:irq number of PCC = %x\n", pccdev.irq);

    pccdev.pccreg.TxData1 = (int)(pccdev.io_base + TXDATA1);
    pccdev.pccreg.TxData2 = (int)(pccdev.io_base + TXDATA2);
    pccdev.pccreg.TxControl = (int)(pccdev.io_base + TXCONTROL);
    pccdev.pccreg.TxStatus = (int)(pccdev.io_base + TXSTATUS);
    pccdev.pccreg.TxAddress = (int)(pccdev.io_base + TXADDRESS);
    pccdev.pccreg.TxPresetCount = (int)(pccdev.io_base + TXPRESETCOUNT);
    pccdev.pccreg.TxActualCount = (int)(pccdev.io_base + TXACTUALCOUNT);
    pccdev.pccreg.TxFifoCount = (int)(pccdev.io_base + TXFIFOCOUNT);
    pccdev.pccreg.RxData1 = (int)(pccdev.io_base + RXDATA1);
    pccdev.pccreg.RxData2 = (int)(pccdev.io_base + RXDATA2);
    pccdev.pccreg.RxControl = (int)(pccdev.io_base + RXCONTROL);
    pccdev.pccreg.RxStatus = (int)(pccdev.io_base + RXSTATUS);
    pccdev.pccreg.RxAddress = (int)(pccdev.io_base + RXADDRESS);
    pccdev.pccreg.RxPresetCount = (int)(pccdev.io_base + RXPRESETCOUNT);
    pccdev.pccreg.RxActualCount = (int)(pccdev.io_base + RXACTUALCOUNT);
    pccdev.pccreg.RxFifoCount = (int)(pccdev.io_base + RXFIFOCOUNT);
    pccdev.pccreg.System = (int)(pccdev.io_base + SYSTEM);
    pccdev.pccreg.IntData1 = (int)(pccdev.io_base + INTDATA1);
    pccdev.pccreg.IntData2 = (int)(pccdev.io_base + INTDATA2);
    pccdev.pccreg.IntControl = (int)(pccdev.io_base + INTCONTROL);
    pccdev.pccreg.IntStatus = (int)(pccdev.io_base + INTSTATUS);
    pccdev.pccreg.IntFifoCount = (int)(pccdev.io_base + INTFIFOCOUNT);

    outl(SYS_RESET|SYS_CAMAC_FRAME_SIZE, pccdev.pccreg.System);
    outl(SYS_READY|SYS_CAMAC_FRAME_SIZE, pccdev.pccreg.System);

#ifdef PCC_IRQ_SHARED
    retval = request_irq(pccdev.irq, (void *)pcc_interrupt, 
			 SA_INTERRUPT|SA_SHIRQ, CARD_NAME, &pccdev);
#else
    retval = request_irq(pccdev.irq, (void *)pcc_interrupt, 
			 SA_INTERRUPT, CARD_NAME, &pccdev);
#endif
    if (retval){
      printk("init_module: interrupt registrationfault on level %d\n", 
	     pccdev.irq);
      return retval;
    }
#if LINUX_VERSION_CODE >= KERNEL_VERSION(2,4,0)
    init_waitqueue_head(&pccdev.waitqTx);
    init_waitqueue_head(&pccdev.waitqRx);
    init_waitqueue_head(&pccdev.waitqInt);
#else
    pccdev.waitqTx = NULL;
    pccdev.waitqRx = NULL;
    pccdev.waitqInt = NULL;
#endif

  needed_pages = __get_order(MAX_BUFFER_SIZE);
  write_data_buf = (char *)__get_free_pages(GFP_KERNEL, needed_pages);
  if( !write_data_buf ) {
    printk("init_module:cound not get free pages for write...\n");
    return(-ENOMEM);
  }
  read_data_buf = (char *)__get_free_pages(GFP_KERNEL, needed_pages);
  if( !read_data_buf ) {
    printk("init_module:cound not get free pages for read...\n");
    return(-ENOMEM);
  }

  cmdbuf = (int *)__get_free_pages(GFP_KERNEL, needed_pages);
  if( !cmdbuf ) {
    printk("init_module:cound not get free pages for cmdbuf...\n");
    return(-ENOMEM);
  }
  rplybuf = (int *)__get_free_pages(GFP_KERNEL, needed_pages);
  if( !rplybuf ) {
    printk("init_module:cound not get free pages for rplybuf...\n");
    return(-ENOMEM);
  }

  retval = register_chrdev(PCC_MAJOR,CARD_NAME,&pcc_fops);
  if (retval) {
    printk("init_module:unable to get major %d for PCC\n", PCC_MAJOR);
    return retval;
  }
  printk("PCC has been installed.\n");
  return 0;
}

/*
 * This is called at releasing this device driver by rmmod command.
 */
void cleanup_module(void)
{
  free_irq(pccdev.irq, &pccdev);
  unregister_chrdev(PCC_MAJOR,CARD_NAME);
  printk("PCC has been removed.\n");
}

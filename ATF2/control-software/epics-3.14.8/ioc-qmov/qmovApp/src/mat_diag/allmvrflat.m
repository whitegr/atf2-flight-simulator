for mm=1:16
  lcaPut(sprintf('c1:qmov:m%d:pot2step.PROC',mm),1)
  for nn=1:3
    ss=lcaGet(sprintf('c1:qmov:m%d:motor%d:sumsteps',mm,nn));
    lcaPut(sprintf('c1:qmov:m%d:motor%d:stepdiag',mm,nn),-1*ss)
  end
  pause(2)
  lcaPut(sprintf('c1:qmov:m%d:pot2lvdt.PROC',mm),1)
  disp(mm)
end

pvi='c1:qmov:m';
for mm=1:16
  stepdiag1{mm,1}=sprintf('%s%d:motor1:stepdiag',pvi,mm);
  stepdiag2{mm,1}=sprintf('%s%d:motor2:stepdiag',pvi,mm);
  stepdiag3{mm,1}=sprintf('%s%d:motor3:stepdiag',pvi,mm);
end



lcaPut(stepdiag1,-10000);
lcaPut(stepdiag2,15000);
lcaPut(stepdiag3,-15000);

for mm=1:16
  lcaPut(sprintf('c1:qmov:m%d:pot2step.PROC',mm),1)
  for nn=1:3
    ss(mm,nn)=lcaGet(sprintf('c1:qmov:m%d:motor%d:sumsteps',mm,nn));
  end
end


starttim=now;
%mm=17;
pvn=cell(23*28,1);
for mm=1:28
  pvn{1+(mm-1)*23}=sprintf('c1:qmov:m%d:x',mm);
  pvn{2+(mm-1)*23}=sprintf('c1:qmov:m%d:y',mm);
  pvn{3+(mm-1)*23}=sprintf('c1:qmov:m%d:tilt',mm);
  pvn{4+(mm-1)*23}=sprintf('c1:qmov:m%d:x:alt',mm);
  pvn{5+(mm-1)*23}=sprintf('c1:qmov:m%d:y:alt',mm);
  pvn{6+(mm-1)*23}=sprintf('c1:qmov:m%d:tilt:alt',mm);
  pvn{7+(mm-1)*23}=sprintf('c1:qmov:m%d:x:set',mm);
  pvn{8+(mm-1)*23}=sprintf('c1:qmov:m%d:y:set',mm);
  pvn{9+(mm-1)*23}=sprintf('c1:qmov:m%d:tilt:set',mm);
  pvn{10+(mm-1)*23}=sprintf('c1:qmov:m%d:dotrim.RVAL',mm);
  pvn{11+(mm-1)*23}=sprintf('c1:qmov:m%d:itry',mm);
  pvn{12+(mm-1)*23}=sprintf('c1:qmov:m%d:pot1',mm);
  pvn{13+(mm-1)*23}=sprintf('c1:qmov:m%d:pot2',mm);
  pvn{14+(mm-1)*23}=sprintf('c1:qmov:m%d:pot3',mm);
  pvn{15+(mm-1)*23}=sprintf('c1:qmov:m%d:lvdt1:raw',mm);
  pvn{16+(mm-1)*23}=sprintf('c1:qmov:m%d:lvdt2:raw',mm);
  pvn{17+(mm-1)*23}=sprintf('c1:qmov:m%d:lvdt3:raw',mm);
  pvn{18+(mm-1)*23}=sprintf('c1:qmov:m%d:lvdt1:ped',mm);
  pvn{19+(mm-1)*23}=sprintf('c1:qmov:m%d:lvdt2:ped',mm);
  pvn{20+(mm-1)*23}=sprintf('c1:qmov:m%d:lvdt3:ped',mm);
  pvn{21+(mm-1)*23}=sprintf('c1:qmov:m%d:lvdt1',mm);
  pvn{22+(mm-1)*23}=sprintf('c1:qmov:m%d:lvdt2',mm);
  pvn{23+(mm-1)*23}=sprintf('c1:qmov:m%d:lvdt3',mm);
  
end
ii=1;bigA=zeros(644,9000);timi=zeros(9000,1);
while ii<9001
  %get all current values
  foo=lcaGet(pvn);
  bigA(:,ii)=foo(:,1);
  if mod(ii,100)==0
    disp(ii)
  end
  timi(ii)=now;
  pause(1)
  ii=ii+1;
end
stoptim=now;
tt=clock;
fnam=sprintf('bigA_%02d%02d%02dT%02d%02d%02d.mat',...
  tt(1)-2000,round(tt(2:6)));

save(fnam)









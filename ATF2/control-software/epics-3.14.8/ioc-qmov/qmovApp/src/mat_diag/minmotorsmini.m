badmotors=[2 3 9 13 16 27];
pvi='c1:qmov:m';
pvn=cell(length(badmotors)*18,1);
movem1=cell(length(badmotors),1);
movem2=cell(length(badmotors),1);
movem3=cell(length(badmotors),1);
nstepm1=cell(length(badmotors),1);
nstepm2=cell(length(badmotors),1);
nstepm3=cell(length(badmotors),1);
ii=1;
for nn=1:length(badmotors)
  mm=badmotors(nn);%1:28
  pvn{ii,1}=sprintf('%s%d:pot1',pvi,mm);ii=ii+1;
  pvn{ii,1}=sprintf('%s%d:pot2',pvi,mm);ii=ii+1;
  pvn{ii,1}=sprintf('%s%d:pot3',pvi,mm);ii=ii+1;
  pvn{ii,1}=sprintf('%s%d:lvdt1',pvi,mm);ii=ii+1;
  pvn{ii,1}=sprintf('%s%d:lvdt2',pvi,mm);ii=ii+1;
  pvn{ii,1}=sprintf('%s%d:lvdt3',pvi,mm);ii=ii+1;
  pvn{ii,1}=sprintf('%s%d:lvdt1:raw',pvi,mm);ii=ii+1;
  pvn{ii,1}=sprintf('%s%d:lvdt2:raw',pvi,mm);ii=ii+1;
  pvn{ii,1}=sprintf('%s%d:lvdt3:raw',pvi,mm);ii=ii+1;
  pvn{ii,1}=sprintf('%s%d:x:pot',pvi,mm);ii=ii+1;
  pvn{ii,1}=sprintf('%s%d:y:pot',pvi,mm);ii=ii+1;
  pvn{ii,1}=sprintf('%s%d:tilt:pot',pvi,mm);ii=ii+1;
  pvn{ii,1}=sprintf('%s%d:x:lvdt',pvi,mm);ii=ii+1;
  pvn{ii,1}=sprintf('%s%d:y:lvdt',pvi,mm);ii=ii+1;
  pvn{ii,1}=sprintf('%s%d:tilt:lvdt',pvi,mm);ii=ii+1;
  pvn{ii,1}=sprintf('%s%d:motor1:sumsteps',pvi,mm);ii=ii+1;
  pvn{ii,1}=sprintf('%s%d:motor2:sumsteps',pvi,mm);ii=ii+1;
  pvn{ii,1}=sprintf('%s%d:motor3:sumsteps',pvi,mm);ii=ii+1;
  
  movem1{nn,1}=sprintf('%s%d:motor1:stepdiag',pvi,mm);
  movem2{nn,1}=sprintf('%s%d:motor2:stepdiag',pvi,mm);
  movem3{nn,1}=sprintf('%s%d:motor3:stepdiag',pvi,mm);
  nstepm1{nn,1}=sprintf('%s%d:motor1:nStepsRemain',pvi,mm);
  nstepm2{nn,1}=sprintf('%s%d:motor2:nStepsRemain',pvi,mm);
  nstepm3{nn,1}=sprintf('%s%d:motor3:nStepsRemain',pvi,mm);
end


for mm=1:length(badmotors)
  nn=badmotors(mm);%1:28
  cmd=sprintf('lcaPut(''%s%d:motor1:stepdiag'',-10000)',pvi,nn);
  eval(cmd)
   cmd=sprintf('lcaPut(''%s%d:motor2:stepdiag'',15000)',pvi,nn);
   eval(cmd)
   cmd=sprintf('lcaPut(''%s%d:motor3:stepdiag'',-15000)',pvi,nn);
   eval(cmd)
end
pause(0.5)
while (sum(lcaGet(nstepm1))+sum(lcaGet(nstepm2))+sum(lcaGet(nstepm3)))~=0
  pause(1.0)
  fprintf(1,'. ');
end
fprintf(1,'\n');
pause(7)

tic
ii=1;
foo=lcaGet(pvn);
dater(:,ii)=foo(:,1);
lcaPut(movem1,-1000);
pause(0.5)
while (sum(lcaGet(nstepm1)))~=0
  pause(0.5)
  disp('pause')
end
disp('done')
pause(7)
ii=ii+1;
foo=lcaGet(pvn);
dater(:,ii)=foo(:,1);
for nn=1:40
  lcaPut(movem1,50);
  pause(0.5)
  while (sum(lcaGet(nstepm1)))~=0
    pause(0.5)
  end
  pause(7)
  ii=ii+1;
  foo=lcaGet(pvn);
  dater(:,ii)=foo(:,1);
  fprintf(1,'%d ',nn);
end
lcaPut(movem1,-1000);
fprintf(1,'\n');
toc

%dater2=dater;
%pot1=dater2(1,:);
%pot2=dater2(2,:);
%sumstep1=dater2(3,:);
%lvdt1=dater2(4,:);
%lvdt2=dater2(5,:);
%lvdt3=dater2(6,:);
%potx=dater2(7,:);
%poty=dater2(8,:);
%pott=dater2(9,:);
%lvdtx=dater2(10,:);
%lvdty=dater2(11,:);
%lvdtt=dater2(12,:);
% lvdt1r=dater2(13,:);
% lvdt2r=dater2(14,:);
% lvdt3r=dater2(15,:);
%those that show min/max behavior for motor1
%
stepm1=dater(16:18:length(pvn),:);
lvdt2s=dater(5:18:length(pvn),:);
for nn=1:length(badmotors)
  [Q(:,nn),dQ(:,nn)]=plot_parab(stepm1(nn,:),lvdt2s(nn,:),1);
  title(sprintf('%d',nn))
  pause
end


for nn=1:length(badmotors)
  newval=Q(2,nn);
  oldval=lcaGet(sprintf('%s%d:motor1:sumsteps',pvi,nn));
  ov(nn)=oldval;nv(nn)=newval;
  lcaPut(movem1{nn},round(newval-oldval))
end
while (sum(lcaGet(nstepm1))+sum(lcaGet(nstepm2))+sum(lcaGet(nstepm3)))~=0
  pause(0.5)
  fprintf(1,'. ');
end
fprintf(1,'\n');
pause(1)

lcaPut(movem1,ones(length(badmotors),1)*10000)
while (sum(lcaGet(nstepm1))+sum(lcaGet(nstepm2))+sum(lcaGet(nstepm3)))~=0
  pause(0.5)
  fprintf(1,'. ');
end
fprintf(1,'\n');
pause(7)

pot1s=cell(28,1);cam1zs=cell(28,1);
for nn=1:28
  pot1s{nn,1}=sprintf('%s%d:pot1',pvi,nn);
  cam1zs{nn,1}=sprintf('%s%d:cam1:potZAngle',pvi,nn);
end
for nn=1:60
  vals(nn,:)=lcaGet(pot1s);
  pause(1.0)
  fprintf(1,'%d ',60-nn);
end
fprintf(1,'\n')

potv=mean(vals);
potv=potv';
potv=potv-ones(28,1)*5;
oldzs=lcaGet(cam1zs);
valerrs=std(vals);
valm=mean(vals);
plot_bars(1:28,valm,valerrs,'ro')
grid

figure
plot(potv,'.-')
hold on
plot(oldzs,'r.-')
grid

lcaPut(cam1zs,potv);



























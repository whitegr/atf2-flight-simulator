pvi='c1:qmov:m';
pvn=cell(28*18,1);
movem1=cell(28,1);
movem2=cell(28,1);
movem3=cell(28,1);
nstepm1=cell(28,1);
ii=1;
for mm=1:28
  pvn{ii,1}=sprintf('%s%d:pot1',pvi,mm);ii=ii+1;
  pvn{ii,1}=sprintf('%s%d:pot2',pvi,mm);ii=ii+1;
  pvn{ii,1}=sprintf('%s%d:pot3',pvi,mm);ii=ii+1;
  pvn{ii,1}=sprintf('%s%d:lvdt1',pvi,mm);ii=ii+1;
  pvn{ii,1}=sprintf('%s%d:lvdt2',pvi,mm);ii=ii+1;
  pvn{ii,1}=sprintf('%s%d:lvdt3',pvi,mm);ii=ii+1;
  pvn{ii,1}=sprintf('%s%d:lvdt1:raw',pvi,mm);ii=ii+1;
  pvn{ii,1}=sprintf('%s%d:lvdt2:raw',pvi,mm);ii=ii+1;
  pvn{ii,1}=sprintf('%s%d:lvdt3:raw',pvi,mm);ii=ii+1;
  pvn{ii,1}=sprintf('%s%d:x:pot',pvi,mm);ii=ii+1;
  pvn{ii,1}=sprintf('%s%d:y:pot',pvi,mm);ii=ii+1;
  pvn{ii,1}=sprintf('%s%d:tilt:pot',pvi,mm);ii=ii+1;
  pvn{ii,1}=sprintf('%s%d:x:lvdt',pvi,mm);ii=ii+1;
  pvn{ii,1}=sprintf('%s%d:y:lvdt',pvi,mm);ii=ii+1;
  pvn{ii,1}=sprintf('%s%d:tilt:lvdt',pvi,mm);ii=ii+1;
  pvn{ii,1}=sprintf('%s%d:motor1:sumsteps',pvi,mm);ii=ii+1;
  pvn{ii,1}=sprintf('%s%d:motor2:sumsteps',pvi,mm);ii=ii+1;
  pvn{ii,1}=sprintf('%s%d:motor3:sumsteps',pvi,mm);ii=ii+1;
  
  movem1{mm,1}=sprintf('%s%d:motor1:stepdiag',pvi,mm);
  movem2{mm,1}=sprintf('%s%d:motor2:stepdiag',pvi,mm);
  movem3{mm,1}=sprintf('%s%d:motor3:stepdiag',pvi,mm);
  nstepm1{mm,1}=sprintf('%s%d:motor1:nStepsRemain',pvi,mm);
  nstepm2{mm,1}=sprintf('%s%d:motor2:nStepsRemain',pvi,mm);
  nstepm3{mm,1}=sprintf('%s%d:motor3:nStepsRemain',pvi,mm);
end
pause(60)
disp('start iterations')
for JJ=1:5
  fprintf(1,'iteration %d\n',JJ);
  for nn=1:28
    cmd=sprintf('lcaPut(''%s%d:motor1:stepdiag'',-10000)',pvi,nn);
    eval(cmd)
    pause(1)
    cmd=sprintf('lcaPut(''%s%d:motor2:stepdiag'',15000)',pvi,nn);
    eval(cmd)
    pause(1)
    cmd=sprintf('lcaPut(''%s%d:motor3:stepdiag'',-15000)',pvi,nn);
    eval(cmd)
    pause(1)
  end
  pause(0.5)
  ii=0;
  while (sum(lcaGet(nstepm1))+sum(lcaGet(nstepm2))+sum(lcaGet(nstepm3)))~=0
    pause(1.0)
    fprintf(1,'%d ',ii);ii=ii+1;
    if ~mod(ii,40)
      fprintf(1,'\n')
    end
  end
  fprintf(1,'\n');
  disp('pausing for 60s measure')
  pause(60)
  
  for nn=1:28
    cmd=sprintf('lcaPut(''%s%d:motor1:stepdiag'',10000)',pvi,nn);
    eval(cmd)
    pause(1)
    cmd=sprintf('lcaPut(''%s%d:motor2:stepdiag'',-15000)',pvi,nn);
    eval(cmd)
    pause(1)
    cmd=sprintf('lcaPut(''%s%d:motor3:stepdiag'',+15000)',pvi,nn);
    eval(cmd)
    pause(1)
  end
  pause(0.5)
  ii=0;
  while (sum(lcaGet(nstepm1))+sum(lcaGet(nstepm2))+sum(lcaGet(nstepm3)))~=0
    pause(1.0)
    fprintf(1,'%d ',ii);ii=ii+1;
    if ~mod(ii,40)
      fprintf(1,'\n')
    end
  end
  fprintf(1,'\n');
  disp('pausing for 60s measure')
  pause(60)
end
pvi='c1:qmov:m';
mm=16;
ii=1;
pvn=cell(18,1);
pvn{1}=sprintf('%s%d:pot1',pvi,mm);
pvn{2}=sprintf('%s%d:pot2',pvi,mm);
pvn{3}=sprintf('%s%d:pot3',pvi,mm);
pvn{4}=sprintf('%s%d:lvdt1',pvi,mm);
pvn{5}=sprintf('%s%d:lvdt2',pvi,mm);
pvn{6}=sprintf('%s%d:lvdt3',pvi,mm);
pvn{7}=sprintf('%s%d:x',pvi,mm);
pvn{8}=sprintf('%s%d:y',pvi,mm);
pvn{9}=sprintf('%s%d:tilt',pvi,mm);
pvn{10}=sprintf('%s%d:x:alt',pvi,mm);
pvn{11}=sprintf('%s%d:y:alt',pvi,mm);
pvn{12}=sprintf('%s%d:tilt:alt',pvi,mm);
pvn{13}=sprintf('%s%d:lvdt1:raw',pvi,mm);
pvn{14}=sprintf('%s%d:lvdt2:raw',pvi,mm);
pvn{15}=sprintf('%s%d:lvdt3:raw',pvi,mm);
pvn{16}=sprintf('%s%d:motor1:sumsteps',pvi,mm);
pvn{17}=sprintf('%s%d:motor2:sumsteps',pvi,mm);
pvn{18}=sprintf('%s%d:motor3:sumsteps',pvi,mm);

nsteps=cell(3,1);
nsteps{1}=sprintf('%s%d:motor1:nStepsRemain',pvi,mm);
nsteps{2}=sprintf('%s%d:motor2:nStepsRemain',pvi,mm);
nsteps{3}=sprintf('%s%d:motor3:nStepsRemain',pvi,mm);

stepdiag{1,1}=sprintf('%s%d:motor1:stepdiag',pvi,mm);
stepdiag{2,1}=sprintf('%s%d:motor2:stepdiag',pvi,mm);
stepdiag{3,1}=sprintf('%s%d:motor3:stepdiag',pvi,mm);
ii=37;
while(1)
gagie=input('gage block? ');
if gagie==0
break
else
gage(ii)=gagie;
foo=lcaGet(pvn);
dater(:,ii)=foo(:,1);
ii=ii+1;
plot(gage,dater(15,:),'o');
end
end

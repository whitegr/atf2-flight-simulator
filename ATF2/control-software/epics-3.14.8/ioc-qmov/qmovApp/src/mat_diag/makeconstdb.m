% grep record dbQmovConstants.db > constpv.txt
%fid=fopen('constpv.txt');
%ii=0;
%while 1
%  lini=fgetl(fid);
%  if ~ischar(lini),break,end
%  quotes=findstr(lini,'"');
%  if length(quotes)~=2
%   disp(sprintf('not two quotes %s',lini))
%  else
%   parens=findstr(lini,'(');
%   commas=findstr(lini,',');
%   if isempty(parens) | isempty(commas)
%    disp(sprintf('parens/commas busted %s',lini))
%   else
%    newlini=lini(quotes(1)+1:quotes(2)-1);
%    colons=findstr(newlini,':');
%    if isempty(colons)
%     disp(sprintf('not enough colons %s',lini))
%    else
%     ii=ii+1;
%     rtype{ii,1}=lini(parens(1)+1:commas(1)-1);
%     pvn{ii,1}=['c1' newlini(colons(1):end)];
%    end
%   end
%  end
%end
%fclose(fid);
fid=fopen('dbQmovConstants.db');
ii=0;
while(1)
  lini=fgetl(fid);
  if ~ischar(lini),break,end
  if findstr(lini,'record')
   quotes=findstr(lini,'"');
   if length(quotes)~=2
    disp(sprintf('not two quotes %s',lini))
   else
    parens=findstr(lini,'(');
    commas=findstr(lini,',');
    if isempty(parens) | isempty(commas)
     disp(sprintf('parens/commas busted %s',lini))
    else
     newlini=lini(quotes(1)+1:quotes(2)-1);
     colons=findstr(newlini,':');
     if isempty(colons)
      disp(sprintf('not enough colons %s',lini))
     else
      ii=ii+1;
      rtype{ii,1}=lini(parens(1)+1:commas(1)-1);
      pvn{ii,1}=['c1' newlini(colons(1):end)];
     end %if isempty(colons)
    end %if isemtpy(parens)
   end %if quotes
   if strcmp(rtype{ii},'ai')
     found=0;
     while ~found
      lini=fgetl(fid);
      if ~isempty(findstr(lini,'INP'))
       found=1;
       id=findstr(lini,'"');
       inps{ii,1}=lini(id(1)+1:id(2)-1);
      end
     end %while ~found
   end %ai
  end %if findstr(record)
end %while (1)
fclose(fid);
save constpv.mat pvn rtype inps
 
% there are three types of records: ai, ao, stringout
%rtypea=strvcat(rtype);
%[type,ind]=sortrows(rtypea);
% ai, ao, stringout dtypes=unique(type,'rows');
%sort strings from nums
numnum=0;numstr=0;
 for nn=1:length(rtype)
  if ~isempty(findstr(rtype{nn},'string'))
   numstr=numstr+1;
   pvns{numstr,1}=pvn{nn};
  else
   numnum=numnum+1;
   pvnn{numnum,1}=pvn{nn};
   rtypen{numnum,1}=rtype{nn};
   inpstr{numnum,1}=inps{nn};
  end
 end
numvals=lcaGet(pvnn);
strvals=lcaGet(pvns);
fod=fopen('matlabConstants.db','w');
fprintf(fod,'#created by matlab makeconstdb %s\n\n',datestr(now));
for nn=1:length(pvnn)
  fprintf(fod,'record(%s, "%s") {\n',rtypen{nn},pvnn{nn});
  desc=lcaGet([pvnn{nn} '.DESC']);
  if strcmp(desc{1}(1),'$')
    if length(pvnn{nn})>27
     fprintf(fod,'\tfield(DESC, "%s")\n',pvnn{nn}(1:27));
    else
     fprintf(fod,'\tfield(DESC, "%s")\n',pvnn{nn});
    end
  else
    fprintf(fod,'\tfield(DESC, "%s")\n',desc{1});
  end %if 1=$
  fprintf(fod,'\tfield(DTYP, "Soft Channel")\n');
  fprintf(fod,'\tfield(PINI, "YES")\n');
  if strcmp(strtrim(rtypen{nn}),'ai')
    fprintf(fod,'\tfield(INP, "%s")\n',strtrim(inpstr{nn})); %fucked up.
    fprintf(fod,'\tfield(SCAN, "1 second")');
  else
    valstr=sprintf('%20.15g',numvals(nn));
    fprintf(fod,'\tfield(VAL,  "%s")\n',strtrim(valstr));
  end
  fprintf(fod,'}\n#\n'); 
end %for pvnn
  fprintf(fod,'# Here come the strings\n');
for nn=1:length(pvns)
  fprintf(fod,'record(stringout, "%s") {\n',pvns{nn});
  if length(pvnn{nn})>27
   fprintf(fod,'\tfield(DESC, "%s")\n',pvnn{nn}(1:27));
  else
   fprintf(fod,'\tfield(DESC, "%s")\n',pvnn{nn});
  end
  fprintf(fod,'\tfield(DTYP, "Soft Channel")\n');
  fprintf(fod,'\tfield(PINI, "YES")\n');
  fprintf(fod,'\tfield(VAL, "%s")\n',strtrim(strvals{nn})); 
  fprintf(fod,'}\n#\n'); 
end
fclose(fod);




















pvi='c1:qmov:m';
mm=26;
mm=input('Which mover number [1-28]? ');
mm=round(mm);
if mm>28 or mm<1
 return
end
ii=1;
pvn=cell(12,1);
pvn{1}=sprintf('%s%d:pot1',pvi,mm);
pvn{2}=sprintf('%s%d:pot2',pvi,mm);
pvn{3}=sprintf('%s%d:pot3',pvi,mm);
pvn{4}=sprintf('%s%d:lvdt1',pvi,mm);
pvn{5}=sprintf('%s%d:lvdt2',pvi,mm);
pvn{6}=sprintf('%s%d:lvdt3',pvi,mm);
pvn{7}=sprintf('%s%d:x:xps',pvi,mm);
pvn{8}=sprintf('%s%d:y:xps',pvi,mm);
pvn{9}=sprintf('%s%d:tilt:xps',pvi,mm);
pvn{10}=sprintf('%s%d:x:pot',pvi,mm);
pvn{11}=sprintf('%s%d:y:pot',pvi,mm);
pvn{12}=sprintf('%s%d:tilt:pot',pvi,mm);
pvn{13}=sprintf('%s%d:motor1:radians',pvi,mm);
pvn{14}=sprintf('%s%d:motor2:radians',pvi,mm);
pvn{15}=sprintf('%s%d:motor3:radians',pvi,mm);
pvn{16}=sprintf('%s%d:motor1:radians.RBV',pvi,mm);
pvn{17}=sprintf('%s%d:motor2:radians.RBV',pvi,mm);
pvn{18}=sprintf('%s%d:motor3:radians.RBV',pvi,mm);
startval=lcaGet(pvn{13});
%nstep=sprintf('%s%d:motor1:nStepsRemain',pvi,mm);
move1rad=sprintf('%s%d:motor1:radians',pvi,mm);
move2rad=sprintf('%s%d:motor2:radians',pvi,mm);
move3rad=sprintf('%s%d:motor3:radians',pvi,mm);

radvals=-3:.2:3;

dater(:,ii)=lcaGet(pvn);
lcaPut(move1rad,radvals(1));
pause(0.5)
lcaPut(move2rad,radvals(1));
pause(0.5)
lcaPut(move3rad,radvals(1));
pause(7)
ii=ii+1;
dater(:,ii)=lcaGet(pvn);
for nn=2:length(radvals)
  lcaPut(move1rad,radvals(nn));
pause(0.5)
  lcaPut(move2rad,radvals(nn));
pause(0.5)
  lcaPut(move3rad,radvals(nn));
  pause(7)
  ii=ii+1;
  dater(:,ii)=lcaGet(pvn);
  pause(1)
  ii=ii+1;
  dater(:,ii)=lcaGet(pvn);
  pause(1)
  ii=ii+1;
  dater(:,ii)=lcaGet(pvn);
  fprintf(1,'%d ',nn)
end
fprintf(1,'\n');
lcaPut(move1rad,0);
lcaPut(move2rad,0);
lcaPut(move3rad,0);

dater2=dater;
pot1=dater2(1,:);
pot2=dater2(2,:);
pot3=dater2(3,:);
lvdt1=dater2(4,:);
lvdt2=dater2(5,:);
lvdt3=dater2(6,:);
xpsx=dater2(7,:);
xpsy=dater2(8,:);
xpst=dater2(9,:);
potx=dater2(10,:);
poty=dater2(11,:);
pott=dater2(12,:);
m1r=dater2(13,:);
m2r=dater2(14,:);
m3r=dater2(15,:);
m1rbv=dater2(16,:);
m2rbv=dater2(17,:);
m3rbv=dater2(18,:);

%those that show min/max behavior for motor1
%
%for nn=1:12
%  [Q(:,nn),dQ(:,nn)]=plot_parab(pot1,dater2(nn,:)-mean(dater2(nn,:)),1);
%  pause
%end
  lcaPut(pvn{13},startval);


pvi='c1:qmov:m';
clear pvn

mm=input('Which mover number [1-28]? ');
mm=round(mm);
if mm>28 or mm<1
 return
end

npv=18;
ii=1;
pvn=cell(11,1);
pvn{1}=sprintf('%s%d:x:pot',pvi,mm);
pvn{2}=sprintf('%s%d:y:pot',pvi,mm);
pvn{3}=sprintf('%s%d:tilt:pot',pvi,mm);
pvn{4}=sprintf('%s%d:x:lvdt',pvi,mm);
pvn{5}=sprintf('%s%d:y:lvdt',pvi,mm);
pvn{6}=sprintf('%s%d:tilt:lvdt',pvi,mm);
pvn{7}=sprintf('%s%d:x:step',pvi,mm);
pvn{8}=sprintf('%s%d:y:step',pvi,mm);
pvn{9}=sprintf('%s%d:tilt:step',pvi,mm);
pvn{10}=sprintf('%s%d:bore:height',pvi,mm);
pvn{11}=sprintf('%s%d:bore:offsX',pvi,mm);

lcaSetMonitor(pvn{6})
startboreh=lcaGet(pvn{10});
startboreo=lcaGet(pvn{11});
startbh=startboreh-5000;
startbo=startboreo-5000;
steps=1000;
tic;
foo=lcaGet(pvn);
dater(:,ii)=foo(:,1);ii=ii+1;
lcaPut(pvn{11},startbo);
lcaPut(pvn{10},startbh);

pause(1);
try
lcaNewMonitorWait(pvn{6});
catch
fprintf(1,'lcanewmonwait failed, pause 2 sec\n')
pause(2)
end
foo=lcaGet(pvn);
dater(:,ii)=foo(:,1);ii=ii+1;
 for nn=1:11
   lcaPut(pvn{10},startbh+steps*nn);
%   pause(1.5);
   for mm=1:11
     lcaPut(pvn{11},startbo+steps*mm);
     pause(1.5);
     try
      lcaNewMonitorWait(pvn{6});
     catch
      fprintf(1,'lcanewmonwait failed, pause 2 sec\n')
      pause(2)
     end
     foo=lcaGet(pvn);
     dater(:,ii)=foo(:,1);ii=ii+1;
     fprintf(1,'%d ',nn)
   end %for mm
 end
lcaPut(pvn{10},startboreh);
lcaPut(pvn{11},startboreo);
toc
xpot=dater(1,:);
ypot=dater(2,:);
tpot=dater(3,:);
xlvdt=dater(4,:);
ylvdt=dater(5,:);
tlvdt=dater(6,:);
xstep=dater(7,:);
ystep=dater(8,:);
tstep=dater(9,:);
boreh=dater(10,:);
boreo=dater(11,:);

pvi='c1:qmov:m';
mm=26;
mm=input('Which mover number [1-28]? ');
mm=round(mm);
if mm>28 || mm<1
 return
end
mnum=input('Which motor to move? ');
mnum=round(mnum);
if mnum<1 || mnum>3
return
end
mini=input('minimum motor value? ');
maxi=input('maximum motor value? ');
if mini>maxi
foo=mini;
mini=maxi;
maxi=foo;
end
if mini<-pi
mini=-pi;
end
if maxi>pi
maxi=pi;
end
ssiz=(maxi-mini)/24;
initval=mini-ssiz;
ii=1;
pvn=cell(12,1);
pvn{1}=sprintf('%s%d:pot1',pvi,mm);
pvn{2}=sprintf('%s%d:pot2',pvi,mm);
pvn{3}=sprintf('%s%d:pot3',pvi,mm);
pvn{4}=sprintf('%s%d:lvdt1',pvi,mm);
pvn{5}=sprintf('%s%d:lvdt2',pvi,mm);
pvn{6}=sprintf('%s%d:lvdt3',pvi,mm);
pvn{7}=sprintf('%s%d:x:pot',pvi,mm);
pvn{8}=sprintf('%s%d:y:pot',pvi,mm);
pvn{9}=sprintf('%s%d:tilt:pot',pvi,mm);
pvn{10}=sprintf('%s%d:x:xps',pvi,mm);
pvn{11}=sprintf('%s%d:y:xps',pvi,mm);
pvn{12}=sprintf('%s%d:tilt:xps',pvi,mm);
pvn{13}=sprintf('%s%d:x:lvdt',pvi,mm);
pvn{14}=sprintf('%s%d:y:lvdt',pvi,mm);
pvn{15}=sprintf('%s%d:tilt:lvdt',pvi,mm);
pvn{16}=sprintf('%s%d:motor1:radians',pvi,mm);
pvn{17}=sprintf('%s%d:motor2:radians',pvi,mm);
pvn{18}=sprintf('%s%d:motor3:radians',pvi,mm);

move1=sprintf('%s%d:motor1:radians',pvi,mm);
move2=sprintf('%s%d:motor2:radians',pvi,mm);
move3=sprintf('%s%d:motor3:radians',pvi,mm);

switch(mnum)
  case 1
    mmove=move1;
  case 2
    mmove=move2;
  case 3
    mmove=move3;
  otherwise
    mmove=move1;
end

radvals=mini:ssiz:maxi;
startval=lcaGet(mmove);

dater(:,ii)=lcaGet(pvn);
lcaPut(mmove,initval);
pause(7)
ii=ii+1;
dater(:,ii)=lcaGet(pvn);
for nn=1:length(radvals)
  lcaPut(mmove,radvals(nn));
  pause(7)
  ii=ii+1;
  dater(:,ii)=lcaGet(pvn);
  pause(0.9)
  ii=ii+1;
  dater(:,ii)=lcaGet(pvn);
  pause(0.9)
  ii=ii+1;
  dater(:,ii)=lcaGet(pvn);
  fprintf(1,'%d ',nn);
end
fprintf(1,'\n');
lcaPut(mmove,startval);

dater2=dater;
pot1=dater2(1,:);
pot2=dater2(2,:);
pot3=dater2(3,:);
lvdt1=dater2(4,:);
lvdt2=dater2(5,:);
lvdt3=dater2(6,:);
potx=dater2(7,:);
poty=dater2(8,:);
pott=dater2(9,:);
xpsx=dater2(10,:);
xpsy=dater2(11,:);
xpst=dater2(12,:);
lvdtx=dater2(13,:);
lvdty=dater2(14,:);
lvdtt=dater2(15,:);
motor1=dater2(16,:);
motor2=dater2(17,:);
motor3=dater2(18,:);
%those that show min/max behavior for motor1
%
%for nn=1:12
%  [Q(:,nn),dQ(:,nn)]=plot_parab(pot1,dater2(nn,:)-mean(dater2(nn,:)),1);
%  pause
%end
  

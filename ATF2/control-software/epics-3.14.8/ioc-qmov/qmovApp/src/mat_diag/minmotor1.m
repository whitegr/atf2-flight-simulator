pvi='c1:qmov:m';
mm=27;
ii=1;
pvn=cell(15,1);
pvn{1,1}=sprintf('%s%d:pot1',pvi,mm);
pvn{2,1}=sprintf('%s%d:pot2',pvi,mm);
pvn{3,1}=sprintf('%s%d:motor1:sumsteps',pvi,mm);
pvn{4,1}=sprintf('%s%d:lvdt1',pvi,mm);
pvn{5,1}=sprintf('%s%d:lvdt2',pvi,mm);
pvn{6,1}=sprintf('%s%d:lvdt3',pvi,mm);
pvn{7,1}=sprintf('%s%d:x:pot',pvi,mm);
pvn{8,1}=sprintf('%s%d:y:pot',pvi,mm);
pvn{9,1}=sprintf('%s%d:tilt:pot',pvi,mm);
pvn{10,1}=sprintf('%s%d:x:lvdt',pvi,mm);
pvn{11,1}=sprintf('%s%d:y:lvdt',pvi,mm);
pvn{12,1}=sprintf('%s%d:tilt:lvdt',pvi,mm);
pvn{13,1}=sprintf('%s%d:lvdt1:raw',pvi,mm);
pvn{14,1}=sprintf('%s%d:lvdt2:raw',pvi,mm);
pvn{15,1}=sprintf('%s%d:lvdt3:raw',pvi,mm);

nstep1=sprintf('%s%d:motor1:nStepsRemain',pvi,mm);
movesteps1=sprintf('%s%d:motor1:stepdiag',pvi,mm);

foo=lcaGet(pvn);
dater(:,ii)=foo(:,1);
lcaPut(movesteps1,-1000);
pause(0.5)
while (lcaGet(nstep1))~=0
  pause(0.5)
  disp('pause')
end
disp('done')
pause(7)
ii=ii+1;
foo=lcaGet(pvn);
dater(:,ii)=foo(:,1);
for nn=1:40
  lcaPut(movesteps1,50);
  pause(0.5)
  while (lcaGet(nstep1))~=0
    pause(0.5)
  end
  pause(7)
  ii=ii+1;
  foo=lcaGet(pvn);
  dater(:,ii)=foo(:,1);
  fprintf(1,'%d ',nn);
end
fprintf(1,'\n');

dater2=dater;
pot1=dater2(1,:);
pot2=dater2(2,:);
sumstep1=dater2(3,:);
lvdt1=dater2(4,:);
lvdt2=dater2(5,:);
lvdt3=dater2(6,:);
potx=dater2(7,:);
poty=dater2(8,:);
pott=dater2(9,:);
lvdtx=dater2(10,:);
lvdty=dater2(11,:);
lvdtt=dater2(12,:);
% lvdt1r=dater2(13,:);
% lvdt2r=dater2(14,:);
% lvdt3r=dater2(15,:);
%those that show min/max behavior for motor1
%
for nn=4:12
  [Q(:,nn),dQ(:,nn)]=plot_parab(sumstep1,dater2(nn,:)-mean(dater2(nn,:)),1);
  pause
end
  

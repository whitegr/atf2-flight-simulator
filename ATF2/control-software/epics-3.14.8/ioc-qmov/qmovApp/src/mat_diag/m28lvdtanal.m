lvdt1x=82050;lvdt2x=82050;lvdt3y=96455;
offsx1=290500;offsx3=34925;
potx1=10.3373;potx2=10.3582;potx3=10.3977;
potmx=[potx1 potx2 potx3];
potz1=.842529;potz2=-.00439453;potz3=.000976562;
potz=[potz1 potz2 potz3];
boreoffsx=222620;boreoffsy=123952;...
camstep=.000157080;camrad=31000;camlift=1587.5;
poffsRot=[0 3*pi/4 pi/4];

lvdt12x=lvdt1x+lvdt2x;
load m28lvdts
l1ped=l1raw-round(mean(l1raw([3 8])));
l2ped=l2raw-round(mean(l2raw([3 8])));
l3ped=l3raw-round(mean(l3raw([3 8])));
%foo=[xpsx';xpsy';xpst';lvdt1';lvdt2';lvdt3'];
bw=-1;
format short g

allpots(1,:)=pot1;
allpots(2,:)=pot2;
allpots(3,:)=pot3;

for nn=1:8
%calc lvdtxyt from lvdt readings
  Ly(nn)=(lvdt1(nn)+lvdt2(nn))/2;
  Lt(nn)=atan((lvdt1(nn)-lvdt2(nn))/(lvdt12x));
  Lx(nn)=(lvdt3(nn)-tan(Lt(nn))*(Ly(nn)+lvdt3y));
  Lx(nn)=bw*Lx(nn);
  Lt(nn)=bw*Lt(nn);
%calc lvdt readings from xps xyt
  L2(nn)=xpsy(nn)-0.5*tan(bw*xpst(nn)*1e-6)*lvdt12x;
  L1(nn)=2*xpsy(nn)-L2(nn);
  L3(nn)=bw*xpsx(nn)+tan(bw*xpst(nn)*1e-6)*(xpsy(nn)+lvdt3y);
%calc lvdt constants from lvdt values & xps xyt
  L12x(nn)=(lvdt1(nn)-lvdt2(nn))/tan(bw*xpst(nn)*1e-6);
  L3y(nn)=(bw*lvdt3(nn)/1000 -...
    bw*xpsx(nn)/1000)/tan(bw*xpst(nn)*1e-6)-xpsy(nn)/1000;
%calc xyt from pot volts...
  for aa=1:3
    alfa(nn,aa)=rotoffs(aa)+(camstep*allpots(aa,nn)-5-potz(aa))*40000/potmx(aa);
    co(aa)=cos(alfa(nn,aa)-poffsRot(aa));
    si(aa)=cos(alfa(nn,aa)-poffsRot(aa));
  end
  mbh=boreoffsy+offsx3-sqrt(2)*camradius;

  x1=0;y1=boreoffsy-mbh;alpha=0;
  for iti=1:10
    alphanew=((y1_mbh-boreoffsy)+camlift*(alpha*co(1)-si(1)));
    x1new=(alpha*(xoffs3-y1))+camlift/sqt(2);
    y1new=sqrt(2)*camradius-xoffs3+alpha*x1+camlift/sqrt(2) ...
     * (alpha*co(2)-si(2)-alpha*co(3)+si(3));
    alpha=alphanew;
    x1=x1new;
    y1=y1new;
  end

end

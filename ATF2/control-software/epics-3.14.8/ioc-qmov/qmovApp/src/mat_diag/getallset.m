pvi='c1:qmov:m';
mm=1;
xyt{1}='x';
xyt{2}='y';
xyt{3}='tilt';
pvn=cell(3*28,1);
ii=1;
for mm=1:28
  for cc=1:3
    pvn{ii}=sprintf('%s%d:%s:set',pvi,mm,xyt{cc});
    ii=ii+1;
  end
end
for mm=1:28
  for cc=1:3
    pvn{ii}=sprintf('%s%d:%s',pvi,mm,xyt{cc});
    ii=ii+1;
  end
end

setxyt=lcaGet(pvn);
save setxyt_110208_2230

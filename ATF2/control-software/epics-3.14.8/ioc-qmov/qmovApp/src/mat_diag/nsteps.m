pvn=cell(9,1);
mm=28;
pvn{1,1}=sprintf('c1:qmov:m%d:pot1',mm);
pvn{2,1}=sprintf('c1:qmov:m%d:pot2',mm);
pvn{3,1}=sprintf('c1:qmov:m%d:pot3',mm);
pvn{4,1}=sprintf('c1:qmov:m%d:lvdt1',mm);
pvn{5,1}=sprintf('c1:qmov:m%d:lvdt2',mm);
pvn{6,1}=sprintf('c1:qmov:m%d:lvdt3',mm);
pvn{7,1}=sprintf('c1:qmov:m%d:motor1:nStepsRemain',mm);
pvn{8,1}=sprintf('c1:qmov:m%d:motor2:nStepsRemain',mm);
pvn{9,1}=sprintf('c1:qmov:m%d:motor3:nStepsRemain',mm);

for ii=1:200
dater(ii,:)=lcaGet(pvn);
timi(ii)=now;
pause(0.1);
end

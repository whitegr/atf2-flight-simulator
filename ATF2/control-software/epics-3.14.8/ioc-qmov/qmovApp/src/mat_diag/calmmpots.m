pvi='c1:qmov:m';
clear pvn

mm=input('Which mover number [1-28]? ');
mm=round(mm);
if mm>28 or mm<1
 return
end

npv=18;
ii=1;
pvn=cell(18,1);
pvn{1}=sprintf('%s%d:pot1',pvi,mm);
pvn{2}=sprintf('%s%d:pot2',pvi,mm);
pvn{3}=sprintf('%s%d:pot3',pvi,mm);
pvn{4}=sprintf('%s%d:lvdt1',pvi,mm);
pvn{5}=sprintf('%s%d:lvdt2',pvi,mm);
pvn{6}=sprintf('%s%d:lvdt3',pvi,mm);
pvn{7}=sprintf('%s%d:x:pot',pvi,mm);
pvn{8}=sprintf('%s%d:y:pot',pvi,mm);
pvn{9}=sprintf('%s%d:tilt:pot',pvi,mm);
pvn{10}=sprintf('%s%d:x:lvdt',pvi,mm);
pvn{11}=sprintf('%s%d:y:lvdt',pvi,mm);
pvn{12}=sprintf('%s%d:tilt:lvdt',pvi,mm);
pvn{13}=sprintf('%s%d:lvdt1:raw',pvi,mm);
pvn{14}=sprintf('%s%d:lvdt2:raw',pvi,mm);
pvn{15}=sprintf('%s%d:lvdt3:raw',pvi,mm);
pvn{16}=sprintf('%s%d:motor1:sumsteps',pvi,mm);
pvn{17}=sprintf('%s%d:motor2:sumsteps',pvi,mm);
pvn{18}=sprintf('%s%d:motor3:sumsteps',pvi,mm);
nsteps=cell(3,1);stepdiag=cell(3,1);
ii=1;
nsteps{1}=sprintf('%s%d:motor1:nStepsRemain',pvi,mm);
nsteps{2,1}=sprintf('%s%d:motor2:nStepsRemain',pvi,mm);
nsteps{3,1}=sprintf('%s%d:motor3:nStepsRemain',pvi,mm);

stepdiag{1,1}=sprintf('%s%d:motor1:stepdiag',pvi,mm);
stepdiag{2,1}=sprintf('%s%d:motor2:stepdiag',pvi,mm);
stepdiag{3,1}=sprintf('%s%d:motor3:stepdiag',pvi,mm);

lcaSetMonitor(pvn{12})
start=-10000;
steps=500;
tic;
foo=lcaGet(pvn);
dater(:,ii)=foo(:,1);
 for jj=1:3
  lcaPut(stepdiag{jj},start);
  pause(1.0)
 end
  pause(0.5)
  while sum(lcaGet(nsteps))~=0
    pause(0.5)
  end
  pause(7); %sams suck
  ii=ii+1;
     try
      lcaNewMonitorWait(pvn{12});
     catch
      fprintf(1,'lcanewmonwait failed, pause 2 sec\n')
      pause(2)
     end
foo=lcaGet(pvn);
dater(:,ii)=foo(:,1);
tic
for nn=1:40
 for jj=1:3
  lcaPut(stepdiag{jj},steps);
  pause(1)
 end
  pause(9); %sams suck
  ii=ii+1;
     try
      lcaNewMonitorWait(pvn{12});
     catch
      fprintf(1,'lcanewmonwait failed, pause 2 sec\n')
      pause(2)
     end
foo=lcaGet(pvn);
dater(:,ii)=foo(:,1);
  fprintf(1,'%d ',nn);

end
lcaPut(stepdiag,start*ones(size(stepdiag)));
toc
fprintf(1,'\n');
  
potids=sort([1:3]);
stepids=sort([16:18]);
allpots=dater(potids,:);
allsteps=dater(stepids,:);
for nn=1:length(potids)
  [q(:,nn),dq(nn,:)]=plot_polyfit(allsteps(nn,:),allpots(nn,:),1,1);
  gb(nn)=input('good (1) or bad(2)? ');
end

gid=find(gb==1);bid=find(gb==2);
potxn=cell(3,1);
potnn=cell(3,1);
ii=0;
  for cc=1:3
    ii=ii+1;
    potxn{ii,1}=sprintf('c1:qmov:m%d:cam%d:potMax',mm,cc);
    potnn{ii,1}=sprintf('c1:qmov:m%d:cam%d:potMin',mm,cc);
  end
oldmaxps=lcaGet(potxn);

for nn=1:length(gid)
  tid=gid(nn);
  lcaPut(potxn{tid},q(2,tid)*40000)
  lcaPut(potnn{tid},0)
end
newer3maxps=lcaGet(potxn);
fprintf(1,'m%d\n',mm);
fprintf(1,'%8.4g %8.4g %8.4g\n',oldmaxps);
fprintf(1,'%8.4g %8.4g %8.4g\n',newer3maxps);
fprintf(1,'%8.4g %8.4g %8.4g\n',dq(:,2)*40000);
toc

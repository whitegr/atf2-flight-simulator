% load your favorite mvrsave save file
% this should get: allcharvals, allvals, pvn, and pvnchar
allvalsNew=lcaGet(pvn);
allcharvalsNew=lcaGet(pvnchar);

id=find(allvals~=allvalsNew);
if isempty(id)
  fprintf(1,'\nNo differences in the mover constant numbers\n')
else
  fprintf(1,...
    '\nThere is/are %d difference(s) in the mover constant numbers\n\n', ...
    length(id))
    rest=input('Restore all to old values [n]? ','s');
    if ~isempty(rest)
     for nn=1:length(id)
    fprintf(1,...
      '\nFor %s old value=%8.4g new value=%8.4g\n',pvn{id(nn)}, ...
      allvals(id(nn)),allvalsNew(id(nn)));
      lcaPut(pvn{id(nn)},allvals(id(nn)));
     end
    end
end

id=find(~strcmp(allcharvals,allcharvalsNew));
if isempty(id)
  fprintf(1,'\nNo differences in the mover constant strings\n')
else
  fprintf(1,...
    '\nThere is/are %d difference(s) in the mover constant strings\n\n', ...
    length(id))
    rest=input('Restore all to old values [n]? ','s');
    if ~isempty(rest)
  for nn=1:length(id)
    fprintf(1,...
      'For %s old value=%s new value=%s\n',pvnchar{id(nn)}, ...
      allcharvals{id(nn)},allcharvalsNew{id(nn)});
      lcaPut(pvnchar{id(nn)},allcharvals{id(nn)});
    end
  end
end
fprintf(1,'\n');




    starttim=now;
    %mm=17;
    npv=21;
    pvn=cell(npv*28+6,1);
for mm=1:28
    pvn{1+(mm-1)*npv}=sprintf('c1:qmov:m%d:x',mm);
    pvn{2+(mm-1)*npv}=sprintf('c1:qmov:m%d:y',mm);
    pvn{3+(mm-1)*npv}=sprintf('c1:qmov:m%d:tilt',mm);
    pvn{4+(mm-1)*npv}=sprintf('c1:qmov:m%d:x:alt',mm);
    pvn{5+(mm-1)*npv}=sprintf('c1:qmov:m%d:y:alt',mm);
    pvn{6+(mm-1)*npv}=sprintf('c1:qmov:m%d:tilt:alt',mm);
    pvn{7+(mm-1)*npv}=sprintf('c1:qmov:m%d:pot1',mm);
    pvn{8+(mm-1)*npv}=sprintf('c1:qmov:m%d:pot2',mm);
    pvn{9+(mm-1)*npv}=sprintf('c1:qmov:m%d:pot3',mm);
    pvn{10+(mm-1)*npv}=sprintf('c1:qmov:m%d:lvdt1:raw',mm);
    pvn{11+(mm-1)*npv}=sprintf('c1:qmov:m%d:lvdt2:raw',mm);
    pvn{12+(mm-1)*npv}=sprintf('c1:qmov:m%d:lvdt3:raw',mm);
    pvn{13+(mm-1)*npv}=sprintf('c1:qmov:m%d:lvdt1:ped',mm);
    pvn{14+(mm-1)*npv}=sprintf('c1:qmov:m%d:lvdt2:ped',mm);
    pvn{15+(mm-1)*npv}=sprintf('c1:qmov:m%d:lvdt3:ped',mm);
    pvn{16+(mm-1)*npv}=sprintf('c1:qmov:m%d:lvdt1',mm);
    pvn{17+(mm-1)*npv}=sprintf('c1:qmov:m%d:lvdt2',mm);
    pvn{18+(mm-1)*npv}=sprintf('c1:qmov:m%d:lvdt3',mm);
    pvn{19+(mm-1)*npv}=sprintf('c1:qmov:m%d:x:set',mm);
    pvn{20+(mm-1)*npv}=sprintf('c1:qmov:m%d:y:set',mm);
    pvn{21+(mm-1)*npv}=sprintf('c1:qmov:m%d:tilt:set',mm);
end
pvn{589}=sprintf('c1:qmov:diagsam:ch1');
pvn{590}=sprintf('c1:qmov:diagsam:ch2');
pvn{591}=sprintf('c1:qmov:diagsam:ch3');
pvn{592}=sprintf('c1:qmov:diagsam:ch4');
pvn{593}=sprintf('c1:qmov:diagsam:ch5');
pvn{594}=sprintf('c1:qmov:diagsam:ch6');


fnam=datestr(now,'yymmdd_HHMM');
fnam=['allvals_' fnam '.mat'];
ii=1;bigA=zeros(594,43200);
tic
while ii<43201
  %get all current values
  foo=lcaGet(pvn);
  bigA(:,ii)=foo(:,1);
  timi(ii)=now;
  if mod(ii,100)==0
    disp(ii)
    save(fnam,'pvn','ii','bigA','timi')
    toc
  end
  pause(1)
  ii=ii+1;
end

save(fnam,'pvn','ii','bigA','timi')









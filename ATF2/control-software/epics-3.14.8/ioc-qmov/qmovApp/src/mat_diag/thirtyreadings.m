pvi='c1:qmov:m';
npv=18;
ii=1;
pvn=cell(18*28,1);
for mm=1:28
pvn{1+(mm-1)*npv}=sprintf('%s%d:pot1',pvi,mm);
pvn{2+(mm-1)*npv}=sprintf('%s%d:pot2',pvi,mm);
pvn{3+(mm-1)*npv}=sprintf('%s%d:pot3',pvi,mm);
pvn{4+(mm-1)*npv}=sprintf('%s%d:lvdt1',pvi,mm);
pvn{5+(mm-1)*npv}=sprintf('%s%d:lvdt2',pvi,mm);
pvn{6+(mm-1)*npv}=sprintf('%s%d:lvdt3',pvi,mm);
pvn{7+(mm-1)*npv}=sprintf('%s%d:x',pvi,mm);
pvn{8+(mm-1)*npv}=sprintf('%s%d:y',pvi,mm);
pvn{9+(mm-1)*npv}=sprintf('%s%d:tilt',pvi,mm);
pvn{10+(mm-1)*npv}=sprintf('%s%d:x:alt',pvi,mm);
pvn{11+(mm-1)*npv}=sprintf('%s%d:y:alt',pvi,mm);
pvn{12+(mm-1)*npv}=sprintf('%s%d:tilt:alt',pvi,mm);
pvn{13+(mm-1)*npv}=sprintf('%s%d:lvdt1:raw',pvi,mm);
pvn{14+(mm-1)*npv}=sprintf('%s%d:lvdt2:raw',pvi,mm);
pvn{15+(mm-1)*npv}=sprintf('%s%d:lvdt3:raw',pvi,mm);
pvn{16+(mm-1)*npv}=sprintf('%s%d:motor1:sumsteps',pvi,mm);
pvn{17+(mm-1)*npv}=sprintf('%s%d:motor2:sumsteps',pvi,mm);
pvn{18+(mm-1)*npv}=sprintf('%s%d:motor3:sumsteps',pvi,mm);
end

for nn=1:30
  foo=lcaGet(pvn);
  dater(:,nn)=foo(:,1);
  pause(1)
end
  

  

fid=fopen('qmov.template','w');
fprintf(fid,'#\n');
fprintf(fid,'# template file for qmov mover records\n');
fprintf(fid,'#\n');
fprintf(fid,'# dbQmov.template\n');
fprintf(fid,'#\n');
fprintf(fid,'# 29-May-2011 JLN\n');
fprintf(fid,'#\n');
fprintf(fid,'# Mod:\n');
fprintf(fid,'#\n');
fprintf(fid,'#-------------------------------------------------\n');
fprintf(fid,'file db/qmovMover.db\n');
fprintf(fid,'{\n');
fprintf(fid,' pattern { user, nm}\n');
for nn=1:28
  fprintf(fid,'  { c1, %2d}\n',nn);
end
fprintf(fid,'}\n\n');
%%
fprintf(fid,'file db/qmovPotlvdt.db\n');
fprintf(fid,'{\n');
ii=0; 
pred=[3 4 5]; step=[6 7 8]; ghi='GHI';
fprintf(fid,' pattern { user, nm, pl, addr, pred, step, ghi }\n');
for nn=1:28
  for MM=1:3
    fprintf(fid,'  { c1, %2d, %d, %2d, %d, %d, %c}\n',...
      nn,MM,ii,pred(MM),step(MM), ghi(MM));
    ii=ii+1;
  end
end
fprintf(fid,'}\n\n');
%%
ii=0; jkl='JKL';

fprintf(fid,'file db/qmovMotor.db\n');
fprintf(fid,'{\n');
fprintf(fid,' pattern { user, nm, mtr, addr, jkl}\n');
for nn=1:28
  for MM=1:3
    fprintf(fid,'  { c1, %2d, %d, %2d, %c}\n',...
      nn,MM,ii,jkl(MM));
    ii=ii+1;
  end
end
fprintf(fid,'}\n\n');
%%
ii=0; ghi='GHI';jkl='JKL';gik='GIK';hjl='HJL';
abstol=[2 2 7];egu=['um';'um';'ur'];nvl={'blps';'blps';' lps'};
inpd=['bpm';'bpm';'pot'];zot=[0 1 2];
xyt={'   x';'   y';'tilt'};

fprintf(fid,'file db/qmovXyt.db\n');
fprintf(fid,'{\n');
fprintf(fid,' pattern { user, nm, xyt, egu, abs, ghi, jkl, gik, hjl, nvl, inpd, zot}\n');
for nn=1:28
  for MM=1:3
    fprintf(fid,'  { c1, %2d, %s, %s, %d, %c, %c, %c, %c, %s, %s, %d}\n',...
      nn,xyt{MM},egu(MM,:),abstol(MM),ghi(MM),jkl(MM),gik(MM),...
      hjl(MM), nvl{MM},inpd(MM,:), zot(MM));
    ii=ii+1;
  end
end
fprintf(fid,'}\n\n');



%%
fclose(fid);

% begin anew %%

pvi='c1:qmov:m';
mm=28;
ii=1;
pvn=cell(15,1);
pvn{1}=sprintf('%s%d:pot1',pvi,mm);
pvn{2}=sprintf('%s%d:pot2',pvi,mm);
pvn{3}=sprintf('%s%d:pot3',pvi,mm);
pvn{4}=sprintf('%s%d:lvdt1',pvi,mm);
pvn{5}=sprintf('%s%d:lvdt2',pvi,mm);
pvn{6}=sprintf('%s%d:lvdt3',pvi,mm);
pvn{7}=sprintf('%s%d:x',pvi,mm);
pvn{8}=sprintf('%s%d:y',pvi,mm);
pvn{9}=sprintf('%s%d:tilt',pvi,mm);
pvn{10}=sprintf('%s%d:x:alt',pvi,mm);
pvn{11}=sprintf('%s%d:y:alt',pvi,mm);
pvn{12}=sprintf('%s%d:tilt:alt',pvi,mm);
pvn{13}=sprintf('%s%d:motor1:sumsteps',pvi,mm);
pvn{14}=sprintf('%s%d:motor2:sumsteps',pvi,mm);
pvn{15}=sprintf('%s%d:motor3:sumsteps',pvi,mm);

nstep2=sprintf('%s%d:motor2:nStepsRemain',pvi,mm);
movesteps2=sprintf('%s%d:motor2:stepdiag',pvi,mm);
nstep3=sprintf('%s%d:motor3:nStepsRemain',pvi,mm);
movesteps3=sprintf('%s%d:motor3:stepdiag',pvi,mm);

dater(:,ii)=lcaGet(pvn);
%lcaPut(movesteps2,-1000);
lcaPut(movesteps3,1000);
pause(0.5)
while (lcaGet(nstep2)+lcaGet(nstep3))~=0
  pause(0.5)
  disp('pause')
end
disp('done')
pause(7)
ii=ii+1;
dater(:,ii)=lcaGet(pvn);
for nn=1:40
%  lcaPut(movesteps2,50);
  lcaPut(movesteps3,-50);
  pause(0.5)
  while (lcaGet(nstep2)+lcaGet(nstep3))~=0
    pause(0.5)
  end
  pause(7)
  ii=ii+1;
  dater(:,ii)=lcaGet(pvn);
  fprintf(1,'%d ',nn);
end
lcaPut(movesteps3,-1000);
fprintf(1,'\n');

dater2=dater;
pot1=dater2(1,:);
pot2=dater2(2,:);
pot3=dater2(3,:);
lvdt1=dater2(4,:);
lvdt2=dater2(5,:);
lvdt3=dater2(6,:);
potx=dater2(7,:);
poty=dater2(8,:);
pott=dater2(9,:);
lvdtx=dater2(10,:);
lvdty=dater2(11,:);
lvdtt=dater2(12,:);
motor1s=dater2(13,:);
motor2s=dater2(14,:);
motor3s=dater2(15,:);
%those that show min/max behavior for motor1
%
%for nn=1:12
%  [Q(:,nn),dQ(:,nn)]=plot_parab(pot1,dater2(nn,:)-mean(dater2(nn,:)),1);
%  pause
%end
  

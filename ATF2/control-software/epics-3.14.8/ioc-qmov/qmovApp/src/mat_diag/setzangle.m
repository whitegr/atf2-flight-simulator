%clear dater pvn mm
tic
pvi='c1:qmov:m';
mm=26;
mm=input('Which mover number [1-28]? ');
mm=round(mm);
if mm>28 or mm<1
 return
end
ii=1;
steps{1}=sprintf('%s%d:motor1:stepdiag',pvi,mm);
steps{2,1}=sprintf('%s%d:motor2:stepdiag',pvi,mm);
steps{3,1}=sprintf('%s%d:motor3:stepdiag',pvi,mm);
msteps=[10000 -15000 15000];

for nn=3:-1:1
lcaPut(steps{nn},msteps(nn))
pause(2)
end

fprintf(1,'press <return> to continue\n')
pause

pvn=cell(3,1);
potpvn{1}=sprintf('%s%d:pot1',pvi,mm);
potpvn{2,1}=sprintf('%s%d:pot2',pvi,mm);
potpvn{3,1}=sprintf('%s%d:pot3',pvi,mm);

zpvn{1}=sprintf('%s%d:cam1:potZAngle',pvi,mm);
zpvn{2,1}=sprintf('%s%d:cam2:potZAngle',pvi,mm);
zpvn{3,1}=sprintf('%s%d:cam3:potZAngle',pvi,mm);


oldz=lcaGet(zpvn);
currpot=lcaGet(potpvn);
lcaPut(zpvn,currpot-5);
newz=lcaGet(zpvn);
fprintf(1,'m%d\n',mm)
fprintf(1,'%8.4e %8.4e %8.4e\n',oldz(1),oldz(2),oldz(3))
fprintf(1,'%8.4e %8.4e %8.4e\n',newz(1),newz(2),newz(3))
pause(2)
lcaPut(sprintf('%s%d:pot2step.PROC',pvi,mm),1)
lcaPut(sprintf('%s%d:pot2lvdt.PROC',pvi,mm),1)
toc
  

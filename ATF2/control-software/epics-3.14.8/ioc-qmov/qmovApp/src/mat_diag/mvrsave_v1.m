npv=72;%69; %# of pvs per mover
pvn=cell(npv*28+3,1);
size(pvn)
for mm=1:28
  ii=1;
  pvn{ii+(mm-1)*npv}=sprintf('c1:qmov:m%d:bore:offsX',mm);ii=ii+1;
  pvn{ii+(mm-1)*npv}=sprintf('c1:qmov:m%d:bore:offsY',mm);ii=ii+1;
  pvn{ii+(mm-1)*npv}=sprintf('c1:qmov:m%d:bore:offsRot',mm);ii=ii+1;
  pvn{ii+(mm-1)*npv}=sprintf('c1:qmov:m%d:lvdt:chan',mm);ii=ii+1;
  pvn{ii+(mm-1)*npv}=sprintf('c1:qmov:m%d:lvdt:offsX',mm);ii=ii+1;
  pvn{ii+(mm-1)*npv}=sprintf('c1:qmov:m%d:lvdt:offsY',mm);ii=ii+1;
  pvn{ii+(mm-1)*npv}=sprintf('c1:qmov:m%d:lvdt:offsRot',mm);ii=ii+1;
  pvn{ii+(mm-1)*npv}=sprintf('c1:qmov:m%d:cam1:offsX',mm);ii=ii+1;
  pvn{ii+(mm-1)*npv}=sprintf('c1:qmov:m%d:cam1:offsRot',mm);ii=ii+1;
  pvn{ii+(mm-1)*npv}=sprintf('c1:qmov:m%d:cam1:offsY',mm);ii=ii+1;
  pvn{ii+(mm-1)*npv}=sprintf('c1:qmov:m%d:cam2:offsX',mm);ii=ii+1;
  pvn{ii+(mm-1)*npv}=sprintf('c1:qmov:m%d:cam2:offsRot',mm);ii=ii+1;
  pvn{ii+(mm-1)*npv}=sprintf('c1:qmov:m%d:cam2:offsY',mm);ii=ii+1;
  pvn{ii+(mm-1)*npv}=sprintf('c1:qmov:m%d:cam3:offsX',mm);ii=ii+1;
  pvn{ii+(mm-1)*npv}=sprintf('c1:qmov:m%d:cam3:offsRot',mm);ii=ii+1;
  pvn{ii+(mm-1)*npv}=sprintf('c1:qmov:m%d:cam3:offsY',mm);ii=ii+1;
  pvn{ii+(mm-1)*npv}=sprintf('c1:qmov:m%d:cam1:potMin',mm);ii=ii+1;
  pvn{ii+(mm-1)*npv}=sprintf('c1:qmov:m%d:cam1:potMax',mm);ii=ii+1;
  pvn{ii+(mm-1)*npv}=sprintf('c1:qmov:m%d:cam1:potZAngle',mm);ii=ii+1;
  pvn{ii+(mm-1)*npv}=sprintf('c1:qmov:m%d:cam2:potMin',mm);ii=ii+1;
  pvn{ii+(mm-1)*npv}=sprintf('c1:qmov:m%d:cam2:potMax',mm);ii=ii+1;
  pvn{ii+(mm-1)*npv}=sprintf('c1:qmov:m%d:cam2:potZAngle',mm);ii=ii+1;
  pvn{ii+(mm-1)*npv}=sprintf('c1:qmov:m%d:cam3:potMin',mm);ii=ii+1;
  pvn{ii+(mm-1)*npv}=sprintf('c1:qmov:m%d:cam3:potMax',mm);ii=ii+1;
  pvn{ii+(mm-1)*npv}=sprintf('c1:qmov:m%d:cam3:potZAngle',mm);ii=ii+1;
  pvn{ii+(mm-1)*npv}=sprintf('c1:qmov:m%d:lvdt1:X',mm);ii=ii+1;
  pvn{ii+(mm-1)*npv}=sprintf('c1:qmov:m%d:lvdt1:cal',mm);ii=ii+1;
  pvn{ii+(mm-1)*npv}=sprintf('c1:qmov:m%d:lvdt1:n1',mm);ii=ii+1;
  pvn{ii+(mm-1)*npv}=sprintf('c1:qmov:m%d:lvdt1:n2',mm);ii=ii+1;
  pvn{ii+(mm-1)*npv}=sprintf('c1:qmov:m%d:lvdt1:n3',mm);ii=ii+1;
  pvn{ii+(mm-1)*npv}=sprintf('c1:qmov:m%d:lvdt1:n4',mm);ii=ii+1;
  pvn{ii+(mm-1)*npv}=sprintf('c1:qmov:m%d:lvdt1:Y',mm);ii=ii+1;
  pvn{ii+(mm-1)*npv}=sprintf('c1:qmov:m%d:lvdt2:X',mm);ii=ii+1;
  pvn{ii+(mm-1)*npv}=sprintf('c1:qmov:m%d:lvdt2:cal',mm);ii=ii+1;
  pvn{ii+(mm-1)*npv}=sprintf('c1:qmov:m%d:lvdt2:n1',mm);ii=ii+1;
  pvn{ii+(mm-1)*npv}=sprintf('c1:qmov:m%d:lvdt2:n2',mm);ii=ii+1;
  pvn{ii+(mm-1)*npv}=sprintf('c1:qmov:m%d:lvdt2:n3',mm);ii=ii+1;
  pvn{ii+(mm-1)*npv}=sprintf('c1:qmov:m%d:lvdt2:n4',mm);ii=ii+1;
  pvn{ii+(mm-1)*npv}=sprintf('c1:qmov:m%d:lvdt2:Y',mm);ii=ii+1;
  pvn{ii+(mm-1)*npv}=sprintf('c1:qmov:m%d:lvdt3:X',mm);ii=ii+1;
  pvn{ii+(mm-1)*npv}=sprintf('c1:qmov:m%d:lvdt3:cal',mm);ii=ii+1;
  pvn{ii+(mm-1)*npv}=sprintf('c1:qmov:m%d:lvdt3:n1',mm);ii=ii+1;
  pvn{ii+(mm-1)*npv}=sprintf('c1:qmov:m%d:lvdt3:n2',mm);ii=ii+1;
  pvn{ii+(mm-1)*npv}=sprintf('c1:qmov:m%d:lvdt3:n3',mm);ii=ii+1;
  pvn{ii+(mm-1)*npv}=sprintf('c1:qmov:m%d:lvdt3:n4',mm);ii=ii+1;
  pvn{ii+(mm-1)*npv}=sprintf('c1:qmov:m%d:lvdt3:Y',mm);ii=ii+1;
  pvn{ii+(mm-1)*npv}=sprintf('c1:qmov:m%d:bore:height',mm);ii=ii+1;
  pvn{ii+(mm-1)*npv}=sprintf('c1:qmov:m%d:x:abstol',mm);ii=ii+1;
  pvn{ii+(mm-1)*npv}=sprintf('c1:qmov:m%d:y:abstol',mm);ii=ii+1;
  pvn{ii+(mm-1)*npv}=sprintf('c1:qmov:m%d:tilt:abstol',mm);ii=ii+1;
  pvn{ii+(mm-1)*npv}=sprintf('c1:qmov:m%d:x:pctol',mm);ii=ii+1;
  pvn{ii+(mm-1)*npv}=sprintf('c1:qmov:m%d:y:pctol',mm);ii=ii+1;
  pvn{ii+(mm-1)*npv}=sprintf('c1:qmov:m%d:tilt:pctol',mm);ii=ii+1;
  pvn{ii+(mm-1)*npv}=sprintf('c1:qmov:m%d:ntry',mm);ii=ii+1;
  pvn{ii+(mm-1)*npv}=sprintf('c1:qmov:m%d:moveCMD:tilt',mm);ii=ii+1;
  pvn{ii+(mm-1)*npv}=sprintf('c1:qmov:m%d:moveCMD:y',mm);ii=ii+1;
  pvn{ii+(mm-1)*npv}=sprintf('c1:qmov:m%d:moveCMD:x',mm);ii=ii+1;
  pvn{ii+(mm-1)*npv}=sprintf('c1:qmov:m%d:motor3:stepval',mm);ii=ii+1;
  pvn{ii+(mm-1)*npv}=sprintf('c1:qmov:m%d:motor2:stepval',mm);ii=ii+1;
  pvn{ii+(mm-1)*npv}=sprintf('c1:qmov:m%d:motor1:stepval',mm);ii=ii+1;
  pvn{ii+(mm-1)*npv}=sprintf('c1:qmov:m%d:lvdt1:ped',mm);ii=ii+1;
  pvn{ii+(mm-1)*npv}=sprintf('c1:qmov:m%d:lvdt2:ped',mm);ii=ii+1;
  pvn{ii+(mm-1)*npv}=sprintf('c1:qmov:m%d:lvdt3:ped',mm);ii=ii+1;
  pvn{ii+(mm-1)*npv}=sprintf('c1:qmov:m%d:tilt:set',mm);ii=ii+1;
  pvn{ii+(mm-1)*npv}=sprintf('c1:qmov:m%d:y:set',mm);ii=ii+1;
  pvn{ii+(mm-1)*npv}=sprintf('c1:qmov:m%d:x:set',mm);ii=ii+1;
  pvn{ii+(mm-1)*npv}=sprintf('c1:qmov:m%d:x:file',mm);ii=ii+1;
  pvn{ii+(mm-1)*npv}=sprintf('c1:qmov:m%d:y:file',mm);ii=ii+1;
  pvn{ii+(mm-1)*npv}=sprintf('c1:qmov:m%d:tilt:file',mm);ii=ii+1;
  pvn{ii+(mm-1)*npv}=sprintf('c1:qmov:m%d:motor1:indians',mm);ii=ii+1;
  pvn{ii+(mm-1)*npv}=sprintf('c1:qmov:m%d:motor2:indians',mm);ii=ii+1;
  pvn{ii+(mm-1)*npv}=sprintf('c1:qmov:m%d:motor3:indians',mm);ii=ii+1;
end
pvn{end-2}=sprintf('c1:qmov:cam:step');
pvn{end-1}=sprintf('c1:qmov:cam:radius');
pvn{end}=sprintf('c1:qmov:cam:lift');
%
ii=1;
npv=2;
pvnchar=cell(npv*28,1);
size(pvnchar)
for mm=1:28
  pvnchar{ii}=sprintf('c1:qmov:m%d:name',mm);ii=ii+1;
  pvnchar{ii}=sprintf('c1:qmov:m%d:update',mm);ii=ii+1;
  pvnchar{ii}=sprintf('c1:qmov:m%d:blps',mm);ii=ii+1;
  pvnchar{ii}=sprintf('c1:qmov:m%d:lps',mm);ii=ii+1;
end

allvals=lcaGet(pvn);
allcharvals=lcaGet(pvnchar);

timi=round(clock);
filnam=sprintf('mvrvals_%04d%02d%02dT%02d%02d%02dv2.mat', ...
  timi(1), timi(2), timi(3), timi(4), timi(5), timi(6));
save(filnam,'pvn','pvnchar','allvals','allcharvals')
  




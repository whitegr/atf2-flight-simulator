    starttim=now;
    %mm=17;
    npv=27;
    pvn=cell(npv*28+6,1);
for mm=1:28
    pvn{1+(mm-1)*npv}=sprintf('c1:qmov:m%d:x:pot',mm);
    pvn{2+(mm-1)*npv}=sprintf('c1:qmov:m%d:y:pot',mm);
    pvn{3+(mm-1)*npv}=sprintf('c1:qmov:m%d:tilt:pot',mm);
    pvn{4+(mm-1)*npv}=sprintf('c1:qmov:m%d:x:lvdt',mm);
    pvn{5+(mm-1)*npv}=sprintf('c1:qmov:m%d:y:lvdt',mm);
    pvn{6+(mm-1)*npv}=sprintf('c1:qmov:m%d:tilt:lvdt',mm);
    pvn{7+(mm-1)*npv}=sprintf('c1:qmov:m%d:x:step',mm);
    pvn{8+(mm-1)*npv}=sprintf('c1:qmov:m%d:y:step',mm);
    pvn{9+(mm-1)*npv}=sprintf('c1:qmov:m%d:tilt:step',mm);
    pvn{10+(mm-1)*npv}=sprintf('c1:qmov:m%d:x:xps',mm);
    pvn{11+(mm-1)*npv}=sprintf('c1:qmov:m%d:y:xps',mm);
    pvn{12+(mm-1)*npv}=sprintf('c1:qmov:m%d:tilt:xps',mm);
    pvn{13+(mm-1)*npv}=sprintf('c1:qmov:m%d:pot1',mm);
    pvn{14+(mm-1)*npv}=sprintf('c1:qmov:m%d:pot2',mm);
    pvn{15+(mm-1)*npv}=sprintf('c1:qmov:m%d:pot3',mm);
    pvn{16+(mm-1)*npv}=sprintf('c1:qmov:m%d:lvdt1:raw',mm);
    pvn{17+(mm-1)*npv}=sprintf('c1:qmov:m%d:lvdt2:raw',mm);
    pvn{18+(mm-1)*npv}=sprintf('c1:qmov:m%d:lvdt3:raw',mm);
    pvn{19+(mm-1)*npv}=sprintf('c1:qmov:m%d:lvdt1:ped',mm);
    pvn{20+(mm-1)*npv}=sprintf('c1:qmov:m%d:lvdt2:ped',mm);
    pvn{21+(mm-1)*npv}=sprintf('c1:qmov:m%d:lvdt3:ped',mm);
    pvn{22+(mm-1)*npv}=sprintf('c1:qmov:m%d:lvdt1',mm);
    pvn{23+(mm-1)*npv}=sprintf('c1:qmov:m%d:lvdt2',mm);
    pvn{24+(mm-1)*npv}=sprintf('c1:qmov:m%d:lvdt3',mm);
    pvn{25+(mm-1)*npv}=sprintf('c1:qmov:m%d:x:set',mm);
    pvn{26+(mm-1)*npv}=sprintf('c1:qmov:m%d:y:set',mm);
    pvn{27+(mm-1)*npv}=sprintf('c1:qmov:m%d:tilt:set',mm);
end
pvn{757}=sprintf('c1:qmov:diagsam:ch1');
pvn{758}=sprintf('c1:qmov:diagsam:ch2');
pvn{759}=sprintf('c1:qmov:diagsam:ch3');
pvn{760}=sprintf('c1:qmov:diagsam:ch4');
pvn{761}=sprintf('c1:qmov:diagsam:ch5');
pvn{762}=sprintf('c1:qmov:diagsam:ch6');


fnam=datestr(now,'yymmdd_HHMM');
fnam=['allvals_' fnam '.mat'];
ii=1;bigA=zeros(762,43200);
tic
while ii<43201
  %get all current values
  foo=lcaGet(pvn);
  bigA(:,ii)=foo(:,1);
  timi(ii)=now;
  if mod(ii,100)==0
    disp(ii)
    save(fnam,'pvn','ii','bigA','timi')
    toc
  end
  pause(1)
  ii=ii+1;
end

save(fnam,'pvn','ii','bigA','timi')









clear dater pvn mm
tic
pvi='c1:qmov:m';
mm=26;
mm=input('Which mover number [1-28]? ');
mm=round(mm);
if mm>28 or mm<1
 return
end
ii=1;
pvn=cell(10,1);
pvn{1}=sprintf('%s%d:pot1',pvi,mm);
pvn{2}=sprintf('%s%d:pot2',pvi,mm);
pvn{3}=sprintf('%s%d:pot3',pvi,mm);
pvn{4}=sprintf('%s%d:lvdt1',pvi,mm);
pvn{5}=sprintf('%s%d:lvdt2',pvi,mm);
pvn{6}=sprintf('%s%d:lvdt3',pvi,mm);
pvn{7}=sprintf('%s%d:x',pvi,mm);
pvn{8}=sprintf('%s%d:y',pvi,mm);
pvn{9}=sprintf('%s%d:tilt',pvi,mm);
pvn{10}=sprintf('%s%d:motor1:sumsteps',pvi,mm);

nstep=sprintf('%s%d:motor1:nStepsRemain',pvi,mm);
movesteps=sprintf('%s%d:motor1:stepdiag',pvi,mm);

dater(:,ii)=lcaGet(pvn);
lcaSetMonitor(pvn{5});
lcaPut(movesteps,-1000);
pause(0.5)
while lcaGet(nstep)~=0
  pause(0.5)
  disp('pause')
end
disp('done')
pause(7)
ii=ii+1;
dater(:,ii)=lcaGet(pvn);
for nn=1:20
  lcaPut(movesteps,100);
  pause(0.5)
  while lcaGet(nstep)~=0
    pause(0.5)
  end
  pause(7)
  ii=ii+1;
  lcaNewMonitorWait(pvn{5})
  dater(:,ii)=lcaGet(pvn);
  fprintf(1,'%8.4e ',dater(5,ii));  
  pause(1.1)
  ii=ii+1;
  lcaNewMonitorWait(pvn{5})
  dater(:,ii)=lcaGet(pvn);
  fprintf(1,'%8.4e ',dater(5,ii));  
  pause(1.1)
  ii=ii+1;
  lcaNewMonitorWait(pvn{5})
  dater(:,ii)=lcaGet(pvn);
  fprintf(1,'%8.4e %d \n',dater(5,ii),nn);
end
fprintf(1,'\n');
lcaPut(movesteps,-1000);

dater2=dater;
pot1=dater2(1,:);
pot2=dater2(2,:);
pot3=dater2(3,:);
lvdt1=dater2(4,:);
lvdt2=dater2(5,:);
lvdt3=dater2(6,:);
potx=dater2(7,:);
poty=dater2(8,:);
pott=dater2(9,:);
m1s=dater2(10,:);
%those that show min/max behavior for motor1
%
%for nn=1:12
%  [Q(:,nn),dQ(:,nn)]=plot_parab(pot1,dater2(nn,:)-mean(dater2(nn,:)),1);
%  pause
%end
tt=clock;
filnam=sprintf('minlvdt2_m%d_%02d%02d%02dT%02d%02d%02d',mm,tt(1),tt(2),tt(3),tt(4),tt(5),round(tt(6)));
save(filnam) 
toc

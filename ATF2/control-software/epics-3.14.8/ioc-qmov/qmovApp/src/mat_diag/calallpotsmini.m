badmotors=[2 3 9 13 16 27];
pvi='c1:qmov:m';
npv=18;
ii=1;
pvn=cell(18*length(badmotors),1);
for mm=1:length(badmotors)
  MM=badmotors(mm);
  pvn{1+(mm-1)*npv}=sprintf('%s%d:pot1',pvi,MM);
  pvn{2+(mm-1)*npv}=sprintf('%s%d:pot2',pvi,MM);
  pvn{3+(mm-1)*npv}=sprintf('%s%d:pot3',pvi,MM);
  pvn{4+(mm-1)*npv}=sprintf('%s%d:lvdt1',pvi,MM);
  pvn{5+(mm-1)*npv}=sprintf('%s%d:lvdt2',pvi,MM);
  pvn{6+(mm-1)*npv}=sprintf('%s%d:lvdt3',pvi,MM);
  pvn{7+(mm-1)*npv}=sprintf('%s%d:x:pot',pvi,MM);
  pvn{8+(mm-1)*npv}=sprintf('%s%d:y:pot',pvi,MM);
  pvn{9+(mm-1)*npv}=sprintf('%s%d:tilt:pot',pvi,MM);
  pvn{10+(mm-1)*npv}=sprintf('%s%d:x:lvdt',pvi,MM);
  pvn{11+(mm-1)*npv}=sprintf('%s%d:y:lvdt',pvi,MM);
  pvn{12+(mm-1)*npv}=sprintf('%s%d:tilt:lvdt',pvi,MM);
  pvn{13+(mm-1)*npv}=sprintf('%s%d:lvdt1:raw',pvi,MM);
  pvn{14+(mm-1)*npv}=sprintf('%s%d:lvdt2:raw',pvi,MM);
  pvn{15+(mm-1)*npv}=sprintf('%s%d:lvdt3:raw',pvi,MM);
  pvn{16+(mm-1)*npv}=sprintf('%s%d:motor1:sumsteps',pvi,MM);
  pvn{17+(mm-1)*npv}=sprintf('%s%d:motor2:sumsteps',pvi,MM);
  pvn{18+(mm-1)*npv}=sprintf('%s%d:motor3:sumsteps',pvi,MM);
end
nsteps=cell(3*length(badmotors),1);stepdiag=cell(3*length(badmotors),1);
ii=1;
for mm=1:length(badmotors)
  MM=badmotors(mm);
  nsteps{1+(mm-1)*3}=sprintf('%s%d:motor1:nStepsRemain',pvi,MM);
  nsteps{2+(mm-1)*3}=sprintf('%s%d:motor2:nStepsRemain',pvi,MM);
  nsteps{3+(mm-1)*3}=sprintf('%s%d:motor3:nStepsRemain',pvi,MM);
  
  stepdiag{1+(mm-1)*3,1}=sprintf('%s%d:motor1:stepdiag',pvi,MM);
  stepdiag{2+(mm-1)*3,1}=sprintf('%s%d:motor2:stepdiag',pvi,MM);
  stepdiag{3+(mm-1)*3,1}=sprintf('%s%d:motor3:stepdiag',pvi,MM);
end

start=-15000;
steps=300;
tic;
foo=lcaGet(pvn);
dater(:,ii)=foo(:,1);
lcaPut(stepdiag,start*ones(size(stepdiag)));
pause(0.5)
while sum(lcaGet(nsteps))~=0
  pause(0.5)
end
pause(7); %sams suck
ii=ii+1;
foo=lcaGet(pvn);
dater(:,ii)=foo(:,1);
tic
for nn=1:100
  lcaPut(stepdiag,steps*ones(size(stepdiag)));
  pause(12); %sams suck
  ii=ii+1;
  foo=lcaGet(pvn);
  dater(:,ii)=foo(:,1);
  fprintf(1,'%d ',100-nn);
end
lcaPut(stepdiag,start*ones(size(stepdiag)));
toc
fprintf(1,'\n');
timi=clock;
filnam=sprintf('minicalpots_%d%02d%02d%02d%02d%02d',...
timi(1), timi(2),timi(3),timi(4),timi(5),round(timi(6)));
save(filnam)

% pvn{1+(mm-1)*npv}=sprintf('%s%d:pot1',pvi,mm);
% pvn{2+(mm-1)*npv}=sprintf('%s%d:pot2',pvi,mm);
% pvn{3+(mm-1)*npv}=sprintf('%s%d:pot3',pvi,mm);
% pvn{4+(mm-1)*npv}=sprintf('%s%d:lvdt1',pvi,mm);
% pvn{5+(mm-1)*npv}=sprintf('%s%d:lvdt2',pvi,mm);
% pvn{6+(mm-1)*npv}=sprintf('%s%d:lvdt3',pvi,mm);
% pvn{7+(mm-1)*npv}=sprintf('%s%d:x',pvi,mm);
% pvn{8+(mm-1)*npv}=sprintf('%s%d:y',pvi,mm);
% pvn{9+(mm-1)*npv}=sprintf('%s%d:tilt',pvi,mm);
% pvn{10+(mm-1)*npv}=sprintf('%s%d:x:alt',pvi,mm);
% pvn{11+(mm-1)*npv}=sprintf('%s%d:y:alt',pvi,mm);
% pvn{12+(mm-1)*npv}=sprintf('%s%d:tilt:alt',pvi,mm);
% pvn{13+(mm-1)*npv}=sprintf('%s%d:lvdt1:raw',pvi,mm);
% pvn{14+(mm-1)*npv}=sprintf('%s%d:lvdt2:raw',pvi,mm);
% pvn{15+(mm-1)*npv}=sprintf('%s%d:lvdt3:raw',pvi,mm);
% pvn{16+(mm-1)*npv}=sprintf('%s%d:motor1:sumsteps',pvi,mm);
% pvn{17+(mm-1)*npv}=sprintf('%s%d:motor2:sumsteps',pvi,mm);
% pvn{18+(mm-1)*npv}=sprintf('%s%d:motor3:sumsteps',pvi,mm);

potids=sort([1:18:length(pvn) 2:18:length(pvn) 3:18:length(pvn)]);
stepids=sort([16:18:length(pvn) 17:18:length(pvn) 18:18:length(pvn)]);
allpots=dater(potids,:);
allsteps=dater(stepids,:);
for nn=1:length(potids)
 [q(:,nn),dq(nn,:)]=plot_polyfit(allsteps(nn,:),allpots(nn,:),1,1);
 gb(nn)=input('good (1) or bad(2)? ');
end

gid=find(gb==1);bid=find(gb==2);
potxn=cell(length(badmotors)*3,1);
ii=0;
for mm=1:length(badmotors)
 MM=badmotors(mm);
 for cc=1:3
   ii=ii+1;
   potxn{ii,1}=sprintf('c1:qmov:m%d:cam%d:potMax',MM,cc);
 end
end
oldmaxps=lcaGet(potxn);

for nn=1:length(gid)
 tid=gid(nn);
 lcaPut(potxn{tid},q(2,tid)*40000)
end
newmaxps=lcaGet(potxn);

idn=bid(12)
alls=allsteps(idn,:);allp=allpots(idn,:);
plot(allp,'.-');grid
allp(19)=[];alls(end)=[];
plot(alls,allp,'.-')
[q,dq]=plot_polyfit(alls,allp,1,1)
fprintf(1,'\n %8.4f =- %8.4g\n\n',q(2)*40000,dq(2)*40000);
lcaPut(potxn{idn},q(2)*40000)


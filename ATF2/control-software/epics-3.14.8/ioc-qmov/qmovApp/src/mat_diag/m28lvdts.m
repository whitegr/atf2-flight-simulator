pvi='c1:qmov:m';
mm=28;
ii=1;
pvn=cell(18,1);
pvn{1}=sprintf('%s%d:pot1',pvi,mm);
pvn{2}=sprintf('%s%d:pot2',pvi,mm);
pvn{3}=sprintf('%s%d:pot3',pvi,mm);
pvn{4}=sprintf('%s%d:lvdt1',pvi,mm);
pvn{5}=sprintf('%s%d:lvdt2',pvi,mm);
pvn{6}=sprintf('%s%d:lvdt3',pvi,mm);
pvn{7}=sprintf('%s%d:x',pvi,mm);
pvn{8}=sprintf('%s%d:y',pvi,mm);
pvn{9}=sprintf('%s%d:tilt',pvi,mm);
pvn{10}=sprintf('%s%d:x:pot',pvi,mm);
pvn{11}=sprintf('%s%d:y:pot',pvi,mm);
pvn{12}=sprintf('%s%d:tilt:pot',pvi,mm);
pvn{13}=sprintf('%s%d:x:lvdt',pvi,mm);
pvn{14}=sprintf('%s%d:y:lvdt',pvi,mm);
pvn{15}=sprintf('%s%d:tilt:lvdt',pvi,mm);
pvn{16}=sprintf('%s%d:lvdt1:raw',pvi,mm);
pvn{17}=sprintf('%s%d:lvdt2:raw',pvi,mm);
pvn{18}=sprintf('%s%d:lvdt3:raw',pvi,mm);

a=input('just say when ','s');
while (~strcmp(a,'done'))
  foo=lcaGet(pvn);
  dater(:,ii)=foo(:,1);
  ii=ii+1;
a=input('just say when ','s');
 end

pot1=dater(1,:);
pot2=dater(2,:);
pot3=dater(3,:);
lvdt1=dater(4,:);
lvdt2=dater(5,:);
lvdt3=dater(6,:);
xpsx=dater(7,:);
xpsy=dater(8,:);
xpst=dater(9,:);
potx=dater(10,:);
poty=dater(11,:);
pott=dater(12,:);
lvdtx=dater(13,:);
lvdty=dater(14,:);
lvdtt=dater(15,:);
l1raw=dater(16,:);
l2raw=dater(17,:);
l3raw=dater(18,:);
  
save m28lvdts.mat

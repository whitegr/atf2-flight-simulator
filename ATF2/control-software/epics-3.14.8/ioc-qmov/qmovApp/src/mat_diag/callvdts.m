pvi='c1:qmov:m';
mm=13;
ii=1;
clear MM dater foo
pvn=cell(18,1);
pvn{1}=sprintf('%s%d:pot1',pvi,mm);
pvn{2}=sprintf('%s%d:pot2',pvi,mm);
pvn{3}=sprintf('%s%d:pot3',pvi,mm);
pvn{4}=sprintf('%s%d:lvdt1',pvi,mm);
pvn{5}=sprintf('%s%d:lvdt2',pvi,mm);
pvn{6}=sprintf('%s%d:lvdt3',pvi,mm);
pvn{7}=sprintf('%s%d:x',pvi,mm);
pvn{8}=sprintf('%s%d:y',pvi,mm);
pvn{9}=sprintf('%s%d:tilt',pvi,mm);
pvn{10}=sprintf('%s%d:x:alt',pvi,mm);
pvn{11}=sprintf('%s%d:y:alt',pvi,mm);
pvn{12}=sprintf('%s%d:tilt:alt',pvi,mm);
pvn{13}=sprintf('%s%d:lvdt1:raw',pvi,mm);
pvn{14}=sprintf('%s%d:lvdt2:raw',pvi,mm);
pvn{15}=sprintf('%s%d:lvdt3:raw',pvi,mm);
pvn{16}=sprintf('%s%d:motor1:sumsteps',pvi,mm);
pvn{17}=sprintf('%s%d:motor2:sumsteps',pvi,mm);
pvn{18}=sprintf('%s%d:motor3:sumsteps',pvi,mm);


setting=0;ii=0;
while (setting~=9999)
 ii=ii+1;
 setting=input('what is the setting now? ');
 if setting==9999
  ii=ii-1;
  break
 else
  MM(ii)=setting;
  pause(4)
  foo=lcaGet(pvn);
  dater(:,ii)=foo(:,1);
 end
end

  

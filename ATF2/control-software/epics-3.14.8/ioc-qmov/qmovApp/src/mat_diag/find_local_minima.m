function id=find_local_minima(arr)

%function id=find_local_minima(arr)
id=[];
for nn=2:length(arr)-1
 if (arr(nn-1)>arr(nn) & arr(nn)<arr(nn+1))
   id=[id nn];
 end
end

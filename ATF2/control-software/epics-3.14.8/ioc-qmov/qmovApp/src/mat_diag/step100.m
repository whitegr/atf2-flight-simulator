pvi='c1:qmov:m';
mm=1;
ii=1;
pvn=cell(14,1);
pvn{1}=sprintf('%s%d:pot1',pvi,mm);
pvn{2}=sprintf('%s%d:pot2',pvi,mm);
pvn{3}=sprintf('%s%d:pot3',pvi,mm);
pvn{4}=sprintf('%s%d:lvdt1',pvi,mm);
pvn{5}=sprintf('%s%d:lvdt2',pvi,mm);
pvn{6}=sprintf('%s%d:lvdt3',pvi,mm);
pvn{7}=sprintf('%s%d:x',pvi,mm);
pvn{8}=sprintf('%s%d:y',pvi,mm);
pvn{9}=sprintf('%s%d:tilt',pvi,mm);
pvn{10}=sprintf('%s%d:x:alt',pvi,mm);
pvn{11}=sprintf('%s%d:y:alt',pvi,mm);
pvn{12}=sprintf('%s%d:tilt:alt',pvi,mm);
pvn{13}=sprintf('%s%d:x:bpm',pvi,mm);
pvn{14}=sprintf('%s%d:y:bpm',pvi,mm);

nsteps=cell(3,1);
nsteps{1}=sprintf('%s%d:motor1:nStepsRemain',pvi,mm);
nsteps{2}=sprintf('%s%d:motor2:nStepsRemain',pvi,mm);
nsteps{3}=sprintf('%s%d:motor3:nStepsRemain',pvi,mm);

yset=sprintf('%s%d:y:set',pvi,mm);
perturb=sprintf('%s%d:perturb.PROC',pvi,mm);

yvals=-1200:100:1200;

dater(:,ii)=lcaGet(pvn);

for nn=1:length(yvals)
  lcaPut(yset,yvals(nn));
  lcaPut(perturb,1);
  pause(0.5)
  while sum(lcaGet(nsteps))~=0
    pause(0.5)
  end
  pause(7); %sams suck
  ii=ii+1;
  dater(:,ii)=lcaGet(pvn);
end

lcaPut(yset,0);
lcaPut(perturb,1);
  

  

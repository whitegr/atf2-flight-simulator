pvi='c1:qmov:m';
mm=22;
ii=1;
pvn=cell(18,1);
pvn{1}=sprintf('%s%d:pot1',pvi,mm);
pvn{2}=sprintf('%s%d:pot2',pvi,mm);
pvn{3}=sprintf('%s%d:pot3',pvi,mm);
pvn{4}=sprintf('%s%d:lvdt1',pvi,mm);
pvn{5}=sprintf('%s%d:lvdt2',pvi,mm);
pvn{6}=sprintf('%s%d:lvdt3',pvi,mm);
pvn{7}=sprintf('%s%d:x:pot',pvi,mm);
pvn{8}=sprintf('%s%d:y:pot',pvi,mm);
pvn{9}=sprintf('%s%d:tilt:pot',pvi,mm);
pvn{10}=sprintf('%s%d:x:lvdt',pvi,mm);
pvn{11}=sprintf('%s%d:y:lvdt',pvi,mm);
pvn{12}=sprintf('%s%d:tilt:lvdt',pvi,mm);
pvn{13}=sprintf('%s%d:lvdt1:raw',pvi,mm);
pvn{14}=sprintf('%s%d:lvdt2:raw',pvi,mm);
pvn{15}=sprintf('%s%d:lvdt3:raw',pvi,mm);
pvn{16}=sprintf('%s%d:motor1:sumsteps',pvi,mm);
pvn{17}=sprintf('%s%d:motor2:sumsteps',pvi,mm);
pvn{18}=sprintf('%s%d:motor3:sumsteps',pvi,mm);

nsteps=cell(3,1);
nsteps{1}=sprintf('%s%d:motor1:nStepsRemain',pvi,mm);
nsteps{2}=sprintf('%s%d:motor2:nStepsRemain',pvi,mm);
nsteps{3}=sprintf('%s%d:motor3:nStepsRemain',pvi,mm);

stepdiag{1,1}=sprintf('%s%d:motor1:stepdiag',pvi,mm);
stepdiag{2,1}=sprintf('%s%d:motor2:stepdiag',pvi,mm);
stepdiag{3,1}=sprintf('%s%d:motor3:stepdiag',pvi,mm);

start=0;
steps=250;
tic;
foo=lcaGet(pvn);
dater(:,ii)=foo(:,1);
for jj=1:3
  lcaPut(stepdiag{jj,1},start);
  pause(0.25)
end
pause(0.5)
while sum(lcaGet(nsteps))~=0
  pause(0.5)
end
pause(7); %sams suck
ii=ii+1;
foo=lcaGet(pvn);
dater(:,ii)=foo(:,1);

for nn=1:120
  for jj=1:3
    lcaPut(stepdiag{jj,1},steps);
    pause(0.25)
  end
  pause(12); %sams suck
  ii=ii+1;
  foo=lcaGet(pvn);
  dater(:,ii)=foo(:,1);
  fprintf(1,'%d ',nn);
  
end
lcaPut(stepdiag,[-15000;-15000;-15000]);
toc
fprintf(1,'\n');

pot1=dater(1,:);
pot2=dater(2,:);
pot3=dater(3,:);
lvdt1=dater(4,:);
lvdt2=dater(5,:);
lvdt3=dater(6,:);
potx=dater(7,:);
poty=dater(8,:);
pott=dater(9,:);
lvdtx=dater(10,:);
lvdty=dater(11,:);
lvdtt=dater(12,:);
lvdt1r=dater(13,:);
lvdt2r=dater(14,:);
lvdt3r=dater(15,:);
m1steps=dater(16,:);
m2steps=dater(17,:);
m3steps=dater(18,:);

abc=clock;
filnam=sprintf('calpotm22_%d%02d%02d_%02d%02d',...
  abc(1),abc(2),abc(3),abc(4),abc(5),abc(6));
save(filnam)


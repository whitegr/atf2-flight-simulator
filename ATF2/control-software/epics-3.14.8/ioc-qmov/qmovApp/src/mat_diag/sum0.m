sum1=cell(28,1);
sum2=cell(28,1);
sum3=cell(28,1);
set0=cell(28,1);
p2s=cell(28,1);
p2l=cell(28,1);
m1step=cell(28,1);
m2step=cell(28,1);
m3step=cell(28,1);

pvi='c1:qmov:m';

for nn=1:28
  sum1{nn,1}=sprintf('%s%d:motor1:sumsteps',pvi,nn);
  sum2{nn,1}=sprintf('%s%d:motor2:sumsteps',pvi,nn);
  sum3{nn,1}=sprintf('%s%d:motor3:sumsteps',pvi,nn);
  set0{nn,1}=sprintf('%s%d:motor:set0.PROC',pvi,nn);
  p2s{nn,1}=sprintf('%s%d:pot2step.PROC',pvi,nn);
  p2l{nn,1}=sprintf('%s%d:pot2lvdt.PROC',pvi,nn);
  m1step{nn,1}=sprintf('%s%d:motor1:stepdiag',pvi,nn);
  m2step{nn,1}=sprintf('%s%d:motor2:stepdiag',pvi,nn);
  m3step{nn,1}=sprintf('%s%d:motor3:stepdiag',pvi,nn);
end

lcaPut(p2s,ones(28,1));
m1sum=-1*round(lcaGet(sum1));
m2sum=-1*round(lcaGet(sum2));
m3sum=-1*round(lcaGet(sum3));
[m1sum m2sum m3sum] %#ok<NOPTS>
pause(2)
lcaPut(m1step,m1sum);
lcaPut(m2step,m2sum);
lcaPut(m3step,m3sum);
% while (sum(lcaGet(nstepm1))+sum(lcaGet(nstepm2))+sum(lcaGet(nstepm3)))~=0
%   pause(0.5)
%   fprintf(1,'. ');
% end
% fprintf(1,'\n');
pause(7)

%pause(2)
lcaPut(set0,ones(28,1));
pause(10)
lcaPut(p2l,ones(28,1));
fprintf(1,'\n');

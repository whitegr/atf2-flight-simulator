function [wmean,werror]=wmean(xs,sigmas)

wmean=sum(xs./sigmas.^2)/sum(1./sigmas.^2);
werror=sqrt(1/sum(1./sigmas.^2));
return

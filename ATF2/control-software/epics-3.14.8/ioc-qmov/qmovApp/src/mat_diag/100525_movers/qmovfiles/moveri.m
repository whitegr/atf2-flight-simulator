function [x,y,alpha,ccam1,ccam2,ccam3]=moveri(cam1,cam2,cam3)
% [x,y,alpha,ccam1,ccam2,ccam3]=moveri(cam1,cam2,cam3);
%
% Calculate x/y/roll for an FFTB style mover based on it's cam angles.
% Notation according to Gordon Bowden's paper (SLAC-PUB-95-6132)
%
% INPUTs:
%   cam1 = cam 1 angle (midrange value =      0) [radians]
%   cam2 = cam 2 angle (midrange value = 3*pi/4) [radians]
%   cam3 = cam 3 angle (midrange value =   pi/4) [radians]
%
% OUTPUTs:
%   x     = x coordinate (midrange value = 0) [mm]
%   y     = y coordinate (midrange value = 0) [mm]
%   alpha = roll angle   (midrange value = 0) [radians]
%   ccam1 = cam 1 angle (from Gordon Bowden formulae) [radians]
%   ccam2 = cam 2 angle (from Gordon Bowden formulae) [radians]
%   ccam3 = cam 3 angle (from Gordon Bowden formulae) [radians]

% design dimensions [mm]

R=31.0;
L=1.5875;
S1=34.925;
S2=290.5;
a=145.25;
c=154.0;
b=c+S1-sqrt(2)*R;

% convert cam angles to theta values

theta1=cam1;
theta2=cam2-3*pi/4;
theta3=cam3-pi/4;

% initialize

co1=cos(theta1);si1=sin(theta1); 
co2=cos(theta2);si2=sin(theta2); 
co3=cos(theta3);si3=sin(theta3); 
sq2=sqrt(2); 
Niter=10;  %it seems that even 3-4 iterations are good enough

% solve iteratively

y1=c-b;  % initial guess
x1=0;    % initial guess
alpha=0; % initial guess
for n=1:Niter
  alpha_new=((y1+b-c)+L*(alpha*co1-si1))/(S2+x1);
  x1_new=alpha*(S1-y1)+L/sq2*(alpha*co2-si2+alpha*co3-si3);
  y1_new=sq2*R-S1+alpha*x1+L/sq2*(alpha*co2-si2-alpha*co3+si3);
  alpha=alpha_new;
  x1=x1_new;
  y1=y1_new;
 %uncomment the next line to check convergence
 disp(['x1=' num2str(x1) ' y1=' num2str(y1) ' alpha=' num2str(alpha) ]);
end
x=x1+a-a*cos(alpha)-b*sin(alpha);
y=y1-c+b*cos(alpha)-a*sin(alpha);

% cross check with direct formula from Gordon

x1=x+a*cos(alpha)+b*sin(alpha)-a;
y1=y-b*cos(alpha)+a*sin(alpha)+c;
betap=pi/4+alpha;
betam=pi/4-alpha;

arg1=((x1+S2)*sin(alpha)-y1*cos(alpha)+(c-b))/L;
arg2=((x1+S1)*sin(betam)+y1*cos(betam)-R)/L;
arg3=((x1-S1)*sin(betap)-y1*cos(betap)+R)/L;

theta1=alpha-asin(arg1);
theta2=alpha-asin(arg2);
theta3=alpha-asin(arg3);

%disp(['th1=' num2str(th1_check) ' th2=' num2str(th2_check) ' th3=' num2str(th3_check) ]);

ccam1=theta1;
ccam2=theta2+3*pi/4;
ccam3=theta3+pi/4;

end

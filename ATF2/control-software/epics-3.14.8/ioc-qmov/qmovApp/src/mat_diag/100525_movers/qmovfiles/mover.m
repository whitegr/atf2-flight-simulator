function [cam1,cam2,cam3]=mover(x,y,alpha)
% [cam1,cam2,cam3]=mover(x,y,alpha);
%
% Calculate cam angles for an FFTB style mover based on it's x/y/roll.
% Notation according to Gordon Bowden's paper (SLAC-PUB-95-6132)
%
% INPUTs:
%   x     = x coordinate (midrange value = 0) [mm]
%   y     = y coordinate (midrange value = 0) [mm]
%   alpha = roll angle   (midrange value = 0) [radians]
%
% OUTPUTs:
%   cam1 = cam 1 angle (midrange value =      0) [radians]
%   cam2 = cam 2 angle (midrange value = 3*pi/4) [radians]
%   cam3 = cam 3 angle (midrange value =   pi/4) [radians]

% design dimensions [mm]

R=31.0;
L=1.5875;
S1=34.925;
S2=290.5;
a=145.25;
c=154.0;
b=c+S1-sqrt(2)*R;

x1=x+a*cos(alpha)+b*sin(alpha)-a;
y1=y-b*cos(alpha)+a*sin(alpha)+c;
betap=pi/4+alpha;
betam=pi/4-alpha;

arg1=((x1+S2)*sin(alpha)-y1*cos(alpha)+(c-b))/L;
arg2=((x1+S1)*sin(betam)+y1*cos(betam)-R)/L;
arg3=((x1-S1)*sin(betap)-y1*cos(betap)+R)/L;

theta1=alpha-asin(arg1);
theta2=alpha-asin(arg2);
theta3=alpha-asin(arg3);

cam1=theta1;
cam2=theta2+3*pi/4;
cam3=theta3+pi/4;

end

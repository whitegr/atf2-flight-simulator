function indices=between(arin,mini,maxi)
% indices=between(arin,mini,maxi);
indices=find(arin<maxi & arin>mini);
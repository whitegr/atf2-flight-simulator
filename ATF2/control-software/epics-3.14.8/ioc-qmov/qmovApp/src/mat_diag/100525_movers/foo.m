% try to figure out which are worse than the others

% background noise
for nn=1:28
  sd(nn).xyt=md(nn).xyt(:,4300:end);
  sd(nn).xytalt=md(nn).xytalt(:,4300:end);
  sd(nn).pot=md(nn).pot(:,4300:end);
  sd(nn).lraw=md(nn).lraw(:,4300:end);
  sd(nn).lvdt=md(nn).lvdt(:,4300:end);
  sd(nn).xytm=mean(sd(nn).xyt');
  sd(nn).xyts=std(sd(nn).xyt');
  sd(nn).xytam=mean(sd(nn).xytalt');
  sd(nn).xytas=std(sd(nn).xytalt');
  potm(nn,:)=mean(sd(nn).pot');
  pots(nn,:)=std(sd(nn).pot');
  lrawm(nn,:)=mean(sd(nn).lraw');
  lraws(nn,:)=std(sd(nn).lraw');
  lvdtm(nn,:)=mean(sd(nn).lvdt');
  lvdts(nn,:)=std(sd(nn).lvdt');
end

for nn=1:28
  xx=sd(nn).xyt(1,:);
  yy=sd(nn).xyt(2,:);
  tt=sd(nn).xyt(3,:);
  xa=sd(nn).xytalt(1,:);
  ya=sd(nn).xytalt(2,:);
  ta=sd(nn).xytalt(3,:);
  
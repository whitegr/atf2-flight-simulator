function [x,y,t,z]=tiltedMoverAct(xbar,ybar,tbar,roll,pitch)
%
% [x,y,t,z]=tiltedMoverAct(xbar,ybar,tbar,roll,pitch);
%
% Given actual translations and rotation <xbar,ybar,tbar> for a mover mounted on
% a plate whose roll and pitch angles are specified, compute absolute real-space
% translations and rotation <x,y,t>; also return the absolute real-space
% longitudinal shift <z> caused by non-zero mover translations when pitch is
% non-zero.
%
% INPUTs:
%   xbar  = actual mover x position [m]
%   ybar  = actual mover y position [m]
%   tbar  = actual mover roll angle [radians]
%   roll  = installed x-y tilt of mover [radians] (positive roll is measured
%           clockwise from positive X-axis)
%   pitch = installed y-z tilt of mover [radians] (positive pitch is measured
%           counter-clockwise from positive Z-axis)
%
% OUTPUTs:
%   x = absolute x position [m] (midrange value = 0)
%   y = absolute y position [m] (midrange value = 0)
%   t = absolute roll angle [radians] (midrange value = 0)
%   z = absolute z position [m]

psi=[cos(roll),sin(roll);-sin(roll),cos(roll)]; % roll transformation
phi=[1,0;0,1/cos(pitch)]; % pitch transformation
v=[xbar;ybar];
v0=inv(psi*phi)*v;
x=v0(1);
y=v0(2);
t=tbar+roll;
z=-ybar*sin(pitch);

end
function [xbar,ybar,tbar,z]=tiltedMoverDes(x,y,t,roll,pitch)
%
% [xbar,ybar,tbar,z]=tiltedMoverDes(x,y,t,roll,pitch);
%
% Given desired absolute real-space translations and rotation <x,y,t>, compute
% required translations and rotation <xbar,ybar,tbar> for a mover mounted on a
% plate whose roll and pitch angles are specified; also return the absolute
% real-space longitudinal shift <z> caused by non-zero translations when pitch
% is non-zero.
%
% INPUTs:
%   x     = desired absolute x position [m] (midrange value = 0)
%   y     = desired absolute y position [m] (midrange value = 0)
%   t     = desired absolute roll angle [radians] (midrange value = 0)
%   roll  = installed x-y tilt of mover [radians] (positive roll is measured
%           clockwise from positive X-axis)
%   pitch = installed y-z tilt of mover [radians] (positive pitch is measured
%           counter-clockwise from positive Z-axis)
%
% OUTPUTs:
%   xbar = required mover x position [m]
%   ybar = required mover y position [m]
%   tbar = required mover roll angle [radians]
%   z    = absolute z position [m]

psi=[cos(roll),sin(roll);-sin(roll),cos(roll)]; % roll transformation
phi=[1,0;0,1/cos(pitch)]; % pitch transformation
v0=[x;y];
v=psi*phi*v0;
xbar=v(1);
ybar=v(2);
tbar=t-roll;
z=-ybar*sin(pitch);

end

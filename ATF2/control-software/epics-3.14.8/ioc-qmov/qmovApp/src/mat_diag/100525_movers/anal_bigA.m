%decode bigA_frinite
nam=strvcat(pvn); %(cell array to str array)
[rr,cc]=size(bigA);
nmov=28;
npv=rr/28;
%%
for nn=1:nmov
  stind=1+(nn-1)*23;
  mm{nn}=bigA(stind:stind+22,:);
  md(nn).xyt=mm{nn}(1:3,:);
  md(nn).xytalt=mm{nn}(4:6,:);
  md(nn).set=mm{nn}(7:9,:);
  md(nn).trim=mm{nn}(10,:);
  md(nn).itry=mm{nn}(11,:);
  md(nn).pot=mm{nn}(12:14,:);
  md(nn).lraw=mm{nn}(15:17,:);
  md(nn).lped=mm{nn}(18:20,:);
  md(nn).lvdt=mm{nn}(21:23,:);
  md(nn).mov=find(md(nn).trim);
  md(nn).first=find(md(nn).trim, 1 );
  md(nn).last=find(md(nn).trim, 1, 'last' );
end
%%
colors='krbgymc';
lini{1}='-';lini{2}='--';lini{3}=':';lini{4}='-.';
ii=1;linsp=[];
for nn=1:7
  for mm=1:4
    linsp=strvcat(linsp,[colors(nn) lini{mm}]);
  end
end
%referee boundaries
maybebad=[1 2 3 4 11 12 19 20 21 22]
%screw this

for nn=1:28
  clf
  lrstd(nn,:)=std(md(nn).lraw(:,md(nn).first:md(nn).last)');
end

%mover8 lvdt 3 has a bad lvdt & mover 16 lvdt 3
%%
for nn=1:28
  lvdtp(:,nn)=md(nn).lped(:,end);
  lvdtp1(:,nn)=md(nn).lped(:,1);
end
%%
for nn=1:7
  nnval(1)=1+(nn-1)*4;
  nnval(2)=2+(nn-1)*4;
  nnval(3)=3+(nn-1)*4;
  nnval(4)=4+(nn-1)*4;
  clf
  for mm=1:4
    subplot(2,2,mm)
    plot(md(nnval(mm)).xyt(:,md(nnval(mm)).first:md(nnval(mm)).last)')
    hold on
    plot(md(nnval(mm)).xytalt(1,md(nnval(mm)).first:md(nnval(mm)).last),'k-')
    plot(md(nnval(mm)).xytalt(2,md(nnval(mm)).first:md(nnval(mm)).last),'c-')
    plot(md(nnval(mm)).xytalt(3,md(nnval(mm)).first:md(nnval(mm)).last),'m-')
    vv=axis;
    hold on
    if mm==1
      legend('x','y','t','xalt','yalt','talt','Location','Best')
    end
    plot(vv(1:2),[1000 1000],'k:')
    plot(vv(1:2),[-1000 -1000],'k:')
    title(sprintf('%d',nnval(mm)))
  end
  fnam=sprintf('m%d-%dxyt2.jpg',nnval(1),nnval(4));
disp(sprintf('pause %nnval(mm)'))
pause
print('-djpeg',fnam)


end
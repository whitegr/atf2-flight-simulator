Nsteps=[-10000 15000 -15000];
for mm=1:28
  for cc=1:3
    potv(mm,cc)=lcaGet(sprintf('c1:qmov:m%d:pot%d',mm,cc));
    foo=lcaGet(sprintf('c1:qmov:m%d:pot%d:pred',mm,cc));
    potd(mm,cc)=foo(1);
    pota(mm,cc)=lcaGet(sprintf('c1:qmov:m%d:cam%d:potZAngle',mm,cc));
    potx(mm,cc)=lcaGet(sprintf('c1:qmov:m%d:cam%d:potMax',mm,cc));
    potn(mm,cc)=lcaGet(sprintf('c1:qmov:m%d:cam%d:potMin',mm,cc));
    nsteps=Nsteps(cc);
    npotz(mm,cc)=potd(mm,cc)-5-nsteps*(potx(mm,cc)-potn(mm,cc))/40000;
    npotx(mm,cc)=potn(mm,cc)+40000*(potd(mm,cc)-pota(mm,cc)-5)/nsteps;
  end
  fprintf(1,'.');
end
fprintf(1,'\n')

for mm=1:28
  nn=mm;
  potx(nn)=lcaGet(sprintf('c1:qmov:m%d:x:pot',mm));
  poty(nn)=lcaGet(sprintf('c1:qmov:m%d:y:pot',mm));
  pott(nn)=lcaGet(sprintf('c1:qmov:m%d:tilt:pot',mm));
  lvdtx(nn)=lcaGet(sprintf('c1:qmov:m%d:x:lvdt',mm));
  lvdty(nn)=lcaGet(sprintf('c1:qmov:m%d:y:lvdt',mm));
  lvdtt(nn)=lcaGet(sprintf('c1:qmov:m%d:tilt:lvdt',mm));
  foo=lcaGet(sprintf('c1:qmov:m%d:x:step',mm));
  stepx(nn)=foo(1);
  foo=lcaGet(sprintf('c1:qmov:m%d:y:step',mm));
  stepy(nn)=foo(1);
  foo=lcaGet(sprintf('c1:qmov:m%d:tilt:step',mm));
  stept(nn)=foo(1);
end



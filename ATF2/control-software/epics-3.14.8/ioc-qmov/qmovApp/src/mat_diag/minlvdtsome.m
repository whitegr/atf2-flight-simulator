clear dater pvn mm
pvi='c1:qmov:m';
mm=26;
mms=input('Which mover number [1-28]? ','s');
%mm=round(mm);
mms=str2num(mms);
if find(mm>28) | find(mm<1)
 return
end
numm=length(mm);
ii=1;
pvn=cell(10*numm,1);
for nn=1:numm
mm=mms(nn);
pvn{ii}=sprintf('%s%d:pot1',pvi,mm);
pvn{ii+1}=sprintf('%s%d:pot2',pvi,mm);
pvn{ii+2}=sprintf('%s%d:pot3',pvi,mm);
pvn{ii+3}=sprintf('%s%d:lvdt1',pvi,mm);
pvn{ii+4}=sprintf('%s%d:lvdt2',pvi,mm);
pvn{ii+5}=sprintf('%s%d:lvdt3',pvi,mm);
pvn{ii+6}=sprintf('%s%d:x',pvi,mm);
pvn{ii+7}=sprintf('%s%d:y',pvi,mm);
pvn{ii+8}=sprintf('%s%d:tilt',pvi,mm);
pvn{ii+9}=sprintf('%s%d:motor1:sumsteps',pvi,mm);
ii=ii+10;
sstep{nn,1}=sprintf('%s%d:motor1:sumsteps',pvi,mm);
nstep{nn,1}=sprintf('%s%d:motor1:nStepsRemain',pvi,mm);
movesteps{nn,1}=sprintf('%s%d:motor1:stepdiag',pvi,mm);
end
dater(:,ii)=lcaGet(pvn);
startsteps=lcaGet(sstep);
lcaSetMonitor(pvn{5});
lcaPut(movesteps,-1000);
pause(0.5)
while lcaGet(nstep)~=0
  pause(0.5)
  disp('pause')
end
disp('done')
pause(7)
ii=ii+1;
dater(:,ii)=lcaGet(pvn);
for nn=1:40
  lcaPut(movesteps,50);
  pause(0.5)
  while lcaGet(nstep)~=0
    pause(0.5)
  end
  pause(7)
  ii=ii+1;
  lcaNewMonitorWait(pvn{5})
  dater(:,ii)=lcaGet(pvn);
  fprintf(1,'%8.4e ',dater(5,ii));  
  pause(1.1)
  ii=ii+1;
  lcaNewMonitorWait(pvn{5})
  dater(:,ii)=lcaGet(pvn);
  fprintf(1,'%8.4e ',dater(5,ii));  
  pause(1.1)
  ii=ii+1;
  lcaNewMonitorWait(pvn{5})
  dater(:,ii)=lcaGet(pvn);
  fprintf(1,'%8.4e %d \n',dater(5,ii),nn);
end
fprintf(1,'\n');
lcaPut(movesteps,-1000);

dater2=dater;
pot1=dater2(1,:);
pot2=dater2(2,:);
pot3=dater2(3,:);
lvdt1=dater2(4,:);
lvdt2=dater2(5,:);
lvdt3=dater2(6,:);
potx=dater2(7,:);
poty=dater2(8,:);
pott=dater2(9,:);
m1s=dater2(10,:);
%those that show min/max behavior for motor1
%
%for nn=1:12
%  [Q(:,nn),dQ(:,nn)]=plot_parab(pot1,dater2(nn,:)-mean(dater2(nn,:)),1);
%  pause
%end
tt=clock;
filnam=sprintf('minlvdt2_m%d_%02d%02d%02dT%02d%02d%02d.mat',mm,tt(1),tt(2),tt(3),tt(4),tt(5),round(tt(6)))
save(filnam) 

pvi='c1:qmov:m';
npv=18;
ii=1;
pvn=cell(18*28,1);
for mm=1:28
pvn{1+(mm-1)*npv}=sprintf('%s%d:pot1',pvi,mm);
pvn{2+(mm-1)*npv}=sprintf('%s%d:pot2',pvi,mm);
pvn{3+(mm-1)*npv}=sprintf('%s%d:pot3',pvi,mm);
pvn{4+(mm-1)*npv}=sprintf('%s%d:lvdt1',pvi,mm);
pvn{5+(mm-1)*npv}=sprintf('%s%d:lvdt2',pvi,mm);
pvn{6+(mm-1)*npv}=sprintf('%s%d:lvdt3',pvi,mm);
pvn{7+(mm-1)*npv}=sprintf('%s%d:x:pot',pvi,mm);
pvn{8+(mm-1)*npv}=sprintf('%s%d:y:pot',pvi,mm);
pvn{9+(mm-1)*npv}=sprintf('%s%d:tilt:pot',pvi,mm);
pvn{10+(mm-1)*npv}=sprintf('%s%d:x:lvdt',pvi,mm);
pvn{11+(mm-1)*npv}=sprintf('%s%d:y:lvdt',pvi,mm);
pvn{12+(mm-1)*npv}=sprintf('%s%d:tilt:lvdt',pvi,mm);
pvn{13+(mm-1)*npv}=sprintf('%s%d:lvdt1:raw',pvi,mm);
pvn{14+(mm-1)*npv}=sprintf('%s%d:lvdt2:raw',pvi,mm);
pvn{15+(mm-1)*npv}=sprintf('%s%d:lvdt3:raw',pvi,mm);
pvn{16+(mm-1)*npv}=sprintf('%s%d:motor1:sumsteps',pvi,mm);
pvn{17+(mm-1)*npv}=sprintf('%s%d:motor2:sumsteps',pvi,mm);
pvn{18+(mm-1)*npv}=sprintf('%s%d:motor3:sumsteps',pvi,mm);
end
nsteps=cell(3*28,1);stepdiag=cell(3*28,1);
ii=1;
for mm=1:28
nsteps{1+(mm-1)*3}=sprintf('%s%d:motor1:nStepsRemain',pvi,mm);
nsteps{2+(mm-1)*3}=sprintf('%s%d:motor2:nStepsRemain',pvi,mm);
nsteps{3+(mm-1)*3}=sprintf('%s%d:motor3:nStepsRemain',pvi,mm);

stepdiag{1+(mm-1)*3,1}=sprintf('%s%d:motor1:stepdiag',pvi,mm);
stepdiag{2+(mm-1)*3,1}=sprintf('%s%d:motor2:stepdiag',pvi,mm);
stepdiag{3+(mm-1)*3,1}=sprintf('%s%d:motor3:stepdiag',pvi,mm);
end

start=-5000;
steps=250;
tic;
foo=lcaGet(pvn);
dater(:,ii)=foo(:,1);
  lcaPut(stepdiag,start*ones(size(stepdiag)));
  pause(0.5)
  while sum(lcaGet(nsteps))~=0
    pause(0.5)
  end
  pause(7); %sams suck
  ii=ii+1;
foo=lcaGet(pvn);
dater(:,ii)=foo(:,1);
tic
for nn=1:40
  lcaPut(stepdiag,steps*ones(size(stepdiag)));
  pause(12); %sams suck
  ii=ii+1;
foo=lcaGet(pvn);
dater(:,ii)=foo(:,1);
  fprintf(1,'%d ',nn);

end
lcaPut(stepdiag,start*ones(size(stepdiag)));
toc
fprintf(1,'\n');
  
% pvn{1+(mm-1)*npv}=sprintf('%s%d:pot1',pvi,mm);
% pvn{2+(mm-1)*npv}=sprintf('%s%d:pot2',pvi,mm);
% pvn{3+(mm-1)*npv}=sprintf('%s%d:pot3',pvi,mm);
% pvn{4+(mm-1)*npv}=sprintf('%s%d:lvdt1',pvi,mm);
% pvn{5+(mm-1)*npv}=sprintf('%s%d:lvdt2',pvi,mm);
% pvn{6+(mm-1)*npv}=sprintf('%s%d:lvdt3',pvi,mm);
% pvn{7+(mm-1)*npv}=sprintf('%s%d:x',pvi,mm);
% pvn{8+(mm-1)*npv}=sprintf('%s%d:y',pvi,mm);
% pvn{9+(mm-1)*npv}=sprintf('%s%d:tilt',pvi,mm);
% pvn{10+(mm-1)*npv}=sprintf('%s%d:x:alt',pvi,mm);
% pvn{11+(mm-1)*npv}=sprintf('%s%d:y:alt',pvi,mm);
% pvn{12+(mm-1)*npv}=sprintf('%s%d:tilt:alt',pvi,mm);
% pvn{13+(mm-1)*npv}=sprintf('%s%d:lvdt1:raw',pvi,mm);
% pvn{14+(mm-1)*npv}=sprintf('%s%d:lvdt2:raw',pvi,mm);
% pvn{15+(mm-1)*npv}=sprintf('%s%d:lvdt3:raw',pvi,mm);
% pvn{16+(mm-1)*npv}=sprintf('%s%d:motor1:sumsteps',pvi,mm);
% pvn{17+(mm-1)*npv}=sprintf('%s%d:motor2:sumsteps',pvi,mm);
% pvn{18+(mm-1)*npv}=sprintf('%s%d:motor3:sumsteps',pvi,mm);

potids=sort([1:18:504 2:18:504 3:18:504]);
stepids=sort([16:18:504 17:18:504 18:18:504]);
allpots=dater(potids,:);
allsteps=dater(stepids,:);
for nn=1:length(potids)
  [q(:,nn),dq(nn,:)]=plot_polyfit(allsteps(nn,:),allpots(nn,:),1,1);
  gb(nn)=input('good (1) or bad(2)? ');
end

gid=find(gb==1);bid=find(gb==2);
potxn=cell(84,1);
ii=0;
for mm=1:28
  for cc=1:3
    ii=ii+1;
    potxn{ii,1}=sprintf('c1:qmov:m%d:cam%d:potMax',mm,cc);
  end
end
oldmaxps=lcaGet(potxn);

for nn=1:length(gid)
  tid=gid(nn);
  lcaPut(potxn{tid},q(2,tid)*40000)
end
newer3maxps=lcaGet(potxn);

idn=bid(2)
alls=allsteps(idn,:);allp=allpots(idn,:);
plot(allp,'.-')
grid
allp(19)=[];alls(end)=[];
plot(alls,allp,'.-')
[q,dq]=plot_polyfit(alls,allp,1,1)
q(2)*40000
lcaPut(potxn{idn},q(2)*40000)
  

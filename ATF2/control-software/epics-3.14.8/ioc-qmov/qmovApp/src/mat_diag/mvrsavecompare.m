% load your favorite mvrsave save file
% this should get: allcharvals, allvals, pvn, and pvnchar
allvalsNew=lcaGet(pvn);
allcharvalsNew=lcaGet(pvnchar);

%id=find(allvals~=allvalsNew);
id=find(abs(allvals~=allvalsNew)>1e-5);
if isempty(id)
  fprintf(1,'\nNo differences in the mover constant numbers\n')
else
  fprintf(1,...
    '\nThere is/are %d difference(s) in the mover constant numbers\n\n', ...
    length(id))
  for nn=1:length(id)
    fprintf(1,...
      '\nFor %s old value=%8.4g new value=%8.4g\n',pvn{id(nn)}, ...
      allvals(id(nn)),allvalsNew(id(nn)));
    rest=input(sprintf('Restore %s to old value [n]? ',pvn{id(nn)}),'s');
    if ~isempty(rest)
      lcaPut(pvn{id(nn)},allvals(id(nn)));
      newval=lcaGet(pvn{id(nn)});
      fprintf(1,'%s set to %8.4g\n',pvn{id(nn)},newval);
    end
  end
end

id=find(~strcmp(allcharvals,allcharvalsNew));
if isempty(id)
  fprintf(1,'\nNo differences in the mover constant strings\n')
else
  fprintf(1,...
    '\nThere is/are %d difference(s) in the mover constant strings\n\n', ...
    length(id))
  for nn=1:length(id)
    fprintf(1,...
      'For %s old value=%s new value=%s\n',pvnchar{id(nn)}, ...
      allcharvals{id(nn)},allcharvalsNew{id(nn)});
    rest=input(sprintf('Restore %s to old value [n]? ',pvnchar{id(nn)}),'s');
    if ~isempty(rest)
      lcaPut(pvnchar{id(nn)},allcharvals{id(nn)});
      newval=lcaGet(pvnchar{id(nn)},1,'char');
      fprintf(1,'%s set to %s\n',pvnchar{id(nn)},newval{1});
    end
  end
end
fprintf(1,'\n');

if exist('pvnrb')
  allrbvalsNew=lcaGet(pvnrb);
  id=find(allrbvals~=allrbvalsNew);
  if isempty(id)
    fprintf(1,...
    '\nThere are no differences in readback values\n');
  else
    fprintf(1,...
    '\nThere are %d differences in readback values\n',length(id));
    for nn=1:length(id)
      fprintf(1,'%s\t%8.4e\t%8.4e\n',pvnrb{id(nn)},allrbvals(id(nn)),...
      allrbvalsNew(id(nn)));
    end
  end
end

fid=1; %fopen(...
tt=clock;
fnam=sprintf('setupmovers_%02d%02d%02dT%02d%02d%02d.txt',...
    tt(1)-2000,round(tt(2:6)));
fid=fopen(fnam,'a');
for mm=1:28
    
    starttim=now;
    %mm=17;
    pvn=cell(26,1);
    pvn{1}=sprintf('c1:qmov:m%d:x',mm);
    pvn{2}=sprintf('c1:qmov:m%d:y',mm);
    pvn{3}=sprintf('c1:qmov:m%d:tilt',mm);
    pvn{4}=sprintf('c1:qmov:m%d:x:alt',mm);
    pvn{5}=sprintf('c1:qmov:m%d:y:alt',mm);
    pvn{6}=sprintf('c1:qmov:m%d:tilt:alt',mm);
    pvn{7}=sprintf('c1:qmov:m%d:x:set',mm);
    pvn{8}=sprintf('c1:qmov:m%d:y:set',mm);
    pvn{9}=sprintf('c1:qmov:m%d:tilt:set',mm);
    pvn{10}=sprintf('c1:qmov:m%d:x:file',mm);ls
    pvn{11}=sprintf('c1:qmov:m%d:y:file',mm);
    pvn{12}=sprintf('c1:qmov:m%d:tilt:file',mm);
    pvn{13}=sprintf('c1:qmov:m%d:dotrim.RVAL',mm);
    pvn{14}=sprintf('c1:qmov:m%d:itry',mm);
    pvn{15}=sprintf('c1:qmov:m%d:pot1',mm);
    pvn{16}=sprintf('c1:qmov:m%d:pot2',mm);
    pvn{17}=sprintf('c1:qmov:m%d:pot3',mm);
    pvn{18}=sprintf('c1:qmov:m%d:lvdt1:raw',mm);
    pvn{19}=sprintf('c1:qmov:m%d:lvdt2:raw',mm);
    pvn{20}=sprintf('c1:qmov:m%d:lvdt3:raw',mm);
    pvn{21}=sprintf('c1:qmov:m%d:lvdt1:ped',mm);
    pvn{22}=sprintf('c1:qmov:m%d:lvdt2:ped',mm);
    pvn{23}=sprintf('c1:qmov:m%d:lvdt3:ped',mm);
    pvn{24}=sprintf('c1:qmov:m%d:lvdt1',mm);
    pvn{25}=sprintf('c1:qmov:m%d:lvdt2',mm);
    pvn{26}=sprintf('c1:qmov:m%d:lvdt3',mm);
    setpedsn=sprintf('c1:qmov:m%d:lvdt:setpeds.PROC',mm);
    set0n=sprintf('c1:qmov:m%d:set0.PROC',mm);
    dotrimn=sprintf('c1:qmov:m%d:dotrim',mm);
    dopertn=sprintf('c1:qmov:m%d:perturb.PROC',mm);
    setsumn=sprintf('c1:qmov:m%d:motor:set0.PROC',mm);
    %steps remain
    mnsn{1,1}=sprintf('c1:qmov:m%d:motor1:nStepsRemain',mm);
    mnsn{2,1}=sprintf('c1:qmov:m%d:motor2:nStepsRemain',mm);
    mnsn{3,1}=sprintf('c1:qmov:m%d:motor3:nStepsRemain',mm);
    msdn{1,1}=sprintf('c1:qmov:m%d:motor1:stepdiag',mm);
    msdn{2,1}=sprintf('c1:qmov:m%d:motor2:stepdiag',mm);
    msdn{3,1}=sprintf('c1:qmov:m%d:motor3:stepdiag',mm);
    potvn{1,1}=pvn{15};potvn{2,1}=pvn{16};potvn{3,1}=pvn{17};
    name=lcaGet(sprintf('c1:qmov:m%d:name',mm));
    %get all current values
    foo=lcaGet(pvn);
    foo=foo(:,1);
    % pxvals=foo(1);pyvals=foo(2);ptvals=foo(3);
    % lxvals=foo(4);lyvals=foo(5);ltvals=foo(6);
    % sxvals=foo(7);syvals=foo(8);stvals=foo(9);
    % fxvals=foo(10);fyvals=foo(11);ftvals=foo(12);
    % dotrim=foo(13);itry=foo(14);
    % p1vals=foo(15);p2vals=foo(16);p3vals=foo(17);
    % lr1vals=foo(18);lr2vals=foo(19);lr3vals=foo(20);
    % lp1vals=foo(21);lp2vals=foo(22);lp3vals=foo(23);
    % l1vals=foo(24);l2vals=foo(25);l3vals=foo(26);
    origfoo=foo;
    fprintf(fid,'original set vals: %5.3f %5.3f %5.3f\n',origfoo(7:9));
    lcaPut(set0n,1);
    disp('trim to 0')
    lcaPut(dotrimn,1);
    tic
    while lcaGet(pvn{13})
        fprintf(fid,'.');
        pause(1)
    end
    to0tim=toc;
    fprintf(fid,' %d',round(toc));
    fprintf(fid,'\n');
    to0itry=lcaGet(pvn{14});
    potv=lcaGet(potvn);
    potv=round(potv*1000);
    id=find(potv~=5000);
    while ~isempty(id)
        ii=0;
        while ii<5 %perturb 5 tries
            lcaPut(dopertn,1);
            ns=lcaGet(mnsn);
            while sum(ns)
                pause(1)
                ns=lcaGet(mnsn);
            end
            pause(6)
            %check for pots=5
            potv=lcaGet(potvn);
            potv=round(potv*1000);
            id=find(potv~=5000);
            if ~isempty(id)
                ii=ii+1;
            else
                ii=10;
            end
            disp(ii)
        end
        zz=0;
        while ii<10&&zz<3
            %perturb didn't do it
            potv=lcaGet(potvn);
            potv=round(potv*1000);
            id=find(potv~=5000);
            for jj=1:length(id)
                mn=id(jj);
                %how many steps?
                offv=potv(mn)-5000;
                sd=-1*round(offv*1.7);
                lcaPut(msdn{mn},sd);
            end
            pause(5)
            potv=lcaGet(potvn);
            potv=round(potv*1000);
            id=find(potv~=5000);
            if ~isempty(id)
                offv=potv-5000;
                if find(abs(offv)>1)
                    %no change to ii
                else
                    ii=11;id=[];
                end
            end
            zz=zz+1;
        end
        if zz>2 || ii<5
            disp(sprintf('didn''t make it to 5v: %d %d %d',...
                potv(1),potv(2),potv(3)))
            id=[];
        end
    end
    badpots=lcaGet(potvn);
    disp(sprintf('allpots: %g %d %g %d %g %d ii %d zz %d',...
        badpots(1),round(badpots(1)*1000),...
        badpots(2),round(badpots(2)*1000),...
        badpots(3),round(badpots(3)*1000),ii,zz))
    lcaPut(setpedsn,1);
    lcaPut(setsumn,1);
    %get all values again
    foo=lcaGet(pvn);
    atzero=foo(:,1);
    %put to interesting values to check how lvdts do
    % pvn{7}=sprintf('c1:qmov:m%d:x:set',mm);
    % pvn{8}=sprintf('c1:qmov:m%d:y:set',mm);
    % pvn{9}=sprintf('c1:qmov:m%d:tilt:set',mm);
    lcaPut(pvn{7},1000);
    lcaPut(pvn{8},-1000);
    lcaPut(pvn{9},1000);
    disp('trim to 1000/-1000/1000')
    lcaPut(dotrimn,1);
    tic
    while lcaGet(pvn{13})
        fprintf(fid,'.');
        pause(1)
    end
    tobigtim=toc;
    fprintf(fid,' %d',round(toc));
    fprintf(fid,'\n');
    tobigitry=lcaGet(pvn{14});
    foo=lcaGet(pvn);
    atbig=foo(:,1);
    %go back to orig set vals
    for yy=7:9
        lcaPut(pvn{yy},origfoo(yy));
    end
    disp('trim to set')
    lcaPut(dotrimn,1);
    tic
    while lcaGet(pvn{13})
        fprintf(fid,'.');
        pause(1)
    end
    tosettim=round(toc);
    fprintf(fid,' %d',round(toc));
    fprintf(fid,'\n');
    tosetitry=lcaGet(pvn{14});
    foo=lcaGet(pvn);
    atset=foo(:,1);
    stoptim=now;
    
    %display summary
    fprintf(fid,'\n---------\n---------\n');
    fprintf(fid,'%s Mover %d %s\n',datestr(starttim),mm,name{1});
    fprintf(fid,'Total time: %5.1f minutes\n',24*60*(stoptim-starttim));
    fprintf(fid,'Tries to get to zero: %d\n',to0itry);
    fprintf(fid, 'At zero: allpots: %5.3f %5.3f %5.3f ii %d zz %d\n',...
        badpots,ii,zz);
    fprintf(fid,'At zero: x %5.3f %5.3f y %5.3f %5.3f t %5.3f %5.3f\n',...
        atzero([1 4 2 5 3 6]));
    fprintf(fid,...
        'LVDT peds: \n\tbefore:\t%5.3f %5.3f %5.3f \n\tafter:\t%5.3f %5.3f %5.3f \n',...
        origfoo([21:23]),atset([21:23]));
    fprintf(fid,'At 1000/-1000/1000: x %5.3f %5.3f y %5.3f %5.3f t %5.3f %5.3f itry %d\n',...
        atbig([1 4 2 5 3 6]),tobigitry);
    fprintf(fid,'%% diff pot/lvdt %5.3f %5.3f %5.3f\n',...
        100*(atbig(1)-atbig(4))/1000,100*(atbig(2)-atbig(5))/1000,...
        100*(atbig(3)-atbig(6))/1000);
    
    fprintf(fid,'At set %d/%d/%d: x %5.3f %5.3f y %5.3f %5.3f t %5.3f %5.3f itry %d\n',...
        round(origfoo(7:9)),atset([1 4 2 5 3 6]),tosetitry);
    for nn=7:8
        if origfoo(nn)>=2 %tols for position
            divisor(nn-6)=origfoo(nn);
        else
            divisor(nn-6)=2;
        end
    end
    if origfoo(9)>=7 %tols for tilt
        divisor(3)=origfoo(9);
    else
        divisor(3)=7;
    end
    fprintf(fid,'%% diff pot/lvdt %5.3f %5.3f %5.3f\n',...
        100*(atset(1)-atset(4))/divisor(1),100*(atset(2)-atset(5))/divisor(2),...
        100*(atset(3)-atset(6))/divisor(3));
    
    
    % pxvals=foo(1);pyvals=foo(2);ptvals=foo(3);
    % lxvals=foo(4);lyvals=foo(5);ltvals=foo(6);
    % sxvals=foo(7);syvals=foo(8);stvals=foo(9);
    % fxvals=foo(10);fyvals=foo(11);ftvals=foo(12);
    % dotrim=foo(13);itry=foo(14);
    % p1vals=foo(15);p2vals=foo(16);p3vals=foo(17);
    % lr1vals=foo(18);lr2vals=foo(19);lr3vals=foo(20);
    % lp1vals=foo(21);lp2vals=foo(22);lp3vals=foo(23);
    % l1vals=foo(24);l2vals=foo(25);l3vals=foo(26);
    
end
fclose(fid)








#!/usr/bin/perl
#
# kasemir@lanl.gov, based on EPICS RDB tools,
# but circumventing the RDB and directly going
# from DBD + Spreadsheet to EPICS DB file.
#
# This program handles menu and record DBD files
# from EPICS base R3.13.5.

use English;
use vars qw($opt_h $opt_v $opt_s $opt_e $opt_D $opt_x $opt_p $opt_P);
use Getopt::Std;

my ($name_field)  = "NAME";
my ($type_field)  = "TYPE";

sub usage()
{
    print("USAGE: $PROGRAM_NAME dbd-files spreadsheets db-file\n");
    print("    -h: help\n");
    print("    -v: verbose\n");
    print("\n");
    print("\n");
    print("    -s <n>         : Start line\n");
    print("    -e <n>         : End line\n");
    print("    -D field=value : define fields\n");
    print("    -x ....        : Transformations\n");
    print("    -p ....        : Prefixes\n");
    print("    -P ....        : Postfixes\n");
    print("\n");
    print("    dbd_files:\n");
    print("    Can be single DBD file or several,\n");
    print("    including pattern, when enclosed in single quotes.\n");
    print("\n");
    print("    spreadsheets:\n");
    print("    Can be single TAB-delimited spreadsheet file or ...\n");
    print("\n");
    print("    db-file:\n");
    print("    Name of the single DB file to generate.\n");
    print("\n");
    print("  + Empty lines and lines starting with '#' are ignored.\n");
    print("  + First valid line must contain column headers.\n");
    print("  + Columns NAME and TYPE define the record name and type,\n");
    print("    (ai, calc, ...), columns matching EPICS record field names\n");
    print("    are imported (DESC, INP, DTYP, ...).\n");
    print("  + Headers can be transformed.\n");
    print("    Syntax: -x OrigColumn=Column\n");
    print("    Example: -x 'Record=NAME,Input=INP',\n");
    print("             Column 'Record' will determine the record name\n");
    print("             and the column 'Input' will set the INP field.\n");
    print("    Prefix example: -p 'NAME=ccl2_vac:'\n");
    print("    Combined with the above, a column 'Record'\n");
    print("    with a value 'fred' would result in a record\n");
    print("    of NAME 'ccl2_vac:fred'.\n");

    exit(0);
}

usage() if (!getopts('hvs:e:D:x:p:P:')  or  $opt_h  or $#ARGV!=2);
my($dbd_files) = $ARGV[0];
my($sheet_files) = $ARGV[1];
my($db_file) = $ARGV[2];

# menu{name} = array of choices
my(%menues);

# %record_types{$name} = %fields;
my(%record_types);
my(%is_a_field);

# idle, menu, record, field
my($state) = "idle";
my($name);
my($field);

open DB, ">$db_file" or die "Cannot create '$db_file'\n";
print DB "# -*- shell-script -*- Emacs mode\n";
print DB "# Created by sheet2db.pl on ", scalar localtime, "\n";
print DB "#\n";
print DB "# Based on DBD Info from:\n";

my($dbd_file);
foreach $dbd_file ( glob($dbd_files) )
{
    print DB "# $dbd_file\n";
    read_dbd_file($dbd_file);
}
print DB "#\n";

if ($opt_v)
{
    foreach $name ( sort keys %menues )
    {
	print "Menu: $name\n";
    }
    my($property);
    foreach $name ( sort keys %record_types )
    {
	print "Record Type '$name'\n";
	foreach $field ( sort keys %{$record_types{$name}} )
	{
	    print "'$field': ";
	    foreach $property ( sort keys %{$record_types{$name}{$field}} )
	    {
		print "$property: $record_types{$name}{$field}{$property}  ";
	    }
	    print "\n";
	}
    }
}

my ($global_type, %defines, %translation, %prefix, %postfix, $rule);
# -D field=value, field=value, ....
foreach $rule (split /,/, $opt_D)
{   # single "field=value"
    if ($rule =~ m[(.+)=(.+)])
    {
	if ($1 eq $type_field)
	{   # TYPE=xxx ?
	    $global_type = $2;
	}
	else
	{
	    $defines{$1} = $2;
	}
    }
    else
    {
	die "Invalid definition '$rule'\n";
    }
}

# -x 'Record=NAME,Input=INP'
foreach $rule (split /,/, $opt_x)
{   # single "old=new"
    if ($rule =~ m[(.+)=(.+)])
    {
	$translation{$1} = $2;
    }
    else
    {
	die "Invalid translation '$rule'\n";
    }
}

# -p 'field=prefix, field=prefix, ...'
foreach $rule (split /,/, $opt_p)
{
    if ($rule =~ m[(.+)=(.+)])
    {
	$prefix{$1} = $2;
    }
    else
    {
	die "Invalid prefix '$rule'\n";
    }
}
# -P 'field=postfix...'
foreach $rule (split /,/, $opt_P)
{
    if ($rule =~ m[(.+)=(.+)])
    {
	$postfix{$1} = $2;
    }
    else
    {
	die "Invalid postfix '$rule'\n";
    }
}

if ($opt_v)
{
    print "Rules:\n";
    foreach $rule (keys %translation)
    {
	print "$rule -> $translation{$rule}\n";
    }
    foreach $rule (keys %prefix)
    {
	print "Prefix for $rule : '$prefix{$rule}'\n";
    }
    foreach $rule (keys %postfix)
    {
	print "Postfix for $rule : '$postfix{$rule}'\n";
    }
}

my(%records);
foreach $sheet ( glob($sheet_files) )
{
    read_sheet($sheet);
}

my ($record_id, $field_id);
foreach $record_id ( sort keys %records )
{
    print DB "#	$records{$record_id}{source}\n";
    print DB "record ($records{$record_id}{type}, \"$record_id\") {\n";

    foreach $field_id ( sort keys %{ $records{$record_id}{fields} } )
    {
	$val = $records{$record_id}{fields}{$field_id};
	if ($field_id eq "DESC" and length($val) >= 28)
	{
	    printf ("$record_id: Truncating DESC field to <28 characters.\n");
	    $val = substr($val, 0, 27);
	}
	printf DB "\tfield (%-4s, \"%s\")\n",
	$field_id, $val;
    }
    print DB "}\n";
}

exit(0);

# -------------------------------------------------------------------------

# read_dbd_file(filename)
sub read_dbd_file($)   
{
    my($filename) = @ARG;
    my($fh) = "IN$include_level";
    my($line) = "";
    my($include_level) = 0;
    my($field_id);

    ++$include_level;
    open($fh, "<$filename") or die "Cannot open $filename";
    while (<$fh>)
    { 
	s[\n\Z][];
	s[\r\Z][];            # remove line ending (DOS and UNIX case)
	next if (m'\A#');     # skip comments
	next if (m'\A\s*\Z'); # skip blank lines
        # process include files
	if (m'include\s*\"(.+)\"')
	{
	    read_dbd_file($1);
	}
	else
	{
	    handle($_);
	}
    }
    close($fh);
    --$include_level;
}

# handle(line)
sub handle($)
{
    my ($line) = @ARG;

    # print "$line\n";
    
    if ($state eq "idle"  and  $line =~ m'menu\((.+)\)')
    {
	$state = "menu";
	$name = $1;
	@choices = ();
    }
    elsif ($line =~ m'registrar\s*\(.+\)')
    {
        #registrar(xyz)  - just check file syntax to see if we're still on it
	die "Found registrar while in $state state" unless ($state eq "idle");
    }
    elsif ($line =~ m'function\s*\(.+\)')
    {
        #function(xyz)  - just check file syntax to see if we're still on it
	die "Found function while in $state state" unless ($state eq "idle");
    }
    elsif ($line =~ m'variable\s*\(.+\)')
    {
        #variable(xyz,type)  - just check file syntax ... 
	die "Found variable while in $state state" unless ($state eq "idle");
    }
    elsif ($line =~ m'driver\s*\(.+\)')
    {
        #driver(xyz)  - just check file syntax to see if we're still on it
	die "Found driver while in $state state" unless ($state eq "idle");
    }
    elsif ($line =~ m'device\s*\((\w+)\s*,\s*\w+\s*,\s*\w+\s*,\s*"(.+)"\s*\)')
    {
        #device(ai,CONSTANT,devAiSoft,"Soft Channel")
	die "Found device while in $state state" unless ($state eq "idle");
	$devices{$1}{$2} = 1;
    }
    elsif ($line =~ m'choice\s*\(.+,\s*\"(.+)\"\s*\)')
    {
	die "Found choice while in $state state" unless ($state eq "menu");
	push @choices, $1;
    }
    elsif ($line =~ m'breaktable\s*\((.+)\)')
    {
	die "Found breaktable while in $state state" unless ($state eq "idle");
	print("breaktable $1 (ignored)\n");
	$state = "breaktable";
    }
    elsif ($line =~ m'recordtype\s*\((.+)\s*\)') # recordtype(<name>)
    {
	die "Found record def. while in $state state" unless ($state eq "idle");
	$state = "record";
	$name = $1;
    }
    elsif ($line =~ m'field\s*\((.+)\s*,\s*(\w+)\s*\)') # field(<id>,<type>)
    {
	die "Found field while in $state state" unless ($state eq "record");
	$field_id = $1;
	$record_types{$name}{$field_id}{type} = $2;
	$is_a_field{$field_id} = 1;
	$state = "field";
    }
    elsif ($line =~ m'([a-z]+)\s*\("(.+)"\s*\)') # property("value")
    {
	die "Found property '$line' while in $state state"
	    unless ($state eq "field");
	$record_types{$name}{$field_id}{$1} = $2;
    }
    elsif ($line =~ m'([a-z]+)\s*\((.+)\s*\)') # property(value)
    {
	die "Found property '$line' while in $state state"
	    unless ($state eq "field");
	$record_types{$name}{$field_id}{$1} = $2;
    }
    elsif ($line =~ m'\A\s*}\s*\Z') # closing curly brace
    {
	if ($state eq "menu")
	{
	    @menues{$name} = @choices;
	    $state = "idle";
	}
	elsif ($state eq "field")
	{
	    $state = "record";
	}
	elsif ($state eq "record")
	{
	    $state = "idle";
	}
	elsif ($state eq "breaktable")
	{
	    $state = "idle";
	}
	else
	{
	    die "Found end-of item ('}') in $state state";
	}
    }
    else
    {
	die "Cannot handle line '$line'\n" unless ($state eq "breaktable");
    }
}
	

# ----------------------------------------------------------------------

# Read spreadsheet file into RDB
sub read_sheet($)
{
    my ($filename) = @ARG;
    my ($line, $i, @cols, @headers, @hcopy, $field, $col, %fields);
    my ($record_id, $record_type);
    
    @headers = ();
    open FILE, $filename or die "Cannot open $filename";
    while ($opt_s > 1)
    {
	$line = <FILE>;
	--$opt_s;
    }
    while (<FILE>)
    {
	last if ($opt_e and $NR > $opt_e);
	# set $line from input, removing CR/LF
	if (m[(.*)\r\n])
	{
	    $line = $1;
	}
	elsif (m[(.*)\n])
	{
	    $line = $1;
	}
	else
	{
	    $line = $_;
	}
	# print("$NR:$line:\n") if ($opt_v);
        next if ($line =~ m[^\"?\#.*]); # Skip comments
        @cols = split(/\t/, $line);     # Get tab-delimited columns
	next if ($#cols < 0);           # Skip empty lines
	
        if (! @headers)
        {   # First line defines column names
            print "Found headers in line $NR:\n" if ($opt_v);

	    foreach $col ( @cols )
	    {
		# remove quotation marks
		if ($col =~ m'"(.+)"')
		{
		    $col = $1;
		}
		if ($translation{$col})
		{
		    print "Header '$col' -> '$translation{$col}'\n" if ($opt_v);
		    push @headers, $translation{$col};
		}
		else
		{
		    print "Header '$col'\n" if ($opt_v);
		    push @headers, $col;
		}
	    }
            next;
        }
        die "Line $NR: More columns ($#cols) than headers ($#headers)"
            if ($#cols > $#headers);
	
        @hcopy = @headers;  # copy headers because we shift cols. off it
        %fields = %defines;
        $record_id = "";
	$record_type = "";
	$record_type = $global_type if ($global_type);

        while (@cols)
        {
            ($field,$col) = (shift @hcopy, shift @cols);
	    # remove quotation marks
	    if ($col =~ m'"(.+)"')
	    {
		$col = $1;
	    }

	    next unless (length($col) > 0); # skip empty columns

	    if ($prefix{$field})
	    {
		$col = "$prefix{$field}$col";
	    }
	    if ($postfix{$field})
	    {
		$col = "$col$postfix{$field}";
	    }
	    
	    # Trim leading and trailing spaces
	    $col =~ s[^ *][];
	    $col =~ s[ *$][];

            # Keep if field is an EPICS record field (including NAME,TYPE,...):
            if ($field eq $name_field)
            {   $record_id = $col; }
            elsif ($field eq $type_field)
            {   $record_type = $col; }
            elsif ($is_a_field{$field})
            {   $fields{$field} = $col; }
	    # Else: We ignore this column because it does not
	    # match any EPICS field nor NAME nor TYPE.
        }
        unless ($record_id)
        {
            print("Line $NR: No $name_field, skipped\n") if ($opt_v);
	    next;
        }
        unless ($record_type)
        {
            print("Line $NR: No $type_field, skipped\n");
            return;
        }

	my ($field_id);
	if ($opt_v)
	{
	    print("\n$NR: $record_type '$record_id',\n");
	    foreach $field_id ( sort keys %fields )
	    {
		print("\t$field_id = $fields{$field_id}\n");
	    }
	}

	if (length($records{$record_id}{source}) > 0)
	{
	    print "File $filename, line $NR:\n";
	    print "Record '$record_id' already defined in\n";
	    print "$records{$record_id}{source}.\n";
	    die "Cannot handle multiple definitions\n";
	}
	$records{$record_id}{source} = "$filename, $NR";
	$records{$record_id}{type} = $record_type;
	$records{$record_id}{fields} = { %fields };
    }
}


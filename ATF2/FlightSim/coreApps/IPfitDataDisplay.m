function IPfitDataDisplay(varargin)
% IPfitDataDisplay()
%  Update IP data display, must already be visible and store in
%  FL.Gui.IPfitData
% IPfitDisplay('setmarker',dir)
%  Set marker point at current value
%   dir = 'x' or 'y'
global FL BEAMLINE INSTR
persistent ipinstr ipind marker ipData ipJitData blele lastglitch

if isempty(lastglitch); lastglitch=true; end;

% Only run if display open and visible
if ~isfield(FL,'Gui') || ~isfield(FL.Gui,'IPfitData') || ~ishandle(FL.Gui.IPfitData.figure1)
 return
end

% Indicies & initialisation
if isempty(ipinstr)
  ipind=findcells(BEAMLINE,'Name','IP');
  ipinstr=findcells(INSTR,'Index',ipind);
  marker=NaN(1,4);
end

% commands
setmarker=0;
if nargin>1 && isequal(varargin{1},'setmarker')
  if isequal(varargin{2},'x')
    setmarker=1;
  else
    setmarker=2;
  end
end
if nargin>1 && isequal(varargin{1},'cleardata')
  ipData=[];
  return
end

% Get data from GUI
numave=str2double(get(FL.Gui.IPfitData.edit2,'String'));
if isnan(numave); return; end;
numpulse=str2double(get(FL.Gui.IPfitData.edit1,'String'));
if isnan(numpulse); return; end;

% Get IP data
ipd=getIPData('getlast');
if ~lastglitch && (abs(ipd.x-ipData(end).x)>300e-6 || abs(ipd.y-ipData(end).y)>300e-6)
  lastglitch=true;
  return
else
  lastglitch=false;
end
ipData(end+1).x=ipd.x; ipData(end).xp=ipd.xp; ipData(end).y=ipd.y; ipData(end).yp=ipd.yp;
if length(ipData)>numpulse
  ipData=ipData(2:numpulse+1);
end

ind=min([length(ipData) numave]);
mx=mean([ipData(1+length(ipData)-ind:end).x]);
rx=std([ipData(1+length(ipData)-ind:end).x]);
mxp=mean([ipData(1+length(ipData)-ind:end).xp]);
rxp=std([ipData(1+length(ipData)-ind:end).xp]);
my=mean([ipData(1+length(ipData)-ind:end).y]);
ry=std([ipData(1+length(ipData)-ind:end).y]);
myp=mean([ipData(1+length(ipData)-ind:end).yp]);
ryp=std([ipData(1+length(ipData)-ind:end).yp]);
if setmarker==1
  marker(1)=mx*1e6; marker(2)=my*1e6;
elseif setmarker==2
  marker(3)=mxp*1e6; marker(4)=myp*1e6;
end
if ~isnan(marker(1))
  plot(FL.Gui.IPfitData.axes1,marker(1),marker(2),'b*','MarkerSize',10)
  set(FL.Gui.IPfitData.text13,'String',sprintf('%.2f / %.2f',marker(1),marker(2)))
  hold(FL.Gui.IPfitData.axes1,'on');
end
plot(FL.Gui.IPfitData.axes1,mx*1e6,my*1e6,'ro','MarkerSize',10);
if isnan(marker(1)); hold(FL.Gui.IPfitData.axes1,'on'); end;
plot(FL.Gui.IPfitData.axes1,[mx mx].*1e6,[my-ry my+ry].*1e6,'r')
plot(FL.Gui.IPfitData.axes1,[mx-rx mx+rx].*1e6,[my my].*1e6,'r')
hold(FL.Gui.IPfitData.axes1,'off');
if ~isnan(marker(1))
  V(1)=min([ipData.x marker(1)*1e-6]); V(2)=max([ipData.x marker(1)*1e-6]);
else
  V(1)=min([ipData.x]); V(2)=max([ipData.x]);
end
if ~isnan(marker(2))
  V(3)=min([ipData.y marker(2)*1e-6]); V(4)=max([ipData.y marker(2)*1e-6]);
else
  V(3)=min([ipData.y]); V(4)=max([ipData.y]);
end
V(1)=V(1)-10e-6; V(2)=V(2)+10e-6; V(3)=V(3)-10e-6; V(4)=V(4)+10e-6;
axis(FL.Gui.IPfitData.axes1,V*1e6);
grid(FL.Gui.IPfitData.axes1,'on')
xlabel(FL.Gui.IPfitData.axes1,'x^* / um')
ylabel(FL.Gui.IPfitData.axes1,'y^* / um')
if ~isnan(marker(3))
  plot(FL.Gui.IPfitData.axes3,marker(3),marker(4),'b*','MarkerSize',10)
  set(FL.Gui.IPfitData.text14,'String',sprintf('%.2f / %.2f',marker(3),marker(4)))
  hold(FL.Gui.IPfitData.axes3,'on');
end
plot(FL.Gui.IPfitData.axes3,mxp*1e6,myp*1e6,'ro','MarkerSize',10);
if isnan(marker(3)); hold(FL.Gui.IPfitData.axes3,'on'); end;
plot(FL.Gui.IPfitData.axes3,[mxp mxp].*1e6,[myp-ryp myp+ryp].*1e6,'r')
plot(FL.Gui.IPfitData.axes3,[mxp-rxp mxp+rxp].*1e6,[myp myp].*1e6,'r')
hold(FL.Gui.IPfitData.axes3,'off');
if ~isnan(marker(3))
  V(1)=min([ipData.xp marker(3)*1e-6]); V(2)=max([ipData.xp marker(3)*1e-6]);
else
  V(1)=min([ipData.xp]); V(2)=max([ipData.xp]);
end
if ~isnan(marker(4))
  V(3)=min([ipData.yp marker(4)*1e-6]); V(4)=max([ipData.yp marker(4)*1e-6]);
else
  V(3)=min([ipData.yp]); V(4)=max([ipData.yp]);
end
V(1)=V(1)-10e-6; V(2)=V(2)+10e-6; V(3)=V(3)-10e-6; V(4)=V(4)+10e-6;
axis(FL.Gui.IPfitData.axes3,V*1e6);
grid(FL.Gui.IPfitData.axes3,'on')
xlabel(FL.Gui.IPfitData.axes3,'x''^* / urad')
ylabel(FL.Gui.IPfitData.axes3,'y''^* / urad')
AX=plotyy(FL.Gui.IPfitData.axes2,1:length(ipData),[ipData.x]*1e6,1:length(ipData),[ipData.y]*1e6);
  
xlabel(FL.Gui.IPfitData.axes2,'Pulse #')
set(get(AX(1),'Ylabel'),'String','x^* / um')
set(get(AX(2),'Ylabel'),'String','y^* / um')
grid(FL.Gui.IPfitData.axes2,'on')
set(FL.Gui.IPfitData.text1,'String',num2str(mx*1e6));
set(FL.Gui.IPfitData.text2,'String',num2str(mxp*1e6));
set(FL.Gui.IPfitData.text3,'String',num2str(my*1e6));
set(FL.Gui.IPfitData.text4,'String',num2str(myp*1e6));
set(FL.Gui.IPfitData.text5,'String',num2str(rx*1e6));
set(FL.Gui.IPfitData.text6,'String',num2str(rxp*1e6));
set(FL.Gui.IPfitData.text7,'String',num2str(ry*1e6));
set(FL.Gui.IPfitData.text8,'String',num2str(ryp*1e6));
% ipJitdata window
if isfield(FL.Gui,'IPfitData_resjitanal') && ishandle(FL.Gui.IPfitData_resjitanal.figure1)
  buflen=str2double(get(FL.Gui.IPfitData_resjitanal.edit1,'String'));
  bufSize = getIPData('getbufsize');
  if bufSize<buflen; return; end;
  if isempty(ipJitData)
    ipJitData.ipx=[];ipJitData.ipy=[];ipJitData.ipax=[];ipJitData.ipay=[];
    ipJitData.ipbx=[];ipJitData.ipby=[];
  end
  if isempty(blele)
    blele.ipa=findcells(INSTR,'Index',findcells(BEAMLINE,'Name','IPBPMA'));
    blele.ipb=findcells(INSTR,'Index',findcells(BEAMLINE,'Name','IPBPMB'));
    blele.ip=findcells(INSTR,'Index',findcells(BEAMLINE,'Name','IP'));
    blele.preip=findcells(INSTR,'Index',findcells(BEAMLINE,'Name','MPREIP'));
    blele.pip=findcells(INSTR,'Index',findcells(BEAMLINE,'Name','MPIP'));
    blele.qd0=findcells(INSTR,'Index',findcells(BEAMLINE,'Name','MQD0FF'));
  end
  [~, bpmdata]=FlHwUpdate('bpmave',buflen,0,[],[blele.ip blele.ipa blele.ipb]);
  ipJitData.ipx=[ipJitData.ipx bpmdata{1}(4,1)*1e9];
  ipJitData.ipy=[ipJitData.ipy bpmdata{1}(5,1)*1e9];
  ipJitData.ipax=[ipJitData.ipax bpmdata{1}(4,2)*1e9];
  ipJitData.ipay=[ipJitData.ipay bpmdata{1}(5,2)*1e9];
  ipJitData.ipbx=[ipJitData.ipbx bpmdata{1}(4,3)*1e9];
  ipJitData.ipby=[ipJitData.ipby bpmdata{1}(5,3)*1e9];
  if length(ipJitData.ipx)>buflen
    ipJitData.ipx=ipJitData.ipx(end-buflen+1:end);
    ipJitData.ipy=ipJitData.ipy(end-buflen+1:end);
    ipJitData.ipax=ipJitData.ipax(end-buflen+1:end);
    ipJitData.ipay=ipJitData.ipay(end-buflen+1:end);
    ipJitData.ipbx=ipJitData.ipbx(end-buflen+1:end);
    ipJitData.ipby=ipJitData.ipby(end-buflen+1:end);
  end
  % Subtract calculated resolutions?
  if get(FL.Gui.IPfitData_resjitanal.checkbox1,'Value')
    resx=str2double(get(FL.Gui.IPfitData_resjitanal.text14,'String'));
    resy=str2double(get(FL.Gui.IPfitData_resjitanal.text15,'String'));
    if any([isnan(resx) isnan(resy)])
      ipJitData.ipx=sqrt(ipJitData.ipx.^2-INSTR{blele.ip}.Res(1)^2);
      ipJitData.ipax=sqrt(ipJitData.ipax.^2-INSTR{blele.ipa}.Res(1)^2);
      ipJitData.ipbx=sqrt(ipJitData.ipbx.^2-INSTR{blele.ipb}.Res(1)^2);
      if length(INSTR{blele.ip}.Res)>1; ii=2; else ii=1; end;
      ipJitData.ipy=sqrt(ipJitData.ipy.^2-INSTR{blele.ip}.Res(ii)^2);
      ipJitData.ipay=sqrt(ipJitData.ipay.^2-INSTR{blele.ipa}.Res(ii)^2);
      ipJitData.ipby=sqrt(ipJitData.ipby.^2-INSTR{blele.ipb}.Res(ii)^2);
    end
  end
  set(FL.Gui.IPfitData_resjitanal.text2,'String',sprintf('%.0f +/- %.0f',mean(ipJitData.ipax),std(ipJitData.ipax)))
  set(FL.Gui.IPfitData_resjitanal.text3,'String',sprintf('%.0f +/- %.0f',mean(ipJitData.ipay),std(ipJitData.ipay)))
  set(FL.Gui.IPfitData_resjitanal.text4,'String',sprintf('%.0f +/- %.0f',mean(ipJitData.ipbx),std(ipJitData.ipbx)))
  set(FL.Gui.IPfitData_resjitanal.text5,'String',sprintf('%.0f +/- %.0f',mean(ipJitData.ipby),std(ipJitData.ipby)))
  set(FL.Gui.IPfitData_resjitanal.text7,'String',sprintf('%.0f +/- %.0f',mean(ipJitData.ipx),std(ipJitData.ipx)))
  set(FL.Gui.IPfitData_resjitanal.text6,'String',sprintf('%.0f +/- %.0f',mean(ipJitData.ipy),std(ipJitData.ipy)))
  % Plot orbit
  bpmuse=getIPData('getbpmuse');
  data = getIPData('getdatastore');
  xbpm=zeros(buflen,sum(bpmuse)); ybpm=xbpm; sbpm=zeros(1,sum(bpmuse));
  nbpm=0;
  svals=[BEAMLINE{INSTR{blele.qd0}.Index}.S BEAMLINE{INSTR{blele.preip}.Index}.S BEAMLINE{INSTR{blele.ipa}.Index}.S ...
    BEAMLINE{INSTR{blele.ipb}.Index}.S BEAMLINE{INSTR{blele.pip}.Index}.S];
  pcols='cmrbk';
  for ibpm=find(bpmuse)
    nbpm=nbpm+1;
    xbpm(:,nbpm)=data(end-buflen+1:end,ibpm*3-2);
    ybpm(:,nbpm)=data(end-buflen+1:end,ibpm*3-1);
    sbpm(nbpm)=svals(ibpm);
    plot(FL.Gui.IPfitData_resjitanal.axes1,repmat(sbpm(nbpm),buflen,1),xbpm(:,nbpm),[pcols(ibpm) '*']);
    plot(FL.Gui.IPfitData_resjitanal.axes2,repmat(sbpm(nbpm),buflen,1),ybpm(:,nbpm),[pcols(ibpm) '*']);
    if nbpm==1
      hold(FL.Gui.IPfitData_resjitanal.axes1,'on')
      hold(FL.Gui.IPfitData_resjitanal.axes2,'on')
    end
  end
  set(FL.Gui.IPfitData_resjitanal.axes1,'XTick',[])
  set(FL.Gui.IPfitData_resjitanal.axes1,'YTick',[])
  set(FL.Gui.IPfitData_resjitanal.axes2,'XTick',[])
  set(FL.Gui.IPfitData_resjitanal.axes2,'YTick',[])
  splot=sbpm;
  if splot(end)<BEAMLINE{INSTR{blele.ip}.Index}.S
    splot=[splot BEAMLINE{INSTR{blele.pip}.Index}.S];
  end
  for ipulse=1:buflen
    q=noplot_polyfit(sbpm,xbpm(ipulse,:),1,1);
    plot(FL.Gui.IPfitData_resjitanal.axes1,splot,q(1)+q(2).*splot)
    q=noplot_polyfit(sbpm,ybpm(ipulse,:),1,1);
    plot(FL.Gui.IPfitData_resjitanal.axes2,splot,q(1)+q(2).*splot)
  end
  hold(FL.Gui.IPfitData_resjitanal.axes1,'off')
  hold(FL.Gui.IPfitData_resjitanal.axes2,'off')
  xlabel(FL.Gui.IPfitData_resjitanal.axes1,'Horizontal Orbit')
  xlabel(FL.Gui.IPfitData_resjitanal.axes2,'Vertical Orbit')
  dL = getIPData('getdl')+BEAMLINE{INSTR{blele.ip}.Index}.S;
  v1=axis(FL.Gui.IPfitData_resjitanal.axes1);
  v2=axis(FL.Gui.IPfitData_resjitanal.axes2);
  line([dL(end) dL(end)],[v1(3) v1(4)],'Parent',...
    FL.Gui.IPfitData_resjitanal.axes1,'LineStyle',':','Color','r');
  line([dL(end) dL(end)],[v2(3) v2(4)],'Parent',...
    FL.Gui.IPfitData_resjitanal.axes2,'LineStyle',':','Color','r');
  line([BEAMLINE{INSTR{blele.ip}.Index}.S BEAMLINE{INSTR{blele.ip}.Index}.S],[v1(3) v1(4)],'Parent',...
    FL.Gui.IPfitData_resjitanal.axes1,'LineStyle','-','Color','k');
  line([BEAMLINE{INSTR{blele.ip}.Index}.S BEAMLINE{INSTR{blele.ip}.Index}.S],[v2(3) v2(4)],'Parent',...
    FL.Gui.IPfitData_resjitanal.axes2,'LineStyle','-','Color','k');
end

drawnow('expose');

function varargout = IPBPM_waistShift(varargin)
% IPBPM_WAISTSHIFT MATLAB code for IPBPM_waistShift.fig
%      IPBPM_WAISTSHIFT, by itself, creates a new IPBPM_WAISTSHIFT or raises the existing
%      singleton*.
%
%      H = IPBPM_WAISTSHIFT returns the handle to a new IPBPM_WAISTSHIFT or the handle to
%      the existing singleton*.
%
%      IPBPM_WAISTSHIFT('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in IPBPM_WAISTSHIFT.M with the given input arguments.
%
%      IPBPM_WAISTSHIFT('Property','Value',...) creates a new IPBPM_WAISTSHIFT or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before IPBPM_waistShift_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to IPBPM_waistShift_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help IPBPM_waistShift

% Last Modified by GUIDE v2.5 01-Nov-2012 13:14:27

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @IPBPM_waistShift_OpeningFcn, ...
                   'gui_OutputFcn',  @IPBPM_waistShift_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before IPBPM_waistShift is made visible.
function IPBPM_waistShift_OpeningFcn(hObject, eventdata, handles, varargin) %#ok<*INUSL>
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to IPBPM_waistShift (see VARARGIN)
global BEAMLINE PS FL

% Choose default command line output for IPBPM_waistShift
handles.output = handles;

% Get element ID's and store starting magnet strengths
storedata.iqd0=findcells(BEAMLINE,'Name','QD0FF'); storedata.psqd0=BEAMLINE{storedata.iqd0(1)}.PS;
storedata.iqf1=findcells(BEAMLINE,'Name','QF1FF'); storedata.psqf1=BEAMLINE{storedata.iqf1(1)}.PS;
storedata.psInit=[PS(storedata.psqd0).Ampl PS(storedata.psqf1).Ampl];
storedata.nbpm=str2double(get(FL.Gui.IPfitData.edit2,'String'));
set(handles.figure1,'UserData',storedata)
bpmind=cellfun(@(x) findcells(BEAMLINE,'Name',x),get(handles.popupmenu1,'String'));
set(handles.popupmenu1,'UserData',bpmind)

% Initialize axes
rdbkUpdate([],[],handles);
setLims(handles);

% Get calculated waists
popupmenu1_Callback(handles.popupmenu1,[],handles);

% Update timer
than=timer('TimerFcn',@(x,y)IPBPM_waistShift('rdbkUpdate',x,y,handles),...
  'BusyMode','drop','Period',1,'ExecutionMode','fixedDelay');
start(than)

% Update handles structure
guidata(hObject, handles);

% --- Outputs from this function are returned to the command line.
function varargout = IPBPM_waistShift_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on button press in pushbutton5.
function pushbutton5_Callback(hObject, eventdata, handles) %#ok<*INUSD,*DEFNU>
% hObject    handle to pushbutton5 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global FL BEAMLINE
bpmind=get(handles.popupmenu1,'UserData');
ipind=bpmind(get(handles.popupmenu1,'Value'));
FL.noUpdate=true;
try
  xmin=lsqnonlin(@(x) waistMin(x,handles,ipind),[1 1],0.5,2,optimset('Display','off'));
  set(hObject,'UserData',xmin);
  dat=get(handles.figure1,'UserData');
  iqf1=interp1(FL.HwInfo.PS(dat.psqf1).conv(2,:),FL.HwInfo.PS(dat.psqf1).conv(1,:),abs(xmin(1)*BEAMLINE{dat.iqf1(1)}.B),'linear');
  iqd0=interp1(FL.HwInfo.PS(dat.psqd0).conv(2,:),FL.HwInfo.PS(dat.psqd0).conv(1,:),abs(xmin(2)*BEAMLINE{dat.iqd0(1)}.B),'linear');
  set(handles.text6,'String',num2str(iqf1))
  set(handles.text2,'String',num2str(iqd0))
  V=axis(handles.axes1); hold(handles.axes1,'on'); line([iqf1 iqf1],V(3:4),'Parent',handles.axes1,'Color','k'); hold(handles.axes1,'off');
  V=axis(handles.axes2); hold(handles.axes2,'on'); line([iqd0 iqd0],V(3:4),'Parent',handles.axes2,'Color','k'); hold(handles.axes2,'off');
catch
  set(handles.text6,'String','Err')
  set(handles.text2,'String','Err')
end
FL.noUpdate=false;
drawnow('expose')


% --- Executes on button press in pushbutton6.
function pushbutton6_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton6 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global PS

dat=get(handles.figure1,'UserData');

if dat.psInit(1)<PS(dat.psqd0).Ampl
  PS(dat.psqd0).SetPt=dat.psInit(1)-0.1;
  PSTrim(dat.psqd0,1);
  pause(2)
end
if dat.psInit(2)<PS(dat.psqf1).Ampl
  PS(dat.psqf1).SetPt=dat.psInit(2)-0.1;
  PSTrim(dat.psqf1,1);
  pause(2)
end
PS(dat.psqd0).SetPt=dat.psInit(1);
PS(dat.psqf1).SetPt=dat.psInit(2);
PSTrim([dat.psqd0 dat.psqf1],1);

% --- Executes on selection change in popupmenu1.
function popupmenu1_Callback(hObject, eventdata, handles)
% hObject    handle to popupmenu1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

cla(handles.axes1);cla(handles.axes2);setLims(handles);
pushbutton5_Callback(handles.pushbutton5,[],handles);
setLims(handles);

% Reset fit values
set(handles.text5,'String','---')
set(handles.text10,'String','---')
set(handles.text8,'String','---')
set(handles.text9,'String','---')

drawnow('expose')

% --- Executes during object creation, after setting all properties.
function popupmenu1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to popupmenu1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit3_Callback(hObject, eventdata, handles)
% hObject    handle to edit3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

if str2double(get(hObject,'String'))<3; set(hObject,'String','3'); end;


% --- Executes during object creation, after setting all properties.
function edit3_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit2_Callback(hObject, eventdata, handles)
% hObject    handle to edit2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

setLims(handles);


% --- Executes during object creation, after setting all properties.
function edit2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit1_Callback(hObject, eventdata, handles)
% hObject    handle to edit1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

setLims(handles);


% --- Executes during object creation, after setting all properties.
function edit1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in pushbutton2.
function pushbutton2_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
doscan(handles,'y');

% --- Executes on button press in pushbutton4.
function pushbutton4_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
setMagnet(handles,'ymodel');

% --- Executes on button press in pushbutton1.
function pushbutton1_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on button press in pushbutton3.
function pushbutton3_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes when user attempts to close figure1.
function figure1_CloseRequestFcn(hObject, eventdata, handles)
% hObject    handle to figure1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global FL
% Hint: delete(hObject) closes the figure
try
  FL.Gui=rmfield(FL.Gui,'IPBPM_waistShift');
catch
end
delete(hObject);

function alpha=waistMin(x,handles,ipind)
global PS FL BEAMLINE
persistent iex

if isempty(iex)
  iex=findcells(BEAMLINE,'Name','IEX');
end

dat=get(handles.figure1,'UserData');
ps0=[PS(dat.psqf1).Ampl PS(dat.psqd0).Ampl];

PS(dat.psqf1).Ampl=x(1);
PS(dat.psqd0).Ampl=x(2);
Tx=FL.SimModel.Initial.x.Twiss; Ty=FL.SimModel.Initial.y.Twiss;
[stat, T]=GetTwiss(iex,ipind,Tx,Ty);
if stat{1}==1
  alpha=abs([T.betax(end) T.betay(end)]);
else
  alpha=[100 100];
end
PS(dat.psqf1).Ampl=ps0(1);
PS(dat.psqd0).Ampl=ps0(2);

function qdiff=lMinFun(x,handles,ipind,xind,qfit)
global BEAMLINE
drifele=[ipind-1 ipind+1];
d0=[BEAMLINE{drifele(1)}.L BEAMLINE{drifele(2)}.L];
BEAMLINE{drifele(1)}.L=d0(1)+x/2;
BEAMLINE{drifele(2)}.L=d0(2)-x/2;
xmin=lsqnonlin(@(xx) waistMin(xx,handles,ipind),[1 1],0.5,2,optimset('Display','off'));
qdiff=abs(qfit-xmin(xind));
BEAMLINE{drifele(1)}.L=d0(1);
BEAMLINE{drifele(2)}.L=d0(2);

% --- Executes on button press in pushbutton7.
function pushbutton7_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton7 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
setMagnet(handles,'yscan');

% --- Executes on button press in pushbutton8.
function pushbutton8_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton8 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
setMagnet(handles,'xmodel');

% --- Executes on button press in pushbutton9.
function pushbutton9_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton9 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
setMagnet(handles,'xscan');

% --- Executes on button press in pushbutton10.
function pushbutton10_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton10 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
doscan(handles,'x');

function rdbkUpdate(obj,event,handles)
global PS FL BEAMLINE
persistent lastPS

dat=get(handles.figure1,'UserData');
if ~isequal(lastPS,[PS(dat.psqf1).Ampl PS(dat.psqd0).Ampl])
  iqf1=interp1(FL.HwInfo.PS(dat.psqf1).conv(2,:),FL.HwInfo.PS(dat.psqf1).conv(1,:),abs(PS(dat.psqf1).Ampl*BEAMLINE{dat.iqf1(1)}.B),'linear');
  iqd0=interp1(FL.HwInfo.PS(dat.psqd0).conv(2,:),FL.HwInfo.PS(dat.psqd0).conv(1,:),abs(PS(dat.psqd0).Ampl*BEAMLINE{dat.iqd0(1)}.B),'linear');
  set(handles.text7,'String',num2str(iqf1))
  set(handles.text3,'String',num2str(iqd0))
  lastPS=[PS(dat.psqf1).Ampl PS(dat.psqd0).Ampl];
end

drawnow('expose')

function setLims(handles)
global BEAMLINE FL PS

try
  iqf1=str2double(get(handles.text6,'String'));
  iqd0=str2double(get(handles.text2,'String'));
  lw=1-str2double(get(handles.edit1,'String'))*1e-2;
  hi=1+str2double(get(handles.edit2,'String'))*1e-2;
  V=axis(handles.axes1); axis(handles.axes1,[iqf1*lw iqf1*hi V(3:4)]);
  V=axis(handles.axes2); axis(handles.axes2,[iqd0*lw iqd0*hi V(3:4)]);
catch
  dat=get(handles.figure1,'UserData');
  iqf1=interp1(FL.HwInfo.PS(dat.psqf1).conv(2,:),FL.HwInfo.PS(dat.psqf1).conv(1,:),abs(PS(dat.psqf1).Ampl*BEAMLINE{dat.iqf1(1)}.B),'linear');
  iqd0=interp1(FL.HwInfo.PS(dat.psqd0).conv(2,:),FL.HwInfo.PS(dat.psqd0).conv(1,:),abs(PS(dat.psqd0).Ampl*BEAMLINE{dat.iqd0(1)}.B),'linear');
  lw=1-str2double(get(handles.edit1,'String'))*1e-2;
  hi=1+str2double(get(handles.edit2,'String'))*1e-2;
  V=axis(handles.axes1); axis(handles.axes1,[iqf1*lw iqf1*hi V(3:4)]);
  V=axis(handles.axes2); axis(handles.axes2,[iqd0*lw iqd0*hi V(3:4)]);
end
drawnow('expose')

function doscan(handles,dim)
global PS BEAMLINE FL

dat=get(handles.figure1,'UserData');
switch dim
  case 'x'
    ips=dat.psqf1;
    ibl=dat.iqf1(1);
    ax=handles.axes1;
    txtHan=handles.text8;
    pushbutton8_Callback(handles.pushbutton8,[],handles);
    xind=1;
    dltxt=handles.text9;
  case 'y'
    ips=dat.psqd0;
    ibl=dat.iqd0(1);
    ax=handles.axes2;
    txtHan=handles.text5;
    pushbutton4_Callback(handles.pushbutton8,[],handles);
    xind=2;
    dltxt=handles.text10;
end
FlHwUpdate;

% Get Scan ranges
V=[PS(ips).Ampl-PS(ips).Ampl*(str2double(get(handles.edit1,'String'))*1e-2) ...
  PS(ips).Ampl+PS(ips).Ampl*(str2double(get(handles.edit2,'String'))*1e-2)];
ran=linspace(V(1),V(2),str2double(get(handles.edit3,'String')));
iran=interp1(FL.HwInfo.PS(ips).conv(2,:),FL.HwInfo.PS(ips).conv(1,:),abs(ran.*BEAMLINE{ibl}.B),'linear');
iranFit=linspace(iran(1),iran(end),1000);
jit=zeros(size(ran));

% Scan and plot
ps0=PS(ips).Ampl;
PS(ips).SetPt=ps0-0.1; PSTrim(ips,1); pause(2)
dostop=false; q=[];
for n=1:length(ran)
  PS(ips).SetPt=ran(n); PSTrim(ips,1); pause(2)
  jit(n)=getJitter(handles,dim);
  plot(ax,iran(1:n),jit(1:n),'*')
  if n>2
    q=noplot_parab(iran(1:n),jit(1:n));
    hold(ax,'on')
    plot(ax,iranFit,q(1).*(iranFit-q(2)).^2+q(3),'r')
    V=axis(ax);
    if q(2)>=V(1) && q(2)<=V(2)
      line([q(2) q(2)],V(3:4),'Parent',ax,'Color','r');
    end
    hold(ax,'off')
  end
  drawnow('expose');
  if strcmp(get(handles.pushbutton11,'String'),'Stopping')
    dostop=true;
    break
  end
end
PS(ips).SetPt=ps0-0.1; PSTrim(ips,1); pause(2)
PS(ips).SetPt=ps0; PSTrim(ips,1);
if dostop
  set(handles.pushbutton11,'String','Stopped')
  pushbutton11_Callback(handles.pushbutton11,[],handles);
end
if ~isempty(q); set(txtHan,'String',num2str(q(2))); end;
pushbutton5_Callback(handles.pushbutton5,[],handles);

% Get S offset for BPM to best fit measured waist position
bpmind=get(handles.popupmenu1,'UserData');
ipind=bpmind(get(handles.popupmenu1,'Value'));
FL.noUpdate=true;
psval=interp1(FL.HwInfo.PS(ips).conv(1,:),FL.HwInfo.PS(ips).conv(2,:),q(2),'linear');
psval=psval/abs(BEAMLINE{ibl}.B);
try
  lmin=fminbnd(@(x) lMinFun(x,handles,ipind,xind,psval),-0.2,0.2,optimset('Display','off'));
  set(dltxt,'String',num2str(lmin*1e3))
catch
  set(dltxt,'String','Err')
end
FL.noUpdate=false;

function jit=getJitter(handles,dim)
global INSTR FL
bpmind=get(handles.popupmenu1,'UserData');
ipind=findcells(INSTR,'Index',bpmind(get(handles.popupmenu1,'Value')));
nave=str2double(get(FL.Gui.IPfitData.edit2,'String')); % get # pulses to use for jitter from IP display GUI
FlHwUpdate('wait',nave);
[~, bpmdata]=FlHwUpdate('bpmave',nave);
if dim=='x'
  jit=bpmdata{1}(4,ipind)*1e6;
else
  jit=bpmdata{1}(5,ipind)*1e6;
end


% --- Executes on button press in pushbutton11.
function pushbutton11_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton11 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

state=get(hObject,'String');

switch state
  case 'Stop'
    set(hObject,'String','Stopping')
    set(hObject,'BackgroundColor','green')
  case 'Stopping'
  case 'Stopped'
    set(hObject,'String','Stop')
    set(hObject,'BackgroundColor','red')
end
drawnow('expose')

function setMagnet(handles,cmd)
global PS FL BEAMLINE

dat=get(handles.figure1,'UserData');
switch cmd
  case 'xmodel'
    valHan=handles.text6;
    ips=dat.psqf1;
    iele=dat.iqf1(1);
  case 'xscan'
    valHan=handles.text8;
    ips=dat.psqf1;
    iele=dat.iqf1(1);
  case 'ymodel'
    valHan=handles.text2;
    ips=dat.psqd0;
    iele=dat.iqd0(1);
  case 'yscan'
    valHan=handles.text5;
    ips=dat.psqd0;
    iele=dat.iqd0(1);
end

psval=interp1(FL.HwInfo.PS(ips).conv(1,:),FL.HwInfo.PS(ips).conv(2,:),str2double(get(valHan,'String')),'linear');
psval=psval/abs(BEAMLINE{iele}.B);
if psval<PS(ips).Ampl
  PS(ips).SetPt=psval-0.1;
  PSTrim(ips,1);
  pause(2)
end
PS(ips).SetPt=psval;
PSTrim(ips,1);

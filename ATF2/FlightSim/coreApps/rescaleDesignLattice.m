function rescaleDesignLattice( Pin )
%rescaleDesignLattice Scale the energy of the design lattice
%   Set BEAMLINE P and B fields scaled to the input momentum (Pin) in GeV
global BEAMLINE FL

E_offset=FL.props.E_offset+Pin-BEAMLINE{1}.P;
FL.props.E_offset=E_offset;
if exist('props.mat','file')
  save -append props.mat E_offset
else
  save props.mat E_offset
end
% set(FL.Gui.main.E_offset,'Label',sprintf('E_offset = %.3g',FL.E_offset))

% for iele=1:length(BEAMLINE)
%   if isfield(BEAMLINE{iele},'P')
%     sf=Pin/BEAMLINE{iele}.P;
%     BEAMLINE{iele}.P=Pin;
%     if isfield(BEAMLINE{iele},'B')
%       BEAMLINE{iele}.B=BEAMLINE{iele}.B.*sf;
%     end
%   end
% end
% for ib=1:length(FL.SimBeam)
%   FL.SimBeam{ib}.Bunch.x(6,:)=FL.SimBeam{ib}.Bunch.x(6,:).*sf;
% end
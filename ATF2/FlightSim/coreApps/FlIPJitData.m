function [stat knob data]=FlIPJitData
global FL BEAMLINE
persistent iex L1 LBPM
Model=FL.SimModel;
if isempty(iex); iex=findcells(BEAMLINE,'Name','IEX'); end;
stat{1}=1;
FL.SimMode=2;
Beam0=FL.SimBeam{2};
sigx=sqrt(Model.Initial.x.Twiss.beta*(Model.Initial.x.NEmit/(1.28/0.511e-3)));
sigxp=sqrt(((1+Model.Initial.x.Twiss.alpha^2)/Model.Initial.x.Twiss.beta)*(Model.Initial.x.NEmit/(1.28/0.511e-3)));
sigy=sqrt(Model.Initial.y.Twiss.beta*(Model.Initial.y.NEmit/(1.28/0.511e-3)));
sigyp=sqrt(((1+Model.Initial.y.Twiss.alpha^2)/Model.Initial.y.Twiss.beta)*(Model.Initial.y.NEmit/(1.28/0.511e-3)));
FL.exciter.FFS_switch='on';
if isempty(L1)
  L1=BEAMLINE{findcells(BEAMLINE,'Name','IP')}.S-BEAMLINE{findcells(BEAMLINE,'Name','IPBPMA')}.S;
  LBPM=BEAMLINE{findcells(BEAMLINE,'Name','IPBPMB')}.S-BEAMLINE{findcells(BEAMLINE,'Name','IPBPMA')}.S;
end
bpmip_reco=[]; bpmip=[];
FlBeamExcite('SetRefV');
for itrack=1:180
  Beam0.Bunch.x([1:4 6])=[0 0 0 0 Model.Initial.Momentum]+randn(1,5).*[sigx sigxp sigy sigyp 1e-4];
  stat=FlBeamExcite('run');
  if stat{1}~=1; fprintf('Track Error in FlIPJitData: %s\n',stat{2}); continue; end;
  [stat bo instdata]=TrackThru(iex,Model.ip_ind,Beam0,1,1,0);
  if stat{1}~=1; fprintf('Track Error in FlIPJitData: %s\n',stat{2}); continue; end;
  bpmip(end+1,:)=bo.Bunch.x(1:4);
  xip=instdata{1}(end-2).x+(L1/LBPM)*(instdata{1}(end-1).x-instdata{1}(end-2).x);
  xpip=(1/LBPM)*(instdata{1}(end-1).x-instdata{1}(end-2).x);
  yip=instdata{1}(end-2).y+(L1/LBPM)*(instdata{1}(end-1).y-instdata{1}(end-2).y);
  ypip=(1/LBPM)*(instdata{1}(end-1).y-instdata{1}(end-2).y);
  bpmip_reco(end+1,:)=[xip xpip yip ypip];
end
FlBeamExcite('ResetCor');
data=[]; knob=[];
if isempty(bpmip_reco)
  return
end
Fs=1.56;
L=itrack;
NFFT = 2^nextpow2(L);
sz=size(bpmip_reco);
for idim=1:sz(2)
  y=bpmip_reco(:,idim);
  Y = fft(y,NFFT)/L;
  f = Fs/2*linspace(0,1,NFFT/2+1);
  f=f(2:end);
  ymag=2*abs(Y(2:NFFT/2+1));
  ypha=angle(Y(2:NFFT/2+1));
  data=[data; ymag.*sign(ypha)];
  knob=[knob f];
end
bpmcov=triu(cov(bpmip_reco));
data=[data; bpmcov(bpmcov~=0)]';
knob=[knob find(bpmcov~=0)'];
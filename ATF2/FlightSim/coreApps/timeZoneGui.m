function varargout = timeZoneGui(varargin)
% TIMEZONEGUI M-file for timeZoneGui.fig
%      TIMEZONEGUI, by itself, creates a new TIMEZONEGUI or raises the existing
%      singleton*.
%
%      H = TIMEZONEGUI returns the handle to a new TIMEZONEGUI or the handle to
%      the existing singleton*.
%
%      TIMEZONEGUI('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in TIMEZONEGUI.M with the given input arguments.
%
%      TIMEZONEGUI('Property','Value',...) creates a new TIMEZONEGUI or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before timeZoneGui_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to timeZoneGui_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help timeZoneGui

% Last Modified by GUIDE v2.5 10-Aug-2009 16:33:46

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @timeZoneGui_OpeningFcn, ...
                   'gui_OutputFcn',  @timeZoneGui_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before timeZoneGui is made visible.
function timeZoneGui_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to timeZoneGui (see VARARGIN)
global FL 

% Choose default command line output for timeZoneGui
handles.output = handles;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes timeZoneGui wait for user response (see UIRESUME)
% uiwait(handles.figure1);
tzp=textscan(evalc('FL.tzfunc.places()'),'%s','Delimiter','\n');
set(handles.listbox1,'String',tzp{1})
set(handles.listbox1,'Value',find(ismember(tzp{1},FL.timezoneName),1))
set(handles.checkbox1,'Value',FL.timezoneDST)


% --- Outputs from this function are returned to the command line.
function varargout = timeZoneGui_OutputFcn(hObject, eventdata, handles) %#ok<*INUSL>
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on selection change in listbox1.
function listbox1_Callback(hObject, eventdata, handles) %#ok<*INUSD,*DEFNU>
% hObject    handle to listbox1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global FL

try
  contents = get(hObject,'String');
  FL.timezone=FL.tzfunc.zone(contents{get(hObject,'Value')})+get(handles.checkbox1,'Value');
  FL.timezoneName=contents{get(hObject,'Value')};
catch
  errordlg('Error setting timezone','timezone error')
end


% --- Executes during object creation, after setting all properties.
function listbox1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to listbox1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: listbox controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in pushbutton1.
function pushbutton1_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

guiCloseFn('timeZoneGui',handles);


% --- Executes when user attempts to close figure1.
function figure1_CloseRequestFcn(hObject, eventdata, handles)
% hObject    handle to figure1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

guiCloseFn('timeZoneGui',handles);


% --- Executes on button press in checkbox1.
function checkbox1_Callback(hObject, eventdata, handles)
% hObject    handle to checkbox1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global FL
try
  contents = get(handles.listbox1,'String');
  FL.timezone=FL.tzfunc.zone(contents{get(handles.listbox1,'Value')})+get(hObject,'Value');
  FL.timezoneDST=get(hObject,'Value');
catch
  errordlg('Error Setting Timezone','timezone error')
end

function [stat output] = UpdateINSTR(Model,Beam) %#ok<INUSL>
% Update data BPM / PROF databases using TrackThru - use for Simulation Mode only
global INSTR FL BEAMLINE VERB
persistent dyninit instr_ip

stat{1}=1;
output=[];
qe=1.60217646e-19;
trackRegion='ring';

% Assume in sim mode outside FS environment given following test:
if ~isfield(FL,'mode') || ~isfield(FL,'SimMode') || FL.SimMode==2
  warning('MATLAB:FlHwUpdate_nonFSsim','Non-FS simulation mode used')
  [stat,beamout,instdata] = TrackThru( FL.SimModel.tstart, FL.SimModel.ip_ind, FL.SimBeam{FL.simBeamID}, 1, 1, 0 );
  if stat{1}==1
    beamStopped=false;
  else
    beamStopped=true;
  end
  % Fill ICTDUMP info
  stopfrac=(length(beamout.Bunch.stop)-sum(beamout.Bunch.stop>0))/length(beamout.Bunch.stop);
  ictdump=findcells(BEAMLINE,'Name','ICTDUMP');
  ictdumpinstr=findcells(INSTR,'Index',ictdump);
  bq=(FL.SimModel.Initial.Q/1.6022e-19);
  if ~isempty(ictdumpinstr)
    INSTR{ictdumpinstr}.Data(3)=(bq+randn*1e8) * stopfrac ;
  end
  if numel(FL.SimBeam{FL.simBeamID}.Bunch.x)>6 % assume want beam moment info
    % Wirescanners etc
    nINSTR=find(ismember(cellfun(@(x) x.Index,INSTR),[instdata{2}.Index]));
    if length(nINSTR)~=length([instdata{2}])
      error('Mismatch between INSTR and tracked instr data');
    end % if INSTR ~= instdata
    if ~isempty(nINSTR)
      for idata=1:length(nINSTR)
        INSTR{nINSTR(idata)}.Data=[instdata{2}(idata).x instdata{2}(idata).y bq+randn*1e8 instdata{2}(idata).sig11 ...
          instdata{2}(idata).sig33 instdata{2}(idata).sig13];
      end % for idata
    end % if nINSTR contains vals
    if ~beamStopped
      % Special additional data for IP
      if isempty(instr_ip)
        ipbsm=findcells(BEAMLINE,'Name','MS1IP');
        instr_ip=findcells(INSTR,'Index',ipbsm);
        if isempty(instr_ip)
          error('IP_SM not found');
        end % if no IP_SM
        if strcmp(INSTR{instr_ip}.Class,'NOHW')
          INSTR{instr_ip}.Class='PROF';
          INSTR{instr_ip}.Type='SM';
        end
      end
      % Subtract IP offset from beam so is positioned wrt instr readout
      if isfield(BEAMLINE{FL.SimModel.ip_ind},'Offset')
        dim=[1 3];
        for iDim=1:2
          beamout.Bunch.x(dim(iDim),:)=beamout.Bunch.x(dim(iDim),:)-BEAMLINE{FL.SimModel.ip_ind}.Offset(dim(iDim));
        end % for dim
      end
      % Take into account element roll
      if isfield(BEAMLINE{FL.SimModel.ip_ind},'Offset') && BEAMLINE{FL.SimModel.ip_ind}.Offset(6)
        th=BEAMLINE{FL.SimModel.ip_ind}.Offset(6);
        beamout.Bunch.x([1 3],:)=[cos(th) -sin(th);sin(th) cos(th)]*beamout.Bunch.x([1 3],:);
      end
      % Shintake monitor sim
      if isfield(FL.SimModel,'Sm')
        %%%%%%%%%%%%%%%%%%%%%%%% Remove beam jitter
        %   beamout.Bunch.x(3,:)=beamout.Bunch.x(3,:)-mean(beamout.Bunch.x(3,:));
        %%%%%%%%%%%%%%%%%%%%%%%%%
        FL.SimModel.Sm = shintakeMonitor(FL.SimModel.Sm,beamout,FL.SimModel.Sm.scanPhases(FL.SimModel.Sm.pulseNum));
        FL.SimModel.Sm.scan.signal(FL.SimModel.Sm.pulseNum)=FL.SimModel.Sm.signal;
        FL.SimModel.Sm.scan.error(FL.SimModel.Sm.pulseNum)=FL.SimModel.Sm.error;
        INSTR{instr_ip}.Data(3) = FL.SimModel.Sm.signal ;
      end % SM sim
      if numel(FL.SimBeam{FL.simBeamID}.Bunch.x)/6>1e4
        rays0=[beamout.Bunch.x]';
        P=mean(rays0(:,6)); % GeV/c
        dp=(rays0(:,6)-P)/P;
        rays0(:,6)=dp;
        [rays,xv,sigm,sigv,emit,emitn,twss,eta1,eta2]=AnalyzeRays(rays0,P,0,any(VERB)); %#ok<ASGLU,NASGU>
        INSTR{instr_ip}.Data(4)=abs(sigv(1));%+randn*INSTR{instr_ip}.Res(4);
        INSTR{instr_ip}.Data(5)=abs(sigv(3));%+randn*INSTR{instr_ip}.Res(5);
        if isfield(FL.SimModel,'Measure_ipbcor') && FL.SimModel.Measure_ipbcor
          [fitTerm,fitCoef,bsize_corrected,bsize,p] = beamTerms(3,beamout);
          INSTR{instr_ip}.bcor.terms=fitTerm;
          INSTR{instr_ip}.bcor.tsize=bsize;
          INSTR{instr_ip}.bcor.vals=fitCoef;
          INSTR{instr_ip}.bcor.gvals=p.Coefficients;
          INSTR{instr_ip}.bcor.bsize_corrected=bsize_corrected;
        end % if want to measure beam term correlations at IP
      end % if macro-bunch
      INSTR{instr_ip}.sigma=cov(beamout.Bunch.x');
      INSTR{instr_ip}.P=mean(beamout.Bunch.x(6,:));
      % fill sigmas
      for ii=find(ismember(arrayfun(@(x) INSTR{x}.Index,1:length(INSTR)),[instdata{2}.Index]))
        INSTR{ii}.sigma(1,1)=instdata{2}([instdata{2}.Index]==INSTR{ii}.Index).sig11;
        INSTR{ii}.sigma(3,3)=instdata{2}([instdata{2}.Index]==INSTR{ii}.Index).sig33;
        INSTR{ii}.sigma(1,3)=instdata{2}([instdata{2}.Index]==INSTR{ii}.Index).sig13;
        INSTR{ii}.sigma(5,5)=instdata{2}([instdata{2}.Index]==INSTR{ii}.Index).sig55;
      end
    end
  else
    nINSTR=find(ismember(cellfun(@(x) x.Index,INSTR),[instdata{2}.Index]));
%     if length(nINSTR)~=length([instdata{2}])
%       error('Mismatch between INSTR and tracked instr data');
%     end % if INSTR ~= instdata
    % Fill BPM info
    for idata=1:length(nINSTR)
      INSTR{nINSTR(idata)}.Data(1:3)=[instdata{2}(idata).x instdata{2}(idata).y bq+randn*1e8];
    end % for idata
  end % if macro-bunch
  nINSTR=find(ismember(cellfun(@(x) x.Index,INSTR),[instdata{1}.Index]));
%   if length(nINSTR)~=length([instdata{1}])
%     error('Mismatch between INSTR and tracked bpm data');
%   end % if INSTR ~= instdata
  % Fill BPM info
  for idata=1:length(nINSTR)
    INSTR{nINSTR(idata)}.Data(1:3)=[instdata{1}(idata).x instdata{1}(idata).y bq+randn*1e8];
  end % for idata
  % Increment track count
  if numel(FL.SimBeam{FL.simBeamID}.Bunch.x)/6>1e4
    FL.SimData.trackCount=FL.SimData.trackCount+FL.SimModel.nPulseMeas;
  else
    FL.SimData.trackCount=FL.SimData.trackCount+1;
  end
  return
end % if simmode non-FS

% Init dynamic errors if not done
if isempty(dyninit)
  dyninit=true;
  stat = SimApplyErrors('dynamic');
  if stat{1}~=1; return; end;
end % if not initialised dynamic errors yet

% Track to get INSTR values, and put into EPICS DB
if ~isfield(FL,'SimMode') || ~FL.SimMode % Sim Mode
  error('Don''t use UpdateINSTR in h/w access mode!')
end

index=1;
if strcmp(FL.simTrackMode,'macro')
  beam=Beam{1};
  trackMode=1;
elseif strcmp(FL.simTrackMode,'single')
  beam=Beam{2};
  trackMode=0;
elseif strcmp(FL.simTrackMode,'sparse')
  beam=Beam{3};
  trackMode=2;
end % which track mode?
% Apply dynamic error conditions
if isfield(FL,'useDynErr') && FL.useDynErr
  [stat, beam] = SimApplyErrors('dynamic',beam);
  if stat{1}~=1; stat{2}=['Error applying dynamic errors to EXT: ',stat{2}]; return; end;
end % if FL.useDynErr
% Need to track in S (BEAMLINE index) order
[Y I]=sort(cellfun(@(x) x.Index,INSTR));
comstack={};
for i_inst=I
  if ~isequal(INSTR{i_inst}.Class,'NOHW') && isfield(FL.HwInfo.INSTR(i_inst),'pvname') && ...
      iscell(FL.HwInfo.INSTR(i_inst).pvname) && ~isempty(FL.HwInfo.INSTR(i_inst).pvname)
    beamStop=false;
    if isequal(trackRegion,'ring') && INSTR{i_inst}.Index>FL.SimModel.extStart
      [tstat,beam] = TrackThru( index, FL.SimModel.extStart, beam, 1, 1, 0 );
      if tstat{1}~=1; beamStop=true; end;
      % ---> Put any EXT specific initial condition stuff here <---
      index=FL.SimModel.extStart;
      trackRegion='EXT';
    end % if passing EXT bondary region
    [tstat,beam,instdata] = TrackThru( index, INSTR{i_inst}.Index, beam, 1, 1, 0 );
    if tstat{1}~=1; beamStop=true; end;
    index=INSTR{i_inst}.Index+1;
    % Fill database
    if strcmp(INSTR{i_inst}.Class,'MONI')
      % X data
      if beamStop
        INSTR{i_inst}.Data(1)=randn*1e-20;
      else
        INSTR{i_inst}.Data(1)=randn*1e-12+instdata{1}(end).x;
      end % if beamStop
      % Y data
      comstack{end+1,1}=FL.HwInfo.INSTR(i_inst).pvname{1}{2};
      if beamStop
        INSTR{i_inst}.Data(2)=randn*1e-20;
      else
        INSTR{i_inst}.Data(2)=randn*1e-12+instdata{1}(end).y;
      end % if beamStop
      % TMIT data
      if trackMode>0
        if beamStop
          tmitVal=abs(randn);
        else
          tmitVal=(sum(beam.Bunch.Q.*(1-beam.Bunch.stop))./qe);
          tmitVal=tmitVal+tmitVal*randn*0.01;
        end % if beamStop
      else
        if beamStop
          tmitVal=abs(randn);
        else
          tmitVal=(sum(Beam{1}.Bunch.Q)./qe);
          tmitVal=tmitVal+tmitVal*randn*0.01;
        end % if beamStop
      end % if trackMode
      INSTR{i_inst}.Data(3)=tmitVal;
    elseif strcmp(INSTR{i_inst}.Type,'bkgmon')
      INSTR{i_inst}.Data(3)=sum(beam.Bunch.Q)./qe;
    elseif strcmp(INSTR{i_inst}.Class,'IMON')
      comstack{end+1,1}=FL.HwInfo.INSTR(i_inst).pvname{1}{1};
      if beamStop
        INSTR{i_inst}.Data(3)=abs(randn);
      else
        INSTR{i_inst}.Data(3)=sum(beam.Bunch.Q)./qe;
      end
    elseif strcmp(INSTR{i_inst}.Type,'otr') && isfield(FL,'IOC') && isfield(FL.IOC,'otr_pid')
      wOtr=str2double(regexp(BEAMLINE{INSTR{i_inst}.Index}.Name,'\d+','match'))+1;
      % Get OTR position
      otrPos_x=lcaGet(sprintf('mOTR:motor%d',wOtr*3-2))*1e-5;
      otrPos_y=lcaGet(sprintf('mOTR:motor%d',wOtr*3-1))*2e-6;
      % Form OTR image
      beamprof=hist2(beam.Bunch.x(1,:)'+otrPos_x,beam.Bunch.x(3,:)'+otrPos_y,linspace(-320e-6,320e-6,1280),linspace(-240e-6,240e-6,960));
      beamprof=beamprof';
      beamprof=(beamprof./max(max(beamprof))).*4000;
      lcaPut(sprintf('mOTR:image%d:ArrayData',wOtr),beamprof(:)');
      lcaPut(sprintf('mOTR:image%d:ArrayData.PROC',wOtr),1);
    else % Assume profile measuring device
      if beamStop
        INSTR{i_inst}.Data=ones(1,6).*randn.*1e-20;
      else
        INSTR{i_inst}.Data=[instdata{2}(end).x instdata{2}(end).y 1 instdata{2}(end).sig11 ...
          instdata{2}(end).sig33 instdata{2}(end).sig13] ;
      end % if beamstop
    end % if moni
  end % if hw available
end % for i_inst
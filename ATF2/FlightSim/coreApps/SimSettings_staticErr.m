function varargout = SimSettings_staticErr(varargin)
% SIMSETTINGS_STATICERR M-file for SimSettings_staticErr.fig
%      SIMSETTINGS_STATICERR, by itself, creates a new SIMSETTINGS_STATICERR or raises the existing
%      singleton*.
%
%      H = SIMSETTINGS_STATICERR returns the handle to a new SIMSETTINGS_STATICERR or the handle to
%      the existing singleton*.
%
%      SIMSETTINGS_STATICERR('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in SIMSETTINGS_STATICERR.M with the given input arguments.
%
%      SIMSETTINGS_STATICERR('Property','Value',...) creates a new SIMSETTINGS_STATICERR or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before SimSettings_staticErr_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to SimSettings_staticErr_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help SimSettings_staticErr

% Last Modified by GUIDE v2.5 05-May-2008 10:45:52

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @SimSettings_staticErr_OpeningFcn, ...
                   'gui_OutputFcn',  @SimSettings_staticErr_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before SimSettings_staticErr is made visible.
function SimSettings_staticErr_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to SimSettings_staticErr (see VARARGIN)
global FL

% Choose default command line output for SimSettings_staticErr
handles.output = handles;

% Update handles structure
guidata(hObject, handles);

% Update error fields from Model structure
set(handles.edit1,'String',num2str(FL.SimModel.align.x*1e6));
set(handles.edit2,'String',num2str(FL.SimModel.align.y*1e6));
set(handles.edit3,'String',num2str(FL.SimModel.align.rot*1e6));
set(handles.edit6,'String',num2str(FL.SimModel.align.z*1e6));
set(handles.edit7,'String',num2str(FL.SimModel.align.magbpm_x*1e6));
set(handles.edit8,'String',num2str(FL.SimModel.align.magbpm_y*1e6));
set(handles.edit4,'String',num2str(FL.SimModel.align.dB*1e5));
set(handles.edit5,'String',num2str(FL.SimModel.align.dB_syst*1e5));

udat{1}=figState(handles);
set(handles.figure1,'UserData',udat);

% --- Outputs from this function are returned to the command line.
function varargout = SimSettings_staticErr_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;



function edit7_Callback(hObject, eventdata, handles)
% hObject    handle to edit7 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit7 as text
%        str2double(get(hObject,'String')) returns contents of edit7 as a double


% --- Executes during object creation, after setting all properties.
function edit7_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit7 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit4_Callback(hObject, eventdata, handles)
% hObject    handle to edit4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit4 as text
%        str2double(get(hObject,'String')) returns contents of edit4 as a double


% --- Executes during object creation, after setting all properties.
function edit4_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit5_Callback(hObject, eventdata, handles)
% hObject    handle to edit5 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit5 as text
%        str2double(get(hObject,'String')) returns contents of edit5 as a double


% --- Executes during object creation, after setting all properties.
function edit5_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit5 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit1_Callback(hObject, eventdata, handles)
% hObject    handle to edit1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit1 as text
%        str2double(get(hObject,'String')) returns contents of edit1 as a double


% --- Executes during object creation, after setting all properties.
function edit1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit2_Callback(hObject, eventdata, handles)
% hObject    handle to edit2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit2 as text
%        str2double(get(hObject,'String')) returns contents of edit2 as a double


% --- Executes during object creation, after setting all properties.
function edit2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit3_Callback(hObject, eventdata, handles)
% hObject    handle to edit3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit3 as text
%        str2double(get(hObject,'String')) returns contents of edit3 as a double


% --- Executes during object creation, after setting all properties.
function edit3_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit6_Callback(hObject, eventdata, handles)
% hObject    handle to edit6 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit6 as text
%        str2double(get(hObject,'String')) returns contents of edit6 as a double


% --- Executes during object creation, after setting all properties.
function edit6_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit6 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit8_Callback(hObject, eventdata, handles)
% hObject    handle to edit8 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit8 as text
%        str2double(get(hObject,'String')) returns contents of edit8 as a double


% --- Executes during object creation, after setting all properties.
function edit8_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit8 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in pushbutton1.
function pushbutton1_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
udat=get(handles.figure1,'UserData');
if ~isequal(figState(handles),udat{1})
  resp=questdlg('Settings have been changed but not saved','Settings Changed',...
    'Quit anyway','Save before quiting','Cancel','Cancel');
  switch resp
    case 'Quit anyway'
      guiCloseFn('SimSettings_dynamicErr',handles);
    case 'Save before quiting'
      stat=saveErrData(handles);
      if stat{1}~=1
        errordlg(stat{2},'Static Error Save Error');
      else
        guiCloseFn('SimSettings_dynamicErr',handles);
      end % if err
  end % switch resp
else
  guiCloseFn('SimSettings_dynamicErr',handles);
end % if settings changed

% --- Executes on button press in pushbutton2.
function pushbutton2_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
stat=saveErrData(handles);
if stat{1}~=1; errordlg(stat{2},'Static Error Save Error'); end;

function stat=saveErrData(handles)
global FL

% Change sim model parameters with field values
x=str2double(get(handles.edit1,'String'))/1e6;
stat=valCheck(x,'Horizontal Magnet Alignment'); if stat{1}~=1; return; end;
FL.SimModel.align.x=x;
x=str2double(get(handles.edit2,'String'))/1e6;
stat=valCheck(x,'Vertical Magnet Alignment'); if stat{1}~=1; return; end;
FL.SimModel.align.y=x;
x=str2double(get(handles.edit3,'String'))/1e6;
stat=valCheck(x,'Roll Magnet Alignment'); if stat{1}~=1; return; end;
FL.SimModel.align.rot=x;
x=str2double(get(handles.edit6,'String'))/1e6;
stat=valCheck(x,'Longitudinal Magnet Alignment'); if stat{1}~=1; return; end;
FL.SimModel.align.z=x;
x=str2double(get(handles.edit7,'String'))/1e6;
stat=valCheck(x,'Horizontal BPM-MAG alignment'); if stat{1}~=1; return; end;
FL.SimModel.align.magbpm_x=x;
x=str2double(get(handles.edit8,'String'))/1e6;
stat=valCheck(x,'Vertical BPM-MAG alignment'); if stat{1}~=1; return; end;
FL.SimModel.align.magbpm_y=x;
x=str2double(get(handles.edit4,'String'))/1e5;
stat=valCheck(x,'dB/B RMS'); if stat{1}~=1; return; end;
FL.SimModel.align.dB=x;
x=str2double(get(handles.edit5,'String'))/1e5;
stat=valCheck(x,'dB/B systematic'); if stat{1}~=1; return; end;
FL.SimModel.align.dB_syst=x;

% Store save state
udat=get(handles.figure1,'UserData');
udat{1}=figState(handles);
set(handles.figure1,'UserData',udat);


function stat=valCheck(x,str)
if isnan(x) || x<0
  stat{1}=-1; stat{2}=[str,' must be numeric and >0'];
else
  stat{1}=1;
end % x xcheck


% --- Executes when user attempts to close figure1.
function figure1_CloseRequestFcn(hObject, eventdata, handles)
% hObject    handle to figure1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

guiCloseFn('SimSettings_staticErr',handles);

% Get array of Value and String properties for this figure
function array = figState(handles)

hans=fieldnames(handles);
array=[];
for ihan=1:length(hans)
  if ~isequal(hans{ihan},'pushbutton1') && ~isequal(hans{ihan},'pushbutton2')
    try
      array=[array double(get(handles.(hans{ihan}),'String'))];
    catch
    end % try/catch
    try
      array=[array get(handles.(hans{ihan}),'Value')];
    catch
    end % try/catch
  end % if not pushbutton1 or 2 (exit or save button)
end % for ihan



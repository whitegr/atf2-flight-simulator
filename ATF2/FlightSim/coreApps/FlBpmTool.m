function varargout = FlBpmTool(varargin)
% FLBPMTOOL M-file for FlBpmTool.fig
%      FLBPMTOOL, by itself, creates a new FLBPMTOOL or raises the existing
%      singleton*.
%
%      H = FLBPMTOOL returns the handle to a new FLBPMTOOL or the handle to
%      the existing singleton*.
%
%      FLBPMTOOL('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in FLBPMTOOL.M with the given input arguments.
%
%      FLBPMTOOL('Property','Value',...) creates a new FLBPMTOOL or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before FlBpmTool_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to FlBpmTool_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help FlBpmTool

% Last Modified by GUIDE v2.5 11-Nov-2011 07:28:11

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @FlBpmTool_OpeningFcn, ...
                   'gui_OutputFcn',  @FlBpmTool_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before FlBpmTool is made visible.
function FlBpmTool_OpeningFcn(hObject, eventdata, handles, varargin) %#ok<*INUSL>
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to FlBpmTool (see VARARGIN)
global FL
% Choose default command line output for FlBpmTool
handles.output = handles;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes FlBpmTool wait for user response (see UIRESUME)
% uiwait(handles.figure1);

mh=msgbox('BPM Tool Loading...');

% Initialise BPM tool function and get pars/data
stat=FlBpmToolFn('init'); if stat{1}~=1; errordlg(stat{2},'Init error'); return; end;
[stat pars]=FlBpmToolFn('GetPars'); if stat{1}~=1; errordlg(stat{2},'GetPars error'); return; end
[stat data]=FlBpmToolFn('GetData'); if stat{1}~=1; errordlg(stat{2},'GetData error'); return; end

% Initialise GUI
set(handles.edit1,'String',num2str(pars.nbpm))
set(handles.edit2,'String',num2str(pars.qcut))
set(handles.popupmenu1,'String',data.tagNames)
if isfield(FL,'Region')
  fn=fieldnames(FL.Region);
  regions={'All' fn{:}}; %#ok<*CCAT>
else
  regions='All';
end
set(handles.popupmenu2,'String',regions)
icts={'ict1x','ictdump','linear'};
set(handles.popupmenu3,'String',icts)
if isfield(data,'refinuse')
  set(handles.edit3,'String',data.refinuse)
end
if isfield(data,'stripCal') && isfield(data.stripCal,'date')
  set(handles.text4,'String',datestr(data.stripCal.date))
end
restoreList={'radiobutton1','Value',0;'radiobutton2','Value',0;'radiobutton3','Value',0;...
  'radiobutton4','Value',0;'popupmenu1','Value',1;'popupmenu2','Value',1;'edit1','String',1;...
  'edit2','String',1;'edit3','String',0;'togglebutton1','Value',0;'togglebutton2','Value',0;...
  'togglebutton3','Value',0;'checkbox1','Value',0;'checkbox2','Value',0};
guiRestoreFn('FlBpmTool',handles,restoreList);
bpms=1:4;
for ibpm=bpms
  if get(handles.(['radiobutton',num2str(ibpm)]),'Value')
    for jbpm=bpms(bpms~=ibpm)
      set(handles.(['radiobutton',num2str(jbpm)]),'Value',0)
    end
  end
end
bpmSel(handles)
% Set ET mode and nread parameters
[stat etmode]=FlET('GetMode');
if stat{1}~=1
  errordlg(stat{2},'Error getting ET mode')
elseif strcmp(etmode,'multi')
  set(handles.radiobutton5,'Value',1)
  set(handles.radiobutton6,'Value',0)
elseif strcmp(etmode,'single')
  set(handles.radiobutton5,'Value',0)
  set(handles.radiobutton6,'Value',1)
end
[stat nread]=FlET('GetNRead');
if stat{1}~=1
  errordlg(stat{2},'Error Getting ET Nread Parameter')
else
  set(handles.edit10,'String',num2str(nread))
end
% Set option to read tbt data from server or write to buffer
if strfind(FL.mode,'test')
  set(handles.checkbox4,'String','Read tbt data from Server?')
else
  set(handles.checkbox4,'String','Buffer tbt data?')
end
[stat bpmArrReq]=FlHwUpdate('GetBpmArrayReq');
if stat{1}==1
  set(handles.checkbox4,'Value',bpmArrReq)
end

if ishandle(mh)
  delete(mh);
end

% --- Outputs from this function are returned to the command line.
function varargout = FlBpmTool_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- EXIT button
function pushbutton1_Callback(hObject, eventdata, handles) %#ok<*DEFNU>
% hObject    handle to pushbutton1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
guiCloseFn('FlBpmTool',handles);

% --- save ref to memory
function pushbutton4_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
if ~strcmp(questdlg('Save Reference Waveform to Memory Location?','Ref Save Request'),'Yes')
  return
end
[stat data] = FlBpmToolFn('SaveRefMemory');
if stat{1}~=1
  errordlg(stat{2},'Ref Error')
  return
end
set(handles.edit3,'String',data.refinuse)
drawnow('expose')

% --- restore mem ref
function pushbutton5_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton5 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
if ~strcmp(questdlg('Load Reference Waveform from Memory Location?','Ref Load Request'),'Yes')
  return
end
[stat data] = FlBpmToolFn('LoadRefMemory');
if stat{1}~=1
  errordlg(stat{2},'Ref Error')
  return
end
set(handles.edit3,'String',data.refinuse)
drawnow('expose')

% --- save ref to file
function pushbutton6_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton6 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
[fn,pn]=uiputfile(fullfile('latticeFiles','archive'),'*.mat');
if isequal(fn,0) || isequal(pn,0)
  return
elseif ~strfind(pn,'archive')
  errordlg('Must put file in archive directory','ref save error')
  return
end
[stat data] = FlBpmToolFn('SaveRefFile',fn);
if stat{1}~=1
  errordlg(stat{2},'Ref Error')
  return
end
set(handles.edit3,'String',data.refinuse)
drawnow('expose')

% --- Restore ref from file
function pushbutton7_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton7 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
[fn,pn]=uigetfile(fullfile('latticeFiles','archive'),'*.mat');
if isequal(fn,0) || isequal(pn,0)
  return
elseif ~strfind(pn,'archive')
  errordlg('Must get file from archive directory','ref restore error')
  return
end
[stat data] = FlBpmToolFn('LoadRefFile',fn);
if stat{1}~=1
  errordlg(stat{2},'Ref Error')
  return
end
set(handles.edit3,'String',data.refinuse)
drawnow('expose')

% --- save default ref
function pushbutton8_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton8 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
if ~strcmp(questdlg('Save Default Reference Waveform?','Ref Save Request'),'Yes')
  return
end
[stat data] = FlBpmToolFn('SaveRefDefault');
if stat{1}~=1
  errordlg(stat{2},'Ref Error')
  return
end
set(handles.edit3,'String',data.refinuse)
drawnow('expose')

% --- Restore default reference
function pushbutton9_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton9 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
if ~strcmp(questdlg('Load Default Reference Waveform?','Ref Load Request'),'Yes')
  return
end
[stat data] = FlBpmToolFn('LoadRefDefault');
if stat{1}~=1
  errordlg(stat{2},'Ref Error')
  return
end
set(handles.edit3,'String',data.refinuse)
drawnow('expose')

% --- Executes on selection change in popupmenu2.
function popupmenu2_Callback(hObject, eventdata, handles)
% hObject    handle to popupmenu2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = get(hObject,'String') returns popupmenu2 contents as cell array
%        contents{get(hObject,'Value')} returns selected item from popupmenu2
bpmSel(handles)

% --- Executes during object creation, after setting all properties.
function popupmenu2_CreateFcn(hObject, eventdata, handles) %#ok<*INUSD>
% hObject    handle to popupmenu2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in listbox1.
function listbox1_Callback(hObject, eventdata, handles)
% hObject    handle to listbox1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = get(hObject,'String') returns listbox1 contents as cell array
%        contents{get(hObject,'Value')} returns selected item from listbox1


% --- Executes during object creation, after setting all properties.
function listbox1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to listbox1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: listbox controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in popupmenu1.
function popupmenu1_Callback(hObject, eventdata, handles)
% hObject    handle to popupmenu1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = get(hObject,'String') returns popupmenu1 contents as cell array
%        contents{get(hObject,'Value')} returns selected item from popupmenu1
bpmSel(handles);

% --- Executes during object creation, after setting all properties.
function popupmenu1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to popupmenu1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- New user tag
function pushbutton2_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
tagName=get(handles.edit4,'String');
if isempty(tagName)
  errordlg('Must supply finite length tag name','New Tag Name Error')
  return
end
stat = FlBpmToolFn('AddUserTag',tagName);
if stat{1}~=1
  errordlg(stat{2},'AddUserTag Error')
  return
end
tags=get(handles.popupmenu1,'String');
tags{end+1}=tagName;
set(handles.popupmenu1,'String',tags)
set(handles.popupmenu1,'Value',length(tags))

% --- hw selection
function radiobutton1_Callback(hObject, eventdata, handles)
% hObject    handle to radiobutton1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of radiobutton1
set(handles.radiobutton1,'Value',1)
set(handles.radiobutton2,'Value',0)
set(handles.radiobutton3,'Value',0)
set(handles.radiobutton4,'Value',0)
bpmSel(handles)
drawnow('expose')

% --- cal selection
function radiobutton2_Callback(hObject, eventdata, handles)
% hObject    handle to radiobutton2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of radiobutton2
set(handles.radiobutton1,'Value',0)
set(handles.radiobutton2,'Value',1)
set(handles.radiobutton3,'Value',0)
set(handles.radiobutton4,'Value',0)
bpmSel(handles);

% --- selection to false
function pushbutton3_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
bpmSelSet(handles,'selfalse');
drawnow('expose')

% --- use selection
function radiobutton3_Callback(hObject, eventdata, handles)
% hObject    handle to radiobutton3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of radiobutton3
set(handles.radiobutton1,'Value',0)
set(handles.radiobutton2,'Value',0)
set(handles.radiobutton3,'Value',1)
set(handles.radiobutton4,'Value',0)
bpmSel(handles);

% --- Executes on button press in pushbutton10.
function pushbutton10_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton10 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- all to true
function pushbutton11_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton11 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
bpmSelSet(handles,'alltrue');
drawnow('expose')

% --- Executes on button press in pushbutton12.
function pushbutton12_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton12 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
bpmSelSet(handles,'allfalse')
drawnow('expose')

% --- Set bpm averaging
function edit1_Callback(hObject, eventdata, handles)
% hObject    handle to edit1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit1 as text
%        str2double(get(hObject,'String')) returns contents of edit1 as a double
newpars.nbpm=str2double(get(hObject,'String'));
stat=FlBpmToolFn('SetPars',newpars);
if stat{1}~=1
  errordlg(stat{2},'SetPars error')
  return
end

% --- Executes during object creation, after setting all properties.
function edit1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Set charge cut parameters
function edit2_Callback(hObject, eventdata, handles)
% hObject    handle to edit2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit2 as text
%        str2double(get(hObject,'String')) returns contents of edit2 as a double
newpars.qcut=str2double(get(hObject,'String'));
stat=FlBpmToolFn('SetPars',newpars);
if stat{1}~=1
  errordlg(stat{2},'SetPars error')
  return
end

% --- Executes during object creation, after setting all properties.
function edit2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Take new ref orbit
function pushbutton13_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton13 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
if ~strcmp(questdlg('Measure New Reference Orbit?','Reference Orbit Request'),'Yes')
  return
end
[stat data] = FlBpmToolFn('MeasureRefOrbit');
if stat{1}~=1
  errordlg(stat{2},'Measure Ref Orbit Error')
  return
end
set(handles.edit3,'String',data.refinuse)
drawnow('expose')

% --- set selection to true
function pushbutton14_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton14 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
bpmSelSet(handles,'seltrue');
drawnow('expose')

% --- Clibrate striplines
function pushbutton15_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton15 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
if ~strcmp(questdlg('Acquire 100 pulses and calculate new stripline calibration constants?',...
    'Stripline Calibration'),'Yes')
  return
end
han=msgbox(['Acquiring pulses and generating calibration constants for stripline bpms...' ...
  'ENSURE SERVER AUTO UPDATE IS SET'],'Stripline Calibration');
[stat data] = FlBpmToolFn('GenerateStriplineCal');
if ishandle(han); delete(han); end;
if stat{1}~=1
  errordlg(stat{2},'Calibration error')
  return
end
set(handles.text4,'String',datestr(data.stripCal.date))
drawnow('expose')

% --- Plot stripline calibration data
function pushbutton16_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton16 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
stat = FlBpmToolFn('normalize',get(handles.popupmenu3,'Value'));
if stat{1}~=1
  errordlg(stat{2},'Normalization Error')
  return
end

% --- use cal selection
function radiobutton4_Callback(hObject, eventdata, handles)
% hObject    handle to radiobutton4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of radiobutton4
set(handles.radiobutton1,'Value',0)
set(handles.radiobutton2,'Value',0)
set(handles.radiobutton3,'Value',0)
set(handles.radiobutton4,'Value',1)
bpmSel(handles);
drawnow('expose')

function edit3_Callback(hObject, eventdata, handles)
% hObject    handle to edit3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit3 as text
%        str2double(get(hObject,'String')) returns contents of edit3 as a double


% --- Executes during object creation, after setting all properties.
function edit3_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% --- bpm selection info set
function bpmSel(handles)
global FL INSTR BEAMLINE
persistent allind
if get(handles.radiobutton1,'Value')
  tag='hw';
elseif get(handles.radiobutton2,'Value')
  tag='cal';
elseif get(handles.radiobutton3,'Value')
  tag='use';
elseif get(handles.radiobutton4,'Value')
  tag='useCal';
else
  return
end
[stat data]=FlBpmToolFn('GetData');
if stat{1}~=1
  errordlg(stat{2},'GetData Error')
  return
end
useTags=get(handles.popupmenu1,'String');
useTag=useTags{get(handles.popupmenu1,'Value')};
bpmData=data.(useTag).(tag);
if length(bpmData)~=length(INSTR)
  errordlg('Mismatch between INSTR and bpm data','bpmSel Error')
  return
end
bpmData(isempty(bpmData))=0;
regions=get(handles.popupmenu2,'String');
region=regions{get(handles.popupmenu2,'Value')};
if isempty(allind)
  allind=findcells(BEAMLINE,'Class','MONI');
end
if strcmp(region,'All')
  instind=arrayfun(@(x) findcells(INSTR,'Index',x),allind);
else
  newind=allind(allind<=FL.Region.(region).ind(2) & allind>=FL.Region.(region).ind(1));
  instind=arrayfun(@(x) findcells(INSTR,'Index',x),newind);
end
selstr={'False' 'True'};
bpmstr=arrayfun(@(x) [num2str(x) ' ' BEAMLINE{INSTR{x}.Index}.Name ' : ' selstr{int8(bpmData(x))+1}],...
  instind,'UniformOutput',false);
set(handles.listbox1,'Value',1)
set(handles.listbox1,'String',bpmstr)

% --- bpm selection change
function bpmSelSet(handles,sel)
if get(handles.radiobutton1,'Value')
  type='hw';
elseif get(handles.radiobutton2,'Value')
  type='cal';
elseif get(handles.radiobutton3,'Value')
  type='use';
elseif get(handles.radiobutton4,'Value')
  type='useCal';
else
  return
end
bpmlist=get(handles.listbox1,'String');
if strcmp(sel,'alltrue') || strcmp(sel,'allfalse')
  bpms=bpmlist;
  listind=1:length(bpmlist);
else
  bpms=bpmlist(get(handles.listbox1,'Value'));
  listind=get(handles.listbox1,'Value');
end
bpmind=zeros(length(bpms),1);
for ibpm=1:length(bpms)
  bpmind(ibpm)=str2double(regexp(bpms{ibpm},'^\d+','match'));
end
tags=get(handles.popupmenu1,'String');
tag=tags{get(handles.popupmenu1,'Value')};
if ~isempty(strfind(sel,'true'))
  stat = FlBpmToolFn('SetStatus',tag,type,bpmind,true);
  if stat{1}~=1
    errordlg(stat{2},'Bpm status change error')
    return
  end
  for ibpm=listind
    bpmlist{ibpm}=regexprep(bpmlist{ibpm},'False','True');
  end
  set(handles.listbox1,'Value',listind(end))
  set(handles.listbox1,'String',bpmlist)  
else
  stat = FlBpmToolFn('SetStatus',tag,type,bpmind,false);
  if stat{1}~=1
    errordlg(stat{2},'Bpm status change error')
    return
  end
  for ibpm=listind
    bpmlist{ibpm}=regexprep(bpmlist{ibpm},'True','False');
  end
  set(handles.listbox1,'Value',listind(end))
  set(handles.listbox1,'String',bpmlist)
end

% --- Upload data to server
function pushbutton19_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton19 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global FL
if strcmp(FL.mode,'trusted')
  errordlg('Dude, you like already are on the server...','Ref Save Error')
  return
end
if ~strcmp(questdlg('Sync Data To Server?','Server Sync Request'),'Yes')
  return
end
stat = FlBpmToolFn('ServerSync');
if stat{1}~=1
  errordlg(stat{2},'Server Sync Error')
  return
end
drawnow('expose')

% --- save ref to server
function pushbutton17_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton17 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global FL
if strcmp(FL.mode,'trusted')
  errordlg('Dude, you like already are on the server...','Ref Save Error')
  return
end
if ~strcmp(questdlg('Upload Reference Waveform to Server?','Ref Save Request'),'Yes')
  return
end
[stat data] = FlBpmToolFn('SaveServerRef');
if stat{1}~=1
  errordlg(stat{2},'Ref Error')
  return
end
set(handles.edit3,'String',data.refinuse)
drawnow('expose')

% --- Restore ref from server
function pushbutton18_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton18 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global FL
if strcmp(FL.mode,'trusted')
  errordlg('Dude, you like already are on the server...','Ref Load Error')
  return
end
if ~strcmp(questdlg('Load Reference Waveform from Server?','Ref Load Request'),'Yes')
  return
end
[stat data] = FlBpmToolFn('LoadServerRef');
if stat{1}~=1
  errordlg(stat{2},'Ref Error')
  return
end
set(handles.edit3,'String',data.refinuse)
drawnow('expose')

function edit4_Callback(hObject, eventdata, handles)
% hObject    handle to edit4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit4 as text
%        str2double(get(hObject,'String')) returns contents of edit4 as a double


% --- Executes during object creation, after setting all properties.
function edit4_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- delete user tag
function pushbutton20_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton20 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
tags=get(handles.popupmenu1,'String');
tag=tags{get(handles.popupmenu1,'Value')};
if strcmp(tag,'global')
  errordlg('Cannot delete global tag','Tag Remove Error')
  return
end
stat = FlBpmToolFn('RemoveUserTag',tag);
if stat{1}~=1
  errordlg(stat{2},'Tag Remove Error')
  return
end
tags=get(handles.popupmenu1,'String');
tags=tags(~ismember(tags,tag));
set(handles.popupmenu1,'Value',1)
set(handles.popupmenu1,'String',tags)
drawnow('expose')

% --- Executes on button press in pushbutton21.
function pushbutton21_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton21 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
stat=FlBpmToolFn('init');
if stat{1}~=1
  errordlg(stat{2},'Initialisation error')
  return
end
[stat pars]=FlBpmToolFn('GetPars'); if stat{1}~=1; errordlg(stat{2},'GetPars error'); return; end
[stat data]=FlBpmToolFn('GetData'); if stat{1}~=1; errordlg(stat{2},'GetData error'); return; end

% Initialise GUI
set(handles.edit1,'String',num2str(pars.nbpm))
set(handles.edit2,'String',num2str(pars.qcut))
set(handles.popupmenu1,'String',data.tagNames)
if isfield(data,'refinuse')
  set(handles.edit3,'String',data.refinuse)
end
if isfield(data,'stripCal') && isfield(data.stripCal,'date')
  set(handles.text4,'String',datestr(data.stripCal.date))
end
bpmSel(handles)
drawnow('expose')

% --- Executes when user attempts to close figure1.
function figure1_CloseRequestFcn(hObject, eventdata, handles)
% hObject    handle to figure1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: delete(hObject) closes the figure
guiCloseFn('FlBpmTool',handles);


% --- Plot orbits
function pushbutton22_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton22 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global INSTR FL

% Get plotting parameters from GUI
domain=get(handles.togglebutton1,'Value');
doref=get(handles.togglebutton2,'Value');
dodiff=get(handles.togglebutton3,'Value');
doave=get(handles.checkbox1,'Value');
doerr=get(handles.checkbox2,'Value');
if ~any([domain doref dodiff])
  errordlg('Must choose at least 1 thing to plot!','Plot error')
  return
end
tags=get(handles.popupmenu1,'String');
thisTag=tags{get(handles.popupmenu1,'Value')};

% Get bpm data to plot
if doave
  nave=str2double(get(handles.edit1,'String'));
  qcut=str2double(get(handles.edit2,'String'));
  [stat avedata]=FlHwUpdate('bpmave',nave,0,[qcut,3,1,0]);
  if stat{1}~=1
    errordlg(stat{2},'Bpm ave request error')
    return
  end
%   bpmdata=avedata{1}(1:3,:)'.*1e6;
%   bpmdata_err=avedata{1}(4:6,:)'.*1e6;
  bpmdata(:,1:2)=avedata{1}(1:2,:)'.*1e6;
  bpmdata_err(:,1:2)=avedata{1}(4:5,:)'.*1e6;
  bpmdata(:,3)=avedata{1}(3,:)'.*1e-10;
  bpmdata_err(:,3)=avedata{1}(6,:)'.*1e-10;
else
  FlHwUpdate;
  bpmdata(:,1)=cellfun(@(x) x.Data(1),INSTR).*1e6;
  bpmdata(:,2)=cellfun(@(x) x.Data(2),INSTR).*1e6;
  bpmdata(:,3)=cellfun(@(x) x.Data(3),INSTR).*1e-10;
  bpmdata_err=zeros(size(bpmdata));
end
refdata(:,1)=cellfun(@(x) x.ref(1),INSTR).*1e6;
refdata(:,2)=cellfun(@(x) x.ref(2),INSTR).*1e6;
refdata(:,3)=cellfun(@(x) x.ref(3),INSTR).*1e-10;
refdata_err(:,1)=cellfun(@(x) x.referr(1),INSTR).*1e6;
refdata_err(:,2)=cellfun(@(x) x.referr(2),INSTR).*1e6;
refdata_err(:,3)=cellfun(@(x) x.referr(3),INSTR).*1e-10;
diffdata=bpmdata-refdata;
diffdata_err=sqrt(bpmdata.^2+refdata.^2);

% Which bpms to plot?
bpmstr=get(handles.listbox1,'String');
bpmind=zeros(length(bpmstr),1);
for ibpm=1:length(bpmstr)
  bpmind(ibpm)=str2double(regexp(bpmstr{ibpm},'^\d+','match'));
end
[~, tdata] = FlBpmToolFn('GetData');
nodisplay=find(~tdata.(thisTag).hw|~tdata.(thisTag).use);
bpmind(ismember(bpmind,nodisplay))=[];

% Make NaN readings obvious
bpmdata(isnan(bpmdata))=99999;

% Plot orbits
if ~isfield(FL.Gui,'FlBpmTool_plot1') || ~ishandle(FL.Gui.FlBpmTool_plot1)
  FL.Gui.FlBpmTool_plot1=figure;
else
  figure(FL.Gui.FlBpmTool_plot1);
end
cols={'r' [0,0.9,0] 'b'};
ylabs={'h BPM (um)' 'v BPM (um)' 'TMIT / 1e10'};
FL.noUpdate=true;
for ipl=1:3
  subplot(3,1,ipl)
  if domain
    if doerr
      errorbar(bpmind,bpmdata(bpmind,ipl),bpmdata_err(bpmind,ipl),'Color',cols{ipl})
    else
      plot(bpmind,bpmdata(bpmind,ipl),'Color',cols{ipl})
    end
    if ipl==3
      ax=axis;
      ax(3)=0;
      ax(4)=max(bpmdata(bpmind,3))*1.1;
      axis(ax)
    end
    hold on
  end
  if doref
    if doerr
      errorbar(bpmind,refdata(bpmind,ipl),refdata_err(bpmind,ipl),'k')
    else
      plot(bpmind,refdata(bpmind,ipl),'k')
    end
    if ipl==3
      ax=axis;
      ax(3)=0;
      ax(4)=max(abs(refdata(bpmind,3)))*1.1;
      if ~isnan(ax(4)) && ax(3)~=ax(4)
        axis(ax)
      end
    end
    hold on
  end
  if dodiff
    if doerr
      errorbar(bpmind,diffdata(bpmind,ipl),diffdata_err(bpmind,ipl),':','Color',cols{ipl})
    else
      plot(bpmind,diffdata(bpmind,ipl),':','Color',cols{ipl})
    end
    if ipl==3
      ax=axis;
      ax(3)=0;
      ax(4)=max([ax(4) max(diffdata(bpmind,3))]);
      axis(ax)
    end
    hold on
  end
  xlabel('INSTR Index')
  ylabel(ylabs{ipl})
  hold off
end
FL.noUpdate=false;

% Fill GUI RMS boxes
set(handles.edit5,'String',num2str(std(bpmdata(bpmind,1))))
set(handles.edit6,'String',num2str(std(bpmdata(bpmind,2))))
set(handles.edit7,'String',num2str(std(diffdata(bpmind,1))))
set(handles.edit8,'String',num2str(std(diffdata(bpmind,2))))



% --- Executes on button press in togglebutton1.
function togglebutton1_Callback(hObject, eventdata, handles)
% hObject    handle to togglebutton1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of togglebutton1


% --- Executes on button press in checkbox3.
function checkbox3_Callback(hObject, eventdata, handles)
% hObject    handle to checkbox3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of checkbox3


% --- Executes on button press in togglebutton2.
function togglebutton2_Callback(hObject, eventdata, handles)
% hObject    handle to togglebutton2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of togglebutton2


% --- Executes on button press in togglebutton3.
function togglebutton3_Callback(hObject, eventdata, handles)
% hObject    handle to togglebutton3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of togglebutton3


% --- Executes on button press in checkbox1.
function checkbox1_Callback(hObject, eventdata, handles)
% hObject    handle to checkbox1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of checkbox1


% --- Executes on button press in checkbox2.
function checkbox2_Callback(hObject, eventdata, handles)
% hObject    handle to checkbox2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of checkbox2



function edit5_Callback(hObject, eventdata, handles)
% hObject    handle to edit5 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit5 as text
%        str2double(get(hObject,'String')) returns contents of edit5 as a double


% --- Executes during object creation, after setting all properties.
function edit5_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit5 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit6_Callback(hObject, eventdata, handles)
% hObject    handle to edit6 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit6 as text
%        str2double(get(hObject,'String')) returns contents of edit6 as a double


% --- Executes during object creation, after setting all properties.
function edit6_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit6 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit7_Callback(hObject, eventdata, handles)
% hObject    handle to edit7 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit7 as text
%        str2double(get(hObject,'String')) returns contents of edit7 as a double


% --- Executes during object creation, after setting all properties.
function edit7_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit7 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit8_Callback(hObject, eventdata, handles)
% hObject    handle to edit8 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit8 as text
%        str2double(get(hObject,'String')) returns contents of edit8 as a double


% --- Executes during object creation, after setting all properties.
function edit8_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit8 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Set ElecOffsets for selected BPM
function edit9_Callback(hObject, eventdata, handles)
% hObject    handle to edit9 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit9 as text
%        str2double(get(hObject,'String')) returns contents of edit9 as a double
global INSTR BEAMLINE
bpms=get(handles.listbox1,'String');
ibpm=get(handles.listbox1,'Value');
wbpm=str2double(regexp(bpms{ibpm(1)},'^\d+','match'));
bbaoff=str2num(get(hObject,'String')).*1e-6; %#ok<ST2NM>
if length(bbaoff)~=2 || any(isnan(bbaoff))
  errordlg('Incorrect BBA offset string','BBA offset set error')
  return
end
BEAMLINE{INSTR{wbpm}.Index}.ElecOffset=bbaoff;

% --- Executes during object creation, after setting all properties.
function edit9_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit9 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Edit BBA offset for selected BPM
function pushbutton23_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton23 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global INSTR BEAMLINE
bpms=get(handles.listbox1,'String');
ibpm=get(handles.listbox1,'Value');
wbpm=str2double(regexp(bpms{ibpm(1)},'^\d+','match'));
set(handles.edit9,'String',sprintf('%g %g',BEAMLINE{INSTR{wbpm}.Index}.ElecOffset.*1e6))


% --- Executes on button press in radiobutton5.
function radiobutton5_Callback(hObject, eventdata, handles)
% hObject    handle to radiobutton5 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of radiobutton5
if ~strcmp(questdlg('Change ET setting? (Takes effect on server)','ET Change Request'),'Yes')
  return
end
stat=FlET('SetMode','multi');
if stat{1}~=1
  errordlg(stat{2},'Set Mode Error')
  return
end
set(handles.radiobutton6,'Value',0);

% --- Executes on button press in radiobutton6.
function radiobutton6_Callback(hObject, eventdata, handles)
% hObject    handle to radiobutton6 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of radiobutton6
if ~strcmp(questdlg('Change ET setting? (Takes effect on server)','ET Change Request'),'Yes')
  return
end
stat=FlET('SetMode','single');
if stat{1}~=1
  errordlg(stat{2},'Set Mode Error')
  return
end
set(handles.radiobutton5,'Value',0);


function edit10_Callback(hObject, eventdata, handles)
% hObject    handle to edit10 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit10 as text
%        str2double(get(hObject,'String')) returns contents of edit10 as a double
if ~strcmp(questdlg('Change ET setting? (Takes effect on server)','ET Change Request'),'Yes')
  [stat nread]=FlET('GetNread');
  if stat{1}==1
    set(hObject,'String',nread)
  end
  return
end
stat=FlET('SetNRead',str2double(get(hObject,'String')));
if stat{1}~=1
  errordlg(stat{2},'Set NRead Error');
  [stat nread]=FlET('GetNread');
  if stat{1}==1
    set(hObject,'String',nread)
  end
end

% --- Executes during object creation, after setting all properties.
function edit10_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit10 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in checkbox4.
function checkbox4_Callback(hObject, eventdata, handles)
% hObject    handle to checkbox4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of checkbox4
FlHwUpdate('bpmArrayReq',get(hObject,'Value'));


% --- Executes on button press in pushbutton24.
function pushbutton24_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton24 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on selection change in popupmenu3.
function popupmenu3_Callback(hObject, eventdata, handles)
% hObject    handle to popupmenu3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns popupmenu3 contents as cell array
%        contents{get(hObject,'Value')} returns selected item from popupmenu3


% --- Executes during object creation, after setting all properties.
function popupmenu3_CreateFcn(hObject, eventdata, handles)
% hObject    handle to popupmenu3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in pushbutton25.
function pushbutton25_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton25 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
if strcmp(questdlg('Reset Reference Orbit to Zero?'),'Yes')
  FlBpmToolFn('ZeroRef')
end

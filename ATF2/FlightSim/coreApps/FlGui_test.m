function varargout = FlGui_test(varargin)
% FLGUI_TEST M-file for FlGui_test.fig
%      FLGUI_TEST, by itself, creates a new FLGUI_TEST or raises the existing
%      singleton*.
%
%      H = FLGUI_TEST returns the handle to a new FLGUI_TEST or the handle to
%      the existing singleton*.
%
%      FLGUI_TEST('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in FLGUI_TEST.M with the given input arguments.
%
%      FLGUI_TEST('Property','Value',...) creates a new FLGUI_TEST or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before FlGui_test_OpeningFunction gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to FlGui_test_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help FlGui_test

% Last Modified by GUIDE v2.5 13-Apr-2009 19:22:20

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @FlGui_test_OpeningFcn, ...
                   'gui_OutputFcn',  @FlGui_test_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before FlGui_test is made visible.
function FlGui_test_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to FlGui_test (see VARARGIN)

% Choose default command line output for FlGui_test
handles.output = handles;

% Update handles structure
guidata(hObject, handles);

% load logo image
logoPIC=imread('images/fslogo.jpg');
axes(handles.axes1); image(logoPIC); axis off;
set(get(handles.axes1,'Children'),'ButtonDownFcn','FlMiscGui');

% UIWAIT makes FlGui_test wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = FlGui_test_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on button press in pushbutton1.
function pushbutton1_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global FL

% Shutdown Floodland timer which executes shutdown tasks
try
  AccessRequest('release');
  stop(FL.t_main);
catch
  exit;
end % try/catch



% --- Start app
function pushbutton2_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global FL

try
  % --- Run requested user app
  appList = get(handles.popupmenu1,'String');
  app = appList{get(handles.popupmenu1,'Value')};
  % Run as thread?
  runthread=0;
  % Are there any available/is this wanted?
  if ~exist(['testApps/',app],'dir') || exist(['testApps/',app,'/NOTHREAD'],'file')
    if get(handles.checkbox4,'Value')
      if isfield(FL,'threads') && isfield(FL.threads,'status') && ~isempty(FL.threads.status)
        if any(cellfun(@(x) strcmp(x,'running')))
          runthread=find(cellfun(@(x) strcmp(x,'running')),'first');
        else
          if ~strcmp(questdlg('No available non-busy threads, run in main thread?','No thread available',...
            'Yes','No','No'),'Yes')
            return
          end
        end
      end
    end
  end
  if runthread
    stat = FlThreads('startApp',runthread,app);
    if stat{1}~=1
      if findstr(stat{2},'Can only run .fig or .m file in thread')
        runthread=0; % 3rd party apps run in own thread by definition
      else
        errordlg(stat{2},'Error starting app in thread')
        return
      end
    end
  end
  if ~runthread
    if ~isdeployed; addpath(['testApps/',app]); end;
    if exist(['testApps/',app,'/',app,'.fig'],'file')
      evalc(['FL.Gui.(app)=',app,'(''UserData'',get(handles.figure1,''UserData''))']);
    elseif exist(['testApps/',app,'/',app,'.m'],'file')
      run(['testApps/',app,'/',app]);
    else
      if ~exist(['testApps/',app,'/',app],'file')
        errordlg('Application not found','Run user app error');
        return
      end 
      evalc(['!testApps/',app,'/',app,'&']);
    end 
  end
catch
  errordlg(lasterr,'Error running requested application');
  bufferFlush;
end
% --- Executes on selection change in popupmenu1.
function popupmenu1_Callback(hObject, eventdata, handles)
% hObject    handle to popupmenu1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = get(hObject,'String') returns popupmenu1 contents as cell array
%        contents{get(hObject,'Value')} returns selected item from popupmenu1


% --- Executes during object creation, after setting all properties.
function popupmenu1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to popupmenu1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end




% --- FL server
function pushbutton3_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global FL

% Start/Stop command handler
if isequal(get(hObject,'BackgroundColor'),[1 0 0])
  % Start command handler
  if ~isfield(FL,'t_commandHandler') || ~isequal(FL.t_commandHandler.running,'on')
    ServerCommandHandler('init');
    FL.t_commandHandler=timer('StartDelay',1,'Period',FL.Period_commandHandler,...
    'ExecutionMode','FixedRate','BusyMode','drop');
    FL.t_commandHandler.TimerFcn = 'ServerCommandHandler(''run'',''fl'')';
    FL.t_commandHandler.StopFcn = 'ServerCommandHandler(''stop'',''fl'')';
    start(FL.t_commandHandler);
  end % if command handler timer running
  % Initialise Floodland Server Socket
  sockrw('init-flserv');
  % indicate server running
  set(hObject,'BackgroundColor','Green');
else
  if isfield(FL,'t_commandHandler') && isequal(FL.t_commandHandler.running,'on') 
    stop(FL.t_commandHandler);
  end % if running, stop command handler
  % Stop connection timer if running
  if isfield(FL.Server,'t_connect') && isequal(FL.Server.t_connect.Running,'on')
    stop(FL.Server.t_connect);
  end % if t_connect timer running, stop
  clear ServerConnect
  set(hObject,'BackgroundColor','Red');
  % Send disconnect messages to any connected clients and delete
  disp('closing server connections...')
  srvs={'Server'};
  ssrvs={'fl'};
  socks={'socket' 'servSocket'};
  for isrv=1:length(srvs)
    if isfield(FL,srvs{isrv})
      users=fieldnames(FL.(srvs{isrv}));
      for iuser=1:length(users)
        if strcmp(users{iuser}(1:4),'user')
          for isock=1:length(socks)
            if isfield(FL.(srvs{isrv}).(users{iuser}),socks{isock}) && ...
                ~FL.(srvs{isrv}).(users{iuser}).(socks{isock}).isClosed
              try
                % Send disconnect message and close socket
                sockrw('set_user',ssrvs{isrv},users{iuser,1});
                sockrw('writechar',ssrvs{isrv},'disconnect');
                pause(1)
                FL.(srvs{isrv}).(users{iuser}).socket.close;
                FL.(srvs{isrv})=rmfield(FL.(srvs{isrv}),users{iuser});
              catch
                FL.(srvs{isrv})=rmfield(FL.(srvs{isrv}),users{iuser});
                continue
              end % try/catch
            end % if ~closed
          end % for isock
        end % if user field
      end % for iuser
    end % if srv field exists
  end % for isrv
end % if button red



% --- Executes on button press in checkbox1.
function checkbox1_Callback(hObject, eventdata, handles)
% hObject    handle to checkbox1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of checkbox1


% --- Executes on selection change in popupmenu2.
function popupmenu2_Callback(hObject, eventdata, handles)
% hObject    handle to popupmenu2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = get(hObject,'String') returns popupmenu2 contents as cell array
%        contents{get(hObject,'Value')} returns selected item from popupmenu2


% --- Executes during object creation, after setting all properties.
function popupmenu2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to popupmenu2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in pushbutton4.
function pushbutton4_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
persistent lastTime
% Slow down rate at which can click update button, otherwise crashes
if ~get(handles.checkbox1,'Value') && (isempty(lastTime) || (toc-lastTime)>3)
  lastTime=toc;
  FlHwUpdate;
end % update only if not already auto-updating


% --- Executes on button press in checkbox2.
function checkbox2_Callback(hObject, eventdata, handles)
% hObject    handle to checkbox2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of checkbox2




% --- Executes on button press in pushbutton5.
function pushbutton5_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton5 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

if isequal(questdlg('Clear all PV access requests?'),'Yes')
  AccessRequest('release');
end % if sure



% --- Executes when user attempts to close figure1.
function figure1_CloseRequestFcn(hObject, eventdata, handles)
% hObject    handle to figure1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: delete(hObject) closes the figure
% Shutdown Floodland timer which executes shutdown tasks
global FL
try
  AccessRequest('release');
  stop(FL.t_main);
catch
  exit;
end % try/catch





% --- Executes on button press in pushbutton6.
function pushbutton6_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton6 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global FL

FL.Gui.BeamlineViewer = BeamlineViewer;



% --- Executes on mouse press over axes background.
function axes1_ButtonDownFcn(hObject, eventdata, handles)
% hObject    handle to axes1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
msgbox('Do not click here again...')



% --- Safely delete an open figure/gui
function pushbutton7_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton7 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)




% --- Thread Control
function pushbutton8_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton8 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global FL

FL.Gui.threadControl=threadControl;

% --- Executes on button press in checkbox4.
function checkbox4_Callback(hObject, eventdata, handles)
% hObject    handle to checkbox4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of checkbox4




% --- Executes on button press in pushbutton9.
function pushbutton9_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton9 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
resp=questdlg('Warning- will potentially interfere with any running client applications, continue?','Flush Comms Buffers?','Yes','No','No');
if strcmp(resp,'Yes')
  stat=bufferFlush;
  if stat{1}~=1
    errordlg(stat{2},'Buffer Flush Error')
  else
    beep
  end
end




% --- Launch BPM tool
function pushbutton10_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton10 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global FL
FL.Gui.FlBpmTool=FlBpmTool;

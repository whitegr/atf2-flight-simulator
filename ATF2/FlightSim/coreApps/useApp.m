function [stat whichapp] = useApp(appName)
% stat = useApp(appName)
% Add application appName to search path
global FL
stat{1}=1;whichapp=[];
if ~ischar(appName)
  stat{1}=-1;
  stat{2}='Need to pass char as appName';
  return
end % if not char appName

if isdeployed
  return
end

% Add app directory contents to search path
if exist(['testApps/',appName],'dir')
  whichapp='test';
  if isempty(findstr(appName,path))
    if isempty(findstr(path,['testApps/',appName]))
    if ~isdeployed; addpath(['testApps/',appName]); end;
    end
  end % if not already on path
elseif exist(['trustedApps/',appName],'dir')
  whichapp='trusted';
  if isempty(findstr(appName,path))
    if isempty(findstr(path,['trustedApps/',appName]))
    if ~isdeployed; addpath(['trustedApps/',appName]); end;
    end
  end %
elseif isequal(FL.mode,'test')
  stat=sockrw('writechar','cas',['useApp(',appName,')']);
  if stat{1}~=1; return; end;
  stat=sockrw('readchar','cas',5);
  if stat{1}~=1; return; end;
else
  stat{1}=0;
  stat{2}='App not found';
end % if app exists

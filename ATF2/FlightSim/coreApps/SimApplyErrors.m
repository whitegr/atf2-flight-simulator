function [stat, Beam] = SimApplyErrors(type,cmd)

global FL BEAMLINE PS INSTR
stat{1}=1;

switch lower(type)
  case 'static'
    if exist('cmd','var')
      if ~isequal(lower(cmd),'zero')
        stat{1}=-1; stat{2}='Only supported second parameter is ''zero''';
        return
      end % if cmd not 'zero'
      stat = ErrorGroupGaussErrors( FL.SimModel.magGroup.Quad.OffUse, zeros(1,6), [0 0 0 0 0 0], zeros(1,6), FL.SimModel.errSig );
      if stat{1}~=1; return; end;
      stat = ErrorGroupGaussErrors( FL.SimModel.magGroup.Sext.Off, zeros(1,6), [0 0 0 0 0 0], zeros(1,6), FL.SimModel.errSig );
      if stat{1}~=1; return; end;
      stat = ErrorGroupGaussErrors( FL.SimModel.magGroup.SBEN.Off, zeros(1,6), [0 0 0 0 0 0], zeros(1,6), FL.SimModel.errSig );
      if stat{1}~=1; return; end;
      stat = ErrorGroupGaussErrors( FL.SimModel.magGroup.Quad.dB, 0, 0, 0, FL.SimModel.errSig );
      if stat{1}~=1; return; end;
      stat = ErrorGroupGaussErrors( FL.SimModel.magGroup.Sext.dB, 0, 0, 0, FL.SimModel.errSig );
      if stat{1}~=1; return; end;
      stat = ErrorGroupGaussErrors( FL.SimModel.magGroup.SBEN.dB, 0, 0, 0, FL.SimModel.errSig );
      if stat{1}~=1; return; end;
    else
      stat = ErrorGroupGaussErrors( FL.SimModel.magGroup.Quad.OffUse, zeros(1,6), ...
        [FL.SimModel.align.x 0 FL.SimModel.align.y 0 FL.SimModel.align.z FL.SimModel.align.rot], zeros(1,6), FL.SimModel.errSig );
      if stat{1}~=1; return; end;
      stat = ErrorGroupGaussErrors( FL.SimModel.magGroup.Sext.Off, zeros(1,6), ...
        [FL.SimModel.align.x 0 FL.SimModel.align.y 0 FL.SimModel.align.z FL.SimModel.align.rot], zeros(1,6), FL.SimModel.errSig );
      if stat{1}~=1; return; end;
      stat = ErrorGroupGaussErrors( FL.SimModel.magGroup.SBEN.Off, zeros(1,6), ...
        [FL.SimModel.align.x 0 FL.SimModel.align.y 0 FL.SimModel.align.z FL.SimModel.align.rot], zeros(1,6), FL.SimModel.errSig );
      if stat{1}~=1; return; end;
      stat = ErrorGroupGaussErrors( FL.SimModel.magGroup.Quad.dB, 0, FL.SimModel.align.dB, 0, FL.SimModel.errSig );
      if stat{1}~=1; return; end;
      stat = ErrorGroupGaussErrors( FL.SimModel.magGroup.Sext.dB, 0, FL.SimModel.align.dB, 0, FL.SimModel.errSig );
      if stat{1}~=1; return; end;
      stat = ErrorGroupGaussErrors( FL.SimModel.magGroup.SBEN.dB, 0, FL.SimModel.align.dB, 0, FL.SimModel.errSig );
      if stat{1}~=1; return; end;
      sysError=FL.SimModel.align.dB_syst*randnt(FL.SimModel.errSig);
      mags=[FL.SimModel.magGroup.Quad.dB.ClusterList.index FL.SimModel.magGroup.Sext.dB.ClusterList.index ...
        FL.SimModel.magGroup.SBEN.dB.ClusterList.index];
      for iMag=mags
        BEAMLINE{iMag}.dB=BEAMLINE{iMag}.dB+sysError;
      end
    end % if cmd given
  case 'dynamic'
    if ~exist('cmd','var') || ~isfield(cmd,'Bunch') % apply errors that don't need updating every pulse
      [FL.SimModel, INSTR] = CreateINSTR(FL.SimModel,[],INSTR) ; % update bpm resolutions etc
      % Init ground motion model if using
      if ~isequal(lower(FL.SimModel.align.gmModel),'none')
        FL.SimModel.gmModel=FL.SimModel.align.gmModel;
        FL.SimModel.jitter.quad=FL.SimModel.align.jitter_quad;
        FL.SimModel.jitter.sext=FL.SimModel.align.jitter_sext;
        FL.SimModel.time=0;
        gmMove(FL.SimModel);
      end % if using GM model
      % Corrector errors
      cors=[findcells(BEAMLINE,'Class','XCOR') findcells(BEAMLINE,'Class','YCOR')];
      for icor=1:length(cors)
        PS(BEAMLINE{cors(icor)}.PS).dAmpl = FL.SimModel.align.cor_dAmpl ;
        PS(BEAMLINE{cors(icor)}.PS).step = FL.SimModel.align.cor_psStep ;
      end % for icor
      return
    else % Errors requiring pulse-pulse setting
      mags={[FL.SimModel.magGroup.Quad.dB.ClusterList.index] [FL.SimModel.magGroup.Sext.dB.ClusterList.index] ...
        [FL.SimModel.magGroup.SBEN.dB.ClusterList.index] [FL.SimModel.magGroup.XCOR.dB.ClusterList.index ...
        FL.SimModel.magGroup.YCOR.dB.ClusterList.index]};
      % Magnet strength jitter
      names={'dB_quad_dyn' 'dB_sext_dyn' 'dB_bend_dyn' 'dB_cor_dyn'};
      for iMag=1:length(mags)
        for iele=mags{iMag}
          BEAMLINE{iele}.dB=BEAMLINE{iele}.dB+BEAMLINE{iele}.B*PS(BEAMLINE{iele}.PS).Ampl*FL.SimModel.align.(names{iMag})*randn;
        end % for iele
      end
      % PS jitter
      for iele=mags{end}
        BEAMLINE{iele}.dB=BEAMLINE{iele}.dB+BEAMLINE{iele}.B*PS(BEAMLINE{iele}.PS).Ampl*FL.SimModel.align.dPSAmpl*randn;
      end % for iele
      % Ground Motion
      if ~isequal(lower(FL.SimModel.align.gmModel),'none')
        FL.SimModel.time=FL.SimModel.time+1/FL.accRate;
        gmMove(FL.SimModel);
      end % if using GM model
      % "Incoming" Beam jitter
      Beam=cmd;
      Beam.Bunch.x(1,:)=cmd.Bunch.x(1,:)+FL.SimModel.align.incoming_x*randn;
      Beam.Bunch.x(2,:)=cmd.Bunch.x(2,:)+FL.SimModel.align.incoming_xp*randn;
      Beam.Bunch.x(3,:)=cmd.Bunch.x(3,:)+FL.SimModel.align.incoming_y*randn;
      Beam.Bunch.x(4,:)=cmd.Bunch.x(4,:)+FL.SimModel.align.incoming_yp*randn;
      Beam.Bunch.x(6,:)=cmd.Bunch.x(6,:)+FL.SimModel.align.incoming_e*randn;
    end % if Bunch passed
  otherwise
    stat{1}=-1;
    stat{2}='must pass ''static'' or ''dynamic'' to SimApplyErrors';
    return
end % switch type
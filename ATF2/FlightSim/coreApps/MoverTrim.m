function stat = MoverTrim( girdlist, onlineCheck )
% MOVERTRIM Set girder mover actual values to desired values
% Floodland Implementation - write to EPICS CA
% onlineCheck: 1 = Move mover, blocking
%              2 = Move mover, non-blocking
%              3 = Trim mover, blocking
%              4 = Trim mover, non-blocking

global GIRDER FL

stat{1}=1;
% Arg checks
if max(girdlist)>length(GIRDER) || any(girdlist<=0)
  stat{1}=0;
  stat{2}='GIRDER index out of range';
  return
end % ps range check

% Are we using online version
if ~exist('onlineCheck','var') || ~onlineCheck || (~isempty(FL) && isfield(FL,'SimMode') && FL.SimMode)
%   warning('MATLAB:MoverTrim_online','Online version of MoverTrim without onlineCheck set-> performing offline MoverTrim function') ;
  for count = 1:length(girdlist) 
    girdno = girdlist(count) ;
    if (girdno > length(GIRDER))
        continue ;
    end % if girdno > length GIRDER
    if (~isfield(GIRDER{girdno},'Mover'))
      stat{2} = 'Girder without mover found in MoverTrim' ;
      stat{1} = -1 ;
      continue ;
    end % if Mover field
    for count2 = 1:length(GIRDER{girdno}.Mover)
      if (GIRDER{girdno}.MoverStep(count2) == 0) 
         GIRDER{girdno}.MoverPos(count2) = ...
         GIRDER{girdno}.MoverSetPt(count2)     ;
      else
        nstep = round( (GIRDER{girdno}.MoverSetPt(count2) - ...
                        GIRDER{girdno}.MoverPos(count2)        ) / ...
                        GIRDER{girdno}.MoverStep(count2)            ) ;
        GIRDER{girdno}.MoverPos(count2) = GIRDER{girdno}.MoverPos(count2) + ...
            nstep * GIRDER{girdno}.MoverStep(count2) ;
      end % if +ve move
    end % for count2
  end % for count
  return
end % online check

% check Movers exist for requested GIRDERS
if sum(cellfun(@(x) isfield(x,'Mover'),{GIRDER{girdlist}})) ~= length(girdlist) %#ok<*CCAT1>
  stat{2} = 'Girder without mover found in MoverTrim' ;
  stat{1} = -1 ;
end

% --- If "test", then send PSTrim command to "trusted"
if ~isequal(FL.mode,'trusted')
  cas_arg=zeros(1,length(girdlist)*7);
  for imov=1:length(girdlist)
    cas_arg(1+(imov-1)*7)=girdlist(imov);
    cas_arg(find(ismember(1:6,GIRDER{girdlist(imov)}.Mover))+1+(imov-1)*7)=GIRDER{girdlist(imov)}.MoverSetPt;
  end % for imov
  cas_cmd=['movertrim: ',num2str(cas_arg,FL.comPrec)];
  stat=sockrw('writechar','cas',cas_cmd); if stat{1}<0; return; end;
  [stat, casout]=sockrw('readchar','cas',120); if stat{1}<0; return; end;
  if isnan(str2double(casout{1})) || str2double(casout{1})~=1
    stat{1}=-1; stat{2}=cell2mat(casout);
  end % if stat=1 not returned
  return
end % if not "trusted"

% Get GIRDERS in PV list
comstack={}; comstackVals=[]; postcom={}; postcomVal=[];
for ig=girdlist
  % Check value validity
  if any(isnan(GIRDER{ig}.MoverSetPt)) || any(isinf(GIRDER{ig}.MoverSetPt))
    error('Invalid Mover SetPt for GIRDER: %d',ig)
  end % if isnan or isinf
  hw=FL.HwInfo.GIRDER(ig);
  if isempty(hw.pvname) || isequal(hw.conv,0) || isempty(hw.conv)
    continue
  end % continue if nothing to do for this PS
  % Add command to stack
  if isfield(hw,'preCommand') && ~isempty(hw.preCommand) && ~isempty(hw.preCommand{2})
    comstack{end+1}=hw.preCommand{2}{1};
    comstackVals(end+1)=hw.preCommand{2}{2};
  end % if precommand
  % Write out move taking into account conversion factor
  for idim=1:length(GIRDER{ig}.MoverSetPt)
    comstack{end+1}=hw.pvname{2}{idim};
    comstackVals(end+1)=GIRDER{ig}.MoverSetPt(idim)/hw.conv(idim);
  end
  if isfield(hw,'postCommand') && ~isempty(hw.postCommand) && ~isempty(hw.postCommand{2})
    if islogical(onlineCheck); onlineCheck=1; end;
    if onlineCheck>2; istrim=2; else istrim=1; end;
    postcom{end+1}=hw.postCommand{2,1}{istrim,1};
    postcomVal(end+1)=hw.postCommand{2,1}{istrim,2};
    if ~isempty(hw.postCommand{2,2}{istrim,1})
      postcom{end+1}=hw.postCommand{2,2}{istrim,1};
      postcomVal(end+1)=hw.postCommand{2,2}{istrim,2};
    end
  end % if precommand
end % for ig
% Issue command(s) via channel access client
% Check validity of values
if any(isnan(comstackVals)) || any(isinf(comstackVals))
  error('Invalid MoverSetPt!')
end % if any isnan | isinf

% Check limits
for igir=girdlist
  if isfield(FL,'HwInfo') && isfield(FL.HwInfo,'GIRDER') && isfield(FL.HwInfo.GIRDER,'low')
    if ~isempty(FL.HwInfo.GIRDER(igir).low) && all(FL.HwInfo.GIRDER(igir).low~=0)
      if any(GIRDER{igir}.MoverSetPt < FL.HwInfo.GIRDER(igir).low)
        stat{1}=-1;
        stat{2}=sprintf('Set Point for GIRDER %d less than limit, see FL.HwInfo.GIRDER.low',igir);
        return
      end
      if any(GIRDER{igir}.MoverSetPt > FL.HwInfo.GIRDER(igir).high)
        stat{1}=-1;
        stat{2}=sprintf('Set Point for GIRDER %d greater than limit, see FL.HwInfo.GIRDER.high',igir);
        return
      end
    end
  end
end

% Write values
try
  lcaPut(comstack',comstackVals');
catch
  stat{1}=-1; stat{2}='Controls not available';
  return
end

% Issue move command
if ~isempty(postcom)
  try
    lcaPut(postcom',postcomVal');
  catch
    stat{1}=-1; stat{2}='Controls not available';
    return
  end
end % if postcom

% Move check if blocking
if ~FL.SimMode && ~isempty(strfind(FL.mode,'trusted')) && (onlineCheck==1 || onlineCheck==3)  % blocking
  for igir=1:length(girdlist)
    if onlineCheck==1
      mcheckpv{igir,1}=FL.HwInfo.GIRDER(girdlist(igir)).status{1};
    else
      mcheckpv{igir,1}=FL.HwInfo.GIRDER(girdlist(igir)).status{2};
    end
  end
  t0=clock;
  if onlineCheck==1
    while any(lcaGet(mcheckpv)) && etime(clock,t0)<110
      pause(0.1);
    end % while still moving
  else
    while any(strcmp(lcaGet(mcheckpv),'trimming')) && etime(clock,t0)<110
      pause(0.1);
    end % while still moving
  end
end
function varargout = FlBeamExciterSettings(varargin)
% FLBEAMEXCITERSETTINGS MATLAB code for FlBeamExciterSettings.fig
%      FLBEAMEXCITERSETTINGS, by itself, creates a new FLBEAMEXCITERSETTINGS or raises the existing
%      singleton*.
%
%      H = FLBEAMEXCITERSETTINGS returns the handle to a new FLBEAMEXCITERSETTINGS or the handle to
%      the existing singleton*.
%
%      FLBEAMEXCITERSETTINGS('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in FLBEAMEXCITERSETTINGS.M with the given input arguments.
%
%      FLBEAMEXCITERSETTINGS('Property','Value',...) creates a new FLBEAMEXCITERSETTINGS or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before FlBeamExciterSettings_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to FlBeamExciterSettings_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help FlBeamExciterSettings

% Last Modified by GUIDE v2.5 14-Feb-2011 16:14:22

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @FlBeamExciterSettings_OpeningFcn, ...
                   'gui_OutputFcn',  @FlBeamExciterSettings_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before FlBeamExciterSettings is made visible.
function FlBeamExciterSettings_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to FlBeamExciterSettings (see VARARGIN)

% Choose default command line output for FlBeamExciterSettings
handles.output = handles;

[stat pars]=FlBeamExcite('GetPars');
set(handles.edit1,'String',num2str(pars.ffs_amplitude*1e6))
set(handles.edit2,'String',num2str(pars.nsteps))

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes FlBeamExciterSettings wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = FlBeamExciterSettings_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;



function edit2_Callback(hObject, eventdata, handles) %#ok<*INUSL,*DEFNU>
% hObject    handle to edit2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

val=str2double(get(hObject,'String'));
if isnan(val) || val<10 || val>1000
  [stat pars]=FlBeamExcite('GetPars');
  set(handles.edit1,'String',num2str(pars.nsteps))
else
  FlBeamExcite('setnsteps',val);
end


% --- Executes during object creation, after setting all properties.
function edit2_CreateFcn(hObject, eventdata, handles) %#ok<*INUSD>
% hObject    handle to edit2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit1_Callback(hObject, eventdata, handles)
% hObject    handle to edit1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

val=str2double(get(hObject,'String'));
if isnan(val) || val<0 || val>1000
  [stat pars]=FlBeamExcite('GetPars');
  set(handles.edit1,'String',num2str(pars.ffs_amplitude*1e6))
else
  FlBeamExcite('setffsamp',val/1e6);
end


% --- Executes during object creation, after setting all properties.
function edit1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes when user attempts to close figure1.
function figure1_CloseRequestFcn(hObject, eventdata, handles)
% hObject    handle to figure1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

guiCloseFn('FlBeamExciterSettings',handles);

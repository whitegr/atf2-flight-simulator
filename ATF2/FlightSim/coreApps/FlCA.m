function varargout=FlCA(varargin)
% FLCA
% lcaSetMonitor / lcaNewMonitorValue / lcaGet / lcaPut / lcaPutNoWait with error catching for missing PVs
% If any PVs not found, they are removed from FL.HwInfo hardware list,
% e.g. [vals timestamps] = FlCA('lcaGet','pvname',1)
% Missing values are replaced with NaN's
% Missing PV's are remembered and excluded from future calls
%
%   FlCA('restore',type,ind) or FlCA('restoreAll')
% - restore some or all of pv's previously excluded 
%   e.g. FlCA('restore','PS',33) or FlCA('restore','INSTR',133) or
%   FlCA('restore','GIRDER',11)
%
%   missingPVs = FlCA('pvlist')
%     - Get list of any missing PV's
global FL
persistent removedPV removedPVinfo da

% init
if isempty(removedPV); removedPV={}; end;
if isempty(removedPVinfo); removedPVinfo={}; end;

% Return bad pv list if requested
if strcmp(varargin{1},'pvlist')
  varargout{1}=removedPV;
  return
end

% arg check
allowedCMD={'lcaSetMonitor' 'lcaPut' 'lcaPutNoWait' 'lcaNewMonitorValue' 'lcaGet' 'restore' 'restoreAll' 'lcaNewMonitorWait' ...
            'aidaGet' 'aidaPut'};
if ~ismember(varargin{1},allowedCMD)
  error('Invalid command');
end
if isempty(strfind(varargin{1},'restore')) && ~iscell(varargin{2})
  varargin{2}={varargin{2}};
end

% Deal with restore requests
if strcmp(varargin{1},'restoreAll') || strcmp(varargin{1},'restore')
  if strcmp(varargin{1},'restore') && (nargin<3 || ~ismember(varargin{2},{'PS' 'GIRDER' 'INSTR'}) || ~isnumeric(varargin{3}))
    error('Invalid arguments')
  end
  if strcmp(varargin{1},'restoreAll')
    rlist=1:length(removedPVinfo);
  else
    rlist=find([removedPVinfo{ismember({removedPVinfo{:,1}},varargin{2}),2}]==varargin{3}); 
  end
  for ilist=rlist
    FL.HwInfo.(removedPVinfo{ilist,1})(removedPVinfo{ilist,2}).pvname=removedPVinfo{ilist,3};
    if ~isempty(removedPV)
      for ipv1=1:length(removedPVinfo{ilist,3})
        for ipv2=1:length(removedPVinfo{ilist,3}{ipv1})
          removedPV={removedPV{ismember(removedPV,removedPVinfo{ilist,3}{ipv1}{ipv2})}}; %#ok<*CCAT1>
        end
      end
    end
  end
  if strcmp(varargin{1},'restoreAll')
    removedPV=[];
  end
  sz=size(removedPVinfo);
  nlist=find(~ismember(1:sz(1),rlist));
  if ~isempty(nlist)
    newpv={};
    for ilist=nlist
      newpv{end+1,1}=removedPVinfo{ilist,1};
      newpv{end,2}=removedPVinfo{ilist,2};
      newpv{end,3}=removedPVinfo{ilist,3};
    end
    removedPVinfo=newpv;
  else
    removedPVinfo={};
  end
  return
end

% Deal with channel access request
missingPV=[];
ismissing=true;
% remove any removedPV entries from list
nanout=nan(size(varargin{2}));
newlist=~ismember(varargin{2},removedPV);
pvlist={varargin{2}{newlist}}';
varargout{1}=nanout; varargout{2}=nanout;
if ~any(newlist); return; end;
while ismissing && ~isempty(pvlist)
  try
    if strcmp(varargin{1},'lcaSetMonitor') || strcmp(varargin{1},'lcaPut') || strcmp(varargin{1},'lcaPutNoWait')
      if nargin>2
        evalc([varargin{1},'(pvlist,varargin{3});']);
      else
        evalc([varargin{1},'(pvlist);']);
      end
    elseif strcmp(varargin{1},'lcaNewMonitorValue')
      if nargin>2
        output=eval([varargin{1},'(pvlist,varargin{3});']);
      else
        output=eval([varargin{1},'(pvlist);']);
      end
      nanout(newlist)=output;
      varargout{1}=nanout;
    elseif ~isempty(regexp(varargin{1},'^aida','once'))
      % Initialise aida if not yet done
      if isempty(da)
        aidainit;
        da = DaObject();
      end
      if strcmp(varargin{1},'aidaGet')
        % any repeated PV names, assume one for all
        uniPV=unique(pvlist);
        dataout=NaN(length(pvlist),1);
        for ipv=1:length(uniPV)
          % Request vector of BPM readings
          if ~isempty(regexp,uniPV{ipv},'//BPMS$')
            if nargin<5
              error('Need to pass name list, channel list and control ID for use with getting BPMS')
            end
            aidaNames=varargin{3}; chNames=varargin{4};
            [names X Y Q]=aidaBPM(da,uniPV{ipv},varargin{5});
            for ipv2=find(ismember(pvlist,uniPV))
              iname=find(ismember(names,aidaNames{ipv2}),1);
              if ~isempty(iname) && strcmp(chNames{ipv2},'x')
                dataout(ipv2)=X(iname);
              elseif ~isempty(iname) && strcmp(chNames{ipv2},'y')
                dataout(ipv2)=Y(iname);
              elseif ~isempty(iname) && strcmp(chNames{ipv2},'Q')
                dataout(ipv2)=Q(iname);
              end
            end
          % Request single data channel (scalar double)
          else
            v=da.getDaValue(uniPV{ipv});
            for ipv2=find(ismember(pvlist,uniPV))
              dataout(ipv2)=double(v.get(0));
            end
          end
        end
        nanout2=nanout;
        nanout(newlist)=dataout;
        varargout{1}=nanout;
        nanout2(newlist)=now;
        varargout{2}=nanout2;
      elseif strcmp(varargin{1},'aidaSet')
        import edu.stanford.slac.aida.lib.util.common.*
        if nargin<4
          error('Must supply trim type')
        end
        trimStyle=varargin{4};
        if ~strcmp(trimStyle,'PTRB') && ~strcmp(trimStyle,'TRIM')
          error('trimStyle must be ''PTRB'' or ''TRIM''')
        end
        type=regexp(pvname,'//(.+)$','match','once');
        pvname=regexprep(pvname,'(//.+$)','');
        uType=unique(type);
        uType=uType(~cellfun(@(x) isempty(x),uType));
        for itype=1:length(uType)
          query = sprintf('MAGNETSET%s',uType{itype});
          da.setParam('MAGFUNC', trimStyle);
          stringsParam = pvname(ismember(type,uType));
          setValues = varargin{3}(ismember(type,uType));
          daV=DaValue;
          daV.type=0;
          daV.addElement(DaValue(stringsParam));
          daV.addElement(DaValue(setValues));
          da.setDaValue(query, daV);
        end
      end
      da.reset();
    else
      if nargin>2
        [output1 output2]=eval([varargin{1},'(pvlist,varargin{3});']);
      else
        [output1 output2]=eval([varargin{1},'(pvlist);']);
      end
      nanout2=nanout;
      nanout(newlist)=output1;
      nanout2(newlist)=output2;
      varargout{1}=nanout; varargout{2}=nanout2;
    end
    ismissing=false;
  catch ME
    if strfind(ME.message,'no monitor')
      lcaSetMonitor(pvname);
      return
    end
    FL.lastError=ME;
    pvname=regexp(ME.message,'variable : (.+)','tokens','once');
    if isempty(pvname)
      pvname=regexp(ME.message,'PV ''(.+)''','tokens','once');
    end
    if isempty(pvname)
      pvname=regexp(ME.message,'currently connected : (.+)','tokens','once');
    end
    if isempty(pvname)
      warning('Lucretia:Floodland:FlCA','FlCA: could not extract PV name');
      return
    end
    missingPV=unique([find(ismember(varargin{2},pvname{1}))' missingPV]);
    removedPV{end+1}=pvname{1};
    newlist(missingPV)=false;
    pvlist={varargin{2}{newlist}}'; 
    disp(['Missing PV: ',pvname{1}]);
    ismissing=true;
  end
end
if ~isempty(missingPV)
  rmlist={varargin{2}{missingPV}};
  % remove pvname entry
  types={'PS' 'GIRDER' 'INSTR'};
  for itype=1:length(types)
    for ind=1:length(FL.HwInfo.(types{itype}))
      wasbad=false;
      if ~isempty(FL.HwInfo.(types{itype})(ind).pvname)
        for ipv=1:length(FL.HwInfo.(types{itype})(ind).pvname)
          for ipv2=1:length(FL.HwInfo.(types{itype})(ind).pvname{ipv})
            if ismember(FL.HwInfo.(types{itype})(ind).pvname{ipv}{ipv2},rmlist)
%               rmlist={rmlist{ismember(rmlist,FL.HwInfo.(types{itype})(ind).pvname{ipv}{ipv2})}};
              removedPVinfo{end+1,1}=types{itype};
              removedPVinfo{end,2}=ind;
              removedPVinfo{end,3}=FL.HwInfo.(types{itype})(ind).pvname;
              wasbad=true;
            end
          end
        end
      end
      if wasbad
        FL.HwInfo.(types{itype})(ind).badpv=FL.HwInfo.(types{itype})(ind).pvname;
        FL.HwInfo.(types{itype})(ind).pvname=[];
      end
    end
  end
end

% --- AIDA BPM Acqisition function ---
function [names X Y Q]=aidaBPM(da,str,bpmd)
    
import edu.stanford.slac.aida.lib.util.common.*
import java.util.Vector

% AIDA readback status bits
% Check STAT_GOOD & ~(STAT_BAD | STAT_OFF)
HSTA_XONLY = 64;    % 0x00000040
HSTA_YONLY = 128;   % 0x00000080
STAT_GOOD  = 1;     % 0x00000001
% STAT_OK    = 2;     % 0x00000002
STAT_OFF   = 8;     % 0x00000008
STAT_BAD   = 256;   % 0x00000100

da.setParam(sprintf('N=%d',1));
da.setParam(sprintf('BPMD=%d',bpmd));
v = da.getDaValue(str);
names = Vector(v.get(0));
tmits = Vector(v.get(4));
hstas = Vector(v.get(5));
stats = Vector(v.get(6));
xvals = Vector(v.get(1));
yvals = Vector(v.get(2));

nsize=names.size;
names=cell(nsize,1);
X=NaN(nsize,1);
Y=X;
Q=X;
for iele = 1:nsize
  names{iele} = char(names.elementAt(iele-1));
  X(iele) = double(xvals.elementAt(iele-1));
  Y(iele) = double(yvals.elementAt(iele-1));
  Q(iele) = double(tmits.elementAt(iele-1));
  % flag bpm readings as bad by setting to NaN
  if ~(bitand( uint32(stats.elementAt(iele-1)),uint32(STAT_GOOD) ) > 0) && ...
          ( bitand( uint32(stats.elementAt(iele-1)),uint32(STAT_OFF) ) > 0 || ...
          bitand( uint32(stats.elementAt(iele-1)),uint32(STAT_BAD) ) > 0 )
    X(iele)=NaN;
    Y(iele)=NaN;
    Q(iele)=NaN;
  end % Good bpm check
  % Check for y-only or x-only bpms
  if (bitand(uint32(hstas.elementAt(iele-1)),uint32(HSTA_YONLY)) ~= 0)
    X(iele)=NaN;
  end % if y-only
  if (bitand(uint32(hstas.elementAt(iele-1)),uint32(HSTA_XONLY)) ~= 0)
    Y(iele)=NaN;
  end % if y-only
end % for iele


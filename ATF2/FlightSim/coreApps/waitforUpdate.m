function waitforUpdate(type)
% Wait for timestamp to update in requested data type (none = wait for all)
global FL

timeout=30; % s

if ~exist('type','var') || isempty(type)
  type='all';
end % arg check

if ~isfield(FL,'PV') || isempty(FL.PV)
  return
end % nothing to do if no PV's

if ~isequal(type,'all')
  list=findcells(FL.PV,upper(type));
else
  list=1:length(FL.PV);
end % if all or given type

for ilist=1:length(list)
  if isfield(FL.PV{list(ilist)},'TimeStamp')
    t0(ilist)=FL.PV{list(ilist)}.TimeStamp;
  else
    t0(ilist)=0;
  end % if ts field
end % for ilist

t1=t0;
tic;
while any(t1==t0)
  for ilist=1:length(list)
    if isfield(FL.PV{list(ilist)},'TimeStamp')
      t1(ilist)=FL.PV{list(ilist)}.TimeStamp;
    else
      t1(ilist)=0;
    end % if ts field
  end % for ilist
  if toc>timeout
    error(['TimeOut waiting for update: ',type]);
  end % if timeout
  pause(0.2);
end % while any not updated
function handles=guiCloseFn(guiName,handles)
% handles=guiCloseFn(guiName,handles)
% Close gui and remove from Floodland GUI list
% Save gui data before deletion

global FL

try
  % save gui data
  guiData=handle2struct(handles.figure1); %#ok<NASGU>
  save(fullfile('userData','guiData',guiName),'guiData');
  % remove gui field (and any associated)
  if isfield(FL,'Gui') && isfield(FL.Gui,guiName)
    FL.Gui=rmfield(FL.Gui,guiName);
  end % remove gui field if there
  gf=fieldnames(FL.Gui);
  for igf=1:length(gf)
    if ~isempty(regexp(gf{igf},['^',guiName,'_'], 'once'))
      FL.Gui=rmfield(FL.Gui,gf{igf});
    end
  end
  % delete gui
  delete(handles.figure1);
catch
  delete(handles.figure1);
end
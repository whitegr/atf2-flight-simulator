function [stat xlim ylim rotlim] = FlSetMoverLimits(igir)
% FLSETMOVERLIMITS
%   Set dynamic high/low limits for given Mover (provide GIRDER index igir)
global GIRDER FL BEAMLINE
persistent camsettings lastgir

stat{1}=1;

% Mover CAM constants
if isempty(camsettings) || length(camsettings)<igir || isempty(camsettings(igir).a)
  camsettings=struct('a',[],'c',[],'R',[],'L',[],'S1',[],'S2',[],'b',[]);
  for iele=GIRDER{igir}.Element(1):GIRDER{igir}.Element(end)
    if isfield(BEAMLINE{iele},'B')
      cs=getMoverConstants(BEAMLINE{iele}.Name);
      if ~isempty(cs)
        camsettings(igir)=cs;
      end
    end
  end
end

% Argument check
igir=round(igir);
if igir<1 || igir>length(GIRDER) || ~isfield(GIRDER{igir},'Mover') 
  stat{1}=-1;
  stat{2}='No MOVER on this GIRDER or invalid index';
  return
end
  
% Get Mover location
xmov = GIRDER{igir}.MoverPos(1);
ymov = GIRDER{igir}.MoverPos(2);
rotmov = GIRDER{igir}.MoverPos(3);
try
  if length(lastgir)>=igir
    if ~any(abs(lastgir{igir}-[xmov ymov rotmov])>15e-6)
      return
    end
  end
catch
  return
end
lastgir{igir}=[xmov ymov rotmov];
% betaminus = pi/4 - rotmov;
% betaplus  = pi/4 + rotmov;

% Get magnet type on Mover
camset=camsettings(igir);

% x_1 = xmov + (camset.a*cos(rotmov)) + (camset.b*sin(rotmov)) - camset.a;
% y_1 = ymov - (camset.b*cos(rotmov)) + (camset.a*sin(rotmov)) + camset.c;

% Compute x limits
% x1max(1) = ((camset.L + y_1*cos(rotmov) - camset.c + camset.b) / sin(rotmov)) - camset.S2 ;
% x1max(2) = ((camset.L - y_1*cos(betaminus) + camset.R) / sin(betaminus)) - camset.S1 ;
% x1max(3) = ((camset.L + y_1*cos(betaplus) - camset.R) / sin(betaplus)) - camset.S1 ;
% xmax = min(abs(x1max - camset.a*cos(rotmov) - camset.b*sin(rotmov) + camset.a));
xlim=[-2.5e-3 2.5e-3];
dx=xmov+(0:-1e-5:-2.5e-3);
for xtry=1:length(dx)
  camangles=camcalc(dx(xtry),ymov,rotmov,camset);
  if ~isreal(camangles) || any(abs(camangles)>pi/2)
    if xtry>1
      xlim(1)=dx(xtry-1);
    else
      xlim(1)=xmov;
    end
    break
  end
end
dx=xmov+(0:1e-5:2.5e-3);
for xtry=1:length(dx)
  camangles=camcalc(dx(xtry),ymov,rotmov,camset);
  if ~isreal(camangles) || any(abs(camangles)>pi/2)
    if xtry>1
      xlim(2)=dx(xtry-1);
    else
      xlim(2)=xmov;
    end
    break
  end
end

% Compute y limits
% y1max(1) = ((x_1+camset.S2)*sin(rotmov) - camset.L + camset.c - camset.b) / cos(rotmov) ;
% y1max(2) = (camset.L - (x_1+camset.S1)*sin(betaminus) + camset.R ) / cos(betaminus) ;
% y1max(3) = ((x_1-camset.S1)*sin(betaplus) + camset.R - camset.L) / cos(betaplus) ;
% ymax = min(abs(y1max + camset.b*cos(rotmov) - camset.a*sin(rotmov) - camset.c)) ;
ylim=[-2e-3 2e-3];
dy=ymov+(0:-1e-5:-2e-3);
for ytry=1:length(dy)
  camangles=camcalc(xmov,dy(ytry),rotmov,camset);
  if ~isreal(camangles) || any(abs(camangles)>pi/2)
    if ytry>1
      ylim(1)=dy(ytry-1);
    else
      ylim(1)=ymov;
    end
    break
  end
end
dy=ymov+(0:1e-5:2e-3);
for ytry=1:length(dy)
  camangles=camcalc(xmov,dy(ytry),rotmov,camset);
  if ~isreal(camangles) || any(abs(camangles)>pi/2)
    if ytry>1
      ylim(2)=dy(ytry-1);
    else
      ylim(2)=ymov;
    end
    break
  end
end

% Compute roll limits
% rotmax(1) = abs(fminbnd(@(x) minth1(x,camset,xmov,ymov),-0.03,0.03,optimset('TolX',1e-6,'TolFun',1e-12,'Display','off')));
% rotmax(2) = abs(fminbnd(@(x) minth2(x,camset,xmov,ymov),-0.03,0.03,optimset('TolX',1e-6,'TolFun',1e-12,'Display','off')));
% rotmax(3) = abs(fminbnd(@(x) minth3(x,camset,xmov,ymov),-0.03,0.03,optimset('TolX',1e-6,'TolFun',1e-12,'Display','off')));
% rotmax = min(rotmax) ;
rotlim=[-10e-3 10e-3];
dt=rotmov+(0:-10e-6:-10e-3);
for ttry=1:length(dt)
  camangles=camcalc(xmov,ymov,dt(ttry),camset);
  if ~isreal(camangles) || any(abs(camangles)>pi/2)
    if ttry>1
      rotlim(1)=dt(ttry-1);
    else
      rotlim(1)=rotmov;
    end
    break
  end
end
dt=rotmov+(0:10e-6:10e-3);
for ttry=1:length(dt)
  camangles=camcalc(xmov,ymov,dt(ttry),camset);
  if ~isreal(camangles) || any(abs(camangles)>pi/2)
    if ttry>1
      rotlim(2)=dt(ttry-1);
    else
      rotlim(2)=rotmov;
    end
    break
  end
end

% Set Limits
FL.HwInfo.GIRDER(igir).low=[xlim(1) ylim(1) rotlim(1)];
FL.HwInfo.GIRDER(igir).high=[xlim(2) ylim(2) rotlim(2)];

% function chi2 = minth1(th,c,x,y)
% x1 = x + (c.a*cos(th)) + (c.b*sin(th)) - c.a;
% y1 = y - (c.b*cos(th)) + (c.a*sin(th)) + c.c;
% chi2=(1/c.L) * ( (x1 + c.S2)*sin(th) - y1*cos(th) + c.c - c.b ) - 1 ;
% chi2 = chi2^2;
% 
% function chi2 = minth2(th,c,x,y)
% x1 = x + (c.a*cos(th)) + (c.b*sin(th)) - c.a;
% y1 = y - (c.b*cos(th)) + (c.a*sin(th)) + c.c;
% betaminus = pi/4 - th;
% chi2=(1/c.L) * ( (x1+c.S1)*sin(betaminus) + y1*cos(betaminus) - c.R ) - 1 ;
% chi2=chi2^2;
% 
% function chi2 = minth3(th,c,x,y)
% x1 = x + (c.a*cos(th)) + (c.b*sin(th)) - c.a;
% y1 = y - (c.b*cos(th)) + (c.a*sin(th)) + c.c;
% betaplus  = pi/4 + th;
% chi2 = (1/c.L) * ( (x1 - c.S1)*sin(betaplus) - y1*cos(betaplus) + c.R ) - 1 ;
% chi2=chi2^2;

function camangles=camcalc(xmov,ymov,rotmov,camset)
betaminus = pi/4 - rotmov;
betaplus  = pi/4 + rotmov;
x_1 = xmov + (camset.a*cos(rotmov)) + (camset.b*sin(rotmov)) - camset.a;
y_1 = ymov - (camset.b*cos(rotmov)) + (camset.a*sin(rotmov)) + camset.c;
camangles(1) = rotmov - asin((1/camset.L) * ((x_1+camset.S2)*sin(rotmov) - y_1*cos(rotmov) + (camset.c - camset.b)));
camangles(2) = rotmov - asin((1/camset.L) * ((x_1+camset.S1)*sin(betaminus) + y_1*cos(betaminus) - camset.R));
camangles(3) = rotmov - asin((1/camset.L) * ((x_1-camset.S1)*sin(betaplus) - y_1*cos(betaplus) + camset.R));

function cs=getMoverConstants(magName)

cs=[];
switch magName
  % [bore:offsX bore:height cam:radius cam:lift offsX(3) offsX(1)]
  case 'QM16FF'
    csval=[0.1431 0.449075 0.031 0.0015875 0.034925 0.2905];
  case 'QM15FF'
    csval=[0.1451 0.456575 0.031 0.0015875 0.034925 0.2905];
  case 'QM14FF'
    csval=[0.1431 0.454575 0.031 0.0015875 0.034925 0.2905];
  case 'QM13FF'
    csval=[0.1466 0.449575 0.0031 0.0015875 0.034925 0.2905];
  case 'QM12FF'
    csval=[149100 4.53575e+05 3.10000e+04 1.58750e+03 3.49250e+04 2.90500e+05].*1e-6;
  case 'QM11FF'
    csval=[145100 4.56075e+05 3.10000e+04 1.58750e+03 3.49250e+04 2.90500e+05].*1e-6;
  case 'QD10BFF'
    csval=[147600 4.54075e+05 3.10000e+04 1.58750e+03 3.49250e+04 2.90500e+05].*1e-6;
  case 'QD10AFF'
    csval=[147100 4.49075e+05 3.10000e+04 1.58750e+03 3.49250e+04 2.90500e+05].*1e-6;
  case 'QF9BFF'
    csval=[146100 4.56075e+05 3.10000e+04 1.58750e+03 3.49250e+04 2.90500e+05].*1e-6;
  case 'SF6FF'
    csval=[141275 4.56575e+05 3.10000e+04 1.58750e+03 3.49250e+04 2.90500e+05].*1e-6;
  case 'QF9AFF'
    csval=[146600 4.49575e+05 3.10000e+04 1.58750e+03 3.49250e+04 2.90500e+05].*1e-6;
  case 'QD8FF'
    csval=[140850 4.52075e+05 3.10000e+04 1.58750e+03 3.49250e+04 2.90500e+05].*1e-6;
  case 'QF7FF'
    csval=[141100 4.50075e+05 3.10000e+04 1.58750e+03 3.49250e+04 2.90500e+05].*1e-6;
  case 'QD6FF'
    csval=[142600 4.47075e+05 3.10000e+04 1.58750e+03 3.49250e+04 2.90500e+05].*1e-6;
  case 'QF5BFF'
    csval=[140100 4.54575e+05 3.10000e+04 1.58750e+03 3.49250e+04 2.90500e+05].*1e-6;
  case 'SF5FF'
    csval=[148025 4.52575e+05 3.10000e+04 1.58750e+03 3.49250e+04 2.90500e+05].*1e-6;
  case 'QF5AFF'
    csval=[142100 4.49075e+05 3.10000e+04 1.58750e+03 3.49250e+04 2.90500e+05].*1e-6;
  case 'QD4BFF'
    csval=[142600 4.54075e+05 3.10000e+04 1.58750e+03 3.49250e+04 2.90500e+05].*1e-6;
  case 'SD4FF'
    csval=[147775 4.55575e+05 3.10000e+04 1.58750e+03 3.49250e+04 2.90500e+05].*1e-6;
  case 'QD4AFF'
    csval=[143600 4.50075e+05 3.10000e+04 1.58750e+03 3.49250e+04 2.90500e+05].*1e-6;
  case 'QF3FF'
    csval=[125600 4.55575e+05 3.10000e+04 1.58750e+03 3.49250e+04 2.90500e+05].*1e-6;
  case 'QD2BFF'
    csval=[129850 4.58575e+05 3.10000e+04 1.58750e+03 3.49250e+04 2.90500e+05].*1e-6;
  case 'QD2AFF'
    csval=[130100 4.50325e+05 3.10000e+04 1.58750e+03 3.49250e+04 2.90500e+05].*1e-6;
  case 'SF1FF'
    csval=[145800 2.11575e+05 3.10000e+04 1.58750e+03 3.49250e+04 2.90500e+05].*1e-6;
  case 'QF1FF'
    csval=[222120 1.56905e+05 3.10000e+04 1.58750e+03 3.49250e+04 2.90500e+05].*1e-6;
  case 'SD0FF'
    csval=[146800 2.17575e+05 3.10000e+04 1.58750e+03 3.49250e+04 2.90500e+05].*1e-6;
  case 'QD0FF'
    csval=[222620 1.58405e+05 3.10000e+04 1.58750e+03 3.49250e+04 2.90500e+05].*1e-6;
  otherwise
    return
end
cs.a = csval(1);
cs.c = csval(2);
cs.R = csval(3);
cs.L = csval(4);
cs.S1 = csval(5);
cs.S2 = csval(6);
cs.b = cs.c + cs.S1 - (sqrt(2)*cs.R);
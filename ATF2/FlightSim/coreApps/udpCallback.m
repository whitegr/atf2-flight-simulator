function udpCallback(obj, event, type) %#ok<INUSL>
% Callback function for use with udp objects in Floodland environment

global FL
FL.udpCallerID=get(obj,'DatagramAddress');
FL.udpReadback=fscanf(obj);
if exist('type','var') && isequal(type,'server')
  fprintf(obj,'readsuccess');
  pause(2);
end % if server- acknowledge response
function [stat output] = FlArchive(varargin)
% Archiving and restoring of Floodland data
% (Needs ioc-atf and Channel Archiver data server running)
%
% stat = standard Lucretia status return
%
% FlArchive OR FlArchive('init')
%
%   Initialise Archive parameters to defaults
%
% [stat pars] = FlArchive('GetPars')
%
%   Get internal parameters structure
%   pars.method = 'mfile' OR 'epics'
%   pars.dirs{2} = epics (1) or mfile (2) data directory
%   pars.dataServerURL = url of data server
%
% stat = FlArchive('SetPars',pars)
%
%   define all or subset of pars structure
%
% --- Restore for pars.method = 'mfile' (Lucretia data format)
% stat = FlArchive('Restore',filename)
%
%   Restore from Lucretia matlab file, if full path not given then assumed
%   the file resides in latticeFiles/archive/
%
% --- Restore for pars.method = 'epics' (restore from EPICS channel archiver)
% stat = FlArchive('Restore',datenum,ShowProgress)
%
%   Ensure that pars.dataServerURL has been correctly set, if not: set and
%   then re-issue FlArchive('init')
%
%   Restore from data nearest Matlab datenum format, should be between
%   earliest and latest available date/times returned in FL.chanArch
%   structure
%
%   If ShowProgress = true then launch progress bar for monitoru=ing
%   archive restore progress
%
% stat = FlArchive('RestoreDefault')
%
%   Restore default lattice into FS
%
% FlArchive('SaveLucretia',filename)
%
%   Save data in Lucretia Matlab file, filename is optional, if given,
%   saves with this file (give full path) - suggest latticeFiles/archive
%   directory
%   If no filename given, saves with same name as default file in
%   latticeFiles/archive directory with timestamp
%
% [stat list] = FlArchive('GetRestorePoints')
%
%   When pars.mode = 'mfile', list is cell array of mfiles in archive
%   directory that can be restored from
%   When pars.mode = 'epics', list is 2 element cell array giving earliest
%   and latest restore points available from EPICS channel archiver in
%   Matlab datenum format (assuming connection to server has been made, if
%   not make sure pars.mode set to 'epics' and re-issue FlArchive('init')
%
% stat = FlArchive('SetDefault')
%
%   Set current running data set as default (causes BEAMLINE B fields to be
%   set so non-corrector PS supplies read out unity, defining as gold the
%   current lattice)
%   Default file saved in latticeFiles directory and will be read in on
%   future server and client restarts on this machine. Old default lattice
%   file gets backed up in same directory with same name appended by
%   timestamp
%
% stat = FlArchive('WriteDefault',sel)
%   
%   Set magnets and mover systems to the current defaults
%     sel is 3 x 6 array of logicals of what to write
%     - Region to restore
%      - sel(1,:) = [DR EXT FFS]
%     - Magnet type
%      - sel(2,:) = [Bends Quads Sexts SkewQuads Xcor Ycor]
%     - Mover axes
%      - sel(3,:) = [x y roll]
%
% =========================================================================
% EXAMPLE:
%   (Restore Flight Simultor to March 9 2009 at 17:32 from EPICS data)
%   >> FlArchive;
%   >> pars.dataServerURL='myserver.mydomain:8080/RPC2';
%   >> pars.method='epics';
%   >> FlArchive('SetPars',pars);
%   >> stat = FlArchive('Restore',datenum(2009,3,9,17,32,0));
persistent pars
stat{1}=1; output=[];

% Argumet check
if nargin>0 && ~ischar(varargin{1})
  error('incorrect arguments');
elseif ~nargin
  varargin{1}='init';
end

% Initialise parameters if not done or requested
if isempty(pars) && ~strcmpi(varargin{1},'init')
  pars=initPars;
end

% Command options
switch lower(varargin{1})
  case 'init'
    pars=initPars;
  case 'getpars'
    output=pars;
  case 'setpars'
    if nargin==2
      newpars=varargin{2};
      if isstruct(newpars)
        parfields=fields(newpars);
        for ipar=1:length(parfields)
          pars.(parfields{ipar})=newpars.(parfields{ipar});
        end
      end
    end
  case 'restore'
    % load restore point
    if nargin==2
      stat=restore(pars,varargin{2},pars.dataServerURL);
    elseif nargin==3
      stat=restore(pars,varargin{2},pars.dataServerURL,varargin{3});
    end
  case 'restoredefault'
    stat=restore('default');
  case 'getrestorepoints'
    [stat output]=restoreList(pars);
  case 'savelucretia'
    saveLucretia(varargin{2});
  case 'setdefault'
    stat=setCurrentAsDefault;
  case 'writedefault'
    stat=setCurrentAsDefault(pars,'write',varargin{1},varargin{2});
  otherwise
    error('Incorrect arguments')
end

%% Internal functions
function stat=setCurrentAsDefault(varargin)
global BEAMLINE PS GIRDER INSTR FL  %#ok<NUSED>
stat{1}=1;
% if cmd=='write', then write current default vals to control system
% instead of defining new
domsg=false;
if nargin>=2 && isequal(varargin{2},'write')
  dowrite=true;
  if varargin{1}.domsg; domsg=true; end;
  writeList=varargin{4};
else
  dowrite=false;
end
% Set BEAMLINE elements so PS reads 1 for non-corrector elements defining
% gold lattice
% Also not do for B field exactly 1, as assume these are e.g. skew quads
% where lattice value is 0 normally
if domsg && dowrite; msgbox('Writing Power Supplies...','Config Restore','replace'); drawnow('expose'); end;
for ips=1:length(PS)
  if isfield(FL,'FlArchive_abort')
    FL=rmfield(FL,'FlArchive_abort');
    stat{1}=0; stat{2}='Abort Requested'; return;
  end
  if dowrite && isfield(FL.HwInfo.PS(ips),'ref') && ~isempty(FL.HwInfo.PS(ips).ref) && FL.HwInfo.PS(ips).ref~=0 && writeCheck('ps',ips,writeList)
    PS(ips).SetPt=FL.HwInfo.PS(ips).ref;
    stat=PSTrim(ips,1);
    if stat{1}~=1
      return
    end
  elseif ~dowrite
    for iele=PS(ips).Element
      if any(PS(ips).Element) && isempty(regexp(BEAMLINE{iele}.Class,'COR$', 'once')) && ~isequal(BEAMLINE{iele}.B,1)
        if PS(ips).Ampl==0
          BEAMLINE{iele}.B(1)=1;
          PS(ips).Ampl=0;
        else
          BEAMLINE{iele}.B=BEAMLINE{iele}.B*PS(ips).Ampl;
          PS(ips).Ampl=1;
        end
      end
    end
    FL.HwInfo.PS(ips).ref=PS(ips).Ampl;
  end
end

% First back up current default file
if ~dowrite
  copyfile(fullfile('latticeFiles',[FL.expt,'lat.mat']),fullfile('latticeFiles',...
    [FL.expt,'lat_',datestr(now,FL.tstamp),'.mat']));
end

% Set reference girder positions
if domsg && dowrite; msgbox('Setting Mover Defaults...','Config Restore','replace'); drawnow('expose'); end;
domove=[];
for igir=1:length(GIRDER)
  if isfield(FL,'FlArchive_abort')
    FL=rmfield(FL,'FlArchive_abort');
    stat{1}=0; stat{2}='Abort Requested'; return;
  end
  if dowrite && any(writeList(3,:)) && isfield(FL.HwInfo.GIRDER(igir),'ref') && ~isempty(FL.HwInfo.GIRDER(igir).ref) && writeCheck('gir',igir,writeList)
    if ~isfield(FL.HwInfo.GIRDER(igir),'pvname') || isempty(FL.HwInfo.GIRDER(igir).pvname)
      continue
    end
    domove(end+1)=igir;
    GIRDER{igir}.MoverSetPt(logical(writeList(3,:)))=FL.HwInfo.GIRDER(igir).ref(logical(writeList(3,:)));
  elseif ~dowrite
    if isfield(GIRDER{igir},'MoverPos')
      FL.HwInfo.GIRDER(igir).ref=GIRDER{igir}.MoverPos;
    end
  end
end
if dowrite && any(writeList(3,:)) && ~isempty(domove)
  if FL.SimMode
    stat=MoverTrim(domove,1);
  else
    stat=MoverTrim(domove,3);
  end
  if stat{1}~=1; return; end;
end

% Save main file as current
if ~dowrite
  load([FL.expt,'lat_orig'],'Model','Beam*');
  HwInfo=FL.HwInfo; %#ok<NASGU>
  save(fullfile('latticeFiles',[FL.expt,'lat.mat']),'Beam*','Model','BEAMLINE','PS','GIRDER','INSTR','HwInfo')
end

function resp=writeCheck(type,ind,list)
global FL PS GIRDER BEAMLINE
% list:
%     - Region to restore
%      - list(1,:) = [DR EXT FFS]
%     - Magnet type
%      - list(2,:) = [Bends Quads Sexts SkewQuads Xcor Ycor]
%     - Mover axes
%      - list(3,:) = [x y roll]
resp=true;
switch type
  case 'ps'
    bi=PS(ind).Element;
  case 'gir'
    bi=GIRDER{ind}.Element;
end
if isempty(any(bi)) || any(bi==0); return; end;
% Check region
if bi(1)>=FL.Region.DR.ind(1) && bi(end)<=FL.Region.DR.ind(end) && ~list(1,1)
  resp=false; return;
end
if bi(1)>=FL.Region.EXT.ind(1) && bi(end)<=FL.Region.EXT.ind(end) && ~list(1,2)
  resp=false; return;
end
if bi(1)>=FL.Region.FFS.ind(1) && bi(end)<=FL.Region.FFS.ind(end) && ~list(1,3)
  resp=false; return;
end
% Check Type
type={'SBEN' 'QUAD' 'SEXT' 'QUAD' 'XCOR' 'YCOR'};
for iele=bi
  itype=find(ismember(type,BEAMLINE{iele}.Class));
  if ~isempty(itype)
    for icl=itype
      wtype=icl;
      if icl==2 || icl==4
        if BEAMLINE{iele}.Tilt
          wtype=4;
        else
          wtype=2;
        end
      end
      if ~list(2,wtype); resp=false; return; end;
    end
  end
end

function [stat list]=restoreList(pars)
list={};
stat{1}=1;
if strcmp(pars.method,'mfile')
  d=dir(pars.dir{2});
  for id=1:length(d)
    if ~isempty(strfind(d.name,'.mat'))
      list{end+1}=[d.name ' :: ' d.date];
    end
  end
else
  % Make sure archive is initialised
  stat=FlUpdate('archInit',pars.dataServerURL);
  if stat{1}~=1; return; end;
  % Fill list based on earliest and latest times available
  t1=min(FL.chanArch.starts);
  t2=max(FL.chanArch.ends);
  list{end+1}=datestr(t1);
  list{end+1}=datestr(t2);
end

function stat=restore(varargin)
global FL
stat{1}=1;
if nargin>=3 && isstruct(varargin{1}) % restore a set point
  pars=varargin{1};
  if isequal(pars.method,'mfile')
    try
      ld=load(varargin{2});
    catch
      ld=load(fullfile(pars.dirs{2},varargin{2}));
    end
    if ~checkLoad(ld); stat{1}=-1; stat{2}='Inconsistent load file with running file'; return; end;
    if isempty(ld.FL) && isfield(ld,'Model') && isfield(ld.Model,'Twiss') && isfield(ld.Model,'Initial')
      ld.FL.SimModel.Twiss=ld.Model.Twiss;
      ld.FL.SimModel.Initial=ld.Model.Initial;
    end
    stat=matLoad(ld);
  elseif isequal(pars.method,'epics')
    if nargin==4
      stat=FlUpdate(varargin{2},varargin{3},varargin{4});
    else
      stat=FlUpdate(varargin{2},varargin{3});
    end
    if stat{1}~=1; return; end;
    % Run Floodland update tasks (e.g. rescaling bends etc), FlUpdate
    % should be disabled at this point so we don't clobber the archive
    % values we just read in
    Floodland('run');
  else
    error('unknown pars.method')
  end
elseif nargin>=1 && isequal(varargin{1},'default') % restore default file
  ld=load(fullfile('latticeFiles',[FL.expt,'lat.mat']));
  stat=matLoad(ld);
else
  error('Incorrect arguments')
end


function stat=matLoad(ld)
global BEAMLINE PS GIRDER INSTR FL
stat{1}=1;

% Get Beam energy
P=BEAMLINE{1}.P;

% BEAMLINE, PS, GIRDER, INSTR load in straight
BEAMLINE=ld.BEAMLINE;
PS=ld.PS;
GIRDER=ld.GIRDER;
INSTR=ld.INSTR;

% Fix any BEAMLINEs that have been set to zero
for ibl=1:length(BEAMLINE)
  % Beamline magnetic fields set to 1.3GeV by design, scale to currently
  % measured value here
  if isfield(BEAMLINE{ibl},'B')
    BEAMLINE{ibl}.B=BEAMLINE{ibl}.B.*(P/1.3);
  end
  if isfield(BEAMLINE{ibl},'B') && BEAMLINE{ibl}.B(1)==0
    BEAMLINE{ibl}.B(1)=1;
    if isfield(BEAMLINE{ibl},'PS') && BEAMLINE{ibl}.PS
      PS(BEAMLINE{ibl}.PS).Ampl=0;
    end
  end
end

% Load desired elements of FL structure
if isfield(ld,'FL')
  if isfield(ld.FL,'SimBeam'); FL.SimBeam=ld.FL.SimBeam; end;
  if isfield(ld.FL,'SimModel')
    smfld=fieldnames(ld.FL.SimModel);
    for ifld=1:length(smfld)
      FL.SimModel.(smfld{ifld})=ld.FL.SimModel.(smfld{ifld});
    end
  end
  if isfield(ld.FL,'FlHwInfo')
    FL.HwInfo=ld.FL.HwInfo;
  end
end

% There should be a model name, if not make it
if ~isfield(FL.SimModel,'opticsName')
  FL.SimModel.opticsName='';
end

function ret=checkLoad(ld)
global BEAMLINE PS GIRDER INSTR %#ok<NUSED>

ret=1;

% Check all the right fields are there
chkFld={'BEAMLINE','PS','GIRDER','INSTR'};
if sum(cellfun(@(x) ismember(x,chkFld),fieldnames(ld)))~=length(chkFld)
  ret=0; return
end
ldFields=fieldnames(ld);
% Check array lengths match
for ifld=1:length(ldFields)
  if ismember(ldFields{ifld},chkFld) && (length(ld.(ldFields{ifld})) ~= length(eval(ldFields{ifld})))
    ret=0; return
  end
end

function saveLucretia(filename)

global BEAMLINE PS GIRDER INSTR FL %#ok<NUSED>
% Get BPM buffer to also save
[stat bpmbuffer]=FlHwUpdate('readbuffer'); %#ok<NASGU>
if exist('filename','var') && ischar(filename)
  save(filename,'BEAMLINE','PS','GIRDER','FL','INSTR','bpmbuffer');
else
  save(fullfile('latticeFiles',[FL.expt,'lat-',datestr(now,FL.tstamp),'.mat']),'BEAMLINE','PS','GIRDER','FL','INSTR','bpmbuffer');
end

function pars=initPars
global FL
pars.method='mfile';
pars.dirs={fullfile(FL.atfDir,'control-software','epics-3.14.8','archive');
           'latticeFiles/archive'};
pars.dataServerURL='http://localhost/archive/cgi/ArchiveDataServer.cgi';
pars.domsg=false;

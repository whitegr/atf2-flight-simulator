function FlUpdateWS
% FLUPDATEWS
%   Update Wirescanner data into EPICS from Vsystem
global BEAMLINE INSTR FL
persistent instmw0x instmw1x instmw2x instmw3x instmw4x

% If client then pass command to server and update
if ~isempty(findstr(FL.mode,'test'))
  sockrw('writechar','cas','FlUpdateWS');
  sockrw('readchar','cas',5);
  FlHwUpdate;
  return
end

% Wirescanner index data
if isempty(instmw0x)
  indmw0x=findcells(BEAMLINE,'Name','MW0X');
  indmw1x=findcells(BEAMLINE,'Name','MW1X');
  indmw2x=findcells(BEAMLINE,'Name','MW2X');
  indmw3x=findcells(BEAMLINE,'Name','MW3X');
  indmw4x=findcells(BEAMLINE,'Name','MW4X');
  instmw0x=findcells(INSTR,'Index',indmw0x);
  if isempty(instmw0x); error('No MW0X INSTR'); end;
  instmw1x=findcells(INSTR,'Index',indmw1x);
  if isempty(instmw1x); error('No MW1X INSTR'); end;
  instmw2x=findcells(INSTR,'Index',indmw2x);
  if isempty(instmw2x); error('No MW2X INSTR'); end;
  instmw3x=findcells(INSTR,'Index',indmw3x);
  if isempty(instmw3x); error('No MW3X INSTR'); end;
  instmw4x=findcells(INSTR,'Index',indmw4x);
  if isempty(instmw4x); error('No MW4X INSTR'); end;
end

% If server then fill wirescanner data from Vsystem
lcaPut('V2EPICS:rRead','DB_EXT::MW0X:FIT.STD.1');
mw0x=lcaGet('V2EPICS:r'); if isnan(mw0x); mw0x=0; end;
lcaPut(FL.HwInfo.INSTR(instmw0x).pvname{1}{5},mw0x^2);
lcaPut('V2EPICS:rRead','DB_EXT::MW1X:FIT.STD.1');
mw1x=lcaGet('V2EPICS:r'); if isnan(mw1x); mw1x=0; end;
lcaPut(FL.HwInfo.INSTR(instmw1x).pvname{1}{5},mw1x^2);
lcaPut('V2EPICS:rRead','DB_EXT::MW2X:FIT.STD.1');
mw2x=lcaGet('V2EPICS:r'); if isnan(mw2x); mw2x=0; end;
lcaPut(FL.HwInfo.INSTR(instmw2x).pvname{1}{5},mw2x^2);
lcaPut('V2EPICS:rRead','DB_EXT::MW3X:FIT.STD.1');
mw3x=lcaGet('V2EPICS:r'); if isnan(mw3x); mw3x=0; end;
lcaPut(FL.HwInfo.INSTR(instmw3x).pvname{1}{5},mw3x^2);
lcaPut('V2EPICS:rRead','DB_EXT::MW4X:FIT.STD.1');
mw4x=lcaGet('V2EPICS:r'); if isnan(mw4x); mw4x=0; end;
lcaPut(FL.HwInfo.INSTR(instmw4x).pvname{1}{5},mw4x^2);

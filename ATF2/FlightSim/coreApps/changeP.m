function stat=changeP(dp,df)
%
% stat=changeP(0.01); % P=P0*(1+0.01)
%
% Change beam momentum by dp*(design momentum) for dispersion measurement
% (change relative to beam first time this function is run; reset reference P
% for this by issuing "clear changeP" command)
%
% stat=changeP(~,'on');  % turn DR RF-ramp ON
% stat=changeP(~,-2);    % set DR RF-ramp dF= -2 kHz
% stat=changeP(~,'off'); % turn DR RF-ramp OFF


global FL BEAMLINE
persistent initX

debug=false;
if (debug)
  fprintf(1,'NOTE: "debug" flag set in changeP\n');
end

stat{1}=1;
if (~isnumeric(dp))
  stat{1}=-1;
  stat{2}='changeP: dp arg must be numeric';
  return
end
twoargs=exist('df','var');

if (isequal(FL.mode,'trusted')&&(~FL.isthread)) % server
  if (FL.SimMode)
    % save initial beam
    if (isempty(initX)||isempty(initX{FL.simBeamID}))
      initX{FL.simBeamID}=FL.SimBeam{FL.simBeamID}.Bunch.x;
    end
    if (twoargs&&ischar(df))
      return % 'on' and 'off' are no-ops in simulation mode
    end
    % for simulation case, change input beam momentum (dp required)
    if (debug)
      if (twoargs)
        fprintf(1,'changeP: dp=%s , df=%s\n',num2str(dp),num2str(df));
      else
        fprintf(1,'changeP: dp=%s\n',num2str(dp));
      end
    else
      FL.SimBeam{FL.simBeamID}.Bunch.x(6,:)= ...
        initX{FL.simBeamID}(6,:)+BEAMLINE{1}.P*dp ;
      % also change initial beam conditions
      FL.SimBeam{FL.simBeamID}.Bunch.x(1,:)= ...
        initX{FL.simBeamID}(1,:)+FL.SimModel.Initial.x.Twiss.eta*dp;
      FL.SimBeam{FL.simBeamID}.Bunch.x(2,:)= ...
        initX{FL.simBeamID}(2,:)+FL.SimModel.Initial.x.Twiss.etap*dp;
      FL.SimBeam{FL.simBeamID}.Bunch.x(3,:)= ...
        initX{FL.simBeamID}(3,:)+FL.SimModel.Initial.y.Twiss.eta*dp;
      FL.SimBeam{FL.simBeamID}.Bunch.x(4,:)= ...
        initX{FL.simBeamID}(4,:)+FL.SimModel.Initial.y.Twiss.etap*dp;
    end % if debug
  else
    % use control system interface (df required)
    if (~twoargs)
      stat{1}=-1;
      stat{2}='changeP: df arg required';
      return
    end
    if (debug)
      if (ischar(df))
        fprintf(1,'changeP: dp=%s , df=''%s''\n',num2str(dp),df);
      else
        fprintf(1,'changeP: dp=%s , df=%s\n',num2str(dp),num2str(df));
      end
      stat{1}=1;
    else
      stat=DRRF_RampControl(df); % df= 'on', 'off', or dF (kHz)
    end
  end % if FL.SimModel
else % client
  % forward request to server
  if (twoargs)
    cmd=['changeP(',num2str(dp,FL.comPrec),','];
    if (ischar(df))
      cmd=[cmd,'''',df,''')'];
    else
      cmd=[cmd,num2str(df,FL.comPrec),')'];
    end
  else
    cmd=['changeP(',num2str(dp,FL.comPrec),')'];
  end
  stat=sockrw('writechar','cas',cmd);
  if (stat{1}~=1),return,end
  sockrw('readchar','cas',10);
end % if trusted

end

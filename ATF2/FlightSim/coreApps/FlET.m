function [stat, varargout] = FlET(cmd,varargin)
% FLET Floodland interface to ATF2 Echotek BPM system
%
% [stat mode] = FlET('GetMode')
%   Get current operating mode of Echotek system, 'single' = "narrowband"
%   mode (mean and rms readings available), 'multi' = "turn-by-turn" mode
%   (multi turn data available)
%
% stat = FlET('SetMode',mode)
%   Request change of ET running mode (mode='single' | 'multi')
%
% [stat nread] = FlET('GetNRead')
%   Get number of turns read out for turn-by-turn mode
%
% stat = FlET('SetNRead')
%   Set number of turns to read out for turn-by-turn mode (max 1280)
%
% [stat instrInds] = FlET('GetINSTR')
%   Get INSTR indicies for BPMs read out through Echotek system
%
% [stat strData] = FlET('Read')
%   Read data from Echotek EPICS server into INSTR global array
%   If client, just read Echotek BPMs into strData char array, INSTR data
%   is updated routinely through the standard way (FlHwUpdate)
%
% stat = FlET('Arm') [server only]
%   Send arm request to Echotek system to acquire new data next machine
%   pulse (this is done routinely in FlUpdate function
%
% [stat crateonline bpmstatus] = FlET('GetStatus')
%   Return status (logical)
%   crateonline = are crates responding to arm commands (ncrates array)
%   bpmstatus = are bpms updating (size(INSTR) array)
%
% stat = FlET('ResetCrateStatus',crate)
%   Reset staus of all crates so the attempt to readout again
%   crate = crate name (string), must be an entry in the crt cell array
%
% ========================
% (stat is standard Lucretia status return type)

% ------------------------------------------------------------------------------
% 29-Nov-2011, M. Woodley
%    Change EPICS PV for 'Read' from 'bpm:orbit*.LAST' to 'bpm:orbit*.EORB'
%    (note: will read on turn number defined by 'bpm:orbit*.EOTN')
% ------------------------------------------------------------------------------

global FL INSTR BEAMLINE
persistent etbpms mver lastbpms crt

stat{1}=1;
varargout{1}=[];

% get matlab version
if isempty(mver)
  mver=ver('MATLAB');
end


% Initialisation
if isempty(etbpms)
  etbpms.label = {(1:24)';(25:48)';(49:72)';(73:96)'};
  crt = {'bpm:orbit1';'bpm:orbit2';'bpm:orbit3';'bpm:orbit4'};
  % Flag Ecotek BPMs
  for icrt=1:length(crt)
    etbpms.ind{icrt}=arrayfun(@(x) findcells(BEAMLINE,'Name',['MB',num2str(x),'R']),etbpms.label{icrt})';
    etbpms.instr{icrt}=arrayfun(@(x) findcells(INSTR,'Index',x),etbpms.ind{icrt})';
  end
  etbpms.mode='single';
  etbpms.conv=[1e-6 1e-6 1];
  etbpms.maxread=1280;
  etbpms.nread=etbpms.maxread;
  FlDataStore('FlET');
  if isfield(FL,'DataStore') && isfield(FL.DataStore,'FlET') && isfield(FL.DataStore.FlET,'nread')
    etbpms.nread=FL.DataStore.FlET.nread;
  end
  etbpms.cratestatus=true(size(crt));
  etbpms.bpmstatus=true(size(INSTR));
end

switch lower(cmd)
  case 'resetcratestatus'
    if nargin<2 || ~ischar(varargin{1}) || ~ismember(varargin{1},crt)
      error('Must provide crate that exists in crt cell array')
    end
    etbpms.cratestatus(ismember(crt,varargin{1})) = true;
  case 'getstatus'
    varargout{1}=etbpms.cratestatus;
    varargout{2}=etbpms.bpmstatus;
  case 'getnread'
    FlDataStore('FlET');
    if isfield(FL,'DataStore') && isfield(FL.DataStore,'FlET') && isfield(FL.DataStore.FlET,'nread')
      etbpms.nread=FL.DataStore.FlET.nread;
    end
    varargout{1}=etbpms.nread;
  case 'setnread'
    if nargin~=2 || ~isnumeric(varargin{1}) || varargin{1}>etbpms.maxread
      stat{1}=-1; stat{2}='Bad input for nread';
      return
    end
    % if client, pass command to server
    if strfind(FL.mode,'test')
      stat=sockrw('writechar','cas',['FlET(''setnread'',',num2str(varargin{1}),')']);
      if stat{1}~=1; return; end;
      [stat, servret]=sockrw('readchar','cas',10);
      if stat{1}~=1; return; end;
      stat=FloodlandAccessServer('decode',servret);
      if stat{1}==1; etbpms.nread=round(varargin{1}); end;
      return
    end
    etbpms.nread=round(varargin{1});
    FlDataStore('FlET','nread',etbpms.nread);
  case 'getinstr'
    varargout{1}=cell2mat(etbpms.instr);
  case 'getmode'
    % if client, pass command to server
    if strfind(FL.mode,'test')
      stat=sockrw('writechar','cas','FlET(''getmode'')');
      if stat{1}~=1; return; end;
      [stat, servret]=sockrw('readchar','cas',10);
      if stat{1}~=1; return; end;
      [stat, etmode]=FloodlandAccessServer('decode',servret);
      if stat{1}==1; etbpms.mode=etmode; end;
      varargout{1}=etbpms.mode;
      return
    end
    if FL.SimMode
      varargout{1}=etbpms.mode;
      return
    end
    % Get current mode (tbt or narrowband)
    for icrt=1:length(crt)
      try  
        if etbpms.cratestatus(icrt)
          measM{icrt} = lcaGet([crt{icrt} '.MODS']);
          cm = lcaGet([crt{icrt} '.MODE']);
          curMode{icrt}=cm{1};
        end
      catch
        etbpms.cratestatus(icrt)=false;
      end
    end
    % Deal with broken crates
    if ~any(etbpms.cratestatus)
      etbpms.mode='single';
    elseif any(~etbpms.cratestatus)
      goodcrates=find(etbpms.cratestatus);
      if isequal(curMode{1},measM{goodcrates(1)}{3})
        etbpms.mode='single';
      else
        etbpms.mode='multi';
      end
      return
    end
    % if crates have mixed status - put them all to multi
    if exist('curMode','var')
      if length(unique(curMode))~=1
        for icrt=1:length(crt)
          if etbpms.cratestatus(icrt)
            lcaPut([crt{icrt} '.MODE'],measM{icrt}{3})
          end
        end
        etbpms.mode='multi';
      elseif isequal(curMode{1},measM{1}{3})
        etbpms.mode='multi';
      else
        etbpms.mode='single';
      end
    else
      etbpms.mode='single';
    end
    varargout{1}=etbpms.mode;
  case 'setmode'
    if nargin~=2 || ~ischar(varargin{1})
      error('Argument error')
    end
    % if client, pass command to server
    if strfind(FL.mode,'test')
      stat=sockrw('writechar','cas',['FlET(''setmode'',''',varargin{1},''')']);
      if stat{1}~=1; return; end;
      [stat, servret]=sockrw('readchar','cas',10);
      if stat{1}~=1; return; end;
      stat=FloodlandAccessServer('decode',servret);
      if stat{1}==1; etbpms.mode=varargin{1}; end;
      return
    end
    etbpms.mode=varargin{1};
    if FL.SimMode
      return
    end
    % Set mode to tbt ('multi') or narrowband ('single')
    for icrt=1:length(crt)
      try
        if etbpms.cratestatus(icrt)
          measM{icrt} = lcaGet([crt{icrt} '.MODS']);
          if isequal(varargin{1},'single')
            lcaPut([crt{icrt} '.MODE'],measM{1}{6});
          elseif isequal(varargin{1},'multi')
            lcaPut([crt{icrt} '.MODE'],measM{1}{3});
          else
            error('Unknown mode')
          end
        end
      catch
        etbpms.cratestatus(icrt)=false;
      end
    end
  case 'arm'
    if ~strcmp(FL.mode,'trusted')
      error('Can only run arm command from server')
    end
    if FL.SimMode
      return
    end
    % Instruct crates to readout next pulse
    for icrt=1:length(crt)
      try
        if etbpms.cratestatus(icrt)
          lcaPut([crt{icrt} '.REPT'],1);
        end
      catch
        etbpms.cratestatus(icrt)=false;
      end
    end
  case 'read'
    % read out data into INSTR arrays
    ostr=[];
    if ~strcmp(FL.mode,'trusted')
      % If not trusted mode, just spit out ET data in text form
      for icrt=1:length(crt)
        if etbpms.cratestatus(icrt)
          if strcmp(etbpms.mode,'single')
            ostr=[];
            for ibpm=1:length(etbpms.label{icrt})
              ostr=[ostr num2str(INSTR{etbpms.instr{icrt}(ibpm)}.DataErr(:)',FL.comPrec)];
              if ibpm~=length(etbpms.label{icrt})
                ostr=[ostr ' '];
              end
            end
          else
            for ibpm=1:length(etbpms.label{icrt})
              ostr=[ostr num2str(INSTR{etbpms.instr{icrt}(ibpm)}.DataArray(:)',FL.comPrec)];
              if ibpm~=length(etbpms.label{icrt})
                ostr=[ostr ' '];
              end
            end
          end
        end
      end
      varargout{1}=ostr;
    end
    if strcmp(etbpms.mode,'single')
      % Deal with sim mode
      if FL.SimMode
        for icrt=1:length(crt)
          if etbpms.cratestatus(icrt)
            for ibpm=1:length(etbpms.label{icrt})
              INSTR{etbpms.instr{icrt}(ibpm)}.DataErr(1)=0;
              INSTR{etbpms.instr{icrt}(ibpm)}.DataErr(2)=0;
              INSTR{etbpms.instr{icrt}(ibpm)}.DataErr(3)=0;
              INSTR{etbpms.instr{icrt}(ibpm)}.DataArray(1)=0;
              INSTR{etbpms.instr{icrt}(ibpm)}.DataArray(2)=0;
              INSTR{etbpms.instr{icrt}(ibpm)}.DataArray(3)=0;
              FL.HwInfo.INSTR(etbpms.instr{icrt}(ibpm)).TimeStamp=now;
            end
          end
        end
        return
      end
      % read data from ET epics system
      % (NOTE: this assumes that EOTN is set properly for each crate)
      for icrt=1:length(crt)
        if etbpms.cratestatus(icrt)
          try
            [rdata, ts]=lcaGet([crt{icrt} '.EORB'],length(etbpms.label{icrt})*7);
            data=reshape(rdata,7,length(etbpms.label{icrt}));
          catch
            etbpms.cratestatus(icrt)=false;
            stat{1}=-1;
            stat{2}=['Failed to get narrowband data for crate: ',num2str(icrt)];
            data=zeros(7,length(etbpms.label{icrt}));
          end
          for ibpm=1:length(etbpms.label{icrt})
            FL.HwInfo.INSTR(etbpms.instr{icrt}(ibpm)).TimeStamp=epicsts2mat(ts);
            INSTR{etbpms.instr{icrt}(ibpm)}.Data(1)=data(3,ibpm).*etbpms.conv(1);
            INSTR{etbpms.instr{icrt}(ibpm)}.DataErr(1)=data(4,ibpm).*etbpms.conv(1);
            INSTR{etbpms.instr{icrt}(ibpm)}.Data(2)=data(5,ibpm).*etbpms.conv(2);
            INSTR{etbpms.instr{icrt}(ibpm)}.DataErr(2)=data(6,ibpm).*etbpms.conv(2);
            INSTR{etbpms.instr{icrt}(ibpm)}.Data(3)=data(1,ibpm).*etbpms.conv(3);
            INSTR{etbpms.instr{icrt}(ibpm)}.DataErr(3)=data(2,ibpm).*etbpms.conv(3);
            INSTR{etbpms.instr{icrt}(ibpm)}.DataArray(1)=0;
            INSTR{etbpms.instr{icrt}(ibpm)}.DataArray(2)=0;
            INSTR{etbpms.instr{icrt}(ibpm)}.DataArray(3)=0;
          end
        end
      end
    else
      % Deal with sim mode
      if FL.SimMode
        for icrt=1:length(crt)
          if etbpms.cratestatus(icrt)
            for ibpm=1:length(etbpms.label{icrt})
              INSTR{etbpms.instr{icrt}(ibpm)}.DataArray=[ones(1,etbpms.nread).*INSTR{etbpms.instr{icrt}(ibpm)}.Data(1);
              ones(1,etbpms.nread).*INSTR{etbpms.instr{icrt}(ibpm)}.Data(2);
              ones(1,etbpms.nread).*INSTR{etbpms.instr{icrt}(ibpm)}.Data(3)];
              INSTR{etbpms.instr{icrt}(ibpm)}.DataErr(1)=0;
              INSTR{etbpms.instr{icrt}(ibpm)}.DataErr(2)=0;
              INSTR{etbpms.instr{icrt}(ibpm)}.DataErr(3)=0;
              FL.HwInfo.INSTR(etbpms.instr{icrt}(ibpm)).TimeStamp=now;
            end
          end
        end
        return
      end
      for icrt=1:length(crt)
        if etbpms.cratestatus(icrt)
        % Read data from ET epics system
          for ibpm=1:length(etbpms.label{icrt})
            pvbpm=sprintf('bpm%02d',etbpms.label{icrt}(ibpm));
            try
              lcaPut([pvbpm '.PROC'],8); % command array data to be put into readout buffers
              [rdata, ts]=lcaGet([pvbpm '.MPOH'],etbpms.nread);
              FL.HwInfo.INSTR(etbpms.instr{icrt}(ibpm)).TimeStamp=epicsts2mat(ts); 
              INSTR{etbpms.instr{icrt}(ibpm)}.DataArray=[rdata.*etbpms.conv(1);
              lcaGet([pvbpm '.MPOV'],etbpms.nread).*etbpms.conv(2);
              lcaGet([pvbpm '.MINT'],etbpms.nread).*etbpms.conv(3)];
            catch
              etbpms.cratestatus(icrt)=false;
              stat{1}=-1;
              stat{2}='Failed to push ET data to readout buffers';
              INSTR{etbpms.instr{icrt}(ibpm)}.DataArray=zeros(3,etbpms.nread);
              INSTR{etbpms.instr{icrt}(ibpm)}.Data(1)=0;
              INSTR{etbpms.instr{icrt}(ibpm)}.Data(2)=0;
              INSTR{etbpms.instr{icrt}(ibpm)}.Data(3)=0;
              INSTR{etbpms.instr{icrt}(ibpm)}.DataErr(1)=0;
              INSTR{etbpms.instr{icrt}(ibpm)}.DataErr(2)=0;
              INSTR{etbpms.instr{icrt}(ibpm)}.DataErr(3)=0;
              continue
            end
            INSTR{etbpms.instr{icrt}(ibpm)}.Data(1)=mean(INSTR{etbpms.instr{icrt}(ibpm)}.DataArray(1,:));
            INSTR{etbpms.instr{icrt}(ibpm)}.DataErr(1)=std(INSTR{etbpms.instr{icrt}(ibpm)}.DataArray(1,:));
            INSTR{etbpms.instr{icrt}(ibpm)}.Data(2)=mean(INSTR{etbpms.instr{icrt}(ibpm)}.DataArray(2,:));
            INSTR{etbpms.instr{icrt}(ibpm)}.DataErr(2)=std(INSTR{etbpms.instr{icrt}(ibpm)}.DataArray(2,:));
            INSTR{etbpms.instr{icrt}(ibpm)}.Data(3)=mean(INSTR{etbpms.instr{icrt}(ibpm)}.DataArray(3,:));
            INSTR{etbpms.instr{icrt}(ibpm)}.DataErr(3)=std(INSTR{etbpms.instr{icrt}(ibpm)}.DataArray(3,:));
          end
        end
      end
    end
    % See if BPMs updating properly
    nbpm=0;
    for icrt=1:length(crt)
      for ibpm=1:length(etbpms.instr{icrt})
        nbpm=nbpm+1;
        if ~isfield(INSTR{etbpms.instr{icrt}(ibpm)},'Data') || isempty(INSTR{etbpms.instr{icrt}(ibpm)}.Data)
          INSTR{etbpms.instr{icrt}(ibpm)}.Data(1)=0;
        end
        if length(lastbpms)<nbpm || lastbpms(nbpm)~=INSTR{etbpms.instr{icrt}(ibpm)}.Data(1)
          etbpms.bpmstatus(etbpms.instr{icrt}(ibpm))=true;
        else
          etbpms.bpmstatus(etbpms.instr{icrt}(ibpm))=false;
        end
        lastbpms(nbpm)=INSTR{etbpms.instr{icrt}(ibpm)}.Data(1);
      end
    end
  otherwise
    error('Unknown command')
end

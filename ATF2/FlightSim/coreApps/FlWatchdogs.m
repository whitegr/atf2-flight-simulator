function varargout = FlWatchdogs(varargin)
% FLWATCHDOGS M-file for FlWatchdogs.fig
%      FLWATCHDOGS, by itself, creates a new FLWATCHDOGS or raises the existing
%      singleton*.
%
%      H = FLWATCHDOGS returns the handle to a new FLWATCHDOGS or the handle to
%      the existing singleton*.
%
%      FLWATCHDOGS('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in FLWATCHDOGS.M with the given input arguments.
%
%      FLWATCHDOGS('Property','Value',...) creates a new FLWATCHDOGS or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before FlWatchdogs_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to FlWatchdogs_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help FlWatchdogs

% Last Modified by GUIDE v2.5 30-Nov-2009 09:01:01

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @FlWatchdogs_OpeningFcn, ...
                   'gui_OutputFcn',  @FlWatchdogs_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before FlWatchdogs is made visible.
function FlWatchdogs_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to FlWatchdogs (see VARARGIN)

% Choose default command line output for FlWatchdogs
handles.output = handles;

% Update handles structure
guidata(hObject, handles);

% Set checkboxes
fn=fieldnames(handles);
for ifn=1:length(fn)
  if ~isempty(strfind(fn{ifn},'uipanel'))
    shan=get(handles.(fn{ifn}),'Children');
    cboxval=[]; apname=[];
    for ishan=1:length(shan)
      if strcmp(get(shan(ishan),'Style'),'checkbox')
        cboxval=shan(ishan);
      elseif strcmp(get(shan(ishan),'Style'),'pushbutton')
        apname=get(shan(ishan),'UserData');
      end
    end
    if isempty(cboxval) || isempty(apname)
      return
    else
      newpars.active=cboxval;
      [~, stat pars]=evalc([apname '(''GetPars'')']);
      if iscell(stat) && stat{1}==1 && isfield(pars,'active')
        set(cboxval,'Value',pars.active)
      end
    end
  end
end


% --- Outputs from this function are returned to the command line.
function varargout = FlWatchdogs_OutputFcn(hObject, eventdata, handles)  %#ok<*INUSL>
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- EXIT button
function pushbutton1_Callback(hObject, eventdata, handles) %#ok<*DEFNU>
% hObject    handle to pushbutton1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
guiCloseFn('FlWatchdogs',handles);

% --- Apertures
function pushbutton2_Callback(hObject, eventdata, handles) %#ok<*INUSD>
% hObject    handle to pushbutton2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global FL
FL.Gui.aperturesGui=aperturesGui;

% --- Executes when user attempts to close figure1.
function figure1_CloseRequestFcn(hObject, eventdata, handles)
% hObject    handle to figure1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
guiCloseFn('FlWatchdogs',handles);


% --- Executes on button press in pushbutton3.
function pushbutton3_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global FL
FL.Gui.componentAvailabilityGui=componentAvailabilityGui;


% --- Executes on button press in checkbox2.
function checkbox2_Callback(hObject, eventdata, handles)
% hObject    handle to checkbox2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

updateCheckboxes(handles);


% --- Executes on button press in checkbox1.
function checkbox1_Callback(hObject, eventdata, handles)
% hObject    handle to checkbox1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

updateCheckboxes(handles);

function updateCheckboxes(handles)

fn=fieldnames(handles);
for ifn=1:length(fn)
  if ~isempty(strfind(fn{ifn},'uipanel'))
    shan=get(handles.(fn{ifn}),'Children');
    cboxval=[]; apname=[];
    for ishan=1:length(shan)
      if strcmp(get(shan(ishan),'Style'),'checkbox')
        cboxval=get(shan(ishan),'Value');
      elseif strcmp(get(shan(ishan),'Style'),'pushbutton')
        apname=get(shan(ishan),'UserData');
      end
    end
    if isempty(cboxval) || isempty(apname)
      stat{1}=-1; %#ok<AGROW>
    else
      newpars.active=cboxval; %#ok<*STRNU>
      evalc(['stat=' apname '(''SetPars'',newpars)']);
    end
    if ~iscell(stat) || stat{1}~=1
      errordlg(apname,'Set Pars Error')
      return
    end
  end
end


% --- ET BPMs
function pushbutton4_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global FL
FL.Gui.etBpmsGui=etBpmsGui;

% --- Executes on button press in checkbox3.
function checkbox3_Callback(hObject, eventdata, handles)
% hObject    handle to checkbox3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

updateCheckboxes(handles);


% --- Executes on button press in checkbox4.
function checkbox4_Callback(hObject, eventdata, handles)
% hObject    handle to checkbox4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

updateCheckboxes(handles);


% --- pvCheck
function pushbutton5_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton5 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global FL
FL.Gui.pvCheckGui=pvCheckGui;

function handles=guiRestoreFn(guiName,handles,restoreList)
% handles = guiRestoreFn(guiName,handles,restoreList)
% restoreList = N x 3 cell array
%               {Tag name , Property name, exec callback (true|false)}
% eg. restoreList={'edit1' 'String' 0; 'radiobutton1' 'Value' 1}

% Check arguments
if ~exist('restoreList','var') || isempty(restoreList) || ~iscell(restoreList)
  error('Invalid argument')
end
rlsize=size(restoreList);
if rlsize(2)~=3
  error('Invalid arguments')
end

% Load GUI data
if exist(fullfile('userData','guiData',[guiName,'.mat']),'file')
  load(fullfile('userData','guiData',[guiName,'.mat']))
else
  return
end

% Set requested fields
if ~isempty(guiData)
  for ilist=1:rlsize(1)
    for itag=1:length(guiData.children)
      if ~isempty(guiData.children(itag).children)
        for itag2=1:length(guiData.children(itag).children)
          if isfield(guiData.children(itag).children(itag2).properties,'Tag') && ...
              strcmp(restoreList{ilist,1},guiData.children(itag).children(itag2).properties.Tag)
            if isfield(guiData.children(itag).children(itag2).properties,restoreList{ilist,2})
              set(handles.(restoreList{ilist,1}),restoreList{ilist,2},...
                guiData.children(itag).children(itag2).properties.(restoreList{ilist,2}));
            end
          end
        end
      else
        if strcmp(restoreList{ilist,1},guiData.children(itag).properties.Tag)
          if isfield(guiData.children(itag).properties,restoreList{ilist,2})
            set(handles.(restoreList{ilist,1}),restoreList{ilist,2},...
              guiData.children(itag).properties.(restoreList{ilist,2}));
          end
        end
      end
    end
    if restoreList{ilist,3}
      evalc([guiName,'(''',restoreList{ilist,1},'_Callback'',handles.',restoreList{ilist,1},',[],handles)']);
    end
  end
end
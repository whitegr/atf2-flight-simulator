function [stat,output]=FlHwUpdate(request,par1,par2,par3,par4)
% [stat,output]=FlHwUpdate(request,par1,par2,par3,par4)
% ------------------------------
% Update Lucretia model from EPICS PV's (trusted) or CAS (test)
% request (optional) = {[GIRDER list] [PS list] [INSTR list]}
% stat: Lucretia status response
% ------------------------------
% Also provides BPM buffering and averaging
% request = 'getpulsenum'
% - get current pulse number
% request = 'wait'
% - Wait for N=par1 pulses to be logged
%   Timeout after par2 seconds
% request = 'bpmave'
% - BPM averaging
%   par1 = N pulses to average (must provide this parameter)
%   par2 = 1: Use N pulses that pass quality cuts, 0: Use however many pass
%   out of the last N (optional, default=0)
%   par3 = Quality cuts to apply ([] = use defaults)
%          [minq maxRMS whichICT returnData] (defaults: [0.5 3 1 0])
%          minq: min charge cut (pulses dropped with charge as measured on
%            dump ICT < this fraction of max charge for N pulse window)
%          maxRMS: fliers > than this calculated RMS removed from average
%          whichICT: 0=ICTDUMP 1=ICT1X
%          returnBadBPMs: 1,0
%   par4 = return data only for this list of INSTRs (optional)
%   output = {[meanData.h; meanData.v; meanData.t; rmsData.h; rmsData.v;
%             rmsData.t; nbad.h; nbad.v; nbad.t] badbpms};
%   where, badbpms = cell(1,N) (logical) of rejected bpm readings based on
%   (1:N) cut criteria or empty if bitget(returnData,1)=1
% request = 'buffersize'
% - Get size of bpm buffer
%   No further input parameters required
%   stat: Lucretia status reply; output: current size of bpm buffer
% request = 'readbuffer'
%   par1 = num_pulses (optional) [get the N last pulses only]
%   par2 = tsCheck (optional) [INSTR index vector]
%          reject any pulse where the requested INSTR indices differ by
%          more than 1/2 a pulse duration
% - readout BPM buffer entries
%   stat: Lucretia status reply
%   output: vector output of bpm buffer
%     format: [pulse# x_bpm1_pulse1 y_bpm1_pulse1 tmit_bpm1_pulse1 x2_1 y2_1 tmit2_1 ... ;
%              pulse# x_bpm1_pulse2 y_bpm1_pulse2 tmit_bpm1_pulse2 x2_2 y2_2 tmit2_2 ... ;
%              ... ]
% request = 'readtsbuffer'
%  par1 = num_pulses (optional) [get the N last pulses only]
%  - readout hardware timestamps for bpm readings in buffer (matlab datenum
%  format)
%    format: [pulse# INSTR1_timestamp_pulse1 INSTR2_timestamp_pulse1 ... ;
%             pulse# INSTR1_timestamp_pulse2 INSTR2_timestamp_pulse2 ...
%             ... ]
% request = 'readarrbuffer'
%   par1 = num_pulses (optional) [get the N last pulses only]
% - readout ET BPM Array (tbt data) buffer entries
%   stat: Lucretia status reply
%   output: vector output of bpm array buffer for ET bpms - get list of ET
%   bpms from FlET('GetINSTR') - returns INSTR indicies
%     format: [pulse# x_bpm1_pulse1_turn1:x_bpm1_pulse1_turnN y_bpm1_pulse1_turn1:turnN tmit_bpm1_pulse1_turn1:turnN x2_1_1:N y2_1_1:N tmit2_1_1:N ... ;
%              pulse# x_bpm1_pulse2_turn1:x_bpm1_pulse2_turnN y_bpm1_pulse2_turn1:turnN tmit_bpm1_pulse2_turn1:turnN x2_2_1:N y2_2_1:N tmit2_2_1:N ... ;
%              ... ]
% request = 'readerrbuffer'
%  par1 = num_pulses (optional) [get the N last pulses only]
%  - Read ET RMS data (error entries for bpm readings)
%  Output format is the same as for 'readbuffer' - only for ET bpms, get
%  list as described above
% request = 'flush'
% - Clear BPM buffer of entries
%   No further input parameters required
% request = 'bpmArrayReq' ; par1 = true|false
%   If server, this determines if bpm array data (ET tbt data or rms quantities from narrowband mode) is buffered
%   If client, this determines if ET bpm data is updated from server
% request = 'getBpmArrayReq' ; output = true|false
%   Request current setting of above
% request = 'force'
%   Force update of all PVs regardless of moni status
% ------------------------------
% Also provides PS limits
% request = 'getPSlimits'
% - get present low/high limits for all PS's
% ---------------------------------------------
%
% ------------------------------------------------------------------------------
% 16-Feb-2010, M. Woodley
%    Special code for ignoring the S-band BPMs (which are sick)
% 14-Feb-2010, M. Woodley
%    Support request for PS low/high limits from client
% 25-Feb-2010, G.White
%    Add force update functionality
% ------------------------------------------------------------------------------

global FL GIRDER PS INSTR BEAMLINE
persistent bpmBuf ictdata lastSave pulsenum lastUpdate beamIndex ...
  bpmArrayReq bpmArrBuf bpmErrBuf tstampBuf idcut idSband

stat{1}=1;
% check no-update flag
if isfield(FL,'noUpdate') && FL.noUpdate && exist('request','var') && ischar(request) && strcmp(request,'autoUpdate')
  return
elseif strcmp('request','autoUpdate')
  clear request
end

% Check for auto updater lock
if exist('request','var') && ischar(request) && strcmp(request,'autoUpdate')
%   disp('Entry (main)')
else
  FL.noUpdate=true;
%   fprintf('Entry: request=%s\n',request)
end

if (isempty(idSband)),idSband=findcells(INSTR,'Type','scav');end
ignoreSband=0;

% Deal with force request
if exist('request','var') && ischar(request) && strcmpi(request,'force')
  forceReq=true;
  request=[];
else
  forceReq=false;
end

% get pointers to those INSTRs that cuts apply to
if (isempty(idcut))
  idcut=false(3,length(INSTR)); % h,v,t
  for n=1:length(INSTR)
    if (isfield(FL.HwInfo.INSTR(n),'conv')&&~isempty(FL.HwInfo.INSTR(n).conv))
      idcut(1,n)= ...
        strcmp(INSTR{n}.Class,'MONI')|| ...
        strcmp(INSTR{n}.Class,'PROF')|| ...
        strcmp(INSTR{n}.Class,'WIRE'); % reads h
      idcut(2,n)= ...
        strcmp(INSTR{n}.Class,'MONI')|| ...
        strcmp(INSTR{n}.Class,'PROF')|| ...
        strcmp(INSTR{n}.Class,'WIRE'); % reads v
      idcut(3,n)= ...
        strcmp(INSTR{n}.Class,'MONI')|| ...
        strcmp(INSTR{n}.Class,'IMON'); % reads t
    end
  end
end

% init
stat{1}=1; output=[];

if exist('request','var') && isequal(request,'get_lastUpdate')
  output=lastUpdate;
  FL.noUpdate=false;
  return
end

% deal with parameter change requests
if exist('request','var') && any(strcmpi(request,'bpmArrayReq')) && nargin==2
  bpmArrayReq=par1;
  FL.noUpdate=false;
  return
elseif exist('request','var') && any(strcmpi(request,'getBpmArrayReq'))
  if isempty(bpmArrayReq)
    output=false;
  else
    output=bpmArrayReq;
  end
  FL.noUpdate=false;
  return
end

% Update time
lastUpdate=now;

% Simulation updating of INSTR arrays
if ~isfield(FL,'SimMode') || FL.SimMode
  [stat output] = UpdateINSTR(FL.SimModel,FL.SimBeam);
  if stat{1}~=1; stat{2}=['UpdateINSTR error: ' stat{2}]; FL.noUpdate=false; return; end;
  % Update mover limits
  for igir=1:length(GIRDER)
    if ~isempty(FL.HwInfo.GIRDER(igir).pvname)
      FlSetMoverLimits(igir);
    end
  end
end

% Request buffered BPM data processing
if isequal(FL.mode,'trusted') && nargin>0 && ~isempty(request)  && ~FL.isthread
  if isequal(request,'bpmave')
    if ~isnumeric(par1) || length(par1)>1; error('Second argument should be scalar number=number of bpm readings to average'); end;
    nrw=size(bpmBuf);
    if par1>nrw(1)
      stat{1}=-1; stat{2}='Length of request greater than bpm buffer size';
      FL.noUpdate=false;
      return
    end % if par>buffer length
    if exist('par2','var') && ~isempty(par2) && length(par2)==1 && par2
      extend=true;
    else
      extend=false;
    end % if par2
    if exist('par4','var') && ~isempty(par4) && any(par4)
      try
        bpmAveData.h=bpmBuf(:,par4.*3-2);
        bpmAveData.v=bpmBuf(:,par4.*3-1);
        bpmAveData.t=bpmBuf(:,par4.*3);
      catch 
        stat{1}=-1; stat{2}=['Bpm list not valid: ',lasterr]; FL.noUpdate=false; return; %#ok<LERR>
      end % try/catch
    else
      bpmAveData.h=bpmBuf(:,1:3:end);
      bpmAveData.v=bpmBuf(:,2:3:end);
      bpmAveData.t=bpmBuf(:,3:3:end);
    end % if list of interested bpms passed
    if exist('par3','var') && ~isempty(par3) && any(par3)
      [stat data badbpm]=bpmave(bpmAveData,par1,extend,par3,ictdata,idcut);
    else
      [stat data badbpm]=bpmave(bpmAveData,par1,extend,[],ictdata,idcut);
    end % if par3
    output={data badbpm};
    if stat{1}<0; error(stat{2}); end;
    FL.noUpdate=false;
    return
  elseif isequal(request,'readbuffer')
    sz=size(bpmBuf);
    if exist('par1','var') && ~isempty(par1)
      if par1>sz(1)
        stat{1}=-1; stat{2}='Length of request greater than bpm buffer size';
        FL.noUpdate=false;
        return
      end % if par>buffer length
      if exist('par2','var') && ~isempty(par2) && ...
          (any(par2<=0) || any(par2>sz(2)))
        stat{1}=-1; stat{2}='one or more of requested INSTR indices does not exist';
        FL.noUpdate=false;
        return
      end
      if exist('par2','var') && ~isempty(par2)
        output=[];
        for ipulse=sz(1)-par1+1:sz(1)
          if (max(tstampBuf(ipulse,par2))-min(tstampBuf(ipulse,par2)))*24*3600 < (1/FL.accRate)*0.5
            output(end+1,:)=[pulsenum(ipulse) bpmBuf(ipulse,:)];
          end
        end
      else
        output=[pulsenum(end-par1+1:end) bpmBuf(end-par1+1:end,:)];
      end
    else
      if exist('par2','var') && ~isempty(par2)
        output=[];
        for ipulse=1:sz(1)
          if (max(tstampBuf(ipulse,par2))-min(tstampBuf(ipulse,par2)))*24*3600 < (1/FL.accRate)*0.5
            output(end+1,:)=[pulsenum(ipulse) bpmBuf(ipulse,:)];
          end
        end
      else
        output=[pulsenum(end-sz(1)+1:end) bpmBuf];
      end
    end % if par1
    FL.noUpdate=false;
    return
  elseif isequal(request,'readarrbuffer')
    if exist('par1','var') && par1
      nrw=size(bpmArrBuf);
      if par1>nrw(1)
        stat{1}=-1; stat{2}='Length of request greater than bpm buffer size';
        FL.noUpdate=false;
        return
      end % if par>buffer length
      output=[pulsenum(end-par1+1:end) bpmArrBuf(end-par1+1:end,:)];
    else
      nrw=size(bpmArrBuf);
      output=[pulsenum(end-nrw(1)+1:end) bpmArrBuf];
    end % if par1
    FL.noUpdate=false;
    return
  elseif isequal(request,'readerrbuffer')
    if isempty(bpmArrayReq) || ~bpmArrayReq
      stat{1}=-1; stat{2}='BPM array buffering not enabled';
      FL.noUpdate=false;
      return
    end
    if exist('par1','var') && par1
      nrw=size(bpmErrBuf);
      if par1>nrw(1)
        stat{1}=-1; stat{2}='Length of request greater than bpm buffer size';
        FL.noUpdate=false;
        return
      end % if par>buffer length
      output=[pulsenum(end-par1+1:end) bpmErrBuf(end-par1+1:end,:)];
    else
      nrw=size(bpmErrBuf);
      output=[pulsenum(end-nrw(1)+1:end) bpmErrBuf];
    end % if par1
    FL.noUpdate=false;
    return
  elseif strcmpi(request,'readtsbuffer')
    if exist('par1','var') && par1
      nrw=size(tstampBuf);
      if par1>nrw(1)
        stat{1}=-1; stat{2}='Length of request greater than bpm buffer size';
        FL.noUpdate=false;
        return
      end % if par>buffer length
      output=[pulsenum(end-par1+1:end) tstampBuf(end-par1+1:end,:)];
    else
      nrw=size(tstampBuf);
      output=[pulsenum(end-nrw(1)+1:end) tstampBuf];
    end % if par1
    FL.noUpdate=false;
    return
  elseif strcmp(request,'getpulsenum')
    output=pulsenum(end);
    FL.noUpdate=false;
    return
  elseif isequal(request,'buffersize')
    nrw=size(bpmBuf);
    output=nrw(1);
    FL.noUpdate=false;
    return;
  elseif isequal(request,'flush')
    bpmBuf=[];
    ictdata=[];
    pulsenum=[];
    tstampBuf=[];
    pause(1/FL.accRate); % wait for next pulse so next buffer fills new BPM readings
    FL.noUpdate=false;
    return;
  elseif strcmp(request,'wait')
    % First wait for the requisite number of machine pulses to pass, then
    % until timeout or forever for pulse number to increment past value
    % required
    
    if ~exist('par1','var') || ~isnumeric(par1) || (exist('par2','var') && ~isnumeric(par2))
      stat{1}=-1; stat{2}='Incorrect parameters for wait command';
      FL.noUpdate=false;
      return
    end
    np=par1;
    if exist('par2','var')
      timeout=par2;
    else
      timeout=1.5*(np/FL.accRate);
    end
    if timeout<(np/FL.accRate)
      stat{1}=-1; stat{2}='Must make timeout longer than N pulses * 1/rep_rate';
      FL.noUpdate=false;
      return
    end
    t0=clock;
    pulse1=pulsenum;
    while etime(clock,t0)<timeout
      if etime(clock,t0)>(np/FL.accRate)
        if stat{1}~=1; return; end;
        etime(clock,t0);
        if (pulsenum(end)-pulse1(end))>=np
          break
        elseif (pulsenum(end)-pulse1(end))<0
          stat{1}=-1; stat{2}='Buffer was flushed whilst waiting'; %#ok<AGROW>
          FL.noUpdate=false;
          return
        end
      end
      FlHwUpdate;
    end
    return
  elseif strcmp(request,'getPSlimits')
    output=NaN(2,length(FL.HwInfo.PS)); % guilty until proven innocent
    for n=1:length(FL.HwInfo.PS)
      if (~isempty(FL.HwInfo.PS(n).low)),output(1,n)=FL.HwInfo.PS(n).low;end
      if (~isempty(FL.HwInfo.PS(n).high)),output(2,n)=FL.HwInfo.PS(n).high;end
    end
    FL.noUpdate=false;
    return
  end % if bpmave data requested
elseif nargin>0 && ~isempty(request) % test interface
  if isequal(request,'bpmave')
    if ~isnumeric(par1) || length(par1)>1; error('Second argument should be scalar number=number of bpm readings to average'); end;
    if exist('par2','var') && ~isempty(par2) && any(par2)
      extend=true;
    else
      extend=false;
    end % if par2
    sendstr=sprintf('bpmavereq: FlHwUpdate(''bpmave'',%d,%d',par1,extend);
    if exist('par3','var') && ~isempty(par3)
      sendstr=[sendstr ',[ ' sprintf('%f ',par3) ']'];
    else
      sendstr=[sendstr ',[]'];
    end % if par3
    ninstr=length(INSTR);
    if exist('par4','var') && ~isempty(par4) && any(par4)
      sendstr=[sendstr ',[' sprintf('%d ',par4) ']'];
      ninstr=length(par4);
    end % if par3
    sendstr=[sendstr ')'];
    stat=sockrw('writechar','cas',sendstr);
    if stat{1}~=1; FL.noUpdate=false; return; end;
    [stat,sout]=sockrw('readchar','cas',30); if stat{1}<0; FL.noUpdate=false; return; end;
    [stat,doutput]=FloodlandAccessServer('decode',sout); if stat{1}<0; FL.noUpdate=false; return; end;
    rawoutput=str2double(regexp(doutput,'\S+(\s+)?','match'))';
    output{1}=reshape(rawoutput(1:9*ninstr),9,ninstr);
    npulse=((length(rawoutput)-(9*ninstr))/3)/ninstr;
    if exist('par3','var') && ~isempty(par3) && par3(end)
      output{2}={reshape(rawoutput(1+9*ninstr:9*ninstr+npulse*ninstr),npulse,ninstr) ...
        reshape(rawoutput(9*ninstr+npulse*ninstr+1:9*ninstr+npulse*ninstr+npulse*ninstr),npulse,ninstr) ...
        reshape(rawoutput(9*ninstr+npulse*ninstr+npulse*ninstr+1:end),npulse,ninstr)};
    end
    FL.noUpdate=false;
    return
  elseif isequal(request,'readbuffer')
    if exist('par1','var') && ~isempty(par1)
      p1req=num2str(par1);
    else
      p1req='[]';
    end
    if exist('par2','var') && ~isempty(par2)
      p2req=num2str(par2);
    else
      p2req='[]';
    end
    stat=sockrw('writechar','cas',sprintf('bpmavereq: FlHwUpdate(''readbuffer'',%s,[%s])',p1req,p2req)); if stat{1}~=1; FL.noUpdate=false; return; end;
    [stat,sout]=sockrw('readchar','cas',30); if stat{1}<0; FL.noUpdate=false; return; end;
    [stat,doutput]=FloodlandAccessServer('decode',sout); if stat{1}<0; FL.noUpdate=false; return; end;
    rawoutput=str2double(regexp(doutput,'\S+(\s+)?','match'));
    try
      output=reshape(rawoutput',length(rawoutput)/(1+length(INSTR)*3),1+length(INSTR)*3);
    catch
      stat{1}=-1; stat{2}='Length mismatch of INSTR between client and server?'; FL.noUpdate=false; return
    end
    return
  elseif isequal(request,'readarrbuffer')
    if exist('par1','var') && par1
      stat=sockrw('writechar','cas',['FlHwUpdate(''readarrbuffer'', ',num2str(par1),')']); if stat{1}~=1; FL.noUpdate=false; return; end;
    else
      stat=sockrw('writechar','cas','FlHwUpdate(''readarrbuffer'')'); if stat{1}~=1; FL.noUpdate=false; return; end;
    end % if par1
    [stat,sout]=sockrw('readchar','cas',30); if stat{1}<0; FL.noUpdate=false; return; end;
    [stat,doutput]=FloodlandAccessServer('decode',sout); if stat{1}<0; FL.noUpdate=false; return; end;
    rawoutput=str2double(regexp(doutput,'\S+(\s+)?','match'));
    [stat instrid]=FlET('GetINSTR'); %#ok<ASGLU>
    [stat nread]=FlET('GetNRead');
    try
      output=reshape(rawoutput',length(rawoutput)/(1+numel(instrid)*3*nread),1+numel(instrid)*3*nread);
    catch
      stat{1}=-1; stat{2}='Length mismatch of INSTR between client and server?'; FL.noUpdate=false; return
    end
    FL.noUpdate=false;
    return
  elseif isequal(request,'readerrbuffer')
    if exist('par1','var') && par1
      stat=sockrw('writechar','cas',['FlHwUpdate(''readerrbuffer'', ',num2str(par1),')']); if stat{1}~=1; return; end;
    else
      stat=sockrw('writechar','cas','FlHwUpdate(''readerrbuffer'')'); if stat{1}~=1; return; end;
    end % if par1
    [stat,sout]=sockrw('readchar','cas',30); if stat{1}<0; return; end;
    [stat,doutput]=FloodlandAccessServer('decode',sout); if stat{1}<0; return; end;
    rawoutput=str2double(regexp(doutput,'\S+(\s+)?','match'));
    [stat instrid]=FlET('GetINSTR');
    try
      output=reshape(rawoutput',length(rawoutput)/(1+numel(instrid)*3),1+numel(instrid)*3);
    catch %#ok<*CTCH>
      stat{1}=-1; stat{2}='Length mismatch of INSTR between client and server?'; return
    end
    return
  elseif isequal(request,'readtsbuffer')
    if exist('par1','var') && par1
      stat=sockrw('writechar','cas',['FlHwUpdate(''readtsbuffer'', ',num2str(par1),')']); if stat{1}~=1; return; end;
    else
      stat=sockrw('writechar','cas','FlHwUpdate(''readtsbuffer'')'); if stat{1}~=1; return; end;
    end % if par1
    [stat,sout]=sockrw('readchar','cas',30); if stat{1}<0; return; end;
    [stat,doutput]=FloodlandAccessServer('decode',sout);
    rawoutput=str2double(regexp(doutput,'\S+(\s+)?','match'));
    try
      output=reshape(rawoutput',length(rawoutput)/(1+length(INSTR)),1+length(INSTR));
    catch
      stat{1}=-1; stat{2}='Length mismatch of INSTR between client and server?'; return
    end
    return
  elseif isequal(request,'buffersize')
    stat=sockrw('writechar','cas','bpmavereq: FlHwUpdate(''buffersize'')'); if stat{1}~=1; return; end;
    [stat,sout]=sockrw('readchar','cas',30); if stat{1}<0; return; end;
    [stat,doutput]=FloodlandAccessServer('decode',sout);
    output=str2double(doutput);
    return
  elseif strcmp(request,'getpulsenum')
    stat=sockrw('writechar','cas','bpmavereq: FlHwUpdate(''getpulsenum'')'); if stat{1}~=1; return; end;
    [stat,sout]=sockrw('readchar','cas',30); if stat{1}<0; return; end;
    [stat,doutput]=FloodlandAccessServer('decode',sout);
    output=str2double(doutput);
    return
  elseif isequal(request,'flush')
    stat=sockrw('writechar','cas','bpmavereq: FlHwUpdate(''flush'')'); if stat{1}~=1; return; end;
    stat=sockrw('readchar','cas',30); if stat{1}<0; return; end;
    return
  elseif strcmp(request,'wait')
    % First wait for the requisite number of machine pulses to pass, then
    % until timeout or forever for pulse number to increment past value
    % required
    if ~exist('par1','var') || ~isnumeric(par1) || (exist('par2','var') && ~isnumeric(par2))
      stat{1}=-1; stat{2}='Incorrect parameters for wait command';
      return
    end
    np=par1;
    if exist('par2','var')
      timeout=par2;
    else
      timeout=1e30;
    end
    if timeout<(np/FL.accRate)
      stat{1}=-1; stat{2}='Must make timeout longer than N pulses * 1/rep_rate';
      return
    end
    t0=toc;
    [stat,data]=FlHwUpdate('getpulsenum');
    pulse1=data(1);
    while (toc-t0)<timeout
      if (toc-t0)>(np/FL.accRate)
        [stat,data]=FlHwUpdate('getpulsenum');
        if stat{1}~=1; return; end;
        if (data(1)-pulse1)>=np
          break
        elseif (data(1)-pulse1)<0
          stat{1}=-1; stat{2}='Buffer was flushed whilst waiting';
          return
        end
      end
    end
    return
  elseif strcmp(request,'getPSlimits')
    stat=sockrw('writechar','cas','FlHwUpdate(''getPSlimits'')');
    if (stat{1}~=1),return,end
    [stat,sout]=sockrw('readchar','cas',30);
    if (stat{1}<0),return,end
    [stat,doutput]=FloodlandAccessServer('decode',sout);
    output=reshape(str2double(regexp(doutput,'\S+(\s+)?','match')),2,[]);
    return
  end % if bpmave data requested
end % if trusted installation

if isequal(FL.mode,'test') || FL.isthread % Update all data from CAS
  if exist('request','var')
    if ~iscell(request) || length(request)~=3 || ~isnumeric(request{1}) || ~isnumeric(request{2}) || ~isnumeric(request{3})
      error('request (optional) = {[GIRDER list] [PS list] [INSTR list]}')
    end % request arg check
  else % update everything
    request={1:length(GIRDER) 1:length(PS) 1:length(INSTR)};
  end % process request
  % Check there are movers on GIRDERs requested
  if ~isempty(request{1})
    request{1}=request{1}(arrayfun(@(x) isfield(GIRDER{x},'MoverPos'),request{1}));
  end % if not empty request{1}
  req=['testUpdate:{[',num2str(request{1}),'] [',num2str(request{2}),'] [',num2str(request{3}),']}'];
  % Request data sync with CAS
  stat=sockrw('writechar','cas',req);
  if stat{1}~=1
    if length(stat)>1 && ischar(stat{2})
      error(stat{2});
    else
      error('writechar error');
    end % if stat{2}
  end % if ~stat
  [stat, casout]=sockrw('readchar','cas',10); % wait up to 10s for response
  if stat{1}~=1; error('cas read error'); end;
  
  % Pull off any B field updates
  s1=regexp(casout{1},'::PS::');
  if ~isempty(s1)
    t=regexp(casout{1}(1:s1-1),'::B::(.+)','tokens');
  else
    t=regexp(casout{1},'::B::(.+)','tokens');
  end
  if ~isempty(t)
    Bstr_tok=t{1}{1};
    Bstr=regexp(Bstr_tok,']','split');
    if ~isempty(Bstr)
      for ib=1:length(Bstr)
        if ~isempty(Bstr{ib})
          Bvals1=regexp(Bstr{ib},'=','split');
          bml_indx=str2double(Bvals1{1});
          if isnan(bml_indx); error('Error reading B fields from server (NaN BEAMLINE index)'); end;
          Bvals2=regexp(Bvals1{2},',','split');
          if length(BEAMLINE{bml_indx}.B)~=length(Bvals2); error('Error reading B fields from server (B dim mismatch)'); end;
          for ival=1:length(Bvals2)
            BEAMLINE{bml_indx}.B(ival)=str2double(Bvals2{ival});
            if isnan(BEAMLINE{bml_indx}.B(ival)); error('Error reading B fields from server (NaN BEAMLINE B value)'); end;
          end
        end
      end
    end
  end
  
  % Pull off any PS SetPt field updates
  t=regexp(casout{1},'::PS::(.+)','tokens');
  if ~isempty(t)
    PSstr_tok=t{1}{1};
    casout{1}=regexprep(casout{1},'::PS::.+','');
    PSstr=regexp(PSstr_tok,']','split');
    if ~isempty(PSstr)
      for ib=1:length(PSstr)
        if ~isempty(PSstr{ib})
          PSvals1=regexp(PSstr{ib},'=','split');
          bml_indx=str2double(PSvals1{1});
          if isnan(bml_indx); error('Error reading PS SetPt fields from server (NaN BEAMLINE index)'); end;
          PSvals2=regexp(PSvals1{2},',','split');
          if length(PS(BEAMLINE{bml_indx}.PS).SetPt)~=length(PSvals2); error('Error reading PS SetPt fields from server (B dim mismatch)'); end;
          for ival=1:length(PSvals2)
            PS(BEAMLINE{bml_indx}.PS).SetPt(ival)=str2double(PSvals2{ival});
            if isnan(PS(BEAMLINE{bml_indx}.PS).SetPt(ival)); error('Error reading PS SetPt fields from server (NaN PS SetPt value)'); end;
          end
        end
      end
    end
  end
  
  % Get rid of B and PS strings
  casout{1}=regexprep(casout{1},'::B::.+','');
  casout{1}=regexprep(casout{1},'::PS::.+','');
  
  % The rest is GIRDER/PS/INSTR updates
  try
    evalc(['readvars=',casout{1}]);
  catch
    error(['Error interpreting response to CAS update request:',lasterr]) %#ok<LERR>
  end % try/catch
  
  % Test response format
  if ~iscell(readvars) || length(readvars)~=3 || ~iscell(readvars{1}) || ~iscell(readvars{2}) || ~iscell(readvars{3})  %#ok<NODEF>
    error('Format error in array returned from CAS testUpdate command');
  end % format test
  
  % Fill globals with data read from CAS
  grpstr={'GIRDER{request{igrp}(iele)}.MoverPos' ...
    'PS(request{igrp}(iele)).Ampl' 'INSTR{request{igrp}(iele)}.Data'};
  for igrp=1:3
    if length(readvars{igrp})~=length(request{igrp})
      if ~(length(readvars{igrp})==1 && length(request{igrp})==0) %#ok<ISMT>
        error('Format error in array returned from CAS testUpdate command');
      end % if not 0 length
    end % format test
    if ~isempty(request{igrp})
      for iele=1:length(request{igrp})
        evalc(['retlen=length(',grpstr{igrp},')']);
        if any(isnan(readvars{igrp}{iele})) || any(isinf(readvars{igrp}{iele}))
          if igrp==3 && ismember(iele,idSband) && ignoreSband
            temp=readvars{igrp}{iele};
            temp(isnan(temp))=0;temp(isinf(temp))=0;
            readvars{igrp}{iele}=temp; %#ok<AGROW>
          else
            error('Bad value returned for: %s iele=%d',grpstr{igrp},iele);
          end
        end % if nan or inf returned
        if ~isnumeric(readvars{igrp}{iele})
          error('Format error in array returned from CAS testUpdate command');
        end % format test
        minlen=min([retlen length(readvars{igrp}{iele})]); %#ok<NASGU>
        eval([grpstr{igrp},'=readvars{igrp}{iele}(1:minlen);']);
      end % for iele
    end % if not empty readvars{igrp}
  end % for igrp
  
  % If requested, also update INSTR DataArray fields / error fields
  if ~isempty(bpmArrayReq) && bpmArrayReq
    [stat etinstr]=FlET('GetINSTR');
    if stat{1}==1 && any(ismember(request{3},etinstr))
      ireq=request{3}(ismember(request{3},etinstr));
      [stat etmode]=FlET('GetMode');
      if stat{1}==1 && strcmp(etmode,'multi')
        stat=sockrw('writechar','cas',['testUpdateInstrDataArray:[',num2str(ireq),']']);
        if stat{1}~=1; warning('Floodland:FlHwUpdate:bpmarrayupdate',['Update bpm array error: ',stat{2}]); return; end;
        [stat output]=sockrw('readchar','cas',10);
        if stat{1}~=1; warning('Floodland:FlHwUpdate:bpmarrayupdate',['Update bpm array error: ',stat{2}]); return; end;
        bpmarray=str2num(output{1}); %#ok<ST2NM>
        % determine number of readings per bpm
        nread=length(bpmarray)/(length(ireq)*3);
        % if bpm tool gui open, update it if needed
        if isfield(FL.Gui,'FlBpmTool') && ishandle(FL.Gui.FlBpmTool.figure1) && str2double(get(FL.Gui.FlBpmTool.edit10,'String'))~=nread
          set(FL.Gui.FlBpmTool.edit10,'String',num2str(nread))
        end
        % unpack data into bpm array
        ni=0; bpmarray=reshape(bpmarray,length(ireq),nread*3);
        for instind=ireq
          ni=ni+1;
          INSTR{instind}.DataArray=[bpmarray(ni,1:nread);
            bpmarray(ni,nread+1:2*nread);
            bpmarray(ni,2*nread+1:3*nread)];
        end
      end
      stat=sockrw('writechar','cas',['testUpdateInstrErr:[',num2str(ireq),']']);
      if stat{1}~=1; warning('Floodland:FlHwUpdate:bpmerrupdate',['Update bpm array error: ',stat{2}]); return; end;
      [stat output]=sockrw('readchar','cas',10);
      if stat{1}~=1; warning('Floodland:FlHwUpdate:bpmerrupdate',['Update bpm array error: ',stat{2}]); return; end;
      bpmerr=str2num(output{1}); %#ok<ST2NM>
      % unpack data into bpm array
      ni=0;
      for instind=ireq
        INSTR{instind}.DataErr(1)=bpmerr(1+ni*3);
        INSTR{instind}.DataErr(2)=bpmerr(2+ni*3);
        INSTR{instind}.DataErr(3)=bpmerr(3+ni*3);
        ni=ni+1;
      end
    end
  end
  
elseif isequal(FL.mode,'trusted') && ~FL.isthread
  
  % Update requested EPICS PVs
  % Triggers on new stripline readout, waits for new data
  if ~FL.SimMode
    stat=FlUpdate(forceReq);
  end
  
  % Perform orbit fit to IP and fill IP VIRT BPM info
  getIPData;
  
  % Update BPM Buffer
  if FL.SimMode==2
    bufferSize=100000;
  else
    bufferSize=str2double(get(FL.Gui.main.edit1,'String'));
  end
  nrw=size(bpmBuf);
  % ensure ET buffer same size as bpm buffer if this is requested to be
  % buffered (throw out old data if any)
  if FL.SimMode~=2 && ~isempty(bpmArrayReq) && bpmArrayReq
    [stat etinstr]=FlET('GetINSTR'); %#ok<ASGLU>
    [stat etmode]=FlET('GetMode');
    if strcmp(etmode,'multi')
      [stat etnread]=FlET('GetNRead');
      nrwArr=size(bpmArrBuf);
      if nrwArr(1)~=nrw(1)
        bpmArrBuf=zeros(nrw(1),etnread*3*numel(etinstr));
      end
    end
    nrwErr=size(bpmErrBuf);
    if nrwErr(1)~=nrw(1)
      bpmErrBuf=zeros(nrw(1),length(INSTR{etinstr(1)}.DataErr)*numel(etinstr));
    end
  end
  if isnumeric(bufferSize)
    if bufferSize<3; bufferSize=3; end;
    if nrw(1)>=bufferSize
      bpmBuf=bpmBuf(2:bufferSize,:);
      if ~isempty(bpmArrayReq) && bpmArrayReq
        bpmErrBuf=bpmErrBuf(2:bufferSize,:);
        if strcmp(etmode,'multi')
          bpmArrBuf=bpmArrBuf(2:bufferSize,:);
        end
      end
      ictdata=ictdata(2:bufferSize,:);
      try
        tstampBuf=tstampBuf(2:bufferSize,:);
      catch ME
        disp(ME.message)
      end
    end % adjust buffer size
    % If running non-FS simulation then done here

    % Subtract BBA offsets?
    [bstat pars]=FlBpmToolFn('GetPars');
    if bstat{1}==1 && pars.bbasub
      newbpms=cell2mat(arrayfun(@(x) INSTR{x}.Data(1:3)-[BEAMLINE{INSTR{x}.Index}.ElecOffset(1:2) 0],...
        1:length(INSTR),'UniformOutput',false));
    else
      if bstat{1}~=1
        warning('Lucretia:Floodland:FlBpmToolFn_getpars',['GetPars error from FlBpmToolFn: ',lasterr]) %#ok<LERR>
      end
      newbpms=cell2mat(cellfun(@(x) x.Data(1:3), INSTR,'UniformOutput',false));
    end
    newbpms(isnan(newbpms))=0;
    if (isempty(bpmBuf) || ~isequal(newbpms,bpmBuf(end,:)))
      if isempty(pulsenum)
        pulsenum(1)=1;
      elseif isempty(bpmBuf)
        pulsenum=pulsenum(end);
      else
        pulsenum(end+1,1)=pulsenum(end)+1;
      end % increment pulsenum
      bpmBuf(end+1,:)=newbpms;
      
      % ET buffering
      if ~isempty(bpmArrayReq) && bpmArrayReq
        if strcmp(etmode,'multi')
          buf=cell2mat(cellfun(@(x) x.DataArray(:)', {INSTR{etinstr(:)}},'UniformOutput',false)); %#ok<*CCAT1>
          [nrw ncol]=size(bpmArrBuf);
          if length(buf)~=ncol
            for icol=1:length(buf)
              bpmArrBuf(nrw+1,icol)=buf(icol);
            end
          else
            bpmArrBuf(end+1,:)=buf;
          end
        end
        bpmErrBuf(end+1,:)=cell2mat(cellfun(@(x) x.DataErr(:)', {INSTR{etinstr(:)}},'UniformOutput',false));
      end
      
      
      % Fill dump ICT data
      if ~isfield(beamIndex,'ICTDUMP')
        iele=findcells(BEAMLINE,'Name','ICTDUMP');
        iele2=findcells(BEAMLINE,'Name','ICT1X');
        if length(iele)~=1 || length(iele2)~=1
          error('Error getting ICT dump index')
        end
        beamIndex.ICTDUMP=findcells(INSTR,'Index',iele);
        beamIndex.ICT1X=findcells(INSTR,'Index',iele2);
        if length(beamIndex.ICTDUMP)~=1 || length(beamIndex.ICT1X)~=1
          error('Error getting ICT INSTR dump index')
        end
      end
      ictdata(end+1,1)=INSTR{beamIndex.ICTDUMP}.Data(3);
      ictdata(end,2)=INSTR{beamIndex.ICT1X}.Data(3);
      
      % Get HW timestamps for BPMs
      if FL.SimMode
        tstampBuf(end+1,:)=ones(size([FL.HwInfo.INSTR.TimeStamp])).*now;
      else
        tstampBuf(end+1,:)=[FL.HwInfo.INSTR.TimeStamp];
      end
      
      if FL.SimMode==2; FL.noUpdate=false; return; end;
      
      % Process Feedbacks
      try
        [stat pars]=FlFB('GetPars');
        if pars.active
          set(FL.Gui.main.pushbutton20,'BackgroundColor','green')
          set(FL.Gui.main.pushbutton20,'String','ON')
          FlFB('Run');
        else
          set(FL.Gui.main.pushbutton20,'BackgroundColor','red')
          set(FL.Gui.main.pushbutton20,'String','OFF')
        end
        if isfield(FL.Gui,'FlFB_settingsGUI') && ishandle(FL.Gui.FlFB_settingsGUI.figure1) && ...
            ~strcmp(get(FL.Gui.FlFB_settingsGUI.pushbutton1,'String'),'OK')
          set(FL.Gui.FlFB_settingsGUI.pushbutton2,'String','OK')
          set(FL.Gui.FlFB_settingsGUI.pushbutton2,'BackgroundColor','green')
          drawnow('expose')
        end
      catch ME
        fprintf('FB error: %s\n',ME.message)
        if isfield(FL.Gui,'FlFB_settingsGUI') && ishandle(FL.Gui.FlFB_settingsGUI.figure1)
          set(FL.Gui.FlFB_settingsGUI.pushbutton2,'String','Error')
          set(FL.Gui.FlFB_settingsGUI.pushbutton2,'BackgroundColor','red')
          drawnow('expose')
        end
      end
      
    end % only store if updated since last time
  end % if proper buffer size argument
  
else
  error('FL.mode must = ''trusted'' or ''test''');
end % if FL.mode==test

% Write out requested lattice file
if isequal(FL.mode,'trusted') && ~FL.isthread
  latsave=[get(FL.Gui.main.radiobutton3,'Value') get(FL.Gui.main.radiobutton4,'Value') ...
    get(FL.Gui.main.radiobutton5,'Value') get(FL.Gui.main.radiobutton6,'Value')];
elseif ~FL.isthread
  latsave=[get(FL.Gui.main.radiobutton5,'Value') get(FL.Gui.main.radiobutton1,'Value') ...
    get(FL.Gui.main.radiobutton3,'Value') get(FL.Gui.main.radiobutton4,'Value')];
end % if trusted
if ~FL.isthread && find(latsave)>1
  try
    if isequal(FL.mode,'trusted')
      if isempty(lastSave)
        lastSave=toc;
      else
        timeStr=get(FL.Gui.main.popupmenu2,'String');
        if isfield(FL,'saveonce') && FL.saveonce
          FL.saveonce=false;
        else
          if isnan(str2double(timeStr{get(FL.Gui.main.popupmenu2,'Value')})) || (toc-lastSave)<(1/str2double(timeStr{get(FL.Gui.main.popupmenu2,'Value')}))
            FL.noUpdate=false;
            return
          end % if enough time elapsed since last save
        end
      end % if lastSave has been initialised
    end % if trusted
    save([FL.expt,'lat_current'],'BEAMLINE','PS','GIRDER','INSTR','FL');
    if find(latsave)==3
      fli=FL.HwInfo.INSTR; FL.HwInfo=rmfield(FL.HwInfo,'INSTR');
      try
        evalc('Lucretia2AML;'); % write out AML file
      catch %#ok<CTCH>
        warning('Lucretia:Floodland:FlHwUpdate:saveFail', ...
          'Lucretia2AML not working. Perhaps unselect it on the main gui.')
      end
      FL.HwInfo.INSTR=fli;
      if exist('testfile.aml','file')
        evalc(['!mv testfile.aml ',FL.expt,'lat_current.aml']);
      end % if testfile.aml written
    end % if AML write requested
    if find(latsave)==4
      fli=FL.HwInfo.INSTR; FL.HwInfo=rmfield(FL.HwInfo,'INSTR');
      evalc('Lucretia2AML(''output'',''testfile.aml'',true);'); % write out AML file [Lucretia2AML(true) if want xsif also]
      FL.HwInfo.INSTR=fli;
      if exist('testfile.aml','file')
        evalc(['!mv testfile.aml ',FL.expt,'lat_current.aml']);
      end % if testfile.aml written
      %       evalc(['!sad_driver ',FL.expt,'lat_current.aml > ',FL.expt,'lat_current.sad']); % SAD file
      if exist('testfile.xsif','file')
        evalc(['!mv testfile.xsif ',FL.expt,'lat_current.xsif']);
      end % if testfile.xsif written
    end % if AML + XSIF requested
  catch
    warning('Lucretia:Floodland:FlHwUpdate:saveFail','Save request failed!');
  end
end % if AML file write requested

FL.noUpdate=false;

% -----------------------------------------------------
% Local Functions
% ----------------------------------------------------

function [stat,output,dataUsed]=bpmave(bpmData,nave,extend,cutvals,ict,idcut)
% Perform quality cuts and bpm averaging on passed data
% bpmData=npulse*length(INSTR)*3 (x,y and tmit per bpm per pulse)
% if extend = true, then use more data than requested to calculate
% averaging

% Argument check
stat{1}=1; output={}; dataUsed={};
[npulse,ndata]=size(bpmData.h);
if ~exist('nave','var')
  stat{1}=-1; stat{2}='bpmave: must pass nave'; return;
elseif nave<2 || nave>npulse
  stat{1}=-1; stat{2}='bpmave: nave must be >=2 and <= size of buffered data'; return;
end % nave check

% cuts to apply
if exist('cutvals','var') && ~isempty(cutvals)
  if isnumeric(cutvals) && length(cutvals)==4
    minq=cutvals(1);maxRMS=cutvals(2);whichICT=cutvals(3);returnData=cutvals(4);
  else
    stat{1}=-1;stat{2}='Bad cutvals argument to bpmave';return;
  end % arg check cutvals
else % use defaults
  minq=0.5; % relative ict charge cut
  maxRMS=3; % fliers > than this calculated RMS removed from average
  whichICT=0; % ICTDUMP used for cuts
  returnData=0; % badbpms or ict data not returned by default
end % if cutvals given
badSIG=0; % bpms with readings exactly = this removed from average
maxVAL=5e-2; % abs Values returned that are greater than this assumed bad
% choose ict data
if whichICT==0
  ict=ict(:,1);
elseif whichICT==1
  ict=ict(:,2);
else
  stat{1}=-1; stat{2}='ICT choice error, must = 0 or 1';
  return
end
% get raw data (last nave pulses)
rawData.h=bpmData.h(end-nave+1:end,:);
rawData.v=bpmData.v(end-nave+1:end,:);
rawData.t=bpmData.t(end-nave+1:end,:);

newDataReq=true;
Npass=0;
dims='hvt';
while newDataReq
  newDataReq=false;
  Npass=Npass+1;
  for idim=1:length(dims)
    np.(dims(idim))=size(rawData.(dims(idim)));
    % change NaN's to badSIG
    rawData.(dims(idim))(isnan(rawData.(dims(idim))))=badSIG;
    % do statistics (mean & std over requested number of pulses)
    goodDataCount=cumsum(rawData.(dims(idim))~=badSIG);
    rmsData.(dims(idim))=arrayfun(@(x) ...
      std(rawData.(dims(idim))(((rawData.(dims(idim))(:,x)~=badSIG)&goodDataCount(:,x)<=nave),x)),1:ndata);
    meanData.(dims(idim))=arrayfun(@(x) ...
      mean(rawData.(dims(idim))(((rawData.(dims(idim))(:,x)~=badSIG)&goodDataCount(:,x)<=nave),x)),1:ndata);
    if ((Npass>1)&&(min(goodDataCount(end,idcut(idim,:)))>=nave))
      %       [nr,nc]=size(rawData.(dims(idim)));
      %fprintf(1,'\nbpmave %s-data: %d pulses used\n',dims(idim),nr);
      continue
    end
    % --- cut low tmit's
    tcut=abs(ict(end-np.(dims(idim))(1)+1:end))<(max(abs(ict(end-np.(dims(idim))(1)+1:end))*minq));
    rawData.(dims(idim))(tcut,:)=badSIG;
    % --- cut large values
    if idim<3
      rawData.(dims(idim))(abs(rawData.(dims(idim)))>maxVAL)=badSIG;
    end % if x or y
    % -- cut repeated entries
    for ibpm=1:ndata
      [~, IC]=unique(rawData.(dims(idim))(:,ibpm),'first');
      rawData.(dims(idim))(setdiff(1:length(rawData.(dims(idim))(:,ibpm)),IC),ibpm)=badSIG;
    end
%     for ipulse=1:np.(dims(idim))
%       for ibpm=1:ndata
%         if ipulse>1
%           lastGood=ipulse-1;
%           if rawData.(dims(idim))(ipulse-1,ibpm)==badSIG % search for last good pulse
%             if (ipulse-1)>1
%               for ip=1:ipulse-1
%                 if rawData.(dims(idim))(ip,ibpm)~=badSIG
%                   lastGood=ip;
%                 end % if good
%               end % for ip
%             end % if ipulse-1>1
%           end % if badSIG previously
%           if rawData.(dims(idim))(ipulse,ibpm)==rawData.(dims(idim))(lastGood,ibpm)
%             for ip=ipulse:np.(dims(idim))
%               if rawData.(dims(idim))(ip,ibpm)==rawData.(dims(idim))(lastGood,ibpm)
%                 rawData.(dims(idim))(ip,ibpm)=badSIG;
%               else
%                 break
%               end % if repeated reading
%             end % for ip
%           end % if same as lastGood
%         end % if not first pulse
%       end % for ibpm
      % -- remove fliers
    for ipulse=1:np.(dims(idim))
      if idim<3
        rawData.(dims(idim))(ipulse,((rawData.(dims(idim))(ipulse,:)-meanData.(dims(idim)))./ ...
          rmsData.(dims(idim)))>maxRMS)=badSIG;
      end % if idim = x or y
    end % for ipulse
    % get number of bad pulses
    goodDataCount=cumsum(rawData.(dims(idim))~=badSIG);
    nbad.(dims(idim))=sum((rawData.(dims(idim))==badSIG)&(goodDataCount<=nave));
    % If extend requested- add more data to raw
    if (extend&& ...
        any(nbad.(dims(idim))(:,idcut(idim,:)))&& ...
        all(npulse-nave-(nbad.(dims(idim))(:,idcut(idim,:))+1)>0))
      rawData.(dims(idim))= ...
        [bpmData.(dims(idim))(end-nave-max(nbad.(dims(idim))(:,idcut(idim,:)))+1:end-nave,:);rawData.(dims(idim))];
      newDataReq=true;
    else
      if (extend&& ...
          any(npulse-nave-(max(nbad.(dims(idim)))+1)<1))
        stat{1}=0; stat{2}='extend fail'; %#ok<AGROW>
      end
    end % if extend wanted and able
  end % for idim
end % while newDataReq
if bitget(returnData,1)
  dataUsed={rawData.h~=badSIG rawData.v~=badSIG rawData.t~=badSIG};
else
  dataUsed={0 0 0};
end
output=[meanData.h; meanData.v; meanData.t; rmsData.h; rmsData.v; rmsData.t; nbad.h; nbad.v; nbad.t];

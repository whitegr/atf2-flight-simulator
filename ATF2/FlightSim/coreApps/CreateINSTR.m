function [Model, INSTR] = CreateINSTR(Model,instdata,instin)
% Create INSTR global based on provided idat from TrackThru
%
% 111112 MDW add MQF1FF and MQD0FF to nohw variable
% 100511 JLN remove MFB2FF from nohw variable, with Glen's permission
%
global BEAMLINE FL
persistent idat

if exist('instdata','var') && iscell(instdata)
  idat=instdata;
elseif isempty(idat)
  error('Must call once with instdata input');
end % if instdata filled once

INSTR={};
nohw={'R','CAVITYCOMP','MB1X','MB2X','FONTP1','FONTP2','FONTP3', ...
  'IPBPM','MQF1FF','MQD0FF','MDUMP','IPBPMA','IPBPMB','IPBPMC'}; % no hardware readout for these
% turn on all hardware for sim-mode (no FS)
if isfield(FL,'SimMode') && FL.SimMode==2
  nohw={};
end
rbpm=findcells(BEAMLINE,'Name','MB*R');
button=cellfun(@(x) x.Name,BEAMLINE(rbpm),'UniformOutput',false)';
button{end+1}='MB1X';button{end+1}='MB2X';
stripline={'MQF1X','MQD2X','MQF3X','MQF4X','MQD5X','MQF6X','MQF7X','MQD8X', ...
  'MQF9X','FONTP1','FONTP2','MQF13X','MQD14X','FONTP3','MQF15X','MFB1FF', ...
  'MDUMP'};
ccav={'MQD10X','MQF11X','MQD12X','MQD16X','MQF17X','MQD18X','MQF19X', ...
  'MQD20X','MQF21X','MQM16FF','MQM15FF','MQM14FF','MFB2FF','MQM13FF', ...
  'MQM12FF','MQM11FF','MQD10BFF','MQD10AFF','MQF9BFF','MSF6FF','MQF9AFF', ...
  'MQD8FF','MQF7FF','MQD6FF','MQF5BFF','MSF5FF','MQF5AFF','MQD4BFF', ...
  'MSD4FF','MQD4AFF','MQF3FF','MQD2BFF','MQD2AFF','MPREIP','MPIP'};
scav={'MSF1FF','MQF1FF','MSD0FF','MQD0FF'};
ip={};
imon={'ICT1X','ICTDUMP'};
extwire={'MW0X','MW1X','MW2X','MW3X','MW4X'};
otr={'OTR0X','OTR1X','OTR2X','OTR3X'};
bkgmon={'IPBSM-BKG'};
virt={'IP'};

if exist('instdata','var') && isequal(instdata,'zero')
  if isempty(idat)
    error('Must run once normally to initialise')
  end % if empty idat
  sres=0;
  cres=0;
  ipres=0;
  bres=0;
  ipsmres=[0 0 0 0 0 0];
  ipcwsres=[0 0 0 0 0 0];
else
  ipsmres=[2e-9 2e-9 0 Model.ipLaserRes(1) Model.ipLaserRes(2) Model.ipLaserRes(2)];
  ipcwsres=[2e-6 2e-6 0 2e-6 2e-6 2e-6];
  sres=5e-6;
  cres=0.1e-6;
  ipres=5e-9;
  bres=20e-6;
end % if instdata=='zero'

for iType=1:3
  if length(idat)>=iType && ~isempty(idat{iType})
    for iCount=1:length(idat{iType})
      if ~ismember(BEAMLINE{idat{iType}(iCount).Index}.Name,nohw)
        INSTR{length(INSTR)+1}.Class = BEAMLINE{idat{iType}(iCount).Index}.Class;
        INSTR{end}.Index = idat{iType}(iCount).Index;
        if ismember(BEAMLINE{INSTR{end}.Index}.Name,stripline)
          INSTR{end}.Type = 'stripline';
          INSTR{end}.Res = sres;
        elseif ismember(BEAMLINE{INSTR{end}.Index}.Name,ccav)
          INSTR{end}.Type = 'ccav';
          INSTR{end}.Res = cres;
        elseif ismember(BEAMLINE{INSTR{end}.Index}.Name,scav)
          INSTR{end}.Type = 'scav';
          INSTR{end}.Res = cres;
        elseif ismember(BEAMLINE{INSTR{end}.Index}.Name,ip)
          INSTR{end}.Type = 'ip';
          INSTR{end}.Res = ipres;
        elseif isequal(BEAMLINE{INSTR{end}.Index}.Name,'MW1IP')
          INSTR{end}.Type = 'IP_CWS';
          INSTR{end}.Res = ipcwsres;
        elseif isequal(BEAMLINE{INSTR{end}.Index}.Name,'MS1IP')
          INSTR{end}.Type = 'IP_SM';
          INSTR{end}.Res = ipsmres;
        elseif ismember(BEAMLINE{INSTR{end}.Index}.Name,button)
          INSTR{end}.Type = 'button';
          INSTR{end}.Res = bres;
        elseif ismember(BEAMLINE{INSTR{end}.Index}.Name,imon)
          INSTR{end}.Type = 'ict';
          INSTR{end}.Res = 0;
        elseif ismember(BEAMLINE{INSTR{end}.Index}.Name,bkgmon)
          INSTR{end}.Type = 'bkgmon';
          INSTR{end}.Res = 0;
        elseif ismember(BEAMLINE{INSTR{end}.Index}.Name,extwire)
          INSTR{end}.Type = 'extwire';
          INSTR{end}.Res = 0;
        elseif ismember(BEAMLINE{INSTR{end}.Index}.Name,otr)
          INSTR{end}.Type = 'otr';
          INSTR{end}.Res = 0;
        elseif ismember(BEAMLINE{INSTR{end}.Index}.Name,virt)
          INSTR{end}.Type = 'virtual';
          INSTR{end}.Res = 0;
        else
          INSTR{end}.Type = BEAMLINE{INSTR{end}.Index}.Class ;
          if ~isequal(BEAMLINE{INSTR{end}.Index}.Class,'MONI')
            INSTR{end}.Res = zeros(1,6);
          else
            INSTR{end}.Res = 0;
          end % if profile monitor
        end % if type
      else % if in model but no hw
        INSTR{length(INSTR)+1}.Class = 'NOHW';
        INSTR{end}.Index = idat{iType}(iCount).Index;
        INSTR{end}.Res = 0;
        INSTR{end}.Type = 'NOHW';
      end % if hw exists
      INSTR{end}.instdata_ind=[iType iCount];
      INSTR{end}.Data=zeros(1,max(length(INSTR{end}.Res),3));
      INSTR{end}.ref=zeros(1,3);
      INSTR{end}.referr=zeros(1,3);
      INSTR{end}.dispref=zeros(1,4);
      INSTR{end}.dispreferr=zeros(1,4);
      if isfield(BEAMLINE{INSTR{end}.Index},'Resolution')
        BEAMLINE{INSTR{end}.Index}.Resolution=INSTR{end}.Res(1);
      end % if Resolution field
    end % for iCount
  end % if ~isempty(idat{iType})
end % for iType
if exist('instin','var') && isfield(instin{1},'ref') && ...
    length(instin)==length(INSTR) && isfield(instin{1},'dispref')
  for ii=1:length(INSTR)
    INSTR{ii}.ref=instin{ii}.ref;
    INSTR{ii}.referr=instin{ii}.referr;
    INSTR{ii}.dispref=instin{ii}.dispref;
    INSTR{ii}.dispreferr=instin{ii}.dispreferr;
  end
end

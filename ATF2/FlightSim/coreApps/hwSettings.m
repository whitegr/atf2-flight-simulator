function varargout = hwSettings(varargin)
% HWSETTINGS M-file for hwSettings.fig
%      HWSETTINGS, by itself, creates a new HWSETTINGS or raises the existing
%      singleton*.
%
%      H = HWSETTINGS returns the handle to a new HWSETTINGS or the handle to
%      the existing singleton*.
%
%      HWSETTINGS('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in HWSETTINGS.M with the given input arguments.
%
%      HWSETTINGS('Property','Value',...) creates a new HWSETTINGS or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before hwSettings_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to hwSettings_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help hwSettings

% Last Modified by GUIDE v2.5 11-May-2010 03:57:04

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @hwSettings_OpeningFcn, ...
                   'gui_OutputFcn',  @hwSettings_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before hwSettings is made visible.
function hwSettings_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to hwSettings (see VARARGIN)

% Choose default command line output for hwSettings
handles.output = handles;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes hwSettings wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = hwSettings_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Exit function
function pushbutton1_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
guiCloseFn('hwSettings',handles);

% --- Lauch magnet standardisation app
function pushbutton2_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
appLaunch('magStandardise');

% --- Launch FFS Quad Polarities change app
function pushbutton3_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
appLaunch('setQMffPolarities');

% --- Executes when user attempts to close figure1.
function figure1_CloseRequestFcn(hObject, eventdata, handles) %#ok<*INUSL>
% hObject    handle to figure1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: delete(hObject) closes the figure
guiCloseFn('hwSettings',handles);

function appLaunch(app)
global FL

try
  % --- Run requested user app
  % Run as thread?
  runthread=0;
  % Are there any available
  if ~exist(['trustedApps/',app],'dir') || exist(['testApps/',app,'/NOTHREAD'],'file')
    if get(FL.Gui.main.checkbox3,'Value')
      if isfield(FL,'threads') && isfield(FL.threads,'status') && ~isempty(FL.threads.status)
        if any(cellfun(@(x) strcmp(x,'running')))
          runthread=find(cellfun(@(x) strcmp(x,'running')),'first');
        else
          if ~strcmp(questdlg('No available non-busy threads, run in main thread?','No thread available',...
            'Yes','No','No'),'Yes')
            return
          end
        end
      end
    end
  end
  if runthread
    stat = FlThreads('startApp',runthread,app);
    if stat{1}~=1
      if findstr(stat{2},'Can only run .fig or .m file in thread')
        runthread=0; % 3rd party apps run in own thread by definition
      else
        errordlg(stat{2},'Error starting app in thread')
        return
      end
    end
  end
  if ~runthread
    if ~isdeployed; addpath(['trustedApps/',app]); end;
    if exist(['trustedApps/',app,'/',app,'.fig'],'file')
      evalc(['FL.Gui.(app)=',app]);
    elseif exist(['trustedApps/',app,'/',app,'.m'],'file')
      run(['trustedApps/',app,'/',app]);
    else
      if ~exist(['trustedApps/',app,'/',app],'file')
        errordlg('Application not found','Run user app error');
        return
      end 
      evalc(['!trustedApps/',app,'/',app,'&']);
    end 
  end
catch
  if isfield(FL,'Gui') && isfield(FL.Gui,'main')
    errordlg(lasterr,'Error running requested application');
  else
    warning('Lucretia:Floodland:trustedApps','Error running requested application');
  end
  bufferFlush;
end


% --- Choose IACT or IDES
function popupmenu1_Callback(hObject, eventdata, handles) %#ok<*DEFNU,*INUSD>
% hObject    handle to popupmenu1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global FL

if get(hObject,'Value')==1 % IACT
  pvnamerep='current.VAL';
  pvnameorig='currentRead.VAL$';
else
  pvnamerep='currentRead.VAL';
  pvnameorig='current.VAL$';
end
for iPS=1:length(FL.HwInfo.PS)
  if isfield(FL.HwInfo.PS(iPS),'pvname') && ~isempty(FL.HwInfo.PS(iPS).pvname)
    for iPV1=1:length(FL.HwInfo.PS(iPS).pvname)
      for iPV2=1:length(FL.HwInfo.PS(iPS).pvname{iPV1})
        if ~isempty(regexp(FL.HwInfo.PS(iPS).pvname{iPV1}{iPV2},'^PS','once'))
          FL.HwInfo.PS(iPS).pvname{iPV1}{iPV2}=regexprep(FL.HwInfo.PS(iPS).pvname{iPV1}{iPV2},pvnameorig,pvnamerep);
        end
      end
    end
  end
end

% --- Executes during object creation, after setting all properties.
function popupmenu1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to popupmenu1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in pushbutton5.
function pushbutton5_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton5 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global FL
useApp('extStripConfig');
mhan=msgbox('Stripline Configuration Application loading...');
FL.Gui.extStripConfig=extStripConfig;
if ishandle(mhan); delete(mhan); end;

function varargout = trustedApps(varargin)
% TRUSTEDAPPS M-file for trustedApps.fig
%      TRUSTEDAPPS, by itself, creates a new TRUSTEDAPPS or raises the existing
%      singleton*.
%
%      H = TRUSTEDAPPS returns the handle to a new TRUSTEDAPPS or the handle to
%      the existing singleton*.
%
%      TRUSTEDAPPS('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in TRUSTEDAPPS.M with the given input arguments.
%
%      TRUSTEDAPPS('Property','Value',...) creates a new TRUSTEDAPPS or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before trustedApps_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to trustedApps_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help trustedApps

% Last Modified by GUIDE v2.5 04-Dec-2012 00:01:34

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @trustedApps_OpeningFcn, ...
                   'gui_OutputFcn',  @trustedApps_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before trustedApps is made visible.
function trustedApps_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to trustedApps (see VARARGIN)

% Choose default command line output for trustedApps
handles.output = handles;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes trustedApps wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = trustedApps_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on button press in pushbutton1.
function pushbutton1_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global FL
if ~isfield(FL,'Gui') || ~isfield(FL.Gui,'IPfitData') || ~ishandle(FL.Gui.IPfitData)
  FL.Gui.IPfitData=IPfitData;
end

% --- Executes on button press in pushbutton2.
function pushbutton2_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

appLaunch('IP_knob');

% --- Executes on button press in pushbutton3.
function pushbutton3_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

guiCloseFn('trustedApps',handles);


% --- Executes when user attempts to close figure1.
function figure1_CloseRequestFcn(hObject, eventdata, handles) %#ok<*INUSL>
% hObject    handle to figure1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

guiCloseFn('trustedApps',handles);

function appLaunch(app)
global FL

try
  % --- Run requested user app
  [stat, wapp]=useApp(app);
  if stat{1}~=1 || isempty(wapp)
    error('Error finding requested app');
  end
  if exist([wapp,'Apps/',app,'/',app,'.fig'],'file')
    evalc(['FL.Gui.(app)=',app]);
  elseif exist([wapp,'Apps/',app,'/',app,'.m'],'file')
    run([wapp,'Apps/',app,'/',app]);
  else
    if ~exist([wapp,'Apps/',app,'/',app],'file')
      errordlg('Application not found','Run user app error');
      return
    end 
    evalc(['!',wapp,'Apps/',app,'/',app,'&']);
  end
catch ME
  FL.lastError=ME;
  if isfield(FL,'Gui') && isfield(FL.Gui,'main')
    errordlg(ME.message,'Error running requested application');
  else
    warning('Lucretia:Floodland:trustedApps','Error running requested application');
  end
  bufferFlush;
end


% --- non-mover cav bpm tools gui
function pushbutton4_Callback(hObject, eventdata, handles) %#ok<*INUSD,*DEFNU>
% hObject    handle to pushbutton4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

appLaunch('bpmcal');


% --- Executes on button press in pushbutton5.
function pushbutton5_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton5 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

appLaunch('multiKnobs');


% --- Executes on button press in pushbutton6.
function pushbutton6_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton6 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
appLaunch('extDispersion');


% --- Executes on button press in pushbutton7.
function pushbutton7_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton7 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
appLaunch('bpmquad_offset');


% --- Executes on button press in pushbutton8.
function pushbutton8_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton8 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
appLaunch('wire_scanner');


% --- Executes on button press in pushbutton9.
function pushbutton9_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton9 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global FL
FL.Gui.FlBeamModelGUI=FlBeamModelGUI;


% --- Executes on button press in pushbutton10.
function pushbutton10_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton10 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
appLaunch('transfert_matrices_check');


% --- Executes on button press in pushbutton11.
function pushbutton11_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton11 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
appLaunch('trajectory_correction');


% --- Executes on button press in pushbutton12.
function pushbutton12_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton12 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
appLaunch('twiss_gui');


% --- Executes on button press in pushbutton13.
function pushbutton13_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton13 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
appLaunch('ipTwiss');

% --- Executes on button press in pushbutton14.
function pushbutton14_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton14 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
appLaunch('ipWireFit');

function [stat, output] = FloodlandAccessServer(opt,arg1,arg2)
% EPICS CA Access Control Server
global FL GIRDER PS INSTR 
persistent t_count timeout_counter last_reqid

% ------------------------------------------------------------------------------
% 15-Feb-2010, M. Woodley
%    Add output arg to PSTrim (I=[Inow;Inew])
% ------------------------------------------------------------------------------

stat{1}=1; stat{2}=[];
output=[];

switch lower(opt)
%% Initialisation
  case 'init'
    disp('Initialising Floodland AccessServer...')
    output=[];
    % Timeout scale options (display in hours)
    FL.AS.timeoutOpt = [1/3600 1 24];
    % setup server tcp socket
    sockrw('init-casserv');
%% Deal with command if one found in read buffer
  case 'command'
    output=[];
    if ~exist('arg1','var') || ~ischar(arg1)
      stat{1}=-1;
      stat{2}='Incorrect arguments given to FloodlandAccessServer';
      return
    end % arg check
    % Client host name
    command=arg1;
    t=regexp(command,'^(\S+):','tokens');
    if isempty(t); command=['generic: ',command]; t{1}{1}='generic'; end;
    switch lower(t{1}{1})
      case 'remove'
        output='1';
        % get Gui listbox entry and remove
        t=regexp(command,'remove:\s+(.+)','tokens');
        reqID=t{1}{1};
        if strcmpi(reqID,'all')
          [stat, reqID]=sockrw('get_user');
          if stat{1}~=1; return; end;
        end
        lbstr=get(FL.Gui.as.listbox1,'String');
        if ~iscell(lbstr); lbstr={lbstr}; end;
        ireq_list=[];
        for ireq=1:length(lbstr)
          t=regexp(lbstr{ireq},'(.+)\s:\s(.+)\s:\s(.+):(.+)\s:\s(.+)','tokens');
          if ~isempty(t) && strcmp(reqID,t{1}{4})
            ireq_list(end+1)=ireq;
          elseif ~isempty(t) && strcmp(reqID,t{1}{3})
            ireq_list(end+1)=ireq;
          end % if find req string
        end % for ireq
        set(FL.Gui.as.listbox1,'Value',ireq_list)
        FAS_main('pushbutton1_Callback',FL.Gui.as.pushbutton1, [], FL.Gui.as);
        return
      case 'testupdateinstrdataarray'
        reqStr=regexp(command,'\[.*','match');
        if isempty(reqStr)
          stat{1}=-1;
          stat{2}='Incorrect format for testUpdateInstrDataArray command';
          return
        end
        ireq=str2num(reqStr{1}); %#ok<ST2NM>
        ni=0; bpmarray=zeros(length(ireq),length(INSTR{ireq(1)}.DataArray(1,:))*3);
        for instind=ireq
          ni=ni+1;
          bpmarray(ni,:)=[INSTR{instind}.DataArray(1,:) INSTR{instind}.DataArray(2,:) ...
            INSTR{instind}.DataArray(3,:)];
        end
        output=num2str(bpmarray(:)',FL.comPrec);
      case 'testupdateinstrerr'
        reqStr=regexp(command,'\[.*','match');
        if isempty(reqStr)
          stat{1}=-1;
          stat{2}='Incorrect format for testUpdateInstrDataArray command';
          return
        end
        ireq=str2num(reqStr{1}); %#ok<ST2NM>
        ni=0; bpmerr=zeros(1,length(ireq)*3);
        for instind=ireq
          bpmerr(1+ni*3)=INSTR{instind}.DataErr(1);
          bpmerr(2+ni*3)=INSTR{instind}.DataErr(2);
          bpmerr(3+ni*3)=INSTR{instind}.DataErr(3);
          ni=ni+1;
        end
        output=num2str(bpmerr,FL.comPrec);
      case 'testupdate'
        reqStr=regexp(command,'\{.*\}','match');
        if isempty(reqStr)
          stat{1}=-1;
          stat{2}='Incorrect format for testUpdate command';
          return
        end % format check
        try
          eval(['request=',reqStr{1},';']);
          if isempty(request) || ~iscell(request) || length(request)~=3 %#ok<USENS>
            error('format error');
          end % format check    
          % Fill data request from globals
          grpstr={'GIRDER{request{igrp}(iele)}.MoverPos' ...
            'PS(request{igrp}(iele)).Ampl' 'INSTR{request{igrp}(iele)}.Data'};
          output='{';
          for igrp=1:3
            output=[output ' {'];
            if isempty(request{igrp})
              output=[output '[]'];
            else
              for iele=1:length(request{igrp})
                try
                  eval(['valstr=',grpstr{igrp},';'])
                catch
                  valstr=NaN;
                end % try/catch
                output=[output ' [',num2str(valstr,FL.comPrec),']'];
              end % for iele
            end % if empty request for this grpstr
            output=[output ' }'];
          end % for igrp
          output=[output ' }'];
          % -- Send B field list (or changes)
          [stat Bdata Bdim PSdata PSdim] = latticeStatus;
          Bchanges=find(cellfun(@(x) ~isempty(x),Bdata));
          PSchanges=find(cellfun(@(x) ~isempty(x),PSdata));
          if ~isempty(Bchanges)
            output=[output '::B::'];
            for ib=Bchanges
              output=[output num2str(Bdim(ib),FL.comPrec),'='];
              for ibd=1:length(Bdata{ib})
                output=[output num2str(Bdata{ib}(ibd),FL.comPrec)];
                if ibd==length(Bdata{ib})
                  output=[output ']'];
                else
                  output=[output ','];
                end
              end
            end
          end
          % -- Send PS SetPt list (or changes)
          if ~isempty(PSchanges)
            output=[output '::PS::'];
            for ips=PSchanges
              output=[output num2str(PSdim(ips),FL.comPrec),'='];
              for ips2=1:length(PSdata{ips})
                output=[output num2str(PSdata{ips}(ips2),FL.comPrec)];
                if ips2==length(PSdata{ips})
                  output=[output ']'];
                else
                  output=[output ','];
                end
              end
            end
          end
        catch
          stat{1}=-1;
          stat{2}=['Incorrect format for testUpdate command: ',lasterr]; %#ok<LERR>
          return
        end % try/catch format check
      case 'pstrim'
        t=regexp(command,'\s+(\S+)','tokens');
        [stat, userID]=sockrw('get_user'); if stat{1}~=1; return; end;
        if mod(length(t),2)
          stat{1}=-1; stat{2}='Require even # args to pstrim'; return;
        end % if not even # args
        % unpack trim values
        ips=zeros(1,length(t)/2);
        for icmd=1:2:length(t)
          ips((icmd+1)/2)=str2double(t{icmd}{1});
          setpt_cmd{(icmd+1)/2}=str2double(t{icmd+1}{1});
          if any(isnan([ips((icmd+1)/2) setpt_cmd{(icmd+1)/2}]))
            stat{1}=-1;stat{2}='Argument format error';return;
          end % if bad num string
        end % for icmd
        % Check access rights and trim if have them
        if isfield(FL,'AS') && isfield(FL.AS,userID)
          reqs=fieldnames(FL.AS.(userID));
          for ireq=1:length(reqs)
            if isempty(regexp(reqs{ireq},'^req', 'once' )) || ... 
                ~isfield(FL.AS.(userID).(reqs{ireq}),'PS') || ... 
                ~all(ismember(ips,FL.AS.(userID).(reqs{ireq}).PS))
              stat{1}=-1;stat{2}='No access permission for this pstrim request';
            else
              stat{1}=1;stat{2}='';
              break
            end % if access rights
          end % for ireq
          if stat{1}~=1; return; end;
        else
          stat{1}=-1;stat{2}='No access permission for this pstrim request';return;
        end % if userID field
        [PS(ips).SetPt]=setpt_cmd{:};
        [stat,I]=PSTrim(ips,1);
        if stat{1}==1
          output=['1 ',num2str(reshape(I,1,[]),FL.comPrec)];
        else
          output=['-1 ',stat{2}];
        end % if stat==1
      case 'movertrim'
        t=regexp(command,'\s+(\S+)','tokens');
        [stat, userID]=sockrw('get_user'); if stat{1}~=1; return; end;
        if mod(length(t),7)
          stat{1}=-1; stat{2}='Require multiple of 7 # args to movertrim'; return;
        end % if not even # args
        % Unpack girder id's
        ig=zeros(1,length(t)/7);
        for icmd=1:7:length(t)
          ig((icmd+6)/7)=str2double(t{icmd}{1});
        end % for icmd
        % Check access rights and trim if have them
        if isfield(FL,'AS') && isfield(FL.AS,userID)
          reqs=fieldnames(FL.AS.(userID));
          for ireq=1:length(reqs)
            if isempty(regexp(reqs{ireq},'^req', 'once' )) || ... 
                ~isfield(FL.AS.(userID).(reqs{ireq}),'GIRDER') || ... 
                ~all(ismember(ig,FL.AS.(userID).(reqs{ireq}).GIRDER))
              stat{1}=-1;stat{2}='No access permission for this movertrim request';
            else
              stat{1}=1; stat{2}='';
              break
            end % if access rights
          end % for ireq
          if stat{1}~=1; return; end;
        else
          stat{1}=-1;stat{2}='No access permission for this movertrim request';return;
        end % if userID field
        % Unpack moves
        for icmd=1:7:length(t)
          DOF=GIRDER{ig((icmd+6)/7)}.Mover;
          for idof=1:length(DOF)
            GIRDER{ig((icmd+6)/7)}.MoverSetPt(idof)=str2double(t{icmd+DOF(idof)}{1});
          end % for idof
        end % for icmd
        stat=MoverTrim(ig,1);
        if stat{1}==1
          output='1';
        else
          output=['-1 ',stat{2}];
        end % if stat==1
      case 'access-request' % format: access_request: PS|GIRDER#NUM# -> return reqID
        t=regexp(command,'\s+(\S+)','tokens');
        [stat userID]=sockrw('get_user','cas');
        if length(t)==1 && length(t{1}{1})>=3 && isequal(t{1}{1}(1:3),'req')
          reqID=t{1}{1};
          if isfield(FL.AS,userID) && isfield(FL.AS.(userID),reqID)
            if isfield(FL.AS.(userID).(reqID),'access')
              output=num2str(FL.AS.(userID).(reqID).access);
            else
              output=['Access error for ID: ',userID,':',reqID];
            end % if access
          end % if uerID + reqID
          return
        else
          if isempty(last_reqid)
            last_reqid=1;
          else
            last_reqid=last_reqid+1;
          end % if empty last_reqid
          reqID=['req',num2str(last_reqid)];
        end % if one arg
        for iar=1:length(t)
          if ~isempty(findstr(t{iar}{1},'PS'))
            if length(t{iar}{1})==2 || isnan(str2double(t{iar}{1}(3:end)))
              stat{1}=-1; stat{2}='access-request format error'; return;
            end % format check
            if ~isfield(FL.AS,userID) || ~isfield(FL.AS.(userID),reqID) || ~isfield(FL.AS.(userID).(reqID),'PS')
              FL.AS.(userID).(reqID).PS=str2double(t{iar}{1}(3:end));
            elseif ~ismember(str2double(t{iar}{1}(3:end)),FL.AS.(userID).(reqID).PS)
              FL.AS.(userID).(reqID).PS=[FL.AS.(userID).(reqID).PS str2double(t{iar}{1}(3:end))];
            end % if first addition
          elseif ~isempty(findstr(t{iar}{1},'GIRDER'))
            if length(t{iar}{1})==6 || isnan(str2double(t{iar}{1}(7:end)))
              stat{1}=-1; stat{2}='access-request format error'; return;
            end % format check
            if ~isfield(FL.AS,userID) || ~isfield(FL.AS.(userID),reqID) || ~isfield(FL.AS.(userID).(reqID),'GIRDER')
              FL.AS.(userID).(reqID).GIRDER=str2double(t{iar}{1}(7:end));
            elseif ~ismember(str2double(t{iar}{1}(3:end)),FL.AS.(userID).(reqID).GIRDER)
              FL.AS.(userID).(reqID).GIRDER=[FL.AS.(userID).(reqID).GIRDER str2double(t{iar}{1}(7:end))];
            end % if first addition
          end % if PS | GIRDER
        end % for ipv
        % Request list to be added, and wait
        procReq(userID,reqID,arg2);
        % Return access permissions of sent list (true = accepted)
        output=reqID;
      otherwise % generic command
        try
          data=[];
          t=regexp(command,'\s+(.+)','tokens');
          if isempty(t)
            t{1}{1}=command;
          end
          if ~isempty(findstr(lower(t{1}{1}),'pstrim')) || ~isempty(findstr(lower(t{1}{1}),'movertrim'))
            stat{1}=-1; stat{2}=['can''t parse this command: ',t{1}{1}]; return;
          end % don't allow a pstrim or movertrim command like this
          try
            eval(['[stat,data]=',t{1}{1},';']);
          catch
            if ~isempty(findstr(lasterr,'Too many output arguments')) %#ok<LERR>
              eval(['stat=',t{1}{1},';']);
            else
              rethrow(lasterror); %#ok<LERR>
            end % if too many output args last time
          end % 
          encodeData=[];
          if iscell(data) 
            for idata=1:length(data)
              if iscell(data{idata})
                for jdata=1:length(data{idata})
                  encodeData=[encodeData num2str(data{idata}{jdata}(:)',FL.comPrec) ' '];
                end % for jdata
              else
                encodeData=[encodeData num2str(data{idata}(:)',FL.comPrec) ' '];
              end % if data{} is cell
            end % for idata
          else
            encodeData=num2str(data(:)',FL.comPrec);
          end % if iscell(data)
          [stat, output]=FloodlandAccessServer('encode',stat,encodeData); 
        catch
          stat{1}=-1; stat{2}=[command 'error: ' lasterr]; encodeData=[]; %#ok<LERR>
          [stat, output]=FloodlandAccessServer('encode',stat,encodeData); 
        end % try/catch
    end % switch command
  case 'encode' % encode stat and output string for output
    stat{1}=1;
    if arg1{1}~=1
      output=[num2str(arg1{1}) ' ''' arg1{2} ''' ' arg2];
    else
      output=['1 ' arg2];
    end % if stat{1}~=1
  case 'decode' % decode stat and output for CAS server response
    output=[]; str=[];
    if iscell(arg1)
      for iarg=1:length(arg1)
        str=[str arg1{iarg}];
      end % for iarg
    else
      str=arg1;
    end % if iscell(iarg)
    stat{1}=str2double(regexp(str,'^-?\d+','match','once'));
    if stat{1}<0; stat{2}=cell2mat(regexp(str,'\s+''(.+)''','tokens','once')); return; end;
    statind=regexp(str,'\s+''(.+)''','once','end');
    if stat{1}~=1 && ~isempty(statind)
      if statind+1<=length(arg1)
        output=arg1(statind+1:end);
      end % if stat{2} not last string entry
      stat{2}=regexp(str,'\s+''(.+)''','tokens','once');
    else
      output=regexp(str,'\s+(.+)','match','once');
      output=output(2:end);
    end % if warning
  case 'run' % Code to run in timer
    % Update timeout option
    approvedReq=get(FL.Gui.as.listbox1,'String');
    if ~isempty(approvedReq) && (iscell(approvedReq) && ~isempty(approvedReq{1}))
      if ~iscell(approvedReq)
        approvedReq={approvedReq};
      end % if not cell, make it so
      for ireq=1:length(approvedReq)
        t=regexp(approvedReq{ireq},'(.+)\s:\s(.+)\s:\s(.+):(.+)\s:\s(.+)','tokens');
        if isempty(findstr('No Timeout',t{1}{5}))
          if isempty(t_count) || length(t_count)<ireq || isempty(timeout_counter)
            t_count(ireq)=str2double(t{1}{5});
            timeout_counter=toc;
          else
            t_count(ireq)=t_count(ireq)-(toc-timeout_counter)/3600;
          end % if t_count not set
          if t_count(ireq)<0 % remove entry
            set(FL.Gui.as.listbox1,'Value',ireq)
            FAS_main('pushbutton1_Callback',FL.Gui.as.pushbutton1, [], FL.gui.as);
          else % update timeout time str
            approvedReq{ireq}=regexprep(approvedReq{ireq},'(.+\s:\s.+\s:\s.+:.+\s:\s)(.+)',sprintf('$1 %.2f',t_count));
            set(FL.Gui.as.listbox1,'String',approvedReq{ireq});
          end % if timed-out
        end % if timeout
      end % for ireq
    end % if approvedReq list poopulated
    timeout_counter=toc;
    drawnow('expose');
  case 'errortrap'
    errordlg('Server Error in main function - shutting down','FloodlandAccessServer');
    LE=lasterror; %#ok<LERR>
    disp(LE.message); %#ok<LERR>
    if ~isempty(LE.stack)
      for istack=1:length(LE.stack)
        LE.stack(istack,1)
      end % for istack
    end % for length LE.stack
  case 'start'
    set(FL.Gui.as.pushbutton7,'BackgroundColor','green');
    set(FL.Gui.as.pushbutton7,'String','Server Running');
  case 'stop'
    % Flag server stopped in FAS
    if isfield(FL,'Gui') && isfield(FL.Gui,'as')
      if ishandle(FL.Gui.as.figure1)
        set(FL.Gui.as.pushbutton7,'BackgroundColor','red');
        set(FL.Gui.as.pushbutton7,'String','Server Stopped');
      end % if gui handle valid
    end % if as gui there
    % Stop server connection timer if running
    if isfield(FL.AS,'t_connect') && isequal(FL.AS.t_connect.Running,'on')
      stop(FL.AS.t_connect);
    end % if connect server timer running
    % Send disconnect messages to any connected clients and delete
    disp('closing server connections...')
    srvs={'AS'};
    ssrvs={'cas'};
    socks={'socket' 'servSocket'};
    for isrv=1:length(srvs)
      if isfield(FL,srvs{isrv})
        users=fieldnames(FL.(srvs{isrv}));
        for iuser=1:length(users)
          if strcmp(users{iuser}(1:4),'user')
            for isock=1:length(socks)
              if isfield(FL.(srvs{isrv}).(users{iuser}),socks{isock}) && ...
                  ~FL.(srvs{isrv}).(users{iuser}).(socks{isock}).isClosed
                try
                  % Send disconnect message and close socket
                  sockrw('set_user',ssrvs{isrv},users{iuser,1});
                  sockrw('writechar',ssrvs{isrv},'disconnect');
                  pause(1)
                  FL.(srvs{isrv}).(users{iuser}).socket.close;
                  FL.(srvs{isrv})=rmfield(FL.(srvs{isrv}),users{iuser});
                catch
                  FL.(srvs{isrv})=rmfield(FL.(srvs{isrv}),users{iuser});
                  continue
                end % try/catch
              end % if ~closed
            end % for isock
          end % if user field
        end % for iuser
      end % if srv field exists
    end % for isrv
  otherwise % Unkown command
    errordlg('Unknown command received','FloodlandAccessServer');
    error('Unknown command received');
end % switch opt

%% Local functions

function procReq(userID,reqID,remoteIP)
% Process requests for PV access
global FL
newReq=get(FL.Gui.as.listbox2,'String');
approvedReq=get(FL.Gui.as.listbox1,'String');

% Get request options from gui
if get(FL.Gui.as.togglebutton2,'Value')
  timeout=str2double(get(FL.Gui.as.edit1,'String'))*FL.AS.timeoutOpt(get(FL.Gui.as.popupmenu2,'Value'));
else
  timeout='No Timeout';
end % if timeout requested
if get(FL.Gui.as.togglebutton1,'Value');
  autoResponse=get(FL.Gui.as.popupmenu1,'Value');
else
  autoResponse=false;
end % if autoresponse set
if autoResponse==2
  FL.AS.(userID).(reqID).access=false;
  return
end % deny if autoresponse==2
if autoResponse % Add to allowed list
  approvedReq{end+1}=[datestr(now),' : ',remoteIP,' : ',userID,':',reqID,' : ',num2str(timeout)];
  FL.AS.(userID).(reqID).access=true;
else % add to request list
  newReq{end+1}=[datestr(now),' : ',remoteIP,' : ',userID,':',reqID,' : ',num2str(timeout)];
end % if autoResponse
% write lists to GUI
set(FL.Gui.as.listbox1,'String',approvedReq);
set(FL.Gui.as.listbox1,'Value',1);
set(FL.Gui.as.listbox2,'String',newReq);
set(FL.Gui.as.listbox2,'Value',1);
drawnow('expose');
% Wait for request queue to empty
while ~isfield(FL.AS.(userID).(reqID),'access')
  pause(1);
end % while full request box


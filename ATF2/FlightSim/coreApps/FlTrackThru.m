function [stat instdata] = FlTrackThru(istart,ifinish,bpmaveData)
% [stat instdata] = FlTrackThru(istart,ifinish,bpmaveData)
% Fill Lucretia trackthru-like instdata structure from Floodland INSTR
% global or bpmaveData (taken with FlHwUpdate bpmave function)
% For direct comparison of Floodland bpm data with Lucretia tracked data
% -----------------------------
% istart, ifinish BEAMLINE indexes to fill instdata between
% if pass bpmaveData array (output from FlHwUpdate('bpmave')), then fill
% instdata from this, else fill from internal Floodland INSTR global
% structure
% ----------------------------
% Output stat (Lucretia status return) and instdata (same format as
% returned from TrackThru)
global INSTR BEAMLINE
persistent imin lastinds
stat{1}=1;
if isempty(imin) || ~isequal(lastinds,[istart ifinish])
  nIndex=cellfun(@(x) x.Index>=istart&x.Index<=ifinish,INSTR);
  indx1=cellfun(@(x) x.instdata_ind(1),INSTR(nIndex));
  indx2=cellfun(@(x) x.instdata_ind(2),INSTR(nIndex));
  imin(1)=min(indx2(indx1==1))-1;
  if(any(indx1==2)); imin(2)=min(indx2(indx1==2))-1; end;
  if(any(indx1==3)); imin(3)=min(indx2(indx1==3))-1; end;
  lastinds=[istart ifinish];
end % if imin not calculated yet or different inds
for ind=1:length(INSTR)
  if INSTR{ind}.Index>=istart && INSTR{ind}.Index<=ifinish
    ind1=INSTR{ind}.instdata_ind(1);
    ind2=INSTR{ind}.instdata_ind(2)-imin(ind1);
    if nargin>2 % get data from bpmaveData (from FlHwUpdate bpmave function)
      instdata{ind1}(ind2).x=bpmaveData{1}(1,ind);
      instdata{ind1}(ind2).y=bpmaveData{1}(2,ind);
      instdata{ind1}(ind2).xrms=bpmaveData{1}(4,ind);
      instdata{ind1}(ind2).yrms=bpmaveData{1}(5,ind);
    else
      instdata{ind1}(ind2).x=INSTR{ind}.Data(1);
      instdata{ind1}(ind2).y=INSTR{ind}.Data(2);
    end % if nargin
    instdata{ind1}(ind2).Index=INSTR{ind}.Index;
    instdata{ind1}(ind2).S=BEAMLINE{INSTR{ind}.Index}.S;
    instdata{ind1}(ind2).Pmod=BEAMLINE{INSTR{ind}.Index}.P;
  end % if INSTR range correct
end % for ind
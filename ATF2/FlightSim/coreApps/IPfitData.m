function varargout = IPfitData(varargin)
% IPFITDATA M-file for IPfitData.fig
%      IPFITDATA, by itself, creates a new IPFITDATA or raises the existing
%      singleton*.
%
%      H = IPFITDATA returns the handle to a new IPFITDATA or the handle to
%      the existing singleton*.
%
%      IPFITDATA('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in IPFITDATA.M with the given input arguments.
%
%      IPFITDATA('Property','Value',...) creates a new IPFITDATA or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before IPfitData_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to IPfitData_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help IPfitData

% Last Modified by GUIDE v2.5 29-Nov-2012 07:40:35

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @IPfitData_OpeningFcn, ...
                   'gui_OutputFcn',  @IPfitData_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before IPfitData is made visible.
function IPfitData_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to IPfitData (see VARARGIN)

% Choose default command line output for IPfitData
handles.output = handles;

% Update handles structure
guidata(hObject, handles);

% Clear data buffer
IPfitDataDisplay('cleardata');

% Get and display last calibration date
caldate = getIPData('getcaldate');
if ~isempty(caldate)
  set(handles.text15,'String',datestr(caldate));
else
  set(handles.text15,'String','--- No Cal ---')
end

% Get BPM selection
bpmuse = getIPData('getbpmuse');
for ibpm=1:5
  set(handles.(sprintf('checkbox%d',ibpm)),'Value',bpmuse(ibpm))
end

% Get Waist offset data
waistoff = getIPData('getdl');
set(handles.text16,'String',num2str(waistoff(end)*1e3,6))


% UIWAIT makes IPfitData wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = IPfitData_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes when user attempts to close figure1.
function figure1_CloseRequestFcn(hObject, eventdata, handles) %#ok<*INUSL,*DEFNU>
% hObject    handle to figure1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

guiCloseFn('IPfitData',handles);



function edit1_Callback(hObject, eventdata, handles)
% hObject    handle to edit1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit1 as text
%        str2double(get(hObject,'String')) returns contents of edit1 as a double
npulse=str2double(get(hObject,'String'));
nave=str2double(get(handles.edit2,'String'));
if npulse<nave
  set(hObject,'String',num2str(nave))
end
getIPData('setbufsize',npulse)

% --- Executes during object creation, after setting all properties.
function edit1_CreateFcn(hObject, eventdata, handles) %#ok<*INUSD>
% hObject    handle to edit1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit2_Callback(hObject, eventdata, handles)
% hObject    handle to edit2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit2 as text
%        str2double(get(hObject,'String')) returns contents of edit2 as a double
nave=str2double(get(hObject,'String'));
npulse=str2double(get(handles.edit1,'String'));
if nave>npulse
  set(hObject,'String',num2str(npulse))
end


% --- Executes during object creation, after setting all properties.
function edit2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit3_Callback(hObject, eventdata, handles)
% hObject    handle to edit3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit3 as text
%        str2double(get(hObject,'String')) returns contents of edit3 as a double


% --- Executes during object creation, after setting all properties.
function edit3_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in pushbutton1.
function pushbutton1_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on button press in pushbutton2.
function pushbutton2_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
getIPData('resetcal');
set(handles.text15,'String','--- No Cal ---')
set(handles.text16,'String','0.0')

% --- Executes on button press in pushbutton3.
function pushbutton3_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
IPfitDataDisplay('setmarker','x');

% --- Executes on button press in pushbutton4.
function pushbutton4_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
IPfitDataDisplay('setmarker','y');


% --- Executes on button press in checkbox1.
function checkbox1_Callback(hObject, eventdata, handles)
% hObject    handle to checkbox1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

bpmuse=getIPData('getbpmuse');
bpmuse(1)=get(hObject,'Value');
if sum(bpmuse)<2
  bpmuse(1)=true;
  set(hObject,'Value',true)
end
getIPData('bpmsel',bpmuse);


% --- Executes on button press in checkbox2.
function checkbox2_Callback(hObject, eventdata, handles)
% hObject    handle to checkbox2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

bpmuse=getIPData('getbpmuse');
bpmuse(2)=get(hObject,'Value');
if sum(bpmuse)<2
  bpmuse(2)=true;
  set(hObject,'Value',true)
end
getIPData('bpmsel',bpmuse);


% --- Executes on button press in checkbox3.
function checkbox3_Callback(hObject, eventdata, handles)
% hObject    handle to checkbox3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

bpmuse=getIPData('getbpmuse');
bpmuse(3)=get(hObject,'Value');
if sum(bpmuse)<2
  bpmuse(3)=true;
  set(hObject,'Value',true)
end
getIPData('bpmsel',bpmuse);


% --- Executes on button press in checkbox4.
function checkbox4_Callback(hObject, eventdata, handles)
% hObject    handle to checkbox4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

bpmuse=getIPData('getbpmuse');
bpmuse(4)=get(hObject,'Value');
if sum(bpmuse)<2
  bpmuse(4)=true;
  set(hObject,'Value',true)
end
getIPData('bpmsel',bpmuse);


% --- Executes on button press in checkbox5.
function checkbox5_Callback(hObject, eventdata, handles)
% hObject    handle to checkbox5 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

bpmuse=getIPData('getbpmuse');
bpmuse(5)=get(hObject,'Value');
if sum(bpmuse)<2
  bpmuse(5)=true;
  set(hObject,'Value',true)
end
getIPData('bpmsel',bpmuse);


% --- Executes on button press in pushbutton5.
function pushbutton5_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton5 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
if ~strcmp(questdlg('Calibrate Offsets?'),'Yes'); return; end;
pause(0.5)
mhan=msgbox('Performing fit to data, please wait...');
pause(0.5)
npulse=str2double(get(handles.edit2,'String'));
caldata=getIPData('cal',npulse);
if ishandle(mhan); delete(mhan); end;
set(handles.text16,'String',num2str(caldata(end)*1e3))
caldate = getIPData('getcaldate');
if ~isempty(caldate)
  set(handles.text15,'String',datestr(caldate));
else
  set(handles.text15,'String','--- No Cal ---')
end


% --- Executes on button press in pushbutton6.
function pushbutton6_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton6 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)



% --------------------------------------------------------------------
function Tools_Callback(hObject, eventdata, handles)
% hObject    handle to Tools (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --------------------------------------------------------------------
function waistToBpm_Callback(hObject, eventdata, handles)
% hObject    handle to waistToBpm (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global FL
FL.Gui.IPBPM_waistShift=IPBPM_waistShift;


% --- Executes during object creation, after setting all properties.
function figure1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to figure1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called


% --------------------------------------------------------------------
function resJitAnal_Callback(hObject, eventdata, handles)
% hObject    handle to resJitAnal (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global FL
FL.Gui.IPfitData_resjitanal=IPfitData_resjitanal;


% --- Executes on button press in togglebutton1.
function togglebutton1_Callback(hObject, eventdata, handles)
% hObject    handle to togglebutton1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of togglebutton1

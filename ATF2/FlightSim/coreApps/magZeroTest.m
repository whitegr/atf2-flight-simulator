function [psvals ipsvals initvals magnames]=magZeroTest
% [psvals ipsvals]=magZeroTest
% Check reported adc readings on magnets set to zero
global BEAMLINE PS FL

% Get list of magnets to check (all HAPS PS's)
mags=[];
for ips=1:length(FL.HwInfo.PS)
  if ~isempty(FL.HwInfo.PS(ips).pvname) && ~isempty(regexp(FL.HwInfo.PS(ips).pvname{1}{1},'^PS','once'))
    mags(end+1)=ips;
  end
end

% Magnet name list
for imag=1:length(mags)
  magnames{imag}=BEAMLINE{PS(mags(imag)).Element(1)}.Name;
end

% Get initial vals
initvals=[PS(mags).Ampl];

% Set to zero and get pv names
disp('Setting Magnets to zero...')
pv=cell(length(mags),1); ipv=0;
for ips=mags
  ipv=ipv+1;
  PS(ips).SetPt=0;
  pv{ipv,1}=FL.HwInfo.PS(ips).pvname{1}{1};
  if isempty(pv{ipv}); error('No PV name for PS: %d',ips); end;
end
PSTrim(mags,1);

% Wait for magnets to stop moving
waitforstop(mags,10);

% Get current and PS readback values
disp('Getting readback values...')
psvals=[PS(mags).Ampl];
ipsvals=lcaGet(pv);

% Return initial vals
disp('Returning to initial Values...')
ival=0;
for ips=mags
  ival=ival+1;
  PS(ips).SetPt=initvals(ival);
end
PSTrim(mags,1);

% -----------------------------------------------------------

function waitforstop(mags,tol)
global PS
% Wait for mags to stop moving at tolerance of tol %
magvals=[PS(mags).Ampl];
while any(abs(magvals-[PS(mags).Ampl])./magvals)>(tol/100)
  magvals=[PS(mags).Ampl];
  pause(2);
end
pause(20);
  
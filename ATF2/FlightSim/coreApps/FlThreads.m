function [stat output] = FlThreads(cmd,varargin)
% FLTHREADS Handles creation/deletion and handling of Floodland threads
% 
%    Default: stat = FlThreads() = FlThreads('update')
%    must call this once before other uses of threads
%    also updates thread status
%
%    stat = FlThreads('startApp',ithread,appName)
%    Run application <appName> (string) in thread <ithread>
% 
%    stat = FlThreads('start',ithread)
%    If not already running, starts thread <ithread> and makes ready for
%    running apps
% 
%    stat = FlThreads('stop',ithread)
%    Commands thread <ithread> to stop and shutdown (can take a long time if
%    the thread is busy), command is non-blocking, if command does not have
%    effect in desired time, use kill command
% 
%    stat = FlThreads('kill',ithread)
%    Kills the running thread <ithread> process
% 
%    [stat user] = FlThreads('setStatus',status)
%    Method for thread process to communicate with controlling Matlab session,
%    <status> should be a recognised status string
% 
%    stat = FlThreads('getStatus',ithread)
%    Method for controlling Matlab session to process thread status
% 
%    [stat output] = FlThreads('serverProc')
%    Designed to run in timer object to process incoming commands on thread
%    process (output of commands read over Floodland socket client returned in
%    output)
% 
%    stat = FlThread('threadErr',errorString)
%    Designed to process errors of serverProc running in timer object given
%    the error string <errorString>
% 
%    stat: standard Lucretia status return

stat{1}=1; output=[];

if ~exist('cmd','var') || isempty(cmd)
  cmd='update';
end % if cmd not passed or empty, assume init
switch lower(cmd)
  case 'update'
    stat=update;
  case 'start'
    if length(varargin)<1 || ~isnumeric(varargin{1})
      stat{1}=-1; stat{2}='Argument error'; return;
    end % if arg error
    stat=start(varargin{1});
  case 'stop'
    if length(varargin)<1 || ~isnumeric(varargin{1})
      stat{1}=-1; stat{2}='Argument error'; return;
    end % if arg error
    if nargin>2
      stat=stop(varargin{1},varargin{2});
    else
      stat=stop(varargin{1});
    end
  case 'kill'
    if length(varargin)<1 || ~isnumeric(varargin{1})
      stat{1}=-1; stat{2}='Argument error'; return;
    end % if arg error
    stat=kill(varargin{1});
  case 'setstatus'
    if length(varargin)<1 || ~ischar(varargin{1})
      stat{1}=-1; stat{2}='Argument error'; return;
    end % if arg error
    [stat output]=setStatus(varargin{1});
  case 'getstatus'
    if length(varargin)<1 || ~isnumeric(varargin{1})
      stat{1}=-1; stat{2}='Argument error'; return;
    end % if arg error
    stat=getStatus(varargin{1});
  case 'serverproc'
    [stat output] = serverProc;
  case 'startapp'
    if length(varargin)<2 || ~isnumeric(varargin{1}) || ~ischar(varargin{2})
      stat{1}=-1; stat{2}='Argument error'; return;
    end % if arg error
    stat = startApp(varargin{1},varargin{2});
  case 'threaderr'
    if length(varargin)<1 || ~ischar(varargin{1})
      stat{1}=-1; stat{2}='Argument error'; return;
    end % if arg error
    stat = threadErr(varargin{1});
  otherwise
    stat{1}=-1; stat{2}='Unknown command';
end
%% Local functions ------------------------------
function [stat output] = serverProc
global FL %#ok<NUSED>
output=[];
[stat cmdstr]=sockrw('readchar','fl');
if stat{1}==1 && ~isempty(cmdstr)
  if ~isempty(regexp(cmdstr,'^threadcmd:', 'once' ))
    cmd=regexprep(cmdstr,'^threadcmd:\s*','');
    try
      evalc(['[stat output] = ',cmd]);
    catch
      try
        if ~isempty(findstr(lasterr,'Too many output arguments')) %#ok<LERR>
          evalc(['stat = ',cmd]);
        end
      catch
        stat{1}=-1; stat{2}=['Thread server error: ',lasterr];
      end
    end
  end
elseif ~isempty(regexp(cmdstr,'^appcmd:', 'once' ))
  try
    app=regexprep(cmdstr,'^appcmd:\s*','');
    stat = useApp(app); % add app to path
    if stat{1}~=1; error(stat{2}); end;
    if exist([app,'.fig'],'file')
      evalc(['FL.Gui.(app)=',app]);
    elseif exist([app,'.m'],'file')
      try
        evalc(['[stat output]=',app]);
      catch
        if ~isempty(findstr(lasterr,'Too many output arguments')) %#ok<LERR>
          evalc(['stat=',app]);
        end
      end
    else
      error('Can only run .fig or .m file in thread')
    end
  catch
    stat{1}=-1; stat{2}=['Error running command: ',lasterr];
    sockrw('writechar','fl',['FlThreads(''threadErr,',stat{2},')']);
    sockrw('readchar','fl',10);
  end
end
function stat = update
global FL
persistent lastUpdate lastNum
stat{1}=1;
if isempty(lastUpdate)
  lastUpdate=toc;
end % if init lastUpdate
if isempty(lastNum)
  lastNum=-1;
end

% number of threads to use (save 1 for main server process)
if ~isfield(FL,'threads') || ~isfield(FL.threads,'num') || FL.threads.num>lastNum
  FL.threads.num=uint16(floor(str2double(getenv('FS_NUMTHREADS'))-1));
  if isnan(FL.threads.num); FL.threads.num=0; end;
  lastNum=FL.threads.num;
  if FL.threads.num
    for ithread=1:FL.threads.num
      if ~isfield(FL.threads,'status') || ithread>length(FL.threads.status)
        FL.threads.status{ithread}='stopped';
        FL.threads.userID{ithread}='unknown';
        FL.threads.psID(ithread)=0;
      end
    end
  end
end

% Update thread status
if FL.isthread && FL.threads.num
  updateTime=toc-lastUpdate;
  stat=setStatus(num2str(updateTime));
  if stat{1}~=1; stat{2}=['FlThreads error setting thread status: ',stat{2}]; end;
  lastUpdate=toc;
  return % nothing else to do if running as a thread
elseif isfield(FL.threads,'status')
  for ithread=1:length(FL.threads.status)
    stat=getStatus(ithread);
  end
  if stat{1}~=1
    stat{2}=['FlThreads error getting thread status: ',stat{2}];
  end % if err
  if stat{1}~=1; return; end;
end % if isthread
function stat = start(ithread)
global FL
stat{1}=1;
libpath=getenv('LD_LIBRARY_PATH');
syspath=getenv('PATH');
if ~strcmp(FL.threads.status{ithread},'running') && ~strcmp(FL.threads.status{ithread},'busy')
  FL.threads.status{ithread}='starting';
  % Start thread proccess
  if strcmp(FL.mode,'trusted')
    startStr=['xterm -T "ATF2 FS (Trusted Thread ',num2str(ithread),...
      ')" -n "ATF2 FS (Trusted Thread ',num2str(ithread),')"',...
      '-iconic -e /bin/bash -i -l -c "LD_LIBRARY_PATH=',libpath,';',...
      'PATH=',syspath,'; matlab -nosplash -nodesktop -r \"FlStart(''trustedthread'',',num2str(FL.SimMode),')\""'];
  else
    startStr=['xterm -T "ATF2 FS (Test Thread ',num2str(ithread),...
      ')" -n "ATF2 FS (Test Thread ',num2str(ithread),')"',...
      '-iconic -e /bin/bash -i -l -c "LD_LIBRARY_PATH=',libpath,';',...
      'PATH=',syspath,'; matlab -nosplash -nodesktop -r "FlStart(''testthread'',',...
      num2str(FL.SimMode),' ',FL.trusted.username,' ',FL.trusted.hostname,')"'];
  end
  evalc(['!',startStr,' &']);
  FL.threads.psID(ithread)=str2double(evalc(['!pgrep ',startStr]));
end
function stat = stop(ithread,force)
global FL
persistent askThreadShutdown
stat{1}=1;
if ~exist('force','var'); force=false; end;
if isempty(askThreadShutdown)
  askThreadShutdown=false;
end % if empty
% If command exectuted in thread- shutdown process
if FL.isthread
  setStatus('stopped');
  stop(FL.t_main);
  return
end
% Question decision to stop thread if busy
nonFreeThreads=find(cellfun(@(x) strcmp(x,'busy'),FL.threads.status));
if ismember(ithread,nonFreeThreads) && ~askThreadShutdown && ~force
  resp=questdlg(['Shut down busy thread ',num2str(ithread),' ?'],'Thread Shutdown Request',...
    'Yes','No','Always Yes','Always No','No');
  switch resp
    case {'No','Always No'}
      stat{1}=0; stat{2}='Busy Thread, not wanting shutdown';
      dostop=false;
    otherwise
      dostop=true;
  end % switch resp
  if strcmp(resp,'Always Yes')
    askThreadShutdown=1;
  elseif strcmp(resp,'Always No')
    askThreadShutdown=2;
  end % if always yes|no resp
elseif ismember(ithread,nonFreeThreads) && askThreadShutdown==1
  dostop=true;
elseif ismember(ithread,nonFreeThreads) && askThreadShutdown==2
  dostop=false;
else
  dostop=true;
end % if busy shutdown
% Issue stop command via server
if dostop
  try
    if ~strcmp(FL.threads.status{ithread},'stopping') && ~strcmp(FL.threads.status{ithread},'stopped')
      [stat cur_user]=sockrw('get_user','fl');
      stat=sockrw('set_user','fl',FL.threads.userID{ithread});
      if stat{1}~=1; stat{2}=['Error sending thread stop command: ',stat{2}]; return; end;
      stat=sockrw('writechar','fl','threadcmd: stop');
      if stat{1}~=1; stat{2}=['Error sending thread stop command: ',stat{2}]; return; end;
      stat=sockrw('set_user','fl',cur_user);
    end % if thread available and stopped
  catch
    stat{1}=-1; stat{2}=['Stop Thread failed: ',lasterr];
  end % try/catch
end
function stat = kill(ithread)
global FL
if isfield(FL.threads,'psID') && length(FL.threads.psID)>=ithread
  evalc(['!kill -9 ',num2str(FL.threads.psID(ithread))]);
end
stat=removeThread(ithread);
function stat = getStatus(ithread)
global FL
persistent lastUpdate lastUpdateVal
busyTime=60; % s
localBusyTime=5; % s
stat{1}=1;
tstat=FL.threads.status{ithread};
tstat_num=str2double(tstat);
if isnan(tstat_num)
  FL.threads.status{ithread}=tstat;
elseif length(lastUpdate)<ithread
  FL.threads.status{ithread}='running';
  lastUpdate(ithread)=toc;
  lastUpdateVal(ithread)=tstat_num;
elseif (toc-lastUpdate(ithread))>busyTime || tstat_num>localBusyTime
  FL.threads.status{ithread}='busy';
  if tstat_num~=lastUpdateVal(ithread)
    lastUpdate(ithread)=toc;
    lastUpdateVal(ithread)=tstat_num;
  end
  % if process if no longer exists, then assume stopped
  if isempty(evalc(['!ps --no-heading ',num2str(FL.threads.psID(ithread))]))
    FL.threads.status{ithread}='stopped';
  end
else
  FL.threads.status{ithread}='running';
  if tstat_num~=lastUpdateVal(ithread)
    lastUpdate(ithread)=toc;
    lastUpdateVal(ithread)=tstat_num;
  end
end
% if flagged as stopped- check ps has stopped, or kill it
if strcmp(FL.threads.status{ithread},'stopped')
  if ~isempty(evalc(['!ps --no-heading ',num2str(FL.threads.psID(ithread))]))
    evalc(['!kill -9 ',num2str(FL.threads.psID(ithread))]);
  end
end
function [stat user] = setStatus(status)
global FL
stat{1}=1; user='unknown';
if ~FL.isthread
  [stat user]=sockrw('get_user','fl');
  if stat{1}~=1; stat{2}=['Error getting user in setStatus: ',stat{2}]; return; end;
  ithread=find(cellfun(@(x) strcmp(x,user),FL.threads.userID), 1);
  if isempty(ithread)
    newthread=find(cellfun(@(x) strcmp(x,'starting'),FL.threads.status),1);
    if isempty(newthread); stat{1}=-1; stat{2}='cannot find new thread'; return; end;
    FL.threads.userID{newthread}=user;
    ithread=newthread;
  end
  FL.threads.status{ithread}=status;
  getStatus(ithread);
else
  try
    stat=sockrw('writechar','fl',['FlThreads(''setStatus(''',num2str(status),''');']); if stat{1}~=1; error(stat{2}); end;
    [stat resp]=sockrw('readchar','fl',30); if stat{1}~=1; error(stat{2}); end;
    stat=FloodlandAccessServer('decode',resp); if stat{1}~=1; error(stat{2}); end;
  catch
    stat{1}=-1; stat{2}=['Error setting status: ',stat{2}];
  end
end
function stat = startApp(ithread,cmd)
global FL
try
  if strcmp(FL.threads.status{ithread},'running')
    [stat cur_user]=sockrw('get_user','fl');
    stat=sockrw('set_user','fl',FL.threads.userID{ithread});
    if stat{1}~=1; error(stat{2}); end;
    stat=sockrw('writechar','fl',['appcmd: ',cmd]);
    if stat{1}~=1; error(stat{2}); end;
    stat=sockrw('set_user','fl',cur_user);
  end
catch
  stat{1}=-1; stat{2}=['Error sending thread command: ',lasterr]; return;
end
function stat = threadErr(errStr)
stat{1}=1;
warning(errStr,'Lucretia:Floodland:FlThreads:threadErr')
errordlg(errStr,'Thread Error Report')
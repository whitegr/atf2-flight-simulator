function stat = bufferFlush()
global FL
stat{1}=1;
try
  srvs={'AS' 'Server'};
  ssrvs={'cas' 'fl'};
  socks={'socket' 'servSocket'};
  for isrv=1:length(srvs)
    if isfield(FL,srvs{isrv})
      users=fieldnames(FL.(srvs{isrv}));
      for iuser=1:length(users)
        if strcmp(users{iuser}(1:4),'user')
          for isock=1:length(socks)
            if isfield(FL.(srvs{isrv}).(users{iuser}),socks{isock}) && ...
                ~FL.(srvs{isrv}).(users{iuser}).(socks{isock}).isClosed
              try
                % Send disconnect message and close socket
                stat=sockrw('set_user',ssrvs{isrv},users{iuser,1}); if stat{1}~=1; error(stat{2}); end;
                stat=sockrw('flush',ssrvs{isrv}); if stat{1}~=1; error(stat{2}); end;
              catch
                continue
              end % try/catch
            end % if ~closed
          end % for isock
        end % if user field
      end % for iuser
    end % if srv field exists
  end % for isrv
catch
  stat{1}=1;
  stat{2}=lasterr;
end
function [stat varargout] = FlBpmToolFn(cmd,varargin)
% FlBpmToolFn
% ===========
%   Return arguments:
%   stat = Lucretia status return
%   data = internal data structure, see 'GetData'
%   pars = internal parameters, see 'GetPars'
%
% [stat data] = FlBpmToolFn('Init')
%   Initialise function parameters and data
%
% [stat pars] = FlBpmToolFn('GetPars')
%   Get internal parameters:
%     pars.nbpm - # bpm readings to use when setting reference orbit
%     pars.qcut - charge cut value to use in bpm averaging request ( see
%       FlHwUpdate('bpmave') )
%     pars.bbasub - subtract BBA orbit in FlHwUpdate (BBA offsets stored in
%       BEAMLINE.ElecOffset fields for MONI Class
%
% [stat data] = FlBpmToolFn('GetData')
%   Get internal data structure:
%     data.ref data.referr (reference orbit data last used)
%     data.global.hw data.global.cal data.global.use data.global.useCal
%       - global bpm useage data (h/w present, calibration good/bad,
%       use=user flag to suggest the useage of this bpm or not,
%       useCal=perform calibration for this bpm or not)
%     data.(userTag).hw data.(userTag).cal data.(userTag).use
%     data.(userTag).useCal
%       - as above but for a user defined case
%     data.stripCal - contains stripline calibration constants and raw bpm
%       data used to generate them
%
% [stat data] = FlBpmToolFn('SetPars',newPars)
%   Set internal parameters
%     - set required newPars structure, same format and meaning as detailed
%     in 'GetPars' useage
%
% [stat data] = FlBpmToolFn('ServerSync')
%   Synchronise data to Server
%
% [stat data] = FlBpmToolFn('SetStatus',Tag,Type,InstrBpmInd,Status)
%   Set BPM status (can be read back with 'GetData')
%     - Tag = 'global' or user defined string
%     - Type = 'hw' | 'cal' | 'use' | 'useCal' (see 'GetData' useage for info)
%     - InstrBpmInd = INSTR array based index list of bpms to apply status
%       to
%     - Status = status flag boolean (true or false)
%
% [stat data] = FlBpmToolFn('AddUserTag',tagName)
%   Add a new user tag to bpm selection database
%     - tagName = name of tag to use - set data.(tagName) with intial
%       values the same as data.global
%
% [stat data] = FlBpmToolFn('RemoveUserTag',tagName)
%   Removes tagName from bpm selection database
%
% [stat data] = FlBpmToolFn('MeasureRefOrbit')
%   Measure reference orbit and store in memory,
%   Reference orbits saved into INSTR array:
%     INSTR{n}.ref - averaged Data array - same meaning as INSTR{n}.Data,
%       number of averaged orbits given by pars.nbpm, charge cut given by
%       pars.qcut
%     INSTR{n}.referr - rms of above
%
% [stat data] = FlBpmToolFn('SaveRefFile',filename)
%   Save current reference orbit data to file (filename in
%   latticeFiles/archive directory)
%
% [stat data] = FlBpmToolFn('SaveRefMemory')
%   Save current reference orbit data to memory location - restore with
%   'LoadRefMemory'
%
% [stat data] = FlBpmToolFn('SaveRefDefault')
%   Save current reference orbit data as default reference data (loaded on
%   startup of future flight simulator sessions on this installation
%   (server or client)
%
% [stat data] = FlBpmToolFn('SaveServerRef')
%   If client installation calling this function, causes the refernce data
%   to be written to the server memory
%
% [stat data] = FlBpmToolFn('LoadRefFile')
%   Load reference orbit to memory from File (file located in
%   latticeFiles/archive)
%
% [stat data] = FlBpmToolFn('LoadRefMemory')
%   Load reference orbit to memory from alternative memory location
%
% [stat data] = FlBpmToolFn('LoadRefDefault')
%   Load default reference orbit data into memory
%
% [stat data] = FlBpmToolFn('LoadServerRef')
%   Load reference orbit data into memory from server
%
% [stat data] = FlBpmToolFn('GenerateStriplineCal')
%   Get bpm data and create stripline calibration constants
%
% [stat convertedData data] = FlBpmToolFn('ApplyStriplineCal',bpmBufferData)
%   Apply stripline calibration to raw data bpmBufferData ( output from
%   FlHwUpdate('readbuffer') ) - convertedData returned in same format
%
% [stat data] = FlBpmToolFn('PlotStriplineCalData')
%   Stripline calibration data plots
%
% [stat] = FlBpmToolFn('normalize',handles)
%   normalize tmits to chosen ict
%
% stat = FlBpmToolFn('ZeroRef')
%    reset reference orbit to zero

persistent pars data
stat{1}=1; varargout={};

% Initialise parameters by default or if not done and not requested
if ~exist('cmd','var') || isempty(pars) && ~isequal(lower(cmd),'init')
  [pars data]=init;
end

switch lower(cmd)
  case 'init'
    [pars data]=init;
  case 'getdata'
    if stat{1}==1
      varargout{1}=data;
    end
  case 'getpars'
    varargout{1}=pars;
  case 'setpars'
    if nargin==2
      newpars=varargin{1};
      if isstruct(newpars)
        parfields=fields(newpars);
        for ipar=1:length(parfields)
          pars.(parfields{ipar})=newpars.(parfields{ipar});
        end
      end
    end
  case 'serversync'
    stat=sockrw('writechar','cas','FlBpmToolFn(''init'')'); if stat{1}~=1; return; end;
    stat=sockrw('readchar','cas',10);
  case 'setstatus' % data.Tag.Selection(bpmind)=status
    if nargin==5
      [stat data]=setStatus(data,varargin{1},varargin{2},varargin{3},varargin{4});
    end
  case 'zeroref'
    [stat data]=getRef(pars,data,'zero');
  case 'measurereforbit'
    [stat data]=getRef(pars,data);
  case 'savereffile'
    [stat data]=saveRef(data,'file',varargin{1});
  case 'saverefmemory'
    [stat data]=saveRef(data,'mem');
  case 'saverefdefault'
    [stat data]=saveRef(data,'def');
  case 'saveserverref'
    [stat data]=saveRef(data,'server');
  case 'loadreffile'
    [stat data]=loadRef(data,'file',varargin{1});
  case 'loadrefmemory'
    [stat data]=loadRef(data,'mem');
  case 'loadrefdefault'
    [stat data]=loadRef(data,'def');
  case 'loadserverref'
    [stat data]=loadRef(data,'server');
  case 'generatestriplinecal'
    [stat data] = stripCalGen(data);
  case 'applystriplinecal'
    [stat raw data] = stripCalApply(data,varargin{1});
    varargout{1}=raw;
  case 'plotstriplinecaldata'
    stat=stripCalPlot(data);
  case 'normalize'
    stat=normalize(varargin{1});
  case 'addusertag'
    if nargin>1
      tf=fieldnames(data);
      if any(cellfun(@(x) strcmp(x,varargin{1}),tf))
        stat{1}=-1; stat{2}='Name exists';
        return
      end
      nn=varargin{1};
      data.(nn)=data.global;
      data.tagNames{end+1}=nn;
      tnst=cell2mat(cellfun(@(x) [x ' '],data.tagNames,'UniformOutput',false));
      stat=FlDataStore('FlBpmToolFn',[nn 'SEPcal'],data.(nn).cal,[nn 'SEPuse'],data.(nn).use,...
        [nn 'SEPuseCal'],data.(nn).useCal,[nn 'SEPhw'],data.(nn).hw,'tagNames',tnst);
    end
  case 'removeusertag'
    if nargin>1
      if isfield(data,varargin{1})
        rn=varargin{1};
        data=rmfield(data,rn);
        data.tagNames=data.tagNames(~ismember(data.tagNames,rn));
        tnst=cell2mat(cellfun(@(x) [x ' '],data.tagNames,'UniformOutput',false));
        stat=FlDataStore('FlBpmToolFn','tagNames',tnst); if stat{1}~=1; return; end;
        stat=FlDataStore('FlBpmToolFn','delete',[rn 'SEPcal'],[rn 'SEPuse'],...
          [rn 'SEPuseCal'],[rn 'SEPhw']);
      else
        stat{1}=-1; stat{2}='No such user tag id found';
      end
    end
  otherwise
    stat{1}=-1;
    stat{2}='unknown command';
end
varargout{end+1}=data;

% =========================================
% Local Functions
% =========================================
function stat=normalize(type)
global FL BEAMLINE INSTR
stat{1}=1;
idict=findcells(BEAMLINE,'Class','IMON');
if ~(strcmp(BEAMLINE{idict(1)}.Name, 'ICT1X') && ...
    strcmp(BEAMLINE{idict(2)}.Name,'ICTDUMP'))
  stat{1}=-1;
  stat{2}= [...
    'TMIT normalization failed. Looked for ICT1X and ICTDUMP and found' ...
    BEAMLINE{idict(1)}.Name, ' and ' BEAMLINE{idict(2)}.Name];
  return
end
icti=findcells(INSTR,'Class','IMON'); %more indices, fun fun fun
idiex=findcells(BEAMLINE,'Name','IEX');
%iexi=findcells(INSTR,'Index',idiex);

% get indices for BPMs between ICTs
idmi=findcells(INSTR,'Class','MONI')'; % pointers to BPMs in INSTR
idm=cellfun(@(x) x.Index,INSTR(idmi))'; % pointers to BPMs in BEAMLINE
idma=find(idm<idict(2) & idm>idict(1));
idma_beg= idm<idict(1) & idm>idiex;
idmi_beg=idmi(idma_beg);%idm_beg=idm(idma_beg);
idm=idm(idma);idmi=idmi(idma);

% get tmit for both icts
FlHwUpdate;
ntmit(1)=INSTR{icti(1)}.Data(3);ntmit(2)=INSTR{icti(2)}.Data(3);
if type<3
  if ntmit(type)==0
    stat{1}=-1;
    stat{2}='FlBpmToolFn normalize error - ICT reads 0';
    return
  end
else
  if any(ntmit==0)
    stat{1}=-1;
    stat{2}='FlBpmToolFn normalize error- both ICTs read 0';
    return;
  end
end
%regardless of type, iex-ict1x bpms get ict(type) or ict(1)
if mod(type,2) %types 1 & 3
  typ=1;
else
  typ=2;
end
FL.noUpdate=true;
for nn=1:length(idmi_beg)
  if INSTR{idmi_beg(nn)}.Data(3) % tmit ~= 0
    FL.HwInfo.INSTR(idmi_beg(nn)).conv(3)=...
      FL.HwInfo.INSTR(idmi_beg(nn)).conv(3)* ...
      ntmit(typ)/...
      INSTR{idmi_beg(nn)}.Data(3);
    if (FL.HwInfo.INSTR(idmi_beg(nn)).conv(3)==0 || ...
        ~isfinite(FL.HwInfo.INSTR(idmi_beg(nn)).conv(3)))
      FL.HwInfo.INSTR(idmi_beg(nn)).conv(3)=1;
      
    end
  else
    FL.HwInfo.INSTR(idmi(nn)).conv(3)=1;
  end
end
%  FL.noUpdate=false;

if type<3
  %  FL.noUpdate=true;
  for nn=1:length(idmi)
    if INSTR{idmi(nn)}.Data(3) % tmit ~= 0
      FL.HwInfo.INSTR(idmi(nn)).conv(3)=...
        FL.HwInfo.INSTR(idmi(nn)).conv(3)* ...
        ntmit(type)/...
        INSTR{idmi(nn)}.Data(3);
      if (FL.HwInfo.INSTR(idmi(nn)).conv(3)==0 || ...
          ~isfinite(FL.HwInfo.INSTR(idmi(nn)).conv(3)))
        FL.HwInfo.INSTR(idmi(nn)).conv(3)=1;
      end
    else
      FL.HwInfo.INSTR(idmi(nn)).conv(3)=1;
    end
  end
  %  FL.noUpdate=false;
  %    FL.HwInfo.INSTR(idmi(nn)).conv(3)=conv(nn)*bpmt(nn)/ntmit(type);
else %type==3
  sict=cellfun(@(x) x.S,BEAMLINE(idict));
  sbpm=cellfun(@(x) x.S,BEAMLINE(idm));
  foo=zeros(1,length(idm));
  for nn=1:length(idmi)
    if INSTR{idmi(nn)}.Data(3) %tmit ~=0
      foo(nn)= FL.HwInfo.INSTR(idmi(nn)).conv(3)*...
        (ntmit(1)+(diff(ntmit)*(sbpm(nn)-sict(1))/(diff(sict))))/...
        INSTR{idmi(nn)}.Data(3);
      if (foo(nn)==0|| ...
          ~isfinite(foo(nn)))
        foo(nn)=1;
      end
    else
      foo(nn)=1;
    end
  end
  %  FL.noUpdate=true;
  for nn=1:length(idmi)
    FL.HwInfo.INSTR(idmi(nn)).conv(3)=foo(nn);
    pause(0.01)
    %disp(nn)
  end
  %  FL.noUpdate=false;
end
FL.noUpdate=false;
tmitConvInstrName={}; tmitConv=[];
for ic=1:length(INSTR)
  if length(FL.HwInfo.INSTR(ic).conv)>=3
    tmitConvInstrName{end+1}=BEAMLINE{INSTR{ic}.Index}.Name;
    tmitConv(end+1)=FL.HwInfo.INSTR(ic).conv(3);
  end
end
save userData/tmitConvVals tmitConvInstrName tmitConv

function stat=stripCalPlot(data)
global FL
stat{1}=1;
if ~isfield(data,'stripCal') || ~isfield(data.stripCal,'xread')
  stat{1}=-1; stat{2}='No cal data to plot';
  return
end
for i=1:length(data.stripCal.stripline)
  if isfield(FL,'Gui') && isfield(FL.Gui,'FlBpmToolFn_plot1') && ishandle(FL.Gui.FlBpmToolFn_plot1)
    figure(FL.Gui.FlBpmToolFn_plot1)
  else
    FL.Gui.FlBpmToolFn_plot1=figure;
  end
  subplot(3,4,i);
  plot(data.stripCal.iread,data.stripCal.xread(:,i),'b.',...
    [data.stripCal.imin data.stripCal.imax],polyval(data.stripCal.xpoly(i,:),[data.stripCal.imin data.stripCal.imax]),'b-');
  title(data.stripCal.stripline_name(i,:));
  if isfield(FL,'Gui') && isfield(FL.Gui,'FlBpmToolFn_plot2') && ishandle(FL.Gui.FlBpmToolFn_plot2)
    figure(FL.Gui.FlBpmToolFn_plot2)
  else
    FL.Gui.FlBpmToolFn_plot2=figure;
  end
  subplot(3,4,i);
  plot(data.stripCal.iread,data.stripCal.yread(:,i),'b.',...
    [data.stripCal.imin data.stripCal.imax],polyval(data.stripCal.ypoly(i,:),[data.stripCal.imin data.stripCal.imax]),'b-');
  title(data.stripCal.stripline_name(i,:));
end
function [stat data] = stripCalGen(data)
global BEAMLINE INSTR
disp('100 orbit are recording');
stat=FlHwUpdate('wait',100); if stat{1}~=1; return; end;
disp('done');
[stat,raw]=FlHwUpdate('readbuffer',100); if stat{1}~=1; return; end;
bpm=findcells(BEAMLINE,'Name','MQ*X');
ICT=findcells(BEAMLINE,'Name','ICT1X');
ICT_index=findcells(INSTR,'Index',ICT);
bpm_name='';
bpm_type='';
bpm_index=[];
for i=bpm
  bpm_name=strvcat(bpm_name,BEAMLINE{i}.Name); %#ok<VCAT>
  bpm_index(end+1)=findcells(INSTR,'Index',i); %#ok<AGROW>
  bpm_type=strvcat(bpm_type,INSTR{bpm_index(end)}.Type); %#ok<VCAT>
end
stripline=find(strcmp('stripline',cellstr(bpm_type))).'; data.stripCal.stripline=stripline;
nstripline=length(stripline);
data.stripCal.stripline_name=bpm_name(stripline,:);
stripline_index=bpm_index(stripline);
data.stripCal.xread=raw(:,1+stripline_index*3-2);
data.stripCal.yread=raw(:,1+stripline_index*3-1);
data.stripCal.iread=raw(:,1+ICT_index*3);
data.stripCal.range_fit=find((data.stripCal.iread<1.1e10) & (data.stripCal.iread>4e9));
imin=max([min(data.stripCal.iread) 4e9]);
imax=min([max(data.stripCal.iread) 1.1e10]);
for i=1:nstripline
  xpoly(i,:)=polyfit(data.stripCal.iread(data.stripCal.range_fit),data.stripCal.xread(data.stripCal.range_fit,i),1); %#ok<AGROW>
  ypoly(i,:)=polyfit(data.stripCal.iread(data.stripCal.range_fit),data.stripCal.yread(data.stripCal.range_fit,i),1); %#ok<AGROW>
end
data.stripCal.imin=imin;
data.stripCal.imax=imax;
data.stripCal.xpoly=xpoly;
data.stripCal.ypoly=ypoly;
data.stripCal.date=now;
stat=FlDataStore('FlBpmToolFn','stripCalSEPimin',data.stripCal.imin,'stripCalSEPimax',data.stripCal.imax,...
  'stripCalSEPxpoly',data.stripCal.xpoly,'stripCalSEPypoly',data.stripCal.ypoly,'stripCalSEPdate',data.stripCal.date,...
  'stripCalSEPxread',data.stripCal.xread,'stripCalSEPyread',data.stripCal.yread,'stripCalSEPiread',data.stripCal.iread,...
  'stripCalSEPrange_fit',data.stripCal.range_fit);
function [stat raw data] = stripCalApply(data,raw)
global BEAMLINE INSTR
persistent bpm ICT ICT_index bpm_name bpm_type bpm_index
persistent stripline
stat{1}=1;

if isempty(data) || ~isfield(data,'stripCal')
  stat{1}=-1;stat{2}='No stripline calibration data';
  return
end
imin=data.stripCal.imin;
imax=data.stripCal.imax;
xpoly=data.stripCal.xpoly;
ypoly=data.stripCal.ypoly;
if isempty(bpm)
  bpm=findcells(BEAMLINE,'Name','MQ*X');
  ICT=findcells(BEAMLINE,'Name','ICT1X');
  ICT_index=findcells(INSTR,'Index',ICT);
  bpm_name='';
  bpm_type='';
  bpm_index=[];
  for i=bpm
    bpm_name=strvcat(bpm_name,BEAMLINE{i}.Name); %#ok<VCAT>
    bpm_index(end+1)=findcells(INSTR,'Index',i); %#ok<AGROW>
    bpm_type=strvcat(bpm_type,INSTR{bpm_index(end)}.Type); %#ok<VCAT>
  end
  stripline=find(strcmp('stripline',cellstr(bpm_type))).';
end
nstripline=length(stripline);
data.stripCal.stripline_name=bpm_name(stripline,:);
stripline_index=bpm_index(stripline);
iread=raw(:,1+ICT_index*3);
data.stripCal.nread_b_cor=length(iread);
raw=raw((iread<=imax) & (iread>=imin),:);
if isempty(raw); return; end; % nothing to be done if all fail cut
xread=raw(:,1+stripline_index*3-2);
yread=raw(:,1+stripline_index*3-1);
iread=raw(:,1+ICT_index*3-2);
for i=1:nstripline
  if data.global.useCal(bpm_index(i)) && data.global.cal(bpm_index(i))
    xread_cor(:,i)=xread(:,i)-iread(:)*xpoly(i,1); %#ok<AGROW>
    yread_cor(:,i)=yread(:,i)-iread(:)*ypoly(i,1); %#ok<AGROW>
  else
    xread_cor(:,i)=xread(:,i); %#ok<AGROW>
    yread_cor(:,i)=yread(:,i); %#ok<AGROW>
  end
end
raw(:,1+stripline_index*3-2)=xread_cor;
raw(:,1+stripline_index*3-1)=yread_cor;
function [stat data] = getRef(pars,data,cmd)
global INSTR
[stat bpmdata]=FlHwUpdate('bpmave',pars.nbpm,0,[pars.qcut 3 1 0]);
if stat{1}~=1; return; end;
if exist('cmd','var') && isequal(cmd,'zero')
  for ii=1:length(INSTR)
    INSTR{ii}.ref(1:3)=[0 0 0];
    INSTR{ii}.referr(1:3)=[0 0 0];
  end
else
  for ii=1:length(INSTR)
    INSTR{ii}.ref(1:3)=[bpmdata{1}(1,ii) bpmdata{1}(2,ii) bpmdata{1}(3,ii)];
    INSTR{ii}.referr(1:3)=[bpmdata{1}(4,ii) bpmdata{1}(5,ii) bpmdata{1}(6,ii)];
  end
end
data.ref=cellfun(@(x) x.ref,INSTR,'UniformOutput',false);
data.referr=cellfun(@(x) x.referr,INSTR,'UniformOutput',false);
data.refinuse=sprintf('New Reference: %s',datestr(now));
function [stat data]=loadRef(data,method,filename)
global INSTR FL
stat{1}=1;
switch method
  case {'file' 'def'}
    if strcmp(method,'def')
      loadfile=fullfile('latticeFiles',[FL.expt,'lat.mat']);
      d=dir(fullfile('latticeFiles',[FL.expt,'lat.mat']));
      data.refinuse=sprintf('Default reference: %s',d(1).date);
    else
      if ~exist(fullfile('latticeFiles','archive',filename),'file')
        stat{1}=-1; stat{2}='Filename not found in latticeFiles/archive directory';
        return
      end
      loadfile=fullfile('latticeFiles','archive',filename);
      d=dir(fullfile('latticeFiles','archive',filename));
      data.refinuse=sprintf('Archive file %s : %s',filename,d(1).date);
    end
    if ~exist(loadfile,'file')
      stat{1}=-1; stat{2}='File not found';
      return
    end
    ld=load(loadfile,'INSTR');
    if ~isfield(ld,'INSTR')
      stat{1}=-1; stat{2}='INSTR array not found in requested file';
    elseif length(ld.INSTR)~=length(INSTR)
      stat{1}=-1; stat{2}='Mismatch in INSTR array length in requested file';
    end
    if stat{1}~=1; return; end;
    for ii=1:length(ld.INSTR)
      INSTR{ii}.ref=ld.INSTR{ii}.ref; data.ref{ii}=ld.INSTR{ii}.ref;
      INSTR{ii}.referr=ld.INSTR{ii}.referr; data.referr{ii}=ld.INSTR{ii}.referr;
      %       INSTR{ii}.dispref=ld.INSTR{ii}.dispref; data.dispref{ii}=ld.INSTR{ii}.dispref;
      %       INSTR{ii}.dispreferr=ld.INSTR{ii}.dispreferr; data.dispreferr{ii}=ld.INSTR{ii}.dispreferr;
    end
  case 'mem'
    stat=FlDataStore('FlBpmToolFn'); data=unpackRef(data);
    if stat{1}~=1; return; end;
    if ~isfield(FL,'DataStore') || ~isfield(FL.DataStore,'FlBpmToolFn') || ...
        ~isfield(FL.DataStore.FlBpmToolFn,'ref')
      stat{1}=-1; stat{2}='No reference data in memory';
      return
    end
    if isfield(FL.DataStore.FlBpmToolFn,'refinuse')
      data.refinuse=FL.DataStore.FlBpmToolFn.refinuse;
    else
      data.refinuse='Memory data';
    end
    for ii=1:length(INSTR)
      INSTR{ii}.ref=data.ref{ii};
      INSTR{ii}.referr=data.referr{ii};
      %       INSTR{ii}.dispref=data.dispref{ii};
      %       INSTR{ii}.dispreferr=data.dispreferr{ii};
    end
  case 'server'
    stat=FlDataStore('FlBpmToolFn'); data=unpackRef(data);
    if stat{1}~=1; return; end;
    if ~isfield(FL,'DataStore') || ~isfield(FL.DataStore,'FlBpmToolFn') || ...
        ~isfield(FL.DataStore.FlBpmToolFn,'serverref')
      stat{1}=-1; stat{2}='No reference data in server memory';
      return
    end
    if isfield(FL.DataStore.FlBpmToolFn,'refinuse')
      data.refinuse=FL.DataStore.FlBpmToolFn.refinuse;
    else
      data.refinuse='Server Reference';
    end
    for ii=1:length(INSTR)
      INSTR{ii}.ref=data.serverref{ii};
      INSTR{ii}.referr=data.serverreferr{ii};
      %       INSTR{ii}.dispref=data.serverdispref{ii};
      %       INSTR{ii}.dispreferr=data.serverdispreferr{ii};
    end
    
end
function [stat data]=saveRef(data,method,filename)
global INSTR FL
stat{1}=1;
switch method
  case {'file','def'}
    if strcmp(method,'def')
      savefile=fullfile('latticeFiles',[FL.expt,'lat.mat']);
      data.refinuse=sprintf('Default reference: %s',datestr(now));
    else
      savefile=fullfile('latticeFiles','archive',filename);
      data.refinuse=sprintf('Archive file %s : %s',filename,datestr(now));
    end
    ld=[];
    if exist(savefile,'file')
      ld=load(savefile,'INSTR');
      thisINSTR=INSTR;
    end
    if isfield(ld,'INSTR')
      INSTR=ld.INSTR;
      if length(INSTR)~=length(thisINSTR)
        stat{1}=-1;
        stat{2}='INSTR length mismatch';
        return
      end
      for ii=1:length(INSTR)
        INSTR{ii}.ref=thisINSTR{ii}.ref;
        INSTR{ii}.referr=thisINSTR{ii}.referr;
        %         INSTR{ii}.dispref=thisINSTR{ii}.dispref;
        %         INSTR{ii}.dispreferr=thisINSTR{ii}.dispreferr;
      end
      save(savefile,'INSTR','-APPEND')
      INSTR=thisINSTR;
    else
      FlArchive('SaveLucretia',savefile);
    end
  case 'mem'
    for ii=1:length(INSTR)
      data.ref{ii}=INSTR{ii}.ref;
      data.referr{ii}=INSTR{ii}.referr;
      %       data.dispref{ii}=INSTR{ii}.dispref;
      %       data.dispreferr{ii}=INSTR{ii}.dispreferr;
    end
    data.refinuse=sprintf('Memory: %s',datestr(now));
    stat=FlDataStore('FlBpmToolFn','ref',reshape(cell2mat(data.ref),length(data.ref),length(data.ref{1})),...
      'referr',reshape(cell2mat(data.referr),length(data.referr),length(data.referr{1})),...
      'refinuse',data.refinuse);
    %       'dispref',reshape(cell2mat(data.dispref),length(data.dispref),length(data.dispref{1})),
    %       'dispreferr',reshape(cell2mat(data.dispreferr),length(data.dispreferr),length(data.dispreferr{1})),
    
  case 'server'
    if strcmp(FL.mode,'trusted')
      stat=loadRef('server');
    elseif strcmp(FL.mode,'test')
      for ii=1:length(INSTR)
        data.ref{ii}=INSTR{ii}.ref;
        data.referr{ii}=INSTR{ii}.referr;
        %         data.dispref{ii}=INSTR{ii}.dispref;
        %         data.dispreferr{ii}=INSTR{ii}.dispreferr;
      end
      data.refinuse=sprintf('Server Reference: %s',datestr(now));
      stat=FlDataStore('FlBpmToolFn','serverref',reshape(cell2mat(data.ref),length(data.ref),length(data.ref{1})),...
        'serverreferr',reshape(cell2mat(data.referr),length(data.referr),length(data.referr{1})),...
        'refinuse',data.refinuse);
      %       'serverdispref',reshape(cell2mat(data.dispref),length(data.dispref),length(data.dispref{1})),...
      %       'serverdispreferr',reshape(cell2mat(data.dispreferr),length(data.dispreferr),length(data.dispreferr{1})),...
      if stat{1}~=1; return; end;
      stat=sockrw('writechar','cas','FlBpmToolFn(''LoadServerRef'');'); if stat{1}~=1; return; end;
      stat=sockrw('readchar','cas',20);
    end
end
function [stat data]=setStatus(data,tag,sel,bpmind,status)
global INSTR

dim=size(status);if dim(1)>dim(2); status=status'; end;
data.(tag).(sel)(bpmind)=status;
stat=FlDataStore('FlBpmToolFn',[tag,'SEP',sel],data.(tag).(sel));
if strcmp(tag,'global') && strcmp(sel,'hw')
  for ii=1:length(bpmind)
    if data.(tag).(sel)(bpmind(ii))
      INSTR{bpmind(ii)}.Class='NOHW';
    else
      INSTR{bpmind(ii)}.Class='MONI';
    end
  end
end
function [pars data] = init
global INSTR FL
% Default pars
pars.nbpm=10;
pars.qcut=0.9;
pars.bbasub=false;
% Get any previously saved data
lstat=FlDataStore('FlBpmToolFn');
if lstat{1}==1 && isfield(FL,'DataStore') && isfield(FL.DataStore,'FlBpmToolFn')
  fn=fieldnames(FL.DataStore.FlBpmToolFn);
  for ifn=1:length(fn)
    t=regexp(fn{ifn},'(\S+)SEP(\S+)','tokens');
    if isempty(t)
      if strcmp(fn{ifn},'tagNames')
        data.tagNames=regexp(FL.DataStore.FlBpmToolFn.(fn{ifn}),' ','split');
        data.tagNames=data.tagNames(1:end-1);
        if length(data.tagNames)==1 && strcmp(data.tagNames{1}(end),' ')
          data.tagNames{1}=data.tagNames{1}(1:end-1);
        end
      else
        data.fn{ifn}=FL.DataStore.FlBpmToolFn.(fn{ifn});
      end
    else
      data.(t{1}{1}).(t{1}{2})=FL.DataStore.FlBpmToolFn.(fn{ifn});
    end
  end
  data=unpackRef(data);
end
% If none, populate global field
if ~exist('data','var') || ~isfield(data,'tagNames') || isempty(data.tagNames)
  data.tagNames={'global'};
end
for ii=1:length(INSTR)
  if ~isfield(data,'global') || ~isfield(data.global,'cal') || length(data.global.cal)<ii
    data.global.cal(ii)=true;
  end
  if ~isfield(data,'global') || ~isfield(data.global,'use') || length(data.global.use)<ii
    data.global.use(ii)=true;
  end
  if ~isfield(data,'global') || ~isfield(data.global,'useCal') || length(data.global.useCal)<ii
    data.global.useCal(ii)=false;
  end
  data.global.hw(ii)=~strcmp(INSTR{ii}.Class,'NOHW');
end
tnst=cell2mat(cellfun(@(x) [x ' '],data.tagNames,'UniformOutput',false));
FlDataStore('FlBpmToolFn','globalSEPcal',data.global.cal,'globalSEPuse',data.global.use,...
  'globalSEPuseCal',data.global.useCal,'globalSEPhw',data.global.hw,'tagNames',tnst);

function data=unpackRef(data)
global FL
if isfield(FL,'DataStore') && isfield(FL.DataStore,'FlBpmToolFn')
  if isfield(FL.DataStore.FlBpmToolFn,'ref')
    data.ref=arrayfun(@(x) FL.DataStore.FlBpmToolFn.ref(x,:),...
      1:length(FL.DataStore.FlBpmToolFn.ref),'UniformOutput',false);
  end
  if isfield(FL.DataStore.FlBpmToolFn,'referr')
    data.referr=arrayfun(@(x) FL.DataStore.FlBpmToolFn.referr(x,:),...
      1:length(FL.DataStore.FlBpmToolFn.referr),'UniformOutput',false);
  end
  %   if isfield(FL.DataStore.FlBpmToolFn,'dispref')
  %     data.dispref=arrayfun(@(x) FL.DataStore.FlBpmToolFn.dispref(x,:),...
  %       1:length(FL.DataStore.FlBpmToolFn.dispref),'UniformOutput',false);
  %   end
  %   if isfield(FL.DataStore.FlBpmToolFn,'dispreferr')
  %     data.dispreferr=arrayfun(@(x) FL.DataStore.FlBpmToolFn.dispreferr(x,:),...
  %       1:length(FL.DataStore.FlBpmToolFn.dispreferr),'UniformOutput',false);
  %   end
end
if isfield(FL,'DataStore') && isfield(FL.DataStore,'FlBpmToolFn')
  if isfield(FL.DataStore.FlBpmToolFn,'serverref')
    data.serverref=arrayfun(@(x) FL.DataStore.FlBpmToolFn.serverref(x,:),...
      1:length(FL.DataStore.FlBpmToolFn.serverref),'UniformOutput',false);
  end
  if isfield(FL.DataStore.FlBpmToolFn,'serverreferr')
    data.serverreferr=arrayfun(@(x) FL.DataStore.FlBpmToolFn.serverreferr(x,:),...
      1:length(FL.DataStore.FlBpmToolFn.serverreferr),'UniformOutput',false);
  end
  %   if isfield(FL.DataStore.FlBpmToolFn,'serverdispref')
  %     data.serverdispref=arrayfun(@(x) FL.DataStore.FlBpmToolFn.serverdispref(x,:),...
  %       1:length(FL.DataStore.FlBpmToolFn.serverdispref),'UniformOutput',false);
  %   end
  %   if isfield(FL.DataStore.FlBpmToolFn,'serverdispreferr')
  %     data.serverdispreferr=arrayfun(@(x) FL.DataStore.FlBpmToolFn.serverdispreferr(x,:),...
  %       1:length(FL.DataStore.FlBpmToolFn.serverdispreferr),'UniformOutput',false);
  %   end
end

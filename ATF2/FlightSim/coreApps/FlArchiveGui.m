function varargout = FlArchiveGui(varargin)
% FLARCHIVEGUI M-file for FlArchiveGui.fig
%      FLARCHIVEGUI, by itself, creates a new FLARCHIVEGUI or raises the existing
%      singleton*.
%
%      H = FLARCHIVEGUI returns the handle to a new FLARCHIVEGUI or the handle to
%      the existing singleton*.
%
%      FLARCHIVEGUI('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in FLARCHIVEGUI.M with the given input arguments.
%
%      FLARCHIVEGUI('Property','Value',...) creates a new FLARCHIVEGUI or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before FlArchiveGui_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to FlArchiveGui_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help FlArchiveGui

% Last Modified by GUIDE v2.5 27-May-2009 05:21:15

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @FlArchiveGui_OpeningFcn, ...
                   'gui_OutputFcn',  @FlArchiveGui_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before FlArchiveGui is made visible.
function FlArchiveGui_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to FlArchiveGui (see VARARGIN)

% Choose default command line output for FlArchiveGui
handles.output = handles;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes FlArchiveGui wait for user response (see UIRESUME)
% uiwait(handles.figure1);

% Restore old settings
restoreList={'radiobutton1','Value',false;
             'radiobutton2','Value',true;
             'edit6','String',false;
             'edit7','String',false;
             'edit5','String',false};
handles=guiRestoreFn('FlArchiveGui',handles,restoreList);
if get(handles.radiobutton1,'Value')
  mhan=msgbox('Attempting to connect to Data Server...');
  radiobutton1_Callback(handles.radiobutton1,[],handles);
  if ishandle(mhan); delete(mhan); end;
end
% Turn messaging functionality on for FlArchive
newpars.domsg=true;
FlArchive('SetPars',newpars);


% --- Outputs from this function are returned to the command line.
function varargout = FlArchiveGui_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- EXIT button
function pushbutton1_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
guiCloseFn('FlArchiveGui',handles);

% --- Select Lucretia restore file
function pushbutton2_Callback(hObject, eventdata, handles) %#ok<*INUSL,*DEFNU>
% hObject    handle to pushbutton2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
[stat pars]=FlArchive('getPars');
if stat{1}~=1 || ~isfield(pars,'dirs') || ~iscell(pars.dirs) || length(pars.dirs)<2
  errordlg('Error getting Archive Parameters','FlArchive error')
  return
end
[filename,pathname]=uigetfile('*.mat','Lucretia Restore File',pars.dirs{2});
if isempty(findstr(pathname,pars.dirs{2}))
  set(handles.edit6,'String',fullfile(pathname,filename));
else
  set(handles.edit6,'String',filename);
end

% --- Restore command
function pushbutton3_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get parameters and check
[stat pars]=FlArchive('getPars');
if stat{1}~=1 || ~isfield(pars,'method') || isempty(pars.method)
  if length(stat)<2; stat{2}=[]; end;
  errordlg(sprintf('Error getting Archive Parameters: %s',stat{2}),'FlArchive error')
  return
end
% If wanting to restore from epics, check connection to DataServer and
% restore info is all good
if strcmp(pars.method,'epics')
  if ~isequal(get(handles.pushbutton10,'BackgroundColor'),[0 1 0])
    errordlg('Must have valid connection to DataServer before getting epics restore point','FlArchive error')
    return
  end
  if ~checkEpicsRestoreInfo(handles)
    errordlg('Bad epics restore data- check time period is showing, requested restore date and time is within this window and the data server connection is good (green)','Restore error')
  end
end
% If wanting to restore from Lucretia file- check file is good
if strcmp(pars.method,'mfile')
  if isempty(get(handles.edit6,'String')) || ~exist(get(handles.edit6,'String'),'file')
    if ~exist(fullfile('latticeFiles','archive',get(handles.edit6,'String')),'file')
      errordlg('Lucretia restore file not found')
    end
  end
end
% Check really want to do this
resp=questdlg(sprintf('Restore archived data from %s? (caution:if server not set to NOAUTO mode, restored data will be automatically overwritten)',pars.method),'Restore Data');
if ~strcmp(resp,'Yes')
  return
end
% Restore
if strcmp(pars.method,'epics')
  datetime=addtodate(datenum(get(handles.edit2,'String')),str2double(get(handles.edit3,'String'))*60+str2double(get(handles.edit4,'String')),'minute');
  stat=FlArchive('Restore',datetime,true);
elseif strcmp(pars.method,'mfile')
  stat=FlArchive('Restore',get(handles.edit6,'String'));
end
if stat{1}~=1
  errordlg(stat{2},'Restore error')
  return
else
  msgbox('Restore Successful')
end

% --- Save Lucretia file
function pushbutton4_Callback(hObject, eventdata, handles) %#ok<*INUSD>
% hObject    handle to pushbutton4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global FL
[filename,pathname]=uiputfile(fullfile('latticeFiles','archive',[FL.expt,'-',datestr(now,FL.tstamp),'.mat']),'Save Lucretia File');
if ~isequal(filename,0) && ~isequal(pathname,0)
  FlArchive('saveLucretia',fullfile(pathname,filename));
end

% --- Executes on selection change in listbox1.
function listbox1_Callback(hObject, eventdata, handles)
% hObject    handle to listbox1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = get(hObject,'String') returns listbox1 contents as cell array
%        contents{get(hObject,'Value')} returns selected item from listbox1


% --- Executes during object creation, after setting all properties.
function listbox1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to listbox1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: listbox controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Restore Default
function pushbutton5_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton5 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Check really want to do this
resp=questdlg('Restore default Lucretia file?','Restore Default Data');
if ~strcmp(resp,'Yes')
  return
else
  stat=FlArchive('restoreDefault');
  if stat{1}~=1
    errordlg(stat{2},'Restore Default Error')
    return
  end
end

% --- Set Default File
function pushbutton6_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton6 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
resp=questdlg('Save current Lucretia data as default?','Set Default');
if ~strcmp(resp,'Yes')
  return
else
  stat=FlArchive('setDefault');
  if stat{1}~=1
    errordlg(stat{2},'Set Default Error')
    return
  end
end


function edit1_Callback(hObject, eventdata, handles)
% hObject    handle to edit1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit1 as text
%        str2double(get(hObject,'String')) returns contents of edit1 as a double


% --- Executes during object creation, after setting all properties.
function edit1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Get date from calander gui
function pushbutton7_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton7 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global FL
try
  if isequal(get(handles.pushbutton10,'BackgroundColor'),[0 1 0])
    t1=min(FL.chanArch.starts);
    t2=max(FL.chanArch.ends);
    datet=uisetdate;
    if datenum(datet)<t1 || datenum(datet)>t2
      errordlg('Must Choose date within available date period shown','epics date error')
      return
    end
    set(handles.edit2,'String',datet)
  else
    errordlg('Must make connection to data server first','Archive error')
    return
  end
catch
  errordlg(lasterr,'Get date error')
end

function edit2_Callback(hObject, eventdata, handles)
% hObject    handle to edit2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit2 as text
%        str2double(get(hObject,'String')) returns contents of edit2 as a double
global FL
try
  if isequal(get(handles.pushbutton10,'BackgroundColor'),[0 1 0])
    t1=min(FL.chanArch.starts);
    t2=max(FL.chanArch.ends);
    datet=get(hObject,'String');
    if datenum(datet)<t1 || datenum(datet)>t2
      errordlg('Must Choose date within available date period shown','epics date error')
      return
    end
  else
    errordlg('Must make connection to data server first','Archive error')
    return
  end
catch
  errordlg(lasterr,'Get date error')
end


% --- Executes during object creation, after setting all properties.
function edit2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit3_Callback(hObject, eventdata, handles)
% hObject    handle to edit3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit3 as text
%        str2double(get(hObject,'String')) returns contents of edit3 as a double
hr=str2double(get(hObject,'String'));
if isnan(hr) || hr<0 || hr>23
  set(hObject,'String',[])
  errordlg('Incorrect time format, enter hour 0-23')
end

% --- Executes during object creation, after setting all properties.
function edit3_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit4_Callback(hObject, eventdata, handles)
% hObject    handle to edit4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit4 as text
%        str2double(get(hObject,'String')) returns contents of edit4 as a double
minute=str2double(get(hObject,'String'));
if isnan(minute) || minute<0 || minute>59
  set(hObject,'String',[])
  errordlg('Incorrect time format, enter Min 0-59')
end

% --- Executes during object creation, after setting all properties.
function edit4_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit5_Callback(hObject, eventdata, handles)
% hObject    handle to edit5 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit5 as text
%        str2double(get(hObject,'String')) returns contents of edit5 as a double
pars.dataServerURL=get(hObject,'String');
FlArchive('SetPars',pars);
FlArchiveGui('pushbutton10_Callback',handles.pushbutton10,[],handles);

% --- Executes during object creation, after setting all properties.
function edit5_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit5 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit6_Callback(hObject, eventdata, handles)
% hObject    handle to edit6 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit6 as text
%        str2double(get(hObject,'String')) returns contents of edit6 as a double


% --- Executes during object creation, after setting all properties.
function edit6_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit6 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Connect / Update info from data server
function pushbutton10_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton10 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global FL
stat=FlUpdate('archInit',get(handles.edit5,'String'));
if stat{1}~=1
  set(hObject,'BackgroundColor','red')
  errordlg(stat{2},'Connection error to Data Server')
else
  set(hObject,'BackgroundColor','green')
  set(handles.text4,'String',sprintf('Available Date/Time Period: %s - %s',datestr(min(FL.chanArch.starts)),datestr(max(FL.chanArch.ends))))
  set(handles.edit2,'String',datestr(min(FL.chanArch.ends)))
  set(handles.edit3,'String','0'); set(handles.edit4,'String','0');
end

% --- Lucretia file restore method
function radiobutton2_Callback(hObject, eventdata, handles)
% hObject    handle to radiobutton2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of radiobutton2

if get(hObject,'Value')
  if get(handles.radiobutton1,'Value'); set(handles.radiobutton1,'Value',0); end;
  % select Lucretia file method
  pars.method='mfile';
  stat=FlArchive('setPars',pars);
  if stat{1}~=1
    errordlg(stat{2},'FlArchive Error')
    return
  end
end


% --- EPICS Channel Archiver restore method
function radiobutton1_Callback(hObject, eventdata, handles)
% hObject    handle to radiobutton1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of radiobutton1

if get(hObject,'Value')
  if get(handles.radiobutton2,'Value'); set(handles.radiobutton2,'Value',0); end;
  % Is dataserver connected, try it?
  FlArchiveGui('pushbutton10_Callback',handles.pushbutton10,[],handles);
  if ~isequal(get(handles.pushbutton10,'BackgroundColor'),[0 1 0])
    set(handles.radiobutton1,'Value',0);
    set(handles.radiobutton2,'Value',1);
    return
  end
  % Select epics method
  pars.method='epics';
  stat=FlArchive('setPars',pars);
  if stat{1}~=1
    errordlg(stat{2},'FlArchive Error')
    return
  end
end

function stat=checkEpicsRestoreInfo(handles)
stat=true;
if strcmp(get(handles.text4,'String'),'Available Date/Time Period')
  stat=false;
end
if isempty(get(handles.edit2,'String')) || isempty(get(handles.edit3,'String')) || isempty(get(handles.edit4,'String'))
  stat=false;
end


% --- Executes during object creation, after setting all properties.
function radiobutton2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to radiobutton2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called


% --- Executes during object creation, after setting all properties.
function radiobutton1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to radiobutton1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called


% --- Executes when user attempts to close figure1.
function figure1_CloseRequestFcn(hObject, eventdata, handles)
% hObject    handle to figure1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


guiCloseFn('FlArchiveGui',handles);


% --- set search time
function edit7_Callback(hObject, eventdata, handles)
% hObject    handle to edit7 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit7 as text
%        str2double(get(hObject,'String')) returns contents of edit7 as a double
global FL

if ~isnan(str2double(get(hObject,'String')))
  FL.chanArch.timeSearch=str2double(get(hObject,'String'));
else
  errordlg('Invalid search time entry','Archive GUI error')
end

% --- Executes during object creation, after setting all properties.
function edit7_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit7 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Load default Config to control system
function pushbutton11_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton11 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global FL
restoreResp=setDefaultsSelect;
if isempty(restoreResp)
  return
end
FL.Gui.FlArchiveGui_msg=msgbox('Restoring default FS values to control system...','Config Restore','replace');
stat=FlArchive('WriteDefault',restoreResp);
if ishandle(FL.Gui.FlArchiveGui_msg)
  delete(FL.Gui.FlArchiveGui_msg)
end
FL.Gui=rmfield(FL.Gui,'FlArchiveGui_msg');
if stat{1}~=1
  errordlg(stat{2},'Config restore error')
else
  msgbox('Config Restore to control system successful','FS','replace')
end


% --- Abort
function pushbutton13_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton13 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global FL

if strcmp(questdlg('Abort current operation?','Abort'),'Yes')
  FL.FlArchive_abort;
end


% --- Set All Movers to zero
function pushbutton14_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton14 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global GIRDER FL BEAMLINE

if ~strcmp(questdlg('Set All Movers to zero in x, y and roll axes?','Mover Zero Set'),'Yes')
  return
end
girs=FL.SimModel.moverList;
% Not AFB2FF
mfb2=findcells(BEAMLINE,'Name','MFB2FF'); mfb2g=BEAMLINE{mfb2}.Girder;
girs(ismember(girs,mfb2g))=[];
for igir=girs
  GIRDER{igir}.MoverSetPt=[0 0 0];
end
stat=MoverTrim(girs,3);
if stat{1}~=1
  errordlg(stat{2},'Mover Trim Error')
end

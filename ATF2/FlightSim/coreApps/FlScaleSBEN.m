function FlScaleSBEN()
%
% Energy scale B for SBENs that aren't connected to the control system

% 05-Dec-2011, M. Woodley
%    Don't skip ANY ATF2 SBEN elements!

global FL BEAMLINE
persistent idb

Cb=1e9/299792458; % rigidity constant (T-m/GeV)

if isempty(idb)
  idb=findcells(BEAMLINE,'Class','SBEN');
end
for m=1:length(idb)
  n=idb(m); % pointer into BEAMLINE
  idps=BEAMLINE{n}.PS; % pointer into PS (and FL.HwInfo.PS)
  if (FL.EScaleATF2Bends && n>FL.SimModel.extStart)
    scale=1;
  elseif (isempty(idps)||(idps==0))
    scale=0;
  elseif (isempty(FL.HwInfo.PS(idps).conv))
    scale=0;
  elseif isequal(FL.HwInfo.PS(idps).conv,0)
    scale=1;
  else
    scale=0;
  end
  if (scale)
    energy=BEAMLINE{n}.P;
    Angle=BEAMLINE{n}.Angle;
    BL=Cb*energy*Angle;
    BEAMLINE{n}.B(1)=BL;
  end
end

end

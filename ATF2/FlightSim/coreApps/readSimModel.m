function [stat, mdldata] = readSimModel(str)
% Read BEAMLINE, PS and GIRDER globals from model (str= model .mat file)
stat{1}=1; mdldata=[];

str=['latticeFiles/',str];
if ~exist(str,'file') && ~exist([str,'.mat'],'file')
  stat{1}=-1; stat{2}='Lattice file not found!';
else
  mdldata=load(str,'BEAMLINE','PS','GIRDER');
end % if lattice file found
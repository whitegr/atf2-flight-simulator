function varargout = clientList(varargin)
% CLIENTLIST M-file for clientList.fig
%      CLIENTLIST, by itself, creates a new CLIENTLIST or raises the existing
%      singleton*.
%
%      H = CLIENTLIST returns the handle to a new CLIENTLIST or the handle to
%      the existing singleton*.
%
%      CLIENTLIST('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in CLIENTLIST.M with the given input arguments.
%
%      CLIENTLIST('Property','Value',...) creates a new CLIENTLIST or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before clientList_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to clientList_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help clientList

% Last Modified by GUIDE v2.5 11-Feb-2009 21:49:35

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @clientList_OpeningFcn, ...
                   'gui_OutputFcn',  @clientList_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before clientList is made visible.
function clientList_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to clientList (see VARARGIN)

% Choose default command line output for clientList
handles.output = handles;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes clientList wait for user response (see UIRESUME)
% uiwait(handles.figure1);

% Refresh list
pushbutton3_Callback(handles.pushbutton3, eventdata, handles)


% --- Outputs from this function are returned to the command line.
function varargout = clientList_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Exit function
function pushbutton1_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
guiCloseFn('clientList',handles);

% --- Executes on selection change in listbox1.
function listbox1_Callback(hObject, eventdata, handles)
% hObject    handle to listbox1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = get(hObject,'String') returns listbox1 contents as cell array
%        contents{get(hObject,'Value')} returns selected item from listbox1


% --- Executes during object creation, after setting all properties.
function listbox1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to listbox1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: listbox controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Close Client
function pushbutton2_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
clist=get(handles.listbox1,'String');
csel=get(handles.listbox1,'Value');
if ~isempty(clist) && csel(1)>0
  for isel=1:length(csel)
    stat=sockrw('set_user','cas',regexprep(clist{csel(isel)},'^(\S+)\s+\S+\s+/.+','$1'));
    if stat{1}~=1
      errordlg(['Unknown user: ',regexprep(clist{csel(isel)},'^(\S+)\s+\S+\s+/.+','$1')],'Error closing client')
      return
    end
    stat=sockrw('close','cas');
    if stat{1}~=1
      errordlg(['Close error: ',stat{2}],'Error closing client')
      return
    end
  end
end
set(handles.listbox1,'Value',1);
pushbutton3_Callback(handles.pushbutton3, eventdata, handles);

% --- Executes when user attempts to close figure1.
function figure1_CloseRequestFcn(hObject, eventdata, handles)
% hObject    handle to figure1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

guiCloseFn('clinetList',handles);


% --- Update List
function pushbutton3_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global FL
[stat,userList]=sockrw('get_userList','cas');
nusers=size(userList);
listStr={};
if nusers(1)>0
  for iuser=1:nusers(1)
    if isfield(FL.AS.(userList{iuser,1}),'firstCom')
      listStr{end+1}=[userList{iuser,1},' (',userList{iuser,2},') / ',datestr(FL.AS.(userList{iuser,1}).firstCom),' / ',datestr(FL.AS.(userList{iuser,1}).lastCom)];
    else
      listStr{end+1}=[userList{iuser,1},' (',userList{iuser,2},') / NOCOM / NOCOM'];
    end
  end
end
set(handles.listbox1,'String',listStr);
  


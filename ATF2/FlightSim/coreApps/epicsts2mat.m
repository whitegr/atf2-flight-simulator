function tsout=epicsts2mat(ts,FlObj)
% EPICSTS2MAT
% Put epics time stamp as Matlab datenum format in gui requested
% local time
global FL
if ~exist('FlObj','var')
  FlObj=FL;
end
persistent toffset
if isempty(toffset)
  toffset=datenum('1-jan-1970');
end
try
  tsout=toffset+floor(real(ts+FlObj.timezone*3600)*1e3+imag(ts)*1e-6)/86400000; 
catch
  tsout=0;
end
function [stat,output]=bpmave(raw,nave,cutvals)
% Perform quality cuts and bpm averaging on passed data
%   raw should be the output of [s,raw]=FlHwUpdate('readbuffer',nave)
%   nave should be the same as 2nd argument of FlHwUpdate('readbuffer',nave)
%   cutvals :Quality cuts to apply ([] = use defaults)
%          [minq maxRMS whichICT returnData] (defaults: [0.5 3 1 0])
%          minq: min charge cut (pulses dropped with charge as measured on
%            dump ICT < this fraction of max charge for N pulse window)
%          maxRMS: fliers > than this calculated RMS removed from average
%          whichICT: 0=ICTDUMP 1=ICT1X
%          returnBadBPMs: 1,0
%   stat: Lucretia status response
%   output = {[meanData.h; meanData.v; meanData.t; rmsData.h; rmsData.v;
%             rmsData.t; nbad.h; nbad.v; nbad.t] badbpms};

global INSTR;
global BEAMLINE;

extend=0;

% Argument check
stat{1}=1; output={}; dataUsed={};
bpmData.h=raw(:,2:3:end);
bpmData.v=raw(:,3:3:end);
bpmData.t=raw(:,4:3:end);
ICT1X=findcells(INSTR,'Index',findcells(BEAMLINE,'Name','ICT1X'));
ICTDUMP=findcells(INSTR,'Index',findcells(BEAMLINE,'Name','ICTDUMP'));
ict(:,1)=raw(:,ICTDUMP*3+1);
ict(:,2)=raw(:,ICT1X*3+1);

[npulse,ndata]=size(bpmData.h);
if ~exist('nave','var')
  stat{1}=-1; stat{2}='bpmave: must pass nave'; return;
elseif nave<2 || nave>npulse
  stat{1}=-1; stat{2}='bpmave: nave must be >=2 and <= size of buffered data'; return;
end % nave check


% cuts to apply
if exist('cutvals','var') && ~isempty(cutvals)
  if isnumeric(cutvals) && length(cutvals)==4
    minq=cutvals(1);maxRMS=cutvals(2);whichICT=cutvals(3);returnData=cutvals(4);
  else
    stat{1}=-1;stat{2}='Bad cutvals argument to bpmave';return;
  end % arg check cutvals
else % use defaults
  minq=0.5; % relative ict charge cut 
  maxRMS=3; % fliers > than this calculated RMS removed from average
  whichICT=0; % ICTDUMP used for cuts
  returnData=0; % badbpms or ict data not returned by default
end % if cutvals given
badSIG=0; % bpms with readings exactly = this removed from average
maxVAL=5e-2; % abs Values returned that are greater than this assumed bad
% choose ict data
if whichICT==0
  ict=ict(:,1);
elseif whichICT==1
  ict=ict(:,2);
else
  stat{1}=-1; stat{2}='ICT choice error, must = 0 or 1';
  return
end
% get raw data 
rawData.h=bpmData.h(end-nave+1:end,:);
rawData.v=bpmData.v(end-nave+1:end,:);
rawData.t=bpmData.t(end-nave+1:end,:);

dims='hvt';

newDataReq=true;
while newDataReq
  for idim=1:length(dims)
    rawData.(dims(idim))(isnan(rawData.(dims(idim))))=badSIG; % make NaN's=badSIG
    np.(dims(idim))=size(rawData.(dims(idim)));
    % get stats (form mean & std from request # of pulses)
    goodDataCount=cumsum(rawData.(dims(idim))~=badSIG);
    rmsData.(dims(idim))=arrayfun(@(x) ...
      std(rawData.(dims(idim))(((rawData.(dims(idim))(:,x)~=badSIG)&goodDataCount(:,x)<=nave),x)),1:ndata);
    meanData.(dims(idim))=arrayfun(@(x) ...
     mean(rawData.(dims(idim))(((rawData.(dims(idim))(:,x)~=badSIG)&goodDataCount(:,x)<=nave),x)),1:ndata);
    % --- cut low tmit's
    tcut=abs(ict(end-np.(dims(idim))(1)+1:end))<(max(abs(ict(end-np.(dims(idim))(1)+1:end))*minq));
    rawData.(dims(idim))(tcut,:)=badSIG;
    % --- cut large values
    if idim<3
      rawData.(dims(idim))(abs(rawData.(dims(idim)))>maxVAL)=badSIG;
    end % if x or y
    % -- cut repeated entries
    for ipulse=1:np.(dims(idim))
      for ibpm=1:ndata
        if ipulse>1
          lastGood=ipulse-1;
          if rawData.(dims(idim))(ipulse-1,ibpm)==badSIG % search for last good pulse
            if (ipulse-1)>1
              for ip=1:ipulse-1
                if rawData.(dims(idim))(ip,ibpm)~=badSIG
                  lastGood=ip;
                end % if good
              end % for ip
            end % if ipulse-1>1
          end % if badSIG previously
          if rawData.(dims(idim))(ipulse,ibpm)==rawData.(dims(idim))(lastGood,ibpm)
            for ip=ipulse:np.(dims(idim))
              if rawData.(dims(idim))(ip,ibpm)==rawData.(dims(idim))(lastGood,ibpm)
                rawData.(dims(idim))(ip,ibpm)=badSIG;
              else
                break
              end % if repeated reading
            end % for ip
          end % if same as lastGood
        end % if not first pulse
      end % for ibpm
      % -- remove fliers
      if idim<3
        rawData.(dims(idim))(ipulse,((rawData.(dims(idim))(ipulse,:)-meanData.(dims(idim)))./ ...
          rmsData.(dims(idim)))>maxRMS)=badSIG;
      end % if idim = x or y
    end % for ipulse
    % get number of bad pulses
    goodDataCount=cumsum(rawData.(dims(idim))~=badSIG);
    nbad.(dims(idim))=sum((rawData.(dims(idim))==badSIG)&(goodDataCount<=nave));
    % If extend requested- add more data to raw
    if idim==1; newDataReq=false; end;
    if extend && any(nbad.(dims(idim))) && all((npulse-nave-(nbad.(dims(idim))+1))>0)
      rawData.(dims(idim))=[bpmData(end-nave-max(nbad.(dims(idim)))+1:end-nave,idim:3:end); rawData.(dims(idim))];
      newDataReq=true;
    else
      if extend && any((npulse-nave-(max(nbad.(dims(idim)))+1))<1); stat{1}=0; stat{2}='extend fail'; end;
    end % if extend wanted and able
  end % for idim
end % while newDataReq
if bitget(returnData,1)
  dataUsed={rawData.h~=badSIG rawData.v~=badSIG rawData.t~=badSIG};
else
  dataUsed={0 0 0};
end
output={[meanData.h; meanData.v; meanData.t; rmsData.h; rmsData.v; rmsData.t; nbad.h; nbad.v; nbad.t] dataUsed};

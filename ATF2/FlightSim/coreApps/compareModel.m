function [stat, bvals_online, bvals_model] = compareModel(mdl_str,class,noplot)
% [stat, bvals_online, bvals_model] = compareModel(mdl_str,class)
% Compare the magnet strengths of the current in-memory ATF2 model and that
% from the requested save model. For the requested magnet class, the true
% magnet strength is plotted as a function of beamline position.
% The results are automatically plotted unless requested not to.
% Inputs:
% mdl_str : String containing name of model for comparison (in latticeFiles
% directory)
% class : Class of magnet to compare (using BEAMLINE convention), e.g.
% 'QUAD' , 'SBEN', 'SEXT' etc...
% noplot : set = 1 if supression of plotting function desired
% Outputs:
% stat : Lucretia status response
% bvals_online : B field values from online model
% bvals_model : B field's from requested comparison model

global BEAMLINE PS

% get model values
[stat, mdldata] = readSimModel(mdl_str);
if stat{1}<0; return; end;
if ~isfield(mdldata,'BEAMLINE') || isempty(mdldata.BEAMLINE) || ~isfield(mdldata,'PS') || isempty(mdldata.PS)
  stat{1}=-1; stat{2}='Error reading BEAMLINE and PS globals from readSimModel';
  return
end % if returned globals empty

% get beamline indicies to compare
ind=findcells(BEAMLINE,'Class',class);

bvals_online=arrayfun(@(x) BEAMLINE{x}.B(1)*PS(BEAMLINE{x}.PS).Ampl,ind);
bvals_model=arrayfun(@(x) mdldata.BEAMLINE{x}.B(1)*mdldata.PS(mdldata.BEAMLINE{x}.PS).Ampl,ind);
svals=arrayfun(@(x) BEAMLINE{x}.S,ind);
% fix S
sfix=0;
for is=2:length(svals)
  if svals(is)<svals(is-1) && ~sfix
    sfix=svals(is-1);
  end % if s decreased
  svals(is)=sfix+svals(is);
end % for is

% plot results
if ~exist('noplot','var') || ~isequal(noplot,1)
  figure(1)
  subplot(111);
  plot(svals,bvals_online,svals,bvals_model)
end % if no request to suppress plotting
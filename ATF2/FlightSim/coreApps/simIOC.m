function simIOC(cmd)
% Start/Stop ATF2 IOC's in simulation environment

global FL

startx=0;
if ispc
  startx=0; % set this to 1 if you're running FS from a "startx" window
end

if ~isfield(FL,'SimMode') || ~exist('cmd','var') || ~ischar(cmd); error('Syntax error in function'); end;

if ~FL.SimMode; error('Trying to start resident IOC''s in production mode!'); end;

% choose ioc's to start based on FL.expt
% IOC's: PS QMOV ATF
if ~isfield(FL,'expt'); error('Need to specify FL.expt field'); end;
switch FL.expt % choose to start [ PS QMOV ATF ] IOC's
  case 'ATF2'
    ioc2init=[true true true];
  case 'ATF'
    ioc2init=[false false true];
  otherwise
    error('Do not know about this experiment!');
end % switch FL.expt

switch lower(cmd)
  case 'start'
    % Get EPICS host arch
    epicsHostArch=getenv('EPICS_HOST_ARCH');
    if isempty(epicsHostArch); error('Must set $EPICS_HOST_ARCH!'); end;
    % configure Access Security File
    fid=fopen(fullfile(FL.atfDir,'control-software','epics-3.14.8','AccessSecurity-simMode.acf'));
    AStext=textscan(fid,'%s'); AStext=AStext{1};
    fclose(fid);
    AStext=regexprep(AStext,'user_trust',FL.trusted.username);
    AStext=regexprep(AStext,'host_trust',FL.trusted.hostname);
    fid=fopen(fullfile(FL.atfDir,'control-software','epics-3.14.8','AccessSecurity.acf'),'w');
    cellfun(@(x) fprintf(fid,'%s\n',x),AStext);
    fclose(fid);
    sh1='#!/bin/bash';
    % PS IOC
    if ioc2init(1)
      disp('Starting PS ioc ...')
      cmd=['rm -f ',fullfile(FL.atfDir,'control-software','epics-3.14.8','ioc-ps','iocBoot','iocps','dbList.txt')];system(cmd);
      sh2=['cd ',fullfile(FL.atfDir,'control-software','epics-3.14.8','ioc-ps','iocBoot','iocps')];
      sh3=['export FS_PORT=',num2str(str2double(getenv('FS_PORT'))+2)];
      fid=fopen('startPSIOC.sh','w');
      if isempty(getenv('FS_PORT'))
        sh4=[fullfile('..','..','bin',epicsHostArch,'ps'),' ./st-simnoport.cmd'];
        fprintf(fid,'%s\n%s\n%s\n',sh1,regexprep(sh2,'\','/'),regexprep(sh4,'\','/'));
      else
        sh4=[fullfile('..','..','bin',epicsHostArch,'ps'),' ./st-sim.cmd'];
        fprintf(fid,'%s\n%s\n%s\n%s\n',sh1,regexprep(sh2,'\','/'),sh3,regexprep(sh4,'\','/'));
      end
      fclose(fid);
      cmd='chmod +x startPSIOC.sh';system(cmd);
      cmd='xterm -e ./startPSIOC.sh &';system(cmd);
      if startx
        disp('... click in startx window, then come back and press any key ...')
        pause
      end
      pid=getPid('ps');
      if isempty(pid) 
        error('Failed to ge PID for PSIOC, aborting...')
      end
      FL.IOC.ps_pid=pid{1}{1};
      fprintf('... PID=%s\n',FL.IOC.ps_pid)
      % Give IOC 60s to initialise
      t0=toc;
      fname=fullfile(FL.atfDir,'control-software','epics-3.14.8','ioc-ps','iocBoot','iocps','dbList.txt');
      while ~exist(fname,'file')
        if (toc-t0)>60
          error('Timeout waiting for PS IOC to start, aborting...');
        end % if timeout
        pause(1)
      end % while IOC still booting
      fprintf('... created %s\n',fname)
    end % if ioc2init(1)
    % QMOVER IOC
    if ioc2init(2)
      disp('Starting QMOV ioc ...')
      cmd=['rm -f ',fullfile(FL.atfDir,'control-software','epics-3.14.8','ioc-qmov','iocBoot','iocqmov','dbList.txt')];system(cmd);
      sh2=['cd ',fullfile(FL.atfDir,'control-software','epics-3.14.8','ioc-qmov','iocBoot','iocqmov')];
      sh3=['export FS_PORT=',num2str(str2double(getenv('FS_PORT'))+4)];
      fid=fopen('startQMOVIOC.sh','w');
      if isempty(getenv('FS_PORT'))
        sh4=[fullfile('..','..','bin',epicsHostArch,'qmov'),' ./st-simnoport.cmd'];
        fprintf(fid,'%s\n%s\n%s\n',sh1,regexprep(sh2,'\','/'),regexprep(sh4,'\','/'));
      else
        sh4=[fullfile('..','..','bin',epicsHostArch,'qmov'),' ./st-sim.cmd'];
        fprintf(fid,'%s\n%s\n%s\n%s\n',sh1,regexprep(sh2,'\','/'),sh3,regexprep(sh4,'\','/'));
      end
      fclose(fid);
      cmd='chmod +x startQMOVIOC.sh';system(cmd);
      cmd='xterm -e ./startQMOVIOC.sh &';system(cmd);
      if startx
        disp('... click in startx window, then come back and press any key ...')
        pause
      end
      pid=getPid('qmov');
      if isempty(pid)
        error('Failed to get PID for QMOVIOC, aborting...')
      end
      FL.IOC.qmov_pid=pid{1}{1};
      fprintf('... PID=%s\n',FL.IOC.qmov_pid)
      % Give IOC 60s to initialise
      t0=toc;
      fname=fullfile(FL.atfDir,'control-software','epics-3.14.8','ioc-qmov','iocBoot','iocqmov','dbList.txt');
      while ~exist(fname,'file')
        if (toc-t0)>60
          error('Timeout waiting for QMOV IOC to start, aborting...');
        end % if timeout
        pause(1)
      end % while IOC still booting
      fprintf('... created %s\n',fname)
    end % if ioc2init(2)
    % ATF IOC
    if ioc2init(3)
      disp('Starting ATF ioc ...')
      cmd=['rm -f ',fullfile(FL.atfDir,'control-software','epics-3.14.8','ioc-atf','iocBoot','iocatf','dbList.txt')];system(cmd);
      sh2=['cd ',fullfile(FL.atfDir,'control-software','epics-3.14.8','ioc-atf','iocBoot','iocatf')];
      sh3=['export FS_PORT=',num2str(str2double(getenv('FS_PORT')))];
      fid=fopen('startATFIOC.sh','w');
      if isempty(getenv('FS_PORT'))
        sh4=[fullfile('..','..','bin',epicsHostArch,'atf'),' ./st-simnoport.cmd'];
        fprintf(fid,'%s\n%s\n%s\n',sh1,regexprep(sh2,'\','/'),regexprep(sh4,'\','/'));
      else
        sh4=[fullfile('..','..','bin',epicsHostArch,'atf'),' ./st-sim.cmd'];
        fprintf(fid,'%s\n%s\n%s\n%s\n',sh1,regexprep(sh2,'\','/'),sh3,regexprep(sh4,'\','/'));
      end
      fclose(fid);
      cmd='chmod +x startATFIOC.sh';system(cmd);
      cmd='xterm -e ./startATFIOC.sh &';system(cmd);
      if startx
        disp('... click in startx window, then come back and press any key ...')
        pause
      end
      pid=getPid('atf');
      if isempty(pid)
        error('Failed to get PID for ATFIOC, aborting...')
      end
      FL.IOC.atf_pid=pid{1}{1};
      fprintf('... PID=%s\n',FL.IOC.atf_pid)
      % Give IOC 60s to initialise
      t0=toc;
      fname=fullfile(FL.atfDir,'control-software','epics-3.14.8','ioc-atf','iocBoot','iocatf','dbList.txt');
      while ~exist(fname,'file')
        if (toc-t0)>60
          error('Timeout waiting for ATF IOC to start, aborting...');
        end % if timeout
        pause(1)
      end % while IOC still booting
      fprintf('... created %s\n',fname)
    end % if ioc2init(3)
    % Initialise read arrays
    pause(0.5);
    try
      lcaPut('ATF:readBPMS',zeros(1,495));
    catch
      pause(10)
      lcaPut('ATF:readBPMS',zeros(1,495));
    end % try/catch
    lcaPut('ATF:readMAGS',zeros(1,522));
  case 'stop'
    % Stop running IOC's
    if isfield(FL,'IOC')
      iocs=fieldnames(FL.IOC);
      for i_ioc=1:length(iocs)
        if ~isempty(strfind(iocs{i_ioc},'_pid'))
          evalc(['!kill ',FL.IOC.(iocs{i_ioc})]);
        end % if pid field
      end % for i_ioc
    end % is FL.IOC
    FL=rmfield(FL,'IOC');
  otherwise
    errordlg('Unkown command in simIOC','IOC error');
    error('Unkown command');
end % switch cmd


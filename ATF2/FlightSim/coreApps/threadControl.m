function varargout = threadControl(varargin)
% THREADCONTROL M-file for threadControl.fig
%      THREADCONTROL, by itself, creates a new THREADCONTROL or raises the existing
%      singleton*.
%
%      H = THREADCONTROL returns the handle to a new THREADCONTROL or the handle to
%      the existing singleton*.
%
%      THREADCONTROL('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in THREADCONTROL.M with the given input arguments.
%
%      THREADCONTROL('Property','Value',...) creates a new THREADCONTROL or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before threadControl_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to threadControl_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help threadControl

% Last Modified by GUIDE v2.5 26-Jan-2009 18:15:48

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @threadControl_OpeningFcn, ...
                   'gui_OutputFcn',  @threadControl_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before threadControl is made visible.
function threadControl_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to threadControl (see VARARGIN)
global FL

% Choose default command line output for threadControl
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes threadControl wait for user response (see UIRESUME)
% uiwait(handles.figure1);

% create and/or start timer to keep this gui updated
if ~isfield(FL,'t_threadgui')
  FL.t_threadgui=timer('StartDelay',2,'Period',0.5,...
  'ExecutionMode','FixedRate','BusyMode','queue');
  FL.t_threadgui.TimerFcn='threadControl(''updateFcn'')';
  start(FL.t_threadgui)
elseif ~strcmp(FL.t_threadgui.Running,'on')
  start(FL.t_threadgui)
end
function updateFcn %#ok<DEFNU>
global FL
persistent lastFlash
handles=FL.Gui.threadControl;
defcol=[0.702 0.702 0.702];
butlabel=3:14;
if isfield(FL,'threads') && isfield(FL.threads,'status') && ~isempty(FL.threads.status)
  for ithread=1:length(FL.threads.status)
    switch FL.threads.status{ithread}
      case 'stopped'
        butcol='red';
        flash=false;
      case 'busy'
        butcol='yellow';
        flash=false;
      case 'running'
        butcol='green';
        flash=false;
      case 'starting'
        butcol='green';
        flash=true;
      case 'stopping'
        butcol='red';
        flash=true;
      otherwise
        butcol='black';
        flash=true;
    end
    % Make buttons corresponding to available threads visible
    set(handles.(['pushbutton',num2str(butlabel(ithread))]),'Visible','on');
    % Set bkg colour of buttons according to thread status
    if length(lastFlash)<ithread; lastFlash(ithread)=false; end;
    if flash && lastFlash(ithread)
      set(handles.(['pushbutton',num2str(butlabel(ithread))]),'BackgroundColor',butcol);
      lastFlash(ithread)=false;
    elseif flash
      set(handles.(['pushbutton',num2str(butlabel(ithread))]),'BackgroundColor',defcol);
      lastFlash(ithread)=true;
    else
      set(handles.(['pushbutton',num2str(butlabel(ithread))]),'BackgroundColor',butcol);
    end
  end
end
drawnow('expose')
% --- Outputs from this function are returned to the command line.
function varargout = threadControl_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles;
% --- Exit
function pushbutton1_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
guiCloseFn('threadControl',handles);

% --- Executes on selection change in popupmenu1.
function popupmenu1_Callback(hObject, eventdata, handles)
% hObject    handle to popupmenu1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% Hints: contents = get(hObject,'String') returns popupmenu1 contents as cell array
%        contents{get(hObject,'Value')} returns selected item from popupmenu1

% --- Executes during object creation, after setting all properties.
function popupmenu1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to popupmenu1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% --- Executes on button press in checkbox1.
function checkbox1_Callback(hObject, eventdata, handles)
% hObject    handle to checkbox1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% --- Start all
function pushbutton2_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
for ibut=3:14
  if strcmp(get(handles.(['pushbutton',num2str(ibut)]),'Visible'),'on')
    set(gcbf,'SelectionType','normal');
    set(gcbf,'CurrentObject',handles.(['pushbutton',num2str(ibut)]));
    ButtonFcn(handles.(['pushbutton',num2str(ibut)]), eventdata, handles)
    pause(1)
  end
end

% --- Stop All
function pushbutton15_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton15 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
for ibut=3:14
  if strcmp(get(handles.(['pushbutton',num2str(ibut)]),'Visible'),'on')
    set(gcbf,'SelectionType','alt');
    set(gcbf,'CurrentObject',handles.(['pushbutton',num2str(ibut)]));
    ButtonFcn(handles.(['pushbutton',num2str(ibut)]), eventdata, handles)
    pause(1)
  end
end

% --- start/stop functionality for numbered buttons
function ButtonFcn(hObject, eventdata, handles)
% hObject    handle to pushbutton3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
set(hObject,'SelectionHighlight','on')
sel_typ = get(gcbf,'SelectionType');
ithread=str2double(get(get(gcbf,'CurrentObject'),'String'));
switch sel_typ
  case 'normal' % left mouse button
    stat = FlThreads('start',ithread);
    if stat{1}~=1
      errordlg(stat{2},'Thread start error')
    end
    set(hObject,'Selected','on')
  case 'extend' % shift-right mouse button
    stat = FlThreads('kill',ithread);
    if stat{1}~=1
      errordlg(stat{2},'Thread kill error')
    end
    set(hObject,'Selected','on')
  case 'alt' % right mouse button
    stat = FlThreads('stop',ithread);
    if stat{1}~=1
      errordlg(stat{2},'Thread stop error')
    end
    set(hObject,'Selected','on')
    set(hObject,'SelectionHighlight','off')
end

% --- Executes on button press in pushbutton3.
function pushbutton3_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% --- Executes when user attempts to close figure1.
function figure1_CloseRequestFcn(hObject, eventdata, handles)
% hObject    handle to figure1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: delete(hObject) closes the figure
guiCloseFn('threadControl',handles);

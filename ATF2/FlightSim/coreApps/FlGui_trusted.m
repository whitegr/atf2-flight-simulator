function varargout = FlGui_trusted(varargin)
% FLGUI_TRUSTED M-file for FlGui_trusted.fig
%      FLGUI_TRUSTED, by itself, creates a new FLGUI_TRUSTED or raises the existing
%      singleton*.
%
%      H = FLGUI_TRUSTED returns the handle to a new FLGUI_TRUSTED or the handle to
%      the existing singleton*.
%
%      FLGUI_TRUSTED('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in FLGUI_TRUSTED.M with the given input arguments.
%
%      FLGUI_TRUSTED('Property','Value',...) creates a new FLGUI_TRUSTED or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before FlGui_trusted_OpeningFunction gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to FlGui_trusted_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help FlGui_trusted

% Last Modified by GUIDE v2.5 24-Oct-2012 10:32:01

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
  'gui_Singleton',  gui_Singleton, ...
  'gui_OpeningFcn', @FlGui_trusted_OpeningFcn, ...
  'gui_OutputFcn',  @FlGui_trusted_OutputFcn, ...
  'gui_LayoutFcn',  [] , ...
  'gui_Callback',   []);
if nargin && ischar(varargin{1})
  gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
  [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
  gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before FlGui_trusted is made visible.
function FlGui_trusted_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to FlGui_trusted (see VARARGIN)
global FL

% Choose default command line output for FlGui_trusted
handles.output = handles;

% Make simulation settings button visible only when in sim mode
if ~FL.SimMode
  set(handles.pushbutton3,'String','h/w settings')
else
  set(handles.text2,'String','Server (SIM-MODE)');
end % if ~FL.SimMode

% Update handles structure
guidata(hObject, handles);

% load logo image
logoPIC=imread('images/fslogo.jpg');
axes(handles.axes1); image(logoPIC); axis off; %#ok<MAXES>
set(get(handles.axes1,'Children'),'ButtonDownFcn','FlMiscGui');

% Restore some stuff from previous session
restoreList={'edit1','String',false;
  'popupmenu1','Value',false;
  'popupmenu2','Value',false;
  'radiobutton3','Value',false;
  'radiobutton4','Value',false;
  'radiobutton5','Value',false;
  'radiobutton6','Value',false;
  'checkbox4','Value',true};
guiRestoreFn('FlGui_trusted',handles,restoreList);

% Display current read method
set(handles.text10,'String',FL.currentReadMethod)

% Set property values
if isfield(FL,'props')
  if isfield(FL.props,'dE')
    set(handles.dE,'Label',sprintf('dE/E = %.3g',FL.props.dE))
  end
end


% --- Outputs from this function are returned to the command line.
function varargout = FlGui_trusted_OutputFcn(hObject, eventdata, handles)
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on button press in pushbutton1.
function pushbutton1_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global FL
resp=questdlg('Are you sure?','Exit Request','Yes','No','No');
if ~strcmp(resp,'Yes'); return; end;
try
  FL.dostop=true;
catch
  exit
end

% --- Executes on button press in pushbutton2.
function pushbutton2_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global FL

% Only allow this for the main server, not for other instances
if FL.instance~=1
  errordlg('This server functions only on main server instance for now');
  return
end

if isequal(get(hObject,'BackgroundColor'),[1 0 0])
  % Launch gui
  if ~isfield(FL,'Gui') || ~isfield(FL.Gui,'as') || ~ishandle(FL.Gui.as.figure1)
    FL.Gui.as = FAS_main;
    % Initialise listboxes to empty
    set(FL.Gui.as.listbox1,'String','');
    set(FL.Gui.as.listbox2,'String','');
  end % start gui if not already running
  if ~isfield(FL,'AS') || ~isfield(FL.AS,'t_main') || isequal(FL.AS.t_main.running,'off')
    % Init and run server code and timer
    FloodlandAccessServer('init');
    FL.AS.t_main=timer('StartDelay',1,'Period',FL.timerPeriod_as,...
      'ExecutionMode','FixedRate','BusyMode','drop');
    FL.AS.t_main.TimerFcn = 'FloodlandAccessServer(''run'');';
    FL.AS.t_main.StopFcn = 'FloodlandAccessServer(''stop'');';
    FL.AS.t_main.StartFcn = 'FloodlandAccessServer(''start'');';
    start(FL.AS.t_main);
    % start command handler only after successfull connection
    if ~isfield(FL,'t_commandHandler') || ~isequal(FL.t_commandHandler.running,'on')
      ServerCommandHandler('init');
      FL.t_commandHandler=timer('StartDelay',1,'Period',FL.Period_commandHandler,...
        'ExecutionMode','FixedRate','BusyMode','drop');
      FL.t_commandHandler.TimerFcn = 'ServerCommandHandler(''run'',''cas'')';
      FL.t_commandHandler.StopFcn = 'ServerCommandHandler(''stop'',''cas'')';
      start(FL.t_commandHandler);
    end % if no command handler timer or not running
  end % if server timer stopped or not created
  % indicate server running
  set(hObject,'BackgroundColor','Green','String','Access Server ON');
else
  % Check really want to disable server
  if strcmp(questdlg('Disable Access Server?','Server Shutdown Request'),'Yes')
    if isfield(FL,'AS') && isfield(FL.AS,'t_main') && isequal(FL.AS.t_main.running,'on')
      stop(FL.AS.t_main);
    end % if running, stop server which also deletes guis
    if isfield(FL,'t_commandHandler') && isequal(FL.t_commandHandler.running,'on')
      stop(FL.t_commandHandler);
    end % stop command handler timer if running
    set(hObject,'BackgroundColor','Red','String','Access Server OFF');
  end
end % if button red





function edit1_Callback(hObject, eventdata, handles)
% hObject    handle to edit1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit1 as text
%        str2double(get(hObject,'String')) returns contents of edit1 as a double


% --- Executes during object creation, after setting all properties.
function edit1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
  set(hObject,'BackgroundColor','white');
end


% --- Sim settings or h/w settings gui launch
function pushbutton3_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global FL

if FL.SimMode
  % Launch SimSettings Gui
  FL.Gui.SimSettings=SimSettings;
else
  FL.Gui.hwSettings=hwSettings;
end



% --- Executes on button press in pushbutton4.
function pushbutton4_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)




% --- Executes on selection change in popupmenu1.
function popupmenu1_Callback(hObject, eventdata, handles)
% hObject    handle to popupmenu1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = get(hObject,'String') returns popupmenu1 contents as cell array
%        contents{get(hObject,'Value')} returns selected item from popupmenu1


% --- Executes during object creation, after setting all properties.
function popupmenu1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to popupmenu1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
  set(hObject,'BackgroundColor','white');
end




% --- Executes when user attempts to close figure1.
function figure1_CloseRequestFcn(hObject, eventdata, handles)
% hObject    handle to figure1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

global FL
try
  guiCloseFn('FlGui_trusted',handles);
  stop(FL.t_main);
catch
  exit
end




% --- Executes on button press in pushbutton5.
function pushbutton5_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton5 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global FL

% Application menu
FL.Gui.trustedApps=trustedApps;



% --- Executes on selection change in popupmenu2.
function popupmenu2_Callback(hObject, eventdata, handles)
% hObject    handle to popupmenu2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = get(hObject,'String') returns popupmenu2 contents as cell array
%        contents{get(hObject,'Value')} returns selected item from popupmenu2


% --- Executes during object creation, after setting all properties.
function popupmenu2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to popupmenu2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
  set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in checkbox1.
function checkbox1_Callback(hObject, eventdata, handles)
% hObject    handle to checkbox1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of checkbox1


% --- Executes on button press in pushbutton6.
function pushbutton6_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton6 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global FL
% Authorization List Gui launch

FL.Gui.authList = authList;


% --- Executes on mouse press over axes background.
function axes1_ButtonDownFcn(hObject, eventdata, handles)
% hObject    handle to axes1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% --- Executes on mouse press over axes background.

msgbox('Do not click here again...','Be Warned...')



% --- Executes on button press in checkbox2.
function checkbox2_Callback(hObject, eventdata, handles)
% hObject    handle to checkbox2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of checkbox2


% --- Executes on button press in radiobutton3.
function radiobutton3_Callback(hObject, eventdata, handles)
% hObject    handle to radiobutton3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of radiobutton3


% --- Executes on button press in radiobutton4.
function radiobutton4_Callback(hObject, eventdata, handles)
% hObject    handle to radiobutton4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of radiobutton4


% --- Executes on button press in radiobutton5.
function radiobutton5_Callback(hObject, eventdata, handles)
% hObject    handle to radiobutton5 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of radiobutton5


% --- Executes on button press in radiobutton6.
function radiobutton6_Callback(hObject, eventdata, handles)
% hObject    handle to radiobutton6 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of radiobutton6




% --- Thread Control
function pushbutton7_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton7 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global FL
FL.Gui.threadControl=threadControl;

% --- User threads checkbox
function checkbox3_Callback(hObject, eventdata, handles)
% hObject    handle to checkbox3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% If want to use threads and server not running- run it
if get(hObject,'Value') && isequal(get(handles.pushbutton9,'BackgroundColor'),[1 0 0])
  pushbutton9_Callback(handles.pushbutton9,eventdata,handles);
end


% --- Executes on button press in pushbutton8.
function pushbutton8_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton8 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global FL

if ~isfield(FL,'t_dispMoni')
  useApp('dispersion_measurement');
  FL.t_dispMoni=timer('TimerFcn',@(x,y)dispMeasUpdate(x,y,'run'),...
  'period',1,'executionmode','fixedrate','BusyMode','drop',...
  'startfcn',@(x,y)dispMeasUpdate(x,y,'start'),...
  'stopfcn',@(x,y)dispMeasUpdate(x,y,'stop'));
  start(FL.t_dispMoni);
elseif ~strcmp(FL.t_dispMoni.running,'on')
  start(FL.t_dispMoni);
else
  stop(FL.t_dispMoni);
end


% --- Start/stop thread server
function pushbutton9_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton9 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% Start/Stop command handler
global FL

if isequal(get(hObject,'BackgroundColor'),[1 0 0])
  % Start command handler
  if ~isfield(FL,'t_commandHandler') || ~isequal(FL.t_commandHandler.running,'on')
    ServerCommandHandler('init');
    FL.t_commandHandler=timer('StartDelay',1,'Period',FL.Period_commandHandler,...
      'ExecutionMode','FixedRate','BusyMode','drop');
    FL.t_commandHandler.TimerFcn = 'ServerCommandHandler(''run'',''fl'')';
    FL.t_commandHandler.StopFcn = 'ServerCommandHandler(''stop'',''fl'')';
    start(FL.t_commandHandler);
  end % if command handler timer running
  % Initialise Floodland Server Socket
  sockrw('init-flserv');
  % indicate server running
  set(hObject,'BackgroundColor','Green');
else
  if isfield(FL,'t_commandHandler') && isequal(FL.t_commandHandler.running,'on')
    stop(FL.t_commandHandler);
  end % if running, stop command handler
  % Stop connection timer if running
  if isfield(FL.Server,'t_connect') && isequal(FL.Server.t_connect.Running,'on')
    stop(FL.Server.t_connect);
  end % if t_connect timer running, stop
  clear ServerConnect
  set(hObject,'BackgroundColor','Red');
  % Send disconnect messages to any connected clients and delete
  disp('closing server connections...')
  srvs={'Server'};
  ssrvs={'fl'};
  socks={'socket' 'servSocket'};
  for isrv=1:length(srvs)
    if isfield(FL,srvs{isrv})
      users=fieldnames(FL.(srvs{isrv}));
      for iuser=1:length(users)
        if strcmp(users{iuser}(1:4),'user')
          for isock=1:length(socks)
            if isfield(FL.(srvs{isrv}).(users{iuser}),socks{isock}) && ...
                ~FL.(srvs{isrv}).(users{iuser}).(socks{isock}).isClosed
              try
                % Send disconnect message and close socket
                sockrw('set_user',ssrvs{isrv},users{iuser,1});
                sockrw('writechar',ssrvs{isrv},'disconnect');
                pause(1)
                FL.(srvs{isrv}).(users{iuser}).socket.close;
                FL.(srvs{isrv})=rmfield(FL.(srvs{isrv}),users{iuser});
              catch
                FL.(srvs{isrv})=rmfield(FL.(srvs{isrv}),users{iuser});
                continue
              end % try/catch
            end % if ~closed
          end % for isock
        end % if user field
      end % for iuser
    end % if srv field exists
  end % for isrv
end % if button red



% --- Launch Client List GUI
function pushbutton11_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton11 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global FL

FL.Gui.clientList=clientList;



% --- Manual Update
function pushbutton12_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton12 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
FlHwUpdate('force'); % force update of all



% --- Archive GUI
function pushbutton13_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton13 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
FL.Gui.archiveGui=FlArchiveGui;



% --- save lattice now
function pushbutton14_Callback(hObject, eventdata, handles) %#ok<*INUSL>
% hObject    handle to pushbutton14 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global FL BEAMLINE PS GIRDER INSTR %#ok<NUSED>

save([FL.expt,'lat_current'],'BEAMLINE','PS','GIRDER','INSTR','FL');

% --- Watchdogs
function pushbutton15_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton15 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global FL
FL.Gui.FlWatchdogs=FlWatchdogs;

% --- Executes on button press in checkbox4.
function checkbox4_Callback(hObject, eventdata, handles)
% hObject    handle to checkbox4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of checkbox4


% --- Launch BPM tool
function pushbutton17_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton17 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global FL
FL.Gui.FlBpmTool=FlBpmTool;


% --- Edit timezone
function pushbutton18_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton18 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
FL.Gui.timeZoneGui=timeZoneGui;



% --- Executes on button press in pushbutton19.
function pushbutton19_Callback(hObject, eventdata, handles) %#ok<*INUSD,*DEFNU>
% hObject    handle to pushbutton19 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global FL
if FL.SimMode; return; end;
% Only allow this for the main server, not for other instances
if FL.instance~=1
  errordlg('This server functions only on main server instance for now');
  return
end
if isequal(get(hObject,'BackgroundColor'),[0 1 0]) && strcmp(FL.t_ecs.running,'on')
  stop(FL.t_ecs);
  set(hObject,'String','ECS OFF');
elseif strcmp(FL.t_ecs.running,'off')
  FlECS('start');
  set(hObject,'String','ECS ON');
end


% --- Executes on button press in pushbutton20.
function pushbutton20_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton20 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
if strcmp(get(hObject,'String'),'OFF')
  newpars.active=true;
else
  newpars.active=false;
end
FlFB('SetPars',newpars);


% --- Executes on button press in pushbutton21.
function pushbutton21_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton21 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global FL

FL.Gui.FlFB_settingsGUI = FlFB_settingsGUI;


% --- Executes during object creation, after setting all properties.
function text2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to text2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called


% --- Executes on button press in pushbutton22.
function pushbutton22_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton22 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global FL
if ~isfield(FL,'exciter') || ~strcmp(FL.exciter.FFS_switch,'on')
  if strcmp(questdlg('Enable excitation of FFS corrector magnets?','FFS Exciter'),'Yes')
    FlBeamExcite('SetRefV');
    FL.exciter.FFS_switch = 'on';
  end
else
  FL.exciter.FFS_switch = 'off';
end


% --- Executes on button press in pushbutton23.
function pushbutton23_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton23 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global FL

FL.Gui.FlBeamExciterSettings=FlBeamExciterSettings;


% --------------------------------------------------------------------
function Props_Callback(hObject, eventdata, handles)
% hObject    handle to Props (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


function E0_Callback(hObject, eventdata, handles)
setLatticeDesignE;

% --------------------------------------------------------------------
function dE_Callback(hObject, eventdata, handles)
% hObject    handle to dE (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global FL
if isfield(FL,'props') && isfield(FL.props,'dE')
  a=inputdlg('dE/E?','dE',1,{sprintf('%.3g',FL.props.dE)});
else
  a=inputdlg('dE/E?','dE',1,{'0.0008'});
end
if ~isempty(a)
  dE=str2double(a{1});
  FL.props.dE=dE;
  if exist('props.mat','file')
    save -append props.mat dE
  else
    save props.mat dE
  end
  set(hObject,'Label',sprintf('dE/E = %.3g',dE))
end


% --------------------------------------------------------------------
function Untitled_1_Callback(hObject, eventdata, handles)
% hObject    handle to Untitled_1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --------------------------------------------------------------------
function latticeSetE_Callback(hObject, eventdata, handles)
% hObject    handle to latticeSetE (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
setLatticeDesignE;


% --------------------------------------------------------------------
function E_offset_Callback(hObject, eventdata, handles)
% hObject    handle to E_offset (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
setLatticeDesignE;

function mdlout=Floodland(opt,dataSelect)
% Main Floodland control loop- the ATF2 "Flight Simulator" controls Module for
% Lucretia
% ------------------------------------------------------------------------------
% 04-Nov-2012, M. Woodley
%    Handle BH1R PS Ampl = NaN
% 10-Nov-2010, M. Woodley
%    Handle corrupted popupmenu1 'Value' (FlGui_trusted)
% 13-Feb-2010, M. Woodley
%    Change sign and ordering of FF matching quad limits when applying polarity
%    setting
% 15-Feb-2010, G. White
%    Change stop functionality (to exit cleanly, write to FL.dostop, then
%    this function picks up stop request at non-dangerous point of update
%    cycle)
% 27-Oct-2010, G. White
%    Add code to cope with multiple running server instances
% ------------------------------------------------------------------------------

global BEAMLINE PS GIRDER FL INSTR %#ok<NUSED>
persistent Model last_SimMode last_mode ringEnergyMag lastMOM BH1Ron tBex initMOM memUpdateTime

if (isempty(BH1Ron)),BH1Ron=1;end

switch opt
  case 'init'
    %% Initialisation
    disp('Initialising Floodland Module...')
    % Time to wait for trims to settle
    FL.trimPause=3; % s
    % Precision & buffer size to use when sending data over tcp/ip
    FL.comPrec=6;
    FL.sendSize=64000; % Max due to Java restricting <64K UTF transfer
    % Timestamp to use
    FL.tstamp='yymmmdd_HHMMSS';
    % Version Number
    FL.version='4.8'; % string
    FL.epicsVersion=2.9; % double
    % Default time zone
    FL.timezone=-7;
    FL.timezoneName='CA';
    FL.timezoneDST=1;
    FL.tzfunc=timeZones();
    if ~isfield(FL.props,'E_offset') || isempty(FL.props.E_offset)
      FL.props.E_offset=0;
    end
    % Get Train mode
    if ~FL.SimMode
      disp('Getting N Train Mode...')
      try
        lcaPut('V2EPICS:iRead','DB_BEAM::BEAMCTRL:NTRAIN');
        [~,ntrain]=system('caget -t V2EPICS:i');
        FL.ntrain=str2double(ntrain);
      catch
        disp('Failed to get N Train mode (maybe V2EPICS IOC not running?), assuming 1 train...')
        FL.ntrain=1;
      end
    end
    % Switch on getting of magnet polarities
    FL.doGetPolarities=false;
    % Load default or most recent data structures
    if ~exist('dataSelect','var') || isempty(dataSelect) || ~dataSelect
      dataSelect='default';
    end % default data set
    
    % load in lattice
    Model=FlLoad(dataSelect);
    
    % Add a vector of pointers to power supplies for off-axis extraction
    % quadrupoles ... they'll need special handling later
    XQuadPSList=[];
    id=findcells(BEAMLINE,'Name','QM6RX');
    if (~isempty(id))
      XQuadPSList=[XQuadPSList,BEAMLINE{id(1)}.PS];
    end
    id=findcells(BEAMLINE,'Name','QM7RX');
    if (~isempty(id))
      XQuadPSList=[XQuadPSList,BEAMLINE{id(1)}.PS];
    end
    if (~isempty(XQuadPSList))
      FL.SimModel.XQuadPSList=XQuadPSList;
    end
    % Rate to run timers at / timeouts etc.
    FL.accRate=3.12;
    if strcmp(FL.mode,'trusted')
      period=0.1;
    else
      period=0.1;
    end % if trusted/test
    FL.timerPeriod_as=period; % s
    FL.Period_commandHandler=period; % s
    FL.Period_floodland=period; % s
    FL.socketTimeout=1; % s
    % Choose sparse beam to track by default
    FL.simTrackMode='sparse'; FL.simBeamID=3;
    % ATF trusted server host MAC(s)
    FL.controlMAC={{'00' '12' '79' 'D3' '75' '49'}; % atfsad pc
      {'00' '30' '48' 'D5' 'A2' '66'}}; % flight-simulator pc
    % List of allowed "test" IP addresses (numerical addresses required)
    % - load auth file if exists
    if exist('authList.mat','file')
      load authList
      FL.allowedTestIP=authListData;
    else
      FL.allowedTestIP={'127.0.0.1'};
    end % if authList exist
    FL.thishostname=regexprep(evalc('!hostname'),'\s','');
    username_raw=regexprep(evalc('!whoami'),'.*\\',''); % remove hostname part of response on windoze vista
    FL.thisusername=regexprep(username_raw,'\s','');
    % Are we in Simulation/Test/Trusted Mode?
    trustMode_check = checkTrusted(FL.controlMAC);
    if strcmp(FL.mode,'trusted') && ~trustMode_check && ~FL.SimMode
      error('Not authorised to run in production trusted (server) mode');
    end % if sim mode allowed
    % Check to see if same sim mode status as before
    if strcmp(FL.mode,'trusted') && exist('runmode.mat','file')
      load runmode lastMode
      if FL.SimMode~=lastMode
        errordlg('Sim Mode not the same as last time Floodland ran- check config!','Startup error')
        error('Sim Mode not the same as last time Floodland ran- check config!')
      end % if mode not match
      lastMode=FL.SimMode;
      save runmode lastMode
    end % if run before
    % pvname list
    if isfield(FL,'PV')
      FL=rmfield(FL,'PV');
    end % if FL.PV
    FL.appsLast_main={};FL.pvs_last_main={};
    mdlout=Model;
    
    % Load settings from last run if exist
    stat=FlDataStore('Floodland');
    if stat{1}==1 && isfield(FL,'DataStore') && isfield(FL.DataStore,'Floodland') && ~isempty(FL.DataStore.Floodland)
      fn=fieldnames(FL.DataStore.Floodland);
      for ifn=1:length(fn)
        FL.(fn{ifn})=FL.DataStore.Floodland.(fn{ifn});
      end
    end
    
    %% Update tasks
  case 'update'
    update;
    % update guis
    drawnow('expose');
    
    %% Run tasks
  case 'run'
    try
      % Make sure command handler running
      if strfind(FL.mode,'trusted')
        %if FL.instance==1 && ~strcmp(FL.t_commandHandler.running,'on')
        %  warning('Lucretia:Floodland:commandHandlerNotRunning',...
        %    'Command Handler not running, restarting...')
        %  start(FL.t_commandHandler)
        %end
      end
      
      % Beam exciter
      if isempty(tBex)
        tBex=clock;
      end
      if etime(clock,tBex)>1/FL.accRate
        if strcmp(FL.mode,'trusted') && FL.instance==1 && isfield(FL,'exciter') && strcmp(FL.exciter.FFS_switch,'on')
          set(FL.Gui.main.pushbutton22,'String','FFS Exciter ON')
          set(FL.Gui.main.pushbutton22,'BackgroundColor','green')
          FlBeamExcite('run');
        elseif strcmp(FL.mode,'trusted')
          set(FL.Gui.main.pushbutton22,'String','FFS Exciter OFF')
          set(FL.Gui.main.pushbutton22,'BackgroundColor','red')
        end
        tBex=clock;
      end
      
      % run update tasks
      update;
      
      % check sim mode / run mode isn't attempted to be changed
      if strcmp(FL.mode,'trusted') && ~isempty(last_SimMode)
        if ~isequal(last_SimMode,FL.SimMode)
          errordlg('Internal SIM MODE changed- aborting','Floodland Error');
          error('Internal SIM MODE changed- aborting');
        end % if sim mode changes
      end % if last_SimMode set
      last_SimMode=FL.SimMode;
      if ~isempty(last_mode)
        if ~isequal(last_mode,FL.mode)
          errordlg('Internal mode changed- aborting','Floodland Error');
          error('Internal mode changed- aborting');
        end % if sim mode changes
      end % if last_SimMode set
      last_mode=FL.mode;
      
      if strcmp(FL.mode,'test') && ~FL.isthread % test environment tasks
        % Update list of user routines
        userapps=dir('testApps');
        if length(userapps)>2
          apps={userapps(arrayfun(@(x) ~isequal(x.name,'.svn'),userapps)).name};
          apps={apps{3:end}}; %#ok<CCAT1>
        else
          apps={};
        end % if any apps
        % only update when changes
        if ~isequal(apps,FL.appsLast_main)
          if isempty(apps)
            set(FL.Gui.main.popupmenu1,'Value',1);
            set(FL.Gui.main.popupmenu1,'String','None found');
          else
            set(FL.Gui.main.popupmenu1,'Value',1);
            set(FL.Gui.main.popupmenu1,'String',apps');
          end % if any found
          FL.appsLast_main=apps;
        end % if apps changed
        % Update Test Gui data fields
        if ~isfield(Model,'FL_period'); Model.FL_period=0.1; end;
        set(FL.Gui.main.text5,'String',sprintf('%.1f Hz',1/(1e-10+max([FL.t_main.InstantPeriod Model.FL_period]))));
        if length(Model.FL_period)>=20
          Model.FL_period(ceil(rand*20))=FL.t_main.InstantPeriod;
        else
          Model.FL_period(end+1)=FL.t_main.InstantPeriod;
        end
      elseif ~FL.isthread % trusted environment tasks
        % Update Trusted Gui data fields
        if ~isfield(Model,'FL_period'); Model.FL_period=0.1; end;
        if isempty(memUpdateTime) || etime(clock,memUpdateTime)>300
          set(FL.Gui.main.text5,'String',sprintf('%.1f MB',sum(arrayfun(@(x) x.bytes,whos))/1024/1024));
          memUpdateTime=clock;
        end
        set(FL.Gui.main.text7,'String',sprintf('%.1f Hz',1/(1e-10+max([FL.t_main.InstantPeriod Model.FL_period]))));
        set(FL.Gui.main.text15,'String',FL.SimModel.opticsName);
        set(FL.Gui.main.text13,'String',FL.SimModel.opticsVersion);
        if length(Model.FL_period)>=20
          Model.FL_period(ceil(rand*20))=FL.t_main.InstantPeriod;
        else
          Model.FL_period(end+1)=FL.t_main.InstantPeriod;
        end
      end % if test environment
      
      % Run any requested user functions
      if isfield(FL,'userfun')
        funs=fieldnames(FL.userfun);
        if ~isempty(funs)
          for ifun=1:length(funs)
            if ~isfield(FL,'userdata') || ~isfield(FL.userdata,funs{ifun})
              warndlg('FL.userfun supplied but not FL.userdata!','User function error');
              break
            end % if no userdata supplied
            if iscell(FL.userfun.(funs{ifun}))
              for iuserfun=1:length(FL.userfun.(funs{ifun}))
                evalc([FL.userfun.(funs{ifun}){iuserfun},'(FL.userdata.',funs{ifun},');']);
              end % for iuserfun
            else
              evalc([FL.userfun.(funs{ifun}),'(FL.userdata.',funs{ifun},');']);
            end % if userfun a cell array
          end % for ifun
        end % if function list not empty
      end % if there is a userfun structure
      
      % Update energy if ring bend current changes
      if isempty(ringEnergyMag)
        ringEnergyMag=findcells(BEAMLINE,'Name','BH1R1A');
      end
      if isnan(PS(BEAMLINE{ringEnergyMag(1)}.PS).Ampl)||PS(BEAMLINE{ringEnergyMag(1)}.PS).Ampl<=0
       if (BH1Ron)
          msg=sprintf('BH1R is OFF(?) ... using design momentum (%g GeV)', ...
            FL.SimModel.Design.Momentum);
          helpdlg(msg,'Floodland:momUpdate')
          BH1Ron=0;
        end
        FL.SimModel.Initial.Momentum=FL.SimModel.Design.Momentum;
      elseif (~isfield(FL,'noUpdate') || ~FL.noUpdate) &&  get(FL.Gui.main.popupmenu1,'Value')>1
        if ~isempty(initMOM) % first time around, just update database
          try
            clight=2.99792458e8; % speed of light (m/sec)
            Cb=1e9/clight;       % rigidity conversion (T-m/GeV)
            theta=-pi/18;        % 36 DR bends (2*pi/36)
            if initMOM==-1
              initMOM=FL.SimModel.Initial.Momentum;
            end
            FL.SimModel.Initial.Momentum=FL.props.E_offset+2*(BEAMLINE{ringEnergyMag(1)}.B(1)*PS(BEAMLINE{ringEnergyMag(1)}.PS).Ampl)/(Cb*theta);
            if isempty(lastMOM) || (abs(FL.SimModel.Initial.Momentum-lastMOM)/lastMOM)>1e-3 || FL.SimModel.Initial.Momentum~=BEAMLINE{1}.P
              for iele=findcells(BEAMLINE,'P')
                BEAMLINE{iele}.P=FL.SimModel.Initial.Momentum;
              end
              FlScaleSBEN % scale B values of SBENs with conv=0
              FlSetCORB(initMOM); % set XCOR and YCOR B-balues to new rigidity value
              % (also set design quad and sext B fields to correspond to new
              % determined E)
              Beam1=MakeBeam6DGauss(FL.SimModel.Initial,10001,5,0) ;
              Beam0=makeSingleBunch(Beam1);
              Beam2=MakeBeam6DSparse(FL.SimModel.Initial,5,1,11);
              FL.SimBeam={Beam1,Beam0,Beam2};
              lastMOM=FL.SimModel.Initial.Momentum;
              BH1Ron=1;
              FlHwUpdate('force'); % Force all database values to refresh to take account of new fields
            end % update twiss if ring energy changes by more than 0.1%
            lastMOM=FL.SimModel.Initial.Momentum;
            BH1Ron=1;
          catch
            warning('Floodland:momUpdate','Invalid setting parameters for momentum update routine: Q: %g P: %g (CHECK SOCKET SERVER)',FL.SimModel.Initial.Q,FL.SimModel.Initial.Momentum)
          end
          if isnan(FL.SimModel.Initial.Momentum)
            FL.SimModel.Initial.Momentum=FL.SimModel.Design.Momentum;
          end
        else
          initMOM=-1;
        end
      end
      % Special handling of off-axis extraction quadrupoles
      FlSetXQUAD;
      
      % Catch stop command
      if isfield(FL,'dostop')
        Floodland('stop');
      end
      
      % Update gui's
      drawnow('expose');
    catch LE
      if ~isempty(LE.stack)
        for istack=1:length(LE.stack)
          LE.stack(istack,1)
        end % for istack
      end % for length LE.stack
      rethrow(LE)
    end
    %% Error handling
  case 'errortrap'
    uiwait(errordlg('Error encountered, see command window for details, restart needed','Floodland Error'));
    LE=lasterror; %#ok<LERR>
    disp(LE.message);
    if ~isempty(LE.stack)
      for istack=1:length(LE.stack)
        LE.stack(istack,1)
      end % for istack
    end % for length LE.stack
    %% Exit tasks
  case 'stop'
    try
      % Store desired FL structure elements
      FlDataStore('Floodland','timezone',FL.timezone,'timezoneName',FL.timezoneName,'timezoneDST',FL.timezoneDST);
      % close any socket connections
      disp('closing server connections...')
      srvs={'AS' 'Server'};
      ssrvs={'cas' 'fl'};
      socks={'socket' 'servSocket'};
      for isrv=1:length(srvs)
        if isfield(FL,srvs{isrv})
          users=fieldnames(FL.(srvs{isrv}));
          for iuser=1:length(users)
            if strcmp(users{iuser}(1:4),'user')
              for isock=1:length(socks)
                if isfield(FL.(srvs{isrv}).(users{iuser}),socks{isock}) && ...
                    ~FL.(srvs{isrv}).(users{iuser}).(socks{isock}).isClosed
                  try
                    % Send disconnect message and close socket
                    sockrw('set_user',ssrvs{isrv},users{iuser,1});
                    sockrw('writechar',ssrvs{isrv},'disconnect');
                    pause(1)
                    FL.(srvs{isrv}).(users{iuser}).socket.close;
                  catch
                    continue
                  end % try/catch
                end % if ~closed
              end % for isock
            end % if user field
          end % for iuser
        end % if srv field exists
      end % for isrv
      % Shutdown main GUI
      guiCloseFn('FlGui_trusted',FL.Gui.main);
    catch
      uiwait(errordlg('Error in shutdown routine- click to exit','Floodland Error'));
      exit
    end % try/catch
    exit
  otherwise % unknown command option
    
    errordlg('Unknown function argument','Floodland Main');
    error('Unknown function argument');
    
end % switch opt

% -----------------------------------------------------------------------
% Functions
% -----------------------------------------------------------------------

function update
global FL BEAMLINE
persistent t0 watchdogList aperloop qmelelist t1

% Initialise timer
if isempty(t0)
  t0=clock;
  t1=clock;
  if FL.isthread
    FlHwUpdate;
  end
end

% write running status to file every minute
if strcmp(FL.mode,'trusted') && etime(clock,t1)>60
  t1=clock;
  sysStat=system(sprintf('touch RUNNING_%d',FL.instance));
  if sysStat; error('System error generating RUNNING file (file system full?)'); end;
end

% check GUI still exists
if ~ishandle(FL.Gui.main.figure1)
  return
end

% Control HW update rate
if isequal(FL.mode,'trusted') && ~FL.isthread
  updateOPTS=get(FL.Gui.main.popupmenu1,'String');
  val=get(FL.Gui.main.popupmenu1,'Value');
  if ~ismember(val,1:length(updateOPTS))
    val=1;
    set(FL.Gui.main.popupmenu1,'Value',val) % fix it
  end
  updateRate=str2double(updateOPTS{val});
  % if menu = 'NO AUTO', updateRate=NAN
elseif ~FL.isthread % test
  updateOPTS=get(FL.Gui.main.popupmenu2,'String');
  val=get(FL.Gui.main.popupmenu2,'Value');
  if ~ismember(val,1:length(updateOPTS))
    val=1;
    set(FL.Gui.main.popupmenu2,'Value',val) % fix it
  end
  updateRate=str2double(updateOPTS{val});
else
  updateRate=FL.Period_floodland;
  FlThreads;
end % if trusted/test

if ~isnan(updateRate)
  if etime(clock,t0)>(1/updateRate)
    % Update HW if trusted version or requested in test (FlHwUpdate also
    % generates AML file if trusted or requested in test
    if ~FL.isthread && (isequal(FL.mode,'trusted') || get(FL.Gui.main.checkbox1,'Value'))
      if ~isfield(FL,'noUpdate') || ~FL.noUpdate
        FlHwUpdate('autoUpdate');
      end
    end % if want hw update
    t0=clock;
  end % if more than 1/updateRate passed
end % if t0 set

% --- Watchdogs
if strcmp(FL.mode,'trusted')
  if isempty(watchdogList)
    % Generate watchdog data structures from watchdogApps
    if ~isdir('watchdogApps')
      errordlg('watchdogApps Directory not found!','Floodland Error')
      error('watchdogApps Directory not found!')
    end
    appList=dir('watchdogApps'); watchdogList={};
    if length(appList)>2
      disp('Initialising Watchdogs...')
      for iapp=3:length(appList)
        if ~strcmp(appList(iapp).name,'.svn')
          disp([appList(iapp).name,'...'])
          if exist(fullfile('watchdogApps',appList(iapp).name,[appList(iapp).name,'.m']),'file') && ...
              (~isfield(FL,'Watchdogs') || ~isfield(FL.Watchdogs,appList(iapp).name))
            try
            eval([appList(iapp).name,'(''init'');']);
            catch ME
              error(ME.message)
            end
            watchdogList{end+1}=appList(iapp).name;
          end
        end
      end
    end
  end
  % loop through watchdog alarms and set master alarm state and app alarm
  % states if master watchdog GUI open
  if isequal(FL.mode,'trusted') && ~FL.isthread && ~isempty(watchdogList) && get(FL.Gui.main.checkbox4,'Value')
    for iw=1:length(watchdogList)
      try
        evalc(['[appstat pars] = ',watchdogList{iw},'(''GetPars'');']);
        if ~iscell(appstat) || appstat{1}~=1 || ~isfield(pars,'active') || ~pars.active
          appstat{1}=-1;
        else
          evalc(['[appstat appAlarm(iw)] = ',watchdogList{iw},'(''isalarm'');']);
          if strcmp(watchdogList{iw},'apertures') && isfield(FL.Gui,'aperturesGui') &&...
              ishandle(FL.Gui.aperturesGui.figure1) && get(FL.Gui.aperturesGui.checkbox2,'Value')
            if isempty(aperloop) || aperloop==1
              aperturesGui('pushbutton9_Callback',FL.Gui.aperturesGui.pushbutton9,[],FL.Gui.aperturesGui);
              aperloop=2;
            else
              aperturesGui('pushbutton6_Callback',FL.Gui.aperturesGui.pushbutton6,[],FL.Gui.aperturesGui);
              aperloop=1;
            end
          end
        end
      catch
        fprintf('Floodland error: %s\n',watchdogList{iw})
        appstat{1}=-1;
      end
      % Get corresponding watchdog button by looking at userData fields
      % of all buttons
      if isfield(FL.Gui,'FlWatchdogs') && ishandle(FL.Gui.FlWatchdogs.figure1)
        fn=fieldnames(FL.Gui.FlWatchdogs);
        for ifn=1:length(fn)
          if ~isempty(strfind(fn{ifn},'pushbutton')) &&...
              ischar(get(FL.Gui.FlWatchdogs.(fn{ifn}),'UserData')) && ...
              strcmp(get(FL.Gui.FlWatchdogs.(fn{ifn}),'UserData'),watchdogList{iw})
            if appstat{1}~=1
              set(FL.Gui.FlWatchdogs.(fn{ifn}),'BackgroundColor',[0.863 0.863 0.863])
            elseif appAlarm(iw)==2 %#ok<NODEF>
              set(FL.Gui.FlWatchdogs.(fn{ifn}),'BackgroundColor','red')
            elseif appAlarm(iw)==1
              set(FL.Gui.FlWatchdogs.(fn{ifn}),'BackgroundColor','yellow')
            else
              set(FL.Gui.FlWatchdogs.(fn{ifn}),'BackgroundColor','green')
            end
          end
        end
      end
      drawnow('expose')
    end
    if appstat{1}~=1
      appAlarm(iw)=-1;
    end
    if ~any(appAlarm>=0)
      set(FL.Gui.main.pushbutton15,'BackgroundColor',[0.702 0.702 0.702])
    elseif any(appAlarm==2)
      set(FL.Gui.main.pushbutton15,'BackgroundColor','red')
    elseif any(appAlarm==1)
      set(FL.Gui.main.pushbutton15,'BackgroundColor','yellow')
    else
      set(FL.Gui.main.pushbutton15,'BackgroundColor','green')
    end
    drawnow('expose')
  elseif isequal(FL.mode,'trusted') && ~FL.isthread
    set(FL.Gui.main.pushbutton15,'BackgroundColor',[0.702 0.702 0.702])
  end
end

function trustMode = checkTrusted(controlMAC)
% Check to see if we are on the conrol system or not

% Get all network MAC addresses for this machine
if isunix
  str=evalc('!/sbin/ifconfig -a');
  pat='(..):(..):(..):(..):(..):(..)';
elseif ispc
  str=evalc('!ipconfig /all');
  pat='(..)-(..)-(..)-(..)-(..)-(..)';
else
  error('Trying to run on unsupported platform!')
end % platform check
t=regexp(str,pat,'tokens');
% See if any match control system MAC
trustMode = false;
for it=1:length(t)
  for itc=1:length(controlMAC)
    if isequal(upper(t{it}),controlMAC{itc})
      if isequal(getenv('ATF_FS_SIMMODE'),'1')
        error('Cannot run production trusted server in sim mode!');
      else
        trustMode=true;
      end % make sim mode if requested, otherwise set to production mode
    end % if MAC matches control system MAC
  end % control MAC loop
end % MAC loop


function Model = FlLoad(dataSelect)
% Load data structures
global BEAMLINE PS GIRDER FL INSTR %#ok<NUSED>

% List of required elements to load in from Lucretia lattice file
requiredList={'BEAMLINE','PS','GIRDER','Model','Model.ip_ind'};

if ~exist('dataSelect','var')
  dataSelect='default';
end % if ~dataSelect

% Choose lattice based on FL.expt
if ~isfield(FL,'expt'); error('No FL.expt set!'); end;

mode=FL.mode; expt=FL.expt;
switch dataSelect
  otherwise
    load([FL.expt,'lat'],'BEAMLINE','PS','GIRDER','Model','Beam*','INSTR');
    fcont=whos('-file','latticeFiles/ATF2lat.mat');
    if ismember('HwInfo',{fcont.name})
      load([FL.expt,'lat'],'HwInfo');
    end
end % switch dataSelect


% Check for things we need to have in the model
for iList=1:length(requiredList)
  try
    evalc(requiredList{iList});
  catch
    error('Missing requried element in lattice file: %s',requiredList{iList});
  end % try/catch
end % for iList
FL.mode=mode; FL.expt=expt;
% Added info
if ~isfield(Model,'extStart') %#ok<NODEF>
  Model.extStart=findcells(BEAMLINE,'Name','IEX');Model.extStart=Model.extStart(end);
end

% Make sparse beam, 1 z slice, 11 energy bins for fast 2nd moment tracking
Beam2 = MakeBeam6DSparse( Model.Initial, 5, 1, 11 );
FL.SimBeam={Beam1,Beam0,Beam2};

% Check tracking and generate INSTR cell array
% If IP not a MONI, make so for use as a virtual BPM to house IP fitted
% data
ipind=findcells(BEAMLINE,'Name','IP');
indqd0=findcells(BEAMLINE,'Name','MQD0FF');
if ~strcmp(BEAMLINE{ipind}.Class,'MONI')
  B=BPMStruc(0,'IP');
  B.S=BEAMLINE{ipind}.S;
  B.P=BEAMLINE{ipind}.P;
  B.TrackFlag=BEAMLINE{indqd0}.TrackFlag;
  BEAMLINE{ipind}=B;
end
Model.ip_ind=length(BEAMLINE);
[stat,~,instdata] = TrackThru( 1, Model.ip_ind, FL.SimBeam{3}, 1, 1, 0 );
if stat{1}~=1;
  warning(['Beam doesn''t track through loaded lattice! : ',stat{2:end}]); %#ok<WNTAG>
  if exist('INSTR','var') && ~isempty(INSTR)
    [~,instdata]=FlTrackThru(1, Model.ip_ind);
  end
end
[Model,INSTR] = CreateINSTR(Model,instdata,INSTR);

% Define regions
FL.Region.DR.ind=[1 Model.extStart-1];
FL.Region.FFS.ind=[Model.ffStart.ind length(BEAMLINE)];
FL.Region.EXT.ind=[Model.extStart Model.ffStart.ind-1];
FL.Region.ALL.ind=[1 length(BEAMLINE)];
FL.Region.EXTFFS.ind=[Model.extStart length(BEAMLINE)];

% Assign Hardware names to elements and load lookup tables (B-I)
FL.HwInfo=FlAssignHW(Model);
if exist('HwInfo','var')
  FL.HwInfo.GIRDER=HwInfo.GIRDER;
end

% Put model into global array
FL.SimModel=Model;

% There should be a model name, if not make it
if ~isfield(FL.SimModel,'opticsName')
  FL.SimModel.opticsName='';
end


function FlSetXQUAD()
%
% Special setting algorithm for off-axis extraction quadrupoles, where
% B(1) should scale with beam energy but not with PS.Ampl

global FL BEAMLINE PS

if (~isfield(FL.SimModel,'XQuadPSList')),return,end
XQuadPSList=FL.SimModel.XQuadPSList;
if (isempty(XQuadPSList)),return,end

Cb=1e9/299792458; % rigidity constant (T-m/GeV)

for m=1:length(XQuadPSList)
  idps=XQuadPSList(m); % pointer into PS (and FL.HwInfo.PS)
  if (idps==0),continue,end
  if (isempty(FL.HwInfo.PS(idps).conv)),continue,end
  if (FL.HwInfo.PS(idps).conv==0),continue,end
  n=PS(idps).Element(1); % pointer into BEAMLINE
  Angle=BEAMLINE{n}.Angle;
  if (Angle==0),continue,end
  energy=BEAMLINE{n}.P;
  BL=Cb*energy*Angle;
  Ampl=PS(idps).Ampl;
  BEAMLINE{n}.B(1)=BL/Ampl; % set B such that Ampl*B is properly scaled
end

end

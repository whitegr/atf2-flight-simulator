function varargout = FAS_main(varargin)
% FAS_MAIN M-file for FAS_main.fig
%      FAS_MAIN, by itself, creates a new FAS_MAIN or raises the existing
%      singleton*.
%
%      H = FAS_MAIN returns the handle to a new FAS_MAIN or the handle to
%      the existing singleton*.
%
%      FAS_MAIN('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in FAS_MAIN.M with the given input arguments.
%
%      FAS_MAIN('Property','Value',...) creates a new FAS_MAIN or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before FAS_main_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to FAS_main_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help FAS_main

% Last Modified by GUIDE v2.5 27-Mar-2009 13:38:23

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @FAS_main_OpeningFcn, ...
                   'gui_OutputFcn',  @FAS_main_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before FAS_main is made visible.
function FAS_main_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to FAS_main (see VARARGIN)

% Choose default command line output for FAS_main
handles.output = handles;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes FAS_main wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = FAS_main_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on selection change in listbox2.
function listbox2_Callback(hObject, eventdata, handles)
% hObject    handle to listbox2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = get(hObject,'String') returns listbox2 contents as cell array
%        contents{get(hObject,'Value')} returns selected item from listbox2


% --- Executes during object creation, after setting all properties.
function listbox2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to listbox2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: listbox controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in pushbutton3.
function pushbutton3_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
allowDeny('allow',handles);

% --- Executes on button press in pushbutton4.
function pushbutton4_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
allowDeny('allow_all',handles);

% --- Executes on button press in pushbutton5.
function pushbutton5_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton5 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
allowDeny('deny',handles);

% --- Executes on button press in pushbutton6.
function pushbutton6_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton6 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
allowDeny('deny_all',handles);


% --- Executes on button press in togglebutton1.
function togglebutton1_Callback(hObject, eventdata, handles)
% hObject    handle to togglebutton1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global FL

% Hint: get(hObject,'Value') returns toggle state of togglebutton1
if get(hObject,'Value')==1
  if FL.SimMode
    set(hObject,'BackGroundColor','cyan');
  else
    switch lower(questdlg('Auto Reply to all incoming requests?'))
      case 'yes'
        set(hObject,'BackGroundColor','cyan');
      otherwise
        set(hObject,'Value',0);
        set(hObject,'BackGroundColor',[204,204,204]./256);
    end % switch question
  end % if sim mode
else
  set(hObject,'BackGroundColor',[204,204,204]./256);
end % if push on
    

% --- Executes on selection change in popupmenu1.
function popupmenu1_Callback(hObject, eventdata, handles)
% hObject    handle to popupmenu1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = get(hObject,'String') returns popupmenu1 contents as cell array
%        contents{get(hObject,'Value')} returns selected item from popupmenu1


% --- Executes during object creation, after setting all properties.
function popupmenu1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to popupmenu1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in togglebutton2.
function togglebutton2_Callback(hObject, eventdata, handles)
% hObject    handle to togglebutton2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of togglebutton2
if get(hObject,'Value')==1
  switch lower(questdlg('Apply Timeout to all approved requests?'))
    case 'yes'
      set(hObject,'BackgroundColor',[255,177,100]./256);
    otherwise
      set(hObject,'Value',0);
      set(hObject,'BackgroundColor',[204,204,204]./256);
  end % switch question
else
  set(hObject,'BackGroundColor',[204,204,204]./256);
end % if push on


function edit1_Callback(hObject, eventdata, handles)
% hObject    handle to edit1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit1 as text
%        str2double(get(hObject,'String')) returns contents of edit1 as a double


% --- Executes during object creation, after setting all properties.
function edit1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in popupmenu2.
function popupmenu2_Callback(hObject, eventdata, handles)
% hObject    handle to popupmenu2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = get(hObject,'String') returns popupmenu2 contents as cell array
%        contents{get(hObject,'Value')} returns selected item from popupmenu2


% --- Executes during object creation, after setting all properties.
function popupmenu2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to popupmenu2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in listbox1.
function listbox1_Callback(hObject, eventdata, handles) %#ok<*INUSD>
% hObject    handle to listbox1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = get(hObject,'String') returns listbox1 contents as cell array
%        contents{get(hObject,'Value')} returns selected item from listbox1


% --- Executes during object creation, after setting all properties.
function listbox1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to listbox1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: listbox controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in pushbutton1.
function pushbutton1_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global FL
% ----------------------------------------
% Clear selected item(s) from allowed list
% ----------------------------------------
% Get selected items to remove
try
  guiList=get(handles.listbox1,'String');
  if ~iscell(guiList); guiList={guiList}; end;
  rmList=get(handles.listbox1,'Value');
  % Get user/req id
  for i_rmList=1:length(rmList)
    t=regexp(guiList{rmList(i_rmList)},'(.+)\s:\s(.+)\s:\s(.+):(.+)\s:\s(.+)','tokens');
    if ~isempty(t) && isfield(FL.AS,t{1}{3})
      FL.AS.(t{1}{3})=rmfield(FL.AS.(t{1}{3}),t{1}{4});
    end
  end % for i_rmList
  guiList={guiList{~ismember(1:length(guiList),rmList)}}';
  set(handles.listbox1,'String',guiList);
  set(handles.listbox1,'Value',1);
catch
  error('Error clearing item from list: %s',lasterr)
end

% --- Executes on button press in pushbutton2.
function pushbutton2_Callback(hObject, eventdata, handles) %#ok<*INUSL,*DEFNU>
% hObject    handle to pushbutton2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% global FL
% ----------------------------------------
% Clear all items from allowed list
% ----------------------------------------
set(handles.listbox1,'Value',1:length(get(handles.listbox1,'String')));
pushbutton1_Callback(handles.pushbutton1, eventdata, handles)
% userList=fieldnames(FL.AS);
% for iuser=1:length(userList)
%   if ~isempty(regexp(userList{iuser},'^user', 'once' ))
%     FL.AS=rmfield(FL.AS,userList{iuser});
%   end % if user field
% end % for iuser
% set(handles.listbox1,'Value',1);
% set(handles.listbox1,'String','');


function allowDeny(opt,handles)
global FL
% ----------------------------------------
% Allow selected items from request list
% ----------------------------------------
% Get selected items AS.pvListReq
newReq=get(handles.listbox2,'String');
approvedReq=get(handles.listbox1,'String');

if isequal(opt,'allow') || isequal(opt,'deny')
  reqListSel=get(handles.listbox2,'Value');
else
  reqListSel=1:length(newReq);
end % if list or all
% Deal with request
if ~isempty(reqListSel)
  for iList=reqListSel
    % Get request data
    t=regexp(newReq{iList},'(.+)\s:\s(.+)\s:\s(.+):(.+)\s:\s(.+)','tokens');
    % Add to approved list and flag access allowed in FL struct
    if isfield(FL.AS,t{1}{3}) && isfield(FL.AS.(t{1}{3}),t{1}{4})
      if isequal(opt,'allow') || isequal(opt,'allow_all')
        approvedReq{end+1}=newReq{iList};
        FL.AS.(t{1}{3}).(t{1}{4}).access=true;
      else
        FL.AS.(t{1}{3}).(t{1}{4}).access=false;
      end % if allowing
      newReq={newReq{~ismember(reqListSel,iList)}}';
    else
      error(['No FL.AS entry for: ',t{1}{3},':',t{1}{4}]);
    end % if field names added to FL.AS
  end % for iList
end % if ~empty
% Update Req and pv name lists
set(handles.listbox2,'Value',1);
set(handles.listbox2,'String',newReq);
if isequal(opt,'allow') || isequal(opt,'allow_all')
  set(handles.listbox1,'Value',1);
  set(handles.listbox1,'String',approvedReq);
end % if allowing


% --- Executes on button press in pushbutton7.
function pushbutton7_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton7 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global FL
% --- Request Run access server
if ~isequal(get(FL.AS.t_main,'Running'),'on')
  start(FL.AS.t_main);
end % start as timer if not running


% --- Executes on selection change in listbox3.
function listbox3_Callback(hObject, eventdata, handles)
% hObject    handle to listbox3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = get(hObject,'String') returns listbox3 contents as cell array
%        contents{get(hObject,'Value')} returns selected item from listbox3


% --- Executes during object creation, after setting all properties.
function listbox3_CreateFcn(hObject, eventdata, handles)
% hObject    handle to listbox3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: listbox controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in pushbutton10.
function pushbutton10_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton10 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% get access request list for this entry and display
global FL BEAMLINE GIRDER PS

% get access request list for this entry and display
set(handles.listbox3,'Value',1)
val=get(handles.listbox2,'Value');
str=get(handles.listbox2,'String');
t=regexp(str{val},'(.+)\s:\s(.+)\s:\s(.+):(.+)\s:\s(.+)','tokens');
accStr={};
if isfield(FL.AS.(t{1}{3}).(t{1}{4}),'PS')
  for iPS=1:length(FL.AS.(t{1}{3}).(t{1}{4}).PS)
    nPS=FL.AS.(t{1}{3}).(t{1}{4}).PS(iPS);
    accStr{end+1}=['PS',num2str(nPS),': ',BEAMLINE{PS(nPS).Element(1)}.Name];
  end % for iPS
elseif isfield(FL.AS.(t{1}{3}).(t{1}{4}),'GIRDER')
  for iGIRDER=1:length(FL.AS.(t{1}{3}).(t{1}{4}).GIRDER)
    nGIRDER=FL.AS.(t{1}{3}).(t{1}{4}).GIRDER(iGIRDER);
    accStr{end+1}=['GIRDER',num2str(nGIRDER),': ',BEAMLINE{GIRDER{nGIRDER}.Element(1)}.Name];
  end % for iGIRDER
end % if PS | GIRDER
set(handles.listbox3,'String',accStr);

% --- Executes on button press in pushbutton8.
function pushbutton8_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton8 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global FL BEAMLINE GIRDER PS

% get access request list for this entry and display
set(handles.listbox3,'Value',1)
val=get(handles.listbox1,'Value');
str=get(handles.listbox1,'String');
t=regexp(str{val},'(.+)\s:\s(.+)\s:\s(.+):(.+)\s:\s(.+)','tokens');
accStr={};
if isfield(FL.AS.(t{1}{3}).(t{1}{4}),'PS')
  for iPS=1:length(FL.AS.(t{1}{3}).(t{1}{4}).PS)
    nPS=FL.AS.(t{1}{3}).(t{1}{4}).PS(iPS);
    accStr{end+1}=['PS',num2str(nPS),': ',BEAMLINE{PS(nPS).Element(1)}.Name];
  end % for iPS
end % if ps
if isfield(FL.AS.(t{1}{3}).(t{1}{4}),'GIRDER')
  for iGIRDER=1:length(FL.AS.(t{1}{3}).(t{1}{4}).GIRDER)
    nGIRDER=FL.AS.(t{1}{3}).(t{1}{4}).GIRDER(iGIRDER);
    accStr{end+1}=['GIRDER',num2str(nGIRDER),': ',BEAMLINE{GIRDER{nGIRDER}.Element(1)}.Name];
  end % for iGIRDER
end % if GIRDER
set(handles.listbox3,'String',accStr);


% --- Executes when user attempts to close figure1.
function figure1_CloseRequestFcn(hObject, eventdata, handles)
% hObject    handle to figure1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global FL

stop(FL.AS.t_main);

% Hint: delete(hObject) closes the figure
delete(hObject);

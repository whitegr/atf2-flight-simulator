function varargout = FlMiscGui(varargin)
% FLMISCGUI M-file for FlMiscGui.fig
%      FLMISCGUI, by itself, creates a new FLMISCGUI or raises the existing
%      singleton*.
%
%      H = FLMISCGUI returns the handle to a new FLMISCGUI or the handle to
%      the existing singleton*.
%
%      FLMISCGUI('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in FLMISCGUI.M with the given input arguments.
%
%      FLMISCGUI('Property','Value',...) creates a new FLMISCGUI or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before FlMiscGui_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to FlMiscGui_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help FlMiscGui

% Last Modified by GUIDE v2.5 24-Sep-2008 12:30:32

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @FlMiscGui_OpeningFcn, ...
                   'gui_OutputFcn',  @FlMiscGui_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before FlMiscGui is made visible.
function FlMiscGui_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to FlMiscGui (see VARARGIN)

% Choose default command line output for FlMiscGui
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes FlMiscGui wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = FlMiscGui_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on button press in checkbox1.
function checkbox1_Callback(hObject, eventdata, handles)
% hObject    handle to checkbox1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of checkbox1


% --- Executes on button press in pushbutton1.
function pushbutton1_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
mhan=msgbox('testing sound... loading audio please wait...','Audio Test');
maxind=23118848; nsamp=0;
try
  for isamp=1:2e6:maxind
    nsamp=nsamp+1;
    [y{nsamp}, Fs, nbits] = wavread('lucretia.wav',[isamp min(isamp+2e6,maxind)]);
  end % for isamp
catch
  disp(lasterr); %#ok<LERR>
  msgbox('testing failed','Audio Test','replace');
end % try/catch
if ishandle(mhan)
  msgbox('testing sound... playing, click OK to stop','Audio Test','replace');
else
  mhan=msgbox('testing sound... playing, click OK to stop','Audio Test');
end % if mhan there still
try
  for isamp=1:nsamp
    player = audioplayer(y{isamp}, Fs, nbits);
    play(player)
    while isplaying(player)
      if ~ishandle(mhan)
        stop(player);
        return
      end % if msgbox closed
      pause(1)
    end % while playings
  end % for isamp
catch
  disp(lasterr); %#ok<LERR>
  msgbox('testing failed','Audio Test','replace');
end % try/catch
if ishandle(mhan),delete(mhan),end



% --- Executes on button press in pushbutton2.
function pushbutton2_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
delete(handles.figure1);


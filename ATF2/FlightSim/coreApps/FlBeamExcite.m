function [stat varargout]=FlBeamExcite(cmd,varargin)
% stat = FlBeamExcite('run')
global PS FL BEAMLINE
persistent pars
stat{1}=1;

% Initialisation
if isempty(pars)
  if exist('userData/FlBeamExcite.mat','file')
    load userData/FlBeamExcite pars
    newpars=pars;
    pars=initPars();
    pars.nsteps=newpars.nsteps;
    pars.ffs_amplitude=newpars.ffs_amplitude; % radians of kick
    pars.istep=newpars.istep;
    pars=setRefV(pars);
  else
    pars=initPars();
  end
end
if ~isfield(pars,'refV')
  pars=setRefV(pars);
end

% Process command
switch lower(cmd)
  case 'run'
    pars.istep=pars.istep+1;
    if pars.istep>pars.nsteps; pars.istep=1; end;
    if isfield(FL.exciter,'FFS_switch') && strcmp(FL.exciter.FFS_switch,'on')
      ncor=numel(pars.ffs_cor);
      for icor=1:ncor
        if BEAMLINE{pars.ffs_cor(icor)}.Name(2)=='H'
          PS(BEAMLINE{pars.ffs_cor(icor)}.PS).SetPt=pars.refV(icor)+pars.ffs_steps(icor,pars.istep)*pars.ffs_amplitude*3;
        else
          PS(BEAMLINE{pars.ffs_cor(icor)}.PS).SetPt=pars.refV(icor)+pars.ffs_steps(icor,pars.istep)*pars.ffs_amplitude;
        end
        stat=PSTrim(BEAMLINE{pars.ffs_cor(icor)}.PS,~FL.SimMode);
        if stat{1}~=1; return; end;
      end
    end
  case 'resetcor'
    for icor=1:numel(pars.ffs_cor)
      PS(BEAMLINE{pars.ffs_cor(icor)}.PS).SetPt=pars.refV(icor);
      stat=PSTrim(BEAMLINE{pars.ffs_cor(icor)}.PS,~FL.SimMode);
      if stat{1}~=1; return; end;
    end
  case 'setrefv'
    pars=setRefV(pars);
  case 'init'
    pars=initPars();
  case 'getpars'
    varargout{1}=pars;
  case 'setnsteps'
    pars.nsteps=varargin{1};
    save userData/FlBeamExcite pars
  case 'setffsamp'
    pars.ffs_amplitude=varargin{1};
    save userData/FlBeamExcite pars
  otherwise
    error('Unknown Command');
end

%%
function pars=initPars
global BEAMLINE

pars.ffs_cor=[findcells(BEAMLINE,'Name','ZH10X') findcells(BEAMLINE,'Name','ZH1FF') ...
  findcells(BEAMLINE,'Name','ZV11X') findcells(BEAMLINE,'Name','ZV1FF')];
pars.nsteps=90;
pars.ffs_amplitude=10e-6; % radians of kick
pars.istep=1;
ncor=numel(pars.ffs_cor);
for icor=1:ncor
  pars.ffs_steps(icor,:)=sin(linspace((icor-1)*((2*pi)/ncor),2*pi+(icor-1)*((2*pi)/ncor),pars.nsteps));
end

function pars=setRefV(pars)
global BEAMLINE PS

ncor=numel(pars.ffs_cor);
for icor=1:ncor
  pars.refV(icor)=PS(BEAMLINE{pars.ffs_cor(icor)}.PS).Ampl;
end
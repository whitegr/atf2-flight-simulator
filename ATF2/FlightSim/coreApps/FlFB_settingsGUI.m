function varargout = FlFB_settingsGUI(varargin)
% FLFB_SETTINGSGUI M-file for FlFB_settingsGUI.fig
%      FLFB_SETTINGSGUI, by itself, creates a new FLFB_SETTINGSGUI or raises the existing
%      singleton*.
%
%      H = FLFB_SETTINGSGUI returns the handle to a new FLFB_SETTINGSGUI or the handle to
%      the existing singleton*.
%
%      FLFB_SETTINGSGUI('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in FLFB_SETTINGSGUI.M with the given input arguments.
%
%      FLFB_SETTINGSGUI('Property','Value',...) creates a new FLFB_SETTINGSGUI or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before FlFB_settingsGUI_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to FlFB_settingsGUI_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help FlFB_settingsGUI

% Last Modified by GUIDE v2.5 08-Nov-2010 16:55:33

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @FlFB_settingsGUI_OpeningFcn, ...
                   'gui_OutputFcn',  @FlFB_settingsGUI_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before FlFB_settingsGUI is made visible.
function FlFB_settingsGUI_OpeningFcn(hObject, eventdata, handles, varargin) %#ok<*INUSL>
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to FlFB_settingsGUI (see VARARGIN)

% Choose default command line output for FlFB_settingsGUI
handles.output = handles;

% Update handles structure
guidata(hObject, handles);

[~, pars]=FlFB('GetPars');
set(handles.edit1,'String',num2str(pars.PI(1)))
set(handles.edit2,'String',num2str(pars.PI(2)))
set(handles.edit5,'String',num2str(pars.PI_sext(1)))
set(handles.edit6,'String',num2str(pars.PI_sext(2)))
set(handles.radiobutton1,'Value',pars.applyFB(1))
set(handles.radiobutton2,'Value',pars.applyFB(2))
set(handles.radiobutton3,'Value',pars.applyFB(3))

for iname=1:length(pars.bpmname)
  wstr{iname}=sprintf('%s : %.2f (x) %.2f (y)',pars.bpmname{iname},pars.weights(iname),pars.weights(length(pars.bpmname)+iname));
end
set(handles.popupmenu1,'String',wstr)
set(handles.popupmenu1,'Value',1)
popupmenu1_Callback(handles.popupmenu1,[],handles);

% UIWAIT makes FlFB_settingsGUI wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = FlFB_settingsGUI_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on button press in pushbutton1.
function pushbutton1_Callback(hObject, eventdata, handles) %#ok<*INUSD,*DEFNU>
% hObject    handle to pushbutton1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
guiCloseFn('FlFB_settingsGUI',handles);

% --- Executes on button press in togglebutton1.
function togglebutton1_Callback(hObject, eventdata, handles)
% hObject    handle to togglebutton1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

if get(hObject,'Value')
   newpars.diagnostics=true;
else
   newpars.diagnostics=false;
end
FlFB('SetPars',newpars);



function edit1_Callback(hObject, eventdata, handles)
% hObject    handle to edit1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

val=str2double(get(hObject,'String'));
if isnan(val) || val<0 || val>2
  set(hObject,'String','---');
else
  [stat pars]=FlFB('GetPars');
  newpars.PI=pars.PI;
  newpars.PI(1)=val;
  FlFB('SetPars',newpars);
end


% --- Executes during object creation, after setting all properties.
function edit1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit2_Callback(hObject, eventdata, handles)
% hObject    handle to edit2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

val=str2double(get(hObject,'String'));
if isnan(val) || val<0 || val>2
  set(hObject,'String','---');
else
  [stat pars]=FlFB('GetPars');
  newpars.PI=pars.PI;
  newpars.PI(2)=val;
  FlFB('SetPars',newpars);
end


% --- Executes during object creation, after setting all properties.
function edit2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes when user attempts to close figure1.
function figure1_CloseRequestFcn(hObject, eventdata, handles)
% hObject    handle to figure1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

guiCloseFn('FlFB_settingsGUI',handles);


% --- Executes on button press in togglebutton2.
function togglebutton2_Callback(hObject, eventdata, handles)
% hObject    handle to togglebutton2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
if get(hObject,'Value')
   newpars.diagnostics_ipfit=true;
else
   newpars.diagnostics_ipfit=false;
end
FlFB('SetPars',newpars);


% --- Use orbit FB
function radiobutton1_Callback(hObject, eventdata, handles)
% hObject    handle to radiobutton1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

[~, pars]=FlFB('GetPars');
newpars.applyFB=pars.applyFB;
if get(hObject,'Value')
  newpars.applyFB(1)=true;
else
  newpars.applyFB(1)=false;
end
FlFB('SetPars',newpars);


% --- Use FFS Sext FB
function radiobutton2_Callback(hObject, eventdata, handles)
% hObject    handle to radiobutton2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

[~, pars]=FlFB('GetPars');
newpars.applyFB=pars.applyFB;
if get(hObject,'Value')
  newpars.applyFB(2)=true;
else
  newpars.applyFB(2)=false;
end
FlFB('SetPars',newpars);


% --- Use IP FB
function radiobutton3_Callback(hObject, eventdata, handles)
% hObject    handle to radiobutton3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

[~, pars]=FlFB('GetPars');
newpars.applyFB=pars.applyFB;
if get(hObject,'Value')
  newpars.applyFB(3)=true;
else
  newpars.applyFB(3)=false;
end
FlFB('SetPars',newpars);


% --- Executes on button press in pushbutton2.
function pushbutton2_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- BPM weights list
function popupmenu1_Callback(hObject, eventdata, handles)
% hObject    handle to popupmenu1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

[stat pars]=FlFB('GetPars');
ibpm=get(hObject,'Value');
set(handles.edit3,'String',num2str(pars.weights(ibpm)))
set(handles.edit4,'String',num2str(pars.weights(length(pars.bpminstr)+ibpm)))



% --- Executes during object creation, after setting all properties.
function popupmenu1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to popupmenu1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit3_Callback(hObject, eventdata, handles)
% hObject    handle to edit3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

weight=str2double(get(hObject,'String'));
if isnan(weight) || weight<0
  errordlg('Weight must be real number > 0');
  set(hObject,'String','1')
else
  [stat pars]=FlFB('GetPars'); newpars.weights=pars.weights;
  ibpm=get(handles.popupmenu1,'Value');
  newpars.weights(ibpm)=weight;
  FlFB('SetPars',newpars);
end


% --- Executes during object creation, after setting all properties.
function edit3_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit4_Callback(hObject, eventdata, handles)
% hObject    handle to edit4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

weight=str2double(get(hObject,'String'));
if isnan(weight) || weight<0
  errordlg('Weight must be real number > 0');
  set(hObject,'String','1')
else
  [stat pars]=FlFB('GetPars'); newpars.weights=pars.weights;
  ibpm=get(handles.popupmenu1,'Value');
  newpars.weights(length(pars.bpminstr)+ibpm)=weight;
  FlFB('SetPars',newpars);
end


% --- Executes during object creation, after setting all properties.
function edit4_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit6_Callback(hObject, eventdata, handles)
% hObject    handle to edit6 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

I=str2double(get(hObject,'String'));
if isnan(I)
  errordlg('I Coefficient must be a real number!')
  set(hObject,'String','0.1')
else
  [stat pars]=FlFB('GetPars'); newpars.PI_sext=pars.PI_sext;
  newpars.PI_sext(2)=I;
  FlFB('SetPars',newpars);
end


% --- Executes during object creation, after setting all properties.
function edit6_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit6 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit5_Callback(hObject, eventdata, handles)
% hObject    handle to edit5 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

P=str2double(get(hObject,'String'));
if isnan(P)
  errordlg('P Coefficient must be a real number!')
  set(hObject,'String','0.3')
else
  [stat pars]=FlFB('GetPars'); newpars.PI_sext=pars.PI_sext;
  newpars.PI_sext(1)=P;
  FlFB('SetPars',newpars);
end


% --- Executes during object creation, after setting all properties.
function edit5_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit5 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- lauch Spectral Analysis GUI
function pushbutton3_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global FL
FL.Gui.FlFB_spectralAnalGUI=FlFB_spectralAnalGUI;

% --- Executes on slider movement.
function slider2_Callback(hObject, eventdata, handles)
% hObject    handle to slider2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider


% --- Executes during object creation, after setting all properties.
function slider2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to slider2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end



function edit8_Callback(hObject, eventdata, handles)
% hObject    handle to edit8 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit8 as text
%        str2double(get(hObject,'String')) returns contents of edit8 as a double


% --- Executes during object creation, after setting all properties.
function edit8_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit8 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

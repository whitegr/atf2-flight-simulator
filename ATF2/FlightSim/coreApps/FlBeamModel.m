function [stat varargout] = FlBeamModel(cmd,varargin)
% Routines for orbit fitting and conditioning of Lucretia initial beam
% structure based on accelerator measurements
%
% (stat = Lucretia status return format)
%
% [stat data Initial MagOffsets] = FlBeamModel('getdata')
%  Get internal data structure:
%    data.lookbackTime
%      Max required age of accelerator measurements for use in fits in
%      hours
%    data.useBpms
%      Logical vector = # of MONI elements in BEAMLINE (in BEAMLINE order)
%      True = use data from this BPM in fits
%    data.useDispData
%      Same format as data.useBpms but used to select desried dispersion
%      data to use in fitter
%    data.nbpmave
%      Scalar integer, # of pulses to average for getting measured BPM
%      orbit to use in fits
%    data.useMagOffsets
%      Logical vector = # of mags in BEAMLINE (in BEAMLINE order)
%      True = allow fitter to vary the offset of this mag
%    data.useEmitData
%      Scalar integer, controls what emittance to use in Initial structure
%      when issuing 'SetInitial' command
%      (not applied if requested measurement is older than
%      data.lookbackTime, this is indicated by a return stat{1}=0)
%      0: Use default Model emittances
%      1: Use measured projected emittances from EXT OTR system
%      2: Use measured intrinsic emittances from EXT OTR system
%    data.dispSelect
%      1 = use model dispersion values
%      2 = use values measured / fitted from extDispersion application
%    Initial
%      Lucretia Initial beam structure
%    MagOffsets
%      List of horizontal and vertical fitted offsets
%      Length = sum(data.useMagOffsets)*2 [x_offsets y_offsets]
%
% [stat l] = FlBeamModel('GetLocal')
%   Get internal (local) data structure, those of likely interest listed
%   below:
%     l.clName: list of magnet names
%     l.cl: "Cluster" list of magnets, including BEAMLINE indexing etc
%     l.allBpms: INSTR index of BPMs
%     l.allBpmsInd: BEAMLINE index of BPMs
%     l.allBpmNames: BEAMLINE Names of BPMs
%
% stat = FlBeamModel('SetNBPMAve',N)
%  Set number of BPM readings to average. If use N=1, then additional
%  constraint that all BPM readings come from the same pulse ID is imposed
%
% stat = FlBeamModel('SetLookbackTime',time)
%  Set max age of dispersion and emittance and bpmorbit measurements to use
%    time in hours
%
% stat = FlBeamModel('UseBpms',useBpms)
%   Set data.useBpms 
%
% stat = FlBeamModel('UseMagOffsets',useMagOffsets)
%   Set data.useMagOffsets
%
% stat = FlBeamModel('UseEmitData',useEmitData)
%   Set data.useEmitData 
%
% stat = FlBeamModel('UseDispData',useDispData)
%   Set data.useDispData 
%
% stat = FlBeamModel('DoOrbitFit')
%   Fit initial orbit (x,x',y,y',E) to requested BPM readings (averaged
%   over requested past number of pulses)
%   Orbit is stored in Initial structure
%
% stat = FlBeamModel('DoMagOffsetFit')
%   Fit requested list of Mag offsets (x and y)
%
% stat = FlBeamModel('SetInitial')
%   Set default Floodland Initial structure used for main tracking routine
%   based on FlBeamModel internal Initial structure which contains the
%   results of all fits performed
%
% stat = FlBeamModel('ResetInitial')
%   Reset Initial structure back to Model version
%
% [stat magNames] = FlBeamModel('GetMagNames')
%   Get list of Mag names used for offset fitting
%     magNames (cell array)
%       length(magNames) = length(data.useMagOffsets)
%
% stat = FlBeamModel('SelectDR',sel)
%   Enable/Disable all selections in the DR region
%     sel = logical scalar
%
% stat = FlBeamModel('SelectEXT',sel)
%   Enable/Disable all selections in the EXT region
%     sel = logical scalar
%
% stat = FlBeamModel('SelectFFS',sel)
%   Enable/Disable all selections in the FFS region
%     sel = logical scalar
%
% [stat xtrack ytrack strack] = FlBeamModel('EleTrack',<trackInd>)
%   Track beam through all beam elements between first and last selected
%   BPMs in data.bpmUse
%     (If trackInd defined, then just track to this array of BEAMLINE indices)
%     xtrack/ytrack/strack = x/y/s positions at each BEAMLINE element
%     selected [min(data.bpmUse):max(data.bpmUse)]
%
% stat = FlBeamModel('SetMaxIter',N)
%   Set maximum iteration steps for optimisers (N steps)
%
% ------------------------------------
% G. White Jan 26, 2011
persistent data l Initial
global FL

% Initialisation
stat{1}=1;
varargout={[] [] [] []};
if isempty(l) && ~strcmpi(cmd,'clear')
  l=init_l();
end
if isempty(Initial)
  % Initial structure
  if isfield(FL,'SimModel') && isfield(FL.SimModel,'Initial')
    Initial=FL.SimModel.Initial;
  else
    error('Need FL.SimModel.Initial')
  end
end
if isempty(data) && ~strcmpi(cmd,'clear')
  % Load last status data?
  if FlFnCheck('FlBeamModel')
    load userData/FlBeamModel data
  else
    data=init_data(l);
  end
end

% Process command
switch lower(cmd)
  case 'clear'
    data=[]; l=[]; Initial=[];
    return
  case 'getdata'
    varargout{1}=data;
    varargout{2}=Initial;
    varargout{3}=MagOffsets('get',l,data);
  case 'setlookbacktime'
    if nargin~=2 || varargin{1}<0
      stat{1}=-1; stat{2}='Invalid lookback time setting value'; return;
    end
    data.lookbackTime=varargin{1};
  case 'resetinitial'
    Initial=[];
  case 'setmaxiter'
    l.optimParam=optimset(l.optimParam,'MaxIter',varargin{1});
  case 'dispselect'
    data.dispSelect=varargin{1};
  case 'setnbpmave'
    data.nbpmave=varargin{1};
  case 'getlocal'
    varargout{1}=l;
  case 'usebpms'
    data.useBpms=varargin{1};
  case 'usemagoffsets'
    data.useMagOffsets=varargin{1};
  case 'useemitdata'
    data.useEmitData=varargin{1};
  case 'usedispdata';
    data.useDispData=varargin{1};
  case 'doorbitfit'
    initRate=get(FL.Gui.main.popupmenu1,'Value');
    set(FL.Gui.main.popupmenu1,'Value',1);
    FlGui_trusted('popupmenu1_Callback',FL.Gui.main.popupmenu1,[],FL.Gui.main);
    [stat data Initial]=orbitFit(l,data,Initial);
    set(FL.Gui.main.popupmenu1,'Value',initRate);
    FlGui_trusted('popupmenu1_Callback',FL.Gui.main.popupmenu1,[],FL.Gui.main);
  case 'domagoffsetfit'
    initRate=get(FL.Gui.main.popupmenu1,'Value');
    set(FL.Gui.main.popupmenu1,'Value',1);
    FlGui_trusted('popupmenu1_Callback',FL.Gui.main.popupmenu1,[],FL.Gui.main);
    [stat data Initial]=magOffsetFit(l,data,Initial);
    set(FL.Gui.main.popupmenu1,'Value',initRate);
    FlGui_trusted('popupmenu1_Callback',FL.Gui.main.popupmenu1,[],FL.Gui.main);
  case 'selectdr'
    data.useBpms(l.drBpms)=logical(varargin{1});
    data.useDispData(l.drBpms)=logical(varargin{1});
    data.useMagOffsets(l.drcl)=logical(varargin{1});
  case 'selectext'
    data.useBpms(l.extBpms)=logical(varargin{1});
    data.useDispData(l.extBpms)=logical(varargin{1});
    data.useMagOffsets(l.extcl)=logical(varargin{1});
  case 'selectffs'
    data.useBpms(l.ffsBpms)=logical(varargin{1});
    data.useDispData(l.ffsBpms)=logical(varargin{1});
    data.useMagOffsets(l.ffscl)=logical(varargin{1});
  case 'getmagnames'
    varargout{1}=l.clName;
  case 'setinitial'
    if data.useEmitData==1
      [emitx ts]=FlCA('lcaGet','mOTR:procData:projemitx');
      if ~isnan(ts)
        elapsedTime=now*24-epicsts2mat(ts)*24;
      else
        errordlg('OTR PV missing','FlBeamModel Error'); return;
      end
      if elapsedTime<data.lookbackTime && ~isnan(emitx) && emitx~=0
        Initial.x.NEmit=emitx*Initial.Momentum/0.511e-3;
      else
        stat{1}=0; stat{2}='x Emittance data older than requested data.lookbackTime';
      end
      [emity ts]=FlCA('lcaGet','mOTR:procData:projemity');
      elapsedTime=now*24-epicsts2mat(ts)*24;
      if elapsedTime<data.lookbackTime && ~isnan(emity) && emity~=0
        Initial.y.NEmit=emity*Initial.Momentum/0.511e-3;
      else
        stat{1}=0; stat{2}='y Emittance data older than requested data.lookbackTime';
      end
    elseif data.useEmitData==2
      [emitx ts]=FlCA('lcaGet','mOTR:procData:intemitx');
      if ~isnan(ts)
        elapsedTime=now*24-epicsts2mat(ts)*24;
      else
        errordlg('OTR PV missing','FlBeamModel Error'); return;
      end
      if elapsedTime<data.lookbackTime && ~isnan(emitx) && emitx~=0
        Initial.x.NEmit=emitx*Initial.Momentum/0.511e-3;
      else
        stat{1}=0; stat{2}='x Emittance data older than requested data.lookbackTime';
      end
      [emity ts]=FlCA('lcaGet','mOTR:procData:intemity');
      elapsedTime=now*24-epicsts2mat(ts)*24;
      if elapsedTime<data.lookbackTime && ~isnan(emity) && emity~=0
        Initial.y.NEmit=emity*Initial.Momentum/0.511e-3;
      else
        stat{1}=0; stat{2}='y Emittance data older than requested data.lookbackTime';
      end
    end
    FL.SimModel.Initial=Initial;
  case 'eletrack'
    if nargin==2
      [stat xtrack ytrack strack]=eletrack(Initial,data,l,varargin{1});
    else
      [stat xtrack ytrack strack]=eletrack(Initial,data,l);
    end
    if stat{1}~=1; return; end;
    varargout{1}=xtrack; varargout{2}=ytrack; varargout{3}=strack;
  otherwise
    error('Unknown command')
end

save userData/FlBeamModel data

%% Initialisation routines
function l =init_l()
global INSTR BEAMLINE FL
iex=findcells(BEAMLINE,'Name','IEX');
ff=findcells(BEAMLINE,'Name','BEGFF');
l.allBpms=[]; l.extBpms=logical([]); l.ffsBpms=logical([]); l.drBpms=logical([]);
l.allBpmsInd=[]; l.allBpmNames={};
l.modelDisp.x=[]; l.modelDisp.y=[];
for i_instr=1:length(INSTR)
  if strcmp(INSTR{i_instr}.Class,'MONI')
    l.allBpms(end+1)=i_instr;
    l.allBpmNames{end+1}=BEAMLINE{INSTR{i_instr}.Index}.Name;
    l.allBpmsInd(end+1)=INSTR{i_instr}.Index;
    l.extBpms(end+1)=false; l.ffsBpms(end+1)=false; l.drBpms(end+1)=false;
    l.modelDisp.x(end+1)=FL.SimModel.Twiss.etax(INSTR{i_instr}.Index);
    l.modelDisp.y(end+1)=FL.SimModel.Twiss.etay(INSTR{i_instr}.Index);
    if INSTR{i_instr}.Index>iex && INSTR{i_instr}.Index<ff
      l.extBpms(end)=true;
    elseif INSTR{i_instr}.Index>=ff
      l.ffsBpms(end)=true;
    elseif INSTR{i_instr}.Index<iex
      l.drBpms(end)=true;
    end
  end
end
% Get TrackThru indices
bl=findcells(BEAMLINE,'Class','MONI');
l.allBpmsInstInd=arrayfun(@(x) find(ismember(bl,x)),l.allBpmsInd);

% Mag indices
l.cl=[FL.SimModel.magGroup.RQuad.Off.ClusterList FL.SimModel.magGroup.Quad.Off.ClusterList FL.SimModel.magGroup.Sext.Off.ClusterList];
l.clName=[arrayfun(@(x) BEAMLINE{FL.SimModel.magGroup.RQuad.OffUse.ClusterList(x).index(1)}.Name,1:length(FL.SimModel.magGroup.RQuad.OffUse.ClusterList),'UniformOutput',false) ...
  arrayfun(@(x) BEAMLINE{FL.SimModel.magGroup.Quad.OffUse.ClusterList(x).index(1)}.Name,1:length(FL.SimModel.magGroup.Quad.OffUse.ClusterList),'UniformOutput',false) ...
  arrayfun(@(x) BEAMLINE{FL.SimModel.magGroup.Sext.Off.ClusterList(x).index(1)}.Name,1:length(FL.SimModel.magGroup.Sext.Off.ClusterList),'UniformOutput',false)];
l.drcl=false(1,length(l.cl));
l.drcl(arrayfun(@(x) l.cl(x).index(1)<iex,1:length(l.cl)))=true;
l.extcl=false(1,length(l.cl));
l.extcl(arrayfun(@(x) l.cl(x).index(1)>iex & l.cl(x).index(1)<ff,1:length(l.cl)))=true;
l.ffscl=false(1,length(l.cl));
l.ffscl(arrayfun(@(x) l.cl(x).index(1)>ff,1:length(l.cl)))=true;
% Do we have optimisation toolbox?
tools=ver;
l.isoptim=~isempty(strfind([tools.Name],'Optimization Toolbox'));
% Turn off tracking in anything not a MONI
for ibl=1:length(BEAMLINE)
  if isfield(BEAMLINE{ibl},'TrackFlag') && ~strcmp(BEAMLINE{ibl}.Class,'MONI')
    BEAMLINE{ibl}.TrackFlag.GetInstData=0;
  end
end
% OPtimisation parameters
l.optimParam=optimset('MaxFunEvals',10000,'MaxIter',500,'TolFun',1e-6,'TolX',1e-6,'UseParallel','never','Display','iter','TypicalX',...
  [1e-6 1e-6 1e-6 1e-6 1e-3 1e-3 1e-3 1e-3 1e-3],'OutputFcn',@optimFun);

function data = init_data(l)
data.lookbackTime=12; % hours
data.useBpms=true(1,length(l.allBpms)); % all
data.useMagOffsets=true(1,length(l.cl)); % all
data.useEmitData=0; % emitdata=1: OTR projected emit, =2: OTR intrinsic emit
data.useDispData=true(1,length(l.allBpms)); % all
data.nbpmave=10;
data.donefit=false;

%% Fit Mag offsets
function [stat data Initial] = magOffsetFit(l,data,Initial)
stat{1}=1;
options=l.optimParam;
% Get list of mags in region fitting beam to bpms and initial offsets
offind=MagOffsets('getind',l,data);
offsets=MagOffsets('get',l,data);

% Fit orbit by varying x/y/roll
sz=size(offsets);
if sz(1)<2
  stat{1}=-1; stat{2}='need at least 2 magnets to fit';
  return
end
% Loop through magnets, perform orbit fit between first magnet and the
% looped magnet, lengthening size of fit until whole region done
bpmUseInit=data.useBpms;
bpmsInLastFit=[];
disp('Fitting Magnet Offsets...')
for ioff=2:length(offind)
  % any new BPMs between these magnets?
  bpmsInThisFit=l.allBpmsInd>=offind(ioff-1) & l.allBpmsInd<=offind(ioff) & bpmUseInit;
  if ~any(bpmsInThisFit) || isequal(bpmsInThisFit,bpmsInLastFit)
    continue
  else
    data.useBpms=bpmsInThisFit;
    offFit=offsets(1:ioff,[1 3 6]);
%     if l.isoptim
%       lb=offFit(:)-1e-3;
%       ub=offFit(:)+1e-3;
%       xmin=fmincon(@(x) doMagOffsetFit(x,Initial,data,l),offFit(:),[],[],[],[],lb,ub,[],options);
%     else
      xmin=fminsearch(@(x) doMagOffsetFit(x,Initial,data,l),offFit(:),options);
%     end
    MagOffsets('set',l,data,reshape(xmin,ioff,3));
  end
  fprintf('%.1f %% complete\n',100*ioff/length(offind))
  bpmsInLastFit=bpmsInThisFit;
end
data.useBpms=bpmUseInit;


function chi2=doMagOffsetFit(x,Initial,data,l)
% Set mag offsets
MagOffsets('set',l,data,reshape(x,numel(x)/3,3));

% Perform orbit fit and get fval output to use for this fval output
% ie minimise difference orbit based on mag offsets
[~, data]=orbitFit(l,data,Initial);
chi2=data.orbitFitFval;

% function to set/get mag offsets
function varargout=MagOffsets(cmd,l,data,setvals)
persistent offind clind
global BEAMLINE

% pre-get indices
if isempty(offind) && ~isequal(cmd,'getind')
  useInd=l.allBpmsInd(data.useBpms);
  useInd=[min(useInd) max(useInd)];
  offind=[]; clind=[];
  for icl=1:length(l.cl)
    if l.cl(icl).index(1)>=useInd(1) && l.cl(icl).index(end)<=useInd(2)
      offind(end+1)=l.cl(icl).index(1);
      clind(end+1)=icl;
    end
  end
end

switch cmd
  case 'getind'
    offind=[]; clind=[];
    useInd=l.allBpmsInd(data.useBpms);
    useInd=[min(useInd) max(useInd)];
    for icl=1:length(l.cl)
      if data.useMagOffsets(icl) && l.cl(icl).index(1)>=useInd(1) && l.cl(icl).index(end)<=useInd(2)
        offind(end+1)=l.cl(icl).index(1);
        clind(end+1)=icl;
      end
    end
    varargout{1}=offind;
    varargout{2}=clind;
  case 'get'
    offsets=reshape(cell2mat(arrayfun(@(x) BEAMLINE{x}.Offset,offind,'UniformOutput',false)),length(offind),6);
    varargout{1}=offsets;
  case 'set'
    sz=size(setvals);
    for ioff=1:sz(1)
      BEAMLINE{offind(ioff)}.Offset([1 3 6])=setvals(ioff,:);
    end
end

%% BPM measured orbit fitting
function [stat data Initial]=orbitFit(l,data,Initial)
global BEAMLINE FL INSTR

% Get averaged BPM data or single pulse
if data.nbpmave>1
  [stat bpmdata]=FlHwUpdate('bpmave',data.nbpmave);
  if stat{1}~=1; stat{2}='Not Enough Data in BPM buffer'; return; end;
else % request single pulse with all BPMs having same pulse timestamp
  for ipc=1:10
    [stat bpmdata]=FlHwUpdate('readbuffer',1,l.allBpms(data.useBpms));
    if ~isempty(data.bpmdata); break; end;
    FlHwUpdate('wait',1);
  end
  if isempty(data.bpmdata)
    stat{1}=-1; stat{2}='Unable to get single pulse with all requested BPMs from same pulse';
    return
  end
end

% Unpack data in instdata order
data.bpmdata.x=arrayfun(@(x) bpmdata{1}(1,l.allBpms(x)),find(data.useBpms));
nandata=isnan(data.bpmdata.x);
if any(nandata)
  nanbpms='No data for BPMS:';
  ubpm=find(data.useBpms);
  for inan=find(nandata)
    nanbpms=[nanbpms ' ' l.allBpmNames{ubpm(inan)}];
  end
  stat{1}=-1; stat{2}=nanbpms;
  return
end
data.bpmdata.dx=arrayfun(@(x) bpmdata{1}(4,l.allBpms(x)),find(data.useBpms));
data.bpmdata.y=arrayfun(@(x) bpmdata{1}(2,l.allBpms(x)),find(data.useBpms));
data.bpmdata.dy=arrayfun(@(x) bpmdata{1}(5,l.allBpms(x)),find(data.useBpms));
data.instind=l.allBpmsInstInd(data.useBpms);
data.bpmdata.s=arrayfun(@(x) BEAMLINE{x}.S,l.allBpmsInd(data.useBpms));
data.blind=l.allBpmsInd(data.useBpms);

% Form initial beam based on any prior fitting or that from the model
try
  beam=MakeBeam6DGauss(Initial,1,1,1);
  xinit=zeros(1,5);
  xinit(1)=Initial.x.pos;
  xinit(2)=Initial.x.ang;
  xinit(3)=Initial.y.pos;
  xinit(4)=Initial.y.ang;
  xinit(5)=Initial.Momentum;
  xinit(6)=Initial.x.Twiss.eta;
  xinit(7)=Initial.x.Twiss.etap;
  xinit(8)=Initial.y.Twiss.eta; 
  xinit(9)=Initial.y.Twiss.etap;
  % Get extra dispersion (difference from Model to that measured) if
  % measurements recent and available
  if isfield(data,'dispSelect') && data.dispSelect && isfield(FL,'lastDispMeas') && ~isempty(FL.lastDispMeas) && (etime(clock,FL.lastDispMeas)/3600) < data.lookbackTime
    data.dispdata.x=arrayfun(@(x) INSTR{x}.dispref(1),l.allBpms(data.useBpms));
    data.dispdata.y=arrayfun(@(x) INSTR{x}.dispref(3),l.allBpms(data.useBpms));
    data.dispdata.dx=arrayfun(@(x) INSTR{x}.dispreferr(1),l.allBpms(data.useBpms));
    data.dispdata.dy=arrayfun(@(x) INSTR{x}.dispreferr(3),l.allBpms(data.useBpms));
  else
    data.dispSelect=false;
  end
  % Do fit
  zerores('zero');
  options=l.optimParam;
  l.isoptim=false;
  if l.isoptim
    lb=xinit-[1e-2 1e-2 1e-2 1e-2 0.3 1 1 1 1];
    ub=xinit+[1e-2 1e-2 1e-2 1e-2 0.3 1 1 1 1];
    [xmin fval]=lsqnonlin(@(x) doOrbitFit(x,beam,data,l.isoptim),xinit,lb,ub,options);
  else
    [xmin fval]=fminsearch(@(x) doOrbitFit(x,beam,data,false),xinit,options);
  end
  zerores('restore');
catch ME
  FL.lastError=ME;
  zerores('restore');
  stat{1}=-1; stat{2}=['Fit Error: ' ME.message]; return;
end

% Store fit results
data.orbitFitFval=fval;
Initial.x.pos=xmin(1);
Initial.x.ang=xmin(2);
Initial.y.pos=xmin(3);
Initial.y.pos=xmin(4);
Initial.Momentum=xmin(5);
Initial.x.Twiss.eta=xmin(6);
Initial.x.Twiss.etap=xmin(7);
Initial.y.Twiss.eta=xmin(8);
Initial.y.Twiss.etap=xmin(9);

% register fitting done
data.donefit=true;

% Orbit fit minimiser function
function chi2=doOrbitFit(x,beam,data,isoptim)
global BEAMLINE FL
beam.Bunch.x([1:4 6])=x(1:5);
dE=(FL.SimModel.Initial.Momentum-x(5))/FL.SimModel.Initial.Momentum;
beam.Bunch.x(1:4)=beam.Bunch.x(1:4)+dE.*x(6:9)';
[stat , ~, instdata]=TrackThru(1,length(BEAMLINE),beam,1,1,0);
if stat{1}~=1 && length(instdata{1})<max(data.instind)
  if isoptim
    chi2=ones(size(data.bpmdata.x)).*(length([instdata{1}.x])-sum(isnan([instdata{1}.x]))).*10000;
  else
    chi2=(length([instdata{1}.x])-sum(isnan([instdata{1}.x]))).*10000;
  end
  return
end
dx=[instdata{1}(data.instind).x] - data.bpmdata.x;
dy=[instdata{1}(data.instind).y] - data.bpmdata.y;
if isoptim
  chi2 = [dx./data.bpmdata.dx dy./data.bpmdata.dy];
else
  chi2=sum((dx.^2 + dy.^2) ./ ...
    abs(2.*data.bpmdata.x.*data.bpmdata.dx.^2+2.*data.bpmdata.y.*data.bpmdata.dy.^2));
end
% If NaN then probably due to not getting beam through lattice, assign
% penalty function based on how far through it got
if isnan(chi2)
  if isoptim
    chi2=ones(size(chi2)).*(length([instdata{1}.x])-sum(isnan([instdata{1}.x]))).*10000;
  else
    chi2=(length([instdata{1}.x])-sum(isnan([instdata{1}.x]))).*10000;
  end
end
% Also fit dispersion function?
if data.dispSelect
  beam.Bunch.x(6)=beam.Bunch.x(6).*1.1;
  dE=(FL.SimModel.Initial.Momentum-beam.Bunch.x(6))/FL.SimModel.Initial.Momentum;
  beam.Bunch.x(1:4)=beam.Bunch.x(1:4)+dE.*x(6:9);
  [stat , ~, instdata2]=TrackThru(1,length(BEAMLINE),beam,1,1,0);
  if stat{1}~=1 && length(instdata{1})<max(data.instind)
    chi2=1e20;
    return
  end
  dispx=([instdata2{1}(data.instind).x] - [instdata{1}(data.instind).x]) ./ 0.1;
  dispy=([instdata2{1}(data.instind).y] - [instdata{1}(data.instind).y]) ./ 0.1;
  if isoptim
    chi2=[chi2 (dispx-data.dispdata.x)./data.dispdata.dx (dispy-data.dispdata.y)./data.dispdata.dy];
  else
    chi2=chi2+sum((dispx-data.dispdata.x).^2./ (2.*data.dispdata.dx.^2) + ...
      abs(dispy-data.dispdata.y).^2./ (2.*data.dispdata.dy.^2));
  end
end
% If NaN then probably due to not getting beam through lattice, assign
% penalty function based on how far through it got
if isnan(chi2)
  chi2=ones(size(chi2)).*(length([instdata2{1}.x])-sum(isnan([instdata2{1}.x]))).*10000;
end

% Function to track fitted beam through all beamline elements
function [stat xtrack ytrack strack]=eletrack(Initial,data,l,trackInd)
global BEAMLINE FL
% zero resolutions
zerores('zero');
% Initialise and check we have at least tried fitting before going on
xtrack=[]; ytrack=[]; strack=[];
stat{1}=1;
if ~data.donefit
  stat{1}=0; stat{2}='No fitting performed yet';
  return
end
eta=[Initial.x.Twiss.eta Initial.x.Twiss.etap Initial.y.Twiss.eta Initial.y.Twiss.etap];
beam=MakeBeam6DGauss(Initial,1,1,1);
% Apply initial orbit offset due to dispersion
dE=(FL.SimModel.Initial.Momentum-Initial.Momentum)/FL.SimModel.Initial.Momentum;
beam.Bunch.x(1:4)=beam.Bunch.x(1:4)+dE.*eta';
% Track beam with current Model Initial parameters
if exist('trackInd','var')
  xtrack=zeros(1,length(trackInd)); ytrack=xtrack; strack=xtrack;
  for ibl=1:length(trackInd)
    [stat bo]=TrackThru(1,trackInd(ibl),beam,1,1,0);
    xtrack(ibl)=bo.Bunch.x(1);
    ytrack(ibl)=bo.Bunch.x(3);
    strack(ibl)=BEAMLINE{trackInd}.S;
  end
else
  bl=min(l.allBpmsInd(data.useBpms)):max(l.allBpmsInd(data.useBpms));
  xtrack=zeros(1,length(bl));
  ytrack=xtrack; strack=ytrack;
  for ibl=1:length(bl)
    if ibl==1
      [stat bo]=TrackThru(1,bl(ibl),beam,1,1,0);
      if stat{1}~=1
        zerores('restore'); return;
      end
    else
      [stat bo]=TrackThru(bl(ibl),bl(ibl),bo,1,1,0);
      if stat{1}~=1
        zerores('restore'); return;
      end
    end
    xtrack(ibl)=bo.Bunch.x(1);
    ytrack(ibl)=bo.Bunch.x(3);
    if isfield(BEAMLINE{bl(ibl)},'L')
      strack(ibl)=BEAMLINE{bl(ibl)}.S+BEAMLINE{bl(ibl)}.L;
    else
      strack(ibl)=BEAMLINE{bl(ibl)}.S;
    end
  end
end
% restore resolutions
zerores('restore');

function zerores(cmd)
global BEAMLINE
persistent restorevals
switch cmd
  case 'zero'
    for ibl=1:length(BEAMLINE)
      if isfield(BEAMLINE{ibl},'Resolution')
        restorevals(ibl)=BEAMLINE{ibl}.Resolution;
        BEAMLINE{ibl}.Resolution=0;
      end
    end
  case 'restore'
    if ~isempty(restorevals)
      for ibl=1:length(BEAMLINE)
        if isfield(BEAMLINE{ibl},'Resolution') && length(restorevals)>=ibl
          BEAMLINE{ibl}.Resolution=restorevals(ibl);
        end
      end
    end
end
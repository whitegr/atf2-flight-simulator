function [ipdata varargout] = getIPData(varargin)
% ipdata = getIPData
% Fit IP position and angles using BPMs in drift space around IP
% Store data in PV's
%  ipdata.x .xp .y .yp
%
% ipdata = getIPData('getlast')
%  get results of last IP data fit 
%
% datenum = getIPData('getcaldate')
% Get date stamp of last cal
%
% offsets = getIPData('getoffsets')
% Get vector of BPM offsets
%  offsets = [ MQD0FF(x) MQD0FF(y) MPREIP(x) MPREIP(y) IPBPMA(x) ...
%  IPBPMA(y) IPBPMB(x) IPBPMB(y) MPIP(x) MPIP(y) ]
%
% dL = getIPData('getdl')
% Get vector of longitudinal offsets
%  dL = [ dL(MQD0FF) dL(MPREIP) dL(IPBPMA) dL(IPBPMB) dL(MPIP) dL(IP) ]
%
% bpmsInUse = getIPData('getbpmuse')
% Logical vector of BPMs in use for IP fit
%  bpmsInUse = [ MQD0FF MPREIP IPBPMA IPBPMB MPIP ]
%
% bufSize = getIPData('getbufsize')
% Get length of internal buffer of BPM data
%
% [data bufind] = getIPData('getdatastore')
% Get internal circular buffer of BPM data
%  data(bufSize,15) = [MQD0FF(x) MQD0FF(y) MQD0FF(TMIT) ... ]
%  bufind = index of last saved data
%
% getIPData('bpmsel',bpmsToUse)
% Set which BPMs to use in IP fit (Logical vector)
%  bpmsToUse = [MQD0FF MPREIP IPBPMA IPBPMB MPIP]
%
% getIPData('resetbuf')
% Reset internal BPM buffer
%
% getIPData('setbufsize',bufSize)
% Change the depth of the internal BPM buffer
%
% [offsets dL] = getIPData('cal',npulse)
% Perform calibration of fitting system- fit transverse and longitudinal
%   npulse = use last given number of pulses in time to perform calibration
% offsets of BPMs
%  Offsets and dL vectors returned as described above
%
% getIPData('resetcal')
% Reset calibration dL's and transverse offsets to zero
global BEAMLINE INSTR FL
persistent bind bpmind bpmdataStore dL bpmuse bufsize bufind caldate offsets lastipdata donewarn

if isempty(donewarn)
  warning('off','MATLAB:polyval:ZeroDOF')
  donewarn=true;
end

% commands to return persistents
if nargin>0 && isequal(varargin{1},'getlast')
  ipdata=lastipdata;
  return
end
if nargin>0 && isequal(varargin{1},'getcaldate')
  ipdata=caldate;
  return
end
if nargin>0 && isequal(varargin{1},'getoffsets')
  ipdata=offsets;
  return
end
if nargin>0 && isequal(varargin{1},'getdl')
  ipdata=dL;
  return
end
if nargin>0 && isequal(varargin{1},'getbpmuse')
  ipdata=bpmuse;
  return
end
if nargin>0 && isequal(varargin{1},'getbufsize')
  ipdata=bufsize;
  return
end
if nargin>0 && isequal(varargin{1},'getdatastore')
  ipdata=bpmdataStore;
  varargout{1}=bufind;
  return
end

% BPM selection
if nargin==2 && isequal(varargin{1},'bpmsel')
  bpmuse=varargin{2};
  save userData/getIPData.mat offsets dL caldate bpmuse bufsize
  return
end
% Reset data buffer
if nargin>0 && isequal(varargin{1},'resetbuf')
  bpmdataStore=zeros(size(bpmdataStore));
  return
end
% Set buffer size
if nargin==2 && isequal(varargin{1},'setbufsize')
  if varargin{2}<bufsize
    bpmdataStore=bpmdataStore(1:varargin{2},:);
    if bufind>varargin{2}
      bufind=1;
    end
  elseif varargin{2}>bufsize
    oldStore=bpmdataStore;
    bpmdataStore=zeros(varargin{2},15);
    bpmdataStore(1:bufsize,:)=oldStore;
  end
  bufsize=varargin{2};
  save userData/getIPData.mat offsets dL caldate bpmuse bufsize
  return
end

% Define indicies and response matrices
if isempty(bind)
  bind(1)=findcells(BEAMLINE,'Name','MQD0FF');
  bind(5)=findcells(BEAMLINE,'Name','MPIP');
  bind(2)=findcells(BEAMLINE,'Name','MPREIP');
  bind(3)=findcells(BEAMLINE,'Name','IPBPMA');
  bind(4)=findcells(BEAMLINE,'Name','IPBPMB');
  bind(6)=findcells(BEAMLINE,'Name','IP');
  for ibpm=1:length(bind)
    bpmind(ibpm)=findcells(INSTR,'Index',bind(ibpm));
  end
  if exist('userData/getIPData.mat','file')
    load userData/getIPData.mat dL offsets caldate bpmuse bufsize
  else
    offsets=zeros(1,15);
    dL=zeros(1,6);
    bpmuse=[false true(1,4)];
    bufsize=200;
  end
  bpmdataStore=zeros(bufsize,15);
  bufind=1;
end

% Cut criteria
cutict=0.15;

% Fit offsets and S vals (calibrate function)
if nargin>0 && isequal(varargin{1},'cal')
  % Minimise chi2 of linear fit to IP BPMs by optimising BPM S locations
  % and offsets
%   opt=fminsearch(@(x) bpmopt(x,bpmdataStore,bpmuse,dL,bind,offsets),[zeros(1,sum(bpmuse)*2) zeros(1,sum(bpmuse))],...
%     optimset('MaxFunEvals',10000,'TolFun',1e-4,'TolX',1e-4,'Display','iter'));
%   dL(1:5)=dL(1:5)+opt(sum(bpmuse)*2+1:end);
%   offsets(1:3:sum(bpmuse)*3)=offsets(1:3:sum(bpmuse)*3)+opt(1:2:sum(bpmuse)*2);
%   offsets(2:3:sum(bpmuse)*3)=offsets(2:3:sum(bpmuse)*3)+opt(2:2:sum(bpmuse)*2);
  % Minimise jitter at ipwaist by optimisation of IP S location
  lmin=fminbnd(@(x) ipwaistopt( x,bpmdataStore,bpmuse,dL,bind,offsets,varargin{2}),-1,1,...
    optimset('MaxFunEvals',10000,'TolFun',1e-7,'TolX',1e-5,'display','iter'));
  dL(6)=lmin;
  ipdata=[offsets dL]; lastipdata=ipdata;
  caldate=now;
  save userData/getIPData.mat offsets dL caldate bpmuse bufsize
  return
elseif nargin>0 && isequal(varargin{1},'resetcal')
  dL=zeros(1,6);
  offsets=zeros(1,15);
  caldate=[];
  save userData/getIPData.mat offsets dL caldate bpmuse bufsize
  return
end

% Get BPM data or use given
bpmdata=zeros(1,15);
if nargin==0
  for ibpm=1:5
    bpmdata(ibpm*3-2:ibpm*3)=[INSTR{bpmind(ibpm)}.Data(1:2)-...
      INSTR{bpmind(ibpm)}.ref(1:2) INSTR{bpmind(ibpm)}.Data(3)];
  end
  % Store data if meets cuts
  ictsum=0;
  for ibpm=find(bpmuse)
    ictsum=ictsum+bpmdata(ibpm*3);
  end
  
  if ictsum/length(bpmuse) > cutict
    bpmdataStore(bufind,:)=bpmdata;
    bufind=bufind+1;
    if bufind>bufsize
      bufind=1;
    end
  end
elseif isnumeric(varargin{1})
  bpmdata=varargin{1};
end
ipdata = ipfit( bpmdata+offsets,bpmuse,dL,bind );
if ~FL.SimMode && nargin==0 % Put data into PVs
  putstr={'VIRTBPM:IP:X'; 'VIRTBPM:IP:XP'; 'VIRTBPM:IP:Y'; 'VIRTBPM:IP:YP'; 'VIRTBPM:IP:TMIT'};
  putvals=[ipdata.x*1e6; ipdata.xp*1e6; ipdata.y*1e6; ipdata.yp*1e6; ictsum/length(bpmuse)];
  if ~any(isnan(putvals))
    FlCA('lcaPut',putstr,putvals);
  end
end
lastipdata=ipdata;

% Update IP display window if open
try
  IPfitDataDisplay;
catch ME
  fprintf('IP Fit Data Display Error: %s\n',ME.message)
end

function chi2=ipwaistopt(x,bpmdataStore,bpmuse,dL,bind,offsets,npulse)
dL(6)=x;
sz=size(bpmdataStore);
X=zeros(1,sz(1)); Y=X; dX=X; dY=X;
for idata=1:npulse
  if bpmdataStore(idata,1)
    ipdata = ipfit( bpmdataStore(sz(1)-npulse+idata,:)+offsets,bpmuse,dL,bind );
    X(idata)=ipdata.x;
    dX(idata)=ipdata.dx; 
    Y(idata)=ipdata.y;
    dY(idata)=ipdata.dy;
  else
    break
  end
end
chi2=std(Y);

function chi2=bpmopt(x,bpmdataStore,bpmuse,dL,bind,offsets) %#ok<DEFNU>
dL(1:length(bpmuse))=dL(1:length(bpmuse))+x(length(bpmuse)*2+1:end);
sz=size(bpmdataStore);
X=zeros(1,sz(1)); Y=X; dX=X; dY=X;
for idata=1:sz(1)
  if bpmdataStore(idata,1)
    bpmdataStore(idata,:)=bpmdataStore(idata,:)+offsets;
    nbpm=0;
    for ibpm=find(bpmuse)
      nbpm=nbpm+1;
      bpmdataStore(idata,ibpm*3-2)=bpmdataStore(idata,ibpm.*3-2)+x(nbpm*2-1);
      bpmdataStore(idata,ibpm*3-1)=bpmdataStore(idata,ibpm.*3-1)+x(nbpm*2);
    end
    ipdata = ipfit( bpmdataStore(idata,:),bpmuse,dL,bind );
    X(idata)=ipdata.x;
    dX(idata)=ipdata.dx;
    Y(idata)=ipdata.y;
    dY(idata)=ipdata.dy;
  else
    break
  end
end
chi2=std(Y);

function ipdata = ipfit( bpmdata,bpmuse,dL,bind)
global BEAMLINE
nbpm=0;
for ibpm=find(bpmuse)
  nbpm=nbpm+1;
  S(nbpm)=BEAMLINE{bind(ibpm)}.S+dL(ibpm);
  X(nbpm)=bpmdata(ibpm*3-2);
  Y(nbpm)=bpmdata(ibpm*3-1);
end
[px,sx,mux]=polyfit(S,X,1);
cov=(inv(sx.R)*inv(sx.R)')*sx.normr^2/sx.df; 
[ipdata.x ipdata.dx]=polyval(px,BEAMLINE{bind(6)}.S+dL(6),sx,mux);
ipdata.xp=px(1); ipdata.dxp=sqrt(cov(1,1));
[py,sy,muy]=polyfit(S,Y,1);
cov=(inv(sy.R)*inv(sy.R)')*sy.normr^2/sy.df; 
[ipdata.y ipdata.dy]=polyval(py,BEAMLINE{bind(6)}.S+dL(6),sy,muy);
ipdata.yp=py(1); ipdata.dyp=sqrt(cov(1,1));

function [stat resp] = AccessRequest(request,reqID)
% [stat resp] = AccessRequest(request,reqID)
% ----------------------------------------
% Request access to hardware attached to Lucretia elements (request sent to
% FS server program, response from operator required before function
% returns)
% request{1}(...) = read+write privs requested for mover on GIRDER{...}
% request{2}(...) = read+write privs requested for power supply PS(...)
% request{3}(...) = read privs requested for instrument INSTR{...}
% resp = ID of request (reqID)
% ----------------------------------------
% Request release of access request(s):
% request = 'release'
% reqID = ID of previous access request
% ----------------------------------------
% Request status of access request:
% request = 'status'
% reqID = ID of previous access request
% resp = access status of request = true or false
global FL

% Initialise stuff
stat{1}=1;
types={'GIRDER' 'PS' 'INSTR'};
resp=[];

% If trusted, just return 1
if strcmp(FL.mode,'trusted')
  resp=1;
  return
end

% Check input parameters
if (~iscell(request) || length(request)~=length(types)) && ~isequal(lower(request),'release')
  if ~(isequal(request,'status') && exist('reqID','var'))
    stat{1}=-1;
    stat{2}='Invalid request';
    return
  end % if request a reqID
else
  for itype=1:length(types)
    if iscell(request) && ~isnumeric(request{itype})
      if ~isempty(request{itype})
        error('invalid request');
      end % ~isempty
    end % check request{itye} is number
  end % for itype
end % arg check

% Release request
if ~iscell(request) && isequal(lower(request),'release')
  if ~exist('reqID','var')
    reqID='all';
  end % if no reqID
  stat=sockrw('writechar','cas',['remove: ',reqID]);
  if stat{1}~=1; return; end;
  stat=sockrw('readchar','cas',5);
  if stat{1}==1; resp='Release Success'; end;
  return
end % if required to release access privs

% Status request
if ~iscell(request) && isequal(lower(request),'status')
  stat=sockrw('writechar','cas',['access-request: ',reqID]);
  if stat{1}~=1; return; end;
  [stat ret]=sockrw('readchar','cas',10);
  if stat{1}~=1; return; end;
  resp=ret{1};
  if isnan(str2double(resp))
    resp=false;
  else
    resp=logical(str2double(resp));
  end
  return
end % if status request

% Access Request
reqCmd='access-request:';
if ~isempty(request{1})
  for ireq=1:length(request{1})
    reqCmd=[reqCmd ' GIRDER' num2str(request{1}(ireq))];
  end % for ireq
end % if not empty req{1}
if ~isempty(request{2})
  for ireq=1:length(request{2})
    reqCmd=[reqCmd ' PS' num2str(request{2}(ireq))];
  end % for ireq
end % if not empty req{1}
if ~isempty(request{3})
  for ireq=1:length(request{3})
    reqCmd=[reqCmd ' INSTR' num2str(request{3}(ireq))];
  end % for ireq
end % if not empty req{1}
stat=sockrw('writechar','cas',reqCmd);
if stat{1}~=1; return; end;
% wait for response
msghan=msgbox('Waiting for server response to access request');
[stat ret]=sockrw('readchar','cas',600); % wait up to 10 mins
if stat{1}~=1; return; end;
resp=ret{1};
if ishandle(msghan)
  delete(msghan);
end % if ishandle
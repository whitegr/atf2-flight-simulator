function [stat Bdata Bdim_out PSdata PSdim_out] = latticeStatus(varargin)
% [stat Bdata Bdim_out] = latticeStatus
% Keep track of lattice and flag changes on a per-user level
% Use for knowing when a user needs information about change of lattice
% data that isn't taken care of through the usual FlHwUpdate procedure
% - need to clear user data with latticeStatus('clearUser',userID) upon client
% disconnect (clears current user set in sockrw)
%    Bdata = cell array of B fields
%    Bdim_out = BEAMLINE indicies of changed data
%    PSdata = BEAMLINE indexed array of PS.SetPt values changed from last
%    call
%    PSdim_out = BEAMLINE indicies of changed PS data
global BEAMLINE PS
persistent B Bdim pssp psspdim

% Clear user data if requested
if nargin==2 && strcmp(varargin{1},'clearUser') && ischar(varargin{2})
  if ~isempty(B) && isfield(B,varargin{2})
    B=rmfield(B,varargin{2});
  end
  return
elseif nargin
  error('Unknown command');
end

% Get current user connection
[stat, userID]=sockrw('get_user'); if stat{1}~=1; return; end; 

% --- BEAMLINE.B changes / PS SetPt changes
if isempty(Bdim)
  blist={}; Bdim=[]; pssplist=[]; psspdim=[];
  for iele=1:length(BEAMLINE)
    if isfield(BEAMLINE{iele},'B')
      blist{end+1}=BEAMLINE{iele}.B;
      Bdim(end+1)=iele;
    end
    if isfield(BEAMLINE{iele},'PS') && ~isempty(BEAMLINE{iele}.PS) && BEAMLINE{iele}.PS>0
      pssplist{end+1}=PS(BEAMLINE{iele}.PS).SetPt;
      psspdim(end+1)=iele;
    end
  end
else
  blist=cellfun(@(x) x.B,{BEAMLINE{Bdim}},'UniformOutput',false);
  pssplist=arrayfun(@(x) PS(BEAMLINE{x}.PS).SetPt,psspdim,'UniformOutput',false);
end
  
% If first connection or bfield vector changed from last time, then return
% updated values
Bdata=cell(size(blist)); PSdata=cell(size(pssplist));
if isempty(B) || ~isfield(B,userID)
  B.(userID)=blist;
  Bdata=blist;
  pssp.(userID)=pssplist;
  PSdata=pssplist;
else
  for ib=1:length(blist)
    if ~isequal(B.(userID){ib},blist{ib})
      Bdata{ib}=blist{ib};
      B.(userID){ib}=blist{ib};
    end
  end
  for ips=1:length(pssplist)
    if ~isequal(pssp.(userID){ips},pssplist{ips})
      PSdata{ips}=pssplist{ips};
      pssp.(userID){ips}=pssplist{ips};
    end
  end
end
PSdim_out=psspdim;
Bdim_out=Bdim;
function FlSetCORB(initP)
%
% Set B-values for XCORs and YCORs to present beam rigidity value

global BEAMLINE FL
persistent idc idb id lastP

if isempty(lastP)
  lastP=initP;
end

disp('DR energy has changed ... updating design B-values')

if isempty(idb)
  idb=findcells(BEAMLINE,'Class','SBEN');
  id=findcells(BEAMLINE,'B');
end

if isempty(idc)
  idc=sort([ ...
    findcells(BEAMLINE,'Class','XCOR'), ...
    findcells(BEAMLINE,'Class','YCOR'), ...
  ]);
end

Cb=1e9/299792458; % rigidity constant (T-m/GeV)

for n=1:length(idc)
  BEAMLINE{idc(n)}.B=BEAMLINE{idc(n)}.P*Cb;
end

% Change default B fields for quads etc
id=id(~ismember(id,idb) & ~ismember(id,idc));
for n=1:length(id)
  BEAMLINE{id(n)}.B=BEAMLINE{id(n)}.B.*(1+(BEAMLINE{id(n)}.P-lastP)./lastP);
end

lastP=BEAMLINE{id(1)}.P;


% Update GUI menu item
set(FL.Gui.main.E0,'Label',sprintf('E0 = %s',num2str(BEAMLINE{1}.P)))

end

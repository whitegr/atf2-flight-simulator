function FlECS(cmd)
% FLECS
% Floodland EPICS Command Server
% ------------------
% Available Commands (command EPICS string)
% ------------------
% GetIndByName
%   (Get BEAMLINE index from Name - use most upstream index)
%   input: strCmd = BEAMLINE Name
%   output: valResp = index
% GetEleField
%   (Get BEAMLINE field contents)
%   input: valCmd = BEAMLINE index
%          strCmd = BEAMLINE field name
%   output: valResp, strResp or arrResp depending on type of field
% GetRmat
%   (Get response matrix ind1:ind2)
%   input: arrCmd(1)=ind1 arrCmd(2)=ind2
%   output: arrResp(1:36)=R(:)
% GetRmati
%   (As for GetRmat, but returns R(*,2) and R(*,4) elements as m/rad per Amp
%   Requires ind1 to be a corrector)
% GetAmpConv
%   (Get conversion ratio or lookup table of control system Current -> PS
%   units)
%   input: valCmd = BEAMLINE index
%   output: valResp = conversion ratio
%           OR
%           arrResp = lookup table entry
% GetPS
%   (Get reading for PS)
%   input: strCmd = BEAMLINE name
%   output: valResp = PS value if exists
% GetK
%   Get K value for magnet
%   input: strCmd = BEAMLINE name
%   output: valResp = K value if exists
% GetAllPS
%   (Get readings of all PS's)
%   output: arrResp = PS setting indexed by BEAMLINE array index
% SetPS
%   (Set power supply)
%   input: strCmd = BEAMLINE name
%          valCmd = PS setting
% nmCavBpmBump
%   (Use corrector bump to produce position displacement in a non-mover cavity bpm)
%   input: strCmd = BEAMLINE name of bpm followed by desire axis (e.g.
%                   "MQD10Xx")
%          valCmd = size of bump / m
%
% nmCavBpmBumpRestore
%   (Restore bump to zero setting)
%   input: strCmd = BEAMLNE name followed by axis
%
% nmCavBpmBumpReset
%   (Define current location of bump to be zero)
%   input: strCmd = BEAMLNE name followed by axis
%
% nmCavBpmBumpInit
%   (Re-initialise bump calculations (e.g. when optics changes)
%
% bumpMPIP / bumpMPREIP / bumpIPBPMA / bumpIPBPMB
%   ( orbit bump for MPIP BPM )
%   input: strCmd = x | y
%          valCmd = bump size / m
%
% GetSIGMA
%   ( retrieve Model beam sigma matrix at given location by tracking )
%   input: valCmd = BEAMLINE element #
%     Only elements in EXT or FFS supported
%   output: arrResp = [nex ney SIGMA(1:36)]
%     - normalised horizontal and vertical emittances and 6*6 sigma matrix
%     in row-major order
%
% bpmOrbitFit
%    ( Fit orbit at provided BEAMLINE index )
%    input: arrCmd = [ Nbpm NfitInds [fitInds] NbpmInds [bpmInds] ]
%      - Nbpm = number of pulses back in time to use
%      - NfitInds, fitInds = Number in list and list of BEAMLINE indices to fit orbit to
%      - NbpmInds, bpmInds = Number in list and list of DOWNSTREAM BPMs to
%      use for fit (BEAMLINE indices)
%    output: arrResp = [ xfits yfits ] at requested locations (array length = 2 * NfitInds) 
%
% getDispFit
%    ( Get fitted dispersion data on requested set of BEAMLINE indices from
%    dispersion measurements taken with extDispersion app- data must have
%    been previously measured and saved)
%    input: arrCmd = list of BEAMLINE indices
%    output: arrResp = [disp(:) disp_err(:)]
%            disp = 4*N dispersion array
%            disp_err = RMS error on above
%
% GetOTREmitData
%    ( Compute emittance data gathered by OTR system using emit2dOTR.m)
%    input: arrCmd = [ useOTR0X useOTR1X useOTR2X useOTR3X ] ( 1 or 0 to
%                    use those OTRs in data analysis, requires at least 3)
%           strCmd = 'projected' | 'intrinsic'
%    output: arrResp = output data vector from emit2dOTR (see function)
%
% GetICT
%    ( Get ICT readout )
%    input: strCmd = 'ictName' (BEAMLINE model name of ICT)
%           valCmd = number of (past) pulses to read out, if 1 then readout
%           current INSTR value
%    output: arrResp = ICT readings (number requested) in # electrons units
%
% OtrOrbitFit
%    ( Fit orbit to OTR location using previous 2 BPM readings)
%    input: valCmd = OTR number (0,1,2 or 3)
%    output: arrResp = [xpos ypos] at chosen OTR in m
%
% moverCalCorFac
%    ( Get correction factor for mover-based cal of BPM attached to quad
%      [R11 element] - divide measured calibration factor by this )
%    input: strCmd = BEAMLINE model name of quadrupole
%           valCmd = 1 for x 2 for y
%    output: valResp = scale factor (R11 matrix element through quad)
%
% -----------
% EPICS PVs
% -----------
% FSECS:command
%   Command request (string)
% FSECS:stat
%   Status return 1=OK 0=error
% FSECS:strResp
%   String response
% FSECS:arrResp
%   Array (double) response
% FSECS:valResp
%   Scalar value response (double)
% FSECS:valCmd
%   Scalar command value (double)
% FSECS:arrCmd
%   Array command value (double)
% FSECS:strCmd
%   String command entry
% FSECS:errStr
%   Error string
global FL
persistent seq
if isempty(seq); seq=0; end;
stat{1}=1;

if ~isfield(FL,'FSECSPROC')
  FL.FSECSPROC=[];
end

% Not for sim mode for now
if FL.SimMode
  errordlg('Sim Mode doesn''t currently support EPICS Command Server!','ECS Error')
  return
end

switch lower(cmd)
  case 'start'
    if ~isfield(FL,'t_ecs')
      FL.t_ecs=timer('StartDelay',1,'Period',FL.Period_floodland,...
        'ExecutionMode','FixedRate','BusyMode','drop');
      FL.t_ecs.TimerFcn = 'FlECS(''cmdProc'')';
      FL.t_ecs.StopFcn = 'FlECS(''stop'')';
      lcaSetMonitor('FSECS:command');
      start(FL.t_ecs);
    elseif ~strcmp(FL.t_ecs.running,'on')
      lcaPut('FSECS:command',' ');
      lcaSetMonitor('FSECS:command');
      start(FL.t_ecs)
    end
    if isfield(FL,'Gui') && isfield(FL.Gui,'main') && ishandle(FL.Gui.main.figure1)
      set(FL.Gui.main.pushbutton19,'BackgroundColor','green')
      drawnow('expose')
    end
    return
  case 'cmdproc'
    % Indicate process running
    lcaPut('FSECS:RUNNING',1);
    if lcaNewMonitorValue('FSECS:command') && isempty(FL.FSECSPROC)
      newcmd=lcaGet('FSECS:command');
      if strcmp(newcmd,' '); return; end;
      if isempty(newcmd); return; end;
      newcmd=lower(newcmd{1});
      if strcmp(newcmd,' ')
        return
      end
      % Only allow to process private subroutines
      w=which(newcmd,'in','FlECS');
      try
        if isempty(w)
          error('Unknown Command')
        elseif isempty(regexp(w,'FlECS.m$', 'once'))
          error('Denied command')
        end
        FL.FSECSPROC=true;
        % ensure FS updated
        FlHwUpdate('force');
        evalc(['stat=' newcmd]);
      catch ME
        lcaPut('FSECS:command',' ');
        FL.FSECSPROC=[];
        stat{1}=-1;
        stat{2}=ME.message;
        lcaSetMonitor('FSECS:command');
      end
      lcaPut('FSECS:command',' '); % reset command string to communicate processing has occured
      FL.FSECSPROC=[];
    else
      return
    end
  case 'stop'
    if isfield(FL,'Gui') && isfield(FL.Gui,'main') && ishandle(FL.Gui.main.figure1)
      set(FL.Gui.main.pushbutton19,'BackgroundColor','red')
      drawnow('expose')
    end
  otherwise
    error('Unknown command')
end

% Write status return and any error message
lcaPut('FSECS:stat',stat{1});
if stat{1}==1
  lcaPut('FSECS:errStr','OK');
else
  lcaPut('FSECS:errStr',{stat{2}(1:min(39,length(stat{2})))})
end


%% Server commands
function stat = getindbyname %#ok<*DEFNU>
global BEAMLINE
stat{1}=1;
name=lcaGet('FSECS:strCmd');
ind=findcells(BEAMLINE,'Name',name{1});
if ~isempty(ind) && any(ind)>0
  lcaPut('FSECS:valResp',ind(end));
else
  stat{1}=-1;
  stat{2}='Unknown element';
end

function stat = getelefield
global BEAMLINE
stat{1}=1;
ind=lcaGet('FSECS:valCmd');
fname=lcaGet('FSECS:strCmd');
try
  resp=BEAMLINE{ind}.(fname);
  if ischar(resp)
    lcaPut('FSECS:strResp',resp);
  elseif ~isnan(resp)
    if length(resp)>1
      lcaPut('FSECS:arrResp',resp);
    else
      lcaPut('FSECS:valResp',resp);
    end
  else
    stat{1}=-1; stat{2}='Couldn''t parse request';
  end
catch ME
  stat{1}=-1; stat{2}=sprintf('Error processing request: %s',ME.message);
end

function stat = getict
global BEAMLINE INSTR
stat{1}=1;
name=lcaGet('FSECS:strCmd');
ind=findcells(BEAMLINE,'Name',name);
if isempty(ind)
  stat{1}=-1;
  stat{2}='No element by this name';
  return
end
instind=findcells(INSTR,'Index',ind);
if isempty(instind)
  stat{1}=-1;
  stat{2}='No INSTR by this name';
  return
end
num=lcaGet('FSECS:valCmd');
if num>1
  [stat, bpmdata]=FlHwUpdate('readbuffer',num);
  lcaPut('FSECS:arrResp',bpmdata(:,1+instind*3));
else
  lcaPut('FSECS:arrResp',INSTR{instind}.Data(3));
end

function stat = getrmat
cmdarr=lcaGet('FSECS:arrCmd',2);
[stat,R]=RmatAtoB(cmdarr(1),cmdarr(2));
if stat{1}~=1; return; end;
lcaPut('FSECS:arrResp',R(:)');

function stat = getrmati
global BEAMLINE FL
cmdarr=lcaGet('FSECS:arrCmd',2);
[stat,R]=RmatAtoB(cmdarr(1),cmdarr(2));
if stat{1}~=1; return; end;
if isfield(BEAMLINE{cmdarr(2)},'PS') && ~isempty(BEAMLINE{cmdarr(2)}.PS) && BEAMLINE{cmdarr(2)}.PS && ...
    length(FL.HwInfo.PS(BEAMLINE{cmdarr(2)}.PS).conv)==1
  R(:,2)=R(:,2)*FL.HwInfo.PS(BEAMLINE{cmdarr(2)}.PS).conv;
  R(:,4)=R(:,4)*FL.HwInfo.PS(BEAMLINE{cmdarr(2)}.PS).conv;
end
lcaPut('FSECS:arrResp',R(:)');

function stat = getampconv
global BEAMLINE
cmdval=lcaGet('FSECS:valCmd');
stat{1}=1;
if ~isempty(cmdval) && cmdval>0 && cmdval<=length(BEAMLINE) && isfield(BEAMLINE{cmdval},'PS') &&...
    ~isempty(BEAMLINE{cmdval}.PS) && BEAMLINE{cmdval}.PS>0
  conv=FL.HwInfo.PS(BEAMLINE{cmdval}.PS).conv;
else
  stat{1}=-1;
  stat{2}='No PS to read for this element';
  return
end
if isempty(conv) || isequal(conv,0)
  stat{1}=-1;
  stat{2}='No conversion for this PS';
  return
end

if length(conv)==1
  lcaPut('FSECS:valResp',conv)
else
  lcaPut('FSECS:valResp',0)
  lcaPut('FSECS:arrResp',conv(:))
end

function stat = setps
global PS BEAMLINE
stat{1}=1;
cmdstr=lcaGet('FSECS:strCmd');
cmdval=lcaGet('FSECS:valCmd');
iele=findcells(BEAMLINE,'Name',cmdstr{1});
if isempty(iele) || ~isfield(BEAMLINE{iele},'PS')
  stat{1}=-1;
  stat{2}='No PS or unknown name';
  return
end
PS(BEAMLINE{iele}.PS).SetPt=cmdval;
stat=PSTrim(BEAMLINE{iele}.PS,1);

function stat = getk
global BEAMLINE PS
stat{1}=1;
cmdstr=lcaGet('FSECS:strCmd');
iele=findcells(BEAMLINE,'Name',cmdstr{1}); iele=iele(1);
if isempty(iele) || ~isfield(BEAMLINE{iele},'PS')
  stat{1}=-1;
  stat{2}='No PS or unknown name';
  return
end
try
  brho=BEAMLINE{iele}.P / 0.299792458 ;
  k=(BEAMLINE{iele}.B*PS(BEAMLINE{iele}.PS).Ampl*2)/(brho);
  lcaPut('FSECS:valResp',k);
catch ME
  stat{1}=-1;
  stat{2}=ME.message;
end
function stat = getps
global BEAMLINE PS
stat{1}=1;
cmdstr=lcaGet('FSECS:strCmd');
iele=findcells(BEAMLINE,'Name',cmdstr{1});
if isempty(iele) || ~isfield(BEAMLINE{iele},'PS')
  stat{1}=-1;
  stat{2}='No PS or unknown name';
  return
end
lcaPut('FSECS:valResp',PS(BEAMLINE{iele}.PS).Ampl);

function stat = getallps
global BEAMLINE PS
stat{1}=1;
psval=zeros(1,length(BEAMLINE));
for ips=1:length(PS)
  if ~isempty(PS(ips).Element) && PS(ips).Element(1)>0
    psval(PS(ips).Element(end))=PS(ips).Ampl(1);
  end
end
lcaPut('FSECS:arrResp',psval)

function stat = nmcavbpmbump
useApp('bpmcal');
stat{1}=1;
bpmcmd=lcaGet('FSECS:strCmd');
bpm=bpmcmd{1};
val=lcaGet('FSECS:valCmd');
if ~strcmp(bpm(end),'x') && ~strcmp(bpm(end),'y')
  stat{1}=-1; stat{2}='must append x or y to bpm name';
  return
end
% Make sure updated
FlHwUpdate;
try
  stat=nmCavCal('SetBump',bpm(1:end-1),bpm(end),val);
catch ME
  stat{1}=-1;
  stat{2}=ME.message;
  return
end

function stat = nmcavbpmbumpreset
useApp('bpmcal');
bpmcmd=lcaGet('FSECS:strCmd');
bpm=bpmcmd{1}(1:end-1);
if ~strcmp(bpmcmd{1}(end),'x') && ~strcmp(bpmcmd{1}(end),'y')
  stat{1}=-1;
  stat{2}='Must supply x or y';
  return
end
stat=nmCavCal('resetbumps',bpm,bpmcmd{1}(end));

function stat = nmcavbpmbumprestore
useApp('bpmcal');
bpmcmd=lcaGet('FSECS:strCmd');
bpm=bpmcmd{1}(1:end-1);
if ~strcmp(bpmcmd{1}(end),'x') && ~strcmp(bpmcmd{1}(end),'y')
  stat{1}=-1;
  stat{2}='Must supply x or y';
  return
end
stat=nmCavCal('restorebumps',bpm,bpmcmd{1}(end));

function stat = nmcavbpmbumpinit
useApp('bpmcal');
stat=nmCavCal('init');

function stat = bumpipinit
stat{1}=1;
qd0bump('init');

function stat = bumpmpip
axis=lcaGet('FSECS:strCmd');
bumpval=lcaGet('FSECS:valCmd');
if ~strcmpi(axis{1},'x') && ~strcmpi(axis{1},'y') && ~strcmpi(axis{1},'xp') && ~strcmpi(axis{1},'yp')
  stat{1}=-1;
  stat{2}='Must supply x, xp, y or yp';
  return
end
if isnan(bumpval) || isinf(bumpval) || abs(bumpval)>1.5e-3
  stat{1}=-1; stat{2}='Bad bump value given (max -1.5e-3 : 1.5e-3)';
  return
end
bump=zeros(4,1);
switch lower(axis{1})
  case 'x'
    bump(1)=bumpval;
  case 'xp'
    bump(2)=bumpval;
  case 'y'
    bump(3)=bumpval;
  case 'yp'
    bump(4)=bumpval;
  otherwise
    stat{1}=-1;
    stat{2}='Must supply x, xp, y or yp in FSECS:strCmd';
    return
end
fprintf('BUMP command MPIP: %g\n',bumpval)
stat=qd0bump('MPIP',bump);
fprintf('status: %d\n',stat{1})

function stat = bumpmpreip
axis=lcaGet('FSECS:strCmd');
bumpval=lcaGet('FSECS:valCmd');
if ~strcmpi(axis{1},'x') && ~strcmpi(axis{1},'y') && ~strcmpi(axis{1},'xp') && ~strcmpi(axis{1},'yp')
  stat{1}=-1;
  stat{2}='Must supply x, xp, y or yp';
  return
end
if isnan(bumpval) || isinf(bumpval) || abs(bumpval)>1.5e-3
  stat{1}=-1; stat{2}='Bad bump value given (max -1.5e-3 : 1.5e-3)';
  return
end
bump=zeros(4,1);
switch lower(axis{1})
  case 'x'
    bump(1)=bumpval;
  case 'xp'
    bump(2)=bumpval;
  case 'y'
    bump(3)=bumpval;
  case 'yp'
    bump(4)=bumpval;
  otherwise
    stat{1}=-1;
    stat{2}='Must supply x, xp, y or yp in FSECS:strCmd';
    return
end
stat=qd0bump('MPREIP',bump);

function stat = bumpipbpma
axis=lcaGet('FSECS:strCmd');
bumpval=lcaGet('FSECS:valCmd');
if ~strcmpi(axis{1},'x') && ~strcmpi(axis{1},'y') && ~strcmpi(axis{1},'xp') && ~strcmpi(axis{1},'yp')
  stat{1}=-1;
  stat{2}='Must supply x, xp, y or yp';
  return
end
if isnan(bumpval) || isinf(bumpval) || abs(bumpval)>1.5e-3
  stat{1}=-1; stat{2}='Bad bump value given (max -1.5e-3 : 1.5e-3)';
  return
end
bump=zeros(4,1);
switch lower(axis{1})
  case 'x'
    bump(1)=bumpval;
  case 'xp'
    bump(2)=bumpval;
  case 'y'
    bump(3)=bumpval;
  case 'yp'
    bump(4)=bumpval;
  otherwise
    stat{1}=-1;
    stat{2}='Must supply x, xp, y or yp in FSECS:strCmd';
    return
end
stat=qd0bump('IPBPMA',bump);

function stat = bumpipbpmb
axis=lcaGet('FSECS:strCmd');
bumpval=lcaGet('FSECS:valCmd');
if ~strcmpi(axis{1},'x') && ~strcmpi(axis{1},'y') && ~strcmpi(axis{1},'xp') && ~strcmpi(axis{1},'yp')
  stat{1}=-1;
  stat{2}='Must supply x, xp, y or yp';
  return
end
if isnan(bumpval) || isinf(bumpval) || abs(bumpval)>1.5e-3
  stat{1}=-1; stat{2}='Bad bump value given (max -1.5e-3 : 1.5e-3)';
  return
end
bump=zeros(4,1);
switch lower(axis{1})
  case 'x'
    bump(1)=bumpval;
  case 'xp'
    bump(2)=bumpval;
  case 'y'
    bump(3)=bumpval;
  case 'yp'
    bump(4)=bumpval;
  otherwise
    stat{1}=-1;
    stat{2}='Must supply x, xp, y or yp in FSECS:strCmd';
    return
end
stat=qd0bump('IPBPMB',bump);


function stat = bpmorbitfit
global BEAMLINE
stat{1}=1;

% Unpack arguments and check
args=lcaGet('FSECS:arrCmd'); args=args(args~=0);
if length(args)<3
  stat{1}=-1; stat{2}='Not enough non-zero arrCmd entries'; return;
end
nbpm=floor(args(1));
if nbpm<1 || nbpm>1000
  stat{1}=-1; stat{2}='1<=Nbpm<=1000'; return;
end
nFitInd=floor(args(2));
fitInd=floor(args(3:3+nFitInd));
if any(fitInd)<1 || any(fitInd)>length(BEAMLINE)
  stat{1}=-1; stat{2}='No such element number in BEAMLINE for fitInd'; return;
end
nBpmInds=floor(args(3+nFitInd+1));
bpmInds=floor(args(3+nFitInd+2:3+nFitInd+2+nBpmInds));
if any(bpmInds>length(BEAMLINE)) || any(bpmInds<1)
  stat{1}=-1; stat{2}='No such element number in BEAMLINE for 1 or more fit BPM'; return;
end
if any(~arrayfun(@(x) strcmp(BEAMLINE{x}.Class,'MONI'),bpmInds))
  stat{1}=-1; stat{2}='One or more request fit BPM index is not really a BPM'; return;
end

% Setup fitting problem
[~, l]=FlBeamModel('GetLocal');
useBpms=false(1,length(l.allBpmsInd));
useBpms(ismember(l.allBpmsInd,bpmInds))=true;
FlBeamModel('UseBPMs',useBpms);
FlBeamModel('SetNBPMAve',nbpm);

% Perform fit
FlBeamModel('DoOrbitFit');

% Get fitted orbit in requested indices
[stat, xtrack, ytrack] = FlBeamModel('EleTrack');
xfit=xtrack(1+fitInd-bpmInds(1));
yfit=ytrack(1+fitInd-bpmInds(1));

% Output results
lcaPut('FSECS:arrResp',[xfit yfit]);

function stat = getsigma
global BEAMLINE FL GIRDER PS
persistent Beam1_IEX
stat{1}=1;
iele=lcaGet('FSECS:valCmd');
iex=findcells(BEAMLINE,'Name','IEX');
if isnan(iele) || isinf(iele) || iele<iex || iele>length(BEAMLINE)
  stat{1}=-1; stat{2}='Invalid BEAMLINE element request, must be int > IEX point and less than BEAMLINE length';
  return
end
% Suspend FS updates
FL.noUpdate=true;
% Load default optics
bl=BEAMLINE; ps=PS; gir=GIRDER;
if isfield(FL.SimModel,'optics_version')
  ver=FL.SimModel.optics_version;
else
  ver=4.2;
end
if isfield(FL.SimModel,'optics')
  load(sprintf('latticeFiles/src/v%g/ATF2lat_%s.mat',ver,FL.SimModel.optics),'BEAMLINE','GIRDER','PS')
else
  load latticeFiles/ATF2lat BEAMLINE PS GIRDER
end
  
% Track beam to desired location and return emittances and sigma matrix
if isempty(Beam1_IEX)
  load latticeFiles/ATF2lat.mat Beam1_IEX
end
[stat,B]=TrackThru(iex,iele,Beam1_IEX,1,1,0); if stat{1}~=1; return; end;
[nx,ny] = GetNEmitFromBeam( B, 1 );
[~,sigma]=GetBeamPars(B,1);
lcaPut('FSECS:arrResp',[nx,ny,sigma(:)']);
% Put back model and resume updates
BEAMLINE=bl; GIRDER=gir; PS=ps;
FL.noUpdate=false;
FlHwUpdate;

function r=getquadr(q,bpm,B)
global BEAMLINE GIRDER
gind=BEAMLINE{q(1)}.Girder;
gpos=linspace(-1e-4,1e-4,10);
opos=GIRDER{gind}.MoverSetPt;
xpos=zeros(length(gpos),1); ypos=xpos;
GIRDER{gind}.MoverPos=[0 0 0];
for ipos=1:length(gpos)
  GIRDER{gind}.MoverPos(1)=opos(1)+gpos(ipos);
  [~,~,instdata]=TrackThru(q(1),bpm,B,1,1,0);
  xpos(ipos)=instdata{1}(end).x;
end
fq=noplot_polyfit(gpos,xpos,1,1); r(1)=fq(end);
GIRDER{gind}.MoverPos=opos;
for ipos=1:length(gpos)
  GIRDER{gind}.MoverPos(2)=opos(2)+gpos(ipos);
  [~,~,instdata]=TrackThru(q(1),bpm,B,1,1,0);
  ypos(ipos)=instdata{1}(end).y;
end
fq=noplot_polyfit(gpos,ypos,1,1); r(2)=-fq(end);
GIRDER{gind}.MoverPos=opos;

function stat = getdispfit
global FL BEAMLINE
stat{1}=1;

useApp('extDispersion');

% Check extDisersion GUI not in use
if isfield(FL.Gui,'extDispersion') && ishandle(FL.Gui.extDispersion.figure1)
  stat{1}=-1; stat{2}='extDispersion program in use, shut down GUI on FS server before trying again';
  return
end

% restore last saved dispersion measurement
stat=extDispersion_run('restore');
if stat{1}~=1
  stat{2}=sprintf('Cannot restore Dispersion measurements- make sure a set is taken with extDispersion app and saved, error message = %s',stat{2});
  return
end

% Fit data to BEAMLINE indices of choice
[stat, data]=extDispersion_run('GetData');
if stat{1}~=1
  stat{2}=sprintf('No dispersion data available, take some and save with extDispersion app, error message = %s',stat{2});
  return
end
bind=lcaGet('FSECS:arrCmd'); bind=bind(bind~=0);
if any(~ismember(bind,1:length(BEAMLINE)))
  stat{1}=-1;
  stat{2}='Requested indices outside BEAMLINE array';
  return
end
[stat,Df,dDf]=dispfit(bind,data);
if stat{1}~=1
  stat{2}=sprintf('Error in dispfit calculation: %s',stat{2});
  return;
end
lcaPut('FSECS:arrResp',[Df(:)' dDf(:)'])

function stat=qd0qf1bump(bpmname,bump)
global BEAMLINE GIRDER FL
persistent initpos

stat{1}=1;

iqf1=findcells(BEAMLINE,'Name','QF7FF');
iqd0=findcells(BEAMLINE,'Name','QD0FF');
ibpm=findcells(BEAMLINE,'Name',bpmname);
gqf1=BEAMLINE{iqf1(1)}.Girder;
gqd0=BEAMLINE{iqd0(1)}.Girder;

if isequal(bpmname,'init') || isempty(initpos)
  initpos{1}=GIRDER{gqf1}.MoverPos(1:2);
  initpos{2}=GIRDER{gqd0}.MoverPos(1:2);
  if isequal(bpmname,'init'); return; end;
end

% Suspend FS updates
FL.noUpdate=true;
% Remove offsets in model so tracking works
bl=BEAMLINE; gir=GIRDER;
for ibl=1:length(BEAMLINE)
  if isfield(BEAMLINE{ibl},'Offset')
    BEAMLINE{ibl}.Offset=BEAMLINE{ibl}.Offset.*0;
  end
end
for ig=1:length(GIRDER)
  if isfield(GIRDER{ig},'MoverPos')
    GIRDER{ig}.MoverPos=GIRDER{ig}.MoverPos.*0;
    GIRDER{ig}.Offset=GIRDER{ig}.Offset.*0;
  end
end
% Also set corrector strengths to 0
for icor=findcells(BEAMLINE,'Class','*COR')
  BEAMLINE{icor}.B=BEAMLINE{icor}.B.*0;
end

B=FL.SimBeam{2};
B.Bunch.x(1:5)=0;
dpos=linspace(-1e-4,1e-4,20);
gqf1_init=GIRDER{gqf1}.MoverPos;
for ipos=1:length(dpos)
  GIRDER{gqf1}.MoverPos(1)=gqf1_init(1)+dpos(ipos);
  [~, bo]=TrackThru(iqf1(1),ibpm,B,1,1,0);
  x(ipos)=bo.Bunch.x(1);
  xp(ipos)=bo.Bunch.x(2);
  y(ipos)=bo.Bunch.x(3);
  yp(ipos)=bo.Bunch.x(4);
end
q=noplot_polyfit(dpos,x,0,1);
R11_qf1=q(2);
q=noplot_polyfit(dpos,xp,0,1);
R21_qf1=q(2);
q=noplot_polyfit(dpos,y,0,1);
R31_qf1=q(2);
q=noplot_polyfit(dpos,yp,0,1);
R41_qf1=q(2);
GIRDER{gqf1}.MoverPos=gqf1_init;
for ipos=1:length(dpos)
  GIRDER{gqf1}.MoverPos(2)=gqf1_init(2)+dpos(ipos);
  [~, bo]=TrackThru(iqf1(1),ibpm,B,1,1,0);
  x(ipos)=bo.Bunch.x(1);
  xp(ipos)=bo.Bunch.x(2);
  y(ipos)=bo.Bunch.x(3);
  yp(ipos)=bo.Bunch.x(4);
end
q=noplot_polyfit(dpos,x,0,1);
R13_qf1=q(2);
q=noplot_polyfit(dpos,xp,0,1);
R23_qf1=q(2);
q=noplot_polyfit(dpos,y,0,1);
R33_qf1=q(2);
q=noplot_polyfit(dpos,yp,0,1);
R43_qf1=q(2);
GIRDER{gqf1}.MoverPos=gqf1_init;
gqd0_init=GIRDER{gqd0}.MoverPos;
for ipos=1:length(dpos)
  GIRDER{gqd0}.MoverPos(1)=gqd0_init(1)+dpos(ipos);
  [~, bo]=TrackThru(iqd0(1),ibpm,B,1,1,0);
  x(ipos)=bo.Bunch.x(1);
  xp(ipos)=bo.Bunch.x(2);
  y(ipos)=bo.Bunch.x(3);
  yp(ipos)=bo.Bunch.x(4);
end
q=noplot_polyfit(dpos,x,0,1);
R11_qd0=q(2);
q=noplot_polyfit(dpos,xp,0,1);
R21_qd0=q(2);
q=noplot_polyfit(dpos,y,0,1);
R31_qd0=q(2);
q=noplot_polyfit(dpos,yp,0,1);
R41_qd0=q(2);
GIRDER{gqd0}.MoverPos=gqd0_init;
for ipos=1:length(dpos)
  GIRDER{gqd0}.MoverPos(2)=gqd0_init(2)+dpos(ipos);
  [~, bo]=TrackThru(iqd0(1),ibpm,B,1,1,0);
  x(ipos)=bo.Bunch.x(1);
  xp(ipos)=bo.Bunch.x(2);
  y(ipos)=bo.Bunch.x(3);
  yp(ipos)=bo.Bunch.x(4);
end
q=noplot_polyfit(dpos,x,0,1);
R13_qd0=q(2);
q=noplot_polyfit(dpos,xp,0,1);
R23_qd0=q(2);
q=noplot_polyfit(dpos,y,0,1);
R33_qd0=q(2);
q=noplot_polyfit(dpos,yp,0,1);
R43_qd0=q(2);
R=[R11_qf1 R13_qf1 R11_qd0 R13_qd0;
   R21_qf1 R23_qf1 R21_qd0 R23_qd0;
   R31_qf1 R33_qf1 R31_qd0 R33_qd0;
   R41_qf1 R43_qf1 R41_qd0 R43_qd0];
X=R\bump;
% Put back model and resume updates ...
BEAMLINE=bl; GIRDER=gir;
FL.noUpdate=false;
FlHwUpdate;
% Apply bump
GIRDER{gqf1}.MoverSetPt(1:2)=initpos{1}+X(1:2)';
GIRDER{gqd0}.MoverSetPt(1:2)=initpos{2}+X(3:4)';
stat=MoverTrim([gqf1 gqd0],3);

function stat=qd0bump(bpmname,bump)
global BEAMLINE GIRDER FL
persistent initpos

stat{1}=1;
iqd0=findcells(BEAMLINE,'Name','QD0FF');
ibpm=findcells(BEAMLINE,'Name',bpmname);
gqd0=BEAMLINE{iqd0(1)}.Girder;

if isequal(bpmname,'init') || isempty(initpos)
  initpos=GIRDER{gqd0}.MoverPos(1:2);
  if isequal(bpmname,'init'); return; end;
end

% Suspend FS updates
FL.noUpdate=true;
% Remove offsets in model so tracking works
bl=BEAMLINE; gir=GIRDER;
for ibl=1:length(BEAMLINE)
  if isfield(BEAMLINE{ibl},'Offset')
    BEAMLINE{ibl}.Offset=BEAMLINE{ibl}.Offset.*0;
  end
end
for ig=1:length(GIRDER)
  if isfield(GIRDER{ig},'MoverPos')
    GIRDER{ig}.MoverPos=GIRDER{ig}.MoverPos.*0;
    GIRDER{ig}.Offset=GIRDER{ig}.Offset.*0;
  end
end

B=FL.SimBeam{2};
B.Bunch.x(1:5)=0;
dpos=linspace(-1e-4,1e-4,20);
gqd0_init=GIRDER{gqd0}.MoverPos;
for ipos=1:length(dpos)
  GIRDER{gqd0}.MoverPos(1)=gqd0_init(1)+dpos(ipos);
  [~, bo]=TrackThru(iqd0(1),ibpm,B,1,1,0);
  x(ipos)=bo.Bunch.x(1);
end
q=noplot_polyfit(dpos,x,0,1);
R11_qd0=q(2);
GIRDER{gqd0}.MoverPos=gqd0_init;
for ipos=1:length(dpos)
  GIRDER{gqd0}.MoverPos(2)=gqd0_init(2)+dpos(ipos);
  [~, bo]=TrackThru(iqd0(1),ibpm,B,1,1,0);
  y(ipos)=bo.Bunch.x(3);
end
q=noplot_polyfit(dpos,y,0,1);
R33_qd0=q(2);
X=[bump(1)./R11_qd0 bump(3)./R33_qd0];
% Put back model and resume updates
BEAMLINE=bl; GIRDER=gir;
FL.noUpdate=false;
FlHwUpdate;
% Apply bump
GIRDER{gqd0}.MoverSetPt(1:2)=initpos+X;
stat=MoverTrim(gqd0,3);

function stat=getotremitdata
global BEAMLINE PS %#ok<NUSED>
otruse=lcaGet('FSECS:arrCmd',4);
type=lcaGet('FSECS:strCmd');
if strcmp(type,'intrinsic')
  dointrinsic=true;
elseif strcmp(type,'projected')
  dointrinsic=false;
else
  stat{1}=-1;
  stat{2}='FSECS:strCmd must be ''intrinsic'' or ''projected''';
  return
end
try
  [stat, emitData] = emit2dOTR(otruse,dointrinsic,false);
  if stat{1}~=1; return; end;
catch ME
  stat{1}=-1;
  stat{2}=sprintf('Error in emit2dOTR code: %s',ME.message);
  return
end
try
  lcaPut('FSECS:arrResp',emitData);
  if ~exist('userData/OTR','dir')
    mkdir('userData','OTR');
  end
catch
  stat{1}=-1;
  stat{2}='Error putting emittance data to database';
end

function stat=otrorbitfit
global BEAMLINE INSTR

% Which OTR?
otr=lcaGet('FSECS:valCmd');

% Get OTR index
iotr=findcells(BEAMLINE,'Name',sprintf('OTR%dX',otr));

% Get BPM indices
wbpm={{'MQF17X' 'MQD18X'} {'MQD18X' 'MQF19X'} {'MQF19X' 'MQD20X'} {'MQD20X' 'MQF21X'}};
ibpm1=findcells(BEAMLINE,'Name',wbpm{otr+1}{1}); instbpm1=findcells(INSTR,'Index',ibpm1);
ibpm2=findcells(BEAMLINE,'Name',wbpm{otr+1}{2}); instbpm2=findcells(INSTR,'Index',ibpm2);

% Get response matrices
[stat, R12]=RmatAtoB(ibpm1,ibpm2); if stat{1}~=1; return; end;
[stat, R2X]=RmatAtoB(ibpm2,iotr); if stat{1}~=1; return; end;

% Get BPM readings
[stat, bpmdata]=FlHwUpdate('bpmave',10);
x1=bpmdata{1}(instbpm1,1);
x2=bpmdata{1}(instbpm2,1);
y1=bpmdata{1}(instbpm1,2);
y2=bpmdata{1}(instbpm2,2);

% Fit to OTR
xp1=( ((x2-R12(11)*x1-R12(1,3)*y1)/R12(1,2)) - (R12(1,4)/(R12(3,4)*R12(1,2)))*(y2-R12(3,3)*y1-R12(3,1)*x1) ) / ( 1 - (R12(3,2)*R12(1,4))/(R12(3,4)*R12(1,2))) ;
yp1=( ((y2-R12(33)*y1-R12(3,1)*x1)/R12(3,4)) - (R12(3,2)/(R12(1,2)*R12(3,4)))*(x2-R12(1,1)*x1-R12(1,3)*y1) ) / ( 1 - (R12(1,4)*R12(3,2))/(R12(1,2)*R12(3,4))) ;
xotr=R2X(1,1)*x1 + R2X(1,2)*xp1 + R2X(1,3)*y1 + R2X(1,4)*yp1;
yotr=R2X(3,1)*x1 + R2X(3,2)*xp1 + R2X(3,3)*y1 + R2X(3,4)*yp1;

% Send response
lcaPut('FSECS:arrResp',[xotr yotr]);


function stat = movercalcorfac
%    ( Get correction factor for mover-based cal of BPM attached to quad
%      [R11 element] - divide measured calibration factor by this )
%    input: strCmd = BEAMLINE model name of quadrupole
%           valCmd = 1 for x 2 for y
%    output: valResp = scale factor (R11 matrix element through quad)
global BEAMLINE
stat{1}=1;

name=lcaGet('FSECS:strCmd');
dim=lcaGet('FSECS:valCmd');
iele=findcells(BEAMLINE,'Name',name);
if length(iele)~=2
  stat{1}=-1; stat{2}='Unknown BEAMLINE element name';
  return
end

[stat,R]=RmatAtoB(iele(1),iele(2));
if stat{1}~=1; return; end;

if dim==1
  lcaPut('FSECS:valResp',R(1,1));
elseif dim==2
  lcaPut('FSECS:valResp',R(3,3));
end

function stat=correctcoupling
global PS GIRDER BEAMLINE FL
persistent Bcorrect skewps
otruse=lcaGet('FSECS:arrCmd',4);
method=lcaGet('FSECS:strCmd'); if iscell(method); method=method{1}; end;
try
  Bcorrect=[];
  FL.noUpdate=true;
  bl=BEAMLINE; ps=PS; gir=GIRDER;%save real values apart
  if strcmp(method,'modelmin')
    [stat, Bcorrect, skewps] = correct_coupling(otruse, BEAMLINE, PS, GIRDER, FL, 2);
  elseif strcmp(method,'model')
    latticeversion=FL.SimModel.opticsVersion;
    latticename=FL.SimModel.opticsName;
    % Keep EXT quad strengths for modeling of coupling response
    % qn={'QF1X' 'QD2X' 'QF3X' 'QF4X' 'QD5X' 'QF6X' 'QF7X' 'QD8X' 'QD9X' 'QD10X' 'QF11X' 'QD12X' 'QF13X' 'QD14X' 'QF15X' ...
    %   'QD16X' 'QF17X' 'QD18X' 'QF19X' 'QD20X' 'QF21X'};
    % for iq=1:length(qn);qnind=findcells(BEAMLINE,'Name',qn{iq}); qnps(iq)=BEAMLINE{qnind(1)}.PS; psval(iq)=PS(qnps(iq)).Ampl; end;
    load(sprintf('latticeFiles/src/v%s/ATF2lat_%s',latticeversion,latticename),'BEAMLINE','PS','GIRDER')%load model values for tracking
    % for iq=1:length(qn); PS(qnps(iq)).Ampl=psval(iq); PS(qnps(iq)).SetPt=psval(iq); end;
    [stat, Bcorrect, skewps] = correct_coupling(otruse, BEAMLINE, PS, GIRDER, FL, 1);
  elseif strcmp(method,'apply')
    %Change the skew strengths
    for i=1:4
       PS(skewps(i)).SetPt=Bcorrect(i);
       stat=PSTrim(skewps(i),1);
    end
    return
  end
  if stat{1}~=1; return; end;
catch ME
  stat{1}=-1;
  stat{2}=sprintf('Error in correct_coupling code: %s',ME.message);
  %Return to the real values and restart updating
  if strcmp(method,'mode')
    BEAMLINE=bl; PS=ps; GIRDER=gir;
    FL.noUpdate=false;
  end
  return
end

%Return to the real values and restart updating
BEAMLINE=bl; PS=ps; GIRDER=gir;
FL.noUpdate=false;

%Calculate the final set value
if strcmp(method,'modelmin')
  for i=1:4
    Bcorrect(i)=ps(skewps(i)).Ampl+Bcorrect(i);
  end
end

%Limit protection
Bcorrect(Bcorrect>0.2227)=0.2227;
Bcorrect(Bcorrect<-0.2227)=-0.2227;

try
  lcaPut('FSECS:arrResp',Bcorrect');
catch
  stat{1}=-1;
  stat{2}='Error putting Bcorrect to database';
end

function stat=extmatch
persistent M
stat{1}=1;

cmd=lcaGet('FSECS:strCmd');
switch cmd{1}
  case 'domatch'
    delete(M)
    M=extMatch;
    T=M.twissMatched; % alpha_x beta_x alpha_y beta_y eta_x etap_x eta_y etap_y Matched values
    Td=M.designTwiss; % Design match parameters
    lcaPut('FSECS:arrResp',[T.alphax(end) T.betax(end) T.alphay(end) T.betay(end) Td.alpha_x Td.beta_x Td.alpha_y Td.beta_y ...
      M.psValsI M.psMatchValsI]);
  case 'dotrim'
    if isempty(M) || isempty(M.twissMatched) || any(isnan(M.twissMatched))
      stat{1}=-1; stat{2}='No matching results to trim';
      return;
    end
    M.setPS;
    FlHwUpdate('force');
end

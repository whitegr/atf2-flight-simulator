function varargout = FlFB_spectralAnalGUI(varargin)
% FLFB_SPECTRALANALGUI MATLAB code for FlFB_spectralAnalGUI.fig
%      FLFB_SPECTRALANALGUI, by itself, creates a new FLFB_SPECTRALANALGUI or raises the existing
%      singleton*.
%
%      H = FLFB_SPECTRALANALGUI returns the handle to a new FLFB_SPECTRALANALGUI or the handle to
%      the existing singleton*.
%
%      FLFB_SPECTRALANALGUI('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in FLFB_SPECTRALANALGUI.M with the given input arguments.
%
%      FLFB_SPECTRALANALGUI('Property','Value',...) creates a new FLFB_SPECTRALANALGUI or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before FlFB_spectralAnalGUI_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to FlFB_spectralAnalGUI_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help FlFB_spectralAnalGUI

% Last Modified by GUIDE v2.5 02-Nov-2010 15:06:26

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @FlFB_spectralAnalGUI_OpeningFcn, ...
                   'gui_OutputFcn',  @FlFB_spectralAnalGUI_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before FlFB_spectralAnalGUI is made visible.
function FlFB_spectralAnalGUI_OpeningFcn(hObject, eventdata, handles, varargin) %#ok<*INUSL>
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to FlFB_spectralAnalGUI (see VARARGIN)

% Choose default command line output for FlFB_spectralAnalGUI
handles.output = handles;

% Restore setting from previous GUI session
restoreList={'checkbox1','Value',0;'checkbox2','Value',0;'checkbox3','Value',0;
  'checkbox4','Value',0;'radiobutton8','Value',0;
  'radiobutton9','Value',0;'radiobutton1','Value',0;'radiobutton2','Value',0;
  'radiobutton3','Value',0;'radiobutton4','Value',0;'radiobutton5','Value',0;
  'uipanel10','Title',0;'text6','String',0;'edit4','String',0;
  'checkbox14','Value',0};
guiRestoreFn('FlFB_spectralAnalGUI',handles,restoreList);

% Set options from FlFB function pars
[stat pars]=FlFB('GetPars');
if ~pars.specAnal_npulse
  set(handles.checkbox14,'Value',1)
else
  set(handles.edit4,'String',num2str(pars.specAnal_npulse))
end
set(handles.edit5,'String',num2str(pars.specAnal_nsimTrack))
set(handles.checkbox8,'Value',pars.specAnal_drivex)
set(handles.checkbox9,'Value',pars.specAnal_drivey)
set(handles.checkbox7,'Value',pars.specAnal_drivee)
set(handles.checkbox5,'Value',pars.specAnal_drivexp)
set(handles.checkbox6,'Value',pars.specAnal_driveyp)

% Update handles structure
guidata(hObject, handles);
  
% UIWAIT makes FlFB_spectralAnalGUI wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = FlFB_spectralAnalGUI_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on button press in pushbutton1.
function pushbutton1_Callback(hObject, eventdata, handles) %#ok<*DEFNU,*INUSD>
% hObject    handle to pushbutton1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
guiCloseFn('FlFB_spectralAnalGUI',handles);

% --- Refresh function -> get and display required data
function pushbutton2_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
persistent freq orbit sorbit ip freq_fb orbit_fb sorbit_fb ip_fb

% Was this called by pushing refresh button?
if get(handles.figure1,'CurrentObject') == hObject || isempty(freq)
  refreshPush=true;
else
  refreshPush=false;
end

% Set required data buffer length to use
if get(handles.checkbox14,'Value')
  newpars.specAnal_npulse=0;
end
FlFB('SetPars',newpars);

% Get data from FlFB and check status
if refreshPush
  [stat freq orbit sorbit ip] = FlFB('SpecAnal','GetData');
  if stat{1}~=1
    errordlg(sprintf('Error getting spectral data from Feedback function: %s',stat{2}),'FlFB Error')
    return
  end
end

% Set max pulses info
[stat pars]=FlFB('GetPars');
set(handles.text6,'String',sprintf('Npulses (Max %d):',pars.specAnal_npulseAvail))

% Get pars
[stat pars] = FlFB('GetPars');
% Fill data fragmentation box
set(handles.text9,'String',pars.specAnal_frag)

% Get file data if any
fileData=get(hObject,'UserData');

% Get required plane
if get(handles.radiobutton1,'Value')
  plane='x';
else
  plane='y';
end

% Get required data type
if get(handles.radiobutton3,'Value')
  dataType=1;
elseif get(handles.radiobutton4,'Value')
  dataType=2;
else
  dataType=3;
end

% Get required plots
curMeasDisp=false; fileMeasDisp=false;
curFbDisp=false; fileFbDisp=false;
if get(handles.checkbox1,'Value')
  curMeasDisp=true;
end
if get(handles.checkbox2,'Value')
  fileMeasDisp=true;
end
if get(handles.checkbox3,'Value')
  fileFbDisp=true;
end
if get(handles.checkbox4,'Value')
  curFbDisp=true;
end
doPSD=false;
if get(handles.radiobutton8,'Value')
  doPSD=true;
  wpl=['psd_' plane];
else
  wpl=['phi_' plane];
end
df=freq(3)-freq(2);
orbit_int=sqrt(cumsum(orbit.(wpl)(2:end).*df)).*1e9;
sorbit_int=sqrt(cumsum(sorbit.(wpl)(2:end).*df)).*1e9;
ip_int=sqrt(cumsum(ip.(wpl)(2:end).*df)).*1e9;
if ~isempty(fileData)
  df_file=fileData(dataType).freq(3)-fileData(dataType).freq(2);
  fileData_int(dataType).(wpl)=cumsum(fileData(dataType).(wpl)(2:end).*df_file);
end
if length(fileData)>3
  df_file_fb=fileData(dataType+3).freq(3)-fileData(dataType+3).freq(2);
  fileData_int(3+dataType).(wpl)=cumsum(fileData(3+dataType).(wpl)(2:end).*df_file_fb);
end
if refreshPush || isempty(freq_fb)
  [stat freq_fb orbit_fb sorbit_fb ip_fb] = FlFB('SpecAnal','FBTF');
  if stat{1}~=1
    errordlg(sprintf('Error getting spectral data from Feedback function: %s',stat{2}),'FlFB Error')
    return
  end
end
df_fb=freq_fb(3)-freq_fb(2);
orbit_fb_int=sqrt(cumsum(orbit_fb(2).(wpl)(2:end).*df_fb)).*1e9;
sorbit_fb_int=sqrt(cumsum(sorbit_fb(2).(wpl)(2:end).*df_fb)).*1e9;
ip_fb_int=sqrt(cumsum(ip_fb(2).(wpl)(2:end).*df_fb)).*1e9;
% Form TF's in dB
if doPSD
  tf_orbit=10*log10(orbit_fb(2).(wpl))-10*log10(orbit_fb(1).(wpl));
  tf_sorbit=10*log10(sorbit_fb(2).(wpl))-10*log10(sorbit_fb(1).(wpl));
  tf_ip=10*log10(ip_fb(2).(wpl))-10*log10(ip_fb(1).(wpl));
else
  tf_orbit=orbit_fb(2).(wpl)-orbit_fb(1).(wpl);
  tf_sorbit=sorbit_fb(2).(wpl)-sorbit_fb(1).(wpl);
  tf_ip=ip_fb(2).(wpl)-ip_fb(1).(wpl);
end

% Get data to plot
cla(handles.axes1)
cla(handles.axes2)
xpl1={}; ypl1={};
xpl2={}; ypl2={}; pf={}; col={};
if curMeasDisp
  xpl1{1}=freq(2:end);
  xpl2{1}=freq(2:end);
  if dataType==1
    ypl1{1}=orbit_int;
    ypl2{1}=orbit.(wpl)(2:end);
  elseif dataType==2
    ypl1{1}=sorbit_int;
    ypl2{1}=sorbit.(wpl)(2:end);
  else
    ypl1{1}=ip_int;
    ypl2{1}=ip.(wpl)(2:end);
  end
  pf{1}='loglog';
  col{1}='blue';
end
if fileMeasDisp
  xpl2{end+1}=fileData(dataType).freq(2:end);
  ypl2{end+1}=fileData(dataType).(wpl)(2:end);
  xpl1{end+1}=fileData(dataType).freq(2:end);
  ypl1{end+1}=fileData_int(dataType).(wpl);
  pf{end+1}='loglog';
  col{end+1}='black';
end
if curFbDisp
  xpl1{end+1}=freq_fb(2:end);
  xpl2{end+1}=freq_fb(2:end);
  if dataType==1
    ypl2{end+1}=tf_orbit.(plane)(2:end);
    ypl1{end+1}=orbit_fb_int;
  elseif dataType==2
    ypl2{end+1}=tf_sorbit.(plane)(2:end);
    ypl1{end+1}=sorbit_fb_int;
  else
    ypl2{end+1}=tf_ip.(plane)(2:end);
    ypl1{end+1}=ip_fb_int;
  end
  pf{end+1}='semilogx';
  col{end+1}='red';
end
if fileFbDisp
  xpl1{end+1}=fileData(dataType+3).freq(2:end);
  xpl2{end+1}=fileData(dataType+3).freq(2:end);
  ypl2{end+1}=fileData(dataType+3).(wpl)(2:end);
  ypl1{end+1}=fileData_int(dataType+3).(wpl);
  pf{end+1}='semilogx';
  col{end+1}='magenta';
end

% perform plotting
[ax,h1,h2]=plotyy(handles.axes1,xpl1{1},ypl1{1},xpl1{2},ypl1{2},'loglog');
set(h1,'Color',col{1}); set(h2,'Color',col{2});
if doPSD
  [ax,h1,h2]=plotyy(handles.axes2,xpl2{1},ypl2{1},xpl2{2},ypl2{2},pf{1},pf{2});
  set(h1,'Color',col{1}); set(h2,'Color',col{2});
else
  [ax,h1,h2]=plotyy(handles.axes2,xpl2{1},ypl2{1},xpl2{2},ypl2{2});
  set(h1,'Color',col{1}); set(h2,'Color',col{2});
end

% Display integrated noise data
f0=str2double(regexp(get(handles.uipanel10,'title'),'\d+\.?(\d+)?','match'));
[X I]=min(abs(freq-f0));
[X I_fb]=min(abs(freq_fb-f0));
if dataType==1
  set(handles.text13,'String',num2str(orbit_fb_int(I_fb-1)))
elseif dataType==2
  set(handles.text13,'String',num2str(sorbit_fb_int(I_fb-1)))
else
  set(handles.text13,'String',num2str(ip_fb_int(I_fb-1)))
end
if dataType==1
  set(handles.text11,'String',num2str(orbit_int(I-1)))
elseif dataType==2
  set(handles.text11,'String',num2str(sorbit_int(I-1)))
else
  set(handles.text11,'String',num2str(ip_int(I-1)))
end
if ~isempty(fileData)
  [X I]=min(abs(fileData(dataType).freq-f0));
  set(handles.text12,'String',num2str(fileData_int(dataType).(wpl)(I-1)))
  if length(fileData)>3
    [X I_fb]=min(abs(fileData(dataType+3).freq-f0));
    set(handles.text14,'String',num2str(fileData_int(dataType+3).(wpl)(I_fb-1)))
  else
    set(handles.text14,'String','---')
  end
else
  set(handles.text12,'String','---')
  set(handles.text14,'String','---')
end
  
% Write save file data to save button UserData for saving
if refreshPush
  fileData_save(1).psd_x=orbit.psd_x;
  fileData_save(2).psd_x=sorbit.psd_x;
  fileData_save(3).psd_x=ip.psd_x;
  fileData_save(1).psd_y=orbit.psd_y;
  fileData_save(2).psd_y=sorbit.psd_y;
  fileData_save(3).psd_y=ip.psd_y;
  fileData_save(1).phase_x=orbit.phase_x;
  fileData_save(2).phase_x=sorbit.phase_x;
  fileData_save(3).phase_x=ip.phase_x;
  fileData_save(1).phase_y=orbit.phase_y;
  fileData_save(2).phase_y=sorbit.phase_y;
  fileData_save(3).phase_y=ip.phase_y;
  for ind=1:3
    fileData_save(ind).freq=freq;
  end
  fileData_save(4).psd_x=orbit_fb(2).psd_x;
  fileData_save(5).psd_x=sorbit_fb(2).psd_x;
  fileData_save(6).psd_x=ip_fb(2).psd_x;
  fileData_save(4).psd_y=orbit_fb(2).psd_y;
  fileData_save(5).psd_y=sorbit_fb(2).psd_y;
  fileData_save(6).psd_y=ip_fb(2).psd_y;
  fileData_save(4).phase_x=orbit_fb(2).phase_x;
  fileData_save(5).phase_x=sorbit_fb(2).phase_x;
  fileData_save(6).phase_x=ip_fb(2).phase_x;
  fileData_save(4).phase_y=orbit_fb(2).phase_y;
  fileData_save(5).phase_y=sorbit_fb(2).phase_y;
  fileData_save(6).phase_y=ip_fb(2).phase_y;
  for ind=4:6
    fileData_save(ind).freq=freq_fb;
  end
  set(handles.pushbutton5,'UserData',fileData_save)
end


% --- Executes on button press in pushbutton3.
function pushbutton3_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on slider movement.
function slider1_Callback(hObject, eventdata, handles)
% hObject    handle to slider1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider


% --- Executes during object creation, after setting all properties.
function slider1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to slider1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end


% --- Executes on button press in radiobutton3.
function radiobutton3_Callback(hObject, eventdata, handles)
% hObject    handle to radiobutton3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

set(handles.radiobutton4,'Value',false)
set(handles.radiobutton5,'Value',false)
set(hObject,'Value',true)
pushbutton2_Callback(handles.pushbutton2,[],handles);


% --- Executes on button press in radiobutton4.
function radiobutton4_Callback(hObject, eventdata, handles)
% hObject    handle to radiobutton4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

set(handles.radiobutton3,'Value',false)
set(handles.radiobutton5,'Value',false)
set(hObject,'Value',true)
pushbutton2_Callback(handles.pushbutton2,[],handles);


% --- Executes on button press in radiobutton5.
function radiobutton5_Callback(hObject, eventdata, handles)
% hObject    handle to radiobutton5 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

set(handles.radiobutton4,'Value',false)
set(handles.radiobutton3,'Value',false)
set(hObject,'Value',true)
pushbutton2_Callback(handles.pushbutton2,[],handles);


% --- Executes on button press in radiobutton1.
function radiobutton1_Callback(hObject, eventdata, handles)
% hObject    handle to radiobutton1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

if get(hObject,'Value')
  set(handles.radiobutton2,'Value',false)
else
  set(handles.radiobutton2,'Value',true)
end
pushbutton2_Callback(handles.pushbutton2,[],handles);


% --- Executes on button press in radiobutton2.
function radiobutton2_Callback(hObject, eventdata, handles)
% hObject    handle to radiobutton2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

if get(hObject,'Value')
  set(handles.radiobutton1,'Value',false)
else
  set(handles.radiobutton1,'Value',true)
end
pushbutton2_Callback(handles.pushbutton2,[],handles);


% --- Executes on selection change in popupmenu1.
function popupmenu1_Callback(hObject, eventdata, handles)
% hObject    handle to popupmenu1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns popupmenu1 contents as cell array
%        contents{get(hObject,'Value')} returns selected item from popupmenu1


% --- Executes during object creation, after setting all properties.
function popupmenu1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to popupmenu1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit1_Callback(hObject, eventdata, handles)
% hObject    handle to edit1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit1 as text
%        str2double(get(hObject,'String')) returns contents of edit1 as a double


% --- Executes during object creation, after setting all properties.
function edit1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in pushbutton5.
function pushbutton5_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton5 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

[fn pn]=uiputfile(sprintf('userData/FlFB_spectralAnal_%s.mat',datestr(now,30)),'Save Data File');
if fn
  saveData=get(hObject,'UserData');
  save(fullfile(pn,fn),saveData);
end

% --- Executes on button press in pushbutton6.
function pushbutton6_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton6 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

[fn pn]=uigetfile('userData/FlFB_spectralAnal*.mat','Get Data File');
if fn
  load(fullfile(pn,fn))
  set(handles.pushbutton2,'UserData',saveData)
  finfo=dir(fullfile(pn,fn));
  set(handles.text2,'String',sprintf('%s\n%s',fn,finfo.date));
end

% --- Executes on button press in radiobutton6.
function radiobutton6_Callback(hObject, eventdata, handles)
% hObject    handle to radiobutton6 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of radiobutton6


% --- Executes on button press in radiobutton7.
function radiobutton7_Callback(hObject, eventdata, handles)
% hObject    handle to radiobutton7 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of radiobutton7


% --- Executes on button press in checkbox1.
function checkbox1_Callback(hObject, eventdata, handles)
% hObject    handle to checkbox1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

if get(hObject,'Value')
  setDisplayCB(1,handles);
else
  set(hObject,'Value',true)
end


% --- Executes on button press in checkbox2.
function checkbox2_Callback(hObject, eventdata, handles)
% hObject    handle to checkbox2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

if get(hObject,'Value')
  if isempty(get(handles.pushbutton2,'UserData'))
    errordlg('No file data loaded','Selection Error');
    set(hObject,'Value',false)
    return
  end
  setDisplayCB(2,handles);
else
  set(hObject,'Value',true)
end


% --- Executes on button press in checkbox3.
function checkbox3_Callback(hObject, eventdata, handles)
% hObject    handle to checkbox3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

if get(hObject,'Value')
  if isempty(get(handles.pushbutton2,'UserData'))
    errordlg('No file data loaded','Selection Error');
    set(hObject,'Value',false)
    return
  end
  setDisplayCB(3,handles);
else
  set(hObject,'Value',true)
end


% --- Executes on button press in checkbox4.
function checkbox4_Callback(hObject, eventdata, handles)
% hObject    handle to checkbox4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

if get(hObject,'Value')
  setDisplayCB(4,handles);
else
  set(hObject,'Value',true)
end


function setDisplayCB(wCB,handles)
persistent lastCB
curCB=false(1,4);
for iCB=1:4
  curCB(iCB)=get(handles.(sprintf('checkbox%d',iCB)),'Value');
end
curCB(wCB)=false;
if isempty(lastCB)
  lastCB=find(curCB, 1, 'first' );
end
curCB(lastCB)=false;
for iCB=find(curCB)
  set(handles.(sprintf('checkbox%d',iCB)),'Value',false)
end
pushbutton2_Callback(handles.pushbutton2,[],handles);

% --- Executes on button press in checkbox10.
function checkbox10_Callback(hObject, eventdata, handles)
% hObject    handle to checkbox10 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of checkbox10


% --- Executes on button press in checkbox11.
function checkbox11_Callback(hObject, eventdata, handles)
% hObject    handle to checkbox11 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of checkbox11


% --- Executes on button press in checkbox5.
function checkbox5_Callback(hObject, eventdata, handles)
% hObject    handle to checkbox5 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

newpars.specAnal_drivexp=get(hObject,'Value');
FlFB('SetPars',newpars);


% --- Executes on button press in checkbox6.
function checkbox6_Callback(hObject, eventdata, handles)
% hObject    handle to checkbox6 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

newpars.specAnal_driveyp=get(hObject,'Value');
FlFB('SetPars',newpars);



% --- Executes on button press in checkbox7.
function checkbox7_Callback(hObject, eventdata, handles)
% hObject    handle to checkbox7 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

newpars.specAnal_drivee=get(hObject,'Value');
FlFB('SetPars',newpars);


% --- Executes on button press in checkbox8.
function checkbox8_Callback(hObject, eventdata, handles)
% hObject    handle to checkbox8 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

newpars.specAnal_drivex=get(hObject,'Value');
FlFB('SetPars',newpars);


% --- Executes on button press in checkbox9.
function checkbox9_Callback(hObject, eventdata, handles)
% hObject    handle to checkbox9 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

newpars.specAnal_drivey=get(hObject,'Value');
FlFB('SetPars',newpars);


% --- Executes on button press in checkbox12.
function checkbox12_Callback(hObject, eventdata, handles)
% hObject    handle to checkbox12 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of checkbox12


% --- Executes on button press in checkbox13.
function checkbox13_Callback(hObject, eventdata, handles)
% hObject    handle to checkbox13 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of checkbox13



function edit2_Callback(hObject, eventdata, handles)
% hObject    handle to edit2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit2 as text
%        str2double(get(hObject,'String')) returns contents of edit2 as a double


% --- Executes during object creation, after setting all properties.
function edit2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in popupmenu2.
function popupmenu2_Callback(hObject, eventdata, handles)
% hObject    handle to popupmenu2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns popupmenu2 contents as cell array
%        contents{get(hObject,'Value')} returns selected item from popupmenu2


% --- Executes during object creation, after setting all properties.
function popupmenu2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to popupmenu2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit3_Callback(hObject, eventdata, handles)
% hObject    handle to edit3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit3 as text
%        str2double(get(hObject,'String')) returns contents of edit3 as a double


% --- Executes during object creation, after setting all properties.
function edit3_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit4_Callback(hObject, eventdata, handles)
% hObject    handle to edit4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

val=round(str2double(get(hObject,'String')));
[stat pars]=FlFB('GetPars');
if isnan(val) || val<0
  set(hObject,'String',num2str(pars.specAnal_npulse))
else
  newpars.specAnal_npulse=val;
  FlFB('SetPars',newpars);
  pushbutton2_Callback(handles.pushbutton2,[],handles);
end


% --- Executes during object creation, after setting all properties.
function edit4_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit5_Callback(hObject, eventdata, handles)
% hObject    handle to edit5 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

val=round(str2double(get(hObject,'String')));
[stat pars]=FlFB('GetPars');
if isnan(val) || val<0 || val>100000
  set(hObject,'String',num2str(pars.specAnal_nsimTrack))
else
  newpars.specAnal_nsimTrack=val;
  FlFB('SetPars',newpars);
end


% --- Executes during object creation, after setting all properties.
function edit5_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit5 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in radiobutton8.
function radiobutton8_Callback(hObject, eventdata, handles)
% hObject    handle to radiobutton8 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

if get(hObject,'Value')
  set(handles.radiobutton9,'Value',false)
else
  set(handles.radiobutton9,'Value',true)
end
pushbutton2_Callback(handles.pushbutton2,[],handles);


% --- Executes on button press in radiobutton9.
function radiobutton9_Callback(hObject, eventdata, handles)
% hObject    handle to radiobutton9 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

if get(hObject,'Value')
  set(handles.radiobutton8,'Value',false)
else
  set(handles.radiobutton8,'Value',true)
end
pushbutton2_Callback(handles.pushbutton2,[],handles);


% --- Executes on button press in checkbox14.
function checkbox14_Callback(hObject, eventdata, handles)
% hObject    handle to checkbox14 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)



% --- Executes on mouse press over axes background.
function axes1_ButtonDownFcn(hObject, eventdata, handles)
% hObject    handle to axes1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
cp=get(gca,'CurrentPoint');
set(handles.uipanel10,'Title',sprintf('RMS Integrated Noise > %.2f Hz / nm',cp(1)))


% --- Executes when user attempts to close figure1.
function figure1_CloseRequestFcn(hObject, eventdata, handles)
% hObject    handle to figure1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

guiCloseFn('FlFB_spectralAnalGUI',handles);

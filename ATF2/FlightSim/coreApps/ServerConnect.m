function ServerConnect(srv)
% Connect a tcp server stream to a client
% srv = 'fl' | 'cas'
global FL
persistent errorstate srvsel
import java.io.*
import java.net.*

drawnow('expose');

% arg check
if ~ischar(srv)
  errordlg('Incorrect arguments','FloodlandServerConnect');
  error('Incorrect arguments');
end % arg check

switch lower(srv)
  case 'server'
    if errorstate
      errorstate=false;
      return;
    end % return if error
    writecom='writeBytes';
    srvsel='Server';
    sock=8844;
    portmax=8865;
  case 'as'
    if errorstate
      errorstate=false;
      return;
    end % return if error
    writecom='writeUTF';
    srvsel='AS';
    sock=8866;
    portmax=8885;
  case 'errorhandler'
    errordlg(lasterr,'ServerConnect Error'); %#ok<LERR>
    drawnow('expose');
    errorstate=true;
    LE=lasterror; %#ok<LERR>
    disp(LE.message); 
    if ~isempty(LE.stack)
      for istack=1:length(LE.stack)
        LE.stack(istack,1)
      end % for istack
    end % for length LE.stack
  otherwise
    errordlg(['Incorrect arguments: ',srv],'FloodlandServerConnect');
    error('Incorrect arguments');
end % switch srv

% --- Make connection
try
  streamsock=FL.(srvsel).servSocket.accept;
catch
  return
end % try/catch
output_stream=streamsock.getOutputStream;
FL.(srvsel).servSocket_output_stream=DataOutputStream(output_stream);
% Howdy stranger- have we met before?
if isLoopbackAddress(streamsock.getInetAddress) % force to be 127.0.0.1 if lo
  remotehost='/127.0.0.1';
else
  remotehost=char(streamsock.getInetAddress);
end % if lo
if isequal(srvsel,'AS')
  % check host entries and domain entries
  if ~ismember(regexprep(remotehost,'(.*)/(.*)$','$1'),FL.allowedTestIP) && ...
      ~ismember(regexprep(remotehost,'(.*)/(.*)$','$2'),FL.allowedTestIP) && ...
      ~ismember(regexprep(remotehost,'^(\S+)\..+/','$1'),FL.allowedTestIP) && ...
      ~ismember(regexprep(remotehost,'(.*)/(\d+\.\d+).+$','$2'),FL.allowedTestIP) && ...
      ~ismember(regexprep(remotehost,'(.*)/(\d+\.\d+\.+\d+).+$','$2'),FL.allowedTestIP)
    FL.(srvsel).servSocket_output_stream.(writecom)(['Unauthorised IP address: ',remotehost]);
    warning('Lucretia:Floodland:ServerConnect:unauthorisedConnectionAttempt',['Unauthorised connection attempt from: ',remotehost]);
    return
  end % if not on guest list
  userID=['user',num2str(find(ismember(FL.allowedTestIP,...
      regexprep(remotehost,'/(.+)','$1'))))];
  % If this userID already exists- assume trying to start multiple sessions
  % on same host and assign userIDs with assending leeters appended
  while isfield(FL.(srvsel),userID)
    if isnan(str2double(userID(end)))
      userID=[userID(1:end-1) char(double(userID(end))+1)];
    else
      userID=[userID 'a'];
    end % if last userID a char
  end % while userID exists, produce new one with next letter up
else % if 3rd party server
  userID=getNewUser(FL.(srvsel));
end % if AS
FL.(srvsel).(userID).remotehost=remotehost;
% send communication port #
[stat newport]=getPort(FL.(srvsel),sock,portmax);
if stat{1}~=1
  FL.(srvsel).servSocket_output_stream.(writecom)('No more ports available/\\END/\\');
  warning('Lucretia:Floodland:ServerConnect:portsExceeded','No more ports available');
%   FL.(srvsel)=resetServ(FL.(srvsel),sock);
  return
end % if error assigning new port
FL.(srvsel).(userID).comsock=newport;
FL.(srvsel).servSocket_output_stream.(writecom)(num2str(FL.(srvsel).(userID).comsock));
if strcmp(srvsel,'Server')
   FL.(srvsel).servSocket_output_stream.(writecom)('/\END/\');
end
% setup communication port
try
  FL.(srvsel).(userID).socket = ServerSocket(FL.(srvsel).(userID).comsock);
catch
  rethrow(lasterror)
end
try
  streamsock2=FL.(srvsel).(userID).socket.accept;
%   streamsock2.setSoTimeout(30000); % socket timeout / ms
  streamsock2.setReceiveBufferSize(FL.sendSize);
  streamsock2.setSendBufferSize(FL.sendSize);
  FL.(srvsel).(userID).comsock=streamsock2;
catch
  warning('Lucretia:Floodland:ServerConnect:comConErr','Error connecting communication socket');
  FL.(srvsel).servSocket_output_stream.writeUTF('Error connecting communication socket');
  % reset sockets
  try
    streamsock2.close;
  catch
  end
  delete(streamsock2);
  FL.(srvsel)=resetServ(FL.(srvsel),sock);
  FL.(srvsel)=rmfield(FL.(srvsel),userID);
  % delete any msg box
  if isfield(FL,'handlerinfo') && ishandle(FL.handlerinfo)
    delete(FL.handlerinfo);
  end % if handlerinfo box there, delete it
  return
end % try/catch

output_stream2=streamsock2.getOutputStream;
FL.(srvsel).(userID).socket_output_stream=DataOutputStream(output_stream2);
FL.(srvsel).(userID).socketOut=PrintWriter(output_stream2);
input_stream=streamsock2.getInputStream;
FL.(srvsel).(userID).socket_input_stream=DataInputStream(input_stream);
FL.(srvsel).(userID).socketIn=BufferedReader(InputStreamReader(input_stream));

% Close and reset Server socket
% FL.(srvsel)=resetServ(FL.(srvsel),sock);

% delete any msg box
if isfield(FL,'handlerinfo') && ishandle(FL.handlerinfo)
  delete(FL.handlerinfo);
end % if handlerinfo box there, delete it

function sockdata=resetServ(sockdata,sock)
import java.io.*
import java.net.*
sockdata.servSocket.close;
sockdata=rmfield(sockdata,{'servSocket' 'servSocket_output_stream'});
sockdata.servSocket = ServerSocket(sock);
sockdata.servSocket.setSoTimeout(10); % socket timeout / ms

function [stat newport]=getPort(sockdata,sock,portmax)
import java.io.*
import java.net.*
stat{1}=1;
fnames=fieldnames(sockdata);
socklist=[];
if ~isempty(fnames)
  for iname=1:length(fnames)
    if strcmp(fnames{iname}(1:4),'user') && isfield(sockdata.(fnames{iname}),'comsock')
      socklist(end+1)=sockdata.(fnames{iname}).comsock;
    end % if userID field and socket ID found
  end % for iname
end % if not empty fnames
if isempty(socklist)
  newport=sock+1;
else
  socklist=sort(socklist);
  np=setdiff(min(socklist):max(socklist),socklist);
  if isempty(np)
    if max(socklist)<=portmax
      newport=max(socklist)+1;
    else
      stat{1}=-1; stat{2}='max ports reached'; return;
    end % if max ports reached
  else
    newport=np(1);
  end % if empty np
end % if empty socklist

function newUser=getNewUser(sockdata)
import java.io.*
import java.net.*
fnames=fieldnames(sockdata);
userList=[];
for iname=1:length(fnames)
  if strcmp(fnames{iname}(1:4),'user')
    nu=str2double(fnames{iname}(5:end));
    if isreal(nu)
      userList(end+1)=nu;
    end % if nu double
  end % if user field
end % for iname
if isempty(userList)
  newUser=1;
else
  userList=sort(userList);
  nu=setdiff(1:max(userList),userList);
  if isempty(nu)
    newUser=max(nu)+1;
  else
    newUser=nu(1);
  end % if empty nu
end % if empty userList
newUser=['user' num2str(newUser)];
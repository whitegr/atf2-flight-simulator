function [stat ver] = versionCheck
% stat = versionCheck
% Client utility to check Client is running the same version as the Server,
% returns stat{1}<0 if check fails
global FL
stat{1}=1; ver=0;

if strcmp(FL.mode,'trusted')
  ver=FL.version;
else
  stat=sockrw('writechar','cas','versionCheck');
  if stat{1}~=1; return; end;
  [stat data]=sockrw('readchar','cas',10);
  if stat{1}~=1; return; end;
  [stat server_ver]=FloodlandAccessServer('decode',data);
  if stat{1}~=1; return; end;
  ver=server_ver;
  if ~isequal(FL.version,ver)
    stat{1}=-1; stat{2}='Version mismatch';
  end
end
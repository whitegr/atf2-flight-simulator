function [stat output] = FlDataStore(id,varargin)
% [stat output] = FlDataStore(id,varargin)
% --- Trusted App ---
% Store, retrieve, delete user defined data on trusted server
% Can only use string or numeric data types (no cells or structures)
% -------------------
% [stat output]=FlDataStore(id)
% stored data for tag id returned in FL.DataStore.(id) and str representation of data returned in output
% -------------------
% stat=FlDataStore(id,'dataName1',[1 2 3],'dataName2','myData',...)
% Store data under tag id, pass data name/data pairs, data must be char
% or double types only
% -------------------
% stat=FlDataStore(id,'delete')
% delete ALL data associated with id tag
% -------------------
% stat=FlDataStore(id,'delete','dataName1','dataName2',...)
% delete data name fields for id tag
global FL
stat{1}=1; output=[];

% If in non-FS simulated environment, just return
if ~isfield(FL,'mode') || ~isfield(FL,'SimMode') || FL.SimMode==2
  return
end

% --- If "test" or thread, then send command to "trusted"
if ~isequal(FL.mode,'trusted') || FL.isthread
  cas_cmd=['FlDataStore(''',id,''''];
  if nargin>1
    for iarg=1:length(varargin)
      if isstruct(varargin{iarg}) || iscell(varargin{iarg})
        stat{1}=-1;
        stat{2}='Cells or structures not supported';
        return
      end % error if struct or cell
      if ischar(varargin{iarg})
        cas_cmd=[cas_cmd,',''',varargin{iarg},''''];
      else
        % pass data as column array and tag to recover format
        [rw col]=size(varargin{iarg});
        cas_cmd=[cas_cmd,',reshape([',num2str(varargin{iarg}(:)',FL.comPrec),'],',num2str(rw),',',num2str(col),')'];
      end % if char or double
    end % for iarg
  end % if nargin>1
  cas_cmd=[cas_cmd,')'];
  stat=sockrw('writechar','cas',cas_cmd); if stat{1}<0; return; end;
  [stat, datain]=sockrw('readchar','cas',30); if stat{1}<0; return; end;
  [stat, datain]=FloodlandAccessServer('decode',datain{1});
  output=datain;
  if stat{1}~=1; return; end;
  if nargin==1
    data=textscan(datain,'%s','delimiter','$','BufSize',max(length(datain),100));
    idata=1;
    while idata<length(data{1})
      eval(['FL.DataStore.(id).(data{1}{idata})=',data{1}{idata+1},';']);
      idata=idata+2;
    end % while idata<length(data{1})
  end % if nargin==1
  return
end % if not "trusted"

if ~exist('id','var') || ~ischar(id)
  stat{1}=-1; stat{2}='Argument format error';
  return
end % if no id char given

% Deal with delete statements first
if nargin>1 && isequal(lower(varargin{1}),'delete')
  if nargin==2
    if exist(['userData/',id,'.mat'],'file')
      delete(['userData/',id,'.mat']);
    end % if data file exists
    if isfield(FL.DataStore,id)
      FL.DataStore=rmfield(FL.DataStore,id);
    end % remove data field if there
  else
    for idata=1:nargin-2
      if isfield(FL.DataStore.(id),varargin{idata+1})
        FL.DataStore.(id)=rmfield(FL.DataStore.(id),varargin{idata+1});
      end % if FL.DataStore.(id).varargin{idata+1}
    end % for idata
    eval([id,'=FL.DataStore.(id);'])
    d=dir(['userData/',id,'.mat']);
    if ~isempty(d)
      copyfile(['userData/',id,'.mat'],['userData/',id,'_',d(1).date,'.mat']);
    end
    save(['userData/',id,'.mat'],id)
    eval(['clear ',id])
  end % if nargin==2
  return
end % if delete command
if nargin>1 % unpack arguments
  iarg=1;
  while iarg<nargin-1
    if ~ischar(varargin{iarg})
      stat{1}=-1; stat{2}='Argument format error';
      return
    end
    idstr=varargin{iarg};
    % check max length of field
    if length(idstr)>namelengthmax
      stat{1}=-1; stat{2}=sprintf('Name: %s is greater than max allowed length (%d)',idstr,namelengthmax);
      return
    end
    iarg=iarg+1;
    FL.DataStore.(id).(idstr)=varargin{iarg};
    iarg=iarg+1;
  end % while iarg<nargin-1
  eval([id,'=FL.DataStore.(id);'])
  d=dir(['userData/',id,'.mat']);
  if ~isempty(d)
    copyfile(['userData/',id,'.mat'],['userData/',id,'_',datestr(d(1).datenum,FL.tstamp),'.mat']);
  end
  save(['userData/',id,'.mat'],id)
  eval(['clear ',id])
else % return id fields
  if ~isfield(FL,'DataStore') || ~isfield(FL.DataStore,id)
    if ~exist(['userData/',id,'.mat'],'file')
      stat{1}=-1; stat{2}='No data store entry with this ID';
      return
    else
      load(['userData/',id,'.mat'])
      eval(['FL.DataStore.(id)=',id,';']);
      eval(['clear ',id]);
    end % if saved file exists
  end % if no datastore field for this id
  try
    if isfield(FL.DataStore,id)
      fn=fieldnames(FL.DataStore.(id));
      if isempty(fn)
        stat{1}=0; stat{2}='No data';
        return
      else
        for ifn=1:length(fn)
          output=[output fn{ifn} '$'];
          if ischar(FL.DataStore.(id).(fn{ifn}))
            output=[output ' ''' FL.DataStore.(id).(fn{ifn}),''''];
          else
            [rw col]=size(FL.DataStore.(id).(fn{ifn}));
            output=[output ' reshape([' num2str(FL.DataStore.(id).(fn{ifn})(:)',FL.comPrec),'],',num2str(rw),',',num2str(col),')'];
          end % if ischar
          if ifn~=length(fn); output=[output '$']; end;
        end % for ifn
      end % if isempty(fn)
    else
      stat{1}=-1; stat{2}='No data store entry with this ID'; return;
    end % if FL.DataStore.id
  catch ME
    disp(ME.message)
  end
end % if nargin>1
function [stat res]=bpmres(bpmdata,L13,L12)
stat{1}=1;
OS=optimset('TolX',1e-25,'TolFun',1e-25,'Display','off','MaxFunEvals',10000,'MaxIter',1000,'Algorithm','sqp');
xfit=fminsearch(@(x) resfit(x,bpmdata.x1,bpmdata.x2,bpmdata.x3,bpmdata.y1,bpmdata.y2,bpmdata.y3),[L13 L12 0 1 0 0 0],OS);
ydiff=resfit(xfit,bpmdata.x1,bpmdata.x2,bpmdata.x3,bpmdata.y1,bpmdata.y2,bpmdata.y3,'getres');
res.y=sqrt(var(ydiff)/(3+4*L12^2/L13^2-2*L12/L13));
xfit=fminsearch(@(x) resfit(x,bpmdata.y1,bpmdata.y2,bpmdata.y3,bpmdata.x1,bpmdata.x2,bpmdata.x3),[L13 L12 0 1 0 0 0],OS);
ydiff=resfit(xfit,bpmdata.y1,bpmdata.y2,bpmdata.y3,bpmdata.x1,bpmdata.x2,bpmdata.x3,'getres');
res.x=sqrt(var(ydiff)/(3+4*L12^2/L13^2-2*L12/L13));

function chi2=resfit(x,x1,x2,x3,y1,y2,y3,cmd)

if exist('cmd','var') && strcmp(cmd,'getres')
  chi2=x(4)*y2-(x(3)+y1+((y3-y1)./x(1))*x(2)) - ...
    x(6).*(x1+x(2).*(x3-x1)./x(1)+x(5)) - ...
    x(8).*x2;
  return
end
chi2=sum(x(4)*y2-(x(3)+y1+((y3-y1)./x(1))*x(2)) - ...
    x(6).*(x1+x(2).*(x3-x1)./x(1)+x(5)) - ...
    x(8).*x2);
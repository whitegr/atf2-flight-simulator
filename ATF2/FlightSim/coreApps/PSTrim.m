function [stat,I] = PSTrim( pslist, onlineCheck )
% PSTrim Set PS actual values to desired values
% Floodland Implementation - write to EPICS CA if 'onlineCheck' set to true
%
% stat: Lucretia status reply
% I   : array of before/after PS currents [2,length(pslist)]
%       format: [Ibefore(1) ... Ibefore(end); ...
%                Iafter(1)  ... Iafter(end)]

% ------------------------------------------------------------------------------
% 4-Dec-2012, G. White
%    Set VSYSTEM set currents after setting EPICS currents
% 15-Feb-2010, M. Woodley
%    Enable bounds checking; allow optional return of present currents and set
%    currents
% ------------------------------------------------------------------------------

global PS FL BEAMLINE
persistent useca_ind
% Use OS caput call instead of lcaPutNoWait for these
if isfield(FL,'SimMode') && FL.SimMode
  useca={};
else
%   useca={'ZV11X' 'ZH10X' 'ZH1FF' 'ZV1FF'};
  useca={};
end
if ~isempty(useca) && isempty(useca_ind)
  for ica=1:length(useca)
    useca_ind(ica)=findcells(BEAMLINE,'Name',useca{ica});
  end
end
stat{1}=1;
I=[];

if max(pslist)>length(PS) || any(pslist<=0)
  stat{1}=0;
  stat{2}='PS index out of range';
  return
end % ps range check

if ~exist('onlineCheck','var') || ~onlineCheck || (~isempty(FL) && isfield(FL,'SimMode') && FL.SimMode)
  for ips=pslist
    % loop over power supplies
    for count = 1:length(pslist) 
      psno = pslist(count) ;
      if isfield(PS(psno),'dAmpl')
        da=PS(psno).dAmpl*randn;
      else
        da=0;
      end % if PS.dAmpl
      if (PS(psno).Step == 0)
        PS(psno).Ampl = PS(psno).SetPt + da ;
      else
        nstep = round( (PS(psno).SetPt - ...
                PS(psno).Ampl        ) / ...
                PS(psno).Step            ) ;
        PS(psno).Ampl = PS(psno).Ampl + ...
              nstep * PS(psno).Step * (1+da);
      end % if step==0
    end % for count
  end % for ips
  return;
end % online check

% --- If "test", then send PSTrim command to "trusted"
if isfield(FL,'mode') && ~isequal(FL.mode,'trusted')
  cas_arg=zeros(1,length(pslist)*2);
  cas_arg(1:2:end)=pslist;
  cas_arg(2:2:end)=[PS(pslist).SetPt];
  cas_cmd=['pstrim: ',num2str(cas_arg,FL.comPrec)];
  stat=sockrw('writechar','cas',cas_cmd);
  if (stat{1}<0),return,end
  [stat,casout]=sockrw('readchar','cas',1000);
  if (stat{1}<0),return,end
  output=str2num(casout{1}); %#ok<ST2NM>
  if isempty(output)||isnan(output(1))||output(1)~=1
    stat{1}=-1;
    stat{2}=cell2mat(casout);
    return
  end % if stat=1 not returned
  I=reshape(output(2:end),2,[]);
  return
end % if not "trusted"

% --- If "trusted", then write PS to EPICS
% Get PS's in PV list
comstack={}; comstackVals=[]; cips=[]; vcmd={};
Inow=NaN(size(pslist));Inew=NaN(size(pslist));
for n=1:length(pslist)
  ips=pslist(n);
  % Check PS value validity
  if isnan(PS(ips).SetPt) || isinf(PS(ips).SetPt)
    error('Invalid PS SetPt for PS: %d',ips)
  end % if isnan or isinf
  hw=FL.HwInfo.PS(ips);
  if isempty(hw.pvname) || isequal(hw.conv,0) || isempty(hw.conv)
    warning('Lucretia:Floodland:PSTrim:noHwComp',['No hardware component, or conversion value set to zero for PS: ',num2str(ips)]);
    continue
  end % continue if nothing to do for this PS
  % Add command to stack
  if isfield(hw,'preCommand') && ~isempty(hw.preCommand) && ~isempty(hw.preCommand{2})
    comstack{end+1}=hw.preCommand{2}{1}; cips(end+1)=ips; %#ok<*AGROW>
    comstackVals(end+1)=hw.preCommand{2}{2};
  end % if precommand
  % Write out PS val
  % + Scale by conversion factor
  % -- Find if this PS has main and trim coils to deal with
  mainI=0;
  if length(hw.pvname{2})==2 % main {1} and trim {2} currents
    try
      mainI=abs(lcaGet(hw.pvname{1}{1},1));
    catch
      stat{1}=-1; stat{2}='Controls not available';
      return
    end
    if isnan(mainI) || ~isnumeric(mainI)
%       stat{1}=-1; stat{2}=['Bad main current for: ',hw.pvname{2}{1}]; return;
      warning('Lucretia:Floodland:PStrim:badMainI',['Bad main current for: ',hw.pvname{2}{1},', Setting to 0'])
      mainI=0;
    end % if bad val returned
  elseif length(hw.pvname{2})~=1
    stat{1}=-1; stat{2}='Bad PS structure'; return;
  end % if 2 pv's (main and trim)
  comstack{end+1}=hw.pvname{2}{end}; cips(end+1)=ips; % actually set the trim coil
  % Bounds check (flip lookup table sign if bipolar and only given positive
  % lookup values)
  if hw.unipolar && PS(ips).SetPt<0
    warning('Lucretia:Floodland:PSTrim:unipolarErr','Trying to set negative current on unipolar device, ignoring');
    PS(ips).SetPt=PS(ips).Ampl;
    comstack={comstack{1:end-1}}; cips=cips(1:end-1);
    continue
  end % if negative SetPt and unipolar device 
  if ~hw.unipolar && length(hw.conv)>1 && PS(ips).SetPt<0 && hw.conv(2,end)>0 && length(hw.pvname{2})==1 && ~(length(hw.pvname{2})==2)
    hw.conv=-hw.conv;
  end
  if ~hw.unipolar && length(hw.conv)>1 && PS(ips).SetPt>0 && hw.conv(2,end)<0 && length(hw.pvname{2})==1 && ~(length(hw.pvname{2})==2)
    hw.conv=-hw.conv;
  end
  % Check not trying to move beyond control system reported limits if exist
  if isfield(hw,'high') && isfield(hw,'low') && ~isempty(hw.high) && ~isempty(hw.low) && any(abs([hw.high hw.low])) && hw.low~=hw.high
    if PS(ips).SetPt>hw.high || PS(ips).SetPt<hw.low
      if (false)
        disp(['Desired set point exceeds control system limits for PS: ',num2str(ips)]) %#ok<UNRCH>
      else
        stat{1}=-1; stat{2}=['Desired set point exceeds control system limits for PS: ',num2str(ips)];
        return
      end
    end
  end
  % Get present current; compute new current
  Inow(n)=lcaGet(hw.pvname{1}{end},1);
  if strfind(BEAMLINE{PS(ips).Element(1)}.Class,'COR')
    if length(hw.pvname{2})==2
      if length(hw.conv)==1
        if isempty(hw.nt_ratio); hw.nt_ratio=1; end;
        Inew(n)=(PS(ips).SetPt/hw.conv)/hw.nt_ratio;
      else
        thetaMain=interp1(hw.conv(1,:),hw.conv(2,:),mainI,'linear');
        thetaTrim=PS(ips).SetPt;
        iMainPlusTrim=interp1(hw.conv(2,:),hw.conv(1,:),thetaMain+thetaTrim,'linear');
        Inew(n)=(iMainPlusTrim-mainI)/hw.nt_ratio;
      end % simple conv factor or lookup table?
    else
      if length(hw.conv)==1
        Inew(n)=PS(ips).SetPt/hw.conv;
      else
        Inew(n)=interp1(hw.conv(2,:),hw.conv(1,:),PS(ips).SetPt,'linear');
      end % simple conv factor or lookup table?
    end % if a bend trim
  else
    if length(hw.pvname{2})==2
      if length(hw.conv)==1
        Inew(n)=(PS(ips).SetPt*abs(BEAMLINE{PS(ips).Element(1)}.B(1))/hw.conv)/hw.nt_ratio;
      else
        bMain=interp1(hw.conv(1,:),hw.conv(2,:),mainI,'linear');
        bTrim=PS(ips).SetPt*abs(BEAMLINE{PS(ips).Element(1)}.B(1));
        iMainPlusTrim=interp1(hw.conv(2,:),hw.conv(1,:),bMain+bTrim,'linear');
        if isempty(hw.nt_ratio); hw.nt_ratio=1; end;
        Inew(n)=(iMainPlusTrim-mainI)/hw.nt_ratio;
      end % simple conv factor or lookup table?
      % If matching quad, code the polarity into the sign of the current to
      % send to the control system
    elseif strcmp(BEAMLINE{PS(ips).Element(1)}.Name(1:2),'QM')
      Inow=sign(lcaGet(hw.pvname{1}{1}));
      Inew(n)=sign(BEAMLINE{PS(ips).Element(1)}.B(1))*interp1(hw.conv(2,:),hw.conv(1,:),...
        PS(ips).SetPt*abs(BEAMLINE{PS(ips).Element(1)}.B(1)),'linear');
      % If need to change polarity, procedure is, turn off, set current,
      % turn on
      if sign(Inew(n)) ~= Inow
        lcaPut(hw.off{1},hw.off{2});
        lcaPut(hw.pvname{2}{1},Inew(n));
        lcaPut(hw.postCommand{2,1}{1},hw.postCommand{2,1}{2});
        lcaPut(hw.on{1},hw.on{2});
      end
    else
      if length(hw.conv)==1
        Inew(n)=(PS(ips).SetPt/hw.conv)*abs(BEAMLINE{PS(ips).Element(1)}.B(1));
      else
        Inew(n)=interp1(hw.conv(2,:),hw.conv(1,:),PS(ips).SetPt*abs(BEAMLINE{PS(ips).Element(1)}.B(1)),'linear');
      end % simple conv factor or lookup table?
    end
  end % if corrector
  comstackVals(end+1)=Inew(n);
  if isfield(hw,'vput') && ~isempty(hw.vput)
    vcmd{end+1}=hw.vput;
  else
    vcmd{end+1}='';
  end
  if isfield(hw,'postCommand') && ~isempty(hw.postCommand) && ~isempty(hw.postCommand{2})
    comstack{end+1}=hw.postCommand{2}{1}; cips(end+1)=ips;
    comstackVals(end+1)=hw.postCommand{2}{2};
  end % if postcommand
  if (isnan(comstackVals(end)))
    continue % a stopping point for debugging
  end
  if (isinf(comstackVals(end)))
    continue % a stopping point for debugging
  end
end % for n=1:length(ips)
if ~isempty(comstack)
  % Issue command(s) via channel access client
  % Check validity of values
  if any(isnan(comstackVals)) || any(isinf(comstackVals))
    stat{1}=-1;
    stat{2}='PS.SetPt invalid - probably out of lookup table range or conversion slope set to zero';
    idnan=find(isnan(comstackVals));
    for m=1:length(idnan)
      n=idnan(m);
      disp(sprintf('FL.HwInfo.PS(%d)(PV=%s): SetPt = NaN',pslist(n),comstack{n})) %#ok<DSPS>
    end
    idinf=find(isinf(comstackVals));
    for m=1:length(idinf)
      n=idinf(m);
      disp(sprintf('FL.HwInfo.PS(%d)(PV=%s): SetPt = Inf',pslist(n),comstack{n})) %#ok<DSPS>
    end
    return
  end % if any isnan | isinf
  try
    if ~isempty(useca)
      rmcom=[];
      for ica=1:length(useca)
        ib=useca_ind(ica);
        if length(ib)==1
          rmps=find(pslist==BEAMLINE{ib}.PS(1));
          if ~isempty(rmps)
            rmcom(end+1)=find(cips==pslist(rmps));
          end
        end
      end
      if ~isempty(rmcom)
        ca_comstack={comstack{ismember(1:length(comstack),rmcom)}}; %#ok<*CCAT1>
        ca_comstackVals=comstackVals(ismember(1:length(comstack),rmcom));
        comstack={comstack{~ismember(1:length(comstack),rmcom)}};
        comstackVals(ismember(1:length(comstackVals),rmcom))=[];
        for istack=1:length(ca_comstack)
          [~, result]=system(['caput ',ca_comstack{istack},' ',num2str(ca_comstackVals(istack),6) ' &']); %#ok<NASGU>
        end
      end
    end
    if ~isempty(comstack)
      for istack=1:length(comstack)
        lcaPut(comstack{istack},comstackVals(istack));
      end
    end
    % Optionally return old and new PS currents
    I=[Inow;Inew];
  catch ME
    stat{1}=-1; stat{2}=['Error setting PS: ',ME.message]; return;
  end % try/catch
  % Write Vsystem currents
  for icmd=1:length(vcmd)
    if ~isempty(vcmd{icmd})
      lcaPut('V2EPICS:r',comstackVals(icmd));
      lcaPut('V2EPICS:rWrite',vcmd{icmd});
      pause(0.1);
    end
  end
end % if still commands to send

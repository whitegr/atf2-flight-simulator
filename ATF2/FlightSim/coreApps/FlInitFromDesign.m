function stat=FlInitFromDesign()
% Put your calls to initialize from the design model here

global FL BEAMLINE

% innocent until proven guilty
stat={1};

% save design momentum ... use it if Ibh1r=0
FL.SimModel.Design.Momentum=FL.SimModel.Initial.Momentum;

% rerun & save nominal Twiss ... in case elements have been added to BEAMLINE
[stat,FL.SimModel.Twiss]=GetTwiss(1,length(BEAMLINE), ...
  FL.SimModel.Initial.x.Twiss,FL.SimModel.Initial.y.Twiss);

% save design Twiss in a separate place ... useful for computing BMAG, etc.
FL.SimModel.Design.Twiss=FL.SimModel.Twiss;

% extDispersion
disp('FlInitFromDesign: initialization for extDispersion')
fname='MakeGlobalDispersionKnobs'; % want to run this
if (exist(fname)==2)
  p=[];
else
  [iss,r]=system(sprintf('find . -name %s.m',fname)); % find it
  if (iss~=0)
    stat{1}=0;
    stat{2}=sprintf('FlInitFromDesign: could not find %s.m',fname);
    disp(stat{2})
    return
  end
  p=fileparts(r);
  if ~isdeployed; addpath(p); end;
end
eval(strcat('stat=',fname,';'))
if (~isempty(p)),rmpath(p),end

end

function [stat varargout] = FlFB(cmd,varargin)
% FLFB
%    Feedback operation and control
%
%  [stat pars] = FlFB('GetPars')
%    Get internal parameters
%
%  stat = FlFB('SetPars',newpars)
%    Set internal parameters
%
%  stat = FlFB('Init')
%    Initialise Feedback parameters
%
%  stat = FlFB('Run')
%    Run main feedback algorithm
%
%  stat = FlFB('CorRestore')
%    Restore all feedback correctors to their values at last initialise
%
%  [stat freq orbit sorbit ip] = FlFB('SpecAnal','GetData')
%    Read in data from BPM buffers and form PSD's etc for spectral analysis
%      freq=vector of frequencies / Hz
%      orbit=spectral data for RMS feedback BPM orbit
%      sorbit=as above but for FFS Sextupoles
%      ip=spectral data for IP beam position
%
%  [stat freq orbit sorbit ip] = FlFB('SpecAnal','FBTF')
%    Use tracking through current lattice to get spectral data (as above)
%    with and without simulated feedback.
%      Output args as above but of dimension 2, 1 = no FB 2= with FB
%
% ======================================
% Parameters:
%    pars.qual_abs=10e-3; (absolute position cut)
%        .qual_tmit=0.2;  (relative tmit cut)
%        .qual_minaccum=20; (min amount of data to accumulate before
%                            running)
%        .qual_numpulse=100; (number of bpm data pulses to buffer)
%        .ictmin=1e9; (min ICTDUMP to accept)
%        .active=false; (FB on / off)
%        .PI=[0.1 0]; (PI controller coefficients)
%        .setpt=zeros(1,length(pars.bpmname)); (BPM set points)
%        .corInitial=[PS(pars.corind).SetPt]; (Initial corrector settings)
%        .diagnostics=false; (Plot and display diagnostic info)
%        .diagnostics_ipfit=false; (Fit orbit to IP and display results)
%        .applyFB=[0 0 0]; (apply corrections to orbit, sext, IP feedback
%                           devices)
%        .weights=[nbpm*2]'; (Relative weights to apply to BPM readings
%                             [x;y])
%        .specAnal_npulse ; (Max number of pulses from data buffer to use
%                            when calculating fft-related data for specral
%                            analysis stuff, if ZERO then use max available = default)
%        .specAnal_npulseAvail ; (Number of machine pulses available for
%                                 use in last spectral analysis calculations)
%        .specAnal_frag ; (% fragmentation of data caused by bad or missing
%                          pulses, larger % causes worse estimates of
%                          frequency spectra)
%        .specAnal_nsimTrack ; (Number of pulses to generate simulated
%                               spectra from, more pulses makes for better
%                               simulation results but is slower)
%        .specAnal_simNoiseAmp=0.2 ; (amplitude of noise (in sigma units) to
%                                     apply to FBTF modeling)
%        .specAnal_drivex=true ; (drive x phase for FBTF modeling)
%        .specAnal_drivexp=true ; (drive x' phase for FBTF modeling)
%        .specAnal_drivey=true ; (drive y phase for FBTF modeling)
%        .specAnal_driveyp=true ; (drive y' phase for FBTF modeling)
%        .specAnal_drivee=true ; (drive E phase for FBTF modeling)

global FL INSTR PS BEAMLINE
persistent pars diagstore

stat{1}=1;

% Server use only
if isempty(strfind(FL.mode,'trusted'))
  stat{1}=-1;
  stat{2}='Only callable from Server';
  return
end

% Initialisation
if isempty(pars) && ~strcmpi(cmd,'init')
  pars=initPars;
  diagstore.xbpm=[]; diagstore.ybpm=[]; diagstore.cor=[];
end

% Process command
switch lower(cmd)
  case 'init'
    pars=initPars;
    applyFB('reset');
    dataCheck('reset');
  case 'getpars'
    varargout{1}=pars;
  case 'cal'
    calinfo = getIPData('cal');
    pars.waistLoc = calinfo(end);
  case 'resetcal'
    pars.waistLoc = 0;
    getIPData('resetcal');
  case 'setpars'
    if nargin==2
      newpars=varargin{1};
      if isstruct(newpars)
        pf=fields(newpars);
        for ipar=1:length(pf)
          if isstruct(newpars.(pf{ipar}))
            pf2=fields(newpars.(pf{ipar}));
            for ipar2=1:length(pf2)
              pars.(pf{ipar}).(pf2{ipar2})=newpars.(pf{ipar}).(pf2{ipar2});
            end
          else
            pars.(pf{ipar})=newpars.(pf{ipar});
          end
        end
      end
    end
    save userData/FlFB.mat pars
  case 'specanal'
    if nargin<2 || ~ischar(varargin{1})
      error('Need a second command string argument')
    end
    [stat pars varargout{1} varargout{2} varargout{3} varargout{4}]=specAnal(varargin{1},pars);
  case 'run'
    if INSTR{pars.ictinstr}.Data(3)<pars.ictmin
      stat{1}=0;
      stat{2}='ICTDUMP reading too low';
      return
    end
    
    % Get orbit feedback bpm data
    bpmdata=zeros(length(pars.bpminstr),length(INSTR{pars.bpminstr(1)}.Data));
    for ibpm=1:length(pars.bpminstr)
      bpmdata(ibpm,:)=INSTR{pars.bpminstr(ibpm)}.Data-INSTR{pars.bpminstr(ibpm)}.ref;
    end
    
    % Get Sext feedback bpm data
    sextbpmdata=zeros(length(pars.sextinstrind),length(INSTR{pars.sextinstrind(1)}.Data));
    for ibpm=1:length(pars.sextinstrind)
      sextbpmdata(ibpm,:)=INSTR{pars.sextinstrind(ibpm)}.Data;
    end
    
    % Update response matrix if any quad strengths in region of interest
    % have changed since last run
    if ~isequal(pars.lastB,arrayfun(@(x) BEAMLINE{x}.B(1),pars.bCheck))
      if ~FL.SimMode
        disp('FlFB:: Lattice has changed, generating new response matrix...')
      end
      pars = getRmat(pars);
      pars.lastB = arrayfun(@(x) BEAMLINE{x}.B(1),pars.bCheck);
    end
    
    % --- Orbit feedback
    
    % Data quality check
    if ~dataCheck(pars,bpmdata)
      stat{1}=0;
      stat{2}='BPM data quality check failed';
      return
    end
    
    % Calculate and apply feedback correction for this pulse
    C=applyFB(pars,bpmdata,sextbpmdata);
    
    % Corrector strengths / BPM readout stripcharts
    if pars.diagnostics
      if stat{1}~=1
        sprintf('FlFB:: diagnostics plot error: %s\n',stat{2})
      else
        diagstore.xbpm(end+1,:)=bpmdata(:,1);
        diagstore.ybpm(end+1,:)=bpmdata(:,2);
        diagstore.cor(end+1,:)=C;
        if length(diagstore.xbpm)>100
          diagstore.xbpm=diagstore.xbpm(2:end,:);
          diagstore.ybpm=diagstore.ybpm(2:end,:);
          diagstore.cor=diagstore.cor(2:end,:);
        end
        if ~isfield(FL,'Gui') || ~isfield(FL.Gui,'FlFB_diagnostics') || ~ishandle(FL.Gui.FlFB_diagnostics)
          FL.Gui.FlFB_diagnostics=figure;
        end
        subplot(3,1,1,'Parent',FL.Gui.FlFB_diagnostics), plot(diagstore.xbpm.*1e6), xlabel('X BPMs'), ylabel('BPM readings / um'), grid on, legend(pars.bpmname','Location','NorthWest')
        subplot(3,1,2,'Parent',FL.Gui.FlFB_diagnostics), plot(diagstore.ybpm.*1e6), xlabel('Y BPMs'), ylabel('BPM readings / um'), grid on, legend(pars.bpmname','Location','NorthWest')
        subplot(3,1,3,'Parent',FL.Gui.FlFB_diagnostics), plot(diagstore.cor.*1e3), xlabel('CORs'), ylabel('Corrector Strengths / mrad'), grid on, legend(pars.corname(pars.corps>0)','Location','NorthWest')
      end
    end
    % show IP position figure window and associated data - updates with all
    % future calls to getIPData
    if pars.diagnostics_ipfit
      % Open figure window if not already open
      if ~isfield(FL,'Gui') || ~isfield(FL.Gui,'IPfitData') || ~ishandle(FL.Gui.IPfitData.figure1)
        FL.Gui.IPfitData=IPfitData;
      end
    end
    
  case 'correstore'
    nps=0;
    for ips=find(pars.corps)
      nps=nps+1;
      PS(ips).SetPt=pars.corInitial(nps);
    end
    stat=PSTrim(pars.corps(find(pars.corps)),1);
  otherwise
    error('Unknown Command')
end

function pars = initPars
global BEAMLINE PS INSTR FL
disp('FlFB:: Initialising FB parameters...')

% Load previously saved if exists else set defaults
if FlFnCheck('FlFB') && FL.SimMode~=2
  load userData/FlFB.mat
else
  pars.applyFB=[0 0 0];
  pars.qual_abs=10e-3;
  pars.qual_tmit=0.2;
  pars.qual_minaccum=20;
  pars.qual_numpulse=100;
  pars.ictmin=1e9;
  pars.PI=[0.3 0.1];
  pars.sextbpmname={'MSF6FF' 'MSF5FF' 'MSD4FF' 'MSF1FF' 'MSD0FF'};
  pars.sextbpmind=cellfun(@(x) findcells(BEAMLINE,'Name',x),pars.sextbpmname);
  pars.sextinstrind=arrayfun(@(x) findcells(INSTR,'Index',x),pars.sextbpmind);
  pars.sextcorind=arrayfun(@(x) BEAMLINE{x}.Girder,pars.sextbpmind);
  pars.PI_sext=[0.3 0.1];
  pars.specAnal_npulse=0;
  pars.specAnal_simNoiseAmp=0.1;
  pars.specAnal_nsimTrack=1000;
  pars.specAnal_drivex=true;
  pars.specAnal_drivexp=true;
  pars.specAnal_drivey=true;
  pars.specAnal_driveyp=true;
  pars.specAnal_drivee=true;
end
% FB elements
pars.corname={'ZH2X' 'ZH10X' 'ZH1FF' 'ZV1X' 'ZV2X' 'ZV11X' 'ZV1FF' 'AQD0FF$x' 'AQD0FF$y'};
pars.bpmname={'MQF9X' 'MQD10BFF' 'MQF9BFF' 'MQD2X' 'MQF4X' 'MFB2FF' 'MFB1FF' 'IP'};
pars.weights=ones(length(pars.bpmname)*2,1);
pars.setpt=zeros(1,length(pars.bpmname));
pars.waistLoc=[0 0 0 0];
pars.ictind=findcells(BEAMLINE,'Name','ICTDUMP');
pars.ictinstr=findcells(INSTR,'Index',pars.ictind);

for icor=1:length(pars.corname)
  pars.corind(icor)=findcells(BEAMLINE,'Name',regexprep(pars.corname{icor},'\$(x|y)',''));
  if isfield(BEAMLINE{pars.corind(icor)},'PS')
    pars.corps(icor)=BEAMLINE{pars.corind(icor)}.PS;
  else
    pars.corps(icor)=0;
  end
end
pars.bpmind=[]; pars.bpminstr=[];
for ibpm=1:length(pars.bpmname)
  pars.bpmind(ibpm)=findcells(BEAMLINE,'Name',pars.bpmname{ibpm});
  instrid=findcells(INSTR,'Index',pars.bpmind(ibpm));
  pars.bpminstr(ibpm)=instrid;
  pars.bpmres(ibpm)=INSTR{pars.bpminstr(ibpm)}.Res(1);
  if length(INSTR{pars.bpminstr(ibpm)}.Res)>1
    pars.bpmres(length(pars.bpmname)+ibpm)=INSTR{pars.bpminstr(ibpm)}.Res(2);
  else
    pars.bpmres(length(pars.bpmname)+ibpm)=INSTR{pars.bpminstr(ibpm)}.Res(1);
  end
end
pars.bpmres(~pars.bpmres)=1;
pars.diagnostics=false;
pars.diagnostics_ipfit=false;
pars.active=false;
pars.corInitial=[PS(pars.corps(find(pars.corps))).SetPt]; 
% Quad B fields to check so response matrix can be re-formed when needed
pars.bCheck=[];
for iB=pars.corind(1):pars.bpmind(end)
  if strcmp(BEAMLINE{iB}.Class,'QUAD')
    pars.bCheck(end+1) = iB;
  end
end
pars.lastB = arrayfun(@(x) BEAMLINE{x}.B(1),pars.bCheck);
pars = getRmat(pars);

function pars = getRmat(pars)
global BEAMLINE
pars.R=zeros(length(pars.bpmname)*2,length(pars.corname));
pars.Rsext=zeros(length(pars.sextbpmname)*2,length(pars.corname));
% Response matrix correctors -> BPMs
for icor=1:length(pars.corname)
  for ibpm=1:length(pars.bpmname)
    if isfield(BEAMLINE{pars.corind(icor)},'Block')
      [stat RC]=RmatAtoB(BEAMLINE{pars.corind(icor)}.Block(1),BEAMLINE{pars.corind(icor)}.Block(end));
    else
      [stat RC]=RmatAtoB(pars.corind(icor),pars.corind(icor));
    end
    if stat{1}~=1; error('Error getting FB response matrix: %s',stat{2}); end;
    pars.RC{icor}=RC;
    if pars.bpmind(ibpm) > pars.corind(icor)
      [stat R]=RmatAtoB(pars.corind(icor),pars.bpmind(ibpm));
      if stat{1}~=1; error('Error getting FB response matrix: %s',stat{2}); end;
      if ~isempty(strfind(BEAMLINE{pars.corind(icor)}.Class,'COR'))
        Lcor=BEAMLINE{pars.corind(icor)}.L/2;
        Rcor=eye(6); Rcor(1,2)=Lcor; Rcor(3,4)=Lcor;
        R=R*Rcor;
      end
      if strcmp(pars.corname{icor}(1:2),'ZV')
        pars.R(ibpm,icor)=R(1,4);
        pars.R(ibpm+length(pars.bpmname),icor)=R(3,4);
      elseif strcmp(pars.corname{icor}(1:2),'ZH')
        pars.R(ibpm,icor)=R(1,2);
        pars.R(ibpm+length(pars.bpmname),icor)=R(3,2);
      elseif ~isempty(regexp(pars.corname{icor},'\$x','once')) % x mover
        pars.R(ibpm,icor)=R(1,2);
        pars.R(ibpm+length(pars.bpmname),icor)=R(3,2);
      else % y mover
        pars.R(ibpm,icor)=R(1,4);
        pars.R(ibpm+length(pars.bpmname),icor)=R(3,4);
      end
    end
  end
end
% Response matrix correctors -> Sext BPMs
for icor=1:length(pars.corind)
  for ibpm=1:length(pars.sextbpmind)
    if pars.corind(icor)<pars.sextbpmind(ibpm)
      [stat R]=RmatAtoB(pars.corind(icor),pars.sextbpmind(ibpm));
      if stat{1}~=1; error('Error getting FB response matrix: %s',stat{2}); end;
      if strcmp(pars.corname{icor}(1:2),'ZV')
        pars.Rsext(ibpm,icor)=R(1,4);
        pars.Rsext(ibpm+length(pars.sextbpmname),icor)=R(3,4);
      elseif strcmp(pars.corname{icor}(1:2),'ZH')
        pars.Rsext(ibpm,icor)=R(1,2);
        pars.Rsext(ibpm+length(pars.sextbpmname),icor)=R(3,2);
      elseif ~isempty(regexp(pars.corname{icor},'\$x','once')) % x mover
        pars.Rsext(ibpm,icor)=R(1,2);
        pars.Rsext(ibpm+length(pars.sextbpmname),icor)=R(3,2);
      else % y mover
        pars.Rsext(ibpm,icor)=R(1,4);
        pars.Rsext(ibpm+length(pars.sextbpmname),icor)=R(3,4);
      end
    end
  end
end

function resp = dataCheck(pars,data)
persistent bpmdata
resp=false;
if isequal(pars,'reset')
  bpmdata=[];
  return
end
% Check feedback switched on
if ~pars.active; return; end;
% Check for any large readings
if any(any(isnan(data))) || any(any(abs(data(:,1:2))>pars.qual_abs))
  return
end
datasize=size(bpmdata);
% Check for repeated readings
if ~isempty(bpmdata) && isequal(data(:,1),bpmdata)
  return
end
% Wait for enough data
if datasize(2)<pars.qual_minaccum
  bpmdata=[bpmdata data(:,1)];
  return
end
% Fill data and return true
if datasize(2)>=pars.qual_numpulse
  bpmdata=bpmdata(:,datasize(2)-pars.qual_numpulse+2:datasize(2));
end
bpmdata=[bpmdata data(:,3)];
resp=true;

function [stat pars varargout]=specAnal(cmd,pars)
global FL BEAMLINE PS GIRDER

% Index setup
stat{1}=1;

switch lower(cmd)
  case 'getdata'
    % Get bpm data from buffers
    [stat bpmdata]=FlHwUpdate('readbuffer'); if stat{1}~=1; error(stat{2}); end;
    sz=size(bpmdata);
    [stat bpmts]=FlHwUpdate('readtsbuffer',sz(1)); if stat{1}~=1; error(stat{2}); end;
    bpm_id=[pars.bpminstr pars.sextinstrind]; bpm_id=sort(bpm_id);
    sext_id=ismember(bpm_id,pars.sextinstrind);
    ip_id=ismember(bpm_id,0);
    % Restrict number of pulses?
    for ibpm=1:length(bpm_id)
      bdata(:,ibpm*3-2:ibpm*3)=bpmdata(:,bpm_id(ibpm)*3-1:bpm_id(ibpm)*3+1);
      bts(:,ibpm)=bpmts(:,bpm_id(ibpm)+1);
    end
    ictdata=bpmdata(:,pars.ictinstr*3+1);
    bpmsz=size(bdata);
    if pars.specAnal_npulse && pars.specAnal_npulse<bpmsz(1)
      bdata=bdata(1:pars.specAnal_npulse,:);
      bts=bts(1:pars.specAnal_npulse,:);
      bpmsz=size(bdata);
    end
    % Unpack and check data
    % - make bpm grid
    t=0:1/FL.accRate:max(arrayfun(@(x) etime(datevec(bts(end,x)),datevec(bts(1,x))),1:bpmsz(2)/3));
    xbpms=NaN(length(t),bpmsz(2)/3);
    ybpms=xbpms;
    % - put everything in sequential time bins for fft treatment, using
    % spline interpolation for missing data, but don't extend past any
    % region where more than 10 pulses are missing in time
    for ipulse=1:bpmsz(1)
      % Perform data cuts
      if ictdata(ipulse)<pars.ictmin || any(isnan(bdata(ipulse,:))) || any(any(abs(bdata(ipulse,1:3:end))>pars.qual_abs)) || any(any(abs(bdata(ipulse,2:3:end))>pars.qual_abs))
        continue
      end
      % find bin to put data into
      [~, I]=arrayfun(@(x) min(abs(t-x)),etime(datevec(bts(ipulse,:)),datevec(bts(1,:))));
      for ibpm=1:length(I)
        xbpms(I(ibpm),ibpm)=bdata(ipulse,ibpm*3-2);
        ybpms(I(ibpm),ibpm)=bdata(ipulse,ibpm*3-1);
      end
      if ipulse>1
        dts(ipulse,:)=arrayfun(@(x) etime(datevec(bts(ipulse,x)),datevec(lasttime(x))),1:length(bts(ipulse,:)));
        if any(dts(ipulse,:)>FL.accRate*10)
          xbpms=xbpms(1:max(lastI),:);
          ybpms=ybpms(1:max(lastI),:);
          t=t(1:max(lastI));
          break
        end
      end
      lastI=I;
      lasttime=bts(ipulse,:);
    end
    % Use spline interpolation on NaN elements
    pars.specAnal_frag=0;
    for ibpm=1:bpmsz(2)/3
      wnan=isnan(xbpms(:,ibpm));
      frag=100*sum(wnan)/length(wnan);
      if frag>pars.specAnal_frag
        pars.specAnal_frag=frag;
      end
      xbpms(wnan,ibpm)=interp1(t(~wnan),xbpms(~wnan,ibpm),t(wnan),'pchip');
      wnan=isnan(ybpms(:,ibpm));
      ybpms(wnan,ibpm)=interp1(t(~wnan),ybpms(~wnan,ibpm),t(wnan),'pchip');
    end
    pars.specAnal_npulseAvail=length(xbpms);
    % Data formats
    orbit.x=std(xbpms(:,~sext_id),[],2);
    orbit.y=std(ybpms(:,~sext_id),[],2);
    sorbit.x=std(xbpms(:,sext_id),[],2);
    sorbit.y=std(ybpms(:,sext_id),[],2);
    ip.x=xbpms(:,ip_id);
    ip.y=ybpms(:,ip_id);
    % Form fft related data
    Fs=FL.accRate;
    L=length(t);
    NFFT = 2^nextpow2(L);
    BW=1.5*Fs*L;
    xfft=fft(orbit.x,NFFT);
    yfft=fft(orbit.y,NFFT);
    orbit.psd_x = abs(xfft(2:NFFT/2+1)).^2/BW;
    orbit.psd_y = abs(yfft(2:NFFT/2+1)).^2/BW;
    orbit.phi_x = angle(xfft(2:NFFT/2+1));
    orbit.phi_y = angle(yfft(2:NFFT/2+1));
    xfft=fft(sorbit.x,NFFT);
    yfft=fft(sorbit.y,NFFT);
    sorbit.psd_x = abs(xfft(2:NFFT/2+1)).^2/BW;
    sorbit.psd_y = abs(yfft(2:NFFT/2+1)).^2/BW;
    sorbit.phi_x = angle(xfft(2:NFFT/2+1));
    sorbit.phi_y = angle(yfft(2:NFFT/2+1));
    xfft=fft(ip.x,NFFT);
    yfft=fft(ip.y,NFFT);
    ip.psd_x = abs(xfft(2:NFFT/2+1)).^2/BW;
    ip.psd_y = abs(yfft(2:NFFT/2+1)).^2/BW;
    ip.phi_x = angle(xfft(2:NFFT/2+1));
    ip.phi_y = angle(yfft(2:NFFT/2+1));
    f = Fs/2*linspace(0,1,NFFT/2+1);
    varargout{1}=f; varargout{2}=orbit; varargout{3}=sorbit; varargout{4}=ip;
  case 'fbtf'
    % --- Generate Transfer Function for feedback
    
    % stop updating occuring until done here
    FL.noUpdate=true;
    
    % Store FB state and reset FB persistents
    applyFB('store');
    applyFB('reset');
    
    % Make error-free lattice for tracking
    % Switch off correctors, skew quads, zero girders, zero offsets
    strack=FL.SimModel.extStart;
    etrack=FL.SimModel.ip_ind;
    soind=[findcells(BEAMLINE,'Class','XCOR',strack,etrack) findcells(BEAMLINE,'Class','YCOR',strack,etrack)];
    tind=findcells(BEAMLINE,'Tilt',[],strack,etrack);
    soind=[soind tind(cellfun(@(x) x.Tilt(1)~=0,{BEAMLINE{tind}}))']; %#ok<*CCAT1>
    for ind=soind
      if isfield(BEAMLINE{ind},'PS')
        PS(BEAMLINE{ind}.PS).Ampl=0;
      end
    end
    for igir=1:length(GIRDER)
      if isfield(GIRDER{igir},'MoverPos')
        GIRDER{igir}.MoverPos=[0 0 0];
      end
    end
    offEle=findcells(BEAMLINE,'Offset',[],strack,etrack);
    initOffset=cell(1,length(offEle));
    for iele=1:length(offEle)
      initOffset{iele}=BEAMLINE{offEle(iele)}.Offset;
      BEAMLINE{offEle(iele)}.Offset=BEAMLINE{offEle(iele)}.Offset.*0;
    end
    
    % Form beam at track start point
    Initial=FL.SimModel.Initial_IEX;
    Beam = MakeBeam6DGauss( Initial, 1, 5, 1 );
    gamma=Initial.Momentum/0.511e-3;
    gx=(1+Initial.x.Twiss.alpha^2)/Initial.x.Twiss.beta;
    gy=(1+Initial.y.Twiss.alpha^2)/Initial.y.Twiss.beta;
    sx=sqrt(Initial.x.Twiss.beta*Initial.x.NEmit/gamma);
    sxp=sqrt(gx*Initial.x.NEmit/gamma);
    sy=sqrt(Initial.y.Twiss.beta*Initial.y.NEmit/gamma);
    syp=sqrt(gy*Initial.y.NEmit/gamma);
    se=Initial.SigPUncorrel;
    
    % indexing
    bpm_id=[pars.bpmind pars.sextbpmind]; bpm_id=sort(bpm_id);
    sext_id=ismember(bpm_id,pars.sextbpmind);
    ip_id=ismember(bpm_id,findcells(BEAMLINE,'Name','IP'));
    
    % --- Get simulated noise spectra with and without FB
    amp=pars.specAnal_simNoiseAmp;
    bpmind=[];
    xbpms=zeros(pars.specAnal_nsimTrack,length(bpm_id)); xbpms={xbpms xbpms};
    ybpms=xbpms;
    for isFB=1:2
      for ipulse=1:pars.specAnal_nsimTrack
        Beam.Bunch.x(1:5)=0;
        if pars.specAnal_drivex
          Beam.Bunch.x(1)=randn*amp*sx;
        end
        if pars.specAnal_drivexp
          Beam.Bunch.x(2)=randn*amp*sxp;
        end
        if pars.specAnal_drivey
          Beam.Bunch.x(3)=randn*amp*sy;
        end
        if pars.specAnal_driveyp
          Beam.Bunch.x(4)=randn*amp*syp;
        end
        if pars.specAnal_drivee
          Beam.Bunch.x(6)=Beam.Bunch.x(6)+Beam.Bunch.x(6)*randn*amp*se;
        end
        [stat , ~, instdata]=TrackThru(strack,etrack,Beam,1,1,0);
        if stat{1}~=1; stat{2}=['Error tracking getting simulated FB TF''s: ' stat{2}]; return; end;
        if isempty(bpmind)
          bpmind=ismember([instdata{1}.Index],bpm_id(~ismember(bpm_id,ip_id)));
        end
        % Pull out BPMs of interest
        xbpms{isFB}(ipulse,~ismember(bpm_id,ip_id))=[instdata{1}(bpmind).x];
        ybpms{isFB}(ipulse,~ismember(bpm_id,ip_id))=[instdata{1}(bpmind).y];
        
        % Apply FB
        if isFB==2
          bpmdata(:,1)=xbpms{isFB}(ipulse,bpm_id(~ismember(bpm_id,sext_id)))';
          bpmdata(:,2)=ybpms{isFB}(ipulse,bpm_id(~ismember(bpm_id,sext_id)))';
          sextbpmdata(:,1)=xbpms{isFB}(ipulse,bpm_id(ismember(bpm_id,sext_id)))';
          sextbpmdata(:,2)=ybpms{isFB}(ipulse,bpm_id(ismember(bpm_id,sext_id)))';
          applyFB(pars,bpmdata,sextbpmdata);
        end
      end
    end
    % Form required data quantities
    Fs=FL.accRate;
    L=pars.specAnal_nsimTrack;
    NFFT = 2^nextpow2(L);
    BW=1.5*FS*L;
    f = Fs/2*linspace(0,1,NFFT/2+1);
    for isFB=1:2
      orbit.x=std(xbpms{isFB}(:,~sext_id),[],2);
      orbit.y=std(ybpms{isFB}(:,~sext_id),[],2);
      sorbit.x=std(xbpms{isFB}(:,sext_id),[],2);
      sorbit.y=std(ybpms{isFB}(:,sext_id),[],2);
      ip.x=xbpms{isFB}(:,ip_id);
      ip.y=ybpms{isFB}(:,ip_id);
      % Form fft related data
      xfft=fft(orbit.x,NFFT);
      yfft=fft(orbit.y,NFFT);
      orbit(isFB).psd_x = abs(xfft(2:NFFT/2+1)).^2/BW;
      orbit(isFB).psd_y = abs(yfft(2:NFFT/2+1)).^2/BW;
      orbit(isFB).phi_x = angle(xfft(2:NFFT/2+1));
      orbit(isFB).phi_y = angle(yfft(2:NFFT/2+1));
      xfft=fft(sorbit.x,NFFT);
      yfft=fft(sorbit.y,NFFT);
      sorbit(isFB).psd_x = abs(xfft(2:NFFT/2+1)).^2/BW;
      sorbit(isFB).psd_y = abs(yfft(2:NFFT/2+1)).^2/BW;
      sorbit(isFB).phi_x = angle(xfft(2:NFFT/2+1));
      sorbit(isFB).phi_y = angle(yfft(2:NFFT/2+1));
      xfft=fft(ip.x,NFFT);
      yfft=fft(ip.y,NFFT);
      ip(isFB).psd_x = abs(xfft(2:NFFT/2+1)).^2/BW;
      ip(isFB).psd_y = abs(yfft(2:NFFT/2+1)).^2/BW;
      ip(isFB).phi_x = angle(xfft(2:NFFT/2+1));
      ip(isFB).phi_y = angle(yfft(2:NFFT/2+1));
    end
    % form output arguments
    opsd.x
    varargout{1}=f;
    varargout{2}=orbit;
    varargout{3}=sorbit;
    varargout{4}=ip;
    
    % Put any offsets back - other settings will be restored on next
    % FlHwUpdate
    for iele=1:length(offEle)
      BEAMLINE{offEle(iele)}.Offset=initOffset{iele};
    end
    
    % restore FB persistents and allow updating to resume
    applyFB('restore');
    FL=rmfield(FL,'noUpdate');
end

% Apply feedbacks
function C=applyFB(pars,bpmdata,sextbpmdata)
global PS GIRDER BEAMLINE
persistent errlast sextbpmdatalast store

% Store/restore state
if isequal(pars,'store')
  store.errlast=errlast;
  store.sextbpmdatalast=sextbpmdatalast;
  return
elseif isequal(pars,'restore')
  errlast=store.errlast;
  sextbpmdatalast=store.sextbpmdatalast;
  return
end

% Initialisation
if isempty(errlast) || isequal(pars,'reset')
  errlast=[];
  sextbpmdatalast=[];
end
if isequal(pars,'reset'); return; end;
if isempty(errlast); errlast=zeros(length(pars.corps),1); end;

% If not wanting feedback for orbit or IP, force those err readings
% to be zero
if ~pars.applyFB(1)
  bpmdata(~ismember(pars.bpmname,'IP'),:)=0;
end
if ~pars.applyFB(3)
  bpmdata(ismember(pars.bpmname,'IP'),:)=0;
end

% Compute and apply feedback
err=lscov(pars.R,-[bpmdata(:,1); bpmdata(:,2)],pars.weights./(pars.bpmres'.^2));
C=[PS(pars.corps(find(pars.corps))).SetPt]'; %#ok<*FNDSB>
correction=pars.PI(1).*(err-errlast) + pars.PI(2).*errlast;
if max(abs(correction))>1e-7
  C=C + correction(pars.corps~=0);
  K=correction(~pars.corps);
  nps=0;
  for ips=find(pars.corps)
    nps=nps+1;
    PS(pars.corps(ips)).SetPt=C(nps);
  end
  stat=PSTrim(pars.corps(find(pars.corps)),1);
  if stat{1}~=1; fprintf('FlFB PSTrim error: %s\n',stat{2}); return; end;
  ngir=0; wgir=[];
  gmove=[];
  for igir=find(~pars.corps)
    ngir=ngir+1;
    if pars.corname{igir}(end)=='x'
      gmove(end+1)=K(ngir)/pars.RC{igir}(2,1);
      GIRDER{BEAMLINE{pars.corind(igir)-1}.Girder}.MoverSetPt(1)=GIRDER{BEAMLINE{pars.corind(igir)-1}.Girder}.MoverPos(1)-...
        gmove(end);
    else
      gmove(end+1)=K(ngir)/pars.RC{igir}(4,3);
      GIRDER{BEAMLINE{pars.corind(igir)-1}.Girder}.MoverSetPt(2)=GIRDER{BEAMLINE{pars.corind(igir)-1}.Girder}.MoverPos(2)-...
        gmove(end);
    end
    gdiff(igir)=sum(abs(GIRDER{BEAMLINE{pars.corind(igir)-1}.Girder}.MoverPos(1:2)-GIRDER{BEAMLINE{pars.corind(igir)-1}.Girder}.MoverSetPt(1:2)));
    wgir=[wgir BEAMLINE{pars.corind(igir)-1}.Girder];
  end
  if any(gdiff > 1e-6)
    stat=MoverTrim(wgir,1);
    if stat{1}~=1; fprintf('FlFB MoverTrim error: %s\n',stat{2}); return; end;
  end
  errlast=err;
end


% --- FFS Sextupole feedback
if pars.applyFB(2)
  if isempty(sextbpmdatalast); sextbpmdatalast=zeros(size(sextbpmdata)); end;
  % remove effect of this round of feedback from Sext BPM readout
  for ipl=1:2
    for icor=1:length(correction)
      for ibpm=1:length(pars.sextbpmind)
        if pars.corind(icor)<pars.sextbpmind(ibpm)
          if ipl==1
            sextbpmdata(ibpm,1)=sextbpmdata(ibpm,1)+pars.Rsext(ibpm,icor)*correction(icor);
          else
            sextbpmdata(ibpm,2)=sextbpmdata(ibpm,2)+pars.Rsext(length(pars.sextbpmind)+ibpm,icor)*correction(icor);
          end
        end
      end
    end
  end
  % Compute and apply sextupole feedback correction
  sextcorrection=pars.PI_sext(1).*(sextbpmdata-sextbpmdatalast) + pars.PI_sext(2).*sextbpmdatalast;
  for icor=1:length(pars.sextcorind)
    GIRDER{pars.sextcorind(icor)}.MoverSetPt(1:2)=GIRDER{pars.sextcorind(icor)}.MoverSetPt(1:2)+sextcorrection(icor,1:2);
  end
  stat=MoverTrim(pars.sextcorind,1);
  if stat{1}~=1
    if stat{1}~=1; fprintf('FlFB MoverTrim error: %s\n',stat{2}); return; end;
  end
  sextbpmdatalast=sextbpmdata;
end
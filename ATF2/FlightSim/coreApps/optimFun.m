function stop = optimFun(x, optimValues, state)
global FL
persistent lastIter
stop=false;
if isfield(FL,'optimMonitor')
  FL.optimMonitor.optimValues=optimValues;
  FL.optimMonitor.x=x;
  FL.optimMonitor.state=state;
  switch state
    case 'init'
      lastIter=0;
    case 'iter'
      if isfield(FL.optimMonitor,'pbh') && isfield(FL.optimMonitor,'maxIter') && optimValues.iteration~=lastIter
        progressbar(FL.optimMonitor.pbh,1/FL.optimMonitor.maxIter);
        if ~gui_active
          stop=true;
        end
      end
      lastIter=optimValues.iteration;
  end
end
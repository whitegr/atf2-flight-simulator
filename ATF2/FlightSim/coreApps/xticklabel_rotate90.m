function hText=xticklabel_rotate90(handle)
%XTICKLABEL_ROTATE90 - Rotate numeric Xtick labels by 90 degrees
%
% Syntax: xticklabel_rotate90(handle)
%
% Input:  handle to the axe (optional)
%
% Output:  none
%
% 
% Author: Denis Gilbert, Ph.D., physical oceanography
% Maurice Lamontagne Institute, Dept. of Fisheries and Oceans Canada
% email: gilbertd@dfo-mpo.gc.ca  Web: http://www.qc.dfo-mpo.gc.ca/iml/
% February 1998; Last revision: 24-Mar-2003

if(~exist('handle','var'))
  handle=gca();
end

XTick = get(handle,'XTick');
% Define the xtickLabels
xTickLabels = get(handle,'XTickLabel');

%Make sure XTick is a column vector
XTick = XTick(:);

%Set the Xtick locations and set XTicklabel to an empty string
set(handle,'XTick',XTick,'XTickLabel','')


% Determine the location of the labels based on the position
% of the xlabel
hxLabel = get(handle,'XLabel');  % Handle to xlabel

set(hxLabel,'Units','data');
xLabelPosition = get(hxLabel,'Position');
y = xLabelPosition(2);

%CODE below was modified following suggestions from Urs Schwarz
y=repmat(y,size(XTick,1),1);
% retrieve current axis' fontsize
fs = get(handle,'fontsize');

% Place the new xTickLabels by creating TEXT objects
hText = text(XTick, y, xTickLabels,'fontsize',fs,'fontweight','bold');

% Rotate the text objects by 90 degrees
set(hText,'Rotation',90,'HorizontalAlignment','right','Parent',handle)

%------------- END OF CODE --------------

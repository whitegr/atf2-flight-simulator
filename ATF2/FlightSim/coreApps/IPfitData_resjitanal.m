function varargout = IPfitData_resjitanal(varargin)
% IPFITDATA_RESJITANAL MATLAB code for IPfitData_resjitanal.fig
%      IPFITDATA_RESJITANAL, by itself, creates a new IPFITDATA_RESJITANAL or raises the existing
%      singleton*.
%
%      H = IPFITDATA_RESJITANAL returns the handle to a new IPFITDATA_RESJITANAL or the handle to
%      the existing singleton*.
%
%      IPFITDATA_RESJITANAL('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in IPFITDATA_RESJITANAL.M with the given input arguments.
%
%      IPFITDATA_RESJITANAL('Property','Value',...) creates a new IPFITDATA_RESJITANAL or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before IPfitData_resjitanal_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to IPfitData_resjitanal_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help IPfitData_resjitanal

% Last Modified by GUIDE v2.5 15-Feb-2011 16:14:13

% Begin initialization code - DO NOT EDIT

gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @IPfitData_resjitanal_OpeningFcn, ...
                   'gui_OutputFcn',  @IPfitData_resjitanal_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before IPfitData_resjitanal is made visible.
function IPfitData_resjitanal_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to IPfitData_resjitanal (see VARARGIN)
global INSTR BEAMLINE

% Choose default command line output for IPfitData_resjitanal
handles.output = handles;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes IPfitData_resjitanal wait for user response (see UIRESUME)
% uiwait(handles.figure1);

% Display BPM resolutions
res=INSTR{findcells(INSTR,'Index',findcells(BEAMLINE,'Name','IPBPMA'))}.Res;
if length(res)>1
  set(handles.text14,'String',num2str(res(1)*1e9))
  set(handles.text15,'String',num2str(res(2)*1e9))
else
  set(handles.text14,'String',num2str(res*1e9))
  set(handles.text15,'String',num2str(res*1e9))
end

% --- Outputs from this function are returned to the command line.
function varargout = IPfitData_resjitanal_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Calc resolutions
function pushbutton1_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global FL BEAMLINE INSTR
persistent blele instele

if isempty(blele)
  blele.preip=findcells(BEAMLINE,'Name','MPREIP');
  blele.ipa=findcells(BEAMLINE,'Name','IPBPMA');
  blele.ipb=findcells(BEAMLINE,'Name','IPBPMB');
  blele.ip=findcells(BEAMLINE,'Name','IP');
  instele.preip=findcells(INSTR,'Index',blele.preip);
  instele.ipa=findcells(INSTR,'Index',blele.ipa);
  instele.ipb=findcells(INSTR,'Index',blele.ipb);
  instele.ip=findcells(INSTR,'Index',blele.ip);
end

numdata=str2double(get(FL.Gui.IPfitData.edit1,'String'));
[stat bpmdata]=FlHwUpdate('readbuffer',numdata);

resdata.x1=bpmdata(:,instele.preip*3-1);
resdata.y1=bpmdata(:,instele.preip*3);
resdata.x2=bpmdata(:,instele.ipa*3-1);
resdata.y2=bpmdata(:,instele.ipa*3);
resdata.x3=bpmdata(:,instele.ipb*3-1);
resdata.y3=bpmdata(:,instele.ipb*3);
L13=BEAMLINE{blele.ipb}.S-BEAMLINE{blele.preip}.S;
L12=BEAMLINE{blele.ipa}.S-BEAMLINE{blele.preip}.S;
[stat res]=bpmres(resdata,L13,L12);
set(handles.text14,'String',num2str(res.x))
set(handles.text15,'String',num2str(res.y))
INSTR{instele.ipa}.Res=[res.x res.y];
INSTR{instele.ipa}.Res=[res.x res.y];
INSTR{instele.preip}.Res=[res.x res.y];
sipx=sqrt(res.x^2*(1-2*(L12/L13)+2*(L12^2/L13^2)));
sipy=sqrt(res.y^2*(1-2*(L12/L13)+2*(L12^2/L13^2)));
INSTR{instele.ip}.Res=[sipx sipy];


% --- Executes on button press in checkbox1.
function checkbox1_Callback(hObject, eventdata, handles)
% hObject    handle to checkbox1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of checkbox1



function edit1_Callback(hObject, eventdata, handles)
% hObject    handle to edit1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit1 as text
%        str2double(get(hObject,'String')) returns contents of edit1 as a double


% --- Executes during object creation, after setting all properties.
function edit1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes when user attempts to close figure1.
function figure1_CloseRequestFcn(hObject, eventdata, handles) %#ok<*INUSL,*DEFNU>
% hObject    handle to figure1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

guiCloseFn('IPfitData_resjitanal',handles);

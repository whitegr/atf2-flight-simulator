function [stat resp]=FlRemotePVGet(PV,timeout)
% [stat resp]=FlRemotePVGet(PV,timeout)
% Function for remote call of lcaGet
% PV = EPICS PV string
% timeout = timeout in s (=10 if not supplied)
global FL
stat{1}=1;
resp=[];
if strcmp(FL.mode,'trusted')
  try
    resp=lcaGet(PV);
  catch
    stat{1}=-1;
    stat{2}='PV get failed';
    return
  end
  if iscell(resp)
    try
      resp=cell2mat(resp);
    catch
      stat{1}=-1;
      stat{2}='Response cell and failed to convert with cell2mat';
    end
  end
else
  if ~exist('timeout','var')
    timeout=10;
  end
  sockrw('writechar','cas',sprintf('FlRemotePVGet(''%s'')',PV));
  [stat replyData]=sockrw('readchar','cas',timeout);
  if stat{1}~=1 || isempty(replyData); return; end;
  [stat resp]=FloodlandAccessServer('decode',replyData);
end
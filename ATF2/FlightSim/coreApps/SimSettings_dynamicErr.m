function varargout = SimSettings_dynamicErr(varargin)
% SIMSETTINGS_DYNAMICERR M-file for SimSettings_dynamicErr.fig
%      SIMSETTINGS_DYNAMICERR, by itself, creates a new SIMSETTINGS_DYNAMICERR or raises the existing
%      singleton*.
%
%      H = SIMSETTINGS_DYNAMICERR returns the handle to a new SIMSETTINGS_DYNAMICERR or the handle to
%      the existing singleton*.
%
%      SIMSETTINGS_DYNAMICERR('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in SIMSETTINGS_DYNAMICERR.M with the given input arguments.
%
%      SIMSETTINGS_DYNAMICERR('Property','Value',...) creates a new SIMSETTINGS_DYNAMICERR or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before SimSettings_dynamicErr_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to SimSettings_dynamicErr_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help SimSettings_dynamicErr

% Last Modified by GUIDE v2.5 08-Aug-2008 17:33:15

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @SimSettings_dynamicErr_OpeningFcn, ...
                   'gui_OutputFcn',  @SimSettings_dynamicErr_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before SimSettings_dynamicErr is made visible.
function SimSettings_dynamicErr_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to SimSettings_dynamicErr (see
% VARARGIN)
global FL

% Choose default command line output for SimSettings_dynamicErr
handles.output = handles;

% Update handles structure
guidata(hObject, handles);

% Temp align data structure
udat=get(handles.figure1,'UserData');
udat{1}=FL.SimModel.align;

% Update error fields from Model structure
% BPMs
bpmStr={['Stripline = ' num2str(FL.SimModel.align.bpm_sres)];
  ['Cavity = ' num2str(FL.SimModel.align.bpm_cres)];
  ['IP = ' num2str(FL.SimModel.align.bpm_ipres)];
  ['Button = ' num2str(FL.SimModel.align.bpm_bres)]};
set(handles.popupmenu2,'String',bpmStr);
set(handles.popupmenu2,'Value',1);
set(handles.edit1,'String',FL.SimModel.align.bpm_sres);
set(handles.popupmenu2,'UserData',[FL.SimModel.align.bpm_sres FL.SimModel.align.bpm_cres FL.SimModel.align.bpm_ipres FL.SimModel.align.bpm_bres]);
modelSET(handles,7,'dB_quad_dyn',1e5);
modelSET(handles,8,'dB_sext_dyn',1e5);
modelSET(handles,9,'dB_bend_dyn',1e5);
modelSET(handles,10,'dB_cor_dyn',1e5);
modelSET(handles,[3 20 21],'mover_step',1e6);
modelSET(handles,4,'mover_accuracy_x',1e6);
modelSET(handles,5,'mover_accuracy_y',1e6);
modelSET(handles,6,'mover_accuracy_tilt',1e6);
modelSET(handles,11,'incoming_x',1e6);
modelSET(handles,12,'incoming_xp',1e6);
modelSET(handles,13,'incoming_y',1e6);
modelSET(handles,14,'incoming_yp',1e6);
modelSET(handles,15,'incoming_e',1e4);
gmModels=get(handles.popupmenu1,'String');
if isfield(FL,'SimModel') && isfield(FL.SimModel,'align') && isfield(FL.SimModel.align,'gmModel')
  if any(ismember(gmModels,FL.SimModel.align.gmModel))
    set(handles.popupmenu1,'Value',find(ismember(gmModels,FL.SimModel.align.gmModel)));
  end % if find gmModel
end % if FL.SimModel stuff
modelSET(handles,16,'jitter_quad',1e9);
modelSET(handles,17,'jitter_sext',1e9);
modelSET(handles,18,'cor_psStep',1e6);
modelSET(handles,19,'cor_dAmpl',1e5);

udat{2}=figState(handles);

set(handles.figure1,'UserData',udat);

function modelSET(handles,field,str,conv)
global FL

if isfield(FL,'SimModel') && isfield(FL.SimModel,'align') && isfield(FL.SimModel.align,str)
  for ifield=1:length(field)
    set(handles.(['edit',num2str(field(ifield))]),'String',num2str(FL.SimModel.align.(str)(ifield)*conv));
  end % for ifield
else
  errordlg(['Failed to get Model field: ',str],'SimSettings_dynamicErr error');
end % if FL.SimModel.str

% --- Outputs from this function are returned to the command line.
function varargout = SimSettings_dynamicErr_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on button press in pushbutton1.
function pushbutton1_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
udat=get(handles.figure1,'UserData');
if ~isequal(figState(handles),udat{2})
  resp=questdlg('Settings have been changed but not saved','Settings Changed',...
    'Quit anyway','Save before quiting','Cancel','Cancel');
  switch resp
    case 'Quit anyway'
      guiCloseFn('SimSettings_dynamicErr',handles);
    case 'Save before quiting'
      stat=saveErrData(handles);
      if stat{1}~=1
        errordlg(stat{2},'Static Error Save Error');
      else
        guiCloseFn('SimSettings_dynamicErr',handles);
      end % if err
  end % switch resp
else
  guiCloseFn('SimSettings_dynamicErr',handles);
end % if settings changed

% --- Executes on button press in pushbutton2.
function pushbutton2_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
stat=saveErrData(handles);
if stat{1}~=1; errordlg(stat{2},'Static Error Save Error'); end;


function stat=saveErrData(handles)
global FL

stat=modelGET(handles,7,'dB_quad_dyn',1e5); if stat{1}~=1; return; end;
stat=modelGET(handles,8,'dB_sext_dyn',1e5); if stat{1}~=1; return; end;
stat=modelGET(handles,9,'dB_bend_dyn',1e5); if stat{1}~=1; return; end;
stat=modelGET(handles,10,'dB_cor_dyn',1e5); if stat{1}~=1; return; end;
stat=modelGET(handles,[3 20 21],'mover_step',1e6); if stat{1}~=1; return; end;
stat=modelGET(handles,4,'mover_accuracy_x',1e6); if stat{1}~=1; return; end;
stat=modelGET(handles,5,'mover_accuracy_y',1e6); if stat{1}~=1; return; end;
stat=modelGET(handles,6,'mover_accuracy_tilt',1e6); if stat{1}~=1; return; end;
stat=modelGET(handles,11,'incoming_x',1e6); if stat{1}~=1; return; end;
stat=modelGET(handles,12,'incoming_xp',1e6); if stat{1}~=1; return; end;
stat=modelGET(handles,13,'incoming_y',1e6); if stat{1}~=1; return; end;
stat=modelGET(handles,14,'incoming_yp',1e6); if stat{1}~=1; return; end;
stat=modelGET(handles,15,'incoming_e',1e4); if stat{1}~=1; return; end;
gmModels=get(handles.popupmenu1,'String');
if isfield(FL,'SimModel') && isfield(FL.SimModel,'align') && isfield(FL.SimModel.align,'gmModel')
  if any(ismember(gmModels,FL.SimModel.align.gmModel))
    FL.SimModel.align.gmModel=gmModels{get(handles.popupmenu1,'Value')};
  end % if find gmModel
end % if FL.SimModel stuff
stat=modelGET(handles,16,'jitter_quad',1e9); if stat{1}~=1; return; end;
stat=modelGET(handles,17,'jitter_sext',1e9); if stat{1}~=1; return; end;
stat=modelGET(handles,18,'cor_psStep',1e6); if stat{1}~=1; return; end;
stat=modelGET(handles,19,'cor_dAmpl',1e5); if stat{1}~=1; return; end;

% BPM resolutions
bpmres=get(handles.popupmenu2,'UserData');
FL.SimModel.align.bpm_sres=bpmres(1);
FL.SimModel.align.bpm_cres=bpmres(2);
FL.SimModel.align.bpm_ipres=bpmres(3);
FL.SimModel.align.bpm_bres=bpmres(4);

stat = SimApplyErrors('dynamic');
if stat{1}~=1; errordlg(stat{2},'Error saving model'); end;

% Store save state
udat=get(handles.figure1,'UserData');
udat{2}=figState(handles);
set(handles.figure1,'UserData',udat);

function stat=modelGET(handles,field,str,conv)
global FL

if isfield(FL,'SimModel') && isfield(FL.SimModel,'align') && isfield(FL.SimModel.align,str)
  for ifield=1:length(field)
    FL.SimModel.align.(str)(ifield)=str2double(get(handles.(['edit',num2str(field(ifield))]),'String'))/conv;
  end % for ifield
else
  stat{1}=-1; stat{2}=['Sim Model field not found: ',str];
end % if FL.SimModel.str

if any(isnan(FL.SimModel.align.(str))) || any(FL.SimModel.align.(str)<0)
  stat{1}=-1; stat{2}=[FL.SimModel.align.(field),' must be numeric and >0'];
else
  stat{1}=1;
end % check val

function edit16_Callback(hObject, eventdata, handles)
% hObject    handle to edit16 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit16 as text
%        str2double(get(hObject,'String')) returns contents of edit16 as a double


% --- Executes during object creation, after setting all properties.
function edit16_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit16 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in popupmenu1.
function popupmenu1_Callback(hObject, eventdata, handles)
% hObject    handle to popupmenu1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = get(hObject,'String') returns popupmenu1 contents as cell array
%        contents{get(hObject,'Value')} returns selected item from popupmenu1


% --- Executes during object creation, after setting all properties.
function popupmenu1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to popupmenu1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit17_Callback(hObject, eventdata, handles)
% hObject    handle to edit17 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit17 as text
%        str2double(get(hObject,'String')) returns contents of edit17 as a double


% --- Executes during object creation, after setting all properties.
function edit17_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit17 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit11_Callback(hObject, eventdata, handles)
% hObject    handle to edit11 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit11 as text
%        str2double(get(hObject,'String')) returns contents of edit11 as a double


% --- Executes during object creation, after setting all properties.
function edit11_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit11 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit12_Callback(hObject, eventdata, handles)
% hObject    handle to edit12 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit12 as text
%        str2double(get(hObject,'String')) returns contents of edit12 as a double


% --- Executes during object creation, after setting all properties.
function edit12_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit12 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit13_Callback(hObject, eventdata, handles)
% hObject    handle to edit13 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit13 as text
%        str2double(get(hObject,'String')) returns contents of edit13 as a double


% --- Executes during object creation, after setting all properties.
function edit13_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit13 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit14_Callback(hObject, eventdata, handles)
% hObject    handle to edit14 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit14 as text
%        str2double(get(hObject,'String')) returns contents of edit14 as a double


% --- Executes during object creation, after setting all properties.
function edit14_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit14 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit15_Callback(hObject, eventdata, handles)
% hObject    handle to edit15 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit15 as text
%        str2double(get(hObject,'String')) returns contents of edit15 as a double


% --- Executes during object creation, after setting all properties.
function edit15_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit15 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit7_Callback(hObject, eventdata, handles)
% hObject    handle to edit7 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit7 as text
%        str2double(get(hObject,'String')) returns contents of edit7 as a double


% --- Executes during object creation, after setting all properties.
function edit7_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit7 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit8_Callback(hObject, eventdata, handles)
% hObject    handle to edit8 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit8 as text
%        str2double(get(hObject,'String')) returns contents of edit8 as a double


% --- Executes during object creation, after setting all properties.
function edit8_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit8 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit9_Callback(hObject, eventdata, handles)
% hObject    handle to edit9 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit9 as text
%        str2double(get(hObject,'String')) returns contents of edit9 as a double


% --- Executes during object creation, after setting all properties.
function edit9_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit9 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit10_Callback(hObject, eventdata, handles)
% hObject    handle to edit10 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit10 as text
%        str2double(get(hObject,'String')) returns contents of edit10 as a double


% --- Executes during object creation, after setting all properties.
function edit10_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit10 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit3_Callback(hObject, eventdata, handles)
% hObject    handle to edit3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit3 as text
%        str2double(get(hObject,'String')) returns contents of edit3 as a double


% --- Executes during object creation, after setting all properties.
function edit3_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit4_Callback(hObject, eventdata, handles)
% hObject    handle to edit4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit4 as text
%        str2double(get(hObject,'String')) returns contents of edit4 as a double


% --- Executes during object creation, after setting all properties.
function edit4_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit5_Callback(hObject, eventdata, handles)
% hObject    handle to edit5 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit5 as text
%        str2double(get(hObject,'String')) returns contents of edit5 as a double


% --- Executes during object creation, after setting all properties.
function edit5_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit5 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit6_Callback(hObject, eventdata, handles)
% hObject    handle to edit6 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit6 as text
%        str2double(get(hObject,'String')) returns contents of edit6 as a double


% --- Executes during object creation, after setting all properties.
function edit6_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit6 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit1_Callback(hObject, eventdata, handles)
% hObject    handle to edit1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit1 as text
%        str2double(get(hObject,'String')) returns contents of edit1 as a double
if isnan(str2double(get(hObject,'String')))
  errordlg('Not a number!','BPM resolution setting error');
  return
end % if isnan edit
bpmVal=get(handles.popupmenu2,'Value');
data=get(handles.popupmenu2,'UserData');
data(bpmVal)=str2double(get(hObject,'String'));
names={'Stripline = ','Cavity = ','IP = ','Button = '};
bpmstr=get(handles.popupmenu2,'String');
bpmstr{bpmVal}=[names{bpmVal} num2str(data(bpmVal))];
set(handles.popupmenu2,'String',bpmstr);

% --- Executes during object creation, after setting all properties.
function edit1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit2_Callback(hObject, eventdata, handles)
% hObject    handle to edit2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit2 as text
%        str2double(get(hObject,'String')) returns contents of edit2 as a double


% --- Executes during object creation, after setting all properties.
function edit2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit18_Callback(hObject, eventdata, handles)
% hObject    handle to edit18 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit18 as text
%        str2double(get(hObject,'String')) returns contents of edit18 as a double


% --- Executes during object creation, after setting all properties.
function edit18_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit18 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit19_Callback(hObject, eventdata, handles)
% hObject    handle to edit19 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit19 as text
%        str2double(get(hObject,'String')) returns contents of edit19 as a double


% --- Executes during object creation, after setting all properties.
function edit19_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit19 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes when user attempts to close figure1.
function figure1_CloseRequestFcn(hObject, eventdata, handles)
% hObject    handle to figure1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

guiCloseFn('SimSettings_dynamicErr',handles);


% --- Executes on selection change in popupmenu2.
function popupmenu2_Callback(hObject, eventdata, handles)
% hObject    handle to popupmenu2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = get(hObject,'String') returns popupmenu2 contents as cell array
%        contents{get(hObject,'Value')} returns selected item from popupmenu2
data=get(hObject,'UserData');
if isnan(str2double(get(handles.edit1,'String')))
  errordlg('Invalid resolution entry','Dynamic error setting error');
  return
end % if isnan
set(handles.edit1,'String',num2str(data(get(hObject,'Value'))));

% --- Executes during object creation, after setting all properties.
function popupmenu2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to popupmenu2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% Get array of Value and String properties for this figure
function array = figState(handles)

hans=fieldnames(handles);
array=[];
for ihan=1:length(hans)
  if ~isequal(hans{ihan},'pushbutton1') && ~isequal(hans{ihan},'pushbutton2')
    try
      array=[array double(get(handles.(hans{ihan}),'String'))];
    catch
    end % try/catch
    try
      array=[array get(handles.(hans{ihan}),'Value')];
    catch
    end % try/catch
  end % if not pushbutton1 or 2 (exit or save button)
end % for ihan



function edit20_Callback(hObject, eventdata, handles)
% hObject    handle to edit20 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit20 as text
%        str2double(get(hObject,'String')) returns contents of edit20 as a double


% --- Executes during object creation, after setting all properties.
function edit20_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit20 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit21_Callback(hObject, eventdata, handles)
% hObject    handle to edit21 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit21 as text
%        str2double(get(hObject,'String')) returns contents of edit21 as a double


% --- Executes during object creation, after setting all properties.
function edit21_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit21 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function FlStart(testTrusted,simMode,trustedUser,trustedHost)
% FlStart(testTrusted,simMode,trustedUser,trustedHost)
% Startup Lucretia-Floodland for the ATF2 environment
% ---------------------------------
% testTrusted: 'test' | 'trusted' to run this FS instance in client or
% server mode ('testthread', 'trustedthread' should only be called by FS
% software)
% ---------------------------------
% simMode: 1 | 0 to indicate running in simulation mode or not
% ---------------------------------
% trustedUser / trustedHost : if running the client instance and the server
% is not running on the same computer (same host name), specify user name
% and host name of the FS server process here

% ------------------------------------------------------------------------------
% 15-Feb-2010, M. Woodley
%    If FL.Mode='test', get PS low/high limits from server at startup
% ------------------------------------------------------------------------------

global FL

% if running in compiled mode, move to Floodland directory first
if isdeployed
  cd ../..
end % if isdeployed

% Turn off opengl rendering- causes problems with subplot
%opengl neverselect

% Turn off non-important warnings
warning('off','MATLAB:Java:ConvertFromOpaque')
warning('off','MATLAB:illConditionedMatrix')
warning('off','MATLAB:lscov:RankDefDesignMat')

% Make globals visible in workspace
evalin('base','global BEAMLINE PS GIRDER INSTR FL');

% Get Floodland home and atf directories from startup script
FL.homeDir=pwd;
FL.atfDir=fullfile(pwd,'..');

% Select expt (ATF or ATF2)
FL.expt = 'ATF2';

% init timer
tic

% Check args
if nargin<1
  error('Must supply at least one input argument')
end % if no nargin
FL.isthread=false;
if ~isequal(testTrusted,'test') && ~isequal(testTrusted,'trusted')
  if ~isequal(lower(testTrusted),'testthread') && ~isequal(lower(testTrusted),'trustedthread')
    error('First argument must be ''test'', ''trusted'', ''testThread'' or ''trustedThread''');
  elseif strcmp(testTrusted(1:4),'test')
    testTrusted='test'; FL.isthread=true;
  else
    testTrusted='trusted'; FL.isthread=true;
  end % is testThread | trustedThread
elseif strcmp(testTrusted,'trusted') && ~exist('simMode','var') || (isdeployed && isnan(str2double(simMode)))
  error('must supply simMode (1 | 0)');
elseif nargin>2 && nargin~=4
  error('Must supply either 2 or 4 input arguments');
end % arg check

% Multi-Threaded environment settings
% If in a thread- need to initialise server connection first
if ~FL.isthread
  stat=FlThreads; % initialise thread processing function
  if stat{1}~=1; errordlg(stat{2},'FlThreads Init'); end;
end

% Set mode (provided if trusted- get from trusted if test)
FL.mode=lower(testTrusted);
if isdeployed && ~isempty(strfind(FL.mode,'trusted'))
  FL.SimMode=str2double(simMode);
elseif ~isempty(strfind(FL.mode,'trusted'))
  FL.SimMode=simMode;
end % if isdeployed

% lca settings
if ~isempty(strfind(FL.mode,'trusted')) && ~FL.SimMode
  lcaSetSeverityWarnLevel(4);
  lcaSetRetryCount(10);
  lcaSetTimeout(1);
end

% Load properties
if exist('props.mat','file')
  FL.props=load('props.mat');
end

% If server, get instance # and register running status
if strcmp(FL.mode,'trusted')
  FL.instance=getInstance;
  sysStat=system(sprintf('touch RUNNING_%d',FL.instance));
  if sysStat; error('System error generating RUNNING file (file system full?)'); end;
end

% Initialisation of timer functions
% WARNING- this loads in default or saved environment which may wipe out
% any declarations made prior to this next line!
Model=Floodland('init');

% ATF software must be installed or linked from home directory
if ~isempty(strfind(FL.expt,'ATF'))
  if ~exist(FL.atfDir,'dir')
    error('No ''ATF2'' directory found in home dir- install or link ATF2 software there');
  end % if ATF2 dir there
end % if ATF expt

% Set trusted user/hostname
if exist('trustedUser','var') && ischar(trustedUser) && ~isempty(trustedUser)
  FL.trusted.username=trustedUser;
else
  username_raw=regexprep(evalc('!whoami'),'.*\\','');
  FL.trusted.username=regexprep(username_raw,'\s','');
end % if trustedUser
if exist('trustedHost','var') && ischar(trustedHost) && ~isempty(trustedHost)
  FL.trusted.hostname=trustedHost;
else
  FL.trusted.hostname='localhost';
end % if trustedHost

% Simulation mode stuff
if strcmp(FL.mode,'trusted') && ~FL.isthread && FL.SimMode && FL.instance==1
  % Start IOC's and setup vals to match model
  % First check we are not trying to do this on ATF-local network
  if checkATFLOCAL
    uiwait(errordlg('Don''t try and run trusted server in sim mode on ATF-LOCAL!!','Sim Mode Start Error'))
    exit
  end
end % if sim mode

% Check can write to EPICS DB if trusted
if isequal(FL.mode,'trusted') && ~FL.SimMode
  if lcaGet('ATF:SIM_MODE')~=FL.SimMode
    errordlg('Mismatch between IOC SIM_MODE and Floodland, stopping!','Floodland Error');
    error('Mismatch between IOC SIM_MODE and Floodland, stopping!')
  end % if sim mode mismatch
  try
    lcaPut('ATF:SIM_MODE.PROC',1);
  catch
    errordlg('No write access to EPICS Database','Floodland-Trusted setup error');
    error('No write access to EPICS Database');
  end % try/catch
  % Load limits from databases
  FlAssignHW(FL.SimModel,'set_limits');
  % Load TMIT normalisation values if any saved
  loadTmits();
end % if trusted

% Start GUIs (and client socket if test)
if ~FL.isthread
  if strcmp(testTrusted,'trusted')
    FL.Gui.main = FlGui_trusted;
    if FL.instance==1
      % Autostart Access Server
%       FlGui_trusted('pushbutton2_Callback',FL.Gui.main.pushbutton2,[],FL.Gui.main);
%       if FL.SimMode
%         if ~isfield(FL.Gui,'as')
%           errordlg('Access Server Gui did not autostart as is required for Sim Mode','Floodland Startup');
%           error('Access Server Gui did not autostart as is required for Sim Mode')
%         end % if no as field in FL.Gui
%         set(FL.Gui.as.togglebutton1,'Value',1);
%         FAS_main('togglebutton1_Callback',FL.Gui.as.togglebutton1,[],FL.Gui.as);
%       end % if sim mode
      % Start EPICS Command Server
      if ~FL.SimMode
        FlECS('start');
      end
    else % non-main instance default settings
      set(FL.Gui.main.radiobutton3,'Value',1) % default to not saving lattice files
      set(FL.Gui.main.popupmenu1,'Value',1) % default to no auto save
    end
  elseif isequal(lower(testTrusted),'test')
    FL.Gui.main = FlGui_test('UserData',{Model,FL.SimBeam});
    sockrw('init-casclient');
    % Get sim mode status from server
    FL.SimMode=getSimMode;
    % Get version number of server and check it matches client
    [stat server_version]=versionCheck;
    if stat{1}~=1
      errordlg(['Mismatch in version numbers between server and client- Server Version=',server_version,...
        ' Client=',FL.version,' -- Continue at your own risk!'],'Version Check Error')
    end
    % Get PS limits from server
    [~,output]=FlHwUpdate('getPSlimits');
    for n=1:length(FL.HwInfo.PS)
      FL.HwInfo.PS(n).low=output(1,n);
      FL.HwInfo.PS(n).high=output(2,n);
    end
  end % test or trusted
elseif FL.isthread
  % Start CAS and Floodland client connections
  sockrw('init-casclient');
  sockrw('init-flclient');
  FL.SimMode=getSimMode;
  % define and start thread application timer
  FL.t_thread=timer('StartDelay',1,'Period',FL.Period_floodland,...
  'ExecutionMode','FixedRate','BusyMode','drop');
  FL.t_thread.TimerFcn = 'FlThreads';
  start(FL.t_thread);
end % if sim mode

% Do any initialization that requires the design model here
try
  FlInitFromDesign;
catch
  warndlg('FlStart: FlInitFromDesign failed','FlStart')
end

% Start Floodland main timer
FL.t_main=timer('StartDelay',1,'Period',FL.Period_floodland,...
  'ExecutionMode','FixedRate','BusyMode','drop');
FL.t_main.TimerFcn = 'Floodland(''run'')';
FL.t_main.StopFcn = 'Floodland(''stop'')';
FL.t_main.ErrorFcn = 'Floodland(''errortrap'')';
start(FL.t_main);

% If client, get user ID from server and display on GUI
% Also display version number on client and server GUIs
if ~strcmp(FL.mode,'trusted') && ~FL.isthread
  sockrw('writechar','cas','sockrw(''get_user'',''cas'')');
  [~, out]=sockrw('readchar','cas',20);
  [stat userID]=FloodlandAccessServer('decode',out);
  if stat{1}==1 && isfield(FL,'Gui') && isfield(FL.Gui,'main')
    set(FL.Gui.main.text2,'String',['Client (',userID,')']);
  end
elseif strcmp(FL.mode,'trusted') && FL.instance==1
  set(FL.Gui.main.text2,'String','Main Server')
elseif strcmp(FL.mode,'trusted')
  set(FL.Gui.main.text2,'String',sprintf('Server (instance #%d)',FL.instance))
end
if isfield(FL,'Gui') && isfield(FL.Gui,'main')
  set(FL.Gui.main.text1,'String',['ATF2 Flight Simulator (V.',FL.version,')'])
end

function resp=getSimMode
try
  stat=sockrw('writechar','cas','getSimMode');
  if stat{1}~=1; error(stat{2}); end;
  [stat ret]=sockrw('readchar','cas',5);
  if stat{1}~=1; error(stat{2}); end;
  resp=str2double(ret{1});
  if isnan(resp); error('Invalid response from server'); end;
catch ME
  uiwait(errordlg(ME.message,'Error getting sim mode from server'));
  exit
end

function resp=checkATFLOCAL
resp=true; %#ok<NASGU>
if isunix
  resp=~isempty(regexp(evalc('!/sbin/ifconfig'),'20\.10\.\d+\.\d+', 'once' ));
elseif ispc
  resp=~isempty(regexp(evalc('!ipconfig'),'20\.10\.\d+\.\d+', 'once' ));
else
  error('Unsupported OS')
end

function instance=getInstance
% Get list of running servers
servers=dir('RUNNING*');
% If none, just start as main server and done
if isempty(servers)
  instance=1;
  return
end
% look to see if any appear to be zombie (haven't written status for > 10
% mins )
dv=datevec([servers.datenum]);
zombie=arrayfun(@(x) etime(clock,dv(x,:)),1:length(servers))>600;
if any(zombie)
  for iz=find(zombie)
    if strcmp(servers(iz).name,'RUNNING_1') % need main server to be actually running
      errstr='There is a main server registered that appears to have hung, check on it, if it is no longer running delete ''RUNNING_1'' file';
      uiwait(errordlg(errstr))
      error(errstr)
    else % print a warning
      try
        delete(servers(iz).name)
      catch
        warndlg(sprintf('Server instance appears dead, check on it and delete ''%s'' file if no longer running',servers(iz).name))
      end
    end
  end
end
% Set instance as lowest available integer
instance=min(setdiff(1:1000,arrayfun(@(x) str2double(regexp(servers(x).name,'\d+','match')),1:length(servers))));

function loadTmits()
global BEAMLINE INSTR FL

if exist('userData/tmitConvVals.mat','file')
  load userData/tmitConvVals tmitConvInstrName tmitConv
  for instrInd=1:length(INSTR)
    if length(FL.HwInfo.INSTR(instrInd).conv)>=3
      tid=ismember(tmitConvInstrName,BEAMLINE{INSTR{instrInd}.Index}.Name);
      if sum(tid)==1
        FL.HwInfo.INSTR(instrInd).conv(3)=tmitConv(tid);
      end
    end
  end
  disp('Loaded TMIT normalisation values')
end

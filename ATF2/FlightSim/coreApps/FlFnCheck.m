function stat=FlFnCheck(fnName)
% Check to see if the corresponding mfile .mat file is older than m file
% (ie the m file has been updated since last status save)
% if so, delete the old status save file
% stat = 1 means ok, stat = 0 means status file newer than m file and has
% been deleted
% Also delete old if Floodland version has changed and this stored in file
% as the variable name 'version'
global FL

stat=1;
savefile=dir(fullfile('userData',sprintf('%s.mat',fnName)));
mfile=which(sprintf('%s.m',fnName)); mfile=dir(mfile);
if ~isempty(savefile) && etime(datevec(mfile.date),datevec(savefile.date))>0
  warning('Lucretia:Floodland:FlFnSave:deleteOldFile','%s File Updated, deleting old save file',fnName);
  delete(sprintf('userData/%s.mat',fnName));
  stat=0;
  return
end
try
  fe=whos('-file',sprintf('userData/%s',fnName));
catch
  stat=0;
  return
end
for ife=1:length(fe)
  if strcmp(fe(ife),'version')
    ld=load(sprintf('userData/%s',fnName),'version');
    if ~isequal(ld.version,FL.version)
      warning('Lucretia:Floodland:FlFnSave:deleteOldFile','%s File Updated, deleting old save file',fnName);
      delete(sprintf('userData/%s.mat',fnName));
      stat=0;
      return
    end
  end
end
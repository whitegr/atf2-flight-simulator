function ServerCommandHandler(opt,srv)
% Deal with incoming commands to Servers
% srv = cas | fl
global FL

drawnow('expose');
if exist('srv','var')
  switch lower(srv)
    case 'fl'
      srvsel='Server';
    case 'cas'
      srvsel='AS';
    otherwise
      errordlg('Incorrect arguments','FloodlandServerConnect');
      error('Incorrect arguments');
  end % switch srv
end
switch opt
  case 'init'
  case 'stop'
    % Kill all open sockets
    fnames=fieldnames(FL.(srvsel));
    for ifield=1:length(fnames)
      if strcmp(fnames{ifield}(1:4),'user')
        FL.(srvsel).(fnames{ifield}).socket.close;
        FL.(srvsel)=rmfield(FL.(srvsel),fnames{ifield});
      end % if user field
    end % fir ifield
  case 'run'
  % arg check
  if ~ischar(srv)
    errordlg('Incorrect arguments','FloodlandServerConnect');
    error('Incorrect arguments');
  end % arg check

  % Get user list
  [stat, userList]=sockrw('get_userList',srv);
  if stat{1}~=1; return; end;
    
  % --- read anything available from client(s)
  for iuser=1:length({userList{:,1}}) %#ok<CCAT>
    try
      sout=[];
      userID=userList{iuser,1};
      % Only run if there is a connection
      if ~isfield(FL.(srvsel),userID) || ~isfield(FL.(srvsel).(userID),'socket')
        return
      end % if no communication sockets to process
      if ~isfield(FL.(srvsel),userID)
        return
      end
      if ~isfield(FL.(srvsel),userID); return; end;
      stat=sockrw('set_user',srvsel,userID); if stat{1}~=1; error(stat{2}); end;
      [stat, output, remoteHost] = sockrw('readchar',srv); if stat{1}~=1; error(stat{2}); end;
      if ~isempty(output)
        sstr=[];
        for iout=1:length(output)
          if iout>1 && ~strcmp(sstr(end),';'); sstr=[sstr ';']; end;
          output{iout}=regexprep(output{iout},'#testmessage#','');
          % Log communication time
          if ~isfield(FL.(srvsel),userID) || ~isfield(FL.(srvsel).(userID),'lastCom')
            FL.(srvsel).(userID).lastCom=clock;
            FL.(srvsel).(userID).firstCom=FL.(srvsel).(userID).lastCom;
          else
            FL.(srvsel).(userID).lastCom=clock;
          end
          % continue if nothing to do
          if isempty(output{iout})
            continue
          end % if empty message
          % deal with a test message
          if regexp(output{iout},'^testcommand')
            sout='testresponse';
          % Prepare access request command for proccessing
          elseif ~isempty(regexp(output{iout},'^access-request', 'once' )) && isequal(lower(srv),'fl')
            req=regexp(output{iout},'\s(\S+)','tokens');
            [stat,sout]=FlServer('access-request',req);
          % Prepare access status command for proccessing
          elseif ~isempty(regexp(output{iout},'^access-status', 'once' )) && isequal(lower(srv),'fl')
            req=regexp(output{iout},'\s(\S+)','tokens');
            [stat,sout]=FlServer('access-status',req);
          % Prepare access release command for proccessing
          elseif regexp(output{iout},'^access-release')
            if ~isequal(lower(srv),'fl')
              error('access-request Not a supported CAS commad!')
            end % if ~fl
            req=regexp(output{iout},'\s(\S+)','tokens');
            [stat,sout]=FlServer('access-release',req);
          % deal with disconnect command
          elseif regexp(output{iout},'^disconnect')
            disp('Closing Communication Socket...')
            sockrw('close',srv);
            return
          % Get Simulation mode request
          elseif regexp(lower(output{iout}),'^getsimmode')
            sout=num2str(FL.SimMode);
          % Flush buffer on request
          elseif regexp(lower(output{iout}),'^flush')
	          sockrw('flush',srv);
          else
            % otherwise assume a command request
            if isequal(lower(srv),'cas')
              [stat,sout]=FloodlandAccessServer('command',output{iout},remoteHost);
            else
              [stat,sout]=FlServer('command',output{iout});
            end % cas or server command?
          end % if test message
          % --- Send response
          if stat{1}~=1 % report failure to execute command
            err_rep=stat{2};
            sstr=[sstr 'command error:'];
            if iscell(err_rep)
              if any(cellfun(@(x) ~ischar(x),err_rep)); continue; end;
            elseif ~ischar(err_rep)
              continue
            end
            sstr=[sstr err_rep];
            if stat{1}~=1; disp(['Server Error: ',stat{2}]); end;
          else % return given command as indicator of success if no other output
            if isempty(sout)
              sstr=[sstr output{iout}];
            elseif ~iscell(sout)
              if isnumeric(sout) || islogical(sout); sout=num2str(sout,FL.comPrec); end;
              sstr=[sstr sout];
            else
              for icell=1:length(sout)
                if isnumeric(sout{icell}); sout=num2str(sout{icell},FL.comPrec); end;
              end
              sstr=[sstr sout];
            end % if output from server
            if stat{1}<0; disp(['Server Error: ',stat{2}]); end;
          end % success?
        end % for iout
        sockrw('writechar',srv,sstr);
      end % if any output
    catch
      disp('ServerCommand Error:')
      LE=lasterror; %#ok<LERR>
      disp(LE.message); %#ok<LERR>
      if ~isempty(LE.stack)
        for istack=1:length(LE.stack)
          LE.stack(istack,1)
        end % for istack
      end % for length LE.stack
      % reset info box status
      if isfield(FL,'handlerinfo')
        FL=rmfield(FL,'handlerinfo');
      end % if FL.handlerinfo
    end % try/catch
  end % for iuser
  otherwise
    errordlg('Incorrect arguments in main function','ServerCommandHandler');
    error('Incorrect arguments in main function');
end % switch opt

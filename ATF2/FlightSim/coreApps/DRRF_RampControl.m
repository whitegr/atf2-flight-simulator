function stat=DRRF_RampControl(request)
%
% stat=DRRF_RampControl(request);
%
% Turn the DR RF ramp ON or OFF, or change the DR RF frequency in discrete,
% predefined steps
%
% INPUT
%
%   request = 'on' turns the ramp ON
%   request = 'off' turns ramp OFF
%   request = a single desired df setting (kHz) in the range -10 kHz to +10 kHz
%             (only integer values in the range -10:10 are accepted)
%
% OUTPUT
%
%   stat = standard Lucretia return status (1=success,0=failure)

debug=0; % echo lca commands, but don't execute them

% VSYSTEM channel names for DR RF ramp control
vchix={ ...
  'DB_RAMP::ramp:ramp:control.on.sw', ...  % ramp ON
  'DB_RAMP::ramp:ramp:control.off.sw', ... % ramp OFF
  'DB_RAMP::ramp:mi10:onoff.sw', ...       % ramp -10 kHz
  'DB_RAMP::ramp:mi9:onoff.sw', ...        % ramp  -9 kHz
  'DB_RAMP::ramp:mi8:onoff.sw', ...        % ramp  -8 kHz
  'DB_RAMP::ramp:mi7:onoff.sw', ...        % ramp  -7 kHz
  'DB_RAMP::ramp:mi6:onoff.sw', ...        % ramp  -6 kHz
  'DB_RAMP::ramp:mi5:onoff.sw', ...        % ramp  -5 kHz
  'DB_RAMP::ramp:mi4:onoff.sw', ...        % ramp  -4 kHz
  'DB_RAMP::ramp:mi3:onoff.sw', ...        % ramp  -3 kHz
  'DB_RAMP::ramp:mi2:onoff.sw', ...        % ramp  -2 kHz
  'DB_RAMP::ramp:mi1:onoff.sw', ...        % ramp  -1 kHz
  'DB_RAMP::ramp:pm0:onoff.sw', ...        % ramp   0 kHz
  'DB_RAMP::ramp:pl1:onoff.sw', ...        % ramp  +1 kHz
  'DB_RAMP::ramp:pl2:onoff.sw', ...        % ramp  +2 kHz
  'DB_RAMP::ramp:pl3:onoff.sw', ...        % ramp  +3 kHz
  'DB_RAMP::ramp:pl4:onoff.sw', ...        % ramp  +4 kHz
  'DB_RAMP::ramp:pl5:onoff.sw', ...        % ramp  +5 kHz
  'DB_RAMP::ramp:pl6:onoff.sw', ...        % ramp  +6 kHz
  'DB_RAMP::ramp:pl7:onoff.sw', ...        % ramp  +7 kHz
  'DB_RAMP::ramp:pl8:onoff.sw', ...        % ramp  +8 kHz
  'DB_RAMP::ramp:pl9:onoff.sw', ...        % ramp  +9 kHz
  'DB_RAMP::ramp:pl10:onoff.sw', ...       % ramp +10 kHz
}; % VSYSTEM channel names for DR RF ramp control
dFval=(-10:10)'; % allowed DR RF ramp settings
twait=3; % pause time (s) after lcaPut

switch lower(request)
  case 'on'
    n=1;
  case 'off'
    n=2;
  otherwise
    found=isnumeric(request);
    if (found),[found,id]=ismember(request,dFval);end
    if (~found)
      stat{1}=0;
      stat{2}='Invalid DR RF dF value';
      return
    else
      n=id+2;
    end
end
pvname='V2EPICS:bWrite';
pvval=vchix{n};
if (debug)
  disp(['lcaPut(''',pvname,''',''',pvval,''')']) %#ok<*UNRCH>
  stat{1}=1;
else
  try
    lcaPut(pvname,pvval)
    pause(twait)
    stat{1}=1;
  catch
    stat{1}=0;
    stat{2}='lcaPut failed';
  end
end

end

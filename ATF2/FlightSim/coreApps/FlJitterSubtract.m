classdef FlJitterSubtract < handle
  %FLJITTERSUBTRACT Subtract incoming beam jitter from orbit measurements
  %
  
  properties
    useINSTR % List of INSTR indexes to use in fits, if empty attempt to use all
    Nsvd=50; % Number of BPM readings to use to get SVD modes for future jitter subtraction
    Sx % S matrix
    Vx % V matrix
    Ux % U matrix
    Sy % S matrix
    Vy % V matrix
    Uy % U matrix
    Nmodes=5; % number of modes to subtract
    bpmToolINSTR; % select tag to use from FlBpmTool (empty=don't use, use useINSTR property instead)
    doWait=true; % Wait for new pulses (true) or use buffered ones (false)
    qCut=1e9; % Charge cut level (ICTDUMP)
    flyerSubtract=3; % N sigma
  end
  
  properties(Access=private)
    ictind
  end
  
  methods
    function set.bpmToolINSTR(obj,tag)
      global INSTR
      [~, idata]=FlBpmToolFn('getdata');
      % check for existence of tag
      if ~ismember(tag,idata.tagNames)
        error('%s not found in bpmTool tag name list',tag)
      end
      obj.useINSTR=intersect(find(idata.(tag).use),findcells(INSTR,'Class','MONI')); %#ok<MCSUP>
      obj.bpmToolINSTR=tag;
    end
  end
  
  methods
    function obj=FlJitterSubtract
      global BEAMLINE INSTR
      if isempty(BEAMLINE) || isempty(INSTR)
        error('No Lucretia BEAMLINE and/or INSTR global defined')
      end
      obj.useINSTR=findcells(INSTR,'Class','MONI');
      obj.ictind=findcells(INSTR,'Index',findcells(BEAMLINE,'Name','ICTDUMP'));
%       obj.ictind=findcells(INSTR,'Index',findcells(BEAMLINE,'Name','MQF1X'));
    end
    function getModes(obj)
      % getModes
      %   Get BPM data to form modes for future jitter subtraction
      if obj.doWait
        FlHwUpdate('wait',obj.Nsvd);
      end
      nd=0;
      iloop=0; firstcall=true;
      while nd<obj.Nsvd
        if ~firstcall
          FlHwUpdate('wait',obj.Nsvd-nd);
          [~, newdata]=FlHwUpdate('readbuffer',obj.Nsvd-nd);
        else
          [~, newdata]=FlHwUpdate('readbuffer',obj.Nsvd);
        end
        ictdata=newdata(:,1+obj.ictind*3);
        newdata(ictdata<obj.qCut,:)=[];
        sz=size(newdata);
        if firstcall
          bpmdata=newdata;
        else
          bpmdata(end+1:end+sz(1),:)=newdata;
        end
        sz=size(bpmdata); nd=sz(1);
        iloop=iloop+1;
        if iloop>50; error('No valid new data getting SVD data'); end;
        firstcall=false;
      end
        
      
      if isempty(bpmdata); error('No pulses passed charge cut'); end;
      % Choose INSTR
      if isempty(obj.useINSTR)
        datax=bpmdata(:,1+1:3:end);
        datay=bpmdata(:,1+2:3:end);
        nbpm=length(bpmdata)/3;
      else
        datax=bpmdata(:,1+obj.useINSTR.*3-2);
        datay=bpmdata(:,1+obj.useINSTR.*3-1);
        nbpm=length(obj.useINSTR);
      end
      datax=(datax-repmat(mean(datax),sz(1),1))/sqrt(obj.Nsvd*nbpm);
      datay=(datay-repmat(mean(datay),sz(1),1))/sqrt(obj.Nsvd*nbpm);
      % find SVD modes
      [U,S,V]=svd(datax);
      obj.Sx=S;
      obj.Vx=V;
      obj.Ux=U;
      [U,S,V]=svd(datay);
      obj.Sy=S;
      obj.Vy=V;
      obj.Uy=U;
    end
    function data=getSubData(obj,npulses,iele)
      % getSubData(npulses[,iele])
      %   Perform jitter subtraction on the next npulses
      %   Optionally supply iele- a BEAMLINE element number, only fit
      %   jitter up to this point in the BEAMLINE
      global INSTR
      if ~exist('npulses','var') || npulses<1
        error('Must supply number of pulses to take (npulses)')
      end
      if obj.doWait
        FlHwUpdate('wait',npulses);
      end
      nd=0;
      iloop=0; firstcall=true;
      while nd<npulses
        if ~firstcall
          FlHwUpdate('wait',npulses-nd);
          [~, newdata]=FlHwUpdate('readbuffer',npulses-nd);
        else
          [~, newdata]=FlHwUpdate('readbuffer',npulses);
        end
        ictdata=newdata(:,1+obj.ictind*3);
        newdata(ictdata<obj.qCut,:)=[];
        sz=size(newdata);
        if firstcall
          bpmdata=newdata;
        else
          bpmdata(end+1:end+sz(1),:)=newdata;
        end
        sz=size(bpmdata); nd=sz(1);
        iloop=iloop+1;
        if iloop>50; error('No valid new data'); end;
        firstcall=false;
      end
      % Choose INSTR
      if isempty(obj.useINSTR)
        datax=bpmdata(:,1+1:3:end);
        datay=bpmdata(:,1+2:3:end);
        nbpm=length(bpmdata)/3;
      else
        datax=bpmdata(:,1+obj.useINSTR.*3-2);
        datay=bpmdata(:,1+obj.useINSTR.*3-1);
        nbpm=length(obj.useINSTR);
      end
      origdatax=datax;origdatay=datay;
      rawdatax=datax;rawdatay=datay;
      % Subtract fitted jitter
      mdatax=mean(datax); mdatay=mean(datay);
      datax=(datax-repmat(mdatax,sz(1),1))./sqrt(obj.Nsvd*nbpm);
      datay=(datay-repmat(mdatay,sz(1),1))./sqrt(obj.Nsvd*nbpm);
      vx=obj.Vx; vy=obj.Vy;
      if exist('iele','var')
        instele=arrayfun(@(x) INSTR{x}.Index,obj.useINSTR);
        rmele=find(instele<iele);
        vx=vx([rmele nbpm+rmele],:); vy=vy([rmele nbpm+rmele],:);
        datax=datax(:,[rmele nbpm+rmele]);
        datay=datay(:,[rmele nbpm+rmele]);
      end
      for imode=1:obj.Nmodes
        modeAmpx=datax*vx(:,imode);
        modeAmpy=datay*vy(:,imode);
        for ipulse=1:sz(1)
          origdatax(ipulse,:)=origdatax(ipulse,:)-modeAmpx(ipulse).*sqrt(obj.Nsvd*nbpm).*obj.Vx(:,imode)';
          origdatay(ipulse,:)=origdatay(ipulse,:)-modeAmpy(ipulse).*sqrt(obj.Nsvd*nbpm).*obj.Vy(:,imode)';
        end
      end
      data.x=origdatax; data.x_uncor=rawdatax;
      data.y=origdatay; data.y_uncor=rawdatay;
      data.x_mean=mean(origdatax); data.x_uncor_mean=mean(rawdatax);
      data.y_mean=mean(origdatay); data.y_uncor_mean=mean(rawdatay);
      data.x_rms=std(origdatax); data.x_uncor_rms=std(rawdatax);
      data.y_rms=std(origdatay); data.y_uncor_rms=std(rawdatay);
      % Subtract flyer pulses
      for itry=1:5
        for id=1:length(data.x_mean)
          xrej=abs(data.x(:,id)-data.x_mean(id))>obj.flyerSubtract;
          yrej=abs(data.y(:,id)-data.y_mean(id))>obj.flyerSubtract;
          if any(xrej) && sum(xrej)<length(data.x(:,id))
            data.x(xrej,id)=[];
            data.x_mean=mean(data.x);
            data.x_rms=std(data.x);
          end
          if any(yrej) && sum(yrej)<length(data.y(:,id))
            data.y(yrej,id)=[];
            data.y_mean=mean(data.y);
            data.y_rms=std(data.y);
          end
        end
      end
    end
  end
  
end


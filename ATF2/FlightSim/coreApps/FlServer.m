function [stat, output] = FlServer(opt,cmd)
% [stat, output] = FlServer(opt,cmd)
% Floodland Server
% Issue externally requested commands and return response
global FL
% Function pragma for compiler (local scope functions)
%#function datastore bpmave bpmread amlget amlset

stat{1}=1; stat{2}=[];
output=[];

switch opt
  case 'command' % generic Floodland, Lucretia or Matlab command handler
    command=cmd;
    try
      data=[];
      try
        eval(['[stat,data]=',command,';']);
      catch
        if ~isempty(findstr(lasterr,'Too many output arguments')) %#ok<LERR>
          eval(['stat=',command,';']);
        else
          try
            data=evalin('base',command);
          catch
            rethrow(lasterror); %#ok<LERR>
          end
        end % if too many output args last time
      end %
      if ~iscell(stat) || ~isnumeric(stat{1})
        data =[];
        stat={1};
      end
      encodeData=[];
      if iscell(data) 
        for idata=1:length(data)
          if iscell(data{idata})
            for jdata=1:length(data{idata})
              encodeData=[encodeData num2str(data{idata}{jdata}(:)',FL.comPrec) ' '];
            end % for jdata
          else
            encodeData=[encodeData num2str(data{idata}(:)',FL.comPrec) ' '];
          end % if data{} is cell
        end % for idata
      else
        encodeData=num2str(data(:)',FL.comPrec);
      end % if iscell(data)
      [stat, output]=FloodlandAccessServer('encode',stat,encodeData); 
    catch
      stat{1}=-1; stat{2}=[command 'error: ' lasterr]; encodeData=[]; %#ok<LERR>
      [stat, output]=FloodlandAccessServer('encode',stat,encodeData); 
    end % try/catch
  case 'access-request' % PV access request
    request{1}=[];
    request{2}=[];
    request{3}=[];
    for icmd=1:length(cmd)
      reqid=str2double(regexp(cmd{icmd}{1},'\d+','match'));
      if isempty(reqid) || ~isnumeric(reqid)
        stat{1}=-1;
        stat{2}='Invalid access-request command arguments';
        return
      end % if bad reqid
      if findstr(cmd{icmd}{1},'GIRDER')
        request{1}=[request{1} reqid];
      elseif findstr(cmd{icmd}{1},'PS')
        request{2}=[request{2} reqid];
      elseif findstr(cmd{icmd}{1},'INSTR')
        request{3}=[request{3} reqid];
      else
        stat{1}=-1;
        stat{2}='Invalid access-request command arguments';
        return
      end % if request loop
    end % for icmd
    % Send request to trusted Floodland Computer
    % Output = request ID
    [stat output] = AccessRequest(request);
  case 'access-release'
    if exist('cmd','var') && ~isempty(cmd)
      for icmd=1:length(cmd)
        [stat output] = AccessRequest('release',cmd{icmd}{1});
        if stat{1}~=1; return; end;
      end % for icmd
    else
      [stat output] = AccessRequest('release');
    end % if reqID(s) given
  case 'access-status'
    for icmd=1:length(cmd)
      [stat output] = AccessRequest('status',cmd{icmd}{1});
      if stat{1}~=1; return; end;
    end % for icmd
  otherwise
    stat{1}=-1;
    stat{2}='Incorrect arguments in main function';
end % switch opt

% Commands to set/get PS, GIRDER, INSTR vals
function stat=amlset(varargin) %#ok<DEFNU>
global PS GIRDER
% Valid channel IDs:
channels={'GIRDER#NUM#:orientation:x_offset:design';
  'GIRDER#NUM#:orientation:y_offset:design';
  'GIRDER#NUM#:orientation:tilt:design';
  'PS#NUM#:design:num'};
iarg=1;
g_set=[]; ps_set=[];
while iarg<nargin
  if iarg+1>nargin || ~isnumeric(str2double(varargin{iarg+1}))
    error('Incorrect amlset argument(s)')
  end % if next arg not a number
  t=regexp(varargin{iarg},'(\d+):','tokens');
  if isempty(t)
    error('Incorrect amlset argument(s)')
  end % if empty t
  ichan=find(cellfun(@(x) ~isempty(x),regexp(regexprep(channels,'#NUM#',t{1}{1}),varargin{iarg})));
  if ichan==1
    if ~ismember(num2str(t{1}{1}),g_set); g_set=[g_set str2double(t{1}{1})]; end;
    if ~isnumeric(varargin{iarg+1}); stat{1}=-1; stat{2}='Argument error'; return; end;
    GIRDER{str2double(t{1}{1})}.MoverSetPt(1)=varargin{iarg+1};
  elseif ichan==2
    if ~ismember(num2str(t{1}{1}),g_set); g_set=[g_set str2double(t{1}{1})]; end;
    if ~isnumeric(varargin{iarg+1}); stat{1}=-1; stat{2}='Argument error'; return; end;
    GIRDER{str2double(t{1}{1})}.MoverSetPt(2)=varargin{iarg+1};
  elseif ichan==3
    if ~ismember(num2str(t{1}{1}),g_set); g_set=[g_set str2double(t{1}{1})]; end;
    if ~isnumeric(varargin{iarg+1}); stat{1}=-1; stat{2}='Argument error'; return; end;
    GIRDER{str2double(t{1}{1})}.MoverSetPt(3)=varargin{iarg+1};
  elseif ichan==4
    if ~ismember(str2double(t{1}{1}),ps_set); ps_set=[ps_set str2double(t{1}{1})]; end;
    if ~isnumeric(varargin{iarg+1}); stat{1}=-1; stat{2}='Argument error'; return; end;
    PS(str2double(t{1}{1})).SetPt=varargin{iarg+1};
  else
    error('Incorrect amlset argument(s)')
  end
  iarg=iarg+2;
end % while iarg<nargin
if ~isempty(g_set)
  stat=MoverTrim(g_set,1);
  if stat{1}~=1; error(['GIRDER move error in amlset: ',stat{2}]); end;
end % if g_set
if ~isempty(ps_set)
  stat=PSTrim(ps_set,1);
  if stat{1}~=1; error(['PS set error in amlset: ',stat{2}]); end;
end % if ps_set
  
function [stat output]=amlget(varargin) %#ok<DEFNU>
global PS GIRDER INSTR BEAMLINE FL
stat{1}=1;
% Valid channel IDs:
channels={'GIRDER#NUM#:orientation:x_offset:design';
  'GIRDER#NUM#:orientation:y_offset:design';
  'GIRDER#NUM#:orientation:tilt:design';
  'PS#NUM#:design:num'};
for i_instr=1:length(INSTR)
  channels{length(channels)+1,1}=BEAMLINE{INSTR{i_instr}.Index}.Name;
end % for i_instr

% Get requested readback values
iarg=1; output='amlget: ';
while iarg<=nargin
  t=regexp(varargin{iarg},'(\d+):','tokens');
  if ~isempty(t) && (~isempty(regexp(varargin{iarg},'^GIRDER', 'once' )) || ~isempty(regexp(varargin{iarg},'^PS', 'once' )))
    ichan=find(cellfun(@(x) ~isempty(x),regexp(regexprep(channels,'#NUM#',t{1}{1}),varargin{iarg})));
  else
    ichan=find(cellfun(@(x) ~isempty(x),regexp(channels,varargin{iarg})));
  end % find matching channel to request
  for ind=1:length(ichan)
    if ichan(ind)>4 % INSTR channel
      val=num2str(INSTR{ichan(ind)-4}.data,FL.comPrec);
    elseif ichan(ind)==1
      val=num2str(GIRDER{str2double(t{1}{1})}.MoverPos(1),FL.comPrec);
    elseif ichan(ind)==2
      val=num2str(GIRDER{str2double(t{1}{1})}.MoverPos(2),FL.comPrec);
    elseif ichan(ind)==3
      val=num2str(GIRDER{str2double(t{1}{1})}.MoverPos(3),FL.comPrec);
    elseif ichan(ind)==4
      val=num2str(PS(str2double(t{1}{1})).Ampl,FL.comPrec);
    else
      val='!UNKNOWN-CHANNEL!';
    end % if ichan
    if iarg==nargin && ind==length(ichan)
      term=[];
    else
      term=',';
    end % last entry?
    output=[output [varargin{iarg},' ',val,term]];
  end % for ichan ind
  iarg=iarg+1;
end % while iarg<nargin

function [stat output]=bpmread(list) %#ok<DEFNU>
% Pass bpm vals to client
global INSTR BEAMLINE
stat{1}=1;
if ~exist('list','var') || isempty(list)
  list=1:length(INSTR);
end % if empty list request

output=[];
for i_instr=list
  output=[output sprintf('bpmread: %s %.6e %.6e %.6e ',['INSTR',num2str(i_instr),'=',BEAMLINE{INSTR{i_instr}.Index}.Name],INSTR{i_instr}.Data(1:3))];
end % for i_instr

function [stat output]=bpmave(var,extend,list,cuts)
% Get bpm average processed data from trusted
global INSTR BEAMLINE
stat{1}=1;
if ~exist('extend','var') || isempty(extend)
  extend=false;
end % if no extend
if ~exist('list','var') || isempty(list)
  list=1:length(INSTR);
  bpms=[];
else
  bpms=list;
end % if no list
if ~exist('cuts','var')
  cuts=[];
end % if no cuts
output=[];
if isnumeric(var)
  [stat,data] = FlHwUpdate('bpmave',var,extend,cuts,bpms);
  if stat{1}<0; output=['bpmave error: ',stat{2}]; return; end;
  if stat{1}~=1
    output=sprintf('bpmave (warning: %s): ',stat{2});
  else
    output='bpmave: ';
  end % if stat{1}~=1
  idata=0;
  for i_instr=list
    idata=idata+1;
    output=[output sprintf('%s %.6e %.6e %.6e %.6e %.6e %.6e %d %d %d ',...
      ['INSTR',num2str(i_instr),'=',BEAMLINE{INSTR{i_instr}.Index}.Name],data{1}(:,idata))];
  end % for i_instr
elseif isequal(var,'readbuffer')
  try
  if ~isempty(cuts)
    [stat,data] = FlHwUpdate('readbuffer',cuts);
  else
    [stat,data] = FlHwUpdate('readbuffer');
  end % if cuts == N pulses requested
  if stat{1}<0; output=['buffersize error: ',stat{2}]; return; end;
  npulse=size(data);
  output='readbuffer:';
  for i_instr=list
    output=[output sprintf(' %s',['INSTR',num2str(i_instr),'=',BEAMLINE{INSTR{i_instr}.Index}.Name])];
    for i_pulse=1:npulse(1)
      output=[output sprintf(' %d %.6e %.6e %.6e',data(i_pulse,1),data(i_pulse,1+i_instr*3-2:i_instr*3))];
    end % for i_pulse
  end % for i_instr
  catch
    rethrow(lasterror)
  end
elseif isequal(var,'buffersize')
  [stat,data] = FlHwUpdate('buffersize');
  if stat{1}<0; output=['buffersize error: ',stat{2}]; return; end;
  output=['buffersize: ',num2str(data)];
elseif isequal(var,'flush')
  [stat,data] = FlHwUpdate('flush');
  if stat{1}<0; output=['bpmave flush error: ',stat{2}]; return; end;
  output=['flush: ',num2str(data)];
elseif isequal(var,'readerrbuffer')
  if ~isempty(cuts)
    [stat,data] = FlHwUpdate('readerrbuffer',cuts);
  else
    [stat,data] = FlHwUpdate('readerrbuffer');
  end % if cuts == N pulses requested
  output=num2str(data(:)',FL.comPrec);
elseif isequal(var,'readarrbuffer')
  if ~isempty(cuts)
    [stat,data] = FlHwUpdate('readarrbuffer',cuts);
  else
    [stat,data] = FlHwUpdate('readarrbuffer');
  end % if cuts == N pulses requested
  output=num2str(data(:)',FL.comPrec);
end % if isnumeric(var)

function output=BUpdate %#ok<DEFNU>
% Return list of updated B fields since last time user ran this, or whole B
% field list if the first
 % -- Send B field list (or changes)
global FL
[stat Bdata Bdim] = latticeStatus;
Bchanges=cellfun(@(x) isempty(x),Bdata);
output=[];
if any(Bchanges)
  for ib=find(Bchanges)
    output=[output num2str(Bdim(ib)),'='];
    for ibd=1:length(Bdata{ib})
      output=[output num2str(Bdata{ibd},FL.comPrec)];
      if ibd==length(Bdata{ib})
        output=[output ']'];
      else
        output=[output ','];
      end
    end
  end
end
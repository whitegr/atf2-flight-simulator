function HwInfo = FlAssignHW(Model,cmd)
% Allocate EPICS PV Names and associate with Lucretia elements and store other
% HW related info
% ------------------------------------------------------------------------------
% 11-May-2013, G. White
%    Swap lookup tables for SF5FF and SD4FF as magnets physically swapped
% 27-Nov-2012, M. Woodley
%    QF1FF is now a PEPII 4Q17 quadrupole; NOHW for BKX
% 08-Nov-2012, M. Woodley
%    Include INSTR.Type='ip' devices (IPBPMs) in INSTR hardware definitions
% 24-Oct-2012, M. Woodley
%    New BH1R energy fudge per ATF2 eLog: 812.801 amp = 1.269 GeV
% 18-Sep-2012, M. Woodley
%    Add support for 4 FF skew sextupoles
% 08-Mar-2011, M. Woodley
%    Disable mover "ATIPBPM" (associated with element "IPBPM")
% 21-Feb-2011, M. Woodley
%    Add bipolar SK1FF (see I2B_ATF2_SEXT)
% 06-Dec-2010, G. White
%    Make mover conv vals all +ve (roll sign changed in EPICS to also be
%    RHS)
% 17-Nov-2010, G. White
%    Cavity BPMs co-ordinate system fixed to be RHS in EPICS, take out sign
%    flips here
% 13-Feb-2010, M. Woodley
%    Change sign of Tilt in mover conv - dTilt>0 rotates counter-clockwise
%    ->  HwInfo.GIRDER(gir).conv=[1e-6 1e-6 -1e-6]; % readout in um/urad
% 21-Feb-2010, G. White
%    Change Tilt sign in mover conv - all EPICS now in RH co-ord system
% 13-Feb-2010, M. Woodley
%    Changes and updates related to magnet limits, etc.:
%    - fix bug in calculation of limits for "reversed" XCOR, YCOR, and MULT
%    - new BH1R energy fudge factor (per T. Okugi)
%    - use hard-wired current limits for magnet power supplies that don't have
%      limits set in EPICS
%    - use special function to compute limits for ZX* bend trim correctors
%    - reflect MMS data for IDX skew quad (bipolar) in I2B_ATF2_QUAD
% 18-Nov-2009, M. Woodley
%    Signs of FF mover QBPMs checked and set
% 16-Nov-2009, M. Woodley
%    - map everything into an upright locally right-handed coordinate
%      system (+X is LEFT as you look downbeam; +Y is UP; +TILT is
%      clockwise as you look downbeam);
%    - set signs of correctors based on observations with flippy magnet;
%    - set signs of movers based on observations with dial gauges;
%    - remove all sign-flips for BPMs pending observation with beam
% 14-Nov-2009, M. Woodley
%    Fix handling of SF1R and SD1R trims
% 02-Nov-2009, G. White
%    Add ability to switch between DES and ACT current read
% 02-Nov-2009, M. Woodley
%    Fix bug in handling of QM7AR ... 'QM7R1' -> 'QM7R_1'
% 14-Apr-2009, M. Woodley
%    Add fudge factors for the Damping Ring BH1R, QF1R, and QF2R families
% ------------------------------------------------------------------------------

global INSTR BEAMLINE PS FL GIRDER %#ok<NUSED>

% DES or ACT PVs?
% Default to reading IACT unless FS_CURMETHOD=DES
despv=getenv('FS_CURRENT_READ_METHOD');
if isempty(despv) || ~isequal(despv,'DES')
  FL.currentReadMethod='ACT';
  curPV='current';
else
  FL.currentReadMethod='DES';
  curPV='currentWrite';
end

% Use POT or LVDT readback
mmeth=getenv('FS_MOVER_READ_METHOD');
if isempty(mmeth) || ~isequal(mmeth,'LVDT')
  FL.moverReadMethod='POT';
else
  FL.moverReadMethod='LVDT';
end

% Assignment based on FL.expt
if ~isfield(FL,'expt'); error('No FL.expt set!'); end;

FL.EScaleATF2Bends=1; % set =1 to keep design bend angles in EXT & FF

% ------------------------------------------------------------------------------
% PS Device limits and hardwired polarities
% ------------------------------------------------------------------------------
if ((nargin>1)&&strcmp(cmd,'set_limits'))
  disp('Getting PS limits and magnet polarities...')
  for iloop=1:2
    ignoreList={};
    if (~isfield(FL,'HwInfo'))
      warning('Lucretia:Floodland:FlAssignHW','run function normally first')
      return
    end
    if (iloop==2)
      nzi=cellfun(@(x) ~isempty(x),highpv);
      highval(nzi)=FlCA('lcaGet',{highpv{nzi}}'); highval(isnan(highval))=0; %#ok<*CCAT1>
      lowval(nzi)=FlCA('lcaGet',{lowpv{nzi}}'); lowval(isnan(lowval))=0;
      if (FL.doGetPolarities)
        nzi=cellfun(@(x) ~isempty(x),polpv);
        polval(nzi)=FlCA('lcaGet',{polpv{nzi}}'); polval(isnan(polval))=0;
      end
    else
      highval=zeros(1,length(FL.HwInfo.PS));
      lowval=zeros(size(highval));
      polval=zeros(size(highval));
    end
    for ips=1:length(FL.HwInfo.PS)
      if (isfield(FL.HwInfo.PS(ips),'pvname')&& ...
          (length(FL.HwInfo.PS(ips).pvname)>1)&& ...
          isempty(strfind(FL.HwInfo.PS(ips).pvname{2}{1},'SBEN'))&& ...
          ~ismember(FL.HwInfo.PS(ips).pvname{2}{1},ignoreList))
        pv=FL.HwInfo.PS(ips).pvname{2}{1};
        if (iloop==1)
          highpv{ips,1}=regexprep(pv,'VAL$','HIGH');
          lowpv{ips,1}=regexprep(pv,'VAL$','LOW');
          polpv{ips,1}=FL.HwInfo.PS(ips).pvname_pol;
          continue
        else
          high=highval(ips);
          low=lowval(ips);
          pol=polval(ips);
          if (low==high)
            id=PS(ips).Element(1);
            name=BEAMLINE{id}.Name;
            [low,high]=ATF2magLims(name,low,high);
           %fprintf(1,'%-10s %8.3f %8.3f (%4d %3d)\n',name,low,high,id,ips)
          end
        end
        if (~isempty(strfind(BEAMLINE{PS(ips).Element(1)}.Class,'COR'))|| ...
            ~isempty(strfind(BEAMLINE{PS(ips).Element(1)}.Class,'MULT')))
          if (low==high)
            if (~isempty(strfind(BEAMLINE{PS(ips).Element(1)}.Class,'COR')))
              low=-10; % assume 10A power supply
              high=10;
            elseif (~isempty(strfind(BEAMLINE{PS(ips).Element(1)}.Class,'MULT')))
              low=-8; % assume 10A power supply, limited at 8A
              high=8;
            end
          end % if no limits given
          name=BEAMLINE{PS(ips).Element(1)}.Name;
          if (strcmp(name,'ZX1X')||strcmp(name,'ZX2X')||strcmp(name,'ZX3X'))
%             [low,high]=ZXmagLims(ips,low,high);
            low=0; high=0;
          else
            if (length(FL.HwInfo.PS(ips).conv)==1)
              high=high*FL.HwInfo.PS(ips).conv;
              low=low*FL.HwInfo.PS(ips).conv;
            else
              high=interp1(FL.HwInfo.PS(ips).conv(1,:), ...
                FL.HwInfo.PS(ips).conv(2,:),high,'spline');
              low=interp1(FL.HwInfo.PS(ips).conv(1,:), ...
                FL.HwInfo.PS(ips).conv(2,:),low,'spline');
            end % simple conv factor or lookup table?
          end % ZX*X or other?
        else
          if (length(FL.HwInfo.PS(ips).conv)==1)
            high=(high*FL.HwInfo.PS(ips).conv)/ ...
              abs(BEAMLINE{PS(ips).Element(1)}.B(1));
            low=(low*FL.HwInfo.PS(ips).conv)/ ...
              abs(BEAMLINE{PS(ips).Element(1)}.B(1));
          else
            high=interp1(FL.HwInfo.PS(ips).conv(1,:), ...
              FL.HwInfo.PS(ips).conv(2,:),high,'spline')/...
              abs(BEAMLINE{PS(ips).Element(1)}.B(1));
            low=interp1(FL.HwInfo.PS(ips).conv(1,:), ...
              FL.HwInfo.PS(ips).conv(2,:),low,'spline')/...
              abs(BEAMLINE{PS(ips).Element(1)}.B(1));
          end % simple conv factor or lookup table?
        end % if cor or not
        if (high~=low)
          FL.HwInfo.PS(ips).high=max(low,high);
          FL.HwInfo.PS(ips).low=min(low,high);
        end % if high~=low
        % Set polarity of magnets if valid polarity field setting
        if ((pol==1)||(pol==-1))
          for iele=PS(ips).Element
            BEAMLINE{iele}.B=abs(BEAMLINE{iele}.B)*pol;
          end
        end
      end % if write pv
    end % for ips
    if (iloop==2)
      return
    end
  end
end
% ------------------------------------------------------------------------------
% BPMs
% ------------------------------------------------------------------------------
% Known bad bpms
%     badbpms={'MB25R' 'MB47R' 'MB54R' 'MB59R' 'MB1X'};
% BPMs on backwards movers
% backmovers={'MQD0FF' 'MQF1FF' 'MSD0FF' 'MSF1FF' 'MSD4FF' 'MSF5FF' 'MSF6FF'};
bpm2ref_bpmname={};
bpm2ref_refname={};
ref_fid=fopen(fullfile(FL.atfDir,'control-software','epics-3.14.8', ...
  'ioc-atf','iocBoot','iocatf','bpmDefs'));
while (1)
  tline=fgetl(ref_fid);
  if (~ischar(tline)),break,end
  t=regexp(tline,'REFCAV=(.+)")','tokens');
  if (~isempty(t))
    tname=regexp(tline,'BPMNAME=(.+),.+)','tokens');
    if (isempty(tname))
      error('Error mapping REF Cavity to CAV BPM for: %s',tname{1}{1});
    end
    bpm2ref_bpmname{end+1}=tname{1}{1};
    bpm2ref_refname{end+1}=t{1}{1};
  end % if ~isempty(t)
end % while (1)
fclose(ref_fid);
badbpms={};
FL.badbpm.instind=[];FL.badbpm.name={};FL.badbpm.index=[];
[stat, etinstr]=FlET('GetINSTR'); %#ok<*ASGLU> % get data about ET system
iex=findcells(BEAMLINE,'Name','IEX');
for ibpm=findcells(INSTR,'Class','MONI')
  if (INSTR{ibpm}.Index && ~isequal(INSTR{ibpm}.Class,'NOHW'))
    if (ismember(BEAMLINE{INSTR{ibpm}.Index}.Name,badbpms))
      FL.badbpm.instind(end+1)=ibpm;
      FL.badbpm.name{end+1}=BEAMLINE{INSTR{ibpm}.Index}.Name;
      FL.badbpm.index(end+1)=INSTR{ibpm}.Index;
      continue
    end % if bad bpm
    if (strcmp(INSTR{ibpm}.Type,'ccav')|| ...
        strcmp(INSTR{ibpm}.Type,'scav')|| ...
        strcmp(INSTR{ibpm}.Type,'ip'))
      if strcmp(BEAMLINE{INSTR{ibpm}.Index}.Name,'MPIP')
        name='M-PIP';
      else
        name=regexprep(BEAMLINE{INSTR{ibpm}.Index}.Name,'^M','');
      end
      % fill TMIT with the ref cavity used for this cavity
%       if any(ismember(BEAMLINE{INSTR{ibpm}.Index}.Name,{'IPBPMA' 'IPBPMB' 'MPREIP'}))
%         refcav='REFC3:amp';
%       else
%         refcav=bpm2ref_refname{ismember(bpm2ref_bpmname,name)};
%       end
      refcav='REFC1:amp';
      if strcmp(BEAMLINE{INSTR{ibpm}.Index}.Name,'IPBPMA')
        HwInfo.INSTR(ibpm).pvname{1}={'IPBx:pos.VAL' 'IPAy:pos.VAL' 'REFIPY1:amp.VAL'};
      elseif strcmp(BEAMLINE{INSTR{ibpm}.Index}.Name,'IPBPMB')
        HwInfo.INSTR(ibpm).pvname{1}={'IPAx:pos.VAL' 'IPBy:pos.VAL' [refcav,'.VAL']};
      else
        HwInfo.INSTR(ibpm).pvname{1}={[name,'x:pos.VAL'],[name,'y:pos.VAL'], ...
            [refcav,'.VAL']};
      end
      % Nov 17 2010- make conv vals +ve, aim to push all correct conv
      % values to EPICS
      if INSTR{ibpm}.Index>FL.Region.FFS.ind(1) %&& ~strcmp(BEAMLINE{INSTR{ibpm}.Index}.Name,'MFB1FF')
        HwInfo.INSTR(ibpm).conv=[1e-6,1e-6,1]; % FF (18nov09 1550)
      else
        HwInfo.INSTR(ibpm).conv=[1e-6,1e-6,1]; % EXT
      end
    elseif strcmp(BEAMLINE{INSTR{ibpm}.Index}.Name,'MQF15X') || strcmp(BEAMLINE{INSTR{ibpm}.Index}.Name,'MQD14X') || strcmp(BEAMLINE{INSTR{ibpm}.Index}.Name,'MQF13X') ||...
      strcmp(BEAMLINE{INSTR{ibpm}.Index}.Name,'MQF9X') || strcmp(BEAMLINE{INSTR{ibpm}.Index}.Name,'MQD8X') || strcmp(BEAMLINE{INSTR{ibpm}.Index}.Name,'MQF7X') ||...
      strcmp(BEAMLINE{INSTR{ibpm}.Index}.Name,'MQF6X') || strcmp(BEAMLINE{INSTR{ibpm}.Index}.Name,'MQD5X') || strcmp(BEAMLINE{INSTR{ibpm}.Index}.Name,'MQF4X') ||...
      strcmp(BEAMLINE{INSTR{ibpm}.Index}.Name,'MQF3X') || strcmp(BEAMLINE{INSTR{ibpm}.Index}.Name,'MQD2X') || strcmp(BEAMLINE{INSTR{ibpm}.Index}.Name,'MQF1X') ||...
      strcmp(BEAMLINE{INSTR{ibpm}.Index}.Name,'MB2X') || strcmp(BEAMLINE{INSTR{ibpm}.Index}.Name,'MFB1FF')
      if FL.ntrain==2
        HwInfo.INSTR(ibpm).pvname{1}={[BEAMLINE{INSTR{ibpm}.Index}.Name ':X2.VAL'] [BEAMLINE{INSTR{ibpm}.Index}.Name ':Y2.VAL'] [BEAMLINE{INSTR{ibpm}.Index}.Name ':TMIT2.VAL']};
      else
        HwInfo.INSTR(ibpm).pvname{1}={[BEAMLINE{INSTR{ibpm}.Index}.Name ':X.VAL'] [BEAMLINE{INSTR{ibpm}.Index}.Name ':Y.VAL'] [BEAMLINE{INSTR{ibpm}.Index}.Name ':TMIT.VAL']};
      end
      HwInfo.INSTR(ibpm).conv=[1e-6,1e-6,1];
    elseif strcmp(BEAMLINE{INSTR{ibpm}.Index}.Name,'IP')
      HwInfo.INSTR(ibpm).pvname{1}={'VIRTBPM:IP:X' 'VIRTBPM:IP:Y' 'VIRTBPM:IP:TMIT'};
      HwInfo.INSTR(ibpm).conv=[1e-6 1e-6 1];
    elseif INSTR{ibpm}.Index>iex
      name=['ATF:BPMS:',BEAMLINE{INSTR{ibpm}.Index}.Name,':'];
      HwInfo.INSTR(ibpm).pvname{1}={[name,'X.VAL'],[name,'Y.VAL'], ...
        [name,'TMIT.VAL']};
      HwInfo.INSTR(ibpm).conv=[1e-6,1e-6,1]; % EXT/FF
    end % if FFS c-band bpm
   %if ismember(BEAMLINE{INSTR{ibpm}.Index}.Name,backmovers)
   %  HwInfo.INSTR(ibpm).conv(1)=-HwInfo.INSTR(ibpm).conv(1);
   %end
    HwInfo.INSTR(ibpm).units='m';
    HwInfo.INSTR(ibpm).protocol='EPICS';
    % If ET bpm, treat using FlET routine, set conv to 0 to avoid using PV
    % name set here in FlUpdate
    if ismember(ibpm,etinstr) && ~isempty(strfind(FL.mode,'trusted')) && ~FL.SimMode
      HwInfo.INSTR(ibpm).conv=0;
    end
  end % if instrInd
end % for ibpmHan
% ------------------------------------------------------------------------------
% Profile measuring devices
% ------------------------------------------------------------------------------
for iprof=find(~ismember(1:length(INSTR),[findcells(INSTR,'Class','IMON'), ...
  findcells(INSTR,'Class','MONI') findcells(INSTR,'Type','bkgmon')]))
  if (INSTR{iprof}.Index && ~isequal(INSTR{iprof}.Class,'NOHW'))
    name=['ATF:PROF:',BEAMLINE{INSTR{iprof}.Index}.Name,':'];
    HwInfo.INSTR(iprof).units='um';
    HwInfo.INSTR(iprof).pvname{1}={[name,'X.VAL'],[name,'Y.VAL'], ...
      [name,'TMIT.VAL'],[name,'SIG11.VAL'],[name,'SIG33.VAL'], ...
      [name,'SIG13.VAL']};
    HwInfo.INSTR(iprof).protocol='EPICS';
    HwInfo.INSTR(iprof).conv=[1e-6,1e-6,1,1e-6,1e-6,1e-6];
  end % if HW present
end % for iprof
% ------------------------------------------------------------------------------
% Current Measurement Devices
% ------------------------------------------------------------------------------
for imon=findcells(INSTR,'Class','IMON')
  if INSTR{imon}.Index && ~isequal(INSTR{imon}.Class,'NOHW')
    HwInfo.INSTR(imon).units='electrons';
    HwInfo.INSTR(imon).pvname{1}{1}=['ATF:IMON:',BEAMLINE{INSTR{imon}.Index}.Name,'.VAL'];
    HwInfo.INSTR(imon).protocol='EPICS';
    HwInfo.INSTR(imon).conv=1e10;
  else
    HwInfo.INSTR(imon).units='electrons';
    HwInfo.INSTR(imon).pvname{1}{1}='REFIPY1:amp.VAL';
    HwInfo.INSTR(imon).protocol='EPICS';
    HwInfo.INSTR(imon).conv=1e7;
  end
end
ict1x=findcells(BEAMLINE,'Name','ICT1X');
ind=findcells(INSTR,'Index',ict1x);
HwInfo.INSTR(ind).pvname{1}{1}='BIM:EXT:nparticles';
ictip=findcells(BEAMLINE,'Name','ICTDUMP');
ind=findcells(INSTR,'Index',ictip);
HwInfo.INSTR(ind).pvname{1}{1}='BIM:IP:nparticles';
% ------------------------------------------------------------------------------
% Background Monitors
% ------------------------------------------------------------------------------
for ibkg=findcells(INSTR,'Type','bkgmon')
  if INSTR{ibkg}.Index && ~isequal(INSTR{ibkg}.Class,'NOHW')
    HwInfo.INSTR(ibkg).units=[];
    HwInfo.INSTR(ibkg).pvname=[];
    HwInfo.INSTR(ibkg).protocol=[];
    HwInfo.INSTR(ibkg).conv=[];
  end
end
% ------------------------------------------------------------------------------
% OTRs
% ------------------------------------------------------------------------------
% for iotr=findcells(INSTR,'Type','otr')
%   if INSTR{iotr}.Index && ~isequal(INSTR{iotr}.Class,'NOHW')
%     wOtr=str2double(regexp(BEAMLINE{INSTR{iotr}.Index}.Name,'\d+','match','once'));
%     HwInfo.INSTR(iotr).units='pixels';
%     HwInfo.INSTR(iotr).pvname{1}{1}=sprintf('mOTR:image%d:ArrayData',wOtr+1);
%     HwInfo.INSTR(iotr).postCommand{2,1}={sprintf('mOTR:image%d:ArrayData.PROC',wOtr+1) 1};
%     HwInfo.INSTR(iotr).protocol='EPICS';
%     HwInfo.INSTR(iotr).conv=[1 1 1 1 1 1];
%   end
% end

% ------------------------------------------------------------------------------
% Dipole Correctors
% ------------------------------------------------------------------------------
corList=[findcells(BEAMLINE,'Class','XCOR'), ...
         findcells(BEAMLINE,'Class','YCOR')];
dualList=findcells(BEAMLINE,'Name','Z*RX');
excludeList=[ findcells(BEAMLINE,'Name','IPKICK'), ...
  findcells(BEAMLINE,'Name','ZS*X'), ...
  findcells(BEAMLINE,'Name','ZX3X')];
for icor=1:length(corList) % {read write}
  if (ismember(corList(icor),dualList)),continue,end
  psInd=BEAMLINE{corList(icor)}.PS;
  if (ismember(PS(psInd).Element(1),excludeList))
    if psInd
      HwInfo.PS(psInd).pvname=[];
    end
    continue
  end
  if (psInd)
    name=BEAMLINE{corList(icor)}.Name;
    HwInfo.PS(psInd).pvname{1}={[name,':',curPV,'.VAL']}; % current readback
    HwInfo.PS(psInd).pvname{2}={[name,':currentWrite.VAL']}; % current set
    HwInfo.PS(psInd).pvname{3}={[name,':currentRead.VAL']}; % current readback
    HwInfo.PS(psInd).units={'A','A'};
    HwInfo.PS(psInd).protocol='EPICS';
    if (corList(icor)>Model.extStart) % EXT/FF
      if (~isempty(regexp(BEAMLINE{corList(icor)}.Name,'^ZX','once')))
        [ivec,bvec,HwInfo.PS(psInd).unipolar,HwInfo.PS(psInd).nt_ratio]= ...
          I2B_DIPOLE_TRIM_COR(corList(icor));
        if strcmp(BEAMLINE{corList(icor)}.Name,'YCOR')
          HwInfo.PS(psInd).conv=[ivec;bvec]; % amps;radian/amp
        else
          HwInfo.PS(psInd).conv=[ivec;bvec]; % amps;radian/amp
        end
        if corList(icor)>Model.ffStart.ind
          HwInfo.PS(psInd).vput=sprintf('DB_FF::%s:CURRENT.DAC.WRITE',name);
        else
          HwInfo.PS(psInd).vput=sprintf('DB_MAG::%s:CURRENT.DAC.WRITE',name);
        end
        switch BEAMLINE{corList(icor)}.Name
          case 'ZX1X'
            HwInfo.PS(psInd).pvname{1}={'BH1X:current.VAL', ...
              'ZX1X:current.VAL'}; % current readback record
            HwInfo.PS(psInd).pvname{2}={'BH1X:currentWrite.VAL', ...
              'ZX1X:currentWrite.VAL'}; % current write record
            HwInfo.PS(psInd).pvname{3}={'BH1X:currentRead.VAL', ...
              'ZX1X:currentRead.VAL'}; % current readback record
            HwInfo.PS(psInd).nt_ratio=HwInfo.PS(psInd).nt_ratio; % BH1 bends to the right
          case 'ZX2X'
            HwInfo.PS(psInd).pvname{1}={'BH1X:current.VAL', ...
              'ZX2X:current.VAL'}; % current readback record
            HwInfo.PS(psInd).pvname{2}={'BH1X:currentWrite.VAL', ...
              'ZX2X:currentWrite.VAL'}; % current write record
            HwInfo.PS(psInd).pvname{3}={'BH1X:currentRead.VAL', ...
              'ZX2X:currentRead.VAL'}; % current readback record
          case 'ZX3X'
            HwInfo.PS(psInd).pvname{1}={'BH3X:current.VAL', ...
              'ZX3X:current.VAL'}; % current readback record
            HwInfo.PS(psInd).pvname{2}={'BH3X:currentWrite.VAL', ...
              'ZX3X:currentWrite.VAL'}; % current write record
            HwInfo.PS(psInd).pvname{3}={'BH3X:currentRead.VAL', ...
              'ZX3X:currentRead.VAL'}; % current readback record
        end % switch name
      else
        if strcmp(BEAMLINE{corList(icor)}.Class,'YCOR')
          HwInfo.PS(psInd).conv=I2B_ATF2_CORR(corList(icor)); % radian/amp
        else
          HwInfo.PS(psInd).conv=I2B_ATF2_CORR(corList(icor)); % radian/amp
        end
      end % if trim or dipole
    else % DR
      if strcmp(BEAMLINE{corList(icor)}.Class,'YCOR')
        HwInfo.PS(psInd).conv=I2B_ATF_RINGCOR(corList(icor)); % radian/amp
%         temp=1;
      else
        HwInfo.PS(psInd).conv=I2B_ATF_RINGCOR(corList(icor)); % radian/amp
      end
    end % if EXT
  end % if psind
  HwInfo.PS(psInd).unipolar=0;
end % for icorInd
% ------------------------------------------------------------------------------
% Magnet PS's
% ------------------------------------------------------------------------------
haps_name={};
% Get name - > PS num mapping
haps_fid=fopen(fullfile(FL.atfDir,'control-software','epics-3.14.8', ...
  'ioc-ps','iocBoot','iocps','st-sim.cmd'));
while (1)
  tline=fgetl(haps_fid);
  if (~ischar(tline)),break,end
  t=regexp(tline,'NAME=(.+)")','tokens');
  if (~isempty(t))
    haps_name{end+1}=t{1}{1};
  end % if ~empty(t)
end % while (1)
fclose(haps_fid);
hapsPSind=zeros(2,length(haps_name));
for ips=1:length(haps_name)
  altend='AB';
  magInd=findcells(BEAMLINE,'Name',haps_name{ips});
  if (isempty(magInd))
    for itry=1:length(altend)
      magInd=findcells(BEAMLINE,'Name',[haps_name{ips},altend(itry)]);
      if (~isempty(magInd)),break,end
    end % for itry
    if (isempty(magInd))
      error(['Lucretia name not found for HAPS ID: PS',num2str(ips), ...
        ' (',haps_name{ips},')'])
    end
  end % if mag found
  hapsPSind(1,ips)=ips;
  hapsPSind(2,ips)=BEAMLINE{magInd(1)}.PS;
end % for ips
% add QF13X to haps list (in series with QF15X)
indx=findcells(BEAMLINE,'Name','QF15X');
idps=BEAMLINE{indx(1)}.PS;
id=find(hapsPSind(2,:)==idps);
idhaps=hapsPSind(1,id); %#ok<FNDSB>
indx=findcells(BEAMLINE,'Name','QF13X');
idps=BEAMLINE{indx(1)}.PS;
hapsPSind=[hapsPSind,[idhaps;idps]];
%
psList=[Model.PSind.RQuad,Model.PSind.RSext,Model.PSind.Quad, ...
  Model.PSind.Sext,Model.PSind.SBEN,Model.PSind.RSBEN,Model.PSind.RMult];
sk1=findcells(BEAMLINE,'Name','SK1FF');psList=[psList BEAMLINE{sk1(1)}.PS];
excludeList=0; % exclude power supplies with PS().Element=0
for ips=1:length(psList)
  fudge=[]; % placeholder for BH1R,QF1R,QF2R fudge factors
  indx=PS(psList(ips)).Element(1);
  if (ismember(indx,excludeList))
    PS(psList(ips)).Ampl=0;
    continue
  end
  if (ismember(psList(ips),hapsPSind(2,:)))
    idhaps=find(hapsPSind(2,:)==psList(ips));
    name=['PS',num2str(hapsPSind(1,idhaps))]; %#ok<FNDSB>
    HwInfo.PS(psList(ips)).pvname_pol=[name,':polarity.VAL'];
    HwInfo.PS(psList(ips)).vput=sprintf('DB_FF::%s:CURRENT.DAC.WRITE',BEAMLINE{indx}.Name);
  else
    name=BEAMLINE{indx}.Name;
    HwInfo.PS(psList(ips)).vput=sprintf('DB_MAG::%s:CURRENT.DAC.WRITE',BEAMLINE{indx}.Name);
  end % if haps
  % No HW info for extraction kicker, septa, BKX, or dump bend
  if ~isempty(regexp(name,'^KEX', 'once')) || ...
     ~isempty(regexp(name,'^BS\dX.','once')) || ...
     ~isempty(regexp(name,'^BKX', 'once')) || ...
     ~isempty(regexp(name,'^BDUMP','once'))
   continue
  end
  HwInfo.PS(psList(ips)).units={'A','A'};
  if (~isempty(regexp(BEAMLINE{indx}.Name,'(FFA$)|(FFB$)','once')) && ...
      strcmp(BEAMLINE{indx}.Class,'SBEN'))
    [ivec,bvec,unipolar]=I2B_ATF2_DIPOLE(indx); % FF bends
  elseif (indx>Model.extStart && isequal(BEAMLINE{indx}.Class,'QUAD'))
    [ivec,bvec,unipolar,HwInfo.PS(psList(ips)).nt_ratio]= ...
      I2B_ATF2_QUAD(indx); % EXT+FF quads
  elseif (indx>Model.extStart && isequal(BEAMLINE{indx}.Class,'SEXT'))
    [ivec,bvec,unipolar]=I2B_ATF2_SEXT(indx); % FF sexts
  elseif (indx<Model.extStart && isequal(BEAMLINE{indx}.Class,'QUAD'))
    [ivec,bvec,unipolar,HwInfo.PS(psList(ips)).nt_ratio]= ...
      I2B_ATF_RINGQUAD(indx); % DR quads (including trims)
    if ((~isempty(regexp(BEAMLINE{indx}.Name,'^QF1R','once')))|| ...
        (~isempty(regexp(BEAMLINE{indx}.Name,'^QF2R','once'))))
      fudge=1;
    end
  elseif (indx<Model.extStart && isequal(BEAMLINE{indx}.Class,'SEXT'))
    [ivec,bvec,unipolar]=I2B_ATF_RINGSEXT(indx); % DR sextupoles
  elseif (indx<Model.extStart && isequal(BEAMLINE{indx}.Class,'MULT'))
    [ivec,bvec,unipolar]=I2B_ATF_RINGMULT(indx); % DR skew quad trims
  elseif (isequal(BEAMLINE{indx}.Class,'SBEN') && ...
        ( ~isempty(regexp(BEAMLINE{indx}.Name,'^BH1X','once')) || ...
          ~isempty(regexp(BEAMLINE{indx}.Name,'^BH2X','once')) || ...
          ~isempty(regexp(BEAMLINE{indx}.Name,'^BH3X','once')) ))
    [ivec,bvec,unipolar,HwInfo.PS(psList(ips)).nt_ratio]= ...
      I2B_DIPOLE_TRIM_COR(indx);
  elseif (isequal(BEAMLINE{indx}.Class,'SBEN') && ...
          ~isempty(regexp(BEAMLINE{indx}.Name,'^BH1R','once')))
    % pre-computed "bvi" polynomial for BH1R (kG-m = f(amps))
    bvibh1=[ 4.0507212e-03, 9.3415182e-03, 9.9991137e-07, ...
            -2.6931190e-09, 2.7155309e-12,-1.0346054e-15];
    % current fudge factor
   %f=793.50/812.17;
   %f=794.78487151/813.87; % February 2010
    f=786.575527594/812.801; % October 2012 (Ibh1r=812.801 A -> Edr=1.269 GeV) ... see DRenergy.m
    % evaluate polynomial at BH1R current
    ivec=1:1000;
    if (~isempty(regexp(BEAMLINE{indx}.Name,'^BH1R27','once')))
      % for BH1R27 - split = [0.25 0.75]
      bvec=0.1*(polyval(fliplr(bvibh1),f.*ivec)/4); % convert kG to T, too
    else % split 0.5 0.5
      bvec=0.1*(polyval(fliplr(bvibh1),f.*ivec)/2); % convert kG to T, too
    end
    unipolar=1;
    fudge=1; % for matching Damping Ring tunes
  elseif (isequal(BEAMLINE{indx}.Class,'SBEN') && ...
        ( ~isempty(regexp(BEAMLINE{indx}.Name,'^QM6RX','once')) || ...
          ~isempty(regexp(BEAMLINE{indx}.Name,'^QM7RX','once')) ))
    [ivec,bvec,unipolar,HwInfo.PS(psList(ips)).nt_ratio]= ...
      I2B_ATF2_QUAD(indx); % EXT+FF quads
  else
    unipolar=0;ivec=[];bvec=0;
  end % conversion data select
  if (~isempty(ivec))
    [nrw,ncol]=size(ivec);if (nrw>ncol),ivec=ivec';end
    [nrw,ncol]=size(bvec);if (nrw>ncol),bvec=bvec';end
    HwInfo.PS(psList(ips)).conv=[ivec;bvec];
  elseif (isempty(bvec))
    if FL.SimMode~=2
      error('No conversion value for this magnet - PS: %d',psList(ips))
    end
  else
    HwInfo.PS(psList(ips)).conv=bvec;
  end % if lookup table or conv factor
  HwInfo.PS(psList(ips)).unipolar=unipolar;
 %HwInfo.PS(psList(ips)).preCommand{1,1}={[name,':IACT.PROC'],1};
 %HwInfo.PS(psList(ips)).preCommand{2,1}={};
  name=strrep(name,'QM6RX','QM6R1'); % off-axis quadrupole
  name=strrep(name,'QM7RX','QM7R1'); % off-axis quadrupole
  name_orig=name;
  trimq=false;
  name_orig=regexprep(name_orig,'QF(\d+)R(\d+)','QF$1R_$2');
  name_orig=regexprep(name_orig,'QD(\d+)R(\d+)','QD$1R_$2');
  name_orig=regexprep(name_orig,'QM(\d+)R(\d+)','QM$1R_$2');
  name=regexprep(name,'QF(\d+)R\d+','QF$1R');
  if (~isequal(name,name_orig)),trimq=true;end
  name=regexprep(name,'QD(\d+)R\d+','QD$1R');
  if (~isequal(name,name_orig)),trimq=true;end
  name=regexprep(name,'QM(\d+)R\d+','QM$1R');
  if (~isequal(name,name_orig)),trimq=true;end
  name=regexprep(name,'SF1R\d+','SF1R');
  name=regexprep(name,'SD1R\d+','SD1R');
  name=regexprep(name,'SQSF(\d+)','SF1R_$1');
  name=regexprep(name,'SQSD(\d+)','SD1R_$1');
  name=regexprep(name,'BH(\d)X.','BH$1X');
  name=regexprep(name,'BH2X','BH1X');
  name=regexprep(name,'BH1R.+','BH1R');
  if (strcmp(name_orig,'QM7R_1'))
    name=strrep(name,'QM7R','QM7AR');
  end
  if (trimq)
    HwInfo.PS(psList(ips)).pvname{1}= ...
      {[name,':',curPV,'.VAL'],[name_orig,':',curPV,'.VAL']}; % current readback record
    HwInfo.PS(psList(ips)).pvname{2}= ...
      {[name,':currentWrite.VAL'],[name_orig,':currentWrite.VAL']}; % current set record
    HwInfo.PS(psList(ips)).pvname{3}= ...
      {[name,':currentRead.VAL'],[name_orig,':currentRead.VAL']}; % current readback record
  elseif (ismember(psList(ips),hapsPSind(2,:)))
    HwInfo.PS(psList(ips)).pvname{1}= ...
      {[name,':current.VAL']}; % current readback record
    HwInfo.PS(psList(ips)).pvname{2}= ...
      {[name,':currentDES.VAL']}; % current set record
    HwInfo.PS(psList(ips)).pvname{3}= ...
      {[name,':currentDES.VAL']}; % current readback record
    HwInfo.PS(psList(ips)).postCommand{1,1}={};
    HwInfo.PS(psList(ips)).postCommand{2,1}={[name,':setCurrent.PROC'] 1};
    HwInfo.PS(psList(ips)).on={[name,':pwrOn.PROC'] 1};
    HwInfo.PS(psList(ips)).off={[name,':pwrOff.PROC'] 1};
    HwInfo.PS(psList(ips)).status={[name,':status1.VAL']};
    HwInfo.PS(psList(ips)).rampRate=[name,':rampTimeConv.VAL'];
    HwInfo.PS(psList(ips)).standardise.iLow=[name,':standardise:iLow.VAL'];
    HwInfo.PS(psList(ips)).standardise.iHigh=[name,':standardise:iHigh.VAL'];
    HwInfo.PS(psList(ips)).standardise.waitLow= ...
      [name,':standardise:waitLow.VAL'];
    HwInfo.PS(psList(ips)).standardise.waitHigh= ...
      [name,':standardise:waitHigh.VAL'];
    HwInfo.PS(psList(ips)).standardise.nCycles= ...
      [name,':standardise:nCycles.VAL'];
    HwInfo.PS(psList(ips)).standardise.rate=[name,':standardise:rate.VAL'];
    HwInfo.PS(psList(ips)).standardise.settingRate= ...
      [name,':standardise:settingRate.VAL'];
    HwInfo.PS(psList(ips)).polarity=[name,':polarity.VAL'];
  else
    HwInfo.PS(psList(ips)).pvname{1}= ...
      {[name,':',curPV,'.VAL']}; % current readback record
    HwInfo.PS(psList(ips)).pvname{2}= ...
      {[name,':currentWrite.VAL']}; % current set record
    HwInfo.PS(psList(ips)).pvname{3}= ...
      {[name,':currentRead.VAL']}; % current readback record
  end % if ring quad- need main and trim currents
  HwInfo.PS(psList(ips)).protocol='EPICS';
  HwInfo.PS(psList(ips)).fudge=fudge;
end % for ips
%HwInfo.PS(27).conv(1,:)=HwInfo.PS(27).conv(1,:)./1.0465;
% ------------------------------------------------------------------------------
% Magnet Movers
% ------------------------------------------------------------------------------
% get list of magnet mover names from EPICS DB
mag_fid=fopen(fullfile(FL.atfDir,'control-software','epics-3.14.8', ...
  'ioc-qmov','qmovApp','Db','dbQmovConstants.db'));
str=fscanf(mag_fid,'%s');
fclose(mag_fid);
[nameID,mID]=regexp(str,'stringout,\s*"c1:qmov:m(\d[\d]?):name','end', ...
  'tokens');
nameID=[nameID,length(str)];
for iname=1:length(nameID)-1
  ind1=nameID(iname)+regexp(str(nameID(iname):nameID(iname+1)),'VAL,"', ...
    'end','once');
  ind2=ind1+regexp(str((ind1):nameID(iname+1)),'"','once')-2;
  magname{iname}=str(ind1:ind2);
end % for iname
% Set PV names for move commands
for im=1:length(magname)
  mele=findcells(BEAMLINE,'Name',magname{im});
  if (isempty(mele))
    error(['Lucretia Mover name not found for m',num2str(im)]);
  end % if empty mele
  gir=BEAMLINE{mele(1)-1}.Girder;
  if ~gir; continue; end;
  HwInfo.GIRDER(gir).units={'m','m'};
  if strcmp(FL.moverReadMethod,'POT')
    HwInfo.GIRDER(gir).pvname{1}={['c1:qmov:m',mID{im}{1},':x.VAL'] ...
      ['c1:qmov:m',mID{im}{1},':y.VAL'],['c1:qmov:m',mID{im}{1},':tilt.VAL']};
  else
     HwInfo.GIRDER(gir).pvname{1}={['c1:qmov:m',mID{im}{1},':x:alt.VAL'] ...
      ['c1:qmov:m',mID{im}{1},':y:alt.VAL'],['c1:qmov:m',mID{im}{1},':tilt:alt.VAL']};
  end
  HwInfo.GIRDER(gir).pvname{2}={['c1:qmov:m',mID{im}{1},':x:set.VAL'] ...
    ['c1:qmov:m',mID{im}{1},':y:set.VAL'], ...
    ['c1:qmov:m',mID{im}{1},':tilt:set.VAL']};
  HwInfo.GIRDER(gir).pvname{3}={['c1:qmov:m',mID{im}{1},':x:set.VAL'] ...
    ['c1:qmov:m',mID{im}{1},':y:set.VAL'], ...
    ['c1:qmov:m',mID{im}{1},':tilt:set.VAL']};
  HwInfo.GIRDER(gir).postCommand{1,1}={};
  HwInfo.GIRDER(gir).postCommand{2,1}= ...
    {['c1:qmov:m',mID{im}{1},':perturb.VAL'],1; ...
     ['c1:qmov:m',mID{im}{1},':dotrim.VAL'],1};
  HwInfo.GIRDER(gir).postCommand{2,2}= ...
    {['c1:qmov:m',mID{im}{1},':perturb.VAL'],1;[],[]};
  HwInfo.GIRDER(gir).protocol='EPICS';
  HwInfo.GIRDER(gir).conv=[1e-6 1e-6 1e-6]; % readout in um/urad
  % IsMoving PV
  HwInfo.GIRDER(gir).status{1}=['c1:qmov:m',mID{im}{1},':stopDetect'];
  HwInfo.GIRDER(gir).status{2}=['c1:qmov:m',mID{im}{1},':dotrim'];
end % for imover

% Disable mover "ATIPBPM" (associated with element "IPBPM")
gir=BEAMLINE{findcells(BEAMLINE,'Name','ATIPBPM')-1}.Girder;
if gir
  HwInfo.GIRDER(gir).pvname=[];
  HwInfo.GIRDER(gir).conv=[];
end

% MFB2FF
gir=BEAMLINE{findcells(BEAMLINE,'Name','MFB2FF')}.Girder;
HwInfo.GIRDER(gir).pvname=[];
HwInfo.GIRDER(gir).conv=[];

% Initialise timestamp data
for i_inst=1:length(HwInfo.INSTR)
  HwInfo.INSTR(i_inst).TimeStamp=0;
end
for igir=1:length(HwInfo.GIRDER)
  HwInfo.GIRDER(igir).TimeStamp=0;
end
for ips=1:length(HwInfo.PS)
  HwInfo.PS(ips).TimeStamp=0;
end

% ==============================================================================
% Conversion routines
% ------------------------------------------------------------------------------
% NOTE: In conversion routines, B values returned  as lookup values
% reference only the given index (ie one half of a split magnet), signified by
% calculating the integrated strength with respect to the BEAMLINE{indx}.L
% value
% ==============================================================================

function [I,BL]=ATF2_DEAave()
%
% [I,BL]=ATF2_DEAave();
%
% Generate average integrated strength versus current for ATF2 DEA dipoles
%
% OUTPUTs:
%
%   I  = average current values (amps)
%   BL = average integrated strength values (T-m)

% DEA MMS data from Cherrill Spencer' file SummaryDEADipoleIntB.dlVCurrent.xls

Imms=[ ...
%    #1       #2      #3
   74.8737, 74.978, 74.7237; ...
   85.8528, 84.97 , 85.1874; ...
   95.3214, 94.985, 95.1369; ...
  105.3024,104.983,105.0999; ...
  115.2948,114.976,115.0764; ...
  125.2719,124.966,125.0409; ...
  135.2361,134.976,134.9892; ...
  145.2123,144.973,144.9489; ...
  155.205 ,154.966,154.9266; ...
  165.189 ,164.98 ,164.8941; ...
 %174.972 ,174.97 ,174.9624; ...
  175.001 ,175.001,175.001 ; ...
];
BLmms=[ ...
%  #1         #2       #3
  0.1069431 ,0.10729 ,0.10684176 ; ...
  0.1225926 ,0.121638,0.121771999; ...
  0.1360842 ,0.135888,0.13596253 ; ...
  0.1503006 ,0.150169,0.150162438; ...
  0.1645231 ,0.164433,0.1643728  ; ...
  0.1787202 ,0.178669,0.17856412 ; ...
  0.1929014 ,0.192718,0.192736778; ...
  0.2070942 ,0.206832,0.206920116; ...
  0.2212932 ,0.221066,0.22110597 ; ...
  0.2354658 ,0.235292,0.2352682  ; ...
  0.24934725,0.249631,0.249572424; ...
];

% do a linear fit to all of the data

coef=polyfit(reshape(Imms,[],1),reshape(BLmms,[],1),1);

% use the fitted linear coefficients to generate "average" data

I=[0,75:10:175]';
BL=polyval(coef,I);
BL(1)=0; % zero means zero
% ==============================================================================
function [ivec,bvec,unipolar] = I2B_ATF2_DIPOLE(indx)

global BEAMLINE

magNames={'B5FFA' 'B5FFB' 'B2FFA' 'B2FFB' 'B1FFA' 'B1FFB'};
magInd=find(ismember(magNames,BEAMLINE{indx}.Name));
if ~magInd; error('Mag Name not found'); end;

% integrated field strength (T)
% data for #1 magnet

[ivec,bvec]=ATF2_DEAave();
bvec=bvec/2; % magnets split in half ... each gets BL/2
unipolar=1;
% ==============================================================================
function [qI,qG]=QEAmms(id,qleff)

% id name     QEA#  Imax  Col
% -- -------  ----  ----  ---
% 15 QD10X     12    150   13
% 16 QF11X     11    150   12
% 18 QD12X     16    150   15
% 23 QD16X      3    150    4
% 24 QF17X      6    150    7
% 26 QD18X      8    150    9
% 27 QF19X     10    150   11
% 30 QM16FF     9    150   10
% 31 QM15FF     2    150    3
% 32 QM14FF     1    150    2
% 33 QM13FF     5    150    6
% 34 QM12FF    15    150   14
% 35 QM11FF     4    150    5
% 36 QD10BFF   25     50   12
% 37 QD10AFF   22     50    9
% 38 QF9BFF    23     50   10
% 39 QF9AFF    24     50   11
% 40 QD8FF     20     50    7
% 41 QF7FF     27     50   14
% 42 QD6FF     26     50   13
% 43 QF5BFF    29     50   16
% 44 QF5AFF    28     50   15
% 45 QD4BFF    33     50   20
% 46 QD4AFF    30     50   17
% 47 QF3FF     31     50   18
% 48 QD2BFF    34     50   21
% 49 QD2AFF    32     50   19

lookup=[ ...
  15 150   13
  16 150   12
  18 150   15
  23 150    4
  24 150    7
  26 150    9
  27 150   11
  30 150   10
  31 150    3
  32 150    2
  33 150    6
  34 150   14
  35 150    5
  36  50   12
  37  50    9
  38  50   10
  39  50   11
  40  50    7
  41  50   14
  42  50   13
  43  50   16
  44  50   15
  45  50   20
  46  50   17
  47  50   18
  48  50   21
  49  50   19
];

% mainamp150AmaxAbs.txt (M.Masuzawa)

mms150=[ ...
%  1          2           3           4           5           6           7           8           9           10          11          12          13          14          15
%  Current    QEA01       QEA02       QEA03       QEA04       QEA05       QEA06       QEA07       QEA08       QEA09       QEA10       QEA11       QEA12       QEA15       QEA16
% ----------- ----------- ----------- ----------- ----------- ----------- ----------- ----------- ----------- ----------- ----------- ----------- ----------- ----------- -----------
  0.0000000   0.027852904 0.030506356 0.027256228 0.032829583 0.026876856 0.027443571 0.031409271 0.029505244 0.025968155 0.028044462 0.022606753 0.025058137 0.030732028 0.028316030
  10.000000   0.93772334  0.93937820  0.93742323  0.94295686  0.93441892  0.93938303  0.93977302  0.94099826  0.93904907  0.94194758  0.93286663  0.93956733  0.94647944  0.94635600
  20.000000   1.8596991   1.8603314   1.8592361   1.8664562   1.8528848   1.8613006   1.8582008   1.8629626   1.8631428   1.8659338   1.8576188   1.8642662   1.8714479   1.8737825
  30.000000   2.7942219   2.7947519   2.7938874   2.8019083   2.7836847   2.7963331   2.7882466   2.7984815   2.7999740   2.8019509   2.7873280   2.8015041   2.8097651   2.8142490
  40.000000   3.7219207   3.7224891   3.7216418   3.7304349   3.7072167   3.7248392   3.7106743   3.7271931   3.7295382   3.7310123   3.7169988   3.7312465   3.7410450   3.7473235
  50.000000   4.6551557   4.6554065   4.6547413   4.6638241   4.6360159   4.6587200   4.6389837   4.6609015   4.6642461   4.6648588   4.6430759   4.6657333   4.6776905   4.6852636
  60.000000   5.5738125   5.5738368   5.5732274   5.5829530   5.5497336   5.5780277   5.5530548   5.5802903   5.5841994   5.5831165   5.5638790   5.5852056   5.5990934   5.6079135
  70.000000   6.4870849   6.4860616   6.4862618   6.4965849   6.4582496   6.4919772   6.4617090   6.4939456   6.4988241   6.4955730   6.4779158   6.4995794   6.5144272   6.5247884
  80.000000   7.3985457   7.3957953   7.3972039   7.4089684   7.3641810   7.4039865   7.3686643   7.4049659   7.4110003   7.4056296   7.3833327   7.4111490   7.4097128   7.4393244
  90.000000   8.2836657   8.2789059   8.2809420   8.2950335   8.2439737   8.2886238   8.2487793   8.2896624   8.2962818   8.2881460   8.2702589   8.2957354   8.3124323   8.3268652
  100.00000   9.1123867   9.1020203   9.1059580   9.1207247   9.0675383   9.1115141   9.0734730   9.1165915   9.1232882   9.1116486   9.0975027   9.1158381   9.1324730   9.1499929
  110.00000   9.7852097   9.7633123   9.7741728   9.7834826   9.7384729   9.7754278   9.7469425   9.7866335   9.7921076   9.7767239   9.7518406   9.7700624   9.7869825   9.8076000
  120.00000   10.274178   10.242992   10.261633   10.265922   10.228235   10.260318   10.238844   10.274369   10.278795   10.260708   10.233387   10.245369   10.264085   10.285763
  130.00000   10.658867   10.620907   10.645681   10.647207   10.613884   10.643704   10.626049   10.658575   10.662965   10.642764   10.607639   10.621065   10.641808   10.664033
  140.00000   10.975464   10.932746   10.962528   10.961823   10.931271   10.959914   10.944938   10.975292   10.979631   10.957787   10.922914   10.932220   10.954095   10.976683
  150.00000   11.252048   11.205402   11.239195   11.236837   11.208383   11.236096   11.222959   11.252003   11.256350   11.233049   11.191726   11.204431   11.227302   11.250130
];

% set zero current strength to zero (ignore remnant field when interpolating)

n=find(mms150(:,1)==0);
mms150(n,2:end)=zeros(size(mms150(n,2:end)));

% mainamp50AmaxAbs.txt (M.Masuzawa)

mms50=[ ...
%  1          2           3           4           5           6           7           8           9           10          11          12          13          14          15          16          17          18          19          20          21
%  Current    QEA13       QEA14       QEA17       QEA18       QEA19       QEA20       QEA21       QEA22       QEA23       QEA24       QEA25       QEA26       QEA27       QEA28       QEA29       QEA30       QEA31       QEA32       QEA33       QEA34
% ----------- ----------- ----------- ----------- ----------- ----------- ----------- ----------- ----------- ----------- ----------- ----------- ----------- ----------- ----------- ----------- ----------- ----------- ----------- ----------- -----------
  0.0000000   0.028219100 0.040338729 0.029890871 0.029497238 0.034525234 0.029425563 0.027658368 0.027779808 0.027477957 0.027904926 0.028466344 0.027899513 0.021434505 0.020964241 0.022032959 0.031943474 0.028553501 0.032123175 0.028957782 0.030306084
  3.3333001   0.32538533  0.33715573  0.32505092  0.32442081  0.33133936  0.32527959  0.32236442  0.32316262  0.32378274  0.32318223  0.32277632  0.32569003  0.31823730  0.31833690  0.31833255  0.32496139  0.32426021  0.32609594  0.32492208  0.32532936
  6.6666999   0.63208896  0.64047939  0.62909323  0.62828606  0.63420987  0.63093483  0.62648714  0.62732536  0.62899351  0.62777531  0.62660450  0.63004756  0.62380683  0.62063122  0.62245131  0.62730497  0.62851375  0.63048053  0.63080686  0.63031852
  10.000000   0.94148004  0.94603467  0.93590742  0.93486601  0.93937200  0.93977785  0.93356729  0.93421125  0.93711084  0.93520695  0.93332684  0.93697536  0.93220925  0.93336999  0.92926800  0.93291068  0.93584037  0.93825185  0.93971300  0.93844241
  13.333000   1.2529521   1.2537711   1.2449207   1.2435915   1.2468803   1.2507372   1.2427585   1.2436256   1.2473272   1.2448578   1.2420537   1.2458996   1.2426412   1.2442291   1.2379702   1.2409927   1.2454461   1.2481778   1.2507575   1.2488642
  16.667000   1.5639043   1.5619390   1.5534923   1.5519813   1.5545483   1.5611117   1.5514039   1.5526090   1.5572330   1.5539834   1.5500604   1.5545545   1.5523636   1.5558752   1.5462418   1.5489813   1.5547541   1.5578287   1.5613403   1.5589787
  20.000000   1.8741443   1.8705049   1.8618793   1.8599479   1.8627108   1.8707479   1.8593891   1.8607818   1.8663399   1.8625795   1.8572839   1.8631321   1.8616416   1.8612897   1.8541365   1.8565601   1.8693553   1.8670722   1.8712773   1.8684933
  23.333000   2.1841798   2.1801219   2.1701567   2.1678445   2.1716371   2.1799972   2.1672122   2.1689975   2.1754117   2.1708317   2.1641300   2.1717048   2.1705081   2.1738250   2.1618476   2.1639242   2.1730690   2.1759412   2.1809573   2.1777673
  26.667000   2.5015478   2.4985588   2.4863641   2.4835498   2.4888871   2.4966478   2.4827976   2.4850328   2.4919055   2.4866457   2.4782469   2.4880300   2.4868593   2.4898355   2.4769828   2.4786382   2.4888225   2.4917469   2.4980671   2.4942434
  30.000000   2.8104367   2.8091772   2.7940876   2.7817678   2.7979240   2.8048146   2.7898581   2.7925751   2.8000093   2.7939847   2.7838352   2.7957492   2.7943413   2.7978346   2.7833722   2.7845740   2.7960050   2.7988794   2.8063977   2.8015463
  33.333000   3.1178350   3.1189554   3.1004212   3.0964954   3.1058538   3.1115117   3.0956228   3.0989015   3.1066909   3.0998857   3.0877182   3.1027405   3.1005061   3.1060436   3.0885746   3.0891926   3.1019273   3.1042821   3.1130064   3.1073811
  36.667000   3.4247386   3.4286873   3.4063148   3.4016933   3.4134352   3.4176803   3.4008279   3.4047415   3.4126701   3.4052124   3.3910625   3.4092085   3.4063492   3.4109607   3.3936911   3.3930762   3.4074311   3.4094582   3.4190931   3.4125471
  40.000000   3.7313569   3.7384400   3.7118733   3.7068160   3.7210751   3.7232792   3.7056735   3.7103586   3.7184212   3.7102764   3.6940403   3.7157691   3.7122478   3.7137301   3.6987295   3.6968606   3.7129827   3.7139955   3.7245657   3.7174027
  43.333000   4.0364571   4.0471492   4.0160818   4.0105395   4.0274525   4.0274763   4.0088239   4.0143366   4.0225286   4.0137668   3.9955339   4.0211272   4.0167503   4.0174079   4.0025177   3.9992521   4.0173407   4.0175309   4.0290337   4.0212517
  46.667000   4.3408098   4.3552766   4.3197250   4.3134394   4.3334475   4.3303418   4.3110003   4.3178172   4.3259072   4.3162355   4.2959971   4.3253222   4.3204656   4.3214779   4.3060641   4.3016801   4.3217702   4.3206162   4.3329167   4.3249798
  50.000000   4.6463265   4.6635814   4.6250100   4.6178207   4.6411600   4.6339417   4.6146355   4.6222954   4.6305385   4.6191273   4.5978169   4.6309385   4.6259670   4.6268058   4.6109891   4.6052308   4.6270924   4.6251469   4.6381001   4.6298366
];

% set zero current strength to zero (ignore remnant field when interpolating)

n=find(mms50(:,1)==0);
mms50(n,2:end)=zeros(size(mms50(n,2:end)));

n=find(lookup(:,1)==id);
Imax=lookup(n,2);
Col=lookup(n,3);
if (Imax==150)
  qI=mms150(:,1);
  qG=mms150(:,Col)/qleff;
elseif (Imax==50)
  qI=mms50(:,1);
  qG=mms50(:,Col)/qleff;
end
% ==============================================================================
function [ivec,bvec,unipolar,nt_ratio] = I2B_ATF2_QUAD(indx)
global BEAMLINE

% the order of quadrupoles:

magNames={ ...
  'QM6RX','QM7RX','QS1X','QF1X','QD2X', ...
  'QF3X','QF4X','QD5X','QF6X','QS2X', ...
  'QF7X','QD8X','QF9X','QK1X','QD10X', ...
  'QF11X','QK2X','QD12X','QF13X','QD14X', ...
  'QF15X','QK3X','QD16X','QF17X','QK4X', ...
  'QD18X','QF19X','QD20X','QF21X','QM16FF', ...
  'QM15FF','QM14FF','QM13FF','QM12FF','QM11FF', ...
  'QD10BFF','QD10AFF','QF9BFF','QF9AFF','QD8FF', ...
  'QF7FF','QD6FF','QF5BFF','QF5AFF','QD4BFF', ...
  'QD4AFF','QF3FF','QD2BFF','QD2AFF','QF1FF', ...
  'QD0FF'};

magInd=find(ismember(magNames,BEAMLINE{indx}.Name));
if (~magInd),error('Mag Name not found (%s)',BEAMLINE{indx}.Name),end

% there are 8 types of ATF2 quad:

%   1 = Hitachi type 4
%   2 = Tokin type 3581
%   3 = Hitachi type 5
%   4 = Hitachi type 2
%   5 = IDX skew
%   6 = PEPII 4Q17 [QF1FF]
%   7 = FFTB QC3 (shimmed) [QD0FF]
%   8 = QEA-D32T180 [each has it's own MMS data]

% MMS data for quad types (I is amp, G is T/m)
% (from: ATF$MAG:MAG_KI_Q_HITACHI_4.FOR
%        ATF$MAG:MAG_KI_Q_TOKIN_3581.FOR
%        ATF$MAG:MAG_KI_Q_HITACHI_5.FOR
%        ATF$MAG:MAG_KI_Q_HITACHI_2.FOR
%        ATF$MAG:MAG_KI_Q_IDX_SKEW.FOR
%        SLAC magnetic measurements data [QF1FF]
%        SLAC magnetic measurements data [QD0FF]
%        mainamp50AmaxAbs.txt (M.Masuzawa)
%        mainamp150AmaxAbs.txt (M.Masuzawa))

Nmms=[11,11,11,11,11,14];

qI=[ ...
    0.0,  0.0,  0.0,  0.0,-20.0,  0.0   ; ...
   20.2, 50.0, 10.4, 10.2,-16.0, 19.9668; ...
   40.2, 80.0, 20.2, 20.2,-12.0, 39.9533; ...
   60.4,100.0, 30.4, 30.2, -8.0, 59.9290; ...
   80.4,130.0, 40.2, 40.0, -4.0, 79.9956; ...
  100.0,150.0, 50.0, 50.0,  0.0, 89.9940; ...
  120.2,170.0, 60.2, 60.2,  4.0, 99.9888; ...
  140.2,190.0, 70.2, 70.2,  8.0,109.9871; ...
  160.2,210.0, 80.4, 80.2, 12.0,119.9576; ...
  180.0,230.0, 90.4, 90.2, 16.0,124.9373; ...
  200.4,245.0,100.6,100.2, 20.0,130.0423; ...
    0  ,  0  ,  0  ,  0  ,  0  ,134.9714; ...
    0  ,  0  ,  0  ,  0  ,  0  ,140.0186; ...
    0  ,  0  ,  0  ,  0  ,  0  ,150.0187; ...
];

% NOTE: data for 4Q17 (column 6) is integrated gradient (T)

qG=[ ...
   0.000, 0.0, 0.000, 0.000,-5.662,0.0     ; ...
   4.612, 7.5, 4.845, 3.832,-4.522,0.517863; ...
   9.201,11.8, 9.424, 7.646,-3.385,1.030132; ...
  13.858,14.8,14.248,11.498,-2.247,1.544216; ...
  18.440,19.1,18.833,15.202,-1.115,2.061201; ...
  22.884,21.8,23.382,18.927, 0.000,2.318731; ...
  27.464,24.4,27.994,22.657, 1.115,2.576063; ...
  31.925,26.4,32.566,26.285, 2.247,2.833763; ...
  36.386,28.0,37.206,29.896, 3.385,3.090255; ...
  40.726,29.3,41.726,33.438, 4.522,3.218702; ...
  45.143,30.1,46.223,36.758, 5.662,3.350465; ...
   0    , 0  , 0    , 0    , 0    ,3.477235; ...
   0    , 0  , 0    , 0    , 0    ,3.606889; ...
   0    , 0  , 0    , 0    , 0    ,3.863592; ...
];

% Get merged MMS data for QC3
% NOTE: data for QC3 is integrated gradient (T)

[I,GL]=ATF2_QC3merge();
[nr,nc]=size(I);

% add QC3 data to qI and qG arrays

[nrow,ncol]=size(qI);
if (nr>nrow) % add rows of zeros to qI and qG
  qI=[qI;zeros(nr-nrow,ncol)];
  qG=[qG;zeros(nr-nrow,ncol)];
elseif (nr<nrow) % add rows of zeros to I and GL
  I=[I;zeros(nrow-nr,nc)];
  GL=[GL;zeros(nrow-nr,nc)];
end
Nmms=[Nmms,nr,nr];
qI=[qI,I];
qG=[qG,GL];

qleff=[0.198745,0.084339,0.19861,0.07867,0.07867,0.43,0.475,0.19849];

% NOTE: convert integrated strengths to gradients for standard handling

qG(:,6)=qG(:,6)/qleff(6); % 4Q17 [QF1FF]
qG(:,7)=qG(:,7)/qleff(7); % QC3 [QD0FF]

% number of main and trim turns

Nm=[24,26,1,1,1,1,1,1]; % main coil turns (only first 2 are valid)
Nt=[20,20,0,0,0,0,0,0]; % trim coil turns (only first 2 are valid)

% quadrupole family types

qtype=[ ...
  1; 2; 5; 3; 3; 3; 3; 3; 3; 5; ...
  4; 3; 3; 5; 8; 8; 5; 8; 1; 1; ...
  1; 5; 8; 8; 5; 8; 8; 4; 4; 8; ...
  8; 8; 8; 8; 8; 8; 8; 8; 8; 8; ...
  8; 8; 8; 8; 8; 8; 8; 8; 8; 6; ...
  7; ...
];

% polarities (zero means bipolar)
% NOTE: the FF matching quads are polarity-switchable ... I>0 means K>0

qsgn=[...
  -1; 1; 0; 1;-1; 1; 1;-1; 1; 0; ...
   1;-1; 1; 0;-1; 1; 0;-1; 1;-1; ...
   1; 0;-1; 1; 0;-1; 1;-1; 1; 1; ...
   1; 1; 1; 1; 1;-1;-1; 1; 1;-1; ...
   1;-1; 1; 1;-1;-1; 1;-1;-1; 1; ...
  -1; ...
];

nt=qtype(magInd);
if (nt==8)
  [ivec,bvec]=QEAmms(magInd,qleff(8)); % bvec is T/m
else
  ivec=qI(1:Nmms(nt),nt);
  bvec=qG(1:Nmms(nt),nt); % bvec is T/m
end
bvec=bvec*qleff(nt); % bvec is T
unipolar=(qsgn(magInd)~=0);
nt_ratio=Nt(nt)/Nm(nt);
if (nt_ratio==0)
  nt_ratio=[]; % no powered trim winding
end

% scale to the length of this element

id=findcells(BEAMLINE,'Name',BEAMLINE{indx}.Name);
Ltot=0;
for n=1:length(id)
  Ltot=Ltot+BEAMLINE{id(n)}.L;
end
bvec=bvec*(BEAMLINE{indx}.L/Ltot);

% QS1X and QS2X are "reverse" polarity

if (strcmp(BEAMLINE{indx}.Name,'QS1X')||strcmp(BEAMLINE{indx}.Name,'QS2X'))
  bvec=-bvec;
end
% ==============================================================================
function [I,GL]=ATF2_QC3merge()
%
% [I,GL]=ATF2_QC3merge();
%
% Merge low current (50-150 amps) and high current (80-220 amps) MMS data
% for QD0FF
%
% OUTPUTs:
%
%   Imms  = merged current values (amps)
%   GLmms = merged integrated strength values (T)

ImmsLo=[ ...
    0.0      ; ...
   50.0106160; ...
   60.0276535; ...
   70.0855160; ...
   80.0717910; ...
   90.1688160; ...
  100.0348160; ...
  110.0881035; ...
  120.0671535; ...
  130.1366660; ...
  140.1541410; ...
  145.2155410; ...
  150.2204785; ...
];
GLmmsLo=[ ...
  0.0      ; ...
  2.2493081; ...
  2.6988262; ...
  3.1507880; ...
  3.5993780; ...
  4.0525394; ...
  4.4949355; ...
  4.9453874; ...
  5.3913758; ...
  5.8405937; ...
  6.2871601; ...
  6.5118793; ...
  6.7351056; ...
];
ImmsHi=[ ...
   80.062675 ; ...
  130.1277625; ...
  150.2228125; ...
  170.188325 ; ...
  190.2619   ; ...
  200.0862125; ...
  210.1445875; ...
  220.1833125; ...
];
GLmmsHi=[ ...
  3.6005798; ...
  5.8492512; ...
  6.7474992; ...
  7.63769  ; ...
  8.5285362; ...
  8.9629504; ...
  9.4059275; ...
  9.8470287; ...
];

I=[ImmsLo;ImmsHi(4:end,:)];
GL=[GLmmsLo;GLmmsHi(4:end,:)];
% ==============================================================================
function [ivec,bvec,unipolar] = I2B_ATF2_SEXT(indx)
global BEAMLINE

% the order of the sextupoles

% magNames={'SF6FF' 'SF5FF' 'SD4FF' 'SF1FF' 'SD0FF' ...
%   'SK4FF' 'SK3FF' 'SK2FF' 'SK1FF'};
% ========= May 2013, SF5 and SD4 swapped =============
magNames={'SF6FF' 'SD4FF' 'SF5FF' 'SF1FF' 'SD0FF' ...
  'SK4FF' 'SK3FF' 'SK2FF' 'SK1FF'};
magInd=find(ismember(magNames,BEAMLINE{indx}.Name));
if (~magInd),error('Mag Name not found (%s)',BEAMLINE{indx}.Name),end
magInd=min(6,magInd); % all skew sextupoles share a single set of MMS data

% there are 9 ATF2 sextupoles: each of the 5 normal sextupoles has its own
% MMS data; the 4 skew sextupoles share a single set of MMS data

%    1 = SF6 (1.625SX3.53 #02 (SLC))
%    2 = SF5 (1.625SX3.53 #03 (SLC))
%    3 = SD4 (1.625SX3.53 #01 (SLC))
%    4 = SF1 (2.13S3.00 SX2 (FFTB))
%    5 = SD0 (2.13S3.00 SK2 (FFTB))
%    6 = SK* (KEKB skew sextupole)

% MMS data for sextupoles (I is amp, G is integrated strength, T/m);
% data for all but SK1 courtesy of Cherrill Spencer (SLAC); data for SK1
% generated using dG/dI=0.33638 (T/m)/amp (from Mika Masuzawa)

Nmms=[17;17;17;5;5;3];

sI=[ ...
%  SF6    SF5    SD4    SF1      SD0     SK*
   0    , 0    , 0    , 0      , 0      ,-20; ...
   0.398, 0.396, 0.397, 2.9621 , 2.9615 ,  0; ...
   0.829, 0.829, 0.83 , 5.95714, 5.9561 , 20; ...
   1.399, 1.399, 1.399, 8.9525 , 8.9508 ,  0; ...
   1.832, 1.832, 1.832,11.96446,11.96224,  0; ...
   2.301, 2.302, 2.302, 0      , 0      ,  0; ...
   4.972, 4.97 , 4.971, 0      , 0      ,  0; ...
   7.443, 7.442, 7.443, 0      , 0      ,  0; ...
   9.912, 9.909, 9.911, 0      , 0      ,  0; ...
  15.069,15.065,15.068, 0      , 0      ,  0; ...
  19.962,19.962,19.962, 0      , 0      ,  0; ...
  24.962,24.961,24.962, 0      , 0      ,  0; ...
  30.055,30.055,30.055, 0      , 0      ,  0; ...
  35.066,35.066,35.066, 0      , 0      ,  0; ...
  39.908,39.912,39.91 , 0      , 0      ,  0; ...
  45.059,45.063,45.061, 0      , 0      ,  0; ...
 %49.999,50.004,50.002, 0      , 0      ,  0; ...
  50.0  ,50.004,50.002, 0      , 0      ,  0; ...
];

sGL=[ ...
%   SF6         SF5         SD4        SF1     SD0       SK*
    0        ,  0        ,  0        , 0      , 0      ,-6.7276; ...
    2.2498363,  1.9905675,  2.0174219, 8.67672, 8.59829, 0     ; ...
    3.1659143,  2.9116955,  2.9416777,17.585  ,17.47928, 6.7276; ...
    4.3772233,  4.126823 ,  4.1611131,26.46082,26.33882, 0     ; ...
    5.292689 ,  5.0464729,  5.0835106,35.31407,35.13744, 0     ; ...
    6.2995431,  6.0563734,  6.096839 , 0      , 0      , 0     ; ...
   12.1380489, 11.896674 , 11.9431827, 0      , 0      , 0     ; ...
   17.5544078, 17.3217305, 17.3880054, 0      , 0      , 0     ; ...
   23.0261077, 22.7888383, 22.8818389, 0      , 0      , 0     ; ...
   34.5769838, 34.3304156, 34.4413399, 0      , 0      , 0     ; ...
   45.6399439, 45.3629794, 45.5144949, 0      , 0      , 0     ; ...
   57.0404355, 56.6435797, 56.8469279, 0      , 0      , 0     ; ...
   68.6272702, 68.0544545, 68.3276361, 0      , 0      , 0     ; ...
   79.9906013, 79.2441692, 79.5503455, 0      , 0      , 0     ; ...
   90.9077093, 90.0244698, 90.3472626, 0      , 0      , 0     ; ...
  102.4637655,101.4447267,101.7951883, 0      , 0      , 0     ; ...
  113.4777391,112.3449583,112.7209785, 0      , 0      , 0     ; ...
];

stype=[1,2,3,4,5,6];
ssgn=[+1,-1,+1,-1,+1,0];

nt=stype(magInd);
ivec=sI(1:Nmms(nt),nt);
bvec=sGL(1:Nmms(nt),nt); % bvec is T/m
unipolar=(ssgn(magInd)~=0);

% scale to the length of this element

id=findcells(BEAMLINE,'Name',BEAMLINE{indx}.Name);
Ltot=0;
for n=1:length(id)
  Ltot=Ltot+BEAMLINE{id(n)}.L;
end
bvec=bvec*(BEAMLINE{indx}.L/Ltot);
% ==============================================================================
function conv=I2B_ATF2_CORR(indx)
global BEAMLINE

% the order of correctors:

magNames={ ...
  'ZH100RX','ZH101RX','ZX1X','ZH1X','ZH2X','ZX2X','ZX3X','ZH3X','ZH4X', ...
  'ZH5X','ZH6X','ZH7X','ZH8X','ZH9X','ZH10X','ZH1FF', ...
  'ZV100RX','ZV1X','ZV2X','ZV3X','ZV4X','ZV5X','ZV6X','ZV7X','ZV8X','ZV9X', ...
  'ZV10X','ZV11X','ZV1FF'};

magInd=find(ismember(magNames,BEAMLINE{indx}.Name));
if (~magInd),error('Mag Name not found (%s)',BEAMLINE{indx}.Name),end

% there are 6 types of ATF2 corrector:

%    1 = Tecno model 59521 (horizontal)
%    2 = Tecno model 59789 (horizontal)
%    3 = Tecno model 58283 (horizontal)
%    4 = NKK type CH (horizontal)
%    5 = ZV100R (vertical; modified Tecno model 58284)
%    6 = Tecno model 58284 (vertical)
%    7 = NKK type CV (horizontal and vertical)
%    8 = BSHI-H trim (horizontal)
%    9 = BSHI-C trim (horizontal)

% B vs I slopes and effective lengths
% (from: ATF$MAG:MAG_KI_Z_TECNO_ZH82982.FOR
%        ATF$MAG:MAG_KI_Z_TECNO_ZH82983.FOR
%        ATF$MAG:MAG_KI_Z_TECNO_H58283.FOR
%        ATF$MAG:MAG_KI_Z_NKK_CH.FOR
%        ATF$MAG:MAG_KI_ZV100R.FOR
%        ATF$MAG:MAG_KI_Z_TECNO_V58284.FOR
%        ATF$MAG:MAG_KI_Z_NKK_CV.FOR)

cbvi=[12.5e-3;5.9e-3;102.8e-4;108.0e-4;8.5e-3;111.5e-4;112.0e-4]; % T/amp
cleff=[0.24955;0.1679;0.11455;0.11921;0.13874;0.128141;0.1248];   % m

% corrector types (by inspection)

ctype=[1;2;8;3;3;8;9;3;3;7;3;3;7;7;4;4;5;6;6;6;6;6;6;6;6;7;6;6;6];

% corrector polarities

% NOTE: csign is established with an observation and some assumptions:
%   - observation: the magnet polarity when driven by a positive current
%   - assumption: the Flight Simulator software uses an upright locally
%     right-handed coordinate system (+X is LEFT as you look downbeam;
%     +Y is UP)
%   - assumption: a positive kick in Lucretia kicks the beam to positive
%     X or Y

csign=[1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1; ...    % XCORs
       1;-1;-1;-1;-1;-1;-1;-1;-1;-1;-1;-1;-1]; % YCORs

nt=ctype(magInd);
if (nt>7) % bend trims are handled elsewhere
  conv=csign(magInd);
else
  conv=csign(magInd)*cbvi(nt)*cleff(nt); % T-m/amp
  clight=299792458; % speed of light (m/s)
  Cb=1e9/clight; % T-m/GeV
  brho=Cb*BEAMLINE{indx}.P; % rigidity constant (T-m/GeV)
  conv=conv/brho; % radian/amp
end
% ==============================================================================
function [CUR,FLD,unipolar,nt_ratio]=I2B_DIPOLE_TRIM_COR(indx)
global BEAMLINE

unipolar=0;CUR=[];FLD=[];

HTYPE={'ZX1X','ZX2X','BH1X','BH2X'};
CTYPE={'ZX3X','BH3X'};

if (regexp(BEAMLINE{indx}.Name,'^BH\dX','once'))
  mname=BEAMLINE{indx}.Name(1:end-1); % BH1X or BH2X or BH3X
  trim=0;
else
  mname=BEAMLINE{indx}.Name; % ZX1X or ZX2X or ZX3X
  trim=1;
end

if (ismember(mname,HTYPE))
  bleff=0.79771; % m
  Nm=72; % number of main coil turns
  Nt=20; % number of trim coil turns
  CUR=zeros(17,1); % amps
  FLD=zeros(17,1); % Tesla
  CUR( 1)=  0.0;FLD( 1)=0.0000;
  CUR( 2)= 50.0;FLD( 2)=0.1390;
  CUR( 3)=100.0;FLD( 3)=0.2764;
  CUR( 4)=150.0;FLD( 4)=0.4122;
  CUR( 5)=200.0;FLD( 5)=0.5470;
  CUR( 6)=250.0;FLD( 6)=0.6806;
  CUR( 7)=300.0;FLD( 7)=0.8133;
  CUR( 8)=320.0;FLD( 8)=0.8655;
  CUR( 9)=340.0;FLD( 9)=0.9168;
  CUR(10)=360.0;FLD(10)=0.9658;
  CUR(11)=380.0;FLD(11)=1.0100;
  CUR(12)=400.0;FLD(12)=1.0490;
  CUR(13)=420.0;FLD(13)=1.0810;
  CUR(14)=440.0;FLD(14)=1.1085;
  CUR(15)=460.0;FLD(15)=1.1335;
  CUR(16)=480.0;FLD(16)=1.1566;
  CUR(17)=500.0;FLD(17)=1.1750; % add by interpolation
elseif ismember(mname,CTYPE)
  bleff=1.34296; % m
  Nm=80; % number of main coil turns
  Nt=20; % number of trim coil turns
  CUR=zeros(17,1); % amps
  FLD=zeros(17,1); % Tesla
  CUR( 1)=  0.0;FLD( 1)=0.0000;
  CUR( 2)= 50.0;FLD( 2)=0.1560;
  CUR( 3)=100.0;FLD( 3)=0.3095;
  CUR( 4)=150.0;FLD( 4)=0.4632;
  CUR( 5)=200.0;FLD( 5)=0.6145;
  CUR( 6)=250.0;FLD( 6)=0.7631;
  CUR( 7)=300.0;FLD( 7)=0.9092;
  CUR( 8)=320.0;FLD( 8)=0.9635;
  CUR( 9)=340.0;FLD( 9)=1.0110;
  CUR(10)=360.0;FLD(10)=1.0492;
  CUR(11)=380.0;FLD(11)=1.0806;
  CUR(12)=400.0;FLD(12)=1.1079;
  CUR(13)=420.0;FLD(13)=1.1313;
  CUR(14)=440.0;FLD(14)=1.1530;
  CUR(15)=460.0;FLD(15)=1.1727;
  CUR(16)=480.0;FLD(16)=1.1911;
  CUR(17)=500.0;FLD(17)=1.2000;
end % if H or C type

nt_ratio=Nt/Nm;
CUR=CUR';
FLD=FLD'*bleff; % integrated strength for full magnet (T-m)

if (trim)
  clight=299792458; % speed of light (m/s)
  Cb=1e9/clight; % T-m/GeV
  brho=Cb*BEAMLINE{indx}.P; % rigidity constant (T-m/GeV)
  FLD=FLD/brho; % radian/amp
else
  FLD=FLD/2; % assume bends split exactly in half ... each half of the bend
             %  gets half the integrated strength
end
% ==============================================================================
function [ivec,bvec,unipolar,nt_ratio]=I2B_ATF_RINGQUAD(indx)
global BEAMLINE

quadNames={'QF1R','QF2R','QM1R','QM2R','QM3R','QM4R','QM5R',...
           'QM6R','QM7R','QM8R','QM9R','QM10R','QM11R','QM12R',...
           'QM13R','QM14R','QM15R','QM16R','QM17R','QM18R','QM19R',...
           'QM20R','QM21R','QM22R','QM23R'};
nTrims=[28,26,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2];
magNames={};
for iquad=1:length(quadNames)
  for itrim=1:nTrims(iquad)
    magNames{length(magNames)+1}=[quadNames{iquad},num2str(itrim)];
  end % for itrim
end % for iquad
magInd=find(ismember(magNames,BEAMLINE{indx}.Name));
if (~magInd),error('Mag Name not found (%s)',BEAMLINE{indx}.Name),end

% there are 11 DR quadrupole types:

%    1 = Hitachi type 1
%    2 = Hitachi type 2
%    3 = Hitachi type 3
%    4 = Hitachi type 4
%    5 = Tokin type 3325
%    6 = Tokin type 3582
%    7 = Tokin type 3393
%    8 = Tokin type 3581
%    9 = QEA-13 (IHEP type D32L180 # 13) ... in series with QEA-14
%   10 = QEA-19 (IHEP type D32L180 # 19) ... in series with QEA-17
%   11 = QEA-21 (IHEP type D32L180 # 21) ... in series with QEA-18

% magnet data  (I is amp, G is T/m, leff is m)
% (from: ATF$MAG:MAG_KI_Q_HITACHI_1.FOR
%        ATF$MAG:MAG_KI_Q_HITACHI_2.FOR
%        ATF$MAG:MAG_KI_Q_HITACHI_3.FOR
%        ATF$MAG:MAG_KI_Q_HITACHI_4.FOR
%        ATF$MAG:MAG_KI_Q_TOKIN_3325.FOR
%        ATF$MAG:MAG_KI_Q_TOKIN_3582.FOR
%        ATF$MAG:MAG_KI_Q_TOKIN_3393.FOR
%        ATF$MAG:MAG_KI_Q_TOKIN_3581.FOR
%        mainamp50AmaxAbs.txt (M.Masuzawa))

Nmms=[11,11,11,11,11,11,8,11,16,16,16];

qI=[ ...
    0.0,  0.0,  0.0,  0.0,  0.00,  0.0,  0.0,  0.0,0.0000000,0.0000000,0.0000000; ...
   14.2, 10.2, 20.4, 20.2,100.00, 50.0, 20.0, 50.0,3.3333001,3.3333001,3.3333001; ...
   28.6, 20.2, 40.0, 40.2,200.00,100.0, 40.0, 80.0,6.6666999,6.6666999,6.6666999; ...
   42.2, 30.2, 60.0, 60.4,300.00,150.0, 60.0,100.0,10.000000,10.000000,10.000000; ...
   56.0, 40.0, 80.2, 80.4,400.00,170.0, 80.0,130.0,13.333000,13.333000,13.333000; ...
   70.2, 50.0,100.8,100.0,417.24,190.0,100.0,150.0,16.667000,16.667000,16.667000; ...
   84.2, 60.2,120.2,120.2,434.48,210.0,120.0,170.0,20.000000,20.000000,20.000000; ...
   98.2, 70.2,140.0,140.2,451.72,220.0,139.0,190.0,23.333000,23.333000,23.333000; ...
  112.2, 80.2,160.2,160.2,468.97,230.0,  0  ,210.0,26.667000,26.667000,26.667000; ...
  126.0, 90.2,180.2,180.0,503.45,240.0,  0  ,230.0,30.000000,30.000000,30.000000; ...
  140.2,100.2,200.2,200.4,512.07,245.0,  0  ,245.0,33.333000,33.333000,33.333000; ...
    0  ,  0  ,  0  ,  0  ,  0   ,  0  ,  0  ,  0  ,36.667000,36.667000,36.667000; ...
    0  ,  0  ,  0  ,  0  ,  0   ,  0  ,  0  ,  0  ,40.000000,40.000000,40.000000; ...
    0  ,  0  ,  0  ,  0  ,  0   ,  0  ,  0  ,  0  ,43.333000,43.333000,43.333000; ...
    0  ,  0  ,  0  ,  0  ,  0   ,  0  ,  0  ,  0  ,46.667000,46.667000,46.667000; ...
    0  ,  0  ,  0  ,  0  ,  0   ,  0  ,  0  ,  0  ,50.000000,50.000000,50.000000; ...
];

qG=[ ...
   0.000, 0.000, 0.000, 0.000, 0.00, 0.0, 0.0, 0.0,0         ,0         ,0         ; ...
   2.350, 3.832, 5.628, 4.612,10.80, 7.6, 3.4, 7.5,0.32538533,0.33133936,0.32236442; ...
   4.712, 7.646,11.109, 9.201,21.45,14.9, 6.8,11.8,0.63208896,0.63420987,0.62648714; ...
   6.981,11.498,16.656,13.858,32.05,22.2,10.0,14.8,0.94148004,0.93937200,0.93356729; ...
   9.256,15.202,22.243,18.440,42.65,25.0,13.2,19.1,1.2529521 ,1.2468803 ,1.2427585 ; ...
  11.604,18.927,27.866,22.884,44.70,28.0,16.6,21.8,1.5639043 ,1.5545483 ,1.5514039 ; ...
  13.892,22.657,33.122,27.464,46.65,30.8,19.8,24.4,1.8741443 ,1.8627108 ,1.8593891 ; ...
  16.167,26.285,38.487,31.925,48.10,32.1,23.0,26.4,2.1841798 ,2.1716371 ,2.1672122 ; ...
  18.410,29.896,43.828,36.386,49.70,33.3, 0  ,28.0,2.5015478 ,2.4888871 ,2.4827976 ; ...
  20.629,33.438,48.575,40.726,52.35,34.2, 0  ,29.3,2.8104367 ,2.7979240 ,2.7898581 ; ...
  22.898,36.758,51.971,45.143,52.80,34.7, 0  ,30.1,3.1178350 ,3.1058538 ,3.0956228 ; ...
   0    , 0    , 0    , 0    , 0   , 0  , 0  , 0  ,3.4247386 ,3.4134352 ,3.4008279 ; ...
   0    , 0    , 0    , 0    , 0   , 0  , 0  , 0  ,3.7313569 ,3.7210751 ,3.7056735 ; ...
   0    , 0    , 0    , 0    , 0   , 0  , 0  , 0  ,4.0364571 ,4.0274525 ,4.0088239 ; ...
   0    , 0    , 0    , 0    , 0   , 0  , 0  , 0  ,4.3408098 ,4.3334475 ,4.3110003 ; ...
   0    , 0    , 0    , 0    , 0   , 0  , 0  , 0  ,4.6463265 ,4.6411600 ,4.6146355 ; ...
];

qleff=[0.078765,0.07867,0.19847,0.198745,0.19886,0.202628,0.07890677,0.084339,0.19849,0.19849,0.19849];

% NOTE: data for types 9-11 (IHEP type D32L180 ("QEA")) are integrated strength
%       in Tesla ... divide by leff for standard handling

for n=9:11
  qG(:,n)=qG(:,n)/qleff(n);
end

% number of main and trim turns

Nm=[17,39,29,24,11,26,17,26,49,49,49]; % main coil turns
Nt=[20,20,20,20,20,20,20,20,20,20,20]; % trim coil turns

% quadrupole family types
% (from: ATF$MAG:MAG_KI_MAIN.FOR)
qtype=[1;5;3;2;2;3;4;4;7;7;4;4;6;9;10;11;6;4;4;4;4;3;2;2;3;8];

% "Kubo" fudge factors
% (NOTE: set fudge factors to zero ... who knows what they are now)

qfudge=[ ...
  0.0; ... % QF1R
  0.0; ... % QF2R
  0.0; ... % QM1R
  0.0; ... % QM2R
  0.0; ... % QM3R
  0.0; ... % QM4R
  0.0; ... % QM5R
  0.0; ... % QM6R
  0.0; ... % QM7R
  0.0; ... % QM8R
  0.0; ... % QM9R
  0.0; ... % QM10R
  0.0; ... % QM11R
  0.0; ... % QM12R
  0.0; ... % QM13R
  0.0; ... % QM14R
  0.0; ... % QM15R
  0.0; ... % QM16R
  0.0; ... % QM17R
  0.0; ... % QM18R
  0.0; ... % QM19R
  0.0; ... % QM20R
  0.0; ... % QM21R
  0.0; ... % QM22R
  0.0; ... % QM23R
  0.0; ... % QM7AR
];

% quadrupole family pointers
idq=[ ...
   1*ones(28,1); ... % QF1R
   2*ones(26,1); ... % QF2R
   3*ones( 2,1); ... % QM1R
   4*ones( 2,1); ... % QM2R
   5*ones( 2,1); ... % QM3R
   6*ones( 2,1); ... % QM4R
   7*ones( 2,1); ... % QM5R
   8*ones( 2,1); ... % QM6R
         [26;9]; ... % QM7R
  10*ones( 2,1); ... % QM8R
  11*ones( 2,1); ... % QM9R
  12*ones( 2,1); ... % QM10R
  13*ones( 2,1); ... % QM11R
  14*ones( 2,1); ... % QM12R
  15*ones( 2,1); ... % QM13R
  16*ones( 2,1); ... % QM14R
  17*ones( 2,1); ... % QM15R
  18*ones( 2,1); ... % QM16R
  19*ones( 2,1); ... % QM17R
  20*ones( 2,1); ... % QM18R
  21*ones( 2,1); ... % QM19R
  22*ones( 2,1); ... % QM20R
  23*ones( 2,1); ... % QM21R
  24*ones( 2,1); ... % QM22R
  25*ones( 2,1); ... % QM23R
];

% form I and B lookup table vectors

unipolar=1;
m=idq(magInd); % quad family (1-26)
t=qtype(m); % quad type (1-11)
ivec=qI(1:Nmms(t),t);
G=qG(1:Nmms(t),t);
G=G/(1+qfudge(m));
GL=G*qleff(t); % integrated gradient for full magnet (T)
nt_ratio=Nt(t)/Nm(t);

% scale to the length of this element

id=findcells(BEAMLINE,'Name',BEAMLINE{indx}.Name);
Ltot=0;
for n=1:length(id)
  Ltot=Ltot+BEAMLINE{id(n)}.L;
end
bvec=GL*(BEAMLINE{indx}.L/Ltot);
% ==============================================================================
function [ivec,bvec,unipolar]=I2B_ATF_RINGSEXT(indx)
global BEAMLINE

% SF1R1-34 SD1R1-34

magNames={'SF1R','SD1R'};
magInd=find(ismember(magNames,BEAMLINE{indx}.Name));
if (~magInd),error('Mag Name not found (%s)',BEAMLINE{indx}.Name),end

ivec=[ ...
    0.00000,  0.07500, 25.01000, 49.98700, 75.00100, ...
  124.99800,149.99100,175.00000,200.00700,224.98399, ...
  249.98300,275.00299,299.98700,324.99500,350.00000, ...
  374.98999,399.99600];

sB=[ ...
  0.00000,0.00050,0.00986,0.01959,0.02927, ...
  0.04816,0.05737,0.06623,0.07398,0.07947, ...
  0.08339,0.08645,0.08891,0.09093,0.09261, ...
  0.09404,0.09527];

r0=0.006; % position of Hall probe (distance from magnet center; m)
sG=2*sB/r0^2; % T/m^2
sleff=0.07077; % effective length ... POISSON estimate (m)
sGL=sG*sleff; % integrated gradient for full magnet (T/m)
unipolar=1;

% scale to the length of this element

id=findcells(BEAMLINE,'Name',BEAMLINE{indx}.Name);
Ltot=0;
for n=1:length(id)
  Ltot=Ltot+BEAMLINE{id(n)}.L;
end
bvec=sGL*(BEAMLINE{indx}.L/Ltot);
% ==============================================================================
function [ivec,bvec,unipolar]=I2B_ATF_RINGMULT(indx)
global BEAMLINE
% Deal with skew quads (windings on sext's)
% SQSF1-34 SQSD1-34
skewNames={'SQSF','SQSD'};
nTrims=[34,34];
magNames={};
for iskew=1:length(skewNames)
  for itrim=1:nTrims(iskew)
    magNames{length(magNames)+1}=[skewNames{iskew},num2str(itrim)];
  end % for itrim
end % for iskew
csign=[ones(1,34),-ones(1,34)]; % we don't know the actual polarities, but
                                % SFs and SDs should have opposite sign
magInd=find(ismember(magNames,BEAMLINE{indx}.Name));
if (~magInd),error('Mag Name not found (%s)',BEAMLINE{indx}.Name),end

% compute skew strengths
% (NOTE: calibration is 0.001 /m/amp @ 1.28 GeV, per Kubo 03dec09)

clight=299792458; % speed of light (m/sec)
Cb=1e9/clight; % rigidity constant (T-m/GeV)
E0=1.28; % reference energy (GeV)
brho0=Cb*E0; % reference rigidity (T-m)
glvi=0.001*brho0; % T/amp
bvec=csign(magInd)*glvi; % *BEAMLINE{indx}.L;
ivec=[];
unipolar=0;
% ==============================================================================
function [bconv]=I2B_ATF_RINGCOR(indx)
global BEAMLINE

% the order of correctors:

%   ZH1R, ... ,ZH14R,ZH16R, ... ,ZH48R (ZH15R is missing)
%   ZV1R, ... ,ZV15R,ZV17R, ... ,ZV51R (ZV16R is missing)
%   ZH100R,ZH101R,ZH102R,ZV100R (specials)

% corrector names and polarities

% NOTE: csign is established with an observation and some assumptions:
%   - observation: the magnet polarity when driven by a positive current
%   - assumption: the Flight Simulator software uses an upright locally
%     right-handed coordinate system (+X is LEFT as you look downbeam;
%     +Y is UP)
%   - assumption: a positive kick in Lucretia kicks the beam to positive
%     X or Y

magNames={};
csign=[];

corInds=[1:14,16:48]; % XCORs
for icor=1:length(corInds)
  magNames{length(magNames)+1}=['ZH',num2str(corInds(icor)),'R'];
end % for icor
csign=[csign,ones(size(corInds))]; % I>0:top=S:LEFT ... normal polarity

corInds=[1:15,17:51]; % YCORs
for icor=1:length(corInds)
  magNames{length(magNames)+1}=['ZV',num2str(corInds(icor)),'R'];
end % for icor
csign=[csign,-ones(size(corInds))]; % I>0:left=S:DOWN ... reverse polarity

magNames{length(magNames)+1}='ZH100R';csign=[csign,1]; % I>0:top=S:LEFT
magNames{length(magNames)+1}='ZH101R';csign=[csign,1]; % I>0:top=S:LEFT
magNames{length(magNames)+1}='ZH102R';csign=[csign,1]; % I>0:top=S:LEFT
magNames{length(magNames)+1}='ZV100R';csign=[csign,1]; % I>0:left=N:UP

magInd=find(ismember(magNames,BEAMLINE{indx}.Name));
if (~magInd),error('Mag Name not found (%s)',BEAMLINE{indx}.Name),end

% there are 6 types of DR corrector:

%    1 = NKK type CH (horizontal)
%    2 = Tecno model 58283 (horizontal)
%    3 = NKK type CV (vertical)
%    4 = Tecno model 58928 (vertical)
%    5 = Tecno model 58284 (vertical)
%    6 = Tecno model 82982 (horizontal)
%    7 = Tecno model 82983 (horizontal)
%    8 = ZV100R (vertical; modified)

% B vs I slopes and effective lengths
% (from: ATF$MAG:MAG_KI_Z_NKK_CH.FOR
%        ATF$MAG:MAG_KI_Z_TECNO_H58283.FOR
%        ATF$MAG:MAG_KI_Z_NKK_CV.FOR
%        ATF$MAG:MAG_KI_Z_TECNO_V58928.FOR
%        ATF$MAG:MAG_KI_Z_TECNO_V58284.FOR
%        ATF$MAG:MAG_KI_Z_TECNO_ZH82982.FOR
%        ATF$MAG:MAG_KI_Z_TECNO_ZH82983.FOR
%        ATF$MAG:MAG_KI_ZV100R.FOR)

cbvi=[108.0e-4;102.8e-4;112.0e-4;111.5e-4;102.0e-4;12.5e-3;5.9e-3;8.5e-3]; % T/amp
cleff=[0.11921;0.11455;0.1248;0.128141;0.172;0.24955;0.1679;0.13874];      % m

% corrector types
% (from: ATF$MAG:MAG_KI_MAIN.FOR)

ctype=[ ...
  1;1;1;1;1;1;1;1;1;2;2;2;2;2;  1;1;1;1;1;1;1;1;1;1; ... %ZH1-25
  1;1;1;1;1;1;1;1;2;2;2;2;2;2;1;1;1;1;1;1;1;1;1; ...     %ZH26-48
  3;3;3;3;3;3;3;3;3;4;4;4;4;4;5;  3;3;3;3;3;3;3;3;3; ... %ZV1-25
  3;3;3;3;3;3;3;3;5;5;5;4;4;4;4;4;5;3;3;3;3;3;3;3;3; ... %ZV26-50
  3; ...                                                 %ZV51
  6;7;3;8];                                              %ZH100-102,ZV100

% compute conversion factor

nt=ctype(magInd);
conv=csign(magInd)*cbvi(nt)*cleff(nt); % T-m/amp
clight=299792458; % speed of light (m/s)
Cb=1e9/clight; % rigidity constant (T-m/GeV)
brho=Cb*BEAMLINE{indx}.P; % rigidity (T-m)
bconv=conv/brho; % radian/amp
% ==============================================================================
function [Ilow,Ihigh]=ATF2magLims(name,low,high)
% Hardwired operating current limits for ATF2 power supplies ... will not be
% needed once EPICS high and low PVs are populated

persistent C S

% hardwired list of ATF2 power supply current limits
if (isempty(S))
  C={ ...
%     name     Ilow  Ihigh
%     ------ ------ ------
     'BH1XA',   0.0, 500.0; ...
     'BH2XA',   0.0, 500.0; ...
     'BH3XA',   0.0, 500.0; ...
     'QM6RX',   0.0, 200.0; ...
     'QM7RX',   0.0, 200.0; ...
     'QS1X' ,  -5.0,   5.0; ...
     'QF1X' ,   0.0, 100.0; ...
     'QD2X' ,   0.0, 100.0; ...
     'QF3X' ,   0.0, 100.0; ...
     'QF4X' ,   0.0, 100.0; ...
     'QD5X' ,   0.0, 100.0; ...
     'QF6X' ,   0.0, 100.0; ...
     'QS2X' ,  -5.0,   5.0; ...
     'QF7X' ,   0.0, 100.0; ...
     'QD8X' ,   0.0, 100.0; ...
     'QF9X' ,   0.0, 100.0; ...
     'QK1X' , -20.0,  20.0; ...
     'QK2X' , -20.0,  20.0; ...
     'QD14X',   0.0, 100.0; ...
     'QK3X' , -20.0,  20.0; ...
     'QK4X' , -20.0,  20.0; ...
     'QD20X',   0.0, 100.0; ...
     'QF21X',   0.0, 100.0; ...
     'ZX1X' ,  -5.0,   5.0; ...
     'ZH1X' , -10.0,  10.0; ...
     'ZH2X' , -10.0,  10.0; ...
     'ZX2X' , -10.0,  10.0; ...
     'ZX3X' , -10.0,  10.0; ...
     'ZH3X' , -10.0,  10.0; ...
     'ZH4X' , -10.0,  10.0; ...
     'ZH5X' , -10.0,  10.0; ...
     'ZH6X' , -10.0,  10.0; ...
     'ZH7X' , -10.0,  10.0; ...
     'ZH8X' , -10.0,  10.0; ...
     'ZH9X' , -10.0,  10.0; ...
     'ZH10X',  -5.0,   5.0; ...
     'ZH1FF',  -5.0,   5.0; ...
     'ZV1X' , -10.0,  10.0; ...
     'ZV2X' , -10.0,  10.0; ...
     'ZV3X' , -10.0,  10.0; ...
     'ZV4X' , -10.0,  10.0; ...
     'ZV5X' , -10.0,  10.0; ...
     'ZV6X' , -10.0,  10.0; ...
     'ZV7X' , -10.0,  10.0; ...
     'ZV8X' , -10.0,  10.0; ...
     'ZV9X' , -10.0,  10.0; ...
     'ZV10X', -10.0,  10.0; ...
     'ZV11X',  -5.0,   5.0; ...
     'ZV1FF',  -5.0,   5.0; ...
  };
  S=cell2struct(C,{'Name','Ilow','Ihigh'},2);
end

% if limits for this power supply aren't in the hardwired list, simply pass
% the input values back for stanrard handling
Ilow=low;
Ihigh=high;

% get power supply limits, if defined
id=ismember({S.Name},name);
if any(id)
  Ilow=S(id).Ilow;
  Ihigh=S(id).Ihigh;
end
% ==============================================================================
function [lowval,highval]=ZXmagLims(ips,low,high) %#ok<DEFNU>
 % Compute kick angle limits for ZX*X bend trim correctors

global BEAMLINE FL

name0=strtok(FL.HwInfo.PS(ips).pvname{1}{1},':');
id0=findcells(BEAMLINE,'Name',strcat(name0,'*'));
A0=abs(sum(cellfun(@(x) x.Angle(1),BEAMLINE(id0))));
I0=interp1(FL.HwInfo.PS(ips).conv(2,:),FL.HwInfo.PS(ips).conv(1,:),A0,'spline');
Ilow=I0+FL.HwInfo.PS(ips).nt_ratio*low;
Alow=interp1(FL.HwInfo.PS(ips).conv(1,:),FL.HwInfo.PS(ips).conv(2,:),Ilow,'spline');
lowval=Alow-A0;
Ihigh=I0+FL.HwInfo.PS(ips).nt_ratio*high;
Ahigh=interp1(FL.HwInfo.PS(ips).conv(1,:),FL.HwInfo.PS(ips).conv(2,:),Ihigh,'spline');
highval=Ahigh-A0;
% ==============================================================================

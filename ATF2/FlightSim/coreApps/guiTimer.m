function guiTimer(guiarg)
% guiTimer(obj, evennt, guiarg)
% Function to associate with GUI originated timers
% guiarg={handles,obj1,obj2,...}
% pass name of GUI objects to call each time timer runs
% (e.g. 'edit1', 'popupmenu1' etc)
% create with e.g. (from within GUI OpeningFunction) :
% t=timer('TimerFcn',@(x,y)guiTimer({handles,'edit2'}),'StartDelay',1,'ExecutionMode','fixedSpacing','BusyMode','drop','Period',1);

handles=guiarg{1};
eval(sprintf('fh=@%s;',get(handles.figure1,'Name')));
for iarg=1:length(guiarg)-1
  fh(sprintf('%s_Callback',guiarg{1+iarg}),handles.(guiarg{1+iarg}),[],handles);
end
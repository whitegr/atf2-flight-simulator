function varargout = FlBeamModelGUI(varargin)
% FLBEAMMODELGUI MATLAB code for FlBeamModelGUI.fig
%      FLBEAMMODELGUI, by itself, creates a new FLBEAMMODELGUI or raises the existing
%      singleton*.
%
%      H = FLBEAMMODELGUI returns the handle to a new FLBEAMMODELGUI or the handle to
%      the existing singleton*.
%
%      FLBEAMMODELGUI('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in FLBEAMMODELGUI.M with the given input arguments.
%
%      FLBEAMMODELGUI('Property','Value',...) creates a new FLBEAMMODELGUI or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before FlBeamModelGUI_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to FlBeamModelGUI_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help FlBeamModelGUI

% Last Modified by GUIDE v2.5 04-Apr-2011 11:16:32

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @FlBeamModelGUI_OpeningFcn, ...
                   'gui_OutputFcn',  @FlBeamModelGUI_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before FlBeamModelGUI is made visible.
function FlBeamModelGUI_OpeningFcn(hObject, eventdata, handles, varargin) %#ok<*INUSL>
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to FlBeamModelGUI (see VARARGIN)

% Choose default command line output for FlBeamModelGUI
handles.output = handles;

% Update handles structure
guidata(hObject, handles);

% Reset FlBeamModel
FlBeamModel('clear');

% Fill data fields from FlBeamModel routine
[~, data]=FlBeamModel('GetData');
[~, l]=FlBeamModel('GetLocal');
set(handles.edit1,'String',num2str(data.nbpmave));
set(handles.edit2,'String',num2str(data.lookbackTime));
if any(l.drBpms)
  set(handles.togglebutton8,'Value',any(data.useBpms & l.drBpms))
end
set(handles.togglebutton9,'Value',any(data.useBpms & l.extBpms))
set(handles.togglebutton10,'Value',any(data.useBpms & l.ffsBpms))
set(handles.radiobutton4,'Value',0)
set(handles.radiobutton5,'Value',0)
set(handles.radiobutton6,'Value',0)
if data.useEmitData==0
  set(handles.radiobutton4,'Value',1)
elseif data.useEmitData==1
  set(handles.radiobutton5,'Value',1)
else
  set(handles.radiobutton6,'Value',1)
end
set(handles.edit5,'String',num2str(l.optimParam.MaxIter))

% Update bpm and magnet lists
bpmsel('get',handles);
magsel('get',handles);

% GUI timer (keep data age up to date)
t=timer('TimerFcn',@(x,y) guiTimer({handles,'edit2'}),'StartDelay',1,'ExecutionMode','fixedSpacing','BusyMode','drop','Period',1);
set(handles.figure1,'UserData',t);
start(t);


% --- Outputs from this function are returned to the command line.
function varargout = FlBeamModelGUI_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on button press in checkbox1.
function checkbox1_Callback(hObject, eventdata, handles) %#ok<*INUSD,*DEFNU>
% hObject    handle to checkbox1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of checkbox1


% --- Executes on button press in checkbox2.
function checkbox2_Callback(hObject, eventdata, handles)
% hObject    handle to checkbox2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

if get(hObject,'Value')
  set(handles.checkbox3,'Value',0)
  set(handles.checkbox4,'Value',0)
else
  set(hObject,'Value',1)
end


% --- Executes on button press in checkbox3.
function checkbox3_Callback(hObject, eventdata, handles)
% hObject    handle to checkbox3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

if get(hObject,'Value')
  set(handles.checkbox2,'Value',0)
  set(handles.checkbox4,'Value',0)
else
  set(hObject,'Value',1)
end


% --- Executes on button press in checkbox4.
function checkbox4_Callback(hObject, eventdata, handles)
% hObject    handle to checkbox4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

if get(hObject,'Value')
  set(handles.checkbox2,'Value',0)
  set(handles.checkbox3,'Value',0)
else
  set(hObject,'Value',1)
end


% --- Executes on button press in pushbutton1.
function pushbutton1_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

stat = FlBeamModel('DoDispFit');
if stat{1}~=1
  errordlg(stat{2},'Dispersion Fit Error')
  return
end
doplot(handles);

% --- Executes on button press in pushbutton2.
function pushbutton2_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
stat = FlBeamModel('DoMagOffsetFit');
if stat{1}~=1
  errordlg(stat{2},'Mag Offset Fit Error')
  return
end
doplot(handles);

% --- Perform orbit fit
function pushbutton3_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% Setup progress bar
global FL
gui_active(1);
pbh=progressbar([],0,'Orbit Fit Progress');
drawnow('expose')
[~, l]=FlBeamModel('GetLocal');
FL.optimMonitor.pbh=pbh;
FL.optimMonitor.maxIter=l.optimParam.MaxIter;
stat = FlBeamModel('DoOrbitFit',pbh);
FL=rmfield(FL,'optimMonitor');
if ~isempty(pbh)
  progressbar(pbh,-1);
end
if stat{1}~=1
  if strfind(stat{2},'undefined values')
    errordlg('Probable error with beam tracking, lattice too badly mismatched to get an initial beam through','Orbit Fit Error')
  else
    errordlg(stat{2},'Orbit Fit Error')
  end
  return
end
doplot(handles);

% --- Executes on button press in pushbutton4.
function pushbutton4_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on button press in radiobutton4.
function radiobutton4_Callback(hObject, eventdata, handles)
% hObject    handle to radiobutton4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

if get(hObject,'Value')
  FlBeamModel('UseEmitData',0);
  set(handles.radiobutton5,'Value',0)
  set(handles.radiobutton6,'Value',0)
else
  set(hObject,'Value',1)
end

% --- Executes on button press in radiobutton5.
function radiobutton5_Callback(hObject, eventdata, handles)
% hObject    handle to radiobutton5 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

if get(hObject,'Value')
  FlBeamModel('UseEmitData',1)
  set(handles.radiobutton4,'Value',0)
  set(handles.radiobutton6,'Value',0)
else
  set(hObject,'Value',1)
end


% --- Executes on button press in radiobutton6.
function radiobutton6_Callback(hObject, eventdata, handles)
% hObject    handle to radiobutton6 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

if get(hObject,'Value')
  FlBeamModel('UseEmitData',2)
  set(handles.radiobutton4,'Value',0)
  set(handles.radiobutton5,'Value',0)
else
  set(hObject,'Value',1)
end

% --- Executes on button press in togglebutton4.
function togglebutton4_Callback(hObject, eventdata, handles)
% hObject    handle to togglebutton4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of togglebutton4


% --- Executes on button press in togglebutton5.
function togglebutton5_Callback(hObject, eventdata, handles)
% hObject    handle to togglebutton5 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of togglebutton5


% --- Executes on button press in togglebutton6.
function togglebutton6_Callback(hObject, eventdata, handles)
% hObject    handle to togglebutton6 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of togglebutton6


% --- Executes on selection change in listbox2.
function listbox2_Callback(hObject, eventdata, handles)
% hObject    handle to listbox2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns listbox2 contents as cell array
%        contents{get(hObject,'Value')} returns selected item from listbox2


% --- Executes during object creation, after setting all properties.
function listbox2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to listbox2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: listbox controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit2_Callback(hObject, eventdata, handles)
% hObject    handle to edit2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
persistent lastStr
if isempty(lastStr) || ~isequal(lastStr,get(hObject,'String'))
  FlBeamModel('SetLookbackTime',str2double(get(hObject,'String')));
  lastStr=get(hObject,'String');
end
setlastmeas(handles);


% --- Executes during object creation, after setting all properties.
function edit2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in radiobutton2.
function radiobutton2_Callback(hObject, eventdata, handles)
% hObject    handle to radiobutton2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
if get(hObject,'Value')
  set(handles.radiobutton3,'Value',0)
  bpmsel('get',handles);
else
  set(hObject,'Value',1)
end


% --- Executes on button press in radiobutton3.
function radiobutton3_Callback(hObject, eventdata, handles)
% hObject    handle to radiobutton3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
if (get(hObject,'Value'))
  set(handles.radiobutton2,'Value',0)
  bpmsel('get',handles);
else
  set(hObject,'Value',1)
end


% --- Executes on button press in togglebutton1.
function togglebutton1_Callback(hObject, eventdata, handles)
% hObject    handle to togglebutton1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of togglebutton1


% --- Executes on button press in togglebutton2.
function togglebutton2_Callback(hObject, eventdata, handles)
% hObject    handle to togglebutton2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of togglebutton2


% --- Executes on button press in togglebutton3.
function togglebutton3_Callback(hObject, eventdata, handles)
% hObject    handle to togglebutton3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of togglebutton3


% --- Executes on selection change in listbox1.
function listbox1_Callback(hObject, eventdata, handles)
% hObject    handle to listbox1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

bpmsel('set',handles);


% --- Executes during object creation, after setting all properties.
function listbox1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to listbox1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: listbox controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% # bpm pulses to use
function edit1_Callback(hObject, eventdata, handles)
% hObject    handle to edit1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

FlBeamModel('SetNBPMAve',str2double(get(hObject,'String')))


% --- Executes during object creation, after setting all properties.
function edit1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in togglebutton7.
function togglebutton7_Callback(hObject, eventdata, handles)
% hObject    handle to togglebutton7 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

bpmsel('get',handles);



function edit3_Callback(hObject, eventdata, handles)
% hObject    handle to edit3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

[~, data]=FlBeamModel('GetData');
if data.donefit
  doplot(handles)
end


% --- Executes during object creation, after setting all properties.
function edit3_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function stat=bpmsel(cmd,handles)
[stat l]=FlBeamModel('GetLocal'); if stat{1}~=1; return; end;
[stat data]=FlBeamModel('GetData'); if stat{1}~=1; return; end;
switch cmd
  case 'get'
    bpmList=true(1,length(l.allBpms));
    if any(l.drBpms)
      bpmList(l.drBpms)=get(handles.togglebutton8,'Value');
    end
    bpmList(l.extBpms)=get(handles.togglebutton9,'Value');
    bpmList(l.ffsBpms)=get(handles.togglebutton10,'Value');
    set(handles.listbox1,'String',l.allBpmNames(bpmList))
    set(handles.listbox1,'UserData',bpmList)
    if get(handles.radiobutton2,'Value')
      set(handles.listbox1,'Value',find(data.useBpms(bpmList)))
    elseif get(handles.radiobutton3,'Value')
      set(handles.listbox1,'Value',find(data.useDispData(bpmList)))
    end
  case 'set'
    useBpms=false(1,length(l.allBpms));
    bpmList=get(handles.listbox1,'UserData');
    bpmsel=false(1,length(find(bpmList))); bpmsel(get(handles.listbox1,'Value'))=true;
    useBpms(bpmList)=bpmsel;
    if get(handles.radiobutton2,'Value') || get(handles.togglebutton7,'Value')
      stat = FlBeamModel('UseBpms',useBpms); if stat{1}~=1; return; end;
    end
    if get(handles.radiobutton3,'Value') || get(handles.togglebutton7,'Value')
      stat = FlBeamModel('UseDispData',useBpms); if stat{1}~=1; return; end;
    end
  otherwise
    error('No such command in bpmsel')
end

function stat=magsel(cmd,handles)
[stat l]=FlBeamModel('GetLocal'); if stat{1}~=1; return; end;
[stat data]=FlBeamModel('GetData'); if stat{1}~=1; return; end;
switch cmd
  case 'get'
    magList=true(1,length(l.clName));
    magList(l.drcl)=get(handles.togglebutton8,'Value');
    magList(l.extcl)=get(handles.togglebutton9,'Value');
    magList(l.ffscl)=get(handles.togglebutton10,'Value');
    set(handles.listbox2,'String',l.clName(magList))
    set(handles.listbox2,'UserData',magList)
    set(handles.listbox2,'Value',find(data.useMagOffsets(magList)))
  case 'set'
    useMag=false(1,length(l.clName));
    magList=get(handles.listbox2,'UserData');
    magsel=false(1,length(find(magList))); magsel(get(handles.listbox2,'Value'))=true;
    useMag(magList)=magsel;
    stat = FlBeamModel('UseMagOffsets',useMag); if stat{1}~=1; return; end;
  otherwise
    error('No such command in bpmsel')
end

% --- Executes on button press in togglebutton8.
function togglebutton8_Callback(hObject, eventdata, handles)
% hObject    handle to togglebutton8 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
[~, l]=FlBeamModel('GetLocal');
if ~any(l.drBpms)
  errordlg('No DR BPM INSTRs available','No DR BPMs');
  set(hObject,'Value',0)
  return
end
FlBeamModel('SelectDR',get(hObject,'Value'));
bpmsel('get',handles);
magsel('get',handles);

% --- Executes on button press in togglebutton9.
function togglebutton9_Callback(hObject, eventdata, handles)
% hObject    handle to togglebutton9 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

FlBeamModel('SelectEXT',get(hObject,'Value'));
bpmsel('get',handles);
magsel('get',handles);


% --- Executes on button press in togglebutton10.
function togglebutton10_Callback(hObject, eventdata, handles)
% hObject    handle to togglebutton10 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

FlBeamModel('SelectFFS',get(hObject,'Value'));
bpmsel('get',handles);
magsel('get',handles);

% --- Plotting function
function doplot(handles)
global BEAMLINE

% Get data structure
[~, data]=FlBeamModel('GetData');

% Get tracked positions at every BEAMLINE element
[stat xtrack ytrack strack] = FlBeamModel('EleTrack');
if stat{1}~=1; errordlg(stat{2},'EleTrack Error'); end

% Display orbit fit at point request?
sreq=[];
if ~strcmp(get(handles.edit3,'String'),'---')
  ind=findcells(BEAMLINE,'Name',get(handles.edit3,'String'));
  if ~isempty(ind)
    ireq=ismember(strack,BEAMLINE{ind}.S);
    if any(ireq)
      ireq=find(ireq,1);
      sreq=strack(ireq);
      xreq=xtrack(ireq);
      yreq=ytrack(ireq);
      set(handles.text1,'String',num2str(xreq*1e6))
      set(handles.text2,'String',num2str(yreq*1e6))
    end
  end
end

% Orbit plotting
if get(handles.checkbox2,'Value')
  % Plot BPM measurements
  errorbar(handles.axes1,data.bpmdata.s,data.bpmdata.x.*1e6,data.bpmdata.dx.*1e6,'r*');
  hold(handles.axes1,'on')
  errorbar(handles.axes1,data.bpmdata.s,data.bpmdata.y.*1e6,data.bpmdata.dy.*1e6,'b*');
  % Plot tracked orbit
  plot(strack,xtrack.*1e6,'r')
  plot(strack,ytrack.*1e6,'b')
  xlabel(handles.axes1,'S / m')
  ylabel(handles.axes1,'x/y orbit / um')
  grid(handles.axes1,'on')
  if ~isempty(sreq)
    plot(sreq,xreq*1e6,'ro','MarkerSize',12,'LineWidth',2)
    plot(sreq,xreq*1e6,'r+','MarkerSize',12,'LineWidth',2)
    plot(sreq,yreq*1e6,'bo','MarkerSize',12,'LineWidth',2)
    plot(sreq,yreq*1e6,'b+','MarkerSize',12,'LineWidth',2)
  end
  hold(handles.axes1,'off')
elseif get(handles.checkbox3,'Value')
  
end

function setlastmeas(handles)
global FL
if ~isfield(FL,'lastDispMeas') || isempty(FL.lastDispMeas)
  FL.lastDispMeas=datevec('1/1/1970');
end
set(handles.text5,'String',sprintf('%s (%d hrs)',datestr(FL.lastDispMeas),etime(clock,FL.lastDispMeas)/3600))
if (etime(clock,FL.lastDispMeas)/3600) > str2double(get(handles.edit2,'String'))
  set(handles.text5,'ForegroundColor','red')
  set(handles.radiobutton8,'Value',1)
  radiobutton8_Callback(handles.radiobutton4,[],handles);
  set(handles.radiobutton7,'Enable','off')
else
  set(handles.text5,'ForegroundColor','black')
  set(handles.radiobutton8,'Enable','on')
end
[~, ts]=FlCA('lcaGet','mOTR:procData:projemitx');
if isnan(ts); ts=0; end;
elapsedTime=now*24-epicsts2mat(ts)*24;
set(handles.text6,'String',sprintf('%s (%d hrs)',datestr(epicsts2mat(ts)),elapsedTime))
if elapsedTime>str2double(get(handles.edit2,'String'))
  set(handles.text6,'ForegroundColor','red')
  set(handles.radiobutton4,'Value',1)
  radiobutton4_Callback(handles.radiobutton4,[],handles);
  set(handles.radiobutton5,'Enable','off')
  set(handles.radiobutton6,'Enable','off')
else
  set(handles.text6,'ForegroundColor','black')
  set(handles.radiobutton5,'Enable','on')
  set(handles.radiobutton6,'Enable','on')
end


% --- Executes on button press in radiobutton7.
function radiobutton7_Callback(hObject, eventdata, handles)
% hObject    handle to radiobutton7 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

if get(hObject,'Value')
  set(handles.radiobutton8,'Value',0)
else
  set(hObject,'Value',1)
end
FlBeamModel('DispSelect',2);


% --- Executes on button press in radiobutton8.
function radiobutton8_Callback(hObject, eventdata, handles)
% hObject    handle to radiobutton8 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

if get(hObject,'Value')
  set(handles.radiobutton7,'Value',0)
else
  set(hObject,'Value',1)
end
FlBeamModel('DispSelect',1);

% --- Executes when user attempts to close figure1.
function figure1_CloseRequestFcn(hObject, eventdata, handles)
% hObject    handle to figure1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
try
  t=get(handles.figure1,'UserData');
  stop(t);
  guiCloseFn('FlBeamModelGui',handles);
catch
  delete(hObject)
end



function edit5_Callback(hObject, eventdata, handles)
% hObject    handle to edit5 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

val=str2double(get(hObject,'String'));
if ~isnan(val) && val>0 && val <10000
  FlBeamModel('SetMaxIter',val);
else
  [~, l]=FlBeamModel('GetLocal');
  set(hObject,'String',num2str(l.optimParam.MaxIter));
end


% --- Executes during object creation, after setting all properties.
function edit5_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit5 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in pushbutton5.
function pushbutton5_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton5 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

FlBeamModel('ResetInitial');

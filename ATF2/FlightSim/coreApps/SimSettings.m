function varargout = SimSettings(varargin)
% SIMSETTINGS M-file for SimSettings.fig
%      SIMSETTINGS, by itself, creates a new SIMSETTINGS or raises the existing
%      singleton*.
%
%      H = SIMSETTINGS returns the handle to a new SIMSETTINGS or the handle to
%      the existing singleton*.
%
%      SIMSETTINGS('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in SIMSETTINGS.M with the given input arguments.
%
%      SIMSETTINGS('Property','Value',...) creates a new SIMSETTINGS or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before SimSettings_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to SimSettings_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help SimSettings

% Last Modified by GUIDE v2.5 27-Aug-2010 18:07:11

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @SimSettings_OpeningFcn, ...
                   'gui_OutputFcn',  @SimSettings_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before SimSettings is made visible.
function SimSettings_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to SimSettings (see VARARGIN)
global FL

% Set callback function for button groups
set(handles.uipanel1,'SelectionChangeFcn','SimSettings(''uipanel1_change_callback'',gcbo,[],guidata(gcbo))');

% --- Get current settings
% Tracking mode
if isfield(FL,'simTrackMode')
  if strcmp(FL.simTrackMode,'macro')
    set(handles.radiobutton2,'Value',1);
  elseif strcmp(FL.simTrackMode,'single')
    set(handles.radiobutton1,'Value',1);
  elseif strcmp(FL.simTrackMode,'sparse')
    set(handles.radiobutton3,'Value',1);
  end % if macro mode set
else % default to sparse particle tracking
  set(handles.radiobutton3,'Value',1);
end % if FL.simTrackMode
if isfield(FL,'useDynErr')
  set(handles.checkbox2,'Value',FL.useDynErr)
else
  set(handles.checkbox2,'Value',0);
end % if dyn err
if isfield(FL,'useStatErr')
  set(handles.checkbox1,'Value',FL.useStatErr)
else
  set(handles.checkbox1,'Value',0);
end % if dyn err

% Set status of OTR simulation
pid=getPid('mOTR',false);
if isempty(pid) || ~isfield(FL,'IOC') || ~isfield(FL.IOC,'otr_pid') || ~isequal(pid{1}{1},FL.IOC.otr_pid)
  set(handles.pushbutton9,'String','OTR Sim OFF')
  set(handles.pushbutton9,'BackgroundColor','red')
else
  set(handles.pushbutton9,'String','OTR Sim Running')
  set(handles.pushbutton9,'BackgroundColor','green')
end

% Choose default command line output for SimSettings
handles.output = handles;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes SimSettings wait for user response (see UIRESUME)
% uiwait(handles.figure1);

function uipanel1_change_callback(hObject,eventdata,handles)
global FL

if get(handles.radiobutton2,'Value')
  FL.simTrackMode='macro'; FL.simBeamID=1;
  disp('Macro Particle Tracking Mode Enabled...')
elseif get(handles.radiobutton1,'Value')
  FL.simTrackMode='single'; FL.simBeamID=2;
  disp('Single Particle Tracking Mode Enabled...')
elseif get(handles.radiobutton3,'Value')
  FL.simTrackMode='sparse'; FL.simBeamID=3;
  disp('Sparse Particle Tracking Mode Enabled...')
end % if macro tracking

% --- Outputs from this function are returned to the command line.
function varargout = SimSettings_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on button press in pushbutton1.
function pushbutton1_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

guiCloseFn('SimSettings',handles);


% --- Executes on button press in checkbox1.
function checkbox1_Callback(hObject, eventdata, handles)
% hObject    handle to checkbox1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global FL

% Hint: get(hObject,'Value') returns toggle state of checkbox1
if get(hObject,'Value')
  stat=SimApplyErrors('static');
  FL.useStatErr=1;
else
  stat=SimApplyErrors('static','zero');
  FL.useStatErr=0;
end % if checkbox checked
if stat{1}~=1
  errordlg(stat{2},'Error applying static errors')
end % if err

% --- Executes on button press in pushbutton2.
function pushbutton2_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
stat=SimApplyErrors('static');
if stat{1}~=1
  errordlg(stat{2},'Error applying static errors')
end % if err

% --- Executes on button press in pushbutton3.
function pushbutton3_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global FL

FL.Gui.SimSettings_staticErr=SimSettings_staticErr;

% --- Executes on button press in checkbox2.
function checkbox2_Callback(hObject, eventdata, handles)
% hObject    handle to checkbox2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global FL

% Apply dynamic error flag
if get(hObject,'Value')
  stat=SimApplyErrors('dynamic');
  if stat{1}~=1
    errordlg(stat{2},'Error applying dynamic errors')
  end % if err
  FL.useDynErr=1;
else
  FL.useDynErr=0;
end % if checkbox checked


% --- Executes on button press in pushbutton4.
function pushbutton4_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on button press in pushbutton5.
function pushbutton5_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton5 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global FL

FL.Gui.SimSettings_dynamicErr=SimSettings_dynamicErr;

% --- Executes on button press in pushbutton6.
function pushbutton6_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton6 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global FL

FL.Gui.SimBunchParams=SimBunchParams;

% --- Executes on button press in pushbutton7.
function pushbutton7_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton7 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global FL

Beam1 = MakeBeam6DGauss(FL.SimModel.Initial,10001,5,0) ;
Beam0 = makeSingleBunch(Beam1);
Beam2 = MakeBeam6DSparse( FL.SimModel.Initial, 5, 1, 11 );
FL.SimBeam = {Beam1, Beam0, Beam2};


function edit1_Callback(hObject, eventdata, handles)
% hObject    handle to edit1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit1 as text
%        str2double(get(hObject,'String')) returns contents of edit1 as a double


% --- Executes during object creation, after setting all properties.
function edit1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
randseed=str2double(get(hObject,'String'));
if ~isnumeric(randseed) || isnan(randseed)
  errordlg('Must supply numeric entry','SimSettings: rand seed error')
  return
end % if not a number
rand('twister',round(randseed));
randn('state',round(randseed));

% --- Executes on button press in pushbutton8.
function pushbutton8_Callback(hObject, eventdata, handles) %#ok<*INUSL,*DEFNU>
% hObject    handle to pushbutton8 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
randseed=round(rand*1e4);
rand('twister',randseed); %#ok<*RAND>
randn('state',randseed);
set(handles.edit1,'String',num2str(randseed));


% --- Executes when user attempts to close figure1.
function figure1_CloseRequestFcn(hObject, eventdata, handles)
% hObject    handle to figure1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

guiCloseFn('SimSettings',handles);


% --- Executes on button press in pushbutton9.
function pushbutton9_Callback(hObject, eventdata, handles) %#ok<*INUSD>
% hObject    handle to pushbutton9 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global FL

if strcmp(get(hObject,'String'),'OTR Sim OFF')
  FL.IOC.otr_pid=startOtrIoc;
  if ~FL.IOC.otr_pid
    errordlg('OTR Simulated EPICS IOC failed to start','OTR SIM Error')
    return
  end
  set(hObject,'String','OTR Sim Running')
  set(hObject,'BackgroundColor','green')
  lcaPut('mOTR:cam1:SizeX',1280); lcaPut('mOTR:cam1:SizeY',960);
  lcaPut('mOTR:cam2:SizeX',1280); lcaPut('mOTR:cam2:SizeY',960);
  lcaPut('mOTR:cam3:SizeX',1280); lcaPut('mOTR:cam3:SizeY',960);
  lcaPut('mOTR:cam4:SizeX',1280); lcaPut('mOTR:cam4:SizeY',960);
else
  evalc(['!kill ',FL.IOC.otr_pid]);
  FL.IOC=rmfield(FL.IOC,'otr_pid');
  set(hObject,'String','OTR Sim OFF')
  set(hObject,'BackgroundColor','red')
end


function pid = startOtrIoc
global FL

% remove ioc startup file
cmd=['rm -f ',fullfile(FL.atfDir,'control-software','epics-3.14.10','ioc-mOTR','iocBoot','iocmOTR','dbList.txt')];system(cmd);

epicsHostArch=getenv('EPICS_HOST_ARCH');

% Form IOC start script file
sh1='#!/bin/bash';
sh2=['cd ',fullfile(FL.atfDir,'control-software','epics-3.14.10','ioc-mOTR','iocBoot','iocmOTR')];
sh3=['export FS_PORT=',num2str(str2double(getenv('FS_PORT'))+6)]; % EPICS Port to run this IOC on
fid=fopen('startmOTRIOC.sh','w');
if isempty(getenv('FS_PORT'))
  sh4=[fullfile('..','..','bin',epicsHostArch,'mOTR'),' ./st-simnoport.cmd'];
  fprintf(fid,'%s\n%s\n%s\n',sh1,regexprep(sh2,'\','/'),regexprep(sh4,'\','/'));
else
  sh4=[fullfile('..','..','bin',epicsHostArch,'mOTR'),' ./st-sim.cmd'];
  fprintf(fid,'%s\n%s\n%s\n%s\n',sh1,regexprep(sh2,'\','/'),sh3,regexprep(sh4,'\','/'));
end
fclose(fid);
% Command IOC to start, and get process ID
cmd='chmod +x startmOTRIOC.sh';system(cmd);
cmd='xterm -e ./startmOTRIOC.sh &';system(cmd);
pid=getPid('mOTR');
if isempty(pid) 
  pid=0; return
end
pid=pid{1}{1};
fprintf('mOTR PID=%s\n',pid)

#!/bin/bash
# Compilation of Floodland ATF2 Flight Simulator program

MEXNAME=mexmaci64
LABCADIR=/Users/glenwhite/ATF2/control-software/epics-3.14.8/support/labca_3_1-mac

# Generate complete list of m files and mex files to give to the matlab compiler
rm -f compList.m
touch compList.m
if [ $MEXNAME == mexmaci64 ]
then
	find * -name "*.${MEXNAME}" -print0 | xargs -0 -I%X echo "%#function %X" >> compList.m
	find * -name "*.m" -print0 | xargs -0 -I%X echo "%#function %X" >> compList.m
	find ../../Lucretia/src/* -name "*.${MEXNAME}" -print0 | xargs -0 -I%X echo "%#function %X" >> compList.m
	find ../../Lucretia/src/* -name "*.m" -print0 | xargs -0 -I%X echo "%#function %X" >> compList.m
        find ${LABCADIR}/* -name "*.m" -print0 | xargs -0 -I%X echo "%#function %X" >> compList.m
        find ${LABCADIR}/* -name "*.${MEXNAME}" -print0 | xargs -0 -I%X echo "%#function %X" >> compList.m
else
        find * -name "*.${MEXNAME}" -printf "%f\n" | sed "s/\.${MEXNAME}//" | xargs -I%X echo "%#function %X" > compList.m
        find * -name "*.m" -printf "%f\n" | sed "s/\.m//" | xargs -I%X echo "%#function %X" >> compList.m
        find ../../Lucretia/src/* -name "*.${MEXNAME}" -printf "%f\n" | sed "s/\.${MEXNAME}//" | xargs -I%X echo "%#function %X" i>> compList.m
        find ../../Lucretia/src/* -name "*.m" -printf "%f\n" | sed "s/\.m//" | xargs -I%X echo "%#function %X" >> compList.m
        find ${LABCADIR}/* -name "*.m" -printf "%f\n" | sed "s/\.m//" | xargs -I%X echo "%#function %X" >> compList.m
        find ${LABCADIR}/* -name "*.${MEXNAME}" -printf "%f\n" | sed "s/\.${MEXNAME}//" | xargs -I%X echo "%#function %X" >> compList.m
fi


# Build
${MATLAB}/bin/matlab -nodesktop -r "mcc -mv coreApps/FlStart.m -a latticeFiles/ATF2lat.mat; exit"

# Copy files to bin directory
rm -rf bin/${EPICS_HOST_ARCH}
mv FlStart* bin/${EPICS_HOST_ARCH}
mv run_FlStart.sh bin/${EPICS_HOST_ARCH}


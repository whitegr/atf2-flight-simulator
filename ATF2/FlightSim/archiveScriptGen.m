%--- Generate xml script for EPICS Channel Archiver

% after modification, do in order:

% 1) run this script in FS 
% 2) cd /home/atf2-fs/ATF2/FlightSim
% 3) sudo killall -9 ArchiveEngine
% 4) mv archiveEngineConfig.xml /home/atf2-fs/ATF2/control-software/epics-3.14.8/archive/
% 5) cd ~/ATF2/control-software/epics-3.14.8/archive/
% 6) rm archive_active.lck
% 7) archive_engine
% DONE !

% arc cell array format:
% arc{N,1} = PV name
% arc{N,2} = archive period (s)
% arc{N,3} = monitor or scan (default scan)
arc={};
% ============================
% ---- IOC-PS
% ============================
for ips=1:38
  prefix=sprintf('PS%d:',ips);
  bprefix=sprintf('BPS%d:',ips);
  arc{end+1,1}=[prefix,'currentDES'];arc{end,2}=1;
%   arc{end+1,1}=[prefix,'current'];arc{end,2}=60;
%   arc{end+1,1}=[prefix,'polarity'];arc{end,2}=60;
  arc{end+1,1}=[prefix,'status1'];arc{end,2}=6;
  arc{end+1,1}=[prefix,'status2'];arc{end,2}=6;
  arc{end+1,1}=[prefix,'status3'];arc{end,2}=6;
  arc{end+1,1}=[prefix,'status4'];arc{end,2}=6;
  arc{end+1,1}=[prefix,'pwrOn'];arc{end,2}=30;
  arc{end+1,1}=[prefix,'pwrOff'];arc{end,2}=30;
  arc{end+1,1}=[prefix,'pwrOn'];arc{end,2}=30;
  arc{end+1,1}=[prefix,'auxI'];arc{end,2}=60;
  arc{end+1,1}=[prefix,'dacI'];arc{end,2}=60;
  arc{end+1,1}=[prefix,'ripI'];arc{end,2}=60;
  arc{end+1,1}=[prefix,'gndI'];arc{end,2}=60;
  arc{end+1,1}=[prefix,'chassisTemp'];arc{end,2}=60;
  arc{end+1,1}=[prefix,'psV'];arc{end,2}=60;
  % arc{end+1,1}=[prefix,'spareV'];arc{end,2}=30;
  % arc{end+1,1}=[prefix,'lastRst'];arc{end,2}=60;arc{end,3}='monitor';
  % arc{end+1,1}=[prefix,'lastOff'];arc{end,2}=60;arc{end,3}='monitor';
  arc{end+1,1}=[bprefix,'pwrOnOff'];arc{end,2}=60;
  arc{end+1,1}=[bprefix,'status'];arc{end,2}=6;
  arc{end+1,1}=[bprefix,'outputVoltage'];arc{end,2}=60;
  %arc{end+1,1}=[prefix,'module:dataValid'];arc{end,2}=6;
  %arc{end+1,1}=[prefix,'module:faultFlags'];arc{end,2}=6;
  %arc{end+1,1}=[prefix,'module:disableFlags'];arc{end,2}=6;
  %arc{end+1,1}=[prefix,'module:currents'];arc{end,2}=60;
end
% ============================
% EXT Stripline BPMs
% ============================
st=1;
bpmnames={'MQF1X' 'MQD2X' 'MQF3X' 'MQF4X' 'MQD5X' 'MQF6X' 'MQF7X' 'MQD8X' ...
  'MQF9X' 'MQF13X' 'MQD14X' 'MQF15X' 'MFB1FF'};
for ibpm=1:length(bpmnames)
  arc{end+1,1}=[bpmnames{ibpm} ':X'];arc{end,2}=st;
  arc{end+1,1}=[bpmnames{ibpm} ':Y'];arc{end,2}=st;
  arc{end+1,1}=[bpmnames{ibpm} ':X2'];arc{end,2}=st;
  arc{end+1,1}=[bpmnames{ibpm} ':Y2'];arc{end,2}=st;
  arc{end+1,1}=[bpmnames{ibpm} ':TMIT'];arc{end,2}=st;
  arc{end+1,1}=[bpmnames{ibpm} ':TMIT2'];arc{end,2}=st;
  arc{end+1,1}=[bpmnames{ibpm} ':BBA_X'];arc{end,2}=120;
  arc{end+1,1}=[bpmnames{ibpm} ':BBA_Y'];arc{end,2}=120;
  arc{end+1,1}=[bpmnames{ibpm} ':SCALE_X'];arc{end,2}=120;
  arc{end+1,1}=[bpmnames{ibpm} ':SCALE_Y'];arc{end,2}=120;
  arc{end+1,1}=[bpmnames{ibpm} ':XGAIN'];arc{end,2}=600;
  arc{end+1,1}=[bpmnames{ibpm} ':YGAIN'];arc{end,2}=600;
%   extBBA_x(ibpm)=lcaGet(sprintf('%s:BBA_X',bpmnames{ibpm}));
%   extBBA_y(ibpm)=lcaGet(sprintf('%s:BBA_Y',bpmnames{ibpm}));
%   extbpmname{ibpm}=bpmnames{ibpm};
end
% ============================
% IOC-QMOV
% ============================
st=30;
for im=1:28
  prefix=sprintf('c1:qmov:m%d:',im);
  arc{end+1,1}=[prefix,'pot1']; arc{end,2}=st;
  arc{end+1,1}=[prefix,'pot2']; arc{end,2}=st;
  arc{end+1,1}=[prefix,'pot3']; arc{end,2}=st;
  arc{end+1,1}=[prefix,'x:pot']; arc{end,2}=st;
  arc{end+1,1}=[prefix,'y:pot']; arc{end,2}=st;
  arc{end+1,1}=[prefix,'tilt:pot']; arc{end,2}=st;
  arc{end+1,1}=[prefix,'x:lvdt']; arc{end,2}=st;
  arc{end+1,1}=[prefix,'y:lvdt']; arc{end,2}=st;
  arc{end+1,1}=[prefix,'tilt:lvdt']; arc{end,2}=st;
  arc{end+1,1}=[prefix,'x:set']; arc{end,2}=st;
  arc{end+1,1}=[prefix,'y:set']; arc{end,2}=st;
  arc{end+1,1}=[prefix,'tilt:set']; arc{end,2}=st;
  arc{end+1,1}=[prefix,'lvdt1']; arc{end,2}=st;
  arc{end+1,1}=[prefix,'lvdt2']; arc{end,2}=st;
  arc{end+1,1}=[prefix,'lvdt3']; arc{end,2}=st;
  arc{end+1,1}=[prefix,'lvdt1:raw']; arc{end,2}=st;
  arc{end+1,1}=[prefix,'lvdt2:raw']; arc{end,2}=st;
  arc{end+1,1}=[prefix,'lvdt3:raw']; arc{end,2}=st;
  arc{end+1,1}=[prefix,'x:xps']; arc{end,2}=st;
  arc{end+1,1}=[prefix,'y:xps']; arc{end,2}=st;
  arc{end+1,1}=[prefix,'tilt:xps']; arc{end,2}=st;
end
% POT REF V
arc{end+1,1}='c1:qmov:diagsam:ch1'; arc{end,2}=60;
arc{end+1,1}='c1:qmov:diagsam:ch2'; arc{end,2}=60;
arc{end+1,1}='c1:qmov:diagsam:ch3'; arc{end,2}=60;
arc{end+1,1}='c1:qmov:diagsam:ch4'; arc{end,2}=60;
arc{end+1,1}='c1:qmov:diagsam:ch5'; arc{end,2}=60;
arc{end+1,1}='c1:qmov:diagsam:ch6'; arc{end,2}=60;

% ===========================
% EXT Kicker
% ===========================
arc{end+1,1}='EXT_KICKER:HVcountRead'; arc{end,2}=60;

% ===========================
% CBPM IOC
% ===========================
cavinst=[findcells(INSTR,'Type','ccav') findcells(INSTR,'Type','scav')];
st=1;
for icav=cavinst
  if strcmp(BEAMLINE{INSTR{icav}.Index}.Name,'MPIP')
    arc{end+1,1}='M-PIPx:pos'; arc{end,2}=st;
    arc{end+1,1}='M-PIPy:pos'; arc{end,2}=st;
    arc{end+1,1}='M-PIPx:bbaOffset'; arc{end,2}=30;
    arc{end+1,1}='M-PIPy:bbaOffset'; arc{end,2}=30;
    bpmnames=regexprep(BEAMLINE{INSTR{icav}.Index}.Name,'^M(.+)','$1');
    cBBA_name{icav}=bpmnames;
    cBBA_x(icav)=lcaGet('M-PIPx:bbaOffset');
    cBBA_y(icav)=lcaGet('M-PIPy:bbaOffset');
  elseif strcmp(BEAMLINE{INSTR{icav}.Index}.Name,'IPBPMA')
%    arc{end+1,1}='IPAx:pos'; arc{end,2}=st;
%    arc{end+1,1}='IPAx:bbaOffset'; arc{end,2}=30;
%    arc{end+1,1}='IPAy:pos'; arc{end,2}=st;
%    arc{end+1,1}='IPAy:bbaOffset'; arc{end,2}=30;
  elseif strcmp(BEAMLINE{INSTR{icav}.Index}.Name,'IPBPMB')
%    arc{end+1,1}='IPBx:pos'; arc{end,2}=st;
%    arc{end+1,1}='IPBx:bbaOffset'; arc{end,2}=30;
%    arc{end+1,1}='IPBy:pos'; arc{end,2}=st;
%    arc{end+1,1}='IPBy:bbaOffset'; arc{end,2}=30;
  else
    arc{end+1,1}=[regexprep(BEAMLINE{INSTR{icav}.Index}.Name,'^M(.+)','$1') 'x:pos']; arc{end,2}=st;
    arc{end+1,1}=[regexprep(BEAMLINE{INSTR{icav}.Index}.Name,'^M(.+)','$1') 'y:pos']; arc{end,2}=st;
    arc{end+1,1}=[regexprep(BEAMLINE{INSTR{icav}.Index}.Name,'^M(.+)','$1') 'x:amp']; arc{end,2}=st;
    arc{end+1,1}=[regexprep(BEAMLINE{INSTR{icav}.Index}.Name,'^M(.+)','$1') 'y:amp']; arc{end,2}=st;
    arc{end+1,1}=[regexprep(BEAMLINE{INSTR{icav}.Index}.Name,'^M(.+)','$1') 'x:bbaOffset']; arc{end,2}=30;
    arc{end+1,1}=[regexprep(BEAMLINE{INSTR{icav}.Index}.Name,'^M(.+)','$1') 'y:bbaOffset']; arc{end,2}=30;
%     bpmnames=regexprep(BEAMLINE{INSTR{icav}.Index}.Name,'^M(.+)','$1');
%     cBBA_name{icav}=bpmnames;
%     cBBA_x(icav)=lcaGet(sprintf('%sx:bbaOffset',bpmnames));
%     cBBA_y(icav)=lcaGet(sprintf('%sy:bbaOffset',bpmnames));
  end
  
end
arc{end+1,1}='REFC1:amp'; arc{end,2}=st;
%arc{end+1,1}='REFC2:amp'; arc{end,2}=st;
%arc{end+1,1}='REFC3:amp'; arc{end,2}=st;
%arc{end+1,1}='REFIPX1:amp'; arc{end,2}=st;
%arc{end+1,1}='REFIPY1:amp'; arc{end,2}=st;
%arc{end+1,1}='REFIPY2:amp'; arc{end,2}=st;
arc{end+1,1}='REFS1:amp'; arc{end,2}=st;
arc{end+1,1}='REFS2:amp'; arc{end,2}=st;
arc{end+1,1}='REFC1:pha'; arc{end,2}=st;
%arc{end+1,1}='REFC2:pha'; arc{end,2}=st;
%arc{end+1,1}='REFC3:pha'; arc{end,2}=st;
%arc{end+1,1}='REFIPX1:pha'; arc{end,2}=st;
%arc{end+1,1}='REFIPY1:pha'; arc{end,2}=st;
%arc{end+1,1}='REFIPY2:pha'; arc{end,2}=st;
arc{end+1,1}='REFS1:pha'; arc{end,2}=st;
arc{end+1,1}='REFS2:pha'; arc{end,2}=st;
arc{end+1,1}='CDIODE:amp'; arc{end,2}=st;
arc{end+1,1}='CDIODE:t0'; arc{end,2}=st;
%arc{end+1,1}='IPBPMx:signalmax'; arc{end,2}=st;
%arc{end+1,1}='IPBPMy:signalmax'; arc{end,2}=st;


% ===========================
% IPBSM
% ===========================
st=1;
arc{end+1,1}='IPBSM:BGMonitor:State'; arc{end,2}=30;
arc{end+1,1}='IPBSM:BGMonitor:Intensity'; arc{end,2}=st;
arc{end+1,1}='IPBSM:BGMonitor:ADC'; arc{end,2}=st;
arc{end+1,1}='IPBSM:BGMonitor:Pedestal'; arc{end,2}=30;
arc{end+1,1}='IPBSM:BGMonitor:HVRead'; arc{end,2}=30;
arc{end+1,1}='IPBSM:Laser:Mode'; arc{end,2}=30;
arc{end+1,1}='IPBSM:Laser:Path'; arc{end,2}=30;
arc{end+1,1}='IPBSM:Laser:Crossangle'; arc{end,2}=30;
arc{end+1,1}='IPBSM:Laser:Fringepitch'; arc{end,2}=30;
arc{end+1,1}='IPBSM:BG:Energy'; arc{end,2}=st;
%arc{end+1,1}='IPBSM:Signal:Energy'; arc{end,2}=st;
%arc{end+1,1}='IPBSM:TotalEnergy'; arc{end,2}=st;
%IPBSM BG detector pvs
% arc{end+1,1}='IPBSM:DataADC_1L'; arc{end,2}=st;
% arc{end+1,1}='IPBSM:DataADC_2L'; arc{end,2}=st;
% arc{end+1,1}='IPBSM:DataADC_3L'; arc{end,2}=st;
% arc{end+1,1}='IPBSM:DataADC_4L'; arc{end,2}=st;
% arc{end+1,1}='IPBSM:DataADC_1R'; arc{end,2}=st;
% arc{end+1,1}='IPBSM:DataADC_2R'; arc{end,2}=st;
% arc{end+1,1}='IPBSM:DataADC_3R'; arc{end,2}=st;
% arc{end+1,1}='IPBSM:DataADC_4R'; arc{end,2}=st;
% arc{end+1,1}='IPBSM:DataADC_5U'; arc{end,2}=st;
% arc{end+1,1}='IPBSM:DataADC_6D'; arc{end,2}=st;
% arc{end+1,1}='IPBSM:DataADC_7U'; arc{end,2}=st;
% arc{end+1,1}='IPBSM:DataADC_8U'; arc{end,2}=st;
% arc{end+1,1}='IPBSM:DataADC_ICT'; arc{end,2}=st;

% ===========================
% ATF MAGNET IOC
% ===========================
for ips=1:length(FL.HwInfo.PS)
  if ~isempty(FL.HwInfo.PS(ips).pvname)
    arc{end+1,1}=FL.HwInfo.PS(ips).pvname{1}{1}; arc{end,2}=1;
    if length(FL.HwInfo.PS(ips).pvname)>2
      arc{end+1,1}=FL.HwInfo.PS(ips).pvname{3}{1}; arc{end,2}=1;
    end
  end
end
arc{end+1,1}='BS1X:currentWrite'; arc{end,2}=1;
arc{end+1,1}='BS1X_1:currentWrite'; arc{end,2}=1;
arc{end+1,1}='BS3X:currentWrite'; arc{end,2}=1;
% ===========================
% BKG monitor
% ===========================
% for iscope=1:2
%   for ichan=1:4
%     arc{end+1,1}=sprintf('BCKMON:AGILSCOPE%d:CHAN%d',iscope,ichan); arc{end,2}=1;
%   end
% end
% ===========================
% Laserwire stuff
% ===========================
st=20;
% arc{end+1,1}='extlw:cherenkov'; arc{end,2}=st;
% arc{end+1,1}='ip:cham:ver:pos'; arc{end,2}=st;
% arc{end+1,1}='ip:cham:hor:pos'; arc{end,2}=st;
% arc{end+1,1}='extlw:atfcharge'; arc{end,2}=st;
% arc{end+1,1}='extlw:phasev'; arc{end,2}=st;
% arc{end+1,1}='extlw:laser:flt:pos'; arc{end,2}=st;

% ===========================
% mOTR stuff
% ===========================
st=1;
for iotr=1:4
  arc{end+1,1}=sprintf('mOTR:procData%d:sigma11',iotr); arc{end,2}=st; %#ok<*SAGROW>
  arc{end+1,1}=sprintf('mOTR:procData%d:sigma33',iotr); arc{end,2}=st;
  arc{end+1,1}=sprintf('mOTR:procData%d:sigma13',iotr); arc{end,2}=st;
  arc{end+1,1}=sprintf('mOTR:procData%d:sigma11err',iotr); arc{end,2}=st;
  arc{end+1,1}=sprintf('mOTR:procData%d:sigma33err',iotr); arc{end,2}=st;
  arc{end+1,1}=sprintf('mOTR:procData%d:sigma13err',iotr); arc{end,2}=st;
  arc{end+1,1}=sprintf('mOTR:procData%d:x',iotr); arc{end,2}=st;
  arc{end+1,1}=sprintf('mOTR:procData%d:y',iotr); arc{end,2}=st;
  arc{end+1,1}=sprintf('mOTR:procData%d:projx',iotr); arc{end,2}=st;
  arc{end+1,1}=sprintf('mOTR:procData%d:projy',iotr); arc{end,2}=st;
  arc{end+1,1}=sprintf('mOTR:procData%d:xerr',iotr); arc{end,2}=st;
  arc{end+1,1}=sprintf('mOTR:procData%d:yerr',iotr); arc{end,2}=st;
  arc{end+1,1}=sprintf('mOTR:procData%d:projxerr',iotr); arc{end,2}=st;
  arc{end+1,1}=sprintf('mOTR:procData%d:projyerr',iotr); arc{end,2}=st;
end
arc{end+1,1}='mOTR:procData:projemitx'; arc{end,2}=st;
arc{end+1,1}='mOTR:procData:projemity'; arc{end,2}=st;
arc{end+1,1}='mOTR:procData:intemitx'; arc{end,2}=st;
arc{end+1,1}='mOTR:procData:intemity'; arc{end,2}=st;
arc{end+1,1}='mOTR:procData:projemitxerr'; arc{end,2}=st;
arc{end+1,1}='mOTR:procData:projemityerr'; arc{end,2}=st;
arc{end+1,1}='mOTR:procData:intemitxerr'; arc{end,2}=st;
arc{end+1,1}='mOTR:procData:intemityerr'; arc{end,2}=st;
arc{end+1,1}='mOTR:procData:betax'; arc{end,2}=st;
arc{end+1,1}='mOTR:procData:betax_err'; arc{end,2}=st;
arc{end+1,1}='mOTR:procData:betay'; arc{end,2}=st;
arc{end+1,1}='mOTR:procData:betay_err'; arc{end,2}=st;
arc{end+1,1}='mOTR:procData:alphax'; arc{end,2}=st;
arc{end+1,1}='mOTR:procData:alphax_err'; arc{end,2}=st;
arc{end+1,1}='mOTR:procData:alphay'; arc{end,2}=st;
arc{end+1,1}='mOTR:procData:alphay_err'; arc{end,2}=st;

% ===========================
% Temperature sensors
% ===========================
st=30;
arc{end+1,1}='atf-mw100-2:data.CH01'; arc{end,2}=st;
arc{end+1,1}='atf-mw100-2:data.CH02'; arc{end,2}=st;
arc{end+1,1}='atf-mw100-2:data.CH03'; arc{end,2}=st;
arc{end+1,1}='atf-mw100-2:data.CH04'; arc{end,2}=st;
arc{end+1,1}='atf-mw100-2:data.CH05'; arc{end,2}=st;
arc{end+1,1}='atf-mw100-2:data.CH06'; arc{end,2}=st;
arc{end+1,1}='atf-mw100-2:data.CH07'; arc{end,2}=st;
arc{end+1,1}='atf-mw100-2:data.CH08'; arc{end,2}=st;
arc{end+1,1}='atf-mw100-2:data.CH09'; arc{end,2}=st;
arc{end+1,1}='atf-mw100-2:data.CH10'; arc{end,2}=st;
arc{end+1,1}='atf-mw100-2:data.CH11'; arc{end,2}=st;
arc{end+1,1}='atf-mw100-2:data.CH12'; arc{end,2}=st;
arc{end+1,1}='atf-mw100-2:data.CH13'; arc{end,2}=st;
arc{end+1,1}='atf-mw100-2:data.CH14'; arc{end,2}=st;
arc{end+1,1}='atf-mw100-2:data.CH15'; arc{end,2}=st;
arc{end+1,1}='atf-mw100-2:data.CH16'; arc{end,2}=st;
arc{end+1,1}='atf-mw100-2:data.CH17'; arc{end,2}=st;
arc{end+1,1}='atf-mw100-2:data.CH18'; arc{end,2}=st;
arc{end+1,1}='atf-mw100-2:data.CH19'; arc{end,2}=st;
arc{end+1,1}='atf-mw100-2:data.CH20'; arc{end,2}=st;

% ===========================
% PLIC
% ===========================
% arc{end+1,1}='TEST:lwf'; arc{end,2}=1;
% arc{end+1,1}='TEST:rwf'; arc{end,2}=1;

% ===========================
% Form xml file
% ===========================
dtdfile='/home/atf2-fs/ATF2/control-software/epics-3.14.8/extensions/src/ChannelArchiver/Engine/engineconfig.dtd';
header=['<?xml version="1.0" encoding="UTF-8" standalone="no"?>\n' ...
'<!DOCTYPE engineconfig SYSTEM "',dtdfile,'">\n' ...
'<engineconfig>\n' ...
'  <write_period>30</write_period>\n'...
'  <get_threshold>20</get_threshold>\n' ...
'  <file_size>100</file_size>\n' ...
'  <ignored_future>1.0</ignored_future>\n' ...
'  <buffer_reserve>20</buffer_reserve>\n' ...
'  <max_repeat_count>120</max_repeat_count>\n' ...
'  <group>\n' ...
'    <name>Main</name>\n'];
footer=['  </group>\n' ...
        '</engineconfig>\n'];
chfmt=['    <channel><name>%s</name>\n' ...
       '             <period>%d</period><%s/>\n' ...
       '    </channel>\n'];
% --- add in cavity bpm channels
% fid=fopen('bpmArchiveEngine.xml','r');
% a=textscan(fid,'%s','bufsize',1000000);
% fclose(fid);
% c=regexp(a{1}{6},'<channel>');
cbpm=[];
% for ic=1:length(c)-1
%   cbpm=[cbpm a{1}{6}(c(ic):c(ic+1)-1) '\n'];
% end
xmlfile=header;
arcsize=size(arc);
for iarc=1:arcsize(1)
  scan='scan';
  if arcsize(2)==3 && isequal(arc{iarc,3},'monitor'); scan='monitor'; end;
  xmlfile=[xmlfile sprintf(chfmt,arc{iarc,1},arc{iarc,2},scan)];
end
xmlfile=[xmlfile cbpm footer];
fid=fopen('archiveEngineConfig.xml','w');
fprintf(fid,xmlfile);
fclose(fid);


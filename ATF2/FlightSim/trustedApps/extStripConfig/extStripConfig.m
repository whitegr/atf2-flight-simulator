function varargout = extStripConfig(varargin)
% EXTSTRIPCONFIG M-file for extStripConfig.fig
%      EXTSTRIPCONFIG, by itself, creates a new EXTSTRIPCONFIG or raises the existing
%      singleton*.
%
%      H = EXTSTRIPCONFIG returns the handle to a new EXTSTRIPCONFIG or the handle to
%      the existing singleton*.
%
%      EXTSTRIPCONFIG('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in EXTSTRIPCONFIG.M with the given input arguments.
%
%      EXTSTRIPCONFIG('Property','Value',...) creates a new EXTSTRIPCONFIG or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before extStripConfig_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to extStripConfig_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help extStripConfig

% Last Modified by GUIDE v2.5 30-Jan-2014 02:08:05

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @extStripConfig_OpeningFcn, ...
                   'gui_OutputFcn',  @extStripConfig_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before extStripConfig is made visible.
function extStripConfig_OpeningFcn(hObject, eventdata, handles, varargin) %#ok<*INUSL>
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to extStripConfig (see VARARGIN)
global FL

if ~isfield(FL,'mode')
  lcaSetTimeout(0.3);
  lcaSetRetryCount(20);
  lcaSetSeverityWarnLevel(19);
end

% Choose default command line output for extStripConfig
handles.output = handles;

% Update handles structure
guidata(hObject, handles);

% Get N Trains bpm system is setup for
% lcaPut('V2EPICS:iRead','DB_BEAM::BEAMCTRL:NTRAIN');
% ntrain=lcaGet('V2EPICS:i');
set(handles.text12,'String',num2str(lcaGet('STRIPLINE:NTRAIN')))

% Get T_0
% lcaPut('V2EPICS:iRead','DB_TIMING::td2:DR.ek.thy1.read');
% t_thy=lcaGet('V2EPICS:i');
% lcaPut('V2EPICS:iRead','DB_TIMING::TD2:EXT.FINE.DELAY.READ');
% t_ext=lcaGet('V2EPICS:i');
% set(handles.text12,'String',num2str((t_ext-t_thy)/357))

% Get bunch spacing
lcaPut('V2EPICS:sRead','DB_BEAM::TRAIN:WORD.2');
bs_str=lcaGet('V2EPICS:s');
t=regexp(bs_str,'(\d+)wt$','tokens','once');
set(handles.text11,'String',t{1})

% Get list of available BPMs
bpmNames={'MQF1X' 'MQD2X' 'MQF3X' 'MQF4X' 'MQD5X' 'MQF6X' 'MQF7X' 'MQD8X' 'MQF9X' ...
  'MQF13X' 'MQD14X' 'MQF15X' 'MFB1FF'}';
bpmExist=false(size(bpmNames));
for ibpm=1:length(bpmNames)
  try
    lcaGet([bpmNames{ibpm},':X']);
    bpmExist(ibpm)=true;
  catch
    fprintf('BPM: %s not found\n',bpmNames{ibpm})
    bpmExist(ibpm)=false;
  end
end

% If not see any BPM PVs, just exit with error message
if ~any(bpmExist)
  errordlg('No stripline BPM PVs seen','extStripConfig Error');
  if ~isfield(FL,'mode')
    guiCloseFn('extStripConfig',handles);
  else
    delete(handles.figure1);
  end
  return
end

% Populate BPM list
set(handles.popupmenu1,'Value',1);
set(handles.popupmenu1,'String',{bpmNames{bpmExist}}); %#ok<*CCAT1>

% Update GUI data from BPM choice
popupmenu1_Callback(handles.popupmenu1,'firstcall',handles);

% Plot raw waveform
pushbutton1_Callback(handles.pushbutton1,eventdata,handles);

% UIWAIT makes extStripConfig wait for user response (see UIRESUME)
% uiwait(handles.figure1);

% scale approx
% 27mm strips : 7.22
% 37mm strips : 9.9

% Auto Update Timer setup
FL.t_extStripConfig=timer('Period',1,'ExecutionMode','fixedDelay');
set(FL.t_extStripConfig, 'TimerFcn', {'extStripConfig', 'timerFcn', 'pushbutton1_Callback',...
  handles.pushbutton1, handles});


function timerFcn(obj,event,pbhan,handles)
pushbutton1_Callback(pbhan,[],handles);

% --- Outputs from this function are returned to the command line.
function varargout = extStripConfig_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- BPM selection (fills data boxes from EPICS)
function popupmenu1_Callback(hObject, eventdata, handles) %#ok<*INUSD,*DEFNU>
% hObject    handle to popupmenu1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
persistent chConfigTimer
bpmNames=get(hObject,'String');
wbpm=get(hObject,'Value');
bpmName=bpmNames{wbpm};

% Check status
if ~strcmp(get(handles.pushbutton4,'String'),'OK')
  errordlg('BPM in error status','wf update Error')
  return
end

% Button or stripline?
if strcmp(bpmName(1:2),'MB')
  set(handles.radiobutton1,'String','br')
  set(handles.radiobutton3,'String','bl')
  set(handles.radiobutton4,'String','tl')
  set(handles.radiobutton5,'String','tr')
  wfNames={'brwf' 'blwf' 'tlwf' 'trwf'};
  set(handles.edit1,'Enable','on')
  set(handles.edit2,'Enable','on')
  set(handles.edit3,'Enable','off')
  set(handles.edit4,'Enable','off')
  set(handles.edit5,'Enable','off')
  set(handles.edit6,'Enable','off')
else
  set(handles.radiobutton1,'String','l')
  set(handles.radiobutton3,'String','r')
  set(handles.radiobutton4,'String','u')
  set(handles.radiobutton5,'String','d')
  wfNames={'lwf' 'rwf' 'uwf' 'dwf'};
  set(handles.edit1,'Enable','on')
  set(handles.edit2,'Enable','on')
  set(handles.edit3,'Enable','on')
  set(handles.edit4,'Enable','on')
  set(handles.edit5,'Enable','on')
  set(handles.edit6,'Enable','on')
end

% Which waveform displaying
wWf=find([get(handles.radiobutton1,'Value') get(handles.radiobutton3,'Value') ...
  get(handles.radiobutton4,'Value') get(handles.radiobutton5,'Value')],1);
wfName=wfNames{wWf};

% Update chassis config only if new bpm selection
if isequal(get(handles.figure1,'CurrentObject'),hObject) || isequal(eventdata,'firstcall')
  if ~isempty(chConfigTimer) && strcmp(chConfigTimer.Running,'on')
    stop(chConfigTimer);
  end
  chConfigTimer=timer('ExecutionMode','fixedDelay','StartDelay',1,'TasksToExecute',15,'Period',0.1);
  chConfigTimer.TimerFcn = {@chConfigTimerFn, handles, bpmName, wfName} ;
  chConfigTimerFn('reset');
  set(handles.edit19,'BackgroundColor','black');
  set(handles.edit20,'BackgroundColor','black');
  set(handles.edit21,'BackgroundColor','black');
  set(handles.popupmenu2,'Value',4);
  set(handles.popupmenu3,'Value',5);
  set(handles.text7,'String','---');
  set(handles.edit22,'String','---');
  set(handles.edit23,'String','---');
  set(handles.edit24,'String','---');
  set(handles.edit26,'String','---');
  set(handles.edit27,'String','---');
  drawnow('expose');
  start(chConfigTimer);
end

try

  % Stats in use
  wfstats('GetVals',handles)
  
  % Set userdata for other function use
  set(hObject,'UserData',{bpmName wfName});
  
catch ME
  errordlg(ME.message,'PV read error');
  set(handles.pushbutton4,'BackgroundColor','red');
  set(handles.pushbutton4,'String',sprintf('PV Read Error\nPush to try to Clear'));
  drawnow('expose')
  return
end
set(handles.pushbutton4,'BackgroundColor','green');
set(handles.pushbutton4,'String','OK');
drawnow('expose')
pushbutton1_Callback(handles.pushbutton1,[],handles);
  
% --- Executes during object creation, after setting all properties.
function popupmenu1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to popupmenu1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Update waveform / bpm readback
function pushbutton1_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global FL

% Check status
if ~strcmp(get(handles.pushbutton4,'String'),'OK')
  errordlg('BPM in error status','wf update Error')
  return
end

bpmNames=get(handles.popupmenu1,'String');
wbpm=get(handles.popupmenu1,'Value');
bpmName=bpmNames{wbpm};

try
  % Get waveform data
  if strcmp(bpmName(1:2),'MB')
    wfNames={'brwf' 'blwf' 'tlwf' 'trwf'};
  else
    wfNames={'lwf' 'rwf' 'uwf' 'dwf'};
  end
  wWf=find([get(handles.radiobutton1,'Value') get(handles.radiobutton3,'Value') ...
    get(handles.radiobutton4,'Value') get(handles.radiobutton5,'Value')],1);
  wfName=wfNames{wWf};
  wf=lcaGet([bpmName,':',wfName],str2double(get(handles.edit8,'String')));
  pause(0.1); % wait for waveform processing
  
  % X/Y Gain
  if strcmp(bpmName(1:2),'MB')
    set(handles.text2,'String','---');
    set(handles.text3,'String','---');
  else
    gain=[lcaGet([bpmName,':XCALC.D'])/lcaGet([bpmName,':XCALC.C']) ...
          lcaGet([bpmName,':YCALC.D'])/lcaGet([bpmName,':YCALC.C'])];
    set(handles.text2,'String',num2str(gain(1)));
    set(handles.text3,'String',num2str(gain(2)));
  end
  
  % X / Y / TMIT
  rbk=[lcaGet([bpmName,':X']) lcaGet([bpmName,':Y']) lcaGet([bpmName,':TMIT'])];
  set(handles.text4,'String',num2str(rbk(1)));
  set(handles.text5,'String',num2str(rbk(2)));
  set(handles.text6,'String',num2str(rbk(3)));
  rbk2=[lcaGet([bpmName,':X2']) lcaGet([bpmName,':Y2'])];
  set(handles.text9,'String',num2str(rbk2(1)));
  set(handles.text10,'String',num2str(rbk2(2)));
  
  % plot selected waveform on main axis
  if strcmp(bpmName(1:2),'MB')
    roi=lcaGet({[bpmName,':',wfName,'Anal_main.BGRI']; [bpmName,':',wfName,'Anal_main.ENRI']});
  else
    roi=lcaGet({[bpmName,':',wfName,'Anal_main.BGRI']; [bpmName,':',wfName,'Anal_main.ENRI']; ...
      [bpmName,':',wfName,'Anal_cal1.BGRI']; [bpmName,':',wfName,'Anal_cal1.ENRI']; ...
      [bpmName,':',wfName,'Anal_cal2.BGRI']; [bpmName,':',wfName,'Anal_cal2.ENRI']});
    roi2=lcaGet({[bpmName,':',wfName,'Anal_b2.BGRI']; [bpmName,':',wfName,'Anal_b2.ENRI']});
  end
  set(handles.edit1,'String',num2str(roi(1))); set(handles.edit2,'String',num2str(roi(2)))
  set(handles.edit28,'String',num2str(roi2(1))); set(handles.edit29,'String',num2str(roi2(2)))
  if ~strcmp(bpmName(1:2),'MB')
    set(handles.edit3,'String',num2str(roi(3))); set(handles.edit4,'String',num2str(roi(4)))
    set(handles.edit5,'String',num2str(roi(5))); set(handles.edit6,'String',num2str(roi(6)))
  end
  p1=floor(str2double(get(handles.edit7,'String')));
  plot(handles.axes1,p1:length(wf),wf(p1:end)); grid on; axis tight;
  xlabel('Digitization bin #');
  ylabel('Digitizer Counts');
  % show selected ROI on plot
  axdim=axis;
  x1=str2double(get(handles.edit1,'String'));
  x2=str2double(get(handles.edit2,'String'));
  if x1>=axdim(1) && x1<=axdim(2)
    line([x1 x1],[axdim(3) axdim(4)],'Color','black')
  end
  if x2>=axdim(1) && x2<=axdim(2)
    line([x2 x2],[axdim(3) axdim(4)],'Color','black')
  end
  x1_2=str2double(get(handles.edit28,'String'));
  x2_2=str2double(get(handles.edit29,'String'));
  if x1_2>=axdim(1) && x1_2<=axdim(2)
    line([x1_2 x1_2],[axdim(3) axdim(4)],'Color','green')
  end
  if x2_2>=axdim(1) && x2_2<=axdim(2)
    line([x2_2 x2_2],[axdim(3) axdim(4)],'Color','green')
  end
  if ~strcmp(bpmName(1:2),'MB')
    x1=str2double(get(handles.edit3,'String'));
    x2=str2double(get(handles.edit4,'String'));
    if x1>=axdim(1) && x1<=axdim(2)
      line([x1 x1],[axdim(3) axdim(4)],'Color','red')
    end
    if x2>=axdim(1) && x2<=axdim(2)
      line([x2 x2],[axdim(3) axdim(4)],'Color','red')
    end
    x1=str2double(get(handles.edit5,'String'));
    x2=str2double(get(handles.edit6,'String'));
    if x1>=axdim(1) && x1<=axdim(2)
      line([x1 x1],[axdim(3) axdim(4)],'Color','blue')
    end
    if x2>=axdim(1) && x2<=axdim(2)
      line([x2 x2],[axdim(3) axdim(4)],'Color','blue')
    end
  end
  
  % Update stats
  wfstats('GetVals',handles);
 
  
  % If requested, plot all waveforms on seperate figure window
  if isfield(FL,'Gui') && isfield(FL.Gui,['extStripConfig_' bpmName '_fig']) && ishandle(FL.Gui.(['extStripConfig_' bpmName '_fig']))
    figure(FL.Gui.(['extStripConfig_' bpmName '_fig']));
    wf={};
    for iwfm=1:4
      roi1{iwfm}=[lcaGet([bpmName,':',wfNames{iwfm},'Anal_main.BGRI']) lcaGet([bpmName,':',wfNames{iwfm},'Anal_main.ENRI'])];
      roi2{iwfm}=[lcaGet([bpmName,':',wfNames{iwfm},'Anal_cal1.BGRI']) lcaGet([bpmName,':',wfNames{iwfm},'Anal_cal1.ENRI'])];
      roi3{iwfm}=[lcaGet([bpmName,':',wfNames{iwfm},'Anal_cal2.BGRI']) lcaGet([bpmName,':',wfNames{iwfm},'Anal_cal2.ENRI'])];
      wf{iwfm}=lcaGet([bpmName,':',wfNames{iwfm}],str2double(get(handles.edit8,'String')));
    end
      subplot(3,1,1)
      b1=1:roi1{1}(2)-roi1{1}(1)+1;
      b2=1+b1(end)+10:b1(end)+10+roi1{2}(2)-roi1{2}(1)+1;
      b3=1+b2(end)+10:b2(end)+10+roi1{3}(2)-roi1{3}(1)+1;
      b4=1+b3(end)+10:b3(end)+10+roi1{4}(2)-roi1{4}(1)+1;
      plot(b1,wf{1}(roi1{1}(1):roi1{1}(2))-mean(wf{1}(roi1{1}(1):roi1{1}(2))),'k'); hold on;
      plot(b2,wf{2}(roi1{2}(1):roi1{2}(2))-mean(wf{2}(roi1{2}(1):roi1{2}(2))),'b');
      plot(b3,wf{3}(roi1{3}(1):roi1{3}(2))-mean(wf{3}(roi1{3}(1):roi1{3}(2))),'r');
      plot(b4,wf{4}(roi1{4}(1):roi1{4}(2))-mean(wf{4}(roi1{4}(1):roi1{4}(2))),'m'); hold off; grid on; axis tight;
      legend('l','r','u','d')
      title(bpmName)
      ax=axis;
      ax(2)=ax(2)+20;
      axis(ax);
%       xlabel('Digitization bin #');
      ylabel('Main Signal');
      subplot(3,1,2)
      b1=1:roi2{1}(2)-roi2{1}(1)+1;
      b2=1+b1(end)+10:b1(end)+10+roi2{2}(2)-roi2{2}(1)+1;
      b3=1+b2(end)+10:b2(end)+10+roi2{3}(2)-roi2{3}(1)+1;
      b4=1+b3(end)+10:b3(end)+10+roi2{4}(2)-roi2{4}(1)+1;
      plot(b1,wf{1}(roi2{1}(1):roi2{1}(2))-mean(wf{1}(roi2{1}(1):roi2{1}(2))),'k'); hold on;
      plot(b2,wf{2}(roi2{2}(1):roi2{2}(2))-mean(wf{2}(roi2{2}(1):roi2{2}(2))),'b');
      plot(b3,wf{3}(roi2{3}(1):roi2{3}(2))-mean(wf{3}(roi2{3}(1):roi2{3}(2))),'r');
      plot(b4,wf{4}(roi2{4}(1):roi2{4}(2))-mean(wf{4}(roi2{4}(1):roi2{4}(2))),'m'); hold off; grid on; axis tight;
      legend('l','r','u','d')
      ax=axis;
      ax(2)=ax(2)+20;
      axis(ax);
%       xlabel('Digitization bin #');
      ylabel('CAL 1 Signal');
      subplot(3,1,3)
      b1=1:roi3{1}(2)-roi3{1}(1)+1;
      b2=1+b1(end)+10:b1(end)+10+roi3{2}(2)-roi3{2}(1)+1;
      b3=1+b2(end)+10:b2(end)+10+roi3{3}(2)-roi3{3}(1)+1;
      b4=1+b3(end)+10:b3(end)+10+roi3{4}(2)-roi3{4}(1)+1;
      plot(b1,wf{1}(roi3{1}(1):roi3{1}(2))-mean(wf{1}(roi3{1}(1):roi3{1}(2))),'k'); hold on;
      plot(b2,wf{2}(roi3{2}(1):roi3{2}(2))-mean(wf{2}(roi3{2}(1):roi3{2}(2))),'b');
      plot(b3,wf{3}(roi3{3}(1):roi3{3}(2))-mean(wf{3}(roi3{3}(1):roi3{3}(2))),'r');
      plot(b4,wf{4}(roi3{4}(1):roi3{4}(2))-mean(wf{4}(roi3{4}(1):roi3{4}(2)))); hold off; grid on; axis tight;
      legend('l','r','u','d')
      ax=axis;
      ax(2)=ax(2)+20;
      axis(ax);
%       xlabel('Digitization bin #');
      ylabel('CAL 2 Signal');
  end
      
  
catch ME
  errordlg(ME.message,'PV read error');
  set(handles.pushbutton4,'BackgroundColor','red');
  set(handles.pushbutton4,'String',sprintf('PV Read Error\nPush to Clear'));
  drawnow('expose');
  return
end
set(handles.pushbutton4,'BackgroundColor','green');
set(handles.pushbutton4,'String','OK');
drawnow('expose')

function edit6_Callback(hObject, eventdata, handles)
% hObject    handle to edit6 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

names=get(handles.popupmenu1,'UserData');
val=floor(str2double(get(hObject,'String')));
if val<1
  val=1; set(hObject,'String',num2str(val));
elseif val>str2double(get(handles.edit8,'String'))
  val=str2double(get(handles.edit8,'String')); set(hObject,'String',num2str(val));
end
if val<str2double(get(handles.edit5,'String'))
  val=str2double(get(handles.edit5,'String'));
end
lcaPutNoWait([names{1},':',names{2},'Anal_cal2.ENRI'],val);
set(hObject,'String',num2str(floor(val)));

setBeamParams(handles);

% --- Executes during object creation, after setting all properties.
function edit6_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit6 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit5_Callback(hObject, eventdata, handles)
% hObject    handle to edit5 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

names=get(handles.popupmenu1,'UserData');
val=floor(str2double(get(hObject,'String')));
if val<1
  val=1; set(hObject,'String',num2str(val));
elseif val>str2double(get(handles.edit8,'String'))
  val=str2double(get(handles.edit8,'String')); set(hObject,'String',num2str(val));
end
if val>str2double(get(handles.edit6,'String'))
  val=str2double(get(handles.edit6,'String'));
end
lcaPutNoWait([names{1},':',names{2},'Anal_cal2.BGRI'],val);
set(hObject,'String',num2str(floor(val)));

setBeamParams(handles);

% --- Executes during object creation, after setting all properties.
function edit5_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit5 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit4_Callback(hObject, eventdata, handles)
% hObject    handle to edit4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

names=get(handles.popupmenu1,'UserData');
val=floor(str2double(get(hObject,'String')));
if val<1
  val=1; set(hObject,'String',num2str(val));
elseif val>str2double(get(handles.edit8,'String'))
  val=str2double(get(handles.edit8,'String')); set(hObject,'String',num2str(val));
end
if val<str2double(get(handles.edit3,'String'))
  val=str2double(get(handles.edit3,'String'));
end
lcaPutNoWait([names{1},':',names{2},'Anal_cal1.ENRI'],val);
set(hObject,'String',num2str(floor(val)));

setBeamParams(handles);

% --- Executes during object creation, after setting all properties.
function edit4_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit3_Callback(hObject, eventdata, handles)
% hObject    handle to edit3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

names=get(handles.popupmenu1,'UserData');
val=floor(str2double(get(hObject,'String')));
if val<1
  val=1; set(hObject,'String',num2str(val));
elseif val>str2double(get(handles.edit8,'String'))
  val=str2double(get(handles.edit8,'String')); set(hObject,'String',num2str(val));
end
if val>str2double(get(handles.edit4,'String'))
  val=str2double(get(handles.edit4,'String'));
end
lcaPutNoWait([names{1},':',names{2},'Anal_cal1.BGRI'],val);
set(hObject,'String',num2str(floor(val)));

setBeamParams(handles);

% --- Executes during object creation, after setting all properties.
function edit3_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function setBeamParams(handles)
lcaPut('V2EPICS:iRead','DB_BEAM::BEAMCTRL:NTRAIN');
ntrain=lcaGet('V2EPICS:i');
set(handles.text12,'String',num2str(ntrain))
lcaPut('STRIPLINE:NTRAIN',ntrain)
lcaPut('STRIPLINE:DT_BUNCH',str2double(get(handles.text11,'String')))

function edit1_Callback(hObject, eventdata, handles)
% hObject    handle to edit1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

names=get(handles.popupmenu1,'UserData');
val=floor(str2double(get(hObject,'String')));
if val<1
  val=1; set(hObject,'String',num2str(val));
elseif val>str2double(get(handles.edit8,'String'))
  val=str2double(get(handles.edit8,'String')); set(hObject,'String',num2str(val));
end
if val>str2double(get(handles.edit2,'String'))
  val=str2double(get(handles.edit2,'String'));
end

lcaPutNoWait([names{1},':',names{2},'Anal_main.BGRI'],val);
set(hObject,'String',num2str(floor(val)));
setBeamParams(handles);

% --- Executes during object creation, after setting all properties.
function edit1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit2_Callback(hObject, eventdata, handles)
% hObject    handle to edit2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

names=get(handles.popupmenu1,'UserData');
val=floor(str2double(get(hObject,'String')));
if val<1
  val=1; set(hObject,'String',num2str(val));
elseif val>str2double(get(handles.edit8,'String'))
  val=str2double(get(handles.edit8,'String')); set(hObject,'String',num2str(val));
end
if val<str2double(get(handles.edit1,'String'))
  val=str2double(get(handles.edit1,'String'));
end
lcaPutNoWait([names{1},':',names{2},'Anal_main.ENRI'],val);
set(hObject,'String',num2str(floor(val)));

setBeamParams(handles);

% --- Executes during object creation, after setting all properties.
function edit2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit8_Callback(hObject, eventdata, handles)
% hObject    handle to edit8 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit8 as text
%        str2double(get(hObject,'String')) returns contents of edit8 as a double


% --- Executes during object creation, after setting all properties.
function edit8_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit8 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit7_Callback(hObject, eventdata, handles)
% hObject    handle to edit7 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit7 as text
%        str2double(get(hObject,'String')) returns contents of edit7 as a double


% --- Executes during object creation, after setting all properties.
function edit7_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit7 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit9_Callback(hObject, eventdata, handles)
% hObject    handle to edit9 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit9 as text
%        str2double(get(hObject,'String')) returns contents of edit9 as a double


% --- Executes during object creation, after setting all properties.
function edit9_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit9 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit10_Callback(hObject, eventdata, handles)
% hObject    handle to edit10 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit10 as text
%        str2double(get(hObject,'String')) returns contents of edit10 as a double


% --- Executes during object creation, after setting all properties.
function edit10_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit10 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit11_Callback(hObject, eventdata, handles)
% hObject    handle to edit11 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit11 as text
%        str2double(get(hObject,'String')) returns contents of edit11 as a double


% --- Executes during object creation, after setting all properties.
function edit11_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit11 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit15_Callback(hObject, eventdata, handles)
% hObject    handle to edit15 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

names=get(handles.popupmenu1,'UserData');
val=str2double(get(hObject,'String'));
if isnan(val)
  set(hObject,'String','???');
  return
end
lcaPutNoWait([names{1},':BBA_Y'],val);


% --- Executes during object creation, after setting all properties.
function edit15_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit15 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit14_Callback(hObject, eventdata, handles)
% hObject    handle to edit14 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

names=get(handles.popupmenu1,'UserData');
val=str2double(get(hObject,'String'));
if isnan(val)
  set(hObject,'String','???');
  return
end
lcaPutNoWait([names{1},':SCALE_Y'],val);


% --- Executes during object creation, after setting all properties.
function edit14_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit14 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit12_Callback(hObject, eventdata, handles)
% hObject    handle to edit12 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

names=get(handles.popupmenu1,'UserData');
val=str2double(get(hObject,'String'));
if isnan(val)
  set(hObject,'String','???');
  return
end
lcaPutNoWait([names{1},':SCALE_X'],val);


% --- Executes during object creation, after setting all properties.
function edit12_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit12 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit13_Callback(hObject, eventdata, handles)
% hObject    handle to edit13 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

names=get(handles.popupmenu1,'UserData');
val=str2double(get(hObject,'String'));
if isnan(val)
  set(hObject,'String','???');
  return
end
lcaPutNoWait([names{1},':BBA_X'],val);


% --- Executes during object creation, after setting all properties.
function edit13_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit13 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in pushbutton3.
function pushbutton3_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global FL

if ~isfield(FL,'mode')
  guiCloseFn('extStripConfig',handles);
else
  delete(handles.figure1);
end

% Exit if standalone instance
if ~isfield(FL,'trusted')
  exit
end

% --- Executes on button press in togglebutton1.
function togglebutton1_Callback(hObject, eventdata, handles)
% hObject    handle to togglebutton1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
%global FL
%if get(hObject,'Value')
%  if isfield(FL,'t_extStripConfig') && ~strcmp(FL.t_extStripConfig.running,'on')
%    start(FL.t_extStripConfig);
%  end
%  set(hObject,'String','Auto Update On')
%else
%  stop(FL.t_extStripConfig)
%  set(hObject,'String','Auto Update Off')
%end
%drawnow('expose')
    



function edit18_Callback(hObject, eventdata, handles)
% hObject    handle to edit18 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

val=floor(str2double(get(hObject,'String')));
if isnan(val) || val<1
  val=1;
end
set(hObject,'String',num2str(val));

% --- Executes during object creation, after setting all properties.
function edit18_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit18 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in pushbutton2.
function pushbutton2_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global FL

names=get(handles.popupmenu1,'UserData');

if ~isfield(FL,'Gui') || ~isfield(FL.Gui,['extStripConfig_' names{1} '_fig']) || ~ishandle(FL.Gui.(['extStripConfig_' names{1} '_fig']))
  FL.Gui.(['extStripConfig_' names{1} '_fig'])=figure;
end
pushbutton1_Callback(handles.pushbutton1,[],handles);


function edit19_Callback(hObject, eventdata, handles)
% hObject    handle to edit19 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

names=get(handles.popupmenu1,'UserData');
val=str2double(get(hObject,'String'));
if isnan(val)
  set(hObject,'String','???');
  return
end
try
  wval=['0X02' dec2hex(val,2) '\r\l'];
  lcaPutNoWait([names{1},':chassisWrite'],wval);
catch
  set(hObject,'String','???');
end


% --- Executes during object creation, after setting all properties.
function edit19_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit19 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit20_Callback(hObject, eventdata, handles)
% hObject    handle to edit20 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

names=get(handles.popupmenu1,'UserData');
val=str2double(get(hObject,'String'));
if isnan(val)
  set(hObject,'String','???');
  return
end
try
  wval=['0X03' dec2hex(val,2) '\r\l'];
  lcaPutNoWait([names{1},':chassisWrite'],wval);
catch
  set(hObject,'String','???');
end


% --- Executes during object creation, after setting all properties.
function edit20_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit20 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit21_Callback(hObject, eventdata, handles)
% hObject    handle to edit21 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

names=get(handles.popupmenu1,'UserData');
val=str2double(get(hObject,'String'));
if isnan(val)
  set(hObject,'String','???');
  return
end
try
  wval=['0X01' dec2hex(val,2) '\r\l'];
  lcaPutNoWait([names{1},':chassisWrite'],wval);
catch
  set(hObject,'String','???');
end


% --- Executes during object creation, after setting all properties.
function edit21_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit21 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in pushbutton4.
function pushbutton4_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
str=get(hObject,'String');
if ~strcmp(str,'OK')
  set(hObject,'String','OK');
  set(hObject,'BackgroundColor','green')
  popupmenu1_Callback(handles.popupmenu1,eventdata,handles);
end

% --- Executes on button press in pushbutton5.
function pushbutton5_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton5 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
incr=str2double(get(handles.edit18,'String'));
bpmNames=get(handles.popupmenu1,'String');
names=get(handles.popupmenu1,'UserData');
maxval=str2double(regexp(get(handles.uipanel5,'Title'),'\d+','once','match'));
if isnan(maxval); return; end;
for ibpm=1:length(bpmNames)
  val(1)=lcaGet([bpmNames{ibpm},':',names{2},'Anal_main.BGRI']);
  val(2)=lcaGet([bpmNames{ibpm},':',names{2},'Anal_main.ENRI']);
  val(3)=lcaGet([bpmNames{ibpm},':',names{2},'Anal_cal1.BGRI']);
  val(4)=lcaGet([bpmNames{ibpm},':',names{2},'Anal_cal1.ENRI']);
  val(5)=lcaGet([bpmNames{ibpm},':',names{2},'Anal_cal2.BGRI']);
  val(6)=lcaGet([bpmNames{ibpm},':',names{2},'Anal_cal2.ENRI']);
  val(7)=lcaGet([bpmNames{ibpm},':',names{2},'Anal_b2.BGRI']);
  val(8)=lcaGet([bpmNames{ibpm},':',names{2},'Anal_b2.ENRI']);
  val=val+incr;
  if get(handles.radiobutton6,'Value')
    lcaPutNoWait([bpmNames{ibpm},':',names{2},'Anal_main.BGRI'],val(1));
    lcaPutNoWait([bpmNames{ibpm},':',names{2},'Anal_main.ENRI'],val(2));
  end
  if get(handles.radiobutton7,'Value')
    lcaPutNoWait([bpmNames{ibpm},':',names{2},'Anal_cal1.BGRI'],val(3));
    lcaPutNoWait([bpmNames{ibpm},':',names{2},'Anal_cal1.ENRI'],val(4));
  end
  if get(handles.radiobutton8,'Value')
    lcaPutNoWait([bpmNames{ibpm},':',names{2},'Anal_cal2.BGRI'],val(5));
    lcaPutNoWait([bpmNames{ibpm},':',names{2},'Anal_cal2.ENRI'],val(6));
  end
  if get(handles.radiobutton17,'Value')
    lcaPutNoWait([bpmNames{ibpm},':',names{2},'Anal_b2.BGRI'],val(5));
    lcaPutNoWait([bpmNames{ibpm},':',names{2},'Anal_b2.ENRI'],val(6));
  end
end
popupmenu1_Callback(handles.popupmenu1,eventdata,handles);
pushbutton1_Callback(handles.pushbutton1,eventdata,handles);


% --- Executes on button press in pushbutton6.
function pushbutton6_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton6 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
incr=str2double(get(handles.edit18,'String'));
bpmNames=get(handles.popupmenu1,'String');
names=get(handles.popupmenu1,'UserData');
maxval=str2double(regexp(get(handles.uipanel5,'Title'),'\d+','once','match'));
if isnan(maxval); return; end;
for ibpm=1:length(bpmNames)
  val(1)=lcaGet([bpmNames{ibpm},':',names{2},'Anal_main.BGRI']);
  val(2)=lcaGet([bpmNames{ibpm},':',names{2},'Anal_main.ENRI']);
  val(3)=lcaGet([bpmNames{ibpm},':',names{2},'Anal_cal1.BGRI']);
  val(4)=lcaGet([bpmNames{ibpm},':',names{2},'Anal_cal1.ENRI']);
  val(5)=lcaGet([bpmNames{ibpm},':',names{2},'Anal_cal2.BGRI']);
  val(6)=lcaGet([bpmNames{ibpm},':',names{2},'Anal_cal2.ENRI']);
  val(7)=lcaGet([bpmNames{ibpm},':',names{2},'Anal_b2.BGRI']);
  val(8)=lcaGet([bpmNames{ibpm},':',names{2},'Anal_b2.ENRI']);
  val=val-incr;
  if get(handles.radiobutton6,'Value')
    lcaPut([bpmNames{ibpm},':',names{2},'Anal_main.BGRI'],val(1));
    lcaPut([bpmNames{ibpm},':',names{2},'Anal_main.ENRI'],val(2));
  end
  if get(handles.radiobutton7,'Value')
    lcaPut([bpmNames{ibpm},':',names{2},'Anal_cal1.BGRI'],val(3));
    lcaPut([bpmNames{ibpm},':',names{2},'Anal_cal1.ENRI'],val(4));
  end
  if get(handles.radiobutton8,'Value')
    lcaPut([bpmNames{ibpm},':',names{2},'Anal_cal2.BGRI'],val(5));
    lcaPut([bpmNames{ibpm},':',names{2},'Anal_cal2.ENRI'],val(6));
  end
  if get(handles.radiobutton17,'Value')
    lcaPut([bpmNames{ibpm},':',names{2},'Anal_b2.BGRI'],val(7));
    lcaPut([bpmNames{ibpm},':',names{2},'Anal_b2.ENRI'],val(8));
  end
end
popupmenu1_Callback(handles.popupmenu1,eventdata,handles);
pushbutton1_Callback(handles.pushbutton1,eventdata,handles);

% --- Executes on button press in radiobutton6.
function radiobutton6_Callback(hObject, eventdata, handles)
% hObject    handle to radiobutton6 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of radiobutton6


% --- Executes on button press in radiobutton7.
function radiobutton7_Callback(hObject, eventdata, handles)
% hObject    handle to radiobutton7 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of radiobutton7


% --- Executes on button press in radiobutton8.
function radiobutton8_Callback(hObject, eventdata, handles)
% hObject    handle to radiobutton8 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of radiobutton8


% --- Executes on button press in radiobutton9.
function radiobutton9_Callback(hObject, eventdata, handles)
% hObject    handle to radiobutton9 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

wfstats('PutSel',handles);


% --- Executes on button press in radiobutton10.
function radiobutton10_Callback(hObject, eventdata, handles)
% hObject    handle to radiobutton10 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

wfstats('PutSel',handles);


% --- Executes on button press in radiobutton11.
function radiobutton11_Callback(hObject, eventdata, handles)
% hObject    handle to radiobutton11 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

wfstats('PutSel',handles);


% --- Executes on button press in radiobutton12.
function radiobutton12_Callback(hObject, eventdata, handles)
% hObject    handle to radiobutton12 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

wfstats('PutSel',handles);


% --- Executes on button press in radiobutton13.
function radiobutton13_Callback(hObject, eventdata, handles)
% hObject    handle to radiobutton13 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

wfstats('PutSel',handles);


% --- Executes on button press in radiobutton14.
function radiobutton14_Callback(hObject, eventdata, handles)
% hObject    handle to radiobutton14 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

wfstats('PutSel',handles);


% --- Executes on button press in radiobutton15.
function radiobutton15_Callback(hObject, eventdata, handles)
% hObject    handle to radiobutton15 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

wfstats('PutSel',handles);


% --- Executes on button press in radiobutton16.
function radiobutton16_Callback(hObject, eventdata, handles)
% hObject    handle to radiobutton16 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

wfstats('PutSel',handles);

function wfstats(cmd,handles)
bpmNames=get(handles.popupmenu1,'String');
wbpm=get(handles.popupmenu1,'Value');
bpmName=bpmNames{wbpm};
wWf=find([get(handles.radiobutton1,'Value') get(handles.radiobutton3,'Value') ...
  get(handles.radiobutton4,'Value') get(handles.radiobutton5,'Value')],1);
if strcmp(bpmName(1:2),'MB')
  wfNames={'brwf' 'blwf' 'tlwf' 'trwf'};
else
  wfNames={'lwf' 'rwf' 'uwf' 'dwf'};
end
wfName=wfNames{wWf};
statnames={'MAX' 'MIN' 'MADV' 'MEAN' 'VAR' 'SDEV' 'FWHM' 'PKPK'};
pv1=[bpmName,':',wfName,'Anal_main.'];
if ~strcmp(bpmName(1:2),'MB')
  pv2=[bpmName,':',wfName,'Anal_cal1.'];
  pv3=[bpmName,':',wfName,'Anal_cal2.'];
end
for ipv=1:length(statnames)
  pvnames{ipv,1}=[pv1 statnames{ipv}];
  if ~strcmp(bpmName(1:2),'MB')
    pvnames_cal1{ipv,1}=[pv2 statnames{ipv}];
    pvnames_cal2{ipv,1}=[pv3 statnames{ipv}];
  end
end
switch lower(cmd)
  case 'getvals'
    guivals(:,1)=lcaGet(pvnames);
    if ~strcmp(bpmName(1:2),'MB')
      guivals(:,2)=lcaGet(pvnames_cal1);
      guivals(:,3)=lcaGet(pvnames_cal2);
    else
      guivals(:,2)=NaN(size(guivals(:,1)));
      guivals(:,3)=NaN(size(guivals(:,1)));
    end
    set(handles.uitable1,'Data',guivals);
    t=regexp(lcaGet([bpmName,':XCALC.INPA']),'\.(.\S+)','tokens','once');
    wstat=find(ismember(statnames,t{1}));
    for ibut=9:16
      if ibut-8==wstat
        set(handles.(['radiobutton' num2str(ibut)]),'Value',1);
      else
        set(handles.(['radiobutton' num2str(ibut)]),'Value',0);
      end
    end
  case 'putsel'
    calltag=get(get(handles.figure1,'CurrentObject'),'Tag');
    inval=str2double(regexp(calltag,'\d+','match','once'));
    newpv=regexprep(lcaGet([bpmName,':XCALC.INPA']),'\.\S+',['.' statnames{inval-8}]);
    lcaPutNoWait([bpmName,':XCALC.INPA'],newpv);
    newpv=regexprep(lcaGet([bpmName,':YCALC.INPA']),'\.\S+',['.' statnames{inval-8}]);
    lcaPutNoWait([bpmName,':YCALC.INPA'],newpv);
    newpv=regexprep(lcaGet([bpmName,':XCALC.INPB']),'\.\S+',['.' statnames{inval-8}]);
    lcaPutNoWait([bpmName,':XCALC.INPB'],newpv);
    newpv=regexprep(lcaGet([bpmName,':YCALC.INPB']),'\.\S+',['.' statnames{inval-8}]);
    lcaPutNoWait([bpmName,':YCALC.INPB'],newpv);
    newpv=regexprep(lcaGet([bpmName,':XCALC.INPC']),'\.\S+',['.' statnames{inval-8}]);
    lcaPutNoWait([bpmName,':XCALC.INPC'],newpv);
    newpv=regexprep(lcaGet([bpmName,':YCALC.INPC']),'\.\S+',['.' statnames{inval-8}]);
    lcaPutNoWait([bpmName,':YCALC.INPC'],newpv);
    newpv=regexprep(lcaGet([bpmName,':XCALC.INPD']),'\.\S+',['.' statnames{inval-8}]);
    lcaPutNoWait([bpmName,':XCALC.INPD'],newpv);
    newpv=regexprep(lcaGet([bpmName,':YCALC.INPD']),'\.\S+',['.' statnames{inval-8}]);
    lcaPutNoWait([bpmName,':YCALC.INPD'],newpv);
    newpv=regexprep(lcaGet([bpmName,':TMITCALC.INPA']),'\.\S+',['.' statnames{inval-8}]);
    lcaPutNoWait([bpmName,':TMITCALC.INPA'],newpv);
    newpv=regexprep(lcaGet([bpmName,':TMITCALC.INPB']),'\.\S+',['.' statnames{inval-8}]);
    lcaPutNoWait([bpmName,':TMITCALC.INPB'],newpv);
    newpv=regexprep(lcaGet([bpmName,':TMITCALC.INPC']),'\.\S+',['.' statnames{inval-8}]);
    lcaPutNoWait([bpmName,':TMITCALC.INPC'],newpv);
    newpv=regexprep(lcaGet([bpmName,':TMITCALC.INPD']),'\.\S+',['.' statnames{inval-8}]);
    lcaPutNoWait([bpmName,':TMITCALC.INPD'],newpv);
    newpv=regexprep(lcaGet([bpmName,':XCALC.INPA']),'\.\S+',['.' statnames{inval-8}]);
    lcaPutNoWait([bpmName,':XCALC2.INPA'],newpv);
    newpv=regexprep(lcaGet([bpmName,':YCALC.INPA']),'\.\S+',['.' statnames{inval-8}]);
    lcaPutNoWait([bpmName,':YCALC2.INPA'],newpv);
    newpv=regexprep(lcaGet([bpmName,':XCALC.INPB']),'\.\S+',['.' statnames{inval-8}]);
    lcaPutNoWait([bpmName,':XCALC2.INPB'],newpv);
    newpv=regexprep(lcaGet([bpmName,':YCALC.INPB']),'\.\S+',['.' statnames{inval-8}]);
    lcaPutNoWait([bpmName,':YCALC2.INPB'],newpv);
    newpv=regexprep(lcaGet([bpmName,':XCALC.INPC']),'\.\S+',['.' statnames{inval-8}]);
    lcaPutNoWait([bpmName,':XCALC2.INPC'],newpv);
    newpv=regexprep(lcaGet([bpmName,':YCALC.INPC']),'\.\S+',['.' statnames{inval-8}]);
    lcaPutNoWait([bpmName,':YCALC2.INPC'],newpv);
    newpv=regexprep(lcaGet([bpmName,':XCALC.INPD']),'\.\S+',['.' statnames{inval-8}]);
    lcaPutNoWait([bpmName,':XCALC2.INPD'],newpv);
    newpv=regexprep(lcaGet([bpmName,':YCALC.INPD']),'\.\S+',['.' statnames{inval-8}]);
    lcaPutNoWait([bpmName,':YCALC2.INPD'],newpv);
    newpv=regexprep(lcaGet([bpmName,':TMITCALC.INPA']),'\.\S+',['.' statnames{inval-8}]);
    lcaPutNoWait([bpmName,':TMITCALC2.INPA'],newpv);
    newpv=regexprep(lcaGet([bpmName,':TMITCALC.INPB']),'\.\S+',['.' statnames{inval-8}]);
    lcaPutNoWait([bpmName,':TMITCALC2.INPB'],newpv);
    newpv=regexprep(lcaGet([bpmName,':TMITCALC.INPC']),'\.\S+',['.' statnames{inval-8}]);
    lcaPutNoWait([bpmName,':TMITCALC2.INPC'],newpv);
    newpv=regexprep(lcaGet([bpmName,':TMITCALC.INPD']),'\.\S+',['.' statnames{inval-8}]);
    lcaPutNoWait([bpmName,':TMITCALC2.INPD'],newpv);
    if ~strcmp(bpmName(1:2),'MB')
      newpv=regexprep(lcaGet([bpmName,':TMITCALC.INPE']),'\.\S+',['.' statnames{inval-8}]);
      lcaPutNoWait([bpmName,':TMITCALC.INPE'],newpv);
      newpv=regexprep(lcaGet([bpmName,':TMITCALC.INPF']),'\.\S+',['.' statnames{inval-8}]);
      lcaPutNoWait([bpmName,':TMITCALC.INPF'],newpv);
      newpv=regexprep(lcaGet([bpmName,':TMITCALC.INPG']),'\.\S+',['.' statnames{inval-8}]);
      lcaPutNoWait([bpmName,':TMITCALC.INPG'],newpv);
      newpv=regexprep(lcaGet([bpmName,':TMITCALC.INPH']),'\.\S+',['.' statnames{inval-8}]);
      lcaPutNoWait([bpmName,':TMITCALC.INPH'],newpv);
      newpv=regexprep(lcaGet([bpmName,':TMITCALC2.INPE']),'\.\S+',['.' statnames{inval-8}]);
      lcaPutNoWait([bpmName,':TMITCALC.INPE'],newpv);
      newpv=regexprep(lcaGet([bpmName,':TMITCALC2.INPF']),'\.\S+',['.' statnames{inval-8}]);
      lcaPutNoWait([bpmName,':TMITCALC.INPF'],newpv);
      newpv=regexprep(lcaGet([bpmName,':TMITCALC2.INPG']),'\.\S+',['.' statnames{inval-8}]);
      lcaPutNoWait([bpmName,':TMITCALC.INPG'],newpv);
      newpv=regexprep(lcaGet([bpmName,':TMITCALC2.INPH']),'\.\S+',['.' statnames{inval-8}]);
      lcaPutNoWait([bpmName,':TMITCALC.INPH'],newpv);
    end
    for ibut=9:16
      if ibut==inval
        set(handles.(['radiobutton' num2str(ibut)]),'Value',1);
      else
        set(handles.(['radiobutton' num2str(ibut)]),'Value',0);
      end
    end
end
drawnow('expose');


% --- Executes on button press in pushbutton7.
function pushbutton7_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton7 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
names=get(handles.popupmenu1,'UserData');
try
  lcaPutNoWait([names{1},':chassisWrite'],'0X0401\r\l');
catch ME
  errordlg(ME.message,'Reset error');
end

% --- Executes on button press in pushbutton8.
function pushbutton8_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton8 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
names=get(handles.popupmenu1,'UserData');
try
  lcaPutNoWait([names{1},':chassisWrite'],'0X0600\r\l');
catch ME
  errordlg(ME.message,'Trigger error');
end

% --- Executes on selection change in popupmenu2.
function popupmenu2_Callback(hObject, eventdata, handles)
% hObject    handle to popupmenu2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
if get(handles.togglebutton2,'Value')
  wval='1';
else
  wval='';
end
selval=get(hObject,'Value');
if selval==1
  wval=[wval '11'];
elseif selval==2
  wval=[wval '01'];
elseif selval==3
  wval=[wval '00'];
else
  wval=[wval '11'];
end
names=get(handles.popupmenu1,'UserData');
thisval=get(handles.popupmenu3,'Value');
if thisval==1
  wval=[wval '11'];
elseif thisval==2
  wval=[wval '10'];
elseif thisval==3
  wval=[wval '01'];
elseif thisval==4
  wval=[wval '00'];
else
  wval=[wval '11'];
end
try
  lcaPutNoWait([names{1},':chassisWrite'],['0X00' dec2hex(bin2dec(wval),2) '\r\l']);
catch ME
  errordlg(ME.message,'Reset error');
end


% --- Executes during object creation, after setting all properties.
function popupmenu2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to popupmenu2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in popupmenu3.
function popupmenu3_Callback(hObject, eventdata, handles)
% hObject    handle to popupmenu3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
if get(handles.togglebutton2,'Value')
  wval='1';
else
  wval='';
end
selval=get(handles.popupmenu2,'Value');
if selval==1
  wval=[wval '11'];
elseif selval==2
  wval=[wval '01'];
elseif selval==3
  wval=[wval '00'];
else
  wval=[wval '11'];
end
names=get(handles.popupmenu1,'UserData');
thisval=get(hObject,'Value');
if thisval==1
  wval=[wval '11'];
elseif thisval==2
  wval=[wval '10'];
elseif thisval==3
  wval=[wval '01'];
elseif thisval==4
  wval=[wval '00'];
else
  wval=[wval '11'];
end
try
  lcaPutNoWait([names{1},':chassisWrite'],['0X00' dec2hex(bin2dec(wval),2) '\r\l']);
catch ME
  errordlg(ME.message,'Reset error');
end


% --- Executes during object creation, after setting all properties.
function popupmenu3_CreateFcn(hObject, eventdata, handles)
% hObject    handle to popupmenu3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit27_Callback(hObject, eventdata, handles)
% hObject    handle to edit27 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

names=get(handles.popupmenu1,'UserData');
val=str2double(get(hObject,'String'));
if isnan(val) || abs(val)>255
  errordlg('Invalid OFFTIME entry','Invalid Entry');
  return
end
val=floor(val);
try
  lcaPutNoWait([names{1} ':chassisWrite'],['0X14' dec2hex(val,2) '\r\l']);
catch
  errordlg('OFFTIME change request failed','PV error');
end


% --- Executes during object creation, after setting all properties.
function edit27_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit27 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit26_Callback(hObject, eventdata, handles)
% hObject    handle to edit26 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

names=get(handles.popupmenu1,'UserData');
val=str2double(get(hObject,'String'));
if isnan(val) || abs(val)>255
  errordlg('Invalid RFWIDTH entry','Invalid Entry');
  return
end
val=floor(val);
try
  lcaPutNoWait([names{1} ':chassisWrite'],['0X13' dec2hex(val,2) '\r\l']);
catch
  errordlg('RFWIDTH change request failed','PV error');
end


% --- Executes during object creation, after setting all properties.
function edit26_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit26 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit24_Callback(hObject, eventdata, handles)
% hObject    handle to edit24 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

names=get(handles.popupmenu1,'UserData');
val=str2double(get(hObject,'String'));
if isnan(val) || abs(val)>255
  errordlg('Invalid RF12RF2 entry','Invalid Entry');
  return
end
val=floor(val);
try
  lcaPutNoWait([names{1} ':chassisWrite'],['0X12' dec2hex(val,2) '\r\l']);
catch
  errordlg('RF12RF2 change request failed','PV error');
end


% --- Executes during object creation, after setting all properties.
function edit24_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit24 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit23_Callback(hObject, eventdata, handles)
% hObject    handle to edit23 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

names=get(handles.popupmenu1,'UserData');
val=str2double(get(hObject,'String'));
if isnan(val) || abs(val)>255
  errordlg('Invalid AMP2RF1 entry','Invalid Entry');
  return
end
val=floor(val);
try
  lcaPutNoWait([names{1} ':chassisWrite'],['0X11' dec2hex(val,2) '\r\l']);
catch
  errordlg('AMP2RF1 change request failed','PV error');
end


% --- Executes during object creation, after setting all properties.
function edit23_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit23 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit22_Callback(hObject, eventdata, handles)
% hObject    handle to edit22 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

names=get(handles.popupmenu1,'UserData');
val=str2double(get(hObject,'String'));
if isnan(val) || abs(val)>255
  errordlg('Invalid TRIG2AMP entry','Invalid Entry');
  return
end
val=floor(val);
try
  lcaPutNoWait([names{1} ':chassisWrite'],['0X10' dec2hex(val,2) '\r\l']);
catch
  errordlg('TRIG2AMP change request failed','PV error');
end
  


% --- Executes during object creation, after setting all properties.
function edit22_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit22 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in pushbutton9.
function pushbutton9_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton9 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

if strcmp(questdlg('Reset VME controller and IOC?','Reset IOC'),'Yes')
  lcaPutNoWait('ATF:slacvme1:reboot',1);
  names=get(handles.popupmenu1,'UserData');
  mhan=msgbox('IOC (VME crate) rebooting, please wait','IOC reboot','modal');
  pause(1);
  rebooting=true; t0=clock; ndots='.';
  while rebooting && etime(clock,t0)<90
    mhan=msgbox(['IOC (VME crate) rebooting, please wait ',ndots],'IOC reboot','modal');
    pause(1);
    ndots=[ndots '.'];
    t1=clock;
    try
      lcaGet([names{1},':X']);
      rebooting=false;
    catch
      twait=1-etime(clock,t1);
      if twait>0
        pause(twait);
      end
    end
  end
  if ishandle(mhan); delete(mhan); end;
  if etime(clock,t0)>120
    errordlg('Timeout waiting for IOC reboot- suggest power cycle manually','IOC reboot error');
  end
end


% --- Executes when user attempts to close figure1.
function figure1_CloseRequestFcn(hObject, eventdata, handles)
% hObject    handle to figure1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

global FL

if ~isfield(FL,'mode')
  guiCloseFn('extStripConfig',handles);
else
  delete(handles.figure1);
end

% Exit if standalone instance
if ~isfield(FL,'trusted')
  exit
end

% --- Executes when selected object is changed in uipanel4.
function uipanel4_SelectionChangeFcn(hObject, eventdata, handles)
% hObject    handle to the selected object in uipanel4 
% eventdata  structure with the following fields (see UIBUTTONGROUP)
%	EventName: string 'SelectionChanged' (read only)
%	OldValue: handle of the previously selected object or empty if none was selected
%	NewValue: handle of the currently selected object
% handles    structure with handles and user data (see GUIDATA)
popupmenu1_Callback(handles.popupmenu1,[],handles);
pushbutton1_Callback(handles.pushbutton1,[],handles);


% --- Executes on button press in togglebutton2.
function togglebutton2_Callback(hObject, eventdata, handles)
% hObject    handle to togglebutton2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

if get(hObject,'Value')
  wval='1';
  set(hObject,'String','Output Limiter DISABLED')
else
  set(hObject,'String','Output Limiter ENABLED')
  wval='';
end
selval=get(handles.popupmenu2,'Value');
if selval==1
  wval=[wval '11'];
elseif selval==2
  wval=[wval '01'];
elseif selval==3
  wval=[wval '00'];
else
  wval=[wval '11'];
end
names=get(handles.popupmenu1,'UserData');
thisval=get(handles.popupmenu3,'Value');
if thisval==1
  wval=[wval '11'];
elseif thisval==2
  wval=[wval '10'];
elseif thisval==3
  wval=[wval '01'];
elseif thisval==4
  wval=[wval '00'];
else
  wval=[wval '11'];
end
try
  lcaPutNoWait([names{1},':chassisWrite'],['0X00' dec2hex(bin2dec(wval),2) '\r\l']);
catch ME
  errordlg(ME.message,'Reset error');
end


% --- Auto setup
function pushbutton10_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton10 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

swid_main=20;
swid_cal=floor((str2double(get(handles.edit26,'String'))*33)/10);
s2b=1;
axhan=get(handles.axes1,'Children');
xdata=get(axhan(end),'XData');
ydata=get(axhan(end),'YData');

% Find approx signal places
y=abs(ydata-mean(ydata))./mean(abs(ydata-mean(ydata)));
xsig=find(y>s2b);
x_cal2=xdata(xsig(end))-2*swid_cal:min([xsig(end)+swid_cal xdata(end)]);
xsig=find(y(1:x_cal2(1))>s2b);
x_cal1=xdata(xsig(end))-2*swid_cal:min([xsig(end)+swid_cal x_cal2(1)]);
x_sig=swid_main+1:x_cal1(1)-1;

% Find main signal and set ROI
ysum=[];
for xpos=x_sig
  ysum(end+1)=sum(y(xpos-swid_main:xpos));
end
[~, I]=max(ysum);
set(handles.edit1,'String',num2str(x_sig(I)-swid_main));
edit1_Callback(handles.edit1,[],handles);
set(handles.edit2,'String',num2str(x_sig(I)));
edit2_Callback(handles.edit2,[],handles);

% Find CAL2 signal and set ROI
ysum=[];
for xpos=x_cal2
  ysum(end+1)=sum(y(xpos-swid_cal:xpos));
end
[~, I]=max(ysum);
set(handles.edit5,'String',num2str(x_cal2(I)-swid_cal));
edit5_Callback(handles.edit5,[],handles);
set(handles.edit6,'String',num2str(x_cal2(I)));
edit6_Callback(handles.edit6,[],handles);

% Find CAL1 signal and set ROI
ysum=[];
p1=str2double(get(handles.edit2,'String'))+swid_cal*2;
p2=str2double(get(handles.edit5,'String'))-swid_cal*2;
pran=p1:p2;
for xpos=pran
  ysum(end+1)=sum(y(xpos-swid_cal:xpos));
end
[~, I]=max(ysum);
set(handles.edit3,'String',num2str(pran(I)-swid_cal));
edit3_Callback(handles.edit3,[],handles);
set(handles.edit4,'String',num2str(pran(I)));
edit4_Callback(handles.edit4,[],handles);

% Set waveform display
% set(handles.edit8,'String',num2str(x_cal2(I)+swid_cal));
% edit8_Callback(handles.edit8,[],handles);

% Update display
pushbutton1_Callback(handles.pushbutton1,[],handles);


% --- Save all firmware settings
function pushbutton11_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton11 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
bpmNames=get(handles.popupmenu1,'String');
[fn, pn]=uiputfile(sprintf('userData/extStripFW_%s.mat',datestr(now,30)),'Save All Firmware Settings');
if ~fn; return; end;
try
  for ibpm=1:length(bpmNames)
    mh=msgbox(sprintf('Getting all firmware settings (BPM %d of %d)...',ibpm,length(bpmNames)),'Firmware Save','replace');
    lcaPut([bpmNames{ibpm},':chassisRead:ATT1.PROC'],1);
    att1_raw=lcaGet([bpmNames{ibpm},':chassisRead:ATT1']);
    lcaPut([bpmNames{ibpm},':chassisRead:ATT2.PROC'],1);
    att2_raw=lcaGet([bpmNames{ibpm},':chassisRead:ATT2']);
    lcaPut([bpmNames{ibpm},':chassisRead:CAL.PROC'],1);
    attcal_raw=lcaGet([bpmNames{ibpm},':chassisRead:CAL']);
    att1(ibpm)=hex2dec(char(att1_raw(5:6))); %#ok<NASGU>
    att2(ibpm)=hex2dec(char(att2_raw(5:6))); %#ok<NASGU>
    attcal(ibpm)=hex2dec(char(attcal_raw(5:6))); %#ok<NASGU>
    lcaPut([bpmNames{ibpm},':chassisRead:CSR.PROC'],1);
    csr_raw=lcaGet([bpmNames{ibpm},':chassisRead:CSR']);
    csr(ibpm)=hex2dec(char(csr_raw(5:6))); %#ok<NASGU>
    lcaPut([bpmNames{ibpm},':chassisRead:VER.PROC'],1);
    ver_raw=lcaGet([bpmNames{ibpm},':chassisRead:VER']);
    ver(ibpm)=hex2dec(char(ver_raw(5:6))); %#ok<NASGU>
    lcaPut([bpmNames{ibpm},':chassisRead:TRIG2AMP.PROC'],1);
    readval_raw=lcaGet([bpmNames{ibpm},':chassisRead:TRIG2AMP']);
    trig2amp(ibpm)=hex2dec(char(readval_raw(5:6))); %#ok<NASGU>
    lcaPut([bpmNames{ibpm},':chassisRead:AMP2RF1.PROC'],1);
    readval_raw=lcaGet([bpmNames{ibpm},':chassisRead:AMP2RF1']);
    amp2rf1(ibpm)=hex2dec(char(readval_raw(5:6))); %#ok<NASGU>
    lcaPut([bpmNames{ibpm},':chassisRead:RF12RF2.PROC'],1);
    readval_raw=lcaGet([bpmNames{ibpm},':chassisRead:RF12RF2']);
    rf12rf2(ibpm)=hex2dec(char(readval_raw(5:6))); %#ok<NASGU>
    lcaPut([bpmNames{ibpm},':chassisRead:RFWIDTH.PROC'],1);
    readval_raw=lcaGet([bpmNames{ibpm},':chassisRead:RFWIDTH']);
    rfwidth(ibpm)=hex2dec(char(readval_raw(5:6))); %#ok<NASGU>
    lcaPut([bpmNames{ibpm},':chassisRead:OFFTIME.PROC'],1);
    readval_raw=lcaGet([bpmNames{ibpm},':chassisRead:OFFTIME']);
    offtime(ibpm)=hex2dec(char(readval_raw(5:6))); %#ok<NASGU>
  end
  if ishandle(mh); delete(mh); end;
  save(fullfile(pn,fn),'bpmNames','att1','att2','attcal','csr','ver','trig2amp','amp2rf1','rf12rf2','rfwidth','offtime')
catch
  if ishandle(mh); delete(mh); end;
  errordlg('Failed to get all firmware settings, aborting','Firmware Save Failed');
end


% --- Restore firmware settings from file
function pushbutton12_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton12 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
[fn, pn]=uigetfile('userData/extStripFW*.mat','Restore All Firmware Settings');
if ~fn; return; end;
load(fullfile(pn,fn),'bpmNames','att1','att2','attcal','csr','ver','trig2amp','amp2rf1','rf12rf2','rfwidth','offtime')
bpmNamesNow=get(handles.popupmenu1,'String');
if any(~ismember(bpmNamesNow,bpmNames)) %#ok<USENS>
  mstr=[];
  for ibpm=find(~ismember(bpmNamesNow,bpmNames))
    mstr=[bpmNamesNow{ibpm} ' ' mstr];
  end
  if ~strcmp(questdlg(sprintf('These BPMs not in requested file:\n%s\nCarry on loading just others?',mstr)),'Yes')
    return
  end
end
try
  for ibpm=find(ismember(bpmNames,bpmNamesNow))'
    mh=msgbox(sprintf('Restoring all firmware settings (BPM %d of %d)...',...
      ibpm,sum(ismember(bpmNames,bpmNamesNow))),'Firmware Restore','replace');
    nbad=0;
    while nbad<3
      try
        lcaPut([bpmNames{ibpm},':chassisWrite'],['0X02' dec2hex(att1(ibpm),2) '\r\l']);
        lcaPut([bpmNames{ibpm},':chassisWrite'],['0X03' dec2hex(att2(ibpm),2) '\r\l']);
        lcaPut([bpmNames{ibpm},':chassisWrite'],['0X01' dec2hex(attcal(ibpm),2) '\r\l']);
        lcaPut([bpmNames{ibpm},':chassisWrite'],['0X10' dec2hex(trig2amp(ibpm),2) '\r\l']);
        lcaPut([bpmNames{ibpm},':chassisWrite'],['0X11' dec2hex(amp2rf1(ibpm),2) '\r\l']);
        lcaPut([bpmNames{ibpm},':chassisWrite'],['0X12' dec2hex(rf12rf2(ibpm),2) '\r\l']);
        lcaPut([bpmNames{ibpm},':chassisWrite'],['0X13' dec2hex(rfwidth(ibpm),2) '\r\l']);
        lcaPut([bpmNames{ibpm},':chassisWrite'],['0X14' dec2hex(offtime(ibpm),2) '\r\l']);
        lcaPut([bpmNames{ibpm},':chassisWrite'],['0X00' dec2hex(csr(ibpm),2) '\r\l']);
        break
      catch
        nbad=nbad+1;
      end
    end
    if nbad==3
      error('lcaPut error')
    end
  end
catch
  if ishandle(mh); delete(mh); end;
  errordlg('Failed to restore all firmware settings, aborting','Firmware Restore Failed');
end
if ishandle(mh); delete(mh); end;
popupmenu1_Callback(handles.popupmenu1,'firstcall',handles);



function edit28_Callback(hObject, eventdata, handles)
% hObject    handle to edit28 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

names=get(handles.popupmenu1,'UserData');
val=floor(str2double(get(hObject,'String')));
if val<str2double(get(handles.edit2,'String'))
  val=str2double(get(handles.edit2,'String')); set(hObject,'String',num2str(val));
elseif val>str2double(get(handles.edit29,'String'))
  val=str2double(get(handles.edit29,'String')); set(hObject,'String',num2str(val));
end

lcaPutNoWait([names{1},':',names{2},'Anal_b2.BGRI'],val);
set(hObject,'String',num2str(val));

setBeamParams(handles);

% --- Executes during object creation, after setting all properties.
function edit28_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit28 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit29_Callback(hObject, eventdata, handles)
% hObject    handle to edit29 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

names=get(handles.popupmenu1,'UserData');
val=floor(str2double(get(hObject,'String')));
if val<str2double(get(handles.edit28,'String'))
  val=str2double(get(handles.edit28,'String')); set(hObject,'String',num2str(val));
elseif val>str2double(get(handles.edit3,'String'))
  val=str2double(get(handles.edit3,'String')); set(hObject,'String',num2str(val));
end
lcaPutNoWait([names{1},':',names{2},'Anal_b2.ENRI'],val);
set(hObject,'String',num2str(val));

setBeamParams(handles);

% --- Executes during object creation, after setting all properties.
function edit29_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit29 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in popupmenu4.
function popupmenu4_Callback(hObject, eventdata, handles)
% hObject    handle to popupmenu4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

setBeamParams(handles);


% --- Executes during object creation, after setting all properties.
function popupmenu4_CreateFcn(hObject, eventdata, handles)
% hObject    handle to popupmenu4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit30_Callback(hObject, eventdata, handles)
% hObject    handle to edit30 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit30 as text
%        str2double(get(hObject,'String')) returns contents of edit30 as a double


% --- Executes during object creation, after setting all properties.
function edit30_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit30 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit31_Callback(hObject, eventdata, handles)
% hObject    handle to edit31 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit31 as text
%        str2double(get(hObject,'String')) returns contents of edit31 as a double


% --- Executes during object creation, after setting all properties.
function edit31_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit31 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in pushbutton13.
function pushbutton13_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton13 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
loadDef(handles);


function loadDef(handles,cmd)
% Get N Trains
lcaPut('V2EPICS:iRead','DB_BEAM::BEAMCTRL:NTRAIN');
ntrain=lcaGet('V2EPICS:i');
if exist('cmd','var') && strcmp(cmd,'auto')
  resp='Yes';
else
  resp=questdlg(sprintf('Load waveform ROI for %d Train mode (ALL BPMS)? (takes up to 60s)',...
    ntrain),'Load ROIs');
end
if strcmp(resp,'Yes')
  names=get(handles.popupmenu1,'String');
  for iname=1:length(names)
    lcaPut([names{iname} ':CASR'],0);
  end
  % wait for PVs to upate then re-draw waveforms
  iname=get(handles.popupmenu1,'Value');
  mhan=msgbox('Loading ROI PVs...');
  while lcaGet([names{iname} ':CASR'])==0
    pause(1)
  end
  pause(1)
  lcaPut('STRIPLINE:NTRAIN',ntrain)
  set(handles.text12,'String',num2str(ntrain))
  pushbutton1_Callback(handles.pushbutton1,[],handles);
  if ishandle(mhan); delete(mhan); end;
end


% --- Executes on button press in radiobutton17.
function radiobutton17_Callback(hObject, eventdata, handles)
% hObject    handle to radiobutton17 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of radiobutton17

function chConfigTimerFn(com, ~, handles, bpmName, wfName)
persistent cnum
if exist('com','var') && ischar(com) && strcmp(com,'reset')
  cnum=1;
  return
end
% Attenuation settings
switch cnum
  case 1
    % Waveform display parameters
    wfMaxLen=lcaGet([bpmName,':',wfName,'.NELM']);
    set(handles.edit7,'String','1');
    set(handles.edit8,'String',num2str(wfMaxLen));
    set(handles.uipanel5,'Title',sprintf('Waveform display (max len = %d)',wfMaxLen));
    drawnow('expose');
  case 2
    % Signal ROI
    signalROI=[lcaGet([bpmName,':',wfName,'Anal_main.BGRI']) lcaGet([bpmName,':',wfName,'Anal_main.ENRI'])];
    set(handles.edit1,'String',num2str(signalROI(1)));
    set(handles.edit2,'String',num2str(signalROI(2)));
    drawnow('expose');
  case 3
    % Cal 1 ROI
    if ~strcmp(bpmName(1:2),'MB')
      cal1ROI=[lcaGet([bpmName,':',wfName,'Anal_cal1.BGRI']) lcaGet([bpmName,':',wfName,'Anal_cal1.ENRI'])];
      set(handles.edit3,'String',num2str(cal1ROI(1)));
      set(handles.edit4,'String',num2str(cal1ROI(2)));
      drawnow('expose');
    end
  case 4
    % Cal 2 ROI
    if ~strcmp(bpmName(1:2),'MB')
      cal2ROI=[lcaGet([bpmName,':',wfName,'Anal_cal2.BGRI']) lcaGet([bpmName,':',wfName,'Anal_cal2.ENRI'])];
      set(handles.edit5,'String',num2str(cal2ROI(1)));
      set(handles.edit6,'String',num2str(cal2ROI(2)));
      drawnow('expose');
    end
  case 5
    % X scale & BBA
    xscale=lcaGet([bpmName,':SCALE_X']);
    xbba=lcaGet([bpmName,':BBA_X']);
    set(handles.edit12,'String',num2str(xscale));
    set(handles.edit13,'String',num2str(xbba));
    drawnow('expose');
  case 6
    % Y scale & BBA
    yscale=lcaGet([bpmName,':SCALE_Y']);
    ybba=lcaGet([bpmName,':BBA_Y']);
    set(handles.edit14,'String',num2str(yscale));
    set(handles.edit15,'String',num2str(ybba));
    drawnow('expose');
  case 7
    try
      lcaPut([bpmName,':chassisRead:ATT1.PROC'],1);
      att1_raw=lcaGet([bpmName,':chassisRead:ATT1']);
      att1=hex2dec(char(att1_raw(5:6)));
      set(handles.edit19,'String',num2str(att1));
      set(handles.edit19,'BackgroundColor','yellow');
      drawnow('expose')
      lcaPut([bpmName,':chassisRead:ATT2.PROC'],1);
      att2_raw=lcaGet([bpmName,':chassisRead:ATT2']);
      att2=hex2dec(char(att2_raw(5:6)));
      set(handles.edit20,'String',num2str(att2));
      set(handles.edit20,'BackgroundColor','yellow');
      drawnow('expose')
      lcaPut([bpmName,':chassisRead:CAL.PROC'],1);
      attcal_raw=lcaGet([bpmName,':chassisRead:CAL']);
      attcal=hex2dec(char(attcal_raw(5:6)));
      set(handles.edit21,'BackgroundColor','yellow');
      set(handles.edit21,'String',num2str(attcal));
      drawnow('expose')
    catch
      set(handles.edit19,'BackgroundColor','black');
      set(handles.edit20,'BackgroundColor','black');
      set(handles.edit21,'BackgroundColor','black');
      drawnow('expose')
    end
  case 8
    % cal mode and cal select
    try
      lcaPut([bpmName,':chassisRead:CSR.PROC'],1);
      csr_raw=lcaGet([bpmName,':chassisRead:CSR']);
      csr=hex2dec(char(csr_raw(5:6)));
      if bitget(csr,5) % output limiter disabled
        set(handles.togglebutton2,'Value',1)
        set(handles.togglebutton2,'String','Output Limiter DISABLED')
      else
        set(handles.togglebutton2,'Value',0)
        set(handles.togglebutton2,'String','Output Limiter ENABLED')
      end
      if bitget(csr,4) % cal mode "off"
        set(handles.popupmenu2,'Value',1);
      elseif bitget(csr,3) % cal mode "on"
        set(handles.popupmenu2,'Value',2);
      else % cal mode "auto"
        set(handles.popupmenu2,'Value',3);
      end
      if bitget(csr,1) && bitget(csr,2) % cal select "Nothing"
        set(handles.popupmenu3,'Value',1);
      elseif bitget(csr,2) % cal select "both"
        set(handles.popupmenu3,'Value',2);
      elseif bitget(csr,1) % cal select "left"
        set(handles.popupmenu3,'Value',3);
      else % cal select "up"
        set(handles.popupmenu3,'Value',4);
      end
    catch
      set(handles.popupmenu2,'Value',4);
      set(handles.popupmenu3,'Value',5);
    end
    drawnow('expose')
  case 9
    % firmware version
    try
      lcaPut([bpmName,':chassisRead:VER.PROC'],1);
      ver_raw=lcaGet([bpmName,':chassisRead:VER']);
      ver=hex2dec(char(ver_raw(5:6)));
      set(handles.text7,'String',num2str(ver));
    catch
      set(handles.text7,'BackgroundColor','black');
    end
    drawnow('expose')
  case 10
    % TRIG2AMP
    try
      lcaPut([bpmName,':chassisRead:TRIG2AMP.PROC'],1);
      readval_raw=lcaGet([bpmName,':chassisRead:TRIG2AMP']);
      readval=hex2dec(char(readval_raw(5:6)));
      set(handles.edit22,'String',readval);
    catch
      set(handles.edit22,'BackgroundColor','black');
    end
    drawnow('expose')
  case 11
    % AMP2RF1
    try
      lcaPut([bpmName,':chassisRead:AMP2RF1.PROC'],1);
      readval_raw=lcaGet([bpmName,':chassisRead:AMP2RF1']);
      readval=hex2dec(char(readval_raw(5:6)));
      set(handles.edit23,'String',readval);
    catch
      set(handles.edit23,'BackgroundColor','black');
    end
    drawnow('expose')
  case 12
    % RF12RF2
    try
      lcaPut([bpmName,':chassisRead:RF12RF2.PROC'],1);
      readval_raw=lcaGet([bpmName,':chassisRead:RF12RF2']);
      readval=hex2dec(char(readval_raw(5:6)));
      set(handles.edit24,'String',readval);
    catch
      set(handles.edit24,'BackgoundColor','black');
    end
    drawnow('expose')
  case 13
    % RFWIDTH
    try
      lcaPut([bpmName,':chassisRead:RFWIDTH.PROC'],1);
      readval_raw=lcaGet([bpmName,':chassisRead:RFWIDTH']);
      readval=hex2dec(char(readval_raw(5:6)));
      set(handles.edit26,'String',readval);
    catch
      set(handles.edit26,'BackgroundColor','black');
    end
    drawnow('expose')
  case 14
    % OFFTIME
    try
      lcaPut([bpmName,':chassisRead:OFFTIME.PROC'],1);
      readval_raw=lcaGet([bpmName,':chassisRead:OFFTIME']);
      readval=hex2dec(char(readval_raw(5:6)));
      set(handles.edit27,'String',readval);
    catch
      set(handles.edit27,'BackgroundColor','black');
    end
    drawnow('expose')
  case 15
    cnum=1;
    return
end
cnum=cnum+1;
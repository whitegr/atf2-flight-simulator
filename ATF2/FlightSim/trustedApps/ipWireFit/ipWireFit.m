function varargout = ipWireFit(varargin)
% IPWIREFIT MATLAB code for ipWireFit.fig
%      IPWIREFIT, by itself, creates a new IPWIREFIT or raises the existing
%      singleton*.
%
%      H = IPWIREFIT returns the handle to a new IPWIREFIT or the handle to
%      the existing singleton*.
%
%      IPWIREFIT('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in IPWIREFIT.M with the given input arguments.
%
%      IPWIREFIT('Property','Value',...) creates a new IPWIREFIT or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before ipWireFit_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to ipWireFit_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help ipWireFit

% Last Modified by GUIDE v2.5 04-Dec-2012 01:17:01

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @ipWireFit_OpeningFcn, ...
                   'gui_OutputFcn',  @ipWireFit_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before ipWireFit is made visible.
function ipWireFit_OpeningFcn(hObject, eventdata, handles, varargin) %#ok<*INUSL>
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to ipWireFit (see VARARGIN)

% Choose default command line output for ipWireFit
handles.output = handles;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes ipWireFit wait for user response (see UIRESUME)
% uiwait(handles.figure1);
lcaSetSeverityWarnLevel(9)
lcaSetSeverityWarnLevel(19)
warning('off','MATLAB:rankDeficientMatrix');

% Set wire diameter from last session
if exist('/home/atf2-fs/ATF2/FlightSim/userData/guiData/ipWireFit.mat','file')
  load /home/atf2-fs/ATF2/FlightSim/userData/guiData/ipWireFit.mat wireDiam
  set(handles.edit1,'String',num2str(wireDiam*1e6))
end

% Get a data set
pushbutton1_Callback(handles.pushbutton1,[],handles);


% --- Outputs from this function are returned to the command line.
function varargout = ipWireFit_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on button press in pushbutton1.
function pushbutton1_Callback(hObject, eventdata, handles) %#ok<*DEFNU>
% hObject    handle to pushbutton1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
persistent ndata

if isempty(ndata); ndata=0; end;

if get(handles.popupmenu1,'Value')==1
  gfact=sind(37.5752);
else
  gfact=cosd(37.5752);
end
set(handles.text2,'String','Getting new data from VSYSTEM...');drawnow('expose')

% --- Fetch new data from VSYSTEM

if get(handles.popupmenu1,'Value')==1
  vchix_p='DB_IPBSM::MWIP:EVENT.POSITION.1';
  vchix_s='DB_IPBSM::MWIP:EVENT.SIGNAL.1';
else
  vchix_p='DB_IPBSM::MWIP:EVENT.POSITION.2';
  vchix_s='DB_IPBSM::MWIP:EVENT.SIGNAL.2';
end

pvname='V2EPICS:rwaveRead';

lcaPut(pvname,vchix_p); pause(1);
p=lcaGet('V2EPICS:rwave'); p(p==0)=[];p=p.*1e-3;
pd=diff(p);bound=find(abs(pd)>5*abs(pd(1)),1);
if ~isempty(bound)
  p=p(1:bound-1);
end
p=p.*gfact;
p=unique(p);
%p=p-mean(p);
lcaPut(pvname,vchix_s); pause(1);
s=lcaGet('V2EPICS:rwave'); s(length(p)+1:end)=[];

% -- normalise and set background to 0 for signal
[~,q] = gauss_fit(p,s); s=s-q(1); s=s./max(abs(s));

% --- Plot raw data
plot(handles.axes1,p.*1e6,s./max(abs(s)),'*'), hold(handles.axes1,'on')
drawnow('expose');

% Fit convoluted function
wdiam=str2double(get(handles.edit1,'String')); wdiam=wdiam*1e-6;
try
  [gaussVal, gaussData, convVal, convData]=wirefitFn(p,s,wdiam);
catch ME
  set(handles.text2,'String',sprintf('Fitting failed!\n(%s)',ME.message))
  hold(handles.axes1,'off')
  return
end
if isfield(convData,'wireDiam')
  set(handles.edit1,'String',num2str(convData.wireDiam*1e6))
  wireDiam=convData.wireDiam;
  save /home/atf2-fs/ATF2/FlightSim/userData/guiData/ipWireFit.mat wireDiam
end

% Display fitted data and plot fitted functions
dstr=sprintf('Gaussian fit:\n sigma = %.2f (RESNORM= %.3f)\nConvolution fit:\n sigma = %.2f (RESNORM = %.3f)',...
  gaussVal*1e6,gaussData.chi2,convVal*1e6,convData.chi2);
set(handles.text2,'String',dstr);
plot(handles.axes1,gaussData.x.*1e6,gaussData.y,'k')
cx=convData.x;cy=convData.y;
cy(cx<gaussData.x(1)|cx>gaussData.x(end))=[];
cx(cx<gaussData.x(1)|cx>gaussData.x(end))=[];
plot(handles.axes1,cx.*1e6,cy,'r'), hold(handles.axes1,'off')
grid on
legend(handles.axes1,{'Data','Gaussian','Convolution'})
xlabel('Y^* / um')
ylabel('Normalised signal')
drawnow('expose')

[~,I]=max(gaussData.y);
meanPos=gaussData.x(I);
ndata=ndata+1;
set(handles.figure1,'UserData',[gaussVal,convVal,meanPos,ndata]);


function edit1_Callback(hObject, eventdata, handles) %#ok<*INUSD>
% hObject    handle to edit1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

wireDiam=str2double(get(hObject,'String'))*1e-6;
save /home/atf2-fs/ATF2/FlightSim/userData/guiData/ipWireFit.mat wireDiam


% --- Executes during object creation, after setting all properties.
function edit1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in popupmenu1.
function popupmenu1_Callback(hObject, eventdata, handles)
% hObject    handle to popupmenu1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns popupmenu1 contents as cell array
%        contents{get(hObject,'Value')} returns selected item from popupmenu1


% --- Executes during object creation, after setting all properties.
function popupmenu1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to popupmenu1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in pushbutton2.
function pushbutton2_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

set(handles.edit1,'String','0');
pushbutton1_Callback(handles.pushbutton1,[],handles);


% --- Executes when user attempts to close figure1.
function figure1_CloseRequestFcn(hObject, eventdata, handles)
% hObject    handle to figure1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global FL
% Hint: delete(hObject) closes the figure
try
  FL.Gui=rmfield(FL.Gui,'ipWireFit');
catch
end
delete(hObject);

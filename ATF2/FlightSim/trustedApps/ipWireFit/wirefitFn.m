function [gaussVal, gaussData, convVal, convData]=wirefitFn(pos,signal,wireDiam)
% pos in mm

% simple Gaussian fit to data
[gd,q] = gauss_fit(pos,signal);
gaussVal=q(4); xoff=q(3);
gaussData.x=linspace(pos(1),pos(end),10000);
gaussData.y=interp1(pos,gd,gaussData.x,'spline');gaussData.y=gaussData.y./max(abs(gaussData.y));
gaussData.chi2=sum((gd-signal').^2);
pos=pos-xoff;
signal(abs(pos)>gaussVal*4)=[];
pos(abs(pos)>gaussVal*4)=[];
if wireDiam==0 % fit wire diameter
  wireDiam=lsqnonlin(@(x) fitmin(x,pos,signal,gaussVal),5e-6,1e-6,10e-6,...
  optimset('Display','off','TolFun',1e-12));
  convData.wireDiam=wireDiam;
end
[convVal, resnorm]=lsqnonlin(@(x) minfunc(x,pos,signal,wireDiam),gaussVal,1e-8,1e-3,...
  optimset('Display','off'));
[cx,cy]=wireAnal(convVal,wireDiam);
convData.x=cx+xoff;
convData.y=cy./max(abs(cy));
convData.chi2=resnorm;

function resnorm=fitmin(wd,pos,sig,gaussVal)

[~, resnorm]=lsqnonlin(@(x) minfunc(x,pos,sig,wd),gaussVal,1e-8,1e-3,...
  optimset('Display','off','TolX',1e-12,'TolFun',1e-6^2));



function chi2=minfunc(x,pos,sig,wd)

% Get convoluted function for this trial gaussian beam width
[C_x, C_y]=wireAnal(x,wd);

% Normalise amplitude
C_y=C_y./max(abs(C_y));

% Get amplitude and data locations
yConv=interp1(C_x,C_y,pos,'linear');

% Form chi2 return value
chi2=sig-yConv;
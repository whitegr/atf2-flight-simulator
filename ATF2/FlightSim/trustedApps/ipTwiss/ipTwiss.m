function varargout = ipTwiss(varargin)
% IPTWISS MATLAB code for ipTwiss.fig
%      IPTWISS, by itself, creates a new IPTWISS or raises the existing
%      singleton*.
%
%      H = IPTWISS returns the handle to a new IPTWISS or the handle to
%      the existing singleton*.
%
%      IPTWISS('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in IPTWISS.M with the given input arguments.
%
%      IPTWISS('Property','Value',...) creates a new IPTWISS or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before ipTwiss_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to ipTwiss_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help ipTwiss

% Last Modified by GUIDE v2.5 04-Dec-2012 10:39:35

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @ipTwiss_OpeningFcn, ...
                   'gui_OutputFcn',  @ipTwiss_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before ipTwiss is made visible.
function ipTwiss_OpeningFcn(hObject, eventdata, handles, varargin) %#ok<*INUSL>
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to ipTwiss (see VARARGIN)
global FL

% Choose default command line output for ipTwiss
handles.output = handles;

% Update handles structure
guidata(hObject, handles);

useApp('ipWireFit');
if ~isfield(FL.Gui,'ipWireFit') || ~ishandle(FL.Gui.ipWireFit.figure1)
  FL.Gui.ipWireFit=ipWireFit;
end

popupmenu1_Callback(handles.popupmenu1,[],handles);

% --- Outputs from this function are returned to the command line.
function varargout = ipTwiss_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on button press in pushbutton2.
function pushbutton2_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global BEAMLINE PS FL

set(handles.pushbutton2,'Enable','off')
set(handles.text5,'Enable','off')
set(handles.text7,'Enable','off')
set(handles.text50,'Enable','off')
set(handles.text6,'Enable','off')
set(handles.text8,'Enable','off')
set(handles.text49,'Enable','off')

% Get Most recent OTR data
dd=dir('userData/emit2dOTRp*.mat');
[~,I]=sort([dd.datenum]);
ld=load(sprintf('userData/%s',dd(I(end)).name),'data');
ud=get(handles.figure1,'UserData');

% Get expected scan response based on OTR data
FL.noUpdate=true;
init.BL=BEAMLINE;
init.PS=PS;
iotr=findcells(BEAMLINE,'Name','OTR0X');
Tx.beta=ld.data.betax;
Tx.alpha=ld.data.alphx;
Tx.eta=0;
Tx.etap=0;
Tx.nu=0;
Ty.beta=ld.data.betay;
Ty.alpha=ld.data.alphy;
Ty.eta=0;
Ty.etap=0;
Ty.nu=0;
qd0=findcells(BEAMLINE,'Name','QD0FF'); qd0=BEAMLINE{qd0(1)}.PS;
qf1=findcells(BEAMLINE,'Name','QF1FF'); qf1=BEAMLINE{qf1(1)}.PS;
[~,T]=GetTwiss(iotr,PS(qf1).Element(1)-1,Tx,Ty);
Tx.beta=T.betax(end); Tx.alpha=T.alphax(end); Tx.eta=T.etax(end); Tx.etap=T.etapx(end);
Ty.beta=T.betax(end); Ty.alpha=T.alphax(end); Ty.eta=T.etax(end); Ty.etap=T.etapx(end);
gamma=FL.SimModel.Initial.Momentum/0.511e-3;
I=FL.SimModel.Initial;
I.x.NEmit=ld.data.emitx*gamma;
I.y.NEmit=ld.data.emity*gamma;
I.x.Twiss=Tx; I.y.Twiss=Ty;

B=MakeBeam6DGauss(I,10000,3,1);
qvals=ud{1};
for iq=1:length(qvals)
  PS(qd0).Ampl=qvals(iq);
  [~,bo]=TrackThru(PS(qf1).Element(1),FL.SimModel.ip_ind,B,1,1,0);
  sig_y(iq)=std(bo.Bunch.x(3,:));
end
q=noplot_parab(qvals,sig_y.^2);
Ly_ps=q(2);
PS=init.PS;
for iq=1:length(qvals)
  PS(qf1).Ampl=qvals(iq);
  [~,bo]=TrackThru(PS(qf1).Element(1),FL.SimModel.ip_ind,B,1,1,0);
  sig_x(iq)=std(bo.Bunch.x(1,:));
end
q=noplot_parab(qvals,sig_x.^2);
Lx_ps=q(2);
BEAMLINE=init.BL;
PS=init.PS;
FL.noUpdate=false;
Lx_amp=interp1(FL.HwInfo.PS(qf1).conv(2,:),FL.HwInfo.PS(qf1).conv(1,:),...
      Lx_ps.*abs(BEAMLINE{PS(qf1).Element(1)}.B(1)),'linear');
Ly_amp=interp1(FL.HwInfo.PS(qf1).conv(2,:),FL.HwInfo.PS(qf1).conv(1,:),...
      Ly_ps.*abs(BEAMLINE{PS(qf1).Element(1)}.B(1)),'linear');
ud{9}={[ld.data.emitx ld.data.emity ld.data.waist.betax ld.data.waist.betay ...
  ld.data.waist.Lx ld.data.waist.Ly Lx_amp Ly_amp Lx_ps Ly_ps] sig_x sig_y};
set(handles.figure1,'UserData',ud);
set(handles.text5,'String',num2str(ld.data.waist.betax*1e3))
set(handles.text7,'String',num2str(ld.data.emitx*1e9))
set(handles.text50,'String',sprintf('%.3f',ld.data.waist.Lx*1e3))
set(handles.text6,'String',num2str(ld.data.waist.betay*1e3))
set(handles.text8,'String',num2str(ld.data.emity*1e12))
set(handles.text49,'String',sprintf('%.3f',ld.data.waist.Ly*1e3))

set(handles.pushbutton2,'Enable','on')
set(handles.text5,'Enable','on')
set(handles.text7,'Enable','on')
set(handles.text50,'Enable','on')
set(handles.text6,'Enable','on')
set(handles.text8,'Enable','on')
set(handles.text49,'Enable','on')
    
% --- Executes on button press in togglebutton2.
function togglebutton2_Callback(hObject, eventdata, handles)
% hObject    handle to togglebutton2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of togglebutton2


% --- Executes on button press in pushbutton1.
function pushbutton1_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

set(handles.pushbutton1,'Enable','off')
set(handles.edit1,'Enable','off')
set(handles.text1,'Enable','off')
set(handles.text2,'Enable','off')
set(handles.text3,'Enable','off')
set(handles.text4,'Enable','off')
drawnow('expose')

latticeFile=['latticeFiles/',get(handles.edit1,'String')];
ud=get(handles.figure1,'UserData');
qvals=ud{1}-mean(ud{1})+1;
[ax,ay,betax,betay,ipsize_x,ipsize_y]=fdscan(qvals,latticeFile);
ud{4}=ipsize_x; ud{5}=ipsize_y;
set(handles.figure1,'UserData',ud)
set(handles.text1,'String',num2str(betax*1e3))
set(handles.text2,'String',num2str(betay*1e3))
set(handles.text3,'String',num2str(ax))
set(handles.text4,'String',num2str(ay))

set(handles.pushbutton1,'Enable','on')
set(handles.edit1,'Enable','on')
set(handles.text1,'Enable','on')
set(handles.text2,'Enable','on')
set(handles.text3,'Enable','on')
set(handles.text4,'Enable','on')
drawnow('expose')

% --- Executes on button press in togglebutton1.
function togglebutton1_Callback(hObject, eventdata, handles)
% hObject    handle to togglebutton1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of togglebutton1



function edit1_Callback(hObject, eventdata, handles)
% hObject    handle to edit1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit1 as text
%        str2double(get(hObject,'String')) returns contents of edit1 as a double


% --- Executes during object creation, after setting all properties.
function edit1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in pushbutton3.
function pushbutton3_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global FL
whichQuad=get(handles.popupmenu1,'Value');
whichMethod=get(handles.popupmenu3,'Value');
ax=str2double(get(handles.text3,'String'));
ay=str2double(get(handles.text4,'String'));
if isnan(ax) || isnan(ay)
  errordlg('No model data computed, push Model ''Calc'' button','Calc Error')
  return
end
ud=get(handles.figure1,'UserData');
qvals_kl=ud{2};
qvals_amp=ud{3};
qvals_ps=ud{1};
if whichQuad==1
  sigvals=ud{7}{2}(:,whichMethod);
else
  sigvals=ud{7}{1}(:,whichMethod);
end
sel=~isnan(sigvals);
if sum(sel)<3
  errordlg('Need to measure at least 3 points!','Calc Error')
  return
end
qvals=qvals_kl(sel); sigvals=sigvals(sel);
qvals_amp=qvals_amp(sel); qvals_ps=qvals_ps(sel);
dispvals=ud{8}{3}; dispvals=dispvals(sel);
dispsel=~isnan(sigvals);
if ~any(dispsel)
  dispvals=zeros(size(dispvals));
elseif sum(dispsel)<2
  errordlg('Must take no dispersion data or >= 2 sets','Calc Error')
  return
elseif sum(dispsel)~=length(dispsel)
  dispvals(~dispsel)=interp1(qvals(dispsel),dispvals(dispsel),qvals(~dispsel),'linear');
end
if isfield(FL,'props') && isfield(FL.props,'dE')
  dE=FL.props.dE;
else
  dE=0.0008;
end
sigvals=sigvals'.^2-(dE^2.*dispvals.^2);
q=noplot_parab(qvals,sigvals);
q_amp=noplot_parab(qvals_amp,sigvals);
emity=ud{9}{1}(2);
if whichQuad==1
  if isnan(emity)
    errordlg('Must first get OTR data to determine vertical emittance','Calc Error');
    return
  end
  beta=(ay^2*emity)/q(1);
  set(handles.text10,'String',num2str(beta*1e3))
else
  beta=ax*sqrt(q(3)/q(1));
  emit=sqrt(q(1)*q(3))/ax;
  set(handles.text9,'String',num2str(beta*1e3))
  set(handles.text11,'String',num2str(emit*1e9))
end
set(handles.text48,'String',num2str(q_amp(2)))
q_ps=noplot_parab(qvals_ps,sigvals);
set(hObject,'UserData',q_ps(2));

plotFn(handles);

% --- Executes on button press in pushbutton6.
function pushbutton6_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton6 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
setRamp(handles,'plus');

% --- Executes on button press in pushbutton7.
function pushbutton7_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton7 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
getScanPos(handles,'plus',1);

% --- Executes on button press in pushbutton8.
function pushbutton8_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton8 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
setRamp(handles,'minus');

% --- Executes on button press in pushbutton9.
function pushbutton9_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton9 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
getScanPos(handles,'minus',1);

% --- Executes on button press in pushbutton10.
function pushbutton10_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton10 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
setRamp(handles,'off');

% --- Executes on button press in pushbutton5.
function pushbutton5_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton5 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
getBS(handles,1);

% --- Executes on button press in pushbutton4.
function pushbutton4_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
setQuad(handles,1);

% --- Executes on button press in pushbutton11.
function pushbutton11_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton11 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
setRamp(handles,'plus');

% --- Executes on button press in pushbutton12.
function pushbutton12_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton12 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
getScanPos(handles,'plus',2);

% --- Executes on button press in pushbutton13.
function pushbutton13_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton13 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
setRamp(handles,'minus');

% --- Executes on button press in pushbutton14.
function pushbutton14_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton14 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
getScanPos(handles,'minus',2);

% --- Executes on button press in pushbutton15.
function pushbutton15_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton15 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
setRamp(handles,'off');

% --- Executes on button press in pushbutton16.
function pushbutton16_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton16 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
getBS(handles,2);

% --- Executes on button press in pushbutton17.
function pushbutton17_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton17 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
setQuad(handles,2);

% --- Executes on button press in pushbutton18.
function pushbutton18_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton18 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
setRamp(handles,'plus');

% --- Executes on button press in pushbutton19.
function pushbutton19_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton19 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
getScanPos(handles,'plus',3);

% --- Executes on button press in pushbutton20.
function pushbutton20_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton20 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
setRamp(handles,'minus');

% --- Executes on button press in pushbutton21.
function pushbutton21_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton21 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
getScanPos(handles,'minus',3);

% --- Executes on button press in pushbutton22.
function pushbutton22_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton22 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
setRamp(handles,'off');

% --- Executes on button press in pushbutton23.
function pushbutton23_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton23 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
getBS(handles,3);

% --- Executes on button press in pushbutton24.
function pushbutton24_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton24 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
setQuad(handles,3);

% --- Executes on button press in pushbutton25.
function pushbutton25_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton25 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
setRamp(handles,'plus');

% --- Executes on button press in pushbutton26.
function pushbutton26_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton26 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
getScanPos(handles,'plus',4);

% --- Executes on button press in pushbutton27.
function pushbutton27_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton27 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
setRamp(handles,'minus');

% --- Executes on button press in pushbutton28.
function pushbutton28_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton28 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
getScanPos(handles,'minus',4);

% --- Executes on button press in pushbutton29.
function pushbutton29_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton29 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
setRamp(handles,'off');

% --- Executes on button press in pushbutton30.
function pushbutton30_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton30 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
getBS(handles,4);

% --- Executes on button press in pushbutton31.
function pushbutton31_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton31 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
setQuad(handles,4);

% --- Executes on button press in pushbutton32.
function pushbutton32_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton32 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
setRamp(handles,'plus');

% --- Executes on button press in pushbutton33.
function pushbutton33_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton33 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
getScanPos(handles,'plus',5);

% --- Executes on button press in pushbutton34.
function pushbutton34_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton34 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
setRamp(handles,'minus');

% --- Executes on button press in pushbutton35.
function pushbutton35_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton35 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
getScanPos(handles,'minus',5);

% --- Executes on button press in pushbutton36.
function pushbutton36_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton36 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
setRamp(handles,'off');

% --- Executes on button press in pushbutton37.
function pushbutton37_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton37 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
getBS(handles,5);

% --- Executes on button press in pushbutton38.
function pushbutton38_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton38 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
setQuad(handles,5);

% --- Executes on button press in pushbutton39.
function pushbutton39_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton39 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
setRamp(handles,'plus');

% --- Executes on button press in pushbutton40.
function pushbutton40_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton40 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
getScanPos(handles,'plus',6);

% --- Executes on button press in pushbutton41.
function pushbutton41_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton41 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
setRamp(handles,'minus');

% --- Executes on button press in pushbutton42.
function pushbutton42_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton42 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
getScanPos(handles,'minus',6);

% --- Executes on button press in pushbutton43.
function pushbutton43_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton43 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
setRamp(handles,'off');

% --- Executes on button press in pushbutton44.
function pushbutton44_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton44 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
getBS(handles,6);

% --- Executes on button press in pushbutton45.
function pushbutton45_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton45 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
setQuad(handles,6);

% --- Executes on button press in pushbutton46.
function pushbutton46_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton46 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
setRamp(handles,'plus');

% --- Executes on button press in pushbutton47.
function pushbutton47_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton47 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
getScanPos(handles,'plus',7);

% --- Executes on button press in pushbutton48.
function pushbutton48_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton48 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
setRamp(handles,'minus');

% --- Executes on button press in pushbutton49.
function pushbutton49_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton49 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
getScanPos(handles,'minus',7);

% --- Executes on button press in pushbutton50.
function pushbutton50_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton50 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
setRamp(handles,'off');

% --- Executes on button press in pushbutton51.
function pushbutton51_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton51 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
getBS(handles,7);

% --- Executes on button press in pushbutton52.
function pushbutton52_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton52 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
setQuad(handles,7);

% --- Executes on selection change in popupmenu1.
function popupmenu1_Callback(hObject, eventdata, handles)
% hObject    handle to popupmenu1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global PS BEAMLINE FL

clearData(handles);

edit3_Callback(handles.edit3,[],handles);

if get(hObject,'Value')==2
  set(handles.popupmenu3,'Value',2)
else
  set(handles.popupmenu3,'Value',1)
end
popupmenu3_Callback(handles.popupmenu3,[],handles);

qd0=findcells(BEAMLINE,'Name','QD0FF'); qd0=BEAMLINE{qd0(1)}.PS;
qf1=findcells(BEAMLINE,'Name','QF1FF'); qf1=BEAMLINE{qf1(1)}.PS;
ud=get(handles.figure1,'UserData');
if get(hObject,'Value')==1
  psval=PS(qd0).Ampl;
  qamp=interp1(FL.HwInfo.PS(qd0).conv(2,:),FL.HwInfo.PS(qd0).conv(1,:),...
      psval*abs(BEAMLINE{PS(qd0).Element(1)}.B(1)),'linear');
else
  psval=PS(qf1).Ampl;
  qamp=interp1(FL.HwInfo.PS(qf1).conv(2,:),FL.HwInfo.PS(qf1).conv(1,:),...
      psval*abs(BEAMLINE{PS(qf1).Element(1)}.B(1)),'linear');
end
ud{6}=psval;
set(handles.figure1,'UserData',ud);
set(handles.text47,'String',num2str(qamp));

% Set ipWireFit app to take correct Vsystem panel data (for x or y dim)
if get(hObject,'Value')==1
  set(FL.Gui.ipWireFit.popupmenu1,'Value',2);
else
  set(FL.Gui.ipWireFit.popupmenu1,'Value',2);
end
ipWireFit('popupmenu1_Callback',FL.Gui.ipWireFit.popupmenu1,[],FL.Gui.ipWireFit);

% --- Executes during object creation, after setting all properties.
function popupmenu1_CreateFcn(hObject, eventdata, handles) %#ok<*INUSD>
% hObject    handle to popupmenu1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in pushbutton53.
function pushbutton53_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton53 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

popupmenu1_Callback(handles.popupmenu1,[],handles);

function edit3_Callback(hObject, eventdata, handles)
% hObject    handle to edit3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global BEAMLINE PS FL

whichQuad=get(handles.popupmenu1,'Value');
qd0=findcells(BEAMLINE,'Name','QD0FF'); qd0=BEAMLINE{qd0(1)}.PS;
qf1=findcells(BEAMLINE,'Name','QF1FF'); qf1=BEAMLINE{qf1(1)}.PS;
dq=str2double(get(handles.edit3,'String'))*1e-2;
clight=299792458; % speed of light (m/s)
Cb=1e9/clight; % T-m/GeV
brho=Cb*FL.SimModel.Initial.Momentum;
if whichQuad==1
  qvals=linspace(PS(qd0).Ampl-dq,PS(qd0).Ampl+dq,7);
  qvals_kl=(qvals.*GetTrueStrength(PS(qd0).Element(1)).*2)./brho;
  qvals_amp=interp1(FL.HwInfo.PS(qd0).conv(2,:),FL.HwInfo.PS(qd0).conv(1,:),...
      qvals.*abs(BEAMLINE{PS(qd0).Element(1)}.B(1)),'linear');
else
  qvals=linspace(PS(qf1).Ampl-dq,PS(qf1).Ampl+dq,7);
  qvals_kl=(qvals.*GetTrueStrength(PS(qf1).Element(1)).*2)./brho;
  qvals_amp=interp1(FL.HwInfo.PS(qf1).conv(2,:),FL.HwInfo.PS(qf1).conv(1,:),...
      qvals.*abs(BEAMLINE{PS(qf1).Element(1)}.B(1)),'linear');
end
ud=get(handles.figure1,'UserData');
ud{1}=qvals; ud{2}=qvals_kl; ud{3}=qvals_amp;
set(handles.figure1,'UserData',ud)
set(handles.text12,'String',num2str(qvals_amp(1)))
set(handles.text21,'String',num2str(qvals_amp(2)))
set(handles.text26,'String',num2str(qvals_amp(3)))
set(handles.text31,'String',num2str(qvals_amp(4)))
set(handles.text36,'String',num2str(qvals_amp(5)))
set(handles.text41,'String',num2str(qvals_amp(6)))
set(handles.text46,'String',num2str(qvals_amp(7)))

uipan=[16 20 24 28 32 36 40];
for ipan=1:length(uipan)
  set(handles.(sprintf('uipanel%d',uipan(ipan))),'BackgroundColor','white')
end
drawnow('expose')

% --- Executes during object creation, after setting all properties.
function edit3_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in pushbutton54.
function pushbutton54_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton54 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)



% --- Executes on selection change in popupmenu3.
function popupmenu3_Callback(hObject, eventdata, handles)
% hObject    handle to popupmenu3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns popupmenu3 contents as cell array
%        contents{get(hObject,'Value')} returns selected item from popupmenu3


% --- Executes during object creation, after setting all properties.
function popupmenu3_CreateFcn(hObject, eventdata, handles)
% hObject    handle to popupmenu3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in popupmenu2.
function popupmenu2_Callback(hObject, eventdata, handles)
% hObject    handle to popupmenu2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns popupmenu2 contents as cell array
%        contents{get(hObject,'Value')} returns selected item from popupmenu2


% --- Executes during object creation, after setting all properties.
function popupmenu2_CreateFcn(hObject, eventdata, handles) %#ok<*DEFNU>
% hObject    handle to popupmenu2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function setQuad(handles,qnum)
global PS BEAMLINE
persistent lastChange


uipan=[16 20 24 28 32 36 40];
whichQuad=get(handles.popupmenu1,'Value');
if whichQuad==1
  qps=findcells(BEAMLINE,'Name','QD0FF'); qps=BEAMLINE{qps(1)}.PS;
else
  qps=findcells(BEAMLINE,'Name','QF1FF'); qps=BEAMLINE{qps(1)}.PS;
end

ud=get(handles.figure1,'UserData');
sp=PS(qps).SetPt;
if (ud{1}(qnum)-PS(qps).SetPt)<0 && ( isempty(lastChange) || lastChange>0 )
  PS(qps).SetPt=ud{1}(qnum)*0.9;
  PSTrim(qps,1);
  pause(2);
end
lastChange=sign(ud{1}(qnum)-sp);
PS(qps).SetPt=ud{1}(qnum);
PSTrim(qps,1);
for ipan=1:length(uipan)
  if ipan==qnum
    set(handles.(sprintf('uipanel%d',uipan(ipan))),'BackgroundColor','cyan')
  else
    set(handles.(sprintf('uipanel%d',uipan(ipan))),'BackgroundColor','white')
  end
end
drawnow('expose')


% --- Executes on button press in pushbutton55.
function pushbutton55_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton55 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global PS BEAMLINE

whichQuad=get(handles.popupmenu1,'Value');
qd0=findcells(BEAMLINE,'Name','QD0FF'); qd0=BEAMLINE{qd0(1)}.PS;
qf1=findcells(BEAMLINE,'Name','QF1FF'); qf1=BEAMLINE{qf1(1)}.PS;
ud=get(handles.figure1,'UserData');
if whichQuad==1
  PS(qd0).SetPt=ud{6};
  PSTrim(qd0,1);
else
  PS(qf1).SetPt=ud{6};
  PSTrim(qf1,1);
end

uipan=[16 20 24 28 32 36 40];
for ipan=1:length(uipan)
  set(handles.(sprintf('uipanel%d',uipan(ipan))),'BackgroundColor','white')
end
drawnow('expose')

function getBS(handles,idat)
global FL

if ~isfield(FL.Gui,'ipWireFit') || ~ishandle(FL.Gui.ipWireFit.figure1)
  FL.Gui.ipWireFit=ipWireFit;
end



tval=[13 20 25 30 35 40 45];
ud=get(handles.figure1,'UserData');
wud=get(FL.Gui.ipWireFit.figure1,'UserData');
dataInd=wud(4);
ipWireFit('pushbutton1_Callback',FL.Gui.ipWireFit.pushbutton1,[],FL.Gui.ipWireFit);
t0=clock;
while 1
  wud=get(FL.Gui.ipWireFit.figure1,'UserData');
  if wud(4)>dataInd
    break
  elseif etime(clock,t0)>10
    errordlg('Error getting new beam data from ipWireFit application','ipWireFit Error');
    return
  end
end
whichQuad=get(handles.popupmenu1,'Value');
whichMethod=get(handles.popupmenu3,'Value');
if whichQuad==1
  ud{7}{2}(idat,:)=wud(1:3);
else
  ud{7}{1}(idat,:)=wud(1:3);
end
if whichMethod==1
  writeVal=wud(2)*1e6;
else
  writeVal=wud(1)*1e6;
end
set(handles.figure1,'UserData',ud);
set(handles.(sprintf('text%d',tval(idat))),'String',num2str(writeVal));

function setRamp(handles,state)

pbut=[6 11 18 25 32 39 46];
mbut=[8 13 20 27 34 41 48];
offbut=[10 15 22 29 36 43 50];

for ibut=[pbut mbut offbut]
  set(handles.(sprintf('pushbutton%d',ibut)),'BackgroundColor','white');
end
df=get(handles.popupmenu2,'Value');
switch state
  case 'plus'
    but=pbut;
    DRRF_RampControl('on');
    DRRF_RampControl(df);
  case 'minus'
    but=mbut;
    DRRF_RampControl('on');
    DRRF_RampControl(-df);
  case 'off'
    but=offbut;
    DRRF_RampControl('off');
end
for ibut=but
  set(handles.(sprintf('pushbutton%d',ibut)),'BackgroundColor','green');
end

function getScanPos(handles,dir,idat)
global FL

ptxt=[15 18 23 28 33 38 43];
mtxt=[16 19 24 29 34 39 44];
dtxt=[14 17 22 27 32 37 42];

if ~isfield(FL.Gui,'ipWireFit') || ~ishandle(FL.Gui.ipWireFit.figure1)
  FL.Gui.ipWireFit=ipWireFit;
end
ud=get(handles.figure1,'UserData');
wud=get(FL.Gui.ipWireFit.figure1,'UserData');
dataInd=wud(4);
ipWireFit('pushbutton1_Callback',FL.Gui.ipWireFit.pushbutton1,[],FL.Gui.ipWireFit);
t0=clock;
while 1
  wud=get(FL.Gui.ipWireFit.figure1,'UserData');
  if wud(4)>dataInd
    break
  elseif etime(clock,t0)>10
    errordlg('Error getting new beam data from ipWireFit application','ipWireFit Error');
    return
  end
end

switch dir
  case 'plus'
    ud{8}{1}(idat)=wud(3);
    set(handles.(sprintf('text%d',ptxt(idat))),'String',num2str(wud(3)*1e3));
  case 'minus'
    ud{8}{2}(idat)=wud(3);
    set(handles.(sprintf('text%d',mtxt(idat))),'String',num2str(wud(3)*1e3));
end

if ~isnan(ud{8}{1}(idat)) && ~isnan(ud{8}{2}(idat))
  dpos=abs(ud{8}{1}(idat)-ud{8}{2}(idat));
  dp=get(handles.popupmenu2,'Value')*2*1e3; % kHz
  alpha=str2double(get(handles.edit4,'String'))*1e-3;
  dp=-(dp/714e6)/alpha; % dP/P
  ud{8}{3}(idat)=dpos/dp;
  set(handles.(sprintf('text%d',dtxt(idat))),'String',num2str((dpos/dp)*1e3));
end

set(handles.figure1,'UserData',ud);

function edit4_Callback(hObject, eventdata, handles)
% hObject    handle to edit4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit4 as text
%        str2double(get(hObject,'String')) returns contents of edit4 as a double


% --- Executes during object creation, after setting all properties.
function edit4_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in pushbutton56.
function pushbutton56_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton56 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global PS BEAMLINE

minps=get(handles.pushbutton3,'UserData');
whichQuad=get(handles.popupmenu1,'Value');
qd0=findcells(BEAMLINE,'Name','QD0FF'); qd0=BEAMLINE{qd0(1)}.PS;
qf1=findcells(BEAMLINE,'Name','QF1FF'); qf1=BEAMLINE{qf1(1)}.PS;

if whichQuad==1
  PS(qd0).SetPt=minps;
  PSTrim(qd0,1);
else
  PS(qf1).SetPt=minps;
  PSTrim(qf1,1);
end

uipan=[16 20 24 28 32 36 40];
for ipan=1:length(uipan)
  set(handles.(sprintf('uipanel%d',uipan(ipan))),'BackgroundColor','white')
end
drawnow('expose')

function plotFn(handles)
global FL

ud=get(handles.figure1,'UserData');
whichQuad=get(handles.popupmenu1,'Value');
whichMethod=get(handles.popupmenu3,'Value');
dispvals=ud{8}{3};

qvals=ud{3};
% Plot Model values
if whichQuad==1
  mbsvals=ud{5};
else
  mbsvals=ud{4};
end
if any(~isnan(mbsvals)) && get(handles.togglebutton1,'Value')
  q=noplot_parab(qvals,mbsvals);
  qcurve=linspace(qvals(1),qvals(end),1000);
  plot(qcurve,q(1).*(qcurve-q(2)).^2+q(3),'r')
  hold(handles.axes1,'on')
end

% Plot OTR propogated data
obsvals=ud{9}{2};
if any(~isnan(obsvals)) && get(handles.togglebutton2,'Value')
  q=noplot_parab(qvals,obsvals);
  qcurve=linspace(qvals(1),qvals(end),1000);
  plot(qcurve,q(1).*(qcurve-q(2)).^2+q(3),'b')
  hold(handles.axes1,'on')
end

% Plot measured beam sizes
if whichQuad==1
  bsvals=ud{7}{2}(:,whichMethod);
else
  bsvals=ud{7}{1}(:,whichMethod);
end
sel=~isnan(bsvals);
if sum(sel)<3
  hold(handles.axes1,'off');
  return
end
bsvals=bsvals(sel);
qvals=qvals(sel);
if isfield(FL,'props') && isfield(FL.props,'dE')
  dE=FL.props.dE;
else
  dE=0.0008;
end
bsvals=sqrt(bsvals'.^2-(dE^2.*dispvals(sel).^2));
plot(handles.axes1,qvals,bsvals.^2,'k*');
hold(handles.axes1,'on');
q=noplot_parab(qvals,bsvals.^2);
qcurve=linspace(qvals(1),qvals(end),1000);
plot(qcurve,q(1).*(qcurve-q(2)).^2+q(3),'k')
hold(handles.axes1,'off')

function clearData(handles)

ud=get(handles.figure1,'UserData');
if isempty(ud)
  ud=cell(1,9);
end
ud{7}{1}=NaN(7,3); ud{7}{2}=NaN(7,3);
ud{8}={NaN(1,7) NaN(1,7) NaN(1,7)};
if isempty(ud{9})
  ud{9}={NaN(1,10) NaN(1,7)};
end
set(handles.figure1,'UserData',ud);

clearTxt=[9 11 10 48 12 21 26 31 36 41 46 13 20 25 30 35 40 45 15 18 23 28 33 38 43 ...
  16 19 24 29 34 39 44 14 17 22 27 32 37 42];
for itxt=clearTxt
  set(handles.(sprintf('text%d',itxt)),'String','---')
end

cla(handles.axes1);


% --- Executes when user attempts to close figure1.
function figure1_CloseRequestFcn(hObject, eventdata, handles)
% hObject    handle to figure1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global FL
try
  FL.Gui=rmfield(FL.Gui,'ipTwiss');
catch
end
delete(hObject);


% --------------------------------------------------------------------
function File_Callback(hObject, eventdata, handles)
% hObject    handle to File (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --------------------------------------------------------------------
function FileSave_Callback(hObject, eventdata, handles)
% hObject    handle to FileSave (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
ud=get(handles.figure1,'UserData'); %#ok<NASGU>
txtfld=cell(1,52);
for itxt=1:52
  try
    txtfld{itxt}=get(handles.(sprintf('text%d',itxt)),'String');
  catch
    txtfld{itxt}='';
  end
end

[fn,pn]=uiputfile('*.mat','Save ipTwiss Data',sprintf('userData/uiTwiss_%s.mat',datestr(now,30)));
if length(fn)>1
  save(fullfile(pn,fn),'ud','txtfld');
end

% --------------------------------------------------------------------
function FileLoad_Callback(hObject, eventdata, handles)
% hObject    handle to FileLoad (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
[fn,pn]=uigetfile('userData/uiTwiss*.mat','Load ipTwiss Data');
if length(fn)>1
  ld=load(fullfile(pn,fn));
  ud=ld.ud;
  set(handles.figure1,'UserData',ud);
end

% Set display fields
for itxt=1:length(ld.txtfld)
  if ~isempty(ld.txtfld{itxt})
    set(handles.(sprintf('text%d',itxt)),'String',ld.txtfld{itxt});
  end
end

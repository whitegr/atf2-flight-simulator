function [ax,ay,betax,betay,ipsize_x,ipsize_y]=fdscan(qvals,latticeFile)
% Get "a" parameter for IP beta function measurement
ld=load(latticeFile,'BEAMLINE','PS','GIRDER','Model','Beam1');
% load latticeFiles/src/v5.1/ATF2lat_BX10BY1nl
global BEAMLINE PS GIRDER FL

initLattice={BEAMLINE PS GIRDER};
FL.noUpdate=true;
BEAMLINE=ld.BEAMLINE; GIRDER=ld.GIRDER; PS=ld.PS; %#ok<NASGU>
Model=ld.Model; Beam1=ld.Beam1;

clight=299792458; % speed of light (m/s)
Cb=1e9/clight; % T-m/GeV
brho=Cb*Model.Initial.Momentum;
betax=Model.Twiss.betax(Model.ip_ind);
betay=Model.Twiss.betay(Model.ip_ind);
gamma=Model.Initial.Momentum/0.511e-3;
emitx=Model.Initial.x.NEmit/gamma;
emity=Model.Initial.y.NEmit/gamma;

qd0=findcells(BEAMLINE,'Name','QD0FF'); qd0=BEAMLINE{qd0(1)}.PS;
qf1=findcells(BEAMLINE,'Name','QF1FF'); qf1=BEAMLINE{qf1(1)}.PS;

kvals_qd0=(qvals.*GetTrueStrength(PS(qd0).Element(1)).*2)./brho;
kvals_qf1=(qvals.*GetTrueStrength(PS(qf1).Element(1)).*2)./brho;

psinit=[PS(qd0).Ampl PS(qf1).Ampl];

[~,b1]=TrackThru(1,PS(qf1).Element(1)-1,Beam1,1,1,0);
ipsize=zeros(1,length(qvals));
for iq=1:length(qvals)
  PS(qd0).Ampl=qvals(iq);
  [~,bo]=TrackThru(PS(qf1).Element(1),Model.ip_ind,b1,1,1,0);
  ipsize(iq)=std(bo.Bunch.x(3,:));
end
PS(qd0).Ampl=psinit(1);
q=noplot_parab(kvals_qd0,ipsize.^2);
ay=sqrt((betay*q(1))/emity);
ipsize_y=ipsize;

ipsize=zeros(1,length(qvals));
for iq=1:length(qvals)
  PS(qf1).Ampl=qvals(iq);
  [~,bo]=TrackThru(PS(qf1).Element(1),Model.ip_ind,b1,1,1,0);
  ipsize(iq)=std(bo.Bunch.x(1,:));
end
q=noplot_parab(kvals_qf1,ipsize.^2);
ax=sqrt((betax*q(1))/emitx);
ipsize_x=ipsize;

BEAMLINE=initLattice{1}; GIRDER=initLattice{3}; PS=initLattice{2};
FL.noUpdate=false;
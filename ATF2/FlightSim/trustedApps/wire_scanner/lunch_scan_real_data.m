function [get_raw_data_scan,put_pos_write]=lunch_scan_real_data(ws_chosen,begin_scan_ws,end_scan_ws,step_scan_ws,ict_corr,hWsAxes,hWsText7,hWsWriteEdit,hStopButton,hRunButton,hWsMovingText,hWsIctAverageText)
global BEAMLINE INSTR

indict1x=findcells(BEAMLINE,'Name','ICT1X');
instict1x=findcells(INSTR,'Index',indict1x);


switch ws_chosen
   case 'WIP'
      posreadcmd='DB_IPBSM::MWIP:READ.SIGNAL';
      pos_write_var='DB_IPBSM::MWIP:POS.WRITE';
      pos_read_var='DB_IPBSM::MWIP:POS.READ';
      signal_var='DB_IPBSM::BG_MON:NAVE.SIGNAL';
      %ict_var='DB_IPBSM::MWIP:EVENT.ICT';
   case 'WPIP Tungsten'
      posreadcmd='DB_IPBSM::MW1IPT:READ.SIGNAL';
      pos_write_var='DB_IPBSM::MW1IPT:POS.WRITE';
      pos_read_var='DB_IPBSM::MW1IPT:POS.READ';
      signal_var='DB_IPBSM::BG_MON:NAVE.SIGNAL';
      %ict_var='DB_IPBSM::BG_MON:NAVE.SIGNAL';
    case 'WPIP Carbon'
      posreadcmd='DB_IPBSM::MW1IPC:READ.SIGNAL';
      pos_write_var='DB_IPBSM::MW1IPC:POS.WRITE';
      pos_read_var='DB_IPBSM::MW1IPC:POS.READ';
      signal_var='DB_IPBSM::MW1IPC:SIGNAL';
      %ict_var='DB_FF_CWS::MW1IPC:EVENT.ICT'; 
end



%POSITION WRITE AND READBACK POSITION FOR THE WIRE SCANNER

tol_write_read=0.05;
nb_step=floor((end_scan_ws-begin_scan_ws)/step_scan_ws);
for i=1:nb_step
    
    if (get(hStopButton, 'Value') == get(hStopButton,'Min'))
        
        %put_pos_write: variable of the control of the wire scanner position
        put_pos_write(i)=begin_scan_ws+step_scan_ws*(i-1);
        lcaPut('V2EPICS:r',put_pos_write(i));   %
        lcaPutNoWait('V2EPICS:rWrite',pos_write_var);pause(1);
        
        %read of the output of the readback position given by the Epics system 
        lcaPut('V2EPICS:bWrite',posreadcmd);
        lcaPut('V2EPICS:rRead',pos_read_var); 
        get_pos_read=lcaGet('V2EPICS:r');
        set(hWsWriteEdit,'String',num2str(put_pos_write(i)));
        set(hWsText7,'String',num2str(get_pos_read));
        
        %wait that the wire scanner goes to the position asked
        if i==1
            while abs((put_pos_write(i)-get_pos_read))>tol_write_read
                set(hWsMovingText,'String','The WS is moving','BackgroundColor','red');
                %read of the output of the readback position given by the Epics system 
                lcaPut('V2EPICS:bWrite',posreadcmd);
                lcaPut('V2EPICS:rRead',pos_read_var);  
                get_pos_read=lcaGet('V2EPICS:r');
                set(hWsText7,'String',num2str(get_pos_read));
            end
            pause(2);
            set(hWsMovingText,'String','The WS is not moving','BackgroundColor','white');
        else
            set(hWsMovingText,'String','The WS is moving','BackgroundColor','red');
            if step_scan_ws>0.05
                pause(2); 
            else
                pause(1); 
            end
            set(hWsMovingText,'String','The WS is not moving','BackgroundColor','white');
        end
        
        %read of the output signal of the WS detector
        lcaPut('V2EPICS:bWrite',posreadcmd);
        lcaPut('V2EPICS:rRead',signal_var);%pause(5)
        get_raw_data_scan(i)=lcaGet('V2EPICS:r');
        if strcmp(ict_corr,'on')
            FlHwUpdate;  ict=INSTR{instict1x}.Data;
            ict_factor(i)=ict(end);
            get_raw_data_scan(i)=get_raw_data_scan(i)*ict_factor(i);
            mean_ict_factor=mean(ict_factor(1:i));
            set(hWsIctAverageText,'String',num2str(mean_ict_factor));
        end
    
        %plot
        
        plot(hWsAxes,put_pos_write(1:i),get_raw_data_scan(1:i),'LineStyle','none','Marker','+');
        hold(hWsAxes,'on');
        set(hWsAxes,'XLim',[begin_scan_ws end_scan_ws]);
        
    elseif (get(hStopButton, 'Value') == get(hStopButton,'Max'))
        set(hStopButton,'BackgroundColor','green');
        set(hRunButton,'BackgroundColor','white');
        break;
        
    end
    
end
  


%  %GET THE SIGMA (BEAM SIZE) CALCULATED BY THE V-SYSTEM FROM THE WS SCAN
% 
% lcaPut('V2EPICS:rRead','DB_FF_CWS::MW1IPT:FIT.STD');
% sigma=lcaGet('V2EPICS:r');    
    
    
    
    

end

    
    
    
    

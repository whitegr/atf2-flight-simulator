function [sig_v_mean_m_disp_sub,sig_v_std_m_disp_sub]= eta_subtracted_error(sig_v_mean_m,sig_v_std_m,eta_spatial_mean_mm,eta_spatial_std_mm)
    


%subtracting the dispersion contribution to the beam size 

eta_spatial_mean_m=eta_spatial_mean_mm*10^-3;
eta_spatial_std_m=eta_spatial_std_mm*10^-3;
energy_spread=8e-4;

sig_v_mean_m_disp_sub=sqrt(sig_v_mean_m.^2-(eta_spatial_mean_m*energy_spread).^2);
sig_v_std_m_disp_sub= sqrt(((sig_v_mean_m./sqrt(sig_v_mean_m.^2-energy_spread^2*eta_spatial_mean_m.^2)).^2).*sig_v_std_m.^2 + ((energy_spread^2*eta_spatial_mean_m./sqrt(sig_v_mean_m.^2-energy_spread^2*eta_spatial_mean_m.^2)).^2).*eta_spatial_std_m.^2);



end
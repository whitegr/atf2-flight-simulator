function [y_m] = sigma_corr_wire_diam_offline(y_m,wire_diameter_um,ws_chosen)



%subtracting the wire scanner diameter to the beam size 

y_m_square=y_m.^2;
wire_diameter_m=wire_diameter_um*10^-6;

if strcmp(ws_chosen,'WIP') || strcmp(ws_chosen,'WPIP Tungsten')
    if y_m>2.5e-6
        y_m=sqrt(y_m_square-(wire_diameter_m/4)^2);
    end
elseif strcmp(ws_chosen,'WPIP Carbon')
    if y_m>1.75e-6
        y_m=sqrt(y_m_square-(wire_diameter_m/4)^2);
    end
end




end
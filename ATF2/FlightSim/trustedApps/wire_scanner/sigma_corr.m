function [y_m] = sigma_corr(y_m,wire_diameter_um,ws_chosen,sig_x_or_y)



%subtracting the wire scanner diameter to the beam size 

y_m_square=y_m.^2;
wire_diameter_m=wire_diameter_um*10^-6;

if strcmp(ws_chosen,'WIP') || strcmp(ws_chosen,'WPIP Tungsten')
    if y_m>2.5e-6
        y_m=sqrt(y_m_square-(wire_diameter_m/4)^2);
    end
elseif strcmp(ws_chosen,'WPIP Carbon')
    if y_m>1.75e-6
        y_m=sqrt(y_m_square-(wire_diameter_m/4)^2);
    end
end




%subtract the effect of the wire scanner angle

switch ws_chosen
   case 'WIP'
      if strcmp(sig_x_or_y,'X')
          y_m=y_m*sind(37.5);
      elseif strcmp(sig_x_or_y,'Y')
          y_m=y_m*cosd(37.5);
      end
   case 'WPIP Tungsten'
      if strcmp(sig_x_or_y,'X')
          y_m=y_m*sind(45);
      elseif strcmp(sig_x_or_y,'Y')
          y_m=y_m*cosd(45);
      end
end







end
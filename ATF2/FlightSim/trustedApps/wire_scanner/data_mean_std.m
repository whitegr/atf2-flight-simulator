function [y_m_mean,y_m_std,x_A_unique] = data_mean_std(x_A,y_m)


%construction of the input data to calculate the mean and the standard deviation
%if the same values of FD strenght are put, it groups automatically data of
%beam size in order to calculate the mean and the standard deviation


[x_A,ind]=sort(x_A);
y_m=y_m(ind);
[x_A_unique,m,n]=unique(x_A,'first');

if size(x_A,2)>1
  for i=1:size(m,2)
    if i==size(m,2)
       j(i)=size(x_A,2)-m(i);
       y_m_mean(i)=mean(y_m(m(i):(m(i)+(j(i)))));
       y_m_std(i)=std(y_m(m(i):(m(i)+(j(i)))),1);
       if y_m_std(i)==0
           y_m_std(i)=0.1*10^-6;
       end
    else
       j(i)=m(i+1)-m(i);
       y_m_mean(i)=mean(y_m(m(i):(m(i)+(j(i)-1))));
       y_m_std(i)=std(y_m(m(i):(m(i)+(j(i)-1))),1);
       if y_m_std(i)==0
          y_m_std(i)=0.1*10^-6;
       end
    end
  end
else
    y_m_mean=y_m;
    y_m_std=0;
end


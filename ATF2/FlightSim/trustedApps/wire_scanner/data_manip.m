function [y_m_mean,y_m_std,x_A_unique] = data_manip(x_A,y_m,wire_diameter_um,eta_spatial_mm,eta_angular_mm)



% %a parameter
% 
% if strcmp(ws_chosen,'WIP')    %at the IP for the optics beta x=0.04 and beta y=0.001
%     if strcmp(sig_x_or_y,'X') %for X beam size
%       a_par=1.8517915549374;  
%     elseif strcmp(sig_x_or_y,'Y')  %for Y beam size
%       a_par=1.2364609683258; 
%     end
% elseif strcmp(ws_chosen,'WPIP')  %at the PIP for the optics beta x=0.04 and beta y=0.001
%     if strcmp(sig_x_or_y,'X')    %for X beam size
%       a_par=3.1449945094737;   
%     elseif strcmp(sig_x_or_y,'Y') %for Y beam size
%       a_par=2.3453424065088; 
%     end
% end



%subtracting the wire scanner diameter and the dispersion to the beam size 


y_m_square=y_m.^2;
eta_spatial_m=eta_spatial_mm*10^-3;
eta_angular_m=eta_angular_mm*10^-3;
wire_diameter_m=wire_diameter_um*10^-6;
energy_spread=8e-4;

%dQ=abs([x_kl-min_kl_qd0]);



if y_m>7*10^-6
    %y_m=sqrt(y_m_square-(wire_diameter_m/4)^2-(eta_spatial_m*energy_spread)^2-(energy_spread*eta_angular_m*a_par)^2*dQ.^2);
    y_m=sqrt(y_m_square-(wire_diameter_m/4)^2-(eta_spatial_m*energy_spread)^2);

end




%construction of the input data to calculate the mean and the standard deviation
%if the same values of FD strenght are put, it groups automatically data of
%beam size in order to calculate the mean and the standard deviation


[x_A,ind]=sort(x_A);
y_m=y_m(ind);
[x_A_unique,m,n]=unique(x_A,'first');

if size(x_A,2)>1
  for i=1:size(m,2)
    if i==size(m,2)
       j(i)=size(x_A,2)-m(i);
       y_m_mean(i)=mean(y_m(m(i):(m(i)+(j(i)))));
       y_m_std(i)=std(y_m(m(i):(m(i)+(j(i)))));
    else
       j(i)=m(i+1)-m(i);
       y_m_mean(i)=mean(y_m(m(i):(m(i)+(j(i)-1))));
       y_m_std(i)=std(y_m(m(i):(m(i)+(j(i)-1))));
    end
  end
else
    y_m_mean=y_m;
    y_m_std=0;
end


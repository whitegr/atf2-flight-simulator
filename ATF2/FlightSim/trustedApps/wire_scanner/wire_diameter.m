function wire_diam_um=wire_diameter(ws_chosen)

if strcmp(ws_chosen,'WIP') || strcmp(ws_chosen,'WPIP Tungsten')
    wire_diam_um=10;
elseif strcmp(ws_chosen,'WPIP Carbon')
    wire_diam_um=7;
end

end
function ws_write_position_real(ws_write_pos,hWsText7,hWsMovingText)

tol_write_read=0.05;

%ws_write_pos: variable of the control of the wire scanner position
lcaPutNoWait('V2EPICS:r',ws_write_pos);   
lcaPutNoWait('V2EPICS:rWrite','DB_FF_CWS::MW1IPT:POS.WRITE');
        
%read of the output of the readback position given by the Epics system 
lcaPutNoWait('V2EPICS:rRead','DB_FF_CWS::MW1IPT:POS.READ');
get_pos_read=lcaGet('V2EPICS:r');
set(hWsText7,'String',num2str(get_pos_read));
        
%wait that the wire scanner goes to the position asked       
while abs((ws_write_pos-get_pos_read))>tol_write_read
       set(hWsMovingText,'String','The WS is moving','BackgroundColor','red');
       %read of the output of the readback position given by the Epics system 
       lcaPutNoWait('V2EPICS:rRead','DB_FF_CWS::MW1IPT:POS.READ');
       get_pos_read=lcaGet('V2EPICS:r');
       set(hWsText7,'String',num2str(get_pos_read));
end
pause(2);
set(hWsMovingText,'String','The WS is not moving','BackgroundColor','white');

        



end
function table_online(strenght_fd_v,sigma_v,hTwissAxes,emittance_extr_m,sigma_emittance_extr_m,ws_chosen,sig_x_or_y,analysis_sha,analysis_fs,hTwissTable)





% Create a figure that will have a uitable, axes and checkboxes

 hTableFigure = figure(...       % The main GUI figure
                    'MenuBar','none', ...
                    'Toolbar','none', ...
                    'HandleVisibility','callback', ...
                    'Color', get(0,...
                             'defaultuicontrolbackgroundcolor'));  

 
 %Save button 1                        
                         
hSave1Button = uicontrol(... % Button for updating selected plot
                   'Parent', hTableFigure, ...
                   'Units','normalized',...
                   'HandleVisibility','callback', ...
                   'Position',[0.25 0.025 0.18 0.07],...
                   'FontSize',14,...
                   'String','Save data',...
                   'Style','pushbutton',...
                   'Callback', @hSave1ButtonCallback);     

               
%Instruction 1               
               
hInstr1Text = uicontrol(... % Button for updating selected plot
               'Parent', hTableFigure, ...
               'Units','normalized',...
               'HandleVisibility','callback', ...
               'Position',[0.01 0.9 0.64 0.05],...
               'FontSize',12,...
               'String','1. Select data for twiss parameter calculations/plot',...
               'Style','text');
               


%Next button
hNextButton = uicontrol(... % Button for updating selected plot
                   'Parent', hTableFigure, ...
                   'Units','normalized',...
                   'HandleVisibility','callback', ...
                   'Position',[0.5 0.025 0.18 0.07],...
                   'FontSize',14,...
                   'String','Next',...
                   'Style','pushbutton',...
                   'Callback', @hNextButtonCallback);     
               

%Save button 2                        
                         
hSave2Button = uicontrol(... % Button for updating selected plot
                   'Parent', hTableFigure, ...
                   'Units','normalized',...
                   'HandleVisibility','callback', ...
                   'Position',[0.25 0.025 0.18 0.07],...
                   'FontSize',14,...
                   'String','Save data',...
                   'Style','pushbutton',...
                   'Enable','off',...
                   'Visible','off',...
                   'Callback', @hSave2ButtonCallback);                      

               
%Instruction 2               
               
hInstr2Text = uicontrol(... % Button for updating selected plot
               'Parent', hTableFigure, ...
               'Units','normalized',...
               'HandleVisibility','callback', ...
               'Position',[0.01 0.9 0.40 0.05],...
               'FontSize',12,...
               'String','2. Enter dispersion (mean/std)',...
               'Enable','off',...
               'Visible','off',...
               'Style','text');               
               
%Next 2 button

hNext2Button = uicontrol(... % Button for updating selected plot
                   'Parent', hTableFigure, ...
                   'Units','normalized',...
                   'HandleVisibility','callback', ...
                   'Position',[0.5 0.025 0.18 0.07],...
                   'FontSize',14,...
                   'String','Next',...
                   'Style','pushbutton',...
                   'Enable','off',...
                   'Visible','off',...
                   'Callback', @hNext2ButtonCallback);    
           
               
%Radio buttons for the angular nominal dispersion in X

hText1 = uicontrol(... 
                   'Parent', hTableFigure, ...
                   'Units','normalized',...
                   'HandleVisibility','callback', ...
                   'Position',[0.01 0.85 0.58 0.05],...
                   'FontSize',12,...
                   'Enable','off',...
                   'Visible','off',...
                   'String','Or use the nominal angular dispersion (for X)?',...
                   'Style','text');
               
 hRadioButton1 = uicontrol(... 
                   'Parent', hTableFigure, ...
                   'Units','normalized',...
                   'HandleVisibility','callback', ...
                   'Position',[0.61 0.85 0.08 0.05],...
                   'FontSize',12,...
                   'Enable','off',...
                   'Visible','off',...
                   'String','yes',...
                   'Style','radiobutton',...
                   'Value',0,...
                   'Callback', @hRadioButton1Callback);              
               
 hRadioButton2 = uicontrol(... 
                   'Parent', hTableFigure, ...
                   'Units','normalized',...
                   'HandleVisibility','callback', ...
                   'Position',[0.73 0.85 0.08 0.05],...
                   'FontSize',12,...
                   'Enable','off',...
                   'Visible','off',...
                   'String','no',...
                   'Style','radiobutton',...
                   'Value',1,...
                   'Callback', @hRadioButton2Callback);                           
               
               
%Save button 3                                           
hSave3Button = uicontrol(... % Button for updating selected plot
                   'Parent', hTableFigure, ...
                   'Units','normalized',...
                   'HandleVisibility','callback', ...
                   'Position',[0.05 0.025 0.18 0.07],...
                   'FontSize',14,...
                   'String','Save data',...
                   'Style','pushbutton',...
                   'Enable','off',...
                   'Visible','off',...
                   'Callback', @hSave3ButtonCallback);                  
           

%Button create graph                                              
hCreateGraphButton = uicontrol(... % Button for updating selected plot
                   'Parent', hTableFigure, ...
                   'Units','normalized',...
                   'HandleVisibility','callback', ...
                   'Position',[0.25 0.025 0.25 0.07],...
                   'FontSize',14,...
                   'String','Create graph y',...
                   'Style','pushbutton',...
                   'Enable','off',...
                   'Visible','off',...
                   'Callback', @hCreateGraphButtonCallback);                        
         

 %Button create graph 2                                             
hCreateGraph2Button = uicontrol(... % Button for updating selected plot
                   'Parent', hTableFigure, ...
                   'Units','normalized',...
                   'HandleVisibility','callback', ...
                   'Position',[0.52 0.025 0.27 0.07],...
                   'FontSize',14,...
                   'String','Create graph y^2',...
                   'Style','pushbutton',...
                   'Enable','off',...
                   'Visible','off',...
                   'Callback', @hCreateGraph2ButtonCallback);                              
               
               
               
               
%Instruction 3                           
hInstr3Text = uicontrol(... % Button for updating selected plot
               'Parent', hTableFigure, ...
               'Units','normalized',...
               'HandleVisibility','callback', ...
               'Position',[0.01 0.9 0.5 0.05],...
               'FontSize',12,...
               'String','3. Data summary and graph generation',...
               'Enable','off',...
               'Visible','off',...
               'Style','text');           
               
               
%Finish button
hFinishButton = uicontrol(... % Button for updating selected plot
                   'Parent', hTableFigure, ...
                   'Units','normalized',...
                   'HandleVisibility','callback', ...
                   'Position',[0.8 0.025 0.18 0.07],...
                   'FontSize',16,...
                   'String','Finish',...
                   'Style','pushbutton',...
                   'Enable','off',...
                   'Visible','off',...
                   'Callback', @hFinishButtonCallback);       
               


 
%CREATING DATA

eta_spatial_mean=[];
eta_spatial_std=[];
sigma_v_mean=[];
sigma_v_std=[];
sigma_v_mean_disp_sub=[];
sigma_v_std_disp_sub=[];
strenght_fd_v_unique=[];
data_disp=[];
data_sigma_mean_std=[];
x_A_fit=[];
y_m_fit=[];
y_m_fit_disp_sub=[];
x_kl_fit=[];
strenght_fd_v_kl_unique=[];
sigma_v_square_mean=[];
sigma_v_square_std=[];
y_m_square_fit=[];
sigma_v_square_mean_disp_sub=[];
sigma_v_square_std_disp_sub=[];
y_m_square_fit_disp_sub=[];           
               
               
%TABLE 1

% data

data=[strenght_fd_v' sigma_v'];

% Define parameters for a uitable (col headers are fictional)
colnames = {'FD strenght [A]', 'Sigma [m]'};
% All column contain numeric data (integers, actually)
colfmt = {'numeric', 'numeric'};
% Disallow editing values (but this can be changed)
coledit = [false false];
% Set columns all the same width (must be in pixels)
colwdt = {90 90};
% Create a uitable on the left side of the figure
htable = uitable(...,
                 'Parent', hTableFigure, ...
                 'Units', 'normalized',...
                 'Position', [0.025 0.15 0.97 0.65],...
                 'Data',  data,... 
                 'ColumnName', colnames,...
                 'ColumnFormat', colfmt,...
                 'ColumnWidth', colwdt,...
                 'ColumnEditable', coledit,...
                 'ToolTipString',...
                 'Select cells to highlight them on the plot',...
                 'CellSelectionCallback',{@select_callback});

             
             
 % Create an invisible marker plot of the data and save handles
% to the lineseries objects; use this to simulate data brushing.

 h_Parab=plot(hTwissAxes,strenght_fd_v,sigma_v,'b','LineStyle','none','Marker','+','HandleVisibility', 'off','Visible', 'off');               
 h_Parab_fit=plot(hTwissAxes,x_A_fit,y_m_fit,'b','HandleVisibility', 'off','Visible','off');      

 
 %TABLE 2
 
% Define parameters for a uitable (col headers are fictional)
    colnames_disp = {'FD strenght [A]', 'Eta mean [mm]','Eta std [mm]'};
    % All column contain numeric data (integers, actually)
    colfmt_disp = {'numeric', 'numeric', 'numeric'};
    % Disallow editing values (but this can be changed)
    coledit_disp = [false true true];
    % Set columns all the same width (must be in pixels)
    colwdt_disp = {90 90 90};
    % Create a uitable on the left side of the figure
    htable_disp = uitable(...,
                 'Parent', hTableFigure, ...
                 'Units', 'normalized',...
                 'Position', [0.025 0.15 0.97 0.65],...
                 'ColumnName', colnames_disp,...
                 'ColumnFormat', colfmt_disp,...
                 'ColumnWidth', colwdt_disp,...
                 'ColumnEditable', coledit_disp,...
                 'ToolTipString',...
                 'Select cells to highlight them on the plot');       
    set(htable_disp,'Enable','off','Visible','off');         
             
 

%TABLE 3
 
% Define parameters for a uitable (col headers are fictional)
    colnames_sigma_mean_std = {'FD strenght [A]', 'Sigma mean [m]','Sigma std [m]','Sigma mean (disp sub) [m]','Sigma std (disp sub) [m]'};
    % All column contain numeric data (integers, actually)
    colfmt_sigma_mean_std = {'numeric', 'numeric', 'numeric','numeric', 'numeric'};
    % Disallow editing values (but this can be changed)
    coledit_sigma_mean_std = [false false false false false];
    % Set columns all the same width (must be in pixels)
    colwdt_sigma_mean_std = {90 90 90 110 110};
    % Create a uitable on the left side of the figure
    htable_sigma_mean_std = uitable(...,
                            'Parent', hTableFigure, ...
                            'Units', 'normalized',...
                            'Position', [0.025 0.15 0.97 0.65],...
                            'ColumnName', colnames_sigma_mean_std,...
                            'ColumnFormat', colfmt_sigma_mean_std,...
                            'ColumnWidth', colwdt_sigma_mean_std,...
                            'ColumnEditable', coledit_sigma_mean_std,...
                            'ToolTipString',...
                            'Select cells to highlight them on the plot');       
    set(htable_sigma_mean_std,'Enable','off','Visible','off');         
    
    
    
 
%code for the CellSelectionCallback, which shows and hides markers on the axes             
function select_callback(hObject, eventdata)
    % hObject    Handle to uitable1 (see GCBO)
    % eventdata  Currently selected table indices
    % Callback to erase and replot markers, showing only those
    % corresponding to user-selected cells in table. 
    % Repeatedly called while user drags across cells of the uitable

        % h_Parab are handles to lines having markers only
        set(h_Parab, 'Visible', 'off') % turn them off to begin     

        % Get the list of currently selected table cells
        sel = eventdata.Indices;     % Get selection indices (row, col)
                                     % Noncontiguous selections are ok
        
        sel=  unique(sel(:,1));
        table = get(hObject,'Data'); % Get copy of uitable data
        
        % Get vectors of x,y values for each column in the selection;
       
        strenght_fd_v = table(sel, 1);
        sigma_v= table(sel, 2); 
        set(h_Parab, 'XData',strenght_fd_v, 'YData',sigma_v,'Visible', 'on');
        hold(hTwissAxes,'on');
        [sigma_v_mean,sigma_v_std,strenght_fd_v_unique] = data_mean_std(strenght_fd_v',sigma_v');
        if size(sigma_v_mean,2)>3
            delete(h_Parab_fit);
            [q_parab,dq_parab,chisq_parab,Cv_parab]=noplot_parab_2(strenght_fd_v',sigma_v',1);
             min_A_fit=min(strenght_fd_v);
             max_A_fit=max(strenght_fd_v);
            x_A_fit=min_A_fit:1e-3:max_A_fit;
            y_m_fit=q_parab(1)*(x_A_fit-q_parab(2)).^2+q_parab(3);
            h_Parab_fit=plot(hTwissAxes,x_A_fit,y_m_fit,'b','Visible','on');
            if strcmp(analysis_sha,'yes')
               [strenght_fd_v_kl_unique,sigma_v_square_mean,sigma_v_square_std,x_kl_fit,y_m_square_fit,chisq_square_parab,beta_ip_first_meth,sigma_beta_ip_first_meth,beta_ip_second_meth,sigma_beta_ip_second_meth,emittance_ip,sigma_emittance_ip,alpha_ip_first_meth,sigma_alpha_ip_first_meth,alpha_ip_second_meth,sigma_alpha_ip_second_meth]=twiss_analysis_sha(sigma_v_mean,sigma_v_std,strenght_fd_v_unique,emittance_extr_m,sigma_emittance_extr_m,ws_chosen,sig_x_or_y);
                data_no_disp_sub=[beta_ip_first_meth;sigma_beta_ip_first_meth;beta_ip_second_meth;sigma_beta_ip_second_meth;emittance_ip;sigma_emittance_ip;alpha_ip_first_meth;sigma_alpha_ip_first_meth;alpha_ip_second_meth;sigma_alpha_ip_second_meth;chisq_parab];
                data_twiss=[data_no_disp_sub];
                set(hTwissTable,'Data',data_twiss);
            end
            if strcmp(analysis_fs,'yes')
                [beta_fs,alpha_fs,emitt_fs]=twiss_analysis_fs(sigma_v,strenght_fd_v);
            end
        else
            delete(h_Parab_fit);  
            h_Parab_fit=plot(hTwissAxes,x_A_fit,y_m_fit,'b','HandleVisibility', 'off','Visible','off');      
        end
        hold(hTwissAxes,'off');

        
        
end             
             
         

function hNextButtonCallback(hObject, eventdata)             
    
set(hNextButton,'Enable','off','Visible','off');
set(htable,'Enable','off','Visible','off'); 
set(hSave1Button,'Enable','off','Visible','off'); 
set(hInstr1Text,'Enable','off','Visible','off'); 
set(hNext2Button,'Enable','on','Visible','on');
set(htable_disp,'Enable','on','Visible','on');
set(hSave2Button,'Enable','on','Visible','on'); 
set(hInstr2Text,'Enable','on','Visible','on');
set(hText1,'Visible','on');
set(hRadioButton1,'Visible','on');
set(hRadioButton2,'Visible','on');

if strcmp(sig_x_or_y,'X')
    set(hText1,'Enable','on');
    set(hRadioButton1,'Enable','on');
    set(hRadioButton2,'Enable','on');
end

[sigma_v_mean,sigma_v_std,strenght_fd_v_unique] = data_mean_std(strenght_fd_v',sigma_v');
 for i=1:size(strenght_fd_v_unique,2)
            eta_spatial_mean(i,1)=0; 
            eta_spatial_std(i,1)=0; 
 end
data_disp=[strenght_fd_v_unique' eta_spatial_mean eta_spatial_std];
set(htable_disp,'Data',data_disp); 

 
end


function hNext2ButtonCallback(hObject, eventdata)             
    
set(hNext2Button,'Enable','off','Visible','off');
set(htable_disp,'Enable','off','Visible','off'); 
set(hSave2Button,'Enable','off','Visible','off'); 
set(hInstr2Text,'Enable','off','Visible','off');
set(hText1,'Visible','off');
set(hRadioButton1,'Visible','off');
set(hRadioButton2,'Visible','off');
set(hFinishButton,'Enable','on','Visible','on');
set(htable_sigma_mean_std,'Enable','on','Visible','on');
set(hSave3Button,'Enable','on','Visible','on'); 
set(hCreateGraphButton,'Enable','on','Visible','on'); 
set(hCreateGraph2Button,'Enable','on','Visible','on'); 
set(hInstr3Text,'Enable','on','Visible','on'); 


  %data
  
  data_disp = get(htable_disp,'Data');
  eta_spatial_mean=data_disp(:,2);     
  eta_spatial_std=data_disp(:,3);
  [sigma_v_mean_disp_sub,sigma_v_std_disp_sub]= eta_subtracted_error(sigma_v_mean,sigma_v_std,eta_spatial_mean',eta_spatial_std');
  data_sigma_mean_std=[strenght_fd_v_unique' sigma_v_mean' sigma_v_std' sigma_v_mean_disp_sub' sigma_v_std_disp_sub'];
  set(htable_sigma_mean_std,'Data',data_sigma_mean_std);                     
  
  
  %calculation of twiss parameters and twiss
  
  errorbar(hTwissAxes,strenght_fd_v_unique,sigma_v_mean,sigma_v_std,'b+','LineStyle','none');
  hold(hTwissAxes,'on');
  errorbar(hTwissAxes,strenght_fd_v_unique,sigma_v_mean_disp_sub,sigma_v_std_disp_sub,'r+','LineStyle','none');  
  legend(hTwissAxes,'Eta not subtracted','Eta subtracted');
    
  if size(sigma_v_mean,2)>3
         %[A,B,C,rms,chisq] = parabola_fit(strenght_fd_v_unique',sigma_v_mean',sigma_v_std'); %function of the flight simulator
         %[A_disp_sub,B_disp_sub,C_disp_sub,rms,chisq_disp_sub] = parabola_fit(strenght_fd_v_unique',sigma_v_mean_disp_sub',sigma_v_std_disp_sub); %function of the flight simulator
        [q_parab,dq_parab,chisq_parab,Cv_parab]=noplot_parab_2(strenght_fd_v_unique',sigma_v_mean',sigma_v_std');
        [q_parab_disp_sub,dq_parab_disp_sub,chisq_parab_disp_sub,Cv_parab_disp_sub]=noplot_parab_2(strenght_fd_v_unique',sigma_v_mean_disp_sub',sigma_v_std_disp_sub);
        min_A_fit=min(strenght_fd_v_unique);
         max_A_fit=max(strenght_fd_v_unique);
         x_A_fit=min_A_fit:1e-3:max_A_fit;
         y_m_fit=q_parab(1)*(x_A_fit-q_parab(2)).^2+q_parab(3);
         y_m_fit_disp_sub=q_parab_disp_sub(1)*(x_A_fit-q_parab_disp_sub(2)).^2+q_parab_disp_sub(3);
         plot(hTwissAxes,x_A_fit,y_m_fit,'b',x_A_fit,y_m_fit_disp_sub,'r');
         if strcmp(analysis_sha,'yes')
               [strenght_fd_v_kl_unique,sigma_v_square_mean,sigma_v_square_std,x_kl_fit,y_m_square_fit,chisq_square_parab,beta_ip_first_meth,sigma_beta_ip_first_meth,beta_ip_second_meth,sigma_beta_ip_second_meth,emittance_ip,sigma_emittance_ip,alpha_ip_first_meth,sigma_alpha_ip_first_meth,alpha_ip_second_meth,sigma_alpha_ip_second_meth]=twiss_analysis_sha(sigma_v_mean,sigma_v_std,strenght_fd_v_unique,emittance_extr_m,sigma_emittance_extr_m,ws_chosen,sig_x_or_y);
                [strenght_fd_v_kl_unique,sigma_v_square_mean_disp_sub,sigma_v_square_std_disp_sub,x_kl_fit,y_m_square_fit_disp_sub,chisq_square_parab_disp_sub,beta_ip_first_meth_disp_sub,sigma_beta_ip_first_meth_disp_sub,beta_ip_second_meth_disp_sub,sigma_beta_ip_second_meth_disp_sub,emittance_ip_disp_sub,sigma_emittance_ip_disp_sub,alpha_ip_first_meth_disp_sub,sigma_alpha_ip_first_meth_disp_sub,alpha_ip_second_meth_disp_sub,sigma_alpha_ip_second_meth_disp_sub]=twiss_analysis_sha(sigma_v_mean_disp_sub,sigma_v_std_disp_sub,strenght_fd_v_unique,emittance_extr_m,sigma_emittance_extr_m,ws_chosen,sig_x_or_y);
                data_no_disp_sub=[beta_ip_first_meth;sigma_beta_ip_first_meth;beta_ip_second_meth;sigma_beta_ip_second_meth;emittance_ip;sigma_emittance_ip;alpha_ip_first_meth;sigma_alpha_ip_first_meth;alpha_ip_second_meth;sigma_alpha_ip_second_meth;chisq_parab];
                data_disp_sub=[beta_ip_first_meth_disp_sub;sigma_beta_ip_first_meth_disp_sub;beta_ip_second_meth_disp_sub;sigma_beta_ip_second_meth_disp_sub;emittance_ip_disp_sub;sigma_emittance_ip_disp_sub;alpha_ip_first_meth_disp_sub;sigma_alpha_ip_first_meth_disp_sub;alpha_ip_second_meth_disp_sub;sigma_alpha_ip_second_meth_disp_sub;chisq_parab_disp_sub];
                data_twiss=[data_no_disp_sub data_disp_sub];
                set(hTwissTable,'Data',data_twiss);
         end
         if strcmp(analysis_fs,'yes')
                [beta_fs,alpha_fs,emitt_fs]=twiss_analysis_fs(sigma_v,strenght_fd_v);
         end
  end
  hold(hTwissAxes,'off');
  
  
end






function hFinishButtonCallback(hObject, eventdata) 
       
    close(hTableFigure);
    
end            
    

function hRadioButton1Callback(hObject, eventdata) 

if (get(hObject,'Value') == get(hObject,'Max'))
	  set(hRadioButton2,'Value',0);
      [eta_spatial_mean,eta_spatial_std]=yes_nominal_eta(ws_chosen,sig_x_or_y,sigma_v_mean,strenght_fd_v_unique); 
      set(htable_disp,'ColumnEditable',[false false false]);
else
      set(hRadioButton2,'Value',1);
      [eta_spatial_mean,eta_spatial_std]=no_nominal_eta(strenght_fd_v_unique); 
      set(htable_disp,'ColumnEditable',[false true true]);
end
data_disp=[strenght_fd_v_unique' eta_spatial_mean eta_spatial_std];
set(htable_disp,'Data',data_disp);  
      
end


function hRadioButton2Callback(hObject, eventdata) 

if (get(hObject,'Value') == get(hObject,'Max'))
	set(hRadioButton1,'Value',0);
    [eta_spatial_mean,eta_spatial_std]=no_nominal_eta(strenght_fd_v_unique);
    set(htable_disp,'ColumnEditable',[false true true]);
else
    set(hRadioButton1,'Value',1);
    [eta_spatial_mean,eta_spatial_std]=yes_nominal_eta(ws_chosen,sig_x_or_y,sigma_v_mean,strenght_fd_v_unique); 
    set(htable_disp,'ColumnEditable',[false false false]);
end
data_disp=[strenght_fd_v_unique' eta_spatial_mean eta_spatial_std];
set(htable_disp,'Data',data_disp);  

end

function [eta_spatial_mean,eta_spatial_std]=yes_nominal_eta(ws_chosen,sig_x_or_y,sigma_v_mean,strenght_fd_v_unique)
   
  angular_dispersion_ip=0.139;
  if strcmp(ws_chosen,'WIP')    %at the IP for the optics beta x=0.04 and beta y=0.001
          if strcmp(sig_x_or_y,'X') %for X beam size
              a_par=1.8517915549374;  
          elseif strcmp(sig_x_or_y,'Y')  %for Y beam size
              a_par=1.2364609683258; 
          end
          kl_qd0_design=1.36396800693;   %for alpha estimation
  elseif strcmp(ws_chosen,'WPIP Tungsten') || strcmp(ws_chosen,'WPIP Carbon')  %at the PIP for the optics beta x=0.04 and beta y=0.001
         if strcmp(sig_x_or_y,'X')    %for X beam size
             a_par=3.1449945094737;   
         elseif strcmp(sig_x_or_y,'Y') %for Y beam size
             a_par=2.3453424065088; 
         end
         kl_qd0_design=1.112672023613;   %for alpha estimation
  end  
     %[A,B,C,rms,chisq] = parabola_fit(strenght_fd_v_unique',sigma_v_mean',sigma_v_std'); %function of the flight simulator
      [q_parab,dq_parab,chisq_parab,Cv_parab]=noplot_parab_2(strenght_fd_v_unique',sigma_v_mean',sigma_v_std');
      dQ=abs((strenght_fd_v_unique'-q_parab(2)))*0.0104462;
      eta_spatial_mean=sqrt((angular_dispersion_ip*a_par)^2*dQ.^2)*10^3;
      for i=1:size(strenght_fd_v_unique,2)
            eta_spatial_std(i,1)=0;     
      end

end
        

function [eta_spatial_mean,eta_spatial_std]=no_nominal_eta(strenght_fd_v_unique)
   
    for i=1:size(strenght_fd_v_unique,2)
            eta_spatial_mean(i,1)=0; 
            eta_spatial_std(i,1)=0; 
    end
    
end


function hSave1ButtonCallback(hObject, eventdata) 

strenght_sigma=[strenght_fd_v sigma_v];
file_name_1 = inputdlg('Enter file name');
folder_name_1 = uigetdir;
directory_file_name_1=char(strcat(folder_name_1,'/',file_name_1,'.txt'));
%dlmwrite(directory_file_name_1,strenght_sigma,'delimiter','\t','newline','pc');  
dlmwrite(directory_file_name_1,strenght_sigma,'delimiter','\t','newline','unix');  
        
end



function hSave2ButtonCallback(hObject, eventdata) 

data_disp = get(htable_disp,'Data');
file_name_2 = inputdlg('Enter file name');
folder_name_2 = uigetdir;
directory_file_name_2=char(strcat(folder_name_2,'/',file_name_2,'.txt'));
%dlmwrite(directory_file_name_2,data_disp,'delimiter','\t','newline','pc');  
dlmwrite(directory_file_name_2,data_disp,'delimiter','\t','newline','unix');  
        
end



function hSave3ButtonCallback(hObject, eventdata) 

file_name_3 = inputdlg('Enter file name');
folder_name_3 = uigetdir;
directory_file_name_3=char(strcat(folder_name_3,'/',file_name_3,'.txt'));
%dlmwrite(directory_file_name_3,data_sigma_mean_std,'delimiter','\t','newline','pc');  
dlmwrite(directory_file_name_3,data_sigma_mean_std,'delimiter','\t','newline','unix');  
        
end



function hCreateGraphButtonCallback(hObject, eventdata) 

figure;
errorbar(strenght_fd_v_unique,sigma_v_mean,sigma_v_std,'b+','LineStyle','none');
hold on;
errorbar(strenght_fd_v_unique,sigma_v_mean_disp_sub,sigma_v_std_disp_sub,'r+','LineStyle','none');  
legend('Eta not subtracted','Eta subtracted');
plot(x_A_fit,y_m_fit,'b',x_A_fit,y_m_fit_disp_sub,'r');
title('Beam size versus QD0FF strenght');
xlabel('KL QD0FF [m^-1]'); ylabel('{\sigma}y [m]');
grid on;

end



function hCreateGraph2ButtonCallback(hObject, eventdata) 
        
figure;
errorbar(strenght_fd_v_kl_unique,sigma_v_square_mean,sigma_v_square_std,'b+','LineStyle','none');
hold on;
errorbar(strenght_fd_v_kl_unique,sigma_v_square_mean_disp_sub,sigma_v_square_std_disp_sub,'r+','LineStyle','none');  
legend('Eta not subtracted','Eta subtracted');
plot(x_kl_fit,y_m_square_fit,'b',x_kl_fit,y_m_square_fit_disp_sub,'r');
title('Square of the beam size versus QD0FF strenght');
xlabel('KL QD0FF [m^-1]'); ylabel('{\sigma}y^2 [m^2]');
grid on;

end



             
end
function varargout = bpmcal(varargin)
% BPMCAL M-file for bpmcal.fig
%      BPMCAL, by itself, creates a new BPMCAL or raises the existing
%      singleton*.
%
%      H = BPMCAL returns the handle to a new BPMCAL or the handle to
%      the existing singleton*.
%
%      BPMCAL('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in BPMCAL.M with the given input arguments.
%
%      BPMCAL('Property','Value',...) creates a new BPMCAL or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before bpmcal_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to bpmcal_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help bpmcal

% Last Modified by GUIDE v2.5 14-Nov-2010 11:57:10

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @bpmcal_OpeningFcn, ...
                   'gui_OutputFcn',  @bpmcal_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before bpmcal is made visible.
function bpmcal_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to bpmcal (see VARARGIN)
global FL

% Choose default command line output for bpmcal
handles.output = handles;

% Update handles structure
guidata(hObject, handles);

% Initialise cav cal routine
mh=msgbox('Initialising Bump Routines...');
nmCavCal('init');

% Set BPM names
[stat pars] = nmCavCal('GetPars');
if stat{1}~=1; errordlg(stat{2},'Error getting pars'); return; end;
set(handles.popupmenu1,'String',pars.bpmnames);
set(handles.popupmenu1,'Value',1);
popupmenu1_Callback(handles.popupmenu1,[],handles);

% Setup and start update timer
FL.t_bpmcal=timer('TimerFcn',['bpmcal(''pushbutton12_Callback'',',num2str(handles.pushbutton12,20),',[],',...
  '[])'],'ExecutionMode','fixedDelay','Period',1,'StartDelay',2);
start(FL.t_bpmcal);

if ishandle(mh); delete(mh); end;

% --- Outputs from this function are returned to the command line.
function varargout = bpmcal_OutputFcn(hObject, eventdata, handles)  %#ok<*INUSL>
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- EXIT
function pushbutton1_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global FL
stop(FL.t_bpmcal);
guiCloseFn('bpmcal',handles);

% --- Calibrate x
function pushbutton2_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
if ~strcmp(questdlg('Start calibration procedure (x)?'),'Yes')
  return
end
bpmnames=get(handles.popupmenu1,'String');
ibpm=get(handles.popupmenu1,'Value');
bpmname=bpmnames{ibpm};
stat = nmCavCal('CalibBump',bpmname,'x');
if stat{1}~=1
  errordlg(stat{2},'Calibration error')
end
popupmenu1_Callback(handles.popupmenu1,[],handles);

% --- Calibrate y
function pushbutton3_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global FL
if ~strcmp(questdlg('Start calibration procedure (y)?'),'Yes')
  return
end
bpmnames=get(handles.popupmenu1,'String');
ibpm=get(handles.popupmenu1,'Value');
bpmname=bpmnames{ibpm};
FL.Gui.bpmcal_msg=msgbox('Calibration in progress (y)...','Bump Calibration','modal');
stat = nmCavCal('CalibBump',bpmname,'y');
if stat{1}~=1
  errordlg(stat{2},'Calibration error')
end
if ishandle(FL.Gui.bpmcal_msg)
  delete(FL.Gui.bpmcal_msg)
end
popupmenu1_Callback(handles.popupmenu1,[],handles);

% Restore all bumps
function pushbutton4_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
if ~strcmp(questdlg('Restore all bumps to their zero position?'),'Yes')
  return
end
stat = nmCavCal('RestoreBumps');
if stat{1}~=1
  errordlg(stat{2},'Bump Restore error')
end
popupmenu1_Callback(handles.popupmenu1,[],handles);


% --- Reset all bumps
function pushbutton5_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton5 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
if ~strcmp(questdlg('Reset zero position on all bumps?'),'Yes')
  return
end
stat = nmCavCal('ResetBumps');
if stat{1}~=1
  errordlg(stat{2},'Bump Reset error')
end
popupmenu1_Callback(handles.popupmenu1,[],handles);

% --- set x bump
function pushbutton6_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton6 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
bpmnames=get(handles.popupmenu1,'String');
ibpm=get(handles.popupmenu1,'Value');
bpmname=bpmnames{ibpm};
FlHwUpdate; % update setpt values
stat = nmCavCal('SetBump',bpmname,'x',str2double(get(handles.edit1,'String'))*1e-3,...
  ~get(handles.checkbox1,'Value'));
if stat{1}~=1
  errordlg(stat{2},'Bump Set Error')
end
[stat pars]=nmCavCal('GetPars');
if stat{1}==1
  set(handles.text2,'String',num2str(pars.bumpknob.x{ibpm}.Value*1e3,5))
end

% --- Set y bump
function pushbutton7_Callback(hObject, eventdata, handles) %#ok<*INUSD,*DEFNU>
% hObject    handle to pushbutton7 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
bpmnames=get(handles.popupmenu1,'String');
ibpm=get(handles.popupmenu1,'Value');
bpmname=bpmnames{ibpm};
FlHwUpdate; % update setpt values
stat = nmCavCal('SetBump',bpmname,'y',str2double(get(handles.edit2,'String'))*1e-3,...
  ~get(handles.checkbox1,'Value'));
if stat{1}~=1
  errordlg(stat{2},'Bump Set Error')
end
[stat pars]=nmCavCal('GetPars');
if stat{1}==1
  set(handles.text3,'String',num2str(pars.bumpknob.y{ibpm}.Value*1e3,5))
end

% --- BPM select
function popupmenu1_Callback(hObject, eventdata, handles)
% hObject    handle to popupmenu1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

ibpm=get(hObject,'Value');
[stat, pars]=nmCavCal('GetPars');
bpmname=pars.bpmnames{ibpm};
if stat{1}~=1; errordlg(stat{2},'Error getting pars'); return; end;
set(handles.text2,'String',num2str(pars.bumpknob.x{ibpm}.Value*1e3,3))
set(handles.text3,'String',num2str(pars.bumpknob.y{ibpm}.Value*1e3,3))
set(handles.text39,'String',num2str(pars.bumpknob.xp{ibpm}.Value*1e3,3))
set(handles.text40,'String',num2str(pars.bumpknob.yp{ibpm}.Value*1e3,3))
set(handles.edit1,'String',num2str(pars.bumpknob.x{ibpm}.Value*1e3,3))
set(handles.edit2,'String',num2str(pars.bumpknob.y{ibpm}.Value*1e3,3))
set(handles.edit6,'String',num2str(pars.bumpknob.xp{ibpm}.Value*1e3,3))
set(handles.edit7,'String',num2str(pars.bumpknob.yp{ibpm}.Value*1e3,3))
set(handles.pushbutton8,'Visible','off')
set(handles.pushbutton9,'Visible','off')
set(handles.pushbutton10,'Visible','off')
set(handles.pushbutton11,'Visible','off')
if isfield(pars.bumpcalib,bpmname) && isfield(pars.bumpcalib.(bpmname),'xdate')
  set(handles.text4,'String',datestr(pars.bumpcalib.(bpmname).xdate))
else
  set(handles.text4,'String','None')
end
if isfield(pars.bumpcalib,bpmname) && isfield(pars.bumpcalib.(bpmname),'ydate')
  set(handles.text5,'String',datestr(pars.bumpcalib.(bpmname).ydate))
else
  set(handles.text5,'String','None')
end
set(handles.edit3,'String',num2str(pars.bumpcalib.size*1e3,5))
set(handles.edit4,'String',num2str(pars.bumpcalib.nstep))
set(handles.edit5,'String',num2str(pars.bumpcalib.nbpm))
uiresume(handles.figure1)
if get(handles.radiobutton5,'Value')
  wx='x';
elseif get(handles.radiobutton8,'Value')
  wx='xp';
elseif get(handles.radiobutton6,'Value')
  wx='y';
elseif get(handles.radiobutton9,'Value')
  wx='yp';
end
set(handles.text7,'String',pars.cornames.(wx(1)){ibpm}{1});
set(handles.text8,'String',pars.cornames.(wx(1)){ibpm}{2});
set(handles.text9,'String',pars.cornames.(wx(1)){ibpm}{3});
if length(pars.cornames.(wx(1)){ibpm})==4
  set(handles.text10,'String',pars.cornames.(wx(1)){ibpm}{4});
else
  set(handles.text10,'String','---')
end

drawnow('expose');

% Calibration dates
if strcmp(pars.bpmtype(ibpm),'stripline')
  try
    [data ts]=lcaGet([bpmname ':SCALE_X']);
    set(handles.text37,datestr(epicsts2mat(ts)))
  catch
    set(handles.text37,'String','---')
  end
  try
    [data ts]=lcaGet([bpmname ':SCALE_Y']);
    set(handles.text38,datestr(epicsts2mat(ts)))
  catch
    set(handles.text38,'String','---')
  end
else
  set(handles.text37,'String','---')
  set(handles.text38,'String','---')
end

% --- Executes during object creation, after setting all properties.
function popupmenu1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to popupmenu1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit1_Callback(hObject, eventdata, handles)
% hObject    handle to edit1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

set(handles.radiobutton5,'Value',1)
set(handles.radiobutton8,'Value',0)
set(handles.radiobutton6,'Value',0)
set(handles.radiobutton9,'Value',0)


% --- Executes during object creation, after setting all properties.
function edit1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit2_Callback(hObject, eventdata, handles)
% hObject    handle to edit2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

set(handles.radiobutton5,'Value',0)
set(handles.radiobutton8,'Value',0)
set(handles.radiobutton6,'Value',1)
set(handles.radiobutton9,'Value',0)


% --- Executes during object creation, after setting all properties.
function edit2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in pushbutton8.
function pushbutton8_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton8 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
uiresume(handles.figure1)
popupmenu1_Callback(handles.popupmenu1,[],handles);

% --- Executes on button press in pushbutton9.
function pushbutton9_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton9 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
uiresume(handles.figure1)
popupmenu1_Callback(handles.popupmenu1,[],handles);

% --- Executes on button press in pushbutton10.
function pushbutton10_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton10 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
uiresume(handles.figure1)
popupmenu1_Callback(handles.popupmenu1,[],handles);

% --- Executes on button press in pushbutton11.
function pushbutton11_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton11 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
uiresume(handles.figure1)
popupmenu1_Callback(handles.popupmenu1,[],handles);

% bump range
function edit3_Callback(hObject, eventdata, handles)
% hObject    handle to edit3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

newpars.bumpcalib.size=str2double(get(hObject,'String'))*1e-3;
nmCavCal('SetPars',newpars);
[stat pars]=nmCavCal('GetPars');
set(hObject,'String',num2str(pars.bumpcalib.size*1e3,4));


% --- Executes during object creation, after setting all properties.
function edit3_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% nsteps for bump calib
function edit4_Callback(hObject, eventdata, handles)
% hObject    handle to edit4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

newpars.bumpcalib.nstep=str2double(get(hObject,'String'));
nmCavCal('SetPars',newpars);
[stat pars]=nmCavCal('GetPars');
set(hObject,'String',num2str(pars.bumpcalib.nstep));


% --- Executes during object creation, after setting all properties.
function edit4_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in checkbox1.
function checkbox1_Callback(hObject, eventdata, handles)
% hObject    handle to checkbox1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of checkbox1


% --- Executes when user attempts to close figure1.
function figure1_CloseRequestFcn(hObject, eventdata, handles)
% hObject    handle to figure1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global FL
stop(FL.t_bpmcal);
guiCloseFn('bpmcal',handles);


% Nave
function edit5_Callback(hObject, eventdata, handles)
% hObject    handle to edit5 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

newpars.bumpcalib.nbpm=str2double(get(hObject,'String'));
nmCavCal('SetPars',newpars);


% --- Executes during object creation, after setting all properties.
function edit5_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit5 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- FS values
function radiobutton3_Callback(hObject, eventdata, handles)
% hObject    handle to radiobutton3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

if get(hObject,'Value')
  set(handles.radiobutton4,'Value',0)
end


% --- CS values
function radiobutton4_Callback(hObject, eventdata, handles)
% hObject    handle to radiobutton4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

if get(hObject,'Value')
  set(handles.radiobutton3,'Value',0)
end


% --- Horizontal bump readbacks
function radiobutton5_Callback(hObject, eventdata, handles)
% hObject    handle to radiobutton5 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

if get(hObject,'Value')
  set(handles.radiobutton8,'Value',0)
  set(handles.radiobutton9,'Value',0)
  set(handles.radiobutton6,'Value',0)
  popupmenu1_Callback(handles.popupmenu1,[],handles);
else
  set(hObject,'Value',1)
end


% --- Vertical bump readbacks
function radiobutton6_Callback(hObject, eventdata, handles)
% hObject    handle to radiobutton6 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

if get(hObject,'Value')
  set(handles.radiobutton8,'Value',0)
  set(handles.radiobutton9,'Value',0)
  set(handles.radiobutton5,'Value',0)
  popupmenu1_Callback(handles.popupmenu1,[],handles);
else
  set(hObject,'Value',1)
end


% --- Update script
function pushbutton12_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton12 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global FL BEAMLINE PS GIRDER %#ok<NUSED>
if ~isfield(FL.Gui,'bpmcal')
  return
end
handles=FL.Gui.bpmcal;
try
  [stat pars]=nmCavCal('GetPars');
  switch find([get(handles.radiobutton5,'Value') get(handles.radiobutton8,'Value') get(handles.radiobutton6,'Value') get(handles.radiobutton9,'Value') ])
    case 1
      plane='x';
    case 2
      plane='xp';
    case 3
      plane='y';
    case 4
      plane='yp';
  end
  % Get readback values
  ibpm=get(handles.popupmenu1,'Value');
  pl={'x' 'y' 'xp' 'yp'};
  % Init channel text with blanks
  for ihan=11:18
    set(handles.(['text',num2str(ihan)]),'String','---')
  end
  % Display corrector strengths
  sethans=[1 2 6 7];
  bumpsethans=[2 3 39 40];
  for iplane=1:4
    for ichan=1:min([length(pars.bumpknob.(pl{iplane}){ibpm}.Channel) 4])
      actparam=regexprep(pars.bumpknob.(pl{iplane}){ibpm}.Channel(ichan).Parameter,'\.SetPt','\.Ampl');
      actparam=regexprep(actparam,'\.MoverSetPt','\.MoverPos');
      evalc(['crdes=' pars.bumpknob.(pl{iplane}){ibpm}.Channel(ichan).Parameter]);
      evalc(['cract=' actparam]);
      % adjust for new required move if not made yet
      if str2double(get(handles.(['edit' num2str(sethans(iplane))]),'String')) ~= str2double(get(handles.(['text' num2str(bumpsethans(iplane))]),'String'))
        crdes=crdes+1e-3*pars.bumpknob.(pl{iplane}){ibpm}.Channel(ichan).Coefficient*(str2double(get(handles.(['edit',num2str(sethans(iplane))]),'String'))-...
          str2double(get(handles.(['text',num2str(bumpsethans(iplane))]),'String')));
      end
      % Get bump readback
      if iplane==1
        bumprbk_x(ichan)=(cract-pars.bumpcomp.x{ibpm}(ichan))/pars.bumpknob.x{ibpm}.Channel(ichan).Coefficient;
      elseif iplane==2
        bumprbk_y(ichan)=(cract-pars.bumpcomp.y{ibpm}(ichan))/pars.bumpknob.y{ibpm}.Channel(ichan).Coefficient;
      elseif iplane==3
        bumprbk_xp(ichan)=(cract-pars.bumpcomp.xp{ibpm}(ichan))/pars.bumpknob.xp{ibpm}.Channel(ichan).Coefficient;
      else
        bumprbk_yp(ichan)=(cract-pars.bumpcomp.yp{ibpm}(ichan))/pars.bumpknob.yp{ibpm}.Channel(ichan).Coefficient;
      end
      % FS or control system values to display
      tval=1e-6; cdim=[1 2 1 2];
      if get(handles.radiobutton4,'Value')
        if ~isempty(strfind(actparam,'MoverPos'))
          tval=1e-6;
          crdes=crdes/...
            FL.HwInfo.GIRDER(pars.bumpknob.(pl{iplane}){ibpm}.Channel(ichan).Unit).conv(cdim(iplane));
          cract=cract/...
            FL.HwInfo.GIRDER(pars.bumpknob.(pl{iplane}){ibpm}.Channel(ichan).Unit).conv(cdim(iplane));
        else
          tval=0.01;
          conv=FL.HwInfo.PS(pars.bumpknob.(pl{iplane}){ibpm}.Channel(ichan).Unit).conv;
          if length(conv)>1
            pvname=FL.HwInfo.PS(pars.bumpknob.(pl{iplane}){ibpm}.Channel(ichan).Unit).pvname;
            if length(pvname{2})==2
              mainI=abs(lcaGet(pvname{1}{1},1));
              thetaMain=interp1(conv(1,:),conv(2,:),mainI,'linear');
              iMainPlusTrim=interp1(conv(2,:),conv(1,:),thetaMain+crdes,'linear');
              crdes=(iMainPlusTrim-mainI)/FL.HwInfo.PS(pars.bumpknob.(pl{iplane}){ibpm}.Channel(ichan).Unit).nt_ratio;
              iMainPlusTrim=interp1(conv(2,:),conv(1,:),thetaMain+cract,'linear');
              cract=(iMainPlusTrim-mainI)/FL.HwInfo.PS(pars.bumpknob.(pl{iplane}){ibpm}.Channel(ichan).Unit).nt_ratio;
            else
              crdes=interp1(conv(2,:),conv(1,:),crdes,'linear');
              cract=interp1(conv(2,:),conv(1,:),cract,'linear');
            end
          else
            crdes=crdes/conv;
            cract=cract/conv;
          end
        end
      end
      if pl{iplane}==plane
        set(handles.(['text',num2str(10+ichan)]),'String',num2str(crdes,3))
        set(handles.(['text',num2str(14+ichan)]),'String',num2str(cract,3))
        if abs(abs(crdes-cract)/crdes) > 0.1 && cract > tval
          set(handles.(['text',num2str(14+ichan)]),'ForegroundColor','red')
        else
          set(handles.(['text',num2str(14+ichan)]),'ForegroundColor','black')
        end
      end
    end
  end
  if length(pars.bumpknob.(plane){ibpm}.Channel)==3
    set(handles.text14,'String','---')
    set(handles.text18,'String','---')
  end
  % Set bump readback
  set(handles.text2,'String',num2str(pars.bumpknob.x{ibpm}.Value*1e3,5))
  set(handles.text3,'String',num2str(pars.bumpknob.y{ibpm}.Value*1e3,5))
  set(handles.text39,'String',num2str(pars.bumpknob.xp{ibpm}.Value*1e3,5))
  set(handles.text40,'String',num2str(pars.bumpknob.yp{ibpm}.Value*1e3,5))
  set(handles.text33,'String',num2str(mean(bumprbk_x)*1e3,5))
  if abs(abs(str2double(get(handles.text2,'String'))-str2double(get(handles.text33,'String')))/str2double(get(handles.text2,'String'))) > 0.1 && ...
      abs(str2double(get(handles.text33,'String')))>0.001
    set(handles.text33,'ForegroundColor','red')
  else
    set(handles.text33,'ForegroundColor','black')
  end
  set(handles.text34,'String',num2str(mean(bumprbk_y)*1e3,5))
  if abs(abs(str2double(get(handles.text3,'String'))-str2double(get(handles.text34,'String')))/str2double(get(handles.text3,'String'))) > 0.1 && ...
      abs(str2double(get(handles.text34,'String')))>0.001
    set(handles.text34,'ForegroundColor','red')
  else
    set(handles.text34,'ForegroundColor','black')
  end
  set(handles.text41,'String',num2str(mean(bumprbk_xp)*1e3,5))
  if abs(abs(str2double(get(handles.text39,'String'))-str2double(get(handles.text41,'String')))/str2double(get(handles.text39,'String'))) > 0.1 && ...
      abs(str2double(get(handles.text41,'String')))>0.001
    set(handles.text41,'ForegroundColor','red')
  else
    set(handles.text41,'ForegroundColor','black')
  end
  set(handles.text42,'String',num2str(mean(bumprbk_yp)*1e3,5))
  if abs(abs(str2double(get(handles.text40,'String'))-str2double(get(handles.text42,'String')))/str2double(get(handles.text40,'String'))) > 0.1 && ...
      abs(str2double(get(handles.text42,'String')))>0.001
    set(handles.text42,'ForegroundColor','red')
  else
    set(handles.text42,'ForegroundColor','black')
  end
  drawnow('expose');
catch
  warning('Lucretia:Floodland:bpmcalgui',['bpmcal gui error: ',lasterr])
end


% --- Restore bump
function pushbutton13_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton13 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
bpmnames=get(handles.popupmenu1,'String');
ibpm=get(handles.popupmenu1,'Value');
if get(handles.radiobutton5,'Value')
  stat = nmCavCal('RestoreBumps',bpmnames{ibpm},'x');
else
  stat = nmCavCal('RestoreBumps',bpmnames{ibpm},'y');
end
if stat{1}~=1
  errordlg(stat{2},'Bump Restore error')
end
popupmenu1_Callback(handles.popupmenu1,[],handles);

% --- Reset bump
function pushbutton14_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton14 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
bpmnames=get(handles.popupmenu1,'String');
ibpm=get(handles.popupmenu1,'Value');
if get(handles.radiobutton5,'Value')
  stat = nmCavCal('ResetBumps',bpmnames{ibpm},'x');
else
  stat = nmCavCal('ResetBumps',bpmnames{ibpm},'y');
end
if stat{1}~=1
  errordlg(stat{2},'Bump Reset error')
end
popupmenu1_Callback(handles.popupmenu1,[],handles);


% --- Executes on button press in checkbox2.
function checkbox2_Callback(hObject, eventdata, handles)
% hObject    handle to checkbox2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of checkbox2


% --- Launch X calibration panel
function pushbutton16_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton16 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global FL
ibpm=get(handles.popupmenu1,'Value');
[stat, pars]=nmCavCal('GetPars');
bpmname=pars.bpmnames{ibpm};
if bpmname(end)=='X'
  caldata{1}=1; caldata{2}=bpmname;
  FL.Gui.bpmcal_calFig=bpmcal_calFig('UserData',caldata);
else
  errordlg('Only calibration of Extraction Line BPMs through this interface','Cal Error');
  return
end



% --- Launch Y calibration panel
function pushbutton17_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton17 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global FL
ibpm=get(handles.popupmenu1,'Value');
[stat, pars]=nmCavCal('GetPars');
bpmname=pars.bpmnames{ibpm};
if bpmname(end)=='X'
  caldata{1}=2; caldata{2}=bpmname;
  FL.Gui.bpmcal_calFig=bpmcal_calFig('UserData',caldata);
else
  errordlg('Only calibration of Extraction Line BPMs through this interface','Cal Error');
  return
end


% --- Executes on button press in pushbutton18.
function pushbutton18_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton18 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
if ~strcmp(questdlg('Start calibration procedure (x'')?'),'Yes')
  return
end
bpmnames=get(handles.popupmenu1,'String');
ibpm=get(handles.popupmenu1,'Value');
bpmname=bpmnames{ibpm};
stat = nmCavCal('CalibBump',bpmname,'xp');
if stat{1}~=1
  errordlg(stat{2},'Calibration error')
end
popupmenu1_Callback(handles.popupmenu1,[],handles);

% --- Executes on button press in pushbutton19.
function pushbutton19_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton19 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
if ~strcmp(questdlg('Start calibration procedure (y'')?'),'Yes')
  return
end
bpmnames=get(handles.popupmenu1,'String');
ibpm=get(handles.popupmenu1,'Value');
bpmname=bpmnames{ibpm};
stat = nmCavCal('CalibBump',bpmname,'yp');
if stat{1}~=1
  errordlg(stat{2},'Calibration error')
end
popupmenu1_Callback(handles.popupmenu1,[],handles);

% --- Executes on button press in pushbutton20.
function pushbutton20_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton20 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
bpmnames=get(handles.popupmenu1,'String');
ibpm=get(handles.popupmenu1,'Value');
bpmname=bpmnames{ibpm};
FlHwUpdate; % update setpt values
stat = nmCavCal('SetBump',bpmname,'xp',str2double(get(handles.edit6,'String'))*1e-3,...
  ~get(handles.checkbox1,'Value'));
if stat{1}~=1
  errordlg(stat{2},'Bump Set Error')
end
[stat pars]=nmCavCal('GetPars');
if stat{1}==1
  set(handles.text39,'String',num2str(pars.bumpknob.xp{ibpm}.Value*1e3,5))
end


% --- Executes on button press in pushbutton21.
function pushbutton21_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton21 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
bpmnames=get(handles.popupmenu1,'String');
ibpm=get(handles.popupmenu1,'Value');
bpmname=bpmnames{ibpm};
FlHwUpdate; % update setpt values
stat = nmCavCal('SetBump',bpmname,'yp',str2double(get(handles.edit7,'String'))*1e-3,...
  ~get(handles.checkbox1,'Value'));
if stat{1}~=1
  errordlg(stat{2},'Bump Set Error')
end
[stat pars]=nmCavCal('GetPars');
if stat{1}==1
  set(handles.text40,'String',num2str(pars.bumpknob.yp{ibpm}.Value*1e3,5))
end



function edit6_Callback(hObject, eventdata, handles)
% hObject    handle to edit6 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

set(handles.radiobutton5,'Value',0)
set(handles.radiobutton8,'Value',1)
set(handles.radiobutton6,'Value',0)
set(handles.radiobutton9,'Value',0)


% --- Executes during object creation, after setting all properties.
function edit6_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit6 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit7_Callback(hObject, eventdata, handles)
% hObject    handle to edit7 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

set(handles.radiobutton5,'Value',0)
set(handles.radiobutton8,'Value',0)
set(handles.radiobutton6,'Value',0)
set(handles.radiobutton9,'Value',1)


% --- Executes during object creation, after setting all properties.
function edit7_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit7 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in radiobutton8.
function radiobutton8_Callback(hObject, eventdata, handles)
% hObject    handle to radiobutton8 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

if get(hObject,'Value')
  set(handles.radiobutton5,'Value',0)
  set(handles.radiobutton9,'Value',0)
  set(handles.radiobutton6,'Value',0)
  popupmenu1_Callback(handles.popupmenu1,[],handles);
else
  set(hObject,'Value',1)
end


% --- Executes on button press in radiobutton9.
function radiobutton9_Callback(hObject, eventdata, handles)
% hObject    handle to radiobutton9 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

if get(hObject,'Value')
  set(handles.radiobutton8,'Value',0)
  set(handles.radiobutton5,'Value',0)
  set(handles.radiobutton6,'Value',0)
  popupmenu1_Callback(handles.popupmenu1,[],handles);
else
  set(hObject,'Value',1)
end

function varargout = ringEnergyCalc(varargin)
% RINGENERGYCALC M-file for ringEnergyCalc.fig
%      RINGENERGYCALC, by itself, creates a new RINGENERGYCALC or raises the existing
%      singleton*.
%
%      H = RINGENERGYCALC returns the handle to a new RINGENERGYCALC or the handle to
%      the existing singleton*.
%
%      RINGENERGYCALC('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in RINGENERGYCALC.M with the given input arguments.
%
%      RINGENERGYCALC('Property','Value',...) creates a new RINGENERGYCALC or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before ringEnergyCalc_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to ringEnergyCalc_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help ringEnergyCalc

% Last Modified by GUIDE v2.5 05-May-2008 10:49:23

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @ringEnergyCalc_OpeningFcn, ...
                   'gui_OutputFcn',  @ringEnergyCalc_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before ringEnergyCalc is made visible.
function ringEnergyCalc_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to ringEnergyCalc (see VARARGIN)
global FL BEAMLINE

% Choose default command line output for ringEnergyCalc
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% Update online Model Twiss parameters
[stat,FL.SimModel.Twiss] = GetTwiss(1,length(BEAMLINE),FL.SimModel.Initial.x.Twiss,FL.SimModel.Initial.y.Twiss) ;
if stat{1}~=1
  errordlg(['Error updating model Twiss parameters:' stat{2}],'ringEnergyCalc error');
end % if err


% --- Outputs from this function are returned to the command line.
function varargout = ringEnergyCalc_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles;


% --- Executes on button press in pushbutton1.
function pushbutton1_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

set(handles.text4,'String','starting...');
npulse=str2double(get(handles.edit1,'String'));
if isnan(round(npulse)) || (round(npulse)<=1 && round(npulse)~=0)
  errordlg('Must enter an integer number for number of pulses to use','Energy Calc Error');
  return
end % if bad number of pulses
set(handles.text4,'String','Getting Average orbit data and computing dE/E...'); drawnow('expose');
if ~npulse
  [stat,dEmean,dErms]=dECalc;
else
  [stat,dEmean,dErms]=dECalc(npulse);
end % if npulse set to 0, use all available pulses
if stat{1}~=1
  errordlg(stat{2},'dECalc Error')
  return
end % if err
set(handles.text2,'String',sprintf('last: %.3f +/- %.3f',dEmean(end)*1e4,dErms(end)*1e4));
set(handles.text5,'String',sprintf('rms: %.3f',std(dEmean(~isnan(dEmean)))*1e4));
set(handles.text4,'String','done'); drawnow('expose');

% --- Executes on button press in togglebutton1.
function togglebutton1_Callback(hObject, eventdata, handles)
% hObject    handle to togglebutton1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of togglebutton1
global FL
if ~isfield(FL,'t_ringEnergyCalc')
  FL.t_ringEnergyCalc=timer('StartDelay',1,'Period',1/FL.accRate,...
   'ExecutionMode','FixedRate','BusyMode','drop');
  set(FL.t_ringEnergyCalc, 'TimerFcn', {@ringEnergyCalc,'pushbutton1_Callback',handles.pushbutton1,[],handles});
end % if need to create timer object
if get(hObject,'Value')
  if ~isequal(FL.t_ringEnergyCalc.running,'on')
    start(FL.t_ringEnergyCalc);
  end % if not already running
else
  stop(FL.t_ringEnergyCalc);
end % if autorun toggled

function edit1_Callback(hObject, eventdata, handles)
% hObject    handle to edit1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit1 as text
%        str2double(get(hObject,'String')) returns contents of edit1 as a double

% --- Executes during object creation, after setting all properties.
function edit1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
val=str2double(get(hObject,'String'));
if isnan(val) || ~isnumeric(val)
  errordlg('Must pass number in Npulse field','extDispCalc Error');
  return
end % if not num
[stat,output]=FlHwUpdate('buffersize');
if stat{1}~=1
  errordlg('Not able to receive bpm buffer size','FlHwUpdate error');
  return
end % if err
set(hObject,'String',num2str(max(output,val)));

% --- Executes on button press in pushbutton2.
function pushbutton2_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global FL BEAMLINE

% Update Model Twiss parameters
[stat,FL.SimModel.Twiss] = GetTwiss(1,length(BEAMLINE),FL.SimModel.Initial.x.Twiss,FL.SimModel.Initial.y.Twiss) ;
if stat{1}~=1
  errordlg(['Error updating model Twiss parameters:' stat{2}],'ringEnergyCalc error');
end % if err
set(handles.text4,'String','Updated Twiss Parameters from Model.');

% --- Executes on button press in pushbutton3.
function pushbutton3_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global FL

if isfield(FL,'t_ringEnergyCalc') && isequal(FL.t_ringEnergyCalc.running,'on')
  stop(FL.t_ringEnergyCalc);
end % stop updater if running
guiCloseFn('ringEnergyCalc',handles);


% --- Executes when user attempts to close figure1.
function figure1_CloseRequestFcn(hObject, eventdata, handles)
% hObject    handle to figure1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global FL

if isfield(FL,'t_ringEnergyCalc') && isequal(FL.t_ringEnergyCalc.running,'on')
  stop(FL.t_ringEnergyCalc);
end % stop updater if running
guiCloseFn('ringEnergyCalc',handles);



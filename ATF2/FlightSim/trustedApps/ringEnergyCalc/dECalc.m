function [stat,dEmean,dErms,bpm_rawdata]=dECalc(Npulse)
% [stat,dEmean,dErms]=dECalc(Npulse)
% -------------------------------------------------------------------------
% Calculate ATF Ring Energy from buffered BPM data
% Calculates delta-E using deviations in dispersive orbit in ring arcs from
% mean orbit
% -------------------------------------------------------------------------
% 
% Inputs: Npulse (optional): number of buffered pulses to use, if omitted,
%                            use all available
%
% Oututs: stat: Lucretia stat return
%         dEmean: 1 x Npulse vector, mean of arc bpm dE estimates
%         dErms: 1 x Npulse vector, rms of arc bpm dE estimates
%
% -------------------------------------------------------------------------
% Glen White, April 29 2008
%
global BEAMLINE INSTR FL

subrange=25:42;
dEmean=[]; dErms=[];
stat{1}=1; 

% Get bad bpm list
badbpmind=[];
if isfield(FL,'badbpm') && ~isempty(FL.badbpm.instind)
  badbpmind=FL.badbpm.instind;
end % if bad bpm list exists

% Form list of ring arc bpms
t=regexp(sprintf('MB%dR ',[32:65 81:96 1:18]),'(\S+)\s+','tokens');
bmllist=cellfun(@(x) findcells(BEAMLINE,'Name',x{1}),t);
instlist=find(cellfun(@(x) ismember(x.Index,bmllist),INSTR));
instlist=instlist(~ismember(instlist,badbpmind));
if isempty(instlist)
  stat{1}=-1; stat{2}='All required bpms on bad list!';
  return
end % if all required bpms flagged bad

% Get Model dispersion values
etax=FL.SimModel.Twiss.etax(cellfun(@(x) x.Index,INSTR));

% If Npulse not passed- use all available
if ~exist('Npulse','var')
  [stat, Npulse]=FlHwUpdate('buffersize');
  if stat{1}~=1; return; end;
end % if ~Npulse

% Get Required number of bpm readings from buffer
[stat,bpm_rawdata]=FlHwUpdate('readbuffer',Npulse);
if stat{1}~=1; stat{2}=['readbuffer fail: ',stat{2}];return; end;

% Get average orbit in arc bpms
[stat,bpm_avedata]=FlHwUpdate('bpmave',Npulse);
if stat{1}<0; stat{2}=['bpmave fail: ',stat{2}]; return; end;
if max(bpm_avedata{1}(7,:))>Npulse/2; stat{1}=-1; stat{2}='bpmave fail- too many bad pulses'; return; end;

% Get Energy estimate
for ipulse=1:Npulse
  de_calc(ipulse,:)=((bpm_avedata{1}(1,:)-bpm_rawdata(ipulse,instlist.*3-2))./etax(instlist)).*bpm_avedata{2}{1}(ipulse,:);
end % for ipulse
dEmean=arrayfun(@(x) mean(de_calc(x,de_calc(x,subrange)~=0)),1:Npulse);
dErms=arrayfun(@(x) std(de_calc(x,de_calc(x,subrange)~=0)),1:Npulse);
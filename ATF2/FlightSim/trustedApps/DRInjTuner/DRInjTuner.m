function varargout = DRInjTuner(varargin)
% DRINJTUNE MATLAB code for DRInjTune.fig
%      DRINJTUNE, by itself, creates a new DRINJTUNE or raises the existing
%      singleton*.
%
%      H = DRINJTUNE returns the handle to a new DRINJTUNE or the handle to
%      the existing singleton*.
%
%      DRINJTUNE('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in DRINJTUNE.M with the given input arguments.
%
%      DRINJTUNE('Property','Value',...) creates a new DRINJTUNE or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before DRInjTune_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to DRInjTune_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help DRInjTune

% Last Modified by GUIDE v2.5 24-Oct-2012 09:22:29

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @DRInjTune_OpeningFcn, ...
                   'gui_OutputFcn',  @DRInjTune_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before DRInjTune is made visible.
function DRInjTune_OpeningFcn(hObject, eventdata, handles, varargin) %#ok<*INUSL>
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to DRInjTune (see VARARGIN)

% Choose default command line output for DRInjTune
handles.output = hObject;
handles.rlim1 = 400;
handles.rlim2 = 500;

pvBpm = 'bpm21.';
lcaPut([pvBpm 'PROC'],9);
pause(0.1)
% read vert pos array
posv = lcaGet([pvBpm 'WPSV']);
% read horz pos array
posh = lcaGet([pvBpm 'WPSH']);
% do ffts
vfft = fft(posv(2:end));
hfft = fft(posh(2:end));
% find vert tune, search around 565
vi = find(abs(vfft(515:615))==max(abs(vfft(515:615))))+514;
% find horz tune, search around 175
hi = find(abs(hfft(125:225))==max(abs(hfft(125:225))))+124;
pvBpm = 'bpm41.';
lcaPut([pvBpm 'PROC'],9);
pause(0.1)
% read horz pos array
posh = lcaGet([pvBpm 'WPSH']);
sfft = fft(posh(2:end));
% find sync tune, search around 5
si = find(abs(sfft(3:7))==max(abs(sfft(3:7))))+2;

%update plots
hold(handles.axes1,'off')
polar(handles.axes1,handles.rlim1,'k.')
hold(handles.axes1,'on')
polar(handles.axes1,angle(hfft(hi)),abs(hfft(hi))/512,'-d')
polar(handles.axes1,angle(vfft(vi)),abs(vfft(vi))/512,'r-d')
% legend('','X','Y')

hold(handles.axes2,'off')
polar(handles.axes2,handles.rlim2,'k.')
hold(handles.axes2,'on')
polar(handles.axes2,angle(sfft(si)),abs(sfft(si))/512,'g-d')

% Initialize timer for auto update function
than=timer('TimerFcn',@(x,y)DRInjTuner('pushbutton1_Callback',handles.pushbutton1,[],handles),...
  'BusyMode','drop','Period',1,'ExecutionMode','fixedDelay');

set(handles.figure1,'UserData',than);

% Update handles structure
guidata(hObject, handles);

% --- Outputs from this function are returned to the command line.
function varargout = DRInjTune_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on button press in pushbutton1.
function pushbutton1_Callback(hObject, eventdata, handles) %#ok<*DEFNU>
% hObject    handle to pushbutton1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% read injection tbt data
pvBpm = 'bpm21.';
lcaPut([pvBpm 'PROC'],9);
pause(0.1)
% read vert pos array
posv = lcaGet([pvBpm 'WPSV']);
% read horz pos array
posh = lcaGet([pvBpm 'WPSH']);
% do ffts
vfft = fft(posv(2:end));
hfft = fft(posh(2:end));
% find vert tune, search around 565
vi = find(abs(vfft(515:615))==max(abs(vfft(515:615))))+514;
% find horz tune, search around 175
hi = find(abs(hfft(125:225))==max(abs(hfft(125:225))))+124;
pvBpm = 'bpm41.';
lcaPut([pvBpm 'PROC'],9);
pause(0.1)
% read horz pos array
posh = lcaGet([pvBpm 'WPSH']);
sfft = fft(posh(2:end));
% find sync tune, search around 5
si = find(abs(sfft(3:7))==max(abs(sfft(3:7))))+2;

%update plots
polar(handles.axes1,handles.rlim1,'k.')
hold(handles.axes1,'on')
polar(handles.axes1,angle(hfft(hi)),abs(hfft(hi))/512,'-d')
polar(handles.axes1,angle(vfft(vi)),abs(vfft(vi))/512,'r-d')
% legend('','X','Y')

polar(handles.axes2,handles.rlim2,'k.')
hold(handles.axes2,'on')
polar(handles.axes2,angle(sfft(si)),abs(sfft(si))/512,'g-d')

drawnow('expose');

% --- Executes on button press in pushbutton2.
function pushbutton2_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
pvBpm = 'bpm21.';
lcaPut([pvBpm 'PROC'],9);
pause(0.1)
% read vert pos array
posv = lcaGet([pvBpm 'WPSV']);
% read horz pos array
posh = lcaGet([pvBpm 'WPSH']);
% do ffts
vfft = fft(posv(2:end));
hfft = fft(posh(2:end));
% find vert tune, search around 565
vi = find(abs(vfft(515:615))==max(abs(vfft(515:615))))+514;
% find horz tune, search around 175
hi = find(abs(hfft(125:225))==max(abs(hfft(125:225))))+124;
pvBpm = 'bpm41.';
lcaPut([pvBpm 'PROC'],9);
pause(0.1)
% read horz pos array
posh = lcaGet([pvBpm 'WPSH']);
sfft = fft(posh(2:end));
% find sync tune, search around 5
si = find(abs(sfft(3:7))==max(abs(sfft(3:7))))+2;

%update plots
hold(handles.axes1,'off')
polar(handles.axes1,handles.rlim1,'k.')
hold(handles.axes1,'on')
polar(handles.axes1,angle(hfft(hi)),abs(hfft(hi))/512,'-d')
polar(handles.axes1,angle(vfft(vi)),abs(vfft(vi))/512,'r-d')
% legend('','X','Y')

hold(handles.axes2,'off')
polar(handles.axes2,handles.rlim2,'k.')
hold(handles.axes2,'on')
polar(angle(sfft(si)),abs(sfft(si))/512,'g-d')

% Call Scale Edit window callbacks to keep user set scale
edit1_Callback(handles.edit1,[],handles);
edit2_Callback(handles.edit2,[],handles);

function edit1_Callback(hObject, eventdata, handles)
% hObject    handle to edit1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit1 as text
%        str2double(get(hObject,'String')) returns contents of edit1 as a double
str = get(hObject, 'String');
handles.rlim1 = str2double(str);
pvBpm = 'bpm21.';
lcaPut([pvBpm 'PROC'],9);
pause(0.1)
% read vert pos array
posv = lcaGet([pvBpm 'WPSV']);
% read horz pos array
posh = lcaGet([pvBpm 'WPSH']);
% do ffts
vfft = fft(posv(2:end));
hfft = fft(posh(2:end));
% find vert tune, search around 565
vi = find(abs(vfft(515:615))==max(abs(vfft(515:615))))+514;
% find horz tune, search around 175
hi = find(abs(hfft(125:225))==max(abs(hfft(125:225))))+124;
% pvBpm = 'bpm41.';
% lcaPut([pvBpm 'PROC'],9);
% pause(0.1)
% % read horz pos array
% posh = lcaGet([pvBpm 'WPSH']);
% sfft = fft(posh(2:end));
% % find sync tune, search around 5
% si = find(abs(sfft(3:7))==max(abs(sfft(3:7))))+2;

%update plots
hold(handles.axes1,'off')
polar(handles.axes1,handles.rlim1,'k.')
hold(handles.axes1,'on')
polar(handles.axes1,angle(hfft(hi)),abs(hfft(hi))/512,'-d')
polar(handles.axes1,angle(vfft(vi)),abs(vfft(vi))/512,'r-d')
% legend('','X','Y')

% axes(handles.axes2)
% hold off
% polar(0,handles.rlim2,'k.')
% hold on
% polar(angle(sfft(si)),abs(sfft(si))/512,'g-d')

% --- Executes during object creation, after setting all properties.
function edit1_CreateFcn(hObject, eventdata, handles) %#ok<*INUSD>
% hObject    handle to edit1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit2_Callback(hObject, eventdata, handles)
% hObject    handle to edit2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit2 as text
%        str2double(get(hObject,'String')) returns contents of edit2 as a double
str = get(hObject, 'String');
handles.rlim2 = str2double(str);
% pvBpm = 'bpm21.';
% lcaPut([pvBpm 'PROC'],9);
% pause(0.1)
% % read vert pos array
% posv = lcaGet([pvBpm 'WPSV']);
% % read horz pos array
% posh = lcaGet([pvBpm 'WPSH']);
% % do ffts
% vfft = fft(posv(2:end));
% hfft = fft(posh(2:end));
% % find vert tune, search around 565
% vi = find(abs(vfft(515:615))==max(abs(vfft(515:615))))+514;
% % find horz tune, search around 175
% hi = find(abs(hfft(125:225))==max(abs(hfft(125:225))))+124;
pvBpm = 'bpm41.';
lcaPut([pvBpm 'PROC'],9);
pause(0.1)
% read horz pos array
posh = lcaGet([pvBpm 'WPSH']);
sfft = fft(posh(2:end));
% find sync tune, search around 5
si = find(abs(sfft(3:7))==max(abs(sfft(3:7))))+2;

% %update plots
% axes(handles.axes1)
% hold off
% polar(0,handles.rlim1,'k.')
% hold on
% polar(angle(hfft(hi)),abs(hfft(hi))/512,'-d')
% polar(angle(vfft(vi)),abs(vfft(vi))/512,'r-d')
% % legend('','X','Y')
% 
hold(handles.axes2,'off')
polar(handles.axes2,handles.rlim2,'k.')
hold(handles.axes2,'on')
polar(handles.axes2,angle(sfft(si)),abs(sfft(si))/512,'g-d')

% --- Executes during object creation, after setting all properties.
function edit2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in togglebutton1.
function togglebutton1_Callback(hObject, eventdata, handles)
% hObject    handle to togglebutton1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

than=get(handles.figure1,'UserData');
if get(hObject,'Value')
  start(than);
else
  stop(than);
end

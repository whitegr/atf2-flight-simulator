classdef extMatchClass < handle
  %BETA Matching of EXT
  %   Match EXT using quads upstream of OTR0X
  
  properties(Constant)
    matchParams={'alpha_x' 'beta_x' 'alpha_y' 'beta_y' 'eta_x' 'etap_x' 'eta_y' 'etap_y'};
    matchMins=[-10 0 -10 0 -1 -1 -1 -1];
    matchMaxs=[10 1e4 10 1e4 1 1 1 1];
    otrNames={'OTR0X' 'OTR1X' 'OTR2X' 'OTR3X'};
    optimDisplay='off';
  end
  properties(SetObservable,AbortSet)
    matchData % Data from (mOTR) measurements
    matchPoint % lattice location for performing match (usually otr0x)
  end
  properties
    chosenMatchParams % logical vector of matchParams to choose
    designTwiss % design twiss parameters at match point (to recover with matching)
    matchQuads={'QF1X' 'QD2X' 'QF3X' 'QF4X' 'QD5X' 'QF6X' 'QF7X' 'QD8X' 'QF9X'}; % cell array of quad names to use in match
  end
  properties(Dependent)
    matchQuadPS % PS array for matching quads
    psVals % Current PS settings for matching quads
    psValsLow
    psValsHigh
    psMatchValsI % PS current values for psMatchVals / A
    psValsI % Present PS current values
  end
  properties(SetAccess=protected)
    Initial % Initial data structure at first quad to match
    iInitial % Element # corresponding to Initial data structure
    psMatchVals % PS values for rematch
    modelTwiss
    twissMatched % alpha_x beta_x alpha_y beta_y eta_x etap_x eta_y etap_y Matched values
  end
  properties(Access=protected)
    otrInds % BEAMLINE indices corresponding to OTRs
  end
  
  methods
    function obj=extMatchClass()
      global BEAMLINE FL
      % Load design Twiss values for chosen model version
      ld=load(fullfile('latticeFiles','src',['v' FL.SimModel.opticsVersion],['ATF2lat_' FL.SimModel.opticsName]),'Model');
      obj.modelTwiss=ld.Model.Twiss;
      for iotr=1:length(obj.otrNames)
        obj.otrInds(iotr)=findcells(BEAMLINE,'Name',obj.otrNames{iotr});
      end
      obj.chosenMatchParams=true(size(obj.matchParams));
      obj.matchPoint=obj.otrInds(1);
      obj.getDesignTwiss;
      obj.getInitial;
      addlistener(obj,'matchData','PostSet',@extMatchClass.handlePropEvents);
    end
    function obj=setPS(obj)
      % setPS() - Set PS's to match values
      global PS
      psind=obj.matchQuadPS;
      if length(obj.psMatchVals)~=length(psind)
        error('No PS values from matching, or incompatibility between match quad definition and last match')
      end
      for ips=1:length(psind)
        PS(psind(ips)).SetPt=obj.psMatchVals(ips);
      end
      PSTrim(psind,1);
    end
    function obj=doMatch(obj)
      % doMatch() - Perform beta matching
      global FL
      Tx=obj.Initial.x.Twiss;
      Ty=obj.Initial.y.Twiss;
      psinds=obj.matchQuadPS;
      cp=obj.chosenMatchParams;
      flNoUpdateStat=FL.noUpdate;
      FL.noUpdate=true;
      psv=obj.psVals;
      try
        obj.psMatchVals=lsqnonlin(@(x) extMatchClass.matchFun(x,Tx,Ty,psinds,obj.designTwiss,obj.iInitial,obj.matchPoint,cp),...
          psv,obj.psValsLow,obj.psValsHigh,optimset('Display',obj.optimDisplay));
      catch
        obj.psMatchVals=lsqnonlin(@(x) extMatchClass.matchFun(x,Tx,Ty,psinds,obj.designTwiss,obj.iInitial,obj.matchPoint,cp),...
          psv,[],[],optimset('Algorithm','levenberg-marquardt','Display',obj.optimDisplay));
      end
      obj.psVals=psv;
      obj.twissMatched=extMatchClass.matchFun('GetData');
      FL.noUpdate=flNoUpdateStat;
      FlHwUpdate('force');
    end
    function obj=getOtrData(obj,dataFile)
      % getOtrData() - Get processed OTR data
      % default: get latest saved OTR data set
      % dataFile: if provided, get this data file instead of latest
      global BEAMLINE
      if exist('dataFile','var')
        ld=load(fullfile('userData',dataFile));
      else % load latest
        d=dir(fullfile('userData','emit2dOTRp*'));
        [~,ind]=sort([d.datenum]);
        ld=load(fullfile('userData',d(ind(end)).name));
      end
      obj.matchPoint=obj.otrInds(find(ld.otruse,1));
      gamma=BEAMLINE{obj.matchPoint}.P/0.511e-3;
      md.NEmit_x=ld.data.emitx*gamma;
      md.NEmit_y=ld.data.emity*gamma;
      md.beta_x=ld.data.betax;
      md.beta_y=ld.data.betay;
      md.alpha_x=ld.data.alphx;
      md.alpha_y=ld.data.alphy;
      md.eta_x=ld.data.DX(1);
      md.etap_x=ld.data.DPX(1);
      md.eta_y=ld.data.DY(1);
      md.etap_y=ld.data.DPY(1);
      obj.matchData=md;
    end
    function ps=get.matchQuadPS(obj)
      global BEAMLINE
      for iq=1:length(obj.matchQuads)
        iele=findcells(BEAMLINE,'Name',obj.matchQuads{iq});
        ps(iq)=BEAMLINE{iele(1)}.PS;
      end
    end
    function vals=get.psVals(obj)
      global PS
      psind=obj.matchQuadPS;
      try
        vals=[PS(psind).Ampl];
      catch
        vals=nan(size(psind));
      end
    end
    function set.psVals(obj,vals)
      global PS
      psind=obj.matchQuadPS;
      if length(vals)~=length(psind)
        error('Must past vector equal to length of matchQuadPS property')
      end
      for ips=1:length(psind)
        PS(psind(ips)).Ampl=vals(ips);
      end
    end
    function vals=get.psValsLow(obj)
      global FL
      psind=obj.matchQuadPS;
      try
        vals=[FL.HwInfo.PS(psind(psind)).low];
      catch
        vals=zeros(size(psind));
      end
    end
    function vals=get.psValsHigh(obj)
      global FL
      psind=obj.matchQuadPS;
      try
        vals=[FL.HwInfo.PS(psind(psind)).high];
      catch
        vals=ones(size(psind)).*10;
      end
    end
    function vals=get.psValsI(obj)
      global FL PS BEAMLINE
      psind=obj.matchQuadPS;
      for ips=1:length(psind)
        hw=FL.HwInfo.PS(psind(ips));
        vals(ips)=interp1(hw.conv(2,:),hw.conv(1,:),PS(psind(ips)).Ampl*abs(BEAMLINE{PS(psind(ips)).Element(1)}.B(1)),'linear');
      end
    end
    function vals=get.psMatchValsI(obj)
      global FL PS BEAMLINE
      psind=obj.matchQuadPS;
      for ips=1:length(obj.psMatchVals)
        hw=FL.HwInfo.PS(psind(ips));
        vals(ips)=interp1(hw.conv(2,:),hw.conv(1,:),obj.psMatchVals(ips)*abs(BEAMLINE{PS(psind(ips)).Element(1)}.B(1)),'linear');
      end
    end
    function obj=getInitial(obj)
      global BEAMLINE FL
      % getInitial() - form Initial data structure property 
      bl=findcells(BEAMLINE,'Name',obj.matchQuads{1});
      obj.iInitial=bl(1);
      I=FL.SimModel.Initial;
      I.Momentum=BEAMLINE{obj.iInitial}.P;
      I.x.Twiss.beta=FL.SimModel.Twiss.betax(obj.iInitial);
      I.y.Twiss.beta=FL.SimModel.Twiss.betay(obj.iInitial);
      I.x.Twiss.alpha=FL.SimModel.Twiss.alphax(obj.iInitial);
      I.y.Twiss.alpha=FL.SimModel.Twiss.alphay(obj.iInitial);
      I.x.Twiss.eta=FL.SimModel.Twiss.etax(obj.iInitial);
      I.y.Twiss.eta=FL.SimModel.Twiss.etay(obj.iInitial);
      I.x.Twiss.etap=FL.SimModel.Twiss.etapx(obj.iInitial);
      I.y.Twiss.etap=FL.SimModel.Twiss.etapy(obj.iInitial);
      if isempty(obj.matchData)
        obj.Initial=I;
        return
      end
      I.x.NEmit=obj.matchData.NEmit_x;
      I.y.NEmit=obj.matchData.NEmit_y;
      initVals=[I.x.Twiss.alpha I.x.Twiss.beta I.y.Twiss.alpha I.y.Twiss.beta ...
        I.x.Twiss.eta I.x.Twiss.etap I.y.Twiss.eta I.y.Twiss.etap];
      twiss=lsqnonlin(@(x) extMatchClass.matchFun_getInitial(x,obj.matchData,obj.iInitial,obj.matchPoint),...
        initVals,obj.matchMins,obj.matchMaxs,optimset('Display',obj.optimDisplay));
      I.x.Twiss.alpha=twiss(1);
      I.x.Twiss.beta=twiss(2);
      I.y.Twiss.alpha=twiss(3);
      I.y.Twiss.beta=twiss(4);
      I.x.Twiss.eta=twiss(5);
      I.x.Twiss.etap=twiss(6);
      I.y.Twiss.eta=twiss(7);
      I.y.Twiss.etap=twiss(8);
      obj.Initial=I;
    end
  end
  methods(Access=protected)
    function obj=getDesignTwiss(obj)
      % getDesignTwiss() - Fill designTwiss property
      obj.designTwiss.beta_x=obj.modelTwiss.betax(obj.matchPoint);
      obj.designTwiss.beta_y=obj.modelTwiss.betay(obj.matchPoint);
      obj.designTwiss.alpha_x=obj.modelTwiss.alphax(obj.matchPoint);
      obj.designTwiss.alpha_y=obj.modelTwiss.alphay(obj.matchPoint);
      obj.designTwiss.eta_x=obj.modelTwiss.etax(obj.matchPoint);
      obj.designTwiss.eta_y=obj.modelTwiss.etay(obj.matchPoint);
      obj.designTwiss.etap_x=obj.modelTwiss.etapx(obj.matchPoint);
      obj.designTwiss.etap_y=obj.modelTwiss.etapy(obj.matchPoint);
    end
  end
  methods(Static)
    function handlePropEvents(src,evnt)
      obj=evnt.AffectedObject;
      switch src.Name % switch on the property name
        case 'matchData'
          obj.getInitial;
        case 'matchPoint'
          obj.getDesignTwiss;
      end
    end
    function ret=matchFun(x,Tx,Ty,psinds,mT,i1,i2,cp)
      % ret=matchFun(x) - objective function for matching
      global PS
      persistent T_keep
      if isequal(x,'GetData')
        ret=T_keep;
        return
      end
      for ips=1:length(psinds)
        PS(psinds(ips)).Ampl=x(ips);
      end
      [stat,T]=GetTwiss(i1,i2,Tx,Ty);
      if stat{1}~=1
        ret=ones(1,sum(cp)).*1e6;
        T_keep=nan(1,8);
      else
        ret=[T.alphax(end)-mT.alpha_x T.betax(end)-mT.beta_x T.alphay(end)-mT.alpha_y ...
          T.betay(end)-mT.beta_y T.etax(end)-mT.eta_x T.etapx(end)-mT.etap_x ...
          T.etay(end)-mT.eta_y T.etapy(end)-mT.etap_y];
        ret=ret(cp);
        T_keep=T;
      end
    end
    function ret=matchFun_getInitial(x,data,ip,mp)
      % ret=matchFun_getInitial(x,data,ip,mo) - objective function for
      % matching initial twiss conditions given data at match point
      % data.alpha_x etc defines twiss at match point
      % ip is initial BEAMLINE index
      % mp is BEAMLINE index at match location
      Tx.alpha=x(1);
      Tx.beta=x(2);
      Ty.alpha=x(3);
      Ty.beta=x(4);
      Tx.eta=x(5);
      Tx.etap=x(6);
      Ty.eta=x(7);
      Ty.etap=x(8);
      Tx.nu=0; Ty.nu=0;
      [stat,T]=GetTwiss(ip,mp,Tx,Ty);
      if stat{1}~=1
        ret=ones(1,8).*1e6;
      else
        ret=[T.alphax(end)-data.alpha_x T.betax(end)-data.beta_x T.alphay(end)-data.alpha_y ...
          T.betay(end)-data.beta_y T.etax(end)-data.eta_x T.etapx(end)-data.etap_x ...
          T.etay(end)-data.eta_y T.etapy(end)-data.etap_y];
      end
    end
  end
end


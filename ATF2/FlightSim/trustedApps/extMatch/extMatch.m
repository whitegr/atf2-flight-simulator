function [M]=extMatch( )
%extMatch - Front-end function for EXT rematching

% First instantiate matching object
% - defines q1-q9 as matching quads and OTR0X as match destination by
% default
M=extMatchClass;

% Expected Twiss parameters at OTR0X
dT=M.designTwiss;

% Load latest OTR measurement data
M.getOtrData;

% Show Twiss
eT=M.matchData;
fprintf('Measured (error) Twiss parameters at OTR0X: AX=%.2f(%.2f) BX=%.2f(%.2f) AY=%.2f(%.2f) BY=%.2f(%.2f)\n',...
  eT.alpha_x,dT.alpha_x,eT.beta_x,dT.beta_x,eT.alpha_y,dT.alpha_y,eT.beta_y,dT.beta_y)
fprintf('Measured (error) Dispersion parameters at OTR0X: DX=%.2f(%.2f) DPX=%.2f(%.2f) DY=%.2f(%.2f) DPY=%.2f(%.2f)\n',...
  eT.eta_x,dT.eta_x,eT.etap_x,dT.etap_x,eT.eta_y,dT.eta_y,eT.etap_y,dT.etap_y)

% Rematch QF1X-QF9X optics to compensate for error
M.doMatch;
disp('Re-matched PS vals:')
display(M.psMatchVals)
disp('Present PS currents:')
display(M.psValsI)
disp('Re-matched PS currents:')
display(M.psMatchValsI)

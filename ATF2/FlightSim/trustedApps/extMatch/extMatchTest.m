function extMatchTest()
% Test EXT matching class
global PS BEAMLINE

% First instantiate matching object
% - defines q1-q9 as matching quads and OTR0X as match destination by
% default
M=extMatchClass;

% Expected Twiss parameters at OTR0X
dT=M.designTwiss;

% Match to something else
% dT.beta_x=10;
% dT.beta_y=20;
% M.designTwiss=dT;
iqf1=findcells(BEAMLINE,'Name','QD10X'); qf1ps=BEAMLINE{iqf1(1)}.PS;
PS(qf1ps).Ampl=1.2;

% Get resulting Twiss match error
I=M.Initial;
[~,T]=GetTwiss(M.iInitial,M.matchPoint,I.x.Twiss,I.y.Twiss);
fprintf('Measured (error) Twiss parameters at OTR0X: AX=%.2f(%.2f) BX=%.2f(%.2f) AY=%.2f(%.2f) BY=%.2f(%.2f)\n',...
  T.alphax(end),dT.alpha_x,T.betax(end),dT.beta_x,T.alphay(end),dT.alpha_y,T.betay(end),dT.beta_y)
fprintf('Measured (error) Dispersion parameters at OTR0X: DX=%.2f(%.2f) DPX=%.2f(%.2f) DY=%.2f(%.2f) DPY=%.2f(%.2f)\n',...
  T.etax(end),dT.eta_x,T.etapx(end),dT.etap_x,T.etay(end),dT.eta_y,T.etapy(end),dT.etap_y)

% Load the above as measurements into the matching object
matchData.NEmit_x=I.x.NEmit;
matchData.NEmit_y=I.y.NEmit;
matchData.beta_x=T.betax(end);
matchData.beta_y=T.betay(end);
matchData.alpha_x=T.alphax(end);
matchData.alpha_y=T.alphay(end);
matchData.eta_x=T.etax(end);
matchData.etap_x=T.etapx(end);
matchData.eta_y=T.etay(end);
matchData.etap_y=T.etapy(end);
M.matchData=matchData;

% Rematch QF1X-QF9X optics to compensate for error
M.doMatch;
disp('Re-matched PS vals:')
display(M.psMatchVals)
disp('Present PS currents:')
display(M.psValsI)
disp('Re-matched PS currents:')
display(M.psMatchValsI)

% Set matched PS values and check design Twiss values restored
M.setPS;
[~,T]=GetTwiss(M.iInitial,M.matchPoint,I.x.Twiss,I.y.Twiss);
fprintf('Measured (corrected) Twiss parameters at OTR0X: AX=%.2f(%.2f) BX=%.2f(%.2f) AY=%.2f(%.2f) BY=%.2f(%.2f)\n',...
  T.alphax(end),dT.alpha_x,T.betax(end),dT.beta_x,T.alphay(end),dT.alpha_y,T.betay(end),dT.beta_y)
fprintf('Measured (corrected) Dispersion parameters at OTR0X: DX=%.2f(%.2f) DPX=%.2f(%.2f) DY=%.2f(%.2f) DPY=%.2f(%.2f)\n',...
  T.etax(end),dT.eta_x,T.etapx(end),dT.etap_x,T.etay(end),dT.eta_y,T.etapy(end),dT.etap_y)
psvals=M.matchQuadPS;
for ips=1:length(psvals)
  PS(psvals(ips)).Ampl=1;
  PS(psvals(ips)).SetPt=1;
end
PS(qf1ps).Ampl=1;
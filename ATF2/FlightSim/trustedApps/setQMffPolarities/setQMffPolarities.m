function varargout = setQMffPolarities(varargin)
% SETQMFFPOLARITIES M-file for setQMffPolarities.fig
%      SETQMFFPOLARITIES, by itself, creates a new SETQMFFPOLARITIES or raises the existing
%      singleton*.
%
%      H = SETQMFFPOLARITIES returns the handle to a new SETQMFFPOLARITIES or the handle to
%      the existing singleton*.
%
%      SETQMFFPOLARITIES('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in SETQMFFPOLARITIES.M with the given input arguments.
%
%      SETQMFFPOLARITIES('Property','Value',...) creates a new SETQMFFPOLARITIES or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before setQMffPolarities_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to setQMffPolarities_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help setQMffPolarities

% Last Modified by GUIDE v2.5 27-May-2009 23:34:47

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @setQMffPolarities_OpeningFcn, ...
                   'gui_OutputFcn',  @setQMffPolarities_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before setQMffPolarities is made visible.
function setQMffPolarities_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to setQMffPolarities (see VARARGIN)

% Choose default command line output for setQMffPolarities
handles.output = handles;

% Update handles structure
guidata(hObject, handles);

% Refresh data from EPICS
pushbutton3_Callback(handles.pushbutton3, eventdata, handles)

% UIWAIT makes setQMffPolarities wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = setQMffPolarities_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Exit function
function pushbutton1_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
guiCloseFn('hwSettings',handles);


% --- Executes when user attempts to close figure1.
function figure1_CloseRequestFcn(hObject, eventdata, handles)
% hObject    handle to figure1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

guiCloseFn('hwSettings',handles);


% --- Write to EPICS and change Model B field
function pushbutton2_Callback(hObject, eventdata, handles) %#ok<*INUSL,*DEFNU>
% hObject    handle to pushbutton2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global BEAMLINE FL
try
  names={'QM16FF' 'QM15FF' 'QM14FF' 'QM13FF' 'QM12FF' 'QM11FF'};
  but=[ 3 4; 1 2; 5 6; 7 8; 11 12; 9 10];
  for imag=1:length(names)
    ele=findcells(BEAMLINE,'Name',names{imag});
    if get(handles.(['radiobutton',num2str(but(imag,1))]),'Value')
      pval=1;
    elseif get(handles.(['radiobutton',num2str(but(imag,2))]),'Value')
      pval=-1;
    else
      pval=0;
    end
    lcaPut(FL.HwInfo.PS(BEAMLINE{ele(1)}.PS).polarity,pval);
    for iele=ele
      if isfield(BEAMLINE{iele},'B') && pval~=0
        BEAMLINE{iele}.B=abs(BEAMLINE{iele}.B).*pval;
      end
    end
  end
  beep
catch
  errordlg(lasterr,'Error setting EPICS values');
end

% --- Refresh data from EPICS
function pushbutton3_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global BEAMLINE FL

try
  names={'QM16FF' 'QM15FF' 'QM14FF' 'QM13FF' 'QM12FF' 'QM11FF'};
  but=[ 3 4; 1 2; 5 6; 7 8; 11 12; 9 10];
  for imag=1:length(names)
    ele=findcells(BEAMLINE,'Name',names{imag});
    pval=lcaGet(FL.HwInfo.PS(BEAMLINE{ele(1)}.PS).polarity);
    if pval==1
      set(handles.(['radiobutton',num2str(but(imag,1))]),'Value',1)
    elseif pval==-1 
      set(handles.(['radiobutton',num2str(but(imag,2))]),'Value',1)
    else
      set(handles.(['radiobutton',num2str(but(imag,1))]),'Value',0)
      set(handles.(['radiobutton',num2str(but(imag,2))]),'Value',0)
    end
    for iele=ele
      if isfield(BEAMLINE{iele},'B') && pval~=0
        BEAMLINE{iele}.B=abs(BEAMLINE{iele}.B).*pval;
      end
    end
  end
  beep
catch
  errordlg(lasterr,'Error getting EPICS values');
end

function varargout = extDispCalc(varargin)
% EXTDISPCALC M-file for extDispCalc.fig
%      EXTDISPCALC, by itself, creates a new EXTDISPCALC or raises the existing
%      singleton*.
%
%      H = EXTDISPCALC returns the handle to a new EXTDISPCALC or the handle to
%      the existing singleton*.
%
%      EXTDISPCALC('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in EXTDISPCALC.M with the given input arguments.
%
%      EXTDISPCALC('Property','Value',...) creates a new EXTDISPCALC or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before extDispCalc_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to extDispCalc_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help extDispCalc

% Last Modified by GUIDE v2.5 16-May-2008 01:43:40

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @extDispCalc_OpeningFcn, ...
                   'gui_OutputFcn',  @extDispCalc_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before extDispCalc is made visible.
function extDispCalc_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to extDispCalc (see VARARGIN)
global FL BEAMLINE INSTR

% Choose default command line output for extDispCalc
handles.output = handles;

% Update handles structure
guidata(hObject, handles);

% Get bpm names for EXT
nbpm=0;
for ibpm=find(cellfun(@(x) x.Index>FL.SimModel.extStart,INSTR))
  nbpm=nbpm+1;
  UserData{1}{nbpm}=BEAMLINE{INSTR{ibpm}.Index}.Name;
end % for ibpm

% Fill popupmenu with names
set(handles.popupmenu2,'String',UserData{1});

% Fill userdata field
set(handles.figure1,'UserData',UserData);

% Update twiss parameters from online model
[stat,FL.SimModel.Twiss] = GetTwiss(1,length(BEAMLINE),FL.SimModel.Initial.x.Twiss,FL.SimModel.Initial.y.Twiss) ;
if stat{1}~=1
  errordlg(['Error updating model Twiss parameters:' stat{2}],'extDispCalc error');
end % if err

% --- Outputs from this function are returned to the command line.
function varargout = extDispCalc_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on button press in pushbutton2.
function pushbutton2_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

guiCloseFn('extDispCalc',handles);


function edit1_Callback(hObject, eventdata, handles)
% hObject    handle to edit1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit1 as text
%        str2double(get(hObject,'String')) returns contents of edit1 as a double

val=str2double(get(hObject,'String'));
if isnan(val) || ~isnumeric(val)
  errordlg('Must pass number in Npulse field','extDispCalc Error');
  return
end % if not num
[stat,output]=FlHwUpdate('buffersize');
if stat{1}~=1
  errordlg('Not able to receive bpm buffer size','FlHwUpdate error');
  return
end % if err
set(hObject,'String',num2str(min(output,val)));

% --- Executes during object creation, after setting all properties.
function edit1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in pushbutton1.
function pushbutton1_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global BEAMLINE FL

errHandler('reset',handles);drawnow('expose');
UserData=get(handles.figure1,'UserData');

if length(UserData)>=2 && ~isempty(UserData{2})
  [stat, Dmdl] = extDispFit(UserData{2}{1});
  if stat{1}~=1; errHandler(['EXT disp fitting routine: ',stat{2}],handles); return; end;
else
  errHandler('Need to perform dispersion calculation first',handles);
  return
end % if run disp calc

UserData{3}=Dmdl;

% plot fitted functions
try
  sp=subplot(1,1,1,'Parent',handles.uipanel3);
  svals=arrayfun(@(x) BEAMLINE{x}.S,FL.SimModel.extStart+1:length(BEAMLINE))-BEAMLINE{FL.SimModel.extStart+1}.S;
  hold on
  plot(sp,svals,Dmdl(1,:),'r');
  plot(sp,svals,Dmdl(2,:),'b');
  hold off
  axis tight
catch
  errHandler(['Error plotting fitted dispersion values: ',lasterr],handles)
  return
end % try/catch

set(handles.figure1,'UserData',UserData);

% --- Executes on button press in pushbutton3.
function pushbutton3_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global INSTR BEAMLINE FL

UserData=get(handles.figure1,'UserData');

% Run Disp calculationn routine
errHandler('reset',handles);
nave=str2double(get(handles.edit2,'String'));
if isnan(nave) || ~isnumeric(nave)
  errHandler('Not a number in ave box');
  return
end % ave check
Dx=[];Dy=[];
for iave=1:nave
  set(handles.text4,'String',sprintf('Running Dispersion calculation routine, calc %d of %d...',iave,nave)); drawnow('expose');
  [stat, D, D_err, CHI2, dEmean, xbpm_e, ybpm_e] = bpmDispCalc(round(str2double(get(handles.edit1,'String'))));
  if stat{1}~=1; errHandler(['Error In Dispersion calculation routine: ',stat{2}],handles); return; end;
  Dx=[Dx;D(1,:)]; Dy=[Dy;D(2,:)];
end % for iave
D=[mean(Dx); mean(Dy)];
D_err=[std(Dx); std(Dy)];
% Put data in UserData
UserData{2}={D, D_err, CHI2, dEmean, xbpm_e, ybpm_e};

% Get INSTR s vals
instlist=cellfun(@(x) x.Index,INSTR);
svals=arrayfun(@(x) BEAMLINE{x}.S,instlist(instlist>FL.SimModel.extStart));
if length(svals)~=length(D(1,:))
  errHandler('length mismatch between EXT INSTR model and dispersion values',handles);
  return
end % check lengths match

set(handles.text4,'String','Plotting results...'); drawnow('expose');

% Plot dispersion values
sp=subplot(1,1,1,'Parent',handles.uipanel3);
errorbar(sp,svals,D(1,:)*1e3,D_err(1,:)*1e3,'r*-')
hold on
errorbar(sp,svals,D(2,:)*1e3,D_err(2,:)*1e3,'b*-')
hold off
legend('x','y');

% Populate dispersion and chi2 listboxes
if length(D(1,:))~=length(UserData{1})
  errHandler('Mismatch between number of expected bpms and those returned from bpmDispCalc',handles);
  return;
end % if err
lb1str={};
lb2str={};
for ibpm=1:length(UserData{1})
  lb1str{end+1}=[UserData{1}{ibpm} ' (x): ' sprintf('%.3e',D(1,ibpm))];
  lb1str{end+1}=[UserData{1}{ibpm} ' (y): ' sprintf('%.3e',D(2,ibpm))];
  lb2str{end+1}=[UserData{1}{ibpm} ' (x): ' sprintf('%.3e',CHI2(1,ibpm))];
  lb2str{end+1}=[UserData{1}{ibpm} ' (y): ' sprintf('%.3e',CHI2(2,ibpm))];
end
set(handles.listbox1,'String',lb1str);
set(handles.listbox2,'String',lb2str);

set(handles.text4,'String','Completed DispCalc routine'); drawnow('expose');

% Write-out UserData
set(handles.figure1,'UserData',UserData);

% --- Executes on button press in togglebutton1.
function togglebutton1_Callback(hObject, eventdata, handles)
% hObject    handle to togglebutton1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of togglebutton1
global FL
if ~isfield(FL,'t_extDispCalc')
  FL.t_extDispCalc=timer('StartDelay',1,'Period',1/FL.accRate,...
   'ExecutionMode','FixedRate','BusyMode','drop');
  FL.t_extDispCalc.TimerFcn = ['extDispCalc(''pushbutton3_Callback'',',num2str(hObject,100),',[],',num2str(handles,100),')'];
end % if need to create timer object
if get(hObject,'Value')
  if ~isequal(FL.t_extDispCalc.running,'on')
    start(FL.t_extDispCalc);
  end % if not already running
else
  stop(FL.t_extDispCalc);
end % if autorun toggled


% --- Executes on selection change in listbox1.
function listbox1_Callback(hObject, eventdata, handles)
% hObject    handle to listbox1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = get(hObject,'String') returns listbox1 contents as cell array
%        contents{get(hObject,'Value')} returns selected item from listbox1


% --- Executes during object creation, after setting all properties.
function listbox1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to listbox1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: listbox controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in listbox2.
function listbox2_Callback(hObject, eventdata, handles)
% hObject    handle to listbox2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = get(hObject,'String') returns listbox2 contents as cell array
%        contents{get(hObject,'Value')} returns selected item from listbox2


% --- Executes during object creation, after setting all properties.
function listbox2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to listbox2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: listbox controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in pushbutton5.
function pushbutton5_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton5 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global FL BEAMLINE

% Update twiss parameters from online model
[stat,FL.SimModel.Twiss] = GetTwiss(1,length(BEAMLINE),FL.SimModel.Initial.x.Twiss,FL.SimModel.Initial.y.Twiss) ;
if stat{1}~=1
  errordlg(['Error updating model Twiss parameters:' stat{2}],'extDispCalc error');
end % if err

% --- Executes on selection change in popupmenu2.
function popupmenu2_Callback(hObject, eventdata, handles)
% hObject    handle to popupmenu2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = get(hObject,'String') returns popupmenu2 contents as cell array
%        contents{get(hObject,'Value')} returns selected item from popupmenu2


% --- Executes during object creation, after setting all properties.
function popupmenu2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to popupmenu2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in pushbutton6.
function pushbutton6_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton6 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

UserData=get(handles.figure1,'UserData');

% get BPM to plot (x & y)
bpm=get(handles.popupmenu2,'Value');

Px=polyfit(UserData{2}{4}*1e3,UserData{2}{5}(:,bpm)'*1e3,1);
Py=polyfit(UserData{2}{4}*1e3,UserData{2}{6}(:,bpm)'*1e3,1);

% plot energy correlations
sp=subplot(1,2,1,'Parent',handles.uipanel3);
plot(sp,UserData{2}{4}*1e3,UserData{2}{5}(:,bpm)*1e3,'r.')
hold on
plot(sp,UserData{2}{4}*1e3,polyval(Px,UserData{2}{4}*1e3),'r')
hold off
sp=subplot(1,2,2,'Parent',handles.uipanel3);
plot(sp,UserData{2}{4}*1e3,UserData{2}{6}(:,bpm)*1e3,'b.')
hold on
plot(sp,UserData{2}{4}*1e3,polyval(Py,UserData{2}{4}*1e3),'b')
hold off

% --- Executes on button press in pushbutton7.
function pushbutton7_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton7 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global BEAMLINE FL INSTR

UserData=get(handles.figure1,'UserData');

% Get INSTR s vals
instlist=cellfun(@(x) x.Index,INSTR);
svals=arrayfun(@(x) BEAMLINE{x}.S,instlist(instlist>FL.SimModel.extStart));
if length(svals)~=length(UserData{1}(1,:))
  errordlg('length mismatch between EXT INSTR model and dispersion values','dispCalc GUI error')
  return
end % check lengths match

sp=subplot(1,1,1,'Parent',handles.uipanel3);
% Plot dispersion values
errorbar(sp,svals,UserData{2}{1}(1,:)*1e3,UserData{2}{2}(1,:)*1e3,'r*-')
hold on
errorbar(sp,svals,UserData{2}{1}(2,:)*1e3,UserData{2}{2}(2,:)*1e3,'b*-')
hold off
legend('x','y');
% --- Executes on key press with focus on togglebutton1 and none of its controls.
function togglebutton1_KeyPressFcn(hObject, eventdata, handles)
% hObject    handle to togglebutton1 (see GCBO)
% eventdata  structure with the following fields (see UICONTROL)
%	Key: name of the key that was pressed, in lower case
%	Character: character interpretation of the key(s) that was pressed
%	Modifier: name(s) of the modifier key(s) (i.e., control, shift) pressed
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on key press with focus on pushbutton3 and none of its controls.
function pushbutton3_KeyPressFcn(hObject, eventdata, handles)
% hObject    handle to pushbutton3 (see GCBO)
% eventdata  structure with the following fields (see UICONTROL)
%	Key: name of the key that was pressed, in lower case
%	Character: character interpretation of the key(s) that was pressed
%	Modifier: name(s) of the modifier key(s) (i.e., control, shift) pressed
% handles    structure with handles and user data (see GUIDATA)


% --- If Enable == 'on', executes on mouse press in 5 pixel border.
% --- Otherwise, executes on mouse press in 5 pixel border or over pushbutton1.
function pushbutton1_ButtonDownFcn(hObject, eventdata, handles)
% hObject    handle to pushbutton1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes when user attempts to close figure1.
function figure1_CloseRequestFcn(hObject, eventdata, handles)
% hObject    handle to figure1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

try
  guiCloseFn('extDispCalc',handles);
catch
  delete(hObject)
end


% --- Executes on button press in pushbutton8.
function pushbutton8_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton8 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

[stat,out]=FlHwUpdate('buffersize');
if stat{1}~=1; errHandler(['Error getting buffer data: ',stat{2}],handles); return; end;
set(handles.edit1,'String',num2str(out));

% --- Error Handler----
function errHandler(str,handles)

if isequal(str,'reset')
  set(handles.text4,'BackgroundColor','White');
  set(handles.text4,'ForegroundColor','Black');
  set(handles.text4,'String',[]);
else
  set(handles.text4,'String',str);
  set(handles.text4,'BackgroundColor','Red')
  set(handles.text4,'ForegroundColor','White')
  return
end



function edit2_Callback(hObject, eventdata, handles)
% hObject    handle to edit2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit2 as text
%        str2double(get(hObject,'String')) returns contents of edit2 as a double


% --- Executes during object creation, after setting all properties.
function edit2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



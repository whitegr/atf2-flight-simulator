function [stat, Dmdl] = extDispFit(Dbpm)
% [stat, Dmdl] = extDispFit(Dbpm)
% Fit dispersion function to EXT given bpm dispersion values
global BEAMLINE FL INSTR

stat{1}=1; Dmdl=[]; %#ok<NASGU>

% Extraction Line entrance twiss
xTwiss.beta = FL.SimModel.Twiss.betax(FL.SimModel.extStart);
xTwiss.alpha = FL.SimModel.Twiss.alphax(FL.SimModel.extStart);
xTwiss.eta = FL.SimModel.Twiss.etax(FL.SimModel.extStart);
xTwiss.etap = FL.SimModel.Twiss.etapx(FL.SimModel.extStart);
xTwiss.nu = FL.SimModel.Twiss.nux(FL.SimModel.extStart);
yTwiss.beta = FL.SimModel.Twiss.betay(FL.SimModel.extStart);
yTwiss.alpha = FL.SimModel.Twiss.alphay(FL.SimModel.extStart);
yTwiss.eta = FL.SimModel.Twiss.etay(FL.SimModel.extStart);
yTwiss.etap = FL.SimModel.Twiss.etapy(FL.SimModel.extStart);
yTwiss.nu = FL.SimModel.Twiss.nuy(FL.SimModel.extStart);

% Fit initial dispersion parameters
instlist=cellfun(@(x) x.Index,INSTR);
bpmind=instlist(instlist>FL.SimModel.extStart);
X = fminsearch(@(x) dispFit(x,FL.SimModel.extStart,max(bpmind),xTwiss,yTwiss,Dbpm,bpmind-FL.SimModel.extStart+1),...
  [xTwiss.eta xTwiss.etap yTwiss.eta yTwiss.etap]);

% Get fitted dispersion
xTwiss.eta=X(1); xTwiss.etap=X(2);
yTwiss.eta=X(3); yTwiss.etap=X(4);
[stat,twisscalc] = GetTwiss(FL.SimModel.extStart+1,length(BEAMLINE),xTwiss,yTwiss) ;
if stat{1}~=1; return; end;
Dmdl=zeros(2,length(BEAMLINE)-FL.SimModel.extStart);
Dmdl(1,:)=twisscalc.etax(2:end);
Dmdl(2,:)=twisscalc.etay(2:end);

% ---------------
function chi2 = dispFit(x,start,finish,xTwiss,yTwiss,Dbpm,bpmind)

xTwiss.eta=x(1); xTwiss.etap=x(2);
yTwiss.eta=x(3); yTwiss.etap=x(4);

[stat,twisscalc] = GetTwiss(start,finish,xTwiss,yTwiss) ;
if stat{1}~=1; chi2=1e20; return; end;

chi2 = sum((twisscalc.etax(bpmind) - Dbpm(1,:)).^2 + (twisscalc.etay(bpmind) - Dbpm(2,:)).^2) ;
function [stat, Dout, Dout_err, CHI2out, dEmean, xbpm_e, ybpm_e] = bpmDispCalc(Npulse)
% [stat, Dout, Dout_err, CHI2out, dEmean, xbpm_e, ybpm_e] = bpmDispCalc(Npulse)
% Estimate linear dispersion in extraction line bpms from energy jitter
% measured in the ring arcs
% -----------------------------------------
% Inputs:
%         Npulse: Number of buffered bpm readings to use (should be small
%         enough such that the mean energy hasn't drifted much during this
%         time)
% Outputs:
%         stat: Lucretia status
%         Dout: estimated linear dispersion in extraction line bpms [X; Y]
%         Dout_err: estimated rms error of above
%         CHI2out: sum of chi2 results from fits to all dE vs. x/y data
%         dEmean, xbpm_e, ybpm_e: raw data used in fits (dEmean taken from
%         dECalc function, x/ybpm_e= estimated bpm response to energy
%         jitter
% -----------------------------------------
% Glen White May 2008

global INSTR FL

stat{1}=1; %#ok<NASGU>
Dout=[]; Dout_err=[]; CHI2out=[]; dEmean=[]; xbpm_e=[]; ybpm_e=[];

% need ringEnergyCalc application
stat=useApp('ringEnergyCalc');
if stat{1}~=1; stat{2}=['Unable to use ringEnergyCalc app:',stat{2}]; return; end;

% Get dE vector
[stat,dEmean,dErms,bpm_rawdata]=dECalc(Npulse);
if stat{1}~=1; return; end;
% Get buffered bpm data (EXT line)
xbpm=bpm_rawdata(:,find(cellfun(@(x) x.Index>FL.SimModel.extStart,INSTR)).*3-2);
ybpm=bpm_rawdata(:,find(cellfun(@(x) x.Index>FL.SimModel.extStart,INSTR)).*3-1);

% Flag bad pulses
goodind=~isnan(dEmean);
dEmean=dEmean(goodind);
dErms=dErms(goodind); %#ok<NASGU>
xbpm=xbpm(goodind,:);
ybpm=ybpm(goodind,:);

% Do singular value decomposition
[Ux,Sx,Vtx]=svd(xbpm);
[Uy,Sy,Vty]=svd(ybpm);
Vx=Vtx'; Vy=Vty';

% Correlate modes with Energy
for imode=1:10
  [r,p] = corrcoef(dEmean,xbpm*Vx(imode,:)');
  if ~isempty(find(p<0.1, 1))
    rcorx(imode)=r(1,2);
  else
    rcorx(imode)=0;
  end % if any significant correlations
  [r,p] = corrcoef(dEmean,ybpm*Vy(imode,:)');
  if ~isempty(find(p<0.1, 1))
    rcory(imode)=r(1,2);
  else
    rcory(imode)=0;
  end % if any significant correlations
end % for imode
if ~any(rcorx) || ~any(rcory)
  stat{1}=0; stat{2}='No significant correlations found for both x and y';
  return
end % if no correlations found, return

% Get energy contribution to bpm readings using most correlated mode
Sx_new=zeros(size(Sx)); Sy_new=zeros(size(Sy));
[val xind]=max(rcorx);
[val yind]=max(rcory);
Sx_new(xind,xind)=Sx(xind,xind);
Sy_new(yind,yind)=Sy(yind,yind);
xbpm_e=Ux*Sx_new*Vx;
ybpm_e=Uy*Sy_new*Vy;

% Fit dispersion curves

bpmsize=size(xbpm_e);
for ibpm=1:bpmsize(2)
 Px=polyfit(dEmean,xbpm_e(:,ibpm)',1); qx(ibpm)=Px(1);
 Py=polyfit(dEmean,ybpm_e(:,ibpm)',1); qy(ibpm)=Py(1);
 resid_x(ibpm)=std(xbpm_e(:,ibpm)-polyval(Px,dEmean)');
 resid_y(ibpm)=std(ybpm_e(:,ibpm)-polyval(Py,dEmean)');
end % for ibpm

% Ouput data
Dout=[qx' qy']';
Dout_err=[qx'*0 qy'*0]';
CHI2out=[resid_x' resid_y']';
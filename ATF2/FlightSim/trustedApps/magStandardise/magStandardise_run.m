function stat = magStandardise_run(magNames)
% stat = magStandardise_run(magNames)
% Standardise the given list of magnets (BEAMLINE Names)
% magNames should be a cell array or single char string
global BEAMLINE FL PS
stat{1}=1;
messhan=false;
drawnow('expose');

use_pstrim=false;

% abort command flag
FL.abortcmd=false;

if isfield(FL,'Gui') && isfield(FL.Gui,'magStandardise')
  messhan=FL.Gui.magStandardise.text7; set(messhan,'ForegroundColor','black');
end
if ~iscell(magNames); magNames={magNames}; end;
if isempty(magNames); return; end;
try
  % get PS inds corresponding to magnet names
  ps_ind=zeros(size(magNames));
  for icell=1:length(magNames)
    b_ind=findcells(BEAMLINE,'Name',magNames{icell});
    if isempty(b_ind); error('unknown magnet name: %s',magNames{icell}); end;
    if ~isfield(BEAMLINE{b_ind(1)},'PS'); error('No PS element for magnet name: %s',magNames{icell}); end;
    ps_ind(icell)=BEAMLINE{b_ind(1)}.PS;
  end
  % get pv names for standardisation parameters
  for ips=1:length(ps_ind)
    if ~isfield(FL.HwInfo.PS(ps_ind(ips)),'standardise') || isempty(FL.HwInfo.PS(ps_ind(ips)).standardise)
      error('No standardisation entries for PS: %d',ps_ind(ips));
    end
    nCycles.name{ips,1}=FL.HwInfo.PS(ps_ind(ips)).standardise.nCycles;
    iLow.name{ips,1}=FL.HwInfo.PS(ps_ind(ips)).standardise.iLow;
    iHigh.name{ips,1}=FL.HwInfo.PS(ps_ind(ips)).standardise.iHigh;
    waitLow.name{ips,1}=FL.HwInfo.PS(ps_ind(ips)).standardise.waitLow;
    waitHigh.name{ips,1}=FL.HwInfo.PS(ps_ind(ips)).standardise.waitHigh;
    rate.name{ips,1}=FL.HwInfo.PS(ps_ind(ips)).standardise.rate;
    settingRate.name{ips,1}=FL.HwInfo.PS(ps_ind(ips)).standardise.settingRate;
    rampRate.name{ips,1}=FL.HwInfo.PS(ps_ind(ips)).rampRate;
    pvname{ips,1}=FL.HwInfo.PS(ps_ind(ips)).pvname{2}{1};
    pvname_get{ips,1}=FL.HwInfo.PS(ps_ind(ips)).pvname{1}{1};
    if isfield(FL.HwInfo.PS(ps_ind(ips)),'postCommand') && ~isempty(FL.HwInfo.PS(ps_ind(ips)).postCommand)
      pvname_put.pv{ips,1}=FL.HwInfo.PS(ps_ind(ips)).postCommand{2}{1};
      pvname_put.val(ips,1)=FL.HwInfo.PS(ps_ind(ips)).postCommand{2}{2};
    end
  end
  % get parameter values from EPICS database
  nCycles.val=lcaGet(nCycles.name);
  iHigh.val=lcaGet(iHigh.name);
  iHigh.current=iHigh.val;
  iLow.val=lcaGet(iLow.name);
  iLow.current=iLow.val;
  for ips=1:length(ps_ind)
    if length(FL.HwInfo.PS(ps_ind(ips)).conv)==1
      iHigh.val(ips)=iHigh.val(ips)*FL.HwInfo.PS(ps_ind(ips)).conv/abs(BEAMLINE{PS(ps_ind(ips)).Element(1)}.B(1));
      iLow.val(ips)=iLow.val(ips)*FL.HwInfo.PS(ps_ind(ips)).conv/abs(BEAMLINE{PS(ps_ind(ips)).Element(1)}.B(1));
    else
      iHigh.val(ips)=interp1(FL.HwInfo.PS(ps_ind(ips)).conv(1,:),FL.HwInfo.PS(ps_ind(ips)).conv(2,:),iHigh.val(ips),'linear')/abs(BEAMLINE{PS(ps_ind(ips)).Element(1)}.B(1));
      iLow.val(ips)=interp1(FL.HwInfo.PS(ps_ind(ips)).conv(1,:),FL.HwInfo.PS(ps_ind(ips)).conv(2,:),iLow.val(ips),'linear')/abs(BEAMLINE{PS(ps_ind(ips)).Element(1)}.B(1));
    end
    % For matching quads make sure get sign of current correct
    for iips=PS(ps_ind(ips)).Element
      if strcmp(BEAMLINE{iips}.Name,'QM16FF') || strcmp(BEAMLINE{iips}.Name,'QM15FF') || strcmp(BEAMLINE{iips}.Name,'QM14FF') ||...
          strcmp(BEAMLINE{iips}.Name,'QM13FF') || strcmp(BEAMLINE{iips}.Name,'QM12FF') || strcmp(BEAMLINE{iips}.Name,'QM11FF')
        iHigh.val(ips)=abs(iHigh.val(ips)).*sign(BEAMLINE{iips}.B);
        iLow.val(ips)=abs(iLow.val(ips)).*sign(BEAMLINE{iips}.B);
        iHigh.current(ips)=abs(iHigh.current(ips)).*sign(BEAMLINE{iips}.B);
        iLow.current(ips)=abs(iLow.current(ips)).*sign(BEAMLINE{iips}.B);
      end
    end
  end
  waitLow.val=lcaGet(waitLow.name);
  waitHigh.val=lcaGet(waitHigh.name);
  rate.val=lcaGet(rate.name);
  settingRate.val=lcaGet(settingRate.name);
  if any(any(isnan([nCycles.val iHigh.val iLow.val waitLow.val waitHigh.val rate.val settingRate.val])))
    error('Error getting standardisation settings from EPICS- probably one of the desired current settings is beyond the lookup range')
  end
  % Standardisation procedure: set (low, set high)*nCycles, waiting
  % waitHigh/waitLow at each) then set Low and set to initial
  ps_initial=[PS(ps_ind).Ampl]; iCycle=0; numCycles=max(nCycles.val);
  i_initial=lcaGet(pvname_get);
  % Set ramp rate to standardisation ramp rate
  lcaPut(rampRate.name,rate.val)
  while any(nCycles.val>0)
    iCycle=iCycle+1;
    % --- set low
    if abortCheck(ps_initial,ps_ind,rampRate,settingRate,use_pstrim,pvname,i_initial,pvname_put); return; end;
    if messhan
      set(messhan,'String',['Standardisation cycle # ',num2str(iCycle),' of ',...
        num2str(numCycles),': Setting LOW current val(s)']);
      drawnow('expose');
    end
    ips_list=[];
    for ips=1:length(ps_ind)
      if nCycles.val(ips)>0
        PS(ps_ind(ips)).SetPt=iLow.val(ips);
        ips_list=[ips_list ips];
      end
    end
    if use_pstrim
      stat=PSTrim(ps_ind,1); if stat{1}~=1; errordlg(stat{2},'Standardisation error'); end; %#ok<UNRCH>
    else
      lcaPut({pvname{ips_list}}',iLow.current(ips_list,1));
      if ~isempty(pvname_put.pv); lcaPut({pvname_put.pv{ips_list}}',pvname_put.val(ips_list,1)); end;
    end
    pause(max(iHigh.val)/min(rate.val)) % Wait until trim has occurred plus wait time
    pause(max(waitLow.val));
    % --- set high
    if abortCheck(ps_initial,ps_ind,rampRate,settingRate,use_pstrim,pvname,i_initial,pvname_put); return; end;
    if messhan
      set(messhan,'String',['Standardisation cycle # ',num2str(iCycle),' of ',...
        num2str(numCycles),': Setting HIGH current val(s)']);
      drawnow('expose');
    end
    ips_list=[];
    for ips=1:length(ps_ind)
      if nCycles.val(ips)>0
        PS(ps_ind(ips)).SetPt=iHigh.val(ips);
        ips_list=[ips_list ips];
      end
    end
    if use_pstrim
      stat=PSTrim(ps_ind,1); if stat{1}~=1; errordlg(stat{2},'Standardisation error'); end; %#ok<UNRCH>
    else
      lcaPut({pvname{ips_list}}',iHigh.current(ips_list,1));
      if ~isempty(pvname_put.pv); lcaPut({pvname_put.pv{ips_list}}',pvname_put.val(ips_list,1)); end;
    end
    pause(max(iHigh.val)/min(rate.val)) % Wait until trim has occurred plus wait time
    pause(max(waitHigh.val))
    % decrement nCycles
    nCycles.val=nCycles.val-1;
    % --- set low and then initial if nCycles complete
    if abortCheck(ps_initial,ps_ind,rampRate,settingRate,use_pstrim,pvname,i_initial,pvname_put); return; end;
    ps_list=[]; ips_list=[];
    for ips=1:length(ps_ind)
      if nCycles.val(ips)==0
        PS(ps_ind(ips)).SetPt=iLow.val(ips);
        ps_list=[ps_list ps_ind(ips)];
        ips_list=[ips_list ips];
      end
    end
    if messhan && ~isempty(ps_list)
      set(messhan,'String',['Standardisation cycle # ',num2str(iCycle),' of ',...
        num2str(numCycles),': Restoring [setting LOW] PS(s): ',num2str(ps_list)]);
      drawnow('expose');
    end
    if ~isempty(ps_list)
      if use_pstrim
        stat=PSTrim(ps_list,1); if stat{1}~=1; errordlg(stat{2},'Standardisation error'); end; %#ok<UNRCH>
      else
        lcaPut({pvname{ips_list,1}}',iLow.current(ips_list,1));
        if ~isempty({pvname_put.pv{ips_list}}); lcaPut({pvname_put.pv{ips_list}}',pvname_put.val(ips_list,1)); end;
      end
      pause(max(iHigh.val)/min(rate.val)) % Wait until trim has occurred plus wait time
      pause(max(waitLow.val))
    end
    ps_list=[]; ips_list=[];
    if abortCheck(ps_initial,ps_ind,rampRate,settingRate,use_pstrim,pvname,i_initial,pvname_put); return; end;
    for ips=1:length(ps_ind)
      if nCycles.val(ips)==0
        PS(ps_ind(ips)).SetPt=ps_initial(ips);
        ps_list=[ps_list ps_ind(ips)];
        ips_list=[ips_list ips];
        % Set ramp rate to setting ramp rate
        lcaPut(rampRate.name{ips},settingRate.val(ips));
      end
    end
    if messhan && ~isempty(ps_list)
      set(messhan,'String',['Standardisation cycle # ',num2str(iCycle),' of ',...
        num2str(numCycles),': Restoring [setting back to initial value(s)] PS(s): ',num2str(ps_list)]);
      drawnow('expose');
    end
    if ~isempty(ps_list)
      if use_pstrim
        stat=PSTrim(ps_list,1); if stat{1}~=1; errordlg(stat{2},'Standardisation error'); end; %#ok<UNRCH>
      else
        lcaPut({pvname{ips_list,1}}',i_initial(ips_list,1));
        if ~isempty({pvname_put.pv{ips_list,1}}); lcaPut({pvname_put.pv{ips_list}}',pvname_put.val(ips_list,1)); end;
      end
      pause(max(iHigh.val)/min(settingRate.val)) % Wait until trim has occurred plus wait time
    end
  end
catch
  stat{1}=-1;
  stat{2}=['magStandardise_run error: ',lasterr];
end

function hasAborted=abortCheck(ps_initial,ps_ind,rampRate,settingRate,use_pstrim,pvname,i_initial,pvname_put)
global FL PS
if FL.abortcmd || strcmp(get(FL.Gui.magStandardise.edit1,'String'),'ABORT')
  hasAborted=true;
  for ips=1:length(ps_ind)
    PS(ps_ind(ips)).SetPt=ps_initial(ips);
    % Set ramp rate to setting ramp rate
    lcaPut(rampRate.name{ips},settingRate.val(ips));
  end
  if use_pstrim
    stat=PSTrim(ps_ind,1); if stat{1}~=1; errordlg(stat{2},'Standardisation error'); end;
  else
    lcaPut(pvname,i_initial);
    if ~isempty(pvname_put.pv); lcaPut(pvname_put.pv,pvname_put.val); end;
  end
  if isfield(FL,'Gui') && isfield(FL.Gui,'magStandardise')
    messhan=FL.Gui.magStandardise.text7; set(messhan,'ForegroundColor','red');
    set(messhan,'String','Standardise Aborted! Resetting magnet(s) back to initial conditions')
    drawnow('expose')
  end
  FL.abortcmd=false;
else
  hasAborted=false;
end
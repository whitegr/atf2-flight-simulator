function varargout = magStandardise(varargin)
% MAGSTANDARDISE M-file for magStandardise.fig
%      MAGSTANDARDISE, by itself, creates a new MAGSTANDARDISE or raises the existing
%      singleton*.
%
%      H = MAGSTANDARDISE returns the handle to a new MAGSTANDARDISE or the handle to
%      the existing singleton*.
%
%      MAGSTANDARDISE('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in MAGSTANDARDISE.M with the given input arguments.
%
%      MAGSTANDARDISE('Property','Value',...) creates a new MAGSTANDARDISE or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before magStandardise_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to magStandardise_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help magStandardise

% Last Modified by GUIDE v2.5 11-Dec-2011 22:22:00

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @magStandardise_OpeningFcn, ...
                   'gui_OutputFcn',  @magStandardise_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before magStandardise is made visible.
function magStandardise_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to magStandardise (see VARARGIN)

% Choose default command line output for magStandardise
handles.output = handles;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes magStandardise wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = magStandardise_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Exit function
function pushbutton1_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
guiCloseFn('magStandardise',handles);



% --- Execute Standardisation Procedure
function pushbutton5_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton5 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

if ~strcmp(questdlg('Start Standardisation Procedure?','Standardise','Yes','No','No'),'Yes')
  return
end

% Split magnet names by ; separator
magData=regexp(get(handles.edit1,'String'),';','split');
magData={magData{cellfun(@(x) ~isempty(x),magData)}};
set(handles.text7,'ForegroundColor','black');
set(handles.text7,'String','Starting Standardisation Procedure...');
stat=magStandardise_run(magData);
if stat{1}==1
  if isempty(regexp(get(handles.text7,'String'),'^Standardise Aborted','once'))
    set(handles.text7,'String','Standardisation Complete');
  end
else
  set(handles.text7,'ForegroundColor','red');
  set(handles.text7,'String',['Error in standardisation procedure: ',stat{2}]);
end

function edit2_Callback(hObject, eventdata, handles)
% hObject    handle to edit2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit2 as text
%        str2double(get(hObject,'String')) returns contents of edit2 as a double


% --- Executes during object creation, after setting all properties.
function edit2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit3_Callback(hObject, eventdata, handles)
% hObject    handle to edit3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit3 as text
%        str2double(get(hObject,'String')) returns contents of edit3 as a double


% --- Executes during object creation, after setting all properties.
function edit3_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit4_Callback(hObject, eventdata, handles)
% hObject    handle to edit4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit4 as text
%        str2double(get(hObject,'String')) returns contents of edit4 as a double


% --- Executes during object creation, after setting all properties.
function edit4_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit5_Callback(hObject, eventdata, handles)
% hObject    handle to edit5 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit5 as text
%        str2double(get(hObject,'String')) returns contents of edit5 as a double


% --- Executes during object creation, after setting all properties.
function edit5_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit5 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit6_Callback(hObject, eventdata, handles)
% hObject    handle to edit6 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit6 as text
%        str2double(get(hObject,'String')) returns contents of edit6 as a double


% --- Executes during object creation, after setting all properties.
function edit6_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit6 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit1_Callback(hObject, eventdata, handles)
% hObject    handle to edit1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit1 as text
%        str2double(get(hObject,'String')) returns contents of edit1 as a double


% --- Executes during object creation, after setting all properties.
function edit1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Launch Beamlineviewer
function pushbutton2_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global FL

FL.Gui.BeamlineViewer = BeamlineViewer;

% --- Select magnets from choices in BeamlineViewer
function pushbutton3_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global FL
if isfield(FL,'Gui') && isfield(FL.Gui,'BeamlineViewer')
  han=FL.Gui.BeamlineViewer;
  maglist=get(han.listbox1,'String');
  magsel=get(han.listbox1,'Value');
  magstr='';
  if ~isempty(magsel)
    for imag=1:length(magsel)
      magstr=[magstr regexprep(maglist{magsel(imag)},'\S+\s+\S+\s+\S+\s+(\S+)$','$1;')];
    end
    set(handles.edit1,'String',magstr);
  end
end


% --- Clear magnet setting edit window
function pushbutton4_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
set(handles.edit1,'String',[]);

% --- Executes when user attempts to close figure1.
function figure1_CloseRequestFcn(hObject, eventdata, handles)
% hObject    handle to figure1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: delete(hObject) closes the figure
guiCloseFn('magStandardise',handles);


% --- Get standardisation settings
function pushbutton6_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton6 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global FL BEAMLINE
try
  magData=regexp(get(handles.edit1,'String'),';','split');
  magData={magData{cellfun(@(x) ~isempty(x),magData)}};
  if length(magData)==1
    b_ind=findcells(BEAMLINE,'Name',magData{1});
    if isempty(b_ind); error('unknown magnet name: %s',magData{1}); end;
    if ~isfield(BEAMLINE{b_ind(1)},'PS'); error('No PS element for magnet name: %s',magData{1}); end;
    ps_ind=BEAMLINE{b_ind(1)}.PS;
    % get pv names for standardisation parameters
    if ~isfield(FL.HwInfo.PS(ps_ind),'standardise'); error('No standardisation entries for PS: %d',ps_ind); end;
    set(handles.edit4,'String',num2str(lcaGet(FL.HwInfo.PS(ps_ind).standardise.nCycles)));
    set(handles.edit2,'String',num2str(lcaGet(FL.HwInfo.PS(ps_ind).standardise.iLow)));
    set(handles.edit3,'String',num2str(lcaGet(FL.HwInfo.PS(ps_ind).standardise.iHigh)));
    set(handles.edit5,'String',num2str(lcaGet(FL.HwInfo.PS(ps_ind).standardise.waitLow)));
    set(handles.edit6,'String',num2str(lcaGet(FL.HwInfo.PS(ps_ind).standardise.waitHigh)));
    set(handles.edit7,'String',num2str(lcaGet(FL.HwInfo.PS(ps_ind).standardise.rate)));
    set(handles.edit8,'String',num2str(lcaGet(FL.HwInfo.PS(ps_ind).standardise.settingRate)));
    set(handles.text7,'ForegroundColor','black');
    set(handles.text7,'String','Data retrieved from EPICS database');
  else
    error('Enter name of single magnet')
  end
catch
  set(handles.text7,'ForegroundColor','red');
  set(handles.text7,'String',['Error getting standardisation settings: ',lasterr]);
  set(handles.edit2,'String','?');
  set(handles.edit3,'String','?');
  set(handles.edit4,'String','?');
  set(handles.edit5,'String','?');
  set(handles.edit6,'String','?');
  set(handles.edit7,'String','?');
  set(handles.edit8,'String','?');
end

% --- Commit standardisation settings
function pushbutton7_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton7 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global FL BEAMLINE
try
  magData=regexp(get(handles.edit1,'String'),';','split');
  magData={magData{cellfun(@(x) ~isempty(x),magData)}};
  if length(magData)==1
    b_ind=findcells(BEAMLINE,'Name',magData{1});
    if isempty(b_ind); error('unknown magnet name: %s',magData{1}); end;
    if ~isfield(BEAMLINE{b_ind(1)},'PS'); error('No PS element for magnet name: %s',magData{1}); end;
    ps_ind=BEAMLINE{b_ind(1)}.PS;
    % get pv names for standardisation parameters
    if ~isfield(FL.HwInfo.PS(ps_ind),'standardise'); error('No standardisation entries for PS: %d',ps_ind); end;
    if ~isnan(str2double(get(handles.edit4,'String')))
      lcaPut(FL.HwInfo.PS(ps_ind).standardise.nCycles,str2double(get(handles.edit4,'String')));
    else
      error('Incorrect format for nCycles')
    end
    if ~isnan(str2double(get(handles.edit2,'String')))
      lcaPut(FL.HwInfo.PS(ps_ind).standardise.iLow,str2double(get(handles.edit2,'String')));
    else
      error('Incorrect format for iLow')
    end
    if ~isnan(str2double(get(handles.edit3,'String')))
      lcaPut(FL.HwInfo.PS(ps_ind).standardise.iHigh,str2double(get(handles.edit3,'String')));
    else
      error('Incorrect format for iHigh')
    end
    if ~isnan(str2double(get(handles.edit5,'String')))
      lcaPut(FL.HwInfo.PS(ps_ind).standardise.waitLow,str2double(get(handles.edit5,'String')));
    else
      error('Incorrect format for waitLow')
    end
    if ~isnan(str2double(get(handles.edit6,'String')))
      lcaPut(FL.HwInfo.PS(ps_ind).standardise.waitHigh,str2double(get(handles.edit6,'String')));
    else
      error('Incorrect format for waitHigh')
    end
    if ~isnan(str2double(get(handles.edit7,'String')))
      lcaPut(FL.HwInfo.PS(ps_ind).standardise.rate,str2double(get(handles.edit7,'String')));
    else
      error('Incorrect format for standardisation ramp rate')
    end
    if ~isnan(str2double(get(handles.edit8,'String')))
      lcaPut(FL.HwInfo.PS(ps_ind).standardise.settingRate,str2double(get(handles.edit8,'String')));
    else
      error('Incorrect format for setting ramp rate')
    end
  else
    error('Enter name of single magnet')
  end
  set(handles.text7,'ForegroundColor','black');
  set(handles.text7,'String','Data committed to EPICS database');
catch
  set(handles.text7,'ForegroundColor','red');
  set(handles.text7,'String',['Error committing standardisation settings: ',lasterr]);
end


% --- Select magnet list
function popupmenu2_Callback(hObject, eventdata, handles)
% hObject    handle to popupmenu2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = get(hObject,'String') returns popupmenu2 contents as cell array
%        contents{get(hObject,'Value')} returns selected item from popupmenu2
global FL BEAMLINE PS
sel=get(hObject,'Value');
ps_ind=[];
% HAPS PS's
mags=[];
for ips=1:length(FL.HwInfo.PS)
  if ~isempty(FL.HwInfo.PS(ips).pvname) && ~isempty(regexp(FL.HwInfo.PS(ips).pvname{1}{1},'^PS','once'))
    mags(end+1)=ips;
  end
end
switch sel
  case 2
    for ips=mags
      if strcmp(BEAMLINE{PS(ips).Element(1)}.Class,'QUAD')
        ps_ind(end+1)=ips;
      end
    end
  case 3
    for ips=mags
      if strcmp(BEAMLINE{PS(ips).Element(1)}.Class,'SEXT')
        ps_ind(end+1)=ips;
      end
    end
  case 4
    for ips=mags
      if strcmp(BEAMLINE{PS(ips).Element(1)}.Class,'SBEN')
        ps_ind(end+1)=ips;
      end
    end
  case 5
    ps_ind=mags;
end
if ~isempty(ps_ind)
  names=[];
  for ips=1:length(ps_ind)
    names=[names BEAMLINE{PS(ps_ind(ips)).Element(1)}.Name ';'];
  end
  set(handles.edit1,'String',names);
else
  set(handles.edit1,'String',[]);
  errordlg('No magnets set!','Magnet Standardise');
end

    
% --- Executes during object creation, after setting all properties.
function popupmenu2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to popupmenu2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit7_Callback(hObject, eventdata, handles)
% hObject    handle to edit7 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit7 as text
%        str2double(get(hObject,'String')) returns contents of edit7 as a double


% --- Executes during object creation, after setting all properties.
function edit7_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit7 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit8_Callback(hObject, eventdata, handles)
% hObject    handle to edit8 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit8 as text
%        str2double(get(hObject,'String')) returns contents of edit8 as a double


% --- Executes during object creation, after setting all properties.
function edit8_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit8 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in pushbutton8.
function pushbutton8_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton8 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global FL
if ~strcmp(questdlg('Abort Standardisation Procedure?','Standardise','Yes','No','No'),'Yes')
  return
end
FL.abortcmd=true;
set(handles.text7,'String','Abort requested...')
% set(FL.Gui.magStandardise.edit1,'String','ABORT')
drawnow('expose')

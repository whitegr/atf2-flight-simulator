function varargout = plic(varargin)
% PLIC MATLAB code for plic.fig
%      PLIC, by itself, creates a new PLIC or raises the existing
%      singleton*.
%
%      H = PLIC returns the handle to a new PLIC or the handle to
%      the existing singleton*.
%
%      PLIC('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in PLIC.M with the given input arguments.
%
%      PLIC('Property','Value',...) creates a new PLIC or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before plic_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to plic_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help plic

% Last Modified by GUIDE v2.5 19-Nov-2010 06:18:47

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @plic_OpeningFcn, ...
                   'gui_OutputFcn',  @plic_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before plic is made visible.
function plic_OpeningFcn(hObject, eventdata, handles, varargin) %#ok<*INUSL>
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to plic (see VARARGIN)

handles.BL=varargin{1};

% Load previously saved if exists
% - delete saved file if m file newer
savefile=dir('plicData.mat');
if isempty(savefile)
  savefile=dir('trustedApps/plic/plicData.mat');
  dfile='trustedApps/plic/plicData.mat';
else
  dfile='plicData.mat';
end
mfile=dir('plic.m');
if isempty(mfile)
  mfile=dir('trustedApps/plic/plic.m');
end
if ~isempty(savefile) && etime(datevec(mfile.date),datevec(savefile.date))>0
  delete(dfile);
end
handles.scale=1.2721; % m/bin
if exist(dfile,'file')
  lh=load(dfile,'handles');
  bg=load(dfile,'bgdata');
  handles.EXT=lh.handles.EXT;
  handles.FFS=lh.handles.FFS;
  handles.s1=lh.handles.s1;
  handles.s2=lh.handles.s2;
  handles.ind1=lh.handles.ind1;
  handles.ind2=lh.handles.ind2;
  handles.nbkg=lh.handles.nbkg;
  handles.reprate=lh.handles.reprate;
  if ~isempty(bg.bgdata)
    handles.EXT.bkg=bg.bgdata{1};
    handles.FFS.bkg=bg.bgdata{2};
  end
else
  handles.ind1=findcells(handles.BL,'Name','IEX');
  handles.ind2=length(handles.BL);
  handles.s1=handles.BL{handles.ind1}.S;
  handles.s2=handles.BL{handles.ind2}.S;
  handles.EXT.pv='TEST:lwf';
  handles.FFS.pv='TEST:rwf';
  handles.EXT.ind1=19;
  handles.EXT.ind2=46;
  handles.EXT.s1=0;
  handles.EXT.s2=38;
  handles.FFS.ind1=26;
  handles.FFS.ind2=81; % 139
  handles.FFS.s1=39;
  handles.FFS.s2=95.3059;
  handles.nbkg=10;
  handles.reprate=1.56; % Hz
  handles.EXT.bkg=zeros(1,1000);
  handles.FFS.bkg=zeros(1,1000);
end

% Mag bar plot
plot_magnets_external(handles.BL(handles.ind1:handles.ind2),handles.axes2,handles.BL{handles.ind1}.S,handles.BL{handles.ind2}.S,1,1,10);

% Get and plot PLIC signal
plotPlic(handles);

% Choose default command line output for plic
handles.output = handles;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes plic wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = plic_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;

function plotPlic(handles) %#ok<*DEFNU>

bgdata=get(handles.pushbutton1,'UserData');
if ~isempty(bgdata)
  handles.EXT.bkg=bgdata{1};
  handles.FFS.bkg=bgdata{2};
end

% Get PLIC data
extData=lcaGet(handles.EXT.pv,handles.EXT.ind2)-handles.EXT.bkg(1:handles.EXT.ind2);
extData=extData(handles.EXT.ind1:end);
ffsData=lcaGet(handles.FFS.pv,handles.FFS.ind2)-handles.FFS.bkg(1:handles.FFS.ind2);
ffsData=ffsData(handles.FFS.ind1:end);
sData=[linspace(handles.EXT.s1,handles.EXT.s2,length(extData)) linspace(handles.FFS.s1,handles.FFS.s2,length(ffsData))];
plicData=[extData ffsData];

% Plot bkg subtracted PLIC data
area(handles.axes1,sData,abs(plicData)./0.25,'FaceColor','red');
% plot(handles.axes1,sData,abs(plicData)./0.25)
axis(handles.axes1,'tight')
ax=axis(handles.axes1);
ax(3)=0;
ax(4)=1;
axis(handles.axes1,ax);
grid(handles.axes1,'on');

% --- Executes when user attempts to close figure1.
function figure1_CloseRequestFcn(hObject, eventdata, handles)
% hObject    handle to figure1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global FL

try
  bgdata=get(handles.pushbutton1,'UserData'); %#ok<NASGU>
  if isempty(strfind(pwd,'trustedApps'))
    save('trustedApps/plic/plicData.mat',handles);
  else
    save('plicData.mat','handles','bgdata')
  end
  if ~isempty(FL) && isfield(FL,'Gui')
    guiCloseFn('plic',handles);
  else
    exit
  end
catch
  exit
end


% --- Executes on button press in pushbutton1.
function pushbutton1_Callback(hObject, eventdata, handles) %#ok<*INUSD>
% hObject    handle to pushbutton1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
mh=msgbox('Getting Background PLIC signal...');
extData=zeros(handles.nbkg,1000);
ffsData=zeros(handles.nbkg,1000);
for ipulse=1:handles.nbkg
  extData(ipulse,:)=lcaGet(handles.EXT.pv,1000);
  ffsData(ipulse,:)=lcaGet(handles.FFS.pv,1000);
  pause(1/handles.reprate)
end
set(hObject,'UserData',{mean(abs(extData)) mean(abs(ffsData))});
if ishandle(mh); delete(mh); end;
% Update handles structure
guidata(handles.figure1, handles);

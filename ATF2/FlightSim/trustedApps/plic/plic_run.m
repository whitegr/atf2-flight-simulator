function plic_run(BEAMLINE)
% Main loop for PLIC display and functionality

ph=[];
while 1
  if isempty(ph)
    ph=plic(BEAMLINE);
  else
    plic('plotPlic',ph);
  end
  pause(1/ph.reprate)
end
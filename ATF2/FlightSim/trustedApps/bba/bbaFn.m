function [stat varargout] = bbaFn(cmd,varargin)
% BBAFN
%   Companion function to trusted bba appliction
global FL
persistent pars

stat{1}=1; varargout{1}=[];

% Initialise pars on first call
if isempty(pars)
  if exist('userData/bbaFn.mat','file')
    ldpars=load('userData/bbaFn');
  else
    ldpars.pars=[];
  end
  pars=init(ldpars.pars);
  if strcmpi(cmd,'init') || strcmpi(cmd,'updatebba')
    return
  end
end

% Proccess commands
switch lower(cmd)
  case 'init'
    pars=init(pars);
  case 'getpars'
    varargout{1}=pars;
  case 'setpars'
    if nargin>0
      fn=fieldnames(varargin{1});
      for ifn=1:length(fn)
        pars.(fn{ifn})=varargin{1}.(fn{ifn});
      end
    end
  case 'setoffset'
    if nargin<3 || ~isnumeric(varargin{1}) || ~isnumeric(varargin{2}) || length(varargin{2})~=2
      error('Must provide INSTR index and offset values [x y]')
    end
    [stat pars]=setoffset(pars,varargin{1},varargin{2});
  case {'resetbumps' 'restorebumps'}
    if nargin<2 || ~isnumeric(varargin{1})
      error('Must pass bpm instr ind as second arg')
    end
    stat=bumpcmd(cmd,varargin{1},pars.doaxis,pars);
  case 'start'
    if nargin<2 || ~isnumeric(varargin{1})
      error('Must pass bpm instr ind as second arg')
    end
    if ~strcmp(pars.status,'stopped')
      stat{1}=-1;
      stat{2}='BBA already in running state or error needs clearing';
      return
    end
    pars.status='starting';
    pars.thisbpm=varargin{1};
    [stat pars]=runbba(pars);
    if stat{1}<0
      pars.status='error';
      [stat2 pars]=runbba(pars);
    end
  case 'stop'
    if ~strcmp(pars.status,'stopped') && ~isempty(pars.thisbpm)
      pars.status='stopping';
      [stat pars]=runbba(pars);
    end
  case 'continue'
    if isempty(strfind(pars.status,'running'))
      stat{1}=-1;
      stat{2}='BBA not running';
      return
    end
    % Stop if max number of iterations reached
    if pars.iterstep>pars.maxiter(pars.thisbpm)
      pars.status='stopping';
    end
    [stat pars]=runbba(pars);
    if stat{1}<0
      pars.status='error';
      [stat2 pars]=runbba(pars);
    end
  case 'bpmsel'
    if nargin<2 || ~isnumeric(varargin{1})
      error('Must provide bpm index')
    end
    [stat bpms expbpmsig]=bpmsel(pars,varargin{1});
    varargout{1}=bpms;
    varargout{2}=expbpmsig;
  case 'commitbba'
    if nargin<4 || ~isnumeric(varargin{1}) || ~isnumeric(varargin{3}) || ~ischar(varargin{2})
      error('Must provide bpm index, axis (''x'' | ''y'') and bba setting')
    end
    if FL.SimMode
      if strcmp(varargin{2},'x')
        lcaPut(pars.bbapv_x{varargin{1}},varargin{3});
      elseif strcmp(varargin{2},'y')
        lcaPut(pars.bbapv_y{varargin{1}},varargin{3});
      end
    end
  case 'updatebba'
    for ibpm=1:length(pars.allowedbpms)
      if FL.SimMode
        pars.bba_x(pars.allowedbpms(ibpm))=0;
        pars.bba_y(pars.allowedbpms(ibpm))=0;
        pars.bbats_x(pars.allowedbpms(ibpm))=now;
        pars.bbats_y(pars.allowedbpms(ibpm))=now;
      else
        [pars.bba_x(pars.allowedbpms(ibpm)) ts]=lcaGet(pars.bbapv_x{pars.allowedbpms(ibpm)});
        pars.bbats_x(pars.allowedbpms(ibpm))=epicsts2mat(ts);
        [pars.bba_y(pars.allowedbpms(ibpm)) ts]=lcaGet(pars.bbapv_y{pars.allowedbpms(ibpm)});
        pars.bbats_y(pars.allowedbpms(ibpm))=epicsts2mat(ts);
      end
    end
  otherwise
    error('Unknown command')
end

% Save pars
save userData/bbaFn.mat pars

%% Local functions (for local people)
function pars=init(loadpars)
global INSTR BEAMLINE FL
useApp('bumpgui');
pars.blocking=false;
pars.deftol=5e-6;
pars.defmaxiter=10;
pars.defnbpm=10;
pars.defqcut=0.5;
pars.defdps=0.2;
pars.defbpmdev=2;
pars.status='stopped';
pars.measureonce=false;
pars.doaxis=[1 1];
pars.thisOffset=[];
pars.thisBpmMeas=[];
pars.thisbpm=[];
pars.lastpulsenum=[];
pars.iterstep=0;
bpms_allowed=findcells(BEAMLINE,'Name','MQ*');
for ibpm=1:length(bpms_allowed)
  pars.allowedbpms(ibpm)=findcells(INSTR,'Index',bpms_allowed(ibpm));
  if strcmp(INSTR{pars.allowedbpms(ibpm)}.Type,'stripline')
    pars.bbapv_x{pars.allowedbpms(ibpm)}=['ATF:BPMS:',BEAMLINE{bpms_allowed(ibpm)}.Name,':XBBA.VAL'];
    pars.bbapv_y{pars.allowedbpms(ibpm)}=['ATF:BPMS:',BEAMLINE{bpms_allowed(ibpm)}.Name,':YBBA.VAL'];
  else
    pars.bbapv_x{pars.allowedbpms(ibpm)}=[regexprep(BEAMLINE{bpms_allowed(ibpm)}.Name,'^M',''),'x:bbaOffset.VAL'];
    pars.bbapv_y{pars.allowedbpms(ibpm)}=[regexprep(BEAMLINE{bpms_allowed(ibpm)}.Name,'^M',''),'y:bbaOffset.VAL'];
  end
  if FL.SimMode
    pars.bba_x(pars.allowedbpms(ibpm))=0;
    pars.bbats_x(pars.allowedbpms(ibpm))=now;
    pars.bba_y(pars.allowedbpms(ibpm))=0;
    pars.bbats_y(pars.allowedbpms(ibpm))=now;
  else
    [pars.bba_x(pars.allowedbpms(ibpm)) ts]=lcaGet(pars.bbapv_x{pars.allowedbpms(ibpm)});
    pars.bbats_x(pars.allowedbpms(ibpm))=epicsts2mat(ts);
    [pars.bba_y(pars.allowedbpms(ibpm)) ts]=lcaGet(pars.bbapv_y{pars.allowedbpms(ibpm)});
    pars.bbats_y(pars.allowedbpms(ibpm))=epicsts2mat(ts);
  end
end
if exist('loadpars','var') && ~isempty(loadpars)
  pars.tol=loadpars.tol;
  pars.maxiter=loadpars.maxiter;
  pars.nbpm=loadpars.nbpm;
  pars.qcut=loadpars.qcut;
  pars.dps=loadpars.dps;
  pars.bpmdev=loadpars.bpmdev;
  pars.bumpx=loadpars.bumpx;
  pars.bumpy=loadpars.bumpy;
  pars.ignorebpm=loadpars.ignorebpm;
else
  for ibpm=1:length(INSTR)
    pars.tol(ibpm)=pars.deftol;
    pars.maxiter(ibpm)=pars.defmaxiter;
    pars.nbpm(ibpm)=pars.defnbpm;
    pars.qcut(ibpm)=pars.defqcut;
    pars.dps(ibpm)=pars.defdps;
    pars.bpmdev(ibpm)=pars.defbpmdev;
    pars.bumpx{ibpm}=[];
    pars.bumpx_orig{ibpm}=0;
    pars.bumpy{ibpm}=[];
    pars.ignorebpm{1,ibpm}=false(1,length(INSTR));
    pars.ignorebpm{2,ibpm}=false(1,length(INSTR));
  end
end

function [stat pars]=runbba(pars)
global FL
persistent xbpms ybpms
stat{1}=1;
% check we know which bpm we are using
if isempty(pars.thisbpm)
  stat{1}=-1; stat{2}='BPM to use not set in bbaFn runbba function';
  return
end
% Check doing at least one axis
if ~any(pars.doaxis)
  stat{1}=-1; stat{2}='Must select at least one axis to use';
  return
end
% if start command, set up first iteration
if strcmp(pars.status,'starting')
  % Get bpm list to use
  [stat,bpmlist]=bpmsel(pars,pars.thisbpm);
  if stat{1}~=1; return; end;
  xbpms=bpmlist(1,:);
  ybpms=bpmlist(2,:);
  % check we have bpms to work with
  if (~any(xbpms) && pars.doaxis(1)) || (~any(ybpms) && pars.doaxis(2))
    stat{1}=-1; stat{2}='No BPMs to use with current selection criteria';
    return
  end
  pars.lastpulsenum=[];
  pars.thisBpmMeas=[];
  pars.status='running';
  pars.iterstep=0;
  bbameas('clear');
end

% If stopping or error -> stop command, clean up
if strcmp(pars.status,'stopping') || strcmp(pars.status,'error')
  pars.lastpulsenum=[];
  bbameas('clear');
  pars.iterstep=1e10;
  if ~strcmp(pars.status,'error'); pars.status='stopped'; end;
  return
end

% Measure offset at magnet, and if not the first iteration then move beam
% to this offset
if pars.blocking
  itersteps=0:pars.maxiter(pars.thisbpm);
else
  itersteps=pars.iterstep;
end
for istep=itersteps
  if ~pars.iterstep
    % Measure BBA offset
    [stat pars]=bbameas(pars,xbpms,ybpms);
    % stat{1}=1 signals meas complete, =0 waiting for pulses <0 error
    % If blocking, then make off strength measurement now also
    if pars.blocking || pars.measureonce; [stat pars]=bbameas(pars,xbpms,ybpms); end;
    if stat{1}~=1; return; end;
    pars.iterstep=pars.iterstep+1;
  else
    % Move beam to offset
    osize=pars.doaxis.*pars.thisoffset;
    [stat pars]=setoffset(pars,pars.thisbpm,osize);
    % Wait for offset trim to settle
    pause(FL.trimPause);
    % Measure BBA offset
    [stat pars]=bbameas(pars,xbpms,ybpms);
    % If blocking, then make off strength measurement now also
    if pars.blocking; [stat pars]=bbameas(pars,xbpms,ybpms); end;
    % stat{1}=1 signals meas complete, =0 waiting for pulses <0 error
    if stat{1}~=1; return; end;
    pars.iterstep=pars.iterstep+1;
  end
end

function [stat pars]=bbameas(pars,xbpms,ybpms)
global BEAMLINE PS INSTR FL
persistent stage lastbpmMeas
stat{1}=1;

if isequal(pars,'clear')
  stage=[]; lastbpmMeas=[];
  return
end

% Shunt magnet
if ~isempty(stage)
  magind=findcells(BEAMLINE,'Name',regexprep(BEAMLINE{INSTR{pars.thisbpm}.Index}.Name,'^M',''));
  if ~strcmp(BEAMLINE{magind(1)}.Class,'QUAD')
    stat{1}=-1; stat{2}='Can only BBA Quad attached BPMs';
    return
  end
  magps=BEAMLINE{magind(1)}.PS;
  initps=PS(magps).SetPt;
  if initps==0
    stat{1}=-1; stat{2}='Initial magnet strength set to zero, cannot do BBA!';
    return
  end
  PS(magps).SetPt=initps*(1-pars.dps(pars.thisbpm));
  stat=PSTrim(magps,1); if stat{1}~=1; return; end;
  pause(FL.trimPause)
end

% Wait for N bpms
if isempty(pars.lastpulsenum)
  [stat pars.lastpulsenum]=FlHwUpdate('GetPulseNum'); if stat{1}~=1; return; end;
end
if pars.blocking || pars.measureonce
  stat=FlHwUpdate('wait',pars.nbpm(pars.thisbpm));if stat{1}~=1; return; end;
else
  stat=FlHwUpdate; if stat{1}~=1; return; end; % Update machine status
  [stat pulsenum]=FlHwUpdate('GetPulseNum'); if stat{1}~=1; return; end;
  if pulsenum-pars.lastpulsenum < pars.nbpm(pars.thisbpm)
    stat{1}=0;
    return
  end
end
pars.lastpulsenum=[];

% Get bpm readings
[stat bpmdata]=FlHwUpdate('bpmave',pars.nbpm(pars.thisbpm),0,[pars.qcut(pars.thisbpm) 3 1 0]);
if stat{1}~=1; return; end;
if ~isempty(stage)
  lastbpmMeas=pars.thisBpmMeas;
end
pars.thisBpmMeas(1,:)=bpmdata{1}(1,:);
pars.thisBpmMeas(2,:)=bpmdata{1}(2,:);
pars.thisBpmMeas(3,:)=bpmdata{1}(4,:);
pars.thisBpmMeas(4,:)=bpmdata{1}(5,:);

xbpms=logical(xbpms); ybpms=logical(ybpms);
if ~isempty(stage)
  dx=lastbpmMeas(1,:)-pars.thisBpmMeas(1,:);
  dx_err=sqrt(lastbpmMeas(2,:).^2+pars.thisBpmMeas(2,:).^2);
  dy=lastbpmMeas(3,:)-pars.thisBpmMeas(3,:);
  dy_err=sqrt(lastbpmMeas(4,:).^2+pars.thisBpmMeas(4,:).^2);
  pars.thisBpmData={find(xbpms); dx(xbpms); dx_err(xbpms); find(ybpms); dy(ybpms); dy_err(ybpms)};
  % Get Quad R matrix for dps change
  PS(magps).Ampl = initps;
  [stat,Rquad1]=RmatAtoB(magind(1),magind(2)); if stat{1}~=1; return; end;
  PS(magps).Ampl = initps*(1-pars.dps(pars.thisbpm));
  [stat,Rquad2]=RmatAtoB(magind(1),magind(2)); if stat{1}~=1; return; end;
  RQ=Rquad2-Rquad1;
  PS(magps).Ampl = initps;

   % Get transfer matrix elements (magnet - measurement BPMs)
  R11=zeros(1,length(INSTR));
  R12=R11; R33=R11; R34=R11;
  for ibpm=unique([find(xbpms) find(ybpms)])
    [stat,R] = RmatAtoB(magind(1)+1,INSTR{ibpm}.Index); if stat{1}~=1; return; end;
    R11(1,ibpm)=R(1,1); R12(1,ibpm)=R(1,2); R33(1,ibpm)=R(3,3); R34(1,ibpm)=R(3,4);
  end

  % Calculate offset
  [q,dq]=noplot_polyfit(1:sum(xbpms),-dx(xbpms)./(R11(xbpms).*RQ(1,1)+R12(xbpms).*RQ(2,1)),...
    dx_err(xbpms)./(R11(xbpms).*RQ(1,1)+R12(xbpms).*RQ(2,1)),0);
  pars.thisOffset(1)=q(1); pars.thisOffset(2)=dq(1);
  [q,dq]=noplot_polyfit(1:sum(ybpms),-dy(ybpms)./(R33(ybpms).*RQ(3,3)+R34(ybpms).*RQ(4,3)),...
    dy_err(ybpms)./(R33(ybpms).*RQ(3,3)+R34(ybpms).*RQ(4,3)),0);
  pars.thisOffset(3)=q(1); pars.thisOffset(4)=dq(1);
end

% Flag action for next function call
if isempty(stage)
  stage=1;
  stat{1}=0;
else
  % Return magnet setting
  PS(magps).SetPt=initps;
  stat=PSTrim(magps,1); if stat{1}~=1; return; end;
  pause(FL.trimPause)
  stage=[];
end

function [stat bpms expbpmsig]=bpmsel(pars,bpmind)
global INSTR FL BEAMLINE PS
bpms=[]; expbpmsig=[];
% Get list of good downstream bpms
[stat data] = FlBpmToolFn('GetData');
if stat{1}~=1; return; end;
goodind=data.global.hw & data.global.cal & data.global.use & (1:length(INSTR))>bpmind;
if isfield(data,'bba')
  goodind=goodind & data.bba.use;
end
if ~any(goodind)
  stat{1}=-1;
  stat{2}='No good bpms to use';
  return
end

% Get expected bpm readings for given dps
bpmlist=find(goodind&arrayfun(@(x) strcmp(INSTR{x}.Class,'MONI'),1:length(goodind)));
magind=findcells(BEAMLINE,'Name',regexprep(BEAMLINE{INSTR{bpmind}.Index}.Name,'^M',''));
B=MakeBeam6DGauss(FL.SimModel.Initial,1,1,1);
B.Bunch.x(6)=BEAMLINE{INSTR{bpmind}.Index}.P;
initmag=PS(BEAMLINE{magind(1)}.PS).Ampl;
bpms(1,:)=goodind;
bpms(2,:)=goodind;
if initmag<0.1
  stat{1}=-1;
  stat{2}='This magnet less than 10% nominal strength, BBA calculations not performed';
  return
end
% Look at tracking response to tolerence offset beam at magnet face
B.Bunch.x=[pars.tol(bpmind);0;pars.tol(bpmind);0;0;B.Bunch.x(6)];
% Set all BEAMLINE resolutions to zero for tracking
for ibpm=magind(1):INSTR{bpmlist(end)}.Index
  if isfield(BEAMLINE{ibpm},'Resolution')
    initres(ibpm)=BEAMLINE{ibpm}.Resolution;
    BEAMLINE{ibpm}.Resolution=0;
  end
end
[stat,beamout,instdata] = TrackThru( magind(1), INSTR{bpmlist(end)}.Index, B, 1, 1, 0 );
if stat{1}~=1; stat{2}='Failed to track simulated beam to get BPMs to use'; PS(BEAMLINE{magind(1)}.PS).Ampl=initmag; return; end;
xbpm1=[instdata{1}.x]; ybpm1=[instdata{1}.y]; indx=[instdata{1}.Index];
PS(BEAMLINE{magind(1)}.PS).Ampl=initmag*(1-pars.dps(bpmind));
[stat,beamout,instdata] = TrackThru( magind(1), INSTR{bpmlist(end)}.Index, B, 1, 1, 0 );
if stat{1}~=1; stat{2}='Failed to track simulated beam to get BPMs to use'; PS(BEAMLINE{magind(1)}.PS).Ampl=initmag; return; end;
xbpm2=[instdata{1}.x]; ybpm2=[instdata{1}.y];
PS(BEAMLINE{magind(1)}.PS).Ampl=initmag;
bpmdifx=abs(xbpm2-xbpm1); bpmdify=abs(ybpm2-ybpm1);
expbpmsig=zeros(size(bpms));
for ibpm=1:length(bpmlist)
  wbpm=find(ismember(indx,INSTR{bpmlist(ibpm)}.Index));
  expbpmsig(1,bpmlist(ibpm))=bpmdifx(wbpm);
  expbpmsig(2,bpmlist(ibpm))=bpmdify(wbpm);
  bpms(1,bpmlist(ibpm))=bpms(1,bpmlist(ibpm)) & bpmdifx(wbpm)>INSTR{bpmlist(ibpm)}.Res*pars.bpmdev(bpmind) & ~pars.ignorebpm{1,bpmind}(bpmlist(ibpm));
  bpms(2,bpmlist(ibpm))=bpms(2,bpmlist(ibpm)) & bpmdify(wbpm)>INSTR{bpmlist(ibpm)}.Res*pars.bpmdev(bpmind) & ~pars.ignorebpm{2,bpmind}(bpmlist(ibpm));
end
% Put resolution info back
for ibpm=magind(1):INSTR{bpmlist(end)}.Index
  if isfield(BEAMLINE{ibpm},'Resolution')
    BEAMLINE{ibpm}.Resolution=initres(ibpm);
  end
end

function [stat pars]=setoffset(pars,bpmind,osize)
global INSTR BEAMLINE FL GIRDER PS %#ok<NUSED>
stat{1}=1;
% Determine type of bump to use and make bump if not exist
if isempty(pars.bumpx{bpmind})
  magind=findcells(BEAMLINE,'Name',regexprep(BEAMLINE{INSTR{bpmind}.Index}.Name,'^M',''));
  if isfield(BEAMLINE{INSTR{bpmind}.Index},'Girder') && ...
      BEAMLINE{INSTR{bpmind}.Index}.Girder>0 && ...
      ~isempty(FL.HwInfo.GIRDER(BEAMLINE{INSTR{bpmind}.Index}.Girder).pvname)
    pars.bumpx{bpmind}.gind=BEAMLINE{INSTR{bpmind}.Index}.Girder;
    pars.bumpx{bpmind}.Value=0;
    pars.bumpy{bpmind}.gind=BEAMLINE{INSTR{bpmind}.Index}.Girder;
    pars.bumpy{bpmind}.Value=0;
    pars.bumpx_orig{bpmind}=GIRDER{pars.bumpx{bpmind}.gind}.MoverPos(1);
    pars.bumpy_orig{bpmind}=GIRDER{pars.bumpx{bpmind}.gind}.MoverPos(3);
  else
    allcor=findcells(BEAMLINE,'Class','XCOR');
    us=find((INSTR{bpmind}.Index-allcor)>0);
    if length(us)<2
      stat{1}=-1;
      stat{2}=['Not enough X correctors for bump for ',BEAMLINE{INSTR{bpmind}.Index}.Name];
      return
    end
    xcor(1)=allcor(us(end-1)); xcor(2)=allcor(us(end));
    if us(end)+2 < length(allcor)
      qind=findcells(BEAMLINE,'Class','Quad',magind(end)+1,length(BEAMLINE));
      gind=findcells(BEAMLINE,'Girder',[],magind(end)+1,length(BEAMLINE));
      gind=gind(arrayfun(@(x) BEAMLINE{x}.Girder>0,gind));
      gind=gind(arrayfun(@(x) isempty(FL.HwInfo.GIRDER(BEAMLINE{x}.Girder).pvname),gind));
    end
    if us(end)+1 < length(allcor)
      xcor(3)=allcor(us(end)+1);
    else
      moverind=qind(ismember(qind,gind))>magind(end);
      xcor(3)=moverind(1);
    end
    if us(end)+2 < length(allcor)
      xcor(4)=allcor(us(end)+2);
    elseif us(end)+1 < length(allcor)
      moverind=qind(ismember(qind,gind))>allcor(us(end)+1);
      xcor(4)=moverind(1);
    else
      xcor(4)=moverind(3);
    end
    allcor=findcells(BEAMLINE,'Class','YCOR');
    us=find((INSTR{bpmind}.Index-allcor)>0);
    if length(us)<2
      stat{1}=-1;
      stat{2}=['Not enough Y correctors for bump for ',BEAMLINE{INSTR{bpmind}.Index}.Name];
      return
    end
    ycor(1)=allcor(us(end-1)); ycor(2)=allcor(us(end));
    if us(end)+2 < length(allcor)
      qind=findcells(BEAMLINE,'Class','Quad',magind(end)+1,length(BEAMLINE));
      gind=findcells(BEAMLINE,'Girder',[],magind(end)+1,length(BEAMLINE));
      gind=gind(arrayfun(@(x) BEAMLINE{x}.Girder>0,gind));
      gind=gind(arrayfun(@(x) isempty(FL.HwInfo.GIRDER(BEAMLINE{x}.Girder).pvname),gind));
    end
    if us(end)+1 < length(allcor)
      ycor(3)=allcor(us(end)+1);
    else
      moverind=qind(ismember(qind,gind))>magind(end);
      ycor(3)=moverind(1);
    end
    if us(end)+2 < length(allcor)
      ycor(4)=allcor(us(end)+2);
    elseif us(end)+1 < length(allcor)
      moverind=qind(ismember(qind,gind))>allcor(us(end)+1);
      ycor(4)=moverind(1);
    else
      xcor(4)=moverind(3);
    end
    pars.bumpx{bpmind}=bumpgui(1,4,INSTR{bpmind}.Index,xcor);
    for ichan=1:4
      evalc(['pars.bumpx_orig{bpmind}(ichan)=',pars.bumpx{bpmind}.Channel(ichan).Parameter]);
    end
    pars.bumpy{bpmind}=bumpgui(3,4,INSTR{bpmind}.Index,ycor);
    for ichan=1:4
      evalc(['pars.bumpy_orig{bpmind}(ichan)=',pars.bumpy{bpmind}.Channel(ichan).Parameter]);
    end
  end
end
% Make offset
if isfield(pars.bumpx{bpmind},'gind')
  GIRDER{pars.bumpx{bpmind}.gind}.MoverSetPt=...
    GIRDER{pars.bumpx{bpmind}.gind}.MoverSetPt-[osize(1) osize(2) 0];
  stat = MoverTrim(pars.bumpx{bpmind}.gind,1);
  if stat{1}==1
    pars.bumpx{bpmind}.Value=pars.bumpx{bpmind}.Value+osize(1);
    pars.bumpy{bpmind}.Value=pars.bumpy{bpmind}.Value+osize(2);
  else
    return
  end
else
  if osize(1)~=0
    stat=IncrementMultiKnob('pars.bumpx{bpmind}',osize(1),1);
    if stat{1}~=1; return; end;
  end
  if osize(2)~=0
    stat=IncrementMultiKnob('pars.bumpy{bpmind}',osize(2),1);
    if stat{1}~=1; return; end;
  end
end

function stat = bumpcmd(cmd,bpmind,axis,pars)
global GIRDER PS %#ok<NUSED>
stat{1}=1;
bn={'bumpx' 'bumpy'};
bno={'bumpx_orig' 'bumpy_orig'};
for iax=find(axis)
  if isempty(pars.(bn{iax}))
    continue
  end
  pars.(bn{iax}){bpmind}.Value=0;
  if isfield(pars.(bn{iax}){bpmind},'gind')
    if strcmpi(cmd,'restorebumps') && isfield(pars,bno{iax}) && ~isempty(pars.(bno{iax}){bpmind})
      GIRDER{gind}.MoverSetPt(iax)=pars.(bno{iax}){bpmind}(iax);
      stat=MoverTrim(gind,4);
      if stat{1}~=1; return; end;
    else
      pars.(bno{iax}){bpmind}(iax)=GIRDER{gind}.MoverPos(iax);
    end
    continue
  end
  for ichan=1:4
    if ~isempty(pars.(bno{iax}){bpmind})
      if strcmpi(cmd,'restorebumps') && isfield(pars,bno{iax}) && ~isempty(pars.(bno{iax}){bpmind})
        evalc([pars.(bn{iax}){bpmind}.Channel(ichan).Parameter,'=pars.(bno{iax}){bpmind}']);
        if ~isempty(strfind(pars.(bn{iax}){bpmind}.Channel(ichan).Parameter,'GIRDER'))
          stat=MoverTrim(pars.(bn{iax}){bpmind}.Channel(ichan).Unit,1);
          if stat{1}~=1; return; end;
        else
          stat=PSTrim(pars.(bn{iax}){bpmind}.Channel(ichan).Unit,1);
          if stat{1}~=1; return; end;
        end
      elseif strcmpi(cmd,'resetbumps')
        evalc(['pars.(bno{iax}){bpmind}=',pars.(bn{iax}){bpmind}.Channel(ichan).Parameter]);
      end
    end
  end
end
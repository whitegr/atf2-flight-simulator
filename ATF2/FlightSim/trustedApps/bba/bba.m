function varargout = bba(varargin)
% BBA M-file for bba.fig
%      BBA, by itself, creates a new BBA or raises the existing
%      singleton*.
%
%      H = BBA returns the handle to a new BBA or the handle to
%      the existing singleton*.
%
%      BBA('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in BBA.M with the given input arguments.
%
%      BBA('Property','Value',...) creates a new BBA or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before bba_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to bba_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help bba

% Last Modified by GUIDE v2.5 18-Nov-2009 17:55:15

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @bba_OpeningFcn, ...
                   'gui_OutputFcn',  @bba_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before bba is made visible.
function bba_OpeningFcn(hObject, eventdata, handles, varargin) %#ok<*INUSL>
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to bba (see VARARGIN)
global BEAMLINE INSTR

% Choose default command line output for bba
handles.output = handles;

% Update handles structure
guidata(hObject, handles);

% Info message box
mhan=msgbox('Initialising BBA routine...','BBA');

% Fill BPM popup menu
% Allow BBA for Quad attached EXT and FFS bpms
bpmind=findcells(BEAMLINE,'Name','MQ*');
for ibpm=1:length(bpmind)
  pstr{ibpm}=BEAMLINE{bpmind(ibpm)}.Name;
end
set(handles.popupmenu1,'String',pstr);

% Get function parameters and set GUI values
[stat pars]=bbaFn('GetPars');
if stat{1}~=1
  errordlg(stat{2},'bbaFn Error')
  return
end
% - make sure bbaFn in stopped state
if ~strcmp(pars.status,'stopped')
  stat=bbaFn('stop');
  if stat{1}~=1
    errordlg(stat{2},'bbaFn Error')
    return
  end
  [stat pars]=bbaFn('GetPars');
  if stat{1}~=1
    errordlg(stat{2},'bbaFn Error')
    return
  end
end
if ~isempty(pars.thisbpm)
  thisbpm=BEAMLINE{INSTR{pars.thisbpm}.Index}.Name;
  set(handles.popupmenu1,'Value',find(ismember(pstr,thisbpm)));
else
  set(handles.popupmenu1,'Value',1);
end
for ibpm=1:length(bpmind)
  bpminstr(ibpm)=findcells(INSTR,'Index',bpmind(ibpm));
end
set(handles.popupmenu1,'UserData',bpminstr);
popupmenu1_Callback(handles.popupmenu1,[],handles);

% Set bbaFn into non-blocking mode
newpars.blocking=false;
stat=bbaFn('SetPars',newpars);
if stat{1}~=1
  errordlg(stat{2},'bbaFn Error')
  return
end

% Remove info box if still there
if ishandle(mhan); delete(mhan); end;

% --- Outputs from this function are returned to the command line.
function varargout = bba_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on button press in pushbutton1.
function pushbutton1_Callback(hObject, eventdata, handles) %#ok<*INUSD,*DEFNU>
% hObject    handle to pushbutton1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
guiCloseFn('bba',handles);

% N BPM readings to ave
function edit1_Callback(hObject, eventdata, handles)
% hObject    handle to edit1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

bpminstr=get(handles.popupmenu1,'UserData');
bpmind=bpminstr(get(handles.popupmenu1,'Value'));
val=str2double(get(hObject,'String'));
[stat pars]=bbaFn('GetPars');
if isnan(val) || val<2
  set(hObject,'String',num2str(pars.defnbpm));
  return
end
newpars.nbpm=pars.nbpm;
if get(handles.togglebutton2,'Value')
  newpars.nbpm(:)=val;
else
  if get(handles.togglebutton2,'Value')
    newpars.nbpm(:)=val;
  else
    newpars.nbpm(bpmind)=val;
  end
end
bbaFn('SetPars',newpars);


% --- Executes during object creation, after setting all properties.
function edit1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% Q cut
function edit2_Callback(hObject, eventdata, handles)
% hObject    handle to edit2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

bpminstr=get(handles.popupmenu1,'UserData');
bpmind=bpminstr(get(handles.popupmenu1,'Value'));
val=str2double(get(hObject,'String'));
[stat pars]=bbaFn('GetPars');
if isnan(val) || val<=0 || val>=1
  set(hObject,'String',num2str(pars.defqcut));
  return
end
newpars.qcut=pars.qcut;
if get(handles.togglebutton2,'Value')
  newpars.qcut(:)=val;
else
  newpars.qcut(bpmind)=val;
end
bbaFn('SetPars',newpars);


% --- Executes during object creation, after setting all properties.
function edit2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Start BBA procedure
function pushbutton2_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
bpminstr=get(handles.popupmenu1,'UserData');
bpmind=bpminstr(get(handles.popupmenu1,'Value'));
set(handles.pushbutton9,'String','Starting');
set(handles.pushbutton9,'BackgroundColor','green');
set(handles.text6,'String','Starting BBA measurement process...');
drawnow('expose');
maxiter=str2double(get(handles.edit5,'String'));
[stat pars]=bbaFn('GetPars');
if stat{1}~=1
  set(handles.text6,'String',sprintf('Fetch pars error:\n%s',stat{2}))
  set(handles.pushbutton9,'String','Error');
  set(handles.pushbutton9,'BackgroundColor',[1 0.5 0]);
  return
end
% Check bba function in stopped state before starting
if ~strcmp(pars.status,'stopped')
  set(handles.text6,'String','BBA appears to still be running, reset with status button')
  set(handles.pushbutton9,'String','Error');
  set(handles.pushbutton9,'BackgroundColor',[1 0.5 0]);
  return
end
% Start measurement procedure and take first offset measurement
stat = bbaFn('start',bpmind);
if stat{1}<0
  set(handles.text6,'String',sprintf('Run error:\n%s',stat{2}))
  set(handles.pushbutton9,'String','Error');
  set(handles.pushbutton9,'BackgroundColor',[1 0.5 0]);
  return
end
[stat pars]=bbaFn('GetPars');
if stat{1}~=1
  set(handles.text6,'String',sprintf('Fetch pars error:\n%s',stat{2}))
  set(handles.pushbutton9,'String','Error');
  set(handles.pushbutton9,'BackgroundColor',[1 0.5 0]);
  return
end
% Continue taking BBA data and moving to offset until requested to stop or
% max iterations reached
lastiter=pars.iterstep;
diagplot('clear');
while ~isempty(pars.iterstep) && pars.iterstep<=maxiter
  % Update displays
  popupmenu1_Callback(handles.popupmenu1,[],handles);
  % check for stop command or stop if using measureonce command or reached
  % tolerence value
  stcmd=get(hObject,'UserData');
  istol=pars.iterstep>0;
  if get(handles.radiobutton1,'Value') && abs(pars.thisOffset(1))<pars.tol(bpmind)
    istol=false;
  end
  if get(handles.radiobutton2,'Value') && abs(pars.thisOffset(3))<pars.tol(bpmind)
    istol=false;
  end
  if isequal(stcmd,'stop') || (isequal(stcmd,'measureonce') && pars.iterstep>0) || (istol && ~isequal(stcmd,'measureonce'))
    if isequal(stcmd,'measureonce')
      diagplot('bpm',pars,handles);
    end
    stat = bbaFn('Stop');
    if stat{1}<0
      set(handles.text6,['Stop Command error:\n' stat{2}]);
      set(handles.pushbutton9,'String','Error');
      set(handles.pushbutton9,'BackgroundColor',[1 0.5 0]);
    else
      if isequal(stcmd,'stop')
        set(handles.text6,'String','Stop Request');
      else
        set(handles.text6,'String','Finished measurement')
      end
      set(handles.pushbutton9,'String','Stopped');
      set(handles.pushbutton9,'BackgroundColor','red');
    end
    set(hObject,'UserData',[]);
    return
  end
  % Check if stopping or stopped
  [stat pars]=bbaFn('GetPars');
  if stat{1}==1 && (strcmp(pars.status,'stopping') || strcmp(pars.status,'stopped'))
    set(handles.text6,'BBA procedure complete');
    set(handles.pushbutton9,'String','Stopped');
    set(handles.pushbutton9,'BackgroundColor','red');
    return
  end
  % continue with next step of BBA procedure
  stat = bbaFn('continue',bpmind);
  if stat{1}<0
    set(handles.text6,sprintf('Run error:\n %s',stat{2}))
    set(handles.pushbutton9,'String','Error');
    set(handles.pushbutton9,'BackgroundColor',[1 0.5 0]);
    return
  end
  % Get current status
  [stat pars]=bbaFn('GetPars');
  if stat{1}~=1
    set(handles.text6,['Fetch pars error:\n' stat{2}]);
    set(handles.pushbutton9,'String','Error');
    set(handles.pushbutton9,'BackgroundColor',[1 0.5 0]);
    return
  end
  % Show appropriate diagnostic plots
  % & ask accept|reject bpm measurements
  if ~isempty(pars.iterstep) && ~isequal(lastiter,pars.iterstep)
    lastiter=pars.iterstep;
    if ~get(handles.togglebutton1,'Value')
      diagplot('bpm',pars,handles);
      set(handles.pushbutton9,'String','Paused');
      set(handles.pushbutton9,'BackgroundColor',[1 0.5 0]);
      set(handles.text6,'String','Accept or retake BPM measurements?')
      drawnow('expose');
      uiwait(handles.figure1);
      while ishandle(handles.figure1) && ...
          ~(get(handles.figure1,'CurrentObject')==handles.pushbutton10 || get(handles.figure1,'CurrentObject')==handles.pushbutton11)
        uiwait(handles.figure1);
      end
      if ~ishandle(handles.figure1); return; end;   
      if get(handles.figure1,'CurrentObject')==handles.pushbutton11
        newpars.iterstep=newpars.iterstep-1;
        bbaFn('SetPars',newpars); pars.iterstep=newpars.iterstep;
      end
    end
    diagplot('offset',pars,handles);
  end
  set(handles.text6,'String',sprintf('Running...\nIteration step: %d',pars.iterstep));
  set(handles.pushbutton9,'String','Running');
  set(handles.pushbutton9,'BackgroundColor','green');
  drawnow('expose');
  pause(0.2)
end
% Make sure bba in stopped state and signal completion
bbaFn('stop');
set(handles.text6,'BBA procedure complete');
set(handles.pushbutton9,'String','Stopped');
set(handles.pushbutton9,'BackgroundColor','red');

% plotting function
function diagplot(cmd,pars,handles)
persistent offdata lastcmd
if isequal(cmd,'clear')
  offdata=[];
  lastcmd=[];
  return
end
if isempty(cmd)
  cmd=lastcmd;
else
  lastcmd=cmd;
end
%pars.thisBpmData=[find(xbpms); dx(xbpms); dx_err(xbpms); find(ybpms); dy(ybpms); dy_err(ybpms)];
switch cmd
  case 'bpm'
    % plot x or y measurement bpm estimates of beam offset at magnet
    if get(handles.radiobutton3,'Value')
      errorbar(pars.thisBpmData{1},pars.thisBpmData{2}*1e6,pars.thisBpmData{3}*1e6)
      xlabel('INSTR index');
      ylabel('\DeltaX / um');
    else
      errorbar(pars.thisBpmData{4},pars.thisBpmData{5}*1e6,pars.thisBpmData{6}*1e6)
      xlabel('INSTR index');
      ylabel('\DeltaY / um');
    end
  case 'offset'
    offdata(pars.iterstep,:)=pars.thisOffset(:);
    os=size(offdata);
    if get(handles.radiobutton3,'Value')
      errorbar(1:os(1),offdata(:,1)*1e6,offdata(:,2)*1e6);
      xlabel('Iteration #');
      ylabel('Measured Horizontal Beam Offset / um');
    else
      errorbar(1:os(1),offdata(:,3)*1e6,offdata(:,4)*1e6);
      xlabel('Iteration #');
      ylabel('Measured Vertical Beam Offset / um');
    end
end

% --- Executes on button press in pushbutton3.
function pushbutton3_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on button press in pushbutton4.
function pushbutton4_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on button press in pushbutton5.
function pushbutton5_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton5 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on button press in pushbutton6.
function pushbutton6_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton6 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Status
function pushbutton9_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton9 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Reset error status
if strcmpi(get(hObject,'String'),'error')
  newpars.status='stopped';
  bbaFn('SetPars',newpars);
  set(hObject,'String','Stopped');
  set(hObject,'BackgroundColor','red');
  set(handles.text6,'String','')
end

% --- Reset x BBA
function pushbutton7_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton7 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
bpminstr=get(handles.popupmenu1,'UserData');
bpmind=bpminstr(get(handles.popupmenu1,'Value'));
if ~strcmp(questdlg('Reset horizontal BBA offset to zero (takes immediate effect)?','Reset BBA?'),'Yes')
  return
end
stat=bbaFn('CommitBBA',bpmind,'x',0);
if stat{1}~=1
  set(handles.text6,['BBA commit error:\n' stat{2}]);
  set(handles.pushbutton9,'String','Error');
  set(handles.pushbutton9,'BackgroundColor',[1 0.5 0]);
  return
else
  set(handles.text6,'X BBA setting reset to zero');
  popupmenu1_Callback(handles.popupmenu1,[],handles);
end

% --- Executes on button press in pushbutton8.
function pushbutton8_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton8 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- BPM list
function popupmenu1_Callback(hObject, eventdata, handles)
% hObject    handle to popupmenu1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
persistent lastval

if ~isempty(lastval) && ~get(hObject,'Value')
  set(hObject,'Value',lastval)
end
lastval=get(hObject,'Value');
  

% Reset message box
set(handles.text6,'String','');

% Get bba function pars
stat=bbaFn('UpdateBBA'); % make sure have fresh BBA values
if stat{1}~=1
  errordlg(stat{2},'bbaFn Error')
  return
end
[stat pars]=bbaFn('GetPars');
if stat{1}~=1
  errordlg(stat{2},'bbaFn Error')
  return
end

% BPM index
bpminstr=get(hObject,'UserData');
bpmind=bpminstr(get(hObject,'Value'));

% Set GUI boxes
set(handles.edit4,'String',num2str(pars.tol(bpmind)*1e6,4));
set(handles.edit5,'String',num2str(pars.maxiter(bpmind),4));
set(handles.edit8,'String',num2str(pars.dps(bpmind)*100,4));
set(handles.edit1,'String',num2str(pars.nbpm(bpmind)));
set(handles.edit2,'String',num2str(pars.qcut(bpmind)));
set(handles.edit12,'String',num2str(pars.bpmdev(bpmind)));
set(handles.radiobutton1,'Value',pars.doaxis(1));
set(handles.radiobutton2,'Value',pars.doaxis(2));
if ~isempty(pars.thisOffset)
  set(handles.text10,'String',sprintf('%.3g +/- %.3g',pars.thisOffset(1)*1e6,pars.thisOffset(2)*1e6));
  set(handles.text14,'String',sprintf('%.3g +/- %.3g',pars.thisOffset(3)*1e6,pars.thisOffset(4)*1e6));
else
  set(handles.text10,'String','---');
  set(handles.text14,'String','---');
end
if ~isempty(pars.thisBpmMeas)
  set(handles.text12,'String',sprintf('%.3g +/- %.3g',pars.thisBpmMeas(1,pars.thisbpm)*1e6,pars.thisBpmMeas(2,pars.thisbpm)*1e6));
  set(handles.text15,'String',sprintf('%.3g +/- %.3g',pars.thisBpmMeas(3,pars.thisbpm)*1e6,pars.thisBpmMeas(4,pars.thisbpm)*1e6));
else
  set(handles.text12,'String','---');
  set(handles.text15,'String','---');
end
set(handles.text4,'String',num2str(pars.bba_x(bpmind)*1e6))
set(handles.text11,'String',datestr(pars.bbats_x(bpmind)))
set(handles.text13,'String',num2str(pars.bba_y(bpmind)*1e6))
set(handles.text17,'String',datestr(pars.bbats_y(bpmind)))
drawnow('expose')

% Get BPM selection
popupmenu3_Callback(handles.popupmenu3,eventdata,handles);

% --- Executes during object creation, after setting all properties.
function popupmenu1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to popupmenu1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in popupmenu2.
function popupmenu2_Callback(hObject, eventdata, handles)
% hObject    handle to popupmenu2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns popupmenu2 contents as cell array
%        contents{get(hObject,'Value')} returns selected item from popupmenu2


% --- Executes during object creation, after setting all properties.
function popupmenu2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to popupmenu2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% Max iter
function edit5_Callback(hObject, eventdata, handles)
% hObject    handle to edit5 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

bpminstr=get(handles.popupmenu1,'UserData');
bpmind=bpminstr(get(handles.popupmenu1,'Value'));
val=floor(str2double(get(hObject,'String')));
[stat pars]=bbaFn('GetPars');
if isnan(val) || val<2
  set(hObject,'String',num2str(pars.defmaxiter));
  return
end
newpars.maxiter=pars.maxiter;
if get(handles.togglebutton2,'Value')
  newpars.maxiter(:)=val;
else
  newpars.maxiter(bpmind)=val;
end
bbaFn('SetPars',newpars);


% --- Executes during object creation, after setting all properties.
function edit5_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit5 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% Stop Tol
function edit4_Callback(hObject, eventdata, handles)
% hObject    handle to edit4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

bpminstr=get(handles.popupmenu1,'UserData');
bpmind=bpminstr(get(handles.popupmenu1,'Value'));
val=str2double(get(hObject,'String'))*1e-6;
[stat pars]=bbaFn('GetPars');
if isnan(val) || val<=0
  set(hObject,'String',num2str(pars.deftol));
  return
end
newpars.tol=pars.tol;
if get(handles.togglebutton2,'Value')
  newpars.tol(:)=val;
else
  newpars.tol(bpmind)=val;
end
bbaFn('SetPars',newpars);



% --- Executes during object creation, after setting all properties.
function edit4_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit6_Callback(hObject, eventdata, handles)
% hObject    handle to edit6 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit6 as text
%        str2double(get(hObject,'String')) returns contents of edit6 as a double


% --- Executes during object creation, after setting all properties.
function edit6_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit6 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Accept
function pushbutton10_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton10 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
uiresume(handles.figure1);

% --- Reject
function pushbutton11_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton11 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
uiresume(handles.figure1);

% --- Auto Accept
function togglebutton1_Callback(hObject, eventdata, handles)
% hObject    handle to togglebutton1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

if get(hObject,'Value')
  set(hObject,'String','Auto Accept On');
  set(hObject,'BackgroundColor','green')
else
  set(hObject,'String','Auto Accept Off');
  set(hObject,'BackgroundColor','red')
end


% --- Measure once
function pushbutton12_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton12 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
set(handles.pushbutton2,'UserData','measureonce');
newpars.measureonce=true;
bbaFn('SetPars',newpars);
pushbutton2_Callback(handles.pushbutton2,[],handles);
newpars.measureonce=false;
bbaFn('SetPars',newpars);
if ~strcmpi(get(handles.pushbutton9,'String'),'error')
  popupmenu1_Callback(handles.popupmenu1,'noplot',handles);
  set(handles.text6,'String','Single Measurement Completed');
end

% --- Select X axis
function radiobutton1_Callback(hObject, eventdata, handles)
% hObject    handle to radiobutton1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

newpars.doaxis=[get(handles.radiobutton1,'Value') get(handles.radiobutton2,'Value')];
if ~any(newpars.doaxis)
  set(hObject,'Value',1);
  newpars.doaxis=[get(handles.radiobutton1,'Value') get(handles.radiobutton2,'Value')];
end
bbaFn('SetPars',newpars);

% --- Select Y axis
function radiobutton2_Callback(hObject, eventdata, handles)
% hObject    handle to radiobutton2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

newpars.doaxis=[get(handles.radiobutton1,'Value') get(handles.radiobutton2,'Value')];
if ~any(newpars.doaxis)
  set(hObject,'Value',1);
  newpars.doaxis=[get(handles.radiobutton1,'Value') get(handles.radiobutton2,'Value')];
end
bbaFn('SetPars',newpars);


% --- Reset y BBA setting
function pushbutton13_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton13 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
bpminstr=get(handles.popupmenu1,'UserData');
bpmind=bpminstr(get(handles.popupmenu1,'Value'));
if ~strcmp(questdlg('Reset vertical BBA offset to zero (takes immediate effect)?','Reset BBA?'),'Yes')
  return
end
stat=bbaFn('CommitBBA',bpmind,'y',0);
if stat{1}~=1
  set(handles.text6,['BBA commit error:\n' stat{2}]);
  set(handles.pushbutton9,'String','Error');
  set(handles.pushbutton9,'BackgroundColor',[1 0.5 0]);
  return
else
  set(handles.text6,'Y BBA setting reset to zero');
  popupmenu1_Callback(handles.popupmenu1,[],handles);
end

% --- Executes on button press in togglebutton2.
function togglebutton2_Callback(hObject, eventdata, handles)
% hObject    handle to togglebutton2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

if get(hObject,'Value')
  set(hObject,'String','Apply Settings to all BPMs');
else
  set(hObject,'String','Apply Settings to just this BPM')
end


% dPS/PS
function edit8_Callback(hObject, eventdata, handles)
% hObject    handle to edit8 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

bpminstr=get(handles.popupmenu1,'UserData');
bpmind=bpminstr(get(handles.popupmenu1,'Value'));
val=str2double(get(hObject,'String'));
[stat pars]=bbaFn('GetPars');
if isnan(val) || val<=0 || val >=100
  set(hObject,'String',num2str(pars.defdps*100));
  return
end
newpars.dps=pars.dps;
if get(handles.togglebutton2,'Value')
  newpars.dps(:)=val/100;
else
  newpars.dps(bpmind)=val/100;
end
bbaFn('SetPars',newpars);


% --- Executes during object creation, after setting all properties.
function edit8_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit8 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Commit x
function pushbutton15_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton15 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
bpminstr=get(handles.popupmenu1,'UserData');
bpmind=bpminstr(get(handles.popupmenu1,'Value'));
[stat pars]=bbaFn('GetPars');
if stat{1}~=1
  errordlg(stat{2},'bbaFn Error')
  return
end
% If within desired tolerence, call this zero offset
if abs(pars.thisOffset(1)) < pars.tol(bpmind)
  pars.thisOffset(1)=0;
end
bbasetting_x=pars.bba_x(bpmind)+pars.thisOffset(1)-pars.thisBpmMeas(1,bpmind);
if ~strcmp(questdlg(sprintf('Commit New BBA setting to EPICS Database (takes immediate effect)?\n Will submit current BBA + Current Measured Offset - Current Ave BPM = %s',...
    [num2str(bbasetting_x*1e6),' um']),'Submit BBA?'),'Yes')
  return
end
stat=bbaFn('CommitBBA',bpmind,'x',bbasetting_x);
if stat{1}~=1
  set(handles.text6,'String',sprintf('BBA commit error:\n%s',stat{2}));
  set(handles.pushbutton9,'String','Error');
  set(handles.pushbutton9,'BackgroundColor',[1 0.5 0]);
  return
else
  set(handles.text6,'String','X BBA setting commited to EPICS database');
  popupmenu1_Callback(handles.popupmenu1,[],handles);
end

% --- Commit y
function pushbutton16_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton16 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
bpminstr=get(handles.popupmenu1,'UserData');
bpmind=bpminstr(get(handles.popupmenu1,'Value'));
[stat pars]=bbaFn('GetPars');
if stat{1}~=1
  errordlg(stat{2},'bbaFn Error')
  return
end
% If within desired tolerence, call this zero offset
if abs(pars.thisOffset(3)) < pars.tol(bpmind)
  pars.thisOffset(3)=0;
end
bbasetting_y=pars.bba_y(bpmind)+pars.thisOffset(3)-pars.thisBpmMeas(3,bpmind);
if ~strcmp(questdlg(sprintf('Commit New BBA setting to EPICS Database (takes immediate effect)?\n Will submit current BBA + Current Measured Offset - Current Ave BPM = %s',...
    [num2str(bbasetting_y*1e6),' um']),'Submit BBA?'),'Yes')
  return
end
stat=bbaFn('CommitBBA',bpmind,'y',bbasetting_y);
if stat{1}~=1
  set(handles.text6,['BBA commit error:\n' stat{2}]);
  set(handles.pushbutton9,'String','Error');
  set(handles.pushbutton9,'BackgroundColor',[1 0.5 0]);
  return
else
  set(handles.text6,'Y BBA setting commited to EPICS database');
  popupmenu1_Callback(handles.popupmenu1,[],handles);
end


% --- Bump to zero offset
function pushbutton17_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton17 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
set(handles.text6,'String','Commanding bump to beam...');
drawnow('expose');
bpminstr=get(handles.popupmenu1,'UserData');
bpmind=bpminstr(get(handles.popupmenu1,'Value'));
[stat pars]=bbaFn('GetPars');
if stat{1}~=1
  errordlg(stat{2},'bbaFn Error')
  return
end
if isfield(pars.bumpx{bpmind},'Value')
  bumpsetting=[pars.bumpx{bpmind}.Value pars.bumpy{bpmind}.Value] + ...
    [pars.thisOffset(1) pars.thisOffset(3)].*[get(handles.radiobutton1,'Value') get(handles.radiobutton2,'Value')];
else
  bumpsetting=[pars.thisOffset(1) pars.thisOffset(3)].*[get(handles.radiobutton1,'Value') get(handles.radiobutton2,'Value')];
end
stat=bbaFn('SetOffset',bpmind,bumpsetting);
if stat{1}~=1
  set(handles.text6,'String',sprintf('BBA commit error:\n%s',stat{2}));
  set(handles.pushbutton9,'String','Error');
  set(handles.pushbutton9,'BackgroundColor',[1 0.5 0]);
  return
else
  popupmenu1_Callback(handles.popupmenu1,'noplot',handles);
  set(handles.text6,'String','Bump commanded to zero measure beam offset in magnet');
end

% --- Get Bump setting information and display
function pushbutton18_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton18 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% handles    structure with handles and user data (see GUIDATA)
global GIRDER PS %#ok<NUSED>
bpminstr=get(handles.popupmenu1,'UserData');
bpmind=bpminstr(get(handles.popupmenu1,'Value'));
[stat pars]=bbaFn('GetPars');
if stat{1}~=1
  errordlg(stat{2},'bbaFn Error')
  return
end
bumpnames=[]; bumpvals=[];
if isempty(pars.bumpx{bpmind})
  bumpnames='?';
  bumpvals='?';
  bumprefvals='?';
elseif isfield(pars.bumpx{bpmind},'gind')
  bumpnames=['GIRDER ',num2str(pars.bumpx{bpmind}.gind)];
  bumpvals=num2str(GIRDER{pars.bumpx{bpmind}.gind}.MoverSetPt(1)*1e6);
  bumprefvals=num2str(pars.bumpx_orig{bpmind}*1e6);
else
  for ichan=1:length(pars.bumpx{bpmind}.Channel)
    bumpnames=[bumpnames ' ' pars.bumpx{bpmind}.Channel(ichan).Parameter];
    evalc(['val=',pars.bumpx{bpmind}.Channel(ichan).Parameter]);
    bumpvals=[bumpvals ' ' num2str(val*1e6)];
  end
  if ~isfield(pars,'bumpx_orig')
    set(handles.text6,'String','No bump computations made yet')
    return
  end
  bumprefvals=num2str(pars.bumpx_orig{bpmind}.*1e6);
end
str_x=sprintf('X:\nCurrent Bump Value (um): %g \nBump components: %s\nComponent Current Values: %s\nComponent Reference Values: %s\n',...
  pars.bumpx{bpmind}.Value*1e6,bumpnames,bumpvals,bumprefvals);
bumpnames=[]; bumpvals=[];
if isempty(pars.bumpy{bpmind})
  bumpnames='?';
  bumpvals='?';
  bumprefvals='?';
elseif isfield(pars.bumpy{bpmind},'gind')
  bumpnames=['GIRDER ',num2str(pars.bumpy{bpmind}.gind)];
  bumpvals=num2str(GIRDER{pars.bumpy{bpmind}.gind}.MoverSetPt(1)*1e6);
  bumprefvals=num2str(pars.bumpy_orig{bpmind}*1e6);
else
  for ichan=1:length(pars.bumpy{bpmind}.Channel)
    bumpnames=[bumpnames ' ' pars.bumpy{bpmind}.Channel(ichan).Parameter];
    evalc(['val=',pars.bumpy{bpmind}.Channel(ichan).Parameter]);
    bumpvals=[bumpvals ' ' num2str(val*1e6)];
  end
  bumprefvals=num2str(pars.bumpy_orig{bpmind}.*1e6);
end
str_y=sprintf('Y:\nCurrent Bump Value (um): %g \nBump components: %s\nComponent Current Values: %s\nComponent Reference Values: %s\n',...
  pars.bumpy{bpmind}.Value*1e6,bumpnames,bumpvals,bumprefvals);
set(handles.text6,'String',[str_x str_y]);

% --- Restore bump
function pushbutton19_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton19 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
bpminstr=get(handles.popupmenu1,'UserData');
bpmind=bpminstr(get(handles.popupmenu1,'Value'));
stat=bbafn('restorebumps',bpmind);
if stat{1}~=1
  set(handles.text6,'String',sprintf('Error returning bump to default value\n%s',stat{2}));
  set(handles.pushbutton9,'String','Error');
  set(handles.pushbutton9,'BackgroundColor',[1 0.5 0]);
else
  set(handles.text6,'String','Bump restored to default value');
end

% --- Executes on button press in pushbutton21.
function pushbutton21_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton21 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on button press in pushbutton22.
function pushbutton22_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton22 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)



function edit9_Callback(hObject, eventdata, handles)
% hObject    handle to edit9 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

val=str2double(get(hObject,'String'));
if isnan(val) || val<0
  set(hObject,'String','0')
end

% --- Executes during object creation, after setting all properties.
function edit9_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit9 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit10_Callback(hObject, eventdata, handles)
% hObject    handle to edit10 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit10 as text
%        str2double(get(hObject,'String')) returns contents of edit10 as a double


% --- Executes during object creation, after setting all properties.
function edit10_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit10 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in pushbutton20.
function pushbutton20_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton20 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)



function edit11_Callback(hObject, eventdata, handles)
% hObject    handle to edit11 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

val=str2double(get(hObject,'String'));
if isnan(val) || val<0
  set(hObject,'String','0')
end


% --- Executes during object creation, after setting all properties.
function edit11_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit11 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Increment bump
function pushbutton23_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton23 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
bpminstr=get(handles.popupmenu1,'UserData');
bpmind=bpminstr(get(handles.popupmenu1,'Value'));
[stat pars]=bbaFn('GetPars');
if stat{1}~=1
  errordlg(stat{2},'bbaFn Error')
  return
end
if ~isfield(pars.bumpx{bpmind},'Value')
  pars.bumpx{bpmind}.Value=0;
end
if ~isfield(pars.bumpy{bpmind},'Value')
  pars.bumpy{bpmind}.Value=0;
end
newval(1)=str2double(get(handles.edit9,'String')).*1e-6;
newval(2)=str2double(get(handles.edit11,'String')).*1e-6;
stat=bbaFn('setoffset',bpmind,...
  [pars.bumpx{bpmind}.Value pars.bumpy{bpmind}.Value]+newval);
if stat{1}~=1
  set(handles.text6,'String',sprintf('Error setting new bump value\n%s',stat{2}));
  set(handles.pushbutton9,'String','Error');
  set(handles.pushbutton9,'BackgroundColor',[1 0.5 0]);
else
  set(handles.text6,'String','Bump setting completed');
end


% --- Reset Bump
function pushbutton24_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton24 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
bpminstr=get(handles.popupmenu1,'UserData');
bpmind=bpminstr(get(handles.popupmenu1,'Value'));
stat=bbaFn('resetbumps',bpmind);
if stat{1}~=1
  set(handles.text6,['Error resetting bunp reference\n',stat{2}]);
  set(handles.pushbutton9,'String','Error');
  set(handles.pushbutton9,'BackgroundColor',[1 0.5 0]);
else
  set(handles.text6,'Bump reference point reset to current location.');
end


% --- Executes on selection change in listbox1.
function listbox1_Callback(hObject, eventdata, handles)
% hObject    handle to listbox1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns listbox1 contents as cell array
%        contents{get(hObject,'Value')} returns selected item from listbox1


% --- Executes during object creation, after setting all properties.
function listbox1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to listbox1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: listbox controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- add to allowed list
function pushbutton25_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton25 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
bpminstr=get(handles.popupmenu1,'UserData');
bpmind=bpminstr(get(handles.popupmenu1,'Value'));
axis=get(handles.popupmenu3,'Value');
[stat pars]=bbaFn('GetPars');
if stat{1}~=1
  errordlg(stat{2},'bbaFn Error')
  return
end

% Get INSTR ind of selected bpms
bpmlist=get(handles.listbox2,'String');
bpmsel=get(handles.listbox2,'Value');
bpminst=arrayfun(@(x) str2double(regexp(bpmlist{x},'\d+','match','once')),bpmsel);
pars.ignorebpm{axis,bpmind}(bpminst(~isnan(bpminst)))=false;
newpars.ignorebpm=pars.ignorebpm;
stat=bbaFn('SetPars',newpars);
if stat{1}~=1
  errordlg(stat{2},'bbaFn Error')
  return
end

% Update displays
popupmenu3_Callback(handles.popupmenu3,eventdata,handles);


% BPM selection cut criteria
function edit12_Callback(hObject, eventdata, handles)
% hObject    handle to edit12 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
bpminstr=get(handles.popupmenu1,'UserData');
bpmind=bpminstr(get(handles.popupmenu1,'Value'));
[stat pars]=bbaFn('GetPars');
if stat{1}~=1
  errordlg(stat{2},'bbaFn Error')
  return
end
val=str2double(get(hObject,'String'));
if isnan(val) || val<=0
  set(hObject,'String',num2str(pars.defbpmdev))
  return
end
newpars.bpmdev=pars.bpmdev;
if get(handles.togglebutton2,'Value')
  newpars.bpmdev(:)=val;
else
  newpars.bpmdev(bpmind)=val;
end
stat=bbaFn('SetPars',newpars);
if stat{1}~=1
  errordlg(stat{2},'bbaFn Error')
  return
end
popupmenu3_Callback(handles.popupmenu3,[],handles);


% --- Executes during object creation, after setting all properties.
function edit12_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit12 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Apply BPM selection cut
function pushbutton26_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton26 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
%Just need to re-show BPM selections
popupmenu3_Callback(handles.popupmenu3,eventdata,handles);

% --- Select axis for displaying BPM selections
function popupmenu3_Callback(hObject, eventdata, handles)
% hObject    handle to popupmenu3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global BEAMLINE INSTR
persistent lastval
set(handles.text6,'String','')
% BPM index
if ~isempty(lastval) && ~get(hObject,'Value')
  set(hObject,'Value',lastval);
end
lastval=get(hObject,'Value');
bpminstr=get(handles.popupmenu1,'UserData');
bpmind=bpminstr(get(handles.popupmenu1,'Value'));

% get bpm data
[stat bpms expsig]=bbaFn('BpmSel',bpmind);
if stat{1}~=1
  errordlg(stat{2},'bbaFn Error')
  return
end

axis=get(hObject,'Value');

% Check if any BPMs pass the cuts
if ~any(bpms(axis,:))
  set(handles.text6,'String','No BPMs pass the selection cuts!');
  set(handles.pushbutton9,'String','Error');
  set(handles.pushbutton9,'BackgroundColor',[1 0.5 0]);
  drawnow('expose');
  return
end

% Form used bpm string
str={};
for ibpm=find(bpms(axis,:))
  str{end+1}=sprintf('%4d: %13s | %9.4f | %6.2f',ibpm,BEAMLINE{INSTR{ibpm}.Index}.Name,...
    INSTR{ibpm}.Res*1e6,expsig(axis,ibpm)*1e6);
end
set(handles.listbox1,'Value',1);
set(handles.listbox1,'String',str);

[stat pars]=bbaFn('GetPars');

% Form disused bpm string
str={};
badbpms=find(~bpms(axis,:));
for ibpm=badbpms(ismember(badbpms,pars.allowedbpms)&badbpms>bpmind)
  str{end+1}=sprintf('%4d: %13s | %9.4f | %6.2f',ibpm,BEAMLINE{INSTR{ibpm}.Index}.Name,...
    INSTR{ibpm}.Res*1e6,expsig(axis,ibpm)*1e6);
end
set(handles.listbox2,'Value',1);
set(handles.listbox2,'String',str);
if ~isequal(eventdata,'noplot')
  plot(handles.axes1,find(bpms(axis,:)),expsig(axis,find(bpms(axis,:))).*1e6,'*','linewidth',4) %#ok<FNDSB>
  xlabel(handles.axes1,'INSTR ID')
  ylabel(handles.axes1,'Expected BPM signal / um')
  grid(handles.axes1,'on')
end
if axis==1
  set(handles.radiobutton3,'Value',1)
  set(handles.radiobutton4,'Value',0)
else
  set(handles.radiobutton3,'Value',0)
  set(handles.radiobutton4,'Value',1)
end
%%

% --- Executes during object creation, after setting all properties.
function popupmenu3_CreateFcn(hObject, eventdata, handles)
% hObject    handle to popupmenu3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in listbox2.
function listbox2_Callback(hObject, eventdata, handles)
% hObject    handle to listbox2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns listbox2 contents as cell array
%        contents{get(hObject,'Value')} returns selected item from listbox2


% --- Executes during object creation, after setting all properties.
function listbox2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to listbox2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: listbox controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Disable bpm
function pushbutton27_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton27 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
bpminstr=get(handles.popupmenu1,'UserData');
bpmind=bpminstr(get(handles.popupmenu1,'Value'));
axis=get(handles.popupmenu3,'Value');
[stat pars]=bbaFn('GetPars');
if stat{1}~=1
  errordlg(stat{2},'bbaFn Error')
  return
end

% Get INSTR ind of selected bpms
bpmlist=get(handles.listbox1,'String');
bpmsel=get(handles.listbox1,'Value');
bpminst=arrayfun(@(x) str2double(regexp(bpmlist{x},'\d+','match','once')),bpmsel);
pars.ignorebpm{axis,bpmind}(bpminst(~isnan(bpminst)))=true;
newpars.ignorebpm=pars.ignorebpm;
stat=bbaFn('SetPars',newpars);
if stat{1}~=1
  errordlg(stat{2},'bbaFn Error')
  return
end

% Update displays
popupmenu3_Callback(handles.popupmenu3,eventdata,handles);

% --- Executes on selection change in popupmenu4.
function popupmenu4_Callback(hObject, eventdata, handles)
% hObject    handle to popupmenu4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns popupmenu4 contents as cell array
%        contents{get(hObject,'Value')} returns selected item from popupmenu4


% --- Executes during object creation, after setting all properties.
function popupmenu4_CreateFcn(hObject, eventdata, handles)
% hObject    handle to popupmenu4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in pushbutton28.
function pushbutton28_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton28 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on button press in pushbutton29.
function pushbutton29_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton29 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on button press in pushbutton30.
function pushbutton30_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton30 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on button press in pushbutton32.
function pushbutton32_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton32 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on selection change in popupmenu5.
function popupmenu5_Callback(hObject, eventdata, handles)
% hObject    handle to popupmenu5 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns popupmenu5 contents as cell array
%        contents{get(hObject,'Value')} returns selected item from popupmenu5


% --- Executes during object creation, after setting all properties.
function popupmenu5_CreateFcn(hObject, eventdata, handles)
% hObject    handle to popupmenu5 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in radiobutton3.
function radiobutton3_Callback(hObject, eventdata, handles)
% hObject    handle to radiobutton3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

if get(hObject,'Value')
  set(handles.radiobutton4,'Value',0)
  [stat pars]=bbaFn('GetPars');
  if stat{1}~=1
    set(handles.text6,['Fetch pars error:\n' stat{2}])
    set(handles.pushbutton9,'String','Error');
    set(handles.pushbutton9,'BackgroundColor',[1 0.5 0]);
    return
  end
  diagplot([],pars,handles);
end


% --- Executes on button press in radiobutton4.
function radiobutton4_Callback(hObject, eventdata, handles)
% hObject    handle to radiobutton4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

if get(hObject,'Value')
  set(handles.radiobutton3,'Value',0)
  [stat pars]=bbaFn('GetPars');
  if stat{1}~=1
    set(handles.text6,['Fetch pars error:\n' stat{2}])
    set(handles.pushbutton9,'String','Error');
    set(handles.pushbutton9,'BackgroundColor',[1 0.5 0]);
    return
  end
  diagplot([],pars,handles);
end

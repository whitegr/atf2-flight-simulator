function [stat varargout] = sextBBA(cmd,varargin)
%
persistent data
warning('off','MATLAB:nearlySingularMatrix');
stat{1}=1;
varargout{1}=[];

if isempty(data) && ~strcmpi(cmd,'init')
  data=initData();
end

switch lower(cmd)
  case 'init'
    data=initData();
  case 'setplane'
    data.dim=varargin{1};
  case 'setmagnet'
    data.sextName=varargin{1};
  case 'setnpulse'
    data.npulse=varargin{1};
  case 'setscanrange'
    data.scanpoints=linspace(varargin{1},varargin{2},varargin{3});
  case 'dobba'
    [stat data]=doBBA(data);
  case 'dostop'
    FL.sextBBA_dostop=true;
  case 'getdata'
    varargout{1}=data;
  case 'setfighan'
    data.fighan=varargin{1};
  case 'settexthan'
    data.texthan=varargin{1};
  case 'setjs'
    data.JS=varargin{1};
  case 'setsvd'
    data.dosvd=varargin{1};
  otherwise
    error('Unknown Command')
end

function data=initData()
global BEAMLINE INSTR
data.sextName='SF6FF';
data.npulse=20;
data.scanpoints=linspace(-0.8e-3,0.8e-3,10);
data.dim='x';
% INSTRs to use in SVD analysis
data.bpmele=findcells(BEAMLINE,'Class','MONI',findcells(BEAMLINE,'Name','MQF1X'),...
  findcells(BEAMLINE,'Name','MPIP'));
data.instele=arrayfun(@(x) findcells(INSTR,'Index',x),data.bpmele);
data.instele=data.instele(arrayfun(@(x) strcmp(INSTR{x}.Class,'MONI'),data.instele));
data.fighan=[];
data.texthan=[];
data.bbadata=[];
data.usemagpos=true;
data.JS=[];
data.dosvd=true;

function [stat data]=doBBA(data)
global BEAMLINE FL GIRDER INSTR
stat{1}=1;
if isempty(data.JS)
  error('Must supply a FlJitterSubtract object with ''setJS'' command');
end
FlHwUpdate;
isext=findcells(BEAMLINE,'Name',data.sextName);
if isempty(isext)
  stat{1}=-1; stat{2}='No magnet of data.sextName found in BEAMLINE';
  return
end
igir=BEAMLINE{isext(1)}.Girder;
if isempty(igir)
  stat{1}=-1; stat{2}='No girder attached to the requested data.sextName magnet';
  return
end
ginit=GIRDER{igir}.MoverPos;
wdim=find(ismember('xy',data.dim));
if isempty(wdim)
  stat{1}=-1; stat{2}='data.dim must be ''x'' or ''y''';
  return
end
% Get magnet BPM name and index
imagbpm=findcells(BEAMLINE,'Name',['M' data.sextName]);
instmagbpm=findcells(INSTR,'Index',imagbpm);

% do scan
magBpmData=[];
bpmData=[];
bpmDataRaw=[];
nsteps=0;
if isempty(data.fighan)
  data.fighan=axes;
end
% while ~isfield(FL,'sextBBA_dostop')
  for istep=randperm(length(data.scanpoints))
    GIRDER{igir}.MoverSetPt(wdim)=data.scanpoints(istep);
    stat=MoverTrim(igir,3);
    if stat{1}~=1; return; end;
    % take ndata pulses
    if data.dosvd
      jsdata=data.JS.getSubData(data.npulse);
    else
      FlHwUpdate('wait',data.npulse);
      [~, bpmdata]=FlHwUpdate('readbuffer',data.npulse);
      jsdata.x=bpmdata(:,data.JS.useINSTR.*3-1);
      jsdata.x=bpmdata(:,data.JS.useINSTR.*3);
    end
    if isempty(jsdata.x); disp('Error getting pulses, continuing...'); continue; end;
    nsteps=nsteps+1;
    % Form SVD data format
    if data.usemagpos
      magBpmData=[magBpmData; ones(data.npulse,1).*data.scanpoints(istep);];
    else
      if data.dim=='x'
        magBpmData=[magBpmData; jsdata.x(:,ismember(data.JS.useINSTR,instmagbpm))];
      else
        magBpmData=[magBpmData; jsdata.y(:,ismember(data.JS.useINSTR,instmagbpm))];
      end
    end
    bpmData=[bpmData; jsdata.x(:,data.JS.useINSTR>instmagbpm)];
    if data.dosvd
      bpmDataRaw=[bpmDataRaw; jsdata.x_uncor(:,data.JS.useINSTR>instmagbpm)];
    end
    instind=data.JS.useINSTR(data.JS.useINSTR>instmagbpm);
    if nsteps<3; continue; end;
    % Plot data from BPM with best parabolic fit
    [ifit fitData fitVal]=getBestFit(magBpmData,bpmData);
    if data.dosvd
      [ifitRaw fitDataRaw fitValRaw]=getBestFit(magBpmData,bpmDataRaw);
    end
    xfit=linspace(min(magBpmData),max(magBpmData),10000);
    yfit=fitData(1).*(xfit-fitData(2)).^2 + fitData(3);
    if ~data.dosvd || fitVal<fitValRaw
      plot(data.fighan,magBpmData*1e3,bpmData(:,ifit)*1e6,'b.')
      hold on
      if data.dosvd
        plot(data.fighan,magBpmData*1e3,bpmDataRaw(:,ifit)*1e6,'k.')
      end
      plot(data.fighan,xfit*1e3,yfit*1e6,'b')
    else
      plot(data.fighan,magBpmData*1e3,bpmDataRaw(:,ifitRaw)*1e6,'k.')
      hold on
      plot(data.fighan,magBpmData*1e3,bpmData(:,ifitRaw)*1e6,'b.')
      yfit=fitData(1).*(xfit-fitDataRaw(2)).^2 + fitDataRaw(3);
      ifit=ifitRaw;
      fitData=fitDataRaw;
      plot(data.fighan,xfit*1e3,yfit*1e6,'k')
    end
    hold off
    title(data.fighan,['BPM: ' BEAMLINE{INSTR{instind(ifit)}.Index}.Name])
    if isempty(data.texthan)
      fprintf('Offset fit: %g +/- %g (um)\n',fitData(2)*1e6,fitData(5)*1e6)
    else
      set(data.texthan,'String',sprintf('Offset fit: %g +/- %g (um)\n',fitData(2)*1e6,fitData(5)*1e6));
    end
    data.bbadata=[fitData(2) fitData(5)];
    drawnow('expose');
  end
% end
if isfield(FL,'sextBBA_dostop')
  FL=rmfield(FL,'sextBBA_dostop');
end
% Move Magnet back to initial value
disp('Putting magnet to BBA position..')
GIRDER{igir}.MoverSetPt(wdim)=fitData(2);
stat=MoverTrim(igir,3);
if stat{1}~=1; return; end;

function [ifit fitData fitVal]=getBestFit(x,y)
sz=size(y);
for ibpm=1:sz(2)
  if ~any(y(:,ibpm))
    q(ibpm,:)=[0 0 0]; dq(ibpm,:)=ones(1,3).*1e30;
  else
    [q(ibpm,:) dq(ibpm,:)] = noplot_parab(x',y(:,ibpm)');
  end
end
% Order by B error
[fitVal I]=sort(dq(:,2));
% Get BPM with minimum error on B parabola fit value
ifit=I(1);
fitVal=fitVal(1);
fitData=[q(ifit,:) dq(ifit,:)];


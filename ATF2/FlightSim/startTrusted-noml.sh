#!/bin/bash

# Run monitor process
./fsMonitor.sh $$ &

MCRDIR=/Users/glenwhite/MCR/v717

if [ -z $ATF_FS_SIMMODE ]
then
  echo "ATF_FS_SIMMODE environment variable not set"
elif [ -z $EPICS_HOST_ARCH ]; then
  echo "EPICS_HOST_ARCH environment variable not set"
elif ! [ -d "bin/$EPICS_HOST_ARCH" ]; then
  echo "No compiled Floodland code for this HOST ARCHITECTURE" 
else
  cd bin/$EPICS_HOST_ARCH
  if [ -z $1 -a -z $2]
  then
    ./run_FlStart.sh $MCRDIR trusted $ATF_FS_SIMMODE
  else
    ./run_FlStart.sh $MCRDIR trusted $ATF_FS_SIMMODE $1 $2
  fi
fi

if [ $FS_PORT ]
then
  export FS_PORT1=$FS_PORT
  export FS_PORT2=$(( FS_PORT + 2 ))
  export FS_PORT3=$(( FS_PORT + 4 ))
  export FS_PORT4=$(( FS_PORT + 6 ))
fi
if [ -z $ATF_FS_SIMMODE ]
then
  echo "ATF_FS_SIMMODE environment variable not set"
elif [ -z $EPICS_HOST_ARCH ]; then
  echo "EPICS_HOST_ARCH environment variable not set"
elif ! [ -d "bin/$EPICS_HOST_ARCH" ]; then
  echo "No compiled Floodland code for this HOST ARCHITECTURE"
else
  if [ "`printenv ATF_FS_SIMMODE`" == "1" ]
  then # setup EPICS IOC ports used
    if [ $FS_PORT ]
    then
      if  [ "`printenv EPICS_CA_ADDR_LIST | grep localhost:${FS_PORT1}`" == "" ] || [ "`printenv EPICS_CA_ADDR_LIST | grep localhost:${FS_PORT2}`" == "" ] || [ "`printenv EPICS_CA_ADDR_LIST | grep localhost:${FS_PORT3}`" == "" ] || [ "`printenv EPICS_CA_ADDR_LIST | grep localhost:${FS_PORT4}`" == "" ]
      then
        export EPICS_CA_AUTO_ADDR_LIST=NO
        export EPICS_CA_ADDR_LIST="localhost:${FS_PORT1} localhost:${FS_PORT2} localhost:${FS_PORT3} localhost:${FS_PORT4}"
      fi
    fi
  fi
  #cp -r bin/$EPICS_HOST_ARCH/* .
  if [ -z $1 -a -z $2]
  then
    bin/$EPICS_HOST_ARCH/run_FlStart.sh $MCRDIR trusted $ATF_FS_SIMMODE
  else
    bin/$EPICS_HOST_ARCH/run_FlStart.sh $MCRDIR trusted $ATF_FS_SIMMODE $1 $2
  fi
fi

if ~isdeployed
  curDir=pwd;
  % If user has defined LUCRETIADIR as an environment variable, use that definition
  LUCRETIADIR=getenv('LUCRETIADIR');
  if (isempty(LUCRETIADIR))
    % Assume Lucretia installed in directory 'Lucretia' at same path level as
    % ATF2 software was installed, if not then edit below variable to point
    % at correct location
    LUCRETIADIR=fullfile(curDir,'..','..','Lucretia');
  end
  % Check to see if any figs have been updated since last run (and saved in
  % userData), in which case alert user that these need to be cleared and
  % offer to do clear
  gd=dir(fullfile('userData','guiData','*.mat'));
  guiList=regexp(evalc('!find * -name "*.fig"'),'\n','split');
  del_list=[]; del_names=[];
  for igd=1:length(gd)
    igl=find(cellfun(@(x) ~isempty(strfind(regexprep(gd(igd).name,'\.mat',''),x)),guiList));
    if ~isempty(igl)
      gui_info=dir(guiList{igl(1)});
      if gui_info.datenum>gd(igd).datenum
        del_list(end+1)=igd;
        del_names=[del_names gd(igd).name ' '];
      end
    end
  end
  if ~isempty(del_list)
    quest=sprintf('Following GUIs have been updated since last run, delete the data files? ( %s)',del_names);
    resp=questdlg(quest,'Updated GUIs found');
    if strcmp(resp,'Yes')
      for idel=1:length(del_list)
        delete(fullfile('userData','guiData',gd(del_list(idel)).name))
      end
    end
  end
  % ATF2 directory now the parent directory of this startup file
  atfDir=fullfile(curDir,'..');
  % Add symbolic link to mad if not already there
  if ~exist('mad','file')
    evalc(['!ln -s ',fullfile(atfDir,'mad8.51','linux-x86','mad8s'),' mad']);
  end
  addpath userFiles
  % list of function pragmas for compiler
  if exist('compList.m','file'); compList; end;
  % Mexfiles director
  addpath(fullfile(curDir,'mexfiles'));
  hostName=evalc('!hostname');
  % Add core apps and lattice files dirs to path
  addpath(fullfile(curDir,'coreApps'));
  addpath(fullfile(curDir,'latticeFiles'));
  % Add watchdog apps directories
  wdir=fullfile(curDir,'watchdogApps');
  D=dir(wdir);
  for ind=3:length(D)
    if D(ind).isdir && isempty(strfind(D(ind).name,'svn')) && isempty(strfind(D(ind).name,'Floodland')) && ...
        isempty(strfind(D(ind).name,'mexsrc'))
      addpath(fullfile(wdir,D(ind).name),'-END');
    end
  end
  % Add Lucretia directories
  if ~exist(LUCRETIADIR,'dir') || ~exist(fullfile(LUCRETIADIR,'src'),'dir')
    error('Lucretia directory not found, edit LUCRETIADIR variable in startup.m and ensure current version of Lucretia is installed there')
  end
  ldir=fullfile(LUCRETIADIR,'src');
  D=dir(ldir);
  for ind=3:length(D)
    if D(ind).isdir && isempty(strfind(D(ind).name,'svn')) && isempty(strfind(D(ind).name,'Floodland')) && ...
        isempty(strfind(D(ind).name,'mexsrc'))
      addpath(fullfile(ldir,D(ind).name),'-END');
    end
  end
  % Add ATF2 simulation files to end of path
  D=dir(fullfile(atfDir,'simulation'));
  for ind=3:length(D)
    if D(ind).isdir && isempty(strfind(D(ind).name,'svn'))
      addpath(fullfile(atfDir,'simulation',D(ind).name),'-END');
    end
  end
  pathes=regexp(path,pathsep,'split');
  for i=1:length(pathes); 
    if ~isempty(strfind(pathes{i},'curvefit'))
      rmpath(pathes{i});
    end 
  end  
  % Archive mex if running on flight-simulator computer
  [null hostname]=system('hostname');
  addpath('/home/atf2-fs/ATF2/control-software/epics-3.14.8/extensions/src/ChannelArchiver/Matlab')
end


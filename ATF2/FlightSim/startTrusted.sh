#!/bin/bash

# Run monitor process
./fsMonitor.sh $$ &

if [ $FS_PORT ]
then
  export FS_PORT1=$FS_PORT
  export FS_PORT2=$(( FS_PORT + 2 ))
  export FS_PORT3=$(( FS_PORT + 4 ))
  export FS_PORT4=$(( FS_PORT + 6 ))
fi
if [ -z $ATF_FS_SIMMODE ]
then
  echo "ATF_FS_SIMMODE environment variable not set"
else
  if [ "`printenv ATF_FS_SIMMODE`" == "1" ]
  then # setup EPICS IOC ports used
    if [ $FS_PORT ]
    then
      if  [ "`printenv EPICS_CA_ADDR_LIST | grep localhost:${FS_PORT1}`" == "" ] || [ "`printenv EPICS_CA_ADDR_LIST | grep localhost:${FS_PORT2}`" == "" ] || [ "`printenv EPICS_CA_ADDR_LIST | grep localhost:${FS_PORT3}`" == "" ] || [ "`printenv EPICS_CA_ADDR_LIST | grep localhost:${FS_PORT4}`" == "" ]
      then
        export EPICS_CA_AUTO_ADDR_LIST=NO
        export EPICS_CA_ADDR_LIST="localhost:${FS_PORT1} localhost:${FS_PORT2} localhost:${FS_PORT3} localhost:${FS_PORT4}"
      fi
    fi
  fi
  if [ -z $1 ]
  then
    MATCMD="matlab -nodesktop -nosplash -r \"FlStart('trusted',$ATF_FS_SIMMODE)\""
  elif [ $1 == "full" ]
  then
    MATCMD="matlab -r \"FlStart('trusted',$ATF_FS_SIMMODE)\""
  elif [ $1 == "debug" ]
  then
    MATCMD="matlab"
  elif [ $1 == "mdw" ]
  then
    MATCMD="matlab -nodesktop -nosplash"
  else
    MATCMD="matlab -nodesktop -nosplash -r \"FlStart('trusted',$ATF_FS_SIMMODE)\""
  fi
  if [ -z `echo $OS | grep -i windows` ]
  then # unix / mac
    if [ -z `echo $SHELL | grep bash` ]
    then # native csh
      xterm -T "ATF2 FS (Server)" -n "ATF2 FS (Server)" -bg darkgreen -fg white -e /bin/tcsh -c "$MATCMD"
    else # native bash
      xterm -T "ATF2 FS (Server)" -n "ATF2 FS (Server)" -bg darkgreen -fg white -e /bin/bash -i -l -c "$MATCMD"
    fi
  else # windows
    # check labca stuff on search path
    if [ -z `which lcaGet.mexw32` ]
    then
      export PATH=$PATH:../../../ATF2/control-software/epics-3.14.8/support/labca_3_0_beta/bin/win32-x86/labca
      export PATH=$PATH:../../../ATF2/control-software/epics-3.14.8/support/labca_3_0_beta/bin/win32-x86/
    fi
    # launch matlab natively under windows
    if [-z $1 ]
    then
      matlab -nosplash -nodesktop -r "FlStart('trusted',$ATF_FS_SIMMODE)"
    elif [ $1 == "full" ]
    then
      matlab -r "FlStart('trusted',$ATF_FS_SIMMODE)"
    elif [ $1 == "debug" ]
    then
      matlab
    else
      matlab -nosplash -nodesktop -r "FlStart('trusted',$ATF_FS_SIMMODE)"
    fi
  fi
fi


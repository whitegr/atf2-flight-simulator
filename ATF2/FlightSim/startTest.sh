#!/bin/bash
if [ -z $ATF_FS_SIMMODE ]
then
  echo "ATF_FS_SIMMODE environment variable not set"
else
    if [ -z `echo $SHELL | grep bash` ]
    then
      if [ -z $1 ]
      then
        xterm -T "ATF2 FS (Client)" -n "ATF2 FS (Client)" -bg lightblue -fg black -e /bin/tcsh -c "matlab -nodesktop -nosplash -r \"FlStart('test',1)\""
      else
        xterm -T "ATF2 FS (CLient)" -n "ATF2 FS (Client)" -bg lightblue -fg black -e /bin/tcsh -c "matlab -nodesktop -nosplash -r \"FlStart('test',1,'$1','$2')\""
      fi
    else
      if [ -z $1 ]
      then
        xterm -T "ATF2 FS (Client)" -n "ATF2 FS (Client)" -bg lightblue -fg black -e /bin/bash -i -l -c "matlab -nodesktop -nosplash -r \"FlStart('test',1)\""
      else
        xterm -T "ATF2 FS (CLient)" -n "ATF2 FS (Client)" -bg lightblue -fg black -e /bin/bash -i -l -c "matlab -nodesktop -nosplash -r \"FlStart('test',1,'$1','$2')\""
      fi
    fi
  fi
#

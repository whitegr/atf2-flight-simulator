#include "ServerSocket.h"
#include "SocketException.h"
#include "ClientSocket.h"
#include <string.h>
#include <string>
#include <iostream>
#include <stdlib.h>


using namespace std;

int main()
{

  // ---------------------------------------------------
  // Connect to Floodland server (listens on 8844)
  // ---------------------------------------------------


  cout << "Connecting to server...\n";
  ClientSocket fl_ServSocket ( "localhost", 8844 );
  string incomingData, reqID;
  long int commPort,ndata=0;

  sleep(2); // - wait for server to create new port
  // Get new port address from server
  cout << "Getting port number from server...\n";
  
  try { 
    fl_ServSocket >> incomingData;
  } catch ( SocketException& ) {}
  cout << "Reply: " << incomingData << "\n";
  cout << "Getting int...\n";
  commPort=atol(incomingData.c_str());

  // Once have port, don't need server socket any more- delete
  // it so other clients can connect
  cout << "deleting socket...\n";
  fl_ServSocket;

  // Check have good socket id
  if (commPort==0) {cout << "Bad port ID.. quitting\n"; return 0;}
  

  // ---------------------------------------------
  // Connect to comm socket on given port
  // ---------------------------------------------
  cout << "connecting to new socket...\n";
  ClientSocket fl_socket ( "localhost", commPort );

  
  // ----------------------------------------------
  // Send and receive test message
  // ----------------------------------------------
  // Send test message
  cout << "Sending: testcommand (expect reply: testresponse)\n";
  try {
    fl_socket << "testcommand;testcommand;";
  }
  catch ( SocketException& ) {return 0;}
  // Get reply
  fl_socket >> incomingData;
  cout << "Reply: " << incomingData << "\n";
  incomingData.clear();

  // ----------------------------------------------------
  // Request write access to some power supplies / movers
  // request is blocking (server will not respond to
  // subsequent requests until this request has been
  // responded to by Channel Access Server on 'trusted'
  // ----------------------------------------------------
  cout << "request data access\n";
  try {
    fl_socket << "access-request PS1 PS2 PS5 GIRDER28 GIRDER29 GIRDER30;";
  }
  catch ( SocketException& ) {return 0;}
  fl_socket >> incomingData;
  
  // Output is a request ID- use this to request access status
  cout << "Access request ID: " << incomingData << "\n";
  reqID=incomingData;
  incomingData.clear();

  // -------------------------------------------------------
  // Request status of access request
  // -------------------------------------------------------
  try {
    fl_socket << "access-status " << reqID << ";";
  }
  catch ( SocketException& ) {return 0;}
  fl_socket >> incomingData;

  cout << "Access status request response: " << incomingData << "\n";
  incomingData.clear();
  
  // ----------------------------------------------------------------
  // Read all bpms (SI Units)
  // (format = "Lucretia name"="model name" %.6e(x) %.6e(y) %.6(TMIT)
  // ----------------------------------------------------------------
  try {
    fl_socket << "bpmread;";
  }
  catch ( SocketException& ) {return 0;}
  fl_socket >> incomingData;
  cout << "BPM data reply: " << incomingData << "\n";
  incomingData.clear();

  // --------------------------------------------------------
  // Read selected bpm(s)
  // --------------------------------------------------------
  try {
    fl_socket << "bpmread([1 3 5 7]);";
  }
  catch ( SocketException& ) {return 0;}
  fl_socket >> incomingData;
  cout << "Selected BPM readouts: " << incomingData << "\n";
  incomingData.clear();
  
  
  // --------------------------------------------------------
  // Put ET BPMs into narrowband mode
  try {
    fl_socket << "FlET('SetMode','single');";
  }
  catch ( SocketException& ) {return 0;}
  fl_socket >> incomingData;
  cout << "Set ET to turn-by-turn mode: " << incomingData << "\n";
  incomingData.clear();
  // Tell client to update ET arrays from server
  try {
    fl_socket << "FlHwUpdate('bpmArrayReq',1);";
  }
  catch ( SocketException& ) {return 0;}
  fl_socket >> incomingData;
  cout << "Update ET arrays from server: " << incomingData << "\n";
  incomingData.clear();
  
  // --------------------------------------------------------
  // Read ET turn-by-turn data, or rms readings from
  // tbt or narrowband mode (mean values are accessed from
  // standard bpm readouts)
  // Data returned is for ET bpms only- you can get a list
  // of INSTR indicies by passing the command
  // "FlET('GetInstr')"
  // output for narrowband mode is:
  // [bpm1_x_rms bpm1_y_rms bpm1_tmit_rms bpm2_x_rms ...]
  // output for tbt mode is:
  // [bpm1_x_pulse1 bpm1_y_pulse1 bpm1_tmit_pulse1 bpm1_x_p2 ... bpmN_tmit_pN bpm2_x_p1 ...]
  // get cuurent mode with:
  // "FlET('GetMode')" - "single" = narrowband "multi" = tbt
  // change mode with:
  // "FlET('SetMode','single')" or "FlET('SetMode','multi')"
  // get number of pulses for tbt mode with:
  // "FlET('GetNRead')"
  // --------------------------------------------------------
  // Update client
  try {
    fl_socket << "FlHwUpdate;";
  }
  catch ( SocketException& ) {return 0;}
  fl_socket >> incomingData;
  cout << "FlHwUpdate: " << incomingData << "\n";
  incomingData.clear();
  // Read ET data
  try {
    fl_socket << "FlET('read');";
  }
  catch ( SocketException& ) {return 0;}
  fl_socket >> incomingData;
  cout << "ET RMS readings: " << incomingData << "\n";
  incomingData.clear();
  
  // --------------------------------------------------------
  // Read ET data from buffer
  // --------------------------------------------------------
  // Need to have selected the option in FS GUI to store ET
  // data in the buffer - this option is located in the
  // "BPM Tool" GUI at the bottom of the display
  // can specify the number of machine pulses to acquire
  // with the 4th argument to bpmave() - get current buffer
  // size with "bpmave('buffersize')"
  //  "ReadErrBuffer" format: (mp = machine pulse; rt = ring turn)
  // [bpm1_x_rms_mp1 bpm1_x_rms_mp2 ... bpm1_x_rms_mpN bpm1_y_rms_mp1 ...]
  // "ReadArrBuffer" format:
  // [bpm1_x_rt1_mp1 bpm1_x_rt1_mp2 ... bpm1_x_rt1_mpN bpm1_x_rt2_mp1 ...]
  // (ie machine pulse number index changes fastest, then ring
  // turn, then bpm x/y/tmit, then bpm number)
  try {
    fl_socket << "bpmave('readerrbuffer',[],[],3);";
  }
  catch ( SocketException& ) {return 0;}
  fl_socket >> incomingData;
  cout << "ET RMS buffer readings: " << incomingData << "\n";
  cout << "(If error, make sure 'buffer tbt data' selected on Server BPM Tool GUI)\n";
  incomingData.clear();
  try {
    fl_socket << "bpmave('readarrbuffer',[],[],3);";
  }
  catch ( SocketException& ) {return 0;}
  fl_socket >> incomingData;
  cout << "ET tbt buffer readings: " << incomingData << "\n";
  cout << "(If error, make sure 'buffer tbt data' selected on Server BPM Tool GUI)\n";
  incomingData.clear();
  
  
  // --------------------------------------------------------
  // Request buffered bpm proccessing
  // --------------------------------------------------------
  // Get number of buffered BPM readings available, and wait
  // until we can average 20
  cout << "waiting for 20 buffered BPM readings available\n";
  while (ndata<20) {
    try {
      fl_socket << "bpmave('buffersize');";
    }
    catch ( SocketException& ) {return 0;}
    fl_socket >> incomingData;
    // cout << incomingData << "\n";
    cout << "N data: " << atol(incomingData.c_str()+14) << "\n";
    ndata=atol(incomingData.c_str()+14);
  }
  incomingData.clear();
  // Get mean and RMS for last N pulses
  // Quality cuts applied: [minq maxRMS whichICT returnBadBpms] (defaults: [0.5 3 1 0])
  //minq: min charge cut (pulses dropped with charge as measured on
  //dump ICT < this fraction of max charge for N pulse window)
  //maxRMS: fliers > than this calculated RMS removed from average
  //whichICT: 0=ICTDUMP 1=ICT1X
  // Command format: bpmave(N,force N,<[optional bpm list],
  // [optional cuts]>) - default cuts = [0.5 3 1 0]
  // output format:
  // ["Lucretia name"="model name" %.6e(x_mean) %.6e(y_mean) %.6(TMIT_mean)
  //  %.6e(x_rms) %.6e(y_rms) %.6(TMIT_rms) N_bad (x) N_bad (y) N_bad (TMIT)]*nbpm
  try {
    fl_socket << "bpmave(20,0);";
  }
  catch ( SocketException& ) {return 0;}
  fl_socket >> incomingData;
  cout << "bpmave response: " << incomingData << "\n";
  incomingData.clear();
 
  // Output complete bpm buffer (Warning: can be slow)
  // optional second argument=bpm list
  // optional third argument= number of bpm data readings required
  // ouput format:
  // ["Lucretia name"="model name" [%d(pulse id) %.6e(x) %.6e(y) %.6(TMIT)]*npulse ...]*nbpm
  cout << "Requesting dump of bpm buffer\n";
  try {
    fl_socket << "bpmave('readbuffer',[],[],3);";
  }
  catch ( SocketException& ) {return 0;}
  fl_socket >> incomingData;
  cout << "bpm buffer dump response: " << incomingData << "\n";
  incomingData.clear();
  // Flush BPM buffer of stored readings
  cout << "Requesting flush of bpm buffer\n";
  try {
    fl_socket << "bpmave('flush');";
  }
  catch ( SocketException& ) {return 0;}
  fl_socket >> incomingData;
  cout << "bpm buffer flush response: " << incomingData << "\n";
  incomingData.clear();

  
  // --------------------------------------------------------
  // Read a PS setting (amlget)
  // --------------------------------------------------------
  try {
    fl_socket << "amlget('PS1:design:num','PS2:design:num','PS5:design:num');";
  }
  catch ( SocketException& ) {return 0;}
  fl_socket >> incomingData;
  cout << "PS readouts: " << incomingData << "\n";
  incomingData.clear();

  // --------------------------------------------------------
  // Read B fields (if first call) or any B field changes
  // Output format (Lucretia BEAMLINE Index):
  //   1=1.2,2.3]2=1.34]27=0.123 etc...
  // (comma separated vals when 2 B field Vals (SBENs)
  // --------------------------------------------------------
  try {
    fl_socket << "BUpdate;";
  }
  catch ( SocketException& ) {return 0;}
  fl_socket >> incomingData;
  cout << "B fields: " << incomingData << "\n";
  incomingData.clear();
  
  // --------------------------------------------------------
  // Read a Mover setting (amlget)
  // --------------------------------------------------------
  try {
    fl_socket << "amlget('GIRDER28:orientation:x_offset:design','GIRDER29:orientation:y_offset:design','GIRDER30:orientation:tilt:design');";
  }
  catch ( SocketException& ) {return 0;}
  fl_socket >> incomingData;
  cout << "Mover readouts: " << incomingData << "\n";
  incomingData.clear();  

  // --------------------------------------------------------
  // Write to a PS (Must have already gained access) (amlset)
  // --------------------------------------------------------
  try {
    fl_socket << "amlset('PS1:design:num',0.12,'PS2:design:num',0.96,'PS5:design:num',0.5e-3);";
  }
  catch ( SocketException& ) {return 0;}
  fl_socket >> incomingData;
  // Returns status (1= successfull)
  cout << "amlset return stat: " << incomingData << "\n";
  incomingData.clear();
  sleep(2);

  // --------------------------------------------------------
  // Change Mover position (Must have already gained access) (amlset)
  // --------------------------------------------------------
  try {
    fl_socket << "amlset('GIRDER28:orientation:x_offset:design',10e-6,'GIRDER29:orientation:y_offset:design',-32e-6,'GIRDER30:orientation:tilt:design',1e-3);";
  }
  catch ( SocketException& ) {return 0;}
  fl_socket >> incomingData;
  // Returns status (1= successfull)
  cout << "amlset return stat: " << incomingData << "\n";
  incomingData.clear();
  sleep(2);

  // --------------------------------------------------------
  // Update Values in Floodland from trusted server
  // --------------------------------------------------------
  try {
    fl_socket << "FlHwUpdate;";
  }
  catch ( SocketException& ) {return 0;}
  fl_socket >> incomingData;
  cout << "FlHwUpdate: " << incomingData << "\n";
  incomingData.clear();
  // --------------------------------------------------------
  // Read in PS to see if changed
  // --------------------------------------------------------
  try {
    fl_socket << "amlget('PS1:design:num','PS2:design:num','PS5:design:num');";
  }
  catch ( SocketException& ) {return 0;}
  fl_socket >> incomingData;
  cout << "New PS readouts: " << incomingData << "\n";
  incomingData.clear();

  // --------------------------------------------------------
  // Read a Mover setting to see if changed
  // --------------------------------------------------------
  try {
    fl_socket << "amlget('GIRDER28:orientation:x_offset:design','GIRDER29:orientation:y_offset:design','GIRDER30:orientation:tilt:design');";
  }
  catch ( SocketException& ) {return 0;}
  fl_socket >> incomingData;
  cout << "Mover readouts: " << incomingData << "\n";
  incomingData.clear();  

  // --------------------------------------------------------
  // Share some data with other Flight Simulator apps
  // - all data in DataStore is saved and available in
  // all user environments
  // Format: FlDataStore('source','datafield1',<dataval(s)1>,
  //                   'datafield2',<dataval(s)2>,...)
  // --------------------------------------------------------
  // Write to data store e.g.
  // Fist arg of datastore is a user-defined flag to reference your data
  // data store takes data in <field, value> pairs
  // only support double and char arrays at the moment
  
  try {
    fl_socket << "FlDataStore('PLACET','myvals',[1.3 12.3],'morevals','Lucretia Is The Best');";
  }
  catch ( SocketException& ) {return 0;}
  fl_socket >> incomingData;
  cout << "DataStore Write Response: " << incomingData << "\n";
  incomingData.clear();
  // Read from data store e.g.
  try {
    fl_socket << "FlDataStore('PLACET');";
  }
  catch ( SocketException& ) {return 0;}
  fl_socket >> incomingData;
  cout << "DataStore Read Response: " << incomingData << "\n";
  incomingData.clear();
  
  // --------------------------------------------------------
  // Do something using Lucretia
  // e.g. use graphical beamline viewer
  // --------------------------------------------------------
  
  try {
    fl_socket << "BeamlineViewer;";
  }
  catch ( SocketException& ) {return 0;}
  fl_socket >> incomingData;
  // Returns status (1= successfull)
  cout << "launching beamline viewer: " << incomingData << "\n";
  incomingData.clear();
  
  // ---------------------------------------------------------
  // Release access control of previously requested elements
  // ---------------------------------------------------------
  try {
    fl_socket << "access-release " << reqID << ";";
  }
  catch ( SocketException& ) {return 0;}
  fl_socket >> incomingData;
  cout << "Access release request Reply: " << incomingData << "\n";
  incomingData.clear();
  
  // --------------------------------------------------------
  // Send Disconnect Command - !MUST DO THIS BEFORE QUITTING!
  // Also include this step in any error catch routines
  // before quitting in error
  // --------------------------------------------------------
  sleep(2);
  cout << "Disconnecting...\n";
  try {
    fl_socket << "disconnect;";
  }
  catch ( SocketException& ) {return 0;}

  // Close socket
  fl_socket;

  return 0;
}


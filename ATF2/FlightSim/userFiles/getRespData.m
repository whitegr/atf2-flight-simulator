mags={'ZH10X' 'ZH1FF' 'ZV11X' 'ZV1FF' 'QM16FF' 'QM15FF' 'QM14FF' 'QM13FF' 'QM12FF' ...
  'QM11FF' 'QD10BFF' 'QD10AFF' 'QF9BFF' 'QF9AFF' 'QD8FF' 'QF7FF' 'QD6FF' 'QF5BFF' ...
  'QF5AFF' 'QD4BFF' 'QD4AFF' 'QF3FF' 'QD2BFF' 'QD2AFF' 'QF1FF' 'QD0FF'};
nbpm=20;
nsp=20;
%
for imag=1:length(mags)
  mag=mags{imag};
  iele=findcells(BEAMLINE,'Name',mag);iele=iele(1);
  if mag(1)=='Z'
    ips=BEAMLINE{iele}.PS;
    orig=PS(ips).Ampl;
    if mag(2)=='H'
      npl=1;
    else
      npl=2;
    end
    spoints=linspace(-1e-5,1e-5,nsp);
  else
    spoints=linspace(-500,500,nsp);
    igir=BEAMLINE{iele}.Girder;
    orig=GIRDER{igir}.MoverPos;
    npl=1:2;
  end
  bpmdataSave=cell(length(npl),length(spoints));
  pl='xy';
  for ipl=npl
    for iscan=1:length(spoints)
      fprintf('%s (%c) scan %d of %d\n',mag,pl(ipl),iscan,length(spoints))
      if mag(1)=='Z'
        PS(ips).SetPt=orig+spoints(iscan); stat=PSTrim(ips,1); if stat{1}~=1; error(stat{2}); end; pause(2);
      else
        GIRDER{igir}.MoverSetPt(ipl)=orig(ipl)+spoints(iscan); stat=MoverTrim(igir,3); if stat{1}~=1; error(stat{2}); end;
      end
      FlHwUpdate('wait',nbpm);
      [stat bpmdata]=FlHwUpdate('readbuffer',nbpm);
      if length(npl)>1
        bpmdataSave{ipl,iscan}=bpmdata;
      else
        bpmdataSave{iscan}=bpmdata;
      end
    end
  end
  if mag(1)=='Z'
    PS(ips).SetPt=orig; stat=PSTrim(ips,1); if stat{1}~=1; disp(stat{2}); end;
  else
    GIRDER{igir}.MoverSetPt(ipl)=orig(ipl); stat=MoverTrim(igir,3); if stat{1}~=1; disp(stat{2}); end;
  end
  save(sprintf('userData/bpmResData_%s',mag),'bpmdataSave','BEAMLINE','PS')
end
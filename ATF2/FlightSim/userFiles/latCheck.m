clear all global
load ../../FlightSim/latticeFiles/src/v4.5/ATF2lat_BX10BY1
% multswap

names={'QM16FF' 'QM15FF' 'QM14FF' 'QM13FF' 'QM12FF' 'QM11FF' 'QF7FF' 'SF5FF' 'QD2BFF' 'QD2AFF' 'SF1FF'};
diffPS=[0.728 2.12 0.782 0.881 4.63 0 1.86 40.4 0.807 1.26 1.22];

for ibl=1:length(names)
  ele=findcells(BEAMLINE,'Name',names{ibl});
  PS(BEAMLINE{ele(1)}.PS).Ampl=diffPS(ibl);
%   PS(BEAMLINE{ele(1)}.PS).Ampl
end

% SEXT off
for ips=50:54
%   PS(ips).Ampl=0;
end
% sf6=findcells(BEAMLINE,'Name','SD0FF'); GIRDER{BEAMLINE{sf6(1)}.Girder}.MoverPos(1)=0e-3;
% qk=findcells(BEAMLINE,'Name','SK1FF'); PS(BEAMLINE{qk(1)}.PS).Ampl=0; 

ip=findcells(BEAMLINE,'Name','IP');

% 16 -11.2
% otr0=findcells(BEAMLINE,'Name','OTR0X');
% % Model.Initial.x.NEmit=9.53e-6;
% % Model.Initial.y.NEmit=6.3e-8;
% % Model.Initial.x.Twiss.alpha=1.2;
% % Model.Initial.x.Twiss.beta=14.5;
% Model.Initial.SigPUncorrel=8e-4;
% Beam1=MakeBeam6DGauss(Model.Initial,2.1e4,5,0);
% xi.beta=Model.Initial.x.Twiss.beta;
% xi.alpha=Model.Initial.x.Twiss.alpha;
% xi.eta=Model.Initial.x.Twiss.eta;
% xi.etap=Model.Initial.x.Twiss.etap;
% xi.nu=Model.Initial.x.Twiss.nu;
% yi.beta=Model.Initial.y.Twiss.beta;
% yi.alpha=Model.Initial.y.Twiss.alpha;
% yi.eta=Model.Initial.y.Twiss.eta;
% yi.etap=Model.Initial.y.Twiss.etap;
% yi.nu=Model.Initial.y.Twiss.nu;
% [STAT,T] = GetTwiss( 1, otr0, xi, yi );
% T.betax(otr0)
% T.alphax(otr0)

% Find waists
qf1=findcells(BEAMLINE,'Name','QF1FF'); i1=qf1(1)-1; qf1=BEAMLINE{qf1(1)}.PS;
qd0=findcells(BEAMLINE,'Name','QD0FF'); qd0=BEAMLINE{qd0(1)}.PS;
qq=linspace(0.99,1.01,20);
[stat bo]=TrackThru(1,i1,Beam1,1,1,0);
for iqq=1:length(qq)
  PS(qf1).Ampl=qq(iqq);
  [stat bo1]=TrackThru(i1,ip,bo,1,1,0);
  xq(iqq)=std(bo1.Bunch.x(1,:));
end
figure
qfit=plot_parab(qq,xq.^2,1);
disp(qfit(2))
PS(qf1).Ampl=qfit(2);
for iqq=1:length(qq)
  PS(qd0).Ampl=qq(iqq);
  [stat bo1]=TrackThru(i1,ip,bo,1,1,0);
  yq(iqq)=std(bo1.Bunch.x(3,:));
end
figure
qfit=plot_parab(qq,yq.^2,1);
disp(qfit(2))
PS(qd0).Ampl=qfit(2);

data = GetIPData(Beam1,1,ip);
sx=data.xfit
sy=data.yfit
wx=data.xwaist
wy=data.ywaist
% Get data at waist
BEAMLINE{ip-2}.L=wx;
data = GetIPData(Beam1,1,ip);
bx=data.betax
sxw=data.xfit
sxwrms=data.xsize
BEAMLINE{ip-2}.L=wy;
data = GetIPData(Beam1,1,ip);
by=data.betay
syw=data.yfit
sywrms=data.ysize
BEAMLINE{ip-2}.L=0;
data.sigma(3,2)
data.sigma(3,1)
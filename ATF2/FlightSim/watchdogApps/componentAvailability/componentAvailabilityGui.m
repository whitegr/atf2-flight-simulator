function varargout = componentAvailabilityGui(varargin)
% COMPONENTAVAILABILITYGUI M-file for componentAvailabilityGui.fig
%      COMPONENTAVAILABILITYGUI, by itself, creates a new COMPONENTAVAILABILITYGUI or raises the existing
%      singleton*.
%
%      H = COMPONENTAVAILABILITYGUI returns the handle to a new COMPONENTAVAILABILITYGUI or the handle to
%      the existing singleton*.
%
%      COMPONENTAVAILABILITYGUI('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in COMPONENTAVAILABILITYGUI.M with the given input arguments.
%
%      COMPONENTAVAILABILITYGUI('Property','Value',...) creates a new COMPONENTAVAILABILITYGUI or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before componentAvailabilityGui_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to componentAvailabilityGui_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help componentAvailabilityGui

% Last Modified by GUIDE v2.5 20-Nov-2009 15:46:37

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @componentAvailabilityGui_OpeningFcn, ...
                   'gui_OutputFcn',  @componentAvailabilityGui_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before componentAvailabilityGui is made visible.
function componentAvailabilityGui_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to componentAvailabilityGui (see VARARGIN)

% Choose default command line output for componentAvailabilityGui
handles.output = handles;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes componentAvailabilityGui wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% Restore any settings from last time
restoreList={'radiobutton1','Value',0; 'radiobutton2','Value',0;'radiobutton3','Value',0;
             'radiobutton4','Value',0; 'radiobutton5','Value',0;'radiobutton6','Value',0;'radiobutton7','Value',0;
             'radiobutton12','Value',0; 'radiobutton13','Value',0;'radiobutton14','Value',0};
guiRestoreFn('compareAvailabilityGui',handles,restoreList);

% Update lisbox display
pushbutton4_Callback(handles.pushbutton4,[],handles);


% --- Outputs from this function are returned to the command line.
function varargout = componentAvailabilityGui_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on selection change in listbox1.
function listbox1_Callback(hObject, eventdata, handles)
% hObject    handle to listbox1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = get(hObject,'String') returns listbox1 contents as cell array
%        contents{get(hObject,'Value')} returns selected item from listbox1


% --- Executes during object creation, after setting all properties.
function listbox1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to listbox1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: listbox controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- EXIT
function pushbutton1_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
componentAvailability('Save');
guiCloseFn('componentAvailabilityGui',handles);

% --- Toggle HW require
function pushbutton2_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
[stat pars]=componentAvailability('GetPars');
if stat{1}~=1
  errordlg(stat{2},'GetPars failed')
  return
end
listSel=get(handles.listbox1,'Value');
listStr=get(handles.listbox1,'String');
types={'GIRDER' 'INSTR' 'PS'}; typeSel='GIP';
for i_ind=listSel
  indList=str2double(regexp(listStr{i_ind},'\d+','match','once'));
  t=regexp(listStr{i_ind},'\((\w)','tokens','once');
  typeList=types{ismember(typeSel,t{1})};
  toggleInd=ismember(pars.ind,indList)&ismember(pars.types,typeList);
  pars.requireHW(toggleInd)=~pars.requireHW(toggleInd);
end
newpars.requireHW=pars.requireHW;
componentAvailability('SetPars',newpars);
% Update display list
pushbutton4_Callback(handles.pushbutton4,[],handles);

% --- Toggle Tol Require
function pushbutton3_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
[stat pars]=componentAvailability('GetPars');
if stat{1}~=1
  errordlg(stat{2},'GetPars failed')
  return
end
listSel=get(handles.listbox1,'Value');
listStr=get(handles.listbox1,'String');
types={'GIRDER' 'INSTR' 'PS'}; typeSel='GIP';
for i_ind=listSel
  indList=str2double(regexp(listStr{i_ind},'\d+','match','once'));
  t=regexp(listStr{i_ind},'\((\w)','tokens','once');
  typeList=types{ismember(typeSel,t{1})};
  toggleInd=ismember(pars.ind,indList)&ismember(pars.types,typeList);
  pars.requireTol(toggleInd)=~pars.requireTol(toggleInd);
end
newpars.requireTol=pars.requireTol;
componentAvailability('SetPars',newpars);
% Update display list
pushbutton4_Callback(handles.pushbutton4,[],handles);

% --- Display only alaram states
function radiobutton4_Callback(hObject, eventdata, handles)
% hObject    handle to radiobutton4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
Update(hObject, eventdata, handles)

% Update display
function Update(hObject, eventdata, handles)
pushbutton4_Callback(handles.pushbutton4,[],handles);


% --- Executes on button press in radiobutton5.
function radiobutton5_Callback(hObject, eventdata, handles)
% hObject    handle to radiobutton5 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of radiobutton5


% --- Executes on button press in radiobutton6.
function radiobutton6_Callback(hObject, eventdata, handles)
% hObject    handle to radiobutton6 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of radiobutton6


% --- Executes on button press in radiobutton7.
function radiobutton7_Callback(hObject, eventdata, handles)
% hObject    handle to radiobutton7 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of radiobutton7


% --- Display PS select
function radiobutton1_Callback(hObject, eventdata, handles)
% hObject    handle to radiobutton1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Can't unselect all
if ~any([get(handles.radiobutton1,'Value') get(handles.radiobutton2,'Value') get(handles.radiobutton3,'Value')])
  set(hObject,'Value',1)
end

% Update display
pushbutton4_Callback(handles.pushbutton4,[],handles);


% --- Display GIRDER select
function radiobutton2_Callback(hObject, eventdata, handles)
% hObject    handle to radiobutton2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Can't unselect all
if ~any([get(handles.radiobutton1,'Value') get(handles.radiobutton2,'Value') get(handles.radiobutton3,'Value')])
  set(hObject,'Value',1)
end

% Update display
pushbutton4_Callback(handles.pushbutton4,[],handles);



% --- Display INSTR select
function radiobutton3_Callback(hObject, eventdata, handles) %#ok<*INUSD,*DEFNU>
% hObject    handle to radiobutton3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Can't unselect all
if ~any([get(handles.radiobutton1,'Value') get(handles.radiobutton2,'Value') get(handles.radiobutton3,'Value')])
  set(hObject,'Value',1)
end

% Update display
pushbutton4_Callback(handles.pushbutton4,[],handles);



% --- Update button
function pushbutton4_Callback(hObject, eventdata, handles) %#ok<*INUSL>
% hObject    handle to pushbutton4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global FL

% Get options
dopt={};
if get(handles.radiobutton1,'Value'); dopt{end+1}=get(handles.radiobutton1,'String'); end;
if get(handles.radiobutton2,'Value'); dopt{end+1}=get(handles.radiobutton2,'String'); end;
if get(handles.radiobutton3,'Value'); dopt{end+1}=get(handles.radiobutton3,'String'); end;
if get(handles.radiobutton4,'Value')
  alarmopt=true;
else
  alarmopt=false;
end
sortopt=get(handles.popupmenu1,'Value');

% Get new data from function an pars
[stat data]=componentAvailability('Update');
if stat{1}~=1
  errordlg(stat{2},'Update failed')
  return
end
[stat pars]=componentAvailability('GetPars');
if stat{1}~=1
  errordlg(stat{2},'GetPars failed')
  return
end

% Set general alarm state
if data.alarm
  set(handles.text3,'BackgroundColor','red')
else
  set(handles.text3,'BackgroundColor','green')
end

% Display update rate
set(handles.edit1,'String',num2str(pars.updateRate))

% Display requested elemets
if alarmopt
  ele=ismember(pars.types,dopt) & (data.alarmHW | any(data.alarmTol));
else
  ele=ismember(pars.types,dopt);
end
if sortopt==1
  eleSort=1:sum(ele);
elseif sortopt==2
  [~, eleSort]=sort(pars.types(ele));
elseif sortopt==3
  [~, eleSort]=sort(data.HW(ele));
elseif sortopt==4
  [~, eleSort]=sort(base2dec(num2str(data.Tol(ele)'),3));
elseif sortopt==5
  [~, eleSort]=sort(pars.requireHW(ele));
elseif sortopt==6
  [~, eleSort]=sort(pars.requireTol(ele));
else
  errordlg('Unknown sort option','Update failed')
  return
end
displayStr={}; yesno={'No' 'Yes'};
ele=find(ele);
for iele=ele(eleSort)
%   if alarmopt && (data.alarmHW(iele) || any(data.alarmTol(iele))); continue; end;
  if pars.ind(iele)>=FL.Region.DR.ind(1) && pars.ind(iele)<=FL.Region.DR.ind(2) && ~get(handles.radiobutton12,'Value')
    continue
  end
  if pars.ind(iele)>=FL.Region.EXT.ind(1) && pars.ind(iele)<=FL.Region.EXT.ind(2) && ~get(handles.radiobutton13,'Value')
    continue
  end
  if pars.ind(iele)>=FL.Region.FFS.ind(1) && pars.ind(iele)<=FL.Region.FFS.ind(2) && ~get(handles.radiobutton14,'Value')
    continue
  end
  if data.HW(iele)
    hwavail='No';
  else
    hwavail='Yes';
  end
    hwtol='000';
    hwtol(data.Tol(:,iele)==1)='L';
    hwtol(data.Tol(:,iele)==2)='H';
    displayStr{end+1}=sprintf('%4d %20s (%c%4d) |    %3s    |  %3s  |    %3s    |   %3s',...
    pars.ind(iele),pars.names{iele}(1:min(length(pars.names{iele}),20)),pars.types{iele}(1),pars.typeInd(iele),hwavail,hwtol,...
    yesno{pars.requireHW(iele)+1},yesno{pars.requireTol(iele)+1}); %#ok<AGROW>
end
origDispSel=get(handles.listbox1,'Value');
origDispStr=get(handles.listbox1,'String');
set(handles.listbox1,'ListboxTop',1);
set(handles.listbox1,'String',displayStr)
newDispStr=get(handles.listbox1,'String');
if ~isempty(origDispStr) && ~isempty(newDispStr) && ~isempty(origDispSel)
  set(handles.listbox1,'Value',find(ismember(newDispStr,origDispStr{origDispSel(1)})))
else
  set(handles.listbox1,'Value',1)
end

% Update rate
function edit1_Callback(hObject, eventdata, handles)
% hObject    handle to edit1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

if ~isnan(str2double(get(hObject,'String')))
  pars.updateRate=str2double(get(hObject,'String'));
end
componentAvailability('SetPars',pars);


% --- Executes during object creation, after setting all properties.
function edit1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Launch details gui
function pushbutton5_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton5 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global FL
listStr=get(handles.listbox1,'String');
listSel=get(handles.listbox1,'Value');
FL.Gui.detailsGui=detailsGui('UserData',listStr(listSel));

% --- Display sort select
function popupmenu1_Callback(hObject, eventdata, handles)
% hObject    handle to popupmenu1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Update display
pushbutton4_Callback(handles.pushbutton4,[],handles);


% --- Executes during object creation, after setting all properties.
function popupmenu1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to popupmenu1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes when user attempts to close figure1.
function figure1_CloseRequestFcn(hObject, eventdata, handles)
% hObject    handle to figure1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
componentAvailability('Save');
guiCloseFn('componentAvailabilityGui',handles);


% --- Select DR
function radiobutton12_Callback(hObject, eventdata, handles)
% hObject    handle to radiobutton12 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Update display
pushbutton4_Callback(handles.pushbutton4,[],handles);

% --- Select EXT
function radiobutton13_Callback(hObject, eventdata, handles)
% hObject    handle to radiobutton13 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Update display
pushbutton4_Callback(handles.pushbutton4,[],handles);


% --- Select FFS
function radiobutton14_Callback(hObject, eventdata, handles)
% hObject    handle to radiobutton14 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Update display
pushbutton4_Callback(handles.pushbutton4,[],handles);


% --- Executes on button press in pushbutton6.
function pushbutton6_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton6 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global BEAMLINE INSTR
listSel=get(handles.listbox1,'Value');
listStr=get(handles.listbox1,'String');
types={'GIRDER' 'INSTR' 'PS'}; typeSel='GIP';
for i_ind=listSel
  indList=str2double(regexp(listStr{i_ind},'\d+','match','once'));
  t=regexp(listStr{i_ind},'\((\w)','tokens','once');
  typeList=types{ismember(typeSel,t{1})};
  if strcmp(typeList,'GIRDER')
    FlCA('restore','GIRDER',BEAMLINE{indList}.Girder);
  elseif strcmp(typeList,'PS')
    FlCA('restore','PS',BEAMLINE{indList}.PS);
  else
    FlCA('restore','INSTR',findcells(INSTR,'Index',indList));
  end
end
pushbutton4_Callback(handles.pushbutton4,[],handles);

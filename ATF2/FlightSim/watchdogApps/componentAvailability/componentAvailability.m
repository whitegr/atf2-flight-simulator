function [stat output] = componentAvailability(cmd,varargin)
% [stat output] = componentAvailability(cmd,varargin)
% Watchdog function for keeping check on h/w and tolerance status of all
% hardware devices
persistent pars data lastupdate
global FL
stat{1}=1; output=[];

% Make sure initialised
if isempty(pars) && ~isequal(cmd,'init')
  [stat pars]=initPars; if stat{1}~=1; return; end;
  [stat data]=fetchData(pars);
end

% Methods
switch lower(cmd)
  case 'init'
    [stat pars]=initPars; if stat{1}~=1; return; end;
    [stat data]=fetchData(pars);
  case 'reset'
    [stat pars]=initPars; if stat{1}~=1; return; end;
    [stat data]=fetchData(pars);
  case 'isalarm'
    if isempty(lastupdate) || etime(clock,lastupdate)>pars.updateRate
      [stat data]=fetchData(pars);
      if stat{1}~=1; return; end;
      lastupdate=clock;
    end
    output=data.alarm*2;
  case 'getpars'
    output=pars;
  case 'setpars'
    if nargin>0
      fn=fieldnames(varargin{1});
      for ifn=1:length(fn)
        pars.(fn{ifn})=varargin{1}.(fn{ifn});
      end
    end
  case 'update'
    [stat data]=fetchData(pars);
    if stat{1}~=1; return; end;
    output=data;
  case 'getdata'
    output=data;
  case 'save'
      save(fullfile('userData','watchdogData',['componentAvailability_',datestr(now,FL.tstamp),'.mat']),'pars','data');
      save(fullfile('userData','watchdogData','componentAvailability.mat'),'pars','data');
  otherwise
    stat{1}=-1; stat{2}='Unknown command';
    return
end

%% Local functions
function [stat data] = fetchData(pars)
global PS GIRDER INSTR FL
stat{1}=1;

% Get current data values
instType=ismember(pars.types,'INSTR');
psType=ismember(pars.types,'PS');
girType=ismember(pars.types,'GIRDER');
data.val(1:3,instType)=reshape(cell2mat(arrayfun(@(x) INSTR{x}.Data(1:3),pars.typeInd(instType),'UniformOutput',false)),3,sum(instType));
data.val(1,psType)=arrayfun(@(x) PS(x).Ampl(1),pars.typeInd(psType));
data.val(1:3,girType)=reshape(cell2mat(arrayfun(@(x) GIRDER{x}.MoverPos(1:3),pars.typeInd(girType),'UniformOutput',false)),3,sum(girType));
% Get and subtract reference value
if pars.tolRef==1
  if isfield(FL.HwInfo.PS,'ref')
    refVal(1,psType)=cell2mat(arrayfun(@(x) x.ref(1),FL.HwInfo.PS(pars.typeInd(psType)),'UniformOutput',false));
  else
    refVal(1,psType)=pars.defRef(1,psType);
  end
  %091208 JLN there is never fl.hwinfo.girder.ref, I think this should
  % point to girder.MoverSetPt  
  %
  % There's never a fl.hwinfo.ps.ref either.
  %
  % INSTR does have a ref field
  if isfield(FL.HwInfo.GIRDER,'ref')
    refVal(1:3,girType)=reshape(cell2mat(arrayfun(@(x) x.ref(1:3),FL.HwInfo.GIRDER(pars.typeInd(girType)),'UniformOutput',false)),3,sum(girType));
  else
    refVal(1:3,girType)=pars.defRef(1:3,girType);
  end
  if isfield(INSTR{1},'ref')
    refVal(1:3,instType)=reshape(cell2mat(cellfun(@(x) x.ref(1:3),{INSTR{pars.typeInd(instType)}},'UniformOutput',false)),3,sum(instType)); %#ok<*CCAT1>
  else
    refVal(1:3,instType)=pars.defRef(1:3,instType);
  end
else
  refVal=pars.defRef;
end
% Get alarm states
data.alarmTol(1,:)=pars.requireTol.*((data.val(1,:)-refVal(1,:)<pars.tolLow(1,:)).*1+(data.val(1,:)-refVal(1,:)>pars.tolHigh(1,:)).*2);
data.alarmTol(2:3,:)=[~psType.*pars.requireTol;~psType.*pars.requireTol].*((data.val(2:3,:)-refVal(2:3,:)<pars.tolLow(2:3,:)).*1+(data.val(2:3,:)-refVal(2:3,:)>pars.tolHigh(2:3,:)).*2);
data.Tol(1,:)=((data.val(1,:)-refVal(1,:)<pars.tolLow(1,:)).*1+(data.val(1,:)-refVal(1,:)>pars.tolHigh(1,:)).*2);
data.Tol(2:3,:)=[~psType;~psType].*((data.val(2:3,:)-refVal(2:3,:)<pars.tolLow(2:3,:)).*1+(data.val(2:3,:)-refVal(2:3,:)>pars.tolHigh(2:3,:)).*2);
% Use no PV name entry as sign of no hardware
data.alarmHW(psType)=pars.requireHW(psType) & arrayfun(@(x) isempty(x.pvname),FL.HwInfo.PS(pars.typeInd(psType)));
data.alarmHW(girType)=pars.requireHW(girType) & arrayfun(@(x) isempty(x.pvname),FL.HwInfo.GIRDER(pars.typeInd(girType)));
data.alarmHW(instType)=pars.requireHW(instType) & arrayfun(@(x) isempty(x.pvname),FL.HwInfo.INSTR(pars.typeInd(instType)));
data.HW(psType)=arrayfun(@(x) isempty(x.pvname),FL.HwInfo.PS(pars.typeInd(psType)));
data.HW(girType)=arrayfun(@(x) isempty(x.pvname),FL.HwInfo.GIRDER(pars.typeInd(girType)));
data.HW(instType)=arrayfun(@(x) isempty(x.pvname),FL.HwInfo.INSTR(pars.typeInd(instType)));
data.alarm = any(data.alarmHW) | any(any(data.alarmTol));

function [stat pars] = initPars()
global GIRDER BEAMLINE INSTR PS
stat{1}=1;

% Load any saved data
try
  ld=load(fullfile('userData','watchdogData','componentAvailability.mat'));
catch
  ld=[];
end

% Form lists etc
typeNames={'GIRDER' 'PS' 'INSTR'};
typeIndGir=findcells(GIRDER,'Mover');
moverList=cellfun(@(x) x.Element(1),{GIRDER{typeIndGir}});
typeList=ones(1,length(moverList)).*1;
psList=arrayfun(@(x) x.Element(1),PS); usePs=psList~=0;
psList=psList(usePs);
typeList=[typeList ones(1,length(psList)).*2];
typeIndPs=find(usePs);
instrList=cellfun(@(x) x.Index(1),INSTR);
typeList=[typeList ones(1,length(instrList)).*3];
[eleList I]=sort([moverList psList instrList]);
typeIndInstr=1:length(INSTR);
typeInd=[typeIndGir typeIndPs typeIndInstr];

% Default parameters
pars.types=arrayfun(@(x) typeNames{x},typeList(I),'UniformOutput',false);
pars.names=cellfun(@(x) x.Name,{BEAMLINE{eleList}},'UniformOutput',false);
pars.ind=eleList;
pars.typeInd=typeInd(I);
pars.requireHW=ones(size(pars.ind));
pars.requireTol=ones(size(pars.ind));
pars.tolRef=1; % 1=saved ref 2=default ref
pars.updateRate=60; % s
pars.active=true;

% Load in compatible parameters from save file if any
if isfield(ld,'pars')
  defInd= ~ismember(pars.ind,ld.pars.ind);
  pars.tolRef=ld.pars.tolRef;
  for i_ind=1:length(ld.pars.ind)
    wind=ismember(pars.ind,ld.pars.ind(i_ind)) & ismember(pars.types,ld.pars.types{i_ind});
    if any(wind)
      pars.requireHW(wind)=ld.pars.requireHW(i_ind);
      pars.requireTol(wind)=ld.pars.requireTol(i_ind);
      pars.tolLow(:,wind)=ld.pars.tolLow(:,i_ind);
      pars.tolHigh(:,wind)=ld.pars.tolHigh(:,i_ind);
      pars.defRef(:,wind)=ld.pars.defRef(:,i_ind);
      if isfield(pars,'defTolLow')
        pars.defTolLow(:,wind)=ld.pars.defTolLow(:,i_ind);
        pars.defTolHigh(:,wind)=ld.pars.defTolHigh(:,i_ind);
      end
    end
  end
else
  defInd=true(size(pars.ind));
end

% Default tolerances and references
if any(defInd)
  % - PS
  % - - Correctors (ref +/- 1e-5 rad)
  cors=findcells(BEAMLINE,'Class','*COR'); tolInd=ismember(pars.ind,cors) & defInd & ismember(pars.types,'PS');
  pars.tolLow(1,tolInd)=-1e-5; pars.tolHigh(1,tolInd)=1e-5;
  pars.defRef(1,tolInd)=zeros(1,sum(tolInd));
  % - - Skew quads etc (ref +/- 0.1T)
  sq=findcells(BEAMLINE,'B',1); tolInd=ismember(pars.ind,sq) & defInd & ismember(pars.types,'PS');
  pars.tolLow(1,tolInd)=-0.1; pars.tolHigh(1,tolInd)=0.1;
  pars.defRef(1,tolInd)=zeros(1,sum(tolInd));
  % - - Everything else (ref +/- 0.1)
  ps=findcells(BEAMLINE,'PS'); tolInd=ismember(pars.ind,ps)&~ismember(pars.ind,cors)&~ismember(pars.ind,sq)&defInd & ismember(pars.types,'PS');
  pars.tolLow(1,tolInd)=-1e-3; pars.tolHigh(1,tolInd)=1e-3;
  pars.defRef(1,tolInd)=ones(1,sum(tolInd));
  % - GIRDER (ref +/- 10um / 1mrad)
  gir=findcells(BEAMLINE,'Girder'); tolInd=ismember(pars.ind,gir) & defInd & ismember(pars.types,'GIRDER');
  pars.tolLow(1,tolInd)=-10e-6; pars.tolHigh(1,tolInd)=10e-6;
  pars.tolLow(2,tolInd)=-10e-6; pars.tolHigh(2,tolInd)=10e-6;
  pars.tolLow(3,tolInd)=-10e-6; pars.tolHigh(3,tolInd)=10e-6;
  pars.defRef(1:3,tolInd)=zeros(3,sum(tolInd));
  % - INSTR (ref +/- 1mm / 1e10 tmit)
  tolInd=ismember(pars.types,'INSTR') & defInd;
  pars.tolLow(1,tolInd)=-1e-3; pars.tolHigh(1,tolInd)=1e-3;
  pars.tolLow(2,tolInd)=-1e-3; pars.tolHigh(2,tolInd)=1e-3;
  pars.tolLow(3,tolInd)=-2e10; pars.tolHigh(3,tolInd)=2e10;
  pars.defRef(1:2,tolInd)=zeros(2,sum(tolInd));
  pars.defRef(3,tolInd)=ones(1,sum(tolInd)).*1e10;
  pars.defTolLow=pars.tolLow; pars.defTolHigh=pars.tolHigh;
end
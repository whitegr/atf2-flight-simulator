function varargout = detailsGui(varargin)
% DETAILSGUI M-file for detailsGui.fig
%      DETAILSGUI, by itself, creates a new DETAILSGUI or raises the existing
%      singleton*.
%
%      H = DETAILSGUI returns the handle to a new DETAILSGUI or the handle to
%      the existing singleton*.
%
%      DETAILSGUI('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in DETAILSGUI.M with the given input arguments.
%
%      DETAILSGUI('Property','Value',...) creates a new DETAILSGUI or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before detailsGui_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to detailsGui_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help detailsGui

% Last Modified by GUIDE v2.5 31-Aug-2009 16:05:03

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @detailsGui_OpeningFcn, ...
                   'gui_OutputFcn',  @detailsGui_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before detailsGui is made visible.
function detailsGui_OpeningFcn(hObject, eventdata, handles, varargin) %#ok<*INUSL>
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to detailsGui (see VARARGIN)
global PS GIRDER INSTR FL
% Choose default command line output for detailsGui
handles.output = handles;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes detailsGui wait for user response (see UIRESUME)
% uiwait(handles.figure1);

% Get pars from function
[stat pars]=componentAvailability('GetPars');

% Take passed data and display
dataIn=get(handles.figure1,'UserData');
t=regexp(dataIn,'\d+\s*(\S+)','tokens','once');
for it=1:length(t)
  eleStr{it}=t{it}{1};
end
ele=regexp(dataIn,'\d+','match','once');
t=regexp(dataIn,'\((\S\s*\d+\))','tokens','once');
typeIn=t{1}{1};
dispStr={};
for istr=1:length(eleStr)
  ind_all=ismember(pars.ind,str2double(ele{istr}));
  for ind=find(ind_all)
    switch pars.types{ind}
      case 'PS'
        if t{istr}{1}~='P'; continue; end;
        val=PS(pars.typeInd(ind)).Ampl;
        if isfield(FL.HwInfo.PS(pars.typeInd(ind)),'ref')
          ref=FL.HwInfo.PS(pars.typeInd(ind)).ref;
        else
%           ref=pars.defRef(1,pars.typeInd(ind));
% JLN 091208 really want ind here because we're in pars not Fl.Hw...
          ref=pars.defRef(1,ind);
        end
        dof={'Ampl'};
      case 'INSTR'
        if t{istr}{1}~='I'; continue; end;
        val=INSTR{pars.typeInd(ind)}.Data;
        ref=INSTR{pars.typeInd(ind)}.ref;
        dof={'X' 'Y' 'TMIT'};
      case 'GIRDER'
        if t{istr}{1}~='G'; continue; end;
        val=GIRDER{pars.typeInd(ind)}.MoverPos;
        % 091208 JLN I think this should be 
        % ref=GIRDER(pars.typeInd(ind)).MoverSetPt
        if isfield(FL.HwInfo.GIRDER(pars.typeInd(ind)),'ref')
          ref=FL.HwInfo.GIRDER(pars.typeInd(ind)).ref;
        else
          % jln 091208 see PS above
          ref=pars.defRef(:,ind);
        end
        dof={'X' 'Y' 'TILT'};
      otherwise
        continue
    end
    for idof=1:length(dof)
      tol=[pars.tolLow(idof,ind) pars.tolHigh(idof,ind)];
      dispStr{end+1}=sprintf('%4s(%s %16s (%4s) | %9.5g | %9.5g | [%7.2g,%7.2g]',ele{istr},t{istr}{1},eleStr{istr},dof{idof},val(idof),ref(idof),tol(1),tol(2));
    end
  end
end
set(handles.listbox1,'String',dispStr)
set(handles.listbox1,'Value',1)

% --- Outputs from this function are returned to the command line.
function varargout = detailsGui_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on selection change in listbox1.
function listbox1_Callback(hObject, eventdata, handles) %#ok<*INUSD,*DEFNU>
% hObject    handle to listbox1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

listStr=get(hObject,'String');
listSel=get(hObject,'Value');
if length(listSel)==1
  t=regexp(listStr{listSel},'\|\s*\S+\s*\|\s*+(\S+)\s*\|\s*\[\s*(\S+),\s*(\S+)\s*\]','tokens','once');
  set(handles.edit1,'String',t{1})
  tol=[str2num(t{2}) str2num(t{3})];
  set(handles.edit2,'String',num2str(tol(1),6))
  set(handles.edit3,'String',num2str(tol(2),6))
else
  set(handles.edit1,'String',[])
  set(handles.edit2,'String',[])
  set(handles.edit3,'String',[])
end

% --- Executes during object creation, after setting all properties.
function listbox1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to listbox1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: listbox controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- EXIT
function pushbutton1_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
guiCloseFn('detailsGui',handles);

% --- Update
function pushbutton2_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

detailsGui_OpeningFcn(handles.figure1, [], handles, []);

% --- Set to Ref value
function pushbutton7_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton7 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global PS GIRDER FL
if ~strcmp(questdlg('Set this (these) components to their reference values? (Directly write to hardware)','Set to ref'),'Yes')
  return
end
[stat pars]=componentAvailability('GetPars');
listStr=get(handles.listbox1,'String');
listSel=get(handles.listbox1,'Value');
for isel=1:length(listSel)
  thisStr=listStr{listSel(isel)};
  t=regexp(thisStr,'\(\s*(\S+))','tokens');
  dofStr=t{1}{1};
  if ismember(dofStr,{'X' 'Ampl'})
    dof=1;
  elseif strcmp(dofStr,'Y')
    dof=2;
  else
    dof=3;
  end
  allind=ismember(pars.ind,str2double(regexp(thisStr,'\d+','match','once')));
  t=regexp(thisStr,'\((\w)','tokens','once');
  for ind=find(allind)
    switch pars.types{ind}
      case 'PS'
        if ~t{1}=='P'; continue; end;
        if isfield(FL.HwInfo.PS(pars.typeInd(ind)),'ref')
          PS(pars.typeInd(ind)).SetPt=FL.HwInfo.PS(pars.typeInd(ind)).ref(1);
        else
          PS(pars.typeInd(ind)).SetPt=pars.defRef(1,ind);
        end
        stat=PSTrim(pars.typeInd(ind),1);
        if stat{1}~=1
          errordlg(stat{2},'Error setting PS')
          return
        end
      case 'INSTR'
        continue
      case 'GIRDER'
        if ~t{1}=='G'; continue; end;
        if isfield(FL.HwInfo.GIRDER(pars.typeInd(ind)),'ref')
          GIRDER{pars.typeInd(ind)}.MoverSetPt(dof)=FL.HwInfo.GIRDER(pars.typeInd(ind)).ref(dof);
        else
          GIRDER{pars.typeInd(ind)}.MoverSetPt(dof)=pars.defRef(dof,ind);
        end
        stat=MoverTrim(pars.typeInd(ind),1);
        if stat{1}~=1
          errordlg(stat{2},'Error setting Mover')
          return
        end
    end
  end
end
pushbutton2_Callback(handles.pushbutton2,[],handles);


function edit1_Callback(hObject, eventdata, handles)
% hObject    handle to edit1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit1 as text
%        str2double(get(hObject,'String')) returns contents of edit1 as a double


% --- Executes during object creation, after setting all properties.
function edit1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit2_Callback(hObject, eventdata, handles)
% hObject    handle to edit2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit2 as text
%        str2double(get(hObject,'String')) returns contents of edit2 as a double


% --- Executes during object creation, after setting all properties.
function edit2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit3_Callback(hObject, eventdata, handles)
% hObject    handle to edit3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit3 as text
%        str2double(get(hObject,'String')) returns contents of edit3 as a double


% --- Executes during object creation, after setting all properties.
function edit3_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- set reference values / Tols for this line item
function pushbutton5_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton5 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global PS GIRDER INSTR FL %#ok<NUSED>
if ~isequal(get(hObject,'UserData'),'OverideCheck')
  if ~strcmp(questdlg('Apply new Reference and tolerance settings?','Reference and Tol Set'),'Yes')
    return
  end
end
[stat pars]=componentAvailability('GetPars');
newpars.tolLow=pars.tolLow;
newpars.tolHigh=pars.tolHigh;
dispStr=get(handles.listbox1,'String');
selStr={dispStr{get(handles.listbox1,'Value')}};
for istr=1:length(selStr)
  thisStr=selStr{istr};
  % refVal set by other applications
  %refVal=str2double(get(handles.edit1,'String'));
  lowTolVal=str2double(get(handles.edit2,'String'));
  highTolVal=str2double(get(handles.edit3,'String'));
  t=regexp(thisStr,'\(\s*(\S+))','tokens');
  dofStr=t{1}{1};
  if ismember(dofStr,{'X' 'Ampl'})
    dof=1;
  elseif strcmp(dofStr,'Y')
    dof=2;
  else
    dof=3;
  end
  allind=ismember(pars.ind,str2double(regexp(thisStr,'\d+','match','once')));
  for ind=find(allind)
    t=regexp(thisStr,'\((\w)','tokens','once');
    if t{1}~=pars.types{ind}(1); continue; end;
      if ~isnan(lowTolVal)
        newpars.tolLow(dof,ind)=lowTolVal;
      end
      if ~isnan(highTolVal)
        newpars.tolHigh(dof,ind)=highTolVal;
      end
      componentAvailability('SetPars',newpars);
%     switch pars.types{ind}
%       case {'PS','GIRDER'}
%         if ~isnan(refVal)
%           FL.HwInfo.(pars.types{ind})(pars.typeInd(ind)).ref(dof)=refVal;
%         end
%       case 'INSTR'
%         if ~isnan(refVal)
%           INSTR{pars.typeInd(ind)}.ref(dof)=refVal;
%         end
%     end
  end
end
pushbutton2_Callback(handles.pushbutton2,[],handles);

% --- Set default ref setting
function pushbutton6_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton6 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
if ~strcmp(questdlg('Apply default Reference settings?','Set Default Ref'),'Yes')
  return
end
[stat pars]=componentAvailability('GetPars');
listStr=get(handles.listbox1,'String');
listSel=get(handles.listbox1,'Value');
t=regexp(listStr{listSel},'\(\s*(\S+))','tokens');
  dofStr=t{1}{1};
  if ismember(dofStr,{'X' 'Ampl'})
    dof=1;
  elseif strcmp(dofStr,'Y')
    dof=2;
  else
    dof=3;
  end
for isel=1:length(listSel)
  thisStr=listStr{isel};
  allind=ismember(pars.ind,str2double(regexp(thisStr,'\d+','match','once')));
  for ind=find(allind)
    t=regexp(thisStr,'\((\w)','tokens','once');
    if t{1}~=pars.types{ind}(1); continue; end;
    set(handles.edit1,'String',pars.defRef(dof,ind))
    set(handles.pushbutton5,'UserData','OverideCheck')
    pushbutton5_Callback(handles.pushbutton5,[],handles);
    set(handles.pushbutton5,'UserData',[])
  end
end

% --- Executes when user attempts to close figure1.
function figure1_CloseRequestFcn(hObject, eventdata, handles)
% hObject    handle to figure1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

guiCloseFn('componentAvailabilityGui',handles);


% --- Set default Tol settings
function pushbutton8_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton8 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
if ~strcmp(questdlg('Apply default Tolerance settings?','Set Default Tol'),'Yes')
  return
end
[stat pars]=componentAvailability('GetPars');
listStr=get(handles.listbox1,'String');
listSel=get(handles.listbox1,'Value');
t=regexp(listStr{listSel},'\(\s*(\S+))','tokens');
  dofStr=t{1}{1};
  if ismember(dofStr,{'X' 'Ampl'})
    dof=1;
  elseif strcmp(dofStr,'Y')
    dof=2;
  else
    dof=3;
  end
for isel=1:length(listSel)
  thisStr=listStr{isel};
  allind=ismember(pars.ind,str2double(regexp(thisStr,'\d+','match','once')));
  for ind=find(allind)
    t=regexp(thisStr,'\((\w)','tokens','once');
    if t{1}~=pars.types{ind}(1); continue; end;
    set(handles.edit2,'String',pars.defTolLow(dof,ind))
    set(handles.edit3,'String',pars.defTolHigh(dof,ind))
    set(handles.pushbutton5,'UserData','OverideCheck')
    pushbutton5_Callback(handles.pushbutton5,[],handles);
    set(handles.pushbutton5,'UserData',[])
  end
end

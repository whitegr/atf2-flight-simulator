function varargout = aperturesGui(varargin)
% APERTURESGUI M-file for aperturesGui.fig
%      APERTURESGUI, by itself, creates a new APERTURESGUI or raises the existing
%      singleton*.
%
%      H = APERTURESGUI returns the handle to a new APERTURESGUI or the handle to
%      the existing singleton*.
%
%      APERTURESGUI('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in APERTURESGUI.M with the given input arguments.
%
%      APERTURESGUI('Property','Value',...) creates a new APERTURESGUI or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before aperturesGui_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to aperturesGui_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help aperturesGui

% Last Modified by GUIDE v2.5 23-Oct-2009 18:15:48

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @aperturesGui_OpeningFcn, ...
                   'gui_OutputFcn',  @aperturesGui_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before aperturesGui is made visible.
function aperturesGui_OpeningFcn(hObject, eventdata, handles, varargin) %#ok<*INUSL>
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to aperturesGui (see VARARGIN)
global INSTR

% Choose default command line output for aperturesGui
handles.output = handles;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes aperturesGui wait for user response (see UIRESUME)
% uiwait(handles.figure1);

% Get alarm status
[stat alarm]=apertures('IsAlarm');
setAlarm(handles,alarm);

% Check a reference orbit exists
if ~isfield(INSTR{1},'ref')
  errordlg('No reference orbit in INSTR array, aborting startup','Apertures GUI Error')
  delete(handles.figure1)
  return
end

% Setup aperture list
[stat data]=apertures('GetData'); if stat{1}~=1; errordlg(stat{2},'apertures error'); end;
setApList(handles,data); 

% Set GUI fields from pars
[stat pars]=apertures('GetPars'); if stat{1}~=1; errordlg(stat{2},'apertures error'); end;
setPars(handles,pars);

% Plot data
apPlot(handles);

% Tell apertures gui is active
newpars.guiActive=true;
apertures('SetPars',newpars);


% --- Outputs from this function are returned to the command line.
function varargout = aperturesGui_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- EXIT button
function pushbutton1_Callback(hObject, eventdata, handles) %#ok<*DEFNU>
% hObject    handle to pushbutton1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
apertures('save');
newpars.guiActive=false;
apertures('SetPars',newpars);
guiCloseFn('aperturesGui',handles);

% --- BPM Orbit Plots figure
function pushbutton2_Callback(hObject, eventdata, handles) %#ok<*INUSD>
% hObject    handle to pushbutton2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global FL

FL.Gui.aperturesGui_plot1=figure;
apPlot(handles);

% --- Aperture List
function popupmenu1_Callback(hObject, eventdata, handles)
% hObject    handle to popupmenu1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = get(hObject,'String') returns popupmenu1 contents as cell array
%        contents{get(hObject,'Value')} returns selected item from popupmenu1
[stat pars]=apertures('GetPars'); if stat{1}~=1; errordlg(stat{2},'apertures error'); end;
setPars(handles,pars)
pushbutton9_Callback(handles.pushbutton9,[],handles);

% --- Executes during object creation, after setting all properties.
function popupmenu1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to popupmenu1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% aperture x/y offset
function edit1_Callback(hObject, eventdata, handles)
% hObject    handle to edit1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit1 as text
%        str2double(get(hObject,'String')) returns contents of edit1 as a double
% Get ap name
aplist=get(handles.popupmenu1,'String');
aname=aplist{get(handles.popupmenu1,'Value')};
[stat pars]=apertures('GetPars');
if stat{1}~=1
  errordlg(stat{2},'GetPars error')
  return
end
if get(handles.radiobutton1,'Value')
  fn='xoff';
else
  fn='yoff';
end
newpars.(aname).(fn)=str2double(get(hObject,'String'))*1e-3;
if isnan(newpars.(aname).(fn))
  set(hObject,'String',num2str(pars.(aname).(fn)))
  return
end
smin=get(handles.slider3,'Min'); smax=get(handles.slider3,'Max');
if newpars.(aname).(fn)<smin || newpars.(aname).(fn)>smax
  errordlg([fn,' Value out of slider range'],'Aperture set error')
  return
end
set(handles.slider3,'Value',newpars.(aname).(fn))
stat=apertures('SetPars',newpars);
if stat{1}~=1
  errordlg(stat{2},'SetPars error')
  set(hObject,'String',num2str(pars.(aname).(fn)))
  return
end

pushbutton9_Callback(handles.pushbutton9,[],handles);

% --- Executes during object creation, after setting all properties.
function edit1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- xp/yp offset
function edit2_Callback(hObject, eventdata, handles)
% hObject    handle to edit2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit2 as text
%        str2double(get(hObject,'String')) returns contents of edit2 as a double
aplist=get(handles.popupmenu1,'String');
aname=aplist{get(handles.popupmenu1,'Value')};
[stat pars]=apertures('GetPars');
if stat{1}~=1
  errordlg(stat{2},'GetPars error')
  return
end
if get(handles.radiobutton1,'Value')
  fn='xpoff';
else
  fn='ypoff';
end
newpars.(aname).(fn)=str2double(get(hObject,'String'))*1e-3;
if isnan(newpars.(aname).(fn))
  set(hObject,'String',num2str(pars.(aname).(fn)))
  return
end
smin=get(handles.slider4,'Min'); smax=get(handles.slider4,'Max');
if newpars.(aname).(fn)<smin || newpars.(aname).(fn)>smax
  errordlg([fn,' Value out of slider range'],'Aperture set error')
  return
end
set(handles.slider4,'Value',newpars.(aname).(fn))
stat=apertures('SetPars',newpars);
if stat{1}~=1
  errordlg(stat{2},'SetPars error')
  set(hObject,'String',num2str(pars.(aname).(fn)))
  return
end

pushbutton9_Callback(handles.pushbutton9,[],handles);

% --- Executes during object creation, after setting all properties.
function edit2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- aperture rotation
function edit3_Callback(hObject, eventdata, handles)
% hObject    handle to edit3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit3 as text
%        str2double(get(hObject,'String')) returns contents of edit3 as a double
aplist=get(handles.popupmenu1,'String');
aname=aplist{get(handles.popupmenu1,'Value')};
[stat pars]=apertures('GetPars');
if stat{1}~=1
  errordlg(stat{2},'GetPars error')
  return
end
if get(handles.radiobutton1,'Value')
  fn='xrot';
else
  fn='yrot';
end
newpars.(aname).(fn)=str2double(get(hObject,'String'));
if isnan(newpars.(aname).(fn))
  set(hObject,'String',num2str(pars.(aname).(fn)))
  return
end
smin=get(handles.slider5,'Min'); smax=get(handles.slider5,'Max');
if newpars.(aname).(fn)<smin || newpars.(aname).(fn)>smax
  errordlg([fn,' Value out of slider range'],'Aperture set error')
  return
end
set(handles.slider5,'Value',newpars.(aname).(fn))
stat=apertures('SetPars',newpars);
if stat{1}~=1
  errordlg(stat{2},'SetPars error')
  set(hObject,'String',num2str(pars.(aname).(fn)))
  return
end

% --- Executes during object creation, after setting all properties.
function edit3_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- aperture width
function edit4_Callback(hObject, eventdata, handles)
% hObject    handle to edit4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit4 as text
%        str2double(get(hObject,'String')) returns contents of edit4 as a double
aplist=get(handles.popupmenu1,'String');
aname=aplist{get(handles.popupmenu1,'Value')};
[stat pars]=apertures('GetPars');
if stat{1}~=1
  errordlg(stat{2},'GetPars error')
  return
end
if get(handles.radiobutton1,'Value')
  fn='xwid';
else
  fn='ywid';
end
newpars.(aname).(fn)=str2double(get(hObject,'String'))*1e-3;
if isnan(newpars.(aname).(fn))
  set(hObject,'String',num2str(pars.(aname).(fn)))
  return
end
smin=get(handles.slider1,'Min'); smax=get(handles.slider1,'Max');
if newpars.(aname).(fn)<smin || newpars.(aname).(fn)>smax
  errordlg([fn,' Value out of slider range'],'Aperture set error')
  return
end
set(handles.slider1,'Value',newpars.(aname).(fn))
stat=apertures('SetPars',newpars);
if stat{1}~=1
  errordlg(stat{2},'SetPars error')
  set(hObject,'String',num2str(pars.(aname).(fn)))
  return
end

% --- Executes during object creation, after setting all properties.
function edit4_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Aperture height
function edit5_Callback(hObject, eventdata, handles)
% hObject    handle to edit5 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit5 as text
%        str2double(get(hObject,'String')) returns contents of edit5 as a double
aplist=get(handles.popupmenu1,'String');
aname=aplist{get(handles.popupmenu1,'Value')};
[stat pars]=apertures('GetPars');
if stat{1}~=1
  errordlg(stat{2},'GetPars error')
  return
end
if get(handles.radiobutton1,'Value')
  fn='xpwid';
else
  fn='ypwid';
end
newpars.(aname).(fn)=str2double(get(hObject,'String'))*1e-3;
if isnan(newpars.(aname).(fn))
  set(hObject,'String',num2str(pars.(aname).(fn)))
  return
end
smin=get(handles.slider2,'Min'); smax=get(handles.slider2,'Max');
if newpars.(aname).(fn)<smin || newpars.(aname).(fn)>smax
  errordlg([fn,' Value out of slider range'],'Aperture set error')
  return
end
set(handles.slider2,'Value',newpars.(aname).(fn))
stat=apertures('SetPars',newpars);
if stat{1}~=1
  errordlg(stat{2},'SetPars error')
  set(hObject,'String',num2str(pars.(aname).(fn)))
  return
end

% --- Executes during object creation, after setting all properties.
function edit5_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit5 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in pushbutton3.
function pushbutton3_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Make new
function pushbutton4_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
aperturesGui_makeNew;
% Setup aperture list
[stat data]=apertures('GetData'); if stat{1}~=1; errordlg(stat{2},'apertures error'); end;
setApList(handles,data); 




function edit7_Callback(hObject, eventdata, handles)
% hObject    handle to edit7 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit7 as text
%        str2double(get(hObject,'String')) returns contents of edit7 as a double
[stat pars]=apertures('GetPars');
if stat{1}~=1
  errordlg(stat{2},'GetPars error')
  return
end
newpars.nbpm=str2double(get(hObject,'String'));
if isnan(newpars.nbpm)
  set(hObject,'String',num2str(pars.nbpm))
  return
end
stat=apertures('SetPars',newpars);
if stat{1}~=1
  errordlg(stat{2},'SetPars error')
  set(hObject,'String',num2str(pars.nbpm))
  return
end
pars.nbpm=newpars.nbpm;
setBpmList(handles,pars)

% --- Executes during object creation, after setting all properties.
function edit7_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit7 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- width slider
function slider1_Callback(hObject, eventdata, handles)
% hObject    handle to slider1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider
set(handles.edit4,'String',num2str(get(hObject,'Value')*1e3))
edit4_Callback(handles.edit4,[],handles)

% --- Executes during object creation, after setting all properties.
function slider1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to slider1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end


% --- height slider
function slider2_Callback(hObject, eventdata, handles)
% hObject    handle to slider2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider
set(handles.edit5,'String',num2str(get(hObject,'Value')*1e3))
edit5_Callback(handles.edit5,[],handles)

% --- Executes during object creation, after setting all properties.
function slider2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to slider2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end


% --- Aperture x/y offset slider
function slider3_Callback(hObject, eventdata, handles)
% hObject    handle to slider3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider
set(handles.edit1,'String',num2str(get(hObject,'Value')*1e3))
edit1_Callback(handles.edit1,[],handles)

% --- Executes during object creation, after setting all properties.
function slider3_CreateFcn(hObject, eventdata, handles)
% hObject    handle to slider3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end


% --- x'/y' offset slider
function slider4_Callback(hObject, eventdata, handles)
% hObject    handle to slider4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of
%        slider
set(handles.edit2,'String',num2str(get(hObject,'Value')*1e3))
edit2_Callback(handles.edit2,[],handles)

% --- Executes during object creation, after setting all properties.
function slider4_CreateFcn(hObject, eventdata, handles)
% hObject    handle to slider4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end


% --- rotation slider
function slider5_Callback(hObject, eventdata, handles)
% hObject    handle to slider5 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider
set(handles.edit3,'String',num2str(get(hObject,'Value')))
edit3_Callback(handles.edit3,[],handles)

% --- Executes during object creation, after setting all properties.
function slider5_CreateFcn(hObject, eventdata, handles)
% hObject    handle to slider5 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end

% --- alarm update rate
function edit6_Callback(hObject, eventdata, handles)
% hObject    handle to edit6 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit6 as text
%        str2double(get(hObject,'String')) returns contents of edit6 as a double
[stat pars]=apertures('GetPars');
if stat{1}~=1
  errordlg(stat{2},'GetPars error')
  return
end
newpars.updateRate=str2double(get(hObject,'String'));
if isnan(newpars.updateRate)
  set(hObject,'String',num2str(pars.updateRate))
  return
end
stat=apertures('SetPars',newpars);
if stat{1}~=1
  errordlg(stat{2},'SetPars error')
  set(hObject,'String',num2str(pars.updateRate))
  return
end

% --- Executes during object creation, after setting all properties.
function edit6_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit6 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in listbox1.
function listbox1_Callback(hObject, eventdata, handles)
% hObject    handle to listbox1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = get(hObject,'String') returns listbox1 contents as cell array
%        contents{get(hObject,'Value')} returns selected item from listbox1


% --- Executes during object creation, after setting all properties.
function listbox1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to listbox1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: listbox controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Get reference orbit from saved set point file
function pushbutton5_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton5 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
[filename pathname]=uigetfile('*.mat','Select FS Save File','latticeFiles/archive');
if ~isequal(filename,0) && ~isequal(pathname,0)
  ld=load(fullfile(pathname,filename),'INSTR');
  if ~isfield(ld,'INSTR') || isempty(ld.INSTR) || ~isfield(ld.INSTR{1},'ref') || ~isfield(ld.INSTR{1},'referr')
    errordlg('No INSTR reference orbit data found in this file!','Ref Load Error')
    return
  end
  for i_ind=1:2
    newpars.reforbit(:,i_ind)=arrayfun(@(x) ld.INSTR{x}.ref(i_ind),1:length(ld.INSTR))';
    newpars.reforbit(:,i_ind+3)=arrayfun(@(x) ld.INSTR{x}.referr(i_ind),1:length(ld.INSTR))';
  end
  set(handles.text9,'String',filename)
end

% --- Alarm active
function checkbox1_Callback(hObject, eventdata, handles)
% hObject    handle to checkbox1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of checkbox1
aplist=get(handles.popupmenu1,'String');
aname=aplist{get(handles.popupmenu1,'Value')};
newpars.(aname).active=get(hObject,'Value');
stat=apertures('SetPars',newpars);
if stat{1}~=1
  errordlg(stat{2},'SetPars Error')
end

% --- Alarm status
function pushbutton6_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton6 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% Get alarm status
[stat alarm]=apertures('IsAlarm'); if stat{1}~=1; errordlg(stat{2},'apertures error'); end;
setAlarm(handles,alarm);

% Go to first alarmed aperture if alarm and list alarmed aperture
if alarm
  [stat data]=apertures('GetData'); if stat{1}~=1; errordlg(stat{2},'apertures error'); end;
  setApList(handles,data); 
end

% --- # bpm ave
function edit8_Callback(hObject, eventdata, handles)
% hObject    handle to edit8 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit8 as text
%        str2double(get(hObject,'String')) returns contents of edit8 as a double
[stat pars]=apertures('GetPars');
if stat{1}~=1
  errordlg(stat{2},'GetPars error')
  return
end
newpars.bpmave=str2double(get(hObject,'String'));
if isnan(newpars.bpmave)
  set(hObject,'String',num2str(pars.bpmave))
  return
end
stat=apertures('SetPars',newpars);
if stat{1}~=1
  errordlg(stat{2},'SetPars error')
  set(hObject,'String',num2str(pars.bpmave))
  return
end

% --- Executes during object creation, after setting all properties.
function edit8_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit8 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Set Max rms cut
function edit9_Callback(hObject, eventdata, handles)
% hObject    handle to edit9 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

[stat pars]=apertures('GetPars');
if stat{1}~=1
  errordlg(stat{2},'GetPars error')
  return
end
newpars.maxrms=str2double(get(hObject,'String'));
if isnan(newpars.maxrms)
  set(hObject,'String',num2str(pars.maxrms))
  return
end
stat=apertures('SetPars',newpars);
if stat{1}~=1
  errordlg(stat{2},'SetPars error')
  set(hObject,'String',num2str(pars.minq))
  return
end

% --- Executes during object creation, after setting all properties.
function edit9_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit9 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Min Q
function edit10_Callback(hObject, eventdata, handles)
% hObject    handle to edit10 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit10 as text
%        str2double(get(hObject,'String')) returns contents of edit10 as a double
[stat pars]=apertures('GetPars');
if stat{1}~=1
  errordlg(stat{2},'GetPars error')
  return
end
newpars.minq=str2double(get(hObject,'String'));
if isnan(newpars.minq)
  set(hObject,'String',num2str(pars.minq))
  return
end
stat=apertures('SetPars',newpars);
if stat{1}~=1
  errordlg(stat{2},'SetPars error')
  set(hObject,'String',num2str(pars.minq))
  return
end

% --- Executes during object creation, after setting all properties.
function edit10_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit10 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes when user attempts to close figure1.
function figure1_CloseRequestFcn(hObject, eventdata, handles)
% hObject    handle to figure1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
apertures('save');
newpars.guiActive=false;
apertures('SetPars',newpars);
guiCloseFn('aperturesGui',handles);


% --- Executes on button press in radiobutton5.
function radiobutton5_Callback(hObject, eventdata, handles)
% hObject    handle to radiobutton5 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of radiobutton5


% --- fwd propagate
function radiobutton6_Callback(hObject, eventdata, handles)
% hObject    handle to radiobutton6 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

[stat, pars]=apertures('GetPars');
if stat{1}~=1
  warning('Lucretia:aperturesGui:apPlot',stat{2});
  return
end
setBpmList(handles,pars);
pushbutton9_Callback(handles.pushbutton9, eventdata, handles)

% --- Use default reference orbit
function pushbutton7_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton7 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% --- Check want to do this
resp=questdlg('Set New Reference Orbit for this Aperture?','Default Ref Orbit');
if ~strcmp(resp,'Yes'); return; end;

aplist=get(handles.popupmenu1,'String');
aname=aplist{get(handles.popupmenu1,'Value')};
newpars.(aname).reforbit='default';
stat=apertures('SetPars',newpars);
if stat{1}~=1
  errordlg(stat{2},'Set Pars Error')
  return
end
set(handles.text9,'String','Use Default')

% --- Remove BPM(s)
function pushbutton8_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton8 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
[stat, pars]=apertures('GetPars');
if stat{1}~=1
  warning('Lucretia:aperturesGui:apPlot',stat{2});
  return
end
bpms=get(handles.listbox1,'Value');
bpmList=get(handles.listbox1,'String');
bpmList={bpmList{~ismember(1:length(bpmList),bpms)}};
set(handles.listbox1,'String',bpmList)
setBpmList(handles,pars);


% --- Update gui
function pushbutton9_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton9 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
Update(hObject, eventdata, handles)

function Update(hObject, eventdata, handles)
stat=apertures('update');

if stat{1}~=1
  errordlg(stat{2},'update error')
  return
end
apPlot(handles);

% --- Reset BPM list
function pushbutton10_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton10 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
[stat, pars]=apertures('GetPars');
if stat{1}~=1
  warning('Lucretia:aperturesGui:apPlot',stat{2});
  return
end
set(handles.listbox1,'String',{'none'})
setBpmList(handles,pars);


% --- Aperture shape - ellipse
function radiobutton3_Callback(hObject, eventdata, handles)
% hObject    handle to radiobutton3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

if get(hObject,'Value')
  set(handles.radiobutton4,'Value',0)
  alist=get(handles.popupmenu1,'String'); apName=alist{get(handles.popupmenu1,'Value')};
  newpars.(apName).shape=get(hObject,'String');
  stat = apertures('SetPars',newpars);
  if stat{1}~=1
    errordlg(stat{2},'SetPars Error')
    return
  end
  [stat pars]=apertures('GetPars');
  if stat{1}~=1
    errordlg(stat{2},'GetPars Error')
    return
  end
  setPars(handles,pars);
end
  


% --- Aperture shape - rectangle
function radiobutton4_Callback(hObject, eventdata, handles)
% hObject    handle to radiobutton4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

if get(hObject,'Value')
  set(handles.radiobutton3,'Value',0)
  alist=get(handles.popupmenu1,'String'); apName=alist{get(handles.popupmenu1,'Value')};
  newpars.(apName).shape=get(hObject,'String');
  stat = apertures('SetPars',newpars);
  if stat{1}~=1
    errordlg(stat{2},'SetPars Error')
    return
  end
  [stat pars]=apertures('GetPars');
  if stat{1}~=1
    errordlg(stat{2},'GetPars Error')
    return
  end
  setPars(handles,pars);
end


% --- select plane
function radiobutton1_Callback(hObject, eventdata, handles)
% hObject    handle to radiobutton1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

[stat pars]=apertures('GetPars');
if stat{1}~=1
  errordlg(stat{2},'GetPars Error')
  return
elseif isequal(stat{1},'Citation X')
  errordlg(stat{2},'Good choice but sadly not available')
  return
end
setPars(handles,pars);


% --- select plane
function radiobutton2_Callback(hObject, eventdata, handles)
% hObject    handle to radiobutton2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

[stat pars]=apertures('GetPars');
if stat{1}~=1
  errordlg(stat{2},'GetPars Error')
  return
elseif isequal(stat{1},'Citation X')
  errordlg(stat{2},'Good choice but sadly not available')
  return
end
setPars(handles,pars);


% --- use ICTDUMP
function radiobutton7_Callback(hObject, eventdata, handles)
% hObject    handle to radiobutton7 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
if get(hObject,'Value')
  set(handles.radiobutton8,'Value',0)
  newpars.whichICT=get(hObject,'String');
  stat = apertures('SetPars',newpars);
  if stat{1}~=1
    errordlg(stat{2},'SetPars Error')
    return
  end
end


% --- use ICT1X
function radiobutton8_Callback(hObject, eventdata, handles)
% hObject    handle to radiobutton8 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
if get(hObject,'Value')
  set(handles.radiobutton7,'Value',0)
  newpars.whichICT=get(hObject,'String');
  stat = apertures('SetPars',newpars);
  if stat{1}~=1
    errordlg(stat{2},'SetPars Error')
    return
  end
end

% --- Alarm handling
function setAlarm(handles,alarm)
if alarm==1
  set(handles.pushbutton6,'BackgroundColor','yellow')
  set(handles.pushbutton6,'String','Minor Alarm')
elseif alarm==2
  set(handles.pushbutton6,'BackgroundColor','red')
  set(handles.pushbutton6,'String','Major Alarm')
else
  set(handles.pushbutton6,'BackgroundColor','green')
  set(handles.pushbutton6,'String','OK')
end
drawnow('expose')

% --- Setup Aperture list in pulldown menu
function setApList(handles,data)
persistent lastaplist
df=fieldnames(data);
apnames={}; isalarm=[];
for idf=1:length(df)
  if ~strcmp(df{idf},'alarm')
    apnames{end+1}=df{idf};
    isalarm(end+1)=data.(df{idf}).alarm;
  end
end
set(handles.popupmenu1,'String',apnames);
if data.alarm
  set(handles.popupmenu1,'Value',find(isalarm,1))
elseif ~isempty(lastaplist) && ~isequal(lastaplist,apnames)
  % select new entry
  inew=find(~ismember(apnames,lastaplist));
  if ~isempty(inew)
    set(handles.popupmenu1,'Value',inew(1))
  else
    set(handles.popupmenu1,'Value',1)
  end
else
  set(handles.popupmenu1,'Value',1)
end
lastaplist=apnames;

% --- Setup BPM selection list
function setBpmList(handles,pars)
global BEAMLINE
persistent allbi

% Get indicies of current BPM list
guiList=get(handles.listbox1,'String');
if ~iscell(guiList); guiList={guiList}; end;
guiListInd=[];
for iList=1:length(guiList)
  ib=findcells(BEAMLINE,'Name',guiList{iList});
  if ~isempty(ib)
    guiListInd(end+1)=ib(1);
  end
end
aplist=get(handles.popupmenu1,'String');
aname=aplist{get(handles.popupmenu1,'Value')};
% bi=pars.(aname).bpmind;
bi=[];
ti=pars.(aname).targetInd;
% remove any upstream bpms if fwd prop button false
if ~get(handles.radiobutton6,'Value') && any(find(bi<ti)) 
  bi(bi<ti)=[];
end

% try and make it so selected number of bpms existing
if isempty(allbi); allbi=findcells(BEAMLINE,'Class','MONI'); end;
% downstream bpms
[stat bcheck]=apertures('bpmCheck',bi);
bi=bcheck(1,:);
numbpm=sum(bi>ti);
nbpmsearch=pars.nbpm;
% Trim excess
if numbpm>pars.nbpm
  ntrim=numbpm-pars.nbpm;
  bi(find(bi>ti,ntrim,'last'))=[];
end
bi1=bi(bi<ti);
while numbpm<pars.nbpm && sum(allbi>ti)>=nbpmsearch
  bi=[allbi(find(allbi>ti,nbpmsearch,'first')) bi1];
  [stat bcheck]=apertures('bpmCheck',bi);
  bi=bcheck(1,:);
  nbpmsearch=nbpmsearch+1;
  numbpm=sum(bi>ti);
end
if numbpm<pars.nbpm
  errordlg('Insufficient (downstream) BPMs for requested number, only those shown available','BPM allocation error')
end
if get(handles.radiobutton6,'Value') % upstream bpms
  numbpm=sum(bi<ti);
  nbpmsearch=pars.nbpm;
  % Trim excess
  if numbpm>pars.nbpm
    ntrim=numbpm-pars.nbpm;
    bi(find(bi<ti,ntrim,'first'))=[];
  end
  bi1=bi(bi>ti);
  while numbpm<pars.nbpm && sum(allbi<ti)>=nbpmsearch
    bi=[bi1 allbi(find(allbi<ti,nbpmsearch,'last'))];
    [stat bcheck]=apertures('bpmCheck',bi);
    bi=bcheck(1,:);
    nbpmsearch=nbpmsearch+1;
    numbpm=sum(bi<ti);
  end
  if numbpm<pars.nbpm
    errordlg('Insufficient (upstream) BPMs for requested number, only those shown available','BPM allocation error')
  end
end

% fill listbox with bpm names and set apertures pars
set(handles.listbox1,'String',arrayfun(@(x) BEAMLINE{x}.Name,bi,'UniformOutput',false))
if isempty(get(handles.listbox1,'String'))
  set(handles.listbox1,'String',{'none'})
else
  newpars.(aname).bpmind=bi;
  stat=apertures('SetPars',newpars);
  if stat{1}~=1
    errordlg(stat{2},'SetPars Error')
    set(handles.listbox1,'String',guiList)
  end
end
set(handles.listbox1,'Value',1)

% --- Aperture Plotting 
function apPlot(handles)
global FL BEAMLINE

[stat data]=apertures('GetData');data.alarm;
setAlarm(handles,data.alarm);
drawnow('expose')

% Get data for plotting
if ~exist('handles','var')
  handles=FL.Gui.aperturesGui;
end
[stat, data]=evalin('base','apertures(''GetData'')');
if stat{1}~=1
  warning('Lucretia:aperturesGui:apPlot',stat{2});
  return
end
[stat, pars]=apertures('GetPars');
if stat{1}~=1
  warning('Lucretia:aperturesGui:apPlot',stat{2});
  return
end

% Get ap name
aplist=get(handles.popupmenu1,'String');
aname=aplist{get(handles.popupmenu1,'Value')};

% Are using upstream data and fwd prop?
isusdata=get(handles.radiobutton6,'Value');

% get ds derived data and plot
xax=handles.axes1; yax=handles.axes2;
if ~isfield(data.(aname),'pos')
  return
end
pos=data.(aname).pos.*1e3;
err=data.(aname).error.*1e3;
ap_x=pars.(aname).aper_x.*1e3; ap_xp=pars.(aname).aper_xp.*1e3;
ap_y=pars.(aname).aper_y.*1e3; ap_yp=pars.(aname).aper_yp.*1e3;
hold off
plot(xax,pos(1),pos(2),'ko')
hold on
patch(ap_x,ap_xp,'green','EdgeColor','green','Parent',xax)
line([pos(1)-err(1) pos(1)+err(1)],[pos(2) pos(2)],'color','black','Parent',xax)
line([pos(1) pos(1)],[pos(2)-err(2) pos(2)+err(2)],'color','black','Parent',xax)
xlabel(xax,'x / mm','FontSize',12); ylabel(xax,'x'' / mrad','FontSize',12);
hold off
plot(yax,pos(3),pos(4),'ko')
patch(ap_y,ap_yp,'green','EdgeColor','green','Parent',yax)
hold on
line([pos(3)-err(3) pos(3)+err(3)],[pos(4) pos(4)],'color','black','Parent',yax)
line([pos(3) pos(3)],[pos(4)-err(4) pos(4)+err(4)],'color','black','Parent',yax)
xlabel(yax,'y / mm','FontSize',12); ylabel(yax,'y'' / mrad','FontSize',12);
hold off

% Set Aperture title text
txt=sprintf('x = %.2g +/- %.2g mm x'' = %.2g +/- %.2g mrad\n y = %.2g +/- %.2g mm y'' = %.2g +/- %.2g mrad',pos(1),err(1),pos(2),err(2),pos(3),err(3),pos(4),err(4));
set(handles.text8,'String',txt);
if data.(aname).alarm
  set(handles.text8,'ForegroundColor','red')
else
  set(handles.text8,'ForegroundColor','black')
end

% get us derived data and plot
if isusdata
  pos=data.(aname).pos_us.*1e3;
  err=data.(aname).error_us.*1e3;
  hold(xax,'on')
  plot(xax,pos(1),pos(2),'ko')
  line([pos(1)-err(1) pos(1)+err(1)],[pos(2) pos(2)],'color','blue','Parent',xax)
  line([pos(1) pos(1)],[pos(2)-err(2) pos(2)+err(2)],'color','blue','Parent',xax)
  hold(xax,'off')
  hold(yax,'on')
  plot(yax,pos(3),pos(4),'ko')
  line([pos(3)-err(3) pos(3)+err(3)],[pos(4) pos(4)],'color','blue','Parent',yax)
  line([pos(3) pos(3)],[pos(4)-err(4) pos(4)+err(4)],'color','blue','Parent',yax)
  hold(yax,'off')
end

% Plot bpm orbits
S=arrayfun(@(x) BEAMLINE{x}.S,pars.(aname).bpmind);
S_target=BEAMLINE{pars.(aname).targetInd}.S;
subplot(2,1,1,'Parent',handles.uipanel13), errorbar(S(end-length(data.(aname).bpm(:,1))+1:end),data.(aname).bpm(:,1).*1e3,data.(aname).bpm_error(:,1).*1e3,'k')
hold on
subplot(2,1,1,'Parent',handles.uipanel13), errorbar(S_target,data.(aname).pos(1).*1e3,data.(aname).error(1).*1e3,'kx','MarkerSize',20,'LineWidth',2)
if isusdata
  subplot(2,1,1,'Parent',handles.uipanel13), errorbar(S(1:length(data.(aname).bpm_us(:,1))),data.(aname).bpm_us(:,1).*1e3,data.(aname).bpm_error_us(:,1).*1e3,'b')
  subplot(2,1,1,'Parent',handles.uipanel13), errorbar(S_target,data.(aname).pos_us(1).*1e3,data.(aname).error_us(1).*1e3,'bx','MarkerSize',20,'LineWidth',2)
end
hold off
xlabel('S / m','FontSize',12)
ylabel('\Deltax / mm','FontSize',12)
subplot(2,1,2,'Parent',handles.uipanel13), errorbar(S(end-length(data.(aname).bpm(:,1))+1:end),data.(aname).bpm(:,2).*1e3,data.(aname).bpm_error(:,2).*1e3,'k')
hold on
subplot(2,1,2,'Parent',handles.uipanel13), errorbar(S_target,data.(aname).pos(3).*1e3,data.(aname).error(3).*1e3,'kx','MarkerSize',20,'LineWidth',2)
if isusdata
  subplot(2,1,2,'Parent',handles.uipanel13), errorbar(S(1:length(data.(aname).bpm_us(:,1))),data.(aname).bpm_us(:,2).*1e3,data.(aname).bpm_error_us(:,2).*1e3,'b')
  subplot(2,1,2,'Parent',handles.uipanel13), errorbar(S_target,data.(aname).pos_us(3).*1e3,data.(aname).error_us(3).*1e3,'bx','MarkerSize',20,'LineWidth',2)
end
hold off
xlabel('S / m','FontSize',12)
ylabel('\Deltay / mm','FontSize',12)

drawnow('expose')

% --- Set parameters into GUI fields
function setPars(handles,pars)
% Set Title
alist=get(handles.popupmenu1,'String'); apName=alist{get(handles.popupmenu1,'Value')};
set(handles.text1,'String',sprintf('Aperture Watchdog - %s',apName))
% ICT choice
if strcmp(pars.whichICT,'ICTDUMP')
  set(handles.radiobutton8,'Value',0)
  set(handles.radiobutton7,'Value',1)
elseif strcmp(pars.whichICT,'ICT1X')
  set(handles.radiobutton7,'Value',0)
  set(handles.radiobutton8,'Value',1)
end
set(handles.edit9,'String',num2str(pars.maxrms))
set(handles.edit10,'String',num2str(pars.minq))
set(handles.edit6,'String',num2str(pars.updateRate))
set(handles.edit7,'String',num2str(pars.nbpm))
set(handles.edit8,'String',num2str(pars.bpmave))
if get(handles.radiobutton1,'Value'); p='x'; else p='y'; end;
set(handles.edit1,'String',num2str(pars.(apName).([p,'off'])*1e3))
set(handles.slider3,'Value',pars.(apName).([p,'off']))
set(handles.edit2,'String',num2str(pars.(apName).([p,'poff'])*1e3))
set(handles.slider4,'Value',pars.(apName).([p,'poff']))
set(handles.edit3,'String',num2str(pars.(apName).([p,'rot'])))
set(handles.slider5,'Value',pars.(apName).([p,'rot']))
set(handles.edit4,'String',num2str(pars.(apName).([p,'wid'])*1e3))
set(handles.slider1,'Value',pars.(apName).([p,'wid']))
set(handles.edit5,'String',num2str(pars.(apName).([p,'pwid'])*1e3))
set(handles.slider2,'Value',pars.(apName).([p,'pwid']))
if strcmp(pars.(apName).shape,'ellipse')
  set(handles.radiobutton4,'Value',0)
  set(handles.radiobutton3,'Value',1)
elseif strcmp(pars.(apName).shape,'rectangle')
  set(handles.radiobutton3,'Value',0)
  set(handles.radiobutton4,'Value',1)
else
  errordlg('Unknown aperture shape','apertures error')
end
set(handles.checkbox1,'Value',pars.(apName).active)
setBpmList(handles,pars);
% ref orbit
if strcmp(pars.(apName).reforbit,'default')
  set(handles.text9,'String','Use Default')
else
  set(handles.text9,'String','User Defined')
end


% --- Set new reference orbit
function pushbutton11_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton11 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% --- Check want to do this
resp=questdlg('Set New Reference Orbit for this Aperture?','New Ref Orbit');
if ~strcmp(resp,'Yes'); return; end;

[stat pars]=apertures('GetPars');
if stat{1}~=1
  errordlg(stat{2},'GetPars Error')
  return
end

% --- Get reference orbit
% Check there is enough bpm data
[stat bsize]=FlHwUpdate('buffersize');
if stat{1}~=1 || bsize<pars.bpmave
  errordlg('Not enough buffered bpm data or not available','bpm ave error')
  return
end
% Get aperture selected
alist=get(handles.popupmenu1,'String'); apName=alist{get(handles.popupmenu1,'Value')};
% Get averaged bpm readings and set reference orbit
% Check there is enough bpm data
[stat bsize]=FlHwUpdate('buffersize');
if stat{1}~=1 || bsize<pars.bpmave
  errordlg('Not enough buffered bpm data or not available','bpmave error')
  return
end
[stat bpmdata]=FlHwUpdate('bpmave',pars.bpmave,[pars.minq,pars.maxrms,isequal(pars.whichICT,'ICT1X'),0]); if stat{1}~=1; return; end;
for i_ind=1:6
  newpars.(apName).reforbit(:,i_ind)=bpmdata{1}(:,i_ind);
end
stat=apertures('SetPars',newpars);
if stat{1}~=1
  errordlg(stat{2},'Set Pars Error')
  return
end
set(handles.text9,'String',datestr(now))


% --- Executes on button press in checkbox2.
function checkbox2_Callback(hObject, eventdata, handles)
% hObject    handle to checkbox2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of checkbox2

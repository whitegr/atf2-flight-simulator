function varargout = aperturesGui_makeNew(varargin)
% APERTURESGUI_MAKENEW M-file for aperturesGui_makeNew.fig
%      APERTURESGUI_MAKENEW, by itself, creates a new APERTURESGUI_MAKENEW or raises the existing
%      singleton*.
%
%      H = APERTURESGUI_MAKENEW returns the handle to a new APERTURESGUI_MAKENEW or the handle to
%      the existing singleton*.
%
%      APERTURESGUI_MAKENEW('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in APERTURESGUI_MAKENEW.M with the given input arguments.
%
%      APERTURESGUI_MAKENEW('Property','Value',...) creates a new APERTURESGUI_MAKENEW or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before aperturesGui_makeNew_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to aperturesGui_makeNew_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help aperturesGui_makeNew

% Last Modified by GUIDE v2.5 03-Apr-2009 14:31:43

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @aperturesGui_makeNew_OpeningFcn, ...
                   'gui_OutputFcn',  @aperturesGui_makeNew_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before aperturesGui_makeNew is made visible.
function aperturesGui_makeNew_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to aperturesGui_makeNew (see VARARGIN)

% Choose default command line output for aperturesGui_makeNew
handles.output = handles;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes aperturesGui_makeNew wait for user response (see UIRESUME)
uiwait(handles.figure1);
delete(handles.figure1)

% --- Outputs from this function are returned to the command line.
function varargout = aperturesGui_makeNew_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = [];


% --- launch BeamlineViewer
function pushbutton1_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global FL
FL.Gui.BeamlineViewer=BeamlineViewer;

% --- Select element from viewer
function pushbutton2_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global FL BEAMLINE
if isfield(FL,'Gui') && isfield(FL.Gui,'BeamlineViewer') && ishandle(FL.Gui.BeamlineViewer.figure1)
  han=FL.Gui.BeamlineViewer;
  maglist=get(han.listbox1,'String');
  magsel=get(han.listbox1,'Value');
  bi=str2double(regexp(maglist{magsel(1)},'^\d+','match'));
  if isnan(bi)
    errordlg('Error getting index from BeamlineViewer','Index Error')
    return
  end
  set(handles.edit1,'String',num2str(bi))
  set(handles.edit2,'String',BEAMLINE{bi}.Name)
else
  errordlg('Launch BeamlineViewer first','No BeamlineViewer')
end

% --- Make new
function pushbutton3_Callback(hObject, eventdata, handles) %#ok<*INUSL,*DEFNU>
% hObject    handle to pushbutton3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
ib=str2double(get(handles.edit1,'String'));
name=get(handles.edit2,'String');
if ~isnan(ib) && ib && ~isempty(name) && ~strcmp(name,'none')
  stat=apertures('new',ib,name);
  if stat{1}~=1
    errordlg(stat{2},'New Aperture Gen')
    return
  end
  uiresume(handles.figure1)
else
  errordlg('No or bad user input','New Aperture Gen')
end

% --- Cancel
function pushbutton4_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
uiresume(handles.figure1)


function edit2_Callback(hObject, eventdata, handles) %#ok<*INUSD>
% hObject    handle to edit2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit2 as text
%        str2double(get(hObject,'String')) returns contents of edit2 as a double


% --- Executes during object creation, after setting all properties.
function edit2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit1_Callback(hObject, eventdata, handles)
% hObject    handle to edit1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit1 as text
%        str2double(get(hObject,'String')) returns contents of edit1 as a double
global BEAMLINE

bi=round(str2double(get(hObject,'String')));
if isnan(bi) || bi<=0 || bi>length(BEAMLINE)
  errordlg('Invalid Beamline Index Entry','Aperture Gen Error')
  return
end
set(handles.edit2,'String',BEAMLINE{bi}.Name)

% --- Executes during object creation, after setting all properties.
function edit1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

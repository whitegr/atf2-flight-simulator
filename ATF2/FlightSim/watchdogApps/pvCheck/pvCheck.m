function [stat output] = pvCheck(cmd,varargin)
% PVCHECK
%   Watchdog app to keep track of EPICS PVs
persistent pars
stat{1}=1; output=[];

% Make sure initialised
if isempty(pars) && ~isequal(cmd,'init')
  [stat pars]=initPars(pars);
end

% Methods
switch lower(cmd)
  case 'init'
    [stat pars]=initPars(pars);
  case 'isalarm'
    [stat pars]=update(pars);
    output=pars.alarm;
  case 'getpars'
    output=pars;
  case 'setpars'
    if nargin>0
      fn=fieldnames(varargin{1});
      for ifn=1:length(fn)
        pars.(fn{ifn})=varargin{1}.(fn{ifn});
      end
    end
  case 'update'
    [stat pars]=update(pars);
  otherwise
    stat{1}=-1; stat{2}='Unknown command';
    return
end

%% Internal functions
function [stat pars]=initPars(pars)
pars.active=true;
[stat pars]=update(pars);

function [stat pars]=update(pars)
global FL
pars.alarm=0; stat{1}=1;
missingPVs=FlCA('pvlist');
if isempty(missingPVs)
  missingPVs='None';
else
  pars.alarm=2;
end
% Update gui if open
if isfield(FL.Gui,'pvCheckGui') && ishandle(FL.Gui.pvCheckGui.figure1)
  set(FL.Gui.pvCheckGui.listbox1,'Value',1);
  set(FL.Gui.pvCheckGui.listbox1,'String',missingPVs);
end
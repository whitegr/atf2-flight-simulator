#!/bin/bash

echo "Monitoring PS ID: $1"

# get running list
RUNLIST="`ls | sed -n 's/RUNNING_\(.*\)/\1/p'`"

# get Server instance that will run
for i in {1..20};
do
  test="1"
  for j in $RUNLIST;
   do
    if [ $i == $j ]
    then
      test="0"
    fi
  done
  if [ $test == "1" ]
  then
    break
  fi
done

# Wait for server running process to end
psid=$1
#sleep 5
#if [ $i != "1" ]
#then
#  set -- $thispsid
#  foo=$1
#  shift
#  psid=$@
#else
#  psid="$thispsid"
#fi
while [ -n "`ps ${psid} | grep ${psid}`" ]
do
  sleep 1
done

# If RUNNING file still exists delete it
if [ -a "RUNNING_${i}" ]
then
  rm RUNNING_${i}
fi

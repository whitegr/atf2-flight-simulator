%function lucretia(iseed)
clear all global
global BEAMLINE PS GIRDER VERB MDATA FL INSTR%#ok<NUSED>

% Ring+EXT+FFS lattice
[stat, Model.Initial] = XSIFToLucretia('ATF2.xsif','ATF2_full','TWSS0','BEAM_IN') ;
if stat{1}~=1; error(stat{2}); end;
 addMult; % add in measured multipole components
% Initial parameters
Model.Initial.x.NEmit = 3.0364e-006;
Model.Initial.x.pos = 0e-6;
Model.Initial.x.ang = 0e-6;
Model.Initial.y.NEmit = 25e-9;
Model.Initial.y.pos = 0;
Model.Initial.y.ang = 0;
Model.Initial.zpos = 0; % arrival time in meters, zpos < 0 == early arrival wrt reference 
Model.Initial.Momentum=1.28;
Model.Initial.SigPUncorrel = Model.Initial.Momentum * 0.8e-3; % uncorrelated RMS momentum spread in GeV/c
Model.Initial.SigPUncorrel
Model.Initial.PZCorrel = 0; % longitudinally-correlated momentum spread in GeV/c/m.
Model.Initial.NBunch = 1;
Model.Initial.BunchInterval = 337e-9;
Model.Initial.sigz = 8e-3; % RMS Bunch length
Model.Initial.Q = 1e10 * 1.6021773e-19; % Bunch charge in Coulombs
iex_ele=findcells(BEAMLINE,'Name','IEX');iex_ele=iex_ele(end);
stat = SetDesignMomentumProfile( 1, length(BEAMLINE), Model.Initial.Q, Model.Initial.Momentum, Model.Initial.Momentum);
if stat{1}~=1; error(stat{2}); end;
Model.Initial.x.Twiss.beta=6.543;
Model.Initial.x.Twiss.alpha=0.99638;
Model.Initial.y.Twiss.beta=3.2378;
Model.Initial.y.Twiss.alpha=-1.9183;
[stat, twiss] = GetTwiss(iex_ele(1),1,Model.Initial.x.Twiss,Model.Initial.y.Twiss) ;
if stat{1}~=1; error(stat{2}); end;
Model.Initial_IEX=Model.Initial;
Model.Initial.x.Twiss.beta=twiss.betax(1); Model.Initial.y.Twiss.beta=twiss.betay(1);
Model.Initial.x.Twiss.alpha=twiss.alphay(1); Model.Initial.y.Twiss.alpha=twiss.alphay(1);
Model.Initial.x.Twiss.eta=twiss.etax(1); Model.Initial.y.Twiss.eta=twiss.etay(1);
Model.Initial.x.Twiss.etap=twiss.etapx(1); Model.Initial.y.Twiss.eta=twiss.etapy(1);
[stat, Model.Twiss] = GetTwiss(1,length(BEAMLINE),Model.Initial.x.Twiss,Model.Initial.y.Twiss) ;
if stat{1}~=1; error(stat{2}); end;

% Track indicies
Model.startTrack=1;
Model.ip_ind=length(BEAMLINE);

% FF start element
Model.ffStart.ind=findcells(BEAMLINE,'Name','BEGFF');

% Sim params
Model.errSig = 3; % truncate gaussian errors to this sigma level
Model.align.bpm_cres = 100e-9; % Resolution of main BPMs
Model.align.bpm_sres = 10e-6;
Model.align.bpm_ipres = 2e-9;
Model.align.bpm_bres = 10e-6;
Model.ipLaserRes = [2e-9 2e-9]; % IP SM resolution
Model.bpm.DeltaB_qalign=0.8; % Fractional Quad k change for alignment
Model.align.magbpm_x = 30e-6; % Bpm feducial alignment to mag centre
Model.align.magbpm_y = 30e-6;
Model.align.rot = 300e-6;
Model.align.x = 200e-6;
Model.align.y = 200e-6;
Model.align.z = 200e-6;
Model.align.x_bend = 200e-6;
Model.align.y_bend = 200e-6;
Model.align.rot_bend=300e-6; % Set magnet rotation alignment tolerance for bend magnets (RMS / m)
Model.align.dB = 1e-4;
Model.align.dB_sext = 1e-4;
Model.align.dB_syst = 1e-4;
Model.align.dPSAmpl = 0;
Model.align.ps_step = 0;
Model.align.cor_psStep = 1e-6 ; % radians
Model.align.cor_dAmpl = 0;
Model.align.mag_dAmpl = 0;
Model.align.mover_speed = [30e-6 30e-6 15e-6]; % metres/rad per second
Model.align.mover_step = [300e-9 300e-9 600e-9]; % mover step size
Model.align.mover_accuracy_x = 1e-6; % Accuracy of position + tilt positioning / m | rad
Model.align.mover_accuracy_y = 1e-6;
Model.align.mover_accuracy_tilt = 10e-6;
% Random jitter on incoming beam
Model.align.incoming_x = 100e-9;
Model.align.incoming_xp = 100e-9;
Model.align.incoming_y = 100e-9;
Model.align.incoming_yp = 100e-9;
Model.align.incoming_e = 1e-4; % in %
% Error on Energy changes
Model.dE_set_error = 0;
% Ground Motion Model and component jitter
Model.align.gmModel='None'; 
Model.align.jitter_quad=25e-9;
Model.align.jitter_sext=25e-9;
% Dynamic magnetic field errors
Model.align.dB_quad_dyn=0;
Model.align.dB_sext_dyn=0;
Model.align.dB_bend_dyn=0;
Model.align.dB_cor_dyn=1e-4;
% BPM-mag alignment for ext
Model.ext.align = Model.align;
Model.ext.align.magbpm_x = 10e-6;

% Get Handles
Model.handles.BPM = findcells(BEAMLINE,'Class','MONI');
Model.handles.Quad = findcells(BEAMLINE,'Class','QUAD');
Model.handles.Sext = findcells(BEAMLINE,'Class','SEXT');
Model.handles.Oct = findcells(BEAMLINE,'Class','OCTU');
Model.handles.SQuad = findcells(BEAMLINE,'Name','QK*X');
  
% assign element slices and blocks
[stat,Model.slices] = SetElementSlices( 1, length(BEAMLINE) );
[stat,Model.blocks] = SetElementBlocks( 1, length(BEAMLINE) );

% Boundaries
Model.extStart=findcells(BEAMLINE,'Name','IEX');Model.extStart=Model.extStart(end);

% Assign element groupings
[stat, Model.magGroup.BPM.Off] = MakeErrorGroup( {'BEAMLINE'; 'MONI'} , [Model.extStart length(BEAMLINE)], 'Offset', 1, 'BPM Offset');
[stat, Model.magGroup.Quad.Off] = MakeErrorGroup( {'BEAMLINE'; 'QUAD'} , [Model.extStart length(BEAMLINE)], 'Offset', 2, 'Quads and SQuads index' );
[stat, Model.magGroup.Quad.dB] = MakeErrorGroup( {'BEAMLINE'; 'QUAD'} , [Model.extStart length(BEAMLINE)], 'dB', 1, 'Quads and SQuads dB');
[stat, Model.magGroup.Quad.OffUse] = MakeErrorGroup( {'BEAMLINE'; 'QUAD'} , [Model.extStart length(BEAMLINE)], 'Offset', 1, 'Quads and SQuads Offset');
[stat, Model.magGroup.Sext.Off] = MakeErrorGroup( {'BEAMLINE'; 'SEXT'} , [Model.extStart length(BEAMLINE)], 'Offset', 1, 'Sextupoles Offset');
[stat, Model.magGroup.Mult.Off] = MakeErrorGroup( {'BEAMLINE'; 'MULT'} , [Model.extStart length(BEAMLINE)], 'Offset', 1, 'Mult Offset' );
[stat, Model.magGroup.SBEN.Off] = MakeErrorGroup( {'BEAMLINE'; 'SBEN'} , [Model.extStart length(BEAMLINE)], 'Offset', 1, 'SBEN Offset' );
[stat, Model.magGroup.Sext.dB] = MakeErrorGroup( {'BEAMLINE'; 'SEXT'} , [Model.extStart length(BEAMLINE)], 'dB', 1, 'Sextupoles dB' );
[stat, Model.magGroup.Mult.dB] = MakeErrorGroup( {'BEAMLINE'; 'MULT'} , [Model.extStart length(BEAMLINE)], 'dB', 1, 'Mult dB');
[stat, Model.magGroup.SBEN.dB] = MakeErrorGroup( {'BEAMLINE'; 'SBEN'} , [Model.extStart length(BEAMLINE)], 'dB', 1, 'SBEN dB');
[stat, Model.magGroup.XCOR.dB] = MakeErrorGroup( {'BEAMLINE'; 'XCOR'} , [Model.extStart length(BEAMLINE)], 'dB', 1, 'XCOR dB');
[stat, Model.magGroup.YCOR.dB] = MakeErrorGroup( {'BEAMLINE'; 'YCOR'} , [Model.extStart length(BEAMLINE)], 'dB', 1, 'YCOR dB');
% FFS magnet groupings
[stat, Model.ffmagGroup.BPM.Off] = MakeErrorGroup( {'BEAMLINE'; 'MONI'} , [Model.ffStart.ind length(BEAMLINE)], 'Offset', 1, 'ff BPM Offset');
[stat, Model.ffmagGroup.Quad.Off] = MakeErrorGroup( {'BEAMLINE'; 'QUAD'} , [Model.ffStart.ind length(BEAMLINE)], 'Offset', 2, 'ff Quads and SQuads index');
[stat, Model.ffmagGroup.Quad.dB] = MakeErrorGroup( {'BEAMLINE'; 'QUAD'} , [Model.ffStart.ind length(BEAMLINE)], 'dB', 1, 'ff Quads and SQuads dB');
[stat, Model.ffmagGroup.Quad.OffUse] = MakeErrorGroup( {'BEAMLINE'; 'QUAD'} , [Model.ffStart.ind length(BEAMLINE)], 'Offset', 1, 'ff Quads and SQuads Offset');
[stat, Model.ffmagGroup.Sext.Off] = MakeErrorGroup( {'BEAMLINE'; 'SEXT'} , [Model.ffStart.ind length(BEAMLINE)], 'Offset', 1, 'ff Sextupoles Offset');
[stat, Model.ffmagGroup.Mult.Off] = MakeErrorGroup( {'BEAMLINE'; 'MULT'} , [Model.ffStart.ind length(BEAMLINE)], 'Offset', 1, 'ff Mult Offset');
[stat, Model.ffmagGroup.SBEN.Off] = MakeErrorGroup( {'BEAMLINE'; 'SBEN'} , [Model.ffStart.ind length(BEAMLINE)], 'Offset', 1, 'ff SBEN Offset');
[stat, Model.ffmagGroup.Sext.dB] = MakeErrorGroup( {'BEAMLINE'; 'SEXT'} , [Model.ffStart.ind length(BEAMLINE)], 'dB', 1, 'ff Sextupoles dB');
[stat, Model.ffmagGroup.Mult.dB] = MakeErrorGroup( {'BEAMLINE'; 'MULT'} , [Model.ffStart.ind length(BEAMLINE)], 'dB', 1, 'ff Mult dB');
[stat, Model.ffmagGroup.SBEN.dB] = MakeErrorGroup( {'BEAMLINE'; 'SBEN'} , [Model.ffStart.ind length(BEAMLINE)], 'dB', 1, 'ff SBEN dB');
% EXT magnet groupings
[stat, Model.EXTmagGroup.BPM.Off] = MakeErrorGroup( {'BEAMLINE'; 'MONI'} , [1 Model.ffStart.ind], 'Offset', 1, 'ff BPM Offset');
[stat, Model.EXTmagGroup.Quad.Off] = MakeErrorGroup( {'BEAMLINE'; 'QUAD'} , [1 Model.ffStart.ind], 'Offset', 2, 'ff Quads and SQuads index');
[stat, Model.EXTmagGroup.Quad.dB] = MakeErrorGroup( {'BEAMLINE'; 'QUAD'} , [1 Model.ffStart.ind], 'dB', 1, 'ff Quads and SQuads dB');
[stat, Model.EXTmagGroup.Quad.OffUse] = MakeErrorGroup( {'BEAMLINE'; 'QUAD'} , [1 Model.ffStart.ind], 'Offset', 1, 'ff Quads and SQuads Offset');
[stat, Model.EXTmagGroup.Sext.Off] = MakeErrorGroup( {'BEAMLINE'; 'SEXT'} , [1 Model.ffStart.ind], 'Offset', 1, 'ff Sextupoles Offset');
[stat, Model.EXTmagGroup.Mult.Off] = MakeErrorGroup( {'BEAMLINE'; 'MULT'} , [1 Model.ffStart.ind], 'Offset', 1, 'ff Mult Offset');
[stat, Model.EXTmagGroup.SBEN.Off] = MakeErrorGroup( {'BEAMLINE'; 'SBEN'} , [1 Model.ffStart.ind], 'Offset', 1, 'ff SBEN Offset');
[stat, Model.EXTmagGroup.Sext.dB] = MakeErrorGroup( {'BEAMLINE'; 'SEXT'} , [1 Model.ffStart.ind], 'dB', 1, 'ff Sextupoles dB');
[stat, Model.EXTmagGroup.Mult.dB] = MakeErrorGroup( {'BEAMLINE'; 'MULT'} , [1 Model.ffStart.ind], 'dB', 1, 'ff Mult dB');
[stat, Model.EXTmagGroup.SBEN.dB] = MakeErrorGroup( {'BEAMLINE'; 'SBEN'} , [1 Model.ffStart.ind], 'dB', 1, 'ff SBEN dB');
% Ring element groupings
[stat, Model.magGroup.RBPM.Off] = MakeErrorGroup( {'BEAMLINE'; 'MONI'} , [1 Model.extStart], 'Offset', 1, 'BPM Offset');
[stat, Model.magGroup.RQuad.Off] = MakeErrorGroup( {'BEAMLINE'; 'QUAD'} , [1 Model.extStart], 'Offset', 2, 'Quads and SQuads index' );
[stat, Model.magGroup.RQuad.dB] = MakeErrorGroup( {'BEAMLINE'; 'QUAD'} , [1 Model.extStart], 'dB', 1, 'Quads and SQuads dB');
[stat, Model.magGroup.RQuad.OffUse] = MakeErrorGroup( {'BEAMLINE'; 'QUAD'} , [1 Model.extStart], 'Offset', 1, 'Quads and SQuads Offset');
[stat, Model.magGroup.RSext.Off] = MakeErrorGroup( {'BEAMLINE'; 'SEXT'} , [1 Model.extStart], 'Offset', 1, 'Sextupoles Offset');
[stat, Model.magGroup.ROct.Off] = MakeErrorGroup( {'BEAMLINE'; 'OCTU'} , [1 Model.extStart], 'Offset', 1, 'Octupoles Offset');
[stat, Model.magGroup.RMult.Off] = MakeErrorGroup( {'BEAMLINE'; 'MULT'} , [1 Model.extStart], 'Offset', 1, 'Mult Offset' );
[stat, Model.magGroup.RSBEN.Off] = MakeErrorGroup( {'BEAMLINE'; 'SBEN'} , [1 Model.extStart], 'Offset', 1, 'SBEN Offset' );
[stat, Model.magGroup.RSext.dB] = MakeErrorGroup( {'BEAMLINE'; 'SEXT'} , [1 Model.extStart], 'dB', 1, 'Sextupoles dB' );
[stat, Model.magGroup.RMult.dB] = MakeErrorGroup( {'BEAMLINE'; 'MULT'} , [1 Model.extStart], 'dB', 1, 'Mult dB');
[stat, Model.magGroup.RSBEN.dB] = MakeErrorGroup( {'BEAMLINE'; 'SBEN'} , [1 Model.extStart], 'dB', 1, 'SBEN dB');
[stat, Model.magGroup.RXCOR.dB] = MakeErrorGroup( {'BEAMLINE'; 'XCOR'} , [1 Model.extStart], 'dB', 1, 'SBEN dB');
[stat, Model.magGroup.RYCOR.dB] = MakeErrorGroup( {'BEAMLINE'; 'YCOR'} , [1 Model.extStart], 'dB', 1, 'SBEN dB');
Model.magGroup.SQuad_id=[];
for g_id=1:length(Model.magGroup.Quad.dB.ClusterList)
  Model.PSind.Quad(g_id)=length(PS)+1;
  stat=AssignToPS( Model.magGroup.Quad.dB.ClusterList(g_id).index, length(PS)+1 );
  if stat{1}~=1; error(stat{2:end}); end;
  PS(Model.PSind.Quad(g_id)).dAmpl=0;
  PS(Model.PSind.Quad(g_id)).Step=0;
  % Turn off skew quads
  for ind=Model.magGroup.Quad.dB.ClusterList(g_id).index(1):Model.magGroup.Quad.dB.ClusterList(g_id).index(end)
    if find(ind==Model.handles.SQuad)
      Model.magGroup.SQuad_id=[Model.magGroup.SQuad_id g_id];
      PS(Model.PSind.Quad(g_id)).SetPt=0;
      PS(Model.PSind.Quad(g_id)).Ampl=0;
      break
    end % if squad
  end % for ind
end % for g_id
for g_id=1:length(Model.magGroup.Sext.dB.ClusterList)
  Model.PSind.Sext(g_id)=length(PS)+1;
  stat=AssignToPS( Model.magGroup.Sext.dB.ClusterList(g_id).index, length(PS)+1 );
  if stat{1}~=1; error(stat{2:end}); end;
  PS(Model.PSind.Sext(g_id)).dAmpl=0;
end % for g_id
for g_id=1:length(Model.magGroup.SBEN.dB.ClusterList)
  Model.PSind.SBEN(g_id)=length(PS)+1;
  stat=AssignToPS( Model.magGroup.SBEN.dB.ClusterList(g_id).index, length(PS)+1 );
  if stat{1}~=1; error(stat{2:end}); end;
  PS(Model.PSind.SBEN(g_id)).dAmpl=0;
end % for g_id
for g_id=1:length(Model.magGroup.RQuad.dB.ClusterList)
  Model.PSind.RQuad(g_id)=length(PS)+1;
  stat=AssignToPS( Model.magGroup.RQuad.dB.ClusterList(g_id).index, length(PS)+1 );
  if stat{1}~=1; error(stat{2:end}); end;
  PS(Model.PSind.RQuad(g_id)).dAmpl=0;
end % for g_id
for g_id=1:length(Model.magGroup.RSext.dB.ClusterList)
  Model.PSind.RSext(g_id)=length(PS)+1;
  stat=AssignToPS( Model.magGroup.RSext.dB.ClusterList(g_id).index, length(PS)+1 );
  if stat{1}~=1; error(stat{2:end}); end;
  PS(Model.PSind.RSext(g_id)).dAmpl=0;
end
for g_id=1:length(Model.magGroup.RSBEN.Off.ClusterList)
  bhe=[]; bend=[]; bh1r27=[];
  for ind=Model.magGroup.RSBEN.Off.ClusterList(g_id).index
    if ~isempty(findstr(BEAMLINE{ind}.Name,'BH1R27')) && isfield(BEAMLINE{ind},'B')
      bh1r27(end+1)=ind;
    elseif isfield(BEAMLINE{ind},'B') && ind<Model.extStart
      bend(end+1)=ind;
    end
  end
  if ~isempty(bend)
    Model.PSind.RSBEN(g_id)=length(PS)+1;
    stat=AssignToPS( bend, length(PS)+1 );
    if stat{1}~=1; error(stat{2:end}); end;
    PS(Model.PSind.RSBEN(g_id)).dAmpl=0;
  end  
  if ~isempty(bh1r27)
    Model.PSind.RSBEN(g_id)=length(PS)+1;
    stat=AssignToPS( bh1r27, length(PS)+1 );
    if stat{1}~=1; error(stat{2:end}); end;
    PS(Model.PSind.RSBEN(g_id)).dAmpl=0;
  end
end % for g_ig
% Vibration table elements
iSF1=findcells(BEAMLINE,'Name','SF1FF');
iQD0=findcells(BEAMLINE,'Name','QD0FF');
Model.vibTable.ind = iSF1(1)-2:BEAMLINE{iQD0(end)}.Block(end)+2; % inlclude BPMs before SF1 and after QD0
Model.vibTable.S=[];
for iEle=Model.vibTable.ind
  Model.vibTable.S=[Model.vibTable.S BEAMLINE{iEle}.S];
end

moverList=[findcells(BEAMLINE,'Name','AQ*') findcells(BEAMLINE,'Name','AS*') findcells(BEAMLINE,'Name','AFB*') ];
Model.moverList=[];
for g_id=1:length(Model.magGroup.Quad.dB.ClusterList)
  mag_ind=BEAMLINE{Model.magGroup.Quad.OffUse.ClusterList(g_id).index(1)}.Block(1):...
    BEAMLINE{Model.magGroup.Quad.OffUse.ClusterList(g_id).index(1)}.Block(end);
  % Find BPM associated with this magnet if one closer than 10cm (use
  % closest one to mag center if more than 1)
  qbpm=find(cellfun(@(x) (x.S-BEAMLINE{Model.magGroup.Quad.dB.ClusterList(g_id).index(end)+1}.S)<0.1 & ...
    x.S>BEAMLINE{Model.magGroup.Quad.dB.ClusterList(g_id).index(end)}.S, {BEAMLINE{Model.handles.BPM}}));
  if isempty(qbpm)
    qbpm=find(cellfun(@(x) (BEAMLINE{Model.magGroup.Quad.dB.ClusterList(g_id).index(1)}.S-x.S)<0.1 & ...
      x.S<BEAMLINE{Model.magGroup.Quad.dB.ClusterList(g_id).index(1)}.S, {BEAMLINE{Model.handles.BPM}}));
  end % if isempty(qbpm)
  if length(qbpm)>1
    bpmDist=cellfun(@(x) abs(BEAMLINE{Model.magGroup.Quad.dB.ClusterList(g_id).index(end)}.S-x.S),{BEAMLINE{Model.handles.BPM(qbpm)}});
    [minVal minInd]=min(bpmDist); qbpm=qbpm(minInd);
  end % if length(qbpm)>1
  if ~isempty(qbpm)
    qbpm=Model.handles.BPM(qbpm);
    if ~isequal(BEAMLINE{qbpm(1)}.Class,'MONI'); error('QBPM location error!'); end;
    if qbpm<mag_ind(1)
      mag_ind=[qbpm mag_ind]; %#ok<AGROW>
    elseif qbpm>mag_ind(end)
      mag_ind=[mag_ind qbpm]; %#ok<AGROW>
    end % where is qbpm?
    Model.magGroup.Quad.Bpm(g_id).Ind=qbpm;
    Model.magGroup.Quad.Bpm(g_id).Han=find(Model.handles.BPM==qbpm);
  else
    Model.magGroup.Quad.Bpm(g_id).Ind=[];
    Model.magGroup.Quad.Bpm(g_id).Han=[];
  end % isempty(qbpm)?
  % Put Quad stuff on same girder
  gnum=length(GIRDER)+1;
  stat = AssignToGirder( mag_ind, gnum, 0 );
  if stat{1}
    Model.Gind.Quad(g_id)=gnum;
    if any(ismember(GIRDER{gnum}.Element(1):GIRDER{gnum}.Element(end),moverList))
      [stat,GIRDER{gnum}] = AddMoverToGirder( [1 3 6], GIRDER{gnum} ); if ~stat{1}; error(stat{2}); end;
      GIRDER{gnum}.MoverStep=Model.align.mover_step;
      Model.moverList=[Model.moverList gnum];
    end % if mover on girder
  elseif isfield(BEAMLINE{Model.magGroup.Quad.OffUse.ClusterList(g_id).index(1)},'Girder') && ...
      BEAMLINE{Model.magGroup.Quad.OffUse.ClusterList(g_id).index(1)}.Girder
    Model.Gind.Quad(g_id)=BEAMLINE{mag_ind(1)}.Girder;
  else
    error(['Error with Girder assignment for Quad: ',num2str(g_id)])
  end % if stat{1}
end
for g_id=1:length(Model.magGroup.Sext.dB.ClusterList)
  mag_ind=BEAMLINE{Model.magGroup.Sext.Off.ClusterList(g_id).index(1)}.Block(1):...
  BEAMLINE{Model.magGroup.Sext.Off.ClusterList(g_id).index(1)}.Block(end);
  % Find BPM associated with this magnet if one closer than 10cm (use
  % closest one to mag center if more than 1)
  sbpm=find(cellfun(@(x) (x.S-BEAMLINE{Model.magGroup.Sext.dB.ClusterList(g_id).index(end)+1}.S)<0.1 & ...
    x.S>BEAMLINE{Model.magGroup.Sext.dB.ClusterList(g_id).index(end)}.S, {BEAMLINE{Model.handles.BPM}}));
  if isempty(sbpm)
    sbpm=find(cellfun(@(x) (BEAMLINE{Model.magGroup.Sext.dB.ClusterList(g_id).index(1)}.S-x.S)<0.1 & ...
      x.S<BEAMLINE{Model.magGroup.Sext.dB.ClusterList(g_id).index(1)}.S, {BEAMLINE{Model.handles.BPM}}));
  end % if isempty(qbpm)
  if length(sbpm)>1
    bpmDist=cellfun(@(x) abs(BEAMLINE{Model.magGroup.Sext.dB.ClusterList(g_id).index(end)}.S-x.S),{BEAMLINE{Model.handles.BPM(sbpm)}});
    [minVal minInd]=min(bpmDist); sbpm=sbpm(minInd);
  end % if length(sbpm)>1
  if ~isempty(sbpm)
    sbpm=Model.handles.BPM(sbpm);
    if ~isequal(BEAMLINE{sbpm}.Class,'MONI'); error('QBPM location error!'); end;
    if sbpm<mag_ind(1)
      mag_ind=[sbpm mag_ind]; %#ok<AGROW>
    elseif sbpm>mag_ind(end)
      mag_ind=[mag_ind sbpm]; %#ok<AGROW>
    end % where is sbpm?
    Model.magGroup.Sext.Bpm(g_id).Ind=sbpm;
    Model.magGroup.Sext.Bpm(g_id).Han=find(Model.handles.BPM==sbpm);
  else
    Model.magGroup.Sext.Bpm(g_id).Ind=[];
    Model.magGroup.Sext.Bpm(g_id).Han=[];
  end % isempty(qbpm)?
  % Put Sextupole stuff on same girder
  gnum=length(GIRDER)+1;
  stat = AssignToGirder( mag_ind, gnum, 0 );
  if stat{1}
    Model.Gind.Sext(g_id)=gnum;
    if any(ismember(GIRDER{gnum}.Element(1):GIRDER{gnum}.Element(end),moverList))
      [stat,GIRDER{gnum}] = AddMoverToGirder( [1 3 6], GIRDER{gnum} ); if ~stat{1}; error(stat{2}); end;
      GIRDER{gnum}.MoverStep=Model.align.mover_step;
      Model.moverList=[Model.moverList gnum];
    end % if mover on girder
  elseif isfield(BEAMLINE{Model.magGroup.Sext.Off.ClusterList(g_id).index(1)},'Girder') && ...
      BEAMLINE{Model.magGroup.Sext.Off.ClusterList(g_id).index(1)}.Girder
    Model.Gind.Sext(g_id)=BEAMLINE{mag_ind(1)}.Girder;
  else
    error(['Error with Girder assignment for Sext: ',num2str(g_id)])
  end % if stat{1}
end
% Add feedback girder
stat = AssignToGirder( findcells(BEAMLINE,'Name','MFB2FF') , length(GIRDER)+1, 0 );
if stat{1}~=1; error(stat{2}); end;
Model.moverList=[Model.moverList length(GIRDER)];
[stat,GIRDER{end}] = AddMoverToGirder( [1 3 6], GIRDER{end} ); if ~stat{1}; error(stat{2}); end;

% Ground Motion Data
lastGirder=0;Model.gmData=[];lastEle=0;
for iEle=1:length(BEAMLINE)
  if isfield(BEAMLINE{iEle},'Girder') && BEAMLINE{iEle}.Girder && BEAMLINE{iEle}.Girder~=lastGirder
    gmInd=length(Model.gmData)+1;
    Model.gmData(gmInd).sCoords=GIRDER{BEAMLINE{iEle}.Girder}.S ;
    Model.gmData(gmInd).girder=BEAMLINE{iEle}.Girder;
    lastGirder=BEAMLINE{iEle}.Girder;   
  elseif (~isfield(BEAMLINE{iEle},'Girder') || ~BEAMLINE{iEle}.Girder) && isfield(BEAMLINE{iEle},'Offset') && iEle>lastEle
    gmInd=length(Model.gmData)+1;
    if isfield(BEAMLINE{iEle},'Block')
      Model.gmData(gmInd).sCoords=[BEAMLINE{BEAMLINE{iEle}.Block(1)}.S BEAMLINE{BEAMLINE{iEle}.Block(2)}.S];
      Model.gmData(gmInd).element=BEAMLINE{iEle}.Block;
      lastEle=BEAMLINE{iEle}.Block(2);
    else
      Model.gmData(gmInd).sCoords=BEAMLINE{iEle}.S ;
      Model.gmData(gmInd).element=iEle;
      lastEle=iEle;
    end % if is block
  end % if element moveable and not on already dealt-with girder
end % for iEle (e- beamline)

% Make a group of all magnets
allmag_elements=[[PS([Model.PSind.Quad]).Element] [PS([Model.PSind.Sext]).Element]];
vals=sort(allmag_elements(1:2:end));
for mag_ind=1:length(vals)
  switch BEAMLINE{vals(mag_ind)}.Class
    case 'QUAD'
      Model.magGroup.AllMag{mag_ind}.Type = 'Quad';
    case 'SEXT'
      Model.magGroup.AllMag{mag_ind}.Type = 'Sext';
    case 'OCTU'
      Model.magGroup.AllMag{mag_ind}.Type = 'Oct';
    otherwise
      error('Bad type!')
  end
  % Assign pointers to specific magnet groups
  glist = [PS(Model.PSind.(Model.magGroup.AllMag{mag_ind}.Type)).Element];
  Model.magGroup.AllMag{mag_ind}.Pointer = find(glist(1:2:end)==vals(mag_ind));
end
  
% FB correctors
Cb = 3.335640952 ; % T.m / GeV
% Assign X-Correctors
nCor=0;
for iCor=findcells(BEAMLINE,'Class','XCOR')
  nCor=nCor+1;
  Model.PSind.cor.fbX1(nCor)=length(PS)+1;
  stat=AssignToPS( iCor, length(PS)+1 ); if stat{1}~=1; error(stat{2:end}); end;
  % Set Corrector B to put PS in rad units and set initially to zero
  BEAMLINE{iCor}.B=Cb * BEAMLINE{iCor}.P;
  PS(Model.PSind.cor.fbX1(nCor)).SetPt = 0;
  stat = PSTrim( Model.PSind.cor.fbX1(nCor) ); if stat{1}~=1; error(stat{2:end}); end;
end
% Assign Y-Correctors
nCor=0;
for iCor=findcells(BEAMLINE,'Class','YCOR')
  nCor=nCor+1;
  Model.PSind.cor.fbY1(nCor)=length(PS)+1;
  stat=AssignToPS( iCor, length(PS)+1 ); if stat{1}~=1; error(stat{2:end}); end;
  % Set Corrector B to put PS in rad units and set initially to zero
  BEAMLINE{iCor}.B=Cb * BEAMLINE{iCor}.P;
  PS(Model.PSind.cor.fbY1(nCor)).SetPt = 0;
  stat = PSTrim( Model.PSind.cor.fbY1(nCor) ); if stat{1}~=1; error(stat{2:end}); end;
end
% Pulse-pulse feedback parameters (FFS)
Model.fbBpmsToUse=[findcells(BEAMLINE,'Name','MFB2FF') findcells(BEAMLINE,'Name','MQD10BFF')];
Model.fbBpmsToUseX=[findcells(BEAMLINE,'Name','MFB1FF') findcells(BEAMLINE,'Name','MQF9BFF')];
Model.fbCorToUse=[findcells(BEAMLINE,'Name','ZV1FF') findcells(BEAMLINE,'Name','ZV11X')];
Model.fbCorToUseX=[findcells(BEAMLINE,'Name','ZH10X') findcells(BEAMLINE,'Name','ZH1FF')];
Model.fbWeight=0.1;

% Generate Beams
Beam1 = MakeBeam6DGauss(Model.Initial,10001,5,0) ;
Beam0 = makeSingleBunch(Beam1);
Beam2 = MakeBeam6DSparse( Model.Initial, 5, 31, 11 );

% Turn on tracking flags and set bpm resolutions and bpm types etc
ip_instr={'MBS2IP' 'MBS1IP'};
for bpm_ind=Model.handles.BPM
  BEAMLINE{bpm_ind}.TrackFlag.GetBPMData = 1;
  BEAMLINE{bpm_ind}.TrackFlag.GetBPMBeamPars = 1;
end % for bpm_ind
SetTrackFlags('GetInstData',1,1,length(BEAMLINE));
for ii=1:length(ip_instr)
  BEAMLINE{findcells(BEAMLINE,'Name',ip_instr{ii})}.TrackFlag.GetInstData = 1;
end % for ii

% Check tracking of beam through EXT + FFS
Beam1_IEX = MakeBeam6DGauss(Model.Initial_IEX,100000,5,0);
Beam0_IEX = makeSingleBunch(Beam1_IEX);
Beam2_IEX = MakeBeam6DSparse( Model.Initial_IEX, 5, 31, 11 );
[stat,beamout,instdata] = TrackThru( Model.extStart, findcells(BEAMLINE,'Name','MBS1IP'), Beam1_IEX, 1, 1, 0 );
if stat{1}~=1; error(stat{2}); end;
Model.doSMRes=false;
fprintf('IP beamsize: %g (x) %g (y)\n',std(beamout.Bunch.x(1,:)),std(beamout.Bunch.x(3,:)));
fprintf('IP beampos: %g (x) %g (y)\n',mean(beamout.Bunch.x(1,:)),mean(beamout.Bunch.x(3,:)));
% Save lattice
[Model,INSTR] = CreateINSTR(Model,instdata);
save ATF2lat BEAMLINE PS GIRDER FL MDATA INSTR Model Beam*

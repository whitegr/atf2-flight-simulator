nmult=findcells(BEAMLINE,'Name','*MULT');
goodlist={'QD6FFMULT' 'QF7FFMULT' 'QF5AFFMULT' 'QF5BFFMULT'};
for imult=nmult(~ismember(nmult,findcells(BEAMLINE,'Name','KEX*MULT')))
  BEAMLINE{imult}.B=BEAMLINE{imult}.B.*0;
  if ismember(BEAMLINE{imult}.Name,goodlist)
    %BEAMLINE{imult}.B=BEAMLINE{imult}.B.*0;
    for iT=1:length(BEAMLINE{imult}.Tilt)
      BEAMLINE{imult}.Tilt(iT)=pi*(rand*2-1);
    end
  end
end

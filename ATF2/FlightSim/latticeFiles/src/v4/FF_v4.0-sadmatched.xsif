! ==============================================================================
! 21-OCT-2008, G. White [SLAC]
!    Put in modifiers to fix s-bpm - Quad/Sext distances
!    Put in multipoles for ffs bends, sexts and final doublet quads
! 27-MAY-2008, M. Woodley [SLAC]
!    Optics v4.0
! 19-DEC-2007, M. Woodley [SLAC]
!    Optics v3.8 ... extraction septa realigned; rematch geometry and optics;
!    remove movers from QM16FF, QM15FF, QM14FF ... which QM's have movers and
!    which do not may change later
! 18-JUL-2007, M. Woodley [SLAC]
!    Optics v3.7
! 14-MAR-2007, M. Woodley [SLAC]
!    Turn off EXT sextupoles and retune FF sextupoles ... for simulations
! 28-NOV-2006, M. Woodley [SLAC]
!    Optics v3.6: A. Seryi's "ATF2_V3_3_12" optics of 31-JUL-2006 with reduced
!    U3224 aberration
! 01-JUL-2006, M. Woodley [SLAC]
!    Optics v3.5: B5/QD6 spacing adjustment per A. Seryi; change lengths of
!    all sextupoles from 200 mm to 90 mm; simplify element definitions to
!    facilitate translation to SAD (no inheritance)
! 26-MAY-2006, M. Woodley [SLAC]
!    Optics v3.3: rematch after fixing EXT bug; new FF layout per A. Seryi and
!    T. Okugi; per discussion at 9th ATF2 meeting: set drift distance from IP
!    to entrance face of dump bend to 1.5 m; set dump bend "Z-length" to
!    1.35 m; set drift distance from exit face of dump bend to entrance face
!    of dump to 1.0 m
! 26-APR-2006, M. Woodley [SLAC]
!    Redesigned EXT
! 02-FEB-2006, M. Woodley [SLAC]
!    Remove chicane; set quad center spacing between QD12X and QM16 to 1.5 m;
!    rematch
! 25-JUL-2005, M. Woodley [SLAC]
!    Rematch to modified extraction line diagnostics section
! 03-JUN-2005, M. Woodley
!    From A. Seryi's ATFEXTRSHRTFF6.mad: 0.8 m rectangular bends; remove OC0;
!    move OC1 away from SF1; match to new skew correction and emittance
!    diagnostic section, minimizing the strengths the QMs; move some magnet
!    definitions into ATF2_common.xsif 
! ------------------------------------------------------------------------------

! modifications to fix s-bpm - magnet distances
  LSMODS := 0.0655*0
  LSMODQ := 0.0505*0

! global parameters

  BXip  := 0.004
  BYip  := 0.0001
  DPXip := 0.139431518433

! ==============================================================================
! dipoles
! ------------------------------------------------------------------------------

! FF bends

  AB5FF := -0.024947966607 !half-angle
  AB2FF := -0.015087937324 !half-angle
  AB1FF := -0.02690209037  !half-angle

  LB5FF := ZBFF*AB5FF/SIN(AB5FF) !effective length
  LB2FF := ZBFF*AB2FF/SIN(AB2FF) !effective length
  LB1FF := ZBFF*AB1FF/SIN(AB1FF) !effective length

  B5FFa : SBEN, L=LB5FF/2, HGAP=GBFF, ANGLE=AB5FF, &
                E1=AB5FF, FINT=0.5, FINTX=0
  B5FFb : SBEN, L=LB5FF/2, HGAP=GBFF, ANGLE=AB5FF, &
                E2=AB5FF, FINT=0, FINTX=0.5
  B2FFa : SBEN, L=LB2FF/2, HGAP=GBFF, ANGLE=AB2FF, &
                E1=AB2FF, FINT=0.5, FINTX=0
  B2FFb : SBEN, L=LB2FF/2, HGAP=GBFF, ANGLE=AB2FF, &
                E2=AB2FF, FINT=0, FINTX=0.5
  B1FFa : SBEN, L=LB1FF/2, HGAP=GBFF, ANGLE=AB1FF, &
                E1=AB1FF, FINT=0.5, FINTX=0
  B1FFb : SBEN, L=LB1FF/2, HGAP=GBFF, ANGLE=AB1FF, &
                E2=AB1FF, FINT=0, FINTX=0.5

  B1MULT : MULTIPOLE, K1L=1.0373e-4/4, K2L=0.2658/4,&
           K3L=-3.5008/4, K4L=4.5121e3/4, APER=GBFF
  B2MULT : MULTIPOLE, K1L=8.3626e-5/4,K2L=0.1302/4,&
           K3L=0.2182/4, K4L=2.9669e3/4, APER=GBFF
  B5MULT : MULTIPOLE, K1L=7.2718e-5/4, K2L=0.0155/4,&
           K3L=1.0908/4, K4L=2.0943e3/4, APER=GBFF

! dump bend

  ABDUMP := 10*RADDEG                 !half-angle
  LBDUMP := ZBSHIC*ABDUMP/SIN(ABDUMP) !effective length
  
  BDUMPa : SBEN, L=LBDUMP/2, HGAP=GBSHIC, ANGLE=ABDUMP, &
                 E1=ABDUMP, FINT=0.5, FINTX=0
  BDUMPb : SBEN, L=LBDUMP/2, HGAP=GBSHIC, ANGLE=ABDUMP, &
                 E2=ABDUMP, FINT=0, FINTX=0.5

! ==============================================================================
! quadrupoles
! ------------------------------------------------------------------------------

  KLQM16FF :=  0.582086044737 ! 0.577245092355
  KLQM15FF := -0.319675376116 !-0.351898642676
  KLQM14FF := -1.119999994897 !-1.124159191152
  KLQM13FF :=  0.910567434235 ! 0.910688256147
  KLQM12FF :=  0.335904128778 ! 0.339702290193
  KLQM11FF :=  0.0
  KLQD10FF := -0.290019331454
  KLQF9FF  :=  0.378649967733
  KLQD8FF  := -0.604355398609
  KLQF7FF  :=  0.55016183691
  KLQD6FF  := -0.602327211925
  KLQF5FF  :=  0.37605663431
  KLQD4FF  := -0.296792273617
  KLQF3FF  :=  0.552719873686
  KLQD2BFF := -0.198712968919
  KLQD2AFF := -0.289728096191

  QM16FF  : QUAD, L=LQEA/2, APER=RQEA, K1=KLQM16FF/LQEA
  QM15FF  : QUAD, L=LQEA/2, APER=RQEA, K1=KLQM15FF/LQEA
  QM14FF  : QUAD, L=LQEA/2, APER=RQEA, K1=KLQM14FF/LQEA
  QM13FF  : QUAD, L=LQEA/2, APER=RQEA, K1=KLQM13FF/LQEA
  QM12FF  : QUAD, L=LQEA/2, APER=RQEA, K1=KLQM12FF/LQEA
  QM11FF  : QUAD, L=LQEA/2, APER=RQEA, K1=KLQM11FF/LQEA
  QD10BFF : QUAD, L=LQEA/2, APER=RQEA, K1=KLQD10FF/LQEA
  QD10AFF : QUAD, L=LQEA/2, APER=RQEA, K1=KLQD10FF/LQEA
  QF9BFF  : QUAD, L=LQEA/2, APER=RQEA, K1=KLQF9FF /LQEA
  QF9AFF  : QUAD, L=LQEA/2, APER=RQEA, K1=KLQF9FF /LQEA
  QD8FF   : QUAD, L=LQEA/2, APER=RQEA, K1=KLQD8FF /LQEA
  QF7FF   : QUAD, L=LQEA/2, APER=RQEA, K1=KLQF7FF /LQEA
  QD6FF   : QUAD, L=LQEA/2, APER=RQEA, K1=KLQD6FF /LQEA
  QF5BFF  : QUAD, L=LQEA/2, APER=RQEA, K1=KLQF5FF /LQEA
  QF5AFF  : QUAD, L=LQEA/2, APER=RQEA, K1=KLQF5FF /LQEA
  QD4BFF  : QUAD, L=LQEA/2, APER=RQEA, K1=KLQD4FF /LQEA
  QD4AFF  : QUAD, L=LQEA/2, APER=RQEA, K1=KLQD4FF /LQEA
  QF3FF   : QUAD, L=LQEA/2, APER=RQEA, K1=KLQF3FF /LQEA
  QD2BFF  : QUAD, L=LQEA/2, APER=rQEA, K1=KLQD2BFF/LQEA
  QD2AFF  : QUAD, L=LQEA/2, APER=RQEA, K1=KLQD2AFF/LQEA

! final doublet

  KLQF1FF :=  0.741787852156
  KLQD0FF := -1.36396800693

  QF1FF : QUAD, L=LQC3/2, K1=KLQF1FF/LQC3, APER=RQC3
  QD0FF : QUAD, L=LQC3/2, K1=KLQD0FF/LQC3, APER=RQC3

  QF1MULT : MULT,K2L=0.0406/4,T2=1.0816,K3L=2.5592/4,T3=-0.7665,&
            K4L=2.2699e+03/4,T4=0.5981,K5L=3.1654e+06/4,T5=-0.5599,&
            K6L=3.4182e+07/4,T6=-0.0294,K7L=6.7295e+09/4,T7=0.1018,&
            K8L=2.0039e+13/4,T8=0,K9L=7.2948e+16/4,T9=-0.0045,APER=0.025
  QD0MULT : MULT,K2L=-0.0695/4,T2=-0.2541,K3L=-4.2883/4,T3=0.9290,&
            K4L=-2.3471e+03/4,T4=-0.5411,K5L=-5.8973e+06/4,T5=0.5337,&
            K6L=-5.4995e+07/4,T6=-0.1261,K7L=-2.0623e+09/4,T7=0.4210,&
            K8L=-2.0348e+13/4,T8=0.0124,K9L=-1.3413e+17/4,T9=0.001,APER=0.025

! ==============================================================================
! sextupoles
! ------------------------------------------------------------------------------

! FF sextupoles

  KLSF6FF := 8.82 !8.564561598566 ! 8.564403447578
  KLSF5FF := -0.614 !-0.790868336382 !-0.790062859253
  KLSD4FF := 14.954 !14.909987129435 !14.903435337138

  SF6FF : SEXT, L=LSFF/2, APER=RSFF, K2=KLSF6FF/LSFF, TILT=0.00806
  SF5FF : SEXT, L=LSFF/2, APER=RSFF, K2=KLSF5FF/LSFF, TILT=-0.0698
  SD4FF : SEXT, L=LSFF/2, APER=RSFF, K2=KLSD4FF/LSFF, TILT=8.727E-4

! final doublet sextupoles

  KLSF1FF := -2.544 !-2.578002823698 !-2.577547317457
  KLSD0FF := 4.294 !4.311860665982 ! 4.309837672996

  SF1FF : SEXT, L=LSFD/2, APER=RSFD, K2=KLSF1FF/LSFD, TILT=0.00525
  SD0FF : SEXT, L=LSFD/2, APER=RSFD, K2=KLSD0FF/LSFD, TILT=6.632E-4

  SF6MULT : MULT,K3L=3.6434/4,T3=-0.2902,K4L=321.0683/4,T4=-0.4822,&
            K5L=6.4337e+04/4,T5=0.3334,K6L=1.4460e+07/4,T6=-0.5403,&
            K7L=2.2230e+09/4,T7=-0.1173,K8L=3.2978e+13/4,T8=-0.3496,&
            K9L=6.9928e+14/4,T9=0.3983,APER=RSFF
  SF5MULT : MULT,K3L=-0.3777/4,T3=0.5730,K4L=-4.3172/4,T4=0.2557,&
            K5L=-2.7000e+03/4,T5=0.6802,K6L=-1.3296e+06/4,T6=-0.2487,&
            K7L=-3.0891e+08/4,T7=-0.1782,K8L=-3.0501e+12/4,T8=-0.0038,&
            K9L=-3.8744e+13/4,T9=-0.0082,APER=RSFF
  SD4MULT : MULT,K3L=6.1472/4,T3=-1.0676,K4L=136.8737/4,T4=0.0485,&
            K5L=4.5625e+04/4,T5=-0.0571,K6L=6.8705e+06/4,T6=0.2747,&
            K7L=6.1996e+09/4,T7=-0.0859,K8L=5.5308e+13/4,T8=-0.0019,&
            K9L=1.2715e+15/4,T9=-0.1854,APER=RSFF
  SF1MULT : MULT,K3L=-0.7425/4,T3=-2.2532,K4L=-256.7691/4,T4=-2.9130,&
            K5L=-2.6296e+04/4,T5=-2.8693,K6L=-1.2993e+07/4,T6=-1.0796,&
            K7L=-6.4966e+09/4,T7=2.7759,K8L=-2.4427e+13/4,T8=1.6546,&
            K9L=-4.6775e+13/4,T9=-0.2601,APER=RSFF
  SD0MULT : MULT,K3L=1.2289/4,T3=-0.9547,K4L=315.6282/4,T4=-1.3491,&
            K5L=2.3284e+04/4,T5=2.2864,K6L=6.2091e+06/4,T6=3.3800,&
            K7L=1.1952e+10/4,T7=0.6449,K8L=3.9986e+13/4,T8=-0.8535,&
            K9L=3.1294e+14/4,T9=-1.2863,APER=RSFF

! ==============================================================================
! drifts
! ------------------------------------------------------------------------------

  LL200 := LQQBPM-dLQEA
  LL201 := 1.30151
  LL202 := 1.30151
  LL203 := 1.30151
  LL204 := 1.30151
  LL205 := 1.40151
  LL206 := 1.30151
  LL207 := 0.70151
  LL208 := 1.10151
  LL209 := 0.410715984
  LL210 := 0.410715984
  LL211 := 1.60151
  LL212 := 1.70151
  LL213 := 0.444364710161
  LL214 := 0.644364710161
  LL215 := 1.60151
  LL216 := 0.410715984
  LL217 := 0.410715984
  LL218 := 1.10151
  LL219 := 0.410715984
  LL220 := 0.410715984
  LL221 := 1.544384870992
  LL222 := 0.444384870992
  LL223 := 0.444359535201
  LL224 := 0.444359535201
  LL225 := 1.50151
  LL226 := 4.725755
  LL227 := 0.2875
  LL228 := 0.5025
  LL229 := 0.2875
  LL230 := 0.9875

  L200 : DRIF, L=LL200
  L201 : DRIF, L=LL201
  L202 : DRIF, L=LL202
  L203 : DRIF, L=LL203
  L204 : DRIF, L=LL204
  L205 : DRIF, L=LL205
  L206 : DRIF, L=LL206
  L207 : DRIF, L=LL207
  L208 : DRIF, L=LL208
  L209 : DRIF, L=LL209
  L210 : DRIF, L=LL210
  L211 : DRIF, L=LL211
  L212 : DRIF, L=LL212
  L213 : DRIF, L=LL213
  L214 : DRIF, L=LL214
  L215 : DRIF, L=LL215
  L216 : DRIF, L=LL216
  L217 : DRIF, L=LL217
  L218 : DRIF, L=LL218
  L219 : DRIF, L=LL219
  L220 : DRIF, L=LL220
  L221 : DRIF, L=LL221
  L222 : DRIF, L=LL222
  L223 : DRIF, L=LL223
  L224 : DRIF, L=LL224
  L225 : DRIF, L=LL225
  L226 : DRIF, L=LL226
  L227 : DRIF, L=LL227
  L228 : DRIF, L=LL228
  L229 : DRIF, L=LL229
  L230 : DRIF, L=LL230

! post-IP

  LL301 := 1.5 !IP to entrance face of dump bend
  LL302 := 2.0 !exit face of dump bend to entrance face of dump
  LDUMP := 2.3 !full length of dump

  L301  : DRIF, L=LL301
  L302  : DRIF, L=LL302
  DUMPh : DRIF, L=LDUMP/2

! drifts for diagnostic and correction devices

  L201b : DRIF, L=LZZ-(dLCNKKH+dLCNKKV)
  L201c : DRIF, L=LQZ-(dLCNKKV+dLQEA)
  L201a : DRIF, L=L201[L]-(LCNKKH+L201b[L]+LCNKKV+L201c[L])
  L202a : DRIF, L=LQQBPM-dLQEA
  L202b : DRIF, L=L202[L]-L202a[L]
  L203a : DRIF, L=LQQBPM-dLQEA
  L203c : DRIF, L=0.4545-dLQEA
  L203b : DRIF, L=L203[L]-(L203a[L]+L203c[L])
  L204a : DRIF, L=LQQBPM-dLQEA
  L204b : DRIF, L=L204[L]-L204a[L]
  L205a : DRIF, L=LQQBPM-dLQEA
  L205b : DRIF, L=0.6383-LQQBPM
  L205c : DRIF, L=L205[L]-(L205a[L]+L205b[L])
  L206a : DRIF, L=LQQBPM-dLQEA
  L206b : DRIF, L=L206[L]-L206a[L]
  L207a : DRIF, L=LQQBPM-dLQEA
  L207b : DRIF, L=0.27-LQQBPM
  L207c : DRIF, L=L207[L]-(L207a[L]+L207b[L])
  L208a : DRIF, L=LQQBPM-dLQEA
  L208b : DRIF, L=LQW-LQQBPM
  L208c : DRIF, L=L208[L]-(L208a[L]+L208b[L])
  L209a : DRIF, L=LQQBPM-dLQEA
  L209b : DRIF, L=L209[L]-L209a[L]
  L210a : DRIF, L=(ZQEA-ZSFF)/2+LQQBPM-dLSFF
  L210b : DRIF, L=L210[L]-L210a[L]
  L211a : DRIF, L=LQQBPM-dLQEA
  L211b : DRIF, L=L211[L]-L211a[L]
  L212a : DRIF, L=LQQBPM-dLQEA
  L212b : DRIF, L=L212[L]-L212a[L]
  L213a : DRIF, L=LQQBPM-dLQEA
  L213b : DRIF, L=L213[L]-L213a[L]
  L215a : DRIF, L=LQQBPM-dLQEA
  L215b : DRIF, L=L215[L]-L215a[L]
  L216a : DRIF, L=LQQBPM-dLQEA
  L216b : DRIF, L=L216[L]-L216a[L]
  L217a : DRIF, L=(ZQEA-ZSFF)/2+LQQBPM-dLSFF
  L217b : DRIF, L=L217[L]-L217a[L]
  L218a : DRIF, L=LQQBPM-dLQEA
  L218c : DRIF, L=0.23-dLQEA
  L218b : DRIF, L=L218[L]-(L218a[L]+L218c[L])
  L219a : DRIF, L=LQQBPM-dLQEA
  L219b : DRIF, L=L219[L]-L219a[L]
  L220a : DRIF, L=(ZQEA-ZSFF)/2+LQQBPM-dLSFF
  L220b : DRIF, L=L220[L]-L220a[L]
  L221a : DRIF, L=LQQBPM-dLQEA
  L221b : DRIF, L=L221[L]-L221a[L]
  L223a : DRIF, L=LQQBPM-dLQEA
  L223b : DRIF, L=L223[L]-L223a[L]
  L224b : DRIF, L=0.23-dLQEA
  L224a : DRIF, L=L224[L]-L224b[L]
  L225a : DRIF, L=LQQBPM-dLQEA
  L225b : DRIF, L=L225[L]-L225a[L]
  L226a : DRIF, L=LQQBPM-dLQEA
  L226c : DRIF, L=0.37-LSSBPM-LSMODS
  L226d : DRIF, L=LSSBPM-dLSFD+LSMODS
  L226b : DRIF, L=L226[L]-(L226a[L]+L226c[L]+L226d[L])
  L228a : DRIF, L=LQSBPM-dLQC3+LSMODQ
  L228c : DRIF, L=LSSBPM-dLSFD+LSMODS-LSMODQ
  L228b : DRIF, L=L228[L]-(L228a[L]+L228c[L])-LSMODS
  L230a : DRIF, L=LQSBPM-dLQC3+LSMODQ
  L230b : DRIF, L=0.3925-LQSBPM-LSMODQ
  L230c : DRIF, L=0.1825
  L230d : DRIF, L=0.275
  L230e : DRIF, L=L230[L]-(L230a[L]+L230b[L]+L230c[L]+L230d[L])
  L301a : DRIF, L=0.4
  L301b : DRIF, L=0.1675-0.0248 !to short-pipe QBPM "sensor point"
  L301c : DRIF, L=0.2575+0.0248
  L301d : DRIF, L=L301[L]-(L301a[L]+L301b[L]+L301c[L])
  L302a : DRIF, L=0.390
  L302b : DRIF, L=0.180
  L302c : DRIF, L=L302[L]-(L302a[L]+L302b[L])

! ==============================================================================
! diagnostic and correction devices
! ------------------------------------------------------------------------------

! dipole correctors (horizontal)

  ZH1FF : HKIC, L=LCNKKH !pulse-to-pulse feedback

! dipole correctors (vertical)

  ZV1FF : VKIC, L=LCNKKV !pulse-to-pulse feedback

! FFTB x/y/roll magnet movers

  AQM16FF  : MARK
  AQM15FF  : MARK
  AQM14FF  : MARK
  AQM13FF  : MARK
  AQM12FF  : MARK
  AQM11FF  : MARK
  AQD10BFF : MARK
  AQD10AFF : MARK
  AQF9BFF  : MARK
  ASF6FF   : MARK
  AQF9AFF  : MARK
  AQD8FF   : MARK
  AQF7FF   : MARK
  AQD6FF   : MARK
  AQF5BFF  : MARK
  ASF5FF   : MARK
  AQF5AFF  : MARK
  AQD4BFF  : MARK
  ASD4FF   : MARK
  AQD4AFF  : MARK
  AQF3FF   : MARK
  AQD2BFF  : MARK
  AQD2AFF  : MARK
  ASF1FF   : MARK
  AQF1FF   : MARK
  ASD0FF   : MARK
  AQD0FF   : MARK

! special mover for feedback C-band BPM MFB2FF

  AFB2FF : MARK

! horizontal/vertical sweeper (Tokyo University / Mitsubishi)
! (Lcore= 30 mm, Leff= 166.2 mm, Imax= 5 A, dB/dI= 5.49 G/A)

 !HSWEEP : HKIC
 !VSWEEP : VKIC
 !SWEEP  : LINE=(HSWEEP,VSWEEP)
  SWEEP  : MARK

! stripline BPMs

  MFB1FF : MONI !feedback (FD-phase)
  MDUMP  : MONI

! cavity BPMs

  MQM16FF  : MONI !C-band
  MQM15FF  : MONI !C-band
  MQM14FF  : MONI !C-band
  MFB2FF   : MONI !C-band; feedback (IP-phase)
  MQM13FF  : MONI !C-band
  MQM12FF  : MONI !C-band
  MQM11FF  : MONI !C-band
  MQD10BFF : MONI !C-band
  MQD10AFF : MONI !C-band
  MQF9BFF  : MONI !C-band
  MSF6FF   : MONI !C-band
  MQF9AFF  : MONI !C-band
  MQD8FF   : MONI !C-band
  MQF7FF   : MONI !C-band
  MQD6FF   : MONI !C-band
  MQF5BFF  : MONI !C-band
  MSF5FF   : MONI !C-band
  MQF5AFF  : MONI !C-band
  MQD4BFF  : MONI !C-band
  MSD4FF   : MONI !C-band
  MQD4AFF  : MONI !C-band
  MQF3FF   : MONI !C-band
  MQD2BFF  : MONI !C-band
  MQD2AFF  : MONI !C-band
  QBPM24FF : MONI !C-band
  MSF1FF   : MONI !S-band
  MQF1FF   : MONI !S-band
  MSD0FF   : MONI !S-band
  MQD0FF   : MONI !S-band
  M1_2IP : MONI !2-cavity IP BPM
  MPIP     : MONI !C-band (short pipe)

! reference (monopole) cavities (for cavity BPMs)

  MREF3FF : MARK !C-band (for QBPMs)
  MREF2FF : MARK !C-band (for QBPMs)
  MREF1FF : MARK !C-band (for QBPMs)

! screen monitors

  MS1FF : PROF !length ~0.18
  MS2FF : PROF !length ~0.18
  MSPIP : PROF !length ~0.18

! filament wire scanners

  MW1IP : WIRE !length ~0.2 (carbon filament)

! beam size monitors (BSMs)

  MBS2IP : INST !nano pattern (length ~0.18)
  MBS1IP : INST !laser interferometer (length ~0.2)

! beam charge/current monitors

  ICTDUMP : MARK !ICT

! ==============================================================================
! marker points
! ------------------------------------------------------------------------------

  BEGFF  : MARK
  IP     : MARK
  ENDFF  : MARK

! ==============================================================================
! beamlines
! ------------------------------------------------------------------------------
! FFs   : simple Final Focus
! FFRs  : simple Final Focus (reversed)
! FF    : complete Final Focus
! POSTs : simple post-IP line
! POST  : complete post-IP line
! ------------------------------------------------------------------------------

  FFs : LINE=(BEGFF,L200,&
    QM16FF,QM16FF,L201,&
    QM15FF,QM15FF,L202,&
    QM14FF,QM14FF,L203,&
    QM13FF,QM13FF,L204,&
    QM12FF,QM12FF,L205,&
    QM11FF,QM11FF,L206,&
    QD10BFF,QD10BFF,L207,&
    QD10AFF,QD10AFF,L208,&
    QF9BFF,QF9BFF,L209,&
    SF6FF,SF6FF,L210,&
    QF9AFF,QF9AFF,L211,&
    QD8FF,QD8FF,L212,&
    QF7FF,QF7FF,L213,&
    B5FFa,B5FFb,L214,&
    QD6FF,QD6FF,L215,&
    QF5BFF,QF5BFF,L216,&
    SF5FF,SF5FF,L217,&
    QF5AFF,QF5AFF,L218,&
    QD4BFF,QD4BFF,L219,&
    SD4FF,SD4FF,L220,&
    QD4AFF,QD4AFF,L221,&
    B2FFa,B2FFb,L222,&
    QF3FF,QF3FF,L223,&
    B1FFa,B1FFb,L224,&
    QD2BFF,QD2BFF,L225,&
    QD2AFF,QD2AFF,L226,&
    SF1FF,SF1FF,L227,&
    QF1FF,QF1FF,L228,&
    SD0FF,SD0FF,L229,&
    QD0FF,QD0FF,L230,&
    IP,ENDFF)

  FFRs : LINE=(ENDFF,IP,&
    L230,QD0FF,QD0FF,&
    L229,SD0FF,SD0FF,&
    L228,QF1FF,QF1FF,&
    L227,SF1FF,SF1FF,&
    L226,QD2AFF,QD2AFF,&
    L225,QD2BFF,QD2BFF,&
    L224,B1FFa,B1FFb,&
    L223,QF3FF,QF3FF,&
    L222,B2FFa,B2FFb,&
    L221,QD4AFF,QD4AFF,&
    L220,SD4FF,SD4FF,&
    L219,QD4BFF,QD4BFF,&
    L218,QF5AFF,QF5AFF,&
    L217,SF5FF,SF5FF,&
    L216,QF5BFF,QF5BFF,&
    L215,QD6FF,QD6FF,&
    L214,B5FFa,B5FFb,&
    L213,QF7FF,QF7FF,&
    L212,QD8FF,QD8FF,&
    L211,QF9AFF,QF9AFF,&
    L210,SF6FF,SF6FF,&
    L209,QF9BFF,QF9BFF,&
    L208,QD10AFF,QD10AFF,&
    L207,QD10BFF,QD10BFF,&
    L206,QM11FF,QM11FF,&
    L205,QM12FF,QM12FF,&
    L204,QM13FF,QM13FF,&
    L203,QM14FF,QM14FF,&
    L202,QM15FF,QM15FF,&
    L201,QM16FF,QM16FF,&
    L200,BEGFF)

  POSTs : LINE=(L301,BDUMPa,BDUMPb,L302,DUMPh,DUMPh)

  BL200 : LINE=(MQM16FF,L200)
  BL201 : LINE=(L201a,ZH1FF,L201b,ZV1FF,L201c)
  BL202 : LINE=(L202a,MQM15FF,L202b)
  BL203 : LINE=(L203a,MQM14FF,L203b,MFB2FF,AFB2FF,L203c)
  BL204 : LINE=(L204a,MQM13FF,L204b)
  BL205 : LINE=(L205a,MQM12FF,L205b,MFB1FF,L205c)
  BL206 : LINE=(L206a,MQM11FF,L206b)
  BL207 : LINE=(L207a,MQD10BFF,L207b,MREF3FF,L207c)
  BL208 : LINE=(L208a,MQD10AFF,L208b,MS1FF,L208c)
  BL209 : LINE=(L209a,MQF9BFF,L209b)
  BL210 : LINE=(L210a,MSF6FF,L210b)
  BL211 : LINE=(L211a,MQF9AFF,L211b)
  BL212 : LINE=(L212a,MQD8FF,L212b)
  BL213 : LINE=(L213a,MQF7FF,L213b)
  BL215 : LINE=(L215a,MQD6FF,L215b)
  BL216 : LINE=(L216a,MQF5BFF,L216b)
  BL217 : LINE=(L217a,MSF5FF,L217b)
  BL218 : LINE=(L218a,MQF5AFF,L218b,MREF2FF,L218c)
  BL219 : LINE=(L219a,MQD4BFF,L219b)
  BL220 : LINE=(L220a,MSD4FF,L220b)
  BL221 : LINE=(L221a,MQD4AFF,L221b)
  BL223 : LINE=(L223a,MQF3FF,L223b)
  BL224 : LINE=(L224a,MREF1FF,L224b)
  BL225 : LINE=(L225a,MQD2BFF,L225b)
  BL226 : LINE=(L226a,MQD2AFF,L226b,MS2FF,L226c,MSF1FF,&
                L226d)
  BL228 : LINE=(L228a,MQF1FF,L228b,MSD0FF,&
                L228c)
  BL230 : LINE=(L230a,MQD0FF,L230b,SWEEP,&
                L230c,MBS2IP,L230d,M1_2IP,&
                L230e,MBS1IP)

  FF : LINE=(BEGFF,BL200,&
    QM16FF,AQM16FF,QM16FF,BL201,&
    QM15FF,AQM15FF,QM15FF,BL202,&
    QM14FF,AQM14FF,QM14FF,BL203,&
    QM13FF,AQM13FF,QM13FF,BL204,&
    QM12FF,AQM12FF,QM12FF,BL205,&
    QM11FF,AQM11FF,QM11FF,BL206,&
    QD10BFF,AQD10BFF,QD10BFF,BL207,&
    QD10AFF,AQD10AFF,QD10AFF,BL208,&
    QF9BFF,AQF9BFF,QF9BFF,BL209,&
    SF6MULT,SF6FF,SF6MULT,ASF6FF,SF6MULT,SF6FF,SF6MULT,BL210,&
    QF9AFF,AQF9AFF,QF9AFF,BL211,&
    QD8FF,AQD8FF,QD8FF,BL212,&
    QF7FF,AQF7FF,QF7FF,BL213,&
    B5MULT,B5FFa,B5MULT,B5MULT,B5FFb,B5MULT,L214,&
    QD6FF,AQD6FF,QD6FF,BL215,&
    QF5BFF,AQF5BFF,QF5BFF,BL216,&
    SF5MULT,SF5FF,SF5MULT,ASF5FF,SF5MULT,SF5FF,SF5MULT,BL217,&
    QF5AFF,AQF5AFF,QF5AFF,BL218,&
    QD4BFF,AQD4BFF,QD4BFF,BL219,&
    SD4MULT,SD4FF,SD4MULT,ASD4FF,SD4MULT,SD4FF,SD4MULT,BL220,&
    QD4AFF,AQD4AFF,QD4AFF,BL221,&
    B2MULT,B2FFa,B2MULT,B2MULT,B2FFb,B2MULT,L222,&
    QF3FF,AQF3FF,QF3FF,BL223,&
    B1MULT,B1FFa,B1MULT,B1MULT,B1FFb,B1MULT,BL224,&
    QD2BFF,AQD2BFF,QD2BFF,BL225,&
    QD2AFF,AQD2AFF,QD2AFF,BL226,&
    SF1MULT,SF1FF,SF1MULT,ASF1FF,SF1MULT,SF1FF,SF1MULT,L227,&
    QF1MULT,QF1FF,QF1MULT,AQF1FF,QF1MULT,QF1FF,QF1MULT,BL228,&
    SD0MULT,SD0FF,SD0MULT,ASD0FF,SD0MULT,SD0FF,SD0MULT,L229,&
    QD0MULT,QD0FF,QD0MULT,AQD0FF,QD0MULT,QD0FF,QD0MULT,BL230,&
    IP,ENDFF)

  BL301 : LINE=(L301a,MW1IP,L301b,MPIP,L301c,MSPIP,L301d)
  BL302 : LINE=(L302a,MDUMP,L302b,ICTDUMP,L302c)

  POST : LINE=(BL301,BDUMPa,BDUMPb,BL302,DUMPh,DUMPh)

! ==============================================================================

  RETURN

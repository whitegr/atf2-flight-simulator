function [Iquad,GL]=get_ATF2_quads_I(KL,energy,fflag)
%
% [Iquad,GL]=get_ATF2_quads_I(KL,energy,fflag);
%
% Compute ATF2 quad currents from KLs and beam energy
%
% INPUTS:
%
%   KL     = quad KLs (1/m; positive means "QF") [51 element array]
%   energy = beam energy (GeV)
%   fflag  = (optional) if present and zero, fudge factors will not be used
%
%   The order of quad KLs must be:
%
%   QM6R   ,QM7R   ,QS1X   ,QF1X   ,QD2X   ,QF3X   ,QF4X   ,QD5X   ,QF6X   ,
%   QS2X   ,QF7X   ,QD8X   ,QF9X   ,QK1X   ,QD10X  ,QF11X  ,QK2X   ,QD12X  ,
%   QF13X  ,QD14X  ,QF15X  ,QK3X   ,QD16X  ,QF17X  ,QK4X   ,QD18X  ,QF19X  ,
%   QD20X  ,QF21X  ,QM16FF ,QM15FF ,QM14FF ,QM13FF ,QM12FF ,QM11FF ,QD10BFF,
%   QD10AFF,QF9BFF ,QF9AFF ,QD8FF  ,QF7FF  ,QD6FF  ,QF5BFF ,QF5AFF ,QD4BFF ,
%   QD4AFF ,QF3FF  ,QD2BFF ,QD2AFF ,QF1FF  ,QD0FF
%
% OUTPUTS:
%
%   Iquad = ATF2 quad currents (amps)
%   GL    = integrated quad strengths (kG)

% check input args, if provided

NATF2q=51;
if (nargin<2)
  error('At least 2 input arguments required')
end
if (length(KL)~=NATF2q)
  error('Incorrect KL length')
end
if (energy<=0)
  error('Bad energy value')
end
if (nargin==2)
  fudge=1;
else
  fudge=(fflag~=0);
end

% ==============================================================================
%
% ATF2 QUAD MAGNET DATA
%
% ==============================================================================

% there are 8 types of ATF2 quad:

%   1 = Hitachi type 4
%   2 = Tokin 3393
%   3 = Hitachi type 5
%   4 = Hitachi type 2
%   5 = IDX skew
%   6 = QEA-D32T180
%   7 = QC3 (shimmed) [QF1FF]
%   8 = QC3 (shimmed) [QD0FF]

% MMS data for quad types (I is amp, G is T/m)
% (from: ATF$MAG:MAG_KI_Q_HITACHI_4.FOR
%        ATF$MAG:MAG_KI_Q_TOKIN_3393.FOR
%        ATF$MAG:MAG_KI_Q_HITACHI_5.FOR
%        ATF$MAG:MAG_KI_Q_HITACHI_2.FOR
%        ATF$MAG:MAG_KI_Q_IDX_SKEW.FOR
%        ATF$MAG:MAG_KI_Q_D32T180.FOR
%        SLAC magnetic measurements [QF1FF]
%        SLAC magnetic measurements [QD0FF])

Nmms=[11,8,11,11,6,15,13,13];

qI=[ ...
    0.0,  0.0,  0.0,  0.0,  0.0,  0.0,  0.0      ,  0.0      ; ...
   20.2, 20.0, 10.4, 10.2,  4.0,  5.0, 50.0170753, 50.0106160; ...
   40.2, 40.0, 20.2, 20.2,  8.0,  8.0, 60.0253628, 60.0276535; ...
   60.4, 60.0, 30.4, 30.2, 12.0, 10.0, 70.0958753, 70.0855160; ...
   80.4, 80.0, 40.2, 40.0, 16.0, 12.0, 80.0715627, 80.0717910; ...
  100.0,100.0, 50.0, 50.0, 20.0, 14.0, 90.1618378, 90.1688160; ...
  120.2,120.0, 60.2, 60.2,  0  , 16.0,100.0284003,100.0348160; ...
  140.2,139.0, 70.2, 70.2,  0  , 20.0,110.0944627,110.0881035; ...
  160.2,  0  , 80.4, 80.2,  0  , 25.0,120.0628878,120.0671535; ...
  180.0,  0  , 90.4, 90.2,  0  , 30.0,130.1262003,130.1366660; ...
  200.4,  0  ,100.6,100.2,  0  , 35.0,140.1526753,140.1541410; ...
    0  ,  0  ,  0  ,  0  ,  0  , 40.0,145.2119002,145.2155410; ...
    0  ,  0  ,  0  ,  0  ,  0  , 45.0,150.2062628,150.2204785; ...
    0  ,  0  ,  0  ,  0  ,  0  , 50.0,  0        ,  0        ; ...
    0  ,  0  ,  0  ,  0  ,  0  ,150.0,  0        ,  0        ; ...
];

% NOTE: data for QEA-D32T180 and QC3s are integrated gradients (T)

qG=[ ...
   0.000, 0.0, 0.000, 0.000,0.000, 0.0     ,0.0      ,0.0      ; ...
   4.612, 3.4, 4.845, 3.832,1.115, 0.469959,2.2467284,2.2493081; ...
   9.201, 6.8, 9.424, 7.646,2.247, 0.748402,2.6961437,2.6988262; ...
  13.858,10.0,14.248,11.498,3.385, 0.932288,3.1478458,3.1507880; ...
  18.440,13.2,18.833,15.202,4.522, 1.119242,3.5957252,3.5993780; ...
  22.884,16.6,23.382,18.927,5.662, 1.304396,4.0480109,4.0525394; ...
  27.464,19.8,27.994,22.657,0    , 1.490985,4.4894084,4.4949355; ...
  31.925,23.0,32.566,26.285,0    , 1.864267,4.9388469,4.9453874; ...
  36.386, 0  ,37.206,29.896,0    , 2.330425,5.3839400,5.3913758; ...
  40.726, 0  ,41.726,33.438,0    , 2.795264,5.8322381,5.8405937; ...
  45.143, 0  ,46.223,36.758,0    , 3.253318,6.2782832,6.2871601; ...
   0    , 0  , 0    , 0    ,0    , 3.711283,6.5027131,6.5118793; ...
   0    , 0  , 0    , 0    ,0    , 4.168370,6.7255943,6.7351056; ...
   0    , 0  , 0    , 0    ,0    , 4.624610,0        ,0        ; ...
   0    , 0  , 0    , 0    ,0    ,13.890249,0        ,0        ; ...
];

qleff=[0.198745,0.07890677,0.19861,0.07867,0.07867,0.19849,0.475,0.475];

% NOTE: convert integrated strengths to gradients for standard handling

qG(:,6)=qG(:,6)/qleff(6); % QEA
qG(:,7)=qG(:,7)/qleff(7); % QC3 [QF1FF]
qG(:,8)=qG(:,8)/qleff(8); % QC3 [QD0FF]

% quadrupole family types

qtype=[ ...
   1; 2; 5; 3; 3; 3; 3; 3; 3; 5; ...
   4; 3; 3; 5; 6; 6; 5; 6; 1; 1; ...
   1; 5; 6; 6; 5; 6; 6; 4; 4; 6; ...
   6; 6; 6; 6; 6; 6; 6; 6; 6; 6; ...
   6; 6; 6; 6; 6; 6; 6; 6; 6; 7; ...
   8; ...
];

% polarities (zero means bipolar)

qsgn=[...
  -1; 1; 0; 1;-1; 1; 1;-1; ...
   1; 0; 1;-1; 1; 0;-1; 1; ...
   0;-1; 1;-1; 1; 0;-1; 1; ...
   0;-1; 1;-1; 1; 1;-1;-1; ...
   1; 1;-1;-1;-1; 1; 1;-1; ...
   1;-1; 1; 1;-1;-1; 1;-1; ...
  -1; 1;-1; ...
];

% "Kubo" fudge factors
% (from: ATF$SAD_DATA:DR_QUAD_CORRECTION.DAT;2 (21-OCT-2003 11:10:12.08))

qfudge=zeros(NATF2q,1);
qfudge(1)=-0.0103299;  % QM6R
qfudge(2)=-0.0193019;  % QM7R

% ==============================================================================

% compute rigidity

Cb=1e10/2.99792458e8; % kG-m/GeV
brho=Cb*energy;

% compute quad currents
% (NOTE: polynomial evaluation is not presently used to convert current
%        to gradient ... linear interpolation of MMS data is used)

GL=zeros(NATF2q,1);
Iquad=zeros(NATF2q,1);
for n=1:NATF2q
  if (KL(n)==0)
    GL(n)=0;
    Iquad(n)=0;
  else
    GL(n)=brho*KL(n); % kG
    nt=qtype(n);
    bipolar=(qsgn(n)==0);
    G=abs(GL(n)/qleff(nt)); % kG/m
    G=0.1*G; % kG -> T/m
    if (fudge),G=G*(1+qfudge(n));end % apply the fudge factor
    Iquad(n)=interp1(qG(1:Nmms(nt),nt),qI(1:Nmms(nt),nt),G,'linear');
    if (bipolar)
      Iquad(n)=sign(KL(n))*Iquad(n);
    else
      Iquad(n)=sign(KL(n))*qsgn(n)*Iquad(n);
    end
  end
end


global BEAMLINE PS GIRDER MDATA FL INSTR %#ok<NUSED>


Model.opticsVersion='5.2'; % ATF2 optics version
master='ATF2_master.xsif';
switch 3
  case 0 % BetaX*/BetaY*=1cm/0.1mm (all MULTIPOLEs OFF)
    Model.opticsName='BX2.5BY1';
  case 1 % BetaX*/BetaY*=1cm/0.1mm (KEX MULTs OFF ... nominal EXT; all other MULTs ON; MAPCLASS optimized)
    Model.opticsName='BX2.5BY1_opt';
  case 2 % BetaX*/BetaY*=1cm/0.1mm (all MULTIPOLEs ON; EXT rematched; MAPCLASS optimized)
    Model.opticsName='BX2.5BY1k_opt';
  case 3
    Model.opticsName='BX10BY1nl';
end

fprintf(1,'Building ATF2 optics "%s" (v%s) ......\n', ...
  Model.opticsName,Model.opticsVersion);
[stat,Model.Initial]=XSIFToLucretia(master,'ATF_full','ATFTWISS','ATFBEAM');
if (stat{1}~=1),error(stat{2}),end
Nelem=length(BEAMLINE);

E0=1.282; %GeV
for iele=findcells(BEAMLINE,'B')
  BEAMLINE{iele}.B=BEAMLINE{iele}.B.*(E0/BEAMLINE{iele}.P);
  BEAMLINE{iele}.P=E0;
end
Model.Initial.Momentum=E0;

% add some IMON INSTs
Iname={'ICT1X','ICTDUMP','IPBSM-BKG'};
for n=1:length(Iname)
  imon=InstStruc(0,'IMON',Iname{n});
  id=findcells(BEAMLINE,'Name',Iname{n});
  if isempty(id)
    id=Nelem;
    ide=Nelem+1;
  else
    ide=id;
  end
  imon.S=BEAMLINE{id}.S;
  imon.P=BEAMLINE{id}.P;
  imon.TrackFlag.GetBPMData=1;
  imon.TrackFlag.GetBPMBeamPars=1;
  imon.TrackFlag.GetInstData=1;
  BEAMLINE{ide}=imon;
end

% some constants

emass=0.51099906e-3; % electron rest mass (GeV)
eQ=1.602176462e-19;  % electron charge (C)
clight=2.99792458e8; % speed of light (m/sec)
Cb=1e9/clight;       % rigidity conversion (T-m/GeV)

% set some initial parameters that didn't come from the XSIF file

esprd=0.8e-3;   % nominal (uncorrelated) energy spread (1)
bleng=8e-3;     % nominal bunch length (m)
nbunch=1e10;    % nominal number of electrons per bunch

Model.Initial.SigPUncorrel=Model.Initial.Momentum*esprd;
Model.Initial.sigz=bleng;
Model.Initial.Q=nbunch*eQ;
Model.Initial.BunchInterval=337e-9; % sec (where did this come from?)

% set up the energy profile

stat=SetDesignMomentumProfile(1,Nelem, ...
  Model.Initial.Q,Model.Initial.Momentum,Model.Initial.Momentum);
if (stat{1}~=1),error(stat{2}),end

% generate the Twiss parameters for the entire line

[stat,Model.Twiss]=GetTwiss(1,Nelem, ...
  Model.Initial.x.Twiss,Model.Initial.y.Twiss);
if (stat{1}~=1),error(stat{2}),end

% track indicies and points of interest

Model.startTrack=1;
Model.extStart=findcells(BEAMLINE,'Name','IEX');
Model.ffStart.ind=findcells(BEAMLINE,'Name','BEGFF');
Model.ip_ind=findcells(BEAMLINE,'Name','IP');

% save Twiss at start of EXT and start of FF
Model.Initial_IEX=Model.Initial;
Model.Initial_IEX.x.Twiss.beta=Model.Twiss.betax(Model.extStart);
Model.Initial_IEX.x.Twiss.alpha=Model.Twiss.alphax(Model.extStart);
Model.Initial_IEX.x.Twiss.eta=Model.Twiss.etax(Model.extStart);
Model.Initial_IEX.x.Twiss.etap=Model.Twiss.etapx(Model.extStart);
Model.Initial_IEX.x.Twiss.nu=Model.Twiss.nux(Model.extStart);
Model.Initial_IEX.y.Twiss.beta=Model.Twiss.betay(Model.extStart);
Model.Initial_IEX.y.Twiss.alpha=Model.Twiss.alphay(Model.extStart);
Model.Initial_IEX.y.Twiss.eta=Model.Twiss.etay(Model.extStart);
Model.Initial_IEX.y.Twiss.etap=Model.Twiss.etapy(Model.extStart);
Model.Initial_IEX.y.Twiss.nu=Model.Twiss.nuy(Model.extStart);

Model.Initial_BEGFF=Model.Initial;
Model.Initial_BEGFF.x.Twiss.beta=Model.Twiss.betax(Model.ffStart.ind);
Model.Initial_BEGFF.x.Twiss.alpha=Model.Twiss.alphax(Model.ffStart.ind);
Model.Initial_BEGFF.x.Twiss.eta=Model.Twiss.etax(Model.ffStart.ind);
Model.Initial_BEGFF.x.Twiss.etap=Model.Twiss.etapx(Model.ffStart.ind);
Model.Initial_BEGFF.x.Twiss.nu=Model.Twiss.nux(Model.ffStart.ind);
Model.Initial_BEGFF.y.Twiss.beta=Model.Twiss.betay(Model.ffStart.ind);
Model.Initial_BEGFF.y.Twiss.alpha=Model.Twiss.alphay(Model.ffStart.ind);
Model.Initial_BEGFF.y.Twiss.eta=Model.Twiss.etay(Model.ffStart.ind);
Model.Initial_BEGFF.y.Twiss.etap=Model.Twiss.etapy(Model.ffStart.ind);
Model.Initial_BEGFF.y.Twiss.nu=Model.Twiss.nuy(Model.ffStart.ind);

% simulation parameters

Model.errSig=3; % truncate gaussian errors to this sigma level
Model.align.bpm_cres=100e-9; % resolution of main BPMs
Model.align.bpm_sres=10e-6;
Model.align.bpm_ipres=2e-9;
Model.align.bpm_bres=10e-6;
Model.ipLaserRes=[2e-9,2e-9]; % IP SM resolution
Model.bpm.DeltaB_qalign=0.8; % fractional quadrupole k change for alignment
Model.align.magbpm_x=30e-6; % BPM fiducial alignment to magnet center
Model.align.magbpm_y=30e-6;
Model.align.rot=300e-6;
Model.align.x=200e-6;
Model.align.y=200e-6;
Model.align.z=200e-6;
Model.align.x_bend=200e-6;
Model.align.y_bend=200e-6;
Model.align.rot_bend=300e-6; % set magnet rotation alignment tolerance for bend magnets (rms, m)
Model.align.dB=1e-4;
Model.align.dB_sext=1e-4;
Model.align.dB_syst=1e-4;
Model.align.dPSAmpl=0;
Model.align.ps_step=0;
Model.align.cor_psStep=1e-6 ; % radians
Model.align.cor_dAmpl=0;
Model.align.mag_dAmpl=0;
Model.align.mover_speed=[30e-6,30e-6,15e-6]; % meters/rad per second
Model.align.mover_step=[300e-9,300e-9,600e-9]; % mover step size
Model.align.mover_accuracy_x=1e-6; % accuracy of position and tilt positioning (m,rad)
Model.align.mover_accuracy_y=1e-6;
Model.align.mover_accuracy_tilt=10e-6;

% random jitter on incoming beam

Model.align.incoming_x=100e-9;
Model.align.incoming_xp=100e-9;
Model.align.incoming_y=100e-9;
Model.align.incoming_yp=100e-9;
Model.align.incoming_e=1e-4; % percent

% error on energy changes

Model.dE_set_error=0;

% ground motion model and component jitter

Model.align.gmModel='None'; 
Model.align.jitter_quad=25e-9;
Model.align.jitter_sext=25e-9;

% dynamic magnetic field errors

Model.align.dB_quad_dyn=0;
Model.align.dB_sext_dyn=0;
Model.align.dB_bend_dyn=0;
Model.align.dB_cor_dyn=1e-4;

% BPM-mag alignment for EXT

Model.ext.align=Model.align;
Model.ext.align.magbpm_x=10e-6;

% get handles
% ignore SK1FF for grouping purposes
sk1=findcells(BEAMLINE,'Name','SK1FF');
if ~isempty(sk1)
  for isk=1:length(sk1)
    BEAMLINE{sk1(isk)}.Class='MULT';
  end
end
Model.handles.BPM=findcells(BEAMLINE,'Class','MONI');
Model.handles.Quad=findcells(BEAMLINE,'Class','QUAD');
Model.handles.Sext=findcells(BEAMLINE,'Class','SEXT');
Model.handles.Oct=findcells(BEAMLINE,'Class','OCTU');
Model.handles.SQuad=[ ...
  findcells(BEAMLINE,'Name','QS*X'), ... % EXT dispersion correction
  findcells(BEAMLINE,'Name','QK*X')];    % EXT coupling correction
  
% assign element slices and blocks

[stat,Model.slices]=SetElementSlices(1,Nelem);
[stat,Model.blocks]=SetElementBlocks(1,Nelem);

% assign element group
[stat,Model.magGroup.BPM.Off]=MakeErrorGroup( ...
  {'BEAMLINE';'MONI'},[Model.extStart,Nelem],'Offset',1,'BPM Offset');
[stat,Model.magGroup.Quad.Off]=MakeErrorGroup( ...
  {'BEAMLINE';'QUAD'},[Model.extStart,Nelem],'Offset',2,'Quads and SQuads index');
[stat,Model.magGroup.Quad.dB]=MakeErrorGroup( ...
  {'BEAMLINE';'QUAD'},[Model.extStart,Nelem],'dB',1,'Quads and SQuads dB');
[stat,Model.magGroup.Quad.OffUse]=MakeErrorGroup( ...
  {'BEAMLINE';'QUAD'},[Model.extStart,Nelem],'Offset',1,'Quads and SQuads Offset');
[stat,Model.magGroup.Sext.Off]=MakeErrorGroup( ...
  {'BEAMLINE';'SEXT'},[Model.extStart,Nelem],'Offset',1,'Sextupoles Offset');
[stat,Model.magGroup.Mult.Off]=MakeErrorGroup( ...
  {'BEAMLINE';'MULT'},[Model.extStart,Nelem],'Offset',1,'Mult Offset');
[stat,Model.magGroup.SBEN.Off]=MakeErrorGroup( ...
  {'BEAMLINE';'SBEN'},[Model.extStart,Nelem],'Offset',1,'SBEN Offset');
[stat,Model.magGroup.Sext.dB]=MakeErrorGroup( ...
  {'BEAMLINE';'SEXT'},[Model.extStart,Nelem],'dB',1,'Sextupoles dB');
[stat,Model.magGroup.Mult.dB]=MakeErrorGroup( ...
  {'BEAMLINE';'MULT'},[Model.extStart,Nelem],'dB',1,'Mult dB');
[stat,Model.magGroup.SBEN.dB]=MakeErrorGroup( ...
  {'BEAMLINE';'SBEN'},[Model.extStart,Nelem],'dB',1,'SBEN dB');
[stat,Model.magGroup.XCOR.dB]=MakeErrorGroup( ...
  {'BEAMLINE';'XCOR'},[Model.extStart,Nelem],'dB',1,'XCOR dB');
[stat,Model.magGroup.YCOR.dB]=MakeErrorGroup( ...
  {'BEAMLINE';'YCOR'},[Model.extStart,Nelem],'dB',1,'YCOR dB');

% assign FF magnet groups

[stat,Model.ffmagGroup.BPM.Off]=MakeErrorGroup( ...
  {'BEAMLINE';'MONI'},[Model.ffStart.ind Nelem],'Offset',1,'ff BPM Offset');
[stat,Model.ffmagGroup.Quad.Off]=MakeErrorGroup( ...
  {'BEAMLINE';'QUAD'},[Model.ffStart.ind Nelem],'Offset',2,'ff Quads and SQuads index');
[stat,Model.ffmagGroup.Quad.dB]=MakeErrorGroup( ...
  {'BEAMLINE';'QUAD'},[Model.ffStart.ind Nelem],'dB',1,'ff Quads and SQuads dB');
[stat,Model.ffmagGroup.Quad.OffUse]=MakeErrorGroup( ...
  {'BEAMLINE';'QUAD'},[Model.ffStart.ind Nelem],'Offset',1,'ff Quads and SQuads Offset');
[stat,Model.ffmagGroup.Sext.Off]=MakeErrorGroup( ...
  {'BEAMLINE';'SEXT'},[Model.ffStart.ind Nelem],'Offset',1,'ff Sextupoles Offset');
[stat,Model.ffmagGroup.Mult.Off]=MakeErrorGroup( ...
  {'BEAMLINE';'MULT'},[Model.ffStart.ind Nelem],'Offset',1,'ff Mult Offset');
[stat,Model.ffmagGroup.SBEN.Off]=MakeErrorGroup( ...
  {'BEAMLINE';'SBEN'},[Model.ffStart.ind Nelem],'Offset',1,'ff SBEN Offset');
[stat,Model.ffmagGroup.Sext.dB]=MakeErrorGroup( ...
  {'BEAMLINE';'SEXT'},[Model.ffStart.ind Nelem],'dB',1,'ff Sextupoles dB');
[stat,Model.ffmagGroup.Mult.dB]=MakeErrorGroup( ...
  {'BEAMLINE';'MULT'},[Model.ffStart.ind Nelem],'dB',1,'ff Mult dB');
[stat,Model.ffmagGroup.SBEN.dB]=MakeErrorGroup( ...
  {'BEAMLINE';'SBEN'},[Model.ffStart.ind Nelem],'dB',1,'ff SBEN dB');

% assign EXT magnet groups

[stat,Model.EXTmagGroup.BPM.Off]=MakeErrorGroup( ...
  {'BEAMLINE';'MONI'} ,[1 Model.ffStart.ind],'Offset',1,'ff BPM Offset');
[stat,Model.EXTmagGroup.Quad.Off]=MakeErrorGroup( ...
  {'BEAMLINE';'QUAD'} ,[1 Model.ffStart.ind],'Offset',2,'ff Quads and SQuads index');
[stat,Model.EXTmagGroup.Quad.dB]=MakeErrorGroup( ...
  {'BEAMLINE';'QUAD'} ,[1 Model.ffStart.ind],'dB',1,'ff Quads and SQuads dB');
[stat,Model.EXTmagGroup.Quad.OffUse]=MakeErrorGroup( ...
  {'BEAMLINE';'QUAD'} ,[1 Model.ffStart.ind],'Offset',1,'ff Quads and SQuads Offset');
[stat,Model.EXTmagGroup.Sext.Off]=MakeErrorGroup( ...
  {'BEAMLINE';'SEXT'} ,[1 Model.ffStart.ind],'Offset',1,'ff Sextupoles Offset');
[stat,Model.EXTmagGroup.Mult.Off]=MakeErrorGroup( ...
  {'BEAMLINE';'MULT'} ,[1 Model.ffStart.ind],'Offset',1,'ff Mult Offset');
[stat,Model.EXTmagGroup.SBEN.Off]=MakeErrorGroup( ...
  {'BEAMLINE';'SBEN'} ,[1 Model.ffStart.ind],'Offset',1,'ff SBEN Offset');
[stat,Model.EXTmagGroup.Sext.dB]=MakeErrorGroup( ...
  {'BEAMLINE';'SEXT'} ,[1 Model.ffStart.ind],'dB',1,'ff Sextupoles dB');
[stat,Model.EXTmagGroup.Mult.dB]=MakeErrorGroup( ...
  {'BEAMLINE';'MULT'} ,[1 Model.ffStart.ind],'dB',1,'ff Mult dB');
[stat,Model.EXTmagGroup.SBEN.dB]=MakeErrorGroup( ...
  {'BEAMLINE';'SBEN'} ,[1 Model.ffStart.ind],'dB',1,'ff SBEN dB');

% assign DR magnet groups
% (NOTE: for MULTs, only assign sextupole skew quad windings)

[stat,Model.magGroup.RBPM.Off]=MakeErrorGroup( ...
  {'BEAMLINE';'MONI'} ,[1 Model.extStart],'Offset',1,'BPM Offset');
[stat,Model.magGroup.RQuad.Off]=MakeErrorGroup( ...
  {'BEAMLINE';'QUAD'} ,[1 Model.extStart],'Offset',2,'Quads and SQuads index');
[stat,Model.magGroup.RQuad.dB]=MakeErrorGroup( ...
  {'BEAMLINE';'QUAD'} ,[1 Model.extStart],'dB',1,'Quads and SQuads dB');
[stat,Model.magGroup.RQuad.OffUse]=MakeErrorGroup( ...
  {'BEAMLINE';'QUAD'} ,[1 Model.extStart],'Offset',1,'Quads and SQuads Offset');
[stat,Model.magGroup.RSext.Off]=MakeErrorGroup( ...
  {'BEAMLINE';'SEXT'} ,[1 Model.extStart],'Offset',1,'Sextupoles Offset');
[stat,Model.magGroup.ROct.Off]=MakeErrorGroup( ...
  {'BEAMLINE';'OCTU'} ,[1 Model.extStart],'Offset',1,'Octupoles Offset');
[stat,Model.magGroup.RMult.Off]=MakeErrorGroup( ...
  {'BEAMLINE';'MULT';'SQS*'} ,[1 Model.extStart],'Offset',1,'Mult Offset');
[stat,Model.magGroup.RSBEN.Off]=MakeErrorGroup( ...
  {'BEAMLINE';'SBEN'} ,[1 Model.extStart],'Offset',1,'SBEN Offset');
[stat,Model.magGroup.RSext.dB]=MakeErrorGroup( ...
  {'BEAMLINE';'SEXT'} ,[1 Model.extStart],'dB',1,'Sextupoles dB');
[stat,Model.magGroup.RMult.dB]=MakeErrorGroup( ...
  {'BEAMLINE';'MULT';'SQS*'} ,[1 Model.extStart],'dB',1,'Mult dB');
[stat,Model.magGroup.RSBEN.dB]=MakeErrorGroup( ...
  {'BEAMLINE';'SBEN'} ,[1 Model.extStart],'dB',1,'SBEN dB');
[stat,Model.magGroup.RXCOR.dB]=MakeErrorGroup( ...
  {'BEAMLINE';'XCOR'} ,[1 Model.extStart],'dB',1,'SBEN dB');
[stat,Model.magGroup.RYCOR.dB]=MakeErrorGroup( ...
  {'BEAMLINE';'YCOR'} ,[1 Model.extStart],'dB',1,'SBEN dB');


if ~isempty(sk1)
  for isk=1:length(sk1)
    BEAMLINE{sk1(isk)}.Class='SEXT';
  end
end

% set up PS stuff

Model.magGroup.SQuad_id=[];
for g_id=1:length(Model.magGroup.Quad.dB.ClusterList)
  Model.PSind.Quad(g_id)=length(PS)+1;
  stat=AssignToPS(Model.magGroup.Quad.dB.ClusterList(g_id).index,length(PS)+1);
  if (stat{1}~=1),error(stat{2:end}),end
  PS(Model.PSind.Quad(g_id)).dAmpl=0;
  PS(Model.PSind.Quad(g_id)).Step=0;
  % turn off skew quads
  for ind=Model.magGroup.Quad.dB.ClusterList(g_id).index(1):Model.magGroup.Quad.dB.ClusterList(g_id).index(end)
    if (find(ind==Model.handles.SQuad))
      Model.magGroup.SQuad_id=[Model.magGroup.SQuad_id,g_id];
      PS(Model.PSind.Quad(g_id)).SetPt=0;
      PS(Model.PSind.Quad(g_id)).Ampl=0;
      break
    end % if squad
  end % for ind
end % for g_id
for g_id=1:length(Model.magGroup.Sext.dB.ClusterList)
  Model.PSind.Sext(g_id)=length(PS)+1;
  stat=AssignToPS(Model.magGroup.Sext.dB.ClusterList(g_id).index,length(PS)+1);
  if (stat{1}~=1),error(stat{2:end}),end
  PS(Model.PSind.Sext(g_id)).dAmpl=0;
end % for g_id
for g_id=1:length(Model.magGroup.SBEN.dB.ClusterList)
  Model.PSind.SBEN(g_id)=length(PS)+1;
  stat=AssignToPS(Model.magGroup.SBEN.dB.ClusterList(g_id).index,length(PS)+1);
  if (stat{1}~=1),error(stat{2:end}),end
  PS(Model.PSind.SBEN(g_id)).dAmpl=0;
end % for g_id
for g_id=1:length(Model.magGroup.RQuad.dB.ClusterList)
  Model.PSind.RQuad(g_id)=length(PS)+1;
  stat=AssignToPS(Model.magGroup.RQuad.dB.ClusterList(g_id).index,length(PS)+1);
  if (stat{1}~=1),error(stat{2:end}),end
  PS(Model.PSind.RQuad(g_id)).dAmpl=0;
end % for g_id
for g_id=1:length(Model.magGroup.RSext.dB.ClusterList)
  Model.PSind.RSext(g_id)=length(PS)+1;
  stat=AssignToPS(Model.magGroup.RSext.dB.ClusterList(g_id).index,length(PS)+1);
  if (stat{1}~=1),error(stat{2:end}),end
  PS(Model.PSind.RSext(g_id)).dAmpl=0;
end
for g_id=1:length(Model.magGroup.RSBEN.Off.ClusterList)
  bhe=[]; bend=[];
  for ind=Model.magGroup.RSBEN.Off.ClusterList(g_id).index
    if (isfield(BEAMLINE{ind},'B')&&(ind<Model.extStart))
      bend(end+1)=ind;
    end
  end
  if ~isempty(bend)
    Model.PSind.RSBEN(g_id)=length(PS)+1;
    stat=AssignToPS(bend,length(PS)+1);
    if (stat{1}~=1),error(stat{2:end}),end
    PS(Model.PSind.RSBEN(g_id)).dAmpl=0;
  end  
end % for g_ig
for g_id=1:length(Model.magGroup.RMult.dB.ClusterList)
  Model.PSind.RMult(g_id)=length(PS)+1;
  stat=AssignToPS(Model.magGroup.RMult.dB.ClusterList(g_id).index,length(PS)+1);
  if (stat{1}~=1),error(stat{2:end}),end
  PS(Model.PSind.RMult(g_id)).dAmpl=0;
end

% SK1FF PS
if ~BEAMLINE{sk1(1)}.PS
  AssignToPS(sk1,length(PS)+1);
end

% distribute PS pointers to MULTs associated with magnets

id=setdiff(findcells(BEAMLINE,'Class','MULT'),findcells(BEAMLINE,'Name','SQS*'));
name=unique(cellfun(@(x) x.Name,BEAMLINE(id),'UniformOutput',false));
for n=1:length(name)
  mname=strrep(name{n},'MULT',''); % strip off 'MULT' to get magnet name
  id=findcells(BEAMLINE,'Name',mname);
  if (isempty(id))
    mname=strcat(mname,'A'); % SBEN names end in 'A' or 'B'
    id=findcells(BEAMLINE,'Name',mname);
  end
  psid=BEAMLINE{id(1)}.PS;
  id=findcells(BEAMLINE,'Name',name{n});
  for m=1:length(id)
    BEAMLINE{id(m)}.PS=psid;
  end
end

% assign vibration table elements

iSF1=findcells(BEAMLINE,'Name','SF1FF');
iQD0=findcells(BEAMLINE,'Name','QD0FF');
Model.vibTable.ind=iSF1(1)-2:BEAMLINE{iQD0(end)}.Block(end)+2; % inlclude BPMs before SF1 and after QD0
Model.vibTable.S=[];
for iEle=Model.vibTable.ind
  Model.vibTable.S=[Model.vibTable.S,BEAMLINE{iEle}.S];
end

% set up girders

moverList=[findcells(BEAMLINE,'Name','AQ*'), ...
           findcells(BEAMLINE,'Name','AS*'), ...
           findcells(BEAMLINE,'Name','AFB*')];
moverList=sort(moverList);
Model.moverList=[];
for g_id=1:length(Model.magGroup.Quad.dB.ClusterList)
  mag_ind= ...
    BEAMLINE{Model.magGroup.Quad.OffUse.ClusterList(g_id).index(1)}.Block(1): ...
    BEAMLINE{Model.magGroup.Quad.OffUse.ClusterList(g_id).index(1)}.Block(end);
  % find BPM associated with this magnet if one closer than 10cm
  % (use closest one to mag center if more than 1)
  qbpm=find(cellfun(@(x) ...
    (x.S-BEAMLINE{Model.magGroup.Quad.dB.ClusterList(g_id).index(end)+1}.S)<0.13 & ...
    x.S>BEAMLINE{Model.magGroup.Quad.dB.ClusterList(g_id).index(end)}.S, ...
    {BEAMLINE{Model.handles.BPM}}));
  if (isempty(qbpm))
    qbpm=find(cellfun(@(x) ...
      (BEAMLINE{Model.magGroup.Quad.dB.ClusterList(g_id).index(1)}.S-x.S)<0.13 & ...
      x.S<BEAMLINE{Model.magGroup.Quad.dB.ClusterList(g_id).index(1)}.S, ...
      {BEAMLINE{Model.handles.BPM}}));
  end % if isempty(qbpm)
  if (length(qbpm)>1)
    bpmDist=cellfun(@(x) ...
      abs(BEAMLINE{Model.magGroup.Quad.dB.ClusterList(g_id).index(end)}.S-x.S), ...
      {BEAMLINE{Model.handles.BPM(qbpm)}});
    [minVal,minInd]=min(bpmDist);
    qbpm=qbpm(minInd);
  end % if length(qbpm)>1
  if (~isempty(qbpm))
    qbpm=Model.handles.BPM(qbpm);
    if (~isequal(BEAMLINE{qbpm(1)}.Class,'MONI')),error('QBPM location error!'),end
    if (qbpm<mag_ind(1))
      mag_ind=[qbpm,mag_ind]; %#ok<AGROW>
    elseif (qbpm>mag_ind(end))
      mag_ind=[mag_ind,qbpm]; %#ok<AGROW>
    end % where is qbpm?
    Model.magGroup.Quad.Bpm(g_id).Ind=qbpm;
    Model.magGroup.Quad.Bpm(g_id).Han=find(Model.handles.BPM==qbpm);
  else
    Model.magGroup.Quad.Bpm(g_id).Ind=[];
    Model.magGroup.Quad.Bpm(g_id).Han=[];
  end % isempty(qbpm)?
  % put quad stuff on same girder
  gnum=length(GIRDER)+1;
  stat=AssignToGirder(mag_ind,gnum,0);
  if (stat{1})
    Model.Gind.Quad(g_id)=gnum;
    if (any(ismember(GIRDER{gnum}.Element(1):GIRDER{gnum}.Element(end),moverList)))
      [stat,GIRDER{gnum}]=AddMoverToGirder([1,3,6],GIRDER{gnum});
      if (~stat{1}),error(stat{2}),end
      GIRDER{gnum}.MoverStep=Model.align.mover_step;
      Model.moverList=[Model.moverList gnum];
    end % if mover on girder
  elseif (isfield(BEAMLINE{Model.magGroup.Quad.OffUse.ClusterList(g_id).index(1)},'Girder') && ...
    BEAMLINE{Model.magGroup.Quad.OffUse.ClusterList(g_id).index(1)}.Girder)
    Model.Gind.Quad(g_id)=BEAMLINE{mag_ind(1)}.Girder;
  else
    error(['Error with Girder assignment for Quad: ',num2str(g_id)])
  end % if stat{1}
end
for g_id=1:length(Model.magGroup.Sext.dB.ClusterList)
  mag_ind=BEAMLINE{Model.magGroup.Sext.Off.ClusterList(g_id).index(1)}.Block(1): ...
    BEAMLINE{Model.magGroup.Sext.Off.ClusterList(g_id).index(1)}.Block(end);
  % find BPM associated with this magnet if one closer than 10cm
  % (use closest one to mag center if more than 1)
  sbpm=find(cellfun(@(x) ...
    (x.S-BEAMLINE{Model.magGroup.Sext.dB.ClusterList(g_id).index(end)+1}.S)<0.1 & ...
    x.S>BEAMLINE{Model.magGroup.Sext.dB.ClusterList(g_id).index(end)}.S, ...
    {BEAMLINE{Model.handles.BPM}}));
  if (isempty(sbpm))
    sbpm=find(cellfun(@(x) ...
      (BEAMLINE{Model.magGroup.Sext.dB.ClusterList(g_id).index(1)}.S-x.S)<0.1 & ...
      x.S<BEAMLINE{Model.magGroup.Sext.dB.ClusterList(g_id).index(1)}.S, ...
      {BEAMLINE{Model.handles.BPM}}));
  end % if isempty(qbpm)
  if (length(sbpm)>1)
    bpmDist=cellfun(@(x) ...
      abs(BEAMLINE{Model.magGroup.Sext.dB.ClusterList(g_id).index(end)}.S-x.S), ...
      {BEAMLINE{Model.handles.BPM(sbpm)}});
    [minVal,minInd]=min(bpmDist);
    sbpm=sbpm(minInd);
  end % if length(sbpm)>1
  if (~isempty(sbpm))
    sbpm=Model.handles.BPM(sbpm);
    if (~isequal(BEAMLINE{sbpm}.Class,'MONI')),error('QBPM location error!'),end
    if (sbpm<mag_ind(1))
      mag_ind=[sbpm,mag_ind]; %#ok<AGROW>
    elseif (sbpm>mag_ind(end))
      mag_ind=[mag_ind,sbpm]; %#ok<AGROW>
    end % where is sbpm?
    Model.magGroup.Sext.Bpm(g_id).Ind=sbpm;
    Model.magGroup.Sext.Bpm(g_id).Han=find(Model.handles.BPM==sbpm);
  else
    Model.magGroup.Sext.Bpm(g_id).Ind=[];
    Model.magGroup.Sext.Bpm(g_id).Han=[];
  end % isempty(qbpm)?
  % put Sextupole stuff on same girder
  gnum=length(GIRDER)+1;
  stat=AssignToGirder(mag_ind,gnum,0);
  if (stat{1})
    Model.Gind.Sext(g_id)=gnum;
    if (any(ismember(GIRDER{gnum}.Element(1):GIRDER{gnum}.Element(end),moverList)))
      [stat,GIRDER{gnum}]=AddMoverToGirder([1,3,6],GIRDER{gnum});
      if (~stat{1}),error(stat{2}),end
      GIRDER{gnum}.MoverStep=Model.align.mover_step;
      Model.moverList=[Model.moverList,gnum];
    end % if mover on girder
  elseif (isfield(BEAMLINE{Model.magGroup.Sext.Off.ClusterList(g_id).index(1)},'Girder') && ...
    BEAMLINE{Model.magGroup.Sext.Off.ClusterList(g_id).index(1)}.Girder)
    Model.Gind.Sext(g_id)=BEAMLINE{mag_ind(1)}.Girder;
  else
    error(['Error with Girder assignment for Sext: ',num2str(g_id)])
  end % if stat{1}
end

% add feedback girder

stat=AssignToGirder(findcells(BEAMLINE,'Name','MFB2FF'),length(GIRDER)+1,0);
if (stat{1}~=1),error(stat{2}),end
Model.moverList=[Model.moverList,length(GIRDER)];
[stat,GIRDER{end}]=AddMoverToGirder([1,3,6],GIRDER{end});
if (~stat{1}),error(stat{2}),end

% ground motion data

lastGirder=0;Model.gmData=[];lastEle=0;
for iEle=1:Nelem
  if (isfield(BEAMLINE{iEle},'Girder')&&BEAMLINE{iEle}.Girder&& ...
    (BEAMLINE{iEle}.Girder~=lastGirder))
    gmInd=length(Model.gmData)+1;
    Model.gmData(gmInd).sCoords=GIRDER{BEAMLINE{iEle}.Girder}.S ;
    Model.gmData(gmInd).girder=BEAMLINE{iEle}.Girder;
    lastGirder=BEAMLINE{iEle}.Girder;   
  elseif ((~isfield(BEAMLINE{iEle},'Girder')||~BEAMLINE{iEle}.Girder)&& ...
    isfield(BEAMLINE{iEle},'Offset')&&(iEle>lastEle))
    gmInd=length(Model.gmData)+1;
    if (isfield(BEAMLINE{iEle},'Block'))
      Model.gmData(gmInd).sCoords=  ...
        [BEAMLINE{BEAMLINE{iEle}.Block(1)}.S,BEAMLINE{BEAMLINE{iEle}.Block(2)}.S];
      Model.gmData(gmInd).element=BEAMLINE{iEle}.Block;
      lastEle=BEAMLINE{iEle}.Block(2);
    else
      Model.gmData(gmInd).sCoords=BEAMLINE{iEle}.S ;
      Model.gmData(gmInd).element=iEle;
      lastEle=iEle;
    end % if is block
  end % if element moveable and not on already dealt-with girder
end % for iEle (e- beamline)

% make a group of all magnets

allmag_elements=[[PS([Model.PSind.Quad]).Element],[PS([Model.PSind.Sext]).Element]];
vals=sort(allmag_elements(1:2:end));
for mag_ind=1:length(vals)
  switch BEAMLINE{vals(mag_ind)}.Class
    case 'QUAD'
      Model.magGroup.AllMag{mag_ind}.Type='Quad';
    case 'SEXT'
      Model.magGroup.AllMag{mag_ind}.Type='Sext';
    case 'OCTU'
      Model.magGroup.AllMag{mag_ind}.Type='Oct';
    otherwise
      error('Bad type!')
  end
  % assign pointers to specific magnet groups
  glist=[PS(Model.PSind.(Model.magGroup.AllMag{mag_ind}.Type)).Element];
  Model.magGroup.AllMag{mag_ind}.Pointer=find(glist(1:2:end)==vals(mag_ind));
end
  
% FB correctors

% assign X-Correctors
w=warning('off','MATLAB:PSTrim_online');
nCor=0;
for iCor=findcells(BEAMLINE,'Class','XCOR')
  nCor=nCor+1;
  Model.PSind.cor.fbX1(nCor)=length(PS)+1;
  stat=AssignToPS(iCor,length(PS)+1);
  if (stat{1}~=1),error(stat{2:end}),end
  % set corrector B to put PS in rad units and set initially to zero
  BEAMLINE{iCor}.B=Cb*BEAMLINE{iCor}.P;
  PS(Model.PSind.cor.fbX1(nCor)).SetPt=0;
  stat=PSTrim(Model.PSind.cor.fbX1(nCor));
  if (stat{1}~=1),error(stat{2:end}),end
end
% assign Y-Correctors
nCor=0;
for iCor=findcells(BEAMLINE,'Class','YCOR')
  nCor=nCor+1;
  Model.PSind.cor.fbY1(nCor)=length(PS)+1;
  stat=AssignToPS(iCor,length(PS)+1);
  if (stat{1}~=1),error(stat{2:end}),end
  % set Corrector B to put PS in rad units and set initially to zero
  BEAMLINE{iCor}.B=Cb*BEAMLINE{iCor}.P;
  PS(Model.PSind.cor.fbY1(nCor)).SetPt=0;
  stat=PSTrim(Model.PSind.cor.fbY1(nCor));
  if (stat{1}~=1),error(stat{2:end}),end
end
warning(w.state,'MATLAB:PSTrim_online');
% pulse-pulse feedback parameters (FF)
Model.fbBpmsToUse=[ ...
  findcells(BEAMLINE,'Name','MFB2FF'), ...
  findcells(BEAMLINE,'Name','MQD10BFF')];
Model.fbBpmsToUseX=[ ...
  findcells(BEAMLINE,'Name','MFB1FF') ...
  findcells(BEAMLINE,'Name','MQF9BFF')];
Model.fbCorToUse=[ ...
  findcells(BEAMLINE,'Name','ZV1FF') ...
  findcells(BEAMLINE,'Name','ZV11X')];
Model.fbCorToUseX=[ ...
  findcells(BEAMLINE,'Name','ZH10X') ...
  findcells(BEAMLINE,'Name','ZH1FF')];
Model.fbWeight=0.1;

% generate beams

Beam1=MakeBeam6DGauss(Model.Initial,10001,5,0);
Beam0=makeSingleBunch(Beam1);
Beam2=MakeBeam6DSparse(Model.Initial,5,31,11);

% turn on tracking flags and set BPM resolutions and BPM types, etc.

%ip_instr={'MBS2IP','MBS1IP'};
ip_instr={'MBS1IP'}; % Honda-monitor not installed
for bpm_ind=Model.handles.BPM
  BEAMLINE{bpm_ind}.TrackFlag.GetBPMData=1;
  BEAMLINE{bpm_ind}.TrackFlag.GetBPMBeamPars=1;
end % for bpm_ind
SetTrackFlags('GetInstData',1,1,Nelem);
for ii=1:length(ip_instr)
  BEAMLINE{findcells(BEAMLINE,'Name',ip_instr{ii})}.TrackFlag.GetInstData=1;
end % for ii

% kluge the PS list (and BEAMLINE references) for "dual" correctors between
% extraction kicker and extraction septa

dualNames=[ ...
  {'ZH100R'},{'ZH100RX'}; ...
  {'ZH101R'},{'ZH101RX'}; ...
  {'ZV100R'},{'ZV100RX'}; ...
];
[Ndual,dummy]=size(dualNames);
for n=1:Ndual
  name=dualNames{n,1};
  id=findcells(BEAMLINE,'Name',name);
  idp=BEAMLINE{id(1)}.PS;
  namex=dualNames{n,2};
  idx=findcells(BEAMLINE,'Name',namex);
  idpx=BEAMLINE{idx(1)}.PS;
  PS(idp).Element=[id,idx]; % DR power supply points to both incarnations
  for m=1:length(idx)
    BEAMLINE{idx(m)}.PS=idp; % EXT incarnation points to DR power supply 
  end
  PS(idpx).Element=0; % EXT power supply points to nothing
end

% unTILT FF sextupoles and apply rolls to associated girders

name={'SF6FF','SF5FF','SD4FF','SF1FF','SD0FF'};
for n=1:length(name)
  id=findcells(BEAMLINE,'Name',name{n});
  idg=BEAMLINE{id(1)}.Girder;
  tilt=BEAMLINE{id(1)}.Tilt;
  GIRDER{idg}.Offset(6)=tilt; % tilts BPM as well
  for m=1:length(id)
    BEAMLINE{id(m)}.Tilt=0;
  end
end

% check tracking of beam through EXT+FFS

Beam1_IEX=MakeBeam6DGauss(Model.Initial_IEX,10001,5,0);
Beam0_IEX=makeSingleBunch(Beam1_IEX);
Beam2_IEX=MakeBeam6DSparse(Model.Initial_IEX,5,31,11);
[stat,beamout]=TrackThru(Model.extStart, ...
  findcells(BEAMLINE,'Name','MBS1IP'),Beam1_IEX,1,1,0);
if (stat{1}~=1),error(stat{2}),end
Model.doSMRes=false;
fprintf('%s: MULTs ON\n',Model.opticsName);
fprintf('IP beamsize: %g (x) %g (y)\n', ...
  std(beamout.Bunch.x(1,:)),std(beamout.Bunch.x(3,:)));
fprintf('IP beampos: %g (x) %g (y)\n', ...
  mean(beamout.Bunch.x(1,:)),mean(beamout.Bunch.x(3,:)));

% create INSTR structure and save lattice

[stat,beamout,instdata]=TrackThru(1,length(BEAMLINE),Beam2,1,1,0);
[Model,INSTR]=CreateINSTR(Model,instdata);
eval(['save ATF2lat_',strcat(Model.opticsName,'.mat'), ...
  ' BEAMLINE PS GIRDER FL MDATA INSTR Model Beam*'])

% check tracking of beam through EXT+FFS (MULTs OFF)
% (NOTE: keep quadrupole term for KEX MULTs to maintain 1st order match)

BEAMLINE0=BEAMLINE;
idm=setxor(findcells(BEAMLINE,'Class','MULT'),findcells(BEAMLINE,'Name','SQS*'));
for m=1:length(idm)
  n=idm(m);
  if (strcmp(BEAMLINE{n}.Name(1:3),'KEX')&&(BEAMLINE{n}.B(1)~=0))
    BEAMLINE{n}.B=BEAMLINE{n}.B(1);
    BEAMLINE{n}.Tilt=BEAMLINE{n}.Tilt(1);
    BEAMLINE{n}.PoleIndex=BEAMLINE{n}.PoleIndex(1);
  else
    BEAMLINE{n}.B=0;
    BEAMLINE{n}.Tilt=0;
    BEAMLINE{n}.PoleIndex=0;
  end
end
[stat,beamout]=TrackThru(Model.extStart, ...
  findcells(BEAMLINE,'Name','MBS1IP'),Beam1_IEX,1,1,0);
if (stat{1}~=1),error(stat{2}),end
fprintf('%s: MULTs OFF\n',Model.opticsName);
fprintf('IP beamsize: %g (x) %g (y)\n', ...
  std(beamout.Bunch.x(1,:)),std(beamout.Bunch.x(3,:)));
fprintf('IP beampos: %g (x) %g (y)\n', ...
  mean(beamout.Bunch.x(1,:)),mean(beamout.Bunch.x(3,:)));
eval(['save ATF2lat_',strcat(Model.opticsName,'_nomult.mat'), ...
  ' BEAMLINE PS GIRDER FL MDATA INSTR Model Beam*'])
BEAMLINE=BEAMLINE0;
clear BEAMLINE0

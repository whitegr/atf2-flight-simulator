N=length(BEAMLINE);
ip=findcells(BEAMLINE,'Name','IP');
id2=findcells(BEAMLINE,'Name','MFB2FF');
id1=findcells(BEAMLINE,'Name','MFB1FF');

% compute Twiss
[stat,T]=GetTwiss(1,N,FL.SimModel.Initial.x.Twiss,FL.SimModel.Initial.y.Twiss);

% find waists
bxip=T.betax(ip);
axip=T.alphax(ip);
gxip=(1+axip^2)/bxip;
bxw=1/gxip;
lxw=axip/gxip;
muxw=atan2d(lxw,bxip-axip*lxw);
byip=T.betay(ip);
ayip=T.alphay(ip);
gyip=(1+ayip^2)/byip;
byw=1/gyip;
lyw=ayip/gyip;
muyw=atan2d(lyw,byip-ayip*lyw);

% check MFB2FF and MFB1FF
dmuyip=360*(T.nuy(ip)-T.nuy(id2))-540;
dmuxip=360*(T.nux(ip)-T.nux(id1))-540;

% display results
fprintf('\n');
fprintf('At MFB2FF:\n');
fprintf(' ALFY  = %9.4f\n',T.alphay(id2));
fprintf(' dMUY* = %9.4f deg\n',dmuyip);
fprintf('\n');
fprintf('At MFB1FF:\n');
fprintf(' ALFX  = %9.4f\n',T.alphax(id1));
fprintf(' dMUX* = %9.4f deg\n',dmuxip);
fprintf('\n');
fprintf(1,'BETX* = %10.4f mm\n',1e3*bxip);
fprintf(1,'ALFX* = %10.4f\n',axip);
fprintf(1,'DX*   = %10.4f mm\n',1e3*T.etax(ip));
fprintf(1,'DPX*  = %10.4f mr\n',1e3*T.etapx(ip));
fprintf('\n');
fprintf(1,'BETY* = %10.4f mm\n',1e3*byip);
fprintf(1,'ALFY* = %10.4f\n',ayip);
fprintf(1,'DY*   = %10.4f mm\n',1e3*T.etay(ip));
fprintf(1,'DPY*  = %10.4f mr\n',1e3*T.etapy(ip));
fprintf('\n');
fprintf('LXw   = %+10.4f mm\n',1e3*lxw);
fprintf('BETXw = %10.4f mm\n',1e3*bxw);
fprintf('dMUXw = %10.4f deg\n',muxw);
fprintf('\n');
fprintf('LYw   = %+10.4f mm\n',1e3*lyw);
fprintf('BETYw = %+10.4f mm\n',1e3*byw);
fprintf('dMUYw = %10.4f deg\n',muyw);
fprintf('\n');

return

figure(1)
plot(T.S,sqrt([T.betax;T.betay]))
ylabel('RBETA')
xlabel('S')
legend('X','Y',2)

figure(2)
plot(T.S,[T.etax;T.etay])
ylabel('DX')
xlabel('S')


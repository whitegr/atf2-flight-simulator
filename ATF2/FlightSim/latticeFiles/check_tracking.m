
% fix the random number stream

if (verLessThan('matlab','7.13'))
  randn('state',12345)
elseif (verLessThan('matlab','7.14'))
  reset(RandStream.getDefaultStream,12345);
else
  rng(12345)
end

% check tracking (MULTs ON)

ip=findcells(BEAMLINE,'Name','IP');
[stat,beamout]=TrackThru(1,ip,FL.SimBeam{1},1,1,0);
if (stat{1}~=1),error(stat{2}),end
FL.SimModel.doSMRes=false;
fprintf('%s: MULTs ON\n',FL.SimModel.opticsName);
fprintf('IP beamsize: %7.3f um (x) %7.1f nm (y)\n', ...
  1e6*std(beamout.Bunch.x(1,:)),1e9*std(beamout.Bunch.x(3,:)));
fprintf('IP beampos:  %7.3f um (x) %7.1f nm (y)\n', ...
  1e6*mean(beamout.Bunch.x(1,:)),1e9*mean(beamout.Bunch.x(3,:)));

% check tracking (MULTs OFF)
% (NOTE: keep quadrupole terms (PoleIndex=1) to maintain 1st order match)

BEAMLINE0=BEAMLINE;
idm=setxor( ...
  findcells(BEAMLINE,'Class','MULT'), ...
  findcells(BEAMLINE,'Name','SQS*'));
for m=1:length(idm)
  n=idm(m);
  id1=find(BEAMLINE{n}.PoleIndex==1);
  if (isempty(id1))
    BEAMLINE{n}.B=0;
    BEAMLINE{n}.Tilt=0;
    BEAMLINE{n}.PoleIndex=0;
  else
    BEAMLINE{n}.B=BEAMLINE{n}.B(id1);
    BEAMLINE{n}.Tilt=BEAMLINE{n}.Tilt(id1);
    BEAMLINE{n}.PoleIndex=BEAMLINE{n}.PoleIndex(id1);
  end
end
[stat,beamout]=TrackThru(1,ip,FL.SimBeam{1},1,1,0);
if (stat{1}~=1),error(stat{2}),end
fprintf('%s: MULTs OFF\n',FL.SimModel.opticsName);
fprintf('IP beamsize: %7.3f um (x) %7.1f nm (y)\n', ...
  1e6*std(beamout.Bunch.x(1,:)),1e9*std(beamout.Bunch.x(3,:)));
fprintf('IP beampos:  %7.3f um (x) %7.1f nm (y)\n', ...
  1e6*mean(beamout.Bunch.x(1,:)),1e9*mean(beamout.Bunch.x(3,:)));
BEAMLINE=BEAMLINE0;
clear BEAMLINE0

function varargout = lattice_check(varargin)
% LATTICE_CHECK M-file for lattice_check.fig
%      LATTICE_CHECK, by itself, creates a new LATTICE_CHECK or raises the existing
%      singleton*.
%
%      H = LATTICE_CHECK returns the handle to a new LATTICE_CHECK or the handle to
%      the existing singleton*.
%
%      LATTICE_CHECK('CALLBACK',hObject,eventData,handles,...) calls the
%      local
%      function named CALLBACK in LATTICE_CHECK.M with the given input arguments.
%
%      LATTICE_CHECK('Property','Value',...) creates a new LATTICE_CHECK or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before lattice_check_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to lattice_check_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help lattice_check

% Last Modified by GUIDE v2.5 09-Dec-2008 09:31:29

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @lattice_check_OpeningFcn, ...
                   'gui_OutputFcn',  @lattice_check_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before lattice_check is made visible.
function lattice_check_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to lattice_check (see VARARGIN)

% Choose default command line output for lattice_check
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes lattice_check wait for user response (see UIRESUME)
% uiwait(handles.figure1);

global LC_GUIDATA BEAMLINE INSTR GIRDER

% Detects the user pressing the cancel button
LC_GUIDATA.iscancelled = false;

% Fill the "region" list -- ext, ff, all, etc.
list = get(handles.region_menu,'String');
select = get(handles.region_menu,'Value');
LC_GUIDATA.region = list{select};

% Fills the "magnet selection" list -- which magnets to scan
list = get(handles.magselect_menu,'String');
select = get(handles.magselect_menu,'Value');
LC_GUIDATA.magselect = list{select};

% Default kick size
LC_GUIDATA.kicksize = str2double(get(handles.kicksize_edit,'String'));
LC_GUIDATA.moversize = 0.5;

% Number of averages to do
LC_GUIDATA.ave_edit = str2double(get(handles.ave_edit,'String'));

% Find the start and end of each of the regions and a list of the
% correctors in those regions
LC_GUIDATA.ringend = findcells(BEAMLINE,'Name','IEX');
LC_GUIDATA.ringend = LC_GUIDATA.ringend(end);
LC_GUIDATA.extff   = findcells(BEAMLINE,'Name','BEGFF');
LC_GUIDATA.xcorinds = findcells(BEAMLINE,'Class','XCOR');
LC_GUIDATA.xcorinds = ...
    LC_GUIDATA.xcorinds(LC_GUIDATA.xcorinds>LC_GUIDATA.ringend);
LC_GUIDATA.ycorinds = findcells(BEAMLINE,'Class','YCOR');
LC_GUIDATA.ycorinds = ...
    LC_GUIDATA.ycorinds(LC_GUIDATA.ycorinds>LC_GUIDATA.ringend);

% Find all girders on a mover
xmover_ind = [];
ymover_ind = [];
xmover_S = [];
ymover_S = [];
for gnum = 1:length(GIRDER)
    if ~isfield(GIRDER{gnum},'Mover')
        continue
    end
    for dof = GIRDER{gnum}.Mover
        if dof == 1
            xmover_ind = [xmover_ind gnum]; %#ok<AGROW>
            xmover_S = [xmover_S GIRDER{gnum}.S(1)]; %#ok<AGROW>
        elseif dof == 3
            ymover_ind = [ymover_ind gnum]; %#ok<AGROW>
            ymover_S = [ymover_S GIRDER{gnum}.S(1)]; %#ok<AGROW>
        end
    end
end

% Set the names of these girders by the quad/sext name
count = 0;
for mnum=xmover_ind
    for elenum = GIRDER{mnum}.Element
        if strcmpi(BEAMLINE{elenum}.Class,'QUAD')
            count = count + 1;
            LC_GUIDATA.xmover_name{count} = [BEAMLINE{elenum}.Name '_x'];
            LC_GUIDATA.xmover_ind{count} = mnum;
            LC_GUIDATA.xmover_S{count} = GIRDER{mnum}.S;
            break
        end
    end
end
count = 0;
for mnum=ymover_ind
    for elenum = GIRDER{mnum}.Element
        if strcmpi(BEAMLINE{elenum}.Class,'QUAD')
            count = count + 1;
            LC_GUIDATA.ymover_name{count} = [BEAMLINE{elenum}.Name '_y'];
            LC_GUIDATA.ymover_ind{count} = mnum;
            LC_GUIDATA.ymover_S{count} = GIRDER{mnum}.S;
            break
        end
    end
end

% First bpm in the extraction line
bpminds = findcells(BEAMLINE,'Class','MONI');
bpminds = bpminds(bpminds>LC_GUIDATA.ringend);
LC_GUIDATA.firstbpm = findcells(INSTR,'Index',bpminds(1));


% --- Outputs from this function are returned to the command line.
function varargout = lattice_check_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles;
varargout{2} = hObject;


% --- Executes on button press in start_button.
function start_button_Callback(hObject, eventdata, handles)
% hObject    handle to start_button (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

global LC_GUIDATA BEAMLINE PS FL GIRDER

% Set the start button colour and message
set(hObject,'BackgroundColor',[1 0 0])
set(hObject,'String','RUNNING!')

% Disable other front panel buttons to avoid confusion
set(hObject,'Enable','inactive')
set(handles.plot_pushbutton,'Enable','inactive')
set(handles.save_button,'Enable','inactive')
set(handles.load_button,'Enable','inactive')
set(handles.region_menu,'Enable','inactive')
set(handles.magselect_menu,'Enable','inactive')
set(handles.devicetype_menu,'Enable','inactive')
set(handles.ave_edit,'Enable','inactive')
set(handles.kicksize_edit,'Enable','inactive')

% Enable the cancel button and change its colour
set(handles.cancel_button,'Enable','on')
set(handles.cancel_button,'BackgroundColor',[1 0 0])

% Cors or movers
devtype_contents = get(handles.devicetype_menu,'String');
devtype_choice = devtype_contents{get(handles.devicetype_menu,'Value')};
if strcmpi(devtype_choice,'Movers')
    movers = true;
else
    movers = false;
end

% Single particle beam
beamin = FL.SimBeam{2};

% May be calculating bumps, so add this routine to the path
if ~isdeployed
  addpath testApps/bumpgui;
end

% Scale the kick from m.rad to rad.
mradkick = str2double(get(handles.kicksize_edit,'String'))/1e3;

% Get the list of correctors from the input box
corlist = get(handles.maglistbox,'String');
% Convert this to a cell array if necessary
if ~iscell(corlist)
    temp{1} = corlist;
    clear corlist
    corlist = temp;
end

% If this cell array is empty, then we have an error.  Reset the front
% panel buttons and exit.
if (strcmpi(corlist{1},'Empty ...') || strcmpi(corlist{1},'none'))
    errordlg('No magnets selected.','No magnets')
    set(hObject,'BackgroundColor',[0 1 0])
    set(hObject,'String','START!')
    set(hObject,'Enable','on')
    set(handles.plot_pushbutton,'Enable','on')
    set(handles.save_button,'Enable','on')
    set(handles.load_button,'Enable','on')
    set(handles.region_menu,'Enable','on')
    set(handles.magselect_menu,'Enable','on')
    set(handles.devicetype_menu,'Enable','on')
    set(handles.ave_edit,'Enable','on')
    set(handles.kicksize_edit,'Enable','on')
    set(handles.cancel_button,'Enable','off')
    set(handles.cancel_button,'BackgroundColor',[0.502 0.502 0.502])
    return
end

% Get the name of the region we're investigating
region_contents = get(handles.region_menu,'String');
region_choice = region_contents{get(handles.region_menu,'Value')};

% Get the contents of the magnet select drop down menu
mag_contents = get(handles.magselect_menu,'String');
mag_choice = mag_contents{get(handles.magselect_menu,'Value')};

% Figure out if we'll be closing bumps, and, if so, which correctors we'll
% be using to do that.  This is all based on the region and correctors
% selection.
if ~movers
    if strcmpi(region_choice,'Extraction Line')
        close_bumps = true;
        final_xcors = [findcells(BEAMLINE,'Name','ZH10X') ...
            findcells(BEAMLINE,'Name','ZH1FF')];
        final_ycors = [findcells(BEAMLINE,'Name','ZV11X') ...
            findcells(BEAMLINE,'Name','ZV1FF')];
        if strncmpi(mag_choice,'All',3)
        elseif ( sum(strcmpi(corlist,'ZH9X')) || sum(strcmpi(corlist,'ZH10X')) || ...
                sum(strcmpi(corlist,'ZV10X')) || sum(strcmpi(corlist,'ZV11X')) )
            close_bumps = false;
        end
        
    elseif strcmpi(region_choice,'Final Focus')
        close_bumps = false;
        
    elseif strcmpi(region_choice,'All')
        close_bumps = true;
        final_xcors = [findcells(BEAMLINE,'Name','ZH10X') ...
            findcells(BEAMLINE,'Name','ZH1FF')];
        final_ycors = [findcells(BEAMLINE,'Name','ZV11X') ...
            findcells(BEAMLINE,'Name','ZV1FF')];
        
    else
        error('Something''s gone terribly wrong :S')
    end
end

% Find the PSs/GIRDERs used to control our devices
if movers
    cor_ps = zeros(1,length(corlist));
    mover_girder = zeros(1,length(corlist));
    mover_plane = cell(1,length(corlist));
    for movernum = 1:length(corlist)
        l = length(corlist{movernum});
        magname = corlist{movernum}(1:(l-2));
        eleind = findcells(BEAMLINE,'Name',magname);
        mover_girder(movernum) = BEAMLINE{eleind(1)}.Girder;
        mover_plane{movernum} = corlist{movernum}(end);
    end
else
    cor_ps = zeros(1,length(corlist));
    for cornum=1:length(corlist)
        corind(cornum) = findcells(BEAMLINE,'Name',corlist{cornum}); %#ok<AGROW>
        cor_ps(cornum) = BEAMLINE{corind(cornum)}.PS;
    end
end

% Clear the current plots
for plotnum=1:4
    plothand = subplot(2,2,plotnum,'Parent',handles.measdata_panel);
    cla(plothand)
    plothand = subplot(2,2,plotnum,'Parent',handles.preddata_panel);
    cla(plothand)
end
plothand = subplot(1,1,1,'Parent',handles.diffdx_panel);
cla(plothand)
plothand = subplot(1,1,1,'Parent',handles.diffdy_panel);
cla(plothand)

% MAIN LOOP!
% Loop around our correctors and do what we need to do
for cornum=1:length(cor_ps)
    if (strcmpi(corlist{cornum},'ZX2X') || ...
            strcmpi(corlist{cornum},'ZX3X') )
        continue
    end
    % Report the status in the start button string
    set(hObject,'String',[corlist{cornum} ' baseline data'])
    disp(corlist{cornum})  % Also report to the cli
    
    % Request access for the correctors we need (closing bumps or not)
    if movers
        stat = AccessRequest({mover_girder(cornum),[],[]});
        if stat{1}~=1; error(stat{2}); end
    else
        if close_bumps
            if strcmpi(BEAMLINE{corind(cornum)}.Class,'XCOR')
                knob = bumpgui(2,3,corind(cornum),[corind(cornum) final_xcors]);
                stat = AccessRequest({[],...
                    [knob.Channel(1).Unit ...
                    knob.Channel(2).Unit ...
                    knob.Channel(3).Unit],...
                    []});
                if stat{1}~=1; error(stat{2}); end
                
            elseif strcmpi(BEAMLINE{corind(cornum)}.Class,'YCOR')
                knob = bumpgui(4,3,corind(cornum),[corind(cornum) final_ycors]);
                stat = AccessRequest({[],...
                    [knob.Channel(1).Unit ...
                    knob.Channel(2).Unit ...
                    knob.Channel(3).Unit],...
                    []});
                if stat{1}~=1; error(stat{2}); end
                
            else
                error('Something''s not right :(')
            end
        else
            stat = AccessRequest({[],cor_ps(cornum),[]});
            if stat{1}~=1; error(stat{2}); end
        end
    end
    
    % Call FlHwUpdate to synch with the server, then grab the data for the
    % un-kicked (nominal position) beam.
    FlHwUpdate;
    [stat output] = FlHwUpdate('bpmave',LC_GUIDATA.ave_edit);
    if stat{1}~=1; error(stat{2}); end
    [stat instdata] = FlTrackThru(1,length(BEAMLINE),output);
    if stat{1}~=1; error(stat{2}); end
    
    % Sort the data into seperate arrays
    orig_x = [instdata{1}.x]; orig_x = orig_x(LC_GUIDATA.firstbpm:end);
    orig_y = [instdata{1}.y]; orig_y = orig_y(LC_GUIDATA.firstbpm:end);
    orig_S = [instdata{1}.S]; orig_S = orig_S(LC_GUIDATA.firstbpm:end);
    
    % Set the bpmS field of LC_GUIDATA
    LC_GUIDATA.bpmS = orig_S;
    
    % Looping over the number of averages, calculate the predicted position
    % of the beam.  It shouldn't be necessary to average, but the finite
    % BPM resolution in BEAMLINE forces this.  This needs to be fixed.
    for pulsenum=1:LC_GUIDATA.ave_edit
        [stat beamout instdata] = TrackThru(1,length(BEAMLINE),beamin,1,1);
        inds = [instdata{1}.Index];
        inds = find(inds>LC_GUIDATA.ringend);
        if pulsenum==1
            pred_orig_x = [instdata{1}.x];
            pred_orig_y = [instdata{1}.y];
        end
        pred_orig_x = pred_orig_x + [instdata{1}.x];
        pred_orig_y = pred_orig_y + [instdata{1}.y];
    end
    pred_orig_x = pred_orig_x(inds) / LC_GUIDATA.ave_edit;
    pred_orig_y = pred_orig_y(inds) / LC_GUIDATA.ave_edit;
    pred_orig_S = [instdata{1}.S];
    
    % Set aside some memory for the pred-meas matrix
    if ~exist('xdiff_mat','var')
        xdiff_mat = zeros(length(cor_ps),length(orig_x));
        ydiff_mat = zeros(length(cor_ps),length(orig_x));
        xdiff_pred_mat = zeros(length(cor_ps),length(orig_x));
        ydiff_pred_mat = zeros(length(cor_ps),length(orig_x));
    end
    
    % Plot the data!
    plothand = subplot(2,2,1,'Parent',handles.measdata_panel);
    plot(plothand,orig_S,orig_x*1e3,'bx')
    xlim(plothand,[orig_S(1) orig_S(end)])
    title(plothand,[strrep(corlist{cornum},'_','\_') ' x'])
    ylabel(plothand,'x / mm')
    xlabel(plothand,'S / m')
    hold(plothand,'on')
    
    plothand = subplot(2,2,2,'Parent',handles.measdata_panel);
    plot(plothand,orig_S,orig_y*1e3,'bx')
    xlim(plothand,[orig_S(1) orig_S(end)])
    title(plothand,[strrep(corlist{cornum},'_','\_') ' y'])
    ylabel(plothand,'y / mm')
    xlabel(plothand,'S / m')
    hold(plothand,'on')
    
    plothand = subplot(2,2,1,'Parent',handles.preddata_panel);
    plot(plothand,orig_S,pred_orig_x*1e3,'bx')
    xlim(plothand,[orig_S(1) orig_S(end)])
    title(plothand,[strrep(corlist{cornum},'_','\_') ' x'])
    ylabel(plothand,'x / mm')
    xlabel(plothand,'S / m')
    hold(plothand,'on')
    
    plothand = subplot(2,2,2,'Parent',handles.preddata_panel);
    plot(plothand,orig_S,pred_orig_y*1e3,'bx')
    xlim(plothand,[orig_S(1) orig_S(end)])
    title(plothand,[strrep(corlist{cornum},'_','\_') ' y'])
    ylabel(plothand,'y / mm')
    xlabel(plothand,'S / m')
    hold(plothand,'on')
    
    % Report the status in the start button string
    set(hObject,'String',[strrep(corlist{cornum},'_','\_') ' kicked data'])
    
    % Kick the beam
    if movers
        if strcmpi(mover_plane{cornum},'x')
            plane = 1;
        elseif strcmpi(mover_plane{cornum},'y')
            plane = 3;
        else
            error('Something''s really not right :(')
        end
        for loop=1:length(GIRDER{mover_girder(cornum)}.Mover)
            if GIRDER{mover_girder(cornum)}.Mover(loop) == plane;
                ind = loop;
                break
            end
        end
        curr_moverpos = GIRDER{mover_girder(cornum)}.MoverSetPt(ind);
        GIRDER{mover_girder(cornum)}.MoverSetPt(ind) = curr_moverpos + mradkick; 
        stat = MoverTrim(mover_girder(cornum), true);
        if stat{1}~=1; error(stat{2}); end
    else
        if close_bumps && ...
                ~(strcmp(corlist{cornum},'ZV11X') || strcmp(corlist{cornum},'ZV1FF') || ...
                strcmp(corlist{cornum},'ZH10X') || strcmp(corlist{cornum},'ZH1FF'))
            stat = SetMultiKnob('knob',mradkick,true);
            if stat{1}~=1; error(stat{2}); end
        else
            curr_psval = PS(cor_ps(cornum)).SetPt;
            PS(cor_ps(cornum)).SetPt = curr_psval + mradkick;
            PSTrim(cor_ps(cornum), true);
        end
    end
    % Synch with the server
    FlHwUpdate;
    
    % Determine the pulse number of the most recent reading
    [stat output] = FlHwUpdate('readbuffer',1);
    if stat{1}~=1; error(stat{2}); end
    lastpulse = output(end,1);
    
    % Pause for enough time for the number of requested averages to occur
    pause(LC_GUIDATA.ave_edit/1.56)
    
    % Keep looping until we're sure that we will only be averaging pulses
    % from *after* the kick
    [stat output] = FlHwUpdate('readbuffer',1);
    currpulse = output(end,1);
    while currpulse-lastpulse < LC_GUIDATA.ave_edit
        pause(1)
        [stat output] = FlHwUpdate('readbuffer',1);
        if stat{1}~=1; error(stat{2}); end
        currpulse = output(end,1);
    end
    
    % Grab the data
    [stat output] = FlHwUpdate('bpmave',LC_GUIDATA.ave_edit);
    if stat{1}~=1; error(stat{2}); end
    [stat instdata] = FlTrackThru(1,length(BEAMLINE),output);
    if stat{1}~=1; error(stat{2}); end
    
    % Sort into arrays
    new_x = [instdata{1}.x]; new_x = new_x(LC_GUIDATA.firstbpm:end);
    new_y = [instdata{1}.y]; new_y = new_y(LC_GUIDATA.firstbpm:end);
    new_S = [instdata{1}.S]; new_S = new_S(LC_GUIDATA.firstbpm:end);
    
    % Synch with the server
    FlHwUpdate;
    
    % Calculate the predicted data
    for pulsenum=1:LC_GUIDATA.ave_edit
        [stat beamout instdata] = TrackThru(1,length(BEAMLINE),beamin,1,1);
        inds = [instdata{1}.Index];
        inds = find(inds>LC_GUIDATA.ringend);
        if pulsenum==1
            pred_new_x = [instdata{1}.x];
            pred_new_y = [instdata{1}.y];
        end
        pred_new_x = pred_new_x + [instdata{1}.x];
        pred_new_y = pred_new_y + [instdata{1}.y];
    end
    pred_new_x = pred_new_x(inds)/LC_GUIDATA.ave_edit;
    pred_new_y = pred_new_y(inds)/LC_GUIDATA.ave_edit;
    
    % Un-kick the beam
    if movers
        GIRDER{mover_girder(cornum)}.MoverSetPt(ind) = curr_moverpos; 
        stat = MoverTrim(mover_girder(cornum), true);
        if stat{1}~=1; error(stat{2}); end
    else
        if close_bumps && ...
                ~(strcmp(corlist{cornum},'ZV11X') || strcmp(corlist{cornum},'ZV1FF') || ...
                strcmp(corlist{cornum},'ZH10X') || strcmp(corlist{cornum},'ZH1FF'))
            stat = SetMultiKnob('knob',0,true);
            if stat{1}~=1; error(stat{2}); end
        else
            PS(cor_ps(cornum)).SetPt = curr_psval;
            PSTrim(cor_ps(cornum), true);
        end
    end
    % Synch with the server
    FlHwUpdate;
    
    % Plot it!
    plothand = subplot(2,2,1,'Parent',handles.measdata_panel);
    plot(plothand,new_S,new_x*1e3,'rx')
    hold(plothand,'off')
    
    plothand = subplot(2,2,2,'Parent',handles.measdata_panel);
    plot(plothand,new_S,new_y*1e3,'rx')
    hold(plothand,'off')
    
    plothand = subplot(2,2,1,'Parent',handles.preddata_panel);
    plot(plothand,new_S,pred_new_x*1e3,'rx')
    hold(plothand,'off')
    
    plothand = subplot(2,2,2,'Parent',handles.preddata_panel);
    plot(plothand,new_S,pred_new_y*1e3,'rx')
    hold(plothand,'off')
    
    xdiff_mat(cornum,:) = new_x - orig_x;
    ydiff_mat(cornum,:) = new_y - orig_y;
    xdiff_pred_mat(cornum,:) = pred_new_x - pred_orig_x;
    ydiff_pred_mat(cornum,:) = pred_new_y - pred_orig_y;
    
    plothand = subplot(2,2,3,'Parent',handles.measdata_panel);
    if length(corlist)>1
        surf(plothand,new_S,1:length(corlist),xdiff_mat*1e3)
        zlabel(plothand,'x / mm')
        ylabel(plothand,'Corr num')
        xlabel(plothand,'S / m')
        view(plothand,-37.5,30)
        axis(plothand,'tight')
    else
        stem(plothand,new_S,xdiff_mat*1e3,'x')
        xlabel(plothand,'S / m')
        ylabel(plothand,'x / mm')
        xlim(plothand,[new_S(1) new_S(end)])
    end
    title('Diff x')
    hold(plothand,'off')
    
    plothand = subplot(2,2,4,'Parent',handles.measdata_panel);
    if length(corlist)>1
        surf(plothand,new_S,1:length(corlist),ydiff_mat*1e3)
        view(-37.5,30)
        zlabel('y / mm')
        ylabel('Corr num')
        xlabel('S / m')
        axis(plothand,'tight')
    else
        stem(plothand,new_S,ydiff_mat*1e3,'x')
        xlabel('S / m')
        ylabel('y / mm')
        xlim([new_S(1) new_S(end)])
    end
    title('Diff y')
    hold(plothand,'off')
    
    plothand = subplot(2,2,3,'Parent',handles.preddata_panel);
    if length(corlist)>1
        surf(plothand,new_S,1:length(corlist),xdiff_pred_mat*1e3)
        view(-37.5,30)
        zlabel('x / mm')
        ylabel('Corr num')
        xlabel('S / m')
        axis(plothand,'tight')
    else
        stem(plothand,new_S,xdiff_pred_mat*1e3,'x')
        xlabel('S / m')
        ylabel('y / mm')
        xlim([new_S(1) new_S(end)])
    end
    title('Diff x')
    hold(plothand,'off')
    
    plothand = subplot(2,2,4,'Parent',handles.preddata_panel);
    if length(corlist)>1
        surf(plothand,new_S,1:length(corlist),ydiff_pred_mat*1e3)
        view(-37.5,30)
        zlabel('y / mm')
        ylabel('Corr num')
        xlabel('S / m')
        axis(plothand,'tight')
    else
        stem(plothand,new_S,ydiff_pred_mat*1e3,'x')
        xlabel('S / m')
        ylabel('y / mm')
        xlim([new_S(1) new_S(end)])
    end
    title('Diff y')
    hold(plothand,'off')
    
    plothand = subplot(1,1,1,'Parent',handles.diffdx_panel);
    if length(corlist)>1
        surf(plothand,new_S,1:length(corlist),(xdiff_pred_mat-xdiff_mat)*1e3)
        view(-37.5,30)
        zlabel('x / mm')
        ylabel('Corr num')
        xlabel('S / m')
        axis(plothand,'tight')
    else
        stem(plothand,new_S,(xdiff_pred_mat-xdiff_mat)*1e3,'x')
        xlabel('S / m')
        ylabel('x / mm')
        xlim([new_S(1) new_S(end)])
    end
    hold(plothand,'off')
    
    plothand = subplot(1,1,1,'Parent',handles.diffdy_panel);
    if length(corlist)>1
        surf(plothand,new_S,1:length(corlist),(ydiff_pred_mat-ydiff_mat)*1e3)
        view(-37.5,30)
        zlabel('y / mm')
        ylabel('Corr num')
        xlabel('S / m')
        axis(plothand,'tight')
    else
        stem(plothand,new_S,(ydiff_pred_mat-ydiff_mat)*1e3,'x')
        xlabel('S / m')
        ylabel('y / mm')
        xlim([new_S(1) new_S(end)])
    end
    hold(plothand,'off')
    
    % Note status in start button strin
    set(hObject,'String','Removing kick...')
    
    % Start the loop again to make sure the next iteration of the loop only
    % sees unkicked beam
    [stat output] = FlHwUpdate('readbuffer',1);
    if stat{1}~=1; error(stat{2}); end
    lastpulse = output(end,1);
    
    pause(LC_GUIDATA.ave_edit/1.56)
    
    currpulse = lastpulse;
    while currpulse-lastpulse < LC_GUIDATA.ave_edit
        pause(1)
        [stat output] = FlHwUpdate('readbuffer',1);
        if stat{1}~=1; error(stat{2}); end
        currpulse = output(end,1);
    end
    
    % Release the corrector access request
    AccessRequest('release');
    
    % Test for user cancellation during this run.  If so, then deactivate
    % the cancel button, and exit the main loop
    if LC_GUIDATA.iscancelled
        LC_GUIDATA.iscancelled = false;
        set(handles.cancel_button,'Enable','off')
        set(handles.cancel_button,'BackgroundColor',[0.502 0.502 0.502])
        break
    end

    % Create a "save" field in the global in case the user wants to save
    % the data.  Fill it full of data, but make sure to reorder it in S
    % order first.
    eval(['LC_GUIDATA.save.' corlist{cornum} '.orig_x = orig_x;']);
    eval(['LC_GUIDATA.save.' corlist{cornum} '.orig_y = orig_y;']);
    eval(['LC_GUIDATA.save.' corlist{cornum} '.orig_S = orig_S;']);
    eval(['LC_GUIDATA.save.' corlist{cornum} '.new_x = new_x;']);
    eval(['LC_GUIDATA.save.' corlist{cornum} '.new_y = new_y;']);
    eval(['LC_GUIDATA.save.' corlist{cornum} '.pred_orig_x = pred_orig_x;']);
    eval(['LC_GUIDATA.save.' corlist{cornum} '.pred_orig_y = pred_orig_y;']);
    eval(['LC_GUIDATA.save.' corlist{cornum} '.pred_orig_S = pred_orig_S;']);
    eval(['LC_GUIDATA.save.' corlist{cornum} '.pred_new_x = pred_new_x;']);
    eval(['LC_GUIDATA.save.' corlist{cornum} '.pred_new_y = pred_new_y;']);
    eval(['LC_GUIDATA.save.' corlist{cornum} '.xdiff = xdiff_mat(cornum,:);']);
    eval(['LC_GUIDATA.save.' corlist{cornum} '.ydiff = ydiff_mat(cornum,:);']);
    eval(['LC_GUIDATA.save.' corlist{cornum} '.xdiff_pred = xdiff_pred_mat(cornum,:);']);
    eval(['LC_GUIDATA.save.' corlist{cornum} '.ydiff_pred = ydiff_pred_mat(cornum,:);']);
    eval(['LC_GUIDATA.save.' corlist{cornum} '.kick = mradkick;']);
    eval(['LC_GUIDATA.save.' corlist{cornum} '.movers = movers;']);
    eval(['LC_GUIDATA.save.' corlist{cornum} '.close_bumps = close_bumps;']);
end

% Put the data into the main global variable
LC_GUIDATA.diffdx = xdiff_pred_mat-xdiff_mat;
LC_GUIDATA.diffdy = ydiff_pred_mat-ydiff_mat;
LC_GUIDATA.corlist = corlist;

% Reset the start button
set(hObject,'BackgroundColor',[0 1 0])
set(hObject,'String','START!')

% Switch on all the front panel buttons
set(hObject,'Enable','on')
set(handles.plot_pushbutton,'Enable','on')
set(handles.save_button,'Enable','on')
set(handles.load_button,'Enable','on')
set(handles.region_menu,'Enable','on')
set(handles.magselect_menu,'Enable','on')
set(handles.devicetype_menu,'Enable','on')
set(handles.ave_edit,'Enable','on')
set(handles.kicksize_edit,'Enable','on')

% Disable the cancel button
set(handles.cancel_button,'Enable','off')
set(handles.cancel_button,'BackgroundColor',[0.502 0.502 0.502])


% --- Executes on selection change in region_menu.
function region_menu_Callback(hObject, eventdata, handles) %#ok<*INUSL>
% hObject    handle to region_menu (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = get(hObject,'String') returns region_menu contents as cell array
%        contents{get(hObject,'Value')} returns selected item from region_menu

global BEAMLINE LC_GUIDATA

% Get the contents of the magselect list
mag_contents = get(handles.magselect_menu,'String');
mag_choice = mag_contents{get(handles.magselect_menu,'Value')};

% Get the contents of the device-type list
devtype_contents = get(handles.devicetype_menu,'String');
devtype_choice = devtype_contents{get(handles.devicetype_menu,'Value')};

% Get a list of the xcors and remove a few from this list
xcors_temp = LC_GUIDATA.xcorinds;
xcors = [];
for counter=1:length(xcors_temp)
    if ~(xcors_temp(counter)==1257 || ...
            xcors_temp(counter)==1261 || ...
            xcors_temp(counter)==1267)
        xcors = [xcors xcors_temp(counter)]; %#ok<AGROW>
    end
end
% Get a list of the ycors
ycors = LC_GUIDATA.ycorinds;

% Get the user selection for the region list
region_contents = get(handles.region_menu,'String');
region_choice = region_contents{get(handles.region_menu,'Value')};

% Determine the corrector/mover list based on this selection
if strcmpi(region_choice,'Extraction Line')
    begin = LC_GUIDATA.ringend;
    finish = LC_GUIDATA.extff;
    
elseif strcmpi(region_choice,'Final Focus')
    begin = LC_GUIDATA.extff;
    finish = length(BEAMLINE);
    
elseif strcmpi(region_choice,'All')
    begin = LC_GUIDATA.ringend;
    finish = length(BEAMLINE);
    
else
    error('Something''s gone terribly wrong :S')
end

xcors = xcors(xcors>begin & xcors<finish);
ycors = ycors(ycors>begin & ycors<finish);
% Grab the lists of mover names
count = 0;
for mover_num=1:length(LC_GUIDATA.xmover_S)
    if (LC_GUIDATA.xmover_S{mover_num}>BEAMLINE{begin}.S && ...
            LC_GUIDATA.xmover_S{mover_num}<BEAMLINE{finish}.S)
        count = count + 1;
        xmovername{count} = LC_GUIDATA.xmover_name{mover_num}; %#ok<AGROW>
    end
end
count = 0;
for mover_num=1:length(LC_GUIDATA.ymover_S)
    if (LC_GUIDATA.ymover_S{mover_num}>BEAMLINE{begin}.S && ...
            LC_GUIDATA.ymover_S{mover_num}<BEAMLINE{finish}.S)
        count = count + 1;
        ymovername{count} = LC_GUIDATA.ymover_name{mover_num}; %#ok<AGROW>
    end
end

% Generate a list of xcor and ycor names from their indices
xcorname = cell(1,length(xcors));
ycorname = cell(1,length(ycors));
for cornum=1:length(xcors)
    xcorname{cornum} = BEAMLINE{xcors(cornum)}.Name;
end
for cornum=1:length(ycors)
    ycorname{cornum} = BEAMLINE{ycors(cornum)}.Name;
end

% Set the contents of the corrector list based on the user's selection.
% Ask for further input in the case of "user choice"
if strcmpi(devtype_choice,'correctors')
    if strcmpi(mag_choice,'User choice (single)')
        magnets = [xcorname ycorname];
        [selection ok] = listdlg('ListString',magnets,...
            'SelectionMode','single','Name','Corrector',...
            'PromptString','Choose a corrector to scan');
        if ok==0
            return
        end
        
        set(handles.maglistbox,'String',magnets{selection})
        
    elseif strcmpi(mag_choice,'User choice (multiple)')
        magnets = [xcorname ycorname];
        [selection ok] = listdlg('ListString',magnets,...
            'SelectionMode','multiple','Name','Corrector',...
            'PromptString','Choose a corrector to scan');
        if ok==0
            return
        end
        
        for count=1:length(selection)
            maglist{count} = magnets{selection(count)}; %#ok<AGROW>
        end
        
        set(handles.maglistbox,'String',maglist)
        
    elseif strcmpi(mag_choice,'All')
        if strcmpi(region_choice,'Extraction line') || strcmpi(region_choice,'Final Focus')
            maglist = [xcorname(1:end-1) ycorname(1:end-1)];
        else
            maglist = [xcorname(1:end-2) ycorname(1:end-2)];
        end
        set(handles.maglistbox,'String',maglist)
        
    elseif strcmpi(mag_choice,'All (x)')
        if strcmpi(region_choice,'Extraction line') || strcmpi(region_choice,'Final Focus')
            maglist = xcorname(1:end-1);
        else
            maglist = xcorname(1:end-2);
        end
        set(handles.maglistbox,'String',maglist)
        
    elseif strcmpi(mag_choice,'All (y)')
        if strcmpi(region_choice,'Extraction line') || strcmpi(region_choice,'Final Focus')
            maglist = ycorname(1:end-1);
        else
            maglist = ycorname(1:end-2);
        end
        set(handles.maglistbox,'String',maglist)
        
    else
        error('Something''s gone terribly wrong :S')
    end
    
elseif strcmpi(devtype_choice,'movers')
    if strcmpi(mag_choice,'User choice (single)')
        magnets = [xmovername ymovername];
        [selection ok] = listdlg('ListString',magnets,...
            'SelectionMode','single','Name','Mover',...
            'PromptString','Choose a mover to scan');
        if ok==0
            return
        end
        
        set(handles.maglistbox,'String',magnets{selection})
        
    elseif strcmpi(mag_choice,'User choice (multiple)')
        magnets = [xmovername ymovername];
        [selection ok] = listdlg('ListString',magnets,...
            'SelectionMode','multiple','Name','Mover',...
            'PromptString','Choose a mover to scan');
        if ok==0
            return
        end
        
        for count=1:length(selection)
            maglist{count} = magnets{selection(count)}; %#ok<AGROW>
        end
        
        set(handles.maglistbox,'String',maglist)
        
    elseif strcmpi(mag_choice,'All')
        if ~exist('xmovername','var') || ~exist('ymovername','var')
            maglist{1} = 'None';
        elseif ~exist('xmovername','var')
            maglist = ymovername;
        elseif ~exist('xmovername','var')
            maglist = xmovername;
        else
            maglist = [xmovername ymovername];
        end
        set(handles.maglistbox,'String',maglist)
        
    elseif strcmpi(mag_choice,'All (x)')
        if ~exist('xmovername','var')
            set(handles.maglistbox,'String','None')
        else
            set(handles.maglistbox,'String',xmovername)
        end
        
    elseif strcmpi(mag_choice,'All (y)')
        if ~exist('ymovername','var')
            set(handles.maglistbox,'String','None')
        else
            set(handles.maglistbox,'String',ymovername)
        end
        
    else
        error('Something''s gone terribly wrong :S')
    end
        
else
    error('Something''s gone terribly wrong :S')
    
end


% --- Executes during object creation, after setting all properties.
function region_menu_CreateFcn(hObject, eventdata, handles) 
% hObject    handle to region_menu (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in magselect_menu.
function magselect_menu_Callback(hObject, eventdata, handles) 
% hObject    handle to magselect_menu (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = get(hObject,'String') returns magselect_menu contents as cell array
%        contents{get(hObject,'Value')} returns selected item from magselect_menu

global BEAMLINE LC_GUIDATA

% Get the contents of the magselect list
mag_contents = get(handles.magselect_menu,'String');
mag_choice = mag_contents{get(handles.magselect_menu,'Value')};

% Get the contents of the device-type list
devtype_contents = get(handles.devicetype_menu,'String');
devtype_choice = devtype_contents{get(handles.devicetype_menu,'Value')};

% Get a list of the xcors and remove a few from this list
xcors_temp = LC_GUIDATA.xcorinds;
xcors = [];
for counter=1:length(xcors_temp)
    if ~(xcors_temp(counter)==1257 || ...
            xcors_temp(counter)==1261 || ...
            xcors_temp(counter)==1267)
        xcors = [xcors xcors_temp(counter)]; %#ok<AGROW>
    end
end
% Get a list of the ycors
ycors = LC_GUIDATA.ycorinds;

% Get the user selection for the region list
region_contents = get(handles.region_menu,'String');
region_choice = region_contents{get(handles.region_menu,'Value')};

% Determine the corrector/mover list based on this selection
if strcmpi(region_choice,'Extraction Line')
    begin = LC_GUIDATA.ringend;
    finish = LC_GUIDATA.extff;
    
elseif strcmpi(region_choice,'Final Focus')
    begin = LC_GUIDATA.extff;
    finish = length(BEAMLINE);
    
elseif strcmpi(region_choice,'All')
    begin = LC_GUIDATA.ringend;
    finish = length(BEAMLINE);
    
else
    error('Something''s gone terribly wrong :S')
end

xcors = xcors(xcors>begin & xcors<finish);
ycors = ycors(ycors>begin & ycors<finish);
% Grab the lists of mover names
count = 0;
for mover_num=1:length(LC_GUIDATA.xmover_S)
    if (LC_GUIDATA.xmover_S{mover_num}>BEAMLINE{begin}.S && ...
            LC_GUIDATA.xmover_S{mover_num}<BEAMLINE{finish}.S)
        count = count + 1;
        xmovername{count} = LC_GUIDATA.xmover_name{mover_num}; %#ok<AGROW>
    end
end
count = 0;
for mover_num=1:length(LC_GUIDATA.ymover_S)
    if (LC_GUIDATA.ymover_S{mover_num}>BEAMLINE{begin}.S && ...
            LC_GUIDATA.ymover_S{mover_num}<BEAMLINE{finish}.S)
        count = count + 1;
        ymovername{count} = LC_GUIDATA.ymover_name{mover_num}; %#ok<AGROW>
    end
end

% Generate a list of xcor and ycor names from their indices
xcorname = cell(1,length(xcors));
ycorname = cell(1,length(ycors));
for cornum=1:length(xcors)
    xcorname{cornum} = BEAMLINE{xcors(cornum)}.Name;
end
for cornum=1:length(ycors)
    ycorname{cornum} = BEAMLINE{ycors(cornum)}.Name;
end

% Set the contents of the corrector list based on the user's selection.
% Ask for further input in the case of "user choice"
if strcmpi(devtype_choice,'correctors')
    if strcmpi(mag_choice,'User choice (single)')
        magnets = [xcorname ycorname];
        [selection ok] = listdlg('ListString',magnets,...
            'SelectionMode','single','Name','Corrector',...
            'PromptString','Choose a corrector to scan');
        if ok==0
            return
        end
        
        set(handles.maglistbox,'String',magnets{selection})
        
    elseif strcmpi(mag_choice,'User choice (multiple)')
        magnets = [xcorname ycorname];
        [selection ok] = listdlg('ListString',magnets,...
            'SelectionMode','multiple','Name','Corrector',...
            'PromptString','Choose a corrector to scan');
        if ok==0
            return
        end
        
        for count=1:length(selection)
            maglist{count} = magnets{selection(count)}; %#ok<AGROW>
        end
        
        set(handles.maglistbox,'String',maglist)
        
    elseif strcmpi(mag_choice,'All')
        if strcmpi(region_choice,'Extraction line') || strcmpi(region_choice,'Final Focus')
            maglist = [xcorname(1:end-1) ycorname(1:end-1)];
        else
            maglist = [xcorname(1:end-2) ycorname(1:end-2)];
        end
        set(handles.maglistbox,'String',maglist)
        
    elseif strcmpi(mag_choice,'All (x)')
        if strcmpi(region_choice,'Extraction line') || strcmpi(region_choice,'Final Focus')
            maglist = xcorname(1:end-1);
        else
            maglist = xcorname(1:end-2);
        end
        set(handles.maglistbox,'String',maglist)
        
    elseif strcmpi(mag_choice,'All (y)')
        if strcmpi(region_choice,'Extraction line') || strcmpi(region_choice,'Final Focus')
            maglist = ycorname(1:end-1);
        else
            maglist = ycorname(1:end-2);
        end
        set(handles.maglistbox,'String',maglist)
        
    else
        error('Something''s gone terribly wrong :S')
    end
    
elseif strcmpi(devtype_choice,'movers')
    if strcmpi(mag_choice,'User choice (single)')
        magnets = [xmovername ymovername];
        [selection ok] = listdlg('ListString',magnets,...
            'SelectionMode','single','Name','Mover',...
            'PromptString','Choose a mover to scan');
        if ok==0
            return
        end
        
        set(handles.maglistbox,'String',magnets{selection})
        
    elseif strcmpi(mag_choice,'User choice (multiple)')
        magnets = [xmovername ymovername];
        [selection ok] = listdlg('ListString',magnets,...
            'SelectionMode','multiple','Name','Mover',...
            'PromptString','Choose a mover to scan');
        if ok==0
            return
        end
        
        for count=1:length(selection)
            maglist{count} = magnets{selection(count)}; %#ok<AGROW>
        end
        
        set(handles.maglistbox,'String',maglist)
        
    elseif strcmpi(mag_choice,'All')
        if ~exist('xmovername','var') || ~exist('ymovername','var')
            maglist{1} = 'None';
        elseif ~exist('xmovername','var')
            maglist = ymovername;
        elseif ~exist('xmovername','var')
            maglist = xmovername;
        else
            maglist = [xmovername ymovername];
        end
        set(handles.maglistbox,'String',maglist)
        
    elseif strcmpi(mag_choice,'All (x)')
        if ~exist('xmovername','var')
            set(handles.maglistbox,'String','None')
        else
            set(handles.maglistbox,'String',xmovername)
        end
        
    elseif strcmpi(mag_choice,'All (y)')
        if ~exist('ymovername','var')
            set(handles.maglistbox,'String','None')
        else
            set(handles.maglistbox,'String',ymovername)
        end
        
    else
        error('Something''s gone terribly wrong :S')
    end
        
else
    error('Something''s gone terribly wrong :S')
    
end


% --- Executes during object creation, after setting all properties.
function magselect_menu_CreateFcn(hObject, eventdata, handles) %#ok<*INUSD>
% hObject    handle to magselect_menu (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function kicksize_edit_Callback(hObject, eventdata, handles)
% hObject    handle to kicksize_edit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of kicksize_edit as text
%        str2double(get(hObject,'String')) returns contents of kicksize_edit as a double

global LC_GUIDATA

% Check that the input is acceptable, and, if so, set the global
if ~isnan(str2double(get(hObject,'String')))
    input = str2double(get(hObject,'String'));
    
    devtype_contents = get(handles.devicetype_menu,'String');
    devtype_choice = devtype_contents{get(handles.devicetype_menu,'Value')};
    
    if strcmpi(devtype_choice,'movers')
        LC_GUIDATA.moversize = input;
    elseif strcmpi(devtype_choice,'correctors')
        LC_GUIDATA.kicksize = input;
    end
end



% --- Executes during object creation, after setting all properties.
function kicksize_edit_CreateFcn(hObject, eventdata, handles) 
% hObject    handle to kicksize_edit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in maglistbox.
function maglistbox_Callback(hObject, eventdata, handles)
% hObject    handle to maglistbox (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = get(hObject,'String') returns maglistbox contents as cell array
%        contents{get(hObject,'Value')} returns selected item from maglistbox


% --- Executes during object creation, after setting all properties.
function maglistbox_CreateFcn(hObject, eventdata, handles) 
% hObject    handle to maglistbox (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: listbox controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function ave_edit_Callback(hObject, eventdata, handles)
% hObject    handle to ave_edit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of ave_edit as text
%        str2double(get(hObject,'String')) returns contents of ave_edit as a double

global LC_GUIDATA

% Check to see if the input is acceptable, and, if so, set the global
if ~isnan(str2double(get(hObject,'String')))
    LC_GUIDATA.ave_edit = str2double(get(hObject,'String'));
else
    set(hObject,'String',num2str(LC_GUIDATA.ave_edit))
end


% --- Executes during object creation, after setting all properties.
function ave_edit_CreateFcn(hObject, eventdata, handles) 
% hObject    handle to ave_edit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes during object deletion, before destroying properties.
function figure1_DeleteFcn(hObject, eventdata, handles) %#ok<*DEFNU>
% hObject    handle to figure1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

clear global LC_GUIDATA


% --- Executes on button press in plot_pushbutton.
function plot_pushbutton_Callback(hObject, eventdata, handles)
% hObject    handle to plot_pushbutton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

global LC_GUIDATA

% Check to see if data has been collected, if so, generate a figure and
% plot the data
if isfield(LC_GUIDATA,'diffdx')
    figure
    if length(LC_GUIDATA.corlist)>1
        surf(LC_GUIDATA.bpmS,1:length(LC_GUIDATA.corlist),LC_GUIDATA.diffdx*1e3)
        zlabel('x / mm')
        ylabel('Corr num')
        xlabel('S / m')
        title('Pred - Meas (x)')
    else
        stem(LC_GUIDATA.bpmS,LC_GUIDATA.diffdx*1e3,'x')
        xlabel('S / m')
        ylabel('x / mm')
        title(['Pred - Meas (x) (' LC_GUIDATA.corlist{1} ')'])
    end
end

% Check to see if data has been collected, if so, generate a figure and
% plot the data
if isfield(LC_GUIDATA,'diffdy')
    figure
    if length(LC_GUIDATA.corlist)>1
        surf(LC_GUIDATA.bpmS,1:length(LC_GUIDATA.corlist),LC_GUIDATA.diffdy*1e3)
        zlabel('y / mm')
        ylabel('Corr num')
        xlabel('S / m')
        title('Pred - Meas (y)')
    else
        stem(LC_GUIDATA.bpmS,LC_GUIDATA.diffdy*1e3,'x')
        xlabel('S / m')
        ylabel('y / mm')
        title(['Pred - Meas (y) (' LC_GUIDATA.corlist{1} ')'])
    end
end


% --- Executes on button press in cancel_button.
function cancel_button_Callback(hObject, eventdata, handles)
% hObject    handle to cancel_button (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

global LC_GUIDATA

% If the user hits the cancel button, set the global for the start_button
% callback to see.
LC_GUIDATA.iscancelled = true;

% It won't cancel immediately since it has to clear up after itself, so let
% the user know that their request has been acknowledged.
msgbox('Will attempt to terminate this run after this corrector measurement has completed',...
    'Canceling the run','warn')


% --- Executes on selection change in devicetype_menu.
function devicetype_menu_Callback(hObject, eventdata, handles)
% hObject    handle to devicetype_menu (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = get(hObject,'String') returns devicetype_menu contents as cell array
%        contents{get(hObject,'Value')} returns selected item from
%        devicetype_menu

global BEAMLINE LC_GUIDATA

% Get the contents of the magselect list
mag_contents = get(handles.magselect_menu,'String');
mag_choice = mag_contents{get(handles.magselect_menu,'Value')};

% Get the contents of the device-type list
devtype_contents = get(handles.devicetype_menu,'String');
devtype_choice = devtype_contents{get(handles.devicetype_menu,'Value')};

% Get a list of the xcors and remove a few from this list
xcors_temp = LC_GUIDATA.xcorinds;
xcors = [];
for counter=1:length(xcors_temp)
    if ~(xcors_temp(counter)==1257 || ...
            xcors_temp(counter)==1261 || ...
            xcors_temp(counter)==1267)
        xcors = [xcors xcors_temp(counter)]; %#ok<AGROW>
    end
end
% Get a list of the ycors
ycors = LC_GUIDATA.ycorinds;

% Get the user selection for the region list
region_contents = get(handles.region_menu,'String');
region_choice = region_contents{get(handles.region_menu,'Value')};

% Determine the corrector/mover list based on this selection
if strcmpi(region_choice,'Extraction Line')
    begin = LC_GUIDATA.ringend;
    finish = LC_GUIDATA.extff;
    
elseif strcmpi(region_choice,'Final Focus')
    begin = LC_GUIDATA.extff;
    finish = length(BEAMLINE);
    
elseif strcmpi(region_choice,'All')
    begin = LC_GUIDATA.ringend;
    finish = length(BEAMLINE);
    
else
    error('Something''s gone terribly wrong :S')
end

xcors = xcors(xcors>begin & xcors<finish);
ycors = ycors(ycors>begin & ycors<finish);
% Grab the lists of mover names
count = 0;
for mover_num=1:length(LC_GUIDATA.xmover_S)
    if (LC_GUIDATA.xmover_S{mover_num}>BEAMLINE{begin}.S && ...
            LC_GUIDATA.xmover_S{mover_num}<BEAMLINE{finish}.S)
        count = count + 1;
        xmovername{count} = LC_GUIDATA.xmover_name{mover_num}; %#ok<AGROW>
    end
end
count = 0;
for mover_num=1:length(LC_GUIDATA.ymover_S)
    if (LC_GUIDATA.ymover_S{mover_num}>BEAMLINE{begin}.S && ...
            LC_GUIDATA.ymover_S{mover_num}<BEAMLINE{finish}.S)
        count = count + 1;
        ymovername{count} = LC_GUIDATA.ymover_name{mover_num}; %#ok<AGROW>
    end
end

% Generate a list of xcor and ycor names from their indices
xcorname = cell(1,length(xcors));
ycorname = cell(1,length(ycors));
for cornum=1:length(xcors)
    xcorname{cornum} = BEAMLINE{xcors(cornum)}.Name;
end
for cornum=1:length(ycors)
    ycorname{cornum} = BEAMLINE{ycors(cornum)}.Name;
end

% Set the contents of the corrector list based on the user's selection.
% Ask for further input in the case of "user choice"
if strcmpi(devtype_choice,'correctors')
    if strcmpi(mag_choice,'User choice (single)')
        magnets = [xcorname ycorname];
        [selection ok] = listdlg('ListString',magnets,...
            'SelectionMode','single','Name','Corrector',...
            'PromptString','Choose a corrector to scan');
        if ok==0
            return
        end
        
        set(handles.maglistbox,'String',magnets{selection})
        
    elseif strcmpi(mag_choice,'User choice (multiple)')
        magnets = [xcorname ycorname];
        [selection ok] = listdlg('ListString',magnets,...
            'SelectionMode','multiple','Name','Corrector',...
            'PromptString','Choose a corrector to scan');
        if ok==0
            return
        end
        
        for count=1:length(selection)
            maglist{count} = magnets{selection(count)}; %#ok<AGROW>
        end
        
        set(handles.maglistbox,'String',maglist)
        
    elseif strcmpi(mag_choice,'All')
        if strcmpi(region_choice,'Extraction line') || strcmpi(region_choice,'Final Focus')
            maglist = [xcorname(1:end-1) ycorname(1:end-1)];
        else
            maglist = [xcorname(1:end-2) ycorname(1:end-2)];
        end
        set(handles.maglistbox,'String',maglist)
        
    elseif strcmpi(mag_choice,'All (x)')
        if strcmpi(region_choice,'Extraction line') || strcmpi(region_choice,'Final Focus')
            maglist = xcorname(1:end-1);
        else
            maglist = xcorname(1:end-2);
        end
        set(handles.maglistbox,'String',maglist)
        
    elseif strcmpi(mag_choice,'All (y)')
        if strcmpi(region_choice,'Extraction line') || strcmpi(region_choice,'Final Focus')
            maglist = ycorname(1:end-1);
        else
            maglist = ycorname(1:end-2);
        end
        set(handles.maglistbox,'String',maglist)
        
    else
        error('Something''s gone terribly wrong :S')
    end
    
elseif strcmpi(devtype_choice,'movers')
    if strcmpi(mag_choice,'User choice (single)')
        magnets = [xmovername ymovername];
        [selection ok] = listdlg('ListString',magnets,...
            'SelectionMode','single','Name','Mover',...
            'PromptString','Choose a mover to scan');
        if ok==0
            return
        end
        
        set(handles.maglistbox,'String',magnets{selection})
        
    elseif strcmpi(mag_choice,'User choice (multiple)')
        if ~exist('xmovername','var') || ~exist('ymovername','var')
            magnets = {'None'};
        elseif ~exist('xmovername','var')
            magnets = ymovername;
        elseif ~exist('xmovername','var')
            magnets = xmovername;
        else
            magnets = [xmovername ymovername];
        end
        [selection ok] = listdlg('ListString',magnets,...
            'SelectionMode','multiple','Name','Mover',...
            'PromptString','Choose a mover to scan');
        if ok==0
            return
        end
        
        for count=1:length(selection)
            maglist{count} = magnets{selection(count)}; %#ok<AGROW>
        end
        
        set(handles.maglistbox,'String',maglist)
        
    elseif strcmpi(mag_choice,'All')
        if ~exist('xmovername','var') || ~exist('ymovername','var')
            maglist{1} = 'None';
        elseif ~exist('xmovername','var')
            maglist = ymovername;
        elseif ~exist('xmovername','var')
            maglist = xmovername;
        else
            maglist = [xmovername ymovername];
        end
        set(handles.maglistbox,'String',maglist)
        
    elseif strcmpi(mag_choice,'All (x)')
        if ~exist('xmovername','var')
            set(handles.maglistbox,'String','None')
        else
            set(handles.maglistbox,'String',xmovername)
        end
        
    elseif strcmpi(mag_choice,'All (y)')
        if ~exist('ymovername','var')
            set(handles.maglistbox,'String','None')
        else
            set(handles.maglistbox,'String',ymovername)
        end
        
    else
        error('Something''s gone terribly wrong :S')
    end
        
else
    error('Something''s gone terribly wrong :S')
    
end

if strcmpi(devtype_choice,'movers')
    set(handles.kick_panel,'String','Mover move / mm')
    set(handles.kicksize_edit,'String',LC_GUIDATA.moversize)
else
    set(handles.kick_panel,'String','Kick size / mrad')
    set(handles.kicksize_edit,'String',LC_GUIDATA.kicksize)
end


% --- Executes during object creation, after setting all properties.
function devicetype_menu_CreateFcn(hObject, eventdata, handles)
% hObject    handle to devicetype_menu (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in save_button.
function save_button_Callback(hObject, eventdata, handles)
% hObject    handle to save_button (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

global LC_GUIDATA

if ~isfield(LC_GUIDATA,'save')
    uiwait(msgbox('No data to save','No data','warn'))
    return
end

filename = uiputfile(...
    '~/Lucretia/src/Floodland/testApps/lattice_check/*.mat',...
    'Save data');

if filename==0
    return
end

data = LC_GUIDATA.save;
fnames = fieldnames(data);

for loop=1:length(fnames)
    eval([fnames{loop} ' = data.' fnames{loop} ';']);
    if loop==1
        save(['~/Lucretia/src/Floodland/testApps/lattice_check/' filename],fnames{loop})
    else
        save(['~/Lucretia/src/Floodland/testApps/lattice_check/' filename],fnames{loop},'-append')
    end
end


% --- Executes on button press in load_button.
function load_button_Callback(hObject, eventdata, handles)
% hObject    handle to load_button (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

global LC_GUIDATA %#ok<NUSED>

filename = uigetfile(...
    '~/Lucretia/src/Floodland/testApps/lattice_check/*.mat',...
    'Load data');

if filename==0
    return
end

load(['~/Lucretia/src/Floodland/testApps/lattice_check/' filename]);
fnames = fieldnames(data);

for loop=1:length(fnames)
    eval(['LC_GUIDATA.save.' fnames{loop} ' = data.' fnames{loop} ';']);
end


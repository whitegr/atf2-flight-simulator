function [mover_oldread]=set_mover(target,mover_newvalue)
%SET_Mover

    global BEAMLINE GIRDER;

    min_step=1e-6;
    if (ischar(target))
        for i=1:getcolumn(size(target),1)
            girder(i)=BEAMLINE{getcolumn(findcells(BEAMLINE,'Name',target(i,:)),1)}.Girder;
        end
    else
        girder=target;
    end
    
    %check input args
    ngirder=getcolumn(size(girder),2);
    if(ngirder~=getcolumn(size(mover_newvalue),1))
        disp('ERROR : the array girder and the array new value must have the same size');
        return
    end

    mover_oldread=zeros(ngirder,3);
    FlHwUpdate;
    for i=1:ngirder
        mover_oldread(i,:)=GIRDER{girder(i)}.MoverPos;
    end
    rangex=find(abs(mover_oldread(:,1) - mover_newvalue(:,1))>min_step);
    rangey=find(abs(mover_oldread(:,2) - mover_newvalue(:,2))>min_step);
    rangetilt=find(abs(mover_oldread(:,3) - mover_newvalue(:,3))>min_step);
    range=sort(unique([rangex; rangey; rangetilt]));
    girder=girder(range);
    mover_newvalue=mover_newvalue(range,:);
    ngirder=getcolumn(size(girder),2);
    
    if (ngirder==0)
        disp('No change > %fum required !',min_step*1e6);
        return;
    end
    
    request{1}=girder;
    request{2}=[];
    request{3}=[];
    % request{1}(...) = read+write privs requested for mover on GIRDER{...}
    % request{2}(...) = read+write privs requested for power supply PS(...)
    % request{3}(...) = read privs requested for instrument INSTR{...}
    
    %ask for acces
    [stat reqID] = AccessRequest(request);
    if(stat{1}==-1)
        disp(sprintf('AccessRequest reurned error :%s',stat{2}));
    else
        disp(sprintf('Access granted, ID of request : %i',reqID));
    end

    for i=1:ngirder
        GIRDER{girder(i)}.MoverSetPt(1:3)=mover_newvalue(i,1:3);
    end

    %ask to change mover value
    stat=MoverTrim(girder, 3);
    if(stat{1}==-1)
        disp(sprintf('MoverTrim returned error :%s',stat{2}));
    end
    pause(1);
    
    %update mover value
    FlHwUpdate(request);
    mover_newread=zeros(ngirder,3);
    for i=1:ngirder
        mover_newread(i,:)=GIRDER{girder(i)}.MoverPos;
    end

    %if more than 5um difference, ask again
    itteration=0;
    [bad_mover,bad_dim]=find(abs(mover_newvalue-mover_newread)>=5e-6);
%    while( ~isempty( bad_mover ) )
     while( 0 )
        itteration=itteration+1;
        %give up after 5 asks
        if(itteration>5)
            orientation={'x';'y';'roll'};
            for i=bad_mover
               disp(sprintf('Warning : %s of GIRDER %i is set at %f instead of %f.\n',orientation{i},girder(i),mover_newread(i,bad_dim(i)),mover_newvalue(i,bad_dim(i))));
            end
            break;
        end

        %ask to change different mover from what is wanted
        stat=MoverTrim(girder(bad_mover), 3);
        if(stat{1}==-1)
            disp(sprintf('MoverTrim reurned error :%s',stat{2}));
        end
        pause(1);
        
        %update mover values
        request{1}=girder;
        FlHwUpdate(request);
        for i=1:ngirder
            mover_newread(i,:)=GIRDER{girder(i)}.MoverPos;
        end
        [bad_mover,bad_dim]=find(abs(mover_newvalue-mover_newread)>=5e-6);
    end
    
    %release acces
    AccessRequest('release',reqID);
    if(stat{1}==-1)
        disp(sprintf('Error durring access release, reurned error :%s',stat{2}));
    else
        disp(sprintf('Access %i released',reqID));
    end
end

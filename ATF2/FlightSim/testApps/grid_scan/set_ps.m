function [ps_oldread]=set_ps(target,ps_newvalue,min_step)
%SET_PS Summary of this function goes here
%   Detailed explanation goes here

    global BEAMLINE PS;

    if ~exist('min_step','var')
%        min_step=10e-6; %10mrad
        min_step=1e-6;
    end
    ntarget=size(target,1);
    ps=zeros(1,ntarget);
    if (ischar(target))
        for i=1:ntarget
            ps(i)=BEAMLINE{findcells(BEAMLINE,'Name',target(i,:))}.PS;
        end
    else
        ps=target;
    end
    
    %check input args
    nps=getcolumn(size(ps),2);
    if(nps~=getcolumn(size(ps_newvalue),2))
        disp('ERROR : the array PS and the array new value must have the same size');
        return
    end

    ps_oldread=zeros(1,nps);
    FlHwUpdate;
    for i=1:nps
        ps_oldread(i)=PS(ps(i)).Ampl;
    end
    range=find(abs(ps_oldread - ps_newvalue)>min_step);
    ps=ps(range);
    ps_newvalue=ps_newvalue(range);
    nps=getcolumn(size(ps),2);
    
    if (nps==0)
        fprintf('No change > %furad required !\n',min_step*1e6);
        return;
    end    
    
    request{1}=[];
    request{2}=ps;
    request{3}=[];
    % request{1}(...) = read+write privs requested for mover on GIRDER{...}
    % request{2}(...) = read+write privs requested for power supply PS(...)
    % request{3}(...) = read privs requested for instrument INSTR{...}

    %ask for acces
    [stat reqID] = AccessRequest(request);
    if(stat{1}==-1)
        fprintf('AccessRequest reurned error :%s\n',stat{2});
    end

    for i=1:nps
        PS(ps(i)).SetPt=ps_newvalue(i);
    end

    %ask to change ps value
    stat=PSTrim(ps, true);
    if(stat{1}==-1)
        fprintf('PSTrim returned error :%s\n',stat{2});
    end
    pause(1);
    
    %update ps value
    FlHwUpdate(request);
    ps_newread=zeros(1,nps);
    for i=1:nps
        ps_newread(i)=PS(ps(i)).Ampl;
    end

    %if more than min_step difference, ask again
    itteration=0;
    bad_ps=find(abs(ps_newvalue-ps_newread)>=min_step,1);
    while( ~isempty( bad_ps ) )
        itteration=itteration+1;
        %give up after 5 asks
        if(itteration>5)
            fprintf('Warning : PS %i is set at %f instead of %f.\n',bad_ps,ps_newread(bad_ps),ps_newvalue(bad_ps) );
            break;
        end

        %ask to change different ps from what is wanted
        stat=PSTrim(ps(bad_ps), true);
        if(stat{1}==-1)
            fprintf('PSTrim reurned error :%s\n',stat{2});
        end
        pause(1);
        
        %update ps values
        request{2}=ps;
        FlHwUpdate(request);
        for i=1:nps
            ps_newread(i)=PS(ps(i)).Ampl;
        end
        bad_ps=find(abs(ps_newvalue-ps_newread)>=min_step,1);
    end
    
    %release acces
    AccessRequest('release',reqID);
    if(stat{1}==-1)
        fprintf('Error durring access release, reurned error :%s\n',stat{2});
    end
end

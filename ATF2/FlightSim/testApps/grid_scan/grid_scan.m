function varargout = grid_scan(varargin)
% GRID_SCAN MATLAB code for grid_scan.fig
%      GRID_SCAN, by itself, creates a new GRID_SCAN or raises the existing
%      singleton*.
%
%      H = GRID_SCAN returns the handle to a new GRID_SCAN or the handle to
%      the existing singleton*.
%
%      GRID_SCAN('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in GRID_SCAN.M with the given input arguments.
%
%      GRID_SCAN('Property','Value',...) creates a new GRID_SCAN or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before grid_scan_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to grid_scan_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help grid_scan

% Last Modified by GUIDE v2.5 12-Dec-2011 17:05:29

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @grid_scan_OpeningFcn, ...
                   'gui_OutputFcn',  @grid_scan_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin & ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT

function handles=initialize(handles)
% Update handles structure
global BEAMLINE
global INSTR
global PS
global GIRDER

%get bpm info
handles.IEX=findcells(BEAMLINE,'Name','IEX');
handles.BS1XA=findcells(BEAMLINE,'Name','BS1XA');
handles.bpm=setdiff(findcells(BEAMLINE,'Class','MONI'),findcells(BEAMLINE,'Name','FONT*')); % all MONIs except FONT pickups ...
handles.bpm=handles.bpm(handles.bpm>handles.IEX); % ... downstream of IEX
handles.nbpm=length(handles.bpm);
handles.bpm_name=cellfun(@(x) x.Name,BEAMLINE(handles.bpm),'UniformOutput',false);
handles.bpm_s=cellfun(@(x) x.S,BEAMLINE(handles.bpm),'UniformOutput',true);
handles.bpm_index=zeros(size(handles.bpm));
for n=1:length(handles.bpm)
  handles.bpm_index(n)=findcells(INSTR,'Index',handles.bpm(n));
end
handles.bpm_type=cellfun(@(x) x.Type,INSTR(handles.bpm_index),'UniformOutput',false);
%buttons
handles.button=strmatch('button',handles.bpm_type);
handles.nbutton=length(handles.button);
handles.button_name=handles.bpm_name(handles.button);
handles.button_index=handles.bpm_index(handles.button);
%striplines
handles.stripline=strmatch('stripline',handles.bpm_type);
handles.nstripline=length(handles.stripline);
handles.stripline_name=handles.bpm_name(handles.stripline);
handles.stripline_index=handles.bpm_index(handles.stripline);
%cbands
handles.cband=strmatch('ccav',handles.bpm_type);
handles.ncband=length(handles.cband);
handles.cband_name=handles.bpm_name(handles.cband);
handles.cband_index=handles.bpm_index(handles.cband);
%sbands
handles.sband=strmatch('scav',handles.bpm_type);
handles.nsband=length(handles.sband);
handles.sband_name=handles.bpm_name(handles.sband);
handles.sband_index=handles.bpm_index(handles.sband);

%get ICT info
handles.ICT=findcells(BEAMLINE,'Name','ICT1X');
handles.ICT_index=findcells(INSTR,'Index',handles.ICT);

%get correctors info
xcor_temp=[findcells(BEAMLINE, 'Name','ZS*X') findcells(BEAMLINE, 'Name','ZH*X') findcells(BEAMLINE, 'Name','ZH*FF')];
handles.xcor_name='';
nxcor=length(xcor_temp);
handles.xcor=[];
for i=1:nxcor
  if (strcmp(BEAMLINE{xcor_temp(i)}.Class, 'XCOR') & xcor_temp(i)>handles.BS1XA)
    handles.xcor(end+1)=xcor_temp(i);
  end
end
handles.nxcor=length(handles.xcor);
handles.xcor_ps=zeros(1,handles.nxcor);
handles.xcor_ps_read=zeros(1,handles.nxcor);
handles.xcor_B=zeros(1,handles.nxcor);
handles.xcor_S=zeros(1,handles.nxcor);
for i=1:handles.nxcor
  handles.xcor_ps(i)=BEAMLINE{handles.xcor(i)}.PS;
  handles.xcor_B(i)=BEAMLINE{handles.xcor(i)}.B;
  handles.xcor_S(i)=BEAMLINE{handles.xcor(i)}.S;
  handles.xcor_name=strvcat(handles.xcor_name, BEAMLINE{handles.xcor(i)}.Name);
end
handles.xcor_S=handles.xcor_S-BEAMLINE{handles.IEX}.S;
FlHwUpdate();
for i=1:handles.nxcor
  handles.xcor_ps_read(i)=PS(handles.xcor_ps(i)).Ampl;
end

ycor_temp=[findcells(BEAMLINE, 'Name','ZV*X') findcells(BEAMLINE, 'Name','ZV*FF')];
handles.ycor_name='';
nycor=length(ycor_temp);
handles.ycor=[];
for i=1:nycor
  if (strcmp(BEAMLINE{ycor_temp(i)}.Class, 'YCOR') & ycor_temp(i)>handles.BS1XA)
    handles.ycor(end+1)=ycor_temp(i);
  end
end
handles.nycor=length(handles.ycor);
handles.ycor_ps=zeros(1,handles.nycor);
handles.ycor_ps_read=zeros(1,handles.nycor);
handles.ycor_B=zeros(1,handles.nycor);
handles.ycor_S=zeros(1,handles.nycor);
for i=1:handles.nycor
  handles.ycor_ps(i)=BEAMLINE{handles.ycor(i)}.PS;
  handles.ycor_B(i)=BEAMLINE{handles.ycor(i)}.B;
  handles.ycor_S(i)=BEAMLINE{handles.ycor(i)}.S;
  handles.ycor_name=strvcat(handles.ycor_name, BEAMLINE{handles.ycor(i)}.Name);
end
handles.ycor_S=handles.ycor_S-BEAMLINE{handles.IEX}.S;
FlHwUpdate();
for i=1:handles.nycor
  handles.ycor_ps_read(i)=PS(handles.ycor_ps(i)).Ampl;
end

%get movers info
handles.mover=28:49;
handles.nmover=length(handles.mover);
handles.mover_name='';
handles.mover_pos=zeros(handles.nmover,3);
handles.mover_S=zeros(handles.nmover,1);
handles.xmover_KL=zeros(1,handles.nmover);
handles.ymover_KL=zeros(1,handles.nmover);

FlHwUpdate();
for i=1:handles.nmover
  elems=findcells(BEAMLINE,'Girder',handles.mover(i));
  if(strcmp(BEAMLINE{elems(1)}.Class,'MONI'))
    elems(1)=[];
  end
  handles.mover_S(i)=BEAMLINE{elems(1)}.S-BEAMLINE{handles.IEX}.S;
  handles.mover_name=strvcat(handles.mover_name,['A' BEAMLINE{elems(1)}.Name(1:strfind(BEAMLINE{elems(1)}.Name,'MULT')-1)]);
  handles.elem_on_mover{i}=findcells(BEAMLINE,'Girder',handles.mover(i));
  handles.q_on_mover(i,:)=findcells(BEAMLINE,'Class','QUAD',handles.elem_on_mover{i}(1),handles.elem_on_mover{i}(end));
  handles.bpm_on_mover(i)=findcells(BEAMLINE,'Class','MONI',handles.elem_on_mover{i}(1),handles.elem_on_mover{i}(end));
  for quad=handles.q_on_mover(i,:)
    handles.xmover_KL(i)=handles.xmover_KL(i)+BEAMLINE{quad}.B*PS(BEAMLINE{quad}.PS).Ampl*0.2998/BEAMLINE{quad}.P;
    handles.ymover_KL(i)=handles.ymover_KL(i)+BEAMLINE{quad}.B*PS(BEAMLINE{quad}.PS).Ampl*0.2998/BEAMLINE{quad}.P;
  end
  handles.mover_pos(i,:)=GIRDER{handles.mover(i)}.MoverPos;
end
handles.mover_S=handles.mover_S-BEAMLINE{handles.IEX}.S;


% --- Executes just before grid_scan is made visible.
function grid_scan_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to grid_scan (see VARARGIN)

% Choose default command line output for grid_scan
handles.output = hObject;

global FL BEAMLINE

handles=initialize(handles);
handles.plane='Horizontal';
handles.corr_1x_list=1;
set(handles.corr_1x_menu,'String',char(handles.xcor_name,handles.mover_name))
set(handles.corr_1x_menu,'Value',handles.corr_1x_list)
handles.corr_2x_list=2;
set(handles.corr_2x_menu,'String',char(handles.xcor_name,handles.mover_name))
set(handles.corr_2x_menu,'Value',handles.corr_2x_list)
handles.corr_1y_list=1;
set(handles.corr_1y_menu,'String',char(handles.ycor_name,handles.mover_name))
set(handles.corr_1y_menu,'Value',handles.corr_1y_list)
handles.corr_2y_list=2;
set(handles.corr_2y_menu,'String',char(handles.ycor_name,handles.mover_name))
set(handles.corr_2y_menu,'Value',handles.corr_2y_list)
handles.mean_readings=1;
handles.jitter_subtraction=1;
% nux1=FL.SimModel.Twiss.nux(handles.bpm(handles.bpm_1));
% nux2=FL.SimModel.Twiss.nux(handles.bpm(handles.bpm_2));
% phase_adv_bpm=mod(nux1-nux2,2*pi)/pi-1;
% set(handles.phase_adv_bpm_edit,'String',phase_adv_bpm);
nux1=FL.SimModel.Twiss.nux(handles.xcor(handles.corr_1x_list));
nux2=FL.SimModel.Twiss.nux(handles.xcor(handles.corr_2x_list));
phase_adv_corr_x=mod(nux1-nux2,2*pi)/pi-1;
set(handles.phase_adv_corr_x_edit,'String',phase_adv_corr_x);
nuy1=FL.SimModel.Twiss.nuy(handles.ycor(handles.corr_1y_list));
nuy2=FL.SimModel.Twiss.nuy(handles.ycor(handles.corr_2y_list));
phase_adv_corr_y=mod(nuy1-nuy2,2*pi)/pi-1;
set(handles.phase_adv_corr_y_edit,'String',phase_adv_corr_y);

handles.corr_x_ampl_sigma=3;
set(handles.corr_x_ampl_sigma_edit,'String',handles.corr_x_ampl_sigma);
handles.corr_y_ampl_sigma=3;
set(handles.corr_y_ampl_sigma_edit,'String',handles.corr_y_ampl_sigma);

[bpm0,elem1,elem2,elem1_name,elem2_name,corr_ampl_sigma]=get_bpm0(handles);
if strcmp(handles.plane,'Horizontal')
    corr_ampl=get_cor_ampl_wanted(elem1,elem2,bpm0,handles.plane,handles.corr_x_ampl_sigma,FL.SimModel);
    set(handles.corr_x_dampl_edit,'String',sprintf('%g\n%g',corr_ampl(1)*1e6,corr_ampl(2)*1e6));
else
    corr_ampl=get_cor_ampl_wanted(elem1,elem2,bpm0,handles.plane,handles.corr_y_ampl_sigma,FL.SimModel);
    set(handles.corr_y_dampl_edit,'String',sprintf('%g\n%g',corr_ampl(1)*1e6,corr_ampl(2)*1e6));
end

if(FL.SimMode)
    handles.filename_save=sprintf('test_%s_%s_%.1fsigma',elem1_name,elem2_name,corr_ampl_sigma);
else
    handles.filename_save=sprintf('grid_scan_%s_%s_%.1fsigma',elem1_name,elem2_name,corr_ampl_sigma);
end
set(handles.filename_save_edit,'String',handles.filename_save);

handles.bpmave=10;
handles.bpmsvd=100;

set_corr_ampl_sigma_max(handles,elem1,elem2,bpm0);

handles.grid_squaring=1;
w=what('~/ATF2/FlightSim/userData');
load_find=[find(strncmp(w.mat,'grid_scan',9)); find(strncmp(w.mat,'test',4))];
if(length(load_find)>=1)
    set(handles.filename_load_popup,'string',w.mat(load_find))
    handles.filename_load=w.mat{load_find(1)};
else
    set(handles.filename_load_popup,'string',' ')
end
% Update handles structure
guidata(hObject, handles);

% UIWAIT makes grid_scan wait for user response (see UIRESUME)
% uiwait(handles.bpm);


% --- Outputs from this function are returned to the command line.
function varargout = grid_scan_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on selection change in corr_1x_menu.
function corr_1x_menu_Callback(hObject, eventdata, handles)
% hObject    handle to corr_1x_menu (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns corr_1x_menu contents as cell array
%        contents{get(hObject,'Value')} returns selected item from corr_1x_menu
global FL

handles.corr_1x_list=get(hObject,'Value');

[bpm0,elem1,elem2,elem1_name,elem2_name,corr_ampl_sigma]=get_bpm0(handles);

nux1=FL.SimModel.Twiss.nux(elem1);
nux2=FL.SimModel.Twiss.nux(elem2);

phase_adv_corr_x=mod(nux1-nux2,2*pi)/pi-1;
set(handles.phase_adv_corr_x_edit,'String',phase_adv_corr_x);

set_corr_ampl_sigma_max(handles,elem1,elem2,bpm0);

set(handles.bpm0_edit,'String',sprintf('%s',handles.bpm_name{handles.bpm==bpm0}));
if(FL.SimMode)
    handles.filename_save=sprintf('test_%s_%s_%.1fsigma',elem1_name,elem2_name,corr_ampl_sigma);
else
    handles.filename_save=sprintf('grid_scan_%s_%s_%.1fsigma',elem1_name,elem2_name,corr_ampl_sigma);
end
set(handles.filename_save_edit,'String',handles.filename_save);

corr_ampl=get_cor_ampl_wanted(elem1,elem2,bpm0,handles.plane,corr_ampl_sigma,FL.SimModel);
set(handles.corr_x_dampl_edit,'String',sprintf('%g\n%g',corr_ampl(1)*1e6,corr_ampl(2)*1e6))

guidata(hObject, handles);


% --- Executes during object creation, after setting all properties.
function corr_1x_menu_CreateFcn(hObject, eventdata, handles)
% hObject    handle to corr_1x_menu (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc & isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in corr_2x_menu.
function corr_2x_menu_Callback(hObject, eventdata, handles)
% hObject    handle to corr_2x_menu (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns corr_2x_menu contents as cell array
%        contents{get(hObject,'Value')} returns selected item from corr_2x_menu
global FL

handles.corr_2x_list=get(hObject,'Value');
[bpm0,elem1,elem2,elem1_name,elem2_name,corr_ampl_sigma]=get_bpm0(handles);

nux1=FL.SimModel.Twiss.nux(elem1);
nux2=FL.SimModel.Twiss.nux(elem2);

phase_adv_corr_x=mod(nux1-nux2,2*pi)/pi-1;
set(handles.phase_adv_corr_x_edit,'String',phase_adv_corr_x);

set_corr_ampl_sigma_max(handles,elem1,elem2,bpm0);

set(handles.bpm0_edit,'String',sprintf('%s',handles.bpm_name{handles.bpm==bpm0}));
if(FL.SimMode)
    handles.filename_save=sprintf('test_%s_%s_%.1fsigma',elem1_name,elem2_name,corr_ampl_sigma);
else
    handles.filename_save=sprintf('grid_scan_%s_%s_%.1fsigma',elem1_name,elem2_name,corr_ampl_sigma);
end
set(handles.filename_save_edit,'String',handles.filename_save);
corr_ampl=get_cor_ampl_wanted(elem1,elem2,bpm0,handles.plane,corr_ampl_sigma,FL.SimModel);
set(handles.corr_x_dampl_edit,'String',sprintf('%g\n%g',corr_ampl(1)*1e6,corr_ampl(2)*1e6))

guidata(hObject, handles);

% --- Executes during object creation, after setting all properties.
function corr_2x_menu_CreateFcn(hObject, eventdata, handles)
% hObject    handle to corr_2x_menu (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc & isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in run_btn.
function run_btn_Callback(hObject, eventdata, handles)
% hObject    handle to run_btn (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

global BEAMLINE FL PS

if(handles.corr_2x_list==handles.corr_1x_list && strcmp(handles.plane,'Horizontal'))
    errordlg('select 2 different correctors', 'Error');
    return
end
if(handles.corr_2y_list==handles.corr_1y_list && strcmp(handles.plane,'Vertical'))
    errordlg('select 2 different correctors', 'Error');
    return
end

if(exist(handles.filename_save,'file')  | exist([handles.filename_save '.mat'],'file'))
    button = questdlg(sprintf('File %s exist. Overwrite it ?',handles.filename_save),'File exist','Cancel');
    switch button
        case 'No'
            return;
        case 'Cancel'
            return;
        case ''
            return;
    end
end

disp('Measuring SVD matrix for jitter substraction');
FlHwUpdate('wait',handles.bpmsvd);
disp('BPM measured for SVD');
[s,raw]=FlHwUpdate('readbuffer',handles.bpmsvd);
read_svd=[raw(:,handles.bpm_index*3-1) raw(:,handles.bpm_index*3)];

mean_read=zeros(0,2*handles.nbpm);
all_read=zeros(0,2*handles.nbpm);
mean_read_svd=zeros(0,2*handles.nbpm);
all_read_svd=zeros(0,2*handles.nbpm);
cor_1_value=[];
cor_2_value=[];

corr_ampl=get_cor_ampl(handles);

raw_read={};
cor_1_value=[];
cor_2_value=[];

if (strcmp(handles.plane,'Horizontal'))
    for corr_1x_dstrength=-3*corr_ampl(1):corr_ampl(1):3*corr_ampl(1)
        if(handles.corr_1x_list<=handles.nxcor)
            %1st is a corrector        
            ps_1x_newvalue=handles.xcor_ps_read(handles.corr_1x_list)+corr_1x_dstrength;
            set_ps(handles.xcor_ps(handles.corr_1x_list),ps_1x_newvalue);
            cor_1_value(end+(1:7))=get_ps(handles.xcor_ps(handles.corr_1x_list))*ones(1,7);
            elem1=handles.xcor(handles.corr_1x_list);
        else
            %1st is a mover
            %angle=displ*mover_kl
            mover_1x_dpos=(corr_1x_dstrength)/handles.xmover_KL(handles.corr_1x_list-handles.nxcor);
            mover_1x_pos=handles.mover_pos(handles.corr_1x_list-handles.nxcor,:);
            mover_1x_pos(1)=mover_1x_pos(1)+mover_1x_dpos;
            set_mover(handles.mover(handles.corr_1x_list-handles.nxcor),mover_1x_pos);
            pos_1=get_mover(handles.mover(handles.corr_1x_list-handles.nxcor));
            cor_1_value(end+(1:7))=pos_1(1)*handles.xmover_KL(handles.corr_1x_list-handles.nxcor)*ones(1,7);
            elem1=handles.elem_on_mover{handles.corr_1x_list-handles.nxcor}(1);
        end
        for corr_2x_dstrength=-3*corr_ampl(2):corr_ampl(2):3*corr_ampl(2)
            if(handles.corr_2x_list<=handles.nxcor)
                %2nd is a corrector
                ps_2x_newvalue=handles.xcor_ps_read(handles.corr_2x_list)+corr_2x_dstrength;
                set_ps(handles.xcor_ps(handles.corr_2x_list),ps_2x_newvalue);
                cor_2_value(end+1)=get_ps(handles.xcor_ps(handles.corr_2x_list));
                elem2=handles.xcor(handles.corr_2x_list);
            else
                %2nd is a mover
                mover_2x_dpos=(corr_2x_dstrength)/handles.xmover_KL(handles.corr_2x_list-handles.nxcor);
                mover_2x_pos=handles.mover_pos(handles.corr_2x_list-handles.nxcor,:);
                mover_2x_pos(1)=mover_2x_pos(1)+mover_2x_dpos;
                set_mover(handles.mover(handles.corr_2x_list-handles.nxcor),mover_2x_pos);
                pos_2=get_mover(handles.mover(handles.corr_2x_list-handles.nxcor));
                cor_2_value(end+1)=pos_2(1)*handles.xmover_KL(handles.corr_2x_list-handles.nxcor);
                elem2=handles.elem_on_mover{handles.corr_2x_list-handles.nxcor}(1);
            end
            fprintf('measurement %i/49\n',length(cor_2_value));
            FlHwUpdate('wait',handles.bpmave);
            [s,raw]=FlHwUpdate('readbuffer',handles.bpmave);
            raw_read{end+1}=raw;
            read_svd_sub=remove_jitter_svd_auto(handles,raw,read_svd,min([elem1 elem2]));
%             [s,output]=bpmave(raw,handles.bpmave);
            bpm_read=[raw(:,handles.bpm_index*3-1) raw(:,handles.bpm_index*3)];
            mean_read(end+1,:)=mean(bpm_read,1);
            all_read(end+1:end+handles.bpmave,:)=bpm_read;
            mean_read_svd(end+1,:)=mean(read_svd_sub,1);
            all_read_svd(end+1:end+handles.bpmave,:)=read_svd_sub;
        end
    end
    if(handles.corr_1x_list<=handles.nxcor)
        set_ps(handles.xcor_ps(handles.corr_1x_list),handles.xcor_ps_read(handles.corr_1x_list));
    else
        set_mover(handles.mover(handles.corr_1x_list-handles.nxcor),handles.mover_pos(handles.corr_1x_list-handles.nxcor,:));
    end
    if(handles.corr_2x_list<=handles.nxcor)
        set_ps(handles.xcor_ps(handles.corr_2x_list),handles.xcor_ps_read(handles.corr_2x_list));
    else
        set_mover(handles.mover(handles.corr_2x_list-handles.nxcor),handles.mover_pos(handles.corr_2x_list-handles.nxcor,:));
    end
else
    %Vertical measurement
    for corr_1y_dstrength=-3*corr_ampl(1):corr_ampl(1):3*corr_ampl(1)
        if(handles.corr_1y_list<=handles.nycor)
            %1st is a corrector        
            ps_1y_newvalue=handles.ycor_ps_read(handles.corr_1y_list)+corr_1y_dstrength;
            set_ps(handles.ycor_ps(handles.corr_1y_list),ps_1y_newvalue);
            cor_1_value(end+(1:7))=get_ps(handles.ycor_ps(handles.corr_1y_list))*ones(1,7);
            elem1=handles.ycor(handles.corr_1y_list);
        else
            %1st is a mover
            %angle=displ*mover_kl
            mover_1y_dpos=-(corr_1y_dstrength)/handles.ymover_KL(handles.corr_1y_list-handles.nycor);
            mover_1y_pos=handles.mover_pos(handles.corr_1y_list-handles.nycor,:);
            mover_1y_pos(2)=mover_1y_pos(2)+mover_1y_dpos;
            set_mover(handles.mover(handles.corr_1y_list-handles.nycor),mover_1y_pos);
            pos_1=get_mover(handles.mover(handles.corr_1y_list-handles.nycor));
            cor_1_value(end+(1:7))=pos_1(2)*-handles.ymover_KL(handles.corr_1y_list-handles.nycor)*ones(1,7);
            elem1=handles.elem_on_mover{handles.corr_1y_list-handles.nycor}(1);
        end
        for corr_2y_dstrength=-3*corr_ampl(2):corr_ampl(2):3*corr_ampl(2)
            if(handles.corr_2y_list<=handles.nycor)
                %2nd is a corrector
                ps_2y_newvalue=handles.ycor_ps_read(handles.corr_2y_list)+corr_2y_dstrength;
                set_ps(handles.ycor_ps(handles.corr_2y_list),ps_2y_newvalue);
                cor_2_value(end+1)=get_ps(handles.ycor_ps(handles.corr_2y_list));
                elem2=handles.ycor(handles.corr_2y_list);
            else
                %2nd is a mover
                mover_2y_dpos=(corr_2y_dstrength)/-handles.ymover_KL(handles.corr_2y_list-handles.nycor);
                mover_2y_pos=handles.mover_pos(handles.corr_2y_list-handles.nycor,:);
                mover_2y_pos(2)=mover_2y_pos(2)+mover_2y_dpos;
                set_mover(handles.mover(handles.corr_2y_list-handles.nycor),mover_2y_pos);
                pos_2=get_mover(handles.mover(handles.corr_2y_list-handles.nycor));
                cor_2_value(end+1)=pos_2(2)*-handles.ymover_KL(handles.corr_2y_list-handles.nycor);
                elem2=handles.elem_on_mover{handles.corr_2y_list-handles.nycor}(1);
            end
            
            FlHwUpdate('wait',handles.bpmave);
            [s,raw]=FlHwUpdate('readbuffer',handles.bpmave);
            raw_read{end+1}=raw;
            read_svd_sub=remove_jitter_svd_auto(handles,raw,read_svd,min([elem1 elem2]));
%             [s,output]=bpmave(raw,handles.bpmave);
            bpm_read=[raw(:,handles.bpm_index*3-1) raw(:,handles.bpm_index*3)];
            mean_read(end+1,:)=mean(bpm_read,1);
            all_read(end+1:end+handles.bpmave,:)=bpm_read;
            mean_read_svd(end+1,:)=mean(read_svd_sub,1);
            all_read_svd(end+1:end+handles.bpmave,:)=read_svd_sub;
        end
    end
    if(handles.corr_1y_list<=handles.nycor)
        set_ps(handles.ycor_ps(handles.corr_1y_list),handles.ycor_ps_read(handles.corr_1y_list));
    else
        set_mover(handles.mover(handles.corr_1y_list-handles.nycor),handles.mover_pos(handles.corr_1y_list-handles.nycor,:));
    end
    if(handles.corr_2y_list<=handles.nycor)
        set_ps(handles.ycor_ps(handles.corr_2y_list),handles.ycor_ps_read(handles.corr_2y_list));
    else
        set_mover(handles.mover(handles.corr_2y_list-handles.nycor),handles.mover_pos(handles.corr_2y_list-handles.nycor,:));
    end
end
plane=handles.plane;
corr_1x_list=handles.corr_1x_list;
corr_2x_list=handles.corr_2x_list;
corr_1y_list=handles.corr_1y_list;
corr_2y_list=handles.corr_2y_list;
BEAMLINE_saved=BEAMLINE;
PS_saved=PS;
FL_SimModel_saved=FL.SimModel;
corr_x_ampl_sigma=handles.corr_x_ampl_sigma;
corr_y_ampl_sigma=handles.corr_y_ampl_sigma;
bpm_saved=handles.bpm;
save(['~/ATF2/FlightSim/userData/' handles.filename_save '.mat'],'plane','all_read*','mean_read*','cor_*_value','corr_*_list',...
     'read_svd','BEAMLINE_saved','PS_saved','FL_SimModel_saved','corr_*_ampl_sigma',...
     'bpm_saved','raw_read','handles');
w=what('~/ATF2/FlightSim/userData');
load_find=[find(strncmp(w.mat,'grid_scan',9)); find(strncmp(w.mat,'test',4))];
set(handles.filename_load_popup,'String',w.mat(load_find))
load_value=find(strncmp(w.mat(load_find),handles.filename_save,length(handles.filename_save)));
set(handles.filename_load_popup,'Value',load_value)
loadable=get(handles.filename_load_popup,'String');
handles.filename_load=loadable{load_value};
guidata(hObject, handles);


function SVD_matrix=get_SVD_matrix(read_svd,bpm_used_svd_inbpm)
% SVD_matrix            matrix such as if multiplied by the left with 
%                       bpm_used_svd_inbpm BPM readings return readings for
%                       all handles.bpm for the lattice used getting
%                       read_svd.
% read_svd              BPM readings 1 rows= 1 pulses, 1 column = 1 BPM
% bpm_used_svd_inbpm    BPMs used to remove jitter counted as indices of
%                       handles.bpm

    global BEAMLINE;
    global INSTR;

    handles.IEX=findcells(BEAMLINE,'Name','IEX');
    handles.bpm=setdiff(findcells(BEAMLINE,'Class','MONI'),findcells(BEAMLINE,'Name','FONT*')); % all MONIs except FONT pickups ...
    handles.bpm=handles.bpm(handles.bpm>handles.IEX); % ... downstream of IEX
    bpm_used_svd=handles.bpm(bpm_used_svd_inbpm);
%     handles.bpm_index=zeros(size(handles.bpm));
%     for n=1:length(handles.bpm)
%       handles.bpm_index(n)=findcells(INSTR,'Index',handles.bpm(n));
%     end
%     bpm_used_svd_index=handles.bpm_index(bpm_used_svd_inbpm);
%     bpm_used=handles.bpm;
%     bpm_used_index=handles.bpm_index;

    nbpm=size(read_svd,2)/2;
    nbpm_svd=length(bpm_used_svd);
    SVD_matrix=zeros(2*nbpm_svd+1,2*nbpm);
    npulse=size(read_svd,1);
    for bpm=1:nbpm
        if(~any(bpm==bpm_used_svd_inbpm))
            SVD_matrix(:,bpm)= ...
                   lscov([read_svd(:,[bpm_used_svd_inbpm nbpm+bpm_used_svd_inbpm]) ones(npulse,1)],read_svd(:,bpm));
        end        
    end
    for i=1:nbpm_svd
        SVD_matrix([1:i-1 i+1:nbpm_svd+i-1 nbpm_svd+i+1:2*nbpm_svd+1],bpm_used_svd_inbpm(i))=...
            lscov([read_svd(:,[bpm_used_svd_inbpm([1:i-1 i+1:nbpm_svd]) nbpm+bpm_used_svd_inbpm([1:i-1 i+1:nbpm_svd])]) ones(npulse,1)],read_svd(:,bpm_used_svd_inbpm(i)));
    end
    for bpm=1:nbpm
        if(~any(bpm==bpm_used_svd_inbpm))
            SVD_matrix(:,nbpm+bpm)= ...
                   lscov([read_svd(:,[bpm_used_svd_inbpm nbpm+bpm_used_svd_inbpm]) ones(npulse,1)],read_svd(:,nbpm+bpm));
        end        
    end
    for i=1:nbpm_svd
        SVD_matrix([1:i-1 i+1:nbpm_svd+i-1 nbpm_svd+i+1:2*nbpm_svd+1],bpm_used_svd_inbpm(i)+nbpm)=...
            lscov([read_svd(:,[bpm_used_svd_inbpm([1:i-1 i+1:nbpm_svd]) nbpm+bpm_used_svd_inbpm([1:i-1 i+1:nbpm_svd])]) ones(npulse,1)],read_svd(:,bpm_used_svd_inbpm(i)+nbpm));
    end

    
function bpm_read_svd_sub=remove_jitter_svd_auto(handles,bpm_read,read_svd,corrector_used)
    global BEAMLINE
    
    bpm_used=handles.bpm;
    nbpm=length(bpm_used);
    if(size(bpm_read,2)>2*nbpm)
        raw=bpm_read;
        bpm_read=[raw(:,handles.bpm_index*3-1) raw(:,handles.bpm_index*3)];
    end
    nreadings=size(bpm_read,1);
    if(exist('corrector_used'))
        if(length(corrector_used)>1)
            corrector_used=corrector_used(1);
        end
        bpm_used_svd_inbpm=find(bpm_used<corrector_used);
    else
        bpm_used_svd_inbpm=1:nbpm;
    end
    
    SVD_matrix=get_SVD_matrix(read_svd,bpm_used_svd_inbpm);
    bpm_read_svd_sub=bpm_read-[bpm_read(:,[bpm_used_svd_inbpm nbpm+bpm_used_svd_inbpm]) ones(nreadings,1)]*SVD_matrix;


% --- Executes on selection change in bpm_2_menu.
function bpm_2_menu_Callback(hObject, eventdata, handles)
% hObject    handle to bpm_2_menu (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns bpm_2_menu contents as cell array
%        contents{get(hObject,'Value')} returns selected item from bpm_2_menu
load(['~/ATF2/FlightSim/userData/' handles.filename_load],'FL_SimModel_saved')

handles.bpm_2=get(hObject,'Value');
nux1=FL_SimModel_saved.Twiss.nux(handles.bpm(handles.bpm_1));
nux2=FL_SimModel_saved.Twiss.nux(handles.bpm(handles.bpm_2));
phase_adv_bpm=mod(nux1-nux2,2*pi)/pi-1;
set(handles.phase_adv_bpm_edit,'String',phase_adv_bpm);
guidata(hObject, handles);

% --- Executes during object creation, after setting all properties.
function bpm_2_menu_CreateFcn(hObject, eventdata, handles)
% hObject    handle to bpm_2_menu (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc & isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in bpm_1_menu.
function bpm_1_menu_Callback(hObject, eventdata, handles)
% hObject    handle to bpm_1_menu (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns bpm_1_menu contents as cell array
%        contents{get(hObject,'Value')} returns selected item from bpm_1_menu

load(['~/ATF2/FlightSim/userData/' handles.filename_load],'FL_SimModel_saved')
handles.bpm_1=get(hObject,'Value');
nux1=FL_SimModel_saved.Twiss.nux(handles.bpm(handles.bpm_1));
nux2=FL_SimModel_saved.Twiss.nux(handles.bpm(handles.bpm_2));
phase_adv_bpm=mod(nux1-nux2,2*pi)/pi-1;
set(handles.phase_adv_bpm_edit,'String',phase_adv_bpm);
guidata(hObject, handles);


% --- Executes during object creation, after setting all properties.
function bpm_1_menu_CreateFcn(hObject, eventdata, handles)
% hObject    handle to bpm_1_menu (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc & isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in plot_btn.
function plot_btn_Callback(hObject, eventdata, handles)
% hObject    handle to plot_btn (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global FL BEAMLINE PS INSTR

load(['~/ATF2/FlightSim/userData/' handles.filename_load],'plane','all_read*','mean_read*','cor_*_value','corr_*_list',...
     'read_svd','BEAMLINE_saved','PS_saved','FL_SimModel_saved','corr_*_ampl_sigma',...
     'bpm_saved','raw_read');
BEAMLINE=BEAMLINE_saved;
PS=PS_saved;
 
ICTDUMP_index=findcells(INSTR,'Index',findcells(BEAMLINE,'Name','ICTDUMP')); %179
if strncmp(handles.filename_load,'test',4)
    iread_min=1e-3;
else
    iread_min=3e8;
end
if (exist('raw_read'))
    for i=1:49; 
        iread((i-1)*10+(1:10),:)=raw_read{i}(:,[handles.bpm_index ICTDUMP_index]*3+1); 
    end
    for i=find(mean(iread)<=iread_min)
        iread(:,i)=0;
    end
    for i=find(std(iread)==0)load(['~/ATF2/FlightSim/userData/' handles.filename_load],'plane','all_read*','mean_read*','cor_*_value','corr_*_list',...
     'read_svd','BEAMLINE_saved','PS_saved','FL_SimModel_saved','corr_*_ampl_sigma',...
     'bpm_saved','raw_read');
        iread(:,i)=iread(:,find(std(iread)>0 & 1:size(iread,2)>i,1));
    end
else    
    iread=ones(490,handles.nbpm)*1e10;
end
if(handles.bpm(handles.bpm_1)>handles.bpm(handles.bpm_2))
    temp=handles.bpm_1;
    handles.bpm_1=handles.bpm_2;
    handles.bpm_2=temp;
end
[bpm0,elem1,elem2,elem1_name,elem2_name,corr_ampl_sigma]=get_bpm0(handles,plane,corr_1x_list,corr_2x_list,corr_1y_list,corr_2y_list);
bpm_0=min(find(handles.bpm==bpm0));
npulse=size(all_read,1);
npoint=size(mean_read,1);
if(handles.mean_readings & handles.jitter_subtraction)
    x_all=mean_read_svd(:,1:handles.nbpm)-ones(npoint,1)*mean(mean_read_svd(:,1:handles.nbpm));
    y_all=mean_read_svd(:,handles.nbpm+1:2*handles.nbpm)-ones(npoint,1)*mean(mean_read_svd(:,handles.nbpm+1:2*handles.nbpm));
elseif(handles.mean_readings & ~handles.jitter_subtraction)
    x_all=mean_read(:,1:handles.nbpm)-ones(npoint,1)*mean(mean_read(:,1:handles.nbpm));
    y_all=mean_read(:,handles.nbpm+1:2*handles.nbpm)-ones(npoint,1)*mean(mean_read(:,handles.nbpm+1:2*handles.nbpm));
elseif(~handles.mean_readings & handles.jitter_subtraction)
    x_all=all_read_svd(:,1:handles.nbpm)-ones(npulse,1)*mean(all_read_svd(:,1:handles.nbpm));
    y_all=all_read_svd(:,handles.nbpm+1:2*handles.nbpm)-ones(npulse,1)*mean(all_read_svd(:,handles.nbpm+1:2*handles.nbpm));
else
    x_all=all_read(:,1:handles.nbpm)-ones(npulse,1)*mean(all_read(:,1:handles.nbpm));
    y_all=all_read(:,handles.nbpm+1:2*handles.nbpm)-ones(npulse,1)*mean(all_read(:,handles.nbpm+1:2*handles.nbpm));
end
x1=x_all(:,handles.bpm_1);
x2=x_all(:,handles.bpm_2);
y1=y_all(:,handles.bpm_1);
y2=y_all(:,handles.bpm_2);

delta_cor_ampl_wanted=get_cor_ampl_wanted(elem1,elem2,bpm0,plane,corr_ampl_sigma,FL_SimModel_saved)*[-3:3];
[cor_1_value_wanted,cor_2_value_wanted]=meshgrid(delta_cor_ampl_wanted(1,:),delta_cor_ampl_wanted(2,:));
cor_1_value_wanted=reshape(cor_1_value_wanted,1,length(cor_1_value_wanted(:)));
cor_2_value_wanted=reshape(cor_2_value_wanted,1,length(cor_2_value_wanted(:)));

if (strcmp(plane,'Horizontal'))
    if(corr_1x_list<=handles.nxcor)
        [s,R_cor1_bpm0]=RmatAtoB_cor(elem1,bpm0);
    else
        [s,R_cor1_bpm0]=RmatAtoB(elem1,bpm0);
    end
    if(corr_2x_list<=handles.nxcor)
        [s,R_cor2_bpm0]=RmatAtoB_cor(elem2,bpm0);
    else
        [s,R_cor2_bpm0]=RmatAtoB(elem2,bpm0);
    end
    %determination of the optimal twiss parameters at BPM0
    x0=cor_1_value*R_cor1_bpm0(1,2)+cor_2_value*R_cor2_bpm0(1,2);
    %if bpm on mover, reading is changed by -1*mover displacement
    if(corr_1x_list>handles.nxcor && any(bpm0==handles.elem_on_mover{corr_1x_list-handles.nxcor}))
        x0=x0-cor_1_value/handles.xmover_KL(corr_1x_list-handles.nxcor);
    end
    if(corr_2x_list>handles.nxcor && any(bpm0==handles.elem_on_mover{corr_2x_list-handles.nxcor}))
        x0=x0-cor_2_value/handles.xmover_KL(corr_2x_list-handles.nxcor);
    end
    xp0=cor_1_value*R_cor1_bpm0(2,2)+cor_2_value*R_cor2_bpm0(2,2);
    % plot(x0,xp0,'+')
    p0=[x0(7); xp0(7)]-[x0(1); xp0(1)];
    q0=[x0(7*7-7+1); xp0(7*7-7+1)]-[x0(1); xp0(1)];
    if(handles.grid_squaring)
        twiss0=get_twiss0(p0,q0);
    else
        twiss0(1,1)=FL.SimModel.Twiss.betax(bpm0);
        twiss0(2,1)=FL.SimModel.Twiss.alphax(bpm0);
        twiss0(3,1)=(1+twiss0(2)^2)/twiss0(1);
    end
    [u0,v0]=normalize_phase_space(x0,xp0,twiss0);
    [u0_fit,v0_fit,area0,chi0]=fit_phase_space(cor_1_value,cor_2_value,u0,v0,cor_1_value_wanted,cor_2_value_wanted);

    [s,R_bpm1_bpm2]=RmatAtoB_cor(handles.bpm(handles.bpm_1),handles.bpm(handles.bpm_2));
    xp1=(x2-R_bpm1_bpm2(1,1)*x1)/R_bpm1_bpm2(1,2);
    twiss1=transport_twiss(twiss0,bpm0,handles.bpm(handles.bpm_1),plane);
    
    [u1,v1]=normalize_phase_space(x1,xp1,twiss1);

    hor_label1='u [nominal \sigma_x]';
    vert_label1='v [nominal \sigma_{xp}]';
    hor_label2='x_{BPM1} [mm]';
    vert_label2='x_{BPM2} [mm]';
else
    if(corr_1y_list<=handles.nycor)
        [s,R_cor1_bpm0]=RmatAtoB_cor(elem1,bpm0);
    else
        [s,R_cor1_bpm0]=RmatAtoB(elem1,bpm0);
    end
    if(corr_2y_list<=handles.nycor)
        [s,R_cor2_bpm0]=RmatAtoB_cor(elem2,bpm0);
    else
        [s,R_cor2_bpm0]=RmatAtoB(elem2,bpm0);
    end
%determination of the optimal twiss parameters at BPM0
    y0=cor_1_value*R_cor1_bpm0(3,4)+cor_2_value*R_cor2_bpm0(3,4);
    %if bpm on mover, reading is changed by -1*mover displacement
    if(corr_1y_list>handles.nycor && any(bpm0==handles.elem_on_mover{corr_1y_list-handles.nycor}))
        y0=y0-cor_1_value/-handles.ymover_KL(corr_1y_list-handles.nycor);
    end
    if(corr_2y_list>handles.nycor && any(bpm0==handles.elem_on_mover{corr_2y_list-handles.nycor}))
        y0=y0-cor_2_value/-handles.ymover_KL(corr_2y_list-handles.nycor);
    end
    yp0=cor_1_value*R_cor1_bpm0(4,4)+cor_2_value*R_cor2_bpm0(4,4);
    % plot(y0,yp0,'+')
    p0=[y0(7); yp0(7)]-[y0(1); yp0(1)];
    q0=[y0(7*7-7+1); yp0(7*7-7+1)]-[y0(1); yp0(1)];
    if(handles.grid_squaring)
        twiss0=get_twiss0(p0,q0);
    else
        twiss0(1,1)=FL.SimModel.Twiss.betay(bpm0);
        twiss0(2,1)=FL.SimModel.Twiss.alphay(bpm0);
        twiss0(3,1)=(1+twiss0(2)^2)/twiss0(1);
    end
    [u0,v0]=normalize_phase_space(y0,yp0,twiss0,'Vertical');
    [u0_fit,v0_fit,area0,chi0]=fit_phase_space(cor_1_value,cor_2_value,u0,v0,cor_1_value_wanted,cor_2_value_wanted);

    [s,R_bpm1_bpm2]=RmatAtoB_cor(handles.bpm(handles.bpm_1),handles.bpm(handles.bpm_2));
    yp1=(y2-R_bpm1_bpm2(3,3)*y1)/R_bpm1_bpm2(3,4);
    twiss1=transport_twiss(twiss0,bpm0,handles.bpm(handles.bpm_1),plane);    
    [u1,v1]=normalize_phase_space(y1,yp1,twiss1,'Vertical');

    hor_label1='u [nominal \sigma_y]';
    vert_label1='v [nominal \sigma_{yp}]';
    hor_label2='y_{BPM1} [mm]';
    vert_label2='y_{BPM2} [mm]';
end


[u1_fit,v1_fit,area1,chi1,range_1]=fit_phase_space(cor_1_value,cor_2_value,u1,v1,cor_1_value_wanted,cor_2_value_wanted,iread(:,max([handles.bpm_1 handles.bpm_2])));
set(handles.area_edit,'String',sprintf('%f',area1/area0))
set(handles.chi_edit,'String',sprintf('%f',chi1/chi0))

% figure(1)
% plot(iread')

axes(handles.plot3);
if strcmp(handles.plane,'Horizontal')
    plot(x1*1e3,x2*1e3,'r+','MarkerSize',10)
    hold on
    plot(x1(range_1)*1e3,x2(range_1)*1e3,'b+','MarkerSize',10)
    hold off
else
    plot(y1*1e3,y2*1e3,'r+','MarkerSize',10)
    hold on
    plot(y1(range_1)*1e3,y2(range_1)*1e3,'b+','MarkerSize',10)
    hold off
end
xlabel(hor_label2)
ylabel(vert_label2)

axes(handles.plot);
colormap([0 0 0]);
plot(u1,v1,'r+','MarkerSize',10)
hold on
plot(u1(range_1),v1(range_1),'b+','MarkerSize',10)
% mesh(u0_fit,v0_fit,zeros(7,7),'EraseMode','xor');
mesh(u1_fit,v1_fit,zeros(7,7),'EraseMode','xor');
hold off
% xlim([min([u1(range_1) v1(range_1)]) max([u1(range_1) v1(range_1)])]*1.2);
% ylim([min([u1(range_1) v1(range_1)]) max([u1(range_1) v1(range_1)])]*1.2);
xlim([min(u1(range_1)) max(u1(range_1))]*1.2);
ylim([min(v1(range_1)) max(v1(range_1))]*1.2);
xlabel(hor_label1)
ylabel(vert_label1)
area=zeros(1,handles.nbpm);
chi=zeros(1,handles.nbpm);
for bpm=bpm_0:handles.nbpm-1
    if(all(x_all(:,bpm)==0) || all(y_all(:,bpm)==0))
        continue
    end
    i=1;
    while(bpm+i<=handles.nbpm && (all(x_all(:,bpm+i)==0) || all(y_all(:,bpm)==0)) )
        i=i+1;
    end
    if(bpm+i>handles.nbpm)
        continue
    end
    bpm_1=bpm;
    bpm_2=bpm+i;
    [s,R_bpm1_bpm2]=RmatAtoB_cor(handles.bpm(bpm_1),handles.bpm(bpm_2));
    x1=x_all(:,bpm_1)';
    x2=x_all(:,bpm_2)';
    y1=y_all(:,bpm_1)';
    y2=y_all(:,bpm_2)';
    
    xp1=(x2-R_bpm1_bpm2(1,1)*x1)/R_bpm1_bpm2(1,2);
    yp1=(y2-R_bpm1_bpm2(3,3)*y1)/R_bpm1_bpm2(3,4);

    twiss=transport_twiss(twiss0,bpm0,handles.bpm(bpm_1),plane);

    if (strcmp(plane,'Horizontal'))
        [u1,v1]=normalize_phase_space(x1,xp1,twiss,plane);
    else
        [u1,v1]=normalize_phase_space(y1,yp1,twiss,plane);
    end
    [u1_fit,v1_fit,area1,chi1]=fit_phase_space(cor_1_value,cor_2_value,u1,v1,cor_1_value_wanted,cor_2_value_wanted,iread(:,max([handles.bpm_1 handles.bpm_2])));
    area(bpm_1)=area1/area0;
    chi(bpm_1)=chi1/chi0;
end
bpm_to_scale=bpm_0:33;

axes(handles.plot2)
cla
plot_ok=find(chi>1e-2 & area>1e-2 & 1:handles.nbpm>=bpm_to_scale(1) & 1:handles.nbpm<=bpm_to_scale(end));
plot(handles.bpm_s(plot_ok),area(plot_ok),'r-+');
hold on
plot(handles.bpm_s(plot_ok),chi(plot_ok),'b-+');
h=line([min(handles.bpm_s(plot_ok)) max(handles.bpm_s(plot_ok))],[1 1]);
set(h,'color','k','LineStyle','-.')
hold off
xlim_init=[min(handles.bpm_s(plot_ok)) max(handles.bpm_s(plot_ok))];
ylim([0 2])
xlim(xlim_init)
legend('area','squareness')
set(gca,'XTick',handles.bpm_s(plot_ok));
set(gca,'XTickLabel',handles.bpm_name(plot_ok));
xticklabel_rotate90();
%plot_magnets_external(BEAMLINE_saved(handles.IEX:end),handles.beamline_plot,xlim_init(1),xlim_init(2),1,0);
plot_magnets_external(BEAMLINE_saved,handles.beamline_plot,xlim_init(1),xlim_init(2),1,0);
directory=sprintf('userData/figures/%s_%s_%.1f_sigma/',elem1_name,elem2_name,corr_ampl_sigma);
if ~exist(directory,'dir')
    mkdir(directory)
end
print('-dpng',[directory 'bealine.png']) 


% --- Executes on button press in plot_btn.
function plot_all_BPM_btn_Callback(hObject, eventdata, handles)
% hObject    handle to plot_btn (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global FL BEAMLINE PS INSTR

load(['~/ATF2/FlightSim/userData/' handles.filename_load],'plane','all_read*','mean_read*','cor_*_value','corr_*_list',...
     'read_svd','BEAMLINE_saved','PS_saved','FL_SimModel_saved','corr_*_ampl_sigma',...
     'bpm_saved','raw_read');
BEAMLINE=BEAMLINE_saved;
PS=PS_saved;
 
ICTDUMP_index=findcells(INSTR,'Index',findcells(BEAMLINE,'Name','ICTDUMP')); %179
if (exist('raw_read'))
    for i=1:49; 
        iread((i-1)*10+(1:10),:)=raw_read{i}(:,[handles.bpm_index ICTDUMP_index]*3+1); 
    end
    for i=find(mean(iread)<=3e8)
        iread(:,i)=0;
    end
    for i=find(std(iread)==0)
        iread(:,i)=iread(:,find(std(iread)>0 & 1:size(iread,2)>i,1));
    end
figure(1);plot(iread(:,1:end)')
else    
    iread=ones(490,handles.nbpm)*1e10;
end
if(handles.bpm(handles.bpm_1)>handles.bpm(handles.bpm_2))
    temp=handles.bpm_1;
    handles.bpm_1=handles.bpm_2;
    handles.bpm_2=temp;
end
[bpm0,elem1,elem2,elem1_name,elem2_name,corr_ampl_sigma]=get_bpm0(handles,plane,corr_1x_list,corr_2x_list,corr_1y_list,corr_2y_list);
bpm_0=min(find(handles.bpm==bpm0));
npulse=size(all_read,1);
npoint=size(mean_read,1);
if(handles.mean_readings & handles.jitter_subtraction)
    x_all=mean_read_svd(:,1:handles.nbpm)-ones(npoint,1)*mean(mean_read_svd(:,1:handles.nbpm));
    y_all=mean_read_svd(:,handles.nbpm+1:2*handles.nbpm)-ones(npoint,1)*mean(mean_read_svd(:,handles.nbpm+1:2*handles.nbpm));
elseif(handles.mean_readings & ~handles.jitter_subtraction)
    x_all=mean_read(:,1:handles.nbpm)-ones(npoint,1)*mean(mean_read(:,1:handles.nbpm));
    y_all=mean_read(:,handles.nbpm+1:2*handles.nbpm)-ones(npoint,1)*mean(mean_read(:,handles.nbpm+1:2*handles.nbpm));
elseif(~handles.mean_readings & handles.jitter_subtraction)
    x_all=all_read_svd(:,1:handles.nbpm)-ones(npulse,1)*mean(all_read_svd(:,1:handles.nbpm));
    y_all=all_read_svd(:,handles.nbpm+1:2*handles.nbpm)-ones(npulse,1)*mean(all_read_svd(:,handles.nbpm+1:2*handles.nbpm));
else
    x_all=all_read(:,1:handles.nbpm)-ones(npulse,1)*mean(all_read(:,1:handles.nbpm));
    y_all=all_read(:,handles.nbpm+1:2*handles.nbpm)-ones(npulse,1)*mean(all_read(:,handles.nbpm+1:2*handles.nbpm));
end

% x_all(:,9)=.8*x_all(:,9);
% x_all(:,11)=1.2*x_all(:,11);
% x_all(:,15)=.9*x_all(:,15);

x1=x_all(:,handles.bpm_1);
x2=x_all(:,handles.bpm_2);
y1=y_all(:,handles.bpm_1);
y2=y_all(:,handles.bpm_2);

delta_cor_ampl_wanted=get_cor_ampl_wanted(elem1,elem2,bpm0,plane,corr_ampl_sigma,FL_SimModel_saved)*[-3:3];
[cor_1_value_wanted,cor_2_value_wanted]=meshgrid(delta_cor_ampl_wanted(1,:),delta_cor_ampl_wanted(2,:));
cor_1_value_wanted=reshape(cor_1_value_wanted,1,length(cor_1_value_wanted(:)));
cor_2_value_wanted=reshape(cor_2_value_wanted,1,length(cor_2_value_wanted(:)));

if (strcmp(plane,'Horizontal'))
    if(corr_1x_list<=handles.nxcor)
        [s,R_cor1_bpm0]=RmatAtoB_cor(elem1,bpm0);
    else
        [s,R_cor1_bpm0]=RmatAtoB(elem1,bpm0);
    end
    if(corr_2x_list<=handles.nxcor)
        [s,R_cor2_bpm0]=RmatAtoB_cor(elem2,bpm0);
    else
        [s,R_cor2_bpm0]=RmatAtoB(elem2,bpm0);
    end
    %determination of the optimal twiss parameters at BPM0
    x0=cor_1_value*R_cor1_bpm0(1,2)+cor_2_value*R_cor2_bpm0(1,2);
    %if bpm on mover, reading is changed by -1*mover displacement
    if(corr_1x_list>handles.nxcor && any(bpm0==handles.elem_on_mover{corr_1x_list-handles.nxcor}))
        x0=x0-cor_1_value/handles.xmover_KL(corr_1x_list-handles.nxcor);
    end
    if(corr_2x_list>handles.nxcor && any(bpm0==handles.elem_on_mover{corr_2x_list-handles.nxcor}))
        x0=x0-cor_2_value/handles.xmover_KL(corr_2x_list-handles.nxcor);
    end
    xp0=cor_1_value*R_cor1_bpm0(2,2)+cor_2_value*R_cor2_bpm0(2,2);
    % plot(x0,xp0,'+')
    p0=[x0(7); xp0(7)]-[x0(1); xp0(1)];
    q0=[x0(7*7-7+1); xp0(7*7-7+1)]-[x0(1); xp0(1)];
    if(handles.grid_squaring)
        twiss0=get_twiss0(p0,q0);
    else
        twiss0(1,1)=FL.SimModel.Twiss.betax(bpm0);
        twiss0(2,1)=FL.SimModel.Twiss.alphax(bpm0);
        twiss0(3,1)=(1+twiss0(2)^2)/twiss0(1);
    end
    [u0,v0]=normalize_phase_space(x0,xp0,twiss0);
    [u0_fit,v0_fit,area0,chi0]=fit_phase_space(cor_1_value,cor_2_value,u0,v0,cor_1_value_wanted,cor_2_value_wanted);

    [s,R_bpm1_bpm2]=RmatAtoB_cor(handles.bpm(handles.bpm_1),handles.bpm(handles.bpm_2));
    xp1=(x2-R_bpm1_bpm2(1,1)*x1)/R_bpm1_bpm2(1,2);
    twiss1=transport_twiss(twiss0,bpm0,handles.bpm(handles.bpm_1),plane);
        
    [u1,v1]=normalize_phase_space(x1,xp1,twiss1);

    hor_label1='u [nominal \sigma_x]';
    vert_label1='v [nominal \sigma_{xp}]';
    hor_label2='x_{BPM1} [mm]';
    vert_label2='x_{BPM2} [mm]';
else
    if(corr_1y_list<=handles.nycor)
        [s,R_cor1_bpm0]=RmatAtoB_cor(elem1,bpm0);
    else
        [s,R_cor1_bpm0]=RmatAtoB(elem1,bpm0);
    end
    if(corr_2y_list<=handles.nycor)
        [s,R_cor2_bpm0]=RmatAtoB_cor(elem2,bpm0);
    else
        [s,R_cor2_bpm0]=RmatAtoB(elem2,bpm0);
    end
%determination of the optimal twiss parameters at BPM0
    y0=cor_1_value*R_cor1_bpm0(3,4)+cor_2_value*R_cor2_bpm0(3,4);
    %if bpm on mover, reading is changed by -1*mover displacement
    if(corr_1y_list>handles.nycor && any(bpm0==handles.elem_on_mover{corr_1y_list-handles.nycor}))
        y0=y0-cor_1_value/-handles.ymover_KL(corr_1y_list-handles.nycor);
    end
    if(corr_2y_list>handles.nycor && any(bpm0==handles.elem_on_mover{corr_2y_list-handles.nycor}))
        y0=y0-cor_2_value/-handles.ymover_KL(corr_2y_list-handles.nycor);
    end
    yp0=cor_1_value*R_cor1_bpm0(4,4)+cor_2_value*R_cor2_bpm0(4,4);
    % plot(y0,yp0,'+')
    p0=[y0(7); yp0(7)]-[y0(1); yp0(1)];
    q0=[y0(7*7-7+1); yp0(7*7-7+1)]-[y0(1); yp0(1)];
    if(handles.grid_squaring)
        twiss0=get_twiss0(p0,q0);
    else
        twiss0(1,1)=FL.SimModel.Twiss.betay(bpm0);
        twiss0(2,1)=FL.SimModel.Twiss.alphay(bpm0);
        twiss0(3,1)=(1+twiss0(2)^2)/twiss0(1);
    end
    [u0,v0]=normalize_phase_space(y0,yp0,twiss0,'Vertical');
    [u0_fit,v0_fit,area0,chi0]=fit_phase_space(cor_1_value,cor_2_value,u0,v0,cor_1_value_wanted,cor_2_value_wanted);

    [s,R_bpm1_bpm2]=RmatAtoB_cor(handles.bpm(handles.bpm_1),handles.bpm(handles.bpm_2));
    yp1=(y2-R_bpm1_bpm2(3,3)*y1)/R_bpm1_bpm2(3,4);
    twiss1=transport_twiss(twiss0,bpm0,handles.bpm(handles.bpm_1),plane);    
    [u1,v1]=normalize_phase_space(y1,yp1,twiss1,'Vertical');

    hor_label1='u [nominal \sigma_y]';
    vert_label1='v [nominal \sigma_{yp}]';
    hor_label2='y_{BPM1} [mm]';
    vert_label2='y_{BPM2} [mm]';
end

[u1_fit,v1_fit,area1,chi1,range_1]=fit_phase_space(cor_1_value,cor_2_value,u1,v1,cor_1_value_wanted,cor_2_value_wanted,iread(:,max([handles.bpm_1 handles.bpm_2])));
set(handles.area_edit,'String',sprintf('%f',area1/area0))
set(handles.chi_edit,'String',sprintf('%f',chi1/chi0))


axes(handles.plot3);
if strcmp(handles.plane,'Horizontal')
    plot(x1*1e3,x2*1e3,'r+','MarkerSize',10)
    hold on
    plot(x1(range_1)*1e3,x2(range_1)*1e3,'b+','MarkerSize',10)
    hold off
else
    plot(y1*1e3,y2*1e3,'r+','MarkerSize',10)
    hold on
    plot(y1(range_1)*1e3,y2(range_1)*1e3,'b+','MarkerSize',10)
    hold off
end
xlabel(hor_label2)
ylabel(vert_label2)

axes(handles.plot);
colormap([0 0 0]);
plot(u1,v1,'r+','MarkerSize',10)
hold on
plot(u1(range_1),v1(range_1),'b+','MarkerSize',10)
% mesh(u0_fit,v0_fit,zeros(7,7),'EraseMode','xor');

mesh(u1_fit,v1_fit,zeros(7,7),'EraseMode','xor');
hold off
% xlim([min([u1(range_1) v1(range_1)]) max([u1(range_1) v1(range_1)])]*1.2);
% ylim([min([u1(range_1) v1(range_1)]) max([u1(range_1) v1(range_1)])]*1.2);
xlim([min(u1(range_1)) max(u1(range_1))]*1.2);
ylim([min(v1(range_1)) max(v1(range_1))]*1.2);
xlabel(hor_label1)
ylabel(vert_label1)

area=zeros(handles.nbpm,handles.nbpm);
chi=zeros(handles.nbpm,handles.nbpm);
R11_all=zeros(handles.nbpm,handles.nbpm);
R12_all=zeros(handles.nbpm,handles.nbpm);
R33_all=zeros(handles.nbpm,handles.nbpm);
R34_all=zeros(handles.nbpm,handles.nbpm);
x1size=zeros(handles.nbpm,handles.nbpm);
x2size=zeros(handles.nbpm,handles.nbpm);

nitter=20;
mean_area=ones(nitter,handles.nbpm);
scale=ones(nitter,handles.nbpm);
bpm_to_scale=[bpm_0:23 25:33];
for itter=1:nitter
    fprintf('bpm scalling : itteration %i/%i\n',itter,nitter);
    if(itter>1)
        if (strcmp(plane,'Horizontal'))
        	x_all(:,bpm_to_scale)=(ones(size(x_all,1),1)*scale(itter-1,bpm_to_scale)).*x_all(:,bpm_to_scale);
        else
            y_all(:,bpm_to_scale)=(ones(size(y_all,1),1)*scale(itter-1,bpm_to_scale)).*y_all(:,bpm_to_scale);
        end
    end
    for i=1:(handles.nbpm-bpm_0)
        bpm_1=bpm_0+i-1;
        if(all(x_all(:,bpm_1)==0) || all(y_all(:,bpm_1)==0))
            continue
        end
        for j=1:(handles.nbpm-bpm_1)
            bpm_2=bpm_1+j;
            if(all(x_all(:,bpm_2)==0) || all(y_all(:,bpm_2)==0))
                continue
            end
            [s,R_bpm1_bpm2]=RmatAtoB_cor(handles.bpm(bpm_1),handles.bpm(bpm_2));
            x1=x_all(:,bpm_1)';
            x2=x_all(:,bpm_2)';
            y1=y_all(:,bpm_1)';
            y2=y_all(:,bpm_2)';

            xp1=(x2-R_bpm1_bpm2(1,1)*x1)/R_bpm1_bpm2(1,2);
            yp1=(y2-R_bpm1_bpm2(3,3)*y1)/R_bpm1_bpm2(3,4);

            twiss=transport_twiss(twiss0,bpm0,handles.bpm(bpm_1),plane);

            if (strcmp(plane,'Horizontal'))
                [u1,v1]=normalize_phase_space(x1,xp1,twiss,plane);
            else
                [u1,v1]=normalize_phase_space(y1,yp1,twiss,plane);
            end

            [u1_fit,v1_fit,area1,chi1]=fit_phase_space(cor_1_value,cor_2_value,u1,v1,cor_1_value_wanted,cor_2_value_wanted,iread(:,max([handles.bpm_1 handles.bpm_2])));

            area(bpm_1,bpm_2)=area1/area0;
            chi(bpm_1,bpm_2)=chi1/chi0;
            R11_all(bpm_1,bpm_2)=R_bpm1_bpm2(1,1);
            R12_all(bpm_1,bpm_2)=R_bpm1_bpm2(1,2);
            R33_all(bpm_1,bpm_2)=R_bpm1_bpm2(3,3);
            R34_all(bpm_1,bpm_2)=R_bpm1_bpm2(3,4);
            x1size(bpm_1,:)=std(x1)*ones(1,handles.nbpm);
            x2size(:,bpm_2)=std(x2)*ones(handles.nbpm,1);
            areas{itter}=area;
        end
    end
    bpm_scaled=bpm_to_scale;
    for i=bpm_to_scale
        if (strcmp(plane,'Horizontal'))
            rangeok=i+find(abs(R11_all(i,i+1:bpm_to_scale(end)+1)./R12_all(i,i+1:bpm_to_scale(end)+1))*std(x_all(:,i)).^2<5e-6 & ...
                       area(i,i+1:bpm_to_scale(end)+1)>0.5 & ...
                       area(i,i+1:bpm_to_scale(end)+1)<1.5 & ...
                       ~isnan(area(i,i+1:bpm_to_scale(end)+1)));
        else
            rangeok=i+find(abs(R33_all(i,i+1:bpm_to_scale(end)+1)./R34_all(i,i+1:bpm_to_scale(end)+1))*std(y_all(:,i)).^2<5e-6 & ...
                       area(i,i+1:bpm_to_scale(end)+1)>0.5 & ...
                       area(i,i+1:bpm_to_scale(end)+1)<1.5 & ...
                       ~isnan(area(i,i+1:bpm_to_scale(end)+1)));
        end
             %    rangeok=i+1:23;
        if(length(rangeok)>1)
            mean_area(itter,i)=mean(area(i,rangeok));
        else
            bpm_scaled(i==bpm_scaled)=[];
        end
    end
    
    scale(itter,bpm_scaled)=1+0.5*(1./(mean_area(itter,bpm_scaled)-mean(mean_area(itter,bpm_scaled))+1)-1);
    chi_diag=zeros(1,handles.nbpm);
    area_diag=zeros(1,handles.nbpm);

    axes(handles.plot2)
    cla
    chi_diag(1:handles.nbpm-1)=diag(chi(1:handles.nbpm-1,2:handles.nbpm));
    area_diag(1:handles.nbpm-1)=diag(area(1:handles.nbpm-1,2:handles.nbpm));
    plot_ok=find(chi_diag>1e-2 & area_diag>1e-2& 1:handles.nbpm>=bpm_to_scale(1) & 1:handles.nbpm<=bpm_to_scale(end));
    plot(handles.bpm_s(plot_ok),area_diag(plot_ok),'r-+');
    % xlim_init=xlim();
    % ylim([0 2])
    % xlim(xlim_init)
    % ylabel('area')
    hold on
    plot(handles.bpm_s(plot_ok),chi_diag(plot_ok),'b-+');
    h=line([min(handles.bpm_s(plot_ok)) max(handles.bpm_s(plot_ok))],[1 1]);
    set(h,'color','k','LineStyle','-.')
    hold off
    % xlim_init=xlim();
    xlim_init=[min(handles.bpm_s(plot_ok)) max(handles.bpm_s(plot_ok))];
    ylim([0 2])
    xlim(xlim_init)
    %ylabel('squareness')
    legend('area','squareness')
    set(gca,'XTick',handles.bpm_s(plot_ok));
    set(gca,'XTickLabel',handles.bpm_name(plot_ok));
    xticklabel_rotate90();
    %plot_magnets_external(BEAMLINE_saved(handles.IEX:end),handles.beamline_plot,xlim_init(1),xlim_init(2),1,0);
    plot_magnets_external(BEAMLINE_saved,handles.beamline_plot,xlim_init(1),xlim_init(2),1,0);

    directory=sprintf('userData/figures/%s_%s_%.1f_sigma/',elem1_name,elem2_name,corr_ampl_sigma);
    if ~exist(directory,'dir')
        mkdir(directory)
    end
    
    chi(chi==0)=NaN;

    figure(3);
    set(gcf,'PaperUnit','point','PaperSize',[1200 800],'Position',[1 1 800 600])
    pcolor(chi(bpm_to_scale(1:end-1),bpm_to_scale(2:end))');
    caxis([0 2]);
    colorbar;
    title('chi');
    set(gca,'YTick',1:length(bpm_to_scale(2:end)));
    set(gca,'YTickLabel',handles.bpm_name(bpm_to_scale(2:end)));
    set(gca,'XTick',1:length(bpm_to_scale(1:end-1)));
    set(gca,'XTickLabel',handles.bpm_name(bpm_to_scale(1:end-1)));
    xticklabel_rotate90();
    pause(.5)
    print(3,'-dpng',[directory sprintf('chi_%i',itter)]) 

    area(area==0)=NaN;

    figure(4);
    set(gcf,'PaperUnit','point','PaperSize',[1200 800],'Position',[1 1 800 600])
    pcolor(area(bpm_to_scale(1:end-1),bpm_to_scale(2:end))');
    caxis([0 2]);
    colorbar;
    title('area');
    set(gca,'YTick',1:length(bpm_to_scale(2:end)));
    set(gca,'YTickLabel',handles.bpm_name(bpm_to_scale(2:end)));
    set(gca,'XTick',1:length(bpm_to_scale(1:end-1)));
    set(gca,'XTickLabel',handles.bpm_name(bpm_to_scale(1:end-1)));
    xticklabel_rotate90();
    pause(.5)
    print(4,'-dpng',[directory sprintf('area_%i',itter)]) 

    figure(2)
    set(gcf,'PaperUnit','point','PaperSize',[1200 800],'Position',[1 1 800 600])
    set(gcf,'DefaultAxesLineStyleOrder','-|--|:|-.')
    plot(cumprod(scale(:,bpm_to_scale)))
    legend(handles.bpm_name{bpm_to_scale},'Location','NorthEastOutside')
    pause(.5)
    print(2,'-dpng',[directory sprintf('scale_errors_%i',itter)]) 
end


disp('done');

% nbpm=24;
% modelterms=zeros(0,nbpm-bpm_0);
% for i=1:nbpm-bpm_0-1
%     nterm=nbpm-bpm_0-i;
%     modelterms(end+1:end+nterm,:)=[zeros(nterm,nbpm-bpm_0-nterm-1) ones(nterm,1) diag(ones(1,nterm))];
% end
% areafited=area(bpm_0:nbpm,bpm_0:nbpm);
% areafited=areafited(~isnan(areafited));
% 

% --- Executes on button press in mean_readings_chk.
function mean_readings_chk_Callback(hObject, eventdata, handles)
% hObject    handle to mean_readings_chk (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of mean_readings_chk
handles.mean_readings=get(hObject,'Value');
guidata(hObject, handles);


% --- Executes on button press in jitter_sub_chk.
function jitter_sub_chk_Callback(hObject, eventdata, handles)
% hObject    handle to jitter_sub_chk (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of jitter_sub_chk
handles.jitter_subtraction=get(hObject,'Value');
guidata(hObject, handles);



function phase_adv_bpm_edit_Callback(hObject, eventdata, handles)
% hObject    handle to phase_adv_bpm_edit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of phase_adv_bpm_edit as text
%        str2double(get(hObject,'String')) returns contents of phase_adv_bpm_edit as a double


% --- Executes during object creation, after setting all properties.
function phase_adv_bpm_edit_CreateFcn(hObject, eventdata, handles)
% hObject    handle to phase_adv_bpm_edit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc & isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function phase_adv_corr_x_edit_Callback(hObject, eventdata, handles)
% hObject    handle to phase_adv_corr_x_edit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of phase_adv_corr_x_edit as text
%        str2double(get(hObject,'String')) returns contents of phase_adv_corr_x_edit as a double


% --- Executes during object creation, after setting all properties.
function phase_adv_corr_x_edit_CreateFcn(hObject, eventdata, handles)
% hObject    handle to phase_adv_corr_x_edit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc & isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function bpm0_edit_Callback(hObject, eventdata, handles)
% hObject    handle to bpm0_edit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of bpm0_edit as text
%        str2double(get(hObject,'String')) returns contents of bpm0_edit as a double


% --- Executes during object creation, after setting all properties.
function bpm0_edit_CreateFcn(hObject, eventdata, handles)
% hObject    handle to bpm0_edit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function area_edit_Callback(hObject, eventdata, handles)
% hObject    handle to area_edit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of area_edit as text
%        str2double(get(hObject,'String')) returns contents of area_edit as a double


% --- Executes during object creation, after setting all properties.
function area_edit_CreateFcn(hObject, eventdata, handles)
% hObject    handle to area_edit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function chi_edit_Callback(hObject, eventdata, handles)
% hObject    handle to chi_edit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of chi_edit as text
%        str2double(get(hObject,'String')) returns contents of chi_edit as a double


% --- Executes during object creation, after setting all properties.
function chi_edit_CreateFcn(hObject, eventdata, handles)
% hObject    handle to chi_edit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function twiss0=get_twiss0(p0,q0)
G0=[p0 q0];
if(det(G0)<0)
    G0=[p0 -q0];
end
M=sqrt(abs(det(G0)))*inv(G0);
theta=atan(-M(1,2)/M(2,2));
O=[cos(theta) sin(theta); -sin(theta) cos(theta)];
OM=O*M;
beta0=OM(1,1)^-2;
alpha0=OM(2,1)/OM(1,1);
gamma0=(1+alpha0^2)/beta0;
twiss0=[beta0;alpha0;gamma0];

function [u,v]=normalize_phase_space(x,xp,twiss,plane)
if(~exist('plane')); plane='Horizontal'; end
if(strcmp(plane,'Horizontal'))
    emit=2e-9;
else
    emit=30e-12;
end
beta=twiss(1);
alpha=twiss(2);
A=[sqrt(beta) 0; -alpha/sqrt(beta) 1/sqrt(beta)];
X=[x(:) xp(:)]';
U=(sqrt(emit)*A)\X;
u=U(1,:);
v=U(2,:);

function [u_fit,v_fit,area,chi,range_ok]=fit_phase_space(...
            cor_1_value,cor_2_value,u,v,cor_1_value_wanted,cor_2_value_wanted,iread,range_init)
if(~exist('iread'))
    iread=ones(length(u),1)*1e10;
end
if(~exist('range_init'))
    range_init=ones(1,length(u));
end
if all(mean(iread<1e8))
    iread_min=1e-3;
else
    iread_min=2e9;
end

fit_order=1;
readings_per_point=length(u)/length(cor_1_value);

p1=polyfit(cor_1_value_wanted,cor_1_value,1);
cor_1_value_corrected=polyval(p1,cor_1_value_wanted);
d_cor_1_value=cor_1_value_corrected-cor_1_value;
p2=polyfit(cor_2_value_wanted,cor_2_value,1);
cor_2_value_corrected=polyval(p2,cor_2_value_wanted);
d_cor_2_value=cor_2_value_corrected-cor_2_value;

cor_value_ok=(d_cor_1_value/std(cor_1_value_wanted)<1 & d_cor_2_value/std(cor_2_value_wanted)<1);
npoint=length(find(cor_value_ok));
% cor_1_value=cor_1_value(cor_value_ok);
% cor_2_value=cor_2_value(cor_value_ok);
reading_ok=find(reshape(ones(readings_per_point,1)*cor_value_ok,49*readings_per_point,1));
% u=u(reading_ok);
% v=v(reading_ok);
% iread=iread(reading_ok);

range_ok=find((abs(u(reading_ok)-mean(u(reading_ok))))<3*std(u(reading_ok)) & ...
              (abs(v(reading_ok)-mean(v(reading_ok))))<3*std(v(reading_ok)) & ...
              iread(reading_ok)'>iread_min & ...
              range_init);
limit=sqrt(std(u(range_ok))^2+std(v(range_ok))^2)/5;
for i=10:-1:1
    cor_values=reshape(ones(readings_per_point,1)*[cor_1_value(cor_value_ok) cor_2_value(cor_value_ok)],npoint*readings_per_point,2);
%     p_u=polyfitn(cor_values(range_ok,:),u(range_ok)',fit_order);
%     p_v=polyfitn(cor_values(range_ok,:),v(range_ok)',fit_order);
    p_u=polyfitn(cor_values(range_ok,:),u(range_ok)',fit_order);
    p_v=polyfitn(cor_values(range_ok,:),v(range_ok)',fit_order);
%    u_fit=polyvaln(p_u,[cor_1_value_corrected' cor_2_value_corrected']);
%    v_fit=polyvaln(p_v,[cor_1_value_corrected' cor_2_value_corrected']);
    u_fit=polyvaln(p_u,[cor_1_value' cor_2_value']);
    v_fit=polyvaln(p_v,[cor_1_value' cor_2_value']);
%     dist_fit=sqrt((u'-reshape(ones(readings_per_point,1)*u_fit(cor_value_ok)',length(u),1)).^2+...
%                   (v'-reshape(ones(readings_per_point,1)*v_fit(cor_value_ok)',length(v),1)).^2);
    dist_fit=sqrt((u(reading_ok)'-reshape(ones(readings_per_point,1)*u_fit(cor_value_ok)',length(reading_ok),1)).^2+...
                  (v(reading_ok)'-reshape(ones(readings_per_point,1)*v_fit(cor_value_ok)',length(reading_ok),1)).^2);
    if(length(find(dist_fit<limit*i))>length(u)/2)
        range_ok=find(dist_fit<limit*i);
    else
        break
    end
end
range_ok=reading_ok(range_ok);
u_fit=reshape(u_fit,7,7);
v_fit=reshape(v_fit,7,7);
h_all=zeros(36,2);
k_all=zeros(36,2);
areas=zeros(36,1);
chis=zeros(36,1);
for i=1:6
    for j=1:6
        h=([u_fit(i+1,j) v_fit(i+1,j)]-[u_fit(i,j) v_fit(i,j)]);
        k=[u_fit(i,j+1) v_fit(i,j+1)]-[u_fit(i,j) v_fit(i,j)];
        h_all(i+(j-1)*6,:)=h;
        k_all(i+(j-1)*6,:)=k;
        areas(i+(j-1)*6,:)=abs(det([h' k']));
        chis(i+(j-1)*6,:)=0.5*(norm(h)^2+norm(k)^2)/abs(det([h' k']));
    end
end
chi=mean(chis);
area=sum(areas);

function twiss1=transport_twiss(twiss0,bpm0,bpm1,plane)
if(bpm0<bpm1)
    [s,R_bpm0_bpm1]=RmatAtoB_cor(bpm0,bpm1);
else
    [s,R_bpm1_bpm0]=RmatAtoB_cor(bpm1,bpm0);
    R_bpm0_bpm1=inv(R_bpm1_bpm0);
end
if(strcmp(plane,'Horizontal'))
    TM_bpm0_bpm1=[  R_bpm0_bpm1(1,1)^2                  -2*R_bpm0_bpm1(1,1)*R_bpm0_bpm1(1,2)    R_bpm0_bpm1(1,2)^2;...
                    -R_bpm0_bpm1(1,1)*R_bpm0_bpm1(2,1)  1+2*R_bpm0_bpm1(1,2)*R_bpm0_bpm1(2,1)   -R_bpm0_bpm1(1,2)*R_bpm0_bpm1(2,2);...
                    R_bpm0_bpm1(2,1)^2                  -2*R_bpm0_bpm1(2,1)*R_bpm0_bpm1(2,2)    R_bpm0_bpm1(2,2)^2];
else
    TM_bpm0_bpm1=[  R_bpm0_bpm1(3,3)^2                  -2*R_bpm0_bpm1(3,3)*R_bpm0_bpm1(3,4)    R_bpm0_bpm1(3,4)^2;...
                    -R_bpm0_bpm1(3,3)*R_bpm0_bpm1(4,3)  1+2*R_bpm0_bpm1(3,4)*R_bpm0_bpm1(4,3)   -R_bpm0_bpm1(3,4)*R_bpm0_bpm1(4,4);...
                    R_bpm0_bpm1(4,3)^2                  -2*R_bpm0_bpm1(4,3)*R_bpm0_bpm1(4,4)    R_bpm0_bpm1(4,4)^2];
end

twiss1=TM_bpm0_bpm1*twiss0;





function corr_x_ampl_sigma_edit_Callback(hObject, eventdata, handles)
% hObject    handle to corr_x_ampl_sigma_edit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of corr_x_ampl_sigma_edit as text
%        str2double(get(hObject,'String')) returns contents of corr_x_ampl_sigma_edit as a double
global FL
handles.corr_x_ampl_sigma=str2double(get(hObject,'String'));
[bpm0,elem1,elem2,elem1_name,elem2_name,corr_ampl_sigma]=get_bpm0(handles);
if(FL.SimMode)
    handles.filename_save=sprintf('test_%s_%s_%.1fsigma',elem1_name,elem2_name,corr_ampl_sigma);
else
    handles.filename_save=sprintf('grid_scan_%s_%s_%.1fsigma',elem1_name,elem2_name,corr_ampl_sigma);
end
set(handles.filename_save_edit,'String',handles.filename_save);
corr_ampl=get_cor_ampl_wanted(elem1,elem2,bpm0,handles.plane,corr_ampl_sigma,FL.SimModel);
set(handles.corr_x_dampl_edit,'String',sprintf('%g\n%g',corr_ampl(1)*1e6,corr_ampl(2)*1e6))

guidata(hObject, handles);

% --- Executes during object creation, after setting all properties.
function corr_x_ampl_sigma_edit_CreateFcn(hObject, eventdata, handles)
% hObject    handle to corr_x_ampl_sigma_edit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function filename_save_edit_Callback(hObject, eventdata, handles)
% hObject    handle to filename_save_edit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of filename_save_edit as text
%        str2double(get(hObject,'String')) returns contents of filename_save_edit as a double
handles.filename_save=get(hObject,'String');
guidata(hObject, handles);


% --- Executes during object creation, after setting all properties.
function filename_save_edit_CreateFcn(hObject, eventdata, handles)
% hObject    handle to filename_save_edit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% --- Executes on button press in load_btn.
function load_btn_Callback(hObject, eventdata, handles)
% hObject    handle to load_btn (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

global FL

if(exist(['~/ATF2/FlightSim/userData/' handles.filename_load],'file') | exist(['~/ATF2/FlightSim/userData/' handles.filename_load '.mat'],'file'))
    load(['~/ATF2/FlightSim/userData/' handles.filename_load],'plane','all_read*','mean_read*','cor_*_value','corr_*_list',...
     'read_svd','BEAMLINE_saved','PS_saved','FL_SimModel_saved','corr_*_ampl_sigma',...
     'bpm_saved','raw_read');
	set(handles.bpm_1_menu,'Enable','on')
	set(handles.bpm_2_menu,'Enable','on')
	set(handles.plot_btn,'Enable','on')
    set(handles.plot_all_BPM_btn,'Enable','on')
    set(handles.bpm_1_menu,'String',char(handles.bpm_name))
    set(handles.bpm_2_menu,'String',char(handles.bpm_name))
    
	[bpm0,elem1,elem2,elem1_name,elem2_name]=get_bpm0(handles,plane,corr_1x_list,corr_2x_list,corr_1y_list,corr_2y_list);
    bpm_0=find(handles.bpm==bpm0);
    handles.corr_x_ampl_sigma=corr_x_ampl_sigma;
    set(handles.corr_x_ampl_sigma_edit,'String',corr_x_ampl_sigma);
    handles.corr_y_ampl_sigma=corr_y_ampl_sigma;
    set(handles.corr_y_ampl_sigma_edit,'String',corr_y_ampl_sigma);
    handles.bpm_1=bpm_0;
    handles.bpm_2=bpm_0+1;
    set(handles.bpm_1_menu,'Value',handles.bpm_1)
    set(handles.bpm_2_menu,'Value',handles.bpm_2)
    handles.corr_1x_list=corr_1x_list;
    set(handles.corr_1x_menu,'Value',corr_1x_list)
    handles.corr_2x_list=corr_2x_list;
    set(handles.corr_2x_menu,'Value',corr_2x_list)
    handles.corr_1y_list=corr_1y_list;
    set(handles.corr_1y_menu,'Value',corr_1y_list)
    handles.corr_2y_list=corr_2y_list;
    set(handles.corr_2y_menu,'Value',corr_2y_list)

    nux1=FL_SimModel_saved.Twiss.nux(handles.bpm(handles.bpm_1));
    nux2=FL_SimModel_saved.Twiss.nux(handles.bpm(handles.bpm_2));
    phase_adv_bpm=mod(nux1-nux2,2*pi)/pi-1;
    set(handles.phase_adv_bpm_edit,'String',phase_adv_bpm);
    if(strcmp(plane,'Horizontal'))
        nu1=FL_SimModel_saved.Twiss.nux(elem1);
        nu2=FL_SimModel_saved.Twiss.nux(elem2);
        phase_adv_corr=mod(nu1-nu2,2*pi)/pi-1;
        set(handles.phase_adv_corr_x_edit,'String',phase_adv_corr);
        set(handles.vertical_radio,'Value',0);
        set(handles.horizontal_radio,'Value',1);
        corr_ampl_sigma=corr_x_ampl_sigma;
        corr_ampl=get_cor_ampl_wanted(elem1,elem2,bpm0,handles.plane,corr_ampl_sigma,FL.SimModel);
        set(handles.corr_x_dampl_edit,'String',sprintf('%g\n%g',corr_ampl(1)*1e6,corr_ampl(2)*1e6))
    else
        nu1=FL_SimModel_saved.Twiss.nuy(elem1);
        nu2=FL_SimModel_saved.Twiss.nuy(elem2);
        phase_adv_corr=mod(nu1-nu2,2*pi)/pi-1;
        set(handles.phase_adv_corr_y_edit,'String',phase_adv_corr);
        set(handles.vertical_radio,'Value',1);
        set(handles.horizontal_radio,'Value',0);
        corr_ampl_sigma=corr_y_ampl_sigma;
        corr_ampl=get_cor_ampl_wanted(elem1,elem2,bpm0,handles.plane,corr_ampl_sigma,FL.SimModel);
        set(handles.corr_y_dampl_edit,'String',sprintf('%g\n%g',corr_ampl(1)*1e6,corr_ampl(2)*1e6))
    end
    handles.plane=plane;
    set_corr_ampl_sigma_max(handles,elem1,elem2,bpm0);
    if(FL.SimMode)
        handles.filename_save=sprintf('test_%s_%s_%.1fsigma',elem1_name,elem2_name,corr_ampl_sigma);
    else
        handles.filename_save=sprintf('grid_scan_%s_%s_%.1fsigma',elem1_name,elem2_name,corr_ampl_sigma);
    end
    set(handles.filename_save_edit,'String',handles.filename_save);
else
    fprintf('file %s does not exist.\n',handles.filename_load)
end

guidata(hObject, handles);



function corr_x_ampl_sigma_max_edit_Callback(hObject, eventdata, handles)
% hObject    handle to corr_x_ampl_sigma_max_edit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of corr_x_ampl_sigma_max_edit as text
%        str2double(get(hObject,'String')) returns contents of corr_x_ampl_sigma_max_edit as a double


% --- Executes during object creation, after setting all properties.
function corr_x_ampl_sigma_max_edit_CreateFcn(hObject, eventdata, handles)
% hObject    handle to corr_x_ampl_sigma_max_edit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function corr_y_ampl_sigma_max_edit_Callback(hObject, eventdata, handles)
% hObject    handle to corr_y_ampl_sigma_max_edit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of corr_y_ampl_sigma_max_edit as text
%        str2double(get(hObject,'String')) returns contents of corr_y_ampl_sigma_max_edit as a double


% --- Executes during object creation, after setting all properties.
function corr_y_ampl_sigma_max_edit_CreateFcn(hObject, eventdata, handles)
% hObject    handle to corr_y_ampl_sigma_max_edit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function corr_y_ampl_sigma_edit_Callback(hObject, eventdata, handles)
% hObject    handle to corr_y_ampl_sigma_edit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of corr_y_ampl_sigma_edit as text
%        str2double(get(hObject,'String')) returns contents of corr_y_ampl_sigma_edit as a double
global FL
handles.corr_y_ampl_sigma=str2double(get(hObject,'String'));
[bpm0,elem1,elem2,elem1_name,elem2_name,corr_ampl_sigma]=get_bpm0(handles);
if(FL.SimMode)
    handles.filename_save=sprintf('test_%s_%s_%.1fsigma',elem1_name,elem2_name,corr_ampl_sigma);
else
    handles.filename_save=sprintf('grid_scan_%s_%s_%.1fsigma',elem1_name,elem2_name,corr_ampl_sigma);
end
set(handles.filename_save_edit,'String',handles.filename_save);
corr_ampl=get_cor_ampl_wanted(elem1,elem2,bpm0,handles.plane,corr_ampl_sigma,FL.SimModel);
set(handles.corr_y_dampl_edit,'String',sprintf('%g\n%g',corr_ampl(1)*1e6,corr_ampl(2)*1e6))

guidata(hObject, handles);


% --- Executes during object creation, after setting all properties.
function corr_y_ampl_sigma_edit_CreateFcn(hObject, eventdata, handles)
% hObject    handle to corr_y_ampl_sigma_edit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function phase_adv_corr_y_edit_Callback(hObject, eventdata, handles)
% hObject    handle to phase_adv_corr_y_edit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of phase_adv_corr_y_edit as text
%        str2double(get(hObject,'String')) returns contents of phase_adv_corr_y_edit as a double


% --- Executes during object creation, after setting all properties.
function phase_adv_corr_y_edit_CreateFcn(hObject, eventdata, handles)
% hObject    handle to phase_adv_corr_y_edit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in corr_2y_menu.
function corr_2y_menu_Callback(hObject, eventdata, handles)
% hObject    handle to corr_2y_menu (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns corr_2y_menu contents as cell array
%        contents{get(hObject,'Value')} returns selected item from corr_2y_menu
global FL

handles.corr_2y_list=get(hObject,'Value');
[bpm0,elem1,elem2,elem1_name,elem2_name,corr_ampl_sigma]=get_bpm0(handles);

nuy1=FL.SimModel.Twiss.nuy(elem1);
nuy2=FL.SimModel.Twiss.nuy(elem2);

phase_adv_corr_y=mod(nuy1-nuy2,2*pi)/pi-1;
set(handles.phase_adv_corr_y_edit,'String',phase_adv_corr_y);

set_corr_ampl_sigma_max(handles,elem1,elem2,bpm0);

set(handles.bpm0_edit,'String',sprintf('%s',handles.bpm_name{handles.bpm==bpm0}));
if(FL.SimMode)
    handles.filename_save=sprintf('test_%s_%s_%.1fsigma',elem1_name,elem2_name,corr_ampl_sigma);
else
    handles.filename_save=sprintf('grid_scan_%s_%s_%.1fsigma',elem1_name,elem2_name,corr_ampl_sigma);
end
set(handles.filename_save_edit,'String',handles.filename_save);
corr_ampl=get_cor_ampl_wanted(elem1,elem2,bpm0,handles.plane,corr_ampl_sigma,FL.SimModel);
set(handles.corr_y_dampl_edit,'String',sprintf('%g\n%g',corr_ampl(1)*1e6,corr_ampl(2)*1e6))

guidata(hObject, handles);


% --- Executes during object creation, after setting all properties.
function corr_2y_menu_CreateFcn(hObject, eventdata, handles)
% hObject    handle to corr_2y_menu (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in corr_1y_menu.
function corr_1y_menu_Callback(hObject, eventdata, handles)
% hObject    handle to corr_1y_menu (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns corr_1y_menu contents as cell array
%        contents{get(hObject,'Value')} returns selected item from corr_1y_menu
global FL

handles.corr_1y_list=get(hObject,'Value');
[bpm0,elem1,elem2,elem1_name,elem2_name,corr_ampl_sigma]=get_bpm0(handles);

nuy1=FL.SimModel.Twiss.nuy(elem1);
nuy2=FL.SimModel.Twiss.nuy(elem2);

phase_adv_corr_y=mod(nuy1-nuy2,2*pi)/pi-1;
set(handles.phase_adv_corr_y_edit,'String',phase_adv_corr_y);

set_corr_ampl_sigma_max(handles,elem1,elem2,bpm0);

set(handles.bpm0_edit,'String',sprintf('%s',handles.bpm_name{handles.bpm==bpm0}));
if(FL.SimMode)
    handles.filename_save=sprintf('test_%s_%s_%.1fsigma',elem1_name,elem2_name,corr_ampl_sigma);
else
    handles.filename_save=sprintf('grid_scan_%s_%s_%.1fsigma',elem1_name,elem2_name,corr_ampl_sigma);
end
set(handles.filename_save_edit,'String',handles.filename_save);
corr_ampl=get_cor_ampl_wanted(elem1,elem2,bpm0,handles.plane,corr_ampl_sigma,FL.SimModel);
set(handles.corr_y_dampl_edit,'String',sprintf('%g\n%g',corr_ampl(1)*1e6,corr_ampl(2)*1e6))
guidata(hObject, handles);


% --- Executes during object creation, after setting all properties.
function corr_1y_menu_CreateFcn(hObject, eventdata, handles)
% hObject    handle to corr_1y_menu (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes when selected object is changed in plan_to_scan_btngrp.
function plan_to_scan_btngrp_SelectionChangeFcn(hObject, eventdata, handles)
% hObject    handle to the selected object in plan_to_scan_btngrp 
% eventdata  structure with the following fields (see UIBUTTONGROUP)
%	EventName: string 'SelectionChanged' (read only)
%	OldValue: handle of the previously selected object or empty if none was selected
%	NewValue: handle of the currently selected object
% handles    structure with handles and user data (see GUIDATA)
global FL

handles.plane=get(eventdata.NewValue,'String');

[bpm0,elem1,elem2,elem1_name,elem2_name,corr_ampl_sigma]=get_bpm0(handles);
set_corr_ampl_sigma_max(handles,elem1,elem2,bpm0);

set(handles.bpm0_edit,'String',sprintf('%s',handles.bpm_name{handles.bpm==bpm0}));
if(FL.SimMode)
    handles.filename_save=sprintf('test_%s_%s_%.1fsigma',elem1_name,elem2_name,corr_ampl_sigma);
else
    handles.filename_save=sprintf('grid_scan_%s_%s_%.1fsigma',elem1_name,elem2_name,corr_ampl_sigma);
end
set(handles.filename_save_edit,'String',handles.filename_save);
corr_ampl=get_cor_ampl_wanted(elem1,elem2,bpm0,handles.plane,corr_ampl_sigma,FL.SimModel);
if(strcmp(handles.plane,'Horizontal'))
    set(handles.corr_x_dampl_edit,'String',sprintf('%g\n%g',corr_ampl(1)*1e6,corr_ampl(2)*1e6))
else
    set(handles.corr_y_dampl_edit,'String',sprintf('%g\n%g',corr_ampl(1)*1e6,corr_ampl(2)*1e6))
end
guidata(hObject, handles);


function set_corr_ampl_sigma_max(handles,elem1,elem2,bpm0)
global FL BEAMLINE
emitx=2e-9;
emity=30e-12;
if(strcmp(handles.plane,'Horizontal'))
    if(handles.corr_1x_list<=handles.nxcor)
        [s,R_cor1_bpm0]=RmatAtoB_cor(elem1,bpm0);
    else
        [s,R_cor1_bpm0]=RmatAtoB(elem1,bpm0);
    end
    if(handles.corr_2x_list<=handles.nxcor)
        [s,R_cor2_bpm0]=RmatAtoB_cor(elem2,bpm0);
    else
        [s,R_cor2_bpm0]=RmatAtoB(elem2,bpm0);
    end
    betax0=FL.SimModel.Twiss.betax(bpm0);
    alphax0=FL.SimModel.Twiss.alphax(bpm0);
    TM_cor_bpm0=[R_cor1_bpm0(1,2) R_cor2_bpm0(1,2);
                 R_cor1_bpm0(2,2) R_cor2_bpm0(2,2)];
    M0=[1/sqrt(emitx*betax0) 0; alphax0/sqrt(emitx*betax0) sqrt(betax0/emitx)];
    if(handles.corr_1x_list<=handles.nxcor)
        %I=B*PS.Ampl/conv; Ampl=theta; PS_max=5V
        conv_corr_1x= FL.HwInfo.PS(BEAMLINE{elem1}.PS).conv;
        B_corr_1x=BEAMLINE{elem1}.B;
        corr_ampl_max(1,1)=5*conv_corr_1x/B_corr_1x;
    else
        %angle=displ*mover_kl; displ_max=1mm
        corr_ampl_max(1,1)=1e-3*handles.xmover_KL(handles.corr_1x_list-handles.nxcor);
    end
    if(handles.corr_2x_list<=handles.nxcor)
        %I=B*PS.Ampl/conv; Ampl=theta; PS_max=5V
        conv_corr_2x= FL.HwInfo.PS(BEAMLINE{elem2}.PS).conv;
        B_corr_2x=BEAMLINE{elem2}.B;
        corr_ampl_max(2,1)=5*conv_corr_2x/B_corr_2x;
    else
        %angle=displ*mover_kl; displ_max=1mm
        corr_ampl_max(2,1)=1e-3*handles.xmover_KL(handles.corr_2x_list-handles.nxcor);
    end
    %we scan from -3*corr_ampl to 3*corr_ampl
    corr_x_ampl_sigma_max=min(abs(M0*TM_cor_bpm0*corr_ampl_max))/3;
    set(handles.corr_x_ampl_sigma_max_edit,'String',sprintf('%g',corr_x_ampl_sigma_max));
else
    if(handles.corr_1y_list<=handles.nycor)
        [s,R_cor1_bpm0]=RmatAtoB_cor(elem1,bpm0);
    else
        [s,R_cor1_bpm0]=RmatAtoB(elem1,bpm0);
    end
    if(handles.corr_2y_list<=handles.nycor)
        [s,R_cor2_bpm0]=RmatAtoB_cor(elem2,bpm0);
    else
        [s,R_cor2_bpm0]=RmatAtoB(elem2,bpm0);
    end
    betay0=FL.SimModel.Twiss.betay(bpm0);
    alphay0=FL.SimModel.Twiss.alphay(bpm0);
    TM_cor_bpm0=[R_cor1_bpm0(3,4) R_cor2_bpm0(3,4);
                 R_cor1_bpm0(4,4) R_cor2_bpm0(4,4)];
    M0=[1/sqrt(emity*betay0) 0; alphay0/sqrt(emity*betay0) sqrt(betay0/emity)];
    if(handles.corr_1y_list<=handles.nycor)
        %I=B*PS.Ampl/conv; Ampl=theta; PS_max=5V
        conv_corr_1y= FL.HwInfo.PS(BEAMLINE{elem1}.PS).conv;
        B_corr_1y=BEAMLINE{elem1}.B;
        corr_ampl_max(1,1)=5*conv_corr_1y/B_corr_1y;
    else
        %angle=displ*mover_kl; displ_max=1mm
        corr_ampl_max(1,1)=-1e-3*handles.ymover_KL(handles.corr_1y_list-handles.nycor);
    end
    if(handles.corr_2y_list<=handles.nycor)
        %I=B*PS.Ampl/conv; Ampl=theta; PS_max=5V
        conv_corr_2y= FL.HwInfo.PS(BEAMLINE{elem2}.PS).conv;
        B_corr_2y=BEAMLINE{elem2}.B;
        corr_ampl_max(2,1)=5*conv_corr_2y/B_corr_2y;
    else
        %angle=displ*mover_kl; displ_max=1mm
        corr_ampl_max(2,1)=-1e-3*handles.ymover_KL(handles.corr_2y_list-handles.nycor);
    end
    %we scan from -3*corr_ampl to 3*corr_ampl
    corr_y_ampl_sigma_max=min(abs(M0*TM_cor_bpm0*corr_ampl_max))/3;
    set(handles.corr_y_ampl_sigma_max_edit,'String',sprintf('%g',corr_y_ampl_sigma_max));
end

function [bpm0,elem1,elem2,elem1_name,elem2_name,corr_ampl_sigma]=get_bpm0(handles,plane,corr_1x_list,corr_2x_list,corr_1y_list,corr_2y_list)
if(~exist('plane'))
    plane=handles.plane;
end
if(~exist('corr_1x_list'))
    corr_1x_list=handles.corr_1x_list;
end
if(~exist('corr_2x_list'))
    corr_2x_list=handles.corr_2x_list;
end
if(~exist('corr_1y_list'))
    corr_1y_list=handles.corr_1y_list;
end
if(~exist('corr_2y_list'))
    corr_2y_list=handles.corr_2y_list;
end

if(strcmp(plane,'Horizontal'))
    if(corr_1x_list<=handles.nxcor)
        elem1=handles.xcor(corr_1x_list);
        elem1_name=strtrim(handles.xcor_name(corr_1x_list,:));
    else
        elem1=handles.q_on_mover(corr_1x_list-handles.nxcor,1);
        elem1_name=[strtrim(handles.mover_name(corr_1x_list-handles.nxcor,:)) 'X'];
    end
    if(corr_2x_list<=handles.nxcor)
        elem2=handles.xcor(corr_2x_list);
        elem2_name=strtrim(handles.xcor_name(corr_2x_list,:));
    else
        elem2=handles.q_on_mover(corr_2x_list-handles.nxcor,1);
        elem2_name=[strtrim(handles.mover_name(corr_2x_list-handles.nxcor,:)) 'X'];
    end
    set(handles.corr_1y_menu,'Enable','off');
    set(handles.corr_2y_menu,'Enable','off');
    set(handles.corr_y_ampl_sigma_edit,'Enable','off');
    set(handles.corr_1x_menu,'Enable','on');
    set(handles.corr_2x_menu,'Enable','on');
    set(handles.corr_x_ampl_sigma_edit,'Enable','on');
    corr_ampl_sigma=handles.corr_x_ampl_sigma;
else
    if(corr_1y_list<=handles.nycor)
        elem1=handles.ycor(corr_1y_list);
        elem1_name=strtrim(handles.ycor_name(corr_1y_list,:));
    else
        elem1=handles.q_on_mover(corr_1y_list-handles.nycor,1);
        elem1_name=[strtrim(handles.mover_name(corr_1y_list-handles.nycor,:)) 'Y'];
    end
    if(corr_2y_list<=handles.nycor)
        elem2=handles.ycor(corr_2y_list);
        elem2_name=strtrim(handles.ycor_name(corr_2y_list,:));
    else
        elem2=handles.q_on_mover(corr_2y_list-handles.nycor,1);
        elem2_name=[strtrim(handles.mover_name(corr_2y_list-handles.nycor,:)) 'Y'];
    end
    set(handles.corr_1x_menu,'Enable','off');
    set(handles.corr_2x_menu,'Enable','off');
    set(handles.corr_x_ampl_sigma_edit,'Enable','off');
    set(handles.corr_1y_menu,'Enable','on');
    set(handles.corr_2y_menu,'Enable','on');
    set(handles.corr_y_ampl_sigma_edit,'Enable','on');
    corr_ampl_sigma=handles.corr_y_ampl_sigma;
end    
bpm0=min(handles.bpm(handles.bpm>max([elem1 elem2])));

bpm_0=min(find(handles.bpm==bpm0));
if (corr_1x_list>handles.nxcor && any(bpm0==handles.elem_on_mover{corr_1x_list-handles.nxcor}))
    bpm0=handles.bpm(bpm_0+1);
    bpm_0=min(find(handles.bpm==bpm0));
end
if (corr_2x_list>handles.nxcor && any(bpm0==handles.elem_on_mover{corr_2x_list-handles.nxcor}))
    bpm0=handles.bpm(bpm_0+1);
    bpm_0=min(find(handles.bpm==bpm0));
end
if (corr_1y_list>handles.nycor && any(bpm0==handles.elem_on_mover{corr_1y_list-handles.nycor}))
    bpm0=handles.bpm(bpm_0+1);
    bpm_0=min(find(handles.bpm==bpm0));
end
if (corr_2y_list>handles.nycor && any(bpm0==handles.elem_on_mover{corr_2y_list-handles.nycor}))
    bpm0=handles.bpm(bpm_0+1);
    bpm_0=min(find(handles.bpm==bpm0));
end

set(handles.bpm0_edit,'String',sprintf('%s',handles.bpm_name{handles.bpm==bpm0}));

function corr_ampl=get_cor_ampl(handles)
global FL
[bpm0,elem1,elem2]=get_bpm0(handles);
emitx=2e-9;
emity=30e-12;
if(strcmp(handles.plane,'Horizontal'))
    if(handles.corr_1x_list<=handles.nxcor)
        [s,R_cor1_bpm0]=RmatAtoB_cor(elem1,bpm0);
    else
        [s,R_cor1_bpm0]=RmatAtoB(elem1,bpm0);
    end
    if(handles.corr_2x_list<=handles.nxcor)
        [s,R_cor2_bpm0]=RmatAtoB_cor(elem2,bpm0);
    else
        [s,R_cor2_bpm0]=RmatAtoB(elem2,bpm0);
    end
    betax0=FL.SimModel.Twiss.betax(bpm0);
    alphax0=FL.SimModel.Twiss.alphax(bpm0);
    TM_cor_bpm0=[R_cor1_bpm0(1,2) R_cor2_bpm0(1,2);
                 R_cor1_bpm0(2,2) R_cor2_bpm0(2,2)];
    M0=[1/sqrt(emitx*betax0) 0; alphax0/sqrt(emitx*betax0) sqrt(betax0/emitx)];
    corr_ampl=(M0*TM_cor_bpm0)\[handles.corr_x_ampl_sigma;handles.corr_x_ampl_sigma];
else
    if(handles.corr_1y_list<=handles.nycor)
        [s,R_cor1_bpm0]=RmatAtoB_cor(elem1,bpm0);
    else
        [s,R_cor1_bpm0]=RmatAtoB(elem1,bpm0);
    end
    if(handles.corr_2y_list<=handles.nycor)
        [s,R_cor2_bpm0]=RmatAtoB_cor(elem2,bpm0);
    else
        [s,R_cor2_bpm0]=RmatAtoB(elem2,bpm0);
    end
    betay0=FL.SimModel.Twiss.betay(bpm0);
    alphay0=FL.SimModel.Twiss.alphay(bpm0);
    TM_cor_bpm0=[R_cor1_bpm0(3,4) R_cor2_bpm0(3,4);
                 R_cor1_bpm0(4,4) R_cor2_bpm0(4,4)];
    M0=[1/sqrt(emity*betay0) 0; alphay0/sqrt(emity*betay0) sqrt(betay0/emity)];
    corr_ampl=(M0*TM_cor_bpm0)\[handles.corr_y_ampl_sigma;handles.corr_y_ampl_sigma];
end

function corr_ampl=get_cor_ampl_wanted(elem1,elem2,bpm0,plane,corr_ampl_sigma,FL_SimModel)
emitx=2e-9;
emity=30e-12;
if(strcmp(plane,'Horizontal'))
    [s,R_cor1_bpm0]=RmatAtoB_cor(elem1,bpm0);
    [s,R_cor2_bpm0]=RmatAtoB_cor(elem2,bpm0);
    betax0=FL_SimModel.Twiss.betax(bpm0);
    alphax0=FL_SimModel.Twiss.alphax(bpm0);
    TM_cor_bpm0=[R_cor1_bpm0(1,2) R_cor2_bpm0(1,2);
                 R_cor1_bpm0(2,2) R_cor2_bpm0(2,2)];
    M0=[1/sqrt(emitx*betax0) 0; alphax0/sqrt(emitx*betax0) sqrt(betax0/emitx)];
    corr_ampl=(M0*TM_cor_bpm0)\[corr_ampl_sigma;corr_ampl_sigma];
else
    [s,R_cor1_bpm0]=RmatAtoB_cor(elem1,bpm0);
    [s,R_cor2_bpm0]=RmatAtoB_cor(elem2,bpm0);
    betay0=FL_SimModel.Twiss.betay(bpm0);
    alphay0=FL_SimModel.Twiss.alphay(bpm0);
    TM_cor_bpm0=[R_cor1_bpm0(3,4) R_cor2_bpm0(3,4);
                 R_cor1_bpm0(4,4) R_cor2_bpm0(4,4)];
    M0=[1/sqrt(emity*betay0) 0; alphay0/sqrt(emity*betay0) sqrt(betay0/emity)];
    corr_ampl=(M0*TM_cor_bpm0)\[corr_ampl_sigma;corr_ampl_sigma];
end

% --- Executes on button press in grid_squaring_btn.
function grid_squaring_btn_Callback(hObject, eventdata, handles)
% hObject    handle to grid_squaring_btn (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of grid_squaring_btn
handles.grid_squaring=get(hObject,'Value');
guidata(hObject, handles);


% --- Executes on selection change in filename_load_popup.
function filename_load_popup_Callback(hObject, eventdata, handles)
% hObject    handle to filename_load_popup (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns filename_load_popup contents as cell array
%        contents{get(hObject,'Value')} returns selected item from filename_load_popup
contents = cellstr(get(hObject,'String'));
handles.filename_load = contents{get(hObject,'Value')};
guidata(hObject, handles);

% --- Executes during object creation, after setting all properties.
function filename_load_popup_CreateFcn(hObject, eventdata, handles)
% hObject    handle to filename_load_popup (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function corr_y_dampl_edit_Callback(hObject, eventdata, handles)
% hObject    handle to corr_y_dampl_edit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of corr_y_dampl_edit as text
%        str2double(get(hObject,'String')) returns contents of corr_x_dampl_edit as a double


% --- Executes during object creation, after setting all properties.
function corr_y_dampl_edit_CreateFcn(hObject, eventdata, handles)
% hObject    handle to corr_y_dampl_edit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function corr_x_dampl_edit_Callback(hObject, eventdata, handles)
% hObject    handle to corr_x_dampl_edit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of corr_x_dampl_edit as text
%        str2double(get(hObject,'String')) returns contents of corr_x_dampl_edit as a double


function corr_x_dampl_edit_CreateFcn(hObject, eventdata, handles)
% hObject    handle to corr_x_dampl_edit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function bpmave_edit_Callback(hObject, eventdata, handles)
% hObject    handle to bpmave_edit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of bpmave_edit as text
%        str2double(get(hObject,'String')) returns contents of bpmave_edit as a double
handles.bpmave=str2double(get(hObject,'String'));
guidata(hObject, handles);

% --- Executes during object creation, after setting all properties.
function bpmave_edit_CreateFcn(hObject, eventdata, handles)
% hObject    handle to bpmave_edit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function bpmsvd_edit_Callback(hObject, eventdata, handles)
% hObject    handle to bpmsvd_edit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of bpmsvd_edit as text
%        str2double(get(hObject,'String')) returns contents of bpmsvd_edit as a double
handles.bpmsvd=str2double(get(hObject,'String'));
guidata(hObject, handles);


% --- Executes during object creation, after setting all properties.
function bpmsvd_edit_CreateFcn(hObject, eventdata, handles)
% hObject    handle to bpmsvd_edit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function [sig xbar ybar ccdData]=setup(wcam)


% Setup parameters
PVName='mOTR';
camName=sprintf('cam%d',wcam);
imageName=sprintf('image%d',wcam);
pars.sizeX=1280;
pars.sizeY=960;
pars.exposureTime=0.3; % s
pars.acquirePeriod=1; % s
pars.triggerMode=2; % 0 = free running, 1:4=syncIn 1-4
 pars.imageMode=0; % 0=single, 1=multiple, 2=continuous
pars.gain=24.0;
acquireRate=1.56; % Hz

% Send setup parameters to device
camSetup(pars,PVName,camName,imageName);

%% =====================
% Cam Setup function
function camSetup(pars,PVName,camName,imageName)
lcaPut([PVName,':',camName,':SizeX'],pars.sizeX);
lcaPut([PVName,':',camName,':SizeY'],pars.sizeY);
lcaPut([PVName,':',camName,':AcquireTime'],pars.exposureTime);
lcaPut([PVName,':',camName,':AcquirePeriod'],pars.acquirePeriod);
lcaPut([PVName,':',camName,':TriggerMode'],pars.triggerMode);
lcaPut([PVName,':',camName,':ImageMode'],pars.imageMode);
lcaPut([PVName,':',camName,':Gain'],pars.gain);
% Set camera to acquire 12-bit data (as opposed to 8-bit) 
lcaPut([PVName,':',camName,':DataType'],1);
% Set NDStdArrays plugin to enable array callbacks (so new data becomes
% availbale to use through ArrayData record)
lcaPut([PVName,':',imageName,':EnableCallbacks'],1);
lcaPut([PVName,':',camName,':ArrayCallbacks'],1);
lcaPut([PVName,':',camName,':AutoSave'],0);













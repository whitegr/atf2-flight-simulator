function varargout = rotcal(varargin)
% ROTCAL MATLAB code for rotcal.fig
%      ROTCAL, by itself, creates a new ROTCAL or raises the existing
%      singleton*.
%
%      H = ROTCAL returns the handle to a new ROTCAL or the handle to
%      the existing singleton*.
%
%      ROTCAL('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in ROTCAL.M with the given input arguments.
%
%      ROTCAL('Property','Value',...) creates a new ROTCAL or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before rotcal_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to rotcal_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help rotcal

% Last Modified by GUIDE v2.5 11-May-2013 14:22:56

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @rotcal_OpeningFcn, ...
                   'gui_OutputFcn',  @rotcal_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before rotcal is made visible.
function rotcal_OpeningFcn(hObject, eventdata, handles, varargin) %#ok<*INUSL>
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to rotcal (see VARARGIN)

% Choose default command line output for rotcal
handles.output = handles;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes rotcal wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = rotcal_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on button press in togglebutton1.
function togglebutton1_Callback(hObject, eventdata, handles) %#ok<*INUSD,*DEFNU>
% hObject    handle to togglebutton1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of togglebutton1


% --- Executes on button press in pushbutton1.
function pushbutton1_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

acqData(handles,'clear');

function acqData(handles,data)
% data: [x y] centroid of transverse bunch profile (um)
persistent dataStore firstcall

if ~get(handles.togglebutton1,'Value'); return; end;

if ischar(data) && strcmp(data,'clear')
  dataStore=[];
  cla(handles.axes1);
  return
else
  dataStore=[dataStore; data];
end

if length(dataStore)>5
  [q,dq]=noplot_polyfit(dataStore(:,1)',dataStore(:,2)',1,1);
  plot(handles.axes1,dataStore(:,1),dataStore(:,2),'bx','MarkerSize',12);
  hold(handles.axes1,'on')
  plot(handles.axes1,dataStore(:,1),q(1)+q(2).*dataStore(:,1),'r')
  hold(handles.axes1,'off')
  axis(handles.axes1,'tight');
  ax=axis(handles.axes1);
  if abs(ax(1))<abs(ax(3))
    ax(3)=ax(1);
  else
    ax(1)=ax(3);
  end
  if abs(ax(2))>abs(ax(4))
    ax(4)=ax(2);
  else
    ax(2)=ax(4);
  end
  axis(handles.axes1,ax)
  set(handles.text1,'String',sprintf('%.1f +/- %.1f degrees',...
    atand(q(2)),sqrt(dq(2)^2*((1/1+q(2)^2))^2)));
end

if isempty(firstcall)
  xlabel(handles.axes1,'X Centroid / um')
  ylabel(handles.axes1,'Y Centroid / um')
  firstcall=true;
end


% --- Executes when user attempts to close figure1.
function figure1_CloseRequestFcn(hObject, eventdata, handles)
% hObject    handle to figure1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global FL
% Hint: delete(hObject) closes the figure
try
  acqData(handles,'clear');
  FL.Gui=rmfield(FL.Gui,'rotcal');
catch
end
delete(hObject);

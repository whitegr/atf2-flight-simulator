function [stat, varargout]=fsCom(cmd,varargin)
% FSCOM - Communications with Flight Simulator
%
% stat{1}=1 for success, otherwise error reported by server in stat{2}
%
% [stat rmat0 rmat1 rmat2 rmat3] = fsCom('GetRMat')
%   Get 6*6 response matrices from OTR0X -> OTR0X:OTR3X (rmat0 - rmat3)
%
% [stat orbitData] = fsCom('OrbitFit',iotr)
%   Get beam orbit projection to OTR iotr (0,1,2,3)
%   orbitData = [x y] orbit fits in um
%
% [stat ictData] = fsCom('GetICT')
%   Get current ICTDUMP data
%
% [stat disp disp_err] = fsCom('DispFit')
%   Get fitted values for dispersion at OTR0X - OTR3X locations from
%   direct BPM dispersion measurement from extDispersion app. A dispersion
%   measurement with extDispersion must have been taken and saved first.
%   Also, the extDispersion App must be closed before issuing this command
%   to work
%    disp = 4 * 4 array [disp_x; disp_x'; disp_y; disp_y'] * 4 OTRs
%    disp_err = RMS errors on above
%
% [stat data] = fsCom('getEmit',otruse,type)
%   Get emittance data from otr results generated in emittance.m routine
%   otruse = 1*4 logical vector of OTRs to use in calculation, must use at
%            least 3
%   type = 'intrinsic' or 'projected'
persistent otrInd bpmInd

% Initialise output status
stat{1}=1;
if nargout>1
  for iarg=1:nargout-1
    varargout{iarg}=[];
  end
end

% Wait for server to be free
maxWait=20; %s
t0=clock;
try
  while ~strcmp(lcaGet('FSECS:command'),' ')
    pause(0.1)
    if etime(clock,t0)>maxWait
      stat{1}=-1; stat{2}='Timeout waiting for server to become ready'; return;
    end
  end
catch
  error('Cannot communicate with ATF EPICS IOC')
end

% Get OTR BEAMLINE indices
if isempty(otrInd)
  for iotr=1:4
    lcaPut('FSECS:strCmd',sprintf('OTR%dX',iotr-1));
    [stat, ret]=getResp('GetIndByName','valResp');
    if stat{1}~=1; return; end;
    otrInd(iotr)=ret;
  end
end

% Get list of BPM indices for use in OTR orbit projection fitting
if isempty(bpmInd)
  bpmNameList={'MQD16X' 'MQF17X' 'MQD18X' 'MQF19X' 'MQD20X' 'MQF21X' 'MQM16FF'};
  for ibpm=1:length(bpmNameList)
    lcaPut('FSECS:strCmd',bpmNameList{ibpm});
    [stat, ret]=getResp('GetIndByName','valResp');
    if stat{1}~=1; return; end;
    bpmInd(ibpm)=ret;
  end
end

% Process desired command
switch lower(cmd)
  case 'getemit'
    if nargin<3
      stat{1}=-1;
      stat{2}='Need 3 input arguments to getemit';
      return
    end
    lcaPut('FSECS:arrCmd',varargin{1});
    lcaPut('FSECS:strCmd',varargin{2});
    [stat, ret]=getResp('GetOTREmitData','arrResp');
    if stat{1}~=1; return; end;
    varargout{1}=ret;
  case 'correctcoupling'
   if nargin<2
      stat{1}=-1;
      stat{2}='Need 2 input arguments to correctcoupling';
      return
   end
    if varargin{2}==1
      lcaPut('FSECS:strCmd','model')
    elseif varargin{2}==2
      lcaPut('FSECS:strCmd','modelmin')
    elseif varargin{2}==3
      lcaPut('FSECS:strCmd','apply')
    end
    if varargin{2}==1 || varargin{2}==2
      mhan=msgbox('Calculating correction on Flight Simulator, please wait...');
      drawnow('expose')
    end
    lcaPut('FSECS:arrCmd',varargin{1});
    for itry=1:5
      [stat, ret]=getResp('correctcoupling','arrResp');
      if stat{1}==1; break; end;
    end
    if varargin{2}==1 || varargin{2}==2
      if ishandle(mhan); delete(mhan); end;
    end
    if stat{1}~=1; return; end;
    varargout{1}=ret;
  case 'getrmat'
    for iotr=1:4
      lcaPut('FSECS:arrCmd',[otrInd(1) otrInd(iotr)]);
      [stat, ret]=getResp('GetRmat','arrResp');
      if stat{1}~=1; return; end;
      varargout{iotr}=reshape(ret(1:36),6,6);
    end
  case 'orbitfit'
    if nargin<2
      error('Too few arguments')
    end
    lcaPut('FSECS:valCmd',varargin{1});
    [stat, ret]=getResp('otrOrbitFit','arrResp');
    if stat{1}~=1; varargout{1}=0; return; end;
    varargout{1}=ret.*1e6;
  case 'dispfit'
    lcaPut('FSECS:arrCmd',otrInd);
    [stat, ret]=getResp('GetDispFit','arrResp');
    if stat{1}~=1; return; end;
    varargout{1}=reshape(ret(1:16),4,4);
    varargout{2}=reshape(ret(17:32),4,4);
  case 'getict'
    lcaPut('FSECS:strCmd','ICTDUMP');
    lcaPut('FSECS:valCmd',1);
    [stat, ret]=getResp('GetICT','arrResp');
    if stat{1}~=1; varargout{1}=0; return; end;
    varargout{1}=ret(1);
  case 'getmagnet'
    lcaPut('FSECS:strCmd',varargin{1});
    [stat, resp]=getResp('GetMagnetI','valResp');
    varargout{1}=resp(1);
  case 'setmagnet'
    lcaPut('FSECS:strCmd',varargin{1});
    lcaPut('FSECS:valCmd',varargin{2});
    stat=getResp('SetMagnetI','valResp');
  case 'doextmatch'
    lcaPut('FSECS:strCmd','domatch');
    [stat, resp]=getResp('extmatch','arrResp');
    if stat{1}==1
      varargout{1}=resp(1:4);
      varargout{2}=resp(5:8);
      varargout{3}=resp(9:17);
      varargout{4}=resp(18:26);
    else
      varargout{1}=[]; varargout{2}=[]; varargout{3}=[]; varargout{4}=[];
    end
  case 'trimtoextmatch'
    lcaPut('FSECS:strCmd','dotrim');
    stat=getResp('extmatch');
  otherwise
    error('unknown command')
end

% Get response from server
function [stat, resp]=getResp(cmd,wResp)
stat{1}=1; resp=[];
lcaPut('FSECS:command',cmd);
maxWait=60; %s
t0=clock;
while ~strcmp(lcaGet('FSECS:command'),' ')
  if etime(clock,t0)>maxWait
    stat{1}=-1; stat{2}='Timeout waiting for server to respond'; return;
  end
end
stat{1}=lcaGet('FSECS:stat');
if stat{1}==1
  if exist('wResp','var')
    resp=lcaGet(sprintf('FSECS:%s',wResp));
  end
else
  stat{2}=lcaGet('FSECS:errStr');
end
function varargout = reference_voltages(varargin)
% REFERENCE_VOLTAGES MATLAB code for reference_voltages.fig
%      REFERENCE_VOLTAGES, by itself, creates a new REFERENCE_VOLTAGES or raises the existing
%      singleton*.
%
%      H = REFERENCE_VOLTAGES returns the handle to a new REFERENCE_VOLTAGES or the handle to
%      the existing singleton*.
%
%      REFERENCE_VOLTAGES('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in REFERENCE_VOLTAGES.M with the given input arguments.
%
%      REFERENCE_VOLTAGES('Property','Value',...) creates a new REFERENCE_VOLTAGES or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before reference_voltages_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to reference_voltages_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help reference_voltages

% Last Modified by GUIDE v2.5 11-Nov-2010 20:50:39

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @reference_voltages_OpeningFcn, ...
                   'gui_OutputFcn',  @reference_voltages_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before reference_voltages is made visible.
function reference_voltages_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to reference_voltages (see VARARGIN)

% Choose default command line output for reference_voltages
handles.output = hObject;

handles.singlepanel=varargin{1};
handles.rv= load('reference_voltages.dat', 'rv', '-ascii');

for i=1:4
    for j=2:7
       set(handles.(sprintf('rv%d%d',i,j)),'String',num2str(handles.rv(i,j)));
    end
end


% Update handles structure
guidata(hObject, handles);

% UIWAIT makes reference_voltages wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = reference_voltages_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;



function rv13_Callback(hObject, eventdata, handles)
% hObject    handle to rv13 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of rv13 as text
%        str2double(get(hObject,'String')) returns contents of rv13 as a double
temp = str2double(get(hObject, 'String'));
if isnan(temp)
    set(hObject, 'String', 0);
    errordlg('Input must be a number','Error');
end
handles.rv(1,3) = temp;
guidata(gcbo,handles)


% --- Executes during object creation, after setting all properties.
function rv13_CreateFcn(hObject, eventdata, handles)
% hObject    handle to rv13 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function rv16_Callback(hObject, eventdata, handles)
% hObject    handle to rv16 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of rv16 as text
%        str2double(get(hObject,'String')) returns contents of rv16 as a double
temp = str2double(get(hObject, 'String'));
if isnan(temp)
    set(hObject, 'String', 0);
    errordlg('Input must be a number','Error');
end
handles.rv(1,6) = temp;
guidata(gcbo,handles)


% --- Executes during object creation, after setting all properties.
function rv16_CreateFcn(hObject, eventdata, handles)
% hObject    handle to rv16 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function rv23_Callback(hObject, eventdata, handles)
% hObject    handle to rv23 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of rv23 as text
%        str2double(get(hObject,'String')) returns contents of rv23 as a double
temp = str2double(get(hObject, 'String'));
if isnan(temp)
    set(hObject, 'String', 0);
    errordlg('Input must be a number','Error');
end
handles.rv(2,3) = temp;
guidata(gcbo,handles)


% --- Executes during object creation, after setting all properties.
function rv23_CreateFcn(hObject, eventdata, handles)
% hObject    handle to rv23 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function rv26_Callback(hObject, eventdata, handles)
% hObject    handle to rv26 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of rv26 as text
%        str2double(get(hObject,'String')) returns contents of rv26 as a double
temp = str2double(get(hObject, 'String'));
if isnan(temp)
    set(hObject, 'String', 0);
    errordlg('Input must be a number','Error');
end
handles.rv(2,6) = temp;
guidata(gcbo,handles)


% --- Executes during object creation, after setting all properties.
function rv26_CreateFcn(hObject, eventdata, handles)
% hObject    handle to rv26 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function rv33_Callback(hObject, eventdata, handles)
% hObject    handle to rv33 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of rv33 as text
%        str2double(get(hObject,'String')) returns contents of rv33 as a double
temp = str2double(get(hObject, 'String'));
if isnan(temp)
    set(hObject, 'String', 0);
    errordlg('Input must be a number','Error');
end
handles.rv(3,3) = temp;
guidata(gcbo,handles)


% --- Executes during object creation, after setting all properties.
function rv33_CreateFcn(hObject, eventdata, handles)
% hObject    handle to rv33 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function rv36_Callback(hObject, eventdata, handles)
% hObject    handle to rv36 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of rv36 as text
%        str2double(get(hObject,'String')) returns contents of rv36 as a double
temp = str2double(get(hObject, 'String'));
if isnan(temp)
    set(hObject, 'String', 0);
    errordlg('Input must be a number','Error');
end
handles.rv(3,6) = temp;
guidata(gcbo,handles)


% --- Executes during object creation, after setting all properties.
function rv36_CreateFcn(hObject, eventdata, handles)
% hObject    handle to rv36 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function rv43_Callback(hObject, eventdata, handles)
% hObject    handle to rv43 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of rv43 as text
%        str2double(get(hObject,'String')) returns contents of rv43 as a double
temp = str2double(get(hObject, 'String'));
if isnan(temp)
    set(hObject, 'String', 0);
    errordlg('Input must be a number','Error');
end
handles.rv(4,3) = temp;
guidata(gcbo,handles)


% --- Executes during object creation, after setting all properties.
function rv43_CreateFcn(hObject, eventdata, handles)
% hObject    handle to rv43 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function rv46_Callback(hObject, eventdata, handles)
% hObject    handle to rv46 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of rv46 as text
%        str2double(get(hObject,'String')) returns contents of rv46 as a double
temp = str2double(get(hObject, 'String'));
if isnan(temp)
    set(hObject, 'String', 0);
    errordlg('Input must be a number','Error');
end
handles.rv(4,6) = temp;
guidata(gcbo,handles)


% --- Executes during object creation, after setting all properties.
function rv46_CreateFcn(hObject, eventdata, handles)
% hObject    handle to rv46 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function rv12_Callback(hObject, eventdata, handles)
% hObject    handle to rv12 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of rv12 as text
%        str2double(get(hObject,'String')) returns contents of rv12 as a double
temp = str2double(get(hObject, 'String'));
if isnan(temp)
    set(hObject, 'String', 0);
    errordlg('Input must be a number','Error');
end
handles.rv(1,2) = temp;
guidata(gcbo,handles)


% --- Executes during object creation, after setting all properties.
function rv12_CreateFcn(hObject, eventdata, handles)
% hObject    handle to rv12 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function rv15_Callback(hObject, eventdata, handles)
% hObject    handle to rv15 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of rv15 as text
%        str2double(get(hObject,'String')) returns contents of rv15 as a double
temp = str2double(get(hObject, 'String'));
if isnan(temp)
    set(hObject, 'String', 0);
    errordlg('Input must be a number','Error');
end
handles.rv(1,5) = temp;
guidata(gcbo,handles)


% --- Executes during object creation, after setting all properties.
function rv15_CreateFcn(hObject, eventdata, handles)
% hObject    handle to rv15 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function rv22_Callback(hObject, eventdata, handles)
% hObject    handle to rv22 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of rv22 as text
%        str2double(get(hObject,'String')) returns contents of rv22 as a double
temp = str2double(get(hObject, 'String'));
if isnan(temp)
    set(hObject, 'String', 0);
    errordlg('Input must be a number','Error');
end
handles.rv(2,2) = temp;
guidata(gcbo,handles)


% --- Executes during object creation, after setting all properties.
function rv22_CreateFcn(hObject, eventdata, handles)
% hObject    handle to rv22 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function rv25_Callback(hObject, eventdata, handles)
% hObject    handle to rv25 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of rv25 as text
%        str2double(get(hObject,'String')) returns contents of rv25 as a double
temp = str2double(get(hObject, 'String'));
if isnan(temp)
    set(hObject, 'String', 0);
    errordlg('Input must be a number','Error');
end
handles.rv(2,5) = temp;
guidata(gcbo,handles)


% --- Executes during object creation, after setting all properties.
function rv25_CreateFcn(hObject, eventdata, handles)
% hObject    handle to rv25 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function rv32_Callback(hObject, eventdata, handles)
% hObject    handle to rv32 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of rv32 as text
%        str2double(get(hObject,'String')) returns contents of rv32 as a double
temp = str2double(get(hObject, 'String'));
if isnan(temp)
    set(hObject, 'String', 0);
    errordlg('Input must be a number','Error');
end
handles.rv(3,2) = temp;
guidata(gcbo,handles)


% --- Executes during object creation, after setting all properties.
function rv32_CreateFcn(hObject, eventdata, handles)
% hObject    handle to rv32 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function rv35_Callback(hObject, eventdata, handles)
% hObject    handle to rv35 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of rv35 as text
%        str2double(get(hObject,'String')) returns contents of rv35 as a double
temp = str2double(get(hObject, 'String'));
if isnan(temp)
    set(hObject, 'String', 0);
    errordlg('Input must be a number','Error');
end
handles.rv(3,5) = temp;
guidata(gcbo,handles)


% --- Executes during object creation, after setting all properties.
function rv35_CreateFcn(hObject, eventdata, handles)
% hObject    handle to rv35 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function rv42_Callback(hObject, eventdata, handles)
% hObject    handle to rv42 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of rv42 as text
%        str2double(get(hObject,'String')) returns contents of rv42 as a double
temp = str2double(get(hObject, 'String'));
if isnan(temp)
    set(hObject, 'String', 0);
    errordlg('Input must be a number','Error');
end
handles.rv(4,2) = temp;
guidata(gcbo,handles)


% --- Executes during object creation, after setting all properties.
function rv42_CreateFcn(hObject, eventdata, handles)
% hObject    handle to rv42 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function rv45_Callback(hObject, eventdata, handles)
% hObject    handle to rv45 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of rv45 as text
%        str2double(get(hObject,'String')) returns contents of rv45 as a double
temp = str2double(get(hObject, 'String'));
if isnan(temp)
    set(hObject, 'String', 0);
    errordlg('Input must be a number','Error');
end
handles.rv(4,5) = temp;
guidata(gcbo,handles)


% --- Executes during object creation, after setting all properties.
function rv45_CreateFcn(hObject, eventdata, handles)
% hObject    handle to rv45 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function rv14_Callback(hObject, eventdata, handles)
% hObject    handle to rv14 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of rv14 as text
%        str2double(get(hObject,'String')) returns contents of rv14 as a double
temp = str2double(get(hObject, 'String'));
if isnan(temp)
    set(hObject, 'String', 0);
    errordlg('Input must be a number','Error');
end
handles.rv(1,4) = temp;
guidata(gcbo,handles)


% --- Executes during object creation, after setting all properties.
function rv14_CreateFcn(hObject, eventdata, handles)
% hObject    handle to rv14 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function rv17_Callback(hObject, eventdata, handles)
% hObject    handle to rv17 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of rv17 as text
%        str2double(get(hObject,'String')) returns contents of rv17 as a double
temp = str2double(get(hObject, 'String'));
if isnan(temp)
    set(hObject, 'String', 0);
    errordlg('Input must be a number','Error');
end
handles.rv(1,7) = temp;
guidata(gcbo,handles)


% --- Executes during object creation, after setting all properties.
function rv17_CreateFcn(hObject, eventdata, handles)
% hObject    handle to rv17 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function rv24_Callback(hObject, eventdata, handles)
% hObject    handle to rv24 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of rv24 as text
%        str2double(get(hObject,'String')) returns contents of rv24 as a double
temp = str2double(get(hObject, 'String'));
if isnan(temp)
    set(hObject, 'String', 0);
    errordlg('Input must be a number','Error');
end
handles.rv(2,4) = temp;
guidata(gcbo,handles)


% --- Executes during object creation, after setting all properties.
function rv24_CreateFcn(hObject, eventdata, handles)
% hObject    handle to rv24 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function rv27_Callback(hObject, eventdata, handles)
% hObject    handle to rv27 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of rv27 as text
%        str2double(get(hObject,'String')) returns contents of rv27 as a double
temp = str2double(get(hObject, 'String'));
if isnan(temp)
    set(hObject, 'String', 0);
    errordlg('Input must be a number','Error');
end
handles.rv(2,7) = temp;
guidata(gcbo,handles)


% --- Executes during object creation, after setting all properties.
function rv27_CreateFcn(hObject, eventdata, handles)
% hObject    handle to rv27 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function rv34_Callback(hObject, eventdata, handles)
% hObject    handle to rv34 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of rv34 as text
%        str2double(get(hObject,'String')) returns contents of rv34 as a double
temp = str2double(get(hObject, 'String'));
if isnan(temp)
    set(hObject, 'String', 0);
    errordlg('Input must be a number','Error');
end
handles.rv(3,4) = temp;
guidata(gcbo,handles)


% --- Executes during object creation, after setting all properties.
function rv34_CreateFcn(hObject, eventdata, handles)
% hObject    handle to rv34 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function rv37_Callback(hObject, eventdata, handles)
% hObject    handle to rv37 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of rv37 as text
%        str2double(get(hObject,'String')) returns contents of rv37 as a double
temp = str2double(get(hObject, 'String'));
if isnan(temp)
    set(hObject, 'String', 0);
    errordlg('Input must be a number','Error');
end
handles.rv(3,7) = temp;
guidata(gcbo,handles)


% --- Executes during object creation, after setting all properties.
function rv37_CreateFcn(hObject, eventdata, handles)
% hObject    handle to rv37 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function rv44_Callback(hObject, eventdata, handles)
% hObject    handle to rv44 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of rv44 as text
%        str2double(get(hObject,'String')) returns contents of rv44 as a double
temp = str2double(get(hObject, 'String'));
if isnan(temp)
    set(hObject, 'String', 0);
    errordlg('Input must be a number','Error');
end
handles.rv(4,4) = temp;
guidata(gcbo,handles)


% --- Executes during object creation, after setting all properties.
function rv44_CreateFcn(hObject, eventdata, handles)
% hObject    handle to rv44 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function rv47_Callback(hObject, eventdata, handles) %#ok<*INUSL,*DEFNU>
% hObject    handle to rv47 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of rv47 as text
%        str2double(get(hObject,'String')) returns contents of rv47 as a double
temp = str2double(get(hObject, 'String'));
if isnan(temp)
    set(hObject, 'String', 0);
    errordlg('Input must be a number','Error');
end
handles.rv(4,7) = temp;
guidata(gcbo,handles)


% --- Executes during object creation, after setting all properties.
function rv47_CreateFcn(hObject, eventdata, handles) %#ok<*INUSD>
% hObject    handle to rv47 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes when user attempts to close figure1.
function figure1_CloseRequestFcn(hObject, eventdata, handles)
% hObject    handle to figure1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

try
  rv=handles.rv;
  save -ASCII reference_voltages.dat rv
  setappdata(handles.singlepanel.single_otr_general_panel,'reference_voltages',handles.rv)
  delete(hObject);
catch
  delete(hObject);
end


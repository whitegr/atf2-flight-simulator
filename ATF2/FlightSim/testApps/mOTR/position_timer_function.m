
function position_timer_function (obj, event, arg1)

handles=arg1;

handles.wp = getappdata(handles.single_otr_general_panel,'working_point');
handles.cal = getappdata(handles.single_otr_general_panel,'calibration');

otr_numb=handles.otr_numb;


%-----------------------------------------------------------
%Changing target in/out color
if strcmp(get(eval(['handles.startGuiHandles.target_status_',num2str(otr_numb)]),'String'),'IN')
  set(handles.target_in,'BackgroundColor',[0.847 0.161 0])
  set(handles.target_out,'BackgroundColor',[0.702 0.702 0.702])
  set(handles.text_under_screen,'String','Target is IN')
  set(handles.text_under_screen,'BackgroundColor',[0.847 0.161 0])
  
elseif strcmp(get(eval(['handles.startGuiHandles.target_status_',num2str(otr_numb)]),'String'),'OUT')
  set(handles.target_out,'BackgroundColor',[0.749 0.749 0])
  set(handles.target_in,'BackgroundColor',[0.702 0.702 0.702])
  set(handles.text_under_screen,'String','Target is OUT')
  set(handles.text_under_screen,'BackgroundColor',[0.749 0.749 0])
end
%----------------------------------------------------------


%Reading Y, X/F pos of default OTR [pots] and converting x&y into um.
setappdata(handles.single_otr_general_panel,'RDBL',handles.RDBL);
if get(handles.x_switch,'Value')
  x=lcaGet(handles.RDBL{(1+3*otr_numb)});
  set(handles.xpos_read,'String',round((handles.cal(1+otr_numb,5)*(x-handles.wp(1+otr_numb,2)))));
  y=lcaGet(handles.RDBL{(2+3*otr_numb)});
  set(handles.ypos_read,'String',round(handles.cal(1+otr_numb,6)*(y-handles.wp(1+otr_numb,3))));
  set(handles.xpos_read_v,'String',x);
  set(handles.ypos_read_v,'String',y);
  
  if (x>(handles.off_mode(1+otr_numb,2)-0.01)&&(x<(handles.off_mode(1+otr_numb,2)+0.01))&&(y>(handles.off_mode(1+otr_numb,3)-0.01)&&(y<(handles.off_mode(1+otr_numb,3)+0.01))))
    set(handles.startGuiHandles.(sprintf('mode_status_%d',otr_numb)),'String','NonOTR')
    set(handles.startGuiHandles.(sprintf('mode_status_%d',otr_numb)),'BackgroundColor',[0.749 0.749 0.749])
%     set(handles.text_under_screen_mode,'String','NonOTR mode')
%     set(handles.text_under_screen_mode,'BackgroundColor',[0.749 0.749 0.749])
    %set(handles.startGuiHandles.(sprintf('mode_status_%d',otr_numb)),'String','NonOTR')
  elseif (x>(handles.wp(1+otr_numb,2)-0.01)&&(x<(handles.wp(1+otr_numb,2)+0.01))&&(y>(handles.wp(1+otr_numb,3)-0.01)&&(y<(handles.wp(1+otr_numb,3)+0.01))))
    set(handles.startGuiHandles.(sprintf('mode_status_%d',otr_numb)),'String','OTR')
    set(handles.startGuiHandles.(sprintf('mode_status_%d',otr_numb)),'BackgroundColor',[0.847 0.161 0])
%     set(handles.text_under_screen_mode,'String','OTR mode')
%     set(handles.text_under_screen_mode,'BackgroundColor',[0.847 0.161 0])
  elseif isnan(x) || isnan(y)
    set(handles.startGuiHandles.(sprintf('mode_status_%d',otr_numb)),'String','Error,NaN')
%     set(handles.text_under_screen_mode,'String','Error,NaN')
  else
    set(handles.startGuiHandles.(sprintf('mode_status_%d',otr_numb)),'String','OTR')
    set(handles.startGuiHandles.(sprintf('mode_status_%d',otr_numb)),'BackgroundColor',[0.847 0.161 0])
%     set(handles.text_under_screen_mode,'String','OTR mode')
%     set(handles.text_under_screen_mode,'BackgroundColor',[0.847 0.161 0])
  end
  
  %Reading & fixing x&y limits
  for j=[1 2] % for x and y motors
    if lcaNewMonitorValue(['mOTR:motor',num2str(j+(3*otr_numb)),':XPS_STATUS'])
      if lcaGet(['mOTR:motor',num2str(j+(3*otr_numb)),':XPS_STATUS'])==1
        setappdata(handles.single_otr_general_panel,'limit_switch_pressed',1)
        pause(1)
        if j==1
          dim='x';
          if lcaGet(handles.RDBL{(1+3*otr_numb)}) < 5 %voltages lower for positive direction
            pos_lim=0;
          else
            pos_lim=1;
          end
        else
          dim='y'; %voltages grow for positive direction
          if lcaGet(handles.RDBL{(2+3*otr_numb)}) > 5
            pos_lim=1;
          else
            pos_lim=0;
          end
        end
        
        
        for i=1:size(handles.limits_disable_tags,2)
          set(eval(handles.limits_disable_tags{i}),'Enable','off')
        end
        
        
        if pos_lim
          set(handles.text_under_screen,'String',[dim,' PLUS limit switched'])
          set(eval(['handles.clear_limit_',dim,'_max']),'Enable','on')
          set(eval(['handles.clear_limit_',dim,'_max']),'BackgroundColor',[1 0.6 0.784]);
          set(eval(['handles.startGuiHandles.limit_status_',num2str(otr_numb)]),'String',[dim,' PLUS'])
          set(eval(['handles.startGuiHandles.limit_status_',num2str(otr_numb)]),'BackgroundColor',[0.847 0.161 0]);
          pause(3)
        elseif ~pos_lim
          set(handles.text_under_screen,'String',[dim,' MINUS limit switched'])
          set(eval(['handles.clear_limit_',dim,'_min']),'Enable','on')
          set(eval(['handles.clear_limit_',dim,'_min']),'BackgroundColor',[1 0.6 0.784]);
          set(eval(['handles.startGuiHandles.limit_status_',num2str(otr_numb)]),'String',[dim,' MINUS'])
          set(eval(['handles.startGuiHandles.limit_status_',num2str(otr_numb)]),'BackgroundColor',[0.847 0.161 0]);
          pause(3)
        end
        pause(3)
        set(handles.text_under_screen,'String','')
      else
        set(eval('handles.clear_limit_y_max'),'Enable','off')
        set(eval('handles.clear_limit_y_min'),'Enable','off')
        set(eval('handles.clear_limit_y_min'),'BackGroundColor',[0.702 0.702 0.702])
        set(eval('handles.clear_limit_y_max'),'BackGroundColor',[0.702 0.702 0.702])
        set(eval('handles.clear_limit_x_max'),'Enable','off')
        set(eval('handles.clear_limit_x_min'),'Enable','off')
        set(eval('handles.clear_limit_x_min'),'BackGroundColor',[0.702 0.702 0.702])
        set(eval('handles.clear_limit_x_max'),'BackGroundColor',[0.702 0.702 0.702])
      end
    end
  end
  
  
  
else
  
  f=lcaGet(handles.RDBL{(3+3*otr_numb)});
  y=lcaGet(handles.RDBL{(2+3*otr_numb)});
  
  
  set(handles.ypos_read,'String',round((handles.cal(1+otr_numb,6)*(y-handles.wp(1+otr_numb,3)))));
  set(handles.fpos_read,'String',round((handles.cal(1+otr_numb,7)*(f-handles.wp(1+otr_numb,4)))));
  
  set(handles.ypos_read_v,'String',y);
  
  set(handles.fpos_read_v,'String',f);
  
  %Reading & fixing f&y limits
  for j=[3 2] % for f and y motors
    if lcaNewMonitorValue(['mOTR:motor',num2str(j+(3*otr_numb)),':XPS_STATUS'])
      if lcaGet(['mOTR:motor',num2str(j+(3*otr_numb)),':XPS_STATUS'])==1
        setappdata(handles.single_otr_general_panel,'limit_switch_pressed',1)
        pause(1)
        if j==3
          dim='f';
          if lcaGet(handles.RDBL{(3+3*otr_numb)}) < 5 %voltages lower for positive direction
            pos_lim=1;
          else
            pos_lim=0;
          end
        else
          dim='y'; %voltages grow for positive direction
          if lcaGet(handles.RDBL{(2+3*otr_numb)}) > 5
            pos_lim=1;
          else
            pos_lim=0;
          end
        end
        
        
        for i=1:size(handles.limits_disable_tags,2)
          set(eval(handles.limits_disable_tags{i}),'Enable','off')
        end
        if pos_lim
          set(handles.text_under_screen,'String',[dim,' PLUS limit switched'])
          set(eval(['handles.clear_limit_',dim,'_max']),'Enable','on')
          set(eval(['handles.clear_limit_',dim,'_max']),'BackgroundColor',[1 0.6 0.784]);
          set(eval(['handles.startGuiHandles.limit_status_',num2str(otr_numb)]),'String',[dim,' PLUS'])
          set(eval(['handles.startGuiHandles.limit_status_',num2str(otr_numb)]),'BackgroundColor',[0.847 0.161 0]);
          pause(3)
        elseif ~pos_lim
          set(handles.text_under_screen,'String',[dim,' MINUS limit switched'])
          set(eval(['handles.clear_limit_',dim,'_min']),'Enable','on')
          set(eval(['handles.clear_limit_',dim,'_min']),'BackgroundColor',[1 0.6 0.784]);
          set(eval(['handles.startGuiHandles.limit_status_',num2str(otr_numb)]),'String',[dim,' MINUS'])
          set(eval(['handles.startGuiHandles.limit_status_',num2str(otr_numb)]),'BackgroundColor',[0.847 0.161 0]);
          pause(3)
        end
        pause(3)
        set(handles.text_under_screen,'String','')
      else
        set(eval('handles.clear_limit_y_max'),'Enable','off')
        set(eval('handles.clear_limit_y_min'),'Enable','off')
        set(eval('handles.clear_limit_y_min'),'BackGroundColor',[0.702 0.702 0.702])
        set(eval('handles.clear_limit_y_max'),'BackGroundColor',[0.702 0.702 0.702])
        set(eval('handles.clear_limit_f_max'),'Enable','off')
        set(eval('handles.clear_limit_f_min'),'Enable','off')
        set(eval('handles.clear_limit_f_min'),'BackGroundColor',[0.702 0.702 0.702])
        set(eval('handles.clear_limit_f_max'),'BackGroundColor',[0.702 0.702 0.702])
      end
    end
  end
  
  
end


end


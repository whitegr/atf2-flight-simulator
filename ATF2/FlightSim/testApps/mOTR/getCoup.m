function sig13=getCoup(beam)
global BEAMLINE
persistent otrind

if isempty(otrind)
  for ind = 0:3
    otrind(ind+1) = findcells(BEAMLINE,'Name',['OTR',num2str(ind),'X']);
  end
end
[stat bo]=TrackThru(1,otrind(1),beam,1,1,0);
c=cov(bo.Bunch.x');
sig13(1)=c(1,3);
for itrack=2:length(otrind)
  [stat bo1]=TrackThru(otrind(1),otrind(itrack),bo,1,1,0);
  c=cov(bo1.Bunch.x');
%   sig13(itrack)=c(1,3);
  sig13(itrack)=acot((c(1,1)-c(3,3))/(2*c(1,3)))/2;
end
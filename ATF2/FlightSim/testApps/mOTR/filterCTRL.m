function rdbk=filterCTRL(cmd,iotr,val)
% rdbk=filterCNTRL(cmd,iotr,val)
% control for filter insertion stepper motor
%
% iotr = 0-3 for OTR(0-3)X
% cmd = 'readV' or 'stepper' or 'status' or 'in' or 'out'
%  if 'stepper' then provide pot voltage to step to
%
% Error if limit switch becomes active

stepperPV=sprintf('mOTR:XPS3Aux4Bo%d',iotr*3);
stepperDirPV=sprintf('mOTR:XPS3Aux4Bo%d',1+iotr*3);
disablePV=sprintf('mOTR:XPS3Aux4Bo%d',2+iotr*3);
rdbkPV=sprintf('mOTR:XPS3AuxAi%d',iotr);
limitPV={sprintf('mOTR:XPS3Aux4Bi%d',iotr*2); sprintf('mOTR:XPS3Aux4Bi%d',1+iotr*2)};
% ref voltages and other parameters
potTol=0.02;
stepsPerV=66.5;
maxTries=3;
stepRate=10; % Hz
rdbk=nan;
inV=[0.1 0.1 0.1 0.1];
outV=[5 5 5 5];

switch cmd
  case 'status'
    V=lcaGet(rdbkPV);
    if abs(V-inV(iotr+1))<potTol
      rdbk=1;
    elseif abs(V-outV(iotr+1))<potTol
      rdbk=2;
    else
      rdbk=0;
      return
    end
  case 'readV'
    rdbk=lcaGet(rdbkPV);
  case {'stepper','in','out'}
    if strcmp(cmd,'in')
      desV=inV(iotr+1);
    elseif strcmp(cmd,'out')
      desV=outV(iotr+1);
    else
      desV=val;
    end
    lcaPut(disablePV,0);
    V=lcaGet(rdbkPV);
    itry=0;t0=[];
    while abs(V-desV) > potTol
      if (V-desV)>0
        lcaPut(stepperDirPV,0);
      else
        lcaPut(stepperDirPV,1);
      end
      itry=itry+1;
      if itry>maxTries; lcaPut(disablePV,1); return; end;
      allowLimit=false;
      if any(strcmp(lcaGet(limitPV),'Low'))
        if strcmp(lcaGet(limitPV{1}),'Low') && strcmp(lcaGet(stepperDirPV),'High') && ~strcmp(lcaGet(limitPV{2}),'Low')
          errordlg('Low limit active!','filterCTRL error');
          lcaPut(disablePV,1);
          return
        elseif strcmp(lcaGet(limitPV{2}),'Low') && strcmp(lcaGet(stepperDirPV),'Low')
          errordlg('High limit active!','filterCTRL error');
          lcaPut(disablePV,1);
          return
        end
        allowLimit=true;
      end
      for istep=1:ceil(abs(V-desV)*stepsPerV)
        lcaPut(stepperPV,double((-1)^istep>0));
        if ~isempty(t0)
          dt=etime(clock,t0);
          if dt<(1/stepRate)
            system(sprintf('sleep %f',1/(stepRate-dt)));
          end
        end
        t0=clock;
        if ~allowLimit && any(strcmp(lcaGet(limitPV),'Low'))
          if strcmp(lcaGet(limitPV{1}),'Low')
            disp('Low Limit Switch Active!')
          else
            disp('High Limit Switch Active!')
          end
          lcaPut(disablePV,1);
          return
        end
      end
      if itry<maxTries; system('sleep 1'); end;
      V=lcaGet(rdbkPV);
    end
    lcaPut(disablePV,1);
end

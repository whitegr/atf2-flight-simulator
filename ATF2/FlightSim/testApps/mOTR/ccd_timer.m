function results = ccd_timer (obj, event, varargin) %#ok<*INUSL>
global FL
persistent ict sig_y sig_x sig_xy trigSig counter cent_x cent_y proj_x proj_y FSPV theta beamSearchTries nMissedPulses lastRawData lastZoom
results=[];
maxarr=200;

if isempty(nMissedPulses)
  nMissedPulses=0;
end

if isequal(obj,'GetData')
  if nargin>1
    handles=varargin{1};
    otr_numb=varargin{2};
    calx=handles.cal(1+otr_numb,9);
    caly=handles.cal(1+otr_numb,8);
    results.calx=calx; results.caly=caly;
    results.sig_x=sig_x(end); results.sig_y=sig_y(end); results.sig_xy=sig_xy(end);
    results.cent_x=cent_x(end); results.cent_y=cent_y(end); results.proj_x=proj_x(end); results.proj_y=proj_y(end);
    results.theta=theta(end); results.ellipseData=lastRawData; results.counter=counter; results.ict=ict(end);
  else
    results.sig_x=sig_x; results.sig_y=sig_y; results.sig_xy=sig_xy;
    results.cent_x=cent_x; results.cent_y=cent_y; results.proj_x=proj_x; results.proj_y=proj_y;
    results.theta=theta; results.ellipseData=lastRawData; results.counter=counter; results.ict=ict;
  end
end

handles.beamCentring=false;

if isequal(obj,'reset')
  sig_y=[];sig_x=[];sig_xy=[];trigSig=[];counter=[];cent_x=[];
  cent_y=[];proj_x=[];proj_y=[];FSPV=[];theta=[]; ict=[];
  return
end

if isempty(counter)
  counter=0;
end

try
  
  handles=varargin{1};
  
  otr_numb=varargin{2};
  
  if nargin>4
    nave=varargin{3};
    isemit=true;
  else
    isemit=false;
    nave=str2double(get(handles.edit33,'String'));
  end
  
  handles.isemit=isemit;
  if ~isemit
    trigCut=str2double(get(handles.edit36,'String'))*1e8;
    cutRatio=str2double(get(handles.edit35,'String'))*1e-2;
    sigmacut=str2double(get(handles.edit34,'String'));
%     beamCentring=getappdata(handles.single_otr_general_panel,'beamCentring');
%     handles.beamCentring=beamCentring(1+otr_numb);
  else
    trigCut=varargin{4};
    cutRatio=varargin{5};
    sigmacut=varargin{6};
%     setappdata(handles.single_otr_general_panel,'beamCentring',varargin{7});
%     handles.beamCentring=varargin{7};
  end
  
  % ICT cut preferences
  ictCut.apply=get(handles.startGuiHandles.checkbox1,'Value');
  ictCut.lval=str2double(get(handles.startGuiHandles.edit23,'String'));
  ictCut.hval=str2double(get(handles.startGuiHandles.edit24,'String'));
  
  if ~isfield(handles,'cal')
    handles.cal = getappdata(handles.single_otr_general_panel,'calibration');
  end
  
  PVName='mOTR';
  cam=otr_numb;
  camName=sprintf('cam%d',1+cam);
  imageName=sprintf('image%d',1+cam);
  pars.sizeX=1280;
  pars.sizeY=960;
  
  % Fit method
  if exist('fitMethod.mat','file')
    load fitMethod.mat fitMethod
    fitMethod=fitMethod(1+cam);
  else
    fitMethod=1;
  end
  switch fitMethod
    case 1
      set(handles.radiobutton11,'value',1)
      set(handles.radiobutton12,'value',0)
      set(handles.radiobutton13,'value',0)
    case 2
      set(handles.radiobutton11,'value',0)
      set(handles.radiobutton12,'value',1)
      set(handles.radiobutton13,'value',0)
    case 3
      set(handles.radiobutton11,'value',0)
      set(handles.radiobutton12,'value',0)
      set(handles.radiobutton13,'value',1)
  end
  
  zoomLevel=otrZoom('readpos',cam);
  if ~isemit && (isempty(lastZoom) || abs(zoomLevel-lastZoom)>0.05)
    set(handles.edit37,'String',num2str(zoomLevel,2))
    lastZoom=zoomLevel;
  end
  
  % FSECS Status
  if isempty(FSPV) && ~isemit
    try
      fsecs_stat=lcaGet('FSECS:RUNNING.RVAL');
      if fsecs_stat
%         set(handles.togglebutton1,'Enable','on')
%         set(handles.pushbutton55,'Enable','on')
      else
%         set(handles.togglebutton1,'Enable','off')
%         set(handles.pushbutton55,'Enable','off')
      end
    catch ME %#ok<NASGU>
%       set(handles.togglebutton1,'Enable','off')
%       set(handles.pushbutton55,'Enable','off')
      FSPV=true;
    end
  end
  
  % Acquire new image to EPICS waveform PV / set continuous acquire going
  count0=lcaGet([PVName,':',camName,':ArrayCounter_RBV']);
  lcaPutNoWait([PVName,':',camName,':Acquire'],1); pause(0.1)
  while lcaGet([PVName,':',camName,':ArrayCounter_RBV']) == count0
    pause(0.1);
  end
  
  if ishandle(handles.projx)
    cla(handles.projx); cla(handles.projy);
  end
  
  try
    
    %Get waveform
    rawData=uint16(lcaGet([PVName,':',imageName,':ArrayData'],pars.sizeX*pars.sizeY));
    % Put waveform into correctly dimensioned matlab array
    ccdData=reshape(rawData,pars.sizeX,pars.sizeY);
%     load OTR0XData otrData
%     ccdData=otrData.ellipseData;
    lastRawData=ccdData;
    
    % Saturation warning / ADC count information
    if ~isemit
      maxdata=max(ccdData(ccdData<4095));
      if maxdata==4094
        set(handles.text48,'Visible','on')
      else
        set(handles.text48,'Visible','off')
      end
      set(handles.text49,'String',num2str(maxdata))
    end
    
    %Take out the background contribution
    bkgd=uint16(getappdata(handles.startGuiHandles.Emittance,sprintf('bkgd_data%d',otr_numb)));
    ccdData=fliplr(ccdData-bkgd); ccdData(ccdData<0)=0;
    
    % Plot the data
    imagesc(ccdData','Parent',handles.shot)
    set(handles.shot,'YDir','normal')
    % - No axis labels
    set(handles.shot,'XTickLabel','')
    set(handles.shot,'YTickLabel','')
    set(handles.shot,'XTick',[])
    set(handles.shot,'YTick',[])
    
    % Calibrations in um/pixel
    try
      calx=handles.cal(1+otr_numb,9);%/(zoomLevel/2);
    catch
      calx=0.5;%/(zoomLevel/2);
    end
    caly=handles.cal(1+otr_numb,8);%/(zoomLevel/2);
    
    % Gaussian fits for bkg subtraction based on peak y height
    ccdData=double(ccdData);
    sz=size(ccdData);
    posvals=sum(ccdData,2);
    [~, q] = agauss_fit((1:sz(1)),posvals);
    
    % cut data
    ts=sum(ccdData(ccdData<4095));
    if ~isemit
      set(handles.text62,'String',num2str(ts/1e8))
      barh(handles.axes6,ts);
      ax=axis(handles.axes6);
      ax(2)=max([ts trigCut*1.25]); axis(handles.axes6,ax);
      lh=line([trigCut trigCut],[ax(3) ax(4)],'Parent',handles.axes6);
      set(lh,'LineWidth',4); set(lh,'Color','red');
      set(handles.axes6,'XTickLabel','')
      set(handles.axes6,'YTickLabel','')
      set(handles.axes6,'XTick',[])
      set(handles.axes6,'YTick',[])
    end    
    if ts<trigCut; return; end;
    
    if ~get(handles.radiobutton12,'Value')
      ccdData(ccdData<q(2)*cutRatio)=0;
    end
    
    if ~any(ccdData); return; end;
    % x
    posvals=sum(ccdData,2);
    plot(handles.projx,1:sz(1),posvals,'k.')
    set(handles.projx,'XTickLabel','')
    set(handles.projx,'YTickLabel','')
    hold(handles.projx,'on')
    x1=round(max([q(3)-sigmacut*q(4) 1])); x2=round(min([q(3)+sigmacut*q(4) sz(1)]));
    if fitMethod==2 % optimize bkg
      [~,q]=agauss_fit(1:length(posvals),posvals);
      xmin=fminbnd(@(x) bminfn(x,posvals),q(1),q(1)+(max(posvals)-q(1))*0.5);
      posvals(posvals<xmin)=q(1);
%       hold(handles.projx,'off')
      q = agauss_plot((x1:x2),posvals(x1:x2),[],[],0,handles.projx,'r');
    elseif fitMethod==1
      q = agauss_plot((x1:x2),posvals(x1:x2),[],[],0,handles.projx,'r');
    elseif fitMethod==3
      q = gaussHalo_plot(1:sz(1),posvals,sigmacut,0,handles.projx,'r');
    end
    set(handles.projx,'YScale','linear');
    hold(handles.projx,'off')
    axis(handles.projx,'tight')
    grid(handles.projx,'on')
    projx=q(4)*calx;
    projxOff=q(3)*calx;
    if get(handles.radiobutton13,'Value')
      x1=1; x2=sz(1);
    else
      x1=round(max([q(3)-(sigmacut)*q(4) 1]));
      x2=round(min([q(3)+(sigmacut)*q(4) sz(1)]));
    end
    % y
    posvals=sum(ccdData,1);
    plot(handles.projy,sz(2):-1:1,posvals,'k.')
    set(handles.projy,'XTickLabel','')
    set(handles.projy,'YTickLabel','')
    hold(handles.projy,'on')
    [~,q] = agauss_fit((1:sz(2)),posvals);
    y1=round(max([q(3)-(sigmacut)*q(4) 1])); y2=round(min([q(3)+(sigmacut)*q(4) sz(2)]));
    if fitMethod==2
      [~,q]=agauss_fit(1:length(posvals),posvals);
      xmin=fminbnd(@(x) bminfn(x,posvals),q(1),q(1)+(max(posvals)-q(1))*0.5);
      posvals(posvals<xmin)=q(1);
%       hold(handles.projy,'off')
      q=agauss_plot(sz(2)-(y1:y2),posvals(y1:y2),[],[],0,handles.projy,'r');
    elseif fitMethod==1
      y1=round(max([q(3)-(sigmacut)*q(4) 1]));
      y2=round(min([q(3)+(sigmacut)*q(4) sz(2)]));
      q=agauss_plot(sz(2)-(y1:y2),posvals(y1:y2),[],[],0,handles.projy,'r');
    elseif fitMethod==3
      q = gaussHalo_plot(sz(2):-1:1,posvals,sigmacut,0,handles.projy,'r');
    end
    set(handles.projy,'YScale','linear');
    hold(handles.projy,'off')
    axis(handles.projy,'tight')
    grid(handles.projy,'on')
    projy=q(4)*caly;
    projyOff=q(3)*caly;
    if get(handles.radiobutton13,'Value')
      y1=1;y2=sz(2);
    else
      y1=round(max([q(3)-sigmacut*q(4) 1]));
      y2=round(min([q(3)+sigmacut*q(4) sz(2)]));
    end
    axis(handles.projy,'tight')
    grid(handles.projy,'on')
    set(handles.projy,'CameraUpVector',[-1 0 0])
    
    % Elipse fit
    [sig, ybar, xbar, locus] = ellipseFit(ccdData(x1:x2,y1:y2),...
      (x1:x2)*calx,(y1:y2)*caly);
    
    
    % rotation calibration if GUI active
    if isfield(FL,'Gui') && isfield(FL.Gui,'rotcal') && ishandle(FL.Gui.rotcal.figure1)
      rotcal('acqData',FL.Gui.rotcal,[xbar ybar]);
    end
    
  catch
    return
  end
  
  % Get ICT data and 
  ictVal=lcaGet('BIM:EXT:nparticles');
  if ~isemit
    set(handles.text59,'BackgroundColor','green')
  end
  if ictCut.apply
    try
      if ictVal<ictCut.lval || ictVal>ictCut.hval
        set(handles.text59,'BackgroundColor','red')
      end
    catch
      warning('mOTR:noict','Cannot get new ICT reading')
    end
  end
  
  % Cut on summed ccd image
  if ts>trigCut && (~ictCut.apply || (ictVal>ictCut.lval && ictVal<ictCut.hval))
    
    % Get variables of interest
    ict(end+1)=ictVal;
    sig_y(end+1)=sqrt(sig(1,1));
    sig_x(end+1)=sqrt(sig(2,2));
    sig_xy(end+1)=sig(1,2);
    proj_x(end+1)=projx;
    proj_y(end+1)=projy;
    % Get sig_13 term from angular projection of 2-d image (like a u-wire
    % measurement)
%     sig_xy(end+1)=GetS13(ccdData,pars.sizeX*calx,pars.sizeY*caly);
    theta(end+1)=acot((sig(1,1)-sig(2,2))/(2*sig(1,2)))/2;
    trigSig(end+1)=ts;
    cent_x(end+1)=projxOff;%xbar;
    cent_y(end+1)=projyOff;%ybar;
    if length(sig_y) > maxarr
      ict=ict(2:end);
      sig_y=sig_y(2:end);
      sig_x=sig_x(2:end);
      sig_xy=sig_xy(2:end);
      trigSig=trigSig(2:end);
      cent_x=cent_x(2:end);
      cent_y=cent_y(2:end);
      proj_x=proj_x(2:end);
      proj_y=proj_y(2:end);
      theta=theta(2:end);
    end
    
    % increment the good pulse counter
    counter=counter+1;
    if isemit
      results{1}=counter;
    end
    
    % Plot ellipse
    if isemit || get(handles.checkbox1,'Value')
      hold(handles.shot,'on')
      plot(handles.shot,locus.ellipse(:,2)/calx,locus.ellipse(:,1)/caly,'-w')
      plot(handles.shot,locus.semimajor(:,2)/calx,locus.semimajor(:,1)/caly,'-w')
      plot(handles.shot,locus.semiminor(:,2)/calx,locus.semiminor(:,1)/caly,'-w')
      plot(handles.shot,locus.xaxis(:,2)/calx,locus.xaxis(:,1)/caly,'--w')
      plot(handles.shot,locus.yaxis(:,2)/calx,locus.yaxis(:,1)/caly,'--w')
      hold(handles.shot,'off')
    end
    
    % Get mean and rms values
    if length(sig_x)>=nave
      ictmean=mean(ict(end-nave+1:end));
      icterr=std(ict(end-nave+1:end));
      th=mean(theta(end-nave+1:end));
      therr=std(theta(end-nave+1:end));
      sx=mean(sig_x(end-nave+1:end));
      sy=mean(sig_y(end-nave+1:end));
      sxy=mean(sig_xy(end-nave+1:end));
      cx=mean(cent_x(end-nave+1:end));
      cy=mean(cent_y(end-nave+1:end));
      px=mean(proj_x(end-nave+1:end));
      py=mean(proj_y(end-nave+1:end));
      sxerr=std(sig_x(end-nave+1:end));
      syerr=std(sig_y(end-nave+1:end));
      sxyerr=std(sig_xy(end-nave+1:end));
      cxerr=std(cent_x(end-nave+1:end));
      cyerr=std(cent_y(end-nave+1:end));
      pxerr=std(proj_x(end-nave+1:end));
      pyerr=std(proj_y(end-nave+1:end));
    else
      ictmean=mean(ict);
      icterr=std(ict);
      th=mean(theta);
      therr=std(theta);
      sx=mean(sig_x);
      sy=mean(sig_y);
      sxy=mean(sig_xy);
      cx=mean(cent_x);
      cy=mean(cent_y);
      px=mean(proj_x);
      py=mean(proj_y);
      sxerr=std(sig_x);
      syerr=std(sig_y);
      sxyerr=std(sig_xy);
      cxerr=std(cent_x);
      cyerr=std(cent_y);
      pxerr=std(proj_x);
      pyerr=std(proj_y);
    end
    
    % Publish PVs
    if ~any(isnan([sx sy sxy cx cy px py sxerr syerr cxerr cyerr pxerr pyerr ]))
      lcaPut(['mOTR:procData',num2str(1+otr_numb),':sigma11'],sx^2)
      lcaPut(['mOTR:procData',num2str(1+otr_numb),':sigma33'],sy^2)
      lcaPut(['mOTR:procData',num2str(1+otr_numb),':sigma13'],sxy)
      lcaPut(['mOTR:procData',num2str(1+otr_numb),':x'],cx)
      lcaPut(['mOTR:procData',num2str(1+otr_numb),':y'],cy)
      lcaPut(['mOTR:procData',num2str(1+otr_numb),':projx'],px)
      lcaPut(['mOTR:procData',num2str(1+otr_numb),':projy'],py)
      lcaPut(['mOTR:procData',num2str(1+otr_numb),':sigma11err'],sxerr^2)
      lcaPut(['mOTR:procData',num2str(1+otr_numb),':sigma33err'],syerr^2)
      lcaPut(['mOTR:procData',num2str(1+otr_numb),':sigma13err'],sxyerr)
      lcaPut(['mOTR:procData',num2str(1+otr_numb),':xerr'],cxerr)
      lcaPut(['mOTR:procData',num2str(1+otr_numb),':yerr'],cyerr)
      lcaPut(['mOTR:procData',num2str(1+otr_numb),':projxerr'],pxerr)
      lcaPut(['mOTR:procData',num2str(1+otr_numb),':projyerr'],pyerr)
      lcaPut(['mOTR:procData',num2str(1+otr_numb),':ict'],ictmean)
      lcaPut(['mOTR:procData',num2str(1+otr_numb),':icterr'],icterr)
    end
    
    %write the centroid
    if ~isemit && get(handles.radiobutton9,'Value')
      set(handles.x_centroid,'String',num2str(cx/calx))
      set(handles.y_centroid,'String',num2str(cy/caly))
      set(handles.text55,'String',num2str(cxerr/calx))
      set(handles.text56,'String',num2str(cyerr/caly))
    elseif ~isemit
      set(handles.x_centroid,'String',num2str(cx))
      set(handles.y_centroid,'String',num2str(cy))
      set(handles.text55,'String',num2str(cxerr))
      set(handles.text56,'String',num2str(cyerr))
    else
      results{2}=[cx cxerr cy cyerr];
    end
    
    %Put beamsize and error in microns or pixels in displays
    if ~isemit && get(handles.radiobutton9,'Value')
      set(handles.sigma_11,'String',num2str(sx/calx))
      set(handles.sigma_33,'String',num2str(sy/caly)) %sig value is 2x2, only spatial coords beam matrix
      set(handles.sigma_13,'String',num2str(th))
      set(handles.sigma_std_11,'String',num2str(sxerr/calx));
      set(handles.sigma_std_33,'String',num2str(syerr/caly));
      set(handles.sigma_std_13,'String',num2str(therr));
      set(handles.text16,'String','Tilt')
    elseif ~isemit
      set(handles.sigma_11,'String',num2str(sx))
      set(handles.sigma_33,'String',num2str(sy)) %sig value is 2x2, only spatial coords beam matrix
      set(handles.sigma_13,'String',num2str(sxy))
      set(handles.sigma_std_11,'String',num2str(sxerr));
      set(handles.sigma_std_33,'String',num2str(syerr));
      set(handles.sigma_std_13,'String',num2str(sxyerr));
      set(handles.text16,'String','sxy')
    else
      results{3}=[sx sxerr sy syerr sxy sxyerr];
    end
    
    % Write projected sizes
    if ~isemit && get(handles.radiobutton9,'Value')
      set(handles.text50,'String',num2str(px/calx))
      set(handles.text57,'String',num2str(pxerr/calx))
      set(handles.text53,'String',num2str(py/caly))
      set(handles.text58,'String',num2str(pyerr/caly))
    elseif ~isemit
      set(handles.text50,'String',num2str(px))
      set(handles.text57,'String',num2str(pxerr))
      set(handles.text53,'String',num2str(py))
      set(handles.text58,'String',num2str(pyerr))
    else
      results{4}=[px pxerr py pyerr];
    end
    
    % Move camera to beam centre?
%     if handles.beamCentring
%       iscentre=centreBeam(cent_x(end),cent_y(end),calx,caly,pars,handles,cam,sig_x(end),sig_y(end));
%       results{5}=iscentre;
%     end
    
    % Clear beamSearchTries & missed pulses counter
    beamSearchTries=[];
    nMissedPulses=0;
    
  elseif isfield(handles,'beamCentring') && handles.beamCentring && nMissedPulses>5 % auto beam search 
    
    % Is beam getting to dump?
    [stat, ictData] = fsCom('GetICT');
    if stat{1}~=1 || ictData<1e9 % no beam or communiction error
      cla(ax); text(ax(1)+(ax(2)-ax(1))*0.1,(ax(4)-ax(3))/2,'Beam not reaching ICTDUMP','Parent',handles.shot,'FontSize',22,'FontWeight','bold')
      drawnow('expose');
      return
    end 
    
    % Increment search tries counter and reset missed pulses counter
    nMissedPulses=0;
    if isempty(beamSearchTries)
      beamSearchTries=1;
    else
      beamSearchTries=beamSearchTries+1;
    end
    
    nBpmSearchTries=5;
    if beamSearchTries<=nBpmSearchTries % Move to beam orbit calculated place
      
      % Indicate activity
      if handles.isemit
        fsize=12;
      else
        fsize=22;
      end
      ax=axis(handles.projx);
      cla(ax); text(ax(1)+(ax(2)-ax(1))*0.05,(ax(4)-ax(3))/2,'BPM Orbit-Based Search','Parent',handles.shot,'FontSize',fsize,'FontWeight','bold')
      ax=axis(handles.shot);
      cla(ax); text(ax(1)+(ax(2)-ax(1))*0.1,(ax(4)-ax(3))/2,'Searching for beam...','Parent',handles.shot,'FontSize',fsize,'FontWeight','bold')
      drawnow('expose')
    
      % Get fitted beam position at OTR locations and offset from that
      refOrbit=getappdata(handles.singlepanel.single_otr_general_panel,'refOrbit');
      stepcal=getappdata(handles.single_otr_general_panel,'calibration');
      if length(refOrbit)==1; return; end; % no reference orbit ever taken
      [stat, orbitData] = fsCom('OrbitFit',otr_numb);
      if stat{1}~=1; return; end;
      orbitData=refOrbit-orbitData;
      beamPos=[orbitData(1) orbitData(2)];

      % Try moving OTR onto beam
      motor_move('move',0,-beamPos(1)/stepcal(1+otr_numb,2),1,0,1,0,1+(3*otr_numb),3);
      motor_move('move',0,beamPos(2)/stepcal(1+otr_numb,3),1,0,1,0,2+(3*otr_numb),3);
      system(sprintf('sleep %f',max([3 [beamPos(1) beamPos(2)]./[400 800]])));
      
    else % Try grid search
      
      % Get grid position to move to
      gsize=11;
      searchStep=beamSearchTries-nBpmSearchTries;
      searchStepSize=[500 300]; % um
      searchGrid=spiral(gsize);
      [gi, gj]=ind2sub(size(searchGrid),find(searchGrid==searchStep));
      if isempty(gi) % search grid complete, start over
        beamSearchTries=[];
        return
      end
      gi=(gi-floor(gsize/2))*searchStepSize(1); gj=(gj-floor(gsize/2))*searchStepSize(2);
      wp=getappdata(handles.single_otr_general_panel,'working_point');
      
      % Indicate activity
      if handles.isemit
        fsize=12;
        msize=12;
      else
        fsize=22;
        msize=18;
      end
      ax=axis(handles.projx);
      cla(ax); text(ax(1)+(ax(2)-ax(1))*0.05,(ax(4)-ax(3))/2,'Grid Search','Parent',handles.shot,'FontSize',fsize,'FontWeight','bold')
      
      % Draw grid progress on display
      [mi, mj]=meshgrid(1:gsize,1:gsize);
      scatter(handles.shot,mi(:),mj(:));
      axis(handles.shot,[0.5 gsize+0.5 0.5 gsize+0.5])
      grid(handles.shot,'on')
      hold(handles.shot,'on')
      plot(gi,gj,'rs','MarkerSize',msize)
      if searchStep>1
        for istep=1:searchStep
          [pi, pj]=ind2sub(size(searchGrid),find(searchGrid==istep));
          plot(pi,pj,'rx','MarkerSize',msize)
        end
      end
      hold(handles.shot,'off')
      drawnow('expose');
      
      % Make abs move
      XFSWITCH={'mOTR:XPS1Aux3Bo0'; 'mOTR:XPS1Aux3Bo1'; 'mOTR:XPS2Aux3Bo0'; 'mOTR:XPS2Aux3Bo1'};
      rdbl=getappdata(handles.single_otr_general_panel,'RDBL');
      stepcal=getappdata(handles.single_otr_general_panel,'calibration');
      if lcaGet(XFSWITCH{1+otr_numb})~=0
        lcaPut(XFSWITCH{1+otr_numb},0);
      end
      whichMotor = 1+(3*otr_numb);
      x=lcaGet(rdbl{(1+3*otr_numb)});
      xsteps=(gi/stepcal(1+otr_numb,2))-((x-wp(1+otr_numb,2)*stepcal(1+otr_numb,5)/stepcal(1+otr_numb,2)));
      motor_move('move',0,xsteps,1,0,1,0,whichMotor,3);
      whichMotor = 2+(3*otr_numb);
      y=lcaGet(rdbl{(2+3*otr_numb)});
      ysteps=(gj/stepcal(1+otr_numb,3))-((y-wp(1+otr_numb,3)*stepcal(1+otr_numb,6)/stepcal(1+otr_numb,3)));
      motor_move('move',0,ysteps,1,0,1,0,whichMotor,3);
      xn=(gi/stepcal(1+otr_numb,2))-((x-wp(1+otr_numb,2)*stepcal(1+otr_numb,5)));
      yn=(gj/stepcal(1+otr_numb,3))-((y-wp(1+otr_numb,3)*stepcal(1+otr_numb,6)));
      system(sprintf('sleep %f',max([3 [xn yn]./[400 800]])));
      
    end
    
  else
    
    nMissedPulses=nMissedPulses+1;
    
  end
  
  drawnow('expose')
  
catch ME
  disp(ME.message)
end

% Beam centring function
function iscentre=centreBeam(cx,cy,calx,caly,pars,handles,otr_numb,sx,sy)
persistent lastmove
if isempty(lastmove)
  lastmove=[0 0];
end
PI=[0.3 0.1];
XFSWITCH={'mOTR:XPS1Aux3Bo0'; 'mOTR:XPS1Aux3Bo1'; 'mOTR:XPS2Aux3Bo0'; 'mOTR:XPS2Aux3Bo1'};
% Get distance to CCD centre
midx=pars.sizeX/2; midx=midx*calx;
midy=pars.sizeY/2; midy=midy*caly;
moveX=midx-cx;
moveY=midy-cy;
% um -> steps conversion
stepcal=getappdata(handles.single_otr_general_panel,'calibration');
% Are we in the centre? (within +/- 0.5sigma)
iscentre=false;
if abs(moveX)<sx*0.5 && abs(moveY)<sy*0.5
  iscentre=true;
  % set new working point as current location
  rdbl=getappdata(handles.single_otr_general_panel,'RDBL');
  lcaPut(XFSWITCH{(1+otr_numb)},0);
  xvolt=lcaGet(rdbl{(1+3*otr_numb)});
  yvolt=lcaGet(rdbl{(2+3*otr_numb)});
  wp = getappdata(handles.single_otr_general_panel,'working_point');
  wp(1+otr_numb,2)=xvolt;
  wp(1+otr_numb,3)=yvolt;
  setappdata(handles.single_otr_general_panel,'working_point',wp);
  save -ASCII working_point.set wp
  rv=getappdata(handles.single_otr_general_panel,'reference_voltages');
  rv(1+otr_numb,2)=xvolt;
  rv(1+otr_numb,3)=yvolt;
  save -ASCII reference_voltages.dat rv
  setappdata(handles.singlepanel.single_otr_general_panel,'reference_voltages',rv)
  refOrbitDate=getappdata(handles.singlepanel.single_otr_general_panel,'refOrbitDate');
  if etime(clock,refOrbitDate)>3600*2
    ax=axis(handles.shot);
    cla(ax); text(ax(1)+(ax(2)-ax(1))*0.1,(ax(4)-ax(3))/2,'Updating beam orbit offset...','Parent',handles.shot,'FontSize',22,'FontWeight','bold')
    drawnow('expose');
    [stat, orbitData] = fsCom('OrbitFit',otr_numb);
    if stat{1}~=1; return; end;
    setappdata(handles.singlepanel.single_otr_general_panel,'refOrbit',orbitData);
    save refOrbit orbitData
    setappdata(handles.singlepanel.single_otr_general_panel,'refOrbitDate',clock);
    cla(ax);
  end
else
  % send move command
  move=[moveX moveY];
  movePI=PI(1).*(move-lastmove) + PI(2).*lastmove;
  motor_move('move',0,movePI(1)/stepcal(1+handles.otr_numb,2),1,0,1,0,1+(3*otr_numb),3);
  motor_move('move',0,movePI(2)/stepcal(1+handles.otr_numb,3),1,0,1,0,2+(3*otr_numb),3);
  system(sprintf('sleep %f',max([3 [movePI(1) movePI(2)]./[400 800]])));
  lastmove=move;
end

% Get sig_13 by getting 45-degree projected beam size
function s13=GetS13(im,xwid,ywid,sx,sy)
% Make the grid
sz=size(im);
I=im';
[xgrid,ygrid]=meshgrid(linspace(0,xwid,sz(1)),linspace(0,ywid,sz(2)));

% Rotate the coordinates
th=45;
NC=[cosd(th) -sind(th);sind(th) cosd(th)]*[xgrid(:)';ygrid(:)'];

% Get projected vertical beam size in rotated co-ordinates
xbins=linspace(min(NC(1,:)),max(NC(1,:)),50);
[~,bn]=histc(NC(1,:),xbins);
A = accumarray(bn',I(:));
[~,q] = gauss_fit_rot(xbins,A,[],1,accumarray(bn',ones(size(I(:)))));
%   A(xbins<(q(3)-1*q(4)))=0;
%   A(xbins>(q(3)+1*q(4)))=0;
A(A<(q(2)*0.1))=0;
[~,q] = agauss_fit(xbins,A);
s13=(q(4)^2-sx^2*cosd(th)^2-sy^2*sind(th)^2)/(2*cosd(th)*sind(th));

% minimisation function for optimal background cut
function ret=bminfn(x,data)
data=data-x;
data(data<0)=0;
[~,~,~,chi2]=agauss_fit(1:length(data),data);
ret=chi2;

% ellipse fitting routine
function [sig, xbar, ybar, locus]=ellipseFit(c,x,y,xbar,ybar)
% compute x-y beam matrix from statistical second moments
c=double(c);
norm=sum(sum(c));
y=y';

if ~exist('xbar','var')
  ybar=sum(x*c)/norm;
  xbar=sum(c*y)/norm;
end
sigyy=sum(((x-ybar).*(x-ybar))*c)/norm;
sigxx=sum(c*((y-xbar).*(y-xbar)))/norm;
sigxy=(x-ybar)*c*(y-xbar)/norm;
sig=[sigxx,sigxy;sigxy,sigyy];
% generate locus of (many!) points along the ellipse for plotting
X=inv(sig);
delta=0.001;ang=(0:delta:pi)';
C=cos(ang);S=sin(ang);
r=sqrt((X(1,1)*(C.^2)+2*X(1,2)*(C.*S)+X(2,2)*(S.^2)).^(-1));
r=[r;r];C=[C;-C];S=[S;-S];
Xe=r.*C+xbar;
Ye=r.*S+ybar;
locus.ellipse=[Xe,Ye]; % ellipse
% generate semimajor and semiminor axis plot data
thetaa=acot((sigxx-sigyy)/(2*sigxy))/2;
C=cos(thetaa);
S=sin(thetaa);
siga=sqrt((X(1,1)*(C^2)+2*X(1,2)*(C*S)+X(2,2)*(S^2))^(-1));
Xa=siga*C*[-1;1]+xbar;
Ya=siga*S*[-1;1]+ybar;
locus.semimajor=[Xa,Ya]; % semimajor axis
thetab=thetaa+pi/2;
C=cos(thetab);
S=sin(thetab);
sigb=sqrt((X(1,1)*(C^2)+2*X(1,2)*(C*S)+X(2,2)*(S^2))^(-1));
Xb=sigb*C*[-1;1]+xbar;
Yb=sigb*S*[-1;1]+ybar;
locus.semiminor=[Xb,Yb]; % semiminor axis
% generate X-axis and Y-axis plot data
locus.xaxis=[min(Xe),ybar;max(Xe),ybar]; % horizontal axis
locus.yaxis=[xbar,min(Ye);xbar,max(Ye)]; % vertical axis

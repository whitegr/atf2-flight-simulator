function varargout = cal_settings(varargin)
% CAL_SETTINGS M-file for cal_settings.fig
%      CAL_SETTINGS, by itself, creates a new CAL_SETTINGS or raises the existing
%      singleton*.
%
%      H = CAL_SETTINGS returns the handle to a new CAL_SETTINGS or the handle to
%      the existing singleton*.
%
%      CAL_SETTINGS('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in CAL_SETTINGS.M with the given input arguments.
%
%      CAL_SETTINGS('Property','Value',...) creates a new CAL_SETTINGS or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before cal_settings_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to cal_settings_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help cal_settings

% Last Modified by GUIDE v2.5 08-Nov-2011 16:03:39

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @cal_settings_OpeningFcn, ...
                   'gui_OutputFcn',  @cal_settings_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before cal_settings is made visible.
function cal_settings_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to cal_settings (see VARARGIN)

% Choose default command line output for cal_settings
handles.output = hObject;
handles.single_otr_general_panel = varargin{2};
handles.cal = load('cal.set', 'calibration', '-ascii');
otr_numb=varargin{1};

%Before it made only changeable the 'parent'OTR: [better the possibility to change all
for i=1:4
    for j=2:8
       set(handles.(sprintf('cal_%d_%d',i,j)),'String',num2str(handles.cal(i,j))); 
	%Before it made only changeable the 'parent'OTR: [better the possibility to change all
	%set(handles.(sprintf('cal_%d_%d',i,j)),'Enable','off')
    end    
end
% X cal settings
try
  set(handles.edit29,'String',num2str(handles.cal(1,9)))
  set(handles.edit30,'String',num2str(handles.cal(2,9)))
  set(handles.edit31,'String',num2str(handles.cal(3,9)))
  set(handles.edit32,'String',num2str(handles.cal(4,9)))
catch
  set(handles.edit29,'String','0.5')
  set(handles.edit30,'String','0.5')
  set(handles.edit31,'String','0.5')
  set(handles.edit32,'String','0.5')
end
  

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes cal_settings wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = cal_settings_OutputFcn(hObject, eventdata, handles)  %#ok<*INUSL>
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;

function cal_1_2_Callback(hObject, eventdata, handles) %#ok<*DEFNU>
temp = str2double(get(hObject, 'String'));
if isnan(temp)
    set(hObject, 'String', 0);
    errordlg('Input must be a number','Error');
end
handles.cal(1,2) = temp;
guidata(gcbo,handles)

function cal_1_2_CreateFcn(hObject, eventdata, handles)

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function cal_1_3_Callback(hObject, eventdata, handles)
temp = str2double(get(hObject, 'String'));
if isnan(temp)
    set(hObject, 'String', 0);
    errordlg('Input must be a number','Error');
end
handles.cal(1,3) = temp;
guidata(gcbo,handles)

function cal_1_3_CreateFcn(hObject, eventdata, handles)

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function cal_1_4_Callback(hObject, eventdata, handles)
temp = str2double(get(hObject, 'String'));
if isnan(temp)
    set(hObject, 'String', 0);
    errordlg('Input must be a number','Error');
end
handles.cal(1,4) = temp;
guidata(gcbo,handles)

function cal_1_4_CreateFcn(hObject, eventdata, handles)

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function cal_1_5_Callback(hObject, eventdata, handles)
temp = str2double(get(hObject, 'String'));
if isnan(temp)
    set(hObject, 'String', 0);
    errordlg('Input must be a number','Error');
end
handles.cal(1,5) = temp;
guidata(gcbo,handles)

function cal_1_5_CreateFcn(hObject, eventdata, handles)

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function cal_1_6_Callback(hObject, eventdata, handles)
temp = str2double(get(hObject, 'String'));
if isnan(temp)
    set(hObject, 'String', 0);
    errordlg('Input must be a number','Error');
end
handles.cal(1,6) = temp;
guidata(gcbo,handles)

function cal_1_6_CreateFcn(hObject, eventdata, handles)

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function cal_1_7_Callback(hObject, eventdata, handles)
temp = str2double(get(hObject, 'String'));
if isnan(temp)
    set(hObject, 'String', 0);
    errordlg('Input must be a number','Error');
end
handles.cal(1,7) = temp;
guidata(gcbo,handles)

function cal_1_7_CreateFcn(hObject, eventdata, handles)

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function cal_1_8_Callback(hObject, eventdata, handles)
temp = str2double(get(hObject, 'String'));
if isnan(temp)
    set(hObject, 'String', 0);
    errordlg('Input must be a number','Error');
end
handles.cal(1,8) = temp;
guidata(gcbo,handles)

function cal_1_8_CreateFcn(hObject, eventdata, handles)

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function cal_2_2_Callback(hObject, eventdata, handles)
temp = str2double(get(hObject, 'String'));
if isnan(temp)
    set(hObject, 'String', 0);
    errordlg('Input must be a number','Error');
end
handles.cal(2,2) = temp;
guidata(gcbo,handles)

function cal_2_2_CreateFcn(hObject, eventdata, handles)

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function cal_2_3_Callback(hObject, eventdata, handles)
temp = str2double(get(hObject, 'String'));
if isnan(temp)
    set(hObject, 'String', 0);
    errordlg('Input must be a number','Error');
end
handles.cal(2,3) = temp;
guidata(gcbo,handles)

function cal_2_3_CreateFcn(hObject, eventdata, handles)

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function cal_2_4_Callback(hObject, eventdata, handles)
temp = str2double(get(hObject, 'String'));
if isnan(temp)
    set(hObject, 'String', 0);
    errordlg('Input must be a number','Error');
end
handles.cal(2,4) = temp;
guidata(gcbo,handles)

function cal_2_4_CreateFcn(hObject, eventdata, handles)

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function cal_2_5_Callback(hObject, eventdata, handles)
temp = str2double(get(hObject, 'String'));
if isnan(temp)
    set(hObject, 'String', 0);
    errordlg('Input must be a number','Error');
end
handles.cal(2,5) = temp;
guidata(gcbo,handles)

function cal_2_5_CreateFcn(hObject, eventdata, handles)

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function cal_2_6_Callback(hObject, eventdata, handles)
temp = str2double(get(hObject, 'String'));
if isnan(temp)
    set(hObject, 'String', 0);
    errordlg('Input must be a number','Error');
end
handles.cal(2,6) = temp;
guidata(gcbo,handles)

function cal_2_6_CreateFcn(hObject, eventdata, handles)

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function cal_2_7_Callback(hObject, eventdata, handles)
temp = str2double(get(hObject, 'String'));
if isnan(temp)
    set(hObject, 'String', 0);
    errordlg('Input must be a number','Error');
end
handles.cal(2,7) = temp;
guidata(gcbo,handles)

function cal_2_7_CreateFcn(hObject, eventdata, handles)

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function cal_2_8_Callback(hObject, eventdata, handles)
temp = str2double(get(hObject, 'String'));
if isnan(temp)
    set(hObject, 'String', 0);
    errordlg('Input must be a number','Error');
end
handles.cal(2,8) = temp;
guidata(gcbo,handles)

function cal_2_8_CreateFcn(hObject, eventdata, handles)

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function cal_3_2_Callback(hObject, eventdata, handles)
temp = str2double(get(hObject, 'String'));
if isnan(temp)
    set(hObject, 'String', 0);
    errordlg('Input must be a number','Error');
end
handles.cal(3,2) = temp;
guidata(gcbo,handles)

function cal_3_2_CreateFcn(hObject, eventdata, handles)

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function cal_3_3_Callback(hObject, eventdata, handles)
temp = str2double(get(hObject, 'String'));
if isnan(temp)
    set(hObject, 'String', 0);
    errordlg('Input must be a number','Error');
end
handles.cal(3,3) = temp;
guidata(gcbo,handles)

function cal_3_3_CreateFcn(hObject, eventdata, handles)

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function cal_3_4_Callback(hObject, eventdata, handles)
temp = str2double(get(hObject, 'String'));
if isnan(temp)
    set(hObject, 'String', 0);
    errordlg('Input must be a number','Error');
end
handles.cal(3,4) = temp;
guidata(gcbo,handles)

function cal_3_4_CreateFcn(hObject, eventdata, handles)

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function cal_3_5_Callback(hObject, eventdata, handles)
temp = str2double(get(hObject, 'String'));
if isnan(temp)
    set(hObject, 'String', 0);
    errordlg('Input must be a number','Error');
end
handles.cal(3,5) = temp;
guidata(gcbo,handles)

function cal_3_5_CreateFcn(hObject, eventdata, handles)

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function cal_3_6_Callback(hObject, eventdata, handles)
temp = str2double(get(hObject, 'String'));
if isnan(temp)
    set(hObject, 'String', 0);
    errordlg('Input must be a number','Error');
end
handles.cal(3,6) = temp;
guidata(gcbo,handles)

function cal_3_6_CreateFcn(hObject, eventdata, handles)

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function cal_3_7_Callback(hObject, eventdata, handles)
temp = str2double(get(hObject, 'String'));
if isnan(temp)
    set(hObject, 'String', 0);
    errordlg('Input must be a number','Error');
end
handles.cal(3,7) = temp;
guidata(gcbo,handles)

function cal_3_7_CreateFcn(hObject, eventdata, handles)

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function cal_3_8_Callback(hObject, eventdata, handles)
temp = str2double(get(hObject, 'String'));
if isnan(temp)
    set(hObject, 'String', 0);
    errordlg('Input must be a number','Error');
end
handles.cal(3,8) = temp;
guidata(gcbo,handles)

function cal_3_8_CreateFcn(hObject, eventdata, handles)

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function cal_4_2_Callback(hObject, eventdata, handles)
temp = str2double(get(hObject, 'String'));
if isnan(temp)
    set(hObject, 'String', 0);
    errordlg('Input must be a number','Error');
end
handles.cal(4,2) = temp;
guidata(gcbo,handles)

function cal_4_2_CreateFcn(hObject, eventdata, handles)

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function cal_4_3_Callback(hObject, eventdata, handles)
temp = str2double(get(hObject, 'String'));
if isnan(temp)
    set(hObject, 'String', 0);
    errordlg('Input must be a number','Error');
end
handles.cal(4,3) = temp;
guidata(gcbo,handles)

function cal_4_3_CreateFcn(hObject, eventdata, handles)

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function cal_4_4_Callback(hObject, eventdata, handles)
temp = str2double(get(hObject, 'String'));
if isnan(temp)
    set(hObject, 'String', 0);
    errordlg('Input must be a number','Error');
end
handles.cal(4,4) = temp;
guidata(gcbo,handles)

function cal_4_4_CreateFcn(hObject, eventdata, handles)

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function cal_4_5_Callback(hObject, eventdata, handles)
temp = str2double(get(hObject, 'String'));
if isnan(temp)
    set(hObject, 'String', 0);
    errordlg('Input must be a number','Error');
end
handles.cal(4,5) = temp;
guidata(gcbo,handles)

function cal_4_5_CreateFcn(hObject, eventdata, handles)

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function cal_4_6_Callback(hObject, eventdata, handles)
temp = str2double(get(hObject, 'String'));
if isnan(temp)
    set(hObject, 'String', 0);
    errordlg('Input must be a number','Error');
end
handles.cal(4,6) = temp;
guidata(gcbo,handles)

function cal_4_6_CreateFcn(hObject, eventdata, handles)

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function cal_4_7_Callback(hObject, eventdata, handles)
temp = str2double(get(hObject, 'String'));
if isnan(temp)
    set(hObject, 'String', 0);
    errordlg('Input must be a number','Error');
end
handles.cal(4,7) = temp;
guidata(gcbo,handles)

function cal_4_7_CreateFcn(hObject, eventdata, handles)

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function cal_4_8_Callback(hObject, eventdata, handles)
temp = str2double(get(hObject, 'String'));
if isnan(temp)
    set(hObject, 'String', 0);
    errordlg('Input must be a number','Error');
end
handles.cal(4,8) = temp;
guidata(gcbo,handles)

function cal_4_8_CreateFcn(hObject, eventdata, handles)

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function figure1_CloseRequestFcn(hObject, eventdata, handles)

calibration=handles.cal;
    save('cal.set', 'calibration', '-ascii')    
setappdata(handles.single_otr_general_panel,'calibration',handles.cal)



delete(hObject);



function edit29_Callback(hObject, eventdata, handles)
% hObject    handle to edit29 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

temp = str2double(get(hObject, 'String'));
if isnan(temp) || temp<=0 || temp>10
    set(hObject, 'String', 0.5);
    return
end
handles.cal(1,9) = temp;
guidata(gcbo,handles)

% --- Executes during object creation, after setting all properties.
function edit29_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit29 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit30_Callback(hObject, eventdata, handles)
% hObject    handle to edit30 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

temp = str2double(get(hObject, 'String'));
if isnan(temp) || temp<=0 || temp>10
    set(hObject, 'String', 0.5);
    return
end
handles.cal(2,9) = temp;
guidata(gcbo,handles)


% --- Executes during object creation, after setting all properties.
function edit30_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit30 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit31_Callback(hObject, eventdata, handles)
% hObject    handle to edit31 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

temp = str2double(get(hObject, 'String'));
if isnan(temp) || temp<=0 || temp>10
    set(hObject, 'String', 0.5);
    return
end
handles.cal(3,9) = temp;
guidata(gcbo,handles)


% --- Executes during object creation, after setting all properties.
function edit31_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit31 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit32_Callback(hObject, eventdata, handles)
% hObject    handle to edit32 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

temp = str2double(get(hObject, 'String'));
if isnan(temp) || temp<=0 || temp>10
    set(hObject, 'String', 0.5);
    return
end
handles.cal(4,9) = temp;
guidata(gcbo,handles)


% --- Executes during object creation, after setting all properties.
function edit32_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit32 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

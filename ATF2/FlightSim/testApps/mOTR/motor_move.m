function motor_move(cmd,varargin)
% Function for driving mOTR motors, comes from Glens motor_test.m
% 
%   motor_move('move',first,last,N,return,Niter,waitTime,whichMotor,kind)
%   Mover motor (whichMotor = integer motor number) from position (first)
%   to position (last) in N steps pausing (waitTime seconds) after each
%   step. If return = true, then subsequently move back from last to first
%   in N steps. Iterate the whole procedure (Niter times).
%   kind is 1 for scan, 2 for abs, 3 for REL movement 
%
%  ABS
%    motor_move('move',0,last,1,0,1,0,whichMotor,2) just change last and motor
%   (Actually now even ABS movements are made with REL movement, in the ABS button of the
%    main GUI is calculated the needed relative distance) 
%  REL
%    motor_move('move',0,rel,1,0,1,0,whichMotor,3) just change rel and motor
%




%% Setup options
% array entries are {X Y FOCUS} * 4 rows for OTR 1-4 (single row assumes
% same for all OTRs)
motorPV='mOTR:motorN:'; % N is replaced by motor # [for setup]
% PV names for digital output to control OTR insertion (0=out 1=in)
insertPV={'mOTR:XPS1Aux3Bo2' 'mOTR:XPS1Aux3Bo3' 'mOTR:XPS2Aux3Bo2' 'mOTR:XPS2Aux3Bo3'};
% PV names for digital output to control switch between Focus and Y stage
% linear readback for Ai1 and Ai3 (0=Y 1=Focus)
rdbkSwitchPV={'mOTR:XPS1Aux3Bo0'; 'mOTR:XPS1Aux3Bo1'; 'mOTR:XPS2Aux3Bo0'; 'mOTR:XPS2Aux3Bo1'};
% resIO=10 / 2^13; % Analogue input resolution: +/- 10V (14-bit) 
setup.EGU={'steps' 'steps' 'steps'}; % Units in use for motor moves
setup.VMAX={500 500 500}; % Max setable velocities
setup.VELO={1000 1000 1000}; % Set velocities
setup.ACCL={1e-4 1e-4 1e-4}; % Set accelerations
setup.BVEL={200 200 200}; % Set velocities to use for backlash correction
setup.BACC={0.01 0.01 0.01}; % set accelerations to use for backlash correction
setup.RDBD={10 10 10}; % Deadband to use to know when motor move has reached destination
setup.RTRY={5 5 5}; % max retries to get motor in deadband
setup.UEIP={1 1 1}; % encoder readback
setup.URIP={0 0 0}; % PV readback
% analogue in channels to use for readback
setup.RDBL={'mOTR:XPS1AuxAi0' 'mOTR:XPS1AuxAi1' 'mOTR:XPS1AuxAi0';
            'mOTR:XPS1AuxAi2' 'mOTR:XPS1AuxAi3' 'mOTR:XPS1AuxAi2';
            'mOTR:XPS2AuxAi0' 'mOTR:XPS2AuxAi1' 'mOTR:XPS2AuxAi0';
            'mOTR:XPS2AuxAi2' 'mOTR:XPS2AuxAi3' 'mOTR:XPS2AuxAi2'};
setup.OFF={0 0 0}; % calibration offsets
setup.SREV={200 200 200}; % Steps per revolution
setup.UREV={200 200 200}; % Units (EGU) per revolution
setup.RRES={1 1 1}; % Readback units per EGU
setup.HLM={10000 10000 10000}; % High limits
setup.LLM={-10000 -10000 -10000}; % Low limits
setup.SPMG={3 3 3}; % run control 0,1,2,3 = 'stop','pause','move','go' We use 'move' and not 'go' here
setup.BDST={0 0 0}; % backlash correction distance to use
% Pre-move commands to issue (change readback source before issuing move
% command)
% setup.PREM={[] ['PUT(',rdbkSwitchPV{1},',0,0.1)'] ['PUT(',rdbkSwitchPV{1},',1,0.1)'];
%             [] ['PUT(',rdbkSwitchPV{2},',0,0.1)'] ['PUT(',rdbkSwitchPV{2},',1,0.1)'];
%             [] ['PUT(',rdbkSwitchPV{3},',0,0.1)'] ['PUT(',rdbkSwitchPV{3},',1,0.1)'];
%             [] ['PUT(',rdbkSwitchPV{4},',0,0.1)'] ['PUT(',rdbkSwitchPV{4},',1,0.1)']};

%% command options [choose case depending on cmd word]
switch lower(cmd)

  case 'setup' %%function defined down
    motorSetup(setup,motorPV,1); % Issue setup commands

  case 'example'
    %% Example move sequence
lcaPut(insertPV{1},1); % Insert OTR 1, insertPV vector defined below
    % Get current position (X and Y)
    lcaPut(rdbkSwitchPV,0); % select X readback
    curPos=[lcaGet('mOTR:motor1.RBV') lcaGet('mOTR:motor2.RBV')];
    % Get Focus motor position readback
    pause(1); lcaPut(rdbkSwitchPV,1); % select Focus readback
    pause(1); curPos(3)=lcaGet('mOTR:motor3.RBV');
    % Move X + 0.2mm Y - 0.15mm
    lcaPut('mOTR:motor1',curPos(1)+0.2);
    lcaPut('mOTR:motor2',curPos(2)-0.15);
    % Command moves
    lcaPut('mOTR:motor1.SPMG',3);
    lcaPut('mOTR:motor2.SPMG',3);
    % Wait for move to finish
    pause(0.1)
    while any([lcaGet('mOTR:motor1.SPMG') lcaGet('mOTR:motor2.SPMG')]==3)
      pause(0.1)
    end

  case 'move'
     %Selection of X/F when needed
     %is it really necessary?
%   if rem(varargin{7},3)==1
%     lcaPut(rdbkSwitchPV,0);
%     elseif rem(varargin{7},3)==0
%    lcaPut(rdbkSwitchPV,1);
%   end

     %SCAN case
     if (varargin{8}==1)

     first=varargin{1}; last=varargin{2}; nsteps=varargin{3}; ret=varargin{4}; niter=varargin{5};
     waitTime=varargin{6}; imotor=varargin{7};

     %ABS case (not anymore used, made via REL movement)
     elseif (varargin{8}==2)
    
     last=varargin{2}; nsteps=varargin{3}; ret=varargin{4}; niter=varargin{5};
     waitTime=varargin{6}; imotor=varargin{7};
     first=lcaGet(['mOTR:motor',num2str(imotor),'.RBV']);

     %REL case
     else

     rel=varargin{2}; nsteps=varargin{3}; ret=varargin{4}; niter=varargin{5};
     waitTime=varargin{6}; imotor=varargin{7};
     %first=lcaGet(['mOTR:motor',num2str(imotor),'.RBV']);
     first=lcaGet(['mOTR:motor',num2str(imotor)]);
     last=first+rel;
    end

 
    lcaPutNoWait(['mOTR:motor',num2str(imotor),'.SPMG'],3);
    for iter=1:niter
      for isteps=linspace(first,last,nsteps)
        lcaPutNoWait(['mOTR:motor',num2str(imotor)],isteps);
        lcaPutNoWait(['mOTR:motor',num2str(imotor),'.SPMG'],3);
        pause(waitTime)
      end
      if ret
        for isteps=linspace(last,first,nsteps)
          lcaPutNoWait(['mOTR:motor',num2str(imotor)],isteps);
          lcaPutNoWait(['mOTR:motor',num2str(imotor),'.SPMG'],3);
          pause(waitTime)
        end
      end
    end



end




%% Process setup commands
function motorSetup(pv,mpv,otrList)
fn=fieldames(pv);
for iotr=otrList
  for ifn=1:length(fn)
    psize=size(pv.(fn{ifn}));
    for imotor=1:3
      if psize(1)>=iotr
        nmotor=(iotr-1)*3+imotor;
        lcaPut([regexprep(mpv,'N:',[num2str(nmotor),':']),fn{ifn}],pv.(fn{ifn}){iotr,imotor});
      else
        lcaPut([regexprep(mpv,'N:',[num2str(nmotor),':']),fn{ifn}],pv.(fn{ifn}){1,imotor});
      end
    end
  end
end

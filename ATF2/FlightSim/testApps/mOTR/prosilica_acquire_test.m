function [sig xbar ybar ccdData]=prosilica_acquire_test(dofit,doloop,handles)
% Example setup and image acquire and plot for prosilica cam using
% areaDetector EPICS module
% Set dofit=1 to do basic linear ellipse fit and plot
%     dofit=2 to also perform full 2d gauusian fit (slower)
% Set doloop=true to continuosly acquire and plot (ctrl-c to quit)
persistent bkgdata

% Setup parameters
nbkg=10;
if isequal(dofit,'getbkg')
  dobkg=true;
  dofit=true;
  doloop=true;
else
  dobkg=false;
end
if ~exist('dofit','var'); dofit=false; end;
if ~exist('doloop','var'); doloop=false; end;
PVName='mOTR';
cam=handles.otr_numb;
camName=sprintf('cam%d',1+cam);
imageName=sprintf('image%d',1+cam);
pars.sizeX=1280;
pars.sizeY=960;
pars.exposureTime=0.1; % s
pars.acquirePeriod=0.1; % s
pars.triggerMode=2; % 0 = free running, 1:4=syncIn 1-4
if doloop 
  pars.imageMode=2;
else
 pars.imageMode=0; % 0=single, 1=multiple, 2=continuous
end
pars.gain=24.0;
acquireRate=1.56; % Hz

% Send setup parameters to device
camSetup(pars,PVName,camName,imageName);

% Acquire new image to EPICS waveform PV / set continuous acquire going
lcaPutNoWait([PVName,':',camName,':Acquire'],0);
pause(0.5)
lcaPutNoWait([PVName,':',camName,':Acquire'],1);

ibkg=0;
while 1
  % Get waveform
  rawData=uint16(lcaGet([PVName,':',imageName,':ArrayData'],pars.sizeX*pars.sizeY));

  % Put waveform into correctly dimensioned matlab array
  ccdData=reshape(rawData,pars.sizeX,pars.sizeY);

  % bkg data
  if dobkg
    if ibkg==0
      bkgdata{1+cam}=ccdData;
    else
      bkgdata{1+cam}=bkgdata{1+cam}+ccdData;
    end
    ibkg=ibkg+1;
    if ibkg>nbkg; disp('done'); bkgdata{1+cam}=bkgdata{1+cam}./nbkg; return; end;
  elseif ~isempty(bkgdata) && length(bkgdata)>=1+cam
    ccdData=ccdData-bkgdata{1+cam};
  end
  
  %Do axes name depending on OTR
  % imagesc(...,'Parent',handles.shot)
  if ~dobkg; imagesc(ccdData','Parent',handles.shot);colorbar;colormap('Gray'); end;
  %if ~dobkg; imagesc(ccdData');colorbar;colormap('Gray'); end;
  
  % Perform ellipse fit?
  if dofit && ~dobkg
    [sig xbar ybar gplot1 gplot2 gplot3] = ellipseFit(ccdData,pars.sizeX,pars.sizeY);
    if dofit==2
      [sig xbar ybar gplot1 gplot2 gplot3] = gaussfit2d(ccdData,pars,sig,xbar,ybar);
    end
    hold on
%     fprintf('sig11: %g sig22: %g sig12: %g xbar: %g ybar:%g\n',sig(1,1),sig(2,2),sig(1,2),xbar,ybar)
    %plot(handles.shot,gplot1(2,:),gplot1(1,:),'-b') <- I think is the way to
    %choose right axes. Do it! OOPS, think in the axes name of multi-otr panel 
    plot(gplot1(2,:),gplot1(1,:),'-b')
    plot(gplot2(2,:),gplot2(1,:),'-b')
    plot(gplot3(2,:),gplot3(1,:),'-b')
    hold off
    % Write out processed data to EPICS DB
%     lcaPut('mOTR:procData1:sigma',[sig(1,1) sig(2,2) sig(1,2)]); 
  

%TAL VOLTA POSAR UN IF DEPENENT SI ES SINGLE O MULTI OTR
%----------------------
    %Put beamsize in microns in displays, leave sig(1,3) as it is
    %mirar-ho be! mirar si canviar el titol sig(1,1) del gui front panel
    set(handles.sigma_11,'String',num2str(sqrt(sig(1,1))*handles.cal(1+handles.otr_numb,8))) %why sqrt? look minty page 11. sig is beam matrix element, beamsize is sqrooted
    set(handles.sigma_33,'String',num2str(sqrt(sig(2,2))*handles.cal(1+handles.otr_numb,8))) %sig value is 2x2, only spatial coords beam matrix
    %set(handles.sigma_13,'String',num2str(sqrt(sig(1,2))*handles.cal(1+handles.otr_numb,8))) %makes it physical sense?
    set(handles.sigma_13,'String',num2str(sig(1,2)))
    
    %handles.ellipse(1+cam,1) = sig(1,1) mirar si el algoritme necessita la sig o la size [arrel quadrada] o si esta en microns
    %handles.ellipse(1+cam,2) = sig(3,3) mirar si el algoritme necessita la sig o la size [arrel quadrada] o si esta en microns
    %handles.ellipse(1+cam,3) = sig(1,3) mirar si el algoritme necessita la sig o la size [arrel quadrada] o si esta en microns
%-------------------------------
    
  end
  
  % Loop?
  if ~doloop
    break
  else
    pause(1/acquireRate)
    drawnow
  end
end

%% =====================
% Cam Setup function
function camSetup(pars,PVName,camName,imageName)
lcaPut([PVName,':',camName,':SizeX'],pars.sizeX);
lcaPut([PVName,':',camName,':SizeY'],pars.sizeY);
lcaPut([PVName,':',camName,':AcquireTime'],pars.exposureTime);
lcaPut([PVName,':',camName,':AcquirePeriod'],pars.acquirePeriod);
lcaPut([PVName,':',camName,':TriggerMode'],pars.triggerMode);
% lcaPut([PVName,':',camName,':ImageMode'],pars.imageMode);
lcaPut([PVName,':',camName,':Gain'],pars.gain);
% Set camera to acquire 12-bit data (as opposed to 8-bit)
lcaPut([PVName,':',camName,':DataType'],0);
% Set NDStdArrays plugin to enable array callbacks (so new data becomes
% availbale to use through ArrayData record)
lcaPut([PVName,':',imageName,':EnableCallbacks'],1);

% Gaussian fitting routines
function [sig xbar ybar gplot1 gplot2 gplot3]=ellipseFit(c,resx,resy)
x=1:resx;
y=(1:resy)';
c=double(c);
norm=sum(sum(c));
ybar=sum(x*c)/norm;
xbar=sum(c*y)/norm;
sigyy=sum(((x-ybar).*(x-ybar))*c)/norm;
sigxx=sum(c*((y-xbar).*(y-xbar)))/norm;
sigxy=(x-ybar)*c*(y-xbar)/norm;
sig=[sigxx,sigxy;sigxy,sigyy];
sigx=sqrt(sigxx)/resx;
sigy=sqrt(sigyy)/resy;
theta=atan(2*sigxy/(sigyy-sigxx))/2;
sigaa=(sigx*sigx*cos(theta)*cos(theta)-sigy*sigy*sin(theta)*sin(theta))/cos(2*theta);
sigbb=(sigy*sigy*cos(theta)*cos(theta)-sigx*sigx*sin(theta)*sin(theta))/cos(2*theta);
siga  = sqrt(sigaa);
sigb  = sqrt(sigbb);
phi = 0:pi/100:2*pi;
a1 = siga*cos(phi);
b1 = sigb*sin(phi);
x1 = [cos(theta) sin(theta); -sin(theta) cos(theta)]*[a1; b1];
gplot1(1,:)=(x1(1,:).*resx+xbar);
gplot1(2,:)=(x1(2,:).*resy+ybar);
x1 = [cos(theta) sin(theta); -sin(theta) cos(theta)]*[-siga siga; 0 0];
gplot2(1,:)=(x1(1,:).*resx+xbar);
gplot2(2,:)=(x1(2,:).*resy+ybar);
x1 = [cos(theta) sin(theta); -sin(theta) cos(theta)]*[0 0; -sigb sigb];
gplot3(1,:)=(x1(1,:).*resx+xbar);
gplot3(2,:)=(x1(2,:).*resy+ybar);

function [sig, xbar, ybar, gplot1, gplot2, gplot3] = gaussfit2d(data,pars,sig,xbar,ybar)

p0 = [0 max(max(data)) xbar/pars.sizeX ybar/pars.sizeY sig(1,1) sig(1,2) sig(2,2)];
options = optimset('TolX',1000.0,'Display','iter');
p  = fminsearch(@(p) chisquare(p,data), p0, options);

xbar  = p(3)*pars.sizeX;
ybar  = p(4)*pars.sizeY;
sigxx = p(5);
sigxy = p(6);
sigyy = p(7);

sigx = sqrt(sigxx);
sigy = sqrt(sigyy);
sig=[sigxx,sigxy;sigxy,sigyy];

theta = atan(2*sigxy/(sigyy-sigxx))/2;

sigaa = (sigx*sigx*cos(theta)*cos(theta) - sigy*sigy*sin(theta)*sin(theta))/cos(2*theta);
siga  = sqrt(sigaa);
sigbb = (sigy*sigy*cos(theta)*cos(theta) - sigx*sigx*sin(theta)*sin(theta))/cos(2*theta);
sigb  = sqrt(sigbb);

phi = 0:pi/100:2*pi;
a1 = siga*cos(phi);
b1 = sigb*sin(phi);
x1 = [cos(theta) sin(theta); -sin(theta) cos(theta)]*[a1; b1];
gplot1(1,:)=(x1(1,:).*pars.sizeX+xbar);
gplot1(2,:)=(x1(2,:).*pars.sizeY+ybar);
x1 = [cos(theta) sin(theta); -sin(theta) cos(theta)]*[-siga siga; 0 0];
gplot2(1,:)=(x1(1,:).*pars.sizeX+xbar);
gplot2(2,:)=(x1(2,:).*pars.sizeY+ybar);
x1 = [cos(theta) sin(theta); -sin(theta) cos(theta)]*[0 0; -sigb sigb];
gplot3(1,:)=(x1(1,:).*pars.sizeX+xbar);
gplot3(2,:)=(x1(2,:).*pars.sizeY+ybar);

function r = chisquare(p,a)

r = sum(sum((gaussian(p,size(a)) - a).^2));

function g = gaussian(p,a)

x  = 1:a(2);
y  = (1:a(1))';
x2 = ones(a(1),1) * (x - p(3)).^2;
xy = (y - p(4)) * (x - p(3));
y2 = (y - p(4)).^2 * ones(1,a(2));
invsigma2 = inv([p(5) p(6); p(6) p(7)]);
g = p(1) + p(2)*exp(-x2*invsigma2(1,1)/2-xy*invsigma2(1,2)-y2*invsigma2(2,2)/2);


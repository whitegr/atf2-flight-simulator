

function emittance_refresh (obj, event, handles)
%function emittance_refresh (handles)

set(handles.emittance_status,'String','Taking all targets out');
%Take out targets
for i=0:3
    target_in_out(i,0);
end

%Making SIGMA measurements [ccd_timer does it]
for i=0:3
        set(handles.emittance_status,'String',sprintf('Measuring OTR%d',i));

	%TO DO: PASS PERIOD IN PARAMETERS COMING FROM GUI NOT IN VALUES
	ave=5;
	acquireRate=1.56;
%	ccd_timer_emitt=timer('StartDelay',1,'Period',round(1000*ave/acquireRate)/1000,...
%     	'ExecutionMode','FixedRate','BusyMode','drop');
%	set(ccd_timer_emitt, 'TimerFcn', {@ccd_timer, guidata(hObject),i,0,acquireRate,ave,1})
%	set(ccd_timer_emitt, 'TimerFcn', {@ccd_timer_emitt,handles,i,acquireRate,ave})

	target_in_out(i,1);
	ccd_timer_emit(handles,i,acquireRate,ave)

%    	start(ccd_timer_emitt)
%	pause(6/acquireRate) %TO DO:_ How many time? ccdtimer has to take 5 measurements per OTR. Maybe
    	%do it depending on acquire rate. ~5*acquirerate NO! BECAUSE PROGRAM ITSELF WAIT FOR NEW IMAGES!
    	
	target_in_out(i,0)

%	stop(ccd_timer_emitt)
%	delete(ccd_timer_emitt)
end

set(handles.emittance_status,'String','Emittance calculation');

%Building SIGMA11 and SIGMA33 matrices (ccd timer builds each SIGMAaa_b)
SIGMA11=[getappdata(handles.startGuiHandles.Emittance,'SIGMA11_0');
        getappdata(handles.startGuiHandles.Emittance,'SIGMA11_1');
        getappdata(handles.startGuiHandles.Emittance,'SIGMA11_2');
        getappdata(handles.startGuiHandles.Emittance,'SIGMA11_3')];

SIGMA33=[getappdata(handles.startGuiHandles.Emittance,'SIGMA33_0');
        getappdata(handles.startGuiHandles.Emittance,'SIGMA33_1');
        getappdata(handles.startGuiHandles.Emittance,'SIGMA33_2');
        getappdata(handles.startGuiHandles.Emittance,'SIGMA33_3')];


%Getting 6*6 response matrices from OTR0X to OTR0-3 and building Rotr
[stat rmat0 rmat1 rmat2 rmat3] = fsCom('GetRMat');
if stat{1}~=1
    set(handles.emittance_status,'String',stat{2});
    return;
end
for i=0:3
   Rotr{i+1}=eval(sprintf('rmat%d',i));
end

[emitx,emitx_err,emity,emity_err]=calcEmit(SIGMA11,SIGMA33,Rotr);

 if imag(emitx)+imag(emity)~=0
  set(handles.emittance_status,'String','imaginary emittance!');
  pause(1.5);
  set(handles.emittance_status,'String','');
else
  set(handles.emittance_display_x,'String',num2str(emitx))
  set(handles.emittance_display_x_err,'String',num2str(emitx_err))
  set(handles.emittance_display_y,'String',num2str(emity))
  set(handles.emittance_display_y_err,'String',num2str(emity_err))
  set(handles.emittance_status,'String','Done!');
 end

end




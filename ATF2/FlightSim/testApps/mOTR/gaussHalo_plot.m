function [q,chisq,dq,xf,yf] = gaussHalo_plot(x,y,NSIGHALO,no_txt,ax,fcol,pcol)
%		y = A + B*exp( -(x-C)^2/2*D^2 ) + E*(X-C)^-F
% q(1:6) : A-F
x = x(:);
y = y(:);
nx = length(x);
ny = length(y);

% Which axes to plot on
if ~exist('ax','var')
  figure
  ax=gca;
end

if ~exist('fcol','var')
  fcol='r';
end

if ~exist('pcol','var')
  pcol='b';
end

if nx ~= ny
  error('X and Y data vectors must be the same length')
end

if ~exist('no_txt','var') || isempty(no_txt)
  no_txt = 1;
end

xx = linspace(min(x),max(x),1000);
xx = xx(:);

[~,p,chisq] = gaussHalo_fit(x,y,NSIGHALO);
yyf = gaussHaloFn(xx,NSIGHALO,p(1),p(2),p(3),p(4),p(5),p(6));
semilogy(ax,x,y,[pcol '.'],xx,yyf,[fcol '-'],'LineWidth',3)
dp=zeros(size(p));
if no_txt==1
  title('A + B*exp( -(X-C)^2/2*D^2 + E*X^{-F}')

  [tx,ty]=text_locator(2,-2,'t');
  text(tx,ty,sprintf('A = %7.3g +- %7.3g',p(1),dp(1)),'FontSize',10)
  [tx,ty]=text_locator(2,-3,'t');
  text(tx,ty,sprintf('B = %7.3g +- %7.3g',p(2),dp(2)),'FontSize',10)
  [tx,ty]=text_locator(2,-4,'t');
  text(tx,ty,sprintf('C = %7.3g +- %7.3g',p(3),dp(3)),'FontSize',10)
  [tx,ty]=text_locator(2,-5,'t');
  text(tx,ty,sprintf('D = %7.3g +- %7.3g',p(4),dp(4)),'FontSize',10)
  [tx,ty]=text_locator(2,-7,'t');
  text(tx,ty,sprintf('CHISQ/NDF = %8.3g',chisq),'FontSize',10)
end

q = [p(5) p(6) p(1) p(2) p(3) p(4)];
dq = dp;

xf = xx;
yf = yyf;

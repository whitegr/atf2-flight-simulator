function [emitx,emitx_err,emity,emity_err]=calcEmit(SIGMA11,SIGMA33,Rotr)
% CALCEMIT - Calculate emittance at OTR0X given Sigma(1,1) and Sigma(3,3)
% measurements for OTR0X - OTR3X
%
% [emitx,emity]=calcEmit(SIGMA11,SIGMA33,Rotr)
%  emitx / emity = un-normalised horizontal and vertical emittances at
%                  OTR0X
%  emitx_err / emity_err = RMS errors on above 
%  SIGMA11/SIGMA33 = vector or array of Sigma(1,1) and Sigma(3,3) measured values
%                    for OTR0X, OTR1X, OTR2X, OTR3X
%                    if multiple data sets passed, SIGMA11(1:4,1:N) for N
%                    measurements etc
%  Rotr = Cell array of 6*6 response matrices for OTR0-3X (Rotr{1:4}(1:6,1:6))
%

% Get model data into form for emittance calculation based on provided
% response matrix data
for iotr=1:4
  xmat(iotr,1)=Rotr{iotr}(1,1)^2;
  xmat(iotr,2)=2*Rotr{iotr}(1,1)*Rotr{iotr}(1,2);
  xmat(iotr,3)=Rotr{iotr}(1,2)^2;
  ymat(iotr,1)=Rotr{iotr}(3,3)^2;
  ymat(iotr,2)=2*Rotr{iotr}(3,3)*Rotr{iotr}(3,4);
  ymat(iotr,3)=Rotr{iotr}(3,4)^2;
end

% Calculate least-squares fit for emittances
sig0 =  xmat \ SIGMA11 ;
ex = sqrt(sig0(1,:).*sig0(3,:) - sig0(2,:).*sig0(2,:)) ;
emitx = mean(ex);
emitx_err = std(ex);
sig0 = ymat \ SIGMA33 ;
ey = sqrt(sig0(1,:).*sig0(3,:) - sig0(2,:).*sig0(2,:)) ;
emity = mean(ey);
emity_err = std(ey);
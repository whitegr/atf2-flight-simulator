
function target_timer (obj, event, varargin) %#ok<INUSL>
persistent FSPV

handles=varargin{1};
ACTUATORget={'mOTR:XPS1Aux3Bo2.RVAL' 'mOTR:XPS1Aux3Bo3.RVAL' 'mOTR:XPS2Aux3Bo2.RVAL' 'mOTR:XPS2Aux3Bo3.RVAL'};


% FSECS status
if isempty(FSPV)
  try
    fsecs_stat=lcaGet('FSECS:RUNNING.RVAL');
    if fsecs_stat
      set(handles.text9,'String','Running')
      set(handles.text9,'BackgroundColor','green')
    else
      set(handles.text9,'String','Not Running')
      set(handles.text9,'BackgroundColor','red')
    end
  catch
    set(handles.text9,'String','Not Running')
    set(handles.text9,'BackgroundColor','red')
    FSPV=true;
  end
end

% ICT value
try
  lval=str2double(get(handles.edit23,'String'));
  hval=str2double(get(handles.edit24,'String'));
  ictVal=lcaGet('BIM:EXT:nparticles');
  set(handles.text10,'String',sprintf('%.2f',ictVal));
  if ictVal<lval || ictVal>hval
    set(handles.text10,'BackgroundColor','red')
  else
    set(handles.text10,'BackgroundColor','green')
  end
catch
  warning('mOTR:noict','Cannot get new ICT reading')
end

% Target in/out
for otr_numb=0:3
  if lcaNewMonitorValue(ACTUATORget(1+otr_numb))
    if lcaGet(ACTUATORget{(1+otr_numb)})
      set(handles.(sprintf('target_status_%d',otr_numb)),'String','IN')
      set(handles.(sprintf('target_status_%d',otr_numb)),'BackgroundColor',[1 0 0])
      %set(handles.(sprintf('OTR%d',otr_numb)),'BackgroundColor',[0 1 0])
    elseif isnan(lcaGet(ACTUATORget{(1+otr_numb)}))
      set(handles.(sprintf('target_status_%d',otr_numb)),'String','Error,NaN')
      set(handles.(sprintf('target_status_%d',otr_numb)),'BackgroundColor',[1 0 0])
      %set(handles.(sprintf('OTR%d',otr_numb)),'BackgroundColor',[0.702 0.702 0.702])
    else
      set(handles.(sprintf('target_status_%d',otr_numb)),'String','OUT')
      set(handles.(sprintf('target_status_%d',otr_numb)),'BackgroundColor',[0 1 0])
      %set(handles.(sprintf('OTR%d',otr_numb)),'BackgroundColor',[0.702 0.702 0.702])
    end
  end
end

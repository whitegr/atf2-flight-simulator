function mOTRemitPlot(ixy,proj,oname,e0,b0,a0,e,b,a,R,sig,dsig)
% ixy      = X (ixy=0) or Y (ixy=1)
% proj     : 1=projected data, 0=ellipse data
% oname    = OTR names
% e0,b0,a0 = nominal unnormalized emittance, beta, and alpha at target point
% e,b,a    = measured unnormalized emittance, beta, and alpha at target point
% R        = cell array of 4x4 R-matrices from target point to each OTR,
%            and to IP
% sig      = measured beam size at each OTR [m]
% dsig     = measured beam size error (rms) at each OTR [m]

if (ixy==0)
  txt1='Horizontal';
else
  txt1='Vertical';
end
if (proj==0)
  txt2='ellipse';
else
  txt2='projected';
end

% plot measured beam ellipse (normalized coordinates)
r=e/e0;
if ((bmag(b0,a0,b,a)-1)<1e-6)
  Sn=r*eye(2);
else
  g=(1+a^2)/b;
  S=r*[b,-a;-a,g];
  A=[1,0;a0,b0]/sqrt(b0);
  Sn=A*S*A';
end
plot_ellipse(inv(Sn),0,0,'k-')
v=axis;
axis(sqrt(2)*max(abs(v))*[-1,1,-1,1],'square')

% plot nominal beam ellipse (normalized coordinates) ... unit circle
hold on
plot_ellipse(eye(2),0,0,'k--')

% plot mapped OTR measurement phases
ioff=2*ixy;
sig0=sqrt(e0*b0);
big=1e3*sqrt(e0/b0);
c='bgrm';
notr=length(oname);
ho=zeros(1,notr);
for n=1:notr
  R11=R{n}(1+ioff,1+ioff);
  R12=R{n}(1+ioff,2+ioff);
  R21=R{n}(2+ioff,1+ioff);
  R22=R{n}(2+ioff,2+ioff);
  sigw=sig(n);
  dsigw=dsig(n);
  c1=a0*R22-b0*R21;
  c2=b0*R11-a0*R12;
  x1=[R22,-R12;R22,R12]*[sigw+dsigw;big]/sig0;
  y1=[c1,c2;c1,-c2]*[sigw+dsigw;big]/sig0;
  x2=[R22,-R12;R22,R12]*[sigw-dsigw;big]/sig0;
  y2=[c1,c2;c1,-c2]*[sigw-dsigw;big]/sig0;
  h=plot(x1,y1,c(n),x2,y2,c(n));
  ho(n)=h(1);
end
hold off
hor_line(0,'k:'),ver_line(0,'k:')

% title, labels, and legend
title(['Normalized ',txt1,' Phase Space at ',oname{1},' (',txt2,' data)'])
ylabel('Angle')
xlabel('Position')
legend(ho,oname,3)

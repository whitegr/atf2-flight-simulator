function varargout = bkgd(varargin)
% BKGD MATLAB code for bkgd.fig
%      BKGD, by itself, creates a new BKGD or raises the existing
%      singleton*.
%
%      H = BKGD returns the handle to a new BKGD or the handle to
%      the existing singleton*.
%
%      BKGD('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in BKGD.M with the given input arguments.
%
%      BKGD('Property','Value',...) creates a new BKGD or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before bkgd_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to bkgd_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help bkgd

% Last Modified by GUIDE v2.5 14-Nov-2010 09:54:37

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
  'gui_Singleton',  gui_Singleton, ...
  'gui_OpeningFcn', @bkgd_OpeningFcn, ...
  'gui_OutputFcn',  @bkgd_OutputFcn, ...
  'gui_LayoutFcn',  [] , ...
  'gui_Callback',   []);
if nargin && ischar(varargin{1})
  gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
  [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
  gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before bkgd is made visible.
function bkgd_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to bkgd (see VARARGIN)
handles.startGuiHandles=varargin{1};

% Choose default command line output for bkgd
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes bkgd wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = bkgd_OutputFcn(hObject, eventdata, handles)
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on button press in bkgd_proceed.
function bkgd_proceed_Callback(hObject, eventdata, handles)
% hObject    handle to bkgd_proceed (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

set(handles.text1,'String','WAIT, taking bkgd!')

if get(handles.bkgd_check_0,'Value')+get(handles.bkgd_check_1,'Value')+get(handles.bkgd_check_2,'Value')+get(handles.bkgd_check_3,'Value')~=0
  
  cam_on=[];
  for i=0:3
    if get(eval(['handles.bkgd_check_',num2str(i)]),'Value')
      cam_on=[cam_on,i+1];%concatenate the values
    end
  end
  
  %for wcam=1:4
  %for wcam=[3 4]
  for wcam=cam_on
    
    set(handles.startGuiHandles.display,'String',['Taking Bkg Cam',num2str(wcam-1)])
    
    nbkg=10;
    
    PVName='mOTR';
    camName=sprintf('cam%d',wcam);
    imageName=sprintf('image%d',wcam);
    
    % Background subtraction
    img=zeros(1280,960);
    try
      lcaPut([PVName,':',camName,':Acquire'],0);
    catch
      lcaPutNoWait([PVName,':',camName,':Acquire'],0);
      pause(1)
    end
    for itry=1:nbkg
      count0=lcaGet([PVName,':',camName,':ArrayCounter_RBV']);
      lcaPutNoWait([PVName,':',camName,':Acquire'],1); pause(0.1)
      while lcaGet([PVName,':',camName,':ArrayCounter_RBV']) == count0
        pause(0.1);
      end
      rawData=lcaGet([PVName,':',imageName,':ArrayData'],1280*960);
      img=img+reshape(rawData,1280,960);
    end
    bkgdata=img./nbkg;
    
    save(['bkgd',num2str(wcam-1),'.dat'], 'bkgdata', '-ascii')
    setappdata(handles.startGuiHandles.Emittance,sprintf('bkgd_data%d',wcam-1),bkgdata);
    
  end
  
  set(handles.startGuiHandles.display,'String','Done')
  pause(0.5)
  set(handles.startGuiHandles.display,'String','')
  
  
  
else
  set(handles.startGuiHandles.display,'String','Any cam selected')
  pause(1)
  set(handles.startGuiHandles.display,'String','')
end




set(handles.text1,'String','Be sure that target is OUT!')
delete(gcf)


% --- Executes on button press in bkgd_cancel.
function bkgd_cancel_Callback(hObject, eventdata, handles) %#ok<*DEFNU>
% hObject    handle to bkgd_cancel (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
delete(gcf)


% --- Executes when user attempts to close figure1.
function figure1_CloseRequestFcn(hObject, eventdata, handles)
% hObject    handle to figure1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: delete(hObject) closes the figure
delete(hObject);


% --- Executes on button press in bkgd_check_0.
function bkgd_check_0_Callback(hObject, eventdata, handles)
% hObject    handle to bkgd_check_0 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of bkgd_check_0


% --- Executes on button press in bkgd_check_1.
function bkgd_check_1_Callback(hObject, eventdata, handles)
% hObject    handle to bkgd_check_1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of bkgd_check_1


% --- Executes on button press in bkgd_check_2.
function bkgd_check_2_Callback(hObject, eventdata, handles)
% hObject    handle to bkgd_check_2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of bkgd_check_2


% --- Executes on button press in bkgd_check_3.
function bkgd_check_3_Callback(hObject, eventdata, handles)
% hObject    handle to bkgd_check_3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of bkgd_check_3

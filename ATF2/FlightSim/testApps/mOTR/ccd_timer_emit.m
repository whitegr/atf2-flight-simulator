

function ccd_timer_emit (handles,varargin)

otr_numb=varargin{1};

acquireRate=varargin{2};

ave=varargin{3};%number of measurements for average and error

handles.cal = load('cal.set', 'calibration', '-ascii');

%ATTENTION! DO IT SELECTABLE IN GUI? TAKE IT FROM FLIGHTSIM? HOW
energy_spread=0.001;

PVName='mOTR';
cam=otr_numb;
camName=sprintf('cam%d',1+cam);
imageName=sprintf('image%d',1+cam);

pars.sizeX=1280;
pars.sizeY=960;

pars.exposureTime=0.1; % s
pars.acquirePeriod=0.1; % s
pars.triggerMode=2; % 0 = free running, 1:4=syncIn 1-4
pars.imageMode=2; % 0=single, 1=multiple, 2=continuous
pars.gain=20.0;


for i=1:ave
    
    % Acquire new image to EPICS waveform PV / set continuous acquire going
  
    try
      lcaPut([PVName,':',camName,':Acquire'],0);
    catch
      lcaPut([PVName,':',camName,':Acquire'],0);
      pause(0.5)
    end
    count0=lcaGet([PVName,':',camName,':ArrayCounter_RBV']);
    lcaPut([PVName,':',camName,':Acquire'],1);


%     while lcaGet([PVName,':',camName,':ArrayCounter_RBV']) == count0
%       pause(0.1);
%     end
 

	j=0;
	count=0;
	while j~=1 &&count<100
        	try
   %Get waveform
    rawData=uint16(lcaGet([PVName,':',imageName,':ArrayData'],pars.sizeX*pars.sizeY));
    % Put waveform into correctly dimensioned matlab array
    ccdData=reshape(rawData,pars.sizeX,pars.sizeY);     

 bkgd=uint16(getappdata(handles.startGuiHandles.Emittance,sprintf('bkgd_data%d',otr_numb)));
    %Be sure that no value below zero
    bkgd(bkgd>ccdData)=ccdData(bkgd>ccdData);
    ccdData=ccdData-bkgd;

    drawnow('expose')

     %---------------------------------------------------
if 0
     %TEMP GAUSSIAN FOR TEST
     rawData=rand([1228800 1]);
         rawData=reshape(rawData,1280,960);
         A = 1;
         x0 = 0; y0 = 0;
         sigma_x = 200;
         sigma_y = 30;
         %theta = pi+i;
         theta=pi;
         %theta=0;
         a = cos(theta)^2/2/sigma_x^2 + sin(theta)^2/2/sigma_y^2;
         b = -sin(2*theta)/4/sigma_x^2 + sin(2*theta)/4/sigma_y^2 ;
         c = sin(theta)^2/2/sigma_x^2 + cos(theta)^2/2/sigma_y^2;
         [X, Y] = meshgrid(1:1:1280, 1:1:960);
         Z = A*exp( - (a*(X-640).^2 + 2*b*(X-640).*(Y-480) + c*(Y-480).^2)) ;
         rawData = rawData.*Z'; 
         %rawData = Z'; 
         ccdData=rawData;
end
     %---------------------


			sigmacut=5;
			ccdData=double(ccdData); sz=size(ccdData);
			[X I]=max(mean(ccdData,1));
			ybar=I;
			[yf,q]=gauss_fit(1:sz(1),ccdData(:,I)); sig(1,1)=q(4);
			[X I]=max(mean(ccdData,2));
			xbar=I;[yf,q]=gauss_fit(1:sz(2),ccdData(I,:)); sig(2,2)=q(4);
			wndw(1)=ceil(max([1 xbar-sig(1,1)]));
			wndw(2)=floor(min([sz(1) xbar+sig(1,1)]));
			wndw(3)=ceil(max([1 ybar-sig(2,2)]));
			wndw(4)=floor(min([sz(2) ybar+sig(2,2)]));
			[sig ybar xbar ep1 ep2 ep3] = ellipseFit(ccdData(wndw(1):wndw(2),wndw(3):wndw(4)),...
			  1+wndw(2)-wndw(1),1+wndw(4)-wndw(3));
			xb=xbar+wndw(1); yb=ybar+wndw(3);
			xsig=sqrt(sig(2,2)); ysig=sqrt(sig(1,1));
			wndw(1)=ceil(max([1 xb-sigmacut*xsig]));
			wndw(2)=floor(min([sz(1) xb+sigmacut*xsig]));
			wndw(3)=ceil(max([1 yb-sigmacut*ysig]));
			wndw(4)=floor(min([sz(2) yb+sigmacut*ysig]));
			xbar=xb-wndw(1); ybar=yb-wndw(3);
			[sig ybar xbar ep1 ep2 ep3] = ellipseFit(ccdData(wndw(1):wndw(2),wndw(3):wndw(4)),...
			  1+wndw(2)-wndw(1),1+wndw(4)-wndw(3),ybar,xbar);
			xsig=sqrt(sig(2,2)); ysig=sqrt(sig(1,1));
			xbar=xbar+wndw(1); ybar=ybar+wndw(3);

			%CHANGE THIS, SHOW SPOT IN ccd_emittance axes!
			imagesc(ccdData','Parent',handles.ccd_emittance)%;colormap('Gray');colorbar('peer',handles.shot);
			hold(handles.ccd_emittance,'on')
			plot(handles.ccd_emittance,wndw(1)+ep1(2,:),wndw(3)+ep1(1,:),'-k')';
			plot(handles.ccd_emittance,wndw(1)+ep2(2,:),wndw(3)+ep2(1,:),'-k')';
			plot(handles.ccd_emittance,wndw(1)+ep3(2,:),wndw(3)+ep3(1,:),'-k')';
			hold(handles.ccd_emittance,'off')
			    j=1;
	
        	catch
	                count=count+1;
	                pause(1/acquireRate)
	        end
  end
if count==99
	stop(handles.ccd_refresh)
        set(handles.text_under_screen,'String','RESTART PANEL. CCD ACQ ERROR')
        return
end

      
    %TO DO: Make this possibility selectable?
    %[sig xbar ybar gplot1 gplot2 gplot3] = gaussfit2d(ccdData,pars,sig,xbar,ybar);


    %ATTENTION: TO DO. Know if the extDispersion App saved disp values or
    %not. If not, jump this step and write somewhere that emittance didn't
    %take account of dispersion. Or maybe use simulated dispersion.
    %disp= 4 colums like [dx;dxp;dy;dyp], one per otr
    %[stat disp disp_err] = fsCom('DispFit');
        
    %Filling  data for emittance algorithm
    
    %ATTENTION: x<->y seems switched because we're showing ccdData' but fitting ccdData 
    %Put sig in m²
    %TO DO: TAKE OUT THE DISPERSION CONTRIBUTION TO SIGMA // sig(i,i) in meters² [1e-6 factor] because FSimulator works in SI units
        sig(1,1)=sig(1,1)*cos(20);%TAKES IN ACCOUNT THAT SCREEN HAS AN ANGLE! What about this angle in sig(1,2)?!?
        sig_temp_x(i)=sig(2,2)*power(handles.cal(1+otr_numb,8)*1e-6,2);%-(power(disp(1,1+otr_numb)*energy_spread,2));
        sig_temp_y(i)=sig(1,1)*power(handles.cal(1+otr_numb,8)*1e-6,2);%-(power(disp(3,1+otr_numb)*energy_spread,2));
        
        if i==ave
            setappdata(handles.startGuiHandles.Emittance,sprintf('SIGMA11_%d',otr_numb),sig_temp_x)
            setappdata(handles.startGuiHandles.Emittance,sprintf('SIGMA33_%d',otr_numb),sig_temp_y)
        end
    
        lcaPut(['mOTR:procData:',num2str(1+otr_numb),':sigma11'],sig_x(i))
        lcaPut(['mOTR:procData:',num2str(1+otr_numb),':sigma33'],sig_y(i))
        lcaPut(['mOTR:procData:',num2str(1+otr_numb),':sigma13'],sig_xy(i))
        lcaPut(['mOTR:procData:',num2str(1+otr_numb),':x'],xbar)
        lcaPut(['mOTR:procData:',num2str(1+otr_numb),':y'],ybar)
 
  % drawnow('expose')

   pause(1/acquireRate)

if isnan(sig(2,2))|isnan(sig(1,1))|isnan(sig(1,2))|isnan(xbar)|isnan(ybar)
else
        try
                lcaPut(['mOTR:procData',num2str(1+otr_numb),':sigma11'],sig(2,2))%seems changed but not! displaying ccdData' but fitting ccdData
                lcaPut(['mOTR:procData',num2str(1+otr_numb),':sigma33'],sig(1,1))
                lcaPut(['mOTR:procData',num2str(1+otr_numb),':sigma13'],sig(1,2))
                lcaPut(['mOTR:procData',num2str(1+otr_numb),':x'],xbar)
                lcaPut(['mOTR:procData',num2str(1+otr_numb),':y'],ybar)
        catch
                lcaPutNoWait(['mOTR:procData',num2str(1+otr_numb),':sigma11'],sig(2,2))%seems changed but not! displaying ccdData' but fitting ccdData
                lcaPutNoWait(['mOTR:procData',num2str(1+otr_numb),':sigma33'],sig(1,1))
                lcaPutNoWait(['mOTR:procData',num2str(1+otr_numb),':sigma13'],sig(1,2))
                lcaPutNoWait(['mOTR:procData',num2str(1+otr_numb),':x'],xbar)
                lcaPutNoWait(['mOTR:procData',num2str(1+otr_numb),':y'],ybar)

        end
end


   
end
  
% Gaussian fitting routines
function [sig xbar ybar gplot1 gplot2 gplot3]=ellipseFit(c,resx,resy,xbar,ybar)
x=1:resx;
y=(1:resy)';
c=double(c);
norm=sum(sum(c));
if ~exist('xbar','var')
  ybar=sum(x*c)/norm;
  xbar=sum(c*y)/norm;
end
sigyy=sum(((x-ybar).*(x-ybar))*c)/norm;
sigxx=sum(c*((y-xbar).*(y-xbar)))/norm;
sigxy=(x-ybar)*c*(y-xbar)/norm;
sig=[sigxx,sigxy;sigxy,sigyy];
sigx=sqrt(sigxx)/resx;
sigy=sqrt(sigyy)/resy;
theta=atan(2*sigxy/(sigyy-sigxx))/2;
sigaa=(sigx*sigx*cos(theta)*cos(theta)-sigy*sigy*sin(theta)*sin(theta))/cos(2*theta);
sigbb=(sigy*sigy*cos(theta)*cos(theta)-sigx*sigx*sin(theta)*sin(theta))/cos(2*theta);
siga  = sqrt(sigaa);
sigb  = sqrt(sigbb);
phi = 0:pi/100:2*pi;
a1 = siga*cos(phi);
b1 = sigb*sin(phi);
x1 = [cos(theta) sin(theta); -sin(theta) cos(theta)]*[a1; b1];
gplot1(1,:)=(x1(1,:).*resx+xbar);
gplot1(2,:)=(x1(2,:).*resy+ybar);
x1 = [cos(theta) sin(theta); -sin(theta) cos(theta)]*[-siga siga; 0 0];
gplot2(1,:)=(x1(1,:).*resx+xbar);
gplot2(2,:)=(x1(2,:).*resy+ybar);
x1 = [cos(theta) sin(theta); -sin(theta) cos(theta)]*[0 0; -sigb sigb];
gplot3(1,:)=(x1(1,:).*resx+xbar);
gplot3(2,:)=(x1(2,:).*resy+ybar);


function [sig, xbar, ybar, gplot1, gplot2, gplot3] = gaussfit2d(data,pars,sig,xbar,ybar)

p0 = [0 max(max(data)) xbar/pars.sizeX ybar/pars.sizeY sig(1,1) sig(1,2) sig(2,2)];
options = optimset('TolX',1000.0,'Display','iter');
p  = fminsearch(@(p) chisquare(p,double(data)), double(p0), options);

xbar  = p(3)*pars.sizeX;
ybar  = p(4)*pars.sizeY;
sigxx = p(5);
sigxy = p(6);
sigyy = p(7);

sigx = sqrt(sigxx);
sigy = sqrt(sigyy);
sig=[sigxx,sigxy;sigxy,sigyy];

theta = atan(2*sigxy/(sigyy-sigxx))/2;

sigaa = (sigx*sigx*cos(theta)*cos(theta) - sigy*sigy*sin(theta)*sin(theta))/cos(2*theta);
siga  = sqrt(sigaa);
sigbb = (sigy*sigy*cos(theta)*cos(theta) - sigx*sigx*sin(theta)*sin(theta))/cos(2*theta);
sigb  = sqrt(sigbb);

phi = 0:pi/100:2*pi;
a1 = siga*cos(phi);
b1 = sigb*sin(phi);
x1 = [cos(theta) sin(theta); -sin(theta) cos(theta)]*[a1; b1];
gplot1(1,:)=(x1(1,:).*pars.sizeX+xbar);
gplot1(2,:)=(x1(2,:).*pars.sizeY+ybar);
x1 = [cos(theta) sin(theta); -sin(theta) cos(theta)]*[-siga siga; 0 0];
gplot2(1,:)=(x1(1,:).*pars.sizeX+xbar);
gplot2(2,:)=(x1(2,:).*pars.sizeY+ybar);
x1 = [cos(theta) sin(theta); -sin(theta) cos(theta)]*[0 0; -sigb sigb];
gplot3(1,:)=(x1(1,:).*pars.sizeX+xbar);
gplot3(2,:)=(x1(2,:).*pars.sizeY+ybar);

function r = chisquare(p,a)

r = sum(sum((gaussian(p,size(a)) - a).^2));

function g = gaussian(p,a)

x  = 1:a(2);
y  = (1:a(1))';
x2 = ones(a(1),1) * (x - p(3)).^2;
xy = (y - p(4)) * (x - p(3));
y2 = (y - p(4)).^2 * ones(1,a(2));
invsigma2 = inv([p(5) p(6); p(6) p(7)]);
g = p(1) + p(2)*exp(-x2*invsigma2(1,1)/2-xy*invsigma2(1,2)-y2*invsigma2(2,2)/2);


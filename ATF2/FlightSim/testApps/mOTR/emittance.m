function varargout = emittance(varargin)
% EMITTANCE M-file for emittance.fig
%      EMITTANCE, by itself, creates a new EMITTANCE or raises the existing
%      singleton*.
%
%      H = EMITTANCE returns the handle to a new EMITTANCE or the handle to
%      the existing singleton*.
%
%      EMITTANCE('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in EMITTANCE.M with the given input arguments.
%
%      EMITTANCE('Property','Value',...) creates a new EMITTANCE or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before emittance_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to emittance_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help emittance

% Last Modified by GUIDE v2.5 27-Jan-2014 10:17:44

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @emittance_OpeningFcn, ...
                   'gui_OutputFcn',  @emittance_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before emittance is made visible.
function emittance_OpeningFcn(hObject, eventdata, handles, varargin) %#ok<*INUSL>
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to emittance (see VARARGIN)

% Choose default command line output for emittance
handles.output = hObject;

handles.startGuiHandles=varargin{1};

% Update option preference
handles=getOpts(handles);

% Update handles structure
guidata(hObject, handles);


% ignore axes until emittance measurement starts
set(handles.shot,'Visible','off')
set(handles.projx,'Visible','off')
set(handles.projy,'Visible','off')

% UIWAIT makes emittance wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = emittance_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


function figure1_CloseRequestFcn(hObject, eventdata, handles)

% Hint: delete(hObject) closes the figure
delete(hObject);


function calc_emitt_on_Callback(hObject, eventdata, handles) %#ok<*DEFNU>

% Make axis handles visible
set(handles.shot,'Visible','on')
set(handles.projx,'Visible','on')
set(handles.projy,'Visible','on')

set(hObject,'UserData',[]);

% Update option preference
handles=getOpts(handles);

% Get list of OTRs desired for calculation
otruse=[get(handles.checkbox1,'Value') get(handles.checkbox2,'Value') ...
  get(handles.checkbox3,'Value') get(handles.checkbox4,'Value')];
if sum(otruse)<3
  errordlg('Need a minimum of 3 OTRs','Too few OTRs selected')
  return
end

% Number of pulses to acquire on each OTR
nave=round(str2double(get(handles.edit2,'String')));
if isnan(nave) || nave<1 || nave>100
  errordlg('Must choose # pulses >=1 and <=100','# Pulses Error')
  return
end

% Take required number of data and statistics on each OTR if want to take
% new data
handles.cal = load('cal.set', 'calibration', '-ascii');
if get(handles.radiobutton1,'Value')
  % if auto in/out mode then start with all OTR's in
  autoinout=get(handles.checkbox6,'Value');
  lbdisplay(handles,'Setting all OTRs into their measurement positions...');
  if autoinout
    mOTR_start('pushbutton21_Callback',handles.startGuiHandles.pushbutton21,[],handles.startGuiHandles);
  end
  for iotr=1:4
    if ~otruse(iotr); continue; end;
    npulse=0; lastcount=0;
    lbdisplay(handles,sprintf('Taking Data for OTR%dX...',iotr-1))
    ccd_timer('reset');
    % insert OTR
    target_in_out(iotr-1,1);
    system('sleep 2');
    % beam centring?
    if get(handles.checkbox5,'Value')
      beamCentre=true;
    else
      beamCentre=false;
    end
    
    while npulse<nave
      % Check for stop button
      if isequal(get(handles.pushbutton4,'BackgroundColor'),[1 0 0])
        set(handles.pushbutton4,'BackgroundColor','green')
        lbdisplay(handles,'USER STOP REQUESTED')
        return
      end
      % Set OTR / Pulse Counter
      set(handles.text9,'String',sprintf('%d / %d',iotr-1,npulse+1))
      % Process next pulse
      results{iotr}=ccd_timer([],[],handles,iotr-1,nave,handles.beamCut(iotr),handles.cutRatio(iotr),handles.sigmacut(iotr),beamCentre);
      if ~isempty(results{iotr}) && results{iotr}{1}~=lastcount
        npulse=npulse+1;
      end
    end
    % remove OTR
    target_in_out(iotr-1,0);
    % Write OTR results to status display
    txt{1}=sprintf('OTR%dX',iotr-1);
    txt{2}=        '--------';
    txt{3}=sprintf('Centroid (x/y): %.2g +/- %.2g / %.2g +/- %.2g',results{iotr}{2});
    txt{4}=sprintf('Elipse Sigma (x/y): %.2g +/- %.2g / %.2g +/- %.2g',results{iotr}{3}(1:4));
    txt{5}=sprintf('Elipse Sigma_13: %.2g +/- %.2g',results{iotr}{3}(5:6));
    txt{6}=sprintf('Projected Gaussian widths (x/y): %.2g +/- %.2g / %.2g +/- %.2g',results{iotr}{4});
    txt{7}='------------------------------------------';
    lbdisplay(handles,txt);
    % if auto in/out then retract this OTR after the measurement
    if autoinout && iotr>1
      lbdisplay(handles,sprintf('Setting OTR%dX to Non-Measurement position',iotr-1));
      mOTR_start(sprintf('pushbutton%d_Callback',23+(iotr-1)*2),handles.startGuiHandles.(sprintf('pushbutton%d',23+(iotr-1)*2)),[],handles.startGuiHandles);
    end
  end
end

% Request FS server to do the emittance calculation and return the results
% emitData = data formatted for sending over FlECS interface:
%   [energy ...
%    emitx demitx emitxn demitxn embmx dembmx ...
%    bmagx dbmagx bcosx dbcosx bsinx dbsinx ...
%    betax dbetax bx0 alphx dalphx ax0 chi2x ...
%    emity demity emityn demityn embmy dembmy ...
%    bmagy dbmagy bcosy dbcosy bsiny dbsiny ...
%    betay dbetay by0 alphy dalphy ay0 chi2y ...
%    ido length(id) id length(S) S sigxf sigyf ...
%    DX dDX DPX dDPX DY dDY DPY dDPY dp ...
%    sigx dsigx sigy dsigy xf yf ...
%    R{1:notr} ...
%    exip0 bxip0 axip0 eyip0 byip0 ayip0 ...
%    sigxip dsigxip sigpxip dsigpxip betaxip dbetaxip alphxip dalphxip ...
%    sigyip dsigyip sigpyip dsigpyip betayip dbetayip alphyip dalphyip ...
%    Lxw dLxw betaxw dbetaxw sigxw dsigxw ...
%    Lyw dLyw betayw dbetayw sigyw dsigyw ...
%   ]


% use fake data if Woodley wills it
% NOTE: mdwGetFlagVal.m lives in ~/home/mdw
if (exist('mdwGetFlagVal.m','file')==2)
  mdwFlag=mdwGetFlagVal('emittance');
else
  mdwFlag=0;
end

if (mdwFlag)
  otrdata=mdwGetEmit(2,otruse,0.01);
  stat{1}=1;
else
  [stat, otrdata] = fsCom('getemit',otruse,'projected');
end
if stat{1}~=1
  lbdisplay(handles,'Error with projected emittance calculation:');
  lbdisplay(handles,stat{2});
  projdata=[];
else % unpack data
  projdata.energy=otrdata(1);
  projdata.emitx=otrdata(2);
  projdata.demitx=otrdata(3);
  projdata.emitxn=otrdata(4);
  projdata.demitxn=otrdata(5);
  projdata.embmx=otrdata(6);
  projdata.dembmx=otrdata(7);
  projdata.bmagx=otrdata(8);
  projdata.dbmagx=otrdata(9);
  projdata.bcosx=otrdata(10);
  projdata.dbcosx=otrdata(11);
  projdata.bsinx=otrdata(12);
  projdata.dbsinx=otrdata(13);
  projdata.betax=otrdata(14);
  projdata.dbetax=otrdata(15);
  projdata.bx0=otrdata(16);
  projdata.alphx=otrdata(17);
  projdata.dalphx=otrdata(18);
  projdata.ax0=otrdata(19);
  projdata.chi2x=otrdata(20);
  projdata.emity=otrdata(21);
  projdata.demity=otrdata(22);
  projdata.emityn=otrdata(23);
  projdata.demityn=otrdata(24);
  projdata.embmy=otrdata(25);
  projdata.dembmy=otrdata(26);
  projdata.bmagy=otrdata(27);
  projdata.dbmagy=otrdata(28);
  projdata.bcosy=otrdata(29);
  projdata.dbcosy=otrdata(30);
  projdata.bsiny=otrdata(31);
  projdata.dbsiny=otrdata(32);
  projdata.betay=otrdata(33);
  projdata.dbetay=otrdata(34);
  projdata.by0=otrdata(35);
  projdata.alphy=otrdata(36);
  projdata.dalphy=otrdata(37);
  projdata.ay0=otrdata(38);
  projdata.chi2y=otrdata(39);
  notr=0;
  for iotr=find(otruse)
    notr=notr+1;
    projdata.ido(notr)=otrdata(39+notr);
  end
  lid=otrdata(39+notr+1);
  for idata=1:lid
    projdata.id(idata)=otrdata(39+notr+1+idata);
  end
  lS=otrdata(39+notr+1+lid+1);
  for idata=1:lS
    projdata.S(idata)=otrdata(39+notr+1+lid+1+idata);
  end
  ind=39+notr+1+lid+1+lS+1;
  projdata.sigxf=otrdata(ind:ind+lid-1);ind=ind+lid;
  projdata.sigyf=otrdata(ind:ind+lid-1);ind=ind+lid;
  projdata.DX=otrdata(ind:ind+notr-1);ind=ind+notr;
  projdata.dDX=otrdata(ind:ind+notr-1);ind=ind+notr;
  projdata.DPX=otrdata(ind:ind+notr-1);ind=ind+notr;
  projdata.dDPX=otrdata(ind:ind+notr-1);ind=ind+notr;
  projdata.DY=otrdata(ind:ind+notr-1);ind=ind+notr;
  projdata.dDY=otrdata(ind:ind+notr-1);ind=ind+notr;
  projdata.DPY=otrdata(ind:ind+notr-1);ind=ind+notr;
  projdata.dDPY=otrdata(ind:ind+notr-1);ind=ind+notr;
  projdata.dp=otrdata(ind);ind=ind+1;
  projdata.sigx=otrdata(ind:ind+notr-1);ind=ind+notr;
  projdata.dsigx=otrdata(ind:ind+notr-1);ind=ind+notr;
  projdata.sigy=otrdata(ind:ind+notr-1);ind=ind+notr;
  projdata.dsigy=otrdata(ind:ind+notr-1);ind=ind+notr;
  projdata.xf=otrdata(ind:ind+notr-1);ind=ind+notr;
  projdata.yf=otrdata(ind:ind+notr-1);ind=ind+notr;
  for n=1:notr
    projdata.R{n}=reshape(otrdata(ind:ind+15),4,[]); % 4x4 matrices
    ind=ind+16;
  end
  projdata.IP.ex0=otrdata(ind);ind=ind+1;
  projdata.IP.bx0=otrdata(ind);ind=ind+1;
  projdata.IP.ax0=otrdata(ind);ind=ind+1;
  projdata.IP.ey0=otrdata(ind);ind=ind+1;
  projdata.IP.by0=otrdata(ind);ind=ind+1;
  projdata.IP.ay0=otrdata(ind);ind=ind+1;
  projdata.IP.sigx=otrdata(ind);ind=ind+1;
  projdata.IP.dsigx=otrdata(ind);ind=ind+1;
  projdata.IP.sigpx=otrdata(ind);ind=ind+1;
  projdata.IP.dsigpx=otrdata(ind);ind=ind+1;
  projdata.IP.betax=otrdata(ind);ind=ind+1;
  projdata.IP.dbetax=otrdata(ind);ind=ind+1;
  projdata.IP.alphx=otrdata(ind);ind=ind+1;
  projdata.IP.dalphx=otrdata(ind);ind=ind+1;
  projdata.IP.sigy=otrdata(ind);ind=ind+1;
  projdata.IP.dsigy=otrdata(ind);ind=ind+1;
  projdata.IP.sigpy=otrdata(ind);ind=ind+1;
  projdata.IP.dsigpy=otrdata(ind);ind=ind+1;
  projdata.IP.betay=otrdata(ind);ind=ind+1;
  projdata.IP.dbetay=otrdata(ind);ind=ind+1;
  projdata.IP.alphy=otrdata(ind);ind=ind+1;
  projdata.IP.dalphy=otrdata(ind);ind=ind+1;
  projdata.waist.Lx=otrdata(ind);ind=ind+1;
  projdata.waist.dLx=otrdata(ind);ind=ind+1;
  projdata.waist.betax=otrdata(ind);ind=ind+1;
  projdata.waist.dbetax=otrdata(ind);ind=ind+1;
  projdata.waist.sigx=otrdata(ind);ind=ind+1;
  projdata.waist.dsigx=otrdata(ind);ind=ind+1;
  projdata.waist.Ly=otrdata(ind);ind=ind+1;
  projdata.waist.dLy=otrdata(ind);ind=ind+1;
  projdata.waist.betay=otrdata(ind);ind=ind+1;
  projdata.waist.dbetay=otrdata(ind);ind=ind+1;
  projdata.waist.sigy=otrdata(ind);ind=ind+1;
  projdata.waist.dsigy=otrdata(ind);ind=ind+1; %#ok<NASGU>
end
if (~mdwFlag)
  [stat, otrdata] = fsCom('getemit',otruse,'intrinsic');
end
if stat{1}~=1
  lbdisplay(handles,'Error with intrinsic emittance calculation:');
  lbdisplay(handles,stat{2});
  intdata=[];
else % unpack data
  intdata.energy=otrdata(1);
  intdata.emitx=otrdata(2);
  intdata.demitx=otrdata(3);
  intdata.emitxn=otrdata(4);
  intdata.demitxn=otrdata(5);
  intdata.embmx=otrdata(6);
  intdata.dembmx=otrdata(7);
  intdata.bmagx=otrdata(8);
  intdata.dbmagx=otrdata(9);
  intdata.bcosx=otrdata(10);
  intdata.dbcosx=otrdata(11);
  intdata.bsinx=otrdata(12);
  intdata.dbsinx=otrdata(13);
  intdata.betax=otrdata(14);
  intdata.dbetax=otrdata(15);
  intdata.bx0=otrdata(16);
  intdata.alphx=otrdata(17);
  intdata.dalphx=otrdata(18);
  intdata.ax0=otrdata(19);
  intdata.chi2x=otrdata(20);
  intdata.emity=otrdata(21);
  intdata.demity=otrdata(22);
  intdata.emityn=otrdata(23);
  intdata.demityn=otrdata(24);
  intdata.embmy=otrdata(25);
  intdata.dembmy=otrdata(26);
  intdata.bmagy=otrdata(27);
  intdata.dbmagy=otrdata(28);
  intdata.bcosy=otrdata(29);
  intdata.dbcosy=otrdata(30);
  intdata.bsiny=otrdata(31);
  intdata.dbsiny=otrdata(32);
  intdata.betay=otrdata(33);
  intdata.dbetay=otrdata(34);
  intdata.by0=otrdata(35);
  intdata.alphy=otrdata(36);
  intdata.dalphy=otrdata(37);
  intdata.ay0=otrdata(38);
  intdata.chi2y=otrdata(39);
  notr=0;
  for iotr=find(otruse)
    notr=notr+1;
    intdata.ido(notr)=otrdata(39+notr);
  end
  lid=otrdata(39+notr+1);
  for idata=1:lid
    intdata.id(idata)=otrdata(39+notr+1+idata);
  end
  lS=otrdata(39+notr+1+lid+1);
  for idata=1:lS
    intdata.S(idata)=otrdata(39+notr+1+lid+1+idata);
  end
  ind=39+notr+1+lid+1+lS+1;
  intdata.sigxf=otrdata(ind:ind+lid-1);ind=ind+lid;
  intdata.sigyf=otrdata(ind:ind+lid-1);ind=ind+lid;
  intdata.DX=otrdata(ind:ind+notr-1);ind=ind+notr;
  intdata.dDX=otrdata(ind:ind+notr-1);ind=ind+notr;
  intdata.DPX=otrdata(ind:ind+notr-1);ind=ind+notr;
  intdata.dDPX=otrdata(ind:ind+notr-1);ind=ind+notr;
  intdata.DY=otrdata(ind:ind+notr-1);ind=ind+notr;
  intdata.dDY=otrdata(ind:ind+notr-1);ind=ind+notr;
  intdata.DPY=otrdata(ind:ind+notr-1);ind=ind+notr;
  intdata.dDPY=otrdata(ind:ind+notr-1);ind=ind+notr;
  intdata.dp=otrdata(ind);ind=ind+1;
  intdata.sigx=otrdata(ind:ind+notr-1);ind=ind+notr;
  intdata.dsigx=otrdata(ind:ind+notr-1);ind=ind+notr;
  intdata.sigy=otrdata(ind:ind+notr-1);ind=ind+notr;
  intdata.dsigy=otrdata(ind:ind+notr-1);ind=ind+notr;
  intdata.xf=otrdata(ind:ind+notr-1);ind=ind+notr;
  intdata.yf=otrdata(ind:ind+notr-1);ind=ind+notr;
  for n=1:notr
    intdata.R{n}=reshape(otrdata(ind:ind+15),4,[]); % 4x4 matrices
    ind=ind+16;
  end
  intdata.IP.ex0=otrdata(ind);ind=ind+1;
  intdata.IP.bx0=otrdata(ind);ind=ind+1;
  intdata.IP.ax0=otrdata(ind);ind=ind+1;
  intdata.IP.ey0=otrdata(ind);ind=ind+1;
  intdata.IP.by0=otrdata(ind);ind=ind+1;
  intdata.IP.ay0=otrdata(ind);ind=ind+1;
  intdata.IP.sigx=otrdata(ind);ind=ind+1;
  intdata.IP.dsigx=otrdata(ind);ind=ind+1;
  intdata.IP.sigpx=otrdata(ind);ind=ind+1;
  intdata.IP.dsigpx=otrdata(ind);ind=ind+1;
  intdata.IP.betax=otrdata(ind);ind=ind+1;
  intdata.IP.dbetax=otrdata(ind);ind=ind+1;
  intdata.IP.alphx=otrdata(ind);ind=ind+1;
  intdata.IP.dalphx=otrdata(ind);ind=ind+1;
  intdata.IP.sigy=otrdata(ind);ind=ind+1;
  intdata.IP.dsigy=otrdata(ind);ind=ind+1;
  intdata.IP.sigpy=otrdata(ind);ind=ind+1;
  intdata.IP.dsigpy=otrdata(ind);ind=ind+1;
  intdata.IP.betay=otrdata(ind);ind=ind+1;
  intdata.IP.dbetay=otrdata(ind);ind=ind+1;
  intdata.IP.alphy=otrdata(ind);ind=ind+1;
  intdata.IP.dalphy=otrdata(ind);ind=ind+1;
  intdata.waist.Lx=otrdata(ind);ind=ind+1;
  intdata.waist.dLx=otrdata(ind);ind=ind+1;
  intdata.waist.betax=otrdata(ind);ind=ind+1;
  intdata.waist.dbetax=otrdata(ind);ind=ind+1;
  intdata.waist.sigx=otrdata(ind);ind=ind+1;
  intdata.waist.dsigx=otrdata(ind);ind=ind+1;
  intdata.waist.Ly=otrdata(ind);ind=ind+1;
  intdata.waist.dLy=otrdata(ind);ind=ind+1;
  intdata.waist.betay=otrdata(ind);ind=ind+1;
  intdata.waist.dbetay=otrdata(ind);ind=ind+1;
  intdata.waist.sigy=otrdata(ind);ind=ind+1;
  intdata.waist.dsigy=otrdata(ind);ind=ind+1; %#ok<NASGU>
end

% Post data PVs
if (~mdwFlag)
  if ~isempty(projdata)
    lcaPut('mOTR:procData:projemitx',projdata.emitx)
    lcaPut('mOTR:procData:projemity',projdata.emity)
    lcaPut('mOTR:procData:betax',projdata.betax)
    lcaPut('mOTR:procData:betay',projdata.betay)
    lcaPut('mOTR:procData:betax_err',projdata.dbetax)
    lcaPut('mOTR:procData:betay_err',projdata.dbetay)
    lcaPut('mOTR:procData:alphax',projdata.alphx)
    lcaPut('mOTR:procData:alphay',projdata.alphy)
    lcaPut('mOTR:procData:alphax_err',projdata.dalphx)
    lcaPut('mOTR:procData:alphay_err',projdata.dalphy)
  end
  if ~isempty(intdata)
    lcaPut('mOTR:procData:intemitx',intdata.emitx)
    lcaPut('mOTR:procData:intemity',intdata.emity)
  end
end

set(hObject,'UserData',{ ...
  [projdata.emitx projdata.demitx projdata.emity projdata.demity ...
   intdata.emitx intdata.demitx intdata.emity intdata.demity]; ...
  [projdata.embmx projdata.dembmx projdata.embmy projdata.dembmy ...
   intdata.embmx intdata.dembmx intdata.embmy intdata.dembmy]});

% Present data analysis
if ~isempty(projdata)
  displaytxt.proj=presentdata(handles,false,notr,projdata);
else
  displaytxt.proj=[];
end
if ~isempty(intdata)
  displaytxt.int=presentdata(handles,true,notr,intdata);
else
  displaytxt.int=[];
end

% Keep data
handles.displaytxt=displaytxt;
handles.projdata=projdata;
handles.intdata=intdata;
guidata(handles.figure1,handles);

% function to format and display emittance data
function txt=presentdata(handles,dointrinsic,notr,data)
if dointrinsic
  measType='ellipse';
else
  measType='projected';
end
txt={};

txt{end+1}=sprintf('Horizontal %s emittance parameters at first OTR',measType);
txt{end+1}='-----------------------------------------------------';
txt{end+1}=sprintf('energy     = %10.4f              GeV',data.energy);
txt{end+1}=sprintf('emit       = %10.4f +- %9.4f nm',1e9*[data.emitx,data.demitx]);
txt{end+1}=sprintf('emitn      = %10.4f +- %9.4f nm',1e9*[data.emitxn,data.demitxn]);
txt{end+1}=sprintf('emit*bmag  = %10.4f +- %9.4f nm',1e9*[data.embmx,data.dembmx]);
txt{end+1}=sprintf('bmag       = %10.4f +- %9.4f      (%9.4f)',data.bmagx,data.dbmagx,1);
txt{end+1}=sprintf('bmag_cos   = %10.4f +- %9.4f      (%9.4f)',data.bcosx,data.dbcosx,0);
txt{end+1}=sprintf('bmag_sin   = %10.4f +- %9.4f      (%9.4f)',data.bsinx,data.dbsinx,0);
txt{end+1}=sprintf('beta       = %10.4f +- %9.4f m    (%9.4f)',data.betax,data.dbetax,data.bx0);
txt{end+1}=sprintf('alpha      = %10.4f +- %9.4f      (%9.4f)',data.alphx,data.dalphx,data.ax0);
txt{end+1}=sprintf('chisq/N    = %10.4f',data.chi2x);
txt{end+1}=' ';

txt{end+1}=sprintf('Horizontal %s emittance parameters at IP',measType);
txt{end+1}='-----------------------------------------------------';
sigx0=sqrt(data.emitx*data.IP.bx0);
sigpx0=sqrt(data.emitx*(1+data.IP.ax0^2)/data.IP.bx0);
txt{end+1}=sprintf('sig        = %10.4f +- %9.4f um   (%9.4f)',1e6*[data.IP.sigx,data.IP.dsigx,sigx0]);
txt{end+1}=sprintf('sigp       = %10.4f +- %9.4f ur   (%9.4f)',1e6*[data.IP.sigpx,data.IP.dsigpx,sigpx0]);
txt{end+1}=sprintf('beta       = %10.4f +- %9.4f mm   (%9.4f)',1e3*[data.IP.betax,data.IP.dbetax,data.IP.bx0]);
txt{end+1}=sprintf('alpha      = %10.4f +- %9.4f      (%9.4f)',data.IP.alphx,data.IP.dalphx,data.IP.ax0);
txt{end+1}=' ';

txt{end+1}=sprintf('Horizontal %s emittance parameters at waist',measType);
txt{end+1}='-----------------------------------------------------';
txt{end+1}=sprintf('L          = %10.4f +- %9.4f m',data.waist.Lx,data.waist.dLx);
txt{end+1}=sprintf('beta       = %10.4f +- %9.4f mm',1e3*[data.waist.betax,data.waist.dbetax]);
txt{end+1}=sprintf('sig        = %10.4f +- %9.4f um',1e6*[data.waist.sigx,data.waist.dsigx]);
txt{end+1}=' ';

txt{end+1}=sprintf('Vertical %s emittance parameters at first OTR',measType);
txt{end+1}='-----------------------------------------------------';
txt{end+1}=sprintf('energy     = %10.4f              GeV',data.energy);
txt{end+1}=sprintf('emit       = %10.4f +- %9.4f pm',1e12*[data.emity,data.demity]);
txt{end+1}=sprintf('emitn      = %10.4f +- %9.4f nm',1e9*[data.emityn,data.demityn]);
txt{end+1}=sprintf('emit*bmag  = %10.4f +- %9.4f pm',1e12*[data.embmy,data.dembmy]);
txt{end+1}=sprintf('bmag       = %10.4f +- %9.4f      (%9.4f)',data.bmagy,data.dbmagy,1);
txt{end+1}=sprintf('bmag_cos   = %10.4f +- %9.4f      (%9.4f)',data.bcosy,data.dbcosy,0);
txt{end+1}=sprintf('bmag_sin   = %10.4f +- %9.4f      (%9.4f)',data.bsiny,data.dbsiny,0);
txt{end+1}=sprintf('beta       = %10.4f +- %9.4f m    (%9.4f)',data.betay,data.dbetay,data.by0);
txt{end+1}=sprintf('alpha      = %10.4f +- %9.4f      (%9.4f)',data.alphy,data.dalphy,data.ay0);
txt{end+1}=sprintf('chisq/N    = %10.4f',data.chi2y);
txt{end+1}=' ';

txt{end+1}=sprintf('Vertical %s emittance parameters at IP',measType);
txt{end+1}='-----------------------------------------------------';
sigy0=sqrt(data.emity*data.IP.by0);
sigpy0=sqrt(data.emity*(1+data.IP.ay0^2)/data.IP.by0);
txt{end+1}=sprintf('sig        = %10.4f +- %9.4f um   (%9.4f)',1e6*[data.IP.sigy,data.IP.dsigy,sigy0]);
txt{end+1}=sprintf('sigp       = %10.4f +- %9.4f ur   (%9.4f)',1e6*[data.IP.sigpy,data.IP.dsigpy,sigpy0]);
txt{end+1}=sprintf('beta       = %10.4f +- %9.4f mm   (%9.4f)',1e3*[data.IP.betay,data.IP.dbetay,data.IP.by0]);
txt{end+1}=sprintf('alpha      = %10.4f +- %9.4f      (%9.4f)',data.IP.alphy,data.IP.dalphy,data.IP.ay0);
txt{end+1}=' ';

txt{end+1}=sprintf('Vertical %s emittance parameters at waist',measType);
txt{end+1}='-----------------------------------------------------';
txt{end+1}=sprintf('L          = %10.4f +- %9.4f m',data.waist.Ly,data.waist.dLy);
txt{end+1}=sprintf('beta       = %10.4f +- %9.4f mm',1e3*[data.waist.betay,data.waist.dbetay]);
txt{end+1}=sprintf('sig        = %10.4f +- %9.4f um',1e6*[data.waist.sigy,data.waist.dsigy]);
txt{end+1}=' ';

lbdisplay(handles,txt')

function edit2_Callback(hObject, eventdata, handles)
% hObject    handle to edit2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit2 as text
%        str2double(get(hObject,'String')) returns contents of edit2 as a double


% --- Executes during object creation, after setting all properties.
function edit2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in checkbox1.
function checkbox1_Callback(hObject, eventdata, handles)
% hObject    handle to checkbox1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of checkbox1


% --- Executes on button press in checkbox2.
function checkbox2_Callback(hObject, eventdata, handles)
% hObject    handle to checkbox2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of checkbox2


% --- Executes on button press in checkbox3.
function checkbox3_Callback(hObject, eventdata, handles)
% hObject    handle to checkbox3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of checkbox3


% --- Executes on button press in checkbox4.
function checkbox4_Callback(hObject, eventdata, handles)
% hObject    handle to checkbox4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of checkbox4


% --- Executes on selection change in listbox1.
function listbox1_Callback(hObject, eventdata, handles)
% hObject    handle to listbox1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns listbox1 contents as cell array
%        contents{get(hObject,'Value')} returns selected item from listbox1


% --- Executes during object creation, after setting all properties.
function listbox1_CreateFcn(hObject, eventdata, handles) %#ok<*INUSD>
% hObject    handle to listbox1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: listbox controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in pushbutton4.
function pushbutton4_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
set(hObject,'BackgroundColor','red')
drawnow('expose')

function lbdisplay(handles,text)
lbtext=get(handles.listbox1,'String');
if isempty(lbtext)
  if iscell(text)
    lbtext=text;
  else
    lbtext={text};
  end
else
  if ~iscell(lbtext); lbtext={lbtext}; end;
  if iscell(text)
    for itxt=1:length(text)
      lbtext{end+1}=text{itxt};
    end
  else
    lbtext{end+1}=text;
  end
end
set(handles.listbox1,'String',lbtext)
set(handles.listbox1,'Value',length(lbtext))
drawnow('expose')


% --- Projected emittance plots
function pushbutton5_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton5 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
if ~isfield(handles,'projdata') || isempty(handles.projdata)
  errordlg('No emittance calculation performed or error in projected emittance calculation, run emittance calc or see message box for error messages','No data')
  return
end
if ~isdeployed
  load ../../latticeFiles/ATF2lat BEAMLINE Model
else
  load ATF2lat BEAMLINE Model
end

% reconstructed normalized phase space plots
ido=handles.projdata.ido;
notr=length(ido);
oname=cell(1,notr);
for n=1:notr
  oname{n}=BEAMLINE{ido(n)}.Name;
end

egamma=Model.Initial.Momentum/0.51099906e-3;
ex0=Model.Initial.x.NEmit/egamma;
bx0=handles.projdata.bx0;
ax0=handles.projdata.ax0;
ex=handles.projdata.emitx;
bx=handles.projdata.betax;
ax=handles.projdata.alphx;
R=handles.projdata.R;
sigx=handles.projdata.sigx;
dsigx=handles.projdata.dsigx;
figure
mOTRemitPlot(0,1,oname,ex0,bx0,ax0,ex,bx,ax,R,sigx,dsigx) % horizontal

ey0=Model.Initial.y.NEmit/egamma;
by0=handles.projdata.by0;
ay0=handles.projdata.ay0;
ey=handles.projdata.emity;
by=handles.projdata.betay;
ay=handles.projdata.alphy;
sigy=handles.projdata.sigy;
dsigy=handles.projdata.dsigy;
figure
mOTRemitPlot(1,1,oname,ey0,by0,ay0,ey,by,ay,R,sigy,dsigy) % vertical

% propagated fitted beam size plots
figure
plot(handles.projdata.S(handles.projdata.id),1e6*handles.projdata.sigxf,'b--')
hold on
plot_barsc(handles.projdata.S(handles.projdata.ido)',1e6*handles.projdata.sigx',1e6*handles.projdata.dsigx','b','o')
hold off
set(gca,'XLim',[handles.projdata.S(handles.projdata.id(1))',handles.projdata.S(handles.projdata.id(end))])
title('EXT Diagnostics Section')
ylabel('Horizontal Beam Size (um)')
xlabel('S (m)')
plot_magnets_Lucretia(BEAMLINE(handles.projdata.id),1,1);
figure
plot(handles.projdata.S(handles.projdata.id),1e6*handles.projdata.sigyf,'b--')
hold on
plot_barsc(handles.projdata.S(handles.projdata.ido)',1e6*handles.projdata.sigy',1e6*handles.projdata.dsigy','b','o')
hold off
set(gca,'XLim',[handles.projdata.S(handles.projdata.id(1))',handles.projdata.S(handles.projdata.id(end))])
title('EXT Diagnostics Section')
ylabel('Vertical Beam Size (um)')
xlabel('S (m)')
plot_magnets_Lucretia(BEAMLINE(handles.projdata.id),1,1);


% --- Intrinsic emittance plots
function pushbutton6_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton6 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
if ~isfield(handles,'intdata') || isempty(handles.intdata)
  errordlg('No emittance calculation performed or error in intrinsic emittance calculation, run emittance calc or see message box for error messages','No data')
  return
end
if ~isdeployed
  load ../../latticeFiles/ATF2lat BEAMLINE Model
else
  load ATF2lat BEAMLINE Model
end

% reconstructed normalized phase space plots
ido=handles.intdata.ido;
notr=length(ido);
oname=cell(1,notr);
for n=1:notr
  oname{n}=BEAMLINE{ido(n)}.Name;
end

egamma=Model.Initial.Momentum/0.51099906e-3;
ex0=Model.Initial.x.NEmit/egamma;
bx0=handles.intdata.bx0;
ax0=handles.intdata.ax0;
ex=handles.intdata.emitx;
bx=handles.intdata.betax;
ax=handles.intdata.alphx;
R=handles.intdata.R;
sigx=handles.intdata.sigx;
dsigx=handles.intdata.dsigx;
figure
mOTRemitPlot(0,0,oname,ex0,bx0,ax0,ex,bx,ax,R,sigx,dsigx) % horizontal

ey0=Model.Initial.y.NEmit/egamma;
by0=handles.intdata.by0;
ay0=handles.intdata.ay0;
ey=handles.intdata.emity;
by=handles.intdata.betay;
ay=handles.intdata.alphy;
sigy=handles.intdata.sigy;
dsigy=handles.intdata.dsigy;
figure
mOTRemitPlot(1,0,oname,ey0,by0,ay0,ey,by,ay,R,sigy,dsigy) % vertical

% propagated fitted beam size plots
figure
plot(handles.intdata.S(handles.intdata.id),1e6*handles.intdata.sigxf,'b--')
hold on
plot_barsc(handles.intdata.S(handles.intdata.ido)',1e6*handles.intdata.sigx',1e6*handles.intdata.dsigx','b','o')
hold off
set(gca,'XLim',[handles.intdata.S(handles.intdata.id(1)),handles.intdata.S(handles.intdata.id(end))])
title('EXT Diagnostics Section')
ylabel('Horizontal Beam Size (um)')
xlabel('S (m)')
plot_magnets_Lucretia(BEAMLINE(handles.intdata.id),1,1);
figure
plot(handles.intdata.S(handles.intdata.id),1e6*handles.intdata.sigyf,'b--')
hold on
plot_barsc(handles.intdata.S(handles.intdata.ido)',1e6*handles.intdata.sigy',1e6*handles.intdata.dsigy','b','o')
hold off
set(gca,'XLim',[handles.intdata.S(handles.intdata.id(1)),handles.intdata.S(handles.intdata.id(end))])
title('EXT Diagnostics Section')
ylabel('Vertical Beam Size (um)')
xlabel('S (m)')
plot_magnets_Lucretia(BEAMLINE(handles.intdata.id),1,1);

% --- Display projected data
function pushbutton7_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton7 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
if ~isfield(handles,'displaytxt') || ~isfield(handles.displaytxt,'proj') || isempty(handles.displaytxt.proj)
  errordlg('No emittance calculation performed or error in projected emittance calculation, run emittance calc or see message box for error messages','No data')
  return
end
msgbox(handles.displaytxt.proj)

% --- Display intrinsic data
function pushbutton8_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton8 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
if ~isfield(handles,'displaytxt') || ~isfield(handles.displaytxt,'int') || isempty(handles.displaytxt.int)
  errordlg('No emittance calculation performed or error in intrinsic emittance calculation, run emittance calc or see message box for error messages','No data')
  return
end
msgbox(handles.displaytxt.int)


% --- Executes on button press in checkbox5.
function checkbox5_Callback(hObject, eventdata, handles)
% hObject    handle to checkbox5 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of checkbox5


% --------------------------------------------------------------------
function Untitled_1_Callback(hObject, eventdata, handles)
% hObject    handle to Untitled_1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --------------------------------------------------------------------
function saveData_Callback(hObject, eventdata, handles)
% hObject    handle to saveData (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

if ~isfield(handles,'projdata') || ~isfield(handles,'intrinsicData')
  errordlg('No data to save','Save error');
  return
end
projData=handles.projdata; %#ok<NASGU>
intrinsicData=handles.intdata; %#ok<NASGU>
[file, direc]=uiputfile('*.mat');
if ~file; return; end;
save(fullfile(direc,file),'projData','intrinsicData');


% --- Executes on button press in pushbutton9.
function pushbutton9_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton9 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

%intensities to B
IBlookup=[-20 -16 -12 -8 -4 0 4 8 12 16 20;
         -0.2227 -0.1779 -0.1331 -0.0884 -0.0439 0 0.0439 0.0884 0.1331 0.1779 0.2227];

% Get list of OTRs desired for calculation
otruse=[get(handles.checkbox1,'Value') get(handles.checkbox2,'Value') ...
  get(handles.checkbox3,'Value') get(handles.checkbox4,'Value')];

method=get(handles.popupmenu1,'Value');
if method==1
  [stat, Bcorrect] = fsCom('correctcoupling',otruse,1);
elseif method==2
  if any(~otruse)
    errordlg('Must select all 4 OTRs for model optimizer method','Selection error');
  else
    [stat, Bcorrect] = fsCom('correctcoupling',otruse,2);
  end
end

if stat{1}==-1;
  lbdisplay(handles,'Error correcting coupling')
  lbdisplay(handles,stat{2});
  return;
end
lbdisplay(handles,'Coupling correction finished, QK calculated values:');
Icorrect = interp1(IBlookup(2,:),IBlookup(1,:),Bcorrect(1:4));
for i=1:4
  lbdisplay(handles,sprintf('QK%dX: %10.2f A',i,Icorrect(i)));
  lbdisplay(handles,' ');
end
txt=sprintf('Coupling correction calculation complete. Requested Skew quad changes:\nQK1X = %.2f QK2X = %.2f QK3X = %.2f QK4X = %.2f\nApply?',...
  Icorrect);
qresp=questdlg(txt,'Calculation Complete');
if strcmp(qresp,'Yes')
  stat = fsCom('correctcoupling',otruse,3);
end
if stat{1}==-1;
  lbdisplay(handles,'Error applying skew quad values')
  lbdisplay(handles,stat{2});
  return;
end

function handles=getOpts(handles)
% sigma cut setting
if exist('sigmacut.mat','file')
  load sigmacut.mat sigmacut
  handles.sigmacut=sigmacut;
  for iotr=1:4
    if length(handles.sigmacut)<iotr || ~handles.sigmacut(iotr)
      handles.sigmacut(iotr)=3;
    end
  end
end

% cut ratio setting
if exist('cutRatio.mat','file')
  load cutRatio.mat cutRatio
  handles.cutRatio=cutRatio;
  for iotr=1:4
    if length(handles.cutRatio)<iotr || ~handles.cutRatio(iotr)
      handles.cutRatio(iotr)=0.2e-2;
    end
  end
end

% Beam presence cut
if exist('beamCut.mat','file')
  load beamCut.mat beamCut
  handles.beamCut=beamCut;
  for iotr=1:4
    if length(handles.beamCut)<iotr || ~handles.beamCut(iotr)
      handles.beamCut(iotr)=1e8;
    end
  end
end

% Beam centring
% if exist('beamCentring.mat','file')
%   load beamCentring.mat beamCentring
%   if sum(beamCentring)==4
%     set(handles.checkbox5,'Value',1)
%   end
% end

% Update handles structure
guidata(handles.figure1, handles);


% --- QK scan
function pushbutton10_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton10 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
fhan=qkscan(handles);
% uiwait(fhan.figure1);
co=get(fhan.figure1,'CurrentObject');
while isempty(co) || (co~=fhan.pushbutton1 && co~=fhan.pushbutton2)
%   uiwait(fhan.figure1);
  co=get(fhan.figure1,'CurrentObject');
  pause(0.2)
end
if co==fhan.pushbutton2
  delete(fhan.figure1);
  return
end

qk=get(fhan.popupmenu1,'String'); qk=qk{get(fhan.popupmenu1,'Value')};
lval=str2double(get(fhan.edit1,'String'));
hval=str2double(get(fhan.edit2,'String'));
nsteps=str2double(get(fhan.edit3,'String'));
qkscanVal=linspace(lval,hval,nsteps);
useproj=get(fhan.radiobutton1,'Value');
emitType=1+get(fhan.radiobutton3,'Value');
ex=[]; ey=[]; dex=[]; dey=[];
[stat, iMagVal]=fsCom('getmagnet',qk);
if stat{1}~=1
  errordlg(stat{2},'QK scan error');
  return
end
for iscan=qkscanVal
  stat = fsCom('setmagnet',qk,qkscanVal(iscan));
  if stat{1}~=1
    errordlg(stat{2},'QK scan error');
    return
  end
  pause(5)
  takeNew=true;
  % check for abort
  if isequal(get(handles.pushbutton4,'BackgroundColor'),[1 0 0])
    break
  end
  % take new emittance data
  while takeNew
    calc_emitt_on_Callback(handles.calc_emitt_on,[],handles);
    emitdata=get(handles.calc_emitt_on,'UserData');
    if isempty(emitdata)
      if strcmp(questdlg('No emittance data, retake?','Measurement Failed'),'Yes')
        continue
      else
        takeNew=false;
      end
    end
  end
  if useproj
    ex(end+1)=emitdata{emitType}(1);
    dex(end+1)=emitdata{emitType}(2);
    ey(end+1)=emitdata{emitType}(3);
    dey(end+1)=emitdata{emitType}(4);
  else
    ex(end+1)=emitdata{emitType}(5);
    dex(end+1)=emitdata{emitType}(6);
    ey(end+1)=emitdata{emitType}(7);
    dey(end+1)=emitdata{emitType}(8);
  end
  errorbar(fhan.axes1,qkscanVal(1:iscan),ey(1:iscan),dey(1:iscan),'ro');
  grid(fhan.axes1,'on')
  if iscan>=3
    q=noplot_parab(qkscanVal(1:iscan),ey(1:iscan),dey(1:iscan));
    hold(fhan.axes1,'on')
    plot(fhan.axex1,q(1).*(qkscanVal(1:iscan)-q(2)).^2+q(3),'b');
    set(fhan.text4,'String',sprintf('%.2f +/- %.2f',q(2)))
  end
end
resp=questdlg(sprintf('Set %s to optimal value: %.2f A?',qk,q(2)),'Set Magnet?');
if strcmp(resp,'Yes')
  stat = fsCom('setmagnet',qk,q(2));
else
  stat = fsCom('setmagnet',qk,iMagVal);
end
if stat{1}~=1
  errordlg(stat{2},'QK scan error');
  return
end
set(handles.pushbutton4,'BackgroundColor','green')
delete(fhan.figure1);


% --- Executes on selection change in popupmenu1.
function popupmenu1_Callback(hObject, eventdata, handles)
% hObject    handle to popupmenu1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns popupmenu1 contents as cell array
%        contents{get(hObject,'Value')} returns selected item from popupmenu1


% --- Executes during object creation, after setting all properties.
function popupmenu1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to popupmenu1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in checkbox6.
function checkbox6_Callback(hObject, eventdata, handles)
% hObject    handle to checkbox6 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of checkbox6


% --- Executes on button press in pushbutton11.
function pushbutton11_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton11 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
mh=msgbox('Calculating beta match...'); drawnow('expose');
[stat,T,Td,Inow,Imatch]=fsCom('doextmatch');
if stat{1}~=1
  errordlg(sprintf('Error performing EXT Beta matching:\n%s',stat{2}),'EXT Match Error');
  return
elseif isempty(T) || isempty(Td) || isempty(Inow) || isempty(Imatch)
  errordlg(sprintf('Error performing EXT Beta matching:\nEmpty twiss/quad strength data returned'),'EXT Match Error');
  return
end
mags={'QF1X' 'QD2X' 'QF3X' 'QF4X' 'QD5X' 'QF6X' 'QD7X' 'QF9X'};
txt={'Beta Match results:' sprintf('alpha_x: %.2f(%.2f) beta_x: %.2f(%.2f)\nalpha_y: %.2f(%.2f) beta_y: %.2f(%.2f)',...
  T(1),Td(1),T(2),Td(2),T(3),Td(3),T(4),Td(4)) '========================================'} ;
txt{end+1}='Magnet I(Now) I(Matched)'; txt{end+1}='========================================';
for iquad=1:length(mags)
  txt{end+1}=sprintf('%6s %7.3f %7.3f',mags{iquad},Inow(iquad),Imatch(iquad));
end
if ishandle(mh); delete(mh); end;
qresp=questdlg(txt,'Trim Quads?');
if strcmp(qresp,'Yes')
  stat=fsCom('trimtoextmatch');
  if stat{1}~=1
    errordlg(sprintf('Error trimming EXT quads to match values:\n%s',stat{2}),'Trim Error');
  end
end

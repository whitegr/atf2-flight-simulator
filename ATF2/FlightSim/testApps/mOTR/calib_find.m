function varargout = calib_find(varargin)
% CALIB_FIND MATLAB code for calib_find.fig
%      CALIB_FIND, by itself, creates a new CALIB_FIND or raises the existing
%      singleton*.
%
%      H = CALIB_FIND returns the handle to a new CALIB_FIND or the handle to
%      the existing singleton*.
%
%      CALIB_FIND('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in CALIB_FIND.M with the given input arguments.
%
%      CALIB_FIND('Property','Value',...) creates a new CALIB_FIND or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before calib_find_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to calib_find_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help calib_find

% Last Modified by GUIDE v2.5 15-Nov-2010 15:37:45

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @calib_find_OpeningFcn, ...
                   'gui_OutputFcn',  @calib_find_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before calib_find is made visible.
function calib_find_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to calib_find (see VARARGIN)
handles.startGuiHandles=varargin{1};


% Choose default command line output for calib_find
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes calib_find wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = calib_find_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on button press in continue_calib_find.
function continue_calib_find_Callback(hObject, eventdata, handles)
% hObject    handle to continue_calib_find (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
handles=varagin{1}

%   orbit0 - orbit3 = [x x' y y' dx dx' dy dy'] orbit fits and errors
[stat orbit0 orbit1 orbit2 orbit3] = fsCom('OrbitFit',5);
if stat{1}~=1
        set(handles.text_under_screen,'String',stat{2})
        pause(3)
        set(handles.text_under_screen,'String','')
else
	fit_x_zero=lcaGet(handles.RDBL{(1+3*otr_numb)})+eval(['orbit',num2str(handles.startGuiHandles.otr_numb),'(1)'])*10e6/handles.cal(1+handles.otr_numb,5);%PLUS SIGN BECAUSE VOLTS AND MICRONS GROW OPPOSITE
	fit_y_zero=lcaGet(handles.RDBL{(2+3*otr_numb)})-eval(['orbit',num2str(handles.startGuiHandles.otr_numb),'(3)'])*10e6/handles.cal(1+handles.otr_numb,6);

	if ~exist('fit_beam.dat')
		fit_beam(1+handles.startGuiHandles.otr_numb,2)=fit_x_zero;
    	   	fit_beam(1+handles.startGuiHandles.otr_numb,3)=fit_y_zero;
       	 	fit_beam(1+handles.startGuiHandles.otr_numb,4)=1;
		save('fit_beam.dat','fit_beam','-ascii')
	else
		load('fit_beam.dat','fit_beam','-ascii')
		fit_beam(1+handles.startGuiHandles.otr_numb,2)=fit_x_zero;
		fit_beam(1+handles.startGuiHandles.otr_numb,3)=fit_y_zero;
		fit_beam(1+handles.startGuiHandles.otr_numb,4)=1;
		save('fit_beam.dat','fit_beam','-ascii')
	end
end

% --- Executes on button press in cancel_calib_find.
function cancel_calib_find_Callback(hObject, eventdata, handles)
% hObject    handle to cancel_calib_find (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
delete(gcf)

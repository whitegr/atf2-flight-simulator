function motor_test(cmd,varargin)
% Test function for driving mOTR motors
% motor_test('Setup',motorList)
%   setup motors for use (motorList = vector of motor numbers to use)
% motor_test('Move',whichMotor,steps)
%   Move motor "whichMotor" to position "steps"
% motor_test('MotorSeq',first,last,N,return,Niter,waitTime,whichMotor)
%   Mover motor (whichMotor = integer motor number) from position (first)
%   to position (last) in N steps pausing (waitTime seconds) after each
%   step. If return = true, then subsequently move back from last to first
%   in N steps. Iterate the whole procedure (Niter times).
%     e.g. motor_test('MotorSeq',100,300,3,1,3,1,1)
%     moves motor 1 through the following sequence pausing 1 s after each
%     step: 100 200 300 200 100 200 300 200 100 200 300 200 100
%     e.g. motor_test('MotorSeq',100,300,3,0,3,1,1)
%     100 200 300 100 200 300 100 200 300
%% Setup options
% array entries are {X Y FOCUS} * 4 rows for OTR 1-4 (single row assumes
% same for all OTRs)
motorPV='mOTR:motorN:'; % N is replaced by motor #
% PV names for digital output to control OTR insertion (0=out 1=in)
insertPV={'mOTR:XPS1Aux1Bo0' 'mOTR:XPS1Aux1Bo1' 'mOTR:XPS1Aux1Bo2' 'mOTR:XPS1Aux1Bo3'};
% PV names for digital output to control switch between Focus and Y stage
% linear readback for Ai1 and Ai3 (0=X 1=Focus)
rdbkSwitchPV={'mOTR:XPS1Aux1Bo4' 'mOTR:XPS1Aux1Bo5' 'mOTR:XPS1Aux1Bo6' 'mOTR:XPS1Aux1Bo7'};
% resIO=10 / 2^13; % Analogue input resolution: +/- 10V (14-bit) 
setup.EGU={'steps' 'steps' 'steps'}; % Units in use for motor moves
setup.VMAX={500 500 500}; % Max setable velocities
setup.VELO={200 200 200}; % Set velocities
setup.ACCL={0.02 0.02 0.02}; % Set accelerations
setup.BVEL={200 200 200}; % Set velocities to use for backlash correction
setup.BACC={0.01 0.01 0.01}; % set accelerations to use for backlash correction
setup.RDBD={10 10 10}; % Deadband to use to know when motor move has reached destination
setup.RTRY={5 5 5}; % max retries to get motor in deadband
setup.UEIP={1 1 1}; % encoder readback
setup.URIP={0 0 0}; % PV readback
% analogue in channels to use for readback
setup.RDBL={'mOTR:XPS1AuxAi0' 'mOTR:XPS1AuxAi1' 'mOTR:XPS1AuxAi1';
            'mOTR:XPS1AuxAi2' 'mOTR:XPS1AuxAi3' 'mOTR:XPS1AuxAi3';
            'mOTR:XPS2AuxAi0' 'mOTR:XPS2AuxAi1' 'mOTR:XPS2AuxAi1';
            'mOTR:XPS2AuxAi2' 'mOTR:XPS2AuxAi3' 'mOTR:XPS2AuxAi3'};
setup.OFF={0 0 0}; % calibration offsets
setup.SREV={200 200 200}; % Steps per revolution
setup.UREV={200 200 200}; % Units (EGU) per revolution
setup.RRES={1 1 1}; % Readback units per EGU
setup.HLM={10000 10000 10000}; % High limits
setup.LLM={-10000 -10000 -10000}; % Low limits
setup.SPMG={3 3 3}; % run control 0,1,2,3 = 'stop','pause','move','go' We use 'move' and not 'go' here
setup.BDST={0 0 0}; % backlash correction distance to use
% Pre-move commands to issue (change readback source before issuing move
% command)
% setup.PREM={[] ['PUT(',rdbkSwitchPV{1},',0,0.1)'] ['PUT(',rdbkSwitchPV{1},',1,0.1)'];
%             [] ['PUT(',rdbkSwitchPV{2},',0,0.1)'] ['PUT(',rdbkSwitchPV{2},',1,0.1)'];
%             [] ['PUT(',rdbkSwitchPV{3},',0,0.1)'] ['PUT(',rdbkSwitchPV{3},',1,0.1)'];
%             [] ['PUT(',rdbkSwitchPV{4},',0,0.1)'] ['PUT(',rdbkSwitchPV{4},',1,0.1)']};
%% command options
switch lower(cmd)
  case 'setup'
    motorSetup(setup,motorPV,1,varargin{1}); % Issue setup commands
  case 'example'
    %% Example move sequence
    lcaPutNoWait(insertPV{1},1); % Insert OTR 1
    % Get current position (X and Y)
    lcaPutNoWait(rdbkSwitchPV,0); % select Y readback
    curPos=[lcaGet('mOTR:motor1.RBV') lcaGet('mOTR:motor2.RBV')];
    % Get Focus motor position readback
    pause(1); lcaPutNoWait(rdbkSwitchPV,1); % select Focus readback
    pause(1); curPos(3)=lcaGet('mOTR:motor3.RBV');
    % Move X + 0.2mm Y - 0.15mm
    lcaPutNoWait('mOTR:motor1',curPos(1)+0.2);
    lcaPutNoWait('mOTR:motor2',curPos(2)-0.15);
    % Command moves
    lcaPutNoWait('mOTR:motor1.SPMG',2);
    lcaPutNoWait('mOTR:motor2.SPMG',2);
    % Wait for move to finish
    pause(0.1)
    while any([lcaGet('mOTR:motor1.SPMG') lcaGet('mOTR:motor2.SPMG')]==2)
      pause(0.1)
    end
  case 'move'
    % Program move (absolute)
    lcaPutNoWait(['mOTR:motor',num2str(varargin{1})],varargin{2});
    % Command move
%     lcaPutNoWait(['mOTR:motor',num2str(varargin{1}),'.SPMG'],2);
     % Wait for move to finish
%     pause(0.1)
%     while ~strcmp(lcaGet(['mOTR:motor',num2str(varargin{1}),'.SPMG']),'Pause')
%       pause(0.1)
%     end
  case 'motorseq'
    first=varargin{1}; last=varargin{2}; nsteps=varargin{3}; ret=varargin{4}; niter=varargin{5};
    waitTime=varargin{6}; imotor=varargin{7};
    lcaPutNoWait(['mOTR:motor',num2str(imotor),'.SPMG'],2);
    for iter=1:niter
      for isteps=linspace(first,last,nsteps)
        lcaPutNoWait(['mOTR:motor',num2str(imotor)],isteps);
        lcaPutNoWait(['mOTR:motor',num2str(imotor),'.SPMG'],2);
        pause(waitTime)
      end
      if ret
        for isteps=linspace(last,first,nsteps)
          lcaPutNoWait(['mOTR:motor',num2str(imotor)],isteps);
          lcaPutNoWait(['mOTR:motor',num2str(imotor),'.SPMG'],2);
          pause(waitTime)
        end
      end
    end
end
%% Process setup commands
function motorSetup(pv,mpv,otrList,motorList)
fn=fieldnames(pv);
for iotr=otrList
  for ifn=1:length(fn)
    psize=size(pv.(fn{ifn}));
    for imotor=motorList
      if psize(1)>=iotr
        nmotor=(iotr-1)*3+imotor;
        fprintf('%s %s\n',[regexprep(mpv,'N:',[num2str(nmotor),'.']),fn{ifn}],num2str(pv.(fn{ifn}){iotr,imotor}));
        lcaPutNoWait([regexprep(mpv,'N:',[num2str(nmotor),'.']),fn{ifn}],pv.(fn{ifn}){iotr,imotor});
      else
        fprintf('%s %s',[regexprep(mpv,'N:',[num2str(nmotor),'.']),fn{ifn}],num2str(pv.(fn{ifn}){1,imotor}));
        lcaPutNoWait([regexprep(mpv,'N:',[num2str(nmotor),'.']),fn{ifn}],pv.(fn{ifn}){1,imotor});
      end
      pause(0.1)
    end
  end
end
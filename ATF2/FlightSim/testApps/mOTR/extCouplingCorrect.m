function [stat,Bcorrect]=extCouplingCorrect(ModelBeam,sig13,bc,Model)
stat{1}=1;

% first model coupling parameters of incoming beam
cpar=lsqnonlin(@(x) minfun1(x,ModelBeam,sig13),bc,-40,40,optimset('Display','iter','TolX',0.1));
CoupledBeam=beamCouple(ModelBeam,cpar);
beamDiag(CoupledBeam,Model,0); % setup beamDiag function with this beam to track
% find QK / TCAV solution that removes fitted coupled beam
Bcorrect=lsqnonlin(@(x) minfun2(x,CoupledBeam,Model),bc,-0.22,0.22,optimset('Display','iter','TolX',1e-3));
minfun2(Bcorrect,CoupledBeam,Model);

function F=minfun1(x,beam,sig13)
beam=beamCouple(beam,x);
F=abs(getCoup(beam)-sig13)./1e-4;

function F=minfun2(x,beam,Model,iqkscan)
global BEAMLINE PS
persistent qk
if isempty(qk)
  qkind=findcells(BEAMLINE,'Name','QK*X');
  qk=[];
  for iqk=1:2:length(qkind)
    qk(end+1)=BEAMLINE{qkind(iqk)}.PS;
  end
end
if exist('iqkscan','var')
  iqind=iqkscan; x=ones(1,4).*x;
else
  iqind=1:4;
end
for iq=iqind
  PS(qk(iq)).Ampl=x(iq);
end
data=beamDiag(beam,Model,0,'OTR');
F=abs(data.otr.sig13)./1e-4;


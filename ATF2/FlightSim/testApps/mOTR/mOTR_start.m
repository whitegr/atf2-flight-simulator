function varargout = mOTR_start(varargin)
% MOTR_START M-file for mOTR_start.fig
%      MOTR_START, by itself, creates a new MOTR_START or raises the existing
%      singleton*.
%
%      H = MOTR_START returns the handle to a new MOTR_START or the handle to
%      the existing singleton*.
%
%      MOTR_START('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in MOTR_START.M with the given input arguments.
%
%      MOTR_START('Property','Value',...) creates a new MOTR_START or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before mOTR_start_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to mOTR_start_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help mOTR_start

% Last Modified by GUIDE v2.5 27-Jan-2012 06:24:35

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
  'gui_Singleton',  gui_Singleton, ...
  'gui_OpeningFcn', @mOTR_start_OpeningFcn, ...
  'gui_OutputFcn',  @mOTR_start_OutputFcn, ...
  'gui_LayoutFcn',  [] , ...
  'gui_Callback',   []);
if nargin && ischar(varargin{1})
  gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
  [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
  gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before mOTR_start is made visible.
function mOTR_start_OpeningFcn(hObject, eventdata, handles, varargin) %#ok<*INUSL>
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to mOTR_start (see VARARGIN)

% Choose default command line output for mOTR_start
handles.output = hObject;
handles.ACTUATORget={'mOTR:XPS1Aux3Bo2.RVAL' 'mOTR:XPS1Aux3Bo3.RVAL' 'mOTR:XPS2Aux3Bo2.RVAL' 'mOTR:XPS2Aux3Bo3.RVAL'};
handles.XFSWITCH={'mOTR:XPS1Aux3Bo0'; 'mOTR:XPS1Aux3Bo1'; 'mOTR:XPS2Aux3Bo0'; 'mOTR:XPS2Aux3Bo1'};
handles.RDBL={'mOTR:XPS1AuxAi0' 'mOTR:XPS1AuxAi1' 'mOTR:XPS1AuxAi0' 'mOTR:XPS1AuxAi2' 'mOTR:XPS1AuxAi3' 'mOTR:XPS1AuxAi2' 'mOTR:XPS2AuxAi0' 'mOTR:XPS2AuxAi1' 'mOTR:XPS2AuxAi0' 'mOTR:XPS2AuxAi2' 'mOTR:XPS2AuxAi3' 'mOTR:XPS2AuxAi2'};

%Loading bkgd_data for the ccd
for i=0:3
  if ~exist(['bkgd',num2str(i),'.dat'],'file')
    z=zeros(1280,960);
    setappdata(handles.Emittance,['bkgd_data',num2str(i)],z);
  else
    
    bkgd = load(['bkgd',num2str(i),'.dat'],'bkgd','-ascii');
    
    setappdata(handles.Emittance,['bkgd_data',num2str(i)],bkgd);
  end
end

% labCA settings
lcaSetSeverityWarnLevel(14);
lcaSetSeverityWarnLevel(4);
lcaSetSeverityWarnLevel(14);
lcaSetTimeout(0.2);
lcaSetRetryCount(20);

%Monitors for limits
for i=1:12
  lcaSetMonitor(['mOTR:motor',num2str(i),':XPS_STATUS'])
end
%Monitors for target actuators
for i=1:4
  lcaSetMonitor(handles.ACTUATORget(i))
end

%Update once target actuators
for otr_numb=0:3
  if lcaGet(handles.ACTUATORget{(1+otr_numb)})
    set(handles.(sprintf('target_status_%d',otr_numb)),'String','IN')
    set(handles.(sprintf('target_status_%d',otr_numb)),'BackgroundColor',[0.847 0.161 0])
    %set(handles.(sprintf('OTR%d',otr_numb)),'BackgroundColor',[0 1 0])
  elseif isnan(lcaGet(handles.ACTUATORget{(1+otr_numb)}))
    set(handles.(sprintf('target_status_%d',otr_numb)),'String','Error,NaN')
    set(handles.(sprintf('target_status_%d',otr_numb)),'BackgroundColor',[0.847 0.161 0])
    %set(handles.(sprintf('OTR%d',otr_numb)),'BackgroundColor',[0.702 0.702 0.702])
  else
    set(handles.(sprintf('target_status_%d',otr_numb)),'String','OUT')
    set(handles.(sprintf('target_status_%d',otr_numb)),'BackgroundColor',[0.749 0.749 0])
    %set(handles.(sprintf('OTR%d',otr_numb)),'BackgroundColor',[0.702 0.702 0.702])
  end
end

%TO DO!: focus reference voltage!!
%Loading reference_voltages.dat (if doesn't exist creating a default one)<- should be the beamline 0?
% columns: 'otr_numb' 'x_otr' 'y_otr' 'F_otr' 'x_nonotr' 'y_nonotr' 'F_nonotr'
handles.reference_voltages = [0        4.87    6.40    5       4.34    8.62    5;
  1       5.73    4.73    5       5.23    6.88    5;
  2       3.96    3.82    5       3.95    2.01    5;
  3       4.58    5.14    5       4.58    6.35    5];
if ~exist('reference_voltages.dat','file')
  rv=handles.reference_voltages; %#ok<NASGU>
  save('reference_voltages.dat','rv','-ascii')
end

%files=dir;




handles.reference_voltages=load('reference_voltages.dat','rv','ascii');
handles.default_wp(1:4,1:4) = handles.reference_voltages(1:4,1:4);
handles.off_mode(1:4,1:4)= cat(2,handles.reference_voltages(1:4,1),handles.reference_voltages(1:4,5:7));
if ~exist('working_point.set','file')
  wp=handles.default_wp; %#ok<NASGU>
  save('working_point.set', 'wp', '-ascii')
end
handles.wp = load('working_point.set', 'wp', '-ascii');



%Update once mode display---------
RDBL={'mOTR:XPS1AuxAi0' 'mOTR:XPS1AuxAi1' 'mOTR:XPS1AuxAi0' 'mOTR:XPS1AuxAi2' 'mOTR:XPS1AuxAi3' 'mOTR:XPS1AuxAi2' 'mOTR:XPS2AuxAi0' 'mOTR:XPS2AuxAi1' 'mOTR:XPS2AuxAi0' 'mOTR:XPS2AuxAi2' 'mOTR:XPS2AuxAi3' 'mOTR:XPS2AuxAi2'};
XFSWITCH={'mOTR:XPS1Aux3Bo0'; 'mOTR:XPS1Aux3Bo1'; 'mOTR:XPS2Aux3Bo0'; 'mOTR:XPS2Aux3Bo1'};%Remember in binary PV's to put in .VAL and to get in .RVAL fields

lcaPut(XFSWITCH,0);

for otr_numb=0:3
  x=lcaGet(RDBL{(1+3*otr_numb)});
  y=lcaGet(RDBL{(2+3*otr_numb)});
  if (x>(handles.off_mode(1+otr_numb,2)-0.01)&&(x<(handles.off_mode(1+otr_numb,2)+0.01))&&(y>(handles.off_mode(1+otr_numb,3)-0.01)&&(y<(handles.off_mode(1+otr_numb,3)+0.01))))
    set(handles.(sprintf('mode_status_%d',otr_numb)),'String','NonOTR')
    set(handles.(sprintf('mode_status_%d',otr_numb)),'BackgroundColor',[0.749 0.749 0])
  elseif (x>(handles.wp(1+otr_numb,2)-0.01)&&(x<(handles.wp(1+otr_numb,2)+0.01))&&(y>(handles.wp(1+otr_numb,3)-0.01)&&(y<(handles.wp(1+otr_numb,3)+0.01))))
    set(handles.(sprintf('mode_status_%d',otr_numb)),'String','OTR')
    set(handles.(sprintf('mode_status_%d',otr_numb)),'BackgroundColor',[0.847 0.161 0])
  else
    set(handles.(sprintf('mode_status_%d',otr_numb)),'String','OTR')
    set(handles.(sprintf('mode_status_%d',otr_numb)),'BackgroundColor',[0.847 0.161 0])
    
  end
end
%--------------------------------------------

%Initialise target safety time display
set(handles.target_safety_time,'String',lcaGet('mOTR:XPS1Aux3Bo2.HIGH'))%All four will be the same

setappdata(handles.Emittance,'all_out',0)

%Definition of target timer
handles.target_timer = timer('StartDelay',1,'timerfcn', {@target_timer, guidata(hObject),handles},...
  'period',1,'executionmode','fixedrate','BusyMode','drop');
start(handles.target_timer)

% Load last ict cut parameters
if exist('ictcut.mat','file')
  load ictcut ictpar
  set(handles.checkbox1,'Value',ictpar.apply)
  set(handles.edit23,'String',num2str(ictpar.lval))
  set(handles.edit24,'String',num2str(ictpar.hval))
end

% Update handles structure
guidata(hObject, handles);

% Warnings to disable
warning('off','MATLAB:nearlySingularMatrix');
warning('off','MATLAB:rankDeficientMatrix');
warning('off','MATLAB:plot:IgnoreImaginaryXYPart');
warning('off','MATLAB:illConditionedMatrix');
warning('off','MATLAB:singularMatrix');

% UIWAIT makes mOTR_start wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = mOTR_start_OutputFcn(hObject, eventdata, handles)
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on button press in OTR0.
function OTR0_Callback(hObject, eventdata, handles)
% hObject    handle to OTR0 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

persistent otr0_Handle

handles.reference_voltages=load('reference_voltages.dat','rv','ascii');
handles.default_wp(1:4,1:4) = handles.reference_voltages(1:4,1:4);
handles.off_mode(1:4,1:4)= cat(2,handles.reference_voltages(1:4,1),handles.reference_voltages(1:4,5:7));
if ~exist('working_point.set','file')
  wp=handles.default_wp; %#ok<NASGU>
  save('working_point.set', 'wp', '-ascii')
end
handles.wp = load('working_point.set', 'wp', '-ascii');

if isempty(otr0_Handle) || ~ishandle(otr0_Handle)
  otr0_Handle=mOTR(0,handles);
end

% enabling emittance button
%setappdata(handles.Emittance,'enable_0',1)
%if getappdata(handles.Emittance,'enable_0')*getappdata(handles.Emittance,'enable_1')*getappdata(handles.Emittance,'enable_2')*getappdata(handles.Emittance,'enable_3') == 1
%set(handles.Emittance, 'Enable', 'on')
%end



% --- Executes on button press in OTR1.
function OTR1_Callback(hObject, eventdata, handles)
% hObject    handle to OTR1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
persistent otr1_Handle

handles.reference_voltages=load('reference_voltages.dat','rv','ascii');
handles.default_wp(1:4,1:4) = handles.reference_voltages(1:4,1:4);
handles.off_mode(1:4,1:4)= cat(2,handles.reference_voltages(1:4,1),handles.reference_voltages(1:4,5:7));
if ~exist('working_point.set','file')
  wp=handles.default_wp; %#ok<NASGU>
  save('working_point.set', 'wp', '-ascii')
end
handles.wp = load('working_point.set', 'wp', '-ascii');

if isempty(otr1_Handle) || ~ishandle(otr1_Handle)
  otr1_Handle=mOTR(1,handles);
end

%enabling emittance button
%setappdata(handles.Emittance,'enable_1',1)
%if getappdata(handles.Emittance,'enable_0')*getappdata(handles.Emittance,'enable_1')*getappdata(handles.Emittance,'enable_2')*getappdata(handles.Emittance,'enable_3') == 1
% set(handles.Emittance, 'Enable', 'on')
%end



% --- Executes on button press in OTR2.
function OTR2_Callback(hObject, eventdata, handles)
% hObject    handle to OTR2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
persistent otr2_Handle

handles.reference_voltages=load('reference_voltages.dat','rv','ascii');
handles.default_wp(1:4,1:4) = handles.reference_voltages(1:4,1:4);
handles.off_mode(1:4,1:4)= cat(2,handles.reference_voltages(1:4,1),handles.reference_voltages(1:4,5:7));
if ~exist('working_point.set','file')
  wp=handles.default_wp; %#ok<NASGU>
  save('working_point.set', 'wp', '-ascii')
end
handles.wp = load('working_point.set', 'wp', '-ascii');

if isempty(otr2_Handle) || ~ishandle(otr2_Handle)
  otr2_Handle=mOTR(2,handles);
end

%enabling emittance button
%setappdata(handles.Emittance,'enable_2',1)
%if getappdata(handles.Emittance,'enable_0')*getappdata(handles.Emittance,'enable_1')*getappdata(handles.Emittance,'enable_2')*getappdata(handles.Emittance,'enable_3') == 1
% set(handles.Emittance, 'Enable', 'on')
%end


% --- Executes on button press in OTR3.
function OTR3_Callback(hObject, eventdata, handles)
% hObject    handle to OTR3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
persistent otr3_Handle

handles.reference_voltages=load('reference_voltages.dat','rv','ascii');
handles.default_wp(1:4,1:4) = handles.reference_voltages(1:4,1:4);
handles.off_mode(1:4,1:4)= cat(2,handles.reference_voltages(1:4,1),handles.reference_voltages(1:4,5:7));
if ~exist('working_point.set','file')
  wp=handles.default_wp; %#ok<NASGU>
  save('working_point.set', 'wp', '-ascii')
end
handles.wp = load('working_point.set', 'wp', '-ascii');

if isempty(otr3_Handle) || ~ishandle(otr3_Handle)
  otr3_Handle=mOTR(3,handles);
end

%enabling emittance button
%setappdata(handles.Emittance,'enable_3',1)
%if getappdata(handles.Emittance,'enable_0')*getappdata(handles.Emittance,'enable_1')*getappdata(handles.Emittance,'enable_2')*getappdata(handles.Emittance,'enable_3') == 1
% set(handles.Emittance, 'Enable', 'on')
%end


% --- Executes on button press in Emittance.
function Emittance_Callback(hObject, eventdata, handles)
% hObject    handle to Emittance (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Check FS connection good
if ~isequal(get(handles.text9,'BackgroundColor'),[0 1 0])
  errordlg('Connection to Flight Simulator Command Server not present, check FS running and FSECS button green','No FS Connection')
  return
end

% Run emittance calculation GUI
emittance(handles);


function edit1_Callback(hObject, eventdata, handles)
% hObject    handle to edit1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit1 as text
%        str2double(get(hObject,'String')) returns contents of edit1 as a double


% --- Executes during object creation, after setting all properties.
function edit1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
  set(hObject,'BackgroundColor','white');
end



function edit2_Callback(hObject, eventdata, handles)
% hObject    handle to edit2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit2 as text
%        str2double(get(hObject,'String')) returns contents of edit2 as a double


% --- Executes during object creation, after setting all properties.
function edit2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
  set(hObject,'BackgroundColor','white');
end



function edit3_Callback(hObject, eventdata, handles)
% hObject    handle to edit3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit3 as text
%        str2double(get(hObject,'String')) returns contents of edit3 as a double


% --- Executes during object creation, after setting all properties.
function edit3_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
  set(hObject,'BackgroundColor','white');
end



function edit4_Callback(hObject, eventdata, handles)
% hObject    handle to edit4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit4 as text
%        str2double(get(hObject,'String')) returns contents of edit4 as a double


% --- Executes during object creation, after setting all properties.
function edit4_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
  set(hObject,'BackgroundColor','white');
end



function edit5_Callback(hObject, eventdata, handles)
% hObject    handle to edit5 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit5 as text
%        str2double(get(hObject,'String')) returns contents of edit5 as a double


% --- Executes during object creation, after setting all properties.
function edit5_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit5 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
  set(hObject,'BackgroundColor','white');
end



function edit6_Callback(hObject, eventdata, handles)
% hObject    handle to edit6 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit6 as text
%        str2double(get(hObject,'String')) returns contents of edit6 as a double


% --- Executes during object creation, after setting all properties.
function edit6_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit6 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
  set(hObject,'BackgroundColor','white');
end



function edit7_Callback(hObject, eventdata, handles)
% hObject    handle to edit7 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit7 as text
%        str2double(get(hObject,'String')) returns contents of edit7 as a double


% --- Executes during object creation, after setting all properties.
function edit7_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit7 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
  set(hObject,'BackgroundColor','white');
end



function target_status_0_Callback(hObject, eventdata, handles)
% hObject    handle to target_status_0 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of target_status_0 as text
%        str2double(get(hObject,'String')) returns contents of target_status_0 as a double


% --- Executes during object creation, after setting all properties.
function target_status_0_CreateFcn(hObject, eventdata, handles)
% hObject    handle to target_status_0 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
  set(hObject,'BackgroundColor','white');
end



function mode_status_0_Callback(hObject, eventdata, handles)
% hObject    handle to mode_status_0 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of mode_status_0 as text
%        str2double(get(hObject,'String')) returns contents of mode_status_0 as a double


% --- Executes during object creation, after setting all properties.
function mode_status_0_CreateFcn(hObject, eventdata, handles)
% hObject    handle to mode_status_0 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
  set(hObject,'BackgroundColor','white');
end



function limit_status_0_Callback(hObject, eventdata, handles)
% hObject    handle to limit_status_0 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of limit_status_0 as text
%        str2double(get(hObject,'String')) returns contents of limit_status_0 as a double


% --- Executes during object creation, after setting all properties.
function limit_status_0_CreateFcn(hObject, eventdata, handles)
% hObject    handle to limit_status_0 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
  set(hObject,'BackgroundColor','white');
end



function target_status_1_Callback(hObject, eventdata, handles)
% hObject    handle to target_status_1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of target_status_1 as text
%        str2double(get(hObject,'String')) returns contents of target_status_1 as a double


% --- Executes during object creation, after setting all properties.
function target_status_1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to target_status_1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
  set(hObject,'BackgroundColor','white');
end



function mode_status_1_Callback(hObject, eventdata, handles)
% hObject    handle to mode_status_1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of mode_status_1 as text
%        str2double(get(hObject,'String')) returns contents of mode_status_1 as a double


% --- Executes during object creation, after setting all properties.
function mode_status_1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to mode_status_1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
  set(hObject,'BackgroundColor','white');
end



function limit_status_1_Callback(hObject, eventdata, handles)
% hObject    handle to limit_status_1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of limit_status_1 as text
%        str2double(get(hObject,'String')) returns contents of limit_status_1 as a double


% --- Executes during object creation, after setting all properties.
function limit_status_1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to limit_status_1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
  set(hObject,'BackgroundColor','white');
end



function target_status_2_Callback(hObject, eventdata, handles)
% hObject    handle to target_status_2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of target_status_2 as text
%        str2double(get(hObject,'String')) returns contents of target_status_2 as a double


% --- Executes during object creation, after setting all properties.
function target_status_2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to target_status_2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
  set(hObject,'BackgroundColor','white');
end



function mode_status_2_Callback(hObject, eventdata, handles)
% hObject    handle to mode_status_2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of mode_status_2 as text
%        str2double(get(hObject,'String')) returns contents of mode_status_2 as a double


% --- Executes during object creation, after setting all properties.
function mode_status_2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to mode_status_2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
  set(hObject,'BackgroundColor','white');
end



function limit_status_2_Callback(hObject, eventdata, handles)
% hObject    handle to limit_status_2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of limit_status_2 as text
%        str2double(get(hObject,'String')) returns contents of limit_status_2 as a double


% --- Executes during object creation, after setting all properties.
function limit_status_2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to limit_status_2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
  set(hObject,'BackgroundColor','white');
end



function target_status_3_Callback(hObject, eventdata, handles)
% hObject    handle to target_status_3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of target_status_3 as text
%        str2double(get(hObject,'String')) returns contents of target_status_3 as a double


% --- Executes during object creation, after setting all properties.
function target_status_3_CreateFcn(hObject, eventdata, handles)
% hObject    handle to target_status_3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
  set(hObject,'BackgroundColor','white');
end



function mode_status_3_Callback(hObject, eventdata, handles)
% hObject    handle to mode_status_3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of mode_status_3 as text
%        str2double(get(hObject,'String')) returns contents of mode_status_3 as a double


% --- Executes during object creation, after setting all properties.
function mode_status_3_CreateFcn(hObject, eventdata, handles)
% hObject    handle to mode_status_3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
  set(hObject,'BackgroundColor','white');
end



function limit_status_3_Callback(hObject, eventdata, handles)
% hObject    handle to limit_status_3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of limit_status_3 as text
%        str2double(get(hObject,'String')) returns contents of limit_status_3 as a double


% --- Executes during object creation, after setting all properties.
function limit_status_3_CreateFcn(hObject, eventdata, handles)
% hObject    handle to limit_status_3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
  set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in all_target_out.
function all_target_out_Callback(hObject, eventdata, handles)
% hObject    handle to all_target_out (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

%setappdata(handles.Emittance,'all_out',1)
for i=0:3
  target_in_out(i,0);
  %set(eval(sprintf('handles.target_status_%d_time',i)),'String','');
  %Refresh
  if lcaGet(handles.ACTUATORget{(1+i)});
    set(handles.(sprintf('target_status_%d',i)),'String','IN')
    set(handles.(sprintf('target_status_%d',i)),'BackgroundColor',[0 1 0])
    set(handles.(sprintf('OTR%d',i)),'BackgroundColor',[0 1 0])
    
  else
    set(handles.(sprintf('target_status_%d',i)),'String','OUT')
    set(handles.(sprintf('target_status_%d',i)),'BackgroundColor',[0.831 0.816 0.784])
    set(handles.(sprintf('OTR%d',i)),'BackgroundColor',[0.702 0.702 0.702])
    
  end
end

% --- Executes on button press in pushbutton8.
function pushbutton8_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton8 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on button press in look_set_cal.
function look_set_cal_Callback(hObject, eventdata, handles)
% hObject    handle to look_set_cal (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on button press in restore_def_cal.
function restore_def_cal_Callback(hObject, eventdata, handles)
% hObject    handle to restore_def_cal (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes when user attempts to close figure1.
function figure1_CloseRequestFcn(hObject, eventdata, handles) %#ok<*DEFNU,*INUSD>
% hObject    handle to figure1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: delete(hObject) closes the figure
%not clear if it works because I cannot close single mOTR panel

%ATTENTION!! UNCOMMENT AGAIN!!!!!
%for i=0:3
%    target_in_out(i,0);
%end
exit


% --- Executes during object creation, after setting all properties.
function target_safety_time_CreateFcn(hObject, eventdata, handles)
% hObject    handle to target_safety_time (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
  set(hObject,'BackgroundColor','white');
end



function target_safety_time_Callback(hObject, eventdata, handles)
% hObject    handle to target_safety_time (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

handles.target_time = str2double(get(hObject, 'String'));
if isnan(handles.target_time)
  set(hObject, 'String', 0);
  errordlg('Input must be a number','Error');
end

lcaPut('mOTR:XPS1Aux3Bo2.HIGH',handles.target_time)
lcaPut('mOTR:XPS1Aux3Bo3.HIGH',handles.target_time)
lcaPut('mOTR:XPS2Aux3Bo2.HIGH',handles.target_time)
lcaPut('mOTR:XPS2Aux3Bo3.HIGH',handles.target_time)

setappdata(handles.target_safety_time,'target_time',handles.target_time)

guidata(gcbo,handles)


% --- Executes on button press in reset_cams.
function reset_cams_Callback(hObject, eventdata, handles)
% hObject    handle to reset_cams (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

for wcam=1:4
  
  setup(wcam)
  set(handles.display, 'String', ['Setting up cam',num2str(wcam-1)])
  
  %TRIED THIS,BETTER TO MAKE A FUNCTION, GLEN'S WORKS WELL
  % Setup parameters
  % PVName='mOTR';
  % camName=sprintf('cam%d',wcam);
  % imageName=sprintf('image%d',wcam);
  % pars.sizeX=1280;
  % pars.sizeY=960;
  % pars.exposureTime=0.3; % s
  % pars.acquirePeriod=1; % s
  % pars.triggerMode=2; % 0 = free running, 1:4=syncIn 1-4
  % pars.imageMode=2;
  % pars.gain=20.0;
  %
  % lcaPutNoWait([PVName,':',camName,':SizeX'],pars.sizeX);pause(0.1)
  % lcaPutNoWait([PVName,':',camName,':SizeY'],pars.sizeY);pause(0.1)
  % lcaPutNoWait([PVName,':',camName,':AcquireTime'],pars.exposureTime);pause(0.1)
  % lcaPutNoWait([PVName,':',camName,':AcquirePeriod'],pars.acquirePeriod);pause(0.1)
  % lcaPutNoWait([PVName,':',camName,':TriggerMode'],pars.triggerMode);pause(0.1)
  % lcaPutNoWait([PVName,':',camName,':ImageMode'],pars.imageMode);pause(0.1)
  % lcaPutNoWait([PVName,':',camName,':Gain'],pars.gain);pause(0.1)
  % Set camera to acquire 12-bit data (as opposed to 8-bit)
  % lcaPutNoWait([PVName,':',camName,':DataType'],1);pause(0.1)
  % % Set NDStdArrays plugin to enable array callbacks (so new data becomes
  % availbale to use through ArrayData record)
  % lcaPutNoWait([PVName,':',imageName,':EnableCallbacks'],1);pause(0.1)
  % lcaPutNoWait([PVName,':',camName,':ArrayCallbacks'],1);pause(0.1)
  % lcaPutNoWait([PVName,':',camName,':AutoSave'],0);pause(0.1)
end
set(handles.display, 'String', 'Done')
system('sleep 1');
set(handles.display, 'String', '')

% --- Executes on button press in bkgd_0.
function bkgd_0_Callback(hObject, eventdata, handles)
% hObject    handle to bkgd_0 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)



function edit21_Callback(hObject, eventdata, handles)
% hObject    handle to edit21 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit21 as text
%        str2double(get(hObject,'String')) returns contents of edit21 as a double


% --- Executes during object creation, after setting all properties.
function edit21_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit21 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
  set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in bkgd_1.
function bkgd_1_Callback(hObject, eventdata, handles)
% hObject    handle to bkgd_1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on button press in bkgd_2.
function bkgd_2_Callback(hObject, eventdata, handles)
% hObject    handle to bkgd_2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on button press in bkgd_3.
function bkgd_3_Callback(hObject, eventdata, handles)
% hObject    handle to bkgd_3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)



function edit22_Callback(hObject, eventdata, handles)
% hObject    handle to edit22 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit22 as text
%        str2double(get(hObject,'String')) returns contents of edit22 as a double


% --- Executes during object creation, after setting all properties.
function edit22_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit22 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
  set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in get_bkd.
function get_bkd_Callback(hObject, eventdata, handles)
% hObject    handle to get_bkd (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
bkgd(handles)


% --- Executes on button press in ccd_settings.
function ccd_settings_Callback(hObject, eventdata, handles)
% hObject    handle to ccd_settings (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on button press in all_nonotr.
function all_nonotr_Callback(hObject, eventdata, handles)
% hObject    handle to all_nonotr (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

for iotr=1:3
  setModeOut(handles,iotr);
end


% --- Executes on button press in pushbutton19.
function pushbutton19_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton19 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
ccdPowerInstruction;


% --- Executes on button press in pushbutton20.
function pushbutton20_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton20 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

if strcmp(questdlg('Restart the mOTR EPICS IOC?','IOC Restart','Restart','Cancel','Cancel'),'Restart')
  lcaPutNoWait('mOTR:SYSRESET',1);
end


% --- Executes during object creation, after setting all properties.
function pushbutton20_CreateFcn(hObject, eventdata, handles)
% hObject    handle to pushbutton20 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called


% --- Executes on button press in pushbutton21.
function pushbutton21_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton21 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

for iotr=1:3
  setModeIn(handles,iotr);
end


% --- Executes on button press in checkbox1.
function checkbox1_Callback(hObject, eventdata, handles)
% hObject    handle to checkbox1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

ictpar.apply=get(handles.checkbox1,'Value');
ictpar.lval=str2double(get(handles.edit23,'String'));
ictpar.hval=str2double(get(handles.edit24,'String'));
save ictcut ictpar



function edit23_Callback(hObject, eventdata, handles)
% hObject    handle to edit23 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

ictpar.apply=get(handles.checkbox1,'Value');
ictpar.lval=str2double(get(handles.edit23,'String'));
ictpar.hval=str2double(get(handles.edit24,'String'));
save ictcut ictpar


% --- Executes during object creation, after setting all properties.
function edit23_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit23 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
  set(hObject,'BackgroundColor','white');
end



function edit24_Callback(hObject, eventdata, handles)
% hObject    handle to edit24 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

ictpar.apply=get(handles.checkbox1,'Value');
ictpar.lval=str2double(get(handles.edit23,'String'));
ictpar.hval=str2double(get(handles.edit24,'String'));
save ictcut ictpar


% --- Executes during object creation, after setting all properties.
function edit24_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit24 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
  set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in pushbutton22.
function pushbutton22_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton22 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
setModeIn(handles,0)

% --- Executes on button press in pushbutton23.
function pushbutton23_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton23 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
setModeOut(handles,0)

% --- Executes on button press in pushbutton24.
function pushbutton24_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton24 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
setModeIn(handles,1)

% --- Executes on button press in pushbutton25.
function pushbutton25_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton25 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
setModeOut(handles,1)

% --- Executes on button press in pushbutton26.
function pushbutton26_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton26 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
setModeIn(handles,2)

% --- Executes on button press in pushbutton27.
function pushbutton27_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton27 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
setModeOut(handles,2)

% --- Executes on button press in pushbutton28.
function pushbutton28_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton28 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
setModeIn(handles,3)

% --- Executes on button press in pushbutton29.
function pushbutton29_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton29 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
setModeOut(handles,3)

function setModeIn(handles,iotr)

handles.reference_voltages=load('reference_voltages.dat','rv','ascii');
handles.default_wp(1:4,1:4) = handles.reference_voltages(1:4,1:4);
handles.off_mode(1:4,1:4)= cat(2,handles.reference_voltages(1:4,1),handles.reference_voltages(1:4,5:7));
if ~exist('working_point.set','file')
  wp=handles.default_wp; %#ok<NASGU>
  save('working_point.set', 'wp', '-ascii')
end
handles.wp = load('working_point.set', 'wp', '-ascii');

% Make sure target is out
target_in_out(iotr,0);
%If don't exist it's because you've not opened a single mOTR.m
if ~exist('cal.set','file') || ~exist('reference_voltages.dat','file')
  set(handles.display,'String','Please open a single OTR at least once!!')
else
  handles.cal = load('cal.set', 'calibration', '-ascii');
  handles.reference_voltages=load('reference_voltages.dat','rv','ascii');
  handles.off_mode(1:4,1:4)= handles.reference_voltages(1:4,1:4);
  
  set(handles.display,'String',sprintf('Setting OTR%dX to OTR mode',iotr))
  drawnow('expose')
  maxtries=10; ntry=0;
  if ~strcmp(lcaGet(handles.XFSWITCH{(1+iotr)}),'Low')
    lcaPut(handles.XFSWITCH{(1+iotr)},0); system('sleep 2');
  end
  x=lcaGet(handles.RDBL{(1+3*iotr)});
  y=lcaGet(handles.RDBL{(2+3*iotr)});
  x=(handles.off_mode(1+iotr,2)-x)*handles.cal(1+iotr,5);
  y=(handles.off_mode(1+iotr,3)-y)*handles.cal(1+iotr,6);
  while abs(x)>10 || abs(y)>10
    ntry=ntry+1;
    x=lcaGet(handles.RDBL{(1+3*iotr)});
    y=lcaGet(handles.RDBL{(2+3*iotr)});
    handles.off_mode(1:4,1:4)= handles.reference_voltages(1:4,1:4);
    x=(handles.off_mode(1+iotr,2)-x)*handles.cal(1+iotr,5);
    y=(handles.off_mode(1+iotr,3)-y)*handles.cal(1+iotr,6);
    xmv=x/handles.cal(1+iotr,2);
    ymv=y/handles.cal(1+iotr,3);
    motor_move('move',0,-xmv,1,0,1,0,(1+3*iotr),3);
    motor_move('move',0,ymv,1,0,1,0,(2+3*iotr),3);
    system(sprintf('sleep %f',max([3 [x y]./[400 800]])));
    drawnow('expose');
    if ntry>maxtries
      errordlg(sprintf('Warning: OTR%dX did not properly enter working position mode!',iotr),'Mode error')
      break
    end
  end
  set(handles.(sprintf('mode_status_%d',iotr)),'String','OTR')
  set(handles.(sprintf('mode_status_%d',iotr)),'BackgroundColor',[1 0 0])
end
set(handles.display,'String','')
drawnow('expose')

function setModeOut(handles,iotr)

% Make sure target is out
target_in_out(iotr,0);

handles.reference_voltages=load('reference_voltages.dat','rv','ascii');
handles.default_wp(1:4,1:4) = handles.reference_voltages(1:4,1:4);
handles.off_mode(1:4,1:4)= cat(2,handles.reference_voltages(1:4,1),handles.reference_voltages(1:4,5:7));
if ~exist('working_point.set','file')
  wp=handles.default_wp; %#ok<NASGU>
  save('working_point.set', 'wp', '-ascii')
end
handles.wp = load('working_point.set', 'wp', '-ascii');


%If don't exist it's because you've not opened a single mOTR.m  [don't want to think a solution, easier open one!]
if ~exist('cal.set','file') || ~exist('reference_voltages.dat','file')
  set(handles.display,'String','Please open a single OTR at least once!!')
else
  handles.cal = load('cal.set', 'calibration', '-ascii');
  handles.reference_voltages=load('reference_voltages.dat','rv','ascii');
  handles.off_mode(1:4,1:4)= cat(2,handles.reference_voltages(1:4,1),handles.reference_voltages(1:4,5:7));
  
  set(handles.display,'String',sprintf('Setting OTR%dX to Non-OTR mode',iotr))
  drawnow('expose')
  maxtries=5; ntry=0;
  if ~strcmp(lcaGet(handles.XFSWITCH{(1+iotr)}),'Low')
    lcaPut(handles.XFSWITCH{(1+iotr)},0); system('sleep 2');
  end
  x=lcaGet(handles.RDBL{(1+3*iotr)});
  y=lcaGet(handles.RDBL{(2+3*iotr)});
  x=(handles.off_mode(1+iotr,2)-x)*handles.cal(1+iotr,5);
  y=(handles.off_mode(1+iotr,3)-y)*handles.cal(1+iotr,6);
  while abs(x)>10 || abs(y)>10
    ntry=ntry+1;
    x=lcaGet(handles.RDBL{(1+3*iotr)});
    y=lcaGet(handles.RDBL{(2+3*iotr)});
    handles.off_mode(1:4,1:4)= cat(2,handles.reference_voltages(1:4,1),handles.reference_voltages(1:4,5:7));
    x=(handles.off_mode(1+iotr,2)-x)*handles.cal(1+iotr,5);
    y=(handles.off_mode(1+iotr,3)-y)*handles.cal(1+iotr,6);
    xmv=x/handles.cal(1+iotr,2);
    ymv=y/handles.cal(1+iotr,3);
    motor_move('move',0,-xmv,1,0,1,0,(1+3*iotr),3);
    motor_move('move',0,ymv,1,0,1,0,(2+3*iotr),3);
    system(sprintf('sleep %f',max([3 [x y]./[400 800]])));
    drawnow('expose');
    if ntry>maxtries
      errordlg(sprintf('Warning: OTR%dX did not properly enter non-working position mode!',iotr),'Mode error')
      break
    end
  end
  if abs(x)<20 && abs(y)<20
    set(handles.(sprintf('mode_status_%d',iotr)),'String','NonOTR')
    set(handles.(sprintf('mode_status_%d',iotr)),'BackgroundColor',[0.749 0.749 0])
  end
end
set(handles.display,'String','')
drawnow('expose')

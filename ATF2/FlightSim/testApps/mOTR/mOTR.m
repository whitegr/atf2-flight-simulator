function varargout = mOTR(varargin)
% MOTR M-file for mOTR.fig


% Begin initialization code - DO NOT EDIT
gui_Singleton = 0;
gui_State = struct('gui_Name',       mfilename, ...
  'gui_Singleton',  gui_Singleton, ...
  'gui_OpeningFcn', @mOTR_OpeningFcn, ...
  'gui_OutputFcn',  @mOTR_OutputFcn, ...
  'gui_LayoutFcn',  [] , ...
  'gui_Callback',   []);
if nargin && ischar(varargin{1})
  gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
  [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
  gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before mOTR is made visible.
function mOTR_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to mOTR (see VARARGIN)

% Choose default command line output for mOTR
handles.output = hObject;

handles.startGuiHandles=varargin{2};
handles.reference_voltages=handles.startGuiHandles.reference_voltages;
handles.wp=handles.startGuiHandles.wp;
handles.default_wp=handles.startGuiHandles.default_wp;
handles.off_mode=handles.startGuiHandles.off_mode;

handles.otr_numb=varargin{1};

set(gcf,'Name',strcat('mOTR_',num2str(handles.otr_numb)));

handles.pos_entry_x=0;
handles.pos_entry_y=0;
handles.pos_entry_f=0;
handles.pos_abs_steps_x=0;
handles.pos_abs_steps_y=0;
handles.pos_abs_steps_z=0;

set(handles.single_otr_general_panel,'Title',['OTR',num2str(handles.otr_numb)]);

%Loading cal.set (if it doesn't exist, creating a default one)
%"cal.set" file format is: 'otr name' 'x[um/step]' 'y[um/step]' 'F[um/step]' 'potx[um/V]' 'poty[um/V]' 'potF[um/V]' 'cam[um/pixel]'
%TO DO: UPDATE THE DEFAULT CAMERA CALIBRATION! and focus?
%ATTENTION: In simulation for focus is also 1000 step/volt, like y motor [puts same numbs as in y motor i.e. 2um/step & 2000um/V]
handles.default_cal = [   0          10            2            1           2000               2000         1       0.5 ;
  1          10            2            1           2000               2000         1       0.5 ;
  2          10            2            1           2000               2000         1       0.5 ;
  3          10            2            1           2000               2000         1       0.5];
if ~exist('cal.set','file')
  calibration=handles.default_cal; %#ok<NASGU>
  save('cal.set', 'calibration', '-ascii')
end
handles.cal = load('cal.set', 'calibration', '-ascii');

% Reference orbit
if exist('refOrbit.mat','file')
  load refOrbit.mat refOrbit
  setappdata(handles.single_otr_general_panel,'refOrbit',refOrbit)
else
  setappdata(handles.single_otr_general_panel,'refOrbit',0)
end
if exist('refOrbitDate.mat','file')
  load refOrbitDate.mat refOrbitDate
  setappdata(handles.single_otr_general_panel,'refOrbitDate',refOrbitDate)
else
  setappdata(handles.single_otr_general_panel,'refOrbitDate',[1970 0 0 0 0 0])
end

% Gain setting
if exist('gain.mat','file')
  load gain.mat gain
  if length(gain)>=handles.otr_numb+1
    set(handles.edit31,'String',num2str(gain(handles.otr_numb+1)))
  end
end
edit31_Callback(handles.edit31,[],handles);

% Integration time setting
if exist('inttime.mat','file')
  load inttime.mat inttime
  if length(inttime)>=handles.otr_numb+1
    set(handles.edit32,'String',num2str(inttime(handles.otr_numb+1)))
  end
end
edit32_Callback(handles.edit32,[],handles);

% Averaging settings
if exist('ave.mat','file')
  load ave.mat ave
  if length(ave)>=handles.otr_numb+1
    set(handles.edit33,'String',num2str(ave(handles.otr_numb+1)))
  end
end
edit33_Callback(handles.edit33,[],handles);

% sigma cut setting
if exist('sigmacut.mat','file')
  load sigmacut.mat sigmacut
  if length(sigmacut)>=handles.otr_numb+1
    set(handles.edit34,'String',num2str(sigmacut(handles.otr_numb+1)))
  end
end
edit34_Callback(handles.edit34,[],handles);

% cut ratio setting
if exist('cutRatio.mat','file')
  load cutRatio.mat cutRatio
  if length(cutRatio)>=handles.otr_numb+1
    set(handles.edit35,'String',num2str(cutRatio(handles.otr_numb+1)*1e2))
  end
end
edit35_Callback(handles.edit35,[],handles);

% Beam presence cut
if exist('beamCut.mat','file')
  load beamCut.mat beamCut
  if length(beamCut)>=handles.otr_numb+1
    set(handles.edit36,'String',num2str(beamCut(handles.otr_numb+1)/1e8))
  end
end
edit36_Callback(handles.edit36,[],handles);

% Beam centring setting
% if exist('beamCentring.mat','file')
%   load beamCentring.mat beamCentring
%   if length(beamCentring)>=handles.otr_numb+1
% %     set(handles.togglebutton1,'Value',beamCentring(handles.otr_numb+1))
%   end
% end
% togglebutton1_Callback(handles.togglebutton1,[],handles);

setappdata(handles.single_otr_general_panel,'working_point',handles.wp)
setappdata(handles.single_otr_general_panel,'calibration',handles.cal)
setappdata(handles.single_otr_general_panel,'startGuiHandles',handles.startGuiHandles)
setappdata(handles.single_otr_general_panel,'reference_voltages',handles.reference_voltages)
setappdata(handles.single_otr_general_panel,'limit_switch_pressed',0)

%Tags to disable/enable when limit switches on/off
handles.limits_disable_tags={'handles.otr_off' 'handles.otr_working' 'handles.otr_set_working' ...
  'handles.change_defaults_button' 'handles.x_switch' 'handles.F_switch' ...
  'handles.ypos_abs_entry' 'handles.ypos_ABS_move' 'handles.ypos_RELB_move' 'handles.ypos_entry' 'handles.ypos_RELF_move'...
  'handles.xpos_abs_entry' 'handles.xpos_ABS_move' 'handles.xpos_RELB_move' 'handles.xpos_entry' 'handles.xpos_RELF_move'...
  'handles.fpos_abs_entry' 'handles.fpos_ABS_move' 'handles.fpos_RELB_move' 'handles.fpos_entry' 'handles.fpos_RELF_move'};

%PV names
handles.RDBL={'mOTR:XPS1AuxAi0' 'mOTR:XPS1AuxAi1' 'mOTR:XPS1AuxAi0' 'mOTR:XPS1AuxAi2' 'mOTR:XPS1AuxAi3' 'mOTR:XPS1AuxAi2' 'mOTR:XPS2AuxAi0' 'mOTR:XPS2AuxAi1' 'mOTR:XPS2AuxAi0' 'mOTR:XPS2AuxAi2' 'mOTR:XPS2AuxAi3' 'mOTR:XPS2AuxAi2'};
handles.XFSWITCH={'mOTR:XPS1Aux3Bo0'; 'mOTR:XPS1Aux3Bo1'; 'mOTR:XPS2Aux3Bo0'; 'mOTR:XPS2Aux3Bo1'};%Remember in binary PV's to put in .VAL and to get in .RVAL fields
handles.ACTUATORget={'mOTR:XPS1Aux3Bo2.RVAL' 'mOTR:XPS1Aux3Bo3.RVAL' 'mOTR:XPS2Aux3Bo2.RVAL' 'mOTR:XPS2Aux3Bo3.RVAL'};%Remember in binary PV's to caput in .VAL and to caget in .RVAL fields

%Starting position timer [THIS TIMER ALSO CHANGES TARGET IN/OUT COLOR]
guidata(hObject, handles);
handles.position_timer=timer('StartDelay',2,'Period',0.5,...
  'ExecutionMode','FixedRate','BusyMode','drop');
set(handles.position_timer, 'TimerFcn', {@position_timer_function, guidata(hObject)})
setappdata(0,'position_timer',handles.position_timer);

%Starting ccd timer
ccd_timer('reset'); % reset internal persistent variables
handles.ccd_refresh=timer('StartDelay',1,'Period',0.5,...
  'ExecutionMode','FixedRate','BusyMode','drop');
set(handles.ccd_refresh, 'TimerFcn', {@ccd_timer, guidata(hObject),handles.otr_numb})
setappdata(0,'ccd_refresh',handles.ccd_refresh);

% Zoom position
pos=otrZoom('readpos',handles.otr_numb);
set(handles.slider1,'Value',pos);
set(handles.edit37,'String',num2str(pos,2))

% Initialising movement controls
lcaPutNoWait(handles.XFSWITCH,1);
set(handles.fpos_read,'String',round((handles.cal(1+handles.otr_numb,7)*lcaGet(handles.RDBL{(3+3*handles.otr_numb)})-handles.wp(1+handles.otr_numb,4))));
set(handles.fpos_read_v,'String',lcaGet(handles.RDBL{(3+3*handles.otr_numb)}));
set(handles.fpos_entry,'String',0);
set(handles.fpos_abs_entry,'String',round(handles.cal(1+handles.otr_numb,7)*(lcaGet(handles.RDBL{(3+3*handles.otr_numb)})-handles.wp(1+handles.otr_numb,4))));
lcaPutNoWait(handles.XFSWITCH,0);
set(handles.xpos_abs_entry,'String',round(handles.cal(1+handles.otr_numb,5)*(lcaGet(handles.RDBL{(1+3*handles.otr_numb)})-handles.wp(1+handles.otr_numb,2))));
set(handles.xpos_entry,'String',0);
set(handles.ypos_abs_entry,'String',round((handles.cal(1+handles.otr_numb,6)*(lcaGet(handles.RDBL{(2+3*handles.otr_numb)})-handles.wp(1+handles.otr_numb,3)))));
set(handles.ypos_entry,'String',0);

setappdata(handles.single_otr_general_panel,'xFswitch',1)%That's only for All_nonOTR mode button in mOTR_start panel. When switching it updates value

%Disabling F controls
set(handles.fpos_RELF_move,'Enable','off');
set(handles.fpos_RELB_move,'Enable','off');
set(handles.fpos_ABS_move,'Enable','off');
set(handles.fpos_entry,'Enable','off');
set(handles.fpos_abs_entry,'Enable','off');
set(handles.fpos_read,'BackgroundColor',[0.4 0.4 0.4]);
set(handles.fpos_read_v,'BackgroundColor',[0.4 0.4 0.4]);

%Disabling clear limit switches
set(handles.clear_limit_y_min,'Enable','off');
set(handles.clear_limit_y_max,'Enable','off');
set(handles.clear_limit_x_min,'Enable','off');
set(handles.clear_limit_x_max,'Enable','off');
set(handles.clear_limit_f_min,'Enable','off');
set(handles.clear_limit_f_max,'Enable','off');

handles.which_limit=13;%from 0 to 12 for the motors, 13 for any. each time a limit is cleared it takes the value of the motor. in the move functions restrict the motor movement in the same direction of the limit and then after a permitted movement clear which_limit

if ~exist('relative.dat','file')
  relative=[0 0 0 0;
    1 0 0 0;
    2 0 0 0;
    3 0 0 0];
else
  relative=load('relative.dat','relative','-ascii');
end
set(handles.xpos_entry,'String',num2str(relative(1+handles.otr_numb,2)))
handles.pos_entry_x = relative(1+handles.otr_numb,2)/handles.cal(1+handles.otr_numb,2);
set(handles.ypos_entry,'String',num2str(relative(1+handles.otr_numb,3)))
handles.pos_entry_y = relative(1+handles.otr_numb,3)/handles.cal(1+handles.otr_numb,3);
set(handles.fpos_entry,'String',num2str(relative(1+handles.otr_numb,4)))
handles.pos_entry_f = relative(1+handles.otr_numb,4)/handles.cal(1+handles.otr_numb,4);

% Check camera in single acquire mode
% Make sure cam initial status is non-acquiring
lcaPutNoWait(sprintf('mOTR:cam%d:Acquire',1+handles.otr_numb),0);

% start timers now
start(handles.position_timer);
start(handles.ccd_refresh);

% Update handles structure
guidata(hObject, handles)

% --- Outputs from this function are returned to the command line.
function varargout = mOTR_OutputFcn(hObject, eventdata, handles)
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;

% --- Executes on button press in target_in.
function target_in_Callback(hObject, eventdata, handles)
% hObject    handle to taget_in (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
target_in_out(handles.otr_numb,1);


% --- Executes on button press in target_out.
function target_out_Callback(hObject, eventdata, handles)
% hObject    handle to target_out (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
target_in_out(handles.otr_numb,0);

function xpos_entry_Callback(hObject, eventdata, handles)
% hObject    handle to xpos_entry (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of xpos_entry as text
%xpos = str2double(get(hObject,'String')); %returns contents of xpos_entry as a double
xpos = str2double(get(hObject, 'String'));
if isnan(xpos)
  set(hObject, 'String', 0);
  errordlg('Input must be a number','Error');
end

relative=load('relative.dat','relative','-ascii');
relative(1+handles.otr_numb,2)=xpos; %#ok<NASGU>
save('relative.dat','relative','-ascii')


handles.pos_entry_x = xpos/handles.cal(1+handles.otr_numb,2);

guidata(gcbo,handles)

% --- Executes during object creation, after setting all properties.
function xpos_entry_CreateFcn(hObject, eventdata, handles)
% hObject    handle to xpos_entry (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
  set(hObject,'BackgroundColor','white');
end

% --- Executes on button press in xpos_ABS_move.
function xpos_ABS_move_Callback(hObject, eventdata, handles)
% hObject    handle to xpos_ABS_move (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

whichMotor = 1+(3*handles.otr_numb);
x=lcaGet(handles.RDBL{(1+3*handles.otr_numb)});
%relative distance in steps to desired position from the actual position
handles.pos_abs_steps_x=str2double(get(handles.xpos_abs_entry,'String'))/handles.cal(1+handles.otr_numb,2);
x=handles.pos_abs_steps_x-((x-handles.wp(1+handles.otr_numb,2))*handles.cal(1+handles.otr_numb,5)/handles.cal(1+handles.otr_numb,2));
motor_move('move',0,-x,1,0,1,0,whichMotor,3);

%Refresh xpos_read
%set(handles.xpos_read,'String',(handles.cal(1+handles.otr_numb,5)*(lcaGet(handles.RDBL{(1+3*handles.otr_numb)})-handles.wp(1+handles.otr_numb,2))));

% --- Executes on button press in xpos_RELB_move.
function xpos_RELB_move_Callback(hObject, eventdata, handles)
% hObject    handle to xpos_RELB_move (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
whichMotor = 1+(3*handles.otr_numb);
motor_move('move',0,handles.pos_entry_x,1,0,1,0,whichMotor,3);
set(handles.xpos_abs_entry,'String',str2double(get(handles.xpos_abs_entry,'String'))-handles.pos_entry_x*handles.cal(1+handles.otr_numb,2));
%Do if it is limit case
if handles.which_limit==1+(3*handles.otr_numb)&& getappdata(handles.single_otr_general_panel,'limit_switch_pressed')
  system('sleep 1');
  setappdata(handles.single_otr_general_panel,'limit_switch_pressed',0)
  handles.which_limit=13;
  for i=1:size(handles.limits_disable_tags,2)
    set(eval(handles.limits_disable_tags{i}),'Enable','on')
  end
  set(handles.clear_limit_x_min,'Enable','off')
  set(handles.clear_limit_x_min,'BackgroundColor',[0.702 0.702 0.702]);
  set(eval(['handles.startGuiHandles.limit_status_',num2str(handles.otr_numb)]),'String','')
  set(eval(['handles.startGuiHandles.limit_status_',num2str(handles.otr_numb)]),'BackgroundColor',[0.831 0.816 0.784]);
end

% --- Executes on button press in xpos_RELF_move.
function xpos_RELF_move_Callback(hObject, eventdata, handles)
% hObject    handle to xpos_RELF_move (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
whichMotor = 1+(3*handles.otr_numb);
motor_move('move',0,-handles.pos_entry_x,1,0,1,0,whichMotor,3);
set(handles.xpos_abs_entry,'String',str2double(get(handles.xpos_abs_entry,'String'))+handles.pos_entry_x*handles.cal(1+handles.otr_numb,2));
%Do if it is limit case
if handles.which_limit==1+(3*handles.otr_numb)&& getappdata(handles.single_otr_general_panel,'limit_switch_pressed')
  system('sleep 1');
  setappdata(handles.single_otr_general_panel,'limit_switch_pressed',0)
  handles.which_limit=13;
  for i=1:size(handles.limits_disable_tags,2)
    set(eval(handles.limits_disable_tags{i}),'Enable','on')
  end
  set(handles.clear_limit_x_max,'Enable','off')
  set(handles.clear_limit_x_max,'BackgroundColor',[0.702 0.702 0.702]);
  set(eval(['handles.startGuiHandles.limit_status_',num2str(handles.otr_numb)]),'String','')
  set(eval(['handles.startGuiHandles.limit_status_',num2str(handles.otr_numb)]),'BackgroundColor',[0.831 0.816 0.784]);
end


function ypos_entry_Callback(hObject, eventdata, handles)
% hObject    handle to ypos_entry (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of ypos_entry as text
%        str2double(get(hObject,'String')) returns contents of ypos_entry as a double
ypos = str2double(get(hObject, 'String'));
if isnan(ypos)
  set(hObject, 'String', 0);
  errordlg('Input must be a number','Error');
end
%from um to steps

relative=load('relative.dat','relative','-ascii');
relative(1+handles.otr_numb,3)=ypos; %#ok<NASGU>
save('relative.dat','relative','-ascii')

handles.pos_entry_y = ypos/handles.cal(1+handles.otr_numb,3);

guidata(gcbo,handles)

% --- Executes during object creation, after setting all properties.
function ypos_entry_CreateFcn(hObject, eventdata, handles)
% hObject    handle to ypos_entry (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
  set(hObject,'BackgroundColor','white');
end

% --- Executes on button press in ypos_ABS_move.
function ypos_ABS_move_Callback(hObject, eventdata, handles)
% hObject    handle to ypos_ABS_move (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

whichMotor = 2+(3*handles.otr_numb);
y=lcaGet(handles.RDBL{(2+3*handles.otr_numb)});
handles.pos_abs_steps_y=str2double(get(handles.ypos_abs_entry,'String'))/handles.cal(1+handles.otr_numb,3);
%relative distance in steps to desired position
y=handles.pos_abs_steps_y-((y-handles.wp(1+handles.otr_numb,3))*handles.cal(1+handles.otr_numb,6)/handles.cal(1+handles.otr_numb,3));
motor_move('move',0,y,1,0,1,0,whichMotor,3);
%Refresh ypos_read
%set(handles.ypos_read,'String',(handles.cal(1+handles.otr_numb,6)*(lcaGet(handles.RDBL{(2+3*handles.otr_numb)})-handles.wp(1+handles.otr_numb,3))));

% --- Executes on button press in ypos_RELB_move.
function ypos_RELB_move_Callback(hObject, eventdata, handles)
% hObject    handle to ypos_RELB_move (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
whichMotor = 2+(3*handles.otr_numb);
handles.pos_entry_y = -1*handles.pos_entry_y;
motor_move('move',0,handles.pos_entry_y,1,0,1,0,whichMotor,3);
set(handles.ypos_abs_entry,'String',str2double(get(handles.ypos_abs_entry,'String'))+handles.pos_entry_y*handles.cal(1+handles.otr_numb,3));
%Do if it is limit case
if handles.which_limit==2+(3*handles.otr_numb)&&getappdata(handles.single_otr_general_panel,'limit_switch_pressed')
  system('sleep 1');
  setappdata(handles.single_otr_general_panel,'limit_switch_pressed',0)
  handles.which_limit=13;
  for i=1:size(handles.limits_disable_tags,2)
    set(eval(handles.limits_disable_tags{i}),'Enable','on')
  end
  set(handles.clear_limit_y_max,'Enable','off')
  set(handles.clear_limit_y_max,'BackgroundColor',[0.702 0.702 0.702]);
  set(eval(['handles.startGuiHandles.limit_status_',num2str(handles.otr_numb)]),'String','')
  set(eval(['handles.startGuiHandles.limit_status_',num2str(handles.otr_numb)]),'BackgroundColor',[0.831 0.816 0.784]);
end


% --- Executes on button press in ypos_RELF_move.
function ypos_RELF_move_Callback(hObject, eventdata, handles)
% hObject    handle to ypos_RELF_move (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
whichMotor = 2+(3*handles.otr_numb);
motor_move('move',0,handles.pos_entry_y,1,0,1,0,whichMotor,3);
set(handles.ypos_abs_entry,'String',str2double(get(handles.ypos_abs_entry,'String'))+handles.pos_entry_y*handles.cal(1+handles.otr_numb,3));
%Do if it is limit case
if handles.which_limit==2+(3*handles.otr_numb)&&getappdata(handles.single_otr_general_panel,'limit_switch_pressed')
  system('sleep 1');
  setappdata(handles.single_otr_general_panel,'limit_switch_pressed',0)
  handles.which_limit=13;
  for i=1:size(handles.limits_disable_tags,2)
    set(eval(handles.limits_disable_tags{i}),'Enable','on')
  end
  set(handles.clear_limit_y_min,'Enable','off')
  set(handles.clear_limit_y_min,'BackgroundColor',[0.702 0.702 0.702]);
  set(eval(['handles.startGuiHandles.limit_status_',num2str(handles.otr_numb)]),'String','')
  set(eval(['handles.startGuiHandles.limit_status_',num2str(handles.otr_numb)]),'BackgroundColor',[0.831 0.816 0.784]);
end

function fpos_entry_Callback(hObject, eventdata, handles)
% hObject    handle to fpos_entry (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of fpos_entry as text
%        str2double(get(hObject,'String')) returns contents of fpos_entry as a double
fpos = str2double(get(hObject, 'String'));
if isnan(fpos)
  set(hObject, 'String', 0);
  errordlg('Input must be a number','Error');
end
%from um to steps

relative=load('relative.dat','relative','-ascii');
relative(1+handles.otr_numb,4)=fpos; %#ok<NASGU>
save('relative.dat','relative','-ascii')

handles.pos_entry_f = fpos/handles.cal(1+handles.otr_numb,4);
guidata(gcbo,handles)

% --- Executes during object creation, after setting all properties.
function fpos_entry_CreateFcn(hObject, eventdata, handles)
% hObject    handle to fpos_entry (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
  set(hObject,'BackgroundColor','white');
end

% --- Executes on button press in fpos_ABS_move.
function fpos_ABS_move_Callback(hObject, eventdata, handles)
% hObject    handle to fpos_ABS_move (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

whichMotor = 3+(3*handles.otr_numb);
f=lcaGet(handles.RDBL{(3+3*handles.otr_numb)});

handles.pos_abs_steps_f=str2double(get(handles.fpos_abs_entry,'String'))/handles.cal(1+handles.otr_numb,4);

%relative distance in steps to desired position
f=handles.pos_abs_steps_f-((f-handles.wp(1+handles.otr_numb,4))*handles.cal(1+handles.otr_numb,7)/handles.cal(1+handles.otr_numb,4));
motor_move('move',0,f,1,0,1,0,whichMotor,3);


% --- Executes on button press in fpos_RELB_move.
function fpos_RELB_move_Callback(hObject, eventdata, handles)
% hObject    handle to fpos_RELB_move (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
whichMotor = 3+(3*handles.otr_numb);
handles.pos_entry_f = -1*handles.pos_entry_f;
motor_move('move',0,handles.pos_entry_f,1,0,1,0,whichMotor,3);
set(handles.fpos_abs_entry,'String',str2double(get(handles.fpos_abs_entry,'String'))+handles.pos_entry_f*handles.cal(1+handles.otr_numb,4));
%Do if it is limit case
if handles.which_limit==3+(3*handles.otr_numb)&&getappdata(handles.single_otr_general_panel,'limit_switch_pressed')
  system('sleep 1');
  setappdata(handles.single_otr_general_panel,'limit_switch_pressed',0)
  handles.which_limit=13;
  for i=1:size(handles.limits_disable_tags,2)
    set(eval(handles.limits_disable_tags{i}),'Enable','on')
  end
  set(handles.clear_limit_f_max,'Enable','off')
  set(handles.clear_limit_f_max,'BackgroundColor',[0.702 0.702 0.702]);
  set(eval(['handles.startGuiHandles.limit_status_',num2str(handles.otr_numb)]),'String','')
  set(eval(['handles.startGuiHandles.limit_status_',num2str(handles.otr_numb)]),'BackgroundColor',[0.831 0.816 0.784]);
end


% --- Executes on button press in fpos_RELF_move.
function fpos_RELF_move_Callback(hObject, eventdata, handles)
% hObject    handle to fpos_RELF_move (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
whichMotor = 3+(3*handles.otr_numb);
motor_move('move',0,handles.pos_entry_f,1,0,1,0,whichMotor,3);
set(handles.fpos_abs_entry,'String',str2double(get(handles.fpos_abs_entry,'String'))+handles.pos_entry_f*handles.cal(1+handles.otr_numb,4));
%Do if it is limit case
if handles.which_limit==3+(3*handles.otr_numb)&&getappdata(handles.single_otr_general_panel,'limit_switch_pressed')
  system('sleep 1');
  setappdata(handles.single_otr_general_panel,'limit_switch_pressed',0)
  handles.which_limit=13;
  for i=1:size(handles.limits_disable_tags,2)
    set(eval(handles.limits_disable_tags{i}),'Enable','on')
  end
  set(handles.clear_limit_f_min,'Enable','off')
  set(handles.clear_limit_f_min,'BackgroundColor',[0.702 0.702 0.702]);
  set(eval(['handles.startGuiHandles.limit_status_',num2str(handles.otr_numb)]),'String','')
  set(eval(['handles.startGuiHandles.limit_status_',num2str(handles.otr_numb)]),'BackgroundColor',[0.831 0.816 0.784]);
end



% --- Executes during object creation, after setting all properties.
function fpos_read_CreateFcn(hObject, eventdata, handles)
% hObject    handle to fpos_read (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% --- Executes during object creation, after setting all properties.
function shot_CreateFcn(hObject, eventdata, handles)
% hObject    handle to shot (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% --- Executes on button press in otr_off.
function otr_off_Callback(hObject, eventdata, handles)
% hObject    handle to otr_off (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Make sure target is out
target_in_out(handles.otr_numb,0);

for i=1:size(handles.limits_disable_tags,2)
  set(eval(handles.limits_disable_tags{i}),'Enable','off')
end

if ~get(handles.x_switch,'Value')
  lcaPut(handles.XFSWITCH{(1+handles.otr_numb)},0); system('sleep 2');
  x=lcaGet(handles.RDBL{(1+3*handles.otr_numb)});
  lcaPut(handles.XFSWITCH{(1+handles.otr_numb)},1);
else
  x=lcaGet(handles.RDBL{(1+3*handles.otr_numb)});
end
y=lcaGet(handles.RDBL{(2+3*handles.otr_numb)});
x=(handles.off_mode(1+handles.otr_numb,2)-x)*handles.cal(1+handles.otr_numb,5);
y=(handles.off_mode(1+handles.otr_numb,3)-y)*handles.cal(1+handles.otr_numb,6);
maxtries=5; ntry=0;
while abs(x)>10 || abs(y)>10
  ntry=ntry+1;
  x=lcaGet(handles.RDBL{(1+3*handles.otr_numb)});
  y=lcaGet(handles.RDBL{(2+3*handles.otr_numb)});
  
  handles.reference_voltages=getappdata(handles.single_otr_general_panel,'reference_voltages');
  handles.off_mode(1:4,1:4)= cat(2,handles.reference_voltages(1:4,1),handles.reference_voltages(1:4,5:7));
  
  %relative distance in microns to off position & refreshing ABS entry
  x=(handles.off_mode(1+handles.otr_numb,2)-x)*handles.cal(1+handles.otr_numb,5);
  y=(handles.off_mode(1+handles.otr_numb,3)-y)*handles.cal(1+handles.otr_numb,6);
  set(handles.xpos_abs_entry,'String',str2double(get(handles.xpos_abs_entry,'String'))+x);
  set(handles.ypos_abs_entry,'String',str2double(get(handles.ypos_abs_entry,'String'))+y);
  
  %relative distance in steps to off position & moving
  xmv=x/handles.cal(1+handles.otr_numb,2);
  ymv=y/handles.cal(1+handles.otr_numb,3);
  motor_move('move',0,-xmv,1,0,1,0,(1+3*handles.otr_numb),3);
  motor_move('move',0,ymv,1,0,1,0,(2+3*handles.otr_numb),3);
  system(sprintf('sleep %f',max([3 [x y]./[400 800]])));
  drawnow('expose')
  if ntry>maxtries
    break
  end
end

for i=1:size(handles.limits_disable_tags,2)
  set(eval(handles.limits_disable_tags{i}),'Enable','on')
end

guidata(hObject, handles);


% --- Executes on button press in otr_working.
function otr_working_Callback(hObject, eventdata, handles)
% hObject    handle to otr_working (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Make sure target is out
target_in_out(handles.otr_numb,0);

for i=1:size(handles.limits_disable_tags,2)
  set(eval(handles.limits_disable_tags{i}),'Enable','off')
end

if ~get(handles.x_switch,'Value')
  lcaPut(handles.XFSWITCH{(1+handles.otr_numb)},0); system('sleep 2');
  x=lcaGet(handles.RDBL{(1+3*handles.otr_numb)});
  lcaPut(handles.XFSWITCH{(1+handles.otr_numb)},1);
else
  x=lcaGet(handles.RDBL{(1+3*handles.otr_numb)});
end
y=lcaGet(handles.RDBL{(2+3*handles.otr_numb)});
x=(handles.default_wp(1+handles.otr_numb,2)-x)*handles.cal(1+handles.otr_numb,5);
y=(handles.default_wp(1+handles.otr_numb,3)-y)*handles.cal(1+handles.otr_numb,6);

maxtries=5; ntry=0;
while abs(x)>10 || abs(y)>10
  ntry=ntry+1;
  x=lcaGet(handles.RDBL{(1+3*handles.otr_numb)});
  y=lcaGet(handles.RDBL{(2+3*handles.otr_numb)});
  
  handles.reference_voltages=getappdata(handles.single_otr_general_panel,'reference_voltages');
  handles.default_wp(1:4,1:4) = handles.reference_voltages(1:4,1:4);
  
  %relative distance in microns to off position & refreshing ABS entry
  x=(handles.default_wp(1+handles.otr_numb,2)-x)*handles.cal(1+handles.otr_numb,5);
  y=(handles.default_wp(1+handles.otr_numb,3)-y)*handles.cal(1+handles.otr_numb,6);
  set(handles.xpos_abs_entry,'String',str2double(get(handles.xpos_abs_entry,'String'))+x);
  set(handles.ypos_abs_entry,'String',str2double(get(handles.ypos_abs_entry,'String'))+y);
  
  %relative distance in steps to off position & moving
  xmv=x/handles.cal(1+handles.otr_numb,2);
  ymv=y/handles.cal(1+handles.otr_numb,3);
  motor_move('move',0,-xmv,1,0,1,0,(1+3*handles.otr_numb),3);
  motor_move('move',0,ymv,1,0,1,0,(2+3*handles.otr_numb),3);
  system(sprintf('sleep %f',max([3 [x y]./[400 800]])));
  drawnow('expose');
  if ntry>maxtries
    break
  end
end
for i=1:size(handles.limits_disable_tags,2)
  set(eval(handles.limits_disable_tags{i}),'Enable','on')
end

% Updating movement controls
if get(handles.x_switch,'Value')
  set(handles.xpos_abs_entry,'String',(handles.cal(1+handles.otr_numb,5)*(lcaGet(handles.RDBL{(1+3*handles.otr_numb)})-handles.wp(1+handles.otr_numb,2))));
  %set(handles.xpos_entry,'String',0);
else
  lcaPut(handles.XFSWITCH,0);
  set(handles.xpos_abs_entry,'String',(handles.cal(1+handles.otr_numb,5)*(lcaGet(handles.RDBL{(1+3*handles.otr_numb)})-handles.wp(1+handles.otr_numb,2))));
  %set(handles.xpos_entry,'String',0);
  lcaPut(handles.XFSWITCH,1);
end
set(handles.ypos_abs_entry,'String',(handles.cal(1+handles.otr_numb,6)*(lcaGet(handles.RDBL{(2+3*handles.otr_numb)})-handles.wp(1+handles.otr_numb,3))));
%set(handles.ypos_entry,'String',0);

guidata(hObject, handles);

function otr_set_working_Callback(hObject, eventdata, handles)

resp=questdlg(sprintf('Set current target position as main working position? Note: you should only do this if you can see the beam on the target. Also, ensure the Ref. settings panel is closed first'),'Save target location');
if ~strcmp(resp,'Yes'); return; end;

handles.wp = load('working_point.set', 'wp', '-ascii');%cause maybe another OTR changed it

if ~get(handles.x_switch,'Value')
  handles.wp(1+handles.otr_numb,4)=lcaGet(handles.RDBL{(3+3*handles.otr_numb)});
  lcaPut(handles.XFSWITCH{(1+handles.otr_numb)},0);
  handles.wp(1+handles.otr_numb,2)=lcaGet(handles.RDBL{(1+3*handles.otr_numb)});
  lcaPut(handles.XFSWITCH{(1+handles.otr_numb)},1);
else
  handles.wp(1+handles.otr_numb,2)=lcaGet(handles.RDBL{(1+3*handles.otr_numb)});
  lcaPut(handles.XFSWITCH{(1+handles.otr_numb)},1);
  handles.wp(1+handles.otr_numb,4)=lcaGet(handles.RDBL{(3+3*handles.otr_numb)});
  lcaPut(handles.XFSWITCH{(1+handles.otr_numb)},0);
end
handles.wp(1+handles.otr_numb,3)=lcaGet(handles.RDBL{(2+3*handles.otr_numb)});

wp=handles.wp; %#ok<NASGU>
save('working_point.set', 'wp', '-ascii')

setappdata(handles.single_otr_general_panel,'working_point',handles.wp)

% Updating movement controls
if get(handles.x_switch,'Value')
  set(handles.xpos_abs_entry,'String',(handles.cal(1+handles.otr_numb,5)*(lcaGet(handles.RDBL{(1+3*handles.otr_numb)})-handles.wp(1+handles.otr_numb,2))));
  %set(handles.xpos_entry,'String',0);
else
  lcaPut(handles.XFSWITCH,0);
  set(handles.xpos_abs_entry,'String',(handles.cal(1+handles.otr_numb,5)*(lcaGet(handles.RDBL{(1+3*handles.otr_numb)})-handles.wp(1+handles.otr_numb,2))));
  %set(handles.xpos_entry,'String',0);
  lcaPut(handles.XFSWITCH,1);
end
set(handles.ypos_abs_entry,'String',(handles.cal(1+handles.otr_numb,6)*(lcaGet(handles.RDBL{(2+3*handles.otr_numb)})-handles.wp(1+handles.otr_numb,3))));
%set(handles.ypos_entry,'String',0);

% Also set reference voltages
rv=getappdata(handles.single_otr_general_panel,'reference_voltages');
rv(1+handles.otr_numb,2)=handles.wp(1+handles.otr_numb,2);
rv(1+handles.otr_numb,3)=handles.wp(1+handles.otr_numb,3);
save -ASCII reference_voltages.dat rv
setappdata(handles.single_otr_general_panel,'reference_voltages',rv)

guidata(hObject, handles)


function otr_reset_working_Callback(hObject, eventdata, handles)

handles.wp = load('working_point.set', 'wp', '-ascii');%cause maybe another OTR changed it

handles.wp(1+handles.otr_numb,2)=handles.default_wp(1+handles.otr_numb,2);
handles.wp(1+handles.otr_numb,3)=handles.default_wp(1+handles.otr_numb,3);
setappdata(handles.single_otr_general_panel,'working_point',handles.wp)
wp=handles.wp; %#ok<NASGU>
save('working_point.set', 'wp', '-ascii')

% Updating movement controls
if get(handles.x_switch,'Value')
  set(handles.xpos_abs_entry,'String',(handles.cal(1+handles.otr_numb,5)*(lcaGet(handles.RDBL{(1+3*handles.otr_numb)})-handles.wp(1+handles.otr_numb,2))));
  set(handles.xpos_entry,'String',0);
else
  lcaPut(handles.XFSWITCH,0);
  set(handles.xpos_abs_entry,'String',(handles.cal(1+handles.otr_numb,5)*(lcaGet(handles.RDBL{(1+3*handles.otr_numb)})-handles.wp(1+handles.otr_numb,2))));
  set(handles.xpos_entry,'String',0);
  lcaPut(handles.XFSWITCH,1);
end
set(handles.ypos_abs_entry,'String',(handles.cal(1+handles.otr_numb,6)*(lcaGet(handles.RDBL{(2+3*handles.otr_numb)})-handles.wp(1+handles.otr_numb,3))));
set(handles.ypos_entry,'String',0);

guidata(hObject, handles)

function edit7_Callback(hObject, eventdata, handles)


function edit7_CreateFcn(hObject, eventdata, handles)

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
  set(hObject,'BackgroundColor','white');
end


function edit8_Callback(hObject, eventdata, handles)


function edit8_CreateFcn(hObject, eventdata, handles)

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
  set(hObject,'BackgroundColor','white');
end

function ms_set_cal_Callback(hObject, eventdata, handles)

cal_settings(handles.otr_numb,handles.single_otr_general_panel)
handles.cal = load('cal.set', 'calibration', '-ascii');
%setappdata(handles.single_otr_general_panel,'calibration',handles.cal)
guidata(hObject,handles)

function edit9_Callback(hObject, eventdata, handles)


function edit9_CreateFcn(hObject, eventdata, handles)

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
  set(hObject,'BackgroundColor','white');
end


function edit10_Callback(hObject, eventdata, handles)


function edit10_CreateFcn(hObject, eventdata, handles)

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
  set(hObject,'BackgroundColor','white');
end


function edit11_Callback(hObject, eventdata, handles)


function edit11_CreateFcn(hObject, eventdata, handles)

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
  set(hObject,'BackgroundColor','white');
end


function edit12_Callback(hObject, eventdata, handles)


function edit12_CreateFcn(hObject, eventdata, handles)

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
  set(hObject,'BackgroundColor','white');
end


function edit13_Callback(hObject, eventdata, handles)


function edit13_CreateFcn(hObject, eventdata, handles)

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
  set(hObject,'BackgroundColor','white');
end


function edit14_Callback(hObject, eventdata, handles)


function edit14_CreateFcn(hObject, eventdata, handles)

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
  set(hObject,'BackgroundColor','white');
end


function edit15_Callback(hObject, eventdata, handles)


function edit15_CreateFcn(hObject, eventdata, handles)

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
  set(hObject,'BackgroundColor','white');
end


function target_status_0_Callback(hObject, eventdata, handles)


function target_status_0_CreateFcn(hObject, eventdata, handles)

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
  set(hObject,'BackgroundColor','white');
end


function mode_status_0_Callback(hObject, eventdata, handles)


function mode_status_0_CreateFcn(hObject, eventdata, handles)

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
  set(hObject,'BackgroundColor','white');
end


function limits_status_0_Callback(hObject, eventdata, handles)


function limits_status_0_CreateFcn(hObject, eventdata, handles)

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
  set(hObject,'BackgroundColor','white');
end


function target_status_1_Callback(hObject, eventdata, handles)


function target_status_1_CreateFcn(hObject, eventdata, handles)

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
  set(hObject,'BackgroundColor','white');
end


function mode_status_1_Callback(hObject, eventdata, handles)


function mode_status_1_CreateFcn(hObject, eventdata, handles)

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
  set(hObject,'BackgroundColor','white');
end


function limits_status_1_Callback(hObject, eventdata, handles)


function limits_status_1_CreateFcn(hObject, eventdata, handles)

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
  set(hObject,'BackgroundColor','white');
end


function target_status_2_Callback(hObject, eventdata, handles)


function target_status_2_CreateFcn(hObject, eventdata, handles)

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
  set(hObject,'BackgroundColor','white');
end


function mode_status_2_Callback(hObject, eventdata, handles)


function mode_status_2_CreateFcn(hObject, eventdata, handles)

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
  set(hObject,'BackgroundColor','white');
end


function limits_status_2_Callback(hObject, eventdata, handles)


function limits_status_2_CreateFcn(hObject, eventdata, handles)

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
  set(hObject,'BackgroundColor','white');
end


function target_status_3_Callback(hObject, eventdata, handles)


function target_status_3_CreateFcn(hObject, eventdata, handles)

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
  set(hObject,'BackgroundColor','white');
end


function mode_status_3_Callback(hObject, eventdata, handles)


function mode_status_3_CreateFcn(hObject, eventdata, handles)

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
  set(hObject,'BackgroundColor','white');
end


function limits_status_3_Callback(hObject, eventdata, handles)


function limits_status_3_CreateFcn(hObject, eventdata, handles)

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
  set(hObject,'BackgroundColor','white');
end


function ms_reset_cal_Callback(hObject, eventdata, handles)
handles.cal=handles.default_cal;
calibration=handles.default_cal; %#ok<NASGU>
save('cal.set', 'calibration', '-ascii')
setappdata(handles.single_otr_general_panel,'calibration',handles.cal)
guidata(hObject, handles)


function xF_switch_SelectionChangeFcn(hObject, eventdata, handles)

if hObject == handles.F_switch
  set(handles.xpos_RELF_move,'Enable','off');
  set(handles.xpos_RELB_move,'Enable','off');
  set(handles.xpos_ABS_move,'Enable','off');
  set(handles.xpos_entry,'Enable','off');
  set(handles.xpos_abs_entry,'Enable','off');
  set(handles.xpos_read,'BackgroundColor',[0.4 0.4 0.4]);
  set(handles.xpos_read_v,'BackgroundColor',[0.4 0.4 0.4]);
  
  
  lcaPut(handles.XFSWITCH,1);
  
  set(handles.fpos_RELF_move,'Enable','on');
  set(handles.fpos_RELB_move,'Enable','on');
  set(handles.fpos_ABS_move,'Enable','on');
  set(handles.fpos_entry,'Enable','on');
  set(handles.fpos_abs_entry,'Enable','on');
  set(handles.fpos_read,'BackgroundColor',[1 1 1]);
  set(handles.fpos_read_v,'BackgroundColor',[1 1 1]);
  
  setappdata(handles.single_otr_general_panel,'xFswitch',0)
  
elseif hObject == handles.x_switch
  
  set(handles.fpos_RELF_move,'Enable','off');
  set(handles.fpos_RELB_move,'Enable','off');
  set(handles.fpos_ABS_move,'Enable','off');
  set(handles.fpos_entry,'Enable','off');
  set(handles.fpos_abs_entry,'Enable','off');
  set(handles.fpos_read,'BackgroundColor',[0.4 0.4 0.4]);
  set(handles.fpos_read_v,'BackgroundColor',[0.4 0.4 0.4]);
  
  
  lcaPut(handles.XFSWITCH,0);
  
  set(handles.xpos_RELF_move,'Enable','on');
  set(handles.xpos_RELB_move,'Enable','on');
  set(handles.xpos_ABS_move,'Enable','on');
  set(handles.xpos_entry,'Enable','on');
  set(handles.xpos_abs_entry,'Enable','on');
  set(handles.xpos_read,'BackgroundColor',[1 1 1]);
  set(handles.xpos_read_v,'BackgroundColor',[1 1 1]);
  
  setappdata(handles.single_otr_general_panel,'xFswitch',1)
  
end
guidata(hObject, handles)


function otr_selector_CreateFcn(hObject, eventdata, handles) %#ok<*INUSD,*DEFNU>


function xF_switch_CreateFcn(hObject, eventdata, handles)

function figure1_CloseRequestFcn(hObject, eventdata, handles) %#ok<*INUSL>
% Hint: delete(hObject) closes the figure
try
  if getappdata(handles.single_otr_general_panel,'limit_switch_pressed')
    set(handles.text_under_screen,'String','CANNOT CLOSE, LIMIT PRESSED')
    system('sleep 5');
    set(handles.text_under_screen,'String','')
  else
    %Take the target out when closing or not?
    %target_in_out(handles.otr_numb,0);
    
    stop(handles.position_timer); system('sleep 1');
    delete(handles.position_timer)
    stop(handles.ccd_refresh); system('sleep 1');
    delete(handles.ccd_refresh)
    
    
    %setappdata(handles.startGuiHandles.Emittance,sprintf('enable_%d',handles.otr_numb),0)
    %set(handles.startGuiHandles.Emittance,'Enable','off')
    %TO DO: ATTENTION, DOESN'T WORK, need to close cause if mOTR is closed
    %ccdtimer doesnt work and thus emittance doesnt work
    %if ishandle(emittance)
    %    close(emittance)
    %end
    mode=get(eval(sprintf('handles.startGuiHandles.mode_status_%d',handles.otr_numb)),'String');
    set(handles.startGuiHandles.(sprintf('mode_status_%d',handles.otr_numb)),'String',mode)
    delete(hObject)
    
    %relative=load('relative.dat','relative','-ascii');
    %relative(1+handles.otr_numb,2)=str2double(get(handles.xpos_entry,'String'));
    %relative(1+handles.otr_numb,3)=str2double(get(handles.ypos_entry,'String'));
    %relative(1+handles.otr_numb,4)=str2double(get(handles.fpos_entry,'String'));
    %save('relative.dat','relative','-ascii')
  end
catch
  delete(hObject)
end
% --- Executes during object creation, after setting all properties.
function ypos_read_CreateFcn(hObject, eventdata, handles)
% hObject    handle to ypos_read (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called



function xpos_abs_entry_Callback(hObject, eventdata, handles)
% hObject    handle to xpos_abs_entry (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of xpos_abs_entry as text
%        str2double(get(hObject,'String')) returns contents of xpos_abs_entry as a double
xpos_abs = str2double(get(hObject, 'String'));
if isnan(xpos_abs)
  set(hObject, 'String', 0);
  errordlg('Input must be a number','Error');
end

handles.pos_abs_steps_x = xpos_abs/handles.cal(1+handles.otr_numb,2);

guidata(gcbo,handles)


% --- Executes during object creation, after setting all properties.
function xpos_abs_entry_CreateFcn(hObject, eventdata, handles)
% hObject    handle to xpos_abs_entry (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
  set(hObject,'BackgroundColor','white');
end



function fpos_abs_entry_Callback(hObject, eventdata, handles)
% hObject    handle to fpos_abs_entry (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of fpos_abs_entry as text
%        str2double(get(hObject,'String')) returns contents of fpos_abs_entry as a double
fpos_abs = str2double(get(hObject, 'String'));
if isnan(fpos_abs)
  set(hObject, 'String', 0);
  errordlg('Input must be a number','Error');
end

handles.pos_abs_steps_f = fpos_abs/handles.cal(1+handles.otr_numb,4);

guidata(gcbo,handles)

% --- Executes during object creation, after setting all properties.
function fpos_abs_entry_CreateFcn(hObject, eventdata, handles)
% hObject    handle to fpos_abs_entry (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
  set(hObject,'BackgroundColor','white');
end



function ypos_abs_entry_Callback(hObject, eventdata, handles)
% hObject    handle to ypos_abs_entry (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of ypos_abs_entry as text
%        str2double(get(hObject,'String')) returns contents of ypos_abs_entry as a double
ypos_abs = str2double(get(hObject, 'String'));
if isnan(ypos_abs)
  set(hObject, 'String', 0);
  errordlg('Input must be a number','Error');
end

handles.pos_abs_steps_y = ypos_abs/handles.cal(1+handles.otr_numb,3);

guidata(gcbo,handles)

% --- Executes during object creation, after setting all properties.
function ypos_abs_entry_CreateFcn(hObject, eventdata, handles)
% hObject    handle to ypos_abs_entry (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
  set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in change_defaults_button.
function change_defaults_button_Callback(hObject, eventdata, handles)
% hObject    handle to change_defaults_button (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

reference_voltages(handles)
guidata(hObject,handles)


% --- Executes on button press in pushbutton55.
function pushbutton55_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton55 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
if get(handles.F_switch,'Value')
  set(handles.text_under_screen,'String','Switch from focus to x')
  system('sleep 1');
  set(handles.text_under_screen,'String','')
else
  calib_find(handles)
end

% --- Executes on button press in clear_limit_x_min.
function clear_limit_x_min_Callback(hObject, eventdata, handles)
% hObject    handle to clear_limit_x_min (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
lcaPut(['mOTR:motor',num2str(1+(3*handles.otr_numb)),'.UEIP'],1)
set(handles.xpos_RELF_move,'Enable','on')
%set(handles.xpos_entry,'Enable','on')
handles.which_limit=1+(3*handles.otr_numb);
set(handles.xpos_entry,'String',num2str(1000))
handles.pos_entry_x = 1000/handles.cal(1+handles.otr_numb,2);
guidata(hObject,handles)


% --- Executes on button press in clear_limit_x_max.
function clear_limit_x_max_Callback(hObject, eventdata, handles)
% hObject    handle to clear_limit_x_max (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
lcaPut(['mOTR:motor',num2str(1+(3*handles.otr_numb)),'.UEIP'],1)
set(handles.xpos_RELB_move,'Enable','on')
%set(handles.xpos_entry,'Enable','on')
handles.which_limit=1+(3*handles.otr_numb);
set(handles.xpos_entry,'String',num2str(1000))
handles.pos_entry_x = 1000/handles.cal(1+handles.otr_numb,2);
guidata(hObject,handles)



% --- Executes on button press in clear_limit_y_min.
function clear_limit_y_min_Callback(hObject, eventdata, handles)
% hObject    handle to clear_limit_y_min (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
lcaPut(['mOTR:motor',num2str(2+(3*handles.otr_numb)),'.UEIP'],1)
set(handles.ypos_RELF_move,'Enable','on')
%set(handles.ypos_entry,'Enable','on')
handles.which_limit=2+(3*handles.otr_numb);
set(handles.ypos_entry,'String',num2str(1000))
handles.pos_entry_y = 1000/handles.cal(1+handles.otr_numb,3);
guidata(hObject,handles)


% --- Executes on button press in clear_limit_y_max.
function clear_limit_y_max_Callback(hObject, eventdata, handles)
% hObject    handle to clear_limit_y_max (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
lcaPut(['mOTR:motor',num2str(2+(3*handles.otr_numb)),'.UEIP'],1)
set(handles.ypos_RELB_move,'Enable','on')
%set(handles.ypos_entry,'Enable','on')
handles.which_limit=2+(3*handles.otr_numb);
set(handles.ypos_entry,'String',num2str(1000))
handles.pos_entry_y = 1000/handles.cal(1+handles.otr_numb,3);
guidata(hObject,handles)


% --- Executes on button press in clear_limit_f_min.
function clear_limit_f_min_Callback(hObject, eventdata, handles)
% hObject    handle to clear_limit_f_min (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
lcaPut(['mOTR:motor',num2str(3+(3*handles.otr_numb)),'.UEIP'],1)
set(handles.fpos_RELF_move,'Enable','on')
%set(handles.fpos_entry,'Enable','on')
handles.which_limit=3+(3*handles.otr_numb);
set(handles.fpos_entry,'String',num2str(5000))
handles.pos_entry_f = 5000/handles.cal(1+handles.otr_numb,4);
guidata(hObject,handles)


% --- Executes on button press in clear_limit_f_max.
function clear_limit_f_max_Callback(hObject, eventdata, handles)
% hObject    handle to clear_limit_f_max (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
lcaPut(['mOTR:motor',num2str(3+(3*handles.otr_numb)),'.UEIP'],1)
set(handles.fpos_RELB_move,'Enable','on')
%set(handles.fpos_entry,'Enable','on')
handles.which_limit=3+(3*handles.otr_numb);
set(handles.fpos_entry,'String',num2str(5000))
handles.pos_entry_f = 5000/handles.cal(1+handles.otr_numb,4);
guidata(hObject,handles)


% Gain setting
function edit31_Callback(hObject, eventdata, handles)
% hObject    handle to edit31 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
if exist('inttime.mat','file')
  load gain.mat gain
end
gain(handles.otr_numb+1)=str2double(get(hObject,'String'));
if ~isnan(gain(handles.otr_numb+1)) && gain(handles.otr_numb+1)>=0 && gain(handles.otr_numb+1)<=24
  lcaPutNoWait(sprintf('mOTR:cam%d:Gain',1+handles.otr_numb),gain(handles.otr_numb+1))
  system('sleep 1');
  save gain.mat gain
end
set(hObject,'String',num2str(lcaGet(sprintf('mOTR:cam%d:Gain',1+handles.otr_numb))))


% --- Executes during object creation, after setting all properties.
function edit31_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit31 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
  set(hObject,'BackgroundColor','white');
end


% Integration time setting
function edit32_Callback(hObject, eventdata, handles)
% hObject    handle to edit32 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

if exist('inttime.mat','file')
  load inttime.mat inttime
end
inttime(handles.otr_numb+1)=str2double(get(hObject,'String'));
if ~isnan(inttime(handles.otr_numb+1)) && inttime(handles.otr_numb+1)>0 && inttime(handles.otr_numb+1)<1
  lcaPutNoWait(sprintf('mOTR:cam%d:AcquireTime',1+handles.otr_numb),inttime(handles.otr_numb+1))
  system('sleep 1');
  save inttime.mat inttime
end
set(hObject,'String',num2str(lcaGet(sprintf('mOTR:cam%d:AcquireTime',1+handles.otr_numb))))

% --- Executes during object creation, after setting all properties.
function edit32_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit32 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
  set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in checkbox1.
function checkbox1_Callback(hObject, eventdata, handles)
% hObject    handle to checkbox1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of checkbox1


% --- Executes on button press in radiobutton9.
function radiobutton9_Callback(hObject, eventdata, handles)
% hObject    handle to radiobutton9 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

if get(hObject,'Value')
  set(handles.radiobutton10,'Value',0)
elseif ~get(handles.radiobutton10,'Value')
  set(hObject,'Value',1)
end


% --- Executes on button press in radiobutton10.
function radiobutton10_Callback(hObject, eventdata, handles)
% hObject    handle to radiobutton10 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

if get(hObject,'Value')
  set(handles.radiobutton9,'Value',0)
elseif ~get(handles.radiobutton9,'Value')
  set(hObject,'Value',1)
end


% --- Executes on button press in x_switch.
function x_switch_Callback(hObject, eventdata, handles)
% hObject    handle to x_switch (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

if get(hObject,'Value')
  set(handles.F_switch,'Value',0)
elseif ~get(handles.F_switch,'Value')
  set(hObject,'Value',1)
end
xF_switch_SelectionChangeFcn(hObject, eventdata, handles)

% --- Executes on button press in F_switch.
function F_switch_Callback(hObject, eventdata, handles)
% hObject    handle to F_switch (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

if get(hObject,'Value')
  set(handles.x_switch,'Value',0)
elseif ~get(handles.x_switch,'Value')
  set(hObject,'Value',1)
end
xF_switch_SelectionChangeFcn(hObject, eventdata, handles)

% --- Executes during object creation, after setting all properties.
function x_switch_CreateFcn(hObject, eventdata, handles)
% hObject    handle to x_switch (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called



function edit33_Callback(hObject, eventdata, handles)
% hObject    handle to edit33 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

if exist('ave.mat','file')
  load ave.mat ave
end

ave(handles.otr_numb+1)=str2double(get(hObject,'String'));
if isnan(ave(handles.otr_numb+1)) || ave(handles.otr_numb+1)<1 || ave(handles.otr_numb+1)>200
  set(hObject,'String','10')
else
  save ave.mat ave
end


% --- Executes during object creation, after setting all properties.
function edit33_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit33 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
  set(hObject,'BackgroundColor','white');
end


% --- Executes during object creation, after setting all properties.
function xpos_ABS_move_CreateFcn(hObject, eventdata, handles)
% hObject    handle to xpos_ABS_move (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called



function edit34_Callback(hObject, eventdata, handles)
% hObject    handle to edit34 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

if exist('sigmacut.mat','file')
  load sigmacut.mat sigmacut
end

sigmacut(handles.otr_numb+1)=str2double(get(hObject,'String'));
if isnan(sigmacut(handles.otr_numb+1)) || sigmacut(handles.otr_numb+1)<0.1 || sigmacut(handles.otr_numb+1)>10
  set(hObject,'String','3')
else
  save sigmacut.mat sigmacut
end



% --- Executes during object creation, after setting all properties.
function edit34_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit34 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
  set(hObject,'BackgroundColor','white');
end



function edit35_Callback(hObject, eventdata, handles)
% hObject    handle to edit35 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

if exist('cutRatio.mat','file')
  load cutRatio.mat cutRatio
end

cutRatio(handles.otr_numb+1)=str2double(get(hObject,'String'))*1e-2;
if isnan(cutRatio(handles.otr_numb+1)) || cutRatio(handles.otr_numb+1)<=0 || cutRatio(handles.otr_numb+1)>0.5
  set(hObject,'String','0.2')
else
  save cutRatio.mat cutRatio
end


% --- Executes during object creation, after setting all properties.
function edit35_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit35 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
  set(hObject,'BackgroundColor','white');
end



function edit36_Callback(hObject, eventdata, handles)
% hObject    handle to edit36 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

if exist('beamCut.mat','file')
  load beamCut.mat beamCut
end

beamCut(handles.otr_numb+1)=str2double(get(hObject,'String'))*1e8;
if isnan(beamCut(handles.otr_numb+1)) || beamCut(handles.otr_numb+1)<0 || beamCut(handles.otr_numb+1)>1e20
  set(hObject,'String','1')
else
  save beamCut.mat beamCut
end


% --- Executes during object creation, after setting all properties.
function edit36_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit36 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
  set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in togglebutton1.
function togglebutton1_Callback(hObject, eventdata, handles)
% hObject    handle to togglebutton1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% try
%   beamCentring=getappdata(handles.single_otr_general_panel,'beamCentring');
% catch
% end
% beamCentring(handles.otr_numb+1)=get(hObject,'Value');
% setappdata(handles.single_otr_general_panel,'beamCentring',beamCentring);
% save beamCentring.mat beamCentring


% --- Executes on slider movement.
function slider1_Callback(hObject, eventdata, handles)
% hObject    handle to slider1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
if ~isempty(get(hObject,'UserData')) && get(hObject,'UserData')
  return
end
stop(handles.ccd_refresh);
set(hObject,'UserData',1)
set(hObject,'Enable','off')
set(handles.edit37,'Enable','off')
drawnow('expose')
try
  rdbk=otrZoom('stepper',handles.otr_numb,get(hObject,'Value'));
catch
  rdbk=NaN;
end
set(hObject,'UserData',0)
set(hObject,'Enable','on')
set(handles.edit37,'Enable','on')
drawnow('expose')
set(handles.edit37,'String',num2str(rdbk,2))
start(handles.ccd_refresh);

% --- Executes during object creation, after setting all properties.
function slider1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to slider1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
  set(hObject,'BackgroundColor',[.9 .9 .9]);
end



function edit37_Callback(hObject, eventdata, handles)
% hObject    handle to edit37 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

val=str2double(get(hObject,'String'));
if isnan(val) || val<1
  set(hObject,'String','1')
  set(handles.slider1,'Value',1)
elseif val>5.75
  set(hObject,'String','5.75')
  set(handles.slider1,'Value',5.75)
else
  set(handles.slider1,'Value',val)
end
slider1_Callback(handles.slider1,[],handles);


% --- Executes during object creation, after setting all properties.
function edit37_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit37 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
  set(hObject,'BackgroundColor','white');
end


% --------------------------------------------------------------------
function File_Callback(hObject, eventdata, handles)
% hObject    handle to File (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --------------------------------------------------------------------
function Tools_Callback(hObject, eventdata, handles)
% hObject    handle to Tools (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --------------------------------------------------------------------
function saveData_Callback(hObject, eventdata, handles)
% hObject    handle to saveData (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
[file, direc]=uiputfile('*.mat');
if ~file; return; end;
ndata=str2double(get(handles.edit33,'String'));
idata=1;
msgbox('Saving Data...','mOTR save','modal');
drawnow
while idata<=ndata
  otrData(idata)=ccd_timer('GetData',[],handles,handles.otr_numb);
  if idata==1 || ~isequal(otrData(end).sigy,otrData(end-1).sigy)
    idata=idata+1;
  end
end
save(fullfile(direc,file),'otrData');
msgbox('Data saved','mOTR save','modal');
drawnow

% --------------------------------------------------------------------
function focusScan_Callback(hObject, eventdata, handles)
% hObject    handle to focusScan (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Select focus controls
freturn=false;
if ~get(handles.x_switch,'Value')
  freturn=true;
  lcaPutNoWait(handles.XFSWITCH{(1+handles.otr_numb)},1); system('sleep 2');
end

% Launch GUI
fhan=focusScan;

% Wait for start button (or abort)
uiwait(fhan.figure1);
co=get(fhan.figure1,'CurrentObject');
if co~=fhan.pushbutton1
  delete(fhan.figure1);
  return
end

% Get scan points
fscan=linspace(str2double(get(fhan.edit2,'String')),str2double(get(fhan.edit3,'String')),str2double(get(fhan.edit1,'String')));

my=zeros(1,length(fscan)); sy=my;
ipos=str2double(get(handles.fpos_abs_entry,'String'));
stop(handles.ccd_refresh);
try
  for iscan=1:length(fscan)
    % Move focus
    set(handles.fpos_abs_entry,'String',num2str(ipos+fscan(iscan)));
    fpos_abs_entry_Callback(handles.fpos_abs_entry, [], handles);
    system(sprintf('sleep %f',max([3 (ipos+fscan(iscan))/1000])));
    % Get pulses
    ndata=0; itry=0; np=str2double(get(fhan.edit1,'String'));
    res=ccd_timer('GetData'); idata=res.counter; q=0;
    while ndata<np
      % Check for abort command
      if get(fhan.figure1,'CurrentObject')==fhan.pushbutton2
        break
      end
      itry=itry+1;
      % Process next pulse
      ccd_timer([],[],handles,handles.otr_numb);
      if itry>=np
        res=ccd_timer('GetData');
        ndata=res.counter-idata;
      end
      if itry>np*2; errordlg('Timeout waiting for new bunches, quitting','New bunch timeout'); error('OTR timeout'); end;
    end
    % Get beam size
    my(iscan)=mean(res.sig_y(1+end-np:end));
    sy(iscan)=std(res.sig_y(1+end-np:end));
    % plot
    if iscan<3
      errorbar(fhan.axes1,fscan(1:iscan),my(1:iscan),sy(1:iscan))
    else
      plot_parab(fhan.axes1);
      [q dq]=plot_parab(fscan(1:iscan),my(1:iscan),sy(1:iscan),2,'Focus scan position','Vertical size / um');
    end
    drawnow('expose');
  end

  % Process fitted focus position
  if ~isequal(q,0)
    resp=questdlg(sprintf('Minimum beam size from parabolic fit = %.2f +/- %.2f\nApply?',q(2),dq(2)),'Apply fit?');
    if ~strcmp(resp,'Yes')
      set(handles.fpos_abs_entry,'String',num2str(ipos));
      fpos_abs_entry_Callback(handles.fpos_abs_entry, [], handles);
      start(handles.ccd_refresh);
      return
    end
    % Move focus to min beam size position
    set(handles.fpos_abs_entry,'String',num2str(ipos+q(2)));
    fpos_abs_entry_Callback(handles.fpos_abs_entry, [], handles);
  end
catch
  set(handles.fpos_abs_entry,'String',num2str(ipos));
  fpos_abs_entry_Callback(handles.fpos_abs_entry, [], handles);
  start(handles.ccd_refresh);
end
% Put back to x mode?
if freturn
  lcaPutNoWait(handles.XFSWITCH{(1+handles.otr_numb)},0); system('sleep 2');
end

start(handles.ccd_refresh);


% --------------------------------------------------------------------
function help_Callback(hObject, eventdata, handles)
% hObject    handle to help (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --------------------------------------------------------------------
function helpCoord_Callback(hObject, eventdata, handles)
% hObject    handle to helpCoord (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

han=help_coord;
[f fmap]=imread('rhs.gif');
image(f,'Parent',han.axes1);
colormap(han.axes1,fmap);
axis(han.axes1,'off');
f2=imread('imhelp.png');
image(f2,'Parent',han.axes2);
% colormap(han.axes2,f2map);
axis(han.axes2,'off');


% --------------------------------------------------------------------
function Calibrate_Callback(hObject, eventdata, handles)
% hObject    handle to Calibrate (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

stop(handles.ccd_refresh);

% Launch GUI
chan=calibrate;

% Wait for start button (or abort)
uiwait(chan.figure1);
if ~ishandle(chan.figure1)
  start(handles.ccd_refresh);
  return
end
co=get(chan.figure1,'CurrentObject');
if co~=chan.pushbutton1
  delete(chan.figure1);
  start(handles.ccd_refresh);
  return
end

range1=str2double(get(chan.edit1,'String'));
range2=str2double(get(chan.edit2,'String'));
np=str2double(get(chan.edit3,'String'));
nsteps=str2double(get(chan.edit4,'String'));
dir=get(chan.popupmenu1,'Value');
% set(handles.xpos_abs_entry,'String',round(handles.cal(1+handles.otr_numb,5)*(-lcaGet(handles.RDBL{(1+3*handles.otr_numb)})-handles.wp(1+handles.otr_numb,2))));
% set(handles.xpos_entry,'String',0);
% set(handles.ypos_abs_entry,'String',round((handles.cal(1+handles.otr_numb,6)*(lcaGet(handles.RDBL{(2+3*handles.otr_numb)})-handles.wp(1+handles.otr_numb,3)))));
steps=linspace(range1,range2,nsteps);
if dir==1
  if ~get(handles.x_switch,'Value')
    set(handles.x_switch,'Value',1)
    set(handles.F_switch,'Value',0)
    xF_switch_SelectionChangeFcn(hObject, eventdata, handles)
  end
  pause(3)
  ipos=round(handles.cal(1+handles.otr_numb,5)*(lcaGet(handles.RDBL{(1+3*handles.otr_numb)})-handles.wp(1+handles.otr_numb,2)));
else
  ipos=round((handles.cal(1+handles.otr_numb,6)*(lcaGet(handles.RDBL{(2+3*handles.otr_numb)})-handles.wp(1+handles.otr_numb,3))));
end
q=0;
try
  for istep=1:length(steps)
    if dir==1
      set(handles.xpos_abs_entry,'String',num2str(ipos+steps(istep)))
      xpos_ABS_move_Callback(handles.xpos_ABS_move,[],handles);
    else
      set(handles.ypos_abs_entry,'String',num2str(ipos+steps(istep)))
      ypos_ABS_move_Callback(handles.xpos_ABS_move,[],handles);
    end
    ndata=0; itry=0;
    res=ccd_timer('GetData');
    ndata1=res.counter;
    if istep==1
      for itemp=1:3; ccd_timer([],[],handles,handles.otr_numb); end;
    else
      ccd_timer([],[],handles,handles.otr_numb);
    end
    while ndata<np
      % Check for abort command
      if get(chan.figure1,'CurrentObject')==chan.pushbutton2
        break
      end
      itry=itry+1;
      % Process next pulse
      ccd_timer([],[],handles,handles.otr_numb);
      if itry>=np
        res=ccd_timer('GetData');
        ndata=res.counter-ndata1;
      end
      if itry>np*5; errordlg('Timeout waiting for new bunches, quitting','New bunch timeout'); delete(chan.figure1); start(handles.ccd_refresh); return; end;
    end
    % Get beam position
    if dir==1
      my(istep)=mean(res.cent_x(1+end-np:end));
      sy(istep)=std(res.cent_x(1+end-np:end));
    else
      my(istep)=mean(res.cent_y(1+end-np:end));
      sy(istep)=std(res.cent_y(1+end-np:end));
    end
    % plot
    if istep<3
      errorbar(chan.axes1,steps(1:istep),my(1:istep),sy(1:istep))
    else
      plot_polyfit(chan.axes1);
      [q dq]=plot_polyfit(steps(1:istep),my(1:istep),sy(1:istep),1,'Scan position','Beam Centroid','um','um');
    end
    drawnow('expose');
  end
catch
  if dir==1
    set(handles.xpos_abs_entry,'String',num2str(ipos))
    xpos_ABS_move_Callback(handles.xpos_ABS_move,[],handles);
  else
    set(handles.ypos_abs_entry,'String',num2str(ipos))
    ypos_ABS_move_Callback(handles.xpos_ABS_move,[],handles);
  end
  delete(chan.figure1);
  start(handles.ccd_refresh);
  return
end
if dir==1
  set(handles.xpos_abs_entry,'String',num2str(ipos))
  xpos_ABS_move_Callback(handles.xpos_ABS_move,[],handles);
else
  set(handles.ypos_abs_entry,'String',num2str(ipos))
  ypos_ABS_move_Callback(handles.xpos_ABS_move,[],handles);
end
% Process slopes
if ~isequal(q,0)
  cal = load('cal.set', 'calibration', '-ascii');
  if dir==1
    try
      oldcal=cal(1+handles.otr_numb,9);
      newcal=cal(1+handles.otr_numb,9)/abs(q(2));
    catch
      newcal=0.5*q(2);
    end
  else
    oldcal=cal(1+handles.otr_numb,8);
    newcal=cal(1+handles.otr_numb,8)/abs(q(2));
  end
  resp=questdlg(sprintf('Measured slope = %.2f +/- %.2f\nApply correction factor to calibration constant?\nOld cal constant=%g\nNew cal constant=%g',abs(q(2)),dq(2),oldcal,newcal),'Apply fit?');
  if ~strcmp(resp,'Yes')
    start(handles.ccd_refresh);
    return
  end
  if dir==1
    cal(1+handles.otr_numb,9) = newcal;
  else
    cal(1+handles.otr_numb,8) = newcal;
  end
  save('cal.set', 'cal', '-ascii');
%   handles.cal=cal;
%   setappdata(handles,'calibration',handles.cal)
end
delete(chan.figure1);
start(handles.ccd_refresh);


% --- Executes on button press in pushbutton62.
function pushbutton62_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton62 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global FL
FL.Gui.rotcal=rotcal;


% --- Executes on button press in radiobutton11.
function radiobutton11_Callback(hObject, eventdata, handles)
% hObject    handle to radiobutton11 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

if get(hObject,'Value')
  set(handles.radiobutton12,'Value',0)
  set(handles.radiobutton13,'Value',0)
  if exist('fitMethod.mat','file')
    load fitMethod.mat fitMethod
    fitMethod(1+handles.otr_numb)=1;
    save fitMethod.mat fitMethod
  else
    fitMethod=ones(1,4); fitMethod(1+handles.otr_numb)=1;
    save fitMethod.mat fitMethod
  end
end


% --- Executes on button press in radiobutton12.
function radiobutton12_Callback(hObject, eventdata, handles)
% hObject    handle to radiobutton12 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

if get(hObject,'Value')
  set(handles.radiobutton11,'Value',0)
  set(handles.radiobutton13,'Value',0)
  if exist('fitMethod.mat','file')
    load fitMethod.mat fitMethod
    fitMethod(1+handles.otr_numb)=2;
    save fitMethod.mat fitMethod
  else
    fitMethod=ones(1,4); fitMethod(1+handles.otr_numb)=2;
    save fitMethod.mat fitMethod
  end
end


% --- Executes on button press in radiobutton13.
function radiobutton13_Callback(hObject, eventdata, handles)
% hObject    handle to radiobutton13 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

if get(hObject,'Value')
  set(handles.radiobutton11,'Value',0)
  set(handles.radiobutton12,'Value',0)
  if exist('fitMethod.mat','file')
    load fitMethod.mat fitMethod
    fitMethod(1+handles.otr_numb)=3;
    save fitMethod.mat fitMethod
  else
    fitMethod=ones(1,4); fitMethod(1+handles.otr_numb)=3;
    save fitMethod.mat fitMethod
  end
end


% --------------------------------------------------------------------
function stpctrl_Callback(hObject, eventdata, handles)
% hObject    handle to stpctrl (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
han=stepperControl;
set(han.popupmenu1,'Value',handles.otr_numb+1); stepperControl('popupmenu1_Callback',han.popupmenu1,[],han);


% --------------------------------------------------------------------
function filterCntrl_Callback(hObject, eventdata, handles)
% hObject    handle to filterCntrl (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
han=filterControl;
set(han.popupmenu1,'Value',handles.otr_numb+1); filterControl('popupmenu1_Callback',han.popupmenu1,[],han);

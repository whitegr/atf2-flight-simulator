function rdbk=otrZoom(cmd,iotr,val)
% rdbk=otrZoom(cmd,iotr,val)
% zoom control for OTR system
%
% iotr = 0-3 for OTR(0-3)X
% cmd = 'readpos' or 'stepper'
%  if 'stepper' then provide desired zoom level in 'val' [1-7]
%
% Error if limit switch becomes active

% rdbk=1; return;

stepperPV=sprintf('mOTR:XPS3Aux4Bo%d',iotr*3);
stepperDirPV=sprintf('mOTR:XPS3Aux4Bo%d',1+iotr*3);
disablePV=sprintf('mOTR:XPS3Aux4Bo%d',2+iotr*3);
rdbkPV=sprintf('mOTR:XPS3AuxAi%d',iotr);
limitPV={sprintf('mOTR:XPS3Aux4Bi%d',iotr*2); sprintf('mOTR:XPS3Aux4Bi%d',1+iotr*2)};
% V
% 0.612 1.000 1.345 1.645 1.908 2.151 2.370 2.543 2.726 3.030 3.298 3.525 3.718 3.900 4.074 4.232 4.400
% 1.000 1.250 1.500 1.750 2.000 2.250 2.500 2.750 3.000 3.500 4.000 4.500 5.000 5.500 6.000 6.500 7.000
% Zoom
potPoly=[0.0659 -0.0027 0.6592 1]; % 3rd-order polynomial fit to V vs. Zoom data above (offset subtracted)
potPolyV=[0.0177 -0.2993 2.0131 -1.6936];
potOffset=[0.004 0.007 0.55 0.612];
zoomTol=0.02;
stepsPerV=66.5;
maxTries=3;
stepRate=10; % Hz

switch cmd
  case 'readpos'
    rdbk=polyval(potPoly,lcaGet(rdbkPV)-potOffset(iotr+1));
    if rdbk<1; rdbk=1; end; if rdbk>7; rdbk=7; end;
  case 'stepper'
    desV=polyval(potPolyV,val);
    lcaPut(disablePV,0);
    V=lcaGet(rdbkPV)-potOffset(iotr+1);
    rdbk=polyval(potPoly,V);
    if rdbk<1; rdbk=1; end; if rdbk>7; rdbk=7; end;
    itry=0;t0=[];
    while abs(val-rdbk) > zoomTol
      if (val-rdbk)>0
        lcaPut(stepperDirPV,0);
      else
        lcaPut(stepperDirPV,1);
      end
      itry=itry+1;
      if itry>maxTries; lcaPut(disablePV,1); return; end;
      allowLimit=false;
      if any(strcmp(lcaGet(limitPV),'Low'))
        if strcmp(lcaGet(limitPV{1}),'Low') && strcmp(lcaGet(stepperDirPV),'High') && ~strcmp(lcaGet(limitPV{2}),'Low')
          %           errordlg('Low limit active!','Zoom error');
          lcaPut(disablePV,1);
          return
        elseif strcmp(lcaGet(limitPV{2}),'Low') && strcmp(lcaGet(stepperDirPV),'Low')
          %           errordlg('High limit active!','Zoom error');
          lcaPut(disablePV,1);
          return
        end
        allowLimit=true;
      end
      for istep=1:ceil(abs(V-desV)*stepsPerV)
        lcaPut(stepperPV,double((-1)^istep>0));
        if ~isempty(t0)
          dt=etime(clock,t0);
          if dt<(1/stepRate)
            system(sprintf('sleep %f',1/(stepRate-dt)));
          end
        end
        t0=clock;
        if ~allowLimit && any(strcmp(lcaGet(limitPV),'Low'))
          if strcmp(lcaGet(limitPV{1}),'Low')
            disp('Low Limit Switch Active!')
          else
            disp('High Limit Switch Active!')
          end
          V=lcaGet(rdbkPV)-potOffset(iotr+1);
          rdbk=polyval(potPoly,V);
          if rdbk<1; rdbk=1; end; if rdbk>7; rdbk=7; end;
          lcaPut(disablePV,1);
          return
        end
      end
      if itry<maxTries; system('sleep 1'); end;
      V=lcaGet(rdbkPV)-potOffset(iotr+1);
      rdbk=polyval(potPoly,V);
      if rdbk<1; rdbk=1; end; if rdbk>7; rdbk=7; end;
    end
    lcaPut(disablePV,1);
end

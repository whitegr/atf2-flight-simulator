function stat=twiss_gui_FitDRTunes(xtune,ytune)
%
% stat=twiss_gui_FitDRTunes(xtune,ytune);
%
% Use MAD to rematch DR tunes for extant machine: compute fudge factors for
% BH1R and QF2R, write fudge factors to FL.HwInfo.PS(n).fudge, update initial
% Twiss

global FL BEAMLINE PS
persistent lastUpdate

debug=1;
dofudge=0;

% don't do all this again if we don't have to ...

if (debug)
  lastHwUpdate=-1;
else
  [stat,lastHwUpdate]=FlHwUpdate('get_lastUpdate');
  if (isempty(lastHwUpdate)|| ...
      (~isempty(lastUpdate)&&isequal(lastHwUpdate,lastUpdate)))
    stat=1;
    return
  end
end

% oh well ... we have to

lastUpdate=lastHwUpdate;

% let FS_HOME be the Flight Simulator ATF2 folder
% - working directory is FS_HOME/FlightSim
% - MAD is run from: working directory
% - MAD work area is: ./testApps/twiss_gui/MAD
% - MAD dictionary is: ../mad8.51/mad8.dict
% - MAD executable is: ../mad8.51/<EPICS_HOST_ARCH>/mad8s

% MAD setup

arch=getenv('EPICS_HOST_ARCH');
file1=fullfile('..','mad8.51','mad8.dict');
file2=fullfile('..','mad8.51',arch,'mad8s');
file3=fullfile('testApps','twiss_gui','MAD','ATFdr.mad');
MADcmd=[file2,' < ',file3];
switch arch
  case 'cygwin-x86'
    file2=strcat(file2,'.exe');
    MADdict=['set dict=',file1];
    MADcmd=strcat(MADdict,'&',MADcmd);
  case {'linux-x86','linux-x86_64'}
    MADdict=['ln -s ',file1,' ./dict'];
    MADcmd=strcat(MADdict,';',MADcmd);
  otherwise
    disp(['Sorry ... Flight Simulator MAD not set up for ',arch])
    stat=0;
    return
end
MADpatchFile=fullfile('testApps','twiss_gui','MAD','FSData1.mad');

% get initial fudge factor values

if (dofudge)
  id=findcells(BEAMLINE,'Name','BH1R*');
  ips=BEAMLINE{id(1)}.PS;
  fBH1Ri=FL.HwInfo.PS(ips).fudge;
  id=findcells(BEAMLINE,'Name','QF1R*');
  ips=BEAMLINE{id(1)}.PS;
  fQF1Ri=FL.HwInfo.PS(ips).fudge;
  id=findcells(BEAMLINE,'Name','QF2R*');
  ips=BEAMLINE{id(1)}.PS;
  fQF2Ri=FL.HwInfo.PS(ips).fudge;
else
  fBH1Ri=1;
  fQF1Ri=1;
  fQF2Ri=1;
end

% set up the MAD patch file for DR tune fitting

E0=FL.SimModel.Initial.Momentum;
Cb=1e9/299792458; % rigidity constant (T-m/GeV)
brho=Cb*E0; % rigidity (T-m)

iex=findcells(BEAMLINE,'Name','IEX');
iddr=(1:iex);
idb=intersect(findcells(BEAMLINE,'Class','SBEN'),iddr);
idq=intersect(findcells(BEAMLINE,'Class','QUAD'),iddr);
ids=intersect(findcells(BEAMLINE,'Class','SEXT'),iddr);
idm=intersect(findcells(BEAMLINE,'Name','SQS*'),iddr);

fid=fopen(MADpatchFile,'w');
fprintf(fid,'!\n');
fprintf(fid,'! DR magnet strengths from Flight Simulator (%s)\n',datestr(now));
fprintf(fid,'!\n');
for m=1:length(idb)
  n=idb(m);
  name=BEAMLINE{n}.Name;
  family=name(1:strfind(name,'R'));
  ips=BEAMLINE{n}.PS;
  Ampl=PS(ips).Ampl;
  Ampl=Ampl/fBH1Ri; % unfudge before fitting
  GL=BEAMLINE{n}.B(2)*Ampl;
  L=BEAMLINE{n}.L;
  K1=(GL/L)/brho;
  fprintf(fid,'  %s, K1=f%s*(%s)\n',name,family,madval(K1));
end
fprintf(fid,'!\n');
for m=1:2:length(idq)
  n=idq(m);
  name=BEAMLINE{n}.Name;
  if (strcmp(name,'QM7R1'))
    family='QM7AR';
  else
    family=name(1:strfind(name,'R'));
  end
  ips=BEAMLINE{n}.PS;
  Ampl=PS(ips).Ampl;
  if (strcmp(family,'QF1R'))
    Ampl=Ampl/fQF1Ri; % unfudge before fitting
  elseif (strcmp(family,'QF2R'))
    Ampl=Ampl/fQF2Ri; % unfudge before fitting
  end
  GL=BEAMLINE{n}.B*Ampl;
  L=BEAMLINE{n}.L;
  K1=(GL/L)/brho;
  fprintf(fid,'  %s, K1=f%s*(%s)\n',name,family,madval(K1));
end
fprintf(fid,'!\n');
for m=1:2:length(ids)
  n=ids(m);
  name=BEAMLINE{n}.Name;
  ips=BEAMLINE{n}.PS;
  GL=BEAMLINE{n}.B*PS(ips).Ampl;
  L=BEAMLINE{n}.L;
  K2=(GL/L)/brho;
  fprintf(fid,'  SET, %s[K2], %s\n',name,madval(K2));
end
fprintf(fid,'!\n');
for m=1:length(idm)
  n=idm(m);
  name=BEAMLINE{n}.Name;
  ips=BEAMLINE{n}.PS;
  GL=BEAMLINE{n}.B*PS(ips).Ampl;
  K1L=GL/brho;
  fprintf(fid,'  SET, %s[K1L], %s\n',name,madval(K1L));
end
fprintf(fid,'!\n');
fprintf(fid,'  SET, E0, %s\n',madval(E0));
fprintf(fid,'  SET, XTUNE, %s\n',madval(xtune));
fprintf(fid,'  SET, YTUNE, %s\n',madval(ytune));
fprintf(fid,'!\n');
fprintf(fid,'  MTUNES\n');
fprintf(fid,'!\n');
fprintf(fid,'  RETURN\n');
fclose(fid);

% run MAD

[s,r]=system(MADcmd);
if (~isempty(strmatch('ln -s',MADdict)))
  delete dict % cleanup
end

% get the fitted BH1R, QF2R, and QF1R fudge factors, and the initial Twiss

echoFile=fullfile('testApps','twiss_gui','MAD','ATFdr.echo');
[s,r]=system(['grep "Value of expression \"FBH1R\" is:" ',echoFile]);
ic=strfind(r,':');
fBH1R=str2double(r(ic+1:end));
[s,r]=system(['grep "Value of expression \"FQF1R\" is:" ',echoFile]);
ic=strfind(r,':');
fQF1R=str2double(r(ic+1:end));
[s,r]=system(['grep "Value of expression \"FQF2R\" is:" ',echoFile]);
ic=strfind(r,':');
fQF2R=str2double(r(ic+1:end));

Initial0=FL.SimModel.Initial;
Initial=Initial0;
Initial.x.Twiss.nu=0;
Initial.y.Twiss.nu=0;
[s,r]=system(['grep "Value of expression \"TWSS0\[BETX\]\" is:" ',echoFile]);
ic=strfind(r,':');
Initial.x.Twiss.beta=str2double(r(ic+1:end));
[s,r]=system(['grep "Value of expression \"TWSS0\[ALFX\]\" is:" ',echoFile]);
ic=strfind(r,':');
Initial.x.Twiss.alpha=str2double(r(ic+1:end));
[s,r]=system(['grep "Value of expression \"TWSS0\[DX\]\" is:" ',echoFile]);
ic=strfind(r,':');
Initial.x.Twiss.eta=str2double(r(ic+1:end));
[s,r]=system(['grep "Value of expression \"TWSS0\[DPX\]\" is:" ',echoFile]);
ic=strfind(r,':');
Initial.x.Twiss.etap=str2double(r(ic+1:end));
[s,r]=system(['grep "Value of expression \"TWSS0\[BETY\]\" is:" ',echoFile]);
ic=strfind(r,':');
Initial.y.Twiss.beta=str2double(r(ic+1:end));
[s,r]=system(['grep "Value of expression \"TWSS0\[ALFY\]\" is:" ',echoFile]);
ic=strfind(r,':');
Initial.y.Twiss.alpha=str2double(r(ic+1:end));
[s,r]=system(['grep "Value of expression \"TWSS0\[DY\]\" is:" ',echoFile]);
ic=strfind(r,':');
Initial.y.Twiss.eta=str2double(r(ic+1:end));
[s,r]=system(['grep "Value of expression \"TWSS0\[DPY\]\" is:" ',echoFile]);
ic=strfind(r,':');
Initial.y.Twiss.etap=str2double(r(ic+1:end));

% load the fitted fudge factors into FL.HwInfo.PS and apply the new fudges

if (dofudge)
  idbh1=findcells(BEAMLINE,'Name','BH1R*');
  for m=1:length(idbh1)
    n=idbh1(m);
    BEAMLINE{n}.B(2)=fBH1R*(BEAMLINE{n}.B(2)/fBH1Ri);
    if (rem(n,2)) % load fudge to power supply only once
      ips=BEAMLINE{n}.PS;
      FL.HwInfo.PS(ips).fudge=fBH1R;
    end
  end
  
  idqf1=findcells(BEAMLINE,'Name','QF1R*');
  for m=1:2:length(idqf1) % apply fudge to power supply once
    n=idqf1(m);
    ips=BEAMLINE{n}.PS;
    FL.HwInfo.PS(ips).fudge=fQF1R;
    PS(ips).Ampl=fQF1R*(PS(ips).Ampl/fQF1Ri);
  end
  
  idqf2=findcells(BEAMLINE,'Name','QF2R*');
  for m=1:2:length(idqf2) % apply fudge to power supply once
    n=idqf2(m);
    ips=BEAMLINE{n}.PS;
    FL.HwInfo.PS(ips).fudge=fQF2R;
    PS(ips).Ampl=fQF2R*(PS(ips).Ampl/fQF2Ri);
  end
end

% compute initial beta mismatch

energy0=FL.SimModel.Design.Momentum;
betax0=FL.SimModel.Design.Twiss.betax(1);
alphax0=FL.SimModel.Design.Twiss.alphax(1);
etax0=FL.SimModel.Design.Twiss.etax(1);
etapx0=FL.SimModel.Design.Twiss.etapx(1);
betay0=FL.SimModel.Design.Twiss.betay(1);
alphay0=FL.SimModel.Design.Twiss.alphay(1);
etay0=FL.SimModel.Design.Twiss.etay(1);
etapy0=FL.SimModel.Design.Twiss.etapy(1);
[BmagX,BpsiX]=bmag(betax0,alphax0,Initial.x.Twiss.beta,Initial.x.Twiss.alpha);
[BmagY,BpsiY]=bmag(betay0,alphay0,Initial.y.Twiss.beta,Initial.y.Twiss.alpha);

% display results to command window

disp(' ')
disp(sprintf('   Energy  = %10.4f (%10.4f)',Initial.Momentum,energy0))
disp(' ')
disp(sprintf('   Xtune   = %10.4f (   15.18  )',xtune))
disp(sprintf('   Ytune   = %10.4f (    8.57  )',ytune))
disp(' ')
disp(sprintf('   fBH1R   = %10.4f (    1.0   )',fBH1R))
disp(sprintf('   fQF1R   = %10.4f (    1.0   )',fQF1R))
disp(sprintf('   fQF2R   = %10.4f (    1.0   )',fQF2R))
disp(' ')
disp(sprintf('   BetaXi  = %10.4f (%10.4f)',Initial.x.Twiss.beta,betax0))
disp(sprintf('   AplhaXi = %10.4f (%10.4f)',Initial.x.Twiss.alpha,alphax0))
disp(sprintf('   EtaXi   = %10.4f (%10.4f)',Initial.x.Twiss.eta,etax0))
disp(sprintf('   EtaPXi  = %10.4f (%10.4f)',Initial.x.Twiss.etap,etapx0))
disp(' ')
disp(sprintf('   BetaYi  = %10.4f (%10.4f)',Initial.y.Twiss.beta,betay0))
disp(sprintf('   AplhaYi = %10.4f (%10.4f)',Initial.y.Twiss.alpha,alphay0))
disp(sprintf('   EtaYi   = %10.4f (%10.4f)',Initial.y.Twiss.eta,etay0))
disp(sprintf('   EtaPYi  = %10.4f (%10.4f)',Initial.y.Twiss.etap,etapy0))
disp(' ')
disp(sprintf('   BmagXi  = %10.4f (    1.0   )',BmagX))
disp(sprintf('   BmagYi  = %10.4f (    1.0   )',BmagY))
disp(' ')

% set up a MAD patch file for ATF2 ... get Twiss @ IEX

twssx=twiss_gui_ATF2PatchFile(echoFile);

% load the matched Twiss into FL.SimModel (include Initial, Initial_EXT,
% and Initial_BEGFF)

[iss,Twiss]=GetTwiss(1,length(BEAMLINE),Initial.x.Twiss,Initial.y.Twiss);
FL.SimModel.Twiss=Twiss;
FL.SimModel.Initial=Initial;
id=FL.SimModel.extStart;
FL.SimModel.Initial_IEX.Momentum=E0;
FL.SimModel.Initial_IEX.x.Twiss.nu=twssx(1);
FL.SimModel.Initial_IEX.x.Twiss.beta=twssx(2);
FL.SimModel.Initial_IEX.x.Twiss.alpha=twssx(3);
FL.SimModel.Initial_IEX.x.Twiss.eta=twssx(4);
FL.SimModel.Initial_IEX.x.Twiss.etap=twssx(5);
FL.SimModel.Initial_IEX.y.Twiss.nu=twssx(6);
FL.SimModel.Initial_IEX.y.Twiss.beta=twssx(7);
FL.SimModel.Initial_IEX.y.Twiss.alpha=twssx(8);
FL.SimModel.Initial_IEX.y.Twiss.eta=twssx(9);
FL.SimModel.Initial_IEX.y.Twiss.etap=twssx(10);
id=FL.SimModel.ffStart.ind;
FL.SimModel.Initial_BEGFF.x.Twiss.nu=Twiss.nux(id);
FL.SimModel.Initial_BEGFF.x.Twiss.beta=Twiss.betax(id);
FL.SimModel.Initial_BEGFF.x.Twiss.alpha=Twiss.alphax(id);
FL.SimModel.Initial_BEGFF.x.Twiss.eta=Twiss.etax(id);
FL.SimModel.Initial_BEGFF.x.Twiss.etap=Twiss.etapx(id);
FL.SimModel.Initial_BEGFF.y.Twiss.nu=Twiss.nuy(id);
FL.SimModel.Initial_BEGFF.y.Twiss.beta=Twiss.betay(id);
FL.SimModel.Initial_BEGFF.y.Twiss.alpha=Twiss.alphay(id);
FL.SimModel.Initial_BEGFF.y.Twiss.eta=Twiss.etay(id);
FL.SimModel.Initial_BEGFF.y.Twiss.etap=Twiss.etapy(id);

stat=1;

end

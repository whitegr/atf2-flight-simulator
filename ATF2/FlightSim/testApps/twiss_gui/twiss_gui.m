function varargout = twiss_gui(varargin)
% TWISS_GUI M-file for twiss_gui.fig
%      TWISS_GUI, by itself, creates a new TWISS_GUI or raises the existing
%      singleton*.
%
%      H = TWISS_GUI returns the handle to a new TWISS_GUI or the handle to
%      the existing singleton*.
%
%      TWISS_GUI('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in TWISS_GUI.M with the given input arguments.
%
%      TWISS_GUI('Property','Value',...) creates a new TWISS_GUI or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before twiss_gui_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to twiss_gui_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help twiss_gui

% Last Modified by GUIDE v2.5 10-Nov-2009 11:31:10

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @twiss_gui_OpeningFcn, ...
                   'gui_OutputFcn',  @twiss_gui_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before twiss_gui is made visible.
function twiss_gui_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to twiss_gui (see VARARGIN)

% Choose default command line output for twiss_gui
handles.output=handles;

% Update handles structure
guidata(hObject,handles);

% UIWAIT makes twiss_gui wait for user response (see UIRESUME)
% uiwait(handles.figure1);

global GUIDATA BEAMLINE

GUIDATA.iexind=findcells(BEAMLINE,'Name','IEX');
GUIDATA.iexind=GUIDATA.iexind(end);
GUIDATA.begffind=findcells(BEAMLINE,'Name','BEGFF');
GUIDATA.endffind=findcells(BEAMLINE,'Name','IP');
GUIDATA.htune=15.18; % atfdr-design-20080526
GUIDATA.vtune=8.57; % atfdr-design-20080526

set(handles.popupmenu1,'Value',4);
set(handles.popupmenu2,'Value',1);
set(handles.popupmenu3,'Value',3);
calc_n_plot(handles)


% --- Outputs from this function are returned to the command line.
function varargout = twiss_gui_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1}=handles.output;


% --- Executes on button press in pushbutton1.
function pushbutton1_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

calc_n_plot(handles)


% --- Executes on selection change in popupmenu1.
function popupmenu1_Callback(hObject, eventdata, handles)
% hObject    handle to popupmenu1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = get(hObject,'String') returns popupmenu1 contents as cell array
%        contents{get(hObject,'Value')} returns selected item from popupmenu1

calc_n_plot(handles)


% --- Executes during object creation, after setting all properties.
function popupmenu1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to popupmenu1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc&&isequal(get(hObject,'BackgroundColor'),get(0,'defaultUicontrolBackgroundColor'))
  set(hObject,'BackgroundColor','white')
end


function calc_n_plot(handles)

global BEAMLINE PS FL GUIDATA

choice=get(handles.popupmenu1,'Value');
choice_twiss_plot1=get(handles.popupmenu2,'Value');
choice_twiss_plot2=get(handles.popupmenu3,'Value');

zerocorrs=1; % turn off XCORs and YCORs
zeroskews=0; % turn off DR skew quadrupoles (sextupole trims)
fittunes=1;  % match DR tunes; get periodic initial conditions

if (zerocorrs)
  idh=findcells(BEAMLINE,'Class','XCOR');
  idv=findcells(BEAMLINE,'Class','YCOR');
  id=[idh,idv];
  for m=1:length(id)
    n=id(m);
    ips=BEAMLINE{n}.PS;
    PS(ips).Ampl=0;
  end
end
if (zeroskews)
  idm=findcells(BEAMLINE,'Name','SQS*');
  for m=1:length(idm)
    n=idm(m);
    ips=BEAMLINE{n}.PS;
    PS(ips).Ampl=0;
  end
end
if (fittunes)
  stat=twiss_gui_FitDRTunes(GUIDATA.htune,GUIDATA.vtune);
end

[stat,FL.SimModel.Twiss]=GetTwiss(1,length(BEAMLINE), ...
  FL.SimModel.Initial.x.Twiss,FL.SimModel.Initial.y.Twiss);
if (stat{1}~=1),error(stat{2}),end

T=FL.SimModel.Twiss;

switch choice
  case 1 % Damping Ring only
    startind=1;
    endind = GUIDATA.iexind;
  case 2 % EXT only
    startind=GUIDATA.iexind;
    endind=GUIDATA.begffind;
  case 3 % Final Focus only
    startind=GUIDATA.begffind;
    endind=GUIDATA.endffind;
  case 4 % EXT & Final Focus
    startind=GUIDATA.iexind;
    endind=GUIDATA.endffind;
  case 5 % All
    startind=1;
    endind=GUIDATA.endffind;
end
ind=(startind:endind);

axes(handles.axes1)
switch choice_twiss_plot1
  case 1 % beta
    plot(T.S(ind),T.betax(ind),'-kx',T.S(ind),T.betay(ind),'-ro')
    ylabel('Beta / m')
  case 2 % alpha
    plot(T.S(ind),T.alphax(ind),'-kx',T.S(ind),T.alphay(ind),'-ro')
    ylabel('Alpha')
  case 3 % eta
    plot(T.S(ind),T.etax(ind),'-kx',T.S(ind),T.etay(ind),'-ro')
    ylabel('Dispertion / m')
  case 4 % nu
    plot(T.S(ind),mod(T.nux(ind)-T.nux(GUIDATA.endffind),1),'-kx',T.S(ind),mod(T.nuy(ind)-T.nuy(GUIDATA.endffind),1),'-ro')
    ylabel('Phase [rad/2pi]')
end
xlim([T.S(startind),T.S(endind)])
%legend('x','y')

axes(handles.axes2)
switch choice_twiss_plot2
  case 1 % beta
    plot(T.S(ind),T.betax(ind),'-kx',T.S(ind),T.betay(ind),'-ro')
    ylabel('Beta / m')
  case 2 % alpha
    plot(T.S(ind),T.alphax(ind),'-kx',T.S(ind),T.alphay(ind),'-ro')
    ylabel('Alpha')
  case 3 % eta
    plot(T.S(ind),T.etax(ind),'-kx',T.S(ind),T.etay(ind),'-ro')
    ylabel('Dispertion / m')
  case 4 % nu
    plot(T.S(ind),mod(T.nux(ind)-T.nux(GUIDATA.endffind),1),'-kx',T.S(ind),mod(T.nuy(ind)-T.nuy(GUIDATA.endffind),1),'-ro')
    ylabel('Phase [rad/2pi]')
end
xlim([T.S(startind) T.S(endind)])
xlabel('S / m')
%legend('x','y')

plot_magnets_external(BEAMLINE(startind:endind),handles.axes3,T.S(startind),T.S(endind),1,0);


% --- Executes on selection change in popupmenu3.
function popupmenu3_Callback(hObject, eventdata, handles)
% hObject    handle to popupmenu3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = get(hObject,'String') returns popupmenu3 contents as cell array
%        contents{get(hObject,'Value')} returns selected item from popupmenu3

calc_n_plot(handles)


% --- Executes during object creation, after setting all properties.
function popupmenu3_CreateFcn(hObject, eventdata, handles)
% hObject    handle to popupmenu3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc&&isequal(get(hObject,'BackgroundColor'),get(0,'defaultUicontrolBackgroundColor'))
  set(hObject,'BackgroundColor','white')
end


% --- Executes on selection change in popupmenu2.
function popupmenu2_Callback(hObject, eventdata, handles)
% hObject    handle to popupmenu2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = get(hObject,'String') returns popupmenu2 contents as cell array
%        contents{get(hObject,'Value')} returns selected item from popupmenu2

calc_n_plot(handles)


% --- Executes during object creation, after setting all properties.
function popupmenu2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to popupmenu2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc&&isequal(get(hObject,'BackgroundColor'),get(0,'defaultUicontrolBackgroundColor'))
  set(hObject,'BackgroundColor','white')
end


function htune_Callback(hObject, eventdata, handles)
% hObject    handle to htune (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

global GUIDATA
GUIDATA.htune=str2double(get(hObject,'String'));


% --- Executes during object creation, after setting all properties.
function htune_CreateFcn(hObject, eventdata, handles)
% hObject    handle to htune (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called
% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc&&isequal(get(hObject,'BackgroundColor'),get(0,'defaultUicontrolBackgroundColor'))
  set(hObject,'BackgroundColor','white')
end


function vtune_Callback(hObject, eventdata, handles)
% hObject    handle to vtune (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

global GUIDATA
GUIDATA.vtune=str2double(get(hObject,'String'));


% --- Executes during object creation, after setting all properties.
function vtune_CreateFcn(hObject, eventdata, handles)
% hObject    handle to vtune (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called
% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc&&isequal(get(hObject,'BackgroundColor'),get(0,'defaultUicontrolBackgroundColor'))
  set(hObject,'BackgroundColor','white');
end

function twssx=twiss_gui_ATF2PatchFile(echoFile)

global FL BEAMLINE PS

% get initial Twiss (@IEX) from DR echo-file (get as strings)

twss0=cell(10,1);
[s,r]=system(['grep "Value of expression \"TWSSX\[MUX\]\" is:" ',echoFile]);
twss0{1}=r(strfind(r,':')+1:end-1);
[s,r]=system(['grep "Value of expression \"TWSSX\[BETX\]\" is:" ',echoFile]);
twss0{2}=r(strfind(r,':')+1:end-1);
[s,r]=system(['grep "Value of expression \"TWSSX\[ALFX\]\" is:" ',echoFile]);
twss0{3}=r(strfind(r,':')+1:end-1);
[s,r]=system(['grep "Value of expression \"TWSSX\[DX\]\" is:" ',echoFile]);
twss0{4}=r(strfind(r,':')+1:end-1);
[s,r]=system(['grep "Value of expression \"TWSSX\[DPX\]\" is:" ',echoFile]);
twss0{5}=r(strfind(r,':')+1:end-1);
[s,r]=system(['grep "Value of expression \"TWSSX\[MUY\]\" is:" ',echoFile]);
twss0{6}=r(strfind(r,':')+1:end-1);
[s,r]=system(['grep "Value of expression \"TWSSX\[BETY\]\" is:" ',echoFile]);
twss0{7}=r(strfind(r,':')+1:end-1);
[s,r]=system(['grep "Value of expression \"TWSSX\[ALFY\]\" is:" ',echoFile]);
twss0{8}=r(strfind(r,':')+1:end-1);
[s,r]=system(['grep "Value of expression \"TWSSX\[DY\]\" is:" ',echoFile]);
twss0{9}=r(strfind(r,':')+1:end-1);
[s,r]=system(['grep "Value of expression \"TWSSX\[DPY\]\" is:" ',echoFile]);
twss0{10}=r(strfind(r,':')+1:end-1);

% set up the MAD patch file for ATF2 (EXT+FF+POST)

E0=FL.SimModel.Initial.Momentum;
Cb=1e9/299792458; % rigidity constant (T-m/GeV)
brho=Cb*E0; % rigidity (T-m)

iex=findcells(BEAMLINE,'Name','IEX');
idatf2=(iex:length(BEAMLINE));
idb=intersect(findcells(BEAMLINE,'Class','SBEN'),findcells(BEAMLINE,'Name','Q*'));
idq=intersect(findcells(BEAMLINE,'Class','QUAD'),idatf2);
ids=intersect(findcells(BEAMLINE,'Class','SEXT'),idatf2);

MADpatchFile=fullfile('testApps','twiss_gui','MAD','FSData2.mad');
fid=fopen(MADpatchFile,'w');

fprintf(fid,'!\n');
fprintf(fid,'! ATF2 magnet strengths from Flight Simulator (%s)\n',datestr(now));
fprintf(fid,'!\n');
for m=1:2:length(idb)
  n=idb(m);
  name=BEAMLINE{n}.Name;
  ips=BEAMLINE{n}.PS;
  Ampl=PS(ips).Ampl;
  GL=BEAMLINE{n}.B(2)*Ampl;
  L=BEAMLINE{n}.L;
  K1=(GL/L)/brho;
  fprintf(fid,'  SET, %s[K1], %s\n',name,madval(K1));
end
fprintf(fid,'!\n');
for m=1:2:length(idq)
  n=idq(m);
  name=BEAMLINE{n}.Name;
  ips=BEAMLINE{n}.PS;
  Ampl=PS(ips).Ampl;
  GL=BEAMLINE{n}.B*Ampl;
  L=BEAMLINE{n}.L;
  K1=(GL/L)/brho;
  fprintf(fid,'  SET, %s[K1], %s\n',name,madval(K1));
end
fprintf(fid,'!\n');
for m=1:2:length(ids)
  n=ids(m);
  name=BEAMLINE{n}.Name;
  ips=BEAMLINE{n}.PS;
  GL=BEAMLINE{n}.B*PS(ips).Ampl;
  L=BEAMLINE{n}.L;
  K2=(GL/L)/brho;
  fprintf(fid,'  SET, %s[K2], %s\n',name,madval(K2));
end
fprintf(fid,'!\n');
fprintf(fid,'  SET, E0, %s\n',madval(E0));
fprintf(fid,'  SET, TMUX, %s\n',twss0{1});
fprintf(fid,'  SET, TBETX, %s\n',twss0{2});
fprintf(fid,'  SET, TALFX, %s\n',twss0{3});
fprintf(fid,'  SET, TDX, %s\n',twss0{4});
fprintf(fid,'  SET, TDPX, %s\n',twss0{5});
fprintf(fid,'  SET, TMUY, %s\n',twss0{6});
fprintf(fid,'  SET, TBETY, %s\n',twss0{7});
fprintf(fid,'  SET, TALFY, %s\n',twss0{8});
fprintf(fid,'  SET, TDY, %s\n',twss0{9});
fprintf(fid,'  SET, TDPY, %s\n',twss0{10});
fprintf(fid,'!\n');
fprintf(fid,'  RETURN\n');

fclose(fid);

twssx=str2double(twss0);

end

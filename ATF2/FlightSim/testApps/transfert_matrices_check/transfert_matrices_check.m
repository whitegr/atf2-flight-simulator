function varargout = transfert_matrices_check(varargin)
% TRANSFERT_MATRICES_CHECK M-file for transfert_matrices_check.fig
%      TRANSFERT_MATRICES_CHECK, by itself, creates a new TRANSFERT_MATRICES_CHECK or raises the existing
%      singleton*.
%
%      H = TRANSFERT_MATRICES_CHECK returns the handle to a new TRANSFERT_MATRICES_CHECK or the handle to
%      the existing singleton*.
%
%      TRANSFERT_MATRICES_CHECK('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in TRANSFERT_MATRICES_CHECK.M with the given input arguments.
%
%      TRANSFERT_MATRICES_CHECK('Property','Value',...) creates a new TRANSFERT_MATRICES_CHECK or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before transfert_matrices_check_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to transfert_matrices_check_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help transfert_matrices_check

% Last Modified by GUIDE v2.5 14-Nov-2011 15:24:05

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
  'gui_Singleton',  gui_Singleton, ...
  'gui_OpeningFcn', @transfert_matrices_check_OpeningFcn, ...
  'gui_OutputFcn',  @transfert_matrices_check_OutputFcn, ...
  'gui_LayoutFcn',  [] , ...
  'gui_Callback',   []);
if nargin && ischar(varargin{1})
  gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
  [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
  gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


function handles=initialize(handles)
% Update handles structure
global BEAMLINE
global INSTR
global PS
global GIRDER

%get bpm info
handles.IEX=findcells(BEAMLINE,'Name','IEX');
handles.BS1XA=findcells(BEAMLINE,'Name','BS1XA');
handles.bpm=setdiff(findcells(BEAMLINE,'Class','MONI'),findcells(BEAMLINE,'Name','FONT*')); % all MONIs except FONT pickups ...
handles.bpm=handles.bpm(handles.bpm>handles.IEX); % ... downstream of IEX
handles.bpm_name=cellfun(@(x) x.Name,BEAMLINE(handles.bpm),'UniformOutput',false);
handles.bpm_s=cellfun(@(x) x.S,BEAMLINE(handles.bpm),'UniformOutput',true);
handles.bpm_index=zeros(size(handles.bpm));
for n=1:length(handles.bpm)
  handles.bpm_index(n)=findcells(INSTR,'Index',handles.bpm(n));
end
handles.bpm_type=cellfun(@(x) x.Type,INSTR(handles.bpm_index),'UniformOutput',false);
%buttons
handles.button=strmatch('button',handles.bpm_type);
handles.nbutton=length(handles.button);
handles.button_name=handles.bpm_name(handles.button);
handles.button_index=handles.bpm_index(handles.button);
%striplines
handles.stripline=strmatch('stripline',handles.bpm_type);
handles.nstripline=length(handles.stripline);
handles.stripline_name=handles.bpm_name(handles.stripline);
handles.stripline_index=handles.bpm_index(handles.stripline);
%cbands
handles.cband=strmatch('ccav',handles.bpm_type);
handles.ncband=length(handles.cband);
handles.cband_name=handles.bpm_name(handles.cband);
handles.cband_index=handles.bpm_index(handles.cband);
%sbands
handles.sband=strmatch('scav',handles.bpm_type);
handles.nsband=length(handles.sband);
handles.sband_name=handles.bpm_name(handles.sband);
handles.sband_index=handles.bpm_index(handles.sband);

%get ICT info
handles.ICT=findcells(BEAMLINE,'Name','ICT1X');
handles.ICT_index=findcells(INSTR,'Index',handles.ICT);

%get correctors info
xcor_temp=[findcells(BEAMLINE, 'Name','ZS*X') findcells(BEAMLINE, 'Name','ZH*X') findcells(BEAMLINE, 'Name','ZH*FF')];
handles.xcor_name='';
nxcor=length(xcor_temp);
handles.xcor=[];
for i=1:nxcor
  if (strcmp(BEAMLINE{xcor_temp(i)}.Class, 'XCOR') && xcor_temp(i)>handles.BS1XA)
    handles.xcor(end+1)=xcor_temp(i);
  end
end
handles.nxcor=length(handles.xcor);
handles.xcor_ps=zeros(1,handles.nxcor);
handles.xcor_ps_read=zeros(1,handles.nxcor);
handles.xcor_B=zeros(1,handles.nxcor);
handles.xcor_S=zeros(1,handles.nxcor);
for i=1:handles.nxcor
  handles.xcor_ps(i)=BEAMLINE{handles.xcor(i)}.PS;
  handles.xcor_B(i)=BEAMLINE{handles.xcor(i)}.B;
  handles.xcor_S(i)=BEAMLINE{handles.xcor(i)}.S;
  handles.xcor_name=strvcat(handles.xcor_name, BEAMLINE{handles.xcor(i)}.Name);
end
handles.xcor_S=handles.xcor_S-BEAMLINE{handles.IEX}.S;
FlHwUpdate();
for i=1:handles.nxcor
  handles.xcor_ps_read(i)=PS(handles.xcor_ps(i)).Ampl;
end

ycor_temp=[findcells(BEAMLINE, 'Name','ZV*X') findcells(BEAMLINE, 'Name','ZV*FF')];
handles.ycor_name='';
nycor=length(ycor_temp);
handles.ycor=[];
for i=1:nycor
  if (strcmp(BEAMLINE{ycor_temp(i)}.Class, 'YCOR') && ycor_temp(i)>handles.BS1XA)
    handles.ycor(end+1)=ycor_temp(i);
  end
end
handles.nycor=length(handles.ycor);
handles.ycor_ps=zeros(1,handles.nycor);
handles.ycor_ps_read=zeros(1,handles.nycor);
handles.ycor_B=zeros(1,handles.nycor);
handles.ycor_S=zeros(1,handles.nycor);
for i=1:handles.nycor
  handles.ycor_ps(i)=BEAMLINE{handles.ycor(i)}.PS;
  handles.ycor_B(i)=BEAMLINE{handles.ycor(i)}.B;
  handles.ycor_S(i)=BEAMLINE{handles.ycor(i)}.S;
  handles.ycor_name=strvcat(handles.ycor_name, BEAMLINE{handles.ycor(i)}.Name);
end
handles.ycor_S=handles.ycor_S-BEAMLINE{handles.IEX}.S;
FlHwUpdate();
for i=1:handles.nycor
  handles.ycor_ps_read(i)=PS(handles.ycor_ps(i)).Ampl;
end

handles.plot_corr_scale=false;

%get movers info
handles.mover=28:49;
handles.nmover=length(handles.mover);
handles.mover_name='';
handles.mover_pos=zeros(handles.nmover,3);
handles.mover_S=zeros(handles.nmover,1);
handles.xmover_KL=zeros(1,handles.nmover);
handles.ymover_KL=zeros(1,handles.nmover);

FlHwUpdate();
for i=1:handles.nmover
  elems=findcells(BEAMLINE,'Girder',handles.mover(i));
  if(strcmp(BEAMLINE{elems(1)}.Class,'MONI'))
    elems(1)=[];
  end
  handles.mover_S(i)=BEAMLINE{elems(1)}.S-BEAMLINE{handles.IEX}.S;
  handles.mover_name=strvcat(handles.mover_name,['A' BEAMLINE{elems(1)}.Name(1:strfind(BEAMLINE{elems(1)}.Name,'MULT')-1)]);
  handles.elem_on_mover=findcells(BEAMLINE,'Girder',handles.mover(i));
  handles.q_on_mover(i,:)=findcells(BEAMLINE,'Class','QUAD',handles.elem_on_mover(1),handles.elem_on_mover(end));
  handles.bpm_on_mover(i)=findcells(BEAMLINE,'Class','MONI',handles.elem_on_mover(1),handles.elem_on_mover(end));
  for quad=handles.q_on_mover(i,:)
    handles.xmover_KL(i)=handles.xmover_KL(i)+BEAMLINE{quad}.B*PS(BEAMLINE{quad}.PS).Ampl*0.2998/BEAMLINE{quad}.P;
    handles.ymover_KL(i)=handles.ymover_KL(i)+BEAMLINE{quad}.B*PS(BEAMLINE{quad}.PS).Ampl*0.2998/BEAMLINE{quad}.P;
  end
  handles.mover_pos(i,:)=GIRDER{handles.mover(i)}.MoverPos;
end
handles.mover_S=handles.mover_S-BEAMLINE{handles.IEX}.S;
handles.svd_jitter_subtraction=true;



% --- Executes just before transfert_matrices_check is made visible.
function transfert_matrices_check_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to orbit_steering (see VARARGIN)

% Choose default command line output for orbit_steering
handles.out='';
handles=display_output(handles,'loading ...');

global BEAMLINE

handles=initialize(handles);

IEX=handles.IEX;IEX_s=BEAMLINE{IEX}.S;
IP=findcells(BEAMLINE,'Name','IP');IP_s=BEAMLINE{IP}.S;
EOL=length(BEAMLINE);EOL_s=BEAMLINE{EOL}.S;
bpm=handles.bpm; % include all BPMs
bpm_s=handles.bpm_s;
bpm_name=handles.bpm_name;
handles.xlim=[min([bpm_s;IEX_s]),max([bpm_s;EOL_s])]-IEX_s;
axes(handles.plot)
xlim(handles.xlim+IEX_s)
[xt,id]=sort(bpm_s);
xtl=bpm_name;xtl=xtl(id);
set(gca,'XTick',xt,'XTickLabel',xtl)
grid
handles.hText=xticklabel_rotate90();
plot_magnets_external(BEAMLINE,handles.lattice,handles.xlim(1)+IEX_s,handles.xlim(2)+IEX_s);

handles.auto_show=1;
handles.cor_alone=0;
handles.mover_alone=0;
handles.output = hObject;
handles.list_xcor_scanned=[];
handles.list_ycor_scanned=[];
handles.list_xmover_scanned=[];
handles.list_ymover_scanned=[];
handles=display_output(handles,'done');
guidata(hObject, handles);


% UIWAIT makes transfert_matrices_check wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = transfert_matrices_check_OutputFcn(hObject, eventdata, handles)
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on button press in run_check.
function run_check_Callback(hObject, eventdata, handles)
global BEAMLINE
global INSTR
global PS
global GIRDER
% hObject    handle to run_check (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

set(handles.stop_btn,'BackgroundColor',[0.702 0.702 0.702]);

handles.xread_dx=[];
handles.yread_dx=[];
handles.iread_dx=[];
handles.dxread_dx=[];
handles.dyread_dx=[];
handles.diread_dx=[];
handles.rawread_dx=[];

handles.xread_dy=[];
handles.yread_dy=[];
handles.iread_dy=[];
handles.dxread_dy=[];
handles.dyread_dy=[];
handles.diread_dy=[];
handles.rawread_dy=[];

handles.xread_dposx=[];
handles.yread_dposx=[];
handles.iread_dposx=[];
handles.dxread_dposx=[];
handles.dyread_dposx=[];
handles.diread_dposx=[];
handles.rawread_dposx=[];

handles.xread_dposy=[];
handles.yread_dposy=[];
handles.iread_dposy=[];
handles.dxread_dposy=[];
handles.dyread_dposy=[];
handles.diread_dposy=[];
handles.rawread_dposy=[];

nbpm=length(handles.bpm);

if(handles.svd_jitter_subtraction)
    handles=display_output(handles,'Measuring SVD matrix for jitter substraction');
    FlHwUpdate('wait',100);
    handles=display_output(handles,'BPM measured for SVD');
    [s,raw]=FlHwUpdate('readbuffer',100);
    read_svd=[raw(:,handles.bpm_index*3-1) raw(:,handles.bpm_index*3)];
end

handles=display_output(handles,'start measuring transfert matrix');

[stat reqID] = AccessRequest({unique(handles.mover([handles.list_xmover_scanned handles.list_ymover_scanned])) [handles.xcor_ps(handles.list_xcor_scanned) handles.ycor_ps(handles.list_ycor_scanned)] []});
if(stat{1}==-1)
  handles=display_output(handles,sprintf('AccessRequest reurned error :%s',stat{2}));
else
  handles=display_output(handles,sprintf('Access granted, ID of request : %d',reqID));
end

for xcor_scanned=handles.list_xcor_scanned
  for xcor_dstrength=handles.list_xcor_dstrength
    ps_newvalue=handles.xcor_ps_read(xcor_scanned)+xcor_dstrength;
    handles=display_output(handles,sprintf('set %s (PS %i) at %f mrad',handles.xcor_name(xcor_scanned,:),handles.xcor_ps(xcor_scanned),ps_newvalue*1e3));
    PS(handles.xcor_ps(xcor_scanned)).SetPt=ps_newvalue;
    stat=PSTrim(handles.xcor_ps(xcor_scanned), true); pause(2);
    if(stat{1}==-1)
      handles=display_output(sprintf('PSTrim returned error :%s',stat{2}));
    end
    
    FlHwUpdate();
    
    %if more than 5% difference, ask again
    itteration=0;
    while( abs((PS(handles.xcor_ps(xcor_scanned)).Ampl-ps_newvalue)./ps_newvalue)>=.05 && abs(PS(handles.xcor_ps(xcor_scanned)).Ampl-ps_newvalue)>1e-5)
      itteration=itteration+1;
      %give up after 5 asks
      if(itteration>5)
        handles=display_output(handles,sprintf('Warning : PS %i is set at %f instead of %f.',handles.xcor_ps(xcor_scanned),PS(handles.xcor_ps(xcor_scanned)).Ampl,ps_newvalue));
        break;
      end
      
      %ask to change different ps from what is wanted
      stat=PSTrim(handles.xcor_ps(xcor_scanned), true);
      if(stat{1}==-1)
        handles=display_output(handles,sprintf('PSTrim reurned error :%s',stat{2}));
      end
      pause(2);
      
      %update ps values
      FlHwUpdate();
    end
    
    FlHwUpdate('wait',handles.bpmave+3);
    handles=display_output(handles,'BPM measured');
    %        [s,output]=FlHwUpdate('bpmave',10);
    [s,raw]=FlHwUpdate('readbuffer',handles.bpmave);
    if(handles.svd_jitter_subtraction==true)
        read_svd_sub=remove_jitter_svd_auto(handles,raw,read_svd,handles.xcor(xcor_scanned));
        handles.xread_dx(end+1,:)=mean(read_svd_sub(:,1:nbpm));
        handles.yread_dx(end+1,:)=mean(read_svd_sub(:,nbpm+1:2*nbpm));
%         handles.iread_dx(end+1,:)=output{1}(3,handles.bpm_index);
        handles.dxread_dx(end+1,:)=std(read_svd_sub(:,1:nbpm));
        handles.dyread_dx(end+1,:)=std(read_svd_sub(:,nbpm+1:2*nbpm));
%         handles.diread_dx(end+1,:)=output{1}(6,handles.bpm_index);
        handles.rawread_dx(end+1:end+handles.bpmave,:)=raw;        
    else
        [s,output]=bpmave(raw,handles.bpmave,[]);
        %output = {[meanData.h; meanData.v; meanData.t; rmsData.h; rmsData.v; rmsData.t; nbad.h; nbad.v; nbad.t] badbpms};

        handles.xread_dx(end+1,:)=output{1}(1,handles.bpm_index);
        handles.yread_dx(end+1,:)=output{1}(2,handles.bpm_index);
        handles.iread_dx(end+1,:)=output{1}(3,handles.bpm_index);
        handles.dxread_dx(end+1,:)=output{1}(4,handles.bpm_index);
        handles.dyread_dx(end+1,:)=output{1}(5,handles.bpm_index);
        handles.diread_dx(end+1,:)=output{1}(6,handles.bpm_index);
        handles.rawread_dx(end+1:end+handles.bpmave,:)=raw;
    end
  end
  %reset corector to initial value
  handles=display_output(handles,sprintf('set %s (PS %i) back at %f mrad',handles.xcor_name(xcor_scanned,:),handles.xcor_ps(xcor_scanned),handles.xcor_ps_read(xcor_scanned)*1e3));
  PS(handles.xcor_ps(xcor_scanned)).SetPt=handles.xcor_ps_read(xcor_scanned);
  stat=PSTrim(handles.xcor_ps(xcor_scanned), true);
  if(stat{1}==-1)
    handles=display_output(handles,sprintf('PSTrim returned error :%s',stat{2}));
  end
  while( abs((PS(handles.xcor_ps(xcor_scanned)).Ampl-handles.xcor_ps_read(xcor_scanned))./handles.xcor_ps_read(xcor_scanned))>=.05 && abs(PS(handles.xcor_ps(xcor_scanned)).Ampl-handles.xcor_ps_read(xcor_scanned))>1e-5)
    itteration=itteration+1;
    %give up after 5 asks
    if(itteration>5)
      handles=display_output(handles,sprintf('Warning : PS %i is set at %f instead of %f.',handles.xcor_ps(xcor_scanned),PS(handles.xcor_ps(xcor_scanned)).Ampl,handles.xcor_ps_read(xcor_scanned)));
      break;
    end
    
    %ask to change different ps from what is wanted
    stat=PSTrim(handles.xcor_ps(xcor_scanned), true);
    if(stat{1}==-1)
      handles=display_output(handles,sprintf('PSTrim reurned error :%s',stat{2}));
    end
    pause(2);
    
    %update ps values
    FlHwUpdate();
  end
  pause(0.2)
  if(all(get(handles.stop_btn,'BackgroundColor')==[1 0 0]))
      set(handles.stop_btn,'BackgroundColor',[0.702 0.702 0.702]);
      last_scanned=find(xcor_scanned==handles.list_xcor_scanned);
      handles.list_xcor_scanned=handles.list_xcor_scanned(1:last_scanned);
      handles.list_ycor_scanned=[];
      handles.list_xmover_scanned=[];
      handles.list_ymover_scanned=[];
      break
  end
end

for ycor_scanned=handles.list_ycor_scanned
  for ycor_dstrength=handles.list_ycor_dstrength
    ps_newvalue=handles.ycor_ps_read(ycor_scanned)+ycor_dstrength;
    handles=display_output(handles,sprintf('set %s (PS %i) at %f mrad',handles.ycor_name(ycor_scanned,:),handles.ycor_ps(ycor_scanned),ps_newvalue*1e3));
    PS(handles.ycor_ps(ycor_scanned)).SetPt=ps_newvalue;
    stat=PSTrim(handles.ycor_ps(ycor_scanned), true);
    if(stat{1}==-1)
      handles=display_output(handles,sprintf('PSTrim returned error :%s',stat{2}));
    end
    pause(2);
    FlHwUpdate();
    
    %if more than 5% difference, ask again
    itteration=0;
    while( abs((PS(handles.ycor_ps(ycor_scanned)).Ampl-ps_newvalue)./ps_newvalue)>=.05 && abs(PS(handles.ycor_ps(ycor_scanned)).Ampl-ps_newvalue)>1e-5)
      itteration=itteration+1;
      %give up after 5 asks
      if(itteration>5)
        handles=display_output(handles,sprintf('Warning : PS %i is set at %f instead of %f.',handles.ycor_ps(ycor_scanned),PS(handles.ycor_ps(ycor_scanned)).Ampl,ps_newvalue));
        break;
      end
      
      %ask to change different ps from what is wanted
      stat=PSTrim(handles.ycor_ps(ycor_scanned), true);
      if(stat{1}==-1)
        handles=display_output(handles,sprintf('PSTrim reurned error :%s',stat{2}));
      end
      pause(2);
      
      %update ps values
      FlHwUpdate();
    end
    
    FlHwUpdate('wait',handles.bpmave+3);
    handles=display_output(handles,'BPM measured');
    %        [s,output]=FlHwUpdate('bpmave',10);
    [s,raw]=FlHwUpdate('readbuffer',handles.bpmave);
    if(handles.svd_jitter_subtraction==true)
        read_svd_sub=remove_jitter_svd_auto(handles,raw,read_svd,handles.ycor(ycor_scanned));
        handles.xread_dy(end+1,:)=mean(read_svd_sub(:,1:nbpm));
        handles.yread_dy(end+1,:)=mean(read_svd_sub(:,nbpm+1:2*nbpm));
%         handles.iread_dy(end+1,:)=output{1}(3,handles.bpm_index);
        handles.dxread_dy(end+1,:)=std(read_svd_sub(:,1:nbpm));
        handles.dyread_dy(end+1,:)=std(read_svd_sub(:,nbpm+1:2*nbpm));
%         handles.diread_dy(end+1,:)=output{1}(6,handles.bpm_index);
        handles.rawread_dy(end+1:end+handles.bpmave,:)=raw;        
    else
        [s,output]=bpmave(raw,handles.bpmave);
        %output = {[meanData.h; meanData.v; meanData.t; rmsData.h; rmsData.v; rmsData.t; nbad.h; nbad.v; nbad.t] badbpms};

        handles.xread_dy(end+1,:)=output{1}(1,handles.bpm_index);
        handles.yread_dy(end+1,:)=output{1}(2,handles.bpm_index);
        handles.iread_dy(end+1,:)=output{1}(3,handles.bpm_index);
        handles.dxread_dy(end+1,:)=output{1}(4,handles.bpm_index);
        handles.dyread_dy(end+1,:)=output{1}(5,handles.bpm_index);
        handles.diread_dy(end+1,:)=output{1}(6,handles.bpm_index);
        handles.rawread_dy(end+1:end+handles.bpmave,:)=raw;
    end
  end
  %reset corector to initial value
  handles=display_output(handles,sprintf('set %s (PS %i) back at %f mrad',handles.ycor_name(ycor_scanned,:),handles.ycor_ps(ycor_scanned),handles.ycor_ps_read(ycor_scanned)*1e3));
  PS(handles.ycor_ps(ycor_scanned)).SetPt=handles.ycor_ps_read(ycor_scanned);
  stat=PSTrim(handles.ycor_ps(ycor_scanned), true);
  if(stat{1}==-1)
    handles=display_output(handles,sprintf('PSTrim returned error :%s',stat{2}));
  end
  while( abs((PS(handles.ycor_ps(ycor_scanned)).Ampl-handles.ycor_ps_read(ycor_scanned))./handles.ycor_ps_read(ycor_scanned))>=.05 && abs(PS(handles.ycor_ps(ycor_scanned)).Ampl-handles.ycor_ps_read(ycor_scanned))>1e-5)
    itteration=itteration+1;
    %give up after 5 asks
    if(itteration>5)
      handles=display_output(handles,sprintf('Warning : PS %i is set at %f instead of %f.',handles.ycor_ps(ycor_scanned),PS(handles.ycor_ps(ycor_scanned)).Ampl,handles.ycor_ps_read(ycor_scanned)));
      break;
    end
    
    %ask to change different ps from what is wanted
    stat=PSTrim(handles.ycor_ps(ycor_scanned), true);
    if(stat{1}==-1)
      handles=display_output(handles,sprintf('PSTrim reurned error :%s',stat{2}));
    end
    pause(2);
    
    %update ps values
    FlHwUpdate();
  end
  pause(0.2)  
  if(all(get(handles.stop_btn,'BackgroundColor')==[1 0 0]))
      set(handles.stop_btn,'BackgroundColor',[0.702 0.702 0.702]);
      last_scanned=find(ycor_scanned==handles.list_ycor_scanned);
      handles.list_ycor_scanned=handles.list_ycor_scanned(1:last_scanned);
      handles.list_xmover_scanned=[];
      handles.list_ymover_scanned=[];
      break
  end
end

for xmover_scanned=handles.list_xmover_scanned
  for xmover_dpos=handles.list_xmover_dpos
    handles=display_output(handles,sprintf('set %s at x=%f um',handles.mover_name(xmover_scanned,:),(handles.mover_pos(xmover_scanned,1)+xmover_dpos)*1e6));
    pos_newvalue=handles.mover_pos(xmover_scanned,:)+[xmover_dpos 0 0];
    GIRDER{handles.mover(xmover_scanned)}.MoverSetPt(1:2)=pos_newvalue(1:2);
    stat=MoverTrim(handles.mover(xmover_scanned), 3);
    if(stat{1}==-1)
      handles=display_output(handles,sprintf('MoverTrim returned error :%s',stat{2}));
    end
    pause(10);
    FlHwUpdate();
    
    %if more than 5% difference, ask again
    itteration=0;
    while( abs((GIRDER{handles.mover(xmover_scanned)}.MoverPos(1)-pos_newvalue(1))./pos_newvalue(1))>=.05 && abs(GIRDER{handles.mover(xmover_scanned)}.MoverPos(1)-pos_newvalue(1))>1e-5)
      itteration=itteration+1;
      %give up after 5 asks
      if(itteration>5)
        handles=display_output(handles,sprintf('Warning : mover %s is set at x=%f um instead of %f um.',handles.mover_name(xmover_scanned,:),GIRDER{handles.mover(xmover_scanned)}.MoverPos(1)*1e6,pos_newvalue(1)*1e6));
        break;
      end
      
      %ask to change different pos from what is wanted
      stat=MoverTrim(handles.mover(xmover_scanned), 3);
      if(stat{1}==-1)
        handles=display_output(handles,sprintf('MoverTrim reurned error :%s',stat{2}));
      end
      
      %update pos values
      pause(3);
      FlHwUpdate();
    end
    
    FlHwUpdate('wait',handles.bpmave+3);
    handles=display_output(handles,'BPM measured');
    %        [s,output]=FlHwUpdate('bpmave',10);
    [s,raw]=FlHwUpdate('readbuffer',handles.bpmave);
    if(handles.svd_jitter_subtraction==true)
        read_svd_sub=remove_jitter_svd_auto(handles,raw,read_svd,handles.q_on_mover(xmover_scanned,:));
        handles.xread_dposx(end+1,:)=mean(read_svd_sub(:,1:nbpm));
        handles.yread_dposx(end+1,:)=mean(read_svd_sub(:,nbpm+1:2*nbpm));
%         handles.iread_dposx(end+1,:)=output{1}(3,handles.bpm_index);
        handles.dxread_dposx(end+1,:)=std(read_svd_sub(:,1:nbpm));
        handles.dyread_dposx(end+1,:)=std(read_svd_sub(:,nbpm+1:2*nbpm));
%         handles.diread_dposx(end+1,:)=output{1}(6,handles.bpm_index);
        handles.rawread_dposx(end+1:end+handles.bpmave,:)=raw;        
    else
        [s,output]=bpmave(raw,handles.bpmave);
        %output = {[meanData.h; meanData.v; meanData.t; rmsData.h; rmsData.v; rmsData.t; nbad.h; nbad.v; nbad.t] badbpms};

        handles.xread_dposx(end+1,:)=output{1}(1,handles.bpm_index);
        handles.yread_dposx(end+1,:)=output{1}(2,handles.bpm_index);
        handles.iread_dposx(end+1,:)=output{1}(3,handles.bpm_index);
        handles.dxread_dposx(end+1,:)=output{1}(4,handles.bpm_index);
        handles.dyread_dposx(end+1,:)=output{1}(5,handles.bpm_index);
        handles.diread_dposx(end+1,:)=output{1}(6,handles.bpm_index);
        handles.rawread_dposx(end+1:end+handles.bpmave,:)=raw;
    end
  end
  
  % Set back the mover
  
  handles=display_output(handles,sprintf('set back %s at x=%f um',handles.mover_name(xmover_scanned,:),(handles.mover_pos(xmover_scanned,1))*1e6));
  pos_newvalue=handles.mover_pos(xmover_scanned,:);
  GIRDER{handles.mover(xmover_scanned)}.MoverSetPt(1:2)=pos_newvalue(1:2);
  stat=MoverTrim(handles.mover(xmover_scanned), 3);
  if(stat{1}==-1)
    handles=display_output(handles,sprintf('MoverTrim returned error :%s',stat{2}));
  end
  
  pause(10);
  FlHwUpdate();
  %if more than 5% difference, ask again
  itteration=0;
  while( abs((GIRDER{handles.mover(xmover_scanned)}.MoverPos(1)-pos_newvalue(1))./pos_newvalue(1))>=.05 && abs(GIRDER{handles.mover(xmover_scanned)}.MoverPos(1)-pos_newvalue(1))>1e-5)
    itteration=itteration+1;
    %give up after 5 asks
    if(itteration>5)
      handles=display_output(handles,sprintf('Warning : mover %s is set at x=%f um instead of %f um.',handles.mover_name(xmover_scanned,:),GIRDER{handles.mover(xmover_scanned)}.MoverPos(1)*1e6,pos_newvalue(1)*1e6));
      break;
    end
    %ask to change different pos from what is wanted
    stat=MoverTrim(handles.mover(xmover_scanned), 3);
    if(stat{1}==-1)
      handles=display_output(handles,sprintf('MoverTrim reurned error :%s',stat{2}));
    end
    %update pos values
    pause(10);
  end
  pause(0.2)
  if(all(get(handles.stop_btn,'BackgroundColor')==[1 0 0]))
      set(handles.stop_btn,'BackgroundColor',[0.702 0.702 0.702]);
      last_scanned=find(xmover_scanned==handles.list_xmover_scanned);
      handles.list_xmover_scanned=handles.list_xmover_scanned(1:last_scanned);
      handles.list_ymover_scanned=[];
      break
  end
end

for ymover_scanned=handles.list_ymover_scanned
  for ymover_dpos=handles.list_ymover_dpos
    handles=display_output(handles,sprintf('set %s at y=%f um',handles.mover_name(ymover_scanned,:),(handles.mover_pos(ymover_scanned,2)+ymover_dpos)*1e6));
    pos_newvalue=handles.mover_pos(ymover_scanned,:)+[0 ymover_dpos 0];
    GIRDER{handles.mover(ymover_scanned)}.MoverSetPt(1:2)=pos_newvalue(1:2);
    stat=MoverTrim(handles.mover(ymover_scanned), 3);
    if(stat{1}==-1)
      handles=display_output(handles,sprintf('MoverTrim returned error :%s',stat{2}));
    end
    
    pause(10);
    FlHwUpdate();
    
    %if more than 5% difference, ask again
    itteration=0;
    while( abs((GIRDER{handles.mover(ymover_scanned)}.MoverPos(2)-pos_newvalue(2))./pos_newvalue(2))>=.05 && abs(GIRDER{handles.mover(ymover_scanned)}.MoverPos(2)-pos_newvalue(2))>1e-5)
      itteration=itteration+1;
      %give up after 5 asks
      if(itteration>5)
        handles=display_output(handles,sprintf('Warning : mover %s is set at y=%f um instead of %f um.',handles.mover_name(ymover_scanned,:),GIRDER{handles.mover(ymover_scanned)}.MoverPos(2)*1e6,pos_newvalue(2)*1e6));
        break;
      end
      
      %ask to change different pos from what is wanted
      stat=MoverTrim(handles.mover(ymover_scanned), 3);
      if(stat{1}==-1)
        handles=display_output(handles,sprintf('MoverTrim reurned error :%s',stat{2}));
      end
      
      %update pos values
      pause(10);
      FlHwUpdate();
    end
    
    FlHwUpdate('wait',handles.bpmave+3);
    handles=display_output(handles,'BPM measured');
    %        [s,output]=FlHwUpdate('bpmave',10);
    [s,raw]=FlHwUpdate('readbuffer',handles.bpmave);
    if(handles.svd_jitter_subtraction==true)
        read_svd_sub=remove_jitter_svd_auto(handles,raw,read_svd,handles.q_on_mover(ymover_scanned,:));
        handles.xread_dposy(end+1,:)=mean(read_svd_sub(:,1:nbpm));
        handles.yread_dposy(end+1,:)=mean(read_svd_sub(:,nbpm+1:2*nbpm));
%         handles.iread_dposy(end+1,:)=output{1}(3,handles.bpm_index);
        handles.dxread_dposy(end+1,:)=std(read_svd_sub(:,1:nbpm));
        handles.dyread_dposy(end+1,:)=std(read_svd_sub(:,nbpm+1:2*nbpm));
%         handles.diread_dposy(end+1,:)=output{1}(6,handles.bpm_index);
        handles.rawread_dposy(end+1:end+handles.bpmave,:)=raw;        
    else
        [s,output]=bpmave(raw,handles.bpmave);
        %output = {[meanData.h; meanData.v; meanData.t; rmsData.h; rmsData.v; rmsData.t; nbad.h; nbad.v; nbad.t] badbpms};

        handles.xread_dposy(end+1,:)=output{1}(1,handles.bpm_index);
        handles.yread_dposy(end+1,:)=output{1}(2,handles.bpm_index);
        handles.iread_dposy(end+1,:)=output{1}(3,handles.bpm_index);
        handles.dxread_dposy(end+1,:)=output{1}(4,handles.bpm_index);
        handles.dyread_dposy(end+1,:)=output{1}(5,handles.bpm_index);
        handles.diread_dposy(end+1,:)=output{1}(6,handles.bpm_index);
        handles.rawread_dposy(end+1:end+handles.bpmave,:)=raw;
    end
  end
  
  % Set back the mover
  
  handles=display_output(handles,sprintf('set back %s at y=%f um',handles.mover_name(ymover_scanned,:),(handles.mover_pos(ymover_scanned,2))*1e6));
  pos_newvalue=handles.mover_pos(ymover_scanned,:);
  GIRDER{handles.mover(ymover_scanned)}.MoverSetPt(1:2)=pos_newvalue(1:2);
  stat=MoverTrim(handles.mover(ymover_scanned), 3);
  if(stat{1}==-1)
    handles=display_output(handles,sprintf('MoverTrim returned error :%s',stat{2}));
  end
  pause(10);
  FlHwUpdate();
  
  %if more than 5% difference, ask again
  itteration=0;
  while( abs((GIRDER{handles.mover(ymover_scanned)}.MoverPos(2)-pos_newvalue(2))./pos_newvalue(2))>=.05 && abs(GIRDER{handles.mover(ymover_scanned)}.MoverPos(2)-pos_newvalue(2))>1e-5)
    itteration=itteration+1;
    %give up after 5 asks
    if(itteration>5)
      handles=display_output(handles,sprintf('Warning : mover %s is set at y=%f um instead of %f um.',handles.mover_name(ymover_scanned,:),GIRDER{handles.mover(ymover_scanned)}.MoverPos(2)*1e6,pos_newvalue(2)*1e6));
      break;
    end
    
    %ask to change different pos from what is wanted
    stat=MoverTrim(handles.mover(ymover_scanned), 3);
    if(stat{1}==-1)
      handles=display_output(handles,sprintf('MoverTrim reurned error :%s',stat{2}));
    end
    
    %update pos values
    pause(10);
  end
  pause(0.2)
  if(all(get(handles.stop_btn,'BackgroundColor')==[1 0 0]))
      set(handles.stop_btn,'BackgroundColor',[0.702 0.702 0.702]);
      last_scanned=find(ymover_scanned==handles.list_ymover_scanned);
      handles.list_ymover_scanned=handles.list_ymover_scanned(1:last_scanned);
      break
  end
end

AccessRequest('release',reqID);
if(stat{1}==-1)
  handles=display_output(handles,sprintf('Error durring access release, reurned error :%s',stat{2}));
else
  handles=display_output(handles,sprintf('Access %d released',reqID));
end

save(handles.filename_save);

handles=display_output(handles,sprintf('file %s created',handles.filename_save));
pause(.2);
set(handles.filename_load_txt,'String',handles.filename_save);
handles.filename_load=handles.filename_save;
guidata(hObject, handles);
%if (handles.auto_show)
%    handles=load_Callback(handles.load, eventdata, handles);
%    loaded=load(handles.filename_load);
%    if(~isempty([loaded.handles.list_xcor_scanned loaded.handles.list_ycor_scanned]))
%        plot_cor_Callback(handles.plot_cor, eventdata, handles);
%    else
%        plot_mover_Callback(handles.plot_mover, eventdata, handles);
%    end
%end
guidata(hObject, handles);


% --- Executes on selection change in mover_plot.
function mover_plot_Callback(hObject, eventdata, handles)
% hObject    handle to mover_plot (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = get(hObject,'String') returns mover_plot contents as
% cell array
%        contents{get(hObject,'Value')} returns selected item from mover_plot
handles.mover_plotted=get(hObject,'Value');
guidata(hObject, handles);

% --- Executes during object creation, after setting all properties.
function mover_plot_CreateFcn(hObject, eventdata, handles)
% hObject    handle to mover_plot (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
  set(handles.stop_btn,'BackgroundColor','white');
end


% --- Executes on button press in plot_alone_mover.
function plot_alone_mover_Callback(hObject, eventdata, handles)
% hObject    handle to plot_alone_mover (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of plot_alone_mover
handles.mover_alone=get(hObject,'Value');
guidata(hObject, handles);


% --- Executes on selection change in cor_plot.
function cor_plot_Callback(hObject, eventdata, handles)
% hObject    handle to cor_plot (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = get(hObject,'String') returns cor_plot contents as cell array
%        contents{get(hObject,'Value')} returns selected item from cor_plot
handles.cor_plotted=get(hObject,'Value');
guidata(hObject, handles);


% --- Executes during object creation, after setting all properties.
function cor_plot_CreateFcn(hObject, eventdata, handles)
% hObject    handle to cor_plot (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
  set(handles.stop_btn,'BackgroundColor','white');
end


% --- Executes on button press in plot_alone_cor.
function plot_alone_cor_Callback(hObject, eventdata, handles)
% hObject    handle to plot_alone_cor (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of plot_alone_cor
handles.cor_alone=get(hObject,'Value');
guidata(hObject, handles);


% --- Executes on selection change in xaxis_corr_plot.
function xaxis_corr_plot_Callback(hObject, eventdata, handles)
% hObject    handle to xaxis_corr_plot (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = get(hObject,'String') returns xaxis_corr_plot contents as cell array
%        contents{get(hObject,'Value')} returns selected item from xaxis_corr_plot
handles.xaxis_corr_plotted=get(hObject,'Value');
guidata(hObject, handles);

% --- Executes during object creation, after setting all properties.
function xaxis_corr_plot_CreateFcn(hObject, eventdata, handles)
% hObject    handle to xaxis_corr_plot (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
  set(handles.stop_btn,'BackgroundColor','white');
end


% --- Executes on selection change in yaxis_corr_plot.
function yaxis_corr_plot_Callback(hObject, eventdata, handles)
% hObject    handle to yaxis_corr_plot (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = get(hObject,'String') returns yaxis_corr_plot contents as cell array
%        contents{get(hObject,'Value')} returns selected item from yaxis_corr_plot
handles.yaxis_corr_plotted=get(hObject,'Value');
guidata(hObject, handles);


% --- Executes during object creation, after setting all properties.
function yaxis_corr_plot_CreateFcn(hObject, eventdata, handles)
% hObject    handle to yaxis_corr_plot (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
  set(handles.stop_btn,'BackgroundColor','white');
end


% --- Executes on button press in checkbox4.
function checkbox4_Callback(hObject, eventdata, handles)
% hObject    handle to checkbox4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of checkbox4



function bpm_ave_txt_Callback(hObject, eventdata, handles)
% hObject    handle to bpm_ave_txt (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of bpm_ave_txt as text
%        eval(get(hObject,'String')) returns contents of bpm_ave_txt as a double
handles.bpmave=eval(get(hObject,'String'));
guidata(hObject, handles);


% --- Executes during object creation, after setting all properties.
function bpm_ave_txt_CreateFcn(hObject, eventdata, handles)
% hObject    handle to bpm_ave_txt (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
  set(handles.stop_btn,'BackgroundColor','white');
end
handles.bpmave=eval(get(hObject,'String'));
guidata(hObject, handles);


% --- Executes on button press in auto_show_check.
function auto_show_check_Callback(hObject, eventdata, handles)
% hObject    handle to auto_show_check (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of auto_show_check
handles.auto_show=get(hObject,'Value');
guidata(hObject, handles);



function save_filename_txt_Callback(hObject, eventdata, handles)
% hObject    handle to save_filename_txt (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of save_filename_txt as text
%        eval(get(hObject,'String')) returns contents of save_filename_txt as a double
handles.filename_save=get(hObject,'String');
guidata(hObject, handles);


% --- Executes during object creation, after setting all properties.
function save_filename_txt_CreateFcn(hObject, eventdata, handles)
% hObject    handle to save_filename_txt (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
  set(handles.stop_btn,'BackgroundColor','white');
end
handles.filename_save=datestr(now,30);
set(hObject,'String',handles.filename_save)
guidata(hObject, handles);


function ycor_dstrength_txt_Callback(hObject, eventdata, handles)
% hObject    handle to ycor_dstrength_txt (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of ycor_dstrength_txt as text
%        eval(get(hObject,'String')) returns contents of ycor_dstrength_txt as a double
handles.list_ycor_dstrength=eval(get(hObject,'String'));
guidata(hObject, handles);


% --- Executes during object creation, after setting all properties.
function ycor_dstrength_txt_CreateFcn(hObject, eventdata, handles)
% hObject    handle to ycor_dstrength_txt (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
  set(handles.stop_btn,'BackgroundColor','white');
end
handles.list_ycor_dstrength=eval(get(hObject,'String'));
guidata(hObject, handles);


function xcor_dstrength_txt_Callback(hObject, eventdata, handles)
% hObject    handle to xcor_dstrength_txt (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of xcor_dstrength_txt as text
%        eval(get(hObject,'String')) returns contents of xcor_dstrength_txt as a double
handles.list_xcor_dstrength=eval(get(hObject,'String'));
guidata(hObject, handles);


% --- Executes during object creation, after setting all properties.
function xcor_dstrength_txt_CreateFcn(hObject, eventdata, handles)
% hObject    handle to xcor_dstrength_txt (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
  set(handles.stop_btn,'BackgroundColor','white');
end
handles.list_xcor_dstrength=eval(get(hObject,'String'));
guidata(hObject, handles);


function xmover_dpos_txt_Callback(hObject, eventdata, handles)
% hObject    handle to xmover_dpos_txt (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of xmover_dpos_txt as text
%        eval(get(hObject,'String')) returns contents of xmover_dpos_txt as a double
handles.list_xmover_dpos=eval(get(hObject,'String'));
guidata(hObject, handles);


% --- Executes during object creation, after setting all properties.
function xmover_dpos_txt_CreateFcn(hObject, eventdata, handles)
% hObject    handle to xmover_dpos_txt (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
  set(handles.stop_btn,'BackgroundColor','white');
end
handles.list_xmover_dpos=eval(get(hObject,'String'));
guidata(hObject, handles);



function ymover_dpos_txt_Callback(hObject, eventdata, handles)
% hObject    handle to ymover_dpos_txt (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of ymover_dpos_txt as text
%        eval(get(hObject,'String')) returns contents of ymover_dpos_txt as a double
handles.list_ymover_dpos=eval(get(hObject,'String'));
guidata(hObject, handles);


% --- Executes during object creation, after setting all properties.
function ymover_dpos_txt_CreateFcn(hObject, eventdata, handles)
% hObject    handle to ymover_dpos_txt (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
  set(handles.stop_btn,'BackgroundColor','white');
end
handles.list_ymover_dpos=eval(get(hObject,'String'));
guidata(hObject, handles);

function filename_load_txt_Callback(hObject, eventdata, handles)
% hObject    handle to filename_load_txt (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of filename_load_txt as text
%        eval(get(hObject,'String')) returns contents of filename_load_txt as a double
handles.filename_load=get(hObject,'String');
if iscell(handles.filename_load)
  handles.filename_load=handles.filename_load{1};
end
guidata(hObject, handles);


% --- Executes during object creation, after setting all properties.
function filename_load_txt_CreateFcn(hObject, eventdata, handles)
% hObject    handle to filename_load_txt (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
  set(handles.stop_btn,'BackgroundColor','white');
end
guidata(hObject, handles);



function edit20_Callback(hObject, eventdata, handles)
% hObject    handle to edit20 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit20 as text
%        eval(get(hObject,'String')) returns contents of edit20 as a double


% --- Executes during object creation, after setting all properties.
function edit20_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit20 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
  set(handles.stop_btn,'BackgroundColor','white');
end


% --- Executes on button press in plot_mover.
function plot_mover_Callback(hObject, eventdata, handles)
% hObject    handle to plot_mover (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


loaded=load(handles.filename_load);
global BEAMLINE PS GIRDER INSTR

BEAMLINE=loaded.BEAMLINE;
PS=loaded.PS;
GIRDER=loaded.GIRDER;
INSTR=loaded.INSTR;

handles=display_output(handles,sprintf('ploting result for the mover'));
IEX=handles.IEX;IEX_s=BEAMLINE{IEX}.S;
IP=findcells(BEAMLINE,'Name','IP');IP_s=BEAMLINE{IP}.S;
EOL=length(BEAMLINE);EOL_s=BEAMLINE{EOL}.S;
nposx=length(loaded.handles.list_xmover_dpos);
nposy=length(loaded.handles.list_ymover_dpos);
bpm=loaded.handles.bpm; % include all BPMs
nbpm=length(bpm);
bpm_s=loaded.handles.bpm_s;
bpm_name=loaded.handles.bpm_name;
bpm_order=(1:nbpm);

q_x_dposx=zeros(nbpm,2,length(loaded.handles.list_xmover_scanned));
q_x_dposy=zeros(nbpm,2,length(loaded.handles.list_xmover_scanned));
q_y_dposx=zeros(nbpm,2,length(loaded.handles.list_xmover_scanned));
q_y_dposy=zeros(nbpm,2,length(loaded.handles.list_xmover_scanned));
dq_x_dposx=zeros(nbpm,2,length(loaded.handles.list_xmover_scanned));
dq_x_dposy=zeros(nbpm,2,length(loaded.handles.list_xmover_scanned));
dq_y_dposx=zeros(nbpm,2,length(loaded.handles.list_xmover_scanned));
dq_y_dposy=zeros(nbpm,2,length(loaded.handles.list_xmover_scanned));
for i=1:nbpm
  for j=1:length(loaded.handles.list_xmover_scanned)
    if(loaded.handles.bpm_on_mover(loaded.handles.list_xmover_scanned(j))<bpm(i))
      [s,r]=RmatAtoB(loaded.handles.q_on_mover(loaded.handles.list_xmover_scanned(j),1),bpm(i));
      E(i,j)=r(1,2)*loaded.handles.xmover_KL(loaded.handles.list_xmover_scanned(j));
      F(i,j)=r(3,2)*loaded.handles.xmover_KL(loaded.handles.list_xmover_scanned(j));
    else
      if(loaded.handles.bpm_on_mover(loaded.handles.list_xmover_scanned(j))==bpm(i))
        E(i,j)=-1;
        F(i,j)=0;
      else
        E(i,j)=0;
        F(i,j)=0;
      end
    end
    [q_x_dposx(i,:,j),dq_x_dposx(i,:,j)]=noplot_polyfit(loaded.handles.list_xmover_dpos,loaded.handles.xread_dposx((j-1)*nposx+(1:nposx),i),loaded.handles.dxread_dposx((j-1)*nposx+[1:nposx],i),1);
    [q_y_dposx(i,:,j),dq_y_dposx(i,:,j)]=noplot_polyfit(loaded.handles.list_xmover_dpos,loaded.handles.yread_dposx((j-1)*nposx+(1:nposx),i),loaded.handles.dyread_dposx((j-1)*nposx+[1:nposx],i),1);
  end
  for j=1:length(loaded.handles.list_ymover_scanned)
    if(loaded.handles.bpm_on_mover(loaded.handles.list_ymover_scanned(j))<bpm(i))
      [s,r]=RmatAtoB(loaded.handles.q_on_mover(loaded.handles.list_ymover_scanned(j),1),bpm(i));
      G(i,j)=r(1,4)*-loaded.handles.ymover_KL(loaded.handles.list_ymover_scanned(j));
      H(i,j)=r(3,4)*-loaded.handles.ymover_KL(loaded.handles.list_ymover_scanned(j));
    else
      if(loaded.handles.bpm_on_mover(loaded.handles.list_ymover_scanned(j))==bpm(i))
        G(i,j)=0;
        H(i,j)=-1;
      else
        G(i,j)=0;
        H(i,j)=0;
      end
    end
    [q_x_dposy(i,:,j),dq_x_dposy(i,:,j)]=noplot_polyfit(loaded.handles.list_ymover_dpos,loaded.handles.xread_dposy((j-1)*nposy+(1:nposy),i),loaded.handles.dxread_dposy((j-1)*nposy+[1:nposy],i),1);
    [q_y_dposy(i,:,j),dq_y_dposy(i,:,j)]=noplot_polyfit(loaded.handles.list_ymover_dpos,loaded.handles.yread_dposy((j-1)*nposy+(1:nposy),i),loaded.handles.dyread_dposy((j-1)*nposy+[1:nposy],i),1);
  end
end
if (handles.mover_alone)
  figure(1);
  set(gcf,'ActionPostCallback',{@figResize,handles})
  resplot=true;
else
  axes(handles.plot);
  resplot=false;
end
cla('reset');
hold on;
legend_txt={};
if(handles.mover_plotted<=length(loaded.handles.list_xmover_scanned))
  j=handles.mover_plotted;
  errorbar(bpm_s(bpm_order),q_x_dposx(bpm_order,2,j),dq_x_dposx(bpm_order,2,j),'b-');
  errorbar(bpm_s(bpm_order),q_y_dposx(bpm_order,2,j),dq_y_dposx(bpm_order,2,j),'r-');
  plot(bpm_s(bpm_order),E(bpm_order,j),'b--');
  plot(bpm_s(bpm_order),F(bpm_order,j),'r--');
  legend_txt{1}=sprintf('%s measurement X',loaded.handles.mover_name(loaded.handles.list_xmover_scanned(j),:));
  legend_txt{2}=sprintf('%s measurement Y',loaded.handles.mover_name(loaded.handles.list_xmover_scanned(j),:));
  legend_txt{3}=sprintf('%s model X',loaded.handles.mover_name(loaded.handles.list_xmover_scanned(j),:));
  legend_txt{4}=sprintf('%s model Y',loaded.handles.mover_name(loaded.handles.list_xmover_scanned(j),:));
elseif(handles.mover_plotted<=length(loaded.handles.list_xmover_scanned)+length(loaded.handles.list_ymover_scanned))
  j=handles.mover_plotted-length(loaded.handles.list_xmover_scanned);
  errorbar(bpm_s(bpm_order),q_x_dposy(bpm_order,2),dq_x_dposy(bpm_order,2),'b-');
  errorbar(bpm_s(bpm_order),q_y_dposy(bpm_order,2),dq_y_dposy(bpm_order,2),'r-');
  plot(bpm_s(bpm_order),G(bpm_order,j),'b--');
  plot(bpm_s(bpm_order),H(bpm_order,j),'r--');
  legend_txt{1}=sprintf('%s measurement X',loaded.handles.mover_name(loaded.handles.list_ymover_scanned(j),:));
  legend_txt{2}=sprintf('%s measurement Y',loaded.handles.mover_name(loaded.handles.list_ymover_scanned(j),:));
  legend_txt{3}=sprintf('%s model X',loaded.handles.mover_name(loaded.handles.list_ymover_scanned(j),:));
  legend_txt{4}=sprintf('%s model Y',loaded.handles.mover_name(loaded.handles.list_ymover_scanned(j),:));
end
hold off;
ylabel('d(mover)/d(BPM)) [m/m]');
legend(legend_txt,'Location','NorthWest');
handles.xlim=[min([bpm_s;IEX_s]),max([bpm_s;EOL_s])]-IEX_s;
set(handles.xlim_str,'String',sprintf('[%.3g %.3g]',handles.xlim(1),handles.xlim(2)));
xlim(handles.xlim+IEX_s);
yautoscale=get(gca,'Ylim');
set(handles.ylim_str,'String',sprintf('[%.3g %.3g]',yautoscale));
handles.ylim=yautoscale;
ylim(handles.ylim);
[xt,id]=sort(bpm_s);
xtl=bpm_name;xtl=xtl(id);
set(gca,'XTick',xt,'XTickLabel',xtl)
grid;
handles.hText=xticklabel_rotate90();
plot_magnets_external(BEAMLINE,handles.lattice,handles.xlim(1)+IEX_s,handles.xlim(2)+IEX_s);
guidata(hObject, handles);
  
% plot residuals
if resplot
  figure(2)
  cla('reset');
  set(gcf,'ActionPostCallback',{@figResize,handles})
  hold on;
  legend_txt={};
  if(handles.mover_plotted<=length(loaded.handles.list_xmover_scanned))
    j=handles.mover_plotted;
    resx=abs(q_x_dposx(bpm_order,2,j)-E(bpm_order,j));
    resy=abs(q_y_dposx(bpm_order,2,j)-F(bpm_order,j));
    errorbar(bpm_s(bpm_order),resx,dq_x_dposx(bpm_order,2,j),'b*');
    errorbar(bpm_s(bpm_order),resy,dq_y_dposx(bpm_order,2,j),'r*');
    legend_txt{1}=sprintf('%s meas-model residuals (X)',loaded.handles.mover_name(loaded.handles.list_xmover_scanned(j),:));
    legend_txt{2}=sprintf('%s meas-model residuals (Y)',loaded.handles.mover_name(loaded.handles.list_xmover_scanned(j),:));
  elseif(handles.mover_plotted<=length(loaded.handles.list_xmover_scanned)+length(loaded.handles.list_ymover_scanned))
    j=handles.mover_plotted-length(loaded.handles.list_xmover_scanned);
    resx=abs(q_x_dposy(bpm_order,2)-G(bpm_order,j));
    resy=abs(q_y_dposy(bpm_order,2)-H(bpm_order,j));
    errorbar(bpm_s(bpm_order),resx,dq_x_dposy(bpm_order,2),'b*');
    errorbar(bpm_s(bpm_order),resy,dq_y_dposy(bpm_order,2),'r*');
    legend_txt{1}=sprintf('%s meas-model residuals (X)',loaded.handles.mover_name(loaded.handles.list_ymover_scanned(j),:));
    legend_txt{2}=sprintf('%s meas-model residuals (Y)',loaded.handles.mover_name(loaded.handles.list_ymover_scanned(j),:));
  end
  hold off;
  ylabel('R(mover->BPMs) [m/rad]');
  legend(legend_txt,'Location','NorthWest');
  handles.xlim=[min([bpm_s;IEX_s]),max([bpm_s;EOL_s])]-IEX_s;
  set(handles.xlim_str,'String',sprintf('[%.3g %.3g]',handles.xlim(1),handles.xlim(2)));
  xlim(handles.xlim+IEX_s);
  yautoscale=get(gca,'Ylim');
  set(handles.ylim_str,'String',sprintf('[%.3g %.3g]',yautoscale));
  handles.ylim=yautoscale;
  ylim(handles.ylim);
  [xt,id]=sort(bpm_s);
  xtl=bpm_name;xtl=xtl(id);
  set(gca,'XTick',xt,'XTickLabel',xtl)
  grid;
  handles.hText=xticklabel_rotate90();
  plot_magnets_external(BEAMLINE,handles.lattice,handles.xlim(1)+IEX_s,handles.xlim(2)+IEX_s);
  guidata(hObject, handles);
end

% --- Executes on button press in plot_cor.
function plot_cor_Callback(hObject, eventdata, handles)
% hObject    handle to plot_cor (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

loaded=load(handles.filename_load);

global BEAMLINE PS GIRDER INSTR FL

BEAMLINE=loaded.BEAMLINE;
PS=loaded.PS;
GIRDER=loaded.GIRDER;
INSTR=loaded.INSTR;

handles=display_output(handles,'ploting result for the corrector');
IEX=handles.IEX;IEX_s=BEAMLINE{IEX}.S;
IP=findcells(BEAMLINE,'Name','IP');IP_s=BEAMLINE{IP}.S;
EOL=length(BEAMLINE);EOL_s=BEAMLINE{EOL}.S;
nstrengthx=length(loaded.handles.list_xcor_dstrength);
nstrengthy=length(loaded.handles.list_ycor_dstrength);
bpm=loaded.handles.bpm; % include all BPMs
nbpm=length(bpm);
bpm_s=loaded.handles.bpm_s;
bpm_name=loaded.handles.bpm_name;
bpm_order=(1:nbpm);
%rmpath('/usr/local/MATLAB/R2010b/toolbox/curvefit/curvefit')
q_x_dx=[];
q_x_dy=[];
q_y_dx=[];
q_y_dy=[];
for i=1:nbpm
  for j=1:length(loaded.handles.list_xcor_scanned)
    if(loaded.handles.xcor(loaded.handles.list_xcor_scanned(j))<bpm(i))
      [s,r]=RmatAtoB_cor(loaded.handles.xcor(loaded.handles.list_xcor_scanned(j)),bpm(i));
    else
      r=zeros(6,6);
    end
    A(i,:,j)=r(1,:);
    B(i,:,j)=r(3,:);
    [q_x_dx(i,:,j),dq_x_dx(i,:,j)]=noplot_polyfit(loaded.handles.list_xcor_dstrength,loaded.handles.xread_dx((j-1)*nstrengthx+(1:nstrengthx),i),loaded.handles.dxread_dx((j-1)*nstrengthx+[1:nstrengthx],i),1);
    [q_y_dx(i,:,j),dq_y_dx(i,:,j)]=noplot_polyfit(loaded.handles.list_xcor_dstrength,loaded.handles.yread_dx((j-1)*nstrengthx+(1:nstrengthx),i),loaded.handles.dyread_dx((j-1)*nstrengthx+[1:nstrengthx],i),1);
  end
  for j=1:length(loaded.handles.list_ycor_scanned)
    if(loaded.handles.ycor(loaded.handles.list_ycor_scanned(j))<bpm(i))
      [s,r]=RmatAtoB_cor(loaded.handles.ycor(loaded.handles.list_ycor_scanned(j)),bpm(i));
    else
      r=zeros(6,6);
    end
    C(i,:,j)=r(1,:);
    D(i,:,j)=r(3,:);
    [q_x_dy(i,:,j),dq_x_dy(i,:,j)]=noplot_polyfit(loaded.handles.list_ycor_dstrength,loaded.handles.xread_dy((j-1)*nstrengthy+(1:nstrengthy),i),loaded.handles.dxread_dy((j-1)*nstrengthy+[1:nstrengthy],i),1);
    [q_y_dy(i,:,j),dq_y_dy(i,:,j)]=noplot_polyfit(loaded.handles.list_ycor_dstrength,loaded.handles.yread_dy((j-1)*nstrengthy+(1:nstrengthy),i),loaded.handles.dyread_dy((j-1)*nstrengthy+[1:nstrengthy],i),1);
  end
end
if (handles.cor_alone)
  resplot=true;
  figure(1);
%   set(gcf,'ActionPostCallback',{@figResize,handles})
else
  axes(handles.plot);
  resplot=false;
end
cla('reset');
hold on;
legend_txt={};
if(handles.cor_plotted<=length(loaded.handles.list_xcor_scanned))
  j=handles.cor_plotted;
  errorbar(bpm_s(bpm_order),q_x_dx(bpm_order,2,j),dq_x_dx(bpm_order,2,j),'b-');
  plot(bpm_s(bpm_order),A(bpm_order,2,j),'b--');
  errorbar(bpm_s(bpm_order),q_y_dx(bpm_order,2,j),dq_y_dx(bpm_order,2,j),'r-');
  plot(bpm_s(bpm_order),B(bpm_order,2,j),'r--');
  legend_txt{1}=sprintf('%s measurement X',loaded.handles.xcor_name(loaded.handles.list_xcor_scanned(j),:));
  legend_txt{2}=sprintf('%s model X',loaded.handles.xcor_name(loaded.handles.list_xcor_scanned(j),:));
  legend_txt{3}=sprintf('%s measurement Y',loaded.handles.xcor_name(loaded.handles.list_xcor_scanned(j),:));
  legend_txt{4}=sprintf('%s model Y',loaded.handles.xcor_name(loaded.handles.list_xcor_scanned(j),:));
  x=q_x_dx(bpm_order,2,j);
  dx=dq_x_dx(bpm_order,2,j);
  x2=A(bpm_order,2,j);
  range=find(x2~=0 & ~isnan(x2) & x~=0 & ~isnan(x));
  if(handles.plot_corr_scale)
      figure(2)
      set(gcf,'ActionPostCallback',{@figResize,handles})
%       [q,dq]=plot_polyfit(x2(range),x(range),dx(range),[1 1]);
      [q,dq]=plot_polyfit(x2(range),x(range),1,[1 1]);
      if (handles.cor_alone)
        figure(1);
        set(gcf,'ActionPostCallback',{@figResize,handles})
      else
        axes(handles.plot);
      end
  else
%       [q,dq]=noplot_polyfit(x2(range),x(range),dx(range),[1 1]);
      [q,dq]=noplot_polyfit(x2(range),x(range),1,[1 1]);
  end
  corr_factor=q(2);
  dcorr_factor=dq(2);  
  handles=display_output(handles,sprintf('%s factor : %f +- %f',loaded.handles.xcor_name(loaded.handles.list_xcor_scanned(j),:),corr_factor,dcorr_factor));
elseif(handles.cor_plotted<=length(loaded.handles.list_xcor_scanned)+length(loaded.handles.list_ycor_scanned))
  j=handles.cor_plotted-length(loaded.handles.list_xcor_scanned);
  errorbar(bpm_s(bpm_order),q_x_dy(bpm_order,2,j),dq_x_dy(bpm_order,2,j),'b-');
  plot(bpm_s(bpm_order),C(bpm_order,4,j),'b--');
  errorbar(bpm_s(bpm_order),q_y_dy(bpm_order,2,j),dq_y_dy(bpm_order,2,j),'r-');
  plot(bpm_s(bpm_order),D(bpm_order,4,j),'r--');
  legend_txt{1}=sprintf('%s measurement X',loaded.handles.ycor_name(loaded.handles.list_ycor_scanned(j),:));
  legend_txt{2}=sprintf('%s model X',loaded.handles.ycor_name(loaded.handles.list_ycor_scanned(j),:));
  legend_txt{3}=sprintf('%s measurement Y',loaded.handles.ycor_name(loaded.handles.list_ycor_scanned(j),:));
  legend_txt{4}=sprintf('%s model Y',loaded.handles.ycor_name(loaded.handles.list_ycor_scanned(j),:));
  y=q_y_dy(bpm_order,2,j);
  dy=dq_y_dy(bpm_order,2,j);
  y2=D(bpm_order,4,j);
  range=find(y2~=0 & ~isnan(y2) & y~=0 & ~isnan(y));
  if(handles.plot_corr_scale)
      figure(2)
      set(gcf,'ActionPostCallback',{@figResize,handles})
%       [q,dq]=plot_polyfit(y2(range),y(range),dy(range),[1 1]);
      [q,dq]=plot_polyfit(y2(range),y(range),1,[1 1]);
      if (handles.cor_alone)
        figure(1);
        set(gcf,'ActionPostCallback',{@figResize,handles})
      else
        axes(handles.plot);
      end
  else
%       [q,dq]=noplot_polyfit(y2(range),y(range),dy(range),[1 1]);
      [q,dq]=noplot_polyfit(y2(range),y(range),1,[1 1]);
  end
  corr_factor=q(2);
  dcorr_factor=dq(2);  
  handles=display_output(handles,sprintf('%s factor : %f +- %f',loaded.handles.ycor_name(loaded.handles.list_ycor_scanned(j),:),corr_factor,dcorr_factor));
end
hold off;
grid;
ylabel('R(cor->BPMs) [m/rad]');
legend(legend_txt,'Location','NorthWest');
handles.xlim=[min([bpm_s;IEX_s]),max([bpm_s;EOL_s])]-IEX_s;
set(handles.xlim_str,'String',sprintf('[%.3g %.3g]',handles.xlim(1),handles.xlim(2)));
xlim(handles.xlim+IEX_s);
yautoscale=get(gca,'Ylim');
set(handles.ylim_str,'String',sprintf('[%.3g %.3g]',yautoscale));
handles.ylim=yautoscale;
ylim(handles.ylim);
[xt,id]=sort([bpm_s]);
xtl=[bpm_name];xtl=xtl(id);
set(gca,'XTick',xt,'XTickLabel',xtl)
handles.hText=xticklabel_rotate90();
plot_magnets_external(BEAMLINE,handles.lattice,handles.xlim(1)+IEX_s,handles.xlim(2)+IEX_s);
guidata(hObject, handles);

if resplot
  figure(2)
  cla('reset');
%   set(gcf,'ActionPostCallback',{@figResize,handles})
  hold on;
  legend_txt={};
  than=arrayfun(@(x) find(FL.SimModel.Twiss.S==bpm_s(x),1),1:length(bpm_s));
  betax=sqrt(FL.SimModel.Twiss.betax(than))';
  betay=sqrt(FL.SimModel.Twiss.betay(than))';
  if(handles.cor_plotted<=length(loaded.handles.list_xcor_scanned))
    j=handles.cor_plotted;
    resx=abs(q_x_dx(bpm_order,2,j)-A(bpm_order,2,j));
    resy=abs(q_y_dx(bpm_order,2,j)-B(bpm_order,2,j));
    errorbar(bpm_s(bpm_order),resx./betax(bpm_order),dq_x_dx(bpm_order,2,j)./betax(bpm_order),'b*');
    errorbar(bpm_s(bpm_order),resy./betay(bpm_order),dq_y_dx(bpm_order,2,j)./betay(bpm_order),'r*');
    legend_txt{1}=sprintf('%s meas-model residuals (X)',loaded.handles.xcor_name(loaded.handles.list_xcor_scanned(j),:));
    legend_txt{2}=sprintf('%s meas-model residuals (Y)',loaded.handles.xcor_name(loaded.handles.list_xcor_scanned(j),:));
  elseif(handles.cor_plotted<=length(loaded.handles.list_xcor_scanned)+length(loaded.handles.list_ycor_scanned))
    j=handles.cor_plotted-length(loaded.handles.list_xcor_scanned);
    resx=abs(q_x_dy(bpm_order,2,j)-C(bpm_order,4,j));
    resy=abs(q_y_dy(bpm_order,2,j)-D(bpm_order,4,j));
    errorbar(bpm_s(bpm_order),resx./betax(bpm_order),dq_x_dy(bpm_order,2,j)./betax(bpm_order),'b*');
    errorbar(bpm_s(bpm_order),resy./betay(bpm_order),dq_y_dy(bpm_order,2,j)./betax(bpm_order),'r*');
    legend_txt{1}=sprintf('%s meas-model residuals (X)',loaded.handles.ycor_name(loaded.handles.list_ycor_scanned(j),:));
    legend_txt{2}=sprintf('%s meas-model residuals (Y)',loaded.handles.ycor_name(loaded.handles.list_ycor_scanned(j),:));
  end
  hold off;
  grid;
  ylabel('R(cor->BPMs) [m/rad]');
  legend(legend_txt,'Location','NorthWest');
  handles.xlim=[min([bpm_s;IEX_s]),max([bpm_s;EOL_s])]-IEX_s;
  set(handles.xlim_str,'String',sprintf('[%.3g %.3g]',handles.xlim(1),handles.xlim(2)));
  xlim(handles.xlim+IEX_s);
  yautoscale=get(gca,'Ylim');
  set(handles.ylim_str,'String',sprintf('[%.3g %.3g]',yautoscale));
  handles.ylim=yautoscale;
  ylim(handles.ylim);
  [xt,id]=sort([bpm_s]);
  xtl=[bpm_name];xtl=xtl(id);
  set(gca,'XTick',xt,'XTickLabel',xtl)
  handles.hText=xticklabel_rotate90();
  plot_magnets_external(BEAMLINE,handles.lattice,handles.xlim(1)+IEX_s,handles.xlim(2)+IEX_s);
  guidata(hObject, handles);
end

% --- Executes on selection change in listbox2.
function listbox2_Callback(hObject, eventdata, handles)
% hObject    handle to listbox2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = get(hObject,'String') returns listbox2 contents as cell array
%        contents{get(hObject,'Value')} returns selected item from listbox2


% --- Executes during object creation, after setting all properties.
function listbox2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to listbox2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: listbox controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
  set(handles.stop_btn,'BackgroundColor','white');
end


% --- Executes on button press in load.
function handles=load_Callback(hObject, eventdata, handles)
% hObject    handle to load (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
handles=display_output(handles,sprintf('loading %s',handles.filename_load));
loaded=load(handles.filename_load);
if(~isempty([loaded.handles.list_xcor_scanned loaded.handles.list_ycor_scanned]))
  set(handles.cor_plot,'Enable','on');
  set(handles.cor_plot,'String',strvcat(loaded.handles.xcor_name(loaded.handles.list_xcor_scanned,:),loaded.handles.ycor_name(loaded.handles.list_ycor_scanned,:)));
  set(handles.plot_corr_scale_chk,'Enable','on');
  set(handles.plot_alone_cor,'Enable','on');
  set(handles.plot_cor,'Enable','on');
else
  set(handles.cor_plot,'String','nodata');
  set(handles.plot_cor,'Enable','off');
  set(handles.plot_corr_scale_chk,'Enable','off');
  set(handles.plot_alone_cor,'Enable','off');
  set(handles.cor_plot,'Enable','off');
end
handles.cor_plotted=1;
if(~isempty([loaded.handles.list_xmover_scanned loaded.handles.list_ymover_scanned]))
  set(handles.mover_plot,'Enable','on');
  set(handles.plot_alone_mover,'Enable','on');
  set(handles.plot_mover,'Enable','on');
  set(handles.mover_plot,'String',strvcat(strcat(loaded.handles.mover_name(loaded.handles.list_xmover_scanned,:),'X'), strcat(loaded.handles.mover_name(loaded.handles.list_ymover_scanned,:),'Y')));
else
  set(handles.mover_plot,'String','no data');
  set(handles.mover_plot,'Enable','off');
  set(handles.plot_alone_mover,'Enable','off');
  set(handles.plot_mover,'Enable','off');
end
handles.mover_plotted=1;
guidata(hObject, handles);



function xlim_str_Callback(hObject, eventdata, handles)
% hObject    handle to xlim_str (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of xlim_str as text
%        str2double(get(hObject,'String')) returns contents of xlim_str as a double
global BEAMLINE

handles=display_output(handles,'resizing plot');
IEX=findcells(BEAMLINE,'Name','IEX');
IEX_s=BEAMLINE{IEX}.S;
handles.xlim=eval(get(hObject,'String'));
if (handles.cor_alone)
  figure(1);
  xlim(handles.xlim+IEX_s);
  set(gcf,'ActionPostCallback',{@figResize,handles})
else
  axes(handles.plot);
  xlim(handles.xlim+IEX_s);
  axes(handles.lattice);
  xlim(handles.xlim+IEX_s);
end
guidata(hObject, handles);

% --- Executes during object creation, after setting all properties.
function xlim_str_CreateFcn(hObject, eventdata, handles)
% hObject    handle to xlim_str (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
  set(handles.stop_btn,'BackgroundColor','white');
end



function ylim_str_Callback(hObject, eventdata, handles)
% hObject    handle to ylim_str (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of ylim_str as text
%        str2double(get(hObject,'String')) returns contents of ylim_str as a double
global BEAMLINE

handles=display_output(handles,'resizing plot');
IP=findcells(BEAMLINE,'Name','IP');
IP_s=BEAMLINE{IP}.S;
loaded=load(handles.filename_load);
bpm=loaded.handles.bpm([loaded.handles.stripline; loaded.handles.cband]);
[bpm_ordered,bpm_order]=unique(bpm);
bpm_s=loaded.handles.bpm_s([loaded.handles.stripline; loaded.handles.cband]);
bpm_name=loaded.handles.bpm_name([loaded.handles.stripline; loaded.handles.cband],:);

handles.ylim=eval(get(hObject,'String'));
if (handles.cor_alone)
  figure(1);
  delete(handles.hText);
  set(gcf,'ActionPostCallback',{@figResize,handles})
else
  axes(handles.plot);
  delete(handles.hText);
end
ylim(handles.ylim);
set(gca,'XTick',bpm_s(bpm_order));
set(gca,'XTickLabel',bpm_name(bpm_order,:));
handles.hText=xticklabel_rotate90();
guidata(hObject, handles);


% --- Executes during object creation, after setting all properties.
function ylim_str_CreateFcn(hObject, eventdata, handles)
% hObject    handle to ylim_str (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
  set(handles.stop_btn,'BackgroundColor','white');
end


function handles=display_output(handles,string)
pause(0.2);
nline=size(string,1);
handles.out=strvcat(string(nline:-1:1,:),handles.out);
set(handles.output_txt,'String',handles.out);

function [s,r]=RmatAtoB_cor(ind1,ind2)
global BEAMLINE

L = BEAMLINE{ind1}.L/2;
cormat1 = ...
    [1 L 0 0 0 0;
    0 1 0 0 0 0;
    0 0 1 L 0 0;
    0 0 0 1 0 0;
    0 0 0 0 1 0;
    0 0 0 0 0 1;];
  [stat Amat_wrong] = RmatAtoB(ind1,ind2);
  r = cormat1 \ Amat_wrong;
  s = stat;


% --- Executes on button press in plot_corr_scale_chk.
function plot_corr_scale_chk_Callback(hObject, eventdata, handles)
% hObject    handle to plot_corr_scale_chk (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of plot_corr_scale_chk
  handles.plot_corr_scale=get(hObject,'Value');
  guidata(hObject, handles);


% --- Executes on button press in select_hcorr_btn.
function select_hcorr_btn_Callback(hObject, eventdata, handles)
% hObject    handle to select_hcorr_btn (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
if(isempty(handles.list_xcor_scanned))
    init_sel=handles.nxcor+1;
else
    init_sel=handles.list_xcor_scanned;
end
[selection,ok]=listdlg('PromptString',{'Select horizontal correctors :' '' '(Shift+clik = continuous selection)' '(Crtl+clik = uncontinuous selection)'},...
            'ListString',strvcat(handles.xcor_name,'None'),'ListSize',[250 min((handles.nxcor+1)*15,500)],...
            'InitialValue',init_sel,'Name','Select Hcor');
if(ok)
    if(isempty(find(selection==handles.nxcor+1,1)))
        handles.list_xcor_scanned=selection;
    else
        handles.list_xcor_scanned=[];
    end
    handles=display_output(handles,'horizontal corector(s) selected :');
    for i=handles.list_xcor_scanned
        handles=display_output(handles,handles.xcor_name(i,:));
    end
end

guidata(hObject, handles);


% --- Executes on button press in select_vcorr_btn.
function select_vcorr_btn_Callback(hObject, eventdata, handles)
% hObject    handle to select_vcorr_btn (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
if(isempty(handles.list_ycor_scanned))
    init_sel=handles.nycor+1;
else
    init_sel=handles.list_ycor_scanned;
end
[selection,ok]=listdlg('PromptString',{'Select vertical correctors :' '' '(Shift+clik = continuous selection)' '(Crtl+clik = uncontinuous selection)'},...
            'ListString',strvcat(handles.ycor_name,'None'),'ListSize',[250 min((handles.nycor+1)*15,500)],...
            'InitialValue',init_sel,'Name','Select Vcor');
if(ok)
    if(isempty(find(selection==handles.nycor+1,1)))
        handles.list_ycor_scanned=selection;
    else
        handles.list_ycor_scanned=[];
    end
    handles=display_output(handles,'vertical corector(s) selected :');
    for i=handles.list_ycor_scanned
        handles=display_output(handles,handles.ycor_name(i,:));
    end
end

guidata(hObject, handles);


% --- Executes on button press in select_hmov_btn.
function select_hmov_btn_Callback(hObject, eventdata, handles)
% hObject    handle to select_hmov_btn (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
if(isempty(handles.list_xmover_scanned))
    init_sel=handles.nmover+1;
else
    init_sel=handles.list_xmover_scanned;
end
[selection,ok]=listdlg('PromptString',{'Select horizontal movers :' '' '(Shift+clik = continuous selection)' '(Crtl+clik = uncontinuous selection)'},...
            'ListString',strvcat(handles.mover_name,'None'),'ListSize',[250 min((handles.nmover+1)*15,500)],...
            'InitialValue',init_sel,'Name','Select Hcor');
if(ok)
    if(isempty(find(selection==handles.nmover+1,1)))
        handles.list_xmover_scanned=selection;
    else
        handles.list_xmover_scanned=[];
    end
    handles=display_output(handles,'horizontal mover(s) selected :');
    for i=handles.list_xmover_scanned
        handles=display_output(handles,handles.mover_name(i,:));
    end
end

guidata(hObject, handles);


% --- Executes on button press in select_vmov_btn.
function select_vmov_btn_Callback(hObject, eventdata, handles)
% hObject    handle to select_vmov_btn (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
if(isempty(handles.list_ymover_scanned))
    init_sel=handles.nmover+1;
else
    init_sel=handles.list_ymover_scanned;
end
[selection,ok]=listdlg('PromptString',{'Select horizontal movers :' '' '(Shift+clik = continuous selection)' '(Crtl+clik = uncontinuous selection)'},...
            'ListString',strvcat(handles.mover_name,'None'),'ListSize',[250 min((handles.nmover+1)*15,500)],...
            'InitialValue',init_sel,'Name','Select Hcor');
if(ok)
    if(isempty(find(selection==handles.nmover+1,1)))
        handles.list_ymover_scanned=selection;
    else
        handles.list_ymover_scanned=[];
    end
    handles=display_output(handles,'vertical mover(s) selected :');
    for i=handles.list_ymover_scanned
        handles=display_output(handles,handles.mover_name(i,:));
    end
end

guidata(hObject, handles);


% --- Executes on button press in stop_btn.
function stop_btn_Callback(hObject, eventdata, handles)
% hObject    handle to stop_btn (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
set(handles.stop_btn,'BackgroundColor',[1 0 0]);
guidata(hObject, handles);


% --- If Enable == 'on', executes on mouse press in 5 pixel border.
% --- Otherwise, executes on mouse press in 5 pixel border or over stop_btn.
function stop_btn_ButtonDownFcn(hObject, eventdata, handles)
% hObject    handle to stop_btn (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

function SVD_matrix=get_SVD_matrix(read_svd,bpm_used_svd_inbpm)
% SVD_matrix            matrix such as if multiplied by the left with 
%                       bpm_used_svd_inbpm BPM readings return readings for
%                       all handles.bpm for the lattice used getting
%                       read_svd.
% read_svd              BPM readings 1 rows= 1 pulses, 1 column = 1 BPM
% bpm_used_svd_inbpm    BPMs used to remove jitter counted as indices of
%                       handles.bpm

    global BEAMLINE;
    global INSTR;

    handles.IEX=findcells(BEAMLINE,'Name','IEX');
    handles.bpm=setdiff(findcells(BEAMLINE,'Class','MONI'),findcells(BEAMLINE,'Name','FONT*')); % all MONIs except FONT pickups ...
    handles.bpm=handles.bpm(handles.bpm>handles.IEX); % ... downstream of IEX
    handles.bpm_index=zeros(size(handles.bpm));
    for n=1:length(handles.bpm)
      handles.bpm_index(n)=findcells(INSTR,'Index',handles.bpm(n));
    end

    bpm_used_svd=handles.bpm(bpm_used_svd_inbpm);
    bpm_used_svd_index=handles.bpm_index(bpm_used_svd_inbpm);
    bpm_used=handles.bpm;
    bpm_used_index=handles.bpm_index;

    nbpm=size(read_svd,2)/2;
    nbpm_svd=length(bpm_used_svd);
    SVD_matrix=zeros(2*nbpm_svd+1,2*nbpm);
    for bpm=1:nbpm
        if(~any(bpm==bpm_used_svd_inbpm))
            SVD_matrix(:,bpm)= ...
                   lscov([read_svd(:,[bpm_used_svd_inbpm nbpm+bpm_used_svd_inbpm]) ones(100,1)],read_svd(:,bpm));
        end        
    end
    for i=1:nbpm_svd
        SVD_matrix([1:i-1 i+1:nbpm_svd+i-1 nbpm_svd+i+1:2*nbpm_svd+1],bpm_used_svd_inbpm(i))=...
            lscov([read_svd(:,[bpm_used_svd_inbpm([1:i-1 i+1:nbpm_svd]) nbpm+bpm_used_svd_inbpm([1:i-1 i+1:nbpm_svd])]) ones(100,1)],read_svd(:,bpm_used_svd_inbpm(i)));
    end
    for bpm=1:nbpm
        if(~any(bpm==bpm_used_svd_inbpm))
            SVD_matrix(:,nbpm+bpm)= ...
                   lscov([read_svd(:,[bpm_used_svd_inbpm nbpm+bpm_used_svd_inbpm]) ones(100,1)],read_svd(:,nbpm+bpm));
        end        
    end
    for i=1:nbpm_svd
        SVD_matrix([1:i-1 i+1:nbpm_svd+i-1 nbpm_svd+i+1:2*nbpm_svd+1],bpm_used_svd_inbpm(i)+nbpm)=...
            lscov([read_svd(:,[bpm_used_svd_inbpm([1:i-1 i+1:nbpm_svd]) nbpm+bpm_used_svd_inbpm([1:i-1 i+1:nbpm_svd])]) ones(100,1)],read_svd(:,bpm_used_svd_inbpm(i)+nbpm));
    end

function bpm_read_svd_sub=remove_jitter_svd(bpm_read,read_svd,bpm_used_svd_inbpm,handles)
    
    bpm_used=handles.bpm;

    SVD_matrix=get_SVD_matrix(read_svd,bpm_used_svd_inbpm);
    nbpm=length(bpm_used);
    nreadings=size(bpm_read,1);
    bpm_read_svd_sub=bpm_read-[bpm_read(:,[bpm_used_svd_inbpm nbpm+bpm_used_svd_inbpm]) ones(nreadings,1)]*SVD_matrix;

function bpm_read_svd_sub=remove_jitter_svd_auto(handles,bpm_read,read_svd,corrector_used)
    
    bpm_used=handles.bpm;
    nbpm=length(bpm_used);
    if(size(bpm_read>2*nbpm))
        raw=bpm_read;
        bpm_read=[raw(:,handles.bpm_index*3-1) raw(:,handles.bpm_index*3)];
    end
    nreadings=size(bpm_read,1);
    if(exist('corrector_used'))
        if(length(corrector_used)>1)
            corrector_used=corrector_used(1);
        end
        bpm_used_svd_inbpm=find(bpm_used<corrector_used);
    else
        bpm_used_svd_inbpm=1:nbpm;
    end
    
    SVD_matrix=get_SVD_matrix(read_svd,bpm_used_svd_inbpm);
    bpm_read_svd_sub=bpm_read-[bpm_read(:,[bpm_used_svd_inbpm nbpm+bpm_used_svd_inbpm]) ones(nreadings,1)]*SVD_matrix;

    


% --- Executes on button press in svd_jitter_subtraction_check.
function svd_jitter_subtraction_check_Callback(hObject, eventdata, handles)
% hObject    handle to svd_jitter_subtraction_check (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of svd_jitter_subtraction_check
handles.svd_jitter_subtraction=get(hObject,'Value');
guidata(hObject, handles);


function figResize(src,evt,handles)
handles.xlim=xlim();
handles.ylim=ylim();
set(handles.xlim_str,'String',sprintf('[%.3g %.3g]',handles.xlim(1),handles.xlim(2)));
set(handles.ylim_str,'String',sprintf('[%.3g %.3g]',handles.ylim(1),handles.ylim(2)));
[xt,id]=sort(handles.bpm_s);
xtl=handles.bpm_name;xtl=xtl(id);
set(gca,'XTick',xt,'XTickLabel',xtl)
grid;
handles.hText=xticklabel_rotate90();

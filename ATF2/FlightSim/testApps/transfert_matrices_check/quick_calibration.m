function quick_calibration()
global BEAMLINE;
global INSTR;
global PS;
global GIRDER;

% to use this code you need to edit :
% what correctors are used :
list_xcor_scanned=[];
list_ycor_scanned=[];
% what strength are used during the scan :
list_xcor_dstrength=[-1e-4 0 1e-4];
list_ycor_dstrength=[-5e-5 0 5e-5];
%list of movers scanned :
list_xmover_scanned=[4];
list_ymover_scanned=[1];
%list of positions of the scan :
list_xmover_dpos=[-1e-4 0 1e-4];
list_ymover_dpos=[-1e-4 0 1e-4];

FlHwUpdate();

%get bpm info
IEX=findcells(BEAMLINE,'Name','IEX');
BS1XA=findcells(BEAMLINE,'Name','BS1XA');
bpm=[findcells(BEAMLINE,'Name','MB*X') findcells(BEAMLINE,'Name','MQ*X') findcells(BEAMLINE,'Name','MQ*FF') findcells(BEAMLINE,'Name','MS*FF')];
ICT=findcells(BEAMLINE,'Name','ICT1X');
ICT_index=findcells(INSTR,'Index',ICT);
bpm_name='';
bpm_type='';
bpm_s=[];
bpm_index=[];
for i=bpm
    bpm_name=strvcat(bpm_name,BEAMLINE{i}.Name);
    bpm_s(end+1)=BEAMLINE{i}.S;
    bpm_index(end+1)=findcells(INSTR,'Index',i);
    bpm_type=strvcat(bpm_type,INSTR{bpm_index(end)}.Type);
end
stripline=[strmatch('MB',cellstr(bpm_name)); find(strcmp('stripline',cellstr(bpm_type)))].';
nstripline=length(stripline);
stripline_name=bpm_name(stripline,:);
stripline_index=bpm_index(stripline);

cband=find(strcmp('ccav',cellstr(bpm_type))).';
ncband=length(cband);
cband_name=bpm_name(cband,:);
cband_index=bpm_index(cband);

%get correctors info
xcor_temp=[findcells(BEAMLINE, 'Name','ZS*X') findcells(BEAMLINE, 'Name','ZH*X') findcells(BEAMLINE, 'Name','ZH*FF')];
xcor_name='';
nxcor=length(xcor_temp);
xcor=[];
for i=1:nxcor
    if (strcmp(BEAMLINE{xcor_temp(i)}.Class, 'XCOR') && xcor_temp(i)>BS1XA)
        xcor(end+1)=xcor_temp(i);
    end
end
nxcor=length(xcor);
xcor_ps=zeros(1,nxcor);
xcor_ps_read=zeros(1,nxcor);
xcor_B=zeros(1,nxcor);
xcor_S=zeros(1,nxcor);
for i=1:nxcor
    xcor_ps(i)=BEAMLINE{xcor(i)}.PS;
    xcor_B(i)=BEAMLINE{xcor(i)}.B;
    xcor_S(i)=BEAMLINE{xcor(i)}.S;
    xcor_name=strvcat(xcor_name, BEAMLINE{xcor(i)}.Name);
end
xcor_S=xcor_S-BEAMLINE{IEX}.S;
FlHwUpdate();
for i=1:nxcor
    xcor_ps_read(i)=PS(xcor_ps(i)).Ampl;
end

ycor_temp=[findcells(BEAMLINE, 'Name','ZV*X') findcells(BEAMLINE, 'Name','ZV*FF')];
ycor_name='';
nycor=length(ycor_temp);
ycor=[];
for i=1:nycor
    if (strcmp(BEAMLINE{ycor_temp(i)}.Class, 'YCOR') && ycor_temp(i)>BS1XA)
        ycor(end+1)=ycor_temp(i);
    end
end
nycor=length(ycor);
ycor_ps=zeros(1,nycor);
ycor_ps_read=zeros(1,nycor);
ycor_B=zeros(1,nycor);
ycor_S=zeros(1,nycor);
for i=1:nycor
    ycor_ps(i)=BEAMLINE{ycor(i)}.PS;
    ycor_B(i)=BEAMLINE{ycor(i)}.B;
    ycor_S(i)=BEAMLINE{ycor(i)}.S;
    ycor_name=strvcat(ycor_name, BEAMLINE{ycor(i)}.Name);
end
ycor_S=ycor_S-BEAMLINE{IEX}.S;
FlHwUpdate();
for i=1:nycor
    ycor_ps_read(i)=PS(ycor_ps(i)).Ampl;
end

%get movers info
mover=28:47;
nmover=length(mover);
mover_name='';
mover_pos=zeros(nmover,3);
mover_S=zeros(nmover,1);
xmover_KL=zeros(1,nmover);
ymover_KL=zeros(1,nmover);

FlHwUpdate();
for i=1:nmover
    elems=findcells(BEAMLINE,'Girder',mover(i));
    if(strcmp(BEAMLINE{elems(1)}.Class,'MONI'))
        elems(1)=[];
    end
    mover_S(i)=BEAMLINE{elems(1)}.S-BEAMLINE{IEX}.S;
    mover_name=strvcat(mover_name,BEAMLINE{elems(1)}.Name);
    elem_on_mover=findcells(BEAMLINE,'Girder',mover(i));
    q_on_mover(i,:)=findcells(BEAMLINE,'Class','QUAD',elem_on_mover(1),elem_on_mover(end));
    bpm_on_mover(i)=findcells(BEAMLINE,'Class','MONI',elem_on_mover(1),elem_on_mover(end));
    for quad=q_on_mover(i,:)
        xmover_KL(i)=xmover_KL(i)+BEAMLINE{quad}.B*PS(BEAMLINE{quad}.PS).Ampl*0.2998/BEAMLINE{quad}.P;
        ymover_KL(i)=ymover_KL(i)+BEAMLINE{quad}.B*PS(BEAMLINE{quad}.PS).Ampl*0.2998/BEAMLINE{quad}.P;
    end
    mover_pos(i,:)=GIRDER{mover(i)}.MoverPos;
end
mover_S=mover_S-BEAMLINE{IEX}.S;


[stat reqID] = AccessRequest({unique(mover([list_xmover_scanned list_ymover_scanned])) [xcor_ps(list_xcor_scanned) ycor_ps(list_ycor_scanned)] []});
if(stat{1}==-1)
    disp(sprintf('AccessRequest reurned error :%s',stat{2}));
else
    disp(sprintf('Access granted, ID of request : %s',reqID));
end

xread_dx=[];
yread_dx=[];
iread_dx=[];
dxread_dx=[];
dyread_dx=[];
diread_dx=[];
rawread_dx=[];

xread_dy=[];
yread_dy=[];
iread_dy=[];
dxread_dy=[];
dyread_dy=[];
diread_dy=[];
rawread_dy=[];

xread_dposx=[];
yread_dposx=[];
iread_dposx=[];
dxread_dposx=[];
dyread_dposx=[];
diread_dposx=[];
rawread_dposx=[];

xread_dposy=[];
yread_dposy=[];
iread_dposy=[];
dxread_dposy=[];
dyread_dposy=[];
diread_dposy=[];
rawread_dposy=[];

for xcor_scanned=list_xcor_scanned
    for xcor_dstrength=list_xcor_dstrength
        fprintf('set %s at %f mrad\n',xcor_name(xcor_scanned,:),xcor_ps_read(xcor_scanned)+xcor_dstrength);
        ps_newvalue=xcor_ps_read(xcor_scanned)+xcor_dstrength;
        PS(xcor_ps(xcor_scanned)).SetPt=ps_newvalue;
        stat=PSTrim(xcor_ps(xcor_scanned), true);
        if(stat{1}==-1)
            disp(sprintf('PSTrim returned error :%s',stat{2}));
        end
        
        FlHwUpdate();
        
        %if more than 5% difference, ask again
        itteration=0;
        while( abs((PS(xcor_ps(xcor_scanned)).Ampl-ps_newvalue)./ps_newvalue)>=.05 )
            itteration=itteration+1;
            %give up after 5 asks
            if(itteration>5)
                disp(sprintf('Warning : PS %i is set at %f instead of %f.',xcor_ps(xcor_scanned),PS(xcor_ps(xcor_scanned)).Ampl,ps_newvalue));
                break;
            end

            %ask to change different ps from what is wanted
            stat=PSTrim(xcor_ps(xcor_scanned), true);
            if(stat{1}==-1)
                disp(sprintf('PSTrim reurned error :%s',stat{2}));
            end
            pause(1);

            %update ps values
            FlHwUpdate();
        end
        
        FlHwUpdate('wait',10);
        disp('BPM measured');
%        [s,output]=FlHwUpdate('bpmave',10);
        [s,raw]=FlHwUpdate('readbuffer',10);
        [s,output]=bpmave(raw,10);
%output = {[meanData.h; meanData.v; meanData.t; rmsData.h; rmsData.v; rmsData.t; nbad.h; nbad.v; nbad.t] badbpms};
          
        xread_dx(end+1,:)=output{1}(1,[stripline_index cband_index ]);
        yread_dx(end+1,:)=output{1}(2,[stripline_index cband_index ]);
        iread_dx(end+1,:)=output{1}(3,[stripline_index cband_index ]);
        dxread_dx(end+1,:)=output{1}(4,[stripline_index cband_index ]);
        dyread_dx(end+1,:)=output{1}(5,[stripline_index cband_index ]);
        diread_dx(end+1,:)=output{1}(6,[stripline_index cband_index ]);
        rawread_dx(end+1:end+10,:)=raw;
    end
%reset corector to initial value
    fprintf('set %s back at %f mrad\n',xcor_name(xcor_scanned,:),xcor_ps_read(xcor_scanned));
    PS(xcor_ps(xcor_scanned)).SetPt=xcor_ps_read(xcor_scanned);
    stat=PSTrim(xcor_ps(xcor_scanned), true);
    if(stat{1}==-1)
        disp(sprintf('PSTrim returned error :%s',stat{2}));
    end
    while( abs((PS(xcor_ps(xcor_scanned)).Ampl-xcor_ps_read(xcor_scanned))./xcor_ps_read(xcor_scanned))>=.05 )
        itteration=itteration+1;
        %give up after 5 asks
        if(itteration>5)
            disp(sprintf('Warning : PS %i is set at %f instead of %f.',xcor_ps(xcor_scanned),PS(xcor_ps(xcor_scanned)).Ampl,xcor_ps_read(xcor_scanned)));
            break;
        end

        %ask to change different ps from what is wanted
        stat=PSTrim(xcor_ps(xcor_scanned), true);
        if(stat{1}==-1)
            disp(sprintf('PSTrim reurned error :%s',stat{2}));
        end
        pause(1);

        %update ps values
        FlHwUpdate();
    end
end        

for ycor_scanned=list_ycor_scanned
    for ycor_dstrength=list_ycor_dstrength
        fprintf('set %s at %f mrad\n',ycor_name(ycor_scanned,:),ycor_ps_read(ycor_scanned)+ycor_dstrength);
        ps_newvalue=ycor_ps_read(ycor_scanned)+ycor_dstrength;
        PS(ycor_ps(ycor_scanned)).SetPt=ps_newvalue;
        stat=PSTrim(ycor_ps(ycor_scanned), true);
        if(stat{1}==-1)
            disp(sprintf('PSTrim returned error :%s',stat{2}));
        end

        FlHwUpdate();
        
        %if more than 5% difference, ask again
        itteration=0;
        while( abs((PS(ycor_ps(ycor_scanned)).Ampl-ps_newvalue)./ps_newvalue)>=.05 )
            itteration=itteration+1;
            %give up after 5 asks
            if(itteration>5)
                disp(sprintf('Warning : PS %i is set at %f instead of %f.',ycor_ps(ycor_scanned),PS(ycor_ps(ycor_scanned)).Ampl,ps_newvalue));
                break;
            end

            %ask to change different ps from what is wanted
            stat=PSTrim(ycor_ps(ycor_scanned), true);
            if(stat{1}==-1)
                disp(sprintf('PSTrim reurned error :%s',stat{2}));
            end
            pause(1);

            %update ps values
            FlHwUpdate();
        end

        FlHwUpdate('wait',10);
        disp('BPM measured');
%        [s,output]=FlHwUpdate('bpmave',10);
        [s,raw]=FlHwUpdate('readbuffer',10);
        [s,output]=bpmave(raw,10);
%output = {[meanData.h; meanData.v; meanData.t; rmsData.h; rmsData.v; rmsData.t; nbad.h; nbad.v; nbad.t] badbpms};
          
        xread_dy(end+1,:)=output{1}(1,[stripline_index cband_index ]);
        yread_dy(end+1,:)=output{1}(2,[stripline_index cband_index ]);
        iread_dy(end+1,:)=output{1}(3,[stripline_index cband_index ]);
        dxread_dy(end+1,:)=output{1}(4,[stripline_index cband_index ]);
        dyread_dy(end+1,:)=output{1}(5,[stripline_index cband_index ]);
        diread_dy(end+1,:)=output{1}(6,[stripline_index cband_index ]);
        rawread_dy(end+1:end+10,:)=raw;
    end
%reset corector to initial value
    fprintf('set %s back at %f mrad\n',ycor_name(ycor_scanned,:),ycor_ps_read(ycor_scanned));
    PS(ycor_ps(ycor_scanned)).SetPt=ycor_ps_read(ycor_scanned);
    stat=PSTrim(ycor_ps(ycor_scanned), true);
    if(stat{1}==-1)
        disp(sprintf('PSTrim returned error :%s',stat{2}));
    end
    while( abs((PS(ycor_ps(ycor_scanned)).Ampl-ycor_ps_read(ycor_scanned))./ycor_ps_read(ycor_scanned))>=.05 )
        itteration=itteration+1;
        %give up after 5 asks
        if(itteration>5)
            disp(sprintf('Warning : PS %i is set at %f instead of %f.',ycor_ps(ycor_scanned),PS(ycor_ps(ycor_scanned)).Ampl,ycor_ps_read(ycor_scanned)));
            break;
        end

        %ask to change different ps from what is wanted
        stat=PSTrim(ycor_ps(ycor_scanned), true);
        if(stat{1}==-1)
            disp(sprintf('PSTrim reurned error :%s',stat{2}));
        end
        pause(1);

        %update ps values
        FlHwUpdate();
    end
end

for xmover_scanned=list_xmover_scanned
    for xmover_dpos=list_xmover_dpos
        fprintf('set %s at x=%f um\n',mover_name(xmover_scanned,:),(mover_pos(xmover_scanned,1)+xmover_dpos)*1e6);
        pos_newvalue=mover_pos(xmover_scanned,:)+[xmover_dpos 0 0];
        GIRDER{mover(xmover_scanned)}.MoverSetPt=pos_newvalue;
        stat=MoverTrim(mover(xmover_scanned), true);
        if(stat{1}==-1)
            disp(sprintf('MoverTrim returned error :%s',stat{2}));
        end
        pause(10);
        FlHwUpdate();
        
        %if more than 5% difference, ask again
        itteration=0;
        while( abs((GIRDER{mover(xmover_scanned)}.MoverPos(1)-pos_newvalue(1))./pos_newvalue(1))>=.05 )
            itteration=itteration+1;
            %give up after 5 asks
            if(itteration>5)
                disp(sprintf('Warning : mover %s is set at x=%f um instead of %f um.',mover_name(xmover_scanned,:),GIRDER{mover(xmover_scanned)}.MoverPos(1)*1e6,pos_newvalue(1)*1e6));
                break;
            end

            %ask to change different pos from what is wanted
            stat=MoverTrim(mover(xmover_scanned), true);
            if(stat{1}==-1)
                disp(sprintf('MoverTrim reurned error :%s',stat{2}));
            end

            %update pos values
            pause(10);
            FlHwUpdate();
        end

        FlHwUpdate('wait',10);
        disp('BPM measured');
%        [s,output]=FlHwUpdate('bpmave',10);
        [s,raw]=FlHwUpdate('readbuffer',10);
        [s,output]=bpmave(raw,10);
%output = {[meanData.h; meanData.v; meanData.t; rmsData.h; rmsData.v; rmsData.t; nbad.h; nbad.v; nbad.t] badbpms};
          
        xread_dposx(end+1,:)=output{1}(1,[stripline_index cband_index ]);
        yread_dposx(end+1,:)=output{1}(2,[stripline_index cband_index ]);
        iread_dposx(end+1,:)=output{1}(3,[stripline_index cband_index ]);
        dxread_dposx(end+1,:)=output{1}(4,[stripline_index cband_index ]);
        dyread_dposx(end+1,:)=output{1}(5,[stripline_index cband_index ]);
        diread_dposx(end+1,:)=output{1}(6,[stripline_index cband_index ]);
        rawread_dposx(end+1:end+10,:)=raw;
    end

% Set back the mover

    fprintf('set back %s at x=%f um\n',mover_name(xmover_scanned,:),(mover_pos(xmover_scanned,1))*1e6);
    pos_newvalue=mover_pos(xmover_scanned,:);
    GIRDER{mover(xmover_scanned)}.MoverSetPt=pos_newvalue;
    stat=MoverTrim(mover(xmover_scanned), true);
    if(stat{1}==-1)
        disp(sprintf('MoverTrim returned error :%s',stat{2}));
    end

    pause(10);
    FlHwUpdate();
     %if more than 5% difference, ask again
    itteration=0;
    while( abs((GIRDER{mover(xmover_scanned)}.MoverPos(1)-pos_newvalue(1))./pos_newvalue(1))>=.05 )
        itteration=itteration+1;
        %give up after 5 asks
        if(itteration>5)
            disp(sprintf('Warning : mover %s is set at x=%f um instead of %f um.',mover_name(xmover_scanned,:),GIRDER{mover(xmover_scanned)}.MoverPos(1)*1e6,pos_newvalue(1)*1e6));
            break;
        end
         %ask to change different pos from what is wanted
        stat=MoverTrim(mover(xmover_scanned), true);
        if(stat{1}==-1)
            disp(sprintf('MoverTrim reurned error :%s',stat{2}));
        end
         %update pos values
        pause(10);
    end
 end

for ymover_scanned=list_ymover_scanned
    for ymover_dpos=list_ymover_dpos
        fprintf('set %s at y=%f um\n',mover_name(ymover_scanned,:),(mover_pos(ymover_scanned,2)+ymover_dpos)*1e6);
        pos_newvalue=mover_pos(ymover_scanned,:)+[0 ymover_dpos 0];
        GIRDER{mover(ymover_scanned)}.MoverSetPt=pos_newvalue;
        stat=MoverTrim(mover(ymover_scanned), true);
        if(stat{1}==-1)
            disp(sprintf('MoverTrim returned error :%s',stat{2}));
        end

        pause(10);
        FlHwUpdate();
        
        %if more than 5% difference, ask again
        itteration=0;
        while( abs((GIRDER{mover(ymover_scanned)}.MoverPos(2)-pos_newvalue(2))./pos_newvalue(2))>=.05 )
            itteration=itteration+1;
            %give up after 5 asks
            if(itteration>5)
                disp(sprintf('Warning : mover %s is set at y=%f um instead of %f um.',mover_name(ymover_scanned,:),GIRDER{mover(ymover_scanned)}.MoverPos(2)*1e6,pos_newvalue(2)*1e6));
                break;
            end

            %ask to change different pos from what is wanted
            stat=MoverTrim(mover(ymover_scanned), true);
            if(stat{1}==-1)
                disp(sprintf('MoverTrim reurned error :%s',stat{2}));
            end

            %update pos values
            pause(10);
            FlHwUpdate();
        end

        FlHwUpdate('wait',10);
        disp('BPM measured');
%        [s,output]=FlHwUpdate('bpmave',10);
        [s,raw]=FlHwUpdate('readbuffer',10);
        [s,output]=bpmave(raw,10);
%output = {[meanData.h; meanData.v; meanData.t; rmsData.h; rmsData.v; rmsData.t; nbad.h; nbad.v; nbad.t] badbpms};
          
        xread_dposy(end+1,:)=output{1}(1,[stripline_index cband_index ]);
        yread_dposy(end+1,:)=output{1}(2,[stripline_index cband_index ]);
        iread_dposy(end+1,:)=output{1}(3,[stripline_index cband_index ]);
        dxread_dposy(end+1,:)=output{1}(4,[stripline_index cband_index ]);
        dyread_dposy(end+1,:)=output{1}(5,[stripline_index cband_index ]);
        diread_dposy(end+1,:)=output{1}(6,[stripline_index cband_index ]);
        rawread_dposy(end+1:end+10,:)=raw;
    end
    
% Set back the mover
    
     fprintf('set back %s at y=%f um\n',mover_name(ymover_scanned,:),(mover_pos(ymover_scanned,2))*1e6);
     pos_newvalue=mover_pos(ymover_scanned,:);
     GIRDER{mover(ymover_scanned)}.MoverSetPt=pos_newvalue;
     stat=MoverTrim(mover(ymover_scanned), true);
     if(stat{1}==-1)
         disp(sprintf('MoverTrim returned error :%s',stat{2}));
     end
     pause(10);
     FlHwUpdate();
 
     %if more than 5% difference, ask again
     itteration=0;
     while( abs((GIRDER{mover(ymover_scanned)}.MoverPos(2)-pos_newvalue(2))./pos_newvalue(2))>=.05 )
         itteration=itteration+1;
         %give up after 5 asks
         if(itteration>5)
             disp(sprintf('Warning : mover %s is set at y=%f um instead of %f um.',mover_name(ymover_scanned,:),GIRDER{mover(ymover_scanned)}.MoverPos(2)*1e6,pos_newvalue(2)*1e6));
             break;
         end
 
         %ask to change different pos from what is wanted
         stat=MoverTrim(mover(ymover_scanned), true);
         if(stat{1}==-1)
             disp(sprintf('MoverTrim reurned error :%s',stat{2}));
         end

         %update pos values
         pause(10);
     end
end

AccessRequest('release',reqID);
if(stat{1}==-1)
    disp(sprintf('Error durring access release, reurned error :%s',stat{2}));
else
    disp(sprintf('Access %s released',reqID));
end

filename=datestr(now,30);
save(filename);

fprintf('file %s createded\n',filename);

analyse_result(filename);
end

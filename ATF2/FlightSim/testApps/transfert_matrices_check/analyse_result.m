function analyse_result(filename)
%ANALISE_RESULT Summary of this function goes here
%   Detailed explanation goes here

load('-mat',filename);

color=['r' 'b' 'c' 'm '];

%yread_dy(:,1:14)=-yread_dy(:,1:14); %invert button and striplines
%yread_dy(:,24:end)=-yread_dy(:,24:end); %invert cavity on movers
%yread_dy=-yread_dy;

if (~exist('list_xmover_scanned'))
    list_xmover_scanned=[];
else
    xmover_scanned=mover(list_xmover_scanned);
    xmover_scanned_name=mover_name(list_xmover_scanned,:);
end
nxmover_scanned=length(list_xmover_scanned);

if (~exist('list_ymover_scanned'))
    list_ymover_scanned=[];
else
    ymover_scanned=mover(list_ymover_scanned);
    ymover_scanned_name=mover_name(list_ymover_scanned,:);    
end
nymover_scanned=length(list_ymover_scanned);

plot_per_fig=15;
half_plot_per_fig=round(plot_per_fig/2);
bpm_measured=bpm([stripline cband]);
bpm_measured_index=[stripline_index cband_index];
bpm_measured_name=strvcat(stripline_name,cband_name);
nbpm_measured=length(bpm_measured);

xcor_scanned=xcor(list_xcor_scanned);
xcor_scanned_name=xcor_name(list_xcor_scanned,:);
nxcor_scanned=length(list_xcor_scanned);
ycor_scanned=ycor(list_ycor_scanned);
ycor_scanned_name=ycor_name(list_ycor_scanned,:);
nycor_scanned=length(list_ycor_scanned);

A=zeros(nbpm_measured,6,nxcor_scanned);
B=zeros(nbpm_measured,6,nxcor_scanned);
C=zeros(nbpm_measured,6,nycor_scanned);
D=zeros(nbpm_measured,6,nycor_scanned);
E=zeros(nbpm_measured,nxmover_scanned);
F=zeros(nbpm_measured,nxmover_scanned);
G=zeros(nbpm_measured,nymover_scanned);
H=zeros(nbpm_measured,nymover_scanned);

for j=1:nxcor_scanned
    for i=1:nbpm_measured
        if(xcor_scanned(j)<bpm_measured(i))
          [s,r]=RmatAtoB(xcor_scanned(j),bpm_measured(i));
        else
            r=zeros(6,6);
        end
        A(i,:,j)=r(1,:);
        B(i,:,j)=r(3,:);
    end
    dxmodel(:,:,j)=A(:,2,j)*list_xcor_dstrength;
end
for j=1:nycor_scanned
    for i=1:nbpm_measured
        if(ycor_scanned(j)<bpm_measured(i))
          [s,r]=RmatAtoB(ycor_scanned(j),bpm_measured(i));
        else
            r=zeros(6,6);
        end
        C(i,:,j)=r(1,:);
        D(i,:,j)=r(3,:);
    end
    dymodel(:,:,j)=D(:,4,j)*list_ycor_dstrength;
end

for j=1:nxmover_scanned
    for i=1:nbpm_measured
        if(bpm_on_mover(list_xmover_scanned(j))<bpm_measured(i))
          [s,r]=RmatAtoB(q_on_mover(list_xmover_scanned(j),1),bpm_measured(i));
          E(i,j)=r(1,2)*xmover_KL(list_xmover_scanned(j));
          F(i,j)=r(3,2)*xmover_KL(list_xmover_scanned(j));
        else
            if(bpm_on_mover(list_xmover_scanned(j))==bpm_measured(i))
                E(i,j)=-1;
                F(i,j)=0;
            else
                E(i,j)=0;
                F(i,j)=0;
            end
        end
        
    end
end
for j=1:nymover_scanned
    for i=1:nbpm_measured
        if(bpm_on_mover(list_ymover_scanned(j))<bpm_measured(i))
            [s,r]=RmatAtoB(q_on_mover(list_ymover_scanned(j),1),bpm_measured(i));
            G(i,j)=r(1,4)*-ymover_KL(list_ymover_scanned(j));
            H(i,j)=r(3,4)*-ymover_KL(list_ymover_scanned(j));
        else
            if(bpm_on_mover(list_ymover_scanned(j))==bpm_measured(i))
                G(i,j)=0;
                H(i,j)=-1;
            else
                G(i,j)=0;
                H(i,j)=0;
            end
        end
    end
end
q_x_dx=zeros(nbpm_measured,2,nxcor_scanned);
dq_x_dx=zeros(nbpm_measured,2,nxcor_scanned);
q_y_dx=zeros(nbpm_measured,2,nxcor_scanned);
dq_y_dx=zeros(nbpm_measured,2,nxcor_scanned);
q_y_dy=zeros(nbpm_measured,2,nycor_scanned);
dq_x_dy=zeros(nbpm_measured,2,nycor_scanned);
q_x_dy=zeros(nbpm_measured,2,nycor_scanned);
dq_y_dy=zeros(nbpm_measured,2,nycor_scanned);
%nfig=round((nbpm_measured-half_plot_per_fig)/plot_per_fig)+(nxcor_scanned-1)*(round((nbpm_measured-half_plot_per_fig)/plot_per_fig)+1)+1;
% for i=1:nbpm_measured
%     for j=1:nxcor_scanned
%         fig=round((i-half_plot_per_fig)/plot_per_fig)+(j-1)*(round((nbpm_measured-half_plot_per_fig)/plot_per_fig)+1)+1;
%         sub=i-plot_per_fig*round((i-half_plot_per_fig)/plot_per_fig);
% %        fprintf('fig %i sub %i %s(x)=f(%s)\n',fig,sub,bpm_measured_name(i,:),xcor_scanned_name(j,:));
%         figure(fig)
%         subplot(3,5,sub);
%         [q_x_dx(i,:,j),dq_x_dx(i,:,j)]=noplot_polyfit(list_xcor_dstrength,xread_dx((j-1)*5+(1:5),i),dxread_dx((j-1)*5+[1:5],i),1);
%         [q_y_dx(i,:,j),dq_y_dx(i,:,j)]=noplot_polyfit(list_xcor_dstrength,yread_dx((j-1)*5+(1:5),i),dyread_dx((j-1)*5+[1:5],i),1);
%         errorbar(list_xcor_dstrength,xread_dx((j-1)*5+[1:5],i),dxread_dx((j-1)*5+[1:5],i),'ro');
%         hold('on');
%         plot(list_xcor_dstrength,q_x_dx(i,1,j)+ q_x_dx(i,2,j)*list_xcor_dstrength,'r-');
%         plot(list_xcor_dstrength,q_x_dx(i,1,j)+ dxmodel(i,:,j),'b-');
%         hold('off');
%         title(sprintf('%s(x)=f(%s)',bpm_measured_name(i,:),xcor_scanned_name(j,:)));
%     end
%     for j=1:length(list_ycor_scanned)
%         fig=round((i-half_plot_per_fig)/plot_per_fig)+(j-1)*(round((nbpm_measured-half_plot_per_fig)/plot_per_fig)+1)+nfig+1;
%         sub=i-plot_per_fig*round((i-half_plot_per_fig)/plot_per_fig);
% %        fprintf('fig %i sub %i %s(x)=f(%s)\n',fig,sub,bpm_measured_name(i,:),xcor_scanned_name(j,:));
%         figure(fig)
%         subplot(3,5,sub);
%         [q_x_dy(i,:,j),dq_x_dy(i,:,j)]=noplot_polyfit(list_ycor_dstrength,xread_dy((j-1)*5+(1:5),i),dxread_dy((j-1)*5+[1:5],i),1);
%         [q_y_dy(i,:,j),dq_y_dy(i,:,j)]=noplot_polyfit(list_ycor_dstrength,yread_dy((j-1)*5+(1:5),i),dyread_dy((j-1)*5+[1:5],i),1);
%         errorbar(list_ycor_dstrength,yread_dy((j-1)*5+[1:5],i),dyread_dy((j-1)*5+[1:5],i),'ro');
%         hold('on');
%         plot(list_ycor_dstrength,q_y_dy(i,1,j)+ q_y_dy(i,2,j)*list_ycor_dstrength,'r-');
%         plot(list_ycor_dstrength,q_y_dy(i,1,j)+ dymodel(i,:,j),'b-');
%         hold('off');
%         title(sprintf('%s(x)=f(%s)',bpm_measured_name(i,:),ycor_scanned_name(j,:)));
%     end
% end

nfig=round((nbpm_measured-half_plot_per_fig)/plot_per_fig)+1;
nstrengthx=length(list_xcor_dstrength);
nstrengthy=length(list_ycor_dstrength);
nposx=length(list_xmover_dpos);
nposy=length(list_ymover_dpos);

for i=1:nbpm_measured
        fprintf('%s(x) ',bpm_measured_name(i,:))
    for j=1:nxcor_scanned
        fig=round((i-half_plot_per_fig)/plot_per_fig)+1;
        sub=i-plot_per_fig*round((i-half_plot_per_fig)/plot_per_fig);
%        fprintf('fig %i sub %i %s(x)=f(%s)\n',fig,sub,bpm_measured_name(i,:),xcor_scanned_name(j,:));
        figure(fig)
        subplot(3,5,sub);
        if(j==1) 
            cla('reset');
        end
        
        [q_x_dx(i,:,j),dq_x_dx(i,:,j)]=noplot_polyfit(list_xcor_dstrength,xread_dx((j-1)*nstrengthx+(1:nstrengthx),i),dxread_dx((j-1)*nstrengthx+[1:nstrengthx],i),1);
        [q_y_dx(i,:,j),dq_y_dx(i,:,j)]=noplot_polyfit(list_xcor_dstrength,yread_dx((j-1)*nstrengthx+(1:nstrengthx),i),dyread_dx((j-1)*nstrengthx+[1:nstrengthx],i),1);
        hold('on');
        errorbar(list_xcor_dstrength,xread_dx((j-1)*nstrengthx+[1:nstrengthx],i)-q_x_dx(i,1,j),dxread_dx((j-1)*nstrengthx+[1:nstrengthx],i),[color(j) '.']);
        plot(list_xcor_dstrength,q_x_dx(i,2,j)*list_xcor_dstrength,[color(j) '-']);
        plot(list_xcor_dstrength,dxmodel(i,:,j),[color(j) '--']);
        hold('off');
        fprintf(' %f ',A(i,2,j)/q_x_dx(i,2,j))
        title(sprintf('%s(x)',bpm_measured_name(i,:)));
        xlim([min(list_xcor_dstrength) max(list_xcor_dstrength)]);
    end
    fprintf('\n')
    fprintf('%s(y) ',bpm_measured_name(i,:))
    for j=1:nycor_scanned
        fig=round((i-half_plot_per_fig)/plot_per_fig)+nfig+1;
        sub=i-plot_per_fig*round((i-half_plot_per_fig)/plot_per_fig);
%        fprintf('fig %i sub %i %s(x)=f(%s)\n',fig,sub,bpm_measured_name(i,:),xcor_scanned_name(j,:));
        figure(fig)
        subplot(3,5,sub);
        if(j==1) 
            cla('reset');
        end
        
        [q_x_dy(i,:,j),dq_x_dy(i,:,j)]=noplot_polyfit(list_ycor_dstrength,xread_dy((j-1)*nposy+(1:nstrengthy),i),dxread_dy((j-1)*nstrengthy+[1:nstrengthy],i),1);
        [q_y_dy(i,:,j),dq_y_dy(i,:,j)]=noplot_polyfit(list_ycor_dstrength,yread_dy((j-1)*nstrengthy+(1:nstrengthy),i),dyread_dy((j-1)*nstrengthy+[1:nstrengthy],i),1);
        hold('on');
        errorbar(list_ycor_dstrength,yread_dy((j-1)*nstrengthy+[1:nstrengthy],i)-q_y_dy(i,1,j),dyread_dy((j-1)*nstrengthy+[1:nstrengthy],i),[color(j) 'o']);
        plot(list_ycor_dstrength,q_y_dy(i,2,j)*list_ycor_dstrength,[color(j) '-']);
        plot(list_ycor_dstrength,dymodel(i,:,j),[color(j) '--']);
        hold('off');
        fprintf(' %f ',D(i,4,j)/q_y_dy(i,2,j))
        title(sprintf('%s(y)',bpm_measured_name(i,:)));
        xlim([min(list_xcor_dstrength) max(list_xcor_dstrength)]);
    end
    for j=1:nxmover_scanned
        [q_x_dposx(i,:,j),dq_x_dposx(i,:,j)]=noplot_polyfit(list_xmover_dpos,xread_dposx((j-1)*nposx+(1:nposx),i),dxread_dposx((j-1)*nposx+[1:nposx],i),1);
        [q_y_dposx(i,:,j),dq_y_dposx(i,:,j)]=noplot_polyfit(list_xmover_dpos,yread_dposx((j-1)*nposx+(1:nposx),i),dyread_dposx((j-1)*nposx+[1:nposx],i),1);
    end
    for j=1:nymover_scanned
        [q_x_dposy(i,:,j),dq_x_dposy(i,:,j)]=noplot_polyfit(list_xmover_dpos,xread_dposy((j-1)*nposy+(1:nposy),i),dxread_dposy((j-1)*nposy+[1:nposy],i),1);
        [q_y_dposy(i,:,j),dq_y_dposy(i,:,j)]=noplot_polyfit(list_xmover_dpos,yread_dposy((j-1)*nposy+(1:nposy),i),dyread_dposy((j-1)*nposy+[1:nposy],i),1);
    end
    
    fprintf('\n')
end

%plot_bpm=[1:11 15:17 12:14 18:32 44 33:37 45 38:39 46 40:43];
plot_bpm=[1:11 15:17 12:14 18:32  33:37  38:39  40:43];

figure(nfig*2+1);
legend_txt={};
for j=1:nxcor_scanned
    if(j==1) 
        cla('reset');
    end
    hold on;
    errorbar(sort(plot_bpm),q_x_dx(plot_bpm,2,j),dq_x_dx(plot_bpm,2,j),[color(j) '-']);
    plot(sort(plot_bpm),A(plot_bpm,2,j),[color(j) '--']);
    hold off;
    legend_txt{end+1}=sprintf('%s measurement',xcor_scanned_name(j,:));
    legend_txt{end+1}=sprintf('%s model',xcor_scanned_name(j,:));
end
set(gca,'XTick',sort(plot_bpm));
set(gca,'XTickLabel',bpm_measured_name(plot_bpm,:));
xticklabel_rotate90();
grid;
ylabel('R12(cor->BPMs) [m/rad]');
legend(legend_txt,'Location','NorthWest');

figure(nfig*2+2);
legend_txt={};
for j=1:nycor_scanned
    if(j==1) 
        cla('reset');
    end
    hold on;
    errorbar(sort(plot_bpm),q_y_dy(plot_bpm,2,j),dq_y_dy(plot_bpm,2,j),[color(j) '-']);
    plot(sort(plot_bpm),D(plot_bpm,4,j),[color(j) '--']);
    hold off;
    legend_txt{end+1}=sprintf('%s measurement',ycor_scanned_name(j,:));
    legend_txt{end+1}=sprintf('%s model',ycor_scanned_name(j,:));
end
%title('-Y for striplines and cavity on movers');
set(gca,'XTick',sort(plot_bpm));
set(gca,'XTickLabel',bpm_measured_name(plot_bpm,:));
xticklabel_rotate90();
grid;
ylabel('R34 [m/rad]');
legend(legend_txt,'Location','NorthWest');

figure(nfig*2+3);
legend_txt={};
for j=1:nxmover_scanned
    if(j==1) 
        cla('reset');
    end
    hold on;
    errorbar(sort(plot_bpm),q_x_dposx(plot_bpm,2,j),dq_x_dposx(plot_bpm,2,j),[color(j) '-']);
    plot(sort(plot_bpm),E(plot_bpm,j),[color(j) '--']);
    hold off;
    legend_txt{end+1}=sprintf('%s X measurement',xmover_scanned_name(j,:));
    legend_txt{end+1}=sprintf('%s X model',xmover_scanned_name(j,:));
end
%title('-Y for striplines and cavity on movers');
set(gca,'XTick',sort(plot_bpm));
set(gca,'XTickLabel',bpm_measured_name(plot_bpm,:));
xticklabel_rotate90();
grid;
ylabel('mover pos to bpm pos [m/m]');
legend(legend_txt,'Location','NorthWest');

figure(nfig*2+4);
legend_txt={};
for j=1:nxmover_scanned
    if(j==1) 
        cla('reset');
    end
    hold on;
    errorbar(sort(plot_bpm),q_y_dposy(plot_bpm,2,j),dq_y_dposy(plot_bpm,2,j),[color(j) '-']);
    plot(sort(plot_bpm),H(plot_bpm,j),[color(j) '--']);
    hold off;
    legend_txt{end+1}=sprintf('%s Y measurement',ymover_scanned_name(j,:));
    legend_txt{end+1}=sprintf('%s Y model',ymover_scanned_name(j,:));
end
%title('-Y for striplines and cavity on movers');
set(gca,'XTick',sort(plot_bpm));
set(gca,'XTickLabel',bpm_measured_name(plot_bpm,:));
xticklabel_rotate90();
grid;
ylabel('mover pos to bpm pos [m/m]');
legend(legend_txt,'Location','NorthWest');

end

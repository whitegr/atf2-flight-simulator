function [ncor,cor_ps,cor_ps_read,cor_B,cor_S,cor_name]=get_corrector(cor)
%return information on the cor given
    global BEAMLINE PS
    
    IEX=getcolumn(findcells(BEAMLINE, 'Name',upper('iex')),1);
    
    ncor=length(cor);
    cor_ps=zeros(1,ncor);
    cor_ps_read=zeros(1,ncor);
    cor_B=zeros(1,ncor);
    cor_S=zeros(1,ncor);
    cor_name='';
    for i=1:ncor
        cor_ps(i)=BEAMLINE{cor(i)}.PS;
        cor_B(i)=BEAMLINE{cor(i)}.B;
        cor_S(i)=BEAMLINE{cor(i)}.S;
        cor_name=strvcat(cor_name, BEAMLINE{cor(i)}.Name);
    end
    cor_S=cor_S-BEAMLINE{IEX}.S;
    FlHwUpdate({[] [cor_ps] []});
    for i=1:ncor
        cor_ps_read(i)=PS(cor_ps(i)).Ampl;
    end
end

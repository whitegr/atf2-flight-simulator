function [mover_read]=get_mover(target)
%SET_Mover

    global BEAMLINE GIRDER;

    if (ischar(target))
        for i=1:getcolumn(size(target),1)
            girder(i)=BEAMLINE{getcolumn(findcells(BEAMLINE,'Name',target(i,:)),1)}.Girder;
        end
    else
        girder=target;
    end
    
    ngirder=getcolumn(size(girder),2);

    request{1}=girder;
    request{2}=[];
    request{3}=[];
    % request{1}(...) = read+write privs requested for mover on GIRDER{...}
    % request{2}(...) = read+write privs requested for power supply PS(...)
    % request{3}(...) = read privs requested for instrument INSTR{...}

    FlHwUpdate(request);

    for i=1:ngirder
        mover_read(i,:)=GIRDER{girder(i)}.MoverPos;
    end
end

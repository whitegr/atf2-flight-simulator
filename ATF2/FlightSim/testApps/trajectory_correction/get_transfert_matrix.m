function [matrix]=get_transfert_matrix(cor,bpm,index_to,index_from)
%compute transfert matrix

    FlHwUpdate();
    
    nbpm=length(bpm);
    ncor=length(cor);
    matrix=zeros(nbpm,ncor);
    for i=1:nbpm
        for j=1:ncor
            if(bpm(i)>cor(j))
                [stat,R]=RmatAtoB(cor(j),bpm(i));
                matrix(i,j)=R(index_to,index_from);
            else
                matrix(i,j)=0;
            end;
        end;
    end;
end

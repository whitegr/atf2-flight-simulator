function [matrix]=get_transfert_matrix_IP(from_index,to_index,index_to,index_from)
%compute transfert matrix

    FlHwUpdate();
    
    nindex_to=length(index_to);
    nfrom_index=length(from_index);
    matrix=zeros(nindex_to,nfrom_index);
    for j=1:nfrom_index
        if(to_index>from_index(j))
            [stat,R]=RmatAtoB(from_index(j),to_index);
            matrix(:,j)=R(index_to,index_from);
        else
            matrix(:,j)=zeros(nindex_to);
        end;
    end;
end

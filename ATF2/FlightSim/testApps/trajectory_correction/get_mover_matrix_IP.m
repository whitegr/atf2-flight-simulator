function [xmatrix,ymatrix]=get_mover_matrix_IP(xmover,ymover)
%compute the response matrix giving X X' Y Y' due to the displacement of indicated movers
    global BEAMLINE;
    
    IP=findcells(BEAMLINE,'Name','IP');
    nxmover=length(xmover);
    nymover=length(ymover);
    xmover_KL=zeros(1,nxmover);
    ymover_KL=zeros(1,nymover);
    for i=1:nxmover
        elem_on_mover=findcells(BEAMLINE,'Girder',xmover(i));
        q_on_xmover(i,:)=findcells(BEAMLINE,'Class','QUAD',elem_on_mover(1),elem_on_mover(end));
        for j=1:length(q_on_xmover(i,:))
            xmover_KL(i)=xmover_KL(i)+BEAMLINE{q_on_xmover(i,j)}.B*0.2998/BEAMLINE{q_on_xmover(i,j)}.P;
        end
    end
    for i=1:nymover
        elem_on_mover=findcells(BEAMLINE,'Girder',ymover(i));
        q_on_ymover(i,:)=findcells(BEAMLINE,'Class','QUAD',elem_on_mover(1),elem_on_mover(end));
        for j=1:length(q_on_xmover(i,:))
            ymover_KL(i)=ymover_KL(i)+BEAMLINE{q_on_ymover(i,j)}.B*0.2998/BEAMLINE{q_on_ymover(i,j)}.P;
        end
    end
    xmatrix(1:4,:)=get_transfert_matrix_IP(q_on_xmover(:,1),IP,1:4,2);
    ymatrix(1:4,:)=get_transfert_matrix_IP(q_on_ymover(:,1),IP,1:4,4);
    xmatrix=xmatrix.*(ones(4,1)*xmover_KL);
    ymatrix=ymatrix.*(ones(4,1)*-ymover_KL);
end
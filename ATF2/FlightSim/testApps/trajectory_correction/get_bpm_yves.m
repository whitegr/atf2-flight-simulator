function [bpm,nbpm,bpm_read,bpm_resol,bpm_S,bpm_name]=get_bpm_yves(naverage)

global BEAMLINE;

if(nargin==0)
    naverage=10;
    disp('average of 10 bpm reading by default');
end

IEX=getcolumn(findcells(BEAMLINE, 'Name',upper('iex')),1);

%wait naverage new bpm readings
stat=FlHwUpdate('wait',naverage);
while(stat{1}==-1)
    disp(sprintf('repeat FlHwUpdate(''wait'') cause :%s',stat{2}));
    stat=FlHwUpdate('wait',naverage);
end;

%get bpm read
%[stat,output]=FlHwUpdate('bpmave',naverage,0,[0.4 3 1 0]);
[stat,output]=FlHwUpdate('bpmave',naverage);
while(stat{1}==-1)
%    pause(1);
    disp(sprintf('repeat FlHwUpdate(''bpmave'') cause :%s',stat{2}));
%    [stat,output]=FlHwUpdate('bpmave',naverage,0,[0.4 3 1 0]);
    [stat,output]=FlHwUpdate('bpmave',naverage);
end;

%convert it in usable way
[stat instdata]=FlTrackThru(1,length(BEAMLINE),output);
if(stat{1}==-1)
    disp(sprintf('FlTrackThru reurned error :%s',stat{2}));
end
nbpm=getcolumn(size(instdata{1}),2);
bpm=zeros(1,nbpm);
bpm_read=zeros(nbpm,2);
bpm_resol=zeros(nbpm,2);
bpm_S=zeros(1,nbpm);
bpm_name='';
for i=1:nbpm
    bpm(i)=instdata{1}(i).Index;
    bpm_read(i,:)=[instdata{1}(i).x instdata{1}(i).y];
    bpm_resol(i,:)=[instdata{1}(i).xrms instdata{1}(i).yrms];
    bpm_S(i)=instdata{1}(i).S;
    bpm_name=strvcat(bpm_name, BEAMLINE{bpm(i)}.Name);
end
bpm_S=bpm_S-BEAMLINE{IEX}.S;

end

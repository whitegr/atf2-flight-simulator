function [xmatrix,ymatrix]=get_mover_matrix(bpm,xmover,ymover)
%compute the matrix giving displacements in BPM due to displacement of a mover
    global BEAMLINE;
%get transfert matrix betweens movers displacement and bpms
    nxmover=length(xmover);
    nymover=length(ymover);
    nbpm=length(bpm);
    xmover_KL=zeros(1,nxmover);
    ymover_KL=zeros(1,nymover);
    for i=1:nxmover
        elem_on_mover=findcells(BEAMLINE,'Girder',xmover(i));
        q_on_xmover(i,:)=findcells(BEAMLINE,'Class','QUAD',elem_on_mover(1),elem_on_mover(end));
        bpm_on_xmover(i)=findcells(BEAMLINE,'Class','MONI',elem_on_mover(1),elem_on_mover(end));
        for j=1:length(q_on_xmover(i,:))
            xmover_KL(i)=xmover_KL(i)+BEAMLINE{q_on_xmover(i,j)}.B*0.2998/BEAMLINE{q_on_xmover(i,j)}.P;
        end
    end
    for i=1:nymover
        elem_on_mover=findcells(BEAMLINE,'Girder',ymover(i));
        q_on_ymover(i,:)=findcells(BEAMLINE,'Class','QUAD',elem_on_mover(1),elem_on_mover(end));
        bpm_on_ymover(i)=findcells(BEAMLINE,'Class','MONI',elem_on_mover(1),elem_on_mover(end));
        for j=1:length(q_on_xmover(i,:))
            ymover_KL(i)=ymover_KL(i)+BEAMLINE{q_on_ymover(i,j)}.B*0.2998/BEAMLINE{q_on_ymover(i,j)}.P;
        end
    end
    xmatrix=get_transfert_matrix(q_on_xmover(:,1),bpm,1,2);
    ymatrix=get_transfert_matrix(q_on_ymover(:,1),bpm,3,4);
    xmatrix=xmatrix.*(ones(nbpm,1)*xmover_KL);
    ymatrix=ymatrix.*(ones(nbpm,1)*-ymover_KL);
    for i=1:nxmover
        bpm_index=find(bpm==bpm_on_xmover(i));
        xmatrix(bpm_index,i)=xmatrix(bpm_index,i)-1;
    end
    for i=1:nymover
        bpm_index=find(bpm==bpm_on_ymover(i));
        ymatrix(bpm_index,i)=ymatrix(bpm_index,i)-1;
    end
end

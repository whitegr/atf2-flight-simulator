function [matrix]=get_transfert_matrix_IP2(from_index,to_index,index_to,index_from)
%compute transfert matrix

    FlHwUpdate();
    
    nindex_to=length(index_to);
    nfrom_index=length(from_index);
    matrix=zeros(nindex_to,nfrom_index);
    [STAT,R]=GetRmats(from_index(1),to_index(end));
    Rmats={R.RMAT};
    for j=1:nfrom_index
        if(to_index>from_index(j))
                R=eye(6,6);
                for index=to_index-from_index(1)+1:-1:from_index(j)-from_index(1)+1
                    R=R*Rmats{index};
                end
            matrix(:,j)=R(index_to,index_from);
        else
            matrix(:,j)=zeros(nindex_to);
        end;
    end;
end

function [ps_read]=get_ps(target)
%SET_PS Summary of this function goes here
%   Detailed explanation goes here

    global BEAMLINE PS;

    if (ischar(target))
        for i=1:getcolumn(size(target),1)
            ps(i)=BEAMLINE{findcells(BEAMLINE,'Name',target(i,:))}.PS;
        end
    else
        ps=target;
    end
    
    request{1}=[];
    request{2}=ps;
    request{3}=[];
    % request{1}(...) = read+write privs requested for mover on GIRDER{...}
    % request{2}(...) = read+write privs requested for power supply PS(...)
    % request{3}(...) = read privs requested for instrument INSTR{...}

    %exit if no change needed
    nps=getcolumn(size(ps),2);
    ps_read=zeros(1,nps);
    FlHwUpdate(request);
    for i=1:nps
        ps_read(i)=PS(ps(i)).Ampl;
    end
end

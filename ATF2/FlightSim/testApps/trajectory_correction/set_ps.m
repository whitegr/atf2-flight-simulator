function [ps_oldread]=set_ps(target,ps_newvalue)
%SET_PS Summary of this function goes here
%   Detailed explanation goes here

    global BEAMLINE PS;

    if (ischar(target))
        for i=1:getcolumn(size(target),1)
            ps(i)=BEAMLINE{findcells(BEAMLINE,'Name',target(i,:))}.PS;
        end
    else
        ps=target;
    end
    
    %check input args
    nps=getcolumn(size(ps),2);
    if(nps~=getcolumn(size(ps_newvalue),2))
        disp('ERROR : the array PS and the array new value must have the same size');
        return
    end

    ps_oldread=zeros(1,nps);
    FlHwUpdate;
    for i=1:nps
        ps_oldread(i)=PS(ps(i)).Ampl;
    end
    range=find(abs(ps_oldread - ps_newvalue)>1e-5);
    ps=ps(range);
    ps_newvalue=ps_newvalue(range);
    nps=getcolumn(size(ps),2);
    
    if (nps==0)
        disp('No change > 10urad required !');
        return;
    end    
    
    request{1}=[];
    request{2}=ps;
    request{3}=[];
    % request{1}(...) = read+write privs requested for mover on GIRDER{...}
    % request{2}(...) = read+write privs requested for power supply PS(...)
    % request{3}(...) = read privs requested for instrument INSTR{...}

    %ask for acces
    [stat reqID] = AccessRequest(request);
    if(stat{1}==-1)
        disp(sprintf('AccessRequest reurned error :%s',stat{2}));
    else
        disp(sprintf('Access granted, ID of request : %i',reqID));
    end

    for i=1:nps
        PS(ps(i)).SetPt=ps_newvalue(i);
    end

    %ask to change ps value
    stat=PSTrim(ps, true);
    if(stat{1}==-1)
        disp(sprintf('PSTrim returned error :%s',stat{2}));
    end
    pause(1);
    
    %update ps value
    FlHwUpdate(request);
    ps_newread=zeros(1,nps);
    for i=1:nps
        ps_newread(i)=PS(ps(i)).Ampl;
    end

    %if more than 5% difference, ask again
    itteration=0;
    bad_ps=find(abs((ps_newvalue-ps_newread)./ps_newvalue)>=.05,1);
    while( ~isempty( bad_ps ) )
        itteration=itteration+1;
        %give up after 5 asks
        if(itteration>5)
            disp(sprintf('Warning : PS %i is set at %f instead of %f.',bad_ps,ps_newread(bad_ps),ps_newvalue(bad_ps) ));
            break;
        end

        %ask to change different ps from what is wanted
        stat=PSTrim(ps(bad_ps), true);
        if(stat{1}==-1)
            disp(sprintf('PSTrim reurned error :%s',stat{2}));
        end
        pause(1);
        
        %update ps values
        request{2}=ps;
        FlHwUpdate(request);
        for i=1:nps
            ps_newread(i)=PS(ps(i)).Ampl;
        end
        bad_ps=find(abs((ps_newvalue-ps_newread)./ps_newvalue)>=.05,1);
    end
    
    %release acces
    AccessRequest('release',reqID);
    if(stat{1}==-1)
        disp(sprintf('Error durring access release, reurned error :%s',stat{2}));
    else
        disp(sprintf('Access %i released',reqID));
    end
end

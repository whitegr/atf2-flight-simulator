function [matrix]=get_transfert_matrix2(cor,bpm,index_to,index_from)
%compute transfert matrix

    FlHwUpdate();
    
    nbpm=length(bpm);
    ncor=length(cor);
    matrix=zeros(nbpm,ncor);
    [STAT,R]=GetRmats(cor(1),bpm(end));
    Rmats={R.RMAT};
    for i=1:nbpm
        for j=1:ncor
            if(bpm(i)>cor(j))
                R=eye(6,6);
                for index=cor(j)-cor(i)+1:-1:bpm(i)-cor(i)+1
                    R=R*Rmats{index};
                end
                matrix(i,j)=R(index_to,index_from);
            else
                matrix(i,j)=0;
            end;
        end;
    end;
end

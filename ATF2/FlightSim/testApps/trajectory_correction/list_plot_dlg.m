function [selection,value] = list_plot_dlg(varargin)
%LISTDLG  List selection dialog box.
%   [SELECTION,OK] = LISTDLG('ListString',S) creates a modal dialog box
%   which allows you to select a string or multiple strings from a list.
%   SELECTION is a vector of indices of the selected strings (length 1 in
%   the single selection mode).  This will be [] when OK is 0.  OK is 1 if
%   you push the OK button, or 0 if you push the Cancel button or close the
%   figure.
%
%   Double-clicking on an item or pressing <CR> when multiple items are
%   selected has the same effect as clicking the OK button.  Pressing <CR>
%   is the same as clicking the OK button. Pressing <ESC> is the same as
%   clicking the Cancel button.
%
%   Inputs are in parameter,value pairs:
%
%   Parameter       Description
%   'ListString'    cell array of strings for the list box.
%   'SelectionMode' string; can be 'single' or 'multiple'; defaults to
%                   'multiple'.
%   'ListSize'      [width height] of listbox in pixels; defaults
%                   to [160 300].
%   'InitialValue'  vector of indices of which items of the list box
%                   are initially selected; defaults to the first item.
%   'Name'          String for the figure's title; defaults to ''.
%   'PromptString'  string matrix or cell array of strings which appears 
%                   as text above the list box; defaults to {}.
%   'OKString'      string for the OK button; defaults to 'OK'.
%   'CancelString'  string for the Cancel button; defaults to 'Cancel'.
%   'NPlot'         Number of plot to display; defaults to 1.
%   'XData'         (cell of) array of x values for the plot(s).
%   'YData'         cell of array of y values for the plot(s). 1 cell per
%                   item in the list, 1 row per x value, 1 column per plot.
%   'PlotSize'      [width height] of plot in pixels; defaults
%                   to [160 300]
%   'xlim'          cell of the xlim of each plot; defaults to 'auto'.
%   'ylim'          cell of the ylim of each plot; defaults to 'auto'
%
%   A 'Select all' button is provided in the multiple selection case.
%
%   Example:
%     d = dir;
%     str = {d.name};
%     [s,v] = listdlg('PromptString','Select a file:',...
%                     'SelectionMode','single',...
%                     'ListString',str)
 %
%  See also DIALOG, ERRORDLG, HELPDLG, INPUTDLG,
%    MSGBOX, QUESTDLG, WARNDLG.

%   Copyright 1984-2005 The MathWorks, Inc.
%   $Revision: 1.20.4.7 $  $Date: 2007/10/15 22:56:03 $

%   'uh'            uicontrol button height, in pixels; default = 22.
%   'fus'           frame/uicontrol spacing, in pixels; default = 8.
%   'ffs'           frame/figure spacing, in pixels; default = 8.

% simple test:
%
% d = dir; [s,v] = listdlg('PromptString','Select a file:','ListString',{d.name});
% 
error(nargchk(1,inf,nargin))

figname = '';
smode = 2;   % (multiple)
promptstring = {};
liststring = [];
listsize = [160 300];
plotsize = [160 300];
initialvalue = [];
okstring = 'OK';
cancelstring = 'Cancel';
fus = 8;
ffs = 8;
uh = 22;
nplot = 1;
xdata = {};
ydata = {};
xlimit='auto';
ylimit='auto';

if mod(length(varargin),2) ~= 0
    % input args have not com in pairs, woe is me
    error('MATLAB:listdlg:InvalidArgument', 'Arguments to LISTDLG must come param/value in pairs.')
end
for i=1:2:length(varargin)
    switch lower(varargin{i})
     case 'name'
      figname = varargin{i+1};
     case 'promptstring'
      promptstring = varargin{i+1};
     case 'selectionmode'
      switch lower(varargin{i+1})
       case 'single'
        smode = 1;
       case 'multiple'
        smode = 2;
      end
     case 'listsize'
      listsize = varargin{i+1};
     case 'liststring'
      liststring = varargin{i+1};
     case 'initialvalue'
      initialvalue = varargin{i+1};
     case 'uh'
      uh = varargin{i+1};
     case 'fus'
      fus = varargin{i+1};
     case 'ffs'
      ffs = varargin{i+1};
     case 'okstring'
      okstring = varargin{i+1};
     case 'cancelstring'
      cancelstring = varargin{i+1};
     case  'nplot'
      nplot=varargin{i+1};
     case 'xdata'
      xdata=varargin{i+1};
     case 'ydata'
      ydata=varargin{i+1};
     case 'xlim'
      xlimit=varargin{i+1};
     case 'ylim'
      ylimit=varargin{i+1};
     case 'plotsize'
      plotsize=varargin{i+1};
     otherwise
      error('MATLAB:listdlg:UnknownParameter', 'Unknown parameter name passed to LISTDLG.  Name was %s', varargin{i})
    end
end

if ischar(promptstring)
    promptstring = cellstr(promptstring); 
end

if isempty(initialvalue)
    initialvalue = 1;
end

if isempty(liststring)
    error('MATLAB:listdlg:NeedParameter', 'ListString parameter is required.')
end

liststring=cellstr(liststring);

% if (length(xdata)~=length(liststring)*nplot)
%      error('MATLAB:listdlg:InvalidArgument', 'length of xdata must be equal to length of liststring * nplot .')
% end
% if (length(ydata)~=length(liststring)*nplot)
%      error('MATLAB:listdlg:InvalidArgument', 'length of xdata must be equal to length of liststring * nplot .')
% end
% 
ex = get(0,'defaultuicontrolfontsize')*1.7;  % height extent per line of uicontrol text (approx)

fp = get(0,'defaultfigureposition');
w = 3*(fus+ffs)+listsize(1)+plotsize(1);
h = 2*ffs+6*fus+ex*length(promptstring)+max([listsize(2) nplot*(plotsize(2)+2*ex)-ffs])+uh+(smode==2)*(fus+uh);
fp = [fp(1) fp(2)+fp(4)-h w h];  % keep upper left corner fixed
fig_props = { ...
    'name'                   figname ...
    'color'                  get(0,'defaultUicontrolBackgroundColor') ...
    'resize'                 'off' ...
    'numbertitle'            'off' ...
    'menubar'                'none' ...
    'windowstyle'            'modal' ...
    'visible'                'off' ...
    'createfcn'              ''    ...
    'position'               fp   ...
    'closerequestfcn'        'delete(gcbf)' ...
            };

fig = figure(fig_props{:});

if length(promptstring)>0
    prompt_text = uicontrol('style','text','string',promptstring,...
        'horizontalalignment','left',...
        'position',[ffs+fus fp(4)-(ffs+fus+ex*length(promptstring)) ...
        fp(3)-(ffs+fus) ex*length(promptstring)]); %#ok
end

btn_wid = (fp(3)-2*(ffs+fus)-fus)/2;

plot_axe=zeros(1,nplot);
for i=1:nplot
    plot_axe(i)=axes('parent',fig,'Units','pixels',...
                     'position',[2*(ffs+fus)+listsize(1) ffs+uh+4*fus+(smode==2)*(fus+uh)-(nplot-i-1)*(plotsize(2)+2*ex) plotsize],...
                     'color',[1 1 1]);
end

listbox = uicontrol(fig,'style','listbox',...
                    'position',[ffs+fus fp(4)-(2*ffs+fus)-ex*length(promptstring)-listsize(2) listsize],...
                    'string',liststring,...
                    'backgroundcolor','w',...
                    'max',smode,...
                    'tag','listbox',...
                    'value',initialvalue, ...
                    'callback', {@doListboxClick,plot_axe,xdata,ydata,xlimit,ylimit});

ok_btn = uicontrol(fig,'style','pushbutton',...
                   'string',okstring,...
                   'position',[ffs+fus ffs+fus btn_wid uh],...
                   'callback',{@doOK,listbox});

cancel_btn = uicontrol(fig,'style','pushbutton',...
                       'string',cancelstring,...
                       'position',[ffs+2*fus+btn_wid ffs+fus btn_wid uh],...
                       'callback',{@doCancel,listbox});

if smode == 2
    selectall_btn = uicontrol(fig,'style','pushbutton',...
                              'string','Select all',...
                              'position',[ffs+fus 4*fus+ffs+uh listsize(1) uh],...
                              'tag','selectall_btn',...
                              'callback',{@doSelectAll, listbox});

    if length(initialvalue) == length(liststring)
        set(selectall_btn,'enable','off')
    end
    set(listbox,'callback',{@doListboxClick, selectall_btn})
end

set([fig, ok_btn, cancel_btn, listbox], 'keypressfcn', {@doKeypress, listbox});

for i=1:length(plot_axe)
    axes(plot_axe(i));
    if(iscell(xdata))
        if(size(xdata{initialvalue},2)==1)
            plot(xdata{initialvalue},ydata{initialvalue}(:,i));
        else
            plot(xdata{initialvalue}(:,i),ydata{initialvalue}(:,i));
        end
    else
        if(size(xdata,2)==1)
            plot(xdata,ydata{initialvalue}(:,i));
        else
            plot(xdata(:,i),ydata{initialvalue}(:,i));
        end
    end
    if(iscell(xlimit))
        xlim(xlimit{i});
    else
        xlim(xlimit);
    end
    if(iscell(ylimit))
        ylim(ylimit{i});
    else
        ylim(ylimit);
    end
end

%set(fig,'position',getnicedialoglocation(fp, get(fig,'Units')));
% Make ok_btn the default button.
%setdefaultbutton(fig, ok_btn);

% make sure we are on screen
movegui(fig)
set(fig, 'visible','on'); drawnow;

try
    % Give default focus to the listbox *after* the figure is made visible
    uicontrol(listbox);
    uiwait(fig);
catch
    if ishandle(fig)
        delete(fig)
    end
end

if isappdata(0,'ListDialogAppData__')
    ad = getappdata(0,'ListDialogAppData__');
    selection = ad.selection;
    value = ad.value;
    rmappdata(0,'ListDialogAppData__')
else
    % figure was deleted
    selection = [];
    value = 0;
end

%% figure, OK and Cancel KeyPressFcn
function doKeypress(src, evd, listbox) %#ok
switch evd.Key
 case 'escape'
  doCancel([],[],listbox);
end

%% OK callback
function doOK(ok_btn, evd, listbox) %#ok
if (~isappdata(0, 'ListDialogAppData__'))
    ad.value = 1;
    ad.selection = get(listbox,'value');
    setappdata(0,'ListDialogAppData__',ad);
    delete(gcbf);
end

%% Cancel callback
function doCancel(cancel_btn, evd, listbox) %#ok
ad.value = 0;
ad.selection = [];
setappdata(0,'ListDialogAppData__',ad)
delete(gcbf);

%% SelectAll callback
function doSelectAll(selectall_btn, evd, listbox) %#ok
set(selectall_btn,'enable','off')
set(listbox,'value',1:length(get(listbox,'string')));

%% Listbox callback
function doListboxClick(listbox, evd, varargin) %#ok
% if this is a doubleclick, doOK
if strcmp(get(gcbf,'SelectionType'),'open')
    doOK([],[],listbox);
elseif nargin == 3
    selectall_btn=varargin;
    if length(get(listbox,'string'))==length(get(listbox,'value'))
        set(selectall_btn,'enable','off')
    else
        set(selectall_btn,'enable','on')
    end
elseif nargin == 7
    plot_axe=varargin{1};
    xdata=varargin{2};
    ydata=varargin{3};
    xlimit=varargin{4};
    ylimit=varargin{5};
    selection=get(listbox,'value');
    for i=1:length(plot_axe)
        axes(plot_axe(i));
        if(iscell(xdata))
            if(size(xdata{selection},2)==1)
                plot(xdata{selection},ydata{selection}(:,i));
            else
                plot(xdata{selection}(:,i),ydata{selection}(:,i));
            end
        else
            if(size(xdata,2)==1)
                plot(xdata,ydata{selection}(:,i));
            else
                plot(xdata(:,i),ydata{selection}(:,i));
            end
        end
        if(iscell(xlimit))
            xlim(xlimit{i});
        else
            xlim(xlimit);
        end
        if(iscell(ylimit))
            ylim(ylimit{i});
        else
            ylim(ylimit);
        end
    end
end
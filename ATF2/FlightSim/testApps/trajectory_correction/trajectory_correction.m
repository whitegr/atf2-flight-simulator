function varargout = trajectory_correction(varargin)
% trajectory_correction M-file for trajectory_correction.fig
%      trajectory_correction, by itself, creates a new trajectory_correction or raises the existing
%      singleton*.
%
%      H = trajectory_correction returns the handle to a new trajectory_correction or the handle to
%      the existing singleton*.
%
%      trajectory_correction('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in trajectory_correction.M with the given input arguments.
%
%      trajectory_correction('Property','Value',...) creates a new trajectory_correction or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before trajectory_correction_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to trajectory_correction_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help trajectory_correction

% Last Modified by GUIDE v2.5 10-Mar-2011 21:12:13

% Begin initialization code - DO NOT EDIT
    gui_Singleton = 1;
    gui_State = struct('gui_Name',       mfilename, ...
                       'gui_Singleton',  gui_Singleton, ...
                       'gui_OpeningFcn', @trajectory_correction_OpeningFcn, ...
                       'gui_OutputFcn',  @trajectory_correction_OutputFcn, ...
                       'gui_LayoutFcn',  [] , ...
                       'gui_Callback',   []);
    if nargin && ischar(varargin{1})
        gui_State.gui_Callback = str2func(varargin{1});
    end

    if nargout
        [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
    else
        gui_mainfcn(gui_State, varargin{:});
    end
    % End initialization code - DO NOT EDIT
end


% --- Executes just before trajectory_correction is made visible.
function trajectory_correction_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to trajectory_correction (see VARARGIN)

% Choose default command line output for trajectory_correction
handles.output = hObject;

handles.out='';
handles=output(handles,'loading ...');

disable_all(handles);

handles=init(handles);

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes trajectory_correction wait for user response (see UIRESUME)
% uiwait(handles.trajectory_correction);
end

function handles=init(handles)

global BEAMLINE FL INSTR;

handles.choice='plot_bpm';

handles.xlegend=false;
handles.ylegend=false;

handles.xbpm_range_default=[-5e-3 5e-3];
handles.ybpm_range_default=[-5e-3 5e-3];
handles.xIP_range_default=[-5e-3 5e-3];
handles.yIP_range_default=[-5e-3 5e-3];
handles.xcor_range_default=[-2e-3 2e-3];
handles.ycor_range_default=[-2e-3 2e-3];
handles.xmov_range_default=[-2e-3 2e-3];
handles.ymov_range_default=[-2e-3 2e-3];
handles.srange_default=[0 95];
handles.sIPrange_default=[87.5 92];

IEX=findcells(BEAMLINE, 'Name','IEX');
BS1XA=findcells(BEAMLINE, 'Name','BS1XA');

handles.sub_ref_orbit=true;

handles.naverage=5;
[bpm0,nbpm0,bpm_read0,bpm_resol0,bpm_S0,bpm_name0]=get_bpm_yves(handles.naverage);
%range=find( ~isnan(bpm_read0(:,1).') & bpm0>=IEX );
range=find(bpm0>=IEX);
handles.bpm=bpm0(range);
handles.nbpm=getcolumn(size(handles.bpm),2);
handles.bpm_read=bpm_read0(range,1:2);
handles.bpm_resol=bpm_resol0(range,1:2);
handles.bpm_S=bpm_S0(range);
handles.bpm_name=bpm_name0(range,:);
for i=1:handles.nbpm
  instr_index=findcells(INSTR,'Index',handles.bpm(i));
  range_bool(i)=~isempty(FL.HwInfo.INSTR(instr_index).pvname);
end
handles.bpm_range=find(range_bool);
handles.bpm_ref=zeros(handles.nbpm,2);
handles.weight=ones(handles.nbpm,2);

FlHwUpdate;
ipdata = getIPData;
handles.IP_read=[ipdata.x;ipdata.xp;ipdata.y;ipdata.yp];
handles.IP_ref=zeros(4,1);

handles=plot_choice(handles);

xcor=sort([findcells(BEAMLINE, 'Name','ZX*X') findcells(BEAMLINE, 'Name','ZH*X') findcells(BEAMLINE, 'Name','ZH*FF')]);
handles.xcor_name='';
nxcor=length(xcor);
handles.xcor=[];
for i=1:nxcor
    if (strcmp(BEAMLINE{xcor(i)}.Class, 'XCOR') && xcor(i)>BS1XA)
        handles.xcor(end+1)=xcor(i);
    end
end
handles.nxcor=length(handles.xcor);
handles.xcor_range=1:handles.nxcor;
for i=1:handles.nxcor
    handles.xcor_name=strvcat(handles.xcor_name,BEAMLINE{handles.xcor(i)}.Name);
    handles.xcor_S(i)=BEAMLINE{handles.xcor(i)}.S-BEAMLINE{IEX}.S;
end

ycor=[findcells(BEAMLINE, 'Name','ZV*X') findcells(BEAMLINE, 'Name','ZV*FF')];
handles.ycor_name='';
nycor=length(ycor);
handles.ycor=[];
for i=1:nycor
    if (strcmp(BEAMLINE{ycor(i)}.Class, 'YCOR') && ycor(i)>BS1XA)
        handles.ycor(end+1)=ycor(i);
    end
end
handles.nycor=length(handles.ycor);
handles.ycor_range=1:handles.nycor;
for i=1:handles.nycor
    handles.ycor_name=strvcat(handles.ycor_name,BEAMLINE{handles.ycor(i)}.Name);
    handles.ycor_S(i)=BEAMLINE{handles.ycor(i)}.S-BEAMLINE{IEX}.S;
end

handles.mover=28:49;
handles.nmover=length(handles.mover);
handles.mover_name='';
for i=1:handles.nmover
    elems=findcells(BEAMLINE,'Girder',handles.mover(i));
    if(strcmp(BEAMLINE{elems(1)}.Class,'MONI'))
        elems(1)=[];
    end
    handles.mover_S(i)=BEAMLINE{elems(1)}.S-BEAMLINE{IEX}.S;
    mult=findstr(BEAMLINE{elems(1)}.Name,'MULT');
    if(isempty(mult))
        handles.mover_name=strvcat(handles.mover_name,BEAMLINE{elems(1)}.Name);
    else
        handles.mover_name=strvcat(handles.mover_name,BEAMLINE{elems(1)}.Name(1:mult-1));
    end
end
handles.mover_range=1:22;
%handles.xmover_pos=getcolumn(get_mover(handles.mover),1).';
%handles.ymover_pos=getcolumn(get_mover(handles.mover),2).';

handles.oldread_date='';
handles.mover_oldread={};
handles.mover_old={};
handles.ps_oldread={};
handles.ps_oldps={};
handles.bpm_read_old={};
handles.bpm_read_expected_old={};
handles.IP_read_old={};
handles.IP_read_expected_old={};

handles=output(handles,'done');
enable_all(handles);

end

% --- Outputs from this function are returned to the command line.
function varargout = trajectory_correction_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;
end


% --- Executes on button press in select_bpm.
function select_bpm_Callback(hObject, eventdata, handles)
% hObject    handle to select_bpm (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
    global BEAMLINE
    
    if(~isfield(handles,'bpm_name'))
    end
    [selection,ok]=listdlg('PromptString',{'Select bpms :' '' '(Shift+clik = continuous selection)' '(Crtl+clik = uncontinuous selection)'},...
                'ListString',handles.bpm_name,'ListSize',[250 min(handles.nbpm*15,500)],...
                'InitialValue',handles.bpm_range,'Name','Select BPM');
     if(ok)
         handles.bpm_range=selection;
     end
     guidata(hObject,handles);
end


% --- Executes on button press in select_xcorrector.
function select_xcorrector_Callback(hObject, eventdata, handles)
% hObject    handle to select_xcorrector (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
    global BEAMLINE;

    [selection,ok]=listdlg('PromptString',{'Select X correctors :' '' '(Shift+clik = continuous selection)' '(Crtl+clik = uncontinuous selection)'},...
                'ListString',handles.xcor_name,'ListSize',[250 min(handles.nxcor*15,500)],...
                'InitialValue',handles.xcor_range,'Name','Select X cor');
    if(ok)
        handles.xcor_range=selection;
    end
    guidata(hObject,handles);
end


% --- Executes on button press in select_xcorrector.
function select_ycorrector_Callback(hObject, eventdata, handles)
% hObject    handle to select_xcorrector (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
    global BEAMLINE;

    [selection,ok]=listdlg('PromptString',{'Select Y correctors :' '' '(Shift+clik = continuous selection)' '(Crtl+clik = uncontinuous selection)'},...
                'ListString',handles.ycor_name,'ListSize',[250 min(handles.nycor*15,500)],...
                'InitialValue',handles.ycor_range,'Name','Select Y cor');
    if(ok)
        handles.ycor_range=selection;
    end
    guidata(hObject,handles);
end


function nave_Callback(hObject, eventdata, handles)
% hObject    handle to nave (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
    nave=str2double(get(hObject, 'String'));
    if(nave<2)
        set(hObject, 'String','2');
        nave=2;
    end
    handles.naverage =nave;
    guidata(hObject,handles);
% Hints: get(hObject,'String') returns contents of nave as text
%        str2double(get(hObject,'String')) returns contents of nave as a double
end


% --- Executes during object creation, after setting all properties.
function nave_CreateFcn(hObject, eventdata, handles)
% hObject    handle to nave (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
    if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
        set(hObject,'BackgroundColor','white');
    end
    handles.naverage = str2double(get(hObject, 'String'));
    guidata(hObject,handles);
end


% --- Executes on button press in measure_orbit.
function measure_orbit_Callback(hObject, eventdata, handles)
% hObject    handle to measure_orbit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
    global BEAMLINE
    
    disable_all(handles);

    handles=output(handles,'measurement of the orbit ...');
    
    IEX=findcells(BEAMLINE, 'Name','IEX');

    [bpm0,nbpm0,bpm_read0,bpm_resol0,bpm_S0,bpm_name0]=get_bpm_yves(handles.naverage);
    %range=find( ~isnan(bpm_read0(:,1).') & bpm0>=IEX );
    range=find(bpm0>=IEX );
    handles.bpm=bpm0(range);
    handles.nbpm=getcolumn(size(handles.bpm),2);
    handles.bpm_read=bpm_read0(range,1:2);
    handles.bpm_resol=bpm_resol0(range,1:2);
    handles.bpm_S=bpm_S0(range);
    handles.bpm_name=bpm_name0(range,:);
    
    FlHwUpdate;
    ipdata = getIPData;
    handles.IP_read=[ipdata.x;ipdata.xp;ipdata.y;ipdata.yp];
 
    if(isfield(handles,'IP_read_expected'))
        handles=rmfield(handles,'IP_read_expected');
    end
    if(isfield(handles,'IP_read_before'))
        handles=rmfield(handles,'IP_read_before');
    end
    if(isfield(handles,'bpm_read_expected'))
        handles=rmfield(handles,'bpm_read_expected');
    end
    if(isfield(handles,'bpm_read_before'))
        handles=rmfield(handles,'bpm_read_before');
    end
    if(isfield(handles,'xcor_ps_newvalue'))
        handles=rmfield(handles,'xcor_ps_newvalue');
    end
    if(isfield(handles,'ycor_ps_newvalue'))
        handles=rmfield(handles,'ycor_ps_newvalue');
    end
    if(isfield(handles,'xmover_pos'))
        handles=rmfield(handles,'xmover_pos');
    end
    if(isfield(handles,'ymover_pos'))
        handles=rmfield(handles,'ymover_pos');
    end

    handles=output(handles,'done');

    handles=plot_choice(handles);

    enable_all(handles);

    guidata(hObject,handles);
end


% --- Executes on button press in compute_ext.
function compute_ext_Callback(hObject, eventdata, handles)
% hObject    handle to compute_ext (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
    global BEAMLINE;

    handles=output(handles,'compute EXT correction...');
    
    disable_all(handles);

    FlHwUpdate();

    IP=findcells(BEAMLINE,'Name','IP');
        
    invalid=isnan(handles.bpm_read(handles.bpm_range,1)) | isnan(handles.bpm_read(handles.bpm_range,2));
    if any(invalid)
        handles=output(handles,'NaN values BPMS disabled :');
        handles=output(handles,handles.bpm_name(handles.bpm_range(invalid),:));
        handles.bpm_range=handles.bpm_range(~invalid);
    end
    handles=output(handles,'compute EXT transfert matrices...');
    handles.xmatrix_EXT=get_transfert_matrix(handles.xcor(handles.xcor_range),handles.bpm,1,2);
    handles.ymatrix_EXT=get_transfert_matrix(handles.ycor(handles.ycor_range),handles.bpm,3,4);
    handles=output(handles,'compute IP transfert matrices...');
    handles.xmatrix_EXT_IP=get_transfert_matrix_IP(handles.xcor(handles.xcor_range),IP,1:4,2);
    handles.ymatrix_EXT_IP=get_transfert_matrix_IP(handles.ycor(handles.ycor_range),IP,1:4,4);
    handles=output(handles,'compute correction...');
    [handles.xcor_ps,handles.xcor_ps_newvalue,handles.ycor_ps,handles.ycor_ps_newvalue]=compute_steering_EXT(handles.bpm_read(handles.bpm_range,:)*diag([handles.xgain_val handles.ygain_val]),handles.weight(handles.bpm_range,:),handles.bpm_ref(handles.bpm_range,:),handles.xcor(handles.xcor_range),handles.ycor(handles.ycor_range),handles.xmatrix_EXT(handles.bpm_range,:),handles.ymatrix_EXT(handles.bpm_range,:));
    handles.xcor_ps_read=get_ps(handles.xcor_ps);
    handles.ycor_ps_read=get_ps(handles.ycor_ps);
    handles=output(handles,'correctors change :');
    for i=1:length(handles.xcor_range)
        handles=output(handles,sprintf('%s %g', handles.xcor_name(handles.xcor_range(i),:), handles.xcor_ps_newvalue(i)-handles.xcor_ps_read(i)));
    end
    for i=1:length(handles.ycor_range)
        handles=output(handles,sprintf('%s %g', handles.ycor_name(handles.ycor_range(i),:), handles.ycor_ps_newvalue(i)-handles.ycor_ps_read(i)));
    end
    handles.bpm_read_expected(:,1)=handles.xmatrix_EXT*(handles.xcor_ps_newvalue-handles.xcor_ps_read).'+handles.bpm_read(:,1);
    handles.bpm_read_expected(:,2)=handles.ymatrix_EXT*(handles.ycor_ps_newvalue-handles.ycor_ps_read).'+handles.bpm_read(:,2);
    handles.IP_read_expected=   handles.xmatrix_EXT_IP*(handles.xcor_ps_newvalue-handles.xcor_ps_read).'+...
                                handles.ymatrix_EXT_IP*(handles.ycor_ps_newvalue-handles.ycor_ps_read).'+...
                                handles.IP_read;

    
    if(isfield(handles,'bpm_read_before'))
        handles=rmfield(handles,'bpm_read_before');
    end
    if(isfield(handles,'IP_read_before'))
        handles=rmfield(handles,'IP_read_before');
    end
    if(isfield(handles,'xmover_pos'))
        handles=rmfield(handles,'xmover_pos');
    end
    if(isfield(handles,'ymover_pos'))
        handles=rmfield(handles,'ymover_pos');
    end

    handles=plot_choice(handles);

    if(get(handles.autoapply_ext,'Value')==true)
        handles=apply_correction_EXT_Callback(handles.apply_correction_EXT, eventdata, handles);
    end
    
    enable_all(handles);    
    guidata(hObject,handles);
    
end


% --- Executes on button press in autoapply_ext.
function autoapply_ext_Callback(hObject, eventdata, handles)
% hObject    handle to autoapply_ext (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of autoapply_ext

%    set(handles.autoapply_ext,'Value',true);
    guidata(hObject,handles);
end


% --- Executes on button press in apply_correction_EXT.
function handles=apply_correction_EXT_Callback(hObject, eventdata, handles)
% hObject    handle to apply_correction_EXT (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
    global BEAMLINE PS GIRDER INSTR FS;

    handles=output(handles,'apply EXT correction...');
    
    disable_all(handles);

    %need mover info for cancelletion
    mover_pos0=get_mover(handles.mover);

    [ps_oldread]=set_ps([handles.xcor_ps handles.ycor_ps],[handles.xcor_ps_newvalue handles.ycor_ps_newvalue]);
    handles.oldread_date=strvcat(handles.oldread_date,[datestr(now,31) '    EXT cor']);
    handles.ps_oldread{end+1}=ps_oldread;
    handles.ps_oldps{end+1}=[handles.xcor_ps handles.ycor_ps];
    handles.mover_oldread{end+1}=mover_pos0;
    handles.mover_old{end+1}=handles.mover;
    handles.bpm_read_old{end+1}=handles.bpm_read;
    handles.IP_read_old{end+1}=handles.bpm_read;
    handles.bpm_read_expected_old{end+1}=handles.bpm_read_expected;
    handles.IP_read_expected_old{end+1}=handles.bpm_read_expected;
    
    handles.bpm_read_before=handles.bpm_read;
    handles.IP_read_before=handles.IP_read;

    IEX=findcells(BEAMLINE, 'Name','IEX');

    [bpm0,nbpm0,bpm_read0,bpm_resol0,bpm_S0,bpm_name0]=get_bpm_yves(handles.naverage);
    %range=find( ~isnan(bpm_read0(:,1).') & bpm0>=IEX );
    range=find(bpm0>=IEX );
    handles.bpm=bpm0(range);
    handles.nbpm=getcolumn(size(handles.bpm),2);
    handles.bpm_read=bpm_read0(range,1:2);
    handles.bpm_resol=bpm_resol0(range,1:2);
    handles.bpm_S=bpm_S0(range);
    handles.bpm_name=bpm_name0(range,:);

    FlHwUpdate;
    ipdata = getIPData;
    handles.IP_read=[ipdata.x;ipdata.xp;ipdata.y;ipdata.yp];
    
    filename = {['testApps/trajectory_correction/data/' datestr(now,30) '.mat']};
    save(filename{1},'handles','GIRDER','PS','BEAMLINE','INSTR','FS');
    
    handles=plot_choice(handles);

    disable_all(handles);

    handles=output(handles,'EXT correction applied');
    
    guidata(hObject,handles);    

end


% --- Executes on button press in EXIT.
function EXIT_Callback(hObject, eventdata, handles)
% hObject    handle to EXIT (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
    delete(handles.orbit_steering_fig)
end



function xgain_Callback(hObject, eventdata, handles)
% hObject    handle to xgain (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

    if(str2double(get(hObject, 'String'))~=handles.xgain_val)    
	handles=output(handles,'xgain changed, compute correction again !');
    end
    handles.xgain_val = str2double(get(hObject, 'String'));
    guidata(hObject,handles);
% Hints: get(hObject,'String') returns contents of xgain as text
%        str2double(get(hObject,'String')) returns contents of xgain as a double
end

% --- Executes during object creation, after setting all properties.
function xgain_CreateFcn(hObject, eventdata, handles)
% hObject    handle to xgain (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
    handles.xgain_val = str2double(get(hObject, 'String'));
    guidata(hObject,handles);
    if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
        set(hObject,'BackgroundColor','white');
    end
end


function ygain_Callback(hObject, eventdata, handles)
% hObject    handle to ygain (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

    if(str2double(get(hObject, 'String'))~=handles.ygain_val)    
	handles=output(handles,'ygain changed, compute correction again !');
    end
    handles.ygain_val = str2double(get(hObject, 'String'));
    guidata(hObject,handles);
% Hints: get(hObject,'String') returns contents of ygain as text
%        str2double(get(hObject,'String')) returns contents of ygain as a double
end

% --- Executes during object creation, after setting all properties.
function ygain_CreateFcn(hObject, eventdata, handles)
% hObject    handle to ygain (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
    handles.ygain_val = str2double(get(hObject, 'String'));
    guidata(hObject,handles);
    if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
        set(hObject,'BackgroundColor','white');
    end
end


% --- Executes on button press in compute_ff.
function compute_ff_Callback(hObject, eventdata, handles)
% hObject    handle to compute_ff (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
    global BEAMLINE;
        
    handles=output(handles,'compute FF correction...');
    
    disable_all(handles);

    FlHwUpdate();
    
    invalid=isnan(handles.bpm_read(handles.bpm_range,1)) | isnan(handles.bpm_read(handles.bpm_range,2));    
    if any(invalid)
        handles=output(handles,'NaN values BPMS disabled :');
        handles=output(handles,handles.bpm_name(handles.bpm_range(invalid),:));
        handles.bpm_range=handles.bpm_range(~invalid);
    end
    invalid=isnan(handles.bpm_ref(handles.bpm_range,1)) | isnan(handles.bpm_ref(handles.bpm_range,2));
    if any(invalid)
        handles=output(handles,'NaN values BPMS disabled :');
        handles=output(handles,handles.bpm_name(handles.bpm_range(invalid),:));
        handles.bpm_range=handles.bpm_range(~invalid);
    end
    [handles.xmatrix_FF,handles.ymatrix_FF]=get_mover_matrix(handles.bpm,handles.mover(handles.mover_range),handles.mover(handles.mover_range));
    [handles.xmatrix_IP,handles.ymatrix_IP]=get_mover_matrix_IP(handles.mover(handles.mover_range),handles.mover(handles.mover_range));
    [handles.xmover_pos,handles.ymover_pos]=compute_steering_FF(handles.bpm_read(handles.bpm_range,:)*diag([handles.xgain_val handles.ygain_val]),handles.weight(handles.bpm_range,:),handles.bpm_ref(handles.bpm_range,:),handles.mover(handles.mover_range),handles.mover(handles.mover_range),handles.xmatrix_FF(handles.bpm_range,:),handles.ymatrix_FF(handles.bpm_range,:));
    handles.xmover_read=getcolumn(get_mover(handles.mover),1).';
    handles.ymover_read=getcolumn(get_mover(handles.mover),2).';
    handles=output(handles,'mover changes :');
    for i=1:length(handles.mover_range)
        handles=output(handles,sprintf('%s %g %g', handles.mover_name(handles.mover_range(i),:), handles.xmover_pos(i)-handles.xmover_read(i), handles.ymover_pos(i)-handles.ymover_read(i)));    
    end
    handles.nmover=length(handles.mover);
    handles.bpm_read_expected(:,1)=handles.xmatrix_FF*(handles.xmover_pos-handles.xmover_read(handles.mover_range)).'+handles.bpm_read(:,1);
    handles.bpm_read_expected(:,2)=handles.ymatrix_FF*(handles.ymover_pos-handles.ymover_read(handles.mover_range)).'+handles.bpm_read(:,2);
    handles.IP_read_expected= handles.xmatrix_IP*(handles.xmover_pos-handles.xmover_read(handles.mover_range)).'+...
                              handles.ymatrix_IP*(handles.ymover_pos-handles.ymover_read(handles.mover_range)).'+...
                              handles.IP_read;
    
    if(isfield(handles,'bpm_read_before'))
        handles=rmfield(handles,'bpm_read_before');
    end
    if(isfield(handles,'IP_read_before'))
        handles=rmfield(handles,'IP_read_before');
    end
    if(isfield(handles,'xcor_ps_newvalue'))
        handles=rmfield(handles,'xcor_ps_newvalue');
    end
    if(isfield(handles,'ycor_ps_newvalue'))
        handles=rmfield(handles,'ycor_ps_newvalue');
    end
    
    handles=plot_choice(handles);

    if(get(handles.autoapply_ff,'Value')==true)
        handles=apply_ff_Callback(handles.apply_ff, eventdata, handles);
    end

    enable_all(handles);

    guidata(hObject,handles);    
end


% --- Executes on button press in autoapply_ff.
function autoapply_ff_Callback(hObject, eventdata, handles)
% hObject    handle to autoapply_ff (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of autoapply_ff
%    set(handles.autoapply_ff,'Value',true);
end


% --- Executes on button press in apply_ff.
function handles=apply_ff_Callback(hObject, eventdata, handles)
% hObject    handle to apply_ff (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
    global BEAMLINE PS GIRDER INSTR FS;
    
    handles=output(handles,'apply FF correction...');
    
    disable_all(handles);

    %need ps for cancellation
    [nxcor,handles.xcor_ps,handles.xcor_ps_read]=get_corrector(handles.xcor);
    [nycor,handles.ycor_ps,handles.ycor_ps_read]=get_corrector(handles.ycor);
    
    mover_pos0=get_mover(handles.mover);
    set_mover(handles.mover(handles.mover_range),[handles.xmover_pos.' handles.ymover_pos.' mover_pos0(handles.mover_range,3)]);

    handles.oldread_date=strvcat(handles.oldread_date,[datestr(now,31) '    FF cor']);
    handles.mover_oldread{end+1}=mover_pos0;
    handles.mover_old{end+1}=handles.mover;
    handles.ps_oldread{end+1}=[handles.xcor_ps_read handles.ycor_ps_read];
    handles.ps_oldps{end+1}=[handles.xcor_ps handles.ycor_ps];    
    handles.bpm_read_old{end+1}=handles.bpm_read;
    handles.IP_read_old{end+1}=handles.bpm_read;
    handles.bpm_read_expected_old{end+1}=handles.bpm_read_expected;
    handles.IP_read_expected_old{end+1}=handles.bpm_read_expected;

    handles.bpm_read_before=handles.bpm_read;
    handles.IP_read_before=handles.IP_read;

    IEX=findcells(BEAMLINE, 'Name','IEX');

    [bpm0,nbpm0,bpm_read0,bpm_resol0,bpm_S0,bpm_name0]=get_bpm_yves(handles.naverage);
    %range=find( ~isnan(bpm_read0(:,1).') & bpm0>=IEX );
    range=find(bpm0>=IEX );
    handles.bpm=bpm0(range);
    handles.nbpm=getcolumn(size(handles.bpm),2);
    handles.bpm_read=bpm_read0(range,1:2);
    handles.bpm_resol=bpm_resol0(range,1:2);
    handles.bpm_S=bpm_S0(range);
    handles.bpm_name=bpm_name0(range,:);

    FlHwUpdate;
    ipdata = getIPData;
    handles.IP_read=[ipdata.x;ipdata.xp;ipdata.y;ipdata.yp];
    
    filename = {['testApps/trajectory_correction/data/' datestr(now,30) '.mat']};
    save(filename{1},'handles','GIRDER','PS','BEAMLINE','INSTR','FS');

    handles=plot_choice(handles);

    enable_all(handles);

    handles=output(handles,'FF correction applied');
    
    guidata(hObject,handles);    
    
end

% --- Executes on button press in compute_ip.
function compute_ip_Callback(hObject, eventdata, handles)
% hObject    handle to compute_ip (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
    global BEAMLINE;
        
    handles=output(handles,'compute IP correction...');
    
    disable_all(handles);

    FlHwUpdate();

    [handles.xmatrix_FF,handles.ymatrix_FF]=get_mover_matrix(handles.bpm,handles.mover(handles.mover_range),handles.mover(handles.mover_range));    
    [handles.xmatrix_IP,handles.ymatrix_IP]=get_mover_matrix_IP(handles.mover(handles.mover_range),handles.mover(handles.mover_range));
    [handles.xmover_pos,handles.ymover_pos]=compute_steering_IP(handles.IP_read.*[handles.xgain_val;handles.xgain_val;handles.ygain_val;handles.ygain_val],handles.IP_ref,handles.mover(handles.mover_range),handles.mover(handles.mover_range),handles.xmatrix_IP,handles.ymatrix_IP);
    handles.xmover_read=getcolumn(get_mover(handles.mover),1).';
    handles.ymover_read=getcolumn(get_mover(handles.mover),2).';
    handles=output(handles,'mover changes :');
    for i=1:length(handles.mover_range)
        handles=output(handles,sprintf('%s %g %g', handles.mover_name(handles.mover_range(i),:), handles.xmover_pos(i)-handles.xmover_read(i), handles.ymover_pos(i)-handles.ymover_read(i)));    
    end
    handles.nmover=length(handles.mover);
    handles.bpm_read_expected(:,1)=handles.xmatrix_FF*(handles.xmover_pos-handles.xmover_read(handles.mover_range)).'+handles.bpm_read(:,1);
    handles.bpm_read_expected(:,2)=handles.ymatrix_FF*(handles.ymover_pos-handles.ymover_read(handles.mover_range)).'+handles.bpm_read(:,2);
    handles.IP_read_expected= handles.xmatrix_IP*(handles.xmover_pos-handles.xmover_read(handles.mover_range)).'+...
                              handles.ymatrix_IP*(handles.ymover_pos-handles.ymover_read(handles.mover_range)).'+...
                              handles.IP_read;
    
    if(isfield(handles,'bpm_read_before'))
        handles=rmfield(handles,'bpm_read_before');
    end
    if(isfield(handles,'IP_read_before'))
        handles=rmfield(handles,'IP_read_before');
    end
    if(isfield(handles,'xcor_ps_newvalue'))
        handles=rmfield(handles,'xcor_ps_newvalue');
    end
    if(isfield(handles,'ycor_ps_newvalue'))
        handles=rmfield(handles,'ycor_ps_newvalue');
    end
    
    handles=plot_choice(handles);

     if(get(handles.autoapply_ip,'Value')==true)
         handles=apply_ip_Callback(handles.apply_ip, eventdata, handles);
     end

    enable_all(handles);
   
    guidata(hObject,handles);    
end

% --- Executes on button press in autoapply_ip.
function autoapply_ip_Callback(hObject, eventdata, handles)
% hObject    handle to autoapply_ip (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of autoapply_ip
end

% --- Executes on button press in apply_ip.
function handles=apply_ip_Callback(hObject, eventdata, handles)
% hObject    handle to apply_ip (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
    global BEAMLINE PS GIRDER INSTR FS;
    
    handles=output(handles,'apply IP correction...');
    
    disable_all(handles);

    %need ps for cancellation
    [nxcor,handles.xcor_ps,handles.xcor_ps_read]=get_corrector(handles.xcor);
    [nycor,handles.ycor_ps,handles.ycor_ps_read]=get_corrector(handles.ycor);
    
    mover_pos0=get_mover(handles.mover);
    set_mover(handles.mover(handles.mover_range),[handles.xmover_pos.' handles.ymover_pos.' mover_pos0(handles.mover_range,3)]);

    handles.oldread_date=strvcat(handles.oldread_date,[datestr(now,31) '    IP cor']);
    handles.mover_oldread{end+1}=mover_pos0;
    handles.mover_old{end+1}=handles.mover;
    handles.ps_oldread{end+1}=[handles.xcor_ps_read handles.ycor_ps_read];
    handles.ps_oldps{end+1}=[handles.xcor_ps handles.ycor_ps];
    handles.bpm_read_old{end+1}=handles.bpm_read;
    handles.IP_read_old{end+1}=handles.bpm_read;
    handles.bpm_read_expected_old{end+1}=handles.bpm_read_expected;
    handles.IP_read_expected_old{end+1}=handles.bpm_read_expected;

    handles.bpm_read_before=handles.bpm_read;
    handles.IP_read_before=handles.IP_read;

    IEX=findcells(BEAMLINE, 'Name','IEX');

    [bpm0,nbpm0,bpm_read0,bpm_resol0,bpm_S0,bpm_name0]=get_bpm_yves(handles.naverage);
    %range=find( ~isnan(bpm_read0(:,1).') & bpm0>=IEX );
    range=find(bpm0>=IEX );
    handles.bpm=bpm0(range);
    handles.nbpm=getcolumn(size(handles.bpm),2);
    handles.bpm_read=bpm_read0(range,1:2);
    handles.bpm_resol=bpm_resol0(range,1:2);
    handles.bpm_S=bpm_S0(range);
    handles.bpm_name=bpm_name0(range,:);

    FlHwUpdate;
    ipdata = getIPData;
    handles.IP_read=[ipdata.x;ipdata.xp;ipdata.y;ipdata.yp];

    filename = {['testApps/trajectory_correction/data/' datestr(now,30) '.mat']};
    save(filename{1},'handles','GIRDER','PS','BEAMLINE','INSTR','FS');

    handles=plot_choice(handles);

    enable_all(handles);

    handles=output(handles,'IP correction applied');
    
    guidata(hObject,handles);    
    
end

% --- Executes on button press in select_mover.
function select_mover_Callback(hObject, eventdata, handles)
% hObject    handle to select_mover (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
    global BEAMLINE;

    [selection,ok]=listdlg('PromptString',{'Select movers :' '' '(Shift+clik = continuous selection)' '(Crtl+clik = uncontinuous selection)'},...
                'ListString',handles.mover_name,'ListSize',[250 min(handles.nmover*15,500)],...
                'InitialValue',handles.mover_range,'Name','Select Mover');
    if(ok)
        handles.mover_range=selection;
    end
     guidata(hObject,handles);
end


% --- Executes on button press in cancel_correction.
function cancel_correction_Callback(hObject, eventdata, handles)
% hObject    handle to cancel_correction (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
    global BEAMLINE

    if(~isfield(handles,'oldread_date'))
        handles=output(handles,'No action to cancel !');
        return
    end
    ndate=getcolumn(size(handles.oldread_date),1);
    ex = get(0,'defaultuicontrolfontsize')*1.7;
    listsize=[250 min((ndate+1)*ex,500)];
    ylimit{1}=[min(handles.bpm_read(:,1)) max(handles.bpm_read(:,1))];
    for i=1:ndate;
        ylimit{1}=[min([handles.bpm_read_old{i}(:,1);ylimit{1}(1)]) max([handles.bpm_read_old{i}(:,1);ylimit{1}(2)])];
    end
    ylimit{2}=[min(handles.bpm_read(:,2)) max(handles.bpm_read(:,2))];
    for i=1:ndate;
        ylimit{2}=[min([handles.bpm_read_old{i}(:,2);ylimit{2}(1)]) max([handles.bpm_read_old{i}(:,2);ylimit{2}(2)])];
    end
    [selection,ok]=list_plot_dlg('PromptString','Select the date of the setting you want come back :',...
                'ListString',strvcat(handles.oldread_date,'Now'),...
                'SelectionMode','single',...
                'ListSize',listsize,...
                'InitialValue',ndate+1,...
                'Name','Cancel Correction',...
                'NPlot',2,...
                'PlotSize',[500,100],...
                'XData',handles.bpm_S',...
                'YData',[handles.bpm_read_old {handles.bpm_read}],...
                'ylim',ylimit);

    if(ok && selection~=ndate+1)
        handles=output(handles,'Cancel correction...');
        [ps_oldread]=set_ps(handles.ps_oldps{selection},handles.ps_oldread{selection});
        mover_pos0=get_mover(handles.mover_old{selection});
        set_mover(handles.mover_old{selection},handles.mover_oldread{selection});

        handles.oldread_date=strvcat(handles.oldread_date,[datestr(now,31) '    Cancel.']);
        handles.ps_oldread{end+1}=ps_oldread;
        handles.ps_oldps{end+1}=handles.ps_oldps{selection};
        handles.mover_oldread{end+1}=mover_pos0;
        handles.mover_old{end+1}=handles.mover_old{selection};        
        handles.bpm_read_old{end+1}=handles.bpm_read;
        handles.IP_read_old{end+1}=handles.bpm_read;

        IEX=findcells(BEAMLINE, 'Name','IEX');

        [bpm0,nbpm0,bpm_read0,bpm_resol0,bpm_S0,bpm_name0]=get_bpm_yves(handles.naverage);
        %range=find( ~isnan(bpm_read0(:,1).') & bpm0>=IEX );
        range=find(bpm0>=IEX );
        handles.bpm=bpm0(range);
        handles.nbpm=getcolumn(size(handles.bpm),2);
        handles.bpm_read=bpm_read0(range,1:2);
        handles.bpm_resol=bpm_resol0(range,1:2);
        handles.bpm_S=bpm_S0(range);
        handles.bpm_name=bpm_name0(range,:);

        if(isfield(handles,'bpm_read_expected'))
            handles=rmfield(handles,'bpm_read_expected');
        end
        if(isfield(handles,'bpm_read_before'))
            handles=rmfield(handles,'bpm_read_before');
        end
        if(isfield(handles,'xcor_ps_newvalue'))
            handles=rmfield(handles,'xcor_ps_newvalue');
        end
        if(isfield(handles,'ycor_ps_newvalue'))
            handles=rmfield(handles,'ycor_ps_newvalue');
        end
        if(isfield(handles,'xmover_pos'))
            handles=rmfield(handles,'xmover_pos');
        end
        if(isfield(handles,'ymover_pos'))
            handles=rmfield(handles,'ymover_pos');
        end

        handles=plot_choice(handles);

        handles=output(handles,'Correction cancelled');
    end

    guidata(hObject,handles);
end


% --- Executes when selected object is changed in plot_choice.
function plot_choice_SelectionChangeFcn(hObject, eventdata, handles)
% hObject    handle to the selected object in plot_choice 
% eventdata  structure with the following fields (see UIBUTTONGROUP)
%	EventName: string 'SelectionChanged' (read only)
%	OldValue: handle of the previously selected object or empty if none was selected
%	NewValue: handle of the currently selected object
% handles    structure with handles and user data (see GUIDATA)
    handles.choice=get(hObject,'Tag');
    
    disable_all(handles);
    
    handles=plot_choice(handles);
    
    enable_all(handles);
    
    guidata(hObject,handles);
end


% --- Executes on button press in Ref_table.
function Ref_table_Callback(hObject, eventdata, handles)
% hObject    handle to Ref_table (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

%    handles.bpm_ref=handles.bpm_read;
%    handles=plot_choice(handles);
    f=figure('Position',[1 1 700 800]);
    dat =  [handles.bpm_ref(:,1) handles.bpm_read(:,1) handles.weight(:,1) ...
                handles.bpm_ref(:,2) handles.bpm_read(:,2) handles.weight(:,2);...
            handles.IP_ref(1) handles.IP_read(1) NaN ...
                handles.IP_ref(3) handles.IP_read(3) NaN;
            handles.IP_ref(2) handles.IP_read(2) NaN ...
                handles.IP_ref(4) handles.IP_read(4) NaN;];
    columnname =   {'X reference', 'X Reading', 'X Weight', 'Y reference', 'Y Reading', 'Y Weight'};
    columnformat = {'short e', 'short e', 'short e', 'short e', 'short e', 'short e'};
    columneditable =  [true false true true false true];
    columnwidth ={100 100 100 100 100 100};
    rowname = strvcat(handles.bpm_name,['IP pos';'IP ang']);
    t = uitable('Units','normalized',...
                'Position', [0 0 1 1], ...
                'Data', dat,...
                'Parent',f,...
                'RowStriping','on',...
                'ColumnName', columnname,...
                'ColumnFormat', columnformat,...
                'ColumnEditable', columneditable,...
                'ColumnWidth',columnwidth,...
                'RowName',rowname,...
                'DeleteFcn','dat = get(gcbo,''Data'');save -v7 tmp.mat dat;');
    waitfor(t);        
    load('tmp.mat');
    eval('!rm tmp.mat')
    handles.bpm_ref=dat(1:end-2,[1 4]);    
    handles.weight=dat(1:end-2,[3 6]);
    handles.IP_ref=[dat(end-1,1); dat(end,1); dat(end-1,4); dat(end,4)];
    handles=plot_choice(handles);
    guidata(hObject,handles);
end

% --- Executes on button press in save_ref_orbit.
function save_ref_orbit_Callback(hObject, eventdata, handles)
% hObject    handle to save_ref_orbit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
    prompt = {'Enter file name:'};
    dlg_title = 'Save Ref Orbit';
    num_lines = 1;
    def = {[datestr(now,30) '.mat']};
    filename = inputdlg(prompt,dlg_title,num_lines,def);
    save(filename{1});
end


% --- Executes on button press in load_ref_orbit.
function load_ref_orbit_Callback(hObject, eventdata, handles)
% hObject    handle to load_ref_orbit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
    d = dir;
    str = {d.name};
    [selection,ok]=listdlg('PromptString',{'Select the file to load :'},...
                'ListString',str,'ListSize',[250 min(length(str)*15,500)],...
                'InitialValue',1,'Name','Load Ref Orbit',...
                'SelectionMode','single');
    if(ok)
        load_handle=load(str{selection});
        size_bpm_ref=size(handles.bpm_ref);
        size_load_bpm_ref=size(load_handle.handles.bpm_ref);
        if(size_load_bpm_ref~=size_bpm_ref)
            handles=output(handles,'Size of reference and current orbit does not match : load canceled.');
            guidata(hObject,handles);
            return
        end
        handles.bpm_ref=load_handle.handles.bpm_ref;
        handles=output(handles,handles.bpm_ref);
    end
    
    disable_all(handles);

    handles=plot_choice(handles);
    
    enable_all(handles);

    guidata(hObject,handles);

end


% --- Executes on button press in subst_ref_orbit.
function subst_ref_orbit_Callback(hObject, eventdata, handles)
% hObject    handle to subst_ref_orbit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of subst_ref_orbit
    handles.sub_ref_orbit=get(hObject,'Value');
    if strcmp(handles.choice,'plot_bpm')
        disable_all(handles);

        handles=plot_choice(handles);
    
        enable_all(handles);
    end
    guidata(hObject,handles);
end


function sbpm_range_Callback(hObject, eventdata, handles)
% hObject    handle to sbpm_range (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of sbpm_range as text
%        str2double(get(hObject,'String')) returns contents of sbpm_range
%        as a double
    switch handles.choice   % Get Tag of selected object
        case 'plot_bpm'
            handles.srange_default = eval(get(hObject, 'String'));
        case 'plot_cor'
            handles.srange_default = eval(get(hObject, 'String'));
        case 'plot_mover'
            handles.srange_default = eval(get(hObject, 'String'));
        case 'plot_IP'
            handles.sIPrange_default = eval(get(hObject, 'String'));
        otherwise
            handles=output(handles,['ERROR : ' handles.choice ' plot is unknow !']);
    end
    handles=plot_choice(handles);
    
    guidata(hObject,handles);
end


% --- Executes during object creation, after setting all properties.
function sbpm_range_CreateFcn(hObject, eventdata, handles)
% hObject    handle to sbpm_range (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
end


function xbpm_range_Callback(hObject, eventdata, handles)
% hObject    handle to xbpm_range (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of xbpm_range as text
%        str2double(get(hObject,'String')) returns contents of xbpm_range as a double
    switch handles.choice   % Get Tag of selected object
        case 'plot_bpm'
            handles.xbpm_range_default = eval(get(hObject, 'String'));
        case 'plot_cor'
            handles.xcor_range_default = eval(get(hObject, 'String'));
        case 'plot_mover'
            handles.xmov_range_default = eval(get(hObject, 'String'));
        case 'plot_IP'
            handles.xIP_range_default = eval(get(hObject, 'String'));
        otherwise
            handles=output(handles,['ERROR : ' handles.choice ' plot is unknow !']);
    end
    handles=plot_choice(handles);
    
    guidata(hObject,handles);
end


% --- Executes during object creation, after setting all properties.
function xbpm_range_CreateFcn(hObject, eventdata, handles)
% hObject    handle to xbpm_range (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
end

function ybpm_range_Callback(hObject, eventdata, handles)
% hObject    handle to ybpm_range (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of ybpm_range as text
%        str2double(get(hObject,'String')) returns contents of ybpm_range as a
%        double
    switch handles.choice   % Get Tag of selected object
        case 'plot_bpm'
            handles.ybpm_range_default = eval(get(hObject, 'String'));
        case 'plot_cor'
            handles.ycor_range_default = eval(get(hObject, 'String'));
        case 'plot_mover'
            handles.ymov_range_default = eval(get(hObject, 'String'));
        case 'plot_IP'
            handles.yIP_range_default = eval(get(hObject, 'String'));
        otherwise
            handles=output(handles,['ERROR : ' handles.choice ' plot is unknow !']);
    end
    handles=plot_choice(handles);
    
    guidata(hObject,handles);
end

% --- Executes during object creation, after setting all properties.
function ybpm_range_CreateFcn(hObject, eventdata, handles)
% hObject    handle to ybpm_range (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
end


% --------------------------------------------------------------------
function xbpm_legend_Callback(hObject, eventdata, handles)
% hObject    handle to ybpm_legend (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
    if(handles.xlegend)
        handles.xlegend=false;
    else
        handles.xlegend=true;
    end
    handles=plot_choice(handles);
    guidata(hObject,handles);
end


% --------------------------------------------------------------------
function xbpm_context_menu_Callback(hObject, eventdata, handles)
% hObject    handle to xbpm_context_menu (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
end


% --------------------------------------------------------------------
function ybpm_legend_Callback(hObject, eventdata, handles)
% hObject    handle to ybpm_legend (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
    if(handles.ylegend)
        handles.ylegend=false;
    else
        handles.ylegend=true;
    end
    handles=plot_choice(handles);
    guidata(hObject,handles);
end


% --------------------------------------------------------------------
function ybpm_context_menu_Callback(hObject, eventdata, handles)
% hObject    handle to ybpm_context_menu (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
end



% --- Executes during object creation, after setting all properties.
function xbpm_CreateFcn(hObject, eventdata, handles)
% hObject    handle to xbpm (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: place code in OpeningFcn to populate xbpm
end



function output_txt_Callback(hObject, eventdata, handles)
% hObject    handle to output_txt (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of output_txt as text
%        str2double(get(hObject,'String')) returns contents of output_txt as a double
end


% --- Executes during object creation, after setting all properties.
function output_txt_CreateFcn(hObject, eventdata, handles)
% hObject    handle to output_txt (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
end

function handles=plot_choice(handles)

    global BEAMLINE
    
    IEX=getcolumn(findcells(BEAMLINE, 'Name','IEX'),1);
    switch handles.choice   % Get Tag of selected object
        case 'plot_bpm'
            plot_magnets_external(BEAMLINE(IEX:end),handles.beamline,BEAMLINE{IEX}.S+handles.srange_default(1),BEAMLINE{IEX}.S+handles.srange_default(2),1,0);
            set(handles.sbpm_range,'String',sprintf('[%g %g]',handles.srange_default(1), handles.srange_default(2)));
            set(handles.xbpm_range,'String',sprintf('[%g %g]',handles.xbpm_range_default(1), handles.xbpm_range_default(2)));
            set(handles.ybpm_range,'String',sprintf('[%g %g]',handles.ybpm_range_default(1), handles.ybpm_range_default(2)));
            handles=plot_bpm(handles);
        case 'plot_cor'
            plot_magnets_external(BEAMLINE(IEX:end),handles.beamline,BEAMLINE{IEX}.S+handles.srange_default(1),BEAMLINE{IEX}.S+handles.srange_default(2),1,0);
            set(handles.sbpm_range,'String',sprintf('[%g %g]',handles.srange_default(1), handles.srange_default(2)));
            set(handles.xbpm_range,'String',sprintf('[%g %g]',handles.xcor_range_default(1), handles.xcor_range_default(2)));
            set(handles.ybpm_range,'String',sprintf('[%g %g]',handles.ycor_range_default(1), handles.ycor_range_default(2)));
            handles=plot_cor(handles);
        case 'plot_mover'
            plot_magnets_external(BEAMLINE(IEX:end),handles.beamline,BEAMLINE{IEX}.S+handles.srange_default(1),BEAMLINE{IEX}.S+handles.srange_default(2),1,0);
            set(handles.sbpm_range,'String',sprintf('[%g %g]',handles.srange_default(1), handles.srange_default(2)));
            set(handles.xbpm_range,'String',sprintf('[%g %g]',handles.xmov_range_default(1), handles.xmov_range_default(2)));
            set(handles.ybpm_range,'String',sprintf('[%g %g]',handles.ymov_range_default(1), handles.ymov_range_default(2)));
            handles=plot_mover(handles);
        case 'plot_IP'
            plot_magnets_external(BEAMLINE(IEX:end),handles.beamline,BEAMLINE{IEX}.S+handles.sIPrange_default(1),BEAMLINE{IEX}.S+handles.sIPrange_default(2),1,0);
            set(handles.sbpm_range,'String',sprintf('[%g %g]',handles.sIPrange_default(1), handles.sIPrange_default(2)));
            set(handles.xbpm_range,'String',sprintf('[%g %g]',handles.xIP_range_default(1), handles.xIP_range_default(2)));
            set(handles.ybpm_range,'String',sprintf('[%g %g]',handles.yIP_range_default(1), handles.yIP_range_default(2)));
            handles=plot_IP(handles);
        otherwise
            handles=output(handles,['ERROR : ' handles.choice ' plot is unknow !']);
    end
end


function handles=plot_bpm(handles)
    
    global BEAMLINE;

    range=find((handles.bpm_S>=handles.srange_default(1))&(handles.bpm_S<=handles.srange_default(2)));
    
    axes(handles.xbpm);
    if(handles.sub_ref_orbit)
        if(isfield(handles,'bpm_read_before'))
            plot(handles.bpm_S(range),handles.bpm_read_before(range,1)-handles.bpm_ref(range,1),'m--+');
            hold('on');
            plot(handles.bpm_S(range),handles.bpm_read_expected(range,1)-handles.bpm_ref(range,1),'r-+');
            errorbar(handles.bpm_S,handles.bpm_read(range,1)-handles.bpm_ref(range,1),handles.bpm_resol(range,1),'b-+');
            hold('off');
            legend('previous','predicted','present');
        elseif(isfield(handles,'bpm_read_expected'))
            plot(handles.bpm_S(range),handles.bpm_read_expected(range,1)-handles.bpm_ref(range,1),'r-+');
            hold('on');
            errorbar(handles.bpm_S,handles.bpm_read(range,1)-handles.bpm_ref(range,1),handles.bpm_resol(range,1),'b-+');
            hold('off');
            legend('after cor','present');
        else
            errorbar(handles.bpm_S,handles.bpm_read(range,1)-handles.bpm_ref(range,1),handles.bpm_resol(range,1),'b-+');
            legend('present');
        end
    else
        if(isfield(handles,'bpm_read_before'))
            plot(handles.bpm_S(range),handles.bpm_ref(range,1),'g-o');
            hold('on');
            plot(handles.bpm_S(range),handles.bpm_read_before(range,1),'m--+');
            plot(handles.bpm_S(range),handles.bpm_read_expected(range,1),'r-+');
            errorbar(handles.bpm_S,handles.bpm_read(range,1),handles.bpm_resol(range,1),'b-+');
            legend('reference','previous','predicted','present');
            hold('off');
        elseif(isfield(handles,'bpm_read_expected'))
            plot(handles.bpm_S(range),handles.bpm_ref(range,1),'g-o');
            hold('on');
            plot(handles.bpm_S(range),handles.bpm_read_expected(range,1),'r-+');
            errorbar(handles.bpm_S,handles.bpm_read(range,1),handles.bpm_resol(range,1),'b-+');
            hold('off');
            legend('reference','after cor','present');
        else
            plot(handles.bpm_S(range),handles.bpm_ref(range,1),'g-o');
            hold('on');
            errorbar(handles.bpm_S,handles.bpm_read(range,1),handles.bpm_resol(range,1),'b-+');
            hold('off');
            legend('reference','present');
        end
    end
    ylabel('X BPM reading [m]');
    axis([handles.srange_default handles.xbpm_range_default]);
    hor_line(0,'k:');
    set(gca,'XTick',handles.bpm_S(range));
    set(gca,'XTickLabel',handles.bpm_name(range,:));
    xticklabel_rotate90();
    if(handles.xlegend)
        legend(gca,'show')
    else
        legend(gca,'hide')
    end
    
    axes(handles.ybpm);
    if(handles.sub_ref_orbit)
        if(isfield(handles,'bpm_read_before'))
            plot(handles.bpm_S(range),handles.bpm_read_before(range,2)-handles.bpm_ref(range,2),'m--+');
            hold('on');
            plot(handles.bpm_S(range),handles.bpm_read_expected(range,2)-handles.bpm_ref(range,2),'r-+');
            errorbar(handles.bpm_S,handles.bpm_read(range,2)-handles.bpm_ref(range,2),handles.bpm_resol(range,2),'b-+');
            hold('off');
            legend('previous','predicted','present');
        elseif(isfield(handles,'bpm_read_expected'))
            plot(handles.bpm_S(range),handles.bpm_read_expected(range,2)-handles.bpm_ref(range,2),'r-+');
            hold('on');
            errorbar(handles.bpm_S,handles.bpm_read(range,2)-handles.bpm_ref(range,2),handles.bpm_resol(range,2),'b-+');
            hold('off');
            legend('after cor','present');
        else
            errorbar(handles.bpm_S,handles.bpm_read(range,2)-handles.bpm_ref(range,2),handles.bpm_resol(range,2),'b-+');
            legend('present');
        end
    else
        if(isfield(handles,'bpm_read_before'))
            plot(handles.bpm_S(range),handles.bpm_ref(range,2),'g-o');
            hold('on');
            plot(handles.bpm_S(range),handles.bpm_read_before(range,2),'m--+');
            plot(handles.bpm_S(range),handles.bpm_read_expected(range,2),'r-+');
            errorbar(handles.bpm_S,handles.bpm_read(range,2),handles.bpm_resol(range,2),'b-+');
            hold('off');
            legend('reference','previous','predicted','present');
        elseif(isfield(handles,'bpm_read_expected'))
            plot(handles.bpm_S(range),handles.bpm_ref(range,2),'g-o');
            hold('on');
            plot(handles.bpm_S(range),handles.bpm_read_expected(range,2),'r-+');
            errorbar(handles.bpm_S,handles.bpm_read(range,2),handles.bpm_resol(range,2),'b-+');
            hold('off');
            legend('reference','predicted','present');
        else
            plot(handles.bpm_S(range),handles.bpm_ref(range,2),'g-o');
            hold('on');
            errorbar(handles.bpm_S,handles.bpm_read(range,2),handles.bpm_resol(range,2),'b-+');
            hold('off');
            legend('reference','present');
        end
    end
    xlabel('S [m]');
    ylabel('Y BPM reading [m]');
    axis([handles.srange_default handles.ybpm_range_default]);
    hor_line(0,'k:');
    if(handles.ylegend)
        legend(gca,'show')
    else
        legend(gca,'hide')
    end
end


function handles=plot_IP(handles)
    
    global BEAMLINE;
    IEX=findcells(BEAMLINE,'Name','IEX');
    IEX_S=BEAMLINE{IEX}.S;
    IP=findcells(BEAMLINE,'Name','IP');
    IP_S=BEAMLINE{IP}.S;
    QD0=getcolumn( findcells(BEAMLINE,'Name','QD0FF'),2);
    QD0_S=BEAMLINE{QD0}.S;
    BDUMPB=findcells(BEAMLINE,'Name','BDUMPB');
    BDUMPB_S=BEAMLINE{BDUMPB}.S;
    range=find((handles.bpm_S>=handles.sIPrange_default(1))&(handles.bpm_S<=handles.sIPrange_default(2)));

    axes(handles.xbpm);
    if(handles.sub_ref_orbit)
        if(isfield(handles,'bpm_read_before'))
            plot(handles.bpm_S(range),handles.bpm_read_before(range,1)-handles.bpm_ref(range,1),'m+');
            hold('on');
            plot(handles.bpm_S(range),handles.bpm_read_expected(range,1)-handles.bpm_ref(range,1),'r+');
            errorbar(handles.bpm_S(range),handles.bpm_read(range,1)-handles.bpm_ref(range,1),handles.bpm_resol(range,1),'b+');
            plot([QD0_S,IP_S,BDUMPB_S]-IEX_S,polyval(handles.IP_read_before(2:-1:1)-handles.IP_ref(2:-1:1),[QD0_S-IP_S,0,BDUMPB_S-IP_S]),'m--');
            plot([QD0_S,IP_S,BDUMPB_S]-IEX_S,polyval(handles.IP_read_expected(2:-1:1)-handles.IP_ref(2:-1:1),[QD0_S-IP_S,0,BDUMPB_S-IP_S]),'r-');
            plot([QD0_S,IP_S,BDUMPB_S]-IEX_S,polyval(handles.IP_read(2:-1:1)-handles.IP_ref(2:-1:1),[QD0_S-IP_S,0,BDUMPB_S-IP_S]),'b-');
            hold('off');
            legend('previous','predicted','present');
        elseif(isfield(handles,'bpm_read_expected'))
            plot(handles.bpm_S(range),handles.bpm_read_expected(range,1)-handles.bpm_ref(range,1),'r+');
            hold('on');
            errorbar(handles.bpm_S(range),handles.bpm_read(range,1)-handles.bpm_ref(range,1),handles.bpm_resol(range,1),'b+');
            plot([QD0_S,IP_S,BDUMPB_S]-IEX_S,polyval(handles.IP_read_expected(2:-1:1)-handles.IP_ref(2:-1:1),[QD0_S-IP_S,0,BDUMPB_S-IP_S]),'r-');
            plot([QD0_S,IP_S,BDUMPB_S]-IEX_S,polyval(handles.IP_read(2:-1:1)-handles.IP_ref(2:-1:1),[QD0_S-IP_S,0,BDUMPB_S-IP_S]),'b-');
            hold('off');
            legend('after cor','present');
        else
            errorbar(handles.bpm_S(range),handles.bpm_read(range,1)-handles.bpm_ref(range,1),handles.bpm_resol(range,1),'b+');
            hold('on')
            plot([QD0_S,IP_S,BDUMPB_S]-IEX_S,polyval(handles.IP_read(2:-1:1)-handles.IP_ref(2:-1:1),[QD0_S-IP_S,0,BDUMPB_S-IP_S]),'b-');
            hold('off')
            legend('present');
        end
    else
        if(isfield(handles,'bpm_read_before'))
            plot(handles.bpm_S(range),handles.bpm_ref(range,1),'go');
            hold('on');
            plot(handles.bpm_S(range),handles.bpm_read_before(range,1),'m+');
            plot(handles.bpm_S(range),handles.bpm_read_expected(range,1),'r+');
            errorbar(handles.bpm_S(range),handles.bpm_read(range,1),handles.bpm_resol(range,1),'b+');
            plot([QD0_S,IP_S,BDUMPB_S]-IEX_S,polyval(handles.IP_ref(2:-1:1),[QD0_S-IP_S,0,BDUMPB_S-IP_S]),'g-');
            plot([QD0_S,IP_S,BDUMPB_S]-IEX_S,polyval(handles.IP_read_before(2:-1:1),[QD0_S-IP_S,0,BDUMPB_S-IP_S]),'m--');
            plot([QD0_S,IP_S,BDUMPB_S]-IEX_S,polyval(handles.IP_read_expected(2:-1:1),[QD0_S-IP_S,0,BDUMPB_S-IP_S]),'r-');
            plot([QD0_S,IP_S,BDUMPB_S]-IEX_S,polyval(handles.IP_read(2:-1:1),[QD0_S-IP_S,0,BDUMPB_S-IP_S]),'b-');
            legend('reference','previous','predicted','present');
            hold('off');
        elseif(isfield(handles,'bpm_read_expected'))
            plot(handles.bpm_S(range),handles.bpm_ref(range,1),'go');
            hold('on');
            plot(handles.bpm_S(range),handles.bpm_read_expected(range,1),'r+');
            errorbar(handles.bpm_S(range),handles.bpm_read(range,1),handles.bpm_resol(range,1),'b+');
            plot([QD0_S,IP_S,BDUMPB_S]-IEX_S,polyval(handles.IP_ref(2:-1:1),[QD0_S-IP_S,0,BDUMPB_S-IP_S]),'g-');
            plot([QD0_S,IP_S,BDUMPB_S]-IEX_S,polyval(handles.IP_read_expected(2:-1:1),[QD0_S-IP_S,0,BDUMPB_S-IP_S]),'r-');
            plot([QD0_S,IP_S,BDUMPB_S]-IEX_S,polyval(handles.IP_read(2:-1:1),[QD0_S-IP_S,0,BDUMPB_S-IP_S]),'b-');
            hold('off');
            legend('reference','after cor','present');
        else
            plot(handles.bpm_S(range),handles.bpm_ref(range,1),'go');
            hold('on');
            errorbar(handles.bpm_S(range),handles.bpm_read(range,1),handles.bpm_resol(range,1),'b+');
            plot([QD0_S,IP_S,BDUMPB_S]-IEX_S,polyval(handles.IP_ref(2:-1:1),[QD0_S-IP_S,0,BDUMPB_S-IP_S]),'g-');
            plot([QD0_S,IP_S,BDUMPB_S]-IEX_S,polyval(handles.IP_read(2:-1:1),[QD0_S-IP_S,0,BDUMPB_S-IP_S]),'b-');
            hold('off');
            legend('reference','present');
        end
    end
    ylabel('X BPM reading [m]');
    axis([handles.sIPrange_default handles.xIP_range_default]);
    hor_line(0,'k:');
    set(gca,'XTick',handles.bpm_S(range));
    set(gca,'XTickLabel',handles.bpm_name(range,:));
    xticklabel_rotate90();
    if(handles.xlegend)
        legend(gca,'show')
    else
        legend(gca,'hide')
    end
    
    axes(handles.ybpm);
    if(handles.sub_ref_orbit)
        if(isfield(handles,'bpm_read_before'))
            plot(handles.bpm_S(range),handles.bpm_read_before(range,2)-handles.bpm_ref(range,2),'m+');
            hold('on');
            plot(handles.bpm_S(range),handles.bpm_read_expected(range,2)-handles.bpm_ref(range,2),'r+');
            errorbar(handles.bpm_S(range),handles.bpm_read(range,2)-handles.bpm_ref(range,2),handles.bpm_resol(range,2),'b+');
            plot([QD0_S,IP_S,BDUMPB_S]-IEX_S,polyval(handles.IP_read_before(4:-1:3)-handles.IP_ref(4:-1:3),[QD0_S-IP_S,0,BDUMPB_S-IP_S]),'m--');
            plot([QD0_S,IP_S,BDUMPB_S]-IEX_S,polyval(handles.IP_read_expected(4:-1:3)-handles.IP_ref(4:-1:3),[QD0_S-IP_S,0,BDUMPB_S-IP_S]),'r-');
            plot([QD0_S,IP_S,BDUMPB_S]-IEX_S,polyval(handles.IP_read(4:-1:3)-handles.IP_ref(4:-1:3),[QD0_S-IP_S,0,BDUMPB_S-IP_S]),'b-');
            hold('off');
            legend('previous','predicted','present');
        elseif(isfield(handles,'bpm_read_expected'))
            plot(handles.bpm_S(range),handles.bpm_read_expected(range,2)-handles.bpm_ref(range,2),'r+');
            hold('on');
            errorbar(handles.bpm_S(range),handles.bpm_read(range,2)-handles.bpm_ref(range,2),handles.bpm_resol(range,2),'b+');
            plot([QD0_S,IP_S,BDUMPB_S]-IEX_S,polyval(handles.IP_read_expected(4:-1:3)-handles.IP_ref(4:-1:3),[QD0_S-IP_S,0,BDUMPB_S-IP_S]),'r-');
            plot([QD0_S,IP_S,BDUMPB_S]-IEX_S,polyval(handles.IP_read(4:-1:3)-handles.IP_ref(4:-1:3),[QD0_S-IP_S,0,BDUMPB_S-IP_S]),'b-');
            hold('off');
            legend('after cor','present');
        else
            errorbar(handles.bpm_S(range),handles.bpm_read(range,2)-handles.bpm_ref(range,2),handles.bpm_resol(range,2),'b+');
            hold('on');
            plot([QD0_S,IP_S,BDUMPB_S]-IEX_S,polyval(handles.IP_read(4:-1:3)-handles.IP_ref(4:-1:3),[QD0_S-IP_S,0,BDUMPB_S-IP_S]),'b-');
            hold('off');
            legend('present');
        end
    else
        if(isfield(handles,'bpm_read_before'))
            plot(handles.bpm_S(range),handles.bpm_ref(range,2),'go');
            hold('on');
            plot(handles.bpm_S(range),handles.bpm_read_before(range,2),'m+');
            plot(handles.bpm_S(range),handles.bpm_read_expected(range,2),'r+');
            errorbar(handles.bpm_S(range),handles.bpm_read(range,2),handles.bpm_resol(range,2),'b+');
            plot([QD0_S,IP_S,BDUMPB_S]-IEX_S,polyval(handles.IP_ref(4:-1:3),[QD0_S-IP_S,0,BDUMPB_S-IP_S]),'g-');
            plot([QD0_S,IP_S,BDUMPB_S]-IEX_S,polyval(handles.IP_read_before(4:-1:3),[QD0_S-IP_S,0,BDUMPB_S-IP_S]),'m--');
            plot([QD0_S,IP_S,BDUMPB_S]-IEX_S,polyval(handles.IP_read_expected(4:-1:3),[QD0_S-IP_S,0,BDUMPB_S-IP_S]),'r-');
            plot([QD0_S,IP_S,BDUMPB_S]-IEX_S,polyval(handles.IP_read(4:-1:3),[QD0_S-IP_S,0,BDUMPB_S-IP_S]),'b-');
            hold('off');
            legend('reference','previous','predicted','present');
        elseif(isfield(handles,'bpm_read_expected'))
            plot(handles.bpm_S(range),handles.bpm_ref(range,2),'go');
            hold('on');
            plot(handles.bpm_S(range),handles.bpm_read_expected(range,2),'r+');
            errorbar(handles.bpm_S(range),handles.bpm_read(range,2),handles.bpm_resol(range,2),'b+');
            plot([QD0_S,IP_S,BDUMPB_S]-IEX_S,polyval(handles.IP_ref(4:-1:3),[QD0_S-IP_S,0,BDUMPB_S-IP_S]),'g-');
            plot([QD0_S,IP_S,BDUMPB_S]-IEX_S,polyval(handles.IP_read_expected(4:-1:3),[QD0_S-IP_S,0,BDUMPB_S-IP_S]),'r-');
            plot([QD0_S,IP_S,BDUMPB_S]-IEX_S,polyval(handles.IP_read(4:-1:3),[QD0_S-IP_S,0,BDUMPB_S-IP_S]),'b-');
            hold('off');
            legend('reference','predicted','present');
        else
            plot(handles.bpm_S(range),handles.bpm_ref(range,2),'go');
            hold('on');
            errorbar(handles.bpm_S(range),handles.bpm_read(range,2),handles.bpm_resol(range,2),'b+');
            plot([QD0_S,IP_S,BDUMPB_S]-IEX_S,polyval(handles.IP_ref(4:-1:3),[QD0_S-IP_S,0,BDUMPB_S-IP_S]),'g-');
            plot([QD0_S,IP_S,BDUMPB_S]-IEX_S,polyval(handles.IP_read(4:-1:3),[QD0_S-IP_S,0,BDUMPB_S-IP_S]),'b-');
            hold('off');
            legend('reference','present');
        end
    end
    xlabel('S [m]');
    ylabel('Y BPM reading [m]');
    axis([handles.sIPrange_default handles.yIP_range_default]);
    hor_line(0,'k:');
    if(handles.ylegend)
        legend(gca,'show')
    else
        legend(gca,'hide')
    end
end


function handles=plot_cor(handles)
    global BEAMLINE;
    
    [nxcor,xcor_ps,xcor_ps_read,xcor_B]=get_corrector(handles.xcor);
    [nycor,ycor_ps,ycor_ps_read,ycor_B]=get_corrector(handles.ycor);
         
    axes(handles.xbpm);
    if(isfield(handles,'xcor_ps_newvalue'))
        plot(handles.xcor_S(handles.xcor_range),handles.xcor_ps_newvalue,'r-+');
        hold('on');
        plot(handles.xcor_S,xcor_ps_read,'b-+');
        hold('off');
        legend('after cor','present');
    else
        plot(handles.xcor_S,xcor_ps_read,'b-+');
        legend('present');
    end
    ylabel('Xcor angle [rad]');
    axis([handles.srange_default handles.xcor_range_default]);
    hor_line(0,'k:');
    set(gca,'XTick',handles.xcor_S);
    set(gca,'XTickLabel',handles.xcor_name);
    xticklabel_rotate90();
    if(handles.xlegend)
        legend(gca,'show')
    else
        legend(gca,'hide')
    end
   
    axes(handles.ybpm);
    if(isfield(handles,'ycor_ps_newvalue'))
        plot(handles.ycor_S(handles.ycor_range),handles.ycor_ps_newvalue,'r-+');
        hold('on');
        plot(handles.ycor_S,ycor_ps_read,'b-+');
        hold('off');
        legend('after cor','present');
    else
        plot(handles.ycor_S,ycor_ps_read,'b-+');
        legend('present');
    end
    ylabel('Ycor angle [rad]');
    axis([handles.srange_default handles.ycor_range_default]);
    hor_line(0,'k:');
    set(gca,'XTick',handles.ycor_S);
    set(gca,'XTickLabel',handles.ycor_name);
    xticklabel_rotate90();
    if(handles.ylegend)
        legend(gca,'show')
    else
        legend(gca,'hide')
    end
end


function handles=plot_mover(handles)
    global BEAMLINE;
    
    IEX=getcolumn(findcells(BEAMLINE, 'Name',upper('iex')),1);
    mover=28:49;
    mover_name='';
    nmover=length(mover);
    for i=1:nmover
        elems=findcells(BEAMLINE,'Girder',mover(i));
        if(strcmp(BEAMLINE{elems(1)}.Class,'MONI'))
            elems(1)=[];
        end
        mult=findstr(BEAMLINE{elems(1)}.Name,'MULT');
        if(isempty(mult))
            mover_name=strvcat(mover_name,BEAMLINE{elems(1)}.Name);
        else
            mover_name=strvcat(mover_name,BEAMLINE{elems(1)}.Name(1:mult-1));
        end
            mover_S(i)=BEAMLINE{elems(1)}.S-BEAMLINE{IEX}.S;
    end
    mover_pos=get_mover(mover);
    
    axes(handles.xbpm);
    if(isfield(handles,'xmover_pos'))
        plot(handles.mover_S(handles.mover_range),handles.xmover_pos,'r-+');
        hold('on');
        plot(mover_S,mover_pos(:,1),'b-+');
        hold('off');
        legend('after cor','present');
    else
        plot(mover_S,mover_pos(:,1),'b-+');
        legend('present');
    end
    ylabel('X pos of Mover [m]');
    axis([handles.srange_default handles.xmov_range_default]);
    hor_line(0,'k:');
    set(gca,'XTick',mover_S);
    set(gca,'XTickLabel',mover_name);
    xticklabel_rotate90();
    if(handles.xlegend)
        legend(gca,'show')
    else
        legend(gca,'hide')
    end
    
    axes(handles.ybpm);
    if(isfield(handles,'ymover_pos'))
        plot(handles.mover_S(handles.mover_range),handles.ymover_pos,'r-+');
        hold('on');
        plot(mover_S,mover_pos(:,2),'b-+');
        hold('off');
        legend('after cor','present');
    else
        plot(mover_S,mover_pos(:,2),'b-+');
        legend('present');
    end
    ylabel('Y pos of Mover [m]');
    axis([handles.srange_default handles.ymov_range_default]);
    hor_line(0,'k:');
    set(gca,'XTick',mover_S);
    xticklabel_rotate90();
    if(handles.ylegend)
        legend(gca,'show')
    else
        legend(gca,'hide')
    end
end


function disable_all(handles)
%     set(handles.nave,'Enable','off');
%     set(handles.measure_orbit,'Enable','off');
%     set(handles.xgain,'Enable','off');
%     set(handles.ygain,'Enable','off');
%     set(handles.compute_ext,'Enable','off');
%     set(handles.autoapply_ext,'Enable','off');
%     set(handles.apply_correction_EXT,'Enable','off');
%     set(handles.compute_ff,'Enable','off');
%     set(handles.autoapply_ff,'Enable','off');
%     set(handles.apply_ff,'Enable','off');
%     set(handles.cancel_correction,'Enable','off');
%     set(handles.EXIT,'Enable','off');
%     set(handles.select_mover,'Enable','off');
%     set(handles.select_ycorrector,'Enable','off');
%     set(handles.select_xcorrector,'Enable','off');
%     set(handles.select_bpm,'Enable','off');
%     set(handles.plot_mover,'Enable','off');
%     set(handles.plot_cor,'Enable','off');
%     set(handles.plot_bpm,'Enable','off');
%     set(handles.ybpm_min,'Enable','off');
%     set(handles.ybpm_max,'Enable','off');
%     set(handles.xbpm_range,'Enable','off');
%     set(handles.sbpm_range,'Enable','off');
%     set(handles.subst_ref_orbit,'Enable','off');
%     set(handles.load_ref_orbit,'Enable','off');
%     set(handles.save_ref_orbit,'Enable','off');
%     set(handles.Ref_table,'Enable','off');
end


function enable_all(handles)
%     set(handles.nave,'Enable','on');
%     set(handles.measure_orbit,'Enable','on');
%     set(handles.xgain,'Enable','on');
%     set(handles.ygain,'Enable','on');
%     set(handles.compute_ext,'Enable','on');
%     set(handles.autoapply_ext,'Enable','on');
%     set(handles.apply_correction_EXT,'Enable','on');
%     set(handles.compute_ff,'Enable','on');
%     set(handles.autoapply_ff,'Enable','on');
%     set(handles.apply_ff,'Enable','on');
%     set(handles.cancel_correction,'Enable','on');
%     set(handles.EXIT,'Enable','on');
%     set(handles.select_mover,'Enable','on');
%     set(handles.select_ycorrector,'Enable','on');
%     set(handles.select_xcorrector,'Enable','on');
%     set(handles.select_bpm,'Enable','on');
%     set(handles.plot_mover,'Enable','on');
%     set(handles.plot_cor,'Enable','on');
%     set(handles.plot_bpm,'Enable','on');
%     set(handles.ybpm_min,'Enable','on');
%     set(handles.ybpm_max,'Enable','on');
%     set(handles.xbpm_range,'Enable','on');
%     set(handles.sbpm_range,'Enable','on');
%     set(handles.subst_ref_orbit,'Enable','on');
%     set(handles.load_ref_orbit,'Enable','on');
%     set(handles.save_ref_orbit,'Enable','on');
%     set(handles.Ref_table,'Enable','on');
end


function handles=output(handles,string)
    nline=size(string,1);
    handles.out=strvcat(string(nline:-1:1,:),handles.out);
    set(handles.output_txt,'String',handles.out);
    refresh;
end


% --- Executes on button press in plot_bpm.
function plot_bpm_Callback(hObject, eventdata, handles)
% hObject    handle to plot_bpm (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of plot_bpm
    h=get(handles.plot_choice,'SelectedObject');
    handles.choice=get(h,'Tag');
    handles=plot_choice(handles);
    enable_all(handles);
    guidata(hObject,handles);
end


% --- Executes on button press in plot_cor.
function plot_cor_Callback(hObject, eventdata, handles)
% hObject    handle to plot_cor (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of plot_cor
    h=get(handles.plot_choice,'SelectedObject');
    handles.choice=get(h,'Tag');
    handles=plot_choice(handles);
    enable_all(handles);
    guidata(hObject,handles);
end

% --- Executes on button press in plot_mover.
function plot_mover_Callback(hObject, eventdata, handles)
% hObject    handle to plot_mover (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of plot_mover
    h=get(handles.plot_choice,'SelectedObject');
    handles.choice=get(h,'Tag');
    handles=plot_choice(handles);
    enable_all(handles);
    guidata(hObject,handles);
end


% --- Executes on button press in help_btn.
function help_btn_Callback(hObject, eventdata, handles)
% hObject    handle to help_btn (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
open('testApps/trajectory_correction/doc/ATF-Trajectorycorrection.pdf');
end

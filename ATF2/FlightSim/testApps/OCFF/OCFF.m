function varargout = OCFF(varargin)
% OCFF M-file for OCFF.fig
%      OCFF, by itself, creates a new OCFF or raises the existing
%      singleton*.
%
%      H = OCFF returns the handle to a new OCFF or the handle to
%      the existing singleton*.
%
%      OCFF('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in OCFF.M with the given input arguments.
%
%      OCFF('Property','Value',...) creates a new OCFF or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before OCFF_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to OCFF_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help OCFF

% Last Modified by GUIDE v2.5 15-Apr-2009 17:54:44

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @OCFF_OpeningFcn, ...
                   'gui_OutputFcn',  @OCFF_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before OCFF is made visible.
function OCFF_OpeningFcn(hObject, eventdata, handles, varargin)
global FL BEAMLINE GIRDER
FlHwUpdate();
iexlist=findcells(BEAMLINE,'Name','IEX');
fflist=findcells(BEAMLINE,'Name','QM16FF');
  list = findcells(BEAMLINE,'Class','QUAD');
girdpos=list(find(list>=fflist(1),1):length(list));
girdlist=zeros(1, length(girdpos));
for R=1:length(girdpos)
girdlist(R)=BEAMLINE{girdpos(R)}.Girder;
end
girderlist=girdlist(1:2:length(girdlist));
bpmnofull=length(findcells({BEAMLINE{iexlist(end):end}},'Class','MONI'));
bpmnoext=length(findcells({BEAMLINE{iexlist(end):fflist(1)}},'Class','MONI'));
FL.OCFF.corrreset=zeros(length(girderlist),3);
for r=1:length(girderlist)
FL.OCFF.corrreset(r,:)=GIRDER{girderlist(r)}.MoverPos;
end
FL.OCFF.savefilename='testApps/OCFF/FFresults.txt';
FL.OCFF.overwrite=1;
FL.OCFF.bpmweights=ones(bpmnofull-bpmnoext,2);
FL.OCFF.bpmtarget=zeros(bpmnofull-bpmnoext,2);
FL.OCFF.moverweights=ones(length(girderlist),2);
FL.OCFF.maxiter=10;
FL.OCFF.tabledatafile='testApps/OCFF/defaults.mat';
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to OCFF (see VARARGIN)

% Choose default command line output for OCFF
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes OCFF wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = OCFF_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on button press in pushbutton1.
function pushbutton1_Callback(hObject, eventdata, handles)
bpmtargetFF
% hObject    handle to pushbutton1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on button press in pushbutton2.
function pushbutton2_Callback(hObject, eventdata, handles)
bpmweightsFF
% hObject    handle to pushbutton2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on button press in pushbutton3.
function pushbutton3_Callback(hObject, eventdata, handles)
correctorweightsFF
% hObject    handle to pushbutton3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on button press in pushbutton4.
function pushbutton4_Callback(hObject, eventdata, handles)
global FL PS BEAMLINE GIRDER
set(handles.text3, 'String', 'Auto-run')
if FL.OCFF.overwrite==1
    delete(FL.OCFF.savefilename)
end
FlHwUpdate();
openfile=open('thermatsFF.mat');
FL.OCFF.xmat=openfile.xmat;
FL.OCFF.ymat=openfile.ymat;
for R2=1:length(FL.OCFF.bpmweights(:,1))
FL.OCFF.xmat(:,R2)=FL.OCFF.xmat(:,R2)*FL.OCFF.bpmweights(R2,1);
end
for R2=1:length(FL.OCFF.moverweights(:,1))
FL.OCFF.xmat(R2,:)=FL.OCFF.xmat(R2,:)*FL.OCFF.moverweights(R2,1);
end
for R2=1:length(FL.OCFF.bpmweights(:,2))
FL.OCFF.ymat(:,R2)=FL.OCFF.ymat(:,R2)*FL.OCFF.bpmweights(R2,2);
end
for R2=1:length(FL.OCFF.moverweights(:,1))
FL.OCFF.ymat(R2,:)=FL.OCFF.ymat(R2,:)*FL.OCFF.moverweights(R2,2);
end
FL.OCFF.iexlist=findcells(BEAMLINE,'Name','IEX');
FL.OCFF.fflist=findcells(BEAMLINE,'Name','QM16FF');
  list = findcells(BEAMLINE,'Class','QUAD');
girdpos=list(find(list>=FL.OCFF.fflist(1),1):length(list));
girdlist=zeros(1, length(girdpos));
for R=1:length(girdpos)
girdlist(R)=BEAMLINE{girdpos(R)}.Girder;
end
FL.OCFF.girderlist=girdlist(1:2:length(girdlist));
FL.OCFF.bpmnofull=length(findcells({BEAMLINE{FL.OCFF.iexlist(end):end}},'Class','MONI'));
FL.OCFF.bpmnoext=length(findcells({BEAMLINE{FL.OCFF.iexlist(end):FL.OCFF.fflist(1)}},'Class','MONI'));
FL.OCFF.iternum=0;
request{1}=FL.OCFF.girderlist;
request{2}=[];
request{3}=[];
[stat FL.OCFF.resp] = AccessRequest(request);
FL.OCFF.stop=0;
while FL.OCFF.stop==0
pause on
pause(1);
pause off
FlHwUpdate();
FlHwUpdate('wait',10);
stat=FlHwUpdate('wait',10);
[stat,output]=FlHwUpdate('bpmave',10);
[stat instdata] = FlTrackThru(FL.OCFF.fflist(1)-1,length(BEAMLINE),output);
FL.OCFF.bpmxavg=zeros(FL.OCFF.bpmnofull-FL.OCFF.bpmnoext,1);
FL.OCFF.bpmyavg=zeros(FL.OCFF.bpmnofull-FL.OCFF.bpmnoext,1);
for R2=1:FL.OCFF.bpmnofull-FL.OCFF.bpmnoext
if isnan(instdata{1}(R2).x) == 1
FL.OCFF.bpmxavg(R2,1)=FL.OCFF.bpmtarget(R2,1);
else
FL.OCFF.bpmxavg(R2,1)=instdata{1}(R2).x;
end
if isnan(instdata{1}(R2).y) == 1
FL.OCFF.bpmyavg(R2,1)=FL.OCFF.bpmtarget(R2,2);
else
FL.OCFF.bpmyavg(R2,1)=instdata{1}(R2).y;
end
end
FL.OCFF.iternum=FL.OCFF.iternum+1;
list=findcells({BEAMLINE{FL.OCFF.fflist(end):end}},'Class','MONI')+FL.OCFF.fflist(end)-1;
bpmslist=[];
for r=1:length(list)
bpmslist=[bpmslist,BEAMLINE{list(r)}.S];
end
plot(bpmslist,FL.OCFF.bpmxavg*10^6,'Color','blue')
hold on
plot(bpmslist,FL.OCFF.bpmyavg*10^6,'Color','red')
hold off
xlabel('S (m)')
ylabel('BPM Reading (microns)')
title('Current Final Focus Orbit')
h = legend('Horizontal','Vertical',1);
set(h,'Interpreter','none')
dlmwrite(FL.OCFF.savefilename,'====================== new track ==================','delimiter', ' ','roffset', 2,'-append');
dlmwrite(FL.OCFF.savefilename,'iternum','delimiter', ' ','roffset', 2,'-append');
dlmwrite(FL.OCFF.savefilename,FL.OCFF.iternum,'delimiter', ' ','roffset', 2,'-append');
dlmwrite(FL.OCFF.savefilename,'xrms','delimiter', ' ','roffset', 2,'-append');
dlmwrite(FL.OCFF.savefilename,sqrt(mean(FL.OCFF.bpmxavg.^2)),'delimiter', ' ','roffset', 2,'-append');
dlmwrite(FL.OCFF.savefilename,'yrms','delimiter', ' ','roffset', 2,'-append');
dlmwrite(FL.OCFF.savefilename,sqrt(mean(FL.OCFF.bpmyavg.^2)),'delimiter', ' ','roffset', 2,'-append')
dlmwrite(FL.OCFF.savefilename,'hor','delimiter', ' ','roffset', 2,'-append');
dlmwrite(FL.OCFF.savefilename,FL.OCFF.bpmxavg,'delimiter', ' ','roffset', 2,'-append');
dlmwrite(FL.OCFF.savefilename,'vert','delimiter', ' ','roffset', 2,'-append');
dlmwrite(FL.OCFF.savefilename,FL.OCFF.bpmyavg,'delimiter', ' ','roffset', 2,'-append');
dlmwrite(FL.OCFF.savefilename,'---------------------------','delimiter', ' ','roffset', 2,'-append')
FL.OCFF.horcorr=transpose(pinv(FL.OCFF.xmat))*(FL.OCFF.bpmxavg-FL.OCFF.bpmtarget(:,1))*10^-5;
FL.OCFF.vertcorr=transpose(pinv(FL.OCFF.ymat))*(FL.OCFF.bpmyavg-FL.OCFF.bpmtarget(:,2))*10^-5;
FL.OCFF.midhorcorr=transpose(pinv(FL.OCFF.xmat))*((FL.OCFF.bpmxavg-FL.OCFF.bpmtarget(:,1))/2)*10^-5;
FL.OCFF.midvertcorr=transpose(pinv(FL.OCFF.ymat))*((FL.OCFF.bpmyavg-FL.OCFF.bpmtarget(:,2))/2)*10^-5;
dlmwrite(FL.OCFF.savefilename,'horcorr','delimiter', ' ','roffset', 2,'-append');
dlmwrite(FL.OCFF.savefilename,FL.OCFF.horcorr,'delimiter', ' ','roffset', 2,'-append');
dlmwrite(FL.OCFF.savefilename,'vertcorr','delimiter', ' ','roffset', 2,'-append');
dlmwrite(FL.OCFF.savefilename,FL.OCFF.vertcorr,'delimiter', ' ','roffset', 2,'-append');
dlmwrite(FL.OCFF.savefilename,'mid-point horcorr','delimiter', ' ','roffset', 2,'-append');
dlmwrite(FL.OCFF.savefilename,FL.OCFF.midhorcorr,'delimiter', ' ','roffset', 2,'-append');
dlmwrite(FL.OCFF.savefilename,'mid-point vertcorr','delimiter', ' ','roffset', 2,'-append');
dlmwrite(FL.OCFF.savefilename,FL.OCFF.midvertcorr,'delimiter', ' ','roffset', 2,'-append');
    for R1=1:22
GIRDER{FL.OCFF.girderlist(R1)}.MoverSetPt(1)=GIRDER{FL.OCFF.girderlist(R1)}.MoverPos(1)-FL.OCFF.horcorr(R1);
GIRDER{FL.OCFF.girderlist(R1)}.MoverSetPt(2)=GIRDER{FL.OCFF.girderlist(R1)}.MoverPos(2)-FL.OCFF.vertcorr(R1);
GIRDER{FL.OCFF.girderlist(R1)}.MoverSetPt(3)=GIRDER{FL.OCFF.girderlist(R1)}.MoverPos(3);
    end
    stat = MoverTrim(FL.OCFF.girderlist,1);
        pause on
        pause(1);
        pause off
        FlHwUpdate();
FL.OCFF.bpmxavgprev=FL.OCFF.bpmxavg;
FL.OCFF.bpmyavgprev=FL.OCFF.bpmyavg;
FlHwUpdate('wait',10);
stat=FlHwUpdate('wait',10);
[stat,output]=FlHwUpdate('bpmave',10);
[stat instdata] = FlTrackThru(FL.OCFF.fflist(1)-1,length(BEAMLINE),output);
FL.OCFF.bpmxavg=zeros(FL.OCFF.bpmnofull-FL.OCFF.bpmnoext,1);
FL.OCFF.bpmyavg=zeros(FL.OCFF.bpmnofull-FL.OCFF.bpmnoext,1);
for R2=1:FL.OCFF.bpmnofull-FL.OCFF.bpmnoext
if isnan(instdata{1}(R2).x) == 1
FL.OCFF.bpmxavg(R2,1)=FL.OCFF.bpmtarget(R2,1);
else
FL.OCFF.bpmxavg(R2,1)=instdata{1}(R2).x;
end
if isnan(instdata{1}(R2).y) == 1
FL.OCFF.bpmyavg(R2,1)=FL.OCFF.bpmtarget(R2,2);
else
FL.OCFF.bpmyavg(R2,1)=instdata{1}(R2).y;
end
end
FL.OCFF.iternum=FL.OCFF.iternum+1;
list=findcells({BEAMLINE{FL.OCFF.fflist(end):end}},'Class','MONI')+FL.OCFF.fflist(end)-1;
bpmslist=[];
for r=1:length(list)
bpmslist=[bpmslist,BEAMLINE{list(r)}.S];
end
plot(bpmslist,FL.OCFF.bpmxavg*10^6,'Color','blue')
hold on
plot(bpmslist,FL.OCFF.bpmyavg*10^6,'Color','red')
hold off
xlabel('S (m)')
ylabel('BPM Reading (microns)')
title('Current Final Focus Orbit')
h = legend('Horizontal','Vertical',1);
set(h,'Interpreter','none')
dlmwrite(FL.OCFF.savefilename,'iternum','delimiter', ' ','roffset', 2,'-append');
dlmwrite(FL.OCFF.savefilename,FL.OCFF.iternum,'delimiter', ' ','roffset', 2,'-append');
dlmwrite(FL.OCFF.savefilename,'xrms','delimiter', ' ','roffset', 2,'-append');
dlmwrite(FL.OCFF.savefilename,sqrt(mean(FL.OCFF.bpmxavg.^2)),'delimiter', ' ','roffset', 2,'-append');
dlmwrite(FL.OCFF.savefilename,'yrms','delimiter', ' ','roffset', 2,'-append');
dlmwrite(FL.OCFF.savefilename,sqrt(mean(FL.OCFF.bpmyavg.^2)),'delimiter', ' ','roffset', 2,'-append')
dlmwrite(FL.OCFF.savefilename,'hor','delimiter', ' ','roffset', 2,'-append');
dlmwrite(FL.OCFF.savefilename,FL.OCFF.bpmxavg,'delimiter', ' ','roffset', 2,'-append');
dlmwrite(FL.OCFF.savefilename,'vert','delimiter', ' ','roffset', 2,'-append');
dlmwrite(FL.OCFF.savefilename,FL.OCFF.bpmyavg,'delimiter', ' ','roffset', 2,'-append');
dlmwrite(FL.OCFF.savefilename,'---------------------------','delimiter', ' ','roffset', 2,'-append')
if sqrt(mean(FL.OCFF.bpmxavg.^2))/sqrt(mean(FL.OCFF.bpmxavgprev.^2))>=1
    if sqrt(mean(FL.OCFF.bpmyavg.^2))/sqrt(mean(FL.OCFF.bpmyavgprev.^2))>=1
        FL.OCFF.stop=1;
    end
end
if FL.OCFF.iternum>=FL.OCFF.maxiter
    FL.OCFF.stop=1;
end
end
    for R1=1:22
GIRDER{FL.OCFF.girderlist(R1)}.MoverSetPt(1)=GIRDER{FL.OCFF.girderlist(R1)}.MoverPos(1)+FL.OCFF.horcorr(R1);
GIRDER{FL.OCFF.girderlist(R1)}.MoverSetPt(2)=GIRDER{FL.OCFF.girderlist(R1)}.MoverPos(2)+FL.OCFF.vertcorr(R1);
GIRDER{FL.OCFF.girderlist(R1)}.MoverSetPt(3)=GIRDER{FL.OCFF.girderlist(R1)}.MoverPos(3);
    end
    stat = MoverTrim(FL.OCFF.girderlist,1);
        pause on
        pause(1);
        pause off
        FlHwUpdate();
FL.OCFF.bpmxavgprev=FL.OCFF.bpmxavg;
FL.OCFF.bpmyavgprev=FL.OCFF.bpmyavg;
FlHwUpdate('wait',10);
stat=FlHwUpdate('wait',10);
[stat,output]=FlHwUpdate('bpmave',10);
[stat instdata] = FlTrackThru(FL.OCFF.fflist(1)-1,length(BEAMLINE),output);
FL.OCFF.bpmxavg=zeros(FL.OCFF.bpmnofull-FL.OCFF.bpmnoext,1);
FL.OCFF.bpmyavg=zeros(FL.OCFF.bpmnofull-FL.OCFF.bpmnoext,1);
for R2=1:FL.OCFF.bpmnofull-FL.OCFF.bpmnoext
if isnan(instdata{1}(R2).x) == 1
FL.OCFF.bpmxavg(R2,1)=FL.OCFF.bpmtarget(R2,1);
else
FL.OCFF.bpmxavg(R2,1)=instdata{1}(R2).x;
end
if isnan(instdata{1}(R2).y) == 1
FL.OCFF.bpmyavg(R2,1)=FL.OCFF.bpmtarget(R2,2);
else
FL.OCFF.bpmyavg(R2,1)=instdata{1}(R2).y;
end
end
FL.OCFF.iternum=FL.OCFF.iternum+1;
list=findcells({BEAMLINE{FL.OCFF.fflist(end):end}},'Class','MONI')+FL.OCFF.fflist(end)-1;
bpmslist=[];
for r=1:length(list)
bpmslist=[bpmslist,BEAMLINE{list(r)}.S];
end
plot(bpmslist,FL.OCFF.bpmxavg*10^6,'Color','blue')
hold on
plot(bpmslist,FL.OCFF.bpmyavg*10^6,'Color','red')
hold off
xlabel('S (m)')
ylabel('BPM Reading (microns)')
title('Current Final Focus Orbit')
h = legend('Horizontal','Vertical',1);
set(h,'Interpreter','none')
dlmwrite(FL.OCFF.savefilename,'iternum','delimiter', ' ','roffset', 2,'-append');
dlmwrite(FL.OCFF.savefilename,FL.OCFF.iternum,'delimiter', ' ','roffset', 2,'-append');
dlmwrite(FL.OCFF.savefilename,'xrms','delimiter', ' ','roffset', 2,'-append');
dlmwrite(FL.OCFF.savefilename,sqrt(mean(FL.OCFF.bpmxavg.^2)),'delimiter', ' ','roffset', 2,'-append');
dlmwrite(FL.OCFF.savefilename,'yrms','delimiter', ' ','roffset', 2,'-append');
dlmwrite(FL.OCFF.savefilename,sqrt(mean(FL.OCFF.bpmyavg.^2)),'delimiter', ' ','roffset', 2,'-append')
dlmwrite(FL.OCFF.savefilename,'hor','delimiter', ' ','roffset', 2,'-append');
dlmwrite(FL.OCFF.savefilename,FL.OCFF.bpmxavg,'delimiter', ' ','roffset', 2,'-append');
dlmwrite(FL.OCFF.savefilename,'vert','delimiter', ' ','roffset', 2,'-append');
dlmwrite(FL.OCFF.savefilename,FL.OCFF.bpmyavg,'delimiter', ' ','roffset', 2,'-append');
dlmwrite(FL.OCFF.savefilename,'---------------------------','delimiter', ' ','roffset', 2,'-append')
AccessRequest('release',FL.OCFF.resp);
if FL.OCFF.stop==2
   set(handles.text3, 'String', 'Aborted') 
else
set(handles.text3, 'String', 'Ready')
end
% hObject    handle to pushbutton4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on button press in pushbutton5.
function pushbutton5_Callback(hObject, eventdata, handles)
manualFF
% hObject    handle to pushbutton5 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on button press in pushbutton6.
function pushbutton6_Callback(hObject, eventdata, handles)
global FL BEAMLINE PS GIRDER
set(handles.text3, 'String', 'Busy')
FL.OCFF.iexlist=findcells(BEAMLINE,'Name','IEX');
FL.OCFF.fflist=findcells(BEAMLINE,'Name','QM16FF');
  list = findcells(BEAMLINE,'Class','QUAD');
girdpos=list(find(list>=FL.OCFF.fflist(1),1):length(list));
girdlist=zeros(1, length(girdpos));
for R=1:length(girdpos)
girdlist(R)=BEAMLINE{girdpos(R)}.Girder;
end
FL.OCFF.girderlist=girdlist(1:2:length(girdlist));
FL.OCFF.bpmnofull=length(findcells({BEAMLINE{FL.OCFF.iexlist(end):end}},'Class','MONI'));
FL.OCFF.bpmnoext=length(findcells({BEAMLINE{FL.OCFF.iexlist(end):FL.OCFF.fflist(1)}},'Class','MONI'));
FL.OCFF.iternum=0;
request{1}=FL.OCFF.girderlist;
request{2}=[];
request{3}=[];
[stat FL.OCFF.resp] = AccessRequest(request);
    for R1=1:22
GIRDER{FL.OCFF.girderlist(R1)}.MoverSetPt=FL.OCFF.corrreset(R1,:);
    end
    stat = MoverTrim(FL.OCFF.girderlist,1);
pause on
pause(1);
pause off
FlHwUpdate();
AccessRequest('release',FL.OCFF.resp);
set(handles.text3, 'String', 'Ready')
% hObject    handle to pushbutton6 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on button press in pushbutton7.
function pushbutton7_Callback(hObject, eventdata, handles)
global FL BEAMLINE PS GIRDER
set(handles.text3, 'String', 'Busy')
pause on
pause(1)
pause off
FlHwUpdate();
iexlist=findcells(BEAMLINE,'Name','IEX');
fflist=findcells(BEAMLINE,'Name','QM16FF');
  list = findcells(BEAMLINE,'Class','QUAD');
girdpos=list(find(list>=fflist(1),1):length(list));
girdlist=zeros(1, length(girdpos));
for R=1:length(girdpos)
girdlist(R)=BEAMLINE{girdpos(R)}.Girder;
end
girderlist=girdlist(1:2:length(girdlist));
bpmnofull=length(findcells({BEAMLINE{iexlist(end):end}},'Class','MONI'));
bpmnoext=length(findcells({BEAMLINE{iexlist(end):fflist(1)}},'Class','MONI'));
FL.OCFF.corrreset=zeros(length(girderlist),3);
for r=1:length(girderlist)
FL.OCFF.corrreset(r,:)=GIRDER{girderlist(r)}.MoverPos;
end
set(handles.text3, 'String', 'Ready')
% hObject    handle to pushbutton7 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on button press in pushbutton8.
function pushbutton8_Callback(hObject, eventdata, handles)
close(handles.figure1);
% hObject    handle to pushbutton8 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on button press in pushbutton9.
function pushbutton9_Callback(hObject, eventdata, handles)
outputfileFF
% hObject    handle to pushbutton9 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)



function edit1_Callback(hObject, eventdata, handles)
global FL
user_entry = str2double(get(hObject,'string'));
if isnan(user_entry)
  errordlg('You must enter a numeric value','Bad Input','modal')
  uicontrol(hObject)
	return
else
    FL.OCFF.maxiter=user_entry;
end
% hObject    handle to edit1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit1 as text
%        str2double(get(hObject,'String')) returns contents of edit1 as a double


% --- Executes during object creation, after setting all properties.
function edit1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function varargout = correctorweightsFF(varargin)
% CORRECTORWEIGHTSFF M-file for correctorweightsFF.fig
%      CORRECTORWEIGHTSFF, by itself, creates a new CORRECTORWEIGHTSFF or raises the existing
%      singleton*.
%
%      H = CORRECTORWEIGHTSFF returns the handle to a new CORRECTORWEIGHTSFF or the handle to
%      the existing singleton*.
%
%      CORRECTORWEIGHTSFF('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in CORRECTORWEIGHTSFF.M with the given input arguments.
%
%      CORRECTORWEIGHTSFF('Property','Value',...) creates a new CORRECTORWEIGHTSFF or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before correctorweightsFF_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to correctorweightsFF_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help correctorweightsFF

% Last Modified by GUIDE v2.5 17-Apr-2009 06:05:18

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @correctorweightsFF_OpeningFcn, ...
                   'gui_OutputFcn',  @correctorweightsFF_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before correctorweightsFF is made visible.
function correctorweightsFF_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to correctorweightsFF (see VARARGIN)

% Choose default command line output for correctorweightsFF
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes correctorweightsFF wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = correctorweightsFF_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on button press in pushbutton1.
function pushbutton1_Callback(hObject, eventdata, handles)
close(handles.figure1);
% hObject    handle to pushbutton1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes when entered data in editable cell(s) in uitable1.
function uitable1_CellEditCallback(hObject, eventdata, handles)
global FL
FL.OCFF.moverweights=get(hObject,'data');
% hObject    handle to uitable1 (see GCBO)
% eventdata  structure with the following fields (see UITABLE)
%	Indices: row and column indices of the cell(s) edited
%	PreviousData: previous data for the cell(s) edited
%	EditData: string(s) entered by the user
%	NewData: EditData or its converted form set on the Data property. Empty if Data was not changed
%	Error: error string when failed to convert EditData to appropriate value for Data
% handles    structure with handles and user data (see GUIDATA)


% --- Executes during object creation, after setting all properties.
function uitable1_CreateFcn(hObject, eventdata, handles)
global FL
moverweights=FL.OCFF.moverweights;
set(hObject,'Data',moverweights)
% hObject    handle to uitable1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called


% --- Executes on button press in pushbutton2.
function pushbutton2_Callback(hObject, eventdata, handles)
global FL
  moverweights=FL.OCFF.moverweights;
save(FL.OCFF.tabledatafile,'-append','moverweights')
% hObject    handle to pushbutton2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on button press in pushbutton3.
function pushbutton3_Callback(hObject, eventdata, handles)
global FL
load(FL.OCFF.tabledatafile)
FL.OCFF.moverweights=moverweights;
set(handles.uitable1,'Data',moverweights)
% hObject    handle to pushbutton3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

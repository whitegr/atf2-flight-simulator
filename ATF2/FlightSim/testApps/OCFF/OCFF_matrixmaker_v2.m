global BEAMLINE GIRDER
iexlist=findcells(BEAMLINE,'Name','IEX');
fflist=findcells(BEAMLINE,'Name','QM16FF');
  list = findcells(BEAMLINE,'Class','QUAD');
girdpos=list(find(list>=fflist(1),1):length(list));
girdlist=zeros(1, length(girdpos));
for R=1:length(girdpos)
girdlist(R)=BEAMLINE{girdpos(R)}.Girder;
end
girderlist=girdlist(1:2:length(girdlist));
bpmnofull=length(findcells({BEAMLINE{iexlist(end):end}},'Class','MONI'));
bpmnoext=length(findcells({BEAMLINE{iexlist(end):fflist(1)}},'Class','MONI'));
xmat=cell(22,1);
for RR=1:22
bpmx=[];
bpmy=[];
for R=1:10
  beam=Beam0_IEX;
        beam.Bunch.x(1)=0;
        beam.Bunch.x(2)=0;
        beam.Bunch.x(3)=0;
        beam.Bunch.x(4)=0;
[stat,beamout,instdata]=TrackThru(iexlist(end),length(BEAMLINE),beam,1,1,0);
bpmx1=zeros(bpmnoext+1-bpmnoext,bpmnofull-bpmnoext);
bpmy1=zeros(bpmnoext+1-bpmnoext,bpmnofull-bpmnoext);
for R2=bpmnoext+1:bpmnofull
bpmx1(R2-bpmnoext)=instdata{1}(R2).x;
bpmy1(R2-bpmnoext)=instdata{1}(R2).y;
end
bpmx=[bpmx,bpmx1];
bpmy=[bpmy,bpmy1];
end
bpmxavg=zeros(bpmnoext+1-bpmnoext,bpmnofull-bpmnoext);
bpmyavg=zeros(bpmnoext+1-bpmnoext,bpmnofull-bpmnoext);
for R1=bpmnoext+1-bpmnoext:bpmnofull-bpmnoext
    bpmxavg(R1)=mean(bpmx(R1:bpmnofull-bpmnoext:length(bpmx)));
    bpmyavg(R1)=mean(bpmy(R1:bpmnofull-bpmnoext:length(bpmy)));
end
bpmxavgprev=bpmxavg;
bpmyavgprev=bpmyavg;
horcorr=zeros(1,22);
vertcorr=zeros(1,22);
horcorr(RR)=1e-5;
    for R1=1:22
GIRDER{girderlist(R1)}.MoverSetPt(1)=GIRDER{girderlist(R1)}.MoverSetPt(1)+horcorr(R1);
GIRDER{girderlist(R1)}.MoverSetPt(2)=GIRDER{girderlist(R1)}.MoverSetPt(2)+vertcorr(R1);
    end
    stat = MoverTrim(girderlist);
        bpmx=[];
bpmy=[];
for R=1:10
  beam=Beam0_IEX;
        beam.Bunch.x(1)=0;
        beam.Bunch.x(2)=0;
        beam.Bunch.x(3)=0;
        beam.Bunch.x(4)=0;
[stat,beamout,instdata]=TrackThru(iexlist(end),length(BEAMLINE),beam,1,1,0);
bpmx1=zeros(bpmnoext+1-bpmnoext,bpmnofull-bpmnoext);
bpmy1=zeros(bpmnoext+1-bpmnoext,bpmnofull-bpmnoext);
for R2=bpmnoext+1:bpmnofull
bpmx1(R2-bpmnoext)=instdata{1}(R2).x;
bpmy1(R2-bpmnoext)=instdata{1}(R2).y;
end
bpmx=[bpmx,bpmx1];
bpmy=[bpmy,bpmy1];
end
bpmxavg=zeros(bpmnoext+1-bpmnoext,bpmnofull-bpmnoext);
bpmyavg=zeros(bpmnoext+1-bpmnoext,bpmnofull-bpmnoext);
for R1=bpmnoext+1-bpmnoext:bpmnofull-bpmnoext
    bpmxavg(R1)=mean(bpmx(R1:bpmnofull-bpmnoext:length(bpmx)));
    bpmyavg(R1)=mean(bpmy(R1:bpmnofull-bpmnoext:length(bpmy)));
end
        horcorr(RR)=-1e-5;
    for R1=1:22
GIRDER{girderlist(R1)}.MoverSetPt(1)=GIRDER{girderlist(R1)}.MoverSetPt(1)+horcorr(R1);
GIRDER{girderlist(R1)}.MoverSetPt(2)=GIRDER{girderlist(R1)}.MoverSetPt(2)+vertcorr(R1);
    end
    stat = MoverTrim(girderlist);
        xdiff=bpmxavg-bpmxavgprev;
        ydiff=bpmyavg-bpmyavgprev;
       xmat{RR}=xdiff;
end
xmat0=xmat;
ymat=cell(22,1);
for RR=1:22
bpmx=[];
bpmy=[];
for R=1:10
  beam=Beam0_IEX;
        beam.Bunch.x(1)=0;
        beam.Bunch.x(2)=0;
        beam.Bunch.x(3)=0;
        beam.Bunch.x(4)=0;
[stat,beamout,instdata]=TrackThru(iexlist(end),length(BEAMLINE),beam,1,1,0);
bpmx1=zeros(bpmnoext+1-bpmnoext,bpmnofull-bpmnoext);
bpmy1=zeros(bpmnoext+1-bpmnoext,bpmnofull-bpmnoext);
for R2=bpmnoext+1:bpmnofull
bpmx1(R2-bpmnoext)=instdata{1}(R2).x;
bpmy1(R2-bpmnoext)=instdata{1}(R2).y;
end
bpmx=[bpmx,bpmx1];
bpmy=[bpmy,bpmy1];
end
bpmxavg=zeros(bpmnoext+1-bpmnoext,bpmnofull-bpmnoext);
bpmyavg=zeros(bpmnoext+1-bpmnoext,bpmnofull-bpmnoext);
for R1=bpmnoext+1-bpmnoext:bpmnofull-bpmnoext
    bpmxavg(R1)=mean(bpmx(R1:bpmnofull-bpmnoext:length(bpmx)));
    bpmyavg(R1)=mean(bpmy(R1:bpmnofull-bpmnoext:length(bpmy)));
end
bpmxavgprev=bpmxavg;
bpmyavgprev=bpmyavg;
horcorr=zeros(1,22);
vertcorr=zeros(1,22);
vertcorr(RR)=1e-5;
    for R1=1:22
GIRDER{girderlist(R1)}.MoverSetPt(1)=GIRDER{girderlist(R1)}.MoverSetPt(1)+horcorr(R1);
GIRDER{girderlist(R1)}.MoverSetPt(2)=GIRDER{girderlist(R1)}.MoverSetPt(2)+vertcorr(R1);
    end
    stat = MoverTrim(girderlist);
        bpmx=[];
bpmy=[];
for R=1:10
  beam=Beam0_IEX;
        beam.Bunch.x(1)=0;
        beam.Bunch.x(2)=0;
        beam.Bunch.x(3)=0;
        beam.Bunch.x(4)=0;
[stat,beamout,instdata]=TrackThru(iexlist(end),length(BEAMLINE),beam,1,1,0);
bpmx1=zeros(bpmnoext+1-bpmnoext,bpmnofull-bpmnoext);
bpmy1=zeros(bpmnoext+1-bpmnoext,bpmnofull-bpmnoext);
for R2=bpmnoext+1:bpmnofull
bpmx1(R2-bpmnoext)=instdata{1}(R2).x;
bpmy1(R2-bpmnoext)=instdata{1}(R2).y;
end
bpmx=[bpmx,bpmx1];
bpmy=[bpmy,bpmy1];
end
bpmxavg=zeros(bpmnoext+1-bpmnoext,bpmnofull-bpmnoext);
bpmyavg=zeros(bpmnoext+1-bpmnoext,bpmnofull-bpmnoext);
for R1=bpmnoext+1-bpmnoext:bpmnofull-bpmnoext
    bpmxavg(R1)=mean(bpmx(R1:bpmnofull-bpmnoext:length(bpmx)));
    bpmyavg(R1)=mean(bpmy(R1:bpmnofull-bpmnoext:length(bpmy)));
end
        vertcorr(RR)=-1e-5;
    for R1=1:22
GIRDER{girderlist(R1)}.MoverSetPt(1)=GIRDER{girderlist(R1)}.MoverSetPt(1)+horcorr(R1);
GIRDER{girderlist(R1)}.MoverSetPt(2)=GIRDER{girderlist(R1)}.MoverSetPt(2)+vertcorr(R1);
    end
    stat = MoverTrim(girderlist);
        xdiff=bpmxavg-bpmxavgprev;
        ydiff=bpmyavg-bpmyavgprev;
       ymat{RR}=ydiff;
end
ymat0=ymat;
%for R=1:length(ymat)
 %   ymat{R}(1)=0;
 %   ymat{R}(4)=0;
%end
%ymat{6}=zeros(bpmnoext+1-bpmnoext, bpmnofull-bpmnoext);
%ymat{19}=zeros(bpmnoext+1-bpmnoext, bpmnofull-bpmnoext);
xmat=cell2mat(xmat);
ymat=cell2mat(ymat);
[U,S,V]=svd(transpose(xmat));
S2=svds(S,length(svd(transpose(xmat))));
 for R=1:length(S2)
if S2(R)==0
S2(R)=0;
else
S2(R)=1/S2(R);
end
 end
ST=diag(S2);
STadd=zeros(length(U)-length(V),length(ST));
ST=transpose([transpose(ST),transpose(STadd)]);
horresp=V*transpose(ST)*U';
[U,S,V]=svd(transpose(ymat));
S2=svds(S,length(svd(transpose(ymat))));
 for R=1:length(S2)
if S2(R)==0
S2(R)=0;
else
S2(R)=1/S2(R);
end
 end
ST=diag(S2);
STadd=zeros(length(U)-length(V),length(ST));
ST=transpose([transpose(ST),transpose(STadd)]);
vertresp=V*transpose(ST)*U';
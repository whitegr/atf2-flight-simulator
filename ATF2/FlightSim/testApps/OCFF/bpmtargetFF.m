function varargout = bpmtargetFF(varargin)
% BPMTARGETFF M-file for bpmtargetFF.fig
%      BPMTARGETFF, by itself, creates a new BPMTARGETFF or raises the existing
%      singleton*.
%
%      H = BPMTARGETFF returns the handle to a new BPMTARGETFF or the handle to
%      the existing singleton*.
%
%      BPMTARGETFF('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in BPMTARGETFF.M with the given input arguments.
%
%      BPMTARGETFF('Property','Value',...) creates a new BPMTARGETFF or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before bpmtargetFF_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to bpmtargetFF_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help bpmtargetFF

% Last Modified by GUIDE v2.5 17-Apr-2009 05:57:24

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @bpmtargetFF_OpeningFcn, ...
                   'gui_OutputFcn',  @bpmtargetFF_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before bpmtargetFF is made visible.
function bpmtargetFF_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to bpmtargetFF (see VARARGIN)

% Choose default command line output for bpmtargetFF
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes bpmtargetFF wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = bpmtargetFF_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on button press in pushbutton1.
function pushbutton1_Callback(hObject, eventdata, handles)
close(handles.figure1);
% hObject    handle to pushbutton1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes when entered data in editable cell(s) in uitable1.
function uitable1_CellEditCallback(hObject, eventdata, handles)
global FL
FL.OCFF.bpmtarget=get(hObject,'data')*10^-6;
% hObject    handle to uitable1 (see GCBO)
% eventdata  structure with the following fields (see UITABLE)
%	Indices: row and column indices of the cell(s) edited
%	PreviousData: previous data for the cell(s) edited
%	EditData: string(s) entered by the user
%	NewData: EditData or its converted form set on the Data property. Empty if Data was not changed
%	Error: error string when failed to convert EditData to appropriate value for Data
% handles    structure with handles and user data (see GUIDATA)


% --- Executes during object creation, after setting all properties.
function uitable1_CreateFcn(hObject, eventdata, handles)
global FL
bpmtarget=FL.OCFF.bpmtarget;
set(hObject,'Data',bpmtarget*10^6)
% hObject    handle to uitable1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called


% --- Executes on button press in pushbutton2.
function pushbutton2_Callback(hObject, eventdata, handles)
global FL
bpmtarget=FL.OCFF.bpmtarget;
save(FL.OCFF.tabledatafile,'-append','bpmtarget')
% hObject    handle to pushbutton2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on button press in pushbutton3.
function pushbutton3_Callback(hObject, eventdata, handles)
global FL
load(FL.OCFF.tabledatafile)
FL.OCFF.bpmtarget=bpmtarget;
set(handles.uitable1,'Data',bpmtarget*10^6)
% hObject    handle to pushbutton3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on button press in pushbutton4.
function pushbutton4_Callback(hObject, eventdata, handles)
global FL BEAMLINE
set(handles.text2, 'String', 'Busy')
pause on
pause(1);
pause off
FlHwUpdate();
FL.OCFF.iexlist=findcells(BEAMLINE,'Name','IEX');
FL.OCFF.fflist=findcells(BEAMLINE,'Name','QM16FF');
FL.OCFF.bpmnofull=length(findcells({BEAMLINE{FL.OCFF.iexlist(end):end}},'Class','MONI'));
FL.OCFF.bpmnoext=length(findcells({BEAMLINE{FL.OCFF.iexlist(end):FL.OCFF.fflist(1)}},'Class','MONI'));
FlHwUpdate('wait',10);
stat=FlHwUpdate('wait',10);
[stat,output]=FlHwUpdate('bpmave',10);
[stat instdata] = FlTrackThru(FL.OCFF.fflist(1)-1,length(BEAMLINE),output);
FL.OCFF.bpmxavg=zeros(FL.OCFF.bpmnofull-FL.OCFF.bpmnoext,1);
FL.OCFF.bpmyavg=zeros(FL.OCFF.bpmnofull-FL.OCFF.bpmnoext,1);
for R2=1:FL.OCFF.bpmnofull-FL.OCFF.bpmnoext
if isnan(instdata{1}(R2).x) == 1
FL.OCFF.bpmxavg(R2,1)=FL.OCFF.bpmtarget(R2,1);
else
FL.OCFF.bpmxavg(R2,1)=instdata{1}(R2).x;
end
if isnan(instdata{1}(R2).y) == 1
FL.OCFF.bpmyavg(R2,1)=FL.OCFF.bpmtarget(R2,2);
else
FL.OCFF.bpmyavg(R2,1)=instdata{1}(R2).y;
end
end
FL.OCFF.bpmtarget(:,1)=FL.OCFF.bpmxavg(:,1);
FL.OCFF.bpmtarget(:,2)=FL.OCFF.bpmyavg(:,1);
set(handles.uitable1,'Data',FL.OCFF.bpmtarget*10^6)
set(handles.text2, 'String', 'Ready')
% hObject    handle to pushbutton4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

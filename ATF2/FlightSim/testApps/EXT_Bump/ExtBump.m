function [cor_newvalue, hh ] = ExtBump(Target, plane, uu, cor_ind, ps )
%%%
%%% 
%% Bump to steer the horizontal or vertical orbit in extraction region.
%% Example: [aa, bb, cc] = ExtBump('QM7RX', 'h', [-1e-3 0] )
%%
%% function [cor_newvalue, ps, hh ] = ExtBump(Target, plane, uu )
%% Target:  one of the components : 'MB1X', 'QM7RX', 'BS1XA','ZS1X', 'BS1XB',
%%         'BS2XA','ZS2X','BS2XB', 'MB2X','BS3XA','ZS3X','BS3XB' 'OTR1X'. Default
%%          is 'QM7RX'
%% Plane: 'H' for horizontal bump, 'V' for vertical bump
%% uu:    The position and angle required at target in unit of [ m rad ].
%% cor_newvalue : corrector values to construct the bumper [1:4] for DR
%%                [1:2, 5:6] for Extration line.
%% ps: Index number for PS
%% hh: figure handles for GUI
%%
%% Damping ring Y bump: ZV9R, ZV100R, ZV10R, ZV11R
%% Extraction line Y bump: ZV9R, ZV100Rx, ZV1X, ZV2X
%% Damping ring X bump: ZH9R, ZH100R, ZH10R, ZH11R
%% Extraction line X bump: ZH9R, ZH100Rx, ZH1X, ZH2X
%%
%% Auther: Min-Huey Wang SLAC National Accelerator Laboratory
%% Date: Aug 2009 

global FL BEAMLINE PS

ringext = findcells(BEAMLINE,'Name','IEX');  %% 1258 
extff = findcells(BEAMLINE,'Name','BEGFF');
Target_group = {'MB1X', 'QM7RX', 'BS1XA','ZS1X', 'BS1XB', ...
'BS2XA','ZS2X','BS2XB', 'MB2X','BS3XA','ZS3X','BS3XB' 'OTR1X'};
ss = cellfun(@(x) x.S, BEAMLINE, 'UniformOutput',false);
ss = cat(1,ss{1:end});
ff = strcmp(Target,Target_group);
ind = find(ff == 1);
if (isempty(ind))
    Target = 'QM7RX';
else
    Target = Target_group{ind(1)};
end

%% if nargin < 3, uu = [1,0]; end
%% if nargin < 2, plane = 'V'; end
%% if nargin < 1, Target ='QM7RX'; end

%% cor_newvalue = [];
Targetind = findcells(BEAMLINE,'Name',Target);  %% 25
switch lower(plane)
   case 'v'
      for i = 1:length(cor_ind)
           L = BEAMLINE{cor_ind(i)}.L/2;
           eval(['cormat', num2str(i),' = '...
               '[1 L 0 0 0 0;' ...
               '0 1 0 0 0 0;' ...
               '0 0 1 L 0 0;'...
               '0 0 0 1 0 0;' ...
               '0 0 0 0 1 0;' ...
               '0 0 0 0 0 1];']);
       end
       %% QM7Rhind = findcells(BEAMLINE,'Name','QM7R1');
       %% Targetind = findcells(BEAMLINE,'Name','MREXT');  %% 25
       %% vcor_ind = [ZV9Rind, ZV100Rind, ZV10Rind, ZV11Rind, ZV100Rxind, ZV1xind, ZV2xind];

       [stat Y1toT] = RmatAtoB(cor_ind(1),Targetind(1));
       Y1toT = Y1toT*inv(cormat1);
       [stat Y2toT] = RmatAtoB(cor_ind(5),Targetind(1));
       Y2toT = Y2toT*inv(cormat5);
       %% [stat Y1toT] = RmatAtoB(ZV9Rind,Targetind(1));
       %% [stat Y2toT] = RmatAtoB(ZV100Rxind,Targetind(1));
       %% BB = [Y1toT(3,4) Y2toT(3,4); Y1toT(4,4) Y2toT(4,4)];      
       [stat Y1toY4] = RmatAtoB(cor_ind(1),cor_ind(7));
       Y1toY4 = inv(cormat7)*Y1toY4 *inv(cormat1);
       %% [stat BtoY4] = RmatAtoB(ringext,ZV2xind);
       %% BtoY4*R1toE - Y1toY4  %% check the validity 
       [stat Y2toY4] = RmatAtoB(cor_ind(5),cor_ind(7));
       Y2toY4 = inv(cormat7)*Y2toY4*inv(cormat5);
       [stat Y3toY4] = RmatAtoB(cor_ind(6),cor_ind(7));
       Y3toY4 = inv(cormat7)*Y3toY4*inv(cormat6);
       
       AA = [Y1toT(3,4) Y2toT(3,4); Y1toT(4,4) Y2toT(4,4)];
       yp = AA\[uu(1);uu(2)]; 
       yp(5) = -1/Y3toY4(3,4)*(Y1toY4(3,4)*yp(1)+Y2toY4(3,4)*yp(2));
       yp(6) = -(Y1toY4(4,4)*yp(1)+Y2toY4(4,4)*yp(2)+Y3toY4(4,4)*yp(5));
       %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
       %% %% Check the bumper
       %% Y1toT*[0;0;0;yp(1);0;0]+Y2toT*[0 ;0;0;yp(2);0;0] should equal
       %% [0;0; uu(1); uu(2); 0; 0];
       %% Y1toY4*[0;0;0;yp(1);0;0]+Y2toY4*[0 ;0;0;yp(2);0;0]+Y3toY4*[0
       %% ;0;0;yp(5);0;0]+[0;0;0;yp(6);0;0] should equal 0
       %% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
       
       [stat R1toE] = RmatAtoB(cor_ind(1),ringext);  %% ringext It's a mark 
        R1toE = R1toE*inv(cormat1);
       [stat BtoR4] = RmatAtoB(1,cor_ind(4));
       BtoR4 = inv(cormat4)*BtoR4;
       %% [stat R1toR4] = RmatAtoB(ZV9Rind,ZV11Rind);
       R1toR4 = BtoR4*R1toE;
       [stat R2toR4] = RmatAtoB(cor_ind(2),cor_ind(4));
       R2toR4 = inv(cormat4)*R2toR4*inv(cormat2);
       [stat R3toR4] = RmatAtoB(cor_ind(3),cor_ind(4));
       R3toR4 = inv(cormat4)*R3toR4*inv(cormat3);
  
       %% yp_1 = 1e-3/R2toT(3,4)/(R1toT(3,4)/R2toT(3,4) - R1toT(4,4)/R2toT(4,4));
       %% yp_2 = -R1toT(4,4)/R2toT(4,4)*yp_1;9
       yp(3) = -1/R3toR4(3,4)*(R1toR4(3,4)*yp(1)+R2toR4(3,4)*yp(2));
       yp(4) = -(R1toR4(4,4)*yp(1)+R2toR4(4,4)*yp(2)+R3toR4(4,4)*yp(3));
       %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
       %% %% Check the bumper
       %% R1toT*[0;0;0;yp(1);0;0]+R2toT*[0 ;0;0;yp(2);0;0] should equal
       %% [0;0; uu(1); uu(2); 0; 0];
       %% R1toR4*[0;0;0;yp(1);0;0]+R2toR4*[0 ;0;0;yp(2);0;0]+R3toR4*[0
       %% ;0;0;yp(3);0;0]+[0;0;0;yp(4);0;0] should equal 0
       %% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
       for i=1:length(ps)
           ps_oldread(i)=PS(ps(i)).Ampl;
       end
       %% ps_newvalue = [yp_1, yp_2, yp_3, yp_4, yyp_3, yyp_4]*knob_value ;
       %% cor_newvalue = [yp_1, yp_2, yp_3, yp_4, yyp_3, yyp_4]*knob_value + ps_oldread';
       cor_newvalue = yp' + ps_oldread;
       set_ps(ps,cor_newvalue);
       
       DRind1 = [cor_ind(1)-1:ringext-1];
       xx0 = [0;0;0;0;0;0];
       xx = xx0;
       for i = 2:length(DRind1)
           [stat SingE] = RmatAtoB(DRind1(i),DRind1(i)); %% calculate the single element
           if (DRind1(i) == cor_ind(1))
               xx0 = SingE*inv(cormat1)*xx0 + [0;0;0;yp(1);0;0];
               xx0 = cormat1*xx0;
               xx = [xx, xx0];     
           else
               xx0 = SingE*xx0;
               xx = [xx, xx0];
              
           end
       end
       DRind2 = [1:cor_ind(4)];
       for i = 1:length(DRind2)
           [stat SingE] = RmatAtoB(DRind2(i),DRind2(i)); %% calculate the single element
           if (DRind2(i) == cor_ind(2))
               xx0 = SingE*inv(cormat2)*xx0 + [0;0;0;yp(2);0;0];
               xx0 = cormat2*xx0;
               xx = [xx, xx0];
           elseif (DRind2(i) == cor_ind(3))
               xx0 = SingE*inv(cormat3)*xx0 + [0;0;0;yp(3);0;0];
               xx0 = cormat3*xx0;
               xx = [xx, xx0];
           elseif (DRind2(i) == cor_ind(4))
               xx0 = SingE*inv(cormat4)*xx0 + [0;0;0;yp(4);0;0];
               xx0 = cormat4*xx0;
               xx = [xx, xx0];
           else
               xx0 = SingE*xx0;
               xx = [xx, xx0];
           end
       end
       figure(100)
       set(gcf,'Visible', 'Off');
       hh(1) = subplot(2,1,1);
       %% plotyy([ss(DRind1);ss(DRind2)+ss(ringext)],xx(3,:),[ss(DRind1);ss(DRind2)+ss(ringext)],xx(4,:))
       plot([ss(DRind1);ss(DRind2)+ss(ringext)],xx(3,:))
       hold on
       plot([ss(DRind1);ss(DRind2)+ss(ringext)],xx(4,:),'r')
       hold off
       title('Damping ring bump')
       xlabel('position (s)');
       ylabel('y y'' (m , rads)')
       
       
       EXTind = [cor_ind(1)-1:cor_ind(7)];
       xx0 = [0;0;0;0;0;0];
       xx = xx0;
       for i = 2:length(EXTind)
           [stat SingE] = RmatAtoB(EXTind(i),EXTind(i)); %% calculate the single element
           if (EXTind(i) == cor_ind(1))
               xx0 = SingE*inv(cormat1)*xx0 + [0;0;0;yp(1);0;0];
               xx0 = cormat1*xx0;
               xx = [xx, xx0];
           elseif (EXTind(i) == cor_ind(5))
               xx0 = SingE*inv(cormat5)*xx0 + [0;0;0;yp(2);0;0];
               xx0 = cormat5*xx0;
               xx = [xx, xx0];
           elseif (EXTind(i) == cor_ind(6))
               xx0 = SingE*inv(cormat6)*xx0 + [0;0;0;yp(5);0;0];
               xx0 = cormat6*xx0;
               xx = [xx, xx0];
           elseif (EXTind(i) == cor_ind(7))
               xx0 = SingE*inv(cormat7)*xx0 + [0;0;0;yp(6);0;0];
               xx0 = cormat7*xx0;
               xx = [xx, xx0];
           elseif  (EXTind(i) == Targetind(1))
               xx0 = SingE*xx0;
               xx = [xx, xx0];
               %% disp(['position of ',Target, ' is: ', num2str(xx0(1),4),', ',num2str(xx0(2),4),...
               %%    ', ',num2str(xx0(3),4),', ',num2str(xx0(4),4)  ]); 
           else
               xx0 = SingE*xx0;
               xx = [xx, xx0];
           end
       end
       hh(2) = subplot(2,1,2);
       set(hh(2),'Visible', 'Off');
       %% plotyy(ss(EXTind),xx(3,:),ss(EXTind),xx(4,:))
       plot(ss(EXTind),xx(3,:))
       hold on
       plot(ss(EXTind),xx(4,:),'r')
       hold off
       title('Extraction line bump')
       xlabel('position (s)');
       ylabel('y y'' (m , rads)')
       
       %% h = get(gcf,'Children');
       
    case 'h'
        %% ring four X cors bumper
        %% move X orbit at QM7R parallel without angle.

       for i = 1:length(cor_ind)
           L = BEAMLINE{cor_ind(i)}.L/2;
           eval(['cormat', num2str(i),' = '...
               '[1 L 0 0 0 0;' ...
               '0 1 0 0 0 0;' ...
               '0 0 1 L 0 0;'...
               '0 0 0 1 0 0;' ...
               '0 0 0 0 1 0;' ...
               '0 0 0 0 0 1];']);
       end
        %% QM7Rhind = findcells(BEAMLINE,'Name','QM7R1');
        %% hcor_ind = [ZH9Rind, ZH100Rind, ZH10Rind, ZH11Rind, ZH100Rxind, ZH1xind, ZH2xind];

        [stat X1toT] = RmatAtoB(cor_ind(1),Targetind(1));
         X1toT = X1toT*inv(cormat1);
        [stat X2toT] = RmatAtoB(cor_ind(5),Targetind(1));
         X2toT = X2toT*inv(cormat5);
        [stat X1toX4] = RmatAtoB(cor_ind(1),cor_ind(7));
         X1toX4 = inv(cormat7)*X1toX4 *inv(cormat1);
        %% [stat BtoX4] = RmatAtoB(ringext,ZH2xind);
        %% BtoX4*R1toE - X1toX4  %% check the validity
       [stat X2toX4] = RmatAtoB(cor_ind(5),cor_ind(7));
        X2toX4 = inv(cormat7)*X2toX4*inv(cormat5);
       [stat X3toX4] = RmatAtoB(cor_ind(6),cor_ind(7));
        X3toX4 = inv(cormat7)*X3toX4*inv(cormat6);
        AA = [X1toT(1,2) X2toT(1,2); X1toT(2,2) X2toT(2,2)];
        xp = AA\[uu(1);uu(2)]; 
        xp(5) = -1/X3toX4(1,2)*(X1toX4(1,2)*xp(1)+X2toX4(1,2)*xp(2));
        xp(6) = -(X1toX4(2,2)*xp(1)+X2toX4(2,2)*xp(2)+X3toX4(2,2)*xp(5));
       %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
       %% %% Check the bumper
       %% Y1toT*[0;0;0;yp(1);0;0]+Y2toT*[0 ;0;0;yp(2);0;0] should equal
       %% [0;0; uu(1); uu(2); 0; 0];
       %% X1toX4*[0;xp(1);0;0;0;0]+X2toX4*[0;xp(2);0;0;0;0]+X3toX4*[0
       %% ;xp(5);0;0;0;0]+[0;xp(6);0;0;0;0] should equal 0
       %% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%  
        [stat R1toE] = RmatAtoB(cor_ind(1),ringext);
        R1toE = R1toE*inv(cormat1);
        [stat BtoR4] = RmatAtoB(1,cor_ind(4));
        BtoR4 = inv(cormat4)*BtoR4;
        %% [stat R1toR4] = RmatAtoB(ZH9Rind,ZH11Rind);
        R1toR4 = BtoR4*R1toE;
        [stat R2toR4] = RmatAtoB(cor_ind(2),cor_ind(4));
        R2toR4 = inv(cormat4)*R2toR4*inv(cormat2);
        [stat R3toR4] = RmatAtoB(cor_ind(3),cor_ind(4));
        R3toR4 = inv(cormat4)*R3toR4*inv(cormat3);
        %% xp_1 = 1e-3/R2toT(1,2)/(R1toT(1,2)/R2toT(1,2) - R1toT(2,2)/R2toT(2,2));
        %% xp_2 = -R1toT(2,2)/R2toT(2,2)*xp_1;
        xp(3) = -1/R3toR4(1,2)*(R1toR4(1,2)*xp(1)+R2toR4(1,2)*xp(2));
        xp(4) = -(R1toR4(2,2)*xp(1)+R2toR4(2,2)*xp(2)+R3toR4(2,2)*xp(3));
        
       %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
       %% %% Check the bumper
       %% R1toT*[0;xp(1);0;0;0;0]+R2toT*[0;xp(2);0;0;0;0] should equal
       %% [uu(1); uu(2);0;0;  0; 0];
       %% R1toR4*[0;xp(1);0;0;0;0]+R2toR4*[0;xp(2);0;0;0;0]+R3toR4*[0
       %% ;xp(3);0;0;0;0]+[0;xp(4);0;0;0;0] should equal 0
       %% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%                
        for i=1:length(ps)
            ps_oldread(i)=PS(ps(i)).Ampl;
        end
        %% ps_newvalue = [xp_1, xp_2, xp_3, xp_4, xxp_3, xxp_4]*knob_value ;
        %% cor_newvalue = [xp_1, xp_2, xp_3, xp_4, xxp_3, xxp_4]*knob_value + ps_oldread;
        cor_newvalue = xp' + ps_oldread;
        set_ps(ps,cor_newvalue);
       
       EXTind = [cor_ind(1)-1:cor_ind(7)];
       beamin = FL.SimBeam{2};
       beamin.Bunch.x(1)=0;beamin.Bunch.x(2)=0;beamin.Bunch.x(3)=0;beamin.Bunch.x(4)=0;
       EXTorbitdata = FL.SimBeam{2}.Bunch.x;
       for i = 2:length(EXTind)
           [stat,beamout,instdata] = TrackThru( EXTind(i), EXTind(i), beamin, 1, 1, 0 );
           EXTorbitdata = [EXTorbitdata, beamout.Bunch.x];
           beamin = beamout; 
       end
       %% plot(ss(EXTind),EXTorbitdata(1,:))
       %% plot(EXTind,EXTorbitdata(1,:))
        
       DRind1 = [cor_ind(1)-1:ringext-1];
       xx0 = [0;0;0;0;0;0];
       xx = xx0;
       for i = 2:length(DRind1)
           [stat SingE] = RmatAtoB(DRind1(i),DRind1(i)); %% calculate the single element
           if (DRind1(i) == cor_ind(1))
               xx0 = SingE*inv(cormat1)*xx0 + [0;xp(1);0;0;0;0];
               xx0 = cormat1*xx0;
               xx = [xx, xx0];     
           else
               xx0 = SingE*xx0;
               xx = [xx, xx0];
              
           end
       end
       DRind2 = [1:cor_ind(4)];
       for i = 1:length(DRind2)
           [stat SingE] = RmatAtoB(DRind2(i),DRind2(i)); %% calculate the single element
           if (DRind2(i) == cor_ind(2))
               xx0 = SingE*inv(cormat2)*xx0 + [0;xp(2);0;0;0;0];
               xx0 = cormat2*xx0;
               xx = [xx, xx0];
           elseif (DRind2(i) == cor_ind(3))
               xx0 = SingE*inv(cormat3)*xx0 + [0;xp(3);0;0;0;0];
               xx0 = cormat3*xx0;
               xx = [xx, xx0];
           elseif (DRind2(i) == cor_ind(4))
               xx0 = SingE*inv(cormat4)*xx0 + [0;xp(4);0;0;0;0];
               xx0 = cormat4*xx0;
               xx = [xx, xx0];
           else
               xx0 = SingE*xx0;
               xx = [xx, xx0];
           end
       end
       figure(101)
       set(gcf,'Visible', 'Off');
       hh(1) = subplot(2,1,1);
       %% set(hh(1),'Visible', 'Off');
       plot([ss(DRind1);ss(DRind2)+ss(ringext)],xx(1,:))
       hold on
       plot([ss(DRind1);ss(DRind2)+ss(ringext)],xx(2,:),'r')
       hold off
       title('Damping ring bump')
       xlabel('position (s)');
       ylabel('x x'' (m , rads)')
       
       EXTind = [cor_ind(1)-1:cor_ind(7)];
       xx0 = [0;0;0;0;0;0];
       xx = xx0;
       for i = 2:length(EXTind)
           [stat SingE] = RmatAtoB(EXTind(i),EXTind(i)); %% calculate the single element
           if (EXTind(i) == cor_ind(1))
               xx0 = SingE*inv(cormat1)*xx0 + [0;xp(1);0;0;0;0];
               xx0 = cormat1*xx0;
               xx = [xx, xx0];
           elseif (EXTind(i) == cor_ind(5))
               xx0 = SingE*inv(cormat5)*xx0 + [0;xp(2);0;0;0;0];
               xx0 = cormat5*xx0;
               xx = [xx, xx0];
           elseif (EXTind(i) == cor_ind(6))
               xx0 = SingE*inv(cormat6)*xx0 + [0;xp(5);0;0;0;0];
               xx0 = cormat6*xx0;
               xx = [xx, xx0];
           elseif (EXTind(i) == cor_ind(7))
               xx0 = SingE*inv(cormat7)*xx0 + [0;xp(6);0;0;0;0];
               xx0 = cormat7*xx0;
               xx = [xx, xx0];
           elseif  (EXTind(i) == Targetind(1))
               xx0 = SingE*xx0;
               xx = [xx, xx0];
             %%  disp(['position of ',Target, ' is: ', num2str(xx0(1),4),', ',num2str(xx0(2),4),...
             %%      ', ',num2str(xx0(3),4),', ',num2str(xx0(4),4)  ]); 
           else
               xx0 = SingE*xx0;
               xx = [xx, xx0];
           end
       end
       hh(2) = subplot(2,1,2);
       set(hh(2),'Visible', 'Off');
       %% plotyy(ss(EXTind),xx(1,:),ss(EXTind),xx(2,:))
       plot(ss(EXTind),xx(1,:))
       hold on
       plot(ss(EXTind),xx(2,:),'r')
       hold off
       title('Extraction line bump')
       xlabel('position (s)');
       ylabel('x x'' (m , rads)')
       %% h = get(gcf,'Children');
  otherwise
        disp('Please choose ''H'' or ''V'' for horizontal or vertical plane:');
        disp(displacement_knob_Name);
end

end

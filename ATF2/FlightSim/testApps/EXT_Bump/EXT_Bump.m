function varargout = EXT_Bump(varargin)
% EXT_BUMP M-file for EXT_Bump.fig
%      EXT_BUMP, by itself, creates a new EXT_BUMP or raises the existing
%      singleton*.
%
%      H = EXT_BUMP returns the handle to a new EXT_BUMP or the handle to
%      the existing singleton*.
%
%      EXT_BUMP('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in EXT_BUMP.M with the given input arguments.
%
%      EXT_BUMP('Property','Value',...) creates a new EXT_BUMP or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before EXT_Bump_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to EXT_Bump_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help EXT_Bump

% Last Modified by GUIDE v2.5 01-Dec-2009 10:13:27

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @EXT_Bump_OpeningFcn, ...
                   'gui_OutputFcn',  @EXT_Bump_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT

% --- Executes just before EXT_Bump is made visible.
function EXT_Bump_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to EXT_Bump (see VARARGIN)

% Choose default command line output for EXT_Bump
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes EXT_Bump wait for user response (see UIRESUME)
% uiwait(handles.figure1);

global BEAMLINE  INSTR

ZV9Rind = findcells(BEAMLINE,'Name','ZV9R');     %% 1249
ZV2xind = findcells(BEAMLINE,'Name','ZV2X'); %% 1309
ZV11Rind = findcells(BEAMLINE,'Name','ZV11R');  %% 77 
ringext = findcells(BEAMLINE,'Name','IEX');

tt = cellfun(@(x) x.Index, INSTR, 'UniformOutput',false);
FlHwUpdate;
xx = cellfun(@(x) x.Data(1),INSTR);
yy = cellfun(@(x) x.Data(2),INSTR);
%% FlHwUpdate('wait',20)
%% [stat,output] = FlHwUpdate('bpmave',20);
%% xx = output{1}(1,:);
%% yy = output{1}(2,:);
instr_ind = cat(1,tt{1:end});  %% notice it's {} not ()
[mm, ia, ib]  = intersect(instr_ind, [ZV9Rind:ZV2xind]);
[mm1, ia1, ib1]  = intersect(instr_ind, [ZV9Rind:ringext]);
[mm2, ia2, ib2]  = intersect(instr_ind, [1:ZV11Rind]);
handles.mm = mm;
handles.ia = ia;
handles.mmd = [mm1, mm2];
handles.iad = [ia1, ia2];
instr_ss = zeros(length(handles.ia),1);
instr_ssd = zeros(length(handles.iad),1);
for ii = 1:length(handles.ia)
   instr_ss(ii) = BEAMLINE{handles.mm(ii)}.S;
end
for ii = 1:length(ia1)
   instr_ssd(ii) = BEAMLINE{handles.mmd(ii)}.S;
end
for ii = length(ia1)+1:length(ia2)+length(ia1)
   instr_ssd(ii) = BEAMLINE{handles.mmd(ii)}.S+BEAMLINE{ringext}.S;
end
handles.instr_ss = instr_ss;
handles.instr_ssd = instr_ssd;
handles.xxd0 = xx(handles.iad);
handles.xx0 =  xx(handles.ia);
handles.yyd0 = yy(handles.iad);
handles.yy0 = yy(handles.ia);
        ZH9Rind = findcells(BEAMLINE,'Name','ZH9R');     %% 1253 %% 1251
        ZH100Rind = findcells(BEAMLINE,'Name','ZH100R'); %% 11
        ZH100Rxind = findcells(BEAMLINE,'Name','ZH100RX'); %% 1268 %% 1266
        ZH10Rind = findcells(BEAMLINE,'Name','ZH10R');   %% 59
        ZH11Rind = findcells(BEAMLINE,'Name','ZH11R');   %% 70
        ZH1xind = findcells(BEAMLINE,'Name','ZH1X'); %%1323 %% 1321
        ZH2xind = findcells(BEAMLINE,'Name','ZH2X'); %%1334 %% 1332
        hcor_ind = [ZH9Rind, ZH100Rind, ZH10Rind, ZH11Rind, ZH100Rxind, ZH1xind, ZH2xind];
        psh = [BEAMLINE{ZH9Rind}.PS, BEAMLINE{ZH100Rind}.PS, BEAMLINE{ZH10Rind}.PS, BEAMLINE{ZH11Rind}.PS, BEAMLINE{ZH1xind}.PS, BEAMLINE{ZH2xind}.PS];
        request={[] psh []};
        [stat resph] = AccessRequest(request);
        if(stat{1}==-1)
            display(sprintf('AccessRequest returned error :%s',stat{2}));
        else
            display(sprintf('Access granted, ID of request : %s',resph));
        end
        
[cor_h0, hfig ] = ExtBump('QM7RX', 'h', [0 0]*1e-3, hcor_ind, psh );
   t1 = get(hfig(1),'Children');
   t2 = get(hfig(2),'Children');
    handles.DRsh = get(t1(1),'XData');
    handles.DRx = get(t1(2),'YData');  %% reverse order for the data sequence 
    handles.DRxp = get(t1(1),'YData');

    axes(handles.axes3)
    plot(handles.DRsh, handles.DRx*1e3)
    hold on
    plot(handles.DRsh, handles.DRxp*1e3, 'r');
    plot(handles.instr_ssd,xx(handles.iad)*1e3,'g d')
    hold off
    set(gca,'XLim',[min(handles.DRsh), max(handles.DRsh)])
    set(gca,'XTick',[]);
    ylabel('x x'' (mm , mrad)')
    % legend('x', 'x''', 'BPM')
    title('Damping ring bump')
    
    axes(handles.axes5)
    handles.EXTsh = get(t2(1),'XData');
    handles.EXTx = get(t2(2),'YData');
    handles.EXTxp = get(t2(1),'YData');
    plot( handles.EXTsh, handles.EXTx*1e3)
    hold on
    plot( handles.EXTsh, handles.EXTxp*1e3, 'r');
    plot(handles.instr_ss, xx(handles.ia)*1e3,'g d')
    hold off
    set(gca,'XLim',[min(handles.EXTsh), max(handles.EXTsh)]);
    set(gca,'XTick',[]);
    title('Extraction line bump')
    ylabel('x x'' (mm , mrad)')
    % legend('x', 'x''', 'BPM')
    
    axes(handles.axes7)
    bar(cor_h0, 0.8)
    ylabel('HC ps')
    %% set(gca,'XTickLabel',{'ZH9R', 'ZH100R(x)','ZH10R', 'ZH11R', 'ZH1X', 'ZH2X'});
    set(gca,'xlim',[0 7])

       ZV100Rind = findcells(BEAMLINE,'Name','ZV100R'); %% 19
       ZV100Rxind = findcells(BEAMLINE,'Name','ZV100RX'); %% 1272 %% 1270
       ZV10Rind = findcells(BEAMLINE,'Name','ZV10R');   %% 61
       %% ZV11Rind = findcells(BEAMLINE,'Name','ZV11R');   %% 77
       ZV1xind = findcells(BEAMLINE,'Name','ZV1X'); %% 1295 %% 1293
       vcor_ind = [ZV9Rind, ZV100Rind, ZV10Rind, ZV11Rind, ZV100Rxind, ZV1xind, ZV2xind];
       psv = [BEAMLINE{ZV9Rind}.PS, BEAMLINE{ZV100Rind}.PS, BEAMLINE{ZV10Rind}.PS, BEAMLINE{ZV11Rind}.PS, BEAMLINE{ZV1xind}.PS, BEAMLINE{ZV2xind}.PS];
       request={[] psv []};
        [stat respv] = AccessRequest(request);
        if(stat{1}==-1)
            disp(sprintf('AccessRequest returned error :%s',stat{2}));
        else
            disp(sprintf('Access granted, ID of request : %s',respv));
        end

    [cor_v0, hfig ] = ExtBump('QM7RX', 'v', [0 0]*1e-3, vcor_ind, psv );
    t1 = get(hfig(1),'Children');
    t2 = get(hfig(2),'Children');
    handles.DRsv = get(t1(1),'XData');
    handles.DRy = get(t1(2),'YData');
    handles.DRyp = get(t1(1),'YData');

    axes(handles.axes4)
    plot( handles.DRsv, handles.DRy*1e3)
    hold on
    plot( handles.DRsv, handles.DRyp*1e3, 'r');
    plot(handles.instr_ssd,yy(handles.iad)*1e3,'g d')
    hold off
    set(gca,'XLim',[min(handles.DRsh), max(handles.DRsh)])
    xlabel('position (s)')
    ylabel('y y'' (mm , mrad)')
    % legend('y', 'y''', 'BPM')
    
    axes(handles.axes6)
    handles.EXTsv = get(t2(1),'XData');
    handles.EXTy = get(t2(2),'YData');
    handles.EXTyp = get(t2(1),'YData');
    plot( handles.EXTsv, handles.EXTy*1e3)
    hold on
    plot( handles.EXTsv, handles.EXTyp*1e3, 'r');
    plot(handles.instr_ss,yy(handles.ia)*1e3,'g d')
    hold off
    set(gca,'XLim',[min(handles.EXTsh), max(handles.EXTsh)])
    xlabel('position (s)')
    ylabel('y y'' (mm , mrad)')
    % legend('y', 'y''', 'BPM')
    
    axes(handles.axes8)
    bar(cor_v0, 0.8)
    ylabel('VC ps')
    set(gca,'xlim',[0 7])
handles.resph = resph;
handles.respv = respv;
handles.hcor_ind = hcor_ind;
handles.vcor_ind = vcor_ind;    
handles.psh = psh;
handles.psv = psv;
handles.cor_h0 = cor_h0;
handles.cor_v0 = cor_v0;
handles.corh_newv = cor_h0;
handles.corv_newv = cor_v0;
guidata(hObject,handles);
 

% --- Outputs from this function are returned to the command line.
function varargout = EXT_Bump_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on button press in pushbutton1.
function pushbutton1_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global INSTR 
uu = eval(['[',get(handles.edit1,'String'),']']);
str = get(handles.popupmenu1, 'String');
val = get(handles.popupmenu1,'Value');
handles.plane = get(handles.popupmenu2,'Value');
guidata(hObject,handles);
% Create frequency plot
if  handles.plane == 1
    plane = 'h';
    [corh_newv, hfig  ] = ExtBump(str{val}, plane, uu*1e-3, handles.hcor_ind, handles.psh );
    handles.corh_newv = corh_newv;
    FlHwUpdate;
    xx = cellfun(@(x) x.Data(1),INSTR);
    yy = cellfun(@(x) x.Data(2),INSTR);
    %% FlHwUpdate('wait',20)
    %% [stat,output] = FlHwUpdate('bpmave',20)
    %% xx = output{1}(1,:);
    %% yy = output{1}(2,:);

    t1 = get(hfig(1),'Children');
    t2 = get(hfig(2),'Children');
    handles.DRx = handles.DRx + get(t1(2),'YData');
    handles.DRxp = handles.DRxp + get(t1(1),'YData');
    
    axes(handles.axes3)
    plot(handles.DRsh, handles.DRx*1e3)
    hold on
    plot(handles.DRsh, handles.DRxp*1e3, 'r');
    plot(handles.instr_ssd, (xx(handles.iad)-handles.xxd0)*1e3,'g d')
    hold off
    set(gca,'XLim',[min(handles.DRsv), max(handles.DRsv)])
    set(gca,'XTick',[]);
    ylabel('x x'' (mm , mrad)');
    % legend('x', 'x''', 'BPM')
    title('Damping ring bump');
    %% subplot(4,1,2)
    axes(handles.axes5)
    handles.EXTx = handles.EXTx + get(t2(2),'YData');
    handles.EXTxp = handles.EXTxp + get(t2(1),'YData');
    plot( handles.EXTsh, handles.EXTx*1e3)
    hold on
    plot( handles.EXTsh, handles.EXTxp*1e3, 'r');
    plot(handles.instr_ss, (xx(handles.ia)-handles.xx0)*1e3,'g d')
    hold off
    set(gca,'XLim',[min(handles.EXTsh), max(handles.EXTsh)]);
    set(gca,'XTick',[]);
    title('Extraction line bump')
    ylabel('x x'' (mm , mrad)')
    % legend('y', 'y''', 'BPM')
    axes(handles.axes7)
    bar(handles.corh_newv-handles.cor_h0, 0.8)
    ylabel('HC ps')
    set(gca,'xlim',[0 7])

    guidata(hObject,handles);
else
    plane = 'v';
    [corv_newv, hfig  ] = ExtBump(str{val}, plane, uu*1e-3, handles.vcor_ind, handles.psv );
    handles.corv_newv = corv_newv;
    guidata(hObject,handles);
    FlHwUpdate;
    xx = cellfun(@(x) x.Data(1),INSTR);
    yy = cellfun(@(x) x.Data(2),INSTR);
    %% FlHwUpdate('wait',20)
    %% [stat,output] = FlHwUpdate('bpmave',20)
    %% xx = output{1}(1,:);
    %% yy = output{1}(2,:);

    t1 = get(hfig(1),'Children');
    t2 = get(hfig(2),'Children');
    
    %% subplot(4,1,3)
    axes(handles.axes4)
    handles.DRy = handles.DRy + get(t1(2),'YData');
    handles.DRyp = handles.DRyp + get(t1(1),'YData');
    plot( handles.DRsv, handles.DRy*1e3)
    hold on
    plot( handles.DRsv, handles.DRyp*1e3, 'r');
    plot(handles.instr_ssd, (yy(handles.iad)-handles.yyd0)*1e3,'g d')
    hold off
    set(gca,'XLim',[min(handles.DRsv), max(handles.DRsv)])
    xlabel('position (s)')
    ylabel('y y'' (mm , mrad)')
    % legend('y', 'y''', 'BPM')
    
    %% subplot(4,1,4)
    axes(handles.axes6)
    handles.EXTy = handles.EXTy + get(t2(2),'YData');
    handles.EXTyp = handles.EXTyp + get(t2(1),'YData');
    plot( handles.EXTsv, handles.EXTy*1e3)
    hold on
    plot( handles.EXTsv, handles.EXTyp*1e3, 'r');
    plot(handles.instr_ss, (yy(handles.ia)-handles.yy0)*1e3,'g d')
    hold off
    set(gca,'XLim',[min(handles.EXTsh), max(handles.EXTsh)])
    xlabel('position (s)')
    ylabel('y y'' (mm , mrad)')
    % legend('y', 'y''', 'BPM')
    axes(handles.axes8)
    bar(handles.corv_newv-handles.cor_v0, 0.8)
    ylabel('VC ps')
    set(gca,'xlim',[0 7])
    guidata(hObject,handles);
end
    
% Create time plot
%% axes(handles.time_axes)
%% plot(t,x)
%% set(handles.time_axes,'XMinorTick','on')
%% grid on

% --- Executes on selection change in popupmenu1.
function popupmenu1_Callback(hObject, eventdata, handles)
% hObject    handle to popupmenu1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% Get user input from GUI



% Hints: contents = get(hObject,'String') returns popupmenu1 contents as cell array
%        contents{get(hObject,'Value')} returns selected item from popupmenu1


% --- Executes during object creation, after setting all properties.
function popupmenu1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to popupmenu1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in popupmenu2.
function popupmenu2_Callback(hObject, eventdata, handles)
% hObject    handle to popupmenu2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
%% multiKnobsFn('save');
%% handles=guiCloseFn('Ext_Bump',handles);


% Hints: contents = get(hObject,'String') returns popupmenu2 contents as cell array
%        contents{get(hObject,'Value')} returns selected item from popupmenu2


% --- Executes during object creation, after setting all properties.
function popupmenu2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to popupmenu2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit1_Callback(hObject, eventdata, handles)
% hObject    handle to edit1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit1 as text
%        str2double(get(hObject,'String')) returns contents of edit1 as a double


% --- Executes during object creation, after setting all properties.
function edit1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in pushbutton2.
function pushbutton2_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
resp=questdlg('Are you sure?','Exit Request','Yes','No','No');
if ~strcmp(resp,'Yes'); return; end;
try  
  guiCloseFn('EXT_Bump',handles);
  [stat resp] = AccessRequest('release',handles.resph);
  [stat resp] = AccessRequest('release',handles.respv);
catch
 %% exit
end
        %% delete(Ext_Bump1);
        %% guiCloseFn(Ext_Bump,handles)


% --- Executes on button press in pushbutton3.
function pushbutton3_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global INSTR 
handles.plane = get(handles.popupmenu2,'Value');
if  handles.plane == 1
    [handles.corh_newv] = set_ps(handles.psh,handles.cor_h0);
    FlHwUpdate;
    xx = cellfun(@(x) x.Data(1),INSTR);
    yy = cellfun(@(x) x.Data(2),INSTR);
    %%  FlHwUpdate('wait',20)
    %% [stat,output] = FlHwUpdate('bpmave',20)
    %% xx = output{1}(1,:);
    %% yy = output{1}(2,:);

    axes(handles.axes3);
    handles.DRx = zeros(1,length(handles.DRx));
    handles.DRxp = zeros(1,length(handles.DRxp));
    axes(handles.axes3)
    plot(handles.DRsh, handles.DRx*1e3)
    hold on
    plot(handles.DRsh, handles.DRxp*1e3, 'r');
    plot(handles.instr_ssd, xx(handles.iad)*1e3,'g d')
    hold off
    set(gca,'XLim',[min(handles.DRsv), max(handles.DRsv)])
    set(gca,'XTick',[]);
    ylabel('x x'' (mm , mrad)');
    % legend('x', 'x''', 'BPM')
    title('Damping ring bump');
    
    axes(handles.axes5)
    handles.EXTx = zeros(1,length(handles.EXTx));
    handles.EXTxp = zeros(1,length(handles.EXTxp));
    plot( handles.EXTsh, handles.EXTx*1e3)
    hold on
    plot( handles.EXTsh, handles.EXTxp*1e3, 'r');
    plot(handles.instr_ss, xx(handles.ia)*1e3,'g d')
    hold off
    set(gca,'XLim',[min(handles.EXTsh), max(handles.EXTsh)]);
    set(gca,'XTick',[]);
  
    title('Extraction line bump')
    ylabel('x x'' (mm , mrad)')
    % legend('x', 'x''', 'BPM')
    axes(handles.axes7)
    bar(handles.corh_newv, 0.8)
    ylabel('HC ps')
    %% set(gca,'XTickLabel',{'ZH9R', 'ZH100R(x)','ZH10R', 'ZH11R', 'ZH1X', 'ZH2X'});
    set(gca,'xlim',[0 7])
    guidata(hObject,handles);
else
    [handles.corv_newv] = set_ps(handles.psv,handles.cor_v0);
    axes(handles.axes4);
    handles.DRy = zeros(1,length(handles.DRy));
    handles.DRyp = zeros(1,length(handles.DRyp));
    FlHwUpdate;
    xx = cellfun(@(x) x.Data(1),INSTR);
    yy = cellfun(@(x) x.Data(2),INSTR);
    %% FlHwUpdate('wait',20)
    %% [stat,output] = FlHwUpdate('bpmave',20)
    %% xx = output{1}(1,:);
    %% yy = output{1}(2,:);

    plot( handles.DRsv, handles.DRy*1e3)
    hold on
    plot( handles.DRsv, handles.DRyp*1e3, 'r');
    plot(handles.instr_ssd, yy(handles.iad)*1e3,'g d')
    hold off
    set(gca,'XLim',[min(handles.DRsv), max(handles.DRsv)])
    xlabel('position (s)')
    ylabel('y y'' (mm , mrad)')
    % legend('y', 'y''', 'BPM')
    
    %% subplot(4,1,4)
    axes(handles.axes6)
    handles.EXTy = zeros(1,length(handles.EXTy));
    handles.EXTyp = zeros(1,length(handles.EXTyp));
    plot( handles.EXTsv, handles.EXTy*1e3)
    hold on
    plot( handles.EXTsv, handles.EXTyp*1e3, 'r');
    plot(handles.instr_ss, yy(handles.ia)*1e3,'g d')
    hold off
    set(gca,'XLim',[min(handles.EXTsh), max(handles.EXTsh)])
    xlabel('position (s)')
    ylabel('y y'' (mm , mrad)')
    %% legend('y', 'y''', 'BPM')
    axes(handles.axes8)
    bar(handles.corv_newv, 0.8)
    ylabel('VC ps')
    set(gca,'xlim',[0 7])
    guidata(hObject,handles);
 end
    
   



function edit2_Callback(hObject, eventdata, handles)
% hObject    handle to edit2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit2 as text
%        str2double(get(hObject,'String')) returns contents of edit2 as a double


% --- Executes during object creation, after setting all properties.
function edit2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit3_Callback(hObject, eventdata, handles)
% hObject    handle to edit3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit3 as text
%        str2double(get(hObject,'String')) returns contents of edit3 as a double


% --- Executes during object creation, after setting all properties.
function edit3_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- If Enable == 'on', executes on mouse press in 5 pixel border.
% --- Otherwise, executes on mouse press in 5 pixel border or over popupmenu2.
function popupmenu2_ButtonDownFcn(hObject, eventdata, handles)
% hObject    handle to popupmenu2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

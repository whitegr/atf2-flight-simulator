global BEAMLINE

disp(' ')
disp(' ')
disp(' ')

quadinds = findcells(BEAMLINE,'Class','QUAD');
% quadinds = quadinds(quadinds>findcells(BEAMLINE,'Name','IEX'));

fid = fopen('quadbpms_ring.txt','w');

for ind=quadinds
    if strcmp(BEAMLINE{ind+2}.Class,'QUAD')
        continue
    elseif ~strcmp(BEAMLINE{ind+1}.Class,'QUAD') && strcmp(BEAMLINE{ind+2}.Class,'MONI')
        disp(['QUAD #' num2str(ind) ...
            ' : ' BEAMLINE{ind+2}.Class ...
            ' : ' num2str(ind+2)])
        fprintf(fid,'%s\n',[BEAMLINE{ind}.Name ' ' num2str(ind+2)]);
    elseif ~strcmp(BEAMLINE{ind+1}.Class,'QUAD') && strcmp(BEAMLINE{ind-3}.Class,'MONI')
        disp(['QUAD #' num2str(ind) ...
            ' : ' BEAMLINE{ind-3}.Class ...
            ' : ' num2str(ind-3)])
        fprintf(fid,'%s\n',[BEAMLINE{ind}.Name ' ' num2str(ind-3)]);
    end
end
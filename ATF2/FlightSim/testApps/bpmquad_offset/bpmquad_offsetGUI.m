function varargout = bpmquad_offsetGUI(varargin)
% BPMQUAD_OFFSETGUI M-file for bpmquad_offsetGUI.fig
%      BPMQUAD_OFFSETGUI, by itself, creates a new BPMQUAD_OFFSETGUI or raises the existing
%      singleton*.
%
%      H = BPMQUAD_OFFSETGUI returns the handle to a new BPMQUAD_OFFSETGUI
%      or the handle to
%      the existing singleton*.
%
%      BPMQUAD_OFFSETGUI('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in BPMQUAD_OFFSETGUI.M with the given input arguments.
%
%      BPMQUAD_OFFSETGUI('Property','Value',...) creates a new BPMQUAD_OFFSETGUI or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before bpmquad_offsetGUI_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property
%      application
%      stop.  All inputs are passed to bpmquad_offsetGUI_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help bpmquad_offsetGUI

% Last Modified by GUIDE v2.5 11-Dec-2011 23:56:24

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
  'gui_Singleton',  gui_Singleton, ...
  'gui_OpeningFcn', @bpmquad_offsetGUI_OpeningFcn, ...
  'gui_OutputFcn',  @bpmquad_offsetGUI_OutputFcn, ...
  'gui_LayoutFcn',  [] , ...
  'gui_Callback',   []);
if nargin && ischar(varargin{1})
  gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
  [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
  gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT

% --- Executes just before bpmquad_offsetGUI is made visible.
function bpmquad_offsetGUI_OpeningFcn(hObject, eventdata, handles, varargin) %#ok<*INUSL>
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to bpmquad_offsetGUI (see VARARGIN)
global INSTR BEAMLINE

% Choose default command line output for bpmquad_offsetGUI
handles.output = hObject;

% Update FS
FlHwUpdate;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes bpmquad_offsetGUI wait for user response (see UIRESUME)
% uiwait(handles.figure1);

global BBA_GUIDATA

if ~isfield(BBA_GUIDATA,'scanrange')
  BBA_GUIDATA.scanrange = [-1 1] * str2double(get(handles.edit1,'String'));
else
  set(handles.edit1,'String',num2str(BBA_GUIDATA.scanrange(2)));
end

if ~isfield(BBA_GUIDATA,'quadbpminfo')
  BBA_GUIDATA.quadbpminfo = importdata('quadbpms.txt');
end

if ~isfield(BBA_GUIDATA,'pps')
  BBA_GUIDATA.pps = str2double(get(handles.edit2,'String'));
else
  set(handles.edit2,'String',num2str(BBA_GUIDATA.pps));
end

if ~isfield(BBA_GUIDATA,'aves')
  BBA_GUIDATA.aves = str2double(get(handles.edit4,'String'));
else
  set(handles.edit4,'String',num2str(BBA_GUIDATA.aves));
end

if ~isfield(BBA_GUIDATA,'shuntval')
  BBA_GUIDATA.shuntval = str2double(get(handles.edit5,'String'));
else
  set(handles.edit5,'String',num2str(BBA_GUIDATA.shuntval));
end

if ~isfield(BBA_GUIDATA,'scantype')
  BBA_GUIDATA.scantype = 'mover';
end

if ~isfield(BBA_GUIDATA,'devtype')
  BBA_GUIDATA.devtype = 'quad';
else
  if strcmpi(BBA_GUIDATA.devtype,'quad')
    BBA_GUIDATA.devtype = 'quad';
  elseif strcmpi(BBA_GUIDATA.devtype,'sext')
    BBA_GUIDATA.devtype = 'sext';
  else
    error('BBA_GUIDATA.devtype must be ''quad'' or ''sext''.')
  end
end

BBA_GUIDATA.camsettings.quad.a = 0.145250;
BBA_GUIDATA.camsettings.quad.c = 0.123952;
BBA_GUIDATA.camsettings.quad.R = 0.031;
BBA_GUIDATA.camsettings.quad.L = 0.0015875;
BBA_GUIDATA.camsettings.quad.S1 = 0.034925;
BBA_GUIDATA.camsettings.quad.S2 = 0.2905;
BBA_GUIDATA.camsettings.quad.b = BBA_GUIDATA.camsettings.quad.c + ...
  BBA_GUIDATA.camsettings.quad.S1 - (sqrt(2)*BBA_GUIDATA.camsettings.quad.R);

BBA_GUIDATA.camsettings.sext.a = 0.145250;
BBA_GUIDATA.camsettings.sext.c = 0.123952;
BBA_GUIDATA.camsettings.sext.R = 0.031;
BBA_GUIDATA.camsettings.sext.L = 0.0015875;
BBA_GUIDATA.camsettings.sext.S1 = 0.034925;
BBA_GUIDATA.camsettings.sext.S2 = 0.2905;
BBA_GUIDATA.camsettings.sext.b = BBA_GUIDATA.camsettings.sext.c + ...
  BBA_GUIDATA.camsettings.sext.S1 - (sqrt(2)*BBA_GUIDATA.camsettings.sext.R);

BBA_GUIDATA.unsaveddata = false;

% JitterSubtract method
BBA_GUIDATA.JS = FlJitterSubtract;
% BBA_GUIDATA.JS.useINSTR=findcells(INSTR,'Class','MONI',findcells(BEAMLINE,'Name','MQF1X'),length(BEAMLINE));

useApp('sextBBA');

% --- Outputs from this function are returned to the command line.
function varargout = bpmquad_offsetGUI_OutputFcn(hObject, eventdata, handles)
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = hObject;
varargout{2} = eventdata;
varargout{3} = handles;

% --- Executes on button press in pushbutton1.
function [hObject, eventdata, handles] = pushbutton1_Callback(hObject, eventdata, handles) %#ok<*DEFNU>
% hObject    handle to pushbutton1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

global BEAMLINE BBA_GUIDATA

if ~isfield(BBA_GUIDATA,'running')
  BBA_GUIDATA.running = false;
end
if BBA_GUIDATA.running==true
  return
elseif BBA_GUIDATA.running==false
  BBA_GUIDATA.running=true;
end

quadinds = findcells(BEAMLINE,'Class','QUAD',findcells(BEAMLINE,'Name','IEX'),length(BEAMLINE));

for count=1:length(quadinds)
  quadlist{count} = BEAMLINE{quadinds(count)}.Name; %#ok<AGROW>
end
[B I] = unique(quadlist);
quadlist = quadlist(sort(I));

set(handles.listbox2,'Value',1)
set(handles.listbox2,'String',quadlist);

BBA_GUIDATA.devtype = 'quad';

BBA_GUIDATA.running = false;


% --- Executes on button press in pushbutton5.
function pushbutton5_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton5 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

global BEAMLINE BBA_GUIDATA

if ~isfield(BBA_GUIDATA,'running')
  BBA_GUIDATA.running = false;
end
if BBA_GUIDATA.running==true
  return
elseif BBA_GUIDATA.running==false
  BBA_GUIDATA.running=true;
end

sextinds = findcells(BEAMLINE,'Class','SEXT');
ringext = findcells(BEAMLINE,'Name','IEX');
sextinds = sextinds(sextinds>ringext);

sextlist={};
for count=1:length(sextinds)
  if ~strcmp(BEAMLINE{sextinds(count)}.Name,'SK1FF')
    sextlist{end+1} = BEAMLINE{sextinds(count)}.Name; %#ok<AGROW>
  end
end

[B I] = unique(sextlist);
sextlist = sextlist(sort(I));

set(handles.listbox2,'Value',1)
set(handles.listbox2,'String',sextlist);

BBA_GUIDATA.devtype = 'sext';

BBA_GUIDATA.running = false;

% --- Executes on selection change in listbox2.
function listbox2_Callback(hObject, eventdata, handles)
% hObject    handle to listbox2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = get(hObject,'String') returns listbox2 contents as cell array
%        contents{get(hObject,'Value')} returns selected item from listbox2

global BEAMLINE BBA_GUIDATA INSTR FL GIRDER

if ~isfield(BBA_GUIDATA,'running')
  BBA_GUIDATA.running = false;
end
if BBA_GUIDATA.running==true
  return
elseif BBA_GUIDATA.running==false
  BBA_GUIDATA.running=true;
end

contents = get(handles.listbox2,'String');
quadname = contents{get(handles.listbox2,'Value')};

ringext = findcells(BEAMLINE,'Name','IEX');

temp_bpminds = findcells(BEAMLINE,'Class','MONI');
bpminds = [];
for ind=1:length(temp_bpminds)
  instr_ind = findcells(INSTR, 'Index', temp_bpminds(ind));
  if ~strcmp(INSTR{instr_ind}.Class,'NOHW')
    bpminds = [bpminds temp_bpminds(ind)];
  end
end
quadind = max(findcells(BEAMLINE,'Name',quadname));

if get(handles.radiobutton1,'Value')
  planename='x'; iplane=1;
else
  planename='y'; iplane=2;
end

% Get BBA PV
ibpm=findcells(BEAMLINE,'Name',['M' quadname]);
if ~isempty(ibpm)
  bpminstr=findcells(INSTR,'Index',ibpm);
  if strcmp(INSTR{bpminstr}.Type,'stripline')
    bbapv=[['M' quadname] ':BBA_' upper(planename)];
  else
    bbapv=[quadname planename ':bbaOffset'];
  end
  if ~FL.SimMode
    bbaval=lcaGet(bbapv).*FL.HwInfo.INSTR(bpminstr).conv(1);
  else
    bbaval=0;
  end
  if isnan(bbaval)
    set(handles.text9,'String','0.00')
  else
    set(handles.text9,'String',num2str(bbaval*1e6))
  end
else
  set(handles.text9,'String','---')
end
drawnow('expose')

% Set mover limits if on mover
if isfield(GIRDER{BEAMLINE{quadind}.Girder},'Mover') && ~isempty(GIRDER{BEAMLINE{quadind}.Girder}.Mover)
  maxbump=max(abs([FL.HwInfo.GIRDER(BEAMLINE{quadind}.Girder).low(iplane) ...
    FL.HwInfo.GIRDER(BEAMLINE{quadind}.Girder).high(iplane)]))*1e3;
else
  maxbump=2;
end
set(handles.text8,'String',sprintf('( max = %.1f )',maxbump));

[bpmname bpminds_choose] = bpmchoose(quadind,planename);
if isempty(bpmname)
  bpminds = bpminds(bpminds>quadind);
else
  bpminds=bpminds_choose;
end
  
if strcmp(BBA_GUIDATA.devtype,'quad')
  handles.axes8 = subplot(2,2,1,'Parent',handles.uipanel3);
  handles.axes6 = subplot(2,2,2,'Parent',handles.uipanel3);
  handles.axes5 = subplot(2,2,3,'Parent',handles.uipanel3);
  handles.axes7 = subplot(2,2,4,'Parent',handles.uipanel3);
  
  if length(bpminds)>5
    xlabel(handles.axes8,BEAMLINE{bpminds(1)}.Name)
    ylabel(handles.axes8,BEAMLINE{bpminds(2)}.Name)
    xlabel(handles.axes6,BEAMLINE{bpminds(1)}.Name)
    ylabel(handles.axes6,BEAMLINE{bpminds(3)}.Name)
    xlabel(handles.axes5,BEAMLINE{bpminds(1)}.Name)
    ylabel(handles.axes5,BEAMLINE{bpminds(4)}.Name)
    xlabel(handles.axes7,BEAMLINE{bpminds(1)}.Name)
    ylabel(handles.axes7,BEAMLINE{bpminds(5)}.Name)
  else
    xlabel(handles.axes8,BEAMLINE{bpminds(1)}.Name)
    ylabel(handles.axes8,BEAMLINE{bpminds(2)}.Name)
    if length(bpminds)>=3
      xlabel(handles.axes6,BEAMLINE{bpminds(1)}.Name)
      ylabel(handles.axes6,BEAMLINE{bpminds(3)}.Name)
    else
      cla(handles.axes6)
      xlabel(handles.axes6,'')
      ylabel(handles.axes6,'')
    end
    if length(bpminds)>=4
      xlabel(handles.axes5,BEAMLINE{bpminds(1)}.Name)
      ylabel(handles.axes5,BEAMLINE{bpminds(4)}.Name)
    else
      cla(handles.axes5)
      xlabel(handles.axes5,'')
      ylabel(handles.axes5,'')
    end
    if length(bpminds)>=5
      xlabel(handles.axes7,BEAMLINE{bpminds(1)}.Name)
      ylabel(handles.axes7,BEAMLINE{bpminds(5)}.Name)
    else
      cla(handles.axes7)
      xlabel(handles.axes7,'')
      ylabel(handles.axes7,'')
    end
  end
  if isfield(BBA_GUIDATA,planename) && eval(['isfield(BBA_GUIDATA.' planename ',quadname)'])
    cla(handles.axes8)
    cla(handles.axes6)
    cla(handles.axes5)
    cla(handles.axes7)
    newdatavals=BBA_GUIDATA.(planename).(quadname).newdatavals;
    datavals=BBA_GUIDATA.(planename).(quadname).datavals;
    newdatavals_err=BBA_GUIDATA.(planename).(quadname).newdatavals_err;
    datavals_err=BBA_GUIDATA.(planename).(quadname).datavals_err;
    centre = eval(['BBA_GUIDATA.' planename '.' quadname '.centre']);
    c_err = eval(['BBA_GUIDATA.' planename '.' quadname '.c_err']);
    hold(handles.axes8,'on')
    errorbar(handles.axes8,datavals(1,:)*1e3,datavals(2,:)*1e3,datavals_err(2,:)*1e3,'bx')
    errorbar(handles.axes8,newdatavals(1,:)*1e3,newdatavals(2,:)*1e3,newdatavals_err(2,:)*1e3,'rx')
    qa = eval(['BBA_GUIDATA.' planename '.' quadname '.qa']);
    qa_new = eval(['BBA_GUIDATA.' planename '.' quadname '.qa_new']);
    plot(handles.axes8,xlim(handles.axes8),xlim(handles.axes8)*qa(2)+qa(1),'b')
    plot(handles.axes8,xlim(handles.axes8),xlim(handles.axes8)*qa_new(2)+qa_new(1),'r')
    plot(handles.axes8,centre(1)*ones(2,1),ylim(handles.axes8),'k')
    plot(handles.axes8,(centre(1)+c_err(1))*ones(2,1),ylim(handles.axes8),'r')
    plot(handles.axes8,(centre(1)-c_err(1))*ones(2,1),ylim(handles.axes8),'r')
    if size(datavals,1)>=3
      hold(handles.axes6,'on')
      errorbar(handles.axes6,datavals(1,:)*1e3,datavals(3,:)*1e3,datavals_err(3,:)*1e3,'bx')
      errorbar(handles.axes6,newdatavals(1,:)*1e3,newdatavals(3,:)*1e3,newdatavals_err(3,:)*1e3,'rx')
      qb = eval(['BBA_GUIDATA.' planename '.' quadname '.qb']);
      qb_new = eval(['BBA_GUIDATA.' planename '.' quadname '.qb_new']);
      plot(handles.axes6,xlim(handles.axes6),xlim(handles.axes6)*qb(2)+qb(1),'b')
      plot(handles.axes6,xlim(handles.axes6),xlim(handles.axes6)*qb_new(2)+qb_new(1),'r')
      plot(handles.axes6,centre(2)*ones(2,1),ylim(handles.axes6),'k')
      plot(handles.axes6,(centre(2)+c_err(2))*ones(2,1),ylim(handles.axes6),'r')
      plot(handles.axes6,(centre(2)-c_err(2))*ones(2,1),ylim(handles.axes6),'r')
    else
      cla(handles.axes6)
      xlabel(handles.axes6,'')
      ylabel(handles.axes6,'')
    end
    if size(datavals,1)>=4
      hold(handles.axes5,'on')
      errorbar(handles.axes5,datavals(1,:)*1e3,datavals(4,:)*1e3,datavals_err(4,:)*1e3,'bx')
      errorbar(handles.axes5,newdatavals(1,:)*1e3,newdatavals(4,:)*1e3,newdatavals_err(4,:)*1e3,'rx')
      qc = eval(['BBA_GUIDATA.' planename '.' quadname '.qc']);
      qc_new = eval(['BBA_GUIDATA.' planename '.' quadname '.qc_new']);
      plot(handles.axes5,xlim(handles.axes5),xlim(handles.axes5)*qc(2)+qc(1),'b')
      plot(handles.axes5,xlim(handles.axes5),xlim(handles.axes5)*qc_new(2)+qc_new(1),'r')
      plot(handles.axes5,centre(3)*ones(2,1),ylim(handles.axes5),'k')
      plot(handles.axes5,(centre(3)+c_err(3))*ones(2,1),ylim(handles.axes5),'r')
      plot(handles.axes5,(centre(3)-c_err(3))*ones(2,1),ylim(handles.axes5),'r')
    else
      cla(handles.axes5)
      xlabel(handles.axes5,'')
      ylabel(handles.axes5,'')
    end
    if size(datavals,1)>=5
      hold(handles.axes7,'on')
      errorbar(handles.axes7,datavals(1,:)*1e3,datavals(5,:)*1e3,datavals_err(5,:)*1e3,'bx')
      errorbar(handles.axes7,newdatavals(1,:)*1e3,newdatavals(5,:)*1e3,newdatavals_err(5,:)*1e3,'rx')
      qd = eval(['BBA_GUIDATA.' planename '.' quadname '.qd']);
      qd_new = eval(['BBA_GUIDATA.' planename '.' quadname '.qd_new']);
      plot(handles.axes7,xlim(handles.axes7),xlim(handles.axes7)*qd(2)+qd(1),'b')
      plot(handles.axes7,xlim(handles.axes7),xlim(handles.axes7)*qd_new(2)+qd_new(1),'r')
      plot(handles.axes7,centre(4)*ones(2,1),ylim(handles.axes7),'k')
      plot(handles.axes7,(centre(4)+c_err(4))*ones(2,1),ylim(handles.axes7),'r')
      plot(handles.axes7,(centre(4)-c_err(4))*ones(2,1),ylim(handles.axes7),'r')
    else
      cla(handles.axes7)
      xlabel(handles.axes7,'')
      ylabel(handles.axes7,'')
    end
    real_centre = eval(['BBA_GUIDATA.' planename '.' quadname '.real_centre']);
    real_centre_err = eval(['BBA_GUIDATA.' planename '.' quadname '.real_centre_err']);
    if abs(real_centre)>0.1
      txtstr = sprintf('Centre = %1.3f +- %1.3f mm',real_centre,real_centre_err);
    else
      txtstr = sprintf('Centre = %1.3f +- %1.3f um',real_centre*1e3,real_centre_err*1e3);
    end
    set(handles.text6,'String',txtstr);
    centre = eval(['BBA_GUIDATA.' planename '.' quadname '.centre']); %#ok<NASGU>
    c_err = eval(['BBA_GUIDATA.' planename '.' quadname '.c_err']); %#ok<NASGU>
  else
    cla(handles.axes8)
    cla(handles.axes6)
    cla(handles.axes5)
    cla(handles.axes7)
    set(handles.text6,'String','Ready...')
  end
  
elseif strcmp(BBA_GUIDATA.devtype,'sext') && quadind>ringext
  for kids = get(handles.uipanel3,'Children')
    delete(kids);
  end
  reset(handles.uipanel3)
  vplotnum = ceil(sqrt(length(bpminds)));
  hplotnum = round(sqrt(length(bpminds)));
  if isfield(BBA_GUIDATA,planename) && eval(['isfield(BBA_GUIDATA.' planename ',quadname)'])
    newx = eval(['BBA_GUIDATA.' planename '.' quadname '.newx']);
    newdata = BBA_GUIDATA.(planename).(quadname).newdata;
    newdata_err = BBA_GUIDATA.(planename).(quadname).newdata_err;
    q = eval(['BBA_GUIDATA.' planename '.' quadname '.q']);
    dq = eval(['BBA_GUIDATA.' planename '.' quadname '.dq']); %#ok<NASGU>
    t_point = eval(['BBA_GUIDATA.' planename '.' quadname '.t_point']);
    t_point_err = eval(['BBA_GUIDATA.' planename '.' quadname '.t_point_err']);
    real_centre = eval(['BBA_GUIDATA.' planename '.' quadname '.real_centre']);
    real_centre_err = eval(['BBA_GUIDATA.' planename '.' quadname '.real_centre_err']);
    for bpmnum=1:length(bpminds)
      interpedx = linspace(newx(1),newx(end),100);
      fittedx = interpedx.*interpedx*q(3,bpmnum) + interpedx*q(2,bpmnum) + q(1,bpmnum);
      phand = subplot(vplotnum,hplotnum,bpmnum,'Parent',handles.uipanel3);
      errorbar(phand,newx,newdata(bpmnum,:),newdata_err(bpmnum,:),'x')
      hold(phand,'on')
      plot(phand,interpedx,fittedx,'r',t_point(bpmnum)*[1 1],ylim(phand),'b',...
        (t_point(bpmnum)*[1 1])-t_point_err(bpmnum),ylim(phand),'r',...
        (t_point(bpmnum)*[1 1])+t_point_err(bpmnum),ylim(phand),'r')
      axis(phand,'tight')
    end
    if abs(real_centre)>0.1
      txtstr = sprintf('Centre = %1.3f +- %1.3f mm',real_centre,real_centre_err);
    else
      txtstr = sprintf('Centre = %1.3f +- %1.3f um',real_centre*1e3,real_centre_err*1e3);
    end
    set(handles.text6,'String',txtstr);
  end
  for bpmnum=1:length(bpminds)
    temp_hand = subplot(vplotnum,hplotnum,bpmnum,'Parent',handles.uipanel3);
    eval(['handles.axes' num2str(bpmnum) ' = temp_hand;']);
    xlabel(temp_hand,'Mover / mm')
    ylabel(temp_hand,[BEAMLINE{bpminds(bpmnum)}.Name ' / mm'])
  end
end

BBA_GUIDATA.running = false;

% --- Executes during object creation, after setting all properties.
function listbox2_CreateFcn(hObject, eventdata, handles) %#ok<*INUSD>
% hObject    handle to listbox2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: listbox controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
  set(hObject,'BackgroundColor','white');
end

function edit1_Callback(hObject, eventdata, handles)
% hObject    handle to edit1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit1 as text
%        str2double(get(hObject,'String')) returns contents of edit1 as a double

global BBA_GUIDATA

BBA_GUIDATA.scanrange = [-1 1] * str2double(get(handles.edit1,'String'));

% --- Executes during object creation, after setting all properties.
function edit1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
  set(hObject,'BackgroundColor','white');
end

% --- Executes on button press in pushbutton2.
function pushbutton2_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

global BEAMLINE BBA_GUIDATA INSTR PS GIRDER FL
persistent pathdefined

if strcmp(BBA_GUIDATA.devtype,'sext') && ~strcmp(get(handles.pushbutton2,'String'),'CLICK TO RUN')
  disp('in here')
  FL.sextBBA_dostop=true;
  sextBBA('dostop');
  set(handles.pushbutton2,'BackgroundColor',[0 1 0])
  set(handles.pushbutton2,'String','CLICK TO RUN')
  BBA_GUIDATA.running = false;
  return
end
% Get SVD data
BBA_GUIDATA.JS.bpmToolINSTR='global';
BBA_GUIDATA.JS.doWait=true;
BBA_GUIDATA.JS.Nsvd=str2double(get(handles.edit6,'String'));
BBA_GUIDATA.JS.Nmodes=str2double(get(handles.edit7,'String'));
if ~isfield(BBA_GUIDATA,'running') || BBA_GUIDATA.running == false && get(handles.checkbox1,'Value')
  set(handles.text6,'String','Getting SVD data...')
  drawnow('expose');
  BBA_GUIDATA.JS.getModes;
end


FlHwUpdate();
bpmindschanged = false;


BBA_GUIDATA.scantype = 'mover';

if strcmp(BBA_GUIDATA.devtype,'sext')
  pushbutton2_sext_Callback(handles.pushbutton2, eventdata, handles);
  
  BBA_GUIDATA.unsaveddata = true;
  
  return
end

if ~isfield(BBA_GUIDATA,'running')
  BBA_GUIDATA.running = false;
end
if BBA_GUIDATA.running==true
  return
elseif BBA_GUIDATA.running==false
  BBA_GUIDATA.running=true;
end

set(handles.text6,'String','Running...')
set(handles.pushbutton2,'BackgroundColor',[1 0 0])
set(handles.pushbutton2,'String','RUNNING...')

if isempty(pathdefined)
  if ~isdeployed; addpath('testApps/bumpgui'); end;
  pathdefined = true;
end

quads = get(handles.listbox2,'String');
if isempty(quads)
  msgbox(['Please select a device to scan by clicking the "Device Type" button' ...
    ',selecting "QUADs" or "SEXTs", and then selecting a device from the list.']);
  BBA_GUIDATA.running = false;
  return
end
quadoi = quads(get(handles.listbox2,'Value'));
quadindoi = max(findcells(BEAMLINE,'Name',quadoi{1}));
% ind = find(strcmpi(quadoi,BBA_GUIDATA.quadbpminfo.textdata));
% ringext = findcells(BEAMLINE,'Name','IEX');
% if (strcmp(BBA_GUIDATA.devtype,'quad')) && isempty(ind)
%     msghand = end_with_error...
%         (handles, 'This device has no suitable BPM nearby.  Please select another.');
%     uiwait(msghand,5)
%     if ishandle(msghand); delete(msghand); end
%     return
% end
if isfield(BBA_GUIDATA,quadoi{1})
  BBA_GUIDATA = rmfield(BBA_GUIDATA,quadoi{1});
end
% mainbpmind = BBA_GUIDATA.quadbpminfo.data(ind); %#ok<FNDSB>
mainbpmind = findcells(BEAMLINE,'Name',['M' quadoi{1}]);
temp_bpminds = findcells(BEAMLINE,'Class','MONI');
bpminds = [];
for ind=1:length(temp_bpminds)
  instr_ind = findcells(INSTR, 'Index', temp_bpminds(ind));
  if ~strcmp(INSTR{instr_ind}.Class,'NOHW')
    bpminds = [bpminds temp_bpminds(ind)];
  end
end
if isempty(mainbpmind)
  dlgstr = cell(1,length(bpminds));
  for num = 1:length(bpminds)
    dlgstr{num} = BEAMLINE{bpminds(num)}.Name;
  end
  [selection ok] = listdlg('ListString',dlgstr,'PromptString','Please choose a BPM:',...
    'SelectionMode','single','ListSize',[200 600],...
    'Name','No associated BPM');
  if ~ok
    end_with_error(handles, 'No BPM chosen.');
  end
  mainbpmind = findcells(BEAMLINE,'Name',dlgstr{selection});
end


if get(handles.radiobutton1,'Value')
  axesplane = 'x';
  plane = 1;
else
  axesplane = 'y';
  plane = 3;
end

bpmsoi = bpmchoose(quadindoi,axesplane) ;
if isempty(bpmsoi)
  bpminds = bpminds(bpminds>quadindoi);
  if length(bpminds)>=5
    bpmsoi = ...
      {BEAMLINE{bpminds(2)}.Name,...
      BEAMLINE{bpminds(3)}.Name,...
      BEAMLINE{bpminds(4)}.Name,...
      BEAMLINE{bpminds(5)}.Name};
  else
    bpmsoi = cell(1,length(bpminds)-1);
    for ind=1:(length(bpminds)-1)
      bpmsoi{ind} = BEAMLINE{bpminds(ind+1)}.Name;
    end
  end
else
  bpmsoi=bpmsoi(2:end);
end
bpmindsoi = zeros(1,length(bpmsoi));
for ind=1:length(bpmsoi)
  bpmindsoi(ind) = findcells(BEAMLINE,'Name',bpmsoi{ind});
end

if isfield(BBA_GUIDATA,axesplane) && eval(['isfield(BBA_GUIDATA.' axesplane ',quadoi{1})'])
  eval(['BBA_GUIDATA.' axesplane ' = rmfield(BBA_GUIDATA.' axesplane ',quadoi{1});'])
end

if strcmp(BBA_GUIDATA.scantype,'bumps')
  if plane==1
    corinds = findcells(BEAMLINE,'Class','XCOR');
    firstcors = corinds(corinds<quadindoi);
    lastcors = corinds(corinds>max(bpmindsoi));
    if isempty(firstcors)
      msghand = end_with_error...
        (handles, ['Not enough correctors upstream of this quad to ' ...
        'open the bump.  Try using a mover.']);
      uiwait(msghand,5)
      if ishandle(msghand); delete(msghand); end
      return
    end
    while length(lastcors)<2
      bpmindschanged = true;
      if length(bpmindsoi)==1
        msghand = end_with_error...
          (handles, 'Not enough correctors to close the bump');
        uiwait(msghand,5)
        if ishandle(msghand); delete(msghand); end
        return
      end
      bpmindsoi = bpmindsoi(1:end-1);
      lastcors = corinds(corinds>max(bpmindsoi));
    end
    bumpknob = bumpgui(1,4,quadindoi,[firstcors(end-1:end) lastcors(1:2)]);
  elseif plane==3
    corinds = findcells(BEAMLINE,'Class','YCOR');
    firstcors = corinds(corinds<quadindoi);
    lastcors = corinds(corinds>max(bpmindsoi));
    while length(lastcors)<2
      bpmindschanged = true;
      if length(bpmindsoi)==1
        msghand = end_with_error...
          (handles, 'Not enough correctors to close the bump');
        uiwait(msghand,5)
        if ishandle(msghand); delete(msghand); end
        return
      end
      bpmindsoi = bpmindsoi(1:end-1);
      lastcors = corinds(corinds>max(bpmindsoi));
    end
    bumpknob = bumpgui(3,4,quadindoi,[firstcors(end-1:end) lastcors(1:2)]);
  end
  BBA_GUIDATA.bumpknob = bumpknob;
elseif strcmp(BBA_GUIDATA.scantype,'mover')
  if BEAMLINE{quadindoi}.Girder==0 || ~isfield(GIRDER{BEAMLINE{quadindoi}.Girder},'Mover')
    BBA_GUIDATA.scantype = 'bumps';
    if plane==1
      corinds = findcells(BEAMLINE,'Class','XCOR');
      firstcors = corinds(corinds<quadindoi);
      lastcors = corinds(corinds>max(bpmindsoi));
      while length(lastcors)<2
        bpmindschanged = true;
        if length(bpmindsoi)==1
          msghand = end_with_error...
            (handles, 'Not enough correctors to close the bump');
          uiwait(msghand,5)
          if ishandle(msghand); delete(msghand); end
          return
        end
        bpmindsoi = bpmindsoi(1:end-1);
        lastcors = corinds(corinds>max(bpmindsoi));
      end
      if length(firstcors)<2
        msghand = end_with_error...
          (handles, 'Not enough correctors to open the bump');
        uiwait(msghand,5)
        if ishandle(msghand); delete(msghand); end
        return
      end
      bumpknob = bumpgui(1,4,quadindoi,[firstcors(end-1:end) lastcors(1:2)]);
    elseif plane==3
      corinds = findcells(BEAMLINE,'Class','YCOR');
      firstcors = corinds(corinds<quadindoi);
      lastcors = corinds(corinds>max(bpmindsoi));
      while length(lastcors)<2
        bpmindschanged = true;
        if length(bpmindsoi)==1
          msghand = end_with_error...
            (handles, 'Not enough correctors to close the bump');
          uiwait(msghand,5)
          if ishandle(msghand); delete(msghand); end
          return
        end
        bpmindsoi = bpmindsoi(1:end-1);
        lastcors = corinds(corinds>max(bpmindsoi));
      end
      bumpknob = bumpgui(3,4,quadindoi,[firstcors(end-1:end) lastcors(1:2)]);
    end
    BBA_GUIDATA.bumpknob = bumpknob;
  end
end

if bpmindschanged
  clear bpmsoi
  for bpmnum=1:length(bpmindsoi)
    bpmsoi{bpmnum} = BEAMLINE{bpmindsoi(bpmnum)}.Name;
  end
end

if strcmp(BBA_GUIDATA.scantype,'bumps')
  psoi = zeros(1,length(bumpknob.Channel) + 1);
  for count=1:length(bumpknob.Channel)
    psoi(count) = bumpknob.Channel(count).Unit;
  end
  psoi(end) = BEAMLINE{quadindoi}.PS;
  mvoi = {};
else
  psoi = BEAMLINE{quadindoi}.PS;
  mvoi = BEAMLINE{quadindoi}.Girder;
end

[stat] = AccessRequest({mvoi,psoi,[]});
if stat{1}~=1
  msghand = end_with_error(handles,['AccessRequest denied: ' stat{2}]);
  uiwait(msghand,5)
  if ishandle(msghand); delete(msghand); end
  return
end

try
  FlHwUpdate;
catch %#ok<CTCH>
  msghand = end_with_error(handles,'FlHwUpdate failed');
  uiwait(msghand,5)
  if ishandle(msghand); delete(msghand); end
  return;
end
mainbpm_instrind = findcells(INSTR,'Index',mainbpmind);
bpm_instrind = zeros(1,length(bpmindsoi));
for count=1:length(bpmindsoi)
  bpm_instrind(count) = findcells(INSTR,'Index',bpmindsoi(count));
end

handles.axes8 = subplot(2,2,1,'Parent',handles.uipanel3);
handles.axes6 = subplot(2,2,2,'Parent',handles.uipanel3);
handles.axes5 = subplot(2,2,3,'Parent',handles.uipanel3);
handles.axes7 = subplot(2,2,4,'Parent',handles.uipanel3);

cla(handles.axes8)
cla(handles.axes6)
cla(handles.axes5)
cla(handles.axes7)

xlabel(handles.axes8,[BEAMLINE{mainbpmind}.Name ' ' axesplane ' / mm'])
ylabel(handles.axes8,[bpmsoi{1} ' ' axesplane ' / mm'])
if length(bpmsoi)>=2
  xlabel(handles.axes6,[BEAMLINE{mainbpmind}.Name ' ' axesplane ' / mm'])
  ylabel(handles.axes6,[bpmsoi{2} ' ' axesplane ' / mm'])
else
  cla(handles.axes6)
  xlabel(handles.axes6,'')
  ylabel(handles.axes6,'')
end
if length(bpmsoi)>=3
  xlabel(handles.axes5,[BEAMLINE{mainbpmind}.Name ' ' axesplane ' / mm'])
  ylabel(handles.axes5,[bpmsoi{3} ' ' axesplane ' / mm'])
else
  cla(handles.axes5)
  xlabel(handles.axes5,'')
  ylabel(handles.axes5,'')
end
if length(bpmsoi)>=4
  xlabel(handles.axes7,[BEAMLINE{mainbpmind}.Name ' ' axesplane ' / mm'])
  ylabel(handles.axes7,[bpmsoi{4} ' ' axesplane ' / mm'])
else
  cla(handles.axes7)
  xlabel(handles.axes7,'')
  ylabel(handles.axes7,'')
end

if strcmp(BBA_GUIDATA.scantype,'bumps')
  scanrange = BBA_GUIDATA.scanrange;
else
  scanrange = BBA_GUIDATA.scanrange;
end
numaves = BBA_GUIDATA.aves;
numcorsettings = BBA_GUIDATA.pps;
if numcorsettings<2
  msghand = end_with_error...
    (handles, 'Need 2 or more points per scan for quadrupole calculations.');
  uiwait(msghand,5)
  if ishandle(msghand); delete(msghand); end
  return
end

if strcmp(BBA_GUIDATA.scantype,'mover')
  dofind = find(GIRDER{BEAMLINE{quadindoi}.Girder}.Mover==plane); %#ok<NASGU>
  
end

datavals = zeros(1+length(bpmindsoi),length(linspace(scanrange(1),scanrange(2),numcorsettings)));
counter=0;
for bumpsetting=1e-3*linspace(scanrange(1),scanrange(2),numcorsettings)
  counter = counter+1;
  if isfield(handles,'text6')
    set(handles.text6,'String',['Setting ' BBA_GUIDATA.scantype ' to ' ...
      sprintf('%1.3f',bumpsetting*1e3) ' mm'])
  else
    BBA_GUIDATA.running = false;
    return
  end
  if strcmp(BBA_GUIDATA.scantype,'bumps')
    stat = SetMultiKnob('bumpknob',bumpsetting,true);
  else
    dofind = find(GIRDER{BEAMLINE{quadindoi}.Girder}.Mover==plane);
    if counter==1
      orig_moversetting = GIRDER{BEAMLINE{quadindoi}.Girder}.MoverPos(dofind);
      GIRDER{BEAMLINE{quadindoi}.Girder}.MoverSetPt = GIRDER{BEAMLINE{quadindoi}.Girder}.MoverPos;
    end
    GIRDER{BEAMLINE{quadindoi}.Girder}.MoverSetPt(dofind) = orig_moversetting + bumpsetting;
    stat=MoverTrim(BEAMLINE{quadindoi}.Girder, 3); if stat{1}~=1; errorRep(['Mover Error: ',stat{2}]); end;
  end
  if stat{1}~=1; disp(stat{2}); end
  if isfield(handles,'text6')
    set(handles.text6,'String',[sprintf('%1.3f',bumpsetting*1e3) ' mm : Gathering data']);
  else
    BBA_GUIDATA.running = false;
    return
  end
  if get(handles.radiobutton1,'Value')
    axplane='x';
  else
    axplane='y';
  end
  if get(handles.checkbox1,'Value')
    [bpmdata bpmdata_err]=getBpmValsSVD(numaves,[mainbpm_instrind; bpm_instrind'],axplane);
  else
    [bpmdata bpmdata_err]=getBpmVals(numaves,[mainbpm_instrind; bpm_instrind'],axplane);
  end
  bpmdata(isnan(bpmdata))=0; bpmdata_err(isnan(bpmdata_err))=0;
  datavals(:,counter)=bpmdata;
  datavals_err(:,counter)=bpmdata_err;
  if isfield(handles,'axes8')
    hold(handles.axes8,'on')
    errorbar(handles.axes8,datavals(1,counter)*1e3,datavals(2,counter)*1e3,datavals_err(2,counter)*1e3,'bx')
    axis(handles.axes8,'auto')
  else
    BBA_GUIDATA.running = false;
    return
  end
  if isfield(handles,'axes6')
    if length(bpmsoi)>=2
      hold(handles.axes6,'on')
      errorbar(handles.axes6,datavals(1,counter)*1e3,datavals(3,counter)*1e3,datavals_err(3,counter)*1e3,'bx')
      axis(handles.axes6,'auto')
    else
      cla(handles.axes6)
      xlabel(handles.axes6,'')
      ylabel(handles.axes6,'')
    end
  else
    BBA_GUIDATA.running = false;
    return
  end
  if isfield(handles,'axes5')
    if length(bpmsoi)>=3
      hold(handles.axes5,'on')
      errorbar(handles.axes5,datavals(1,counter)*1e3,datavals(4,counter)*1e3,datavals_err(4,counter)*1e3,'bx')
      axis(handles.axes5,'auto')
    else
      cla(handles.axes5)
      xlabel(handles.axes5,'')
      ylabel(handles.axes5,'')
    end
  else
    BBA_GUIDATA.running = false;
    return
  end
  if isfield(handles,'axes7')
    if length(bpmsoi)>=4
      hold(handles.axes7,'on')
      errorbar(handles.axes7,datavals(1,counter)*1e3,datavals(5,counter)*1e3,datavals_err(5,counter)*1e3,'bx')
      axis(handles.axes7,'auto')
    else
      cla(handles.axes7)
      xlabel(handles.axes7,'')
      ylabel(handles.axes7,'')
    end
  else
    BBA_GUIDATA.running = false;
    return
  end
end

if isfield(handles,'text6')
  set(handles.text6,'String','Fitting...')
else
  BBA_GUIDATA.running = false;
  return
end

BBA_GUIDATA.(axesplane).(quadoi{1}).datavals=datavals;
BBA_GUIDATA.(axesplane).(quadoi{1}).datavals_err=datavals_err;

[qa dqa] = ...
  noplot_polyfit(datavals(1,:)*1e3,datavals(2,:)*1e3,datavals_err(2,:)*1e3,1);
eval(['BBA_GUIDATA.' axesplane '.' quadoi{1} '.qa = qa;'])
eval(['BBA_GUIDATA.' axesplane '.' quadoi{1} '.dqa = dqa;'])
plot(handles.axes8,xlim(handles.axes8),xlim(handles.axes8)*qa(2)+qa(1),'b')
if length(bpmsoi)>=2
  [qb dqb] = ...
    noplot_polyfit(datavals(1,:)*1e3,datavals(3,:)*1e3,datavals_err(3,:)*1e3,1);
  eval(['BBA_GUIDATA.' axesplane '.' quadoi{1} '.qb = qb;'])
  eval(['BBA_GUIDATA.' axesplane '.' quadoi{1} '.dqb = dqb;'])
  plot(handles.axes6,xlim(handles.axes6),xlim(handles.axes6)*qb(2)+qb(1),'b')
end
if length(bpmsoi)>=3
  [qc dqc] = ...
    noplot_polyfit(datavals(1,:)*1e3,datavals(4,:)*1e3,datavals_err(4,:)*1e3,1);
  eval(['BBA_GUIDATA.' axesplane '.' quadoi{1} '.qc = qc;'])
  eval(['BBA_GUIDATA.' axesplane '.' quadoi{1} '.dqc = dqc;'])
  plot(handles.axes5,xlim(handles.axes5),xlim(handles.axes5)*qc(2)+qc(1),'b')
end
if length(bpmsoi)>=4
  [qd dqd] = ...
    noplot_polyfit(datavals(1,:)*1e3,datavals(5,:)*1e3,datavals_err(5,:)*1e3,1);
  eval(['BBA_GUIDATA.' axesplane '.' quadoi{1} '.qd = qd;'])
  eval(['BBA_GUIDATA.' axesplane '.' quadoi{1} '.dqd = dqd;'])
  plot(handles.axes7,xlim(handles.axes7),xlim(handles.axes7)*qd(2)+qd(1),'b')
end

if isfield(handles,'text6')
  set(handles.text6,'String',['Putting ' BBA_GUIDATA.scantype ' back to original setting'])
else
  BBA_GUIDATA.running = false;
  return
end
if strcmp(BBA_GUIDATA.scantype,'bumps')
  stat = SetMultiKnob('bumpknob',0,true);
else
  dofind = find(GIRDER{BEAMLINE{quadindoi}.Girder}.Mover==plane);
  GIRDER{BEAMLINE{quadindoi}.Girder}.MoverSetPt(dofind) = orig_moversetting;
  stat=MoverTrim(BEAMLINE{quadindoi}.Girder, 3);if stat{1}~=1; errorRep(['Mover Error: ',stat{2}]); end;
end
if stat{1}~=1; disp(stat{2}); end

if isfield(handles,'text6')
  set(handles.text6,'String',['Setting quad to ' num2str(BBA_GUIDATA.shuntval) '% strength']);
else
  BBA_GUIDATA.running = false;
  return
end
quadcurval = PS(BEAMLINE{quadindoi}.PS).Ampl;
BBA_GUIDATA.quadcurval = quadcurval;
BBA_GUIDATA.quadpsnum = BEAMLINE{quadindoi}.PS;
PS(BEAMLINE{quadindoi}.PS).SetPt = quadcurval*BBA_GUIDATA.shuntval*0.01;
PSTrim(BEAMLINE{quadindoi}.PS,true);

if strcmp(BBA_GUIDATA.scantype,'bumps')
  if get(handles.radiobutton1,'Value')
    bumpknob = bumpgui(1,4,quadindoi,[firstcors(end-1:end) lastcors(1:2)]);
  else
    bumpknob = bumpgui(3,4,quadindoi,[firstcors(end-1:end) lastcors(1:2)]);
  end
  BBA_GUIDATA.bumpknob = bumpknob;
elseif strcmp(BBA_GUIDATA.scantype,'mover')
  if ~isfield(GIRDER{BEAMLINE{quadindoi}.Girder},'Mover')
    if get(handles.radiobutton1,'Value')
      bumpknob = bumpgui(1,4,quadindoi,[firstcors(end-1:end) lastcors(1:2)]); %#ok<NASGU>
    else
      bumpknob = bumpgui(3,4,quadindoi,[firstcors(end-1:end) lastcors(1:2)]); %#ok<NASGU>
    end
  end
end

newdatavals = zeros(1+length(bpmindsoi),length(linspace(scanrange(1),scanrange(2),numcorsettings)));
counter=0;

FlHwUpdate;

for bumpsetting=1e-3*linspace(scanrange(1),scanrange(2),numcorsettings)
  counter = counter+1;
  if isfield(handles,'text6')
    set(handles.text6,'String',['Setting ' BBA_GUIDATA.scantype ' to ' ...
      sprintf('%1.3f',bumpsetting*1e3) ' mm'])
  else
    BBA_GUIDATA.running = false;
    return
  end
  if strcmp(BBA_GUIDATA.scantype,'bumps')
    stat = SetMultiKnob('bumpknob',bumpsetting,true);
  else
    dofind = find(GIRDER{BEAMLINE{quadindoi}.Girder}.Mover==plane);
    GIRDER{BEAMLINE{quadindoi}.Girder}.MoverSetPt(dofind) = orig_moversetting + bumpsetting;
    stat=MoverTrim(BEAMLINE{quadindoi}.Girder, 3);if stat{1}~=1; errorRep(['Mover Error: ',stat{2}]); end;
  end
  if stat{1}~=1; disp(stat{2}); end
  if isfield(handles,'text6')
    set(handles.text6,'String',[sprintf('%1.3f',bumpsetting*1e3) ' mm : Gathering data']);
  else
    BBA_GUIDATA.running = false;
    return
  end
  if get(handles.checkbox1,'Value')
    [bpmdata bpmdata_err]=getBpmValsSVD(numaves,[mainbpm_instrind; bpm_instrind'],axplane);
  else
    [bpmdata bpmdata_err]=getBpmVals(numaves,[mainbpm_instrind; bpm_instrind'],axplane);
  end
  bpmdata(isnan(bpmdata))=0; bpmdata_err(isnan(bpmdata_err))=0;
  newdatavals(:,counter)=bpmdata;
  newdatavals_err(:,counter)=bpmdata_err;
  if isfield(handles,'axes8')
    errorbar(handles.axes8,newdatavals(1,counter)*1e3,newdatavals(2,counter)*1e3,newdatavals_err(2,counter)*1e3,'rx')
    hold(handles.axes8,'on')
  else
    BBA_GUIDATA.running = false;
    return
  end
  if isfield(handles,'axes6')
    if length(bpmsoi)>=2
      errorbar(handles.axes6,newdatavals(1,counter)*1e3,newdatavals(3,counter)*1e3,newdatavals_err(3,counter)*1e3,'rx')
      hold(handles.axes6,'on')
    else
      cla(handles.axes6)
      xlabel(handles.axes6,'')
      ylabel(handles.axes6,'')
    end
  else
    BBA_GUIDATA.running = false;
    return
  end
  if isfield(handles,'axes5')
    if length(bpmsoi)>=3
      errorbar(handles.axes5,newdatavals(1,counter)*1e3,newdatavals(4,counter)*1e3,newdatavals_err(4,counter)*1e3,'rx')
      hold(handles.axes5,'on')
    else
      cla(handles.axes5)
      xlabel(handles.axes5,'')
      ylabel(handles.axes5,'')
    end
  else
    BBA_GUIDATA.running = false;
    return
  end
  if isfield(handles,'axes7')
    if length(bpmsoi)>=4
      errorbar(handles.axes7,newdatavals(1,counter)*1e3,newdatavals(5,counter)*1e3,newdatavals_err(5,counter)*1e3,'rx')
      hold(handles.axes7,'on')
    else
      cla(handles.axes7)
      xlabel(handles.axes7,'')
      ylabel(handles.axes7,'')
    end
  else
    BBA_GUIDATA.running = false;
    return
  end
end

if isfield(handles,'text6')
  set(handles.text6,'String',['Putting ' BBA_GUIDATA.scantype ' back to original setting'])
else
  BBA_GUIDATA.running = false;
  return
end
if strcmp(BBA_GUIDATA.scantype,'bumps')
  stat = SetMultiKnob('bumpknob',0,true);
else
  dofind = find(GIRDER{BEAMLINE{quadindoi}.Girder}.Mover==plane);
  GIRDER{BEAMLINE{quadindoi}.Girder}.MoverSetPt(dofind) = orig_moversetting;
  stat=MoverTrim(BEAMLINE{quadindoi}.Girder, 3);if stat{1}~=1; errorRep(['Mover Error: ',stat{2}]); end;
end
if stat{1}~=1; disp(stat{2}); end

if isfield(handles,'text6')
  set(handles.text6,'String','Setting quad back to 100%')
else
  BBA_GUIDATA.running = false;
  return
end
PS(BEAMLINE{quadindoi}.PS).SetPt = quadcurval;
PSTrim(BEAMLINE{quadindoi}.PS,true);

AccessRequest('release');

if isfield(handles,'text6')
  set(handles.text6,'String','Fitting...')
else
  BBA_GUIDATA.running = false;
  return
end

BBA_GUIDATA.(axesplane).(quadoi{1}).newdatavals=newdatavals;
BBA_GUIDATA.(axesplane).(quadoi{1}).newdatavals_err=newdatavals_err;

[qa_new dqa_new] = ...
  noplot_polyfit(newdatavals(1,:)*1e3,newdatavals(2,:)*1e3,newdatavals_err(2,:)*1e3,1);
eval(['BBA_GUIDATA.' axesplane '.' quadoi{1} '.qa_new = qa_new;'])
eval(['BBA_GUIDATA.' axesplane '.' quadoi{1} '.dqa_new = dqa_new;'])
plot(handles.axes8,xlim(handles.axes8),xlim(handles.axes8)*qa_new(2)+qa_new(1),'r')
if length(bpmsoi)>=2
  [qb_new dqb_new] = ...
    noplot_polyfit(newdatavals(1,:)*1e3,newdatavals(3,:)*1e3,newdatavals_err(3,:)*1e3,1);
  eval(['BBA_GUIDATA.' axesplane '.' quadoi{1} '.qb_new = qb_new;'])
  eval(['BBA_GUIDATA.' axesplane '.' quadoi{1} '.dqb_new = dqb_new;'])
  plot(handles.axes6,xlim(handles.axes6),xlim(handles.axes6)*qb_new(2)+qb_new(1),'r')
end
if length(bpmsoi)>=3
  [qc_new dqc_new] = ...
    noplot_polyfit(newdatavals(1,:)*1e3,newdatavals(4,:)*1e3,newdatavals_err(4,:)*1e3,1);
  eval(['BBA_GUIDATA.' axesplane '.' quadoi{1} '.qc_new = qc_new;'])
  eval(['BBA_GUIDATA.' axesplane '.' quadoi{1} '.dqc_new = dqc_new;'])
  plot(handles.axes5,xlim(handles.axes5),xlim(handles.axes5)*qc_new(2)+qc_new(1),'r')
end
if length(bpmsoi)>=4
  [qd_new dqd_new] = ...
    noplot_polyfit(newdatavals(1,:)*1e3,newdatavals(5,:)*1e3,newdatavals_err(5,:)*1e3,1);
  eval(['BBA_GUIDATA.' axesplane '.' quadoi{1} '.qd_new = qd_new;'])
  eval(['BBA_GUIDATA.' axesplane '.' quadoi{1} '.dqd_new = dqd_new;'])
  plot(handles.axes7,xlim(handles.axes7),xlim(handles.axes7)*qd_new(2)+qd_new(1),'r')
end

centre = (qa_new(1)-qa(1))/(qa(2)-qa_new(2));
if length(bpmsoi)>=2
  centre = [centre (qb_new(1)-qb(1))/(qb(2)-qb_new(2))];
end
if length(bpmsoi)>=3
  centre = [centre (qc_new(1)-qc(1))/(qc(2)-qc_new(2))];
end
if length(bpmsoi)>=4
  centre = [centre (qd_new(1)-qd(1))/(qd(2)-qd_new(2))];
end

eval(['BBA_GUIDATA.' axesplane '.' quadoi{1} '.centre = centre;'])

c_err = sqrt(((dqa_new(1)^2+dqa(1)^2)/((qa_new(1)-qa(1))^2) + ...
  (dqa(2)^2+dqa_new(2)^2)/((qa(2)-qa_new(2))^2)) * centre(1)^2);
if length(bpmsoi)>=2
  c_err = [c_err ...
    sqrt(((dqb_new(1)^2+dqb(1)^2)/((qb_new(1)-qb(1))^2) + ...
    (dqb(2)^2+dqb_new(2)^2)/((qb(2)-qb_new(2))^2)) * centre(2)^2)];
end
if length(bpmsoi)>=3
  c_err = [c_err ...
    sqrt(((dqc_new(1)^2+dqc(1)^2)/((qc_new(1)-qc(1))^2) + ...
    (dqc(2)^2+dqc_new(2)^2)/((qc(2)-qc_new(2))^2)) * centre(3)^2)];
end
if length(bpmsoi)>=4
  c_err = [c_err ...
    sqrt(((dqd_new(1)^2+dqd(1)^2)/((qd_new(1)-qd(1))^2) + ...
    (dqd(2)^2+dqd_new(2)^2)/((qd(2)-qd_new(2))^2)) * centre(4)^2)];
end

eval(['BBA_GUIDATA.' axesplane '.' quadoi{1} '.c_err = c_err;'])

plot(handles.axes8,centre(1)*ones(2,1),ylim(handles.axes8),'k')
plot(handles.axes8,(centre(1)+c_err(1))*ones(2,1),ylim(handles.axes8),'r')
plot(handles.axes8,(centre(1)-c_err(1))*ones(2,1),ylim(handles.axes8),'r')
if length(bpmsoi)>=2
  plot(handles.axes6,centre(2)*ones(2,1),ylim(handles.axes6),'k')
  plot(handles.axes6,(centre(2)+c_err(2))*ones(2,1),ylim(handles.axes6),'r')
  plot(handles.axes6,(centre(2)-c_err(2))*ones(2,1),ylim(handles.axes6),'r')
end
if length(bpmsoi)>=3
  plot(handles.axes5,centre(3)*ones(2,1),ylim(handles.axes5),'k')
  plot(handles.axes5,(centre(3)+c_err(3))*ones(2,1),ylim(handles.axes5),'r')
  plot(handles.axes5,(centre(3)-c_err(3))*ones(2,1),ylim(handles.axes5),'r')
end
if length(bpmsoi)>=4
  plot(handles.axes7,centre(4)*ones(2,1),ylim(handles.axes7),'k')
  plot(handles.axes7,(centre(4)+c_err(4))*ones(2,1),ylim(handles.axes7),'r')
  plot(handles.axes7,(centre(4)-c_err(4))*ones(2,1),ylim(handles.axes7),'r')
end

if length(bpmsoi)>=2 && size(datavals,2)>2
  [real_centre, real_centre_err] = noplot_polyfit(1:length(centre),centre,c_err,0);
elseif length(bpmsoi)>=2 && size(datavals,2)<=2
  [real_centre, real_centre_err] = noplot_polyfit(1:length(centre),centre,1,0);
else
  real_centre = centre;
  real_centre_err = c_err;
end

eval(['BBA_GUIDATA.' axesplane '.' quadoi{1} '.real_centre = real_centre;'])
eval(['BBA_GUIDATA.' axesplane '.' quadoi{1} '.real_centre_err = real_centre_err;'])

if isfield(handles,'text6')
  if abs(real_centre)>0.1
    txtstr = sprintf('Centre = %1.3f +- %1.3f mm',real_centre,real_centre_err);
  else
    txtstr = sprintf('Centre = %1.3f +- %1.3f um',real_centre*1e3,real_centre_err*1e3);
  end
  set(handles.text6,'String',txtstr);
else
  BBA_GUIDATA.running = false;
  return
end

set(handles.pushbutton2,'BackgroundColor',[0 1 0])
set(handles.pushbutton2,'String','CLICK TO RUN')
BBA_GUIDATA.running = false;
BBA_GUIDATA.unsaveddata = true;

% --- Executes during object deletion, before destroying properties.
function figure1_DeleteFcn(hObject, eventdata, handles)
% hObject    handle to figure1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

global BBA_GUIDATA PS

if isfield(BBA_GUIDATA,'quadpsnum') && isfield(BBA_GUIDATA,'quadcurval') && ...
    isfield(BBA_GUIDATA,'bumpknob')
  PS(BBA_GUIDATA.quadpsnum).SetPt = BBA_GUIDATA.quadcurval;
  PSTrim(BBA_GUIDATA.quadpsnum,true);
  bumpknob = BBA_GUIDATA.bumpknob;
  if bumpknob.Value~=0
    SetMultiKnob('bumpknob',0,true)
  end
end

clear global BBA_GUIDATA

AccessRequest('release');

function edit2_Callback(hObject, eventdata, handles)
% hObject    handle to edit2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit2 as text
%        str2double(get(hObject,'String')) returns contents of edit2 as a double

global BBA_GUIDATA
BBA_GUIDATA.pps = str2double(get(handles.edit2,'String'));

% --- Executes during object creation, after setting all properties.
function edit2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
  set(hObject,'BackgroundColor','white');
end

function edit4_Callback(hObject, eventdata, handles)
% hObject    handle to edit4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit4 as text
%        str2double(get(hObject,'String')) returns contents of edit4 as a double

global BBA_GUIDATA
BBA_GUIDATA.aves = str2double(get(handles.edit4,'String'));

% --- Executes during object creation, after setting all properties.
function edit4_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
  set(hObject,'BackgroundColor','white');
end

function edit5_Callback(hObject, eventdata, handles)
% hObject    handle to edit5 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit5 as text
%        str2double(get(hObject,'String')) returns contents of edit5 as a double

global BBA_GUIDATA

val = str2double(get(handles.edit5,'String'));
if isnan(val) || val<0 || val>100
  set(handles.edit5,'String',num2str(BBA_GUIDATA.shuntval));
else
  BBA_GUIDATA.shuntval = val;
end

% --- Executes during object creation, after setting all properties.
function edit5_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit5 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
  set(hObject,'BackgroundColor','white');
end

% --- Executes on button press in pushbutton3.
function pushbutton3_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

global BBA_GUIDATA

savetype = ...
  questdlg('Save locally or on server?','Save location','Locally','Server','Locally');

savetime = now; %#ok<NASGU>

if strcmpi(savetype,'locally')
  filename = uiputfile(...
    '~/Lucretia/src/Floodland/testApps/bpmquad_offset/data/*.mat',...
    'Save quad-centre data in file');
  if filename~=0
    save(['~/Lucretia/src/Floodland/testApps/bpmquad_offset/data/' filename],'BBA_GUIDATA')
  end
elseif strcmpi(savetype,'server')
  if isfield(BBA_GUIDATA,'x')
    x_data = BBA_GUIDATA.x;
    magnames = fieldnames(x_data);
    for datapoint=1:length(magnames)
      stat = eval(['FlDataStore(''BBA'',''' magnames{datapoint} '_x'',[savetime,' ...
        'x_data.' magnames{datapoint} '.real_centre,'...
        'x_data.' magnames{datapoint} '.real_centre_err])']);
      if stat{1}~=1; error(stat{2}); end
    end
  end
  
  if isfield(BBA_GUIDATA,'y')
    y_data = BBA_GUIDATA.y;
    magnames = fieldnames(y_data);
    for datapoint=1:length(magnames)
      eval(['stat = FlDataStore(''BBA'',''' magnames{datapoint} '_y'',[savetime,' ...
        'y_data.' magnames{datapoint} '.real_centre,'...
        'y_data.' magnames{datapoint} '.real_centre_err]);'])
      if stat{1}~=1; error(stat{2}); end
    end
  end
  
  if ~isfield(BBA_GUIDATA,'x') && ~isfield(BBA_GUIDATA,'y')
    error('No data to save to the server.')
  end
end

BBA_GUIDATA.unsaveddata = false;

% --- Executes on button press in pushbutton4.
function pushbutton4_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

global BBA_GUIDATA %#ok<NUSED>

[filename, pathname] = uigetfile(...
  '~/Lucretia/src/Floodland/testApps/bpmquad_offset/data/*.mat',...
  'Load data file'); %#ok<NASGU>

if filename~=0
  load(['~/Lucretia/src/Floodland/testApps/bpmquad_offset/data/' filename],'BBA_GUIDATA')
end

% --- Executes on button press in radiobutton3.
function radiobutton3_Callback(hObject, eventdata, handles)
% hObject    handle to radiobutton3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of radiobutton3

global BBA_GUIDATA

if get(handles.radiobutton3,'Value')
  BBA_GUIDATA.scantype = 'mover';
else
  BBA_GUIDATA.scantype = 'bumps';
  set(handles.radiobutton4,'Value',1);
end

% --- Executes on button press in radiobutton4.
function radiobutton4_Callback(hObject, eventdata, handles)
% hObject    handle to radiobutton4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of radiobutton4

global BBA_GUIDATA

if get(handles.radiobutton4,'Value')
  BBA_GUIDATA.scantype = 'bumps';
else
  BBA_GUIDATA.scantype = 'mover';
  set(handles.radiobutton3,'Value',1);
end

% --- Executes on button press in pushbutton2.
function pushbutton2_sext_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

global BEAMLINE BBA_GUIDATA
persistent pathdefined

useApp('sextBBA');

if ~isfield(BBA_GUIDATA,'running')
  BBA_GUIDATA.running = false;
end
if BBA_GUIDATA.running==true
  return
elseif BBA_GUIDATA.running==false
  BBA_GUIDATA.running=true;
end

plot_handles = get(handles.uipanel3,'Children');
% vplotnum = ceil(sqrt(length(plot_handles)));
% hplotnum = round(sqrt(length(plot_handles)));
for plothand = 1:length(plot_handles)
  delete(plot_handles(plothand));
%   phand = subplot(vplotnum,hplotnum,plothand,'Parent',handles.uipanel3);
%   xl = get(get(phand,'XLabel'),'String');
%   yl = get(get(phand,'YLabel'),'String');
%   cla(phand)
%   xlabel(phand,xl)
%   ylabel(phand,yl)
%   axis(phand,[0 1 0 1])
end

set(handles.text6,'String','Running...')
set(handles.pushbutton2,'BackgroundColor',[1 0 0])
set(handles.pushbutton2,'String','RUNNING...')

if isempty(pathdefined)
  if ~isdeployed; addpath('testApps/bumpgui'); end;
  pathdefined = true;
end

quads = get(handles.listbox2,'String');
if isempty(quads)
  msghand = end_with_error...
    (handles, ['Please select a device to scan by clicking the "Device Type" button' ...
    ',selecting "QUADs" or "SEXTs", and then selecting a device from the list.']);
  uiwait(msghand,5)
  if ishandle(msghand); delete(msghand); end
  return
end
quadoi = quads(get(handles.listbox2,'Value'));
quadindoi = max(findcells(BEAMLINE,'Name',quadoi{1}));

sextBBA('SetJS',BBA_GUIDATA.JS);
sextBBA('SetMagnet',BEAMLINE{quadindoi}.Name);
sextBBA('SetNpulse',str2double(get(handles.edit4,'String')))
sextBBA('SetScanRange',-str2double(get(handles.edit1,'String'))*1e-3,...
  str2double(get(handles.edit1,'String'))*1e-3,str2double(get(handles.edit2,'String')));

if get(handles.radiobutton1,'Value')
  axesplane = 'x';
else
  axesplane = 'y';
end
sextBBA('setPlane',axesplane);
bpminds = bpmchoose(quadindoi,axesplane) ;
bpmsoi = bpminds ;
if isempty(bpmsoi)
  bpmsoi = cell(1,length(bpminds));
  for ind=1:length(bpminds)
    bpmsoi{ind} = BEAMLINE{bpminds(ind)}.Name;
  end
end
bpmindsoi = zeros(1,length(bpmsoi));
for ind=1:length(bpmsoi)
  bpmindsoi(ind) = findcells(BEAMLINE,'Name',bpmsoi{ind});
end

ax=axes('Parent',handles.uipanel3);
sextBBA('setFigHan',ax);
sextBBA('setTextHan',handles.text6);
stat=sextBBA('DoBBA');
if stat{1}~=1; disp(stat{2}); end;
set(handles.pushbutton2,'BackgroundColor',[0 1 0])
set(handles.pushbutton2,'String','CLICK TO RUN')
BBA_GUIDATA.running = false;



% --- Executes on button press in radiobutton1.
function radiobutton1_Callback(hObject, eventdata, handles)
% hObject    handle to radiobutton1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of radiobutton1
listbox2_Callback(handles.listbox2,[],handles);
BBA_GUIDATA.running = false;


% --- Executes on button press in radiobutton2.
function radiobutton2_Callback(hObject, eventdata, handles)
% hObject    handle to radiobutton2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of radiobutton2

listbox2_Callback(handles.listbox2,[],handles);
BBA_GUIDATA.running = false;


% --- Executes when user attempts to close figure1.
function figure1_CloseRequestFcn(hObject, eventdata, handles)
% hObject    handle to figure1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: delete(hObject) closes the figure

global BBA_GUIDATA %#ok<NUSED>
try
  delete(handles.figure1);
  clear global BBA_GUIDATA
catch
  try
    delete(handles.figure1);
    clear global BBA_GUIDATA
  catch
    delete(hObject)
  end
end


function camangles = fftbmover_convert(x,y,rot,magtype)

global BBA_GUIDATA

if strcmpi(magtype,'quad')
  
  x_1 = x + (BBA_GUIDATA.camsettings.quad.a*cos(rot)) + ...
    (BBA_GUIDATA.camsettings.quad.b*sin(rot)) - BBA_GUIDATA.camsettings.quad.a;
  y_1 = y - (BBA_GUIDATA.camsettings.quad.b*cos(rot)) + ...
    (BBA_GUIDATA.camsettings.quad.a*sin(rot)) + BBA_GUIDATA.camsettings.quad.c;
  betaminus = pi/4 - rot;
  betaplus  = pi/4 + rot;
  
  camangles(1) = rot - asin((1/BBA_GUIDATA.camsettings.quad.L) * ...
    ((x_1+BBA_GUIDATA.camsettings.quad.S2)*sin(rot) - y_1*cos(rot) + ...
    (BBA_GUIDATA.camsettings.quad.c - BBA_GUIDATA.camsettings.quad.b)));
  
  camangles(2) = rot - asin((1/BBA_GUIDATA.camsettings.quad.L) * ...
    ((x_1+BBA_GUIDATA.camsettings.quad.S1)*sin(betaminus) + y_1*cos(betaminus) - ...
    BBA_GUIDATA.camsettings.quad.R));
  
  camangles(3) = rot - asin((1/BBA_GUIDATA.camsettings.quad.L) * ...
    ((x_1-BBA_GUIDATA.camsettings.quad.S1)*sin(betaplus) - y_1*cos(betaplus) + ...
    BBA_GUIDATA.camsettings.quad.R));
  
  if ~isreal(camangles(1)) || ~isreal(camangles(2)) || ~isreal(camangles(3))
    camangles = [NaN NaN NaN];
    return
  end
  
  return
  
elseif strcmpi(magtype,'sext')
  
  x_1 = x + (BBA_GUIDATA.camsettings.sext.a*cos(rot)) + ...
    (BBA_GUIDATA.camsettings.sext.b*sin(rot)) - BBA_GUIDATA.camsettings.sext.a;
  y_1 = y - (BBA_GUIDATA.camsettings.sext.b*cos(rot)) + ...
    (BBA_GUIDATA.camsettings.sext.a*sin(rot)) + BBA_GUIDATA.camsettings.sext.c;
  betaminus = pi/4 - rot;
  betaplus  = pi/4 + rot;
  
  camangles(1) = rot - asin((1/BBA_GUIDATA.camsettings.sext.L) * ...
    ((x_1+BBA_GUIDATA.camsettings.sext.S2)*sin(rot) - y_1*cos(rot) + ...
    (BBA_GUIDATA.camsettings.sext.c - BBA_GUIDATA.camsettings.sext.b)));
  
  camangles(2) = rot - asin((1/BBA_GUIDATA.camsettings.sext.L) * ...
    ((x_1+BBA_GUIDATA.camsettings.sext.S1)*sin(betaminus) + y_1*cos(betaminus) - ...
    BBA_GUIDATA.camsettings.sext.R));
  
  camangles(3) = rot - asin((1/BBA_GUIDATA.camsettings.sext.L) * ...
    ((x_1-BBA_GUIDATA.camsettings.sext.S1)*sin(betaplus) - y_1*cos(betaplus) + ...
    BBA_GUIDATA.camsettings.sext.R));
  
  if ~isreal(camangles(1)) || ~isreal(camangles(2)) || ~isreal(camangles(3))
    camangles = [NaN NaN NaN];
    return
  end
  
  return
  
end


function limits = find_mover_lims(xory,orth,rot,magtype)

count = 0;
range = -5e-3:1e-5:5e-3;
camangles = zeros(length(range),3);
for dof=range
  count = count+1;
  if strcmpi(xory,'x')
    camangles(count,:) = fftbmover_convert(dof,orth,rot,magtype);
  elseif strcmpi(xory,'y')
    camangles(count,:) = fftbmover_convert(orth,dof,rot,magtype);
  end
end

[dof1 ind1] = max(camangles);
[dof2 ind2] = min(camangles);

extreme(1) = min([ind1 ind2]);
extreme(2) = max([ind1 ind2]);

if isnan(dof1(1))
  limits = [NaN NaN];
else
  limits = [range(extreme(1)) range(extreme(2))];
end

return


function msghand = end_with_error(handles, message)

global BBA_GUIDATA

msghand = msgbox(message,'Error','modal');
BBA_GUIDATA.running = false;
set(handles.pushbutton2,'BackgroundColor',[0 1 0])
set(handles.pushbutton2,'String','CLICK TO RUN')
set(handles.text6,'String','Ready...')
AccessRequest('release');
return


% --- Executes on button press in pushbutton6.
function pushbutton6_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton6 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Choose BPMs to use for a given magnet
function [bpmname bpminds] = bpmchoose(magind,axis)
global BEAMLINE PS INSTR
% Check magnet on and near normal strength
if BEAMLINE{magind}.B==0 || PS(BEAMLINE{magind}.PS).Ampl<0.5
  errorRep('Magnet not on or not near normal strength, fix before running');
end
bpmname={};
magname=BEAMLINE{magind}.Name;
mbpm=findcells(BEAMLINE,'Name',['M',magname]);
if isempty(mbpm); return; end;
% Choose up to 4 BPMs with largest reponse matricies
[~, data]=FlBpmToolFn('getdata');
useinstr=find(data.global.use); useinstr=arrayfun(@(x) INSTR{x}.Index,useinstr);
Rbpm=[]; Rbpm_list=[];
for ind=findcells(BEAMLINE,'Class','MONI',BEAMLINE{magind}.Block(end)+1,length(BEAMLINE))
  if ismember(ind,useinstr) && ind~=mbpm
    [~, R]=RmatAtoB(magind,ind);
    if axis=='x' || strcmp(BEAMLINE{magind}.Name,'SEXT')
      Rbpm(end+1)=R(1,2);
      Rbpm_list(end+1)=ind;
      corclass='XCOR';
    else
      Rbpm(end+1)=R(3,4);
      Rbpm_list(end+1)=ind;
      corclass='YCOR';
    end
  end
end
if isempty(Rbpm); return; end;
[Y I]=sort(abs(Rbpm),'descend');
bpmname{1,1}=BEAMLINE{mbpm}.Name;
bpminds(1,1)=mbpm; nbpm=1;
for ibpm=1:length(I)
  % if EXT magnet, need 2 downstream correctors
  if magname(end)=='X' && length(findcells(BEAMLINE,'Class',corclass,Rbpm_list(I(ibpm)),length(BEAMLINE)))<2
    continue
  end
  nbpm=nbpm+1;
  if nbpm>5; break; end;
  bpmname{nbpm,1}=BEAMLINE{Rbpm_list(I(ibpm))}.Name;
  bpminds(nbpm,1)=Rbpm_list(I(ibpm));
end
[Y I]=sort(bpminds);
bpminds=Y;
bpmname=bpmname(I);
if strcmp(BEAMLINE{magind}.Class,'SEXT')
  bpminds=bpminds(2:end);
  bpmname=bpmname(2:end);
end

function errorRep(str)
global BBA_GUIDATA
errordlg(str,'BBA Error')
BBA_GUIDATA.running=false;
error(str)

function [bpmdata bpmdata_err]=getBpmValsSVD(numaves,bpmind,axis)
global BBA_GUIDATA
jsdata=BBA_GUIDATA.JS.getSubData(numaves);
bpmdata=jsdata.(sprintf('%s_mean',axis))(:,ismember(BBA_GUIDATA.JS.useINSTR,bpmind));
bpmdata_err=jsdata.(sprintf('%s_rms',axis))(:,ismember(BBA_GUIDATA.JS.useINSTR,bpmind));

function [bpmdata bpmdata_err]=getBpmVals(numaves,bpmind,axis)
FlHwUpdate('wait',numaves);
[~, bpmdataRead]=FlHwUpdate('bpmave',numaves);
if axis=='x'
  bpmdata=bpmdataRead{1}(1,bpmind);
  bpmdata_err=bpmdataRead{1}(4,bpmind);
else
  bpmdata=bpmdataRead{1}(2,bpmind);
  bpmdata_err=bpmdataRead{1}(5,bpmind);
end


% --- Executes on button press in pushbutton7.
function pushbutton7_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton7 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global BEAMLINE INSTR BBA_GUIDATA FL
set(handles.text6,'String','Getting BPM data to calculate new BBA offset')
drawnow('expose')
BBA_GUIDATA.running=true;
% Get current BBA value
bbaval=str2double(get(handles.text9,'String'));
if isnan(bbaval)
  errorRep('Invalid current BBA value, aborting')
else
  bbaval=bbaval*1e-6;
end
% Get magnet and corresponding BPM
mags=get(handles.listbox2,'String');
magname=mags{get(handles.listbox2,'Value')};
mbpm=findcells(BEAMLINE,'Name',['M',magname]);
bpminstr=findcells(INSTR,'Index',mbpm);
if isempty(mbpm)
  errorRep('No related BPM found for this magnet')
end
if get(handles.radiobutton1,'Value')
  axplane='x';
else
  axplane='y';
end
% Check BBA done
if ~isfield(BBA_GUIDATA,axplane) || ~isfield(BBA_GUIDATA.(axplane),magname) || ...
    ~isfield(BBA_GUIDATA.(axplane).(magname),'real_centre') || ...
    isempty(BBA_GUIDATA.(axplane).(magname).real_centre)
  errorRep('No BBA Done yet for this magnet!')
end


% Get BBA PV
if strcmp(INSTR{bpminstr}.Type,'stripline')
  bbapv=[BEAMLINE{mbpm}.Name ':BBA_' upper(axplane)];
else
  bbapv=[BEAMLINE{mbpm}.Name(2:end) axplane ':bbaOffset'];
end
% Commit BBA data
if BEAMLINE{mbpm}.Name(2)=='S'
  if get(handles.checkbox1,'Value')
    bpmdata=getBpmValsSVD(10,bpminstr,axplane);
  else
    bpmdata=getBpmVals(10,bpminstr,axplane);
  end
  bbaval_new=bpmdata;
  set(handles.text9,'String',num2str(bbaval_new*1e6))
  if ~FL.SimMode
    lcaPut(bbapv,num2str(bbaval_new/FL.HwInfo.INSTR(bpminstr).conv(1)));
  end
  set(handles.text6,'String',sprintf('%.2f\nBBA Data commited to EPICS',bbaval_new*1e6))
else
  bbaval_new=bbaval+BBA_GUIDATA.(axplane).(magname).real_centre*1e-3;
  if isequal(questdlg(sprintf('New BBA = %.2f + %.2f = %.2f (um)\nOK to commit?',...
    bbaval*1e6,BBA_GUIDATA.(axplane).(magname).real_centre*1e3,bbaval_new*1e6)),'Yes')
    set(handles.text9,'String',num2str(bbaval_new*1e6))
    if ~FL.SimMode
      lcaPut(bbapv,num2str(bbaval_new/FL.HwInfo.INSTR(bpminstr).conv(1)));
    end
    set(handles.text6,'String',sprintf('%.2f\nBBA Data commited to EPICS',bbaval_new*1e6))
  else
    set(handles.text6,'String',sprintf('%s\nBBA Data NOT commited to EPICS',get(handles.text9,'String')))
  end
end

BBA_GUIDATA.running=false;


% --- Executes on button press in checkbox1.
function checkbox1_Callback(hObject, eventdata, handles)
% hObject    handle to checkbox1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
useApp('sextBBA');
sextBBA('setsvd',get(hObject,'Value'));



function edit6_Callback(hObject, eventdata, handles)
% hObject    handle to edit6 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit6 as text
%        str2double(get(hObject,'String')) returns contents of edit6 as a double


% --- Executes during object creation, after setting all properties.
function edit6_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit6 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in pushbutton8.
function pushbutton8_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton8 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)



function edit7_Callback(hObject, eventdata, handles)
% hObject    handle to edit7 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit7 as text
%        str2double(get(hObject,'String')) returns contents of edit7 as a double


% --- Executes during object creation, after setting all properties.
function edit7_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit7 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function [centres results] = bpmquad_offset(magnet, xory, range, pps, aves, saveornot)

% [centres results] = bpmquad_offset(magnet, xory, range, pps, aves, saveornot)
%
% Performs Beam-Based-Alignment (BBA) of magnets in ATF2 (ring, extraction
% line, and final focus) within the Lucretia Floodland Flight Simulator
% environment.
% 
% Calling bpmquad_offset with no inputs starts the GUI, from which you can
% control the measurement options.  The results, described later, will be
% contained in the global variable BBA_GUIDATA, and will *not* be returned
% to the variable "results" (i.e. results is only used when bpmquad_offset
% is called with input variables).
%
% Once the GUI has been started, select a magnet to scan by pressing the
% appropriate manget type button ("Quads" or "Sexts"), and selecting the
% magnet of interest from the scrolling text box.  The alter the settings
% to fit your needs (see below), and press "CLICK TO RUN" to begin the
% scan.
%
% The results will be reported in the "Status" field near the top-right of
% the GUI.
%
% If called with input variables, then all four inputs are required.
%
% "magnet"    -- A cell array of strings.  Each of these strings must
%                correspond to the name of a quadrupole (ring, EXT, FF) or
%                sextupole (FF).
% "xory"      -- A single character, 'x' or 'y', indicating the plane in
%                which the measurement is to be made.  A cell array of such
%                strings is also allowed, however it is an error if this is
%                not the same length as the "magnet" cell array.
% "range"     -- The extent through which you wish to bump the beam.  This
%                setting has no effect when scanning with a mover.
% "pps"       -- Points per scan.  The number of bump or mover settings to
%                move through in order to make the measurement.  For
%                quadrupoles, this must be >=2, and for sextupoles this
%                must be >=3.
% "aves"      -- The number of beam pulses (averages) to record at each
%                point.
% "saveornot" -- To save locally, on the server, or not at all.  'local'
%                saves the entire dataset to the local machine in the
%                BBA_GUIDATA structure.  The filename is
%                bpmquad_offset/data/datafile.mat.  'server' saves the
%                measured centre and measurement error to the Floodland
%                server.  Default is not to save.
%
% The 'centres' output is an nx2 array where n is the number of
% measurements.  The first column is the measured centre of each of the
% magnets, and the second column is the error on that measurement.
%
% The 'results' output is in the form of a matlab structure.
%
% The 'x' plane results will be in, results.x.MAGNET_NAME.real_centre
% (where "MAGNET_NAME" should be replaced by the name of the magnet), and
% the error on this measurement will be in
% results.x.MAGNET_NAME.real_centre_err.
%
% Exchange the 'x' for a 'y' in the above to get the 'y' plane results.
%
% ALGORITHM
% =========
%
% For quads, the position of the beam with respect to the quad field is
% scanned through a series of steps, and the result is observed on the four
% closest downstream BPMs.  The scan will be performed by moving the quad
% with a mover (if available), or by bumping the beam if it is not mounted
% on a mover system.  The GUI can be forced to always use bumps by
% selecting the appropriate switch on the top-right of the GUI window,
% however an error will result if there aren't sufficient suitably located
% BPMs and correctors.  The range of the scan is controllable by a text
% entry box on the GUI panel, the number of points in the scan can be
% altered using the "Points per scan" field, and the number of beam pulses
% recorded at each point can be set with the "Averages per point" field.
% 
% After this first scan, the quad will be shunted by an amount controllable
% by the by the "shunt value" text box, and the mover (or bump) scan will
% be repeated.
% 
% Two straight lines will be fit to the data for each of the four BPMs, and
% the crossing points indicate the centre of this quad.  The result is
% reported in the "status" panel at the top of the GUI.
%
% For sexts, only a single scan is required.  They are scanned with their
% mover, and the results are observed on *all* downstream BPMs.  Each of
% these scans is fit to a parabola, and the turning point of this fit is
% calculated.  This is the centre of the sext.
%
% Steve Molloy, SLAC, 6th Nov 2008

global BEAMLINE BBA_GUIDATA

if ~(nargin==0 || nargin==5 || nargin==6)
    error('bpmquad_offset requires 0, 5, or 6 input arguments')
end

if nargin==5
    saveornot = 'not';
end

if nargin==5 || nargin==6
    if isempty(range)
        range = 1;
    elseif ~isnumeric(range)
        error('3rd input to bpmquad_offset must be an array of intergers')
    end
    
    if ~isnumeric(pps)
        error('4th input to bpmquad_offset must be an array of integers')
    end
    for ind=1:length(pps)
        if ~rem(pps(ind),1)==0
            error('4th input to bpmquad_offset must be an array of integers')
        end
    end

    if ~isnumeric(aves)
        error('5th input to bpmquad_offset must be an array of integers')
    end
    for ind=1:length(aves)
        if ~rem(aves(ind),1)==0
            error('5th input to bpmquad_offset must be an array of integers')
        end
    end

    if ~iscell(magnet)
        error('First input to bpmquad_offset must be a cell array of strings')
    end
    for magnum=1:length(magnet)
        if ~ischar(magnet{magnum})
            error('First input to bpmquad_offset must be a cell array of strings')
        end
    end

    if ischar(xory)
        if ~(strcmpi(xory,'x') || strcmpi(xory,'y'))
            error('2nd input to bpmquad_offset must be ''x'' or ''y''')
        end
        plane = xory;
        xory = cell(size(magnet));
        for magnum=1:length(magnet)
            xory{magnum} = plane;
        end
    elseif iscell(xory)
        for ind=1:length(xory)
            if ~(strcmpi(xory{ind},'x') || strcmpi(xory{ind},'y'))
                error('2nd input to bpmquad_offset must be ''x'' or ''y''')
            end
        end
        if ~(length(xory)==1 || length(xory)==length(magnet))
            error(['2nd input to bpmquad_offset must be a single character, or a cell '...
                'array of length equal to 1 or the length of the 1st input.'])
        end
        if length(xory)==1
            plane = xory{1};
            xory = cell(size(magnet));
            for magnum=1:length(magnet)
                xory{magnum} = plane;
            end
        end
    end
    
    if ~(length(range)==1 || length(range)==length(magnet))
            error(['3rd input to bpmquad_offset must be a single integer, or an '...
                'integer array of length equal to 1 or the length of the 1st input.'])
    end
    
    if ~(length(pps)==1 || length(pps)==length(magnet))
            error(['4th input to bpmquad_offset must be a single integer, or an '...
                'integer array of length equal to 1 or the length of the 1st input.'])
    end
    
    if ~(length(aves)==1 || length(aves)==length(magnet))
            error(['5th input to bpmquad_offset must be a single integer, or an '...
                'integer array of length equal to 1 or the length of the 1st input.'])
    end
    
    if length(range)==1
        scansize = range;
        range = zeros(size(magnet));
        for magnum=1:length(magnet)
            range(magnum) = scansize;
        end
    end
    
    if length(pps)==1
        numpoints = pps;
        pps = zeros(size(magnet));
        for magnum=1:length(magnet)
            pps(magnum) = numpoints;
        end
    end
    
    if length(aves)==1
        numaves = aves;
        aves = zeros(size(magnet));
        for magnum=1:length(magnet)
            aves(magnum) = numaves;
        end
    end
    
    if ~ischar(saveornot)
        error('6th input to bpmquad_offset must be either ''server'' or ''local''')
    end
    if ~strcmpi(saveornot,'server') && ~strcmpi(saveornot,'local') && ~strcmpi(saveornot,'not')
        error('6th input to bpmquad_offset must be either ''server'' or ''local''')
    end
end

[bbahObj bbaedata bbahand] = bpmquad_offsetGUI;

if nargin==4 || nargin==5
    centres = nan(length(magnet),2);
    for magnum=1:length(magnet)
        disp(['Scanning ' magnet{magnum} ' (' num2str(magnum) '/'...
            num2str(length(magnet)) ')'])
        maginds = findcells(BEAMLINE,'Name',magnet{magnum});
        if isempty(maginds)
            error([magnet{magnum} ' not found in BEAMLINE.'])
        end
        if strcmpi(BEAMLINE{maginds(1)}.Class,'quad')
            feval(get(bbahand.pushbutton1,'Callback'),bbahObj,bbaedata)
        elseif strcmpi(BEAMLINE{maginds(1)}.Class,'sext')
            feval(get(bbahand.pushbutton5,'Callback'),bbahObj,bbaedata)
        else
            error([magnet{magnum} ' must be a quad or a sext.'])
        end
        maglist = get(bbahand.listbox2,'String');
        set(bbahand.listbox2,'Value',strmatch(magnet{magnum},maglist,'exact'))
        feval(get(bbahand.listbox2,'Callback'),bbahObj,bbaedata)
        
        if strcmpi(xory{magnum},'x')
            set(bbahand.radiobutton1,'Value',true);
            set(bbahand.radiobutton2,'Value',false);
        elseif strcmpi(xory{magnum},'y')
            set(bbahand.radiobutton1,'Value',false);
            set(bbahand.radiobutton2,'Value',true);
        end
        
        set(bbahand.edit1,'String',num2str(range(magnum)))
        feval(get(bbahand.edit1,'Callback'),bbahObj,bbaedata)
        
        set(bbahand.edit2,'String',num2str(pps(magnum)))
        feval(get(bbahand.edit2,'Callback'),bbahObj,bbaedata)

        set(bbahand.edit4,'String',num2str(aves(magnum)))
        feval(get(bbahand.edit4,'Callback'),bbahObj,bbaedata)

        feval(get(bbahand.pushbutton2,'Callback'),bbahObj,bbaedata)

        if isfield(BBA_GUIDATA,'x'); results.x = BBA_GUIDATA.x; end
        if isfield(BBA_GUIDATA,'y'); results.y = BBA_GUIDATA.y; end

        if exist('results','var')
            if isfield(results,xory{magnum})
                eval(['tempvar = results.' xory{magnum} ';'])
                if isfield(tempvar,magnet{magnum})
                    eval(['centres(magnum,1) = results.' ...
                        xory{magnum} '.' magnet{magnum} '.real_centre;'])

                    eval(['centres(magnum,2) = results.' ...
                        xory{magnum} '.' magnet{magnum} '.real_centre_err;'])

                    if strcmpi(saveornot,'server')
                        stat = FlDataStore('BBA',[magnet{magnum} '_' ... 
                          xory{magnum}],centres(magnum,:));
                        if stat{1}~=1; error(stat{2}); end
                    end
                end
            end

            if strcmpi(saveornot,'local')
                save('~/Lucretia/src/Floodland/testApps/bpmquad_offset/data/datafile.mat',...
                    'BBA_GUIDATA')
            end
        end
    end

    delete(bbahObj)
end

return


function [stat dataout] = extCoupling_run(cmd,newpars)
% stat = extCoupling_run(cmd,pars)
% Functional interface for Extraction line coupling measurement and
% correction
global FL
persistent Model data pars

stat{1}=1; dataout=[];

if ~exist('cmd','var'); cmd='init'; end;

if isempty(Model) || isequal(lower(cmd),'init') % first call or init request
  Model=FL.SimModel;
  Model=init_setup(Model);
end

if isempty(pars)
  pars.scan_steps = [-1 -0.5 0 0.5 1] ;
  pars.eta_meas = false;
  pars.knob_name = 'Skew1';
end % if isempty(pars)

switch lower(cmd)
  case 'scan'
    if ~isfield(Model.ext,pars.knob_name)
      error('Unknown scan knob given!')
    end % if incorrect knob
    % Check at least 3 wirescanners selected
    if length(Model.ext.wsuse)<3
      errordlg('Must choose >=3 wirescanenrs to use!','extCoupling_run Error');
      return
    end
    [stat,Model.ext.(pars.knob_name),data]=...
      QuadScan(Model,Model.ext.(pars.knob_name),Model.ext.Emat,pars.scan_steps,pars.eta_meas,data);
    if (stat{1}~=1);return;end;
    dataout=data;
  case 'commit'
    if isfield(pars,'knob_name')
      knob=Model.ext.(pars.knob_name);
      SetMultiKnob('knob',data.(pars.knob_name).fit.B(1),1) ;
      Model.ext.(pars.knob_name)=knob;
    end % if pars.knob_name
  case 'setpars'
    if ~exist('newpars','var') || ~isstruct(newpars)
      stat{1}=-1; stat{2}='Incorrect arguments'; return;
    end % if newpars wrong
    names=fieldnames(newpars);
    for iname=1:length(names)
      pars.(names{iname})=newpars.(names{iname});
    end % for iname
  case 'choosews'
    Model.ext.wsuse=newpars;
  case 'getdata'
    dataout=data;
  case 'getpars'
    dataout=pars;
end % switch cmd
% Store Model data
FL.SimModel.ext=Model.ext;

function [stat,knob,data] = QuadScan( Model, knob, Emat, ScanSteps, etaMeas, data ) 
% scan a skew quad knob, measure emittance, find the optimum emittance
% value

global INSTR FL
stat{1}=1;

% scan the knob and get the beam sizes on each wire scanner -- here we have
% a row for each measurement and a column for each wire
WireSig33 = [] ;
initKnobValue=knob.Value;
dE=FL.SimModel.Initial.SigPUncorrel;
for count = ScanSteps
  SetMultiKnob('knob',count,1) ;
  if etaMeas
    dispVals=[INSTR{Model.ext.mw_iind(1)}.dispref(3) ...
      INSTR{Model.ext.mw_iind(2)}.dispref(3) ...
      INSTR{Model.ext.mw_iind(3)}.dispref(3) ...
      INSTR{Model.ext.mw_iind(4)}.dispref(3) ...
      INSTR{Model.ext.mw_iind(5)}.dispref(3)];
  end
  getWsData(Model,FL.Gui.extCoupling,['Knob: ',knob.Comment,'(value=',num2str(count),')']);
  WireSig33 = [WireSig33 ; ...
    sqrt(INSTR{Model.ext.mw_iind(1)}.Data(5)-(dispVals(1)*1e-3)^2*dE^2) ...
    sqrt(INSTR{Model.ext.mw_iind(2)}.Data(5)-(dispVals(2)*1e-3)^2*dE^2) ...
    sqrt(INSTR{Model.ext.mw_iind(3)}.Data(5)-(dispVals(3)*1e-3)^2*dE^2) ...
    sqrt(INSTR{Model.ext.mw_iind(4)}.Data(5)-(dispVals(4)*1e-3)^2*dE^2) ...
    sqrt(INSTR{Model.ext.mw_iind(5)}.Data(5)-(dispVals(5)*1e-3)^2*dE^2)     ] ;
end
SetMultiKnob('knob',initKnobValue,1) ;

% flip the wire size matrix
WireSig33 = WireSig33' ;

% get the least-squares sigma matrix at the first wire scanner on each
% step of the knob
sig0 =  Emat(Model.ext.wsuse,:) \ WireSig33(Model.ext.wsuse,:).^2 ;

% compute the resulting emittance as a function of the knob
emit2y = sig0(1,:).*sig0(3,:) - sig0(2,:).*sig0(2,:) ;
emity = sqrt(emit2y) ;

% convert to un-normalized emittances in pm
emity = 1e12 * emity; % * 1.3/0.000511 ;

% fit a parabola to this thing
[A,B,C] = parabola_fit( ScanSteps, emity, 0 ) ;
dx=(max(ScanSteps)-min(ScanSteps))/100;
data.(knob.Comment).plot.fit.x=min(ScanSteps):dx:max(ScanSteps);
data.(knob.Comment).plot.fit.y=(C(1)+A(1)*(data.(knob.Comment).plot.fit.x-B(1)).^2);
data.(knob.Comment).plot.x=ScanSteps;
data.(knob.Comment).plot.y=emity;
data.(knob.Comment).fit.A=A; data.(knob.Comment).fit.B=B; data.(knob.Comment).fit.C=C;
data.(knob.Comment).wiresig=WireSig33;

function Model = init_setup(Model)
% Setup stuff
global BEAMLINE INSTR
% Define EXT end-point
Model.ext.endInd = findcells(BEAMLINE,'Name','BEGFF') ;
Model.extStart = findcells(BEAMLINE,'Name','IEX'); Model.extStart=Model.extStart(end);
% Number of tuning iterations
Model.ext.nSteer=50;
Model.ext.nEta=1;

% Which wirescanners to use
Model.ext.wsuse=1:5;

% Wirescanners indicies
Model.ext.mw_ind = findcells(BEAMLINE,'Name','MW*X');
Model.ext.mw_iind = find(cellfun(@(x) ismember(x.Index,Model.ext.mw_ind),INSTR));
if length(Model.ext.mw_ind)~=length(Model.ext.mw_iind)
  error('INSTR indexing error');
end % if length INSTR,mw_ind not match

% find the beginning and end of the section with misaligned quads, and the
% beginning and end of the BH1-2 section:
Model.ext.msim=Model.extStart;
Model.ext.begff=findcells(BEAMLINE,'Name','BEGFF');
Model.ext.bh12beg=findcells(BEAMLINE,'Name','BH1XA');
Model.ext.bh12end=findcells(BEAMLINE,'Name','BH2XB')+1;

% construct response matrices for both of the steering jobs
Model.ext.BPMSteerStruc=MakeATF2XYSteerStruc;
Model.ext.BPMLaunchStruc=MakeATF2LaunchSteerStruc;

% construct the matrix which converts the beam matrix at the first wire into 
% the sig33 at each wire in the wire scanner system
Model.ext.Emat=MakeATF2Emat;

% define the 4 skew quad power supplies to be multiknobs
[Skew1,Skew2,Skew3,Skew4]=MakeCouplingCorrectionMultiKnobs;
Model.ext.Skew1=Skew1; Model.ext.Skew2=Skew2;
Model.ext.Skew3=Skew3; Model.ext.Skew4=Skew4;

function dispVals=get_disp
global FL
dispVals=[];
while length(dispVals)~=5
  set(FL.Gui.extCoupling.edit1,'String',[])
  set(FL.Gui.extCoupling.text2,'String','Enter Dispersion measurement for MW0X : MW4X (mm)')
  % wait for submit button to be pushed
  waitforbuttonpress 
  while ~strcmp(get(get(FL.Gui.extCoupling.figure1,'CurrentObject'),'String'),'Submit')
    waitforbuttonpress
  end
  dispVals=str2num(get(FL.Gui.extCoupling.edit1,'String')); %#ok<ST2NM>
  if length(dispVals)~=5
    uiwait(warndlg('Enter 5 vals separated by spaces','Incorrect Format'));
  end
end
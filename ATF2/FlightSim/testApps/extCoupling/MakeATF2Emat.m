function Emat=MakeATF2Emat()

% compute the matrix which transforms the beam matrix at the first wire into 
% the sig33 at each wire in the wire scanner system

global BEAMLINE

idw=findcells(BEAMLINE,'Name','MW0X');
idw=[idw,findcells(BEAMLINE,'Name','MW1X')];
idw=[idw,findcells(BEAMLINE,'Name','MW2X')];
idw=[idw,findcells(BEAMLINE,'Name','MW3X')];
idw=[idw,findcells(BEAMLINE,'Name','MW4X')];
Nw=length(idw);

Emat=zeros(Nw,3);
for n=1:Nw
  [iss,R]=RmatAtoB(idw(1),idw(n));
  Emat(n,1)=R(3,3)^2;
  Emat(n,2)=2*R(3,3)*R(3,4);
  Emat(n,3)=R(3,4)^2;
end

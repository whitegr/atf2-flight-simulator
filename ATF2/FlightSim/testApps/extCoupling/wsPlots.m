function varargout = wsPlots(varargin)
% WSPLOTS M-file for wsPlots.fig
%      WSPLOTS, by itself, creates a new WSPLOTS or raises the existing
%      singleton*.
%
%      H = WSPLOTS returns the handle to a new WSPLOTS or the handle to
%      the existing singleton*.
%
%      WSPLOTS('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in WSPLOTS.M with the given input arguments.
%
%      WSPLOTS('Property','Value',...) creates a new WSPLOTS or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before wsPlots_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to wsPlots_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help wsPlots

% Last Modified by GUIDE v2.5 18-Feb-2009 20:45:45

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @wsPlots_OpeningFcn, ...
                   'gui_OutputFcn',  @wsPlots_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before wsPlots is made visible.
function wsPlots_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to wsPlots (see VARARGIN)

% Choose default command line output for wsPlots
handles.output = handles;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes wsPlots wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = wsPlots_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on button press in pushbutton1.
function pushbutton1_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
guiCloseFn('wsPlots',handles);


% --- Executes when user attempts to close figure1.
function figure1_CloseRequestFcn(hObject, eventdata, handles)
% hObject    handle to figure1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: delete(hObject) closes the figure
guiCloseFn('wsPlots',handles);



function [stat resp] = controls_access(cmd)
% function [stat resp] = controls_access(cmd)
% Deal with access rights for this app
% stat = Lucretia status response
% resp = true if have access to required list
% if cmd='release', then release previous access request
persistent reqID accessList
global BEAMLINE FL

% If in non-Floodland simulation mode, do nothing and return success
if FL.SimMode==2
  stat{1}=1; resp=true; return;
end % if sim mode = 2

% Access requirement list
% Need access to scan 4 coupling section skew quads (QK1-4X)
% (currently only QK1X and QK4X present)
if isempty(accessList)
  qk1x=findcells(BEAMLINE,'Name','QK1X');
  qk4x=findcells(BEAMLINE,'Name','QK4X');
  accessList={[] [BEAMLINE{qk1x(1)}.PS BEAMLINE{qk4x(1)}.PS] []};
end % if not initialised list yet

% Deal with any requested commands
if exist('cmd','var') && isequal(lower(cmd),'release') && ~isempty(reqID)
  [stat resp] = AccessRequest('release',reqID);
  return
end % if release requested

% Request or return status of access request
if isempty(reqID) % no access- request it
  [stat resp] = AccessRequest(accessList);
  if stat{1}~=1; return; end;
  reqID=resp;
  resp=true;
else % access already requested, check status
  [stat resp] = AccessRequest('status',reqID);
  if stat{1}~=1; return; end;
  if ~resp; clear reqID; end;
end % if isempty(reqID)
function getWsData(Model,handles,txt)

global FL INSTR BEAMLINE
if ~exist('txt','var'); txt=[]; end;
for iw=Model.ext.wsuse
  set(handles.edit1,'String','')
  % Update WS data into EPICS
  uiwait(msgbox(sprintf('Acquire MW%dX vertical scan, then click OK...',iw-1)));
  FlUpdateWS;
  % Update INSTR readings
  FlHwUpdate;
  % Display values for acceptance by user
  set(handles.edit1,'String',num2str(sqrt(INSTR{Model.ext.mw_iind(iw)}.Data(5))*1e6));
  set(handles.text2,'String',[txt,': Vertical wirescanner reading for WS: ',...
    BEAMLINE{INSTR{Model.ext.mw_iind(iw)}.Index}.Name,' (um)'])
  set(handles.text2,'foregroundcolor',[0 0 0])
  drawnow('expose');
  waitforbuttonpress % wait for submit button to be pushed
  while ~strcmp(get(get(FL.Gui.extCoupling.figure1,'CurrentObject'),'String'),'Submit')
    waitforbuttonpress % wait for submit button to be pushed
  end
  INSTR{Model.ext.mw_iind(iw)}.Data(5)=(str2double(get(handles.edit1,'String'))*1e-6)^2;
end % for iw
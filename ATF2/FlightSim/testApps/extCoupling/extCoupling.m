function varargout = extCoupling(varargin)
% EXTCOUPLING M-file for extCoupling.fig
%      EXTCOUPLING, by itself, creates a new EXTCOUPLING or raises the existing
%      singleton*.
%
%      H = EXTCOUPLING returns the handle to a new EXTCOUPLING or the handle to
%      the existing singleton*.
%
%      EXTCOUPLING('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in EXTCOUPLING.M with the given input arguments.
%
%      EXTCOUPLING('Property','Value',...) creates a new EXTCOUPLING or raises the
%      existing singleton*.  Starting from the left, property value pairs
%      are
%      applied to the GUI before extCoupling_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to extCoupling_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help extCoupling

% Last Modified by GUIDE v2.5 18-May-2010 06:39:18

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @extCoupling_OpeningFcn, ...
                   'gui_OutputFcn',  @extCoupling_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before extCoupling is made visible.
function extCoupling_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to extCoupling (see VARARGIN)

% Choose default command line output for extCoupling
handles.output = handles;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes extCoupling wait for user response (see UIRESUME)
% uiwait(handles.figure1);

% Check access
accessUpdate(handles);


% --- Outputs from this function are returned to the command line.
function varargout = extCoupling_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on button press in pushbutton8.
function pushbutton8_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton8 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global FL
FL.Gui=rmfield(FL.Gui,'extCoupling');
delete(handles.figure1);

% --- Executes on button press in pushbutton4.
function pushbutton4_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on button press in pushbutton5.
function pushbutton5_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton5 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Apply correction
function pushbutton6_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton6 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
iknob=get(handles.popupmenu2,'Value');
knobs={'Skew1' 'Skew2' 'Skew3' 'Skew4'};
if iknob==1
  for nknob=1:length(knobs)
    set(handles.text2,'String',['Applying optimal measured coupling knob to ',knobs{nknob}])
    set(handles.text2,'foregroundcolor','black')
    drawnow('expose');
    pars.knob_name=knobs{nknob}; extCoupling_run('setPars',pars);
    stat=extCoupling_run('commit');
    if stat{1}~=1; guierr(handles,stat{2}); return; end;
  end % for nknob
else
  set(handles.text2,'String',['Applying optimal measured coupling knob to ',knobs{iknob-1}])
  set(handles.text2,'foregroundcolor','black')
  drawnow('expose');
  pars.knob_name=knobs{iknob-1}; extCoupling_run('setPars',pars);
  stat=extCoupling_run('commit');
  if stat{1}~=1; guierr(handles,stat{2}); end;
end % if iknob

% --- Executes on button press in pushbutton7.
function pushbutton7_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton7 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on button press in pushbutton9.
function pushbutton9_Callback(hObject, eventdata, handles) %#ok<*INUSD,*DEFNU>
% hObject    handle to pushbutton9 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% Remove gui from list and self-terminate 
global FL

FL.Gui.extPars=extPars;

% --- Executes on selection change in popupmenu1.
function popupmenu1_Callback(hObject, eventdata, handles) %#ok<*INUSL>
% hObject    handle to popupmenu1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = get(hObject,'String') returns popupmenu1 contents as cell array
%        contents{get(hObject,'Value')} returns selected item from popupmenu1
global FL
knobs={'Skew1' 'Skew2' 'Skew3' 'Skew4'};
[stat data]=extCoupling_run('getData');
if stat{1}~=1; guierr(handles,stat{2}); return; end;
idata=get(hObject,'Value');
knob=knobs{idata};
if isfield(data,knob)
  plot(handles.axes1,data.(knob).plot.x,data.(knob).plot.y,'ro')
  hold on
  plot(handles.axes1,data.(knob).plot.fit.x,data.(knob).plot.fit.y,'b')
  hold off
  xlabel(handles.axes1,'Knob Value'); ylabel(handles.axes1,'\epsilon_y (pm)');
  ver_line(data.(knob).fit.B(1),'r-',handles.axes1)
  if isfield(FL.Gui,'wsPlots') && isfield(FL.Gui.wsPlots,'figure1') && ishandle(FL.Gui.wsPlots.figure1)
    wstxt={'MW0X' 'MW1X' 'MW2X' 'MW3X' 'MW4X'};
    for iws=1:5
      figure(FL.Gui.wsPlots.figure1)
      [A,B,C] = parabola_fit( data.(knob).plot.x, data.(knob).wiresig(iws,:), 0 ) ;
      fitdata=C(1)+A(1)*(data.(knob).plot.fit.x-B(1)).^2;
      subplot(2,3,iws,'Parent',FL.Gui.wsPlots.uipanel1),plot( data.(knob).plot.x, data.(knob).wiresig(iws,:), 'ro' )
      subplot(2,3,iws,'Parent',FL.Gui.wsPlots.uipanel1),xlabel(wstxt{iws})
      hold on;  plot(data.(knob).plot.fit.x, fitdata, 'b'); hold off;
      ver_line(B(1),'r-')
    end % for iws
  end % if ws plot window open
end % if data field filled
  
% --- Executes during object creation, after setting all properties.
function popupmenu1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to popupmenu1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in pushbutton3.
function pushbutton3_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global FL
FL.Gui.wsPlots = wsPlots;
popupmenu1_Callback(handles.popupmenu1,[],handles);


function edit1_Callback(hObject, eventdata, handles)
% hObject    handle to edit1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit1 as text
%        str2double(get(hObject,'String')) returns contents of edit1 as a double


% --- Executes during object creation, after setting all properties.
function edit1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in pushbutton1.
function pushbutton1_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on button press in pushbutton2.
function pushbutton2_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
set(handles.edit1,'String','')

% --- Executes on button press in pushbutton10.
function pushbutton10_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton10 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
accessUpdate(handles);

% --- Executes on selection change in popupmenu2.
function popupmenu2_Callback(hObject, eventdata, handles)
% hObject    handle to popupmenu2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = get(hObject,'String') returns popupmenu2 contents as cell array
%        contents{get(hObject,'Value')} returns selected item from popupmenu2


% --- Executes during object creation, after setting all properties.
function popupmenu2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to popupmenu2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Scan
function pushbutton11_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton11 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% get list of knobs to scan
knobs={'Skew1' 'Skew2' 'Skew3' 'Skew4'};
scanreq=get(handles.popupmenu2,'Value');
% Initialise to get emit calc data from current lattice
extCoupling_run('init');
opt_text=[];
if scanreq==1
  for iknob=1:length(knobs)
    set(handles.text2,'String',['Starting scan for knob: ',knobs{iknob}]); set(handles.text2,'foregroundcolor','black');
    drawnow('expose');
    pars.knob_name=knobs{iknob};
    extCoupling_run('setPars',pars);
    stat=extCoupling_run('scan');
    if stat{1}~=1; guierr(handles,stat{2}); end;
    extCoupling('popupmenu1_Callback',handles.popupmenu1,[],handles); % update plot axis
    [stat data]=extCoupling_run('getData');
    opt_text=[opt_text pars.knob_name ' : ' num2str(data.(pars.knob_name).fit.B(1)) ' '];
  end % for iknob
else
  set(handles.text2,'String',['Starting scan for knob: ',knobs{scanreq-1}]); set(handles.text2,'foregroundcolor','black');
  drawnow('expose');
  pars.knob_name=knobs{scanreq-1};
  extCoupling_run('setPars',pars);
  stat=extCoupling_run('scan');
  if stat{1}~=1; guierr(handles,stat{2}); end;
  extCoupling('popupmenu1_Callback',handles.popupmenu1,[],handles); % update plot axis
  [stat data]=extCoupling_run('getData');
  opt_text=[opt_text pars.knob_name ' : ' num2str(data.(pars.knob_name).fit.B(1))];
end % if scanreq=1
set(handles.text3,'String',opt_text)
set(handles.text2,'String','Scan Complete.'); set(handles.text2,'foregroundcolor','black');

function guierr(handles,txt)
set(handles.text2,'String',txt)
set(handles.text2,'foregroundcolor','red')
drawnow('expose');
  
function accessUpdate(handles)
set(handles.text2,'String','Checking/requesting access')
set(handles.text2,'foregroundcolor','black')
drawnow('expose');
[stat resp] = controls_access;
bhan=handles.pushbutton10;
if stat{1}~=1
  set(bhan,'backgroundcolor','black')
  set(bhan,'foregroundcolor','white')
  set(handles.text2,'String',stat{2})
  set(handles.text2,'foregroundcolor','red')
  drawnow('expose');
elseif resp
  set(bhan,'backgroundcolor','green')
  set(bhan,'foregroundcolor','black')
  set(handles.text2,'String','Access to required control PV''s granted')
  set(handles.text2,'foregroundcolor','black')
  drawnow('expose');
else
  set(bhan,'backgroundcolor','red')
  set(bhan,'foregroundcolor','black')
  set(handles.text2,'String','Access to required control PV''s denied')
  set(handles.text2,'foregroundcolor','red')
  drawnow('expose');
end % if stat/resp


% --- Executes during object deletion, before destroying properties.
function figure1_DeleteFcn(hObject, eventdata, handles)
% hObject    handle to figure1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global FL
if isfield(FL,'Gui') && isfield(FL.Gui,'extCoupling')
  FL.Gui=rmfield(FL.Gui,'extCoupling');
end
delete(handles.figure1);


% --- Executes on button press in pushbutton12.
function pushbutton12_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton12 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
popupmenu1_Callback(handles.popupmenu1,[],handles);

% --- Executes on button press in checkbox1.
function checkbox1_Callback(hObject, eventdata, handles)
% hObject    handle to checkbox1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of checkbox1
pars.eta_meas=get(hObject,'Value');
extCoupling_run('setPars',pars);



function edit2_Callback(hObject, eventdata, handles)
% hObject    handle to edit2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global INSTR BEAMLINE
dispval=str2double(get(hObject,'String'));
mw=findcells(BEAMLINE,'Name','MW0X');
mwi=findcells(BEAMLINE,'Index',mw);
if ~isempty(mwi)
  INSTR{mwi}.dispref(3)=dispval*1e-3;
else
  errordlg('INSTR not found for this wirescanner!','extCoupling error');
end


% --- Executes during object creation, after setting all properties.
function edit2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit3_Callback(hObject, eventdata, handles)
% hObject    handle to edit3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

global INSTR BEAMLINE
dispval=str2double(get(hObject,'String'));
mw=findcells(BEAMLINE,'Name','MW1X');
mwi=findcells(BEAMLINE,'Index',mw);
if ~isempty(mwi)
  INSTR{mwi}.dispref(3)=dispval*1e-3;
else
  errordlg('INSTR not found for this wirescanner!','extCoupling error');
end

% --- Executes during object creation, after setting all properties.
function edit3_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit4_Callback(hObject, eventdata, handles)
% hObject    handle to edit4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

global INSTR BEAMLINE
dispval=str2double(get(hObject,'String'));
mw=findcells(BEAMLINE,'Name','MW3X');
mwi=findcells(BEAMLINE,'Index',mw);
if ~isempty(mwi)
  INSTR{mwi}.dispref(3)=dispval*1e-3;
else
  errordlg('INSTR not found for this wirescanner!','extCoupling error');
end


% --- Executes during object creation, after setting all properties.
function edit4_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit5_Callback(hObject, eventdata, handles)
% hObject    handle to edit5 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

global INSTR BEAMLINE
dispval=str2double(get(hObject,'String'));
mw=findcells(BEAMLINE,'Name','MW2X');
mwi=findcells(BEAMLINE,'Index',mw);
if ~isempty(mwi)
  INSTR{mwi}.dispref(3)=dispval*1e-3;
else
  errordlg('INSTR not found for this wirescanner!','extCoupling error');
end


% --- Executes during object creation, after setting all properties.
function edit5_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit5 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit6_Callback(hObject, eventdata, handles)
% hObject    handle to edit6 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

global INSTR BEAMLINE
dispval=str2double(get(hObject,'String'));
mw=findcells(BEAMLINE,'Name','MW4X');
mwi=findcells(BEAMLINE,'Index',mw);
if ~isempty(mwi)
  INSTR{mwi}.dispref(3)=dispval*1e-3;
else
  errordlg('INSTR not found for this wirescanner!','extCoupling error');
end


% --- Executes during object creation, after setting all properties.
function edit6_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit6 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in checkbox2.
function checkbox2_Callback(hObject, eventdata, handles)
% hObject    handle to checkbox2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of checkbox2


% --- Executes on button press in checkbox3.
function checkbox3_Callback(hObject, eventdata, handles)
% hObject    handle to checkbox3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of checkbox3


% --- Executes on button press in checkbox4.
function checkbox4_Callback(hObject, eventdata, handles)
% hObject    handle to checkbox4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of checkbox4


% --- Executes on button press in checkbox5.
function checkbox5_Callback(hObject, eventdata, handles)
% hObject    handle to checkbox5 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of checkbox5


% --- Executes on button press in checkbox6.
function checkbox6_Callback(hObject, eventdata, handles)
% hObject    handle to checkbox6 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of checkbox6

function setWhichWS(handles)
for icb=2:6
  ws(icb-1)=get(handles.(sprintf('checkbox%d',icb)),'Value');
end
extCoupling_run('choosews',find(ws));


% --- Executes on button press in pushbutton13.
function pushbutton13_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton13 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

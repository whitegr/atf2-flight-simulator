function [Skew1,Skew2,Skew3,Skew4] = MakeCouplingCorrectionMultiKnobs( )

% defines 4 coupling correction multiknobs in terms of their maximum
% strengths

  global BEAMLINE ;

  brho = 1.3/0.299792458 ;
  K1LMax5  =  2.5363e-2 ; Bmax5  = K1LMax5  * brho ; % +-  5 amp
  K1LMax20 = 10.2720e-2 ; Bmax20 = K1LMax20 * brho ; % +- 20 amp
  
% sq1

  mag = findcells(BEAMLINE,'Name','QK1X') ;
  ps = BEAMLINE{mag(1)}.PS ;
  [stat,Skew1] = MakeMultiKnob('Skew1', ...
      ['PS(',num2str(ps),').SetPt'],Bmax20/2) ;
  
% sq2

  mag = findcells(BEAMLINE,'Name','QK2X') ;
  ps = BEAMLINE{mag(1)}.PS ;
  [stat,Skew2] = MakeMultiKnob('Skew2', ...
      ['PS(',num2str(ps),').SetPt'],Bmax20/2) ;
  
% sq3

  mag = findcells(BEAMLINE,'Name','QK3X') ;
  ps = BEAMLINE{mag(1)}.PS ;
  [stat,Skew3] = MakeMultiKnob('Skew3', ...
      ['PS(',num2str(ps),').SetPt'],Bmax20/2) ;
  
% sq4

  mag = findcells(BEAMLINE,'Name','QK4X') ;
  ps = BEAMLINE{mag(1)}.PS ;
  [stat,Skew4] = MakeMultiKnob('Skew4', ...
      ['PS(',num2str(ps),').SetPt'],Bmax20/2) ;
  
  
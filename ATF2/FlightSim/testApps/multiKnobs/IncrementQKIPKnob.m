function stat=IncrementQKIPKnob(Knobs,wknob,ksize)
global BEAMLINE PS
persistent reqid

% Get QK PS inds
qk=findcells(BEAMLINE,'Name','QK*X'); qkps=[];
for iqk=1:2:length(qk)
  qkps(end+1)=BEAMLINE{qk(iqk)}.PS;
end

% Get Access Permissions QK PS's
if isempty(reqid)
  [stat reqid]=AccessRequest({[] qkps []});
else
  [stat resp]=AccessRequest('status',reqid);
  if ~resp
    [stat reqid]=AccessRequest({[] qkps []});
  end
end
if stat{1}~=1; return; end;
if ~reqid
  stat{1}=0;
  stat{2}='Access request refused';
  return
end

% Form knob data
Knobs.sq.sig13=Knobs.sq.sig13./max(abs(Knobs.sq.sig13));
Knobs.sq.sig23=Knobs.sq.sig23./max(abs(Knobs.sq.sig23));
Knobs.sq.sig14=Knobs.sq.sig14./max(abs(Knobs.sq.sig14));
Knobs.sq.sig24=Knobs.sq.sig24./max(abs(Knobs.sq.sig24));
maxknob=0.1;
kname={'sig13' 'sig14' 'sig23' 'sig24'};
% Move knobs by requested amount
for iqk=1:4
  PS(qkps(iqk)).SetPt=PS(qkps(iqk)).Ampl+Knobs.sq.(kname{wknob})(iqk)*ksize*maxknob;
end
stat=PSTrim(qkps,1);
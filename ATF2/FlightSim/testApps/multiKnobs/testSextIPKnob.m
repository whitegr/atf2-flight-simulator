function stat=testSextIPKnob(Knobs,wknob,ksize,target)
global BEAMLINE GIRDER FL
warning off MATLAB:lscov:RankDefDesignMat
warning off MATLAB:PSTrim_online
warning off MATLAB:MoverTrim_online
warning off MATLAB:FlHwUpdate_nonFSsim

stat{1}=1;

% Get Girder indicies
for isext=1:5
  sgi(isext)=BEAMLINE{FL.SimModel.magGroup.Sext.dB.ClusterList(isext).index(1)}.Girder;
end

% Get track points
strack=findcells(BEAMLINE,'Name','IEX');
if target==1
  etrack=findcells(BEAMLINE,'Name','MW1IP');
elseif target==2
  etrack=findcells(BEAMLINE,'Name','IP');
else
  etrack=[];
end

if isempty(etrack)
  stat{1}=-1; stat{2}='Unknown requested target';
  return
end

% Condition Model for tracking
[Beam1_IEX Beam0_IEX]=trackCondition(strack,etrack);

% Get Target IP beam data
[data_init txt] = GetIPData(Beam1_IEX,Beam0_IEX,strack,etrack);
fprintf('Initial data: %s\n',txt)

% --- Test effect of desired move
% Form knob data
ik{1}=reshape(Knobs.DispY.smoves./max(abs(Knobs.DispY.smoves)),2,5);
ik{2}=reshape(Knobs.XpY.smoves./max(abs(Knobs.XpY.smoves)),2,5);
ik{3}=reshape(Knobs.WaistY.smoves./max(abs(Knobs.WaistY.smoves)),2,5);
maxknob=1.5e-3;

% Get Initial IP data
idata = GetIPData(Beam1_IEX,Beam0_IEX,strack,etrack);

% Move knobs by requested amount
for isext=1:5
  GIRDER{sgi(isext)}.MoverPos=GIRDER{sgi(isext)}.MoverPos+[ik{wknob}(1,isext) ik{wknob}(2,isext) 0].*ksize.*maxknob;
end

% Get new IP data
data = GetIPData(Beam1_IEX,Beam0_IEX,strack,etrack);

% Display expected change in IP parameters
fprintf('Expected Change in IP parameters:\nDispY: %g\nXpY: %g\nWaistY: %g\n',idata.ydisp-data.ydisp,idata.sigma(2,3)-data.sigma(2,3),...
  idata.ywaist-data.ywaist);

% Restore FS to control system values
FlHwUpdate;


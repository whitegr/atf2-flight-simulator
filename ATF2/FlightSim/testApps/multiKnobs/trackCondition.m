function [Beam1_IEX,Beam0_IEX]=trackCondition(strack,etrack,beta_in,whichData,eta_in)
global BEAMLINE PS FL GIRDER

% Switch off correctors, skew quads, zero girders for tracking purposes
soind=[findcells(BEAMLINE,'Class','XCOR') findcells(BEAMLINE,'Class','YCOR')];
tind=findcells(BEAMLINE,'Tilt');
soind=[soind tind(cellfun(@(x) x.Tilt~=0,{BEAMLINE{tind}}))];
for ind=soind
  if isfield(BEAMLINE{ind},'PS')
    PS(BEAMLINE{ind}.PS).Ampl=0;
  end
end
for igir=1:length(GIRDER)
  if isfield(GIRDER{igir},'MoverPos')
    GIRDER{igir}.MoverPos=[0 0 0];
  end
end

% --- Form correct intial beam for lattice
Initial=FL.SimModel.Initial_IEX;
% No initial dispersion
Initial.x.Twiss.eta=0; Initial.x.Twiss.etap=0;
Initial.y.Twiss.eta=0e-3; Initial.y.Twiss.etap=0e-3;
itwiss_x=Initial.x.Twiss;
itwiss_y=Initial.y.Twiss;
if exist('beta_in') && exist('whichData')
  if ~exist('eta_in') || length(eta_in)~=4
    eta_in=[0 0; 0 0];
  end
  iex=findcells(BEAMLINE,'Name','IEX');
  if isequal(whichData,1) % EXT data match at MW0X
    im=findcells(BEAMLINE,'Name','Mw0X');
  elseif isequal(whichData,2) % IP match point
    im=findcells(BEAMLINE,'Name','IP');
  elseif isequal(whichData,3) % PIP matchpoint
    im=findcells(BEAMLINE,'Name','MW1IP');
  else
    error('unkown match point')
  end
  Tmatch_x.beta=beta_in(1); Tmatch_y.beta=beta_in(2);
  Tmatch_x.alpha=0; Tmatch_y.alpha=0;
  Tmatch_x.eta=eta_in(1,1); Tmatch_y.eta=eta_in(1,2);
  Tmatch_x.etap=eta_in(2,1); Tmatch_y.etap=eta_in(2,2);
  [stat,Tiex]=GetTwiss(im,iex,Tmatch_x,Tmatch_y);
end
    

Beam1_IEX = MakeBeam6DGauss( Initial, 10000, 5, 1 );
Beam0_IEX = Beam1_IEX;
Beam0_IEX.Bunch.x=mean(Beam1_IEX.Bunch.x,2);
Beam0_IEX.Bunch.Q=mean(Beam1_IEX.Bunch.Q,2);
Beam0_IEX.Bunch.stop=mean(Beam1_IEX.Bunch.stop,2);

% Find good initial orbit
[stat instdata]=FlTrackThru(strack,etrack);
nbpm=length(instdata{1});
bpms=findcells(BEAMLINE,'Class','MONI',findcells(BEAMLINE,'Name','MQF9X'),findcells(BEAMLINE,'Name','MQF3FF'));
ibpms=ismember([instdata{1}.Index],bpms);
xmin=fminsearch(@(x) minorbit(x,Beam0_IEX,strack,etrack,nbpm,ibpms),[0 0 0 0 0],optimset('Display','none','MaxFunEvals',20000,'MaxIter',4000));
fprintf('Initial trajectory at IEX: %g / %g / %g / %g / %g(x/x''/y/y''/dE)\n',xmin)
Beam1_IEX.Bunch.x(1,:)=Beam1_IEX.Bunch.x(1,:)+xmin(1); 
Beam1_IEX.Bunch.x(2,:)=Beam1_IEX.Bunch.x(2,:)+xmin(2);
Beam1_IEX.Bunch.x(3,:)=Beam1_IEX.Bunch.x(3,:)+xmin(3);
Beam1_IEX.Bunch.x(4,:)=Beam1_IEX.Bunch.x(4,:)+xmin(4);
Beam1_IEX.Bunch.x(6,:)=Beam1_IEX.Bunch.x(6,:)+xmin(5);
Beam0_IEX = Beam1_IEX;
Beam0_IEX.Bunch.x=mean(Beam1_IEX.Bunch.x,2);
Beam0_IEX.Bunch.Q=mean(Beam1_IEX.Bunch.Q,2);
Beam0_IEX.Bunch.stop=mean(Beam1_IEX.Bunch.stop,2);

function chi2 = minorbit(x,beam,istart,iend,nbpm,bpmind)

beam.Bunch.x(1,:)=beam.Bunch.x(1,:)+x(1);
beam.Bunch.x(2,:)=beam.Bunch.x(2,:)+x(2);
beam.Bunch.x(3,:)=beam.Bunch.x(3,:)+x(3);
beam.Bunch.x(4,:)=beam.Bunch.x(4,:)+x(4);
beam.Bunch.x(6,:)=beam.Bunch.x(6,:)+x(5);

[stat,B,instdata]=TrackThru(istart,iend,beam,1,1,0);
xdat=[instdata{1}.x]; ydat=[instdata{1}.y];
if length(xdat)<nbpm
  xdat(end+1:nbpm)=5e-3;
  ydat(end+1:nbpm)=5e-3;
end
chi2=sum(xdat(bpmind).^2+ydat(bpmind).^2)./sum(ones(1,34).*1e-6.^2);
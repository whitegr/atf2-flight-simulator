function sreq_wait(cmd)
global FL
persistent has_stopped
% stat=sreq;

if isempty(has_stopped)
  has_stopped=false;
end

% restart update process request
if exist('cmd','var') && isequal(cmd,'restart') && has_stopped
  sreq('clear');
  start(FL.t_multiKnobs);
  return
end

% If update process running, stop and clear buffer before continuing
stop(FL.t_multiKnobs);
wait(FL.t_multiKnobs); pause(0.1)
has_stopped=true;
if isempty(findstr(FL.mode,'trusted'))
  sockrw('readchar','cas');
end
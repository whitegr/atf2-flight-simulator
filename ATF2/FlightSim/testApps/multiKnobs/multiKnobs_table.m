function varargout = multiKnobs_table(varargin)
% MULTIKNOBS_TABLE M-file for multiKnobs_table.fig
%      MULTIKNOBS_TABLE, by itself, creates a new MULTIKNOBS_TABLE or raises the existing
%      singleton*.
%
%      H = MULTIKNOBS_TABLE returns the handle to a new MULTIKNOBS_TABLE or the handle to
%      the existing singleton*.
%
%      MULTIKNOBS_TABLE('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in MULTIKNOBS_TABLE.M with the given input arguments.
%
%      MULTIKNOBS_TABLE('Property','Value',...) creates a new MULTIKNOBS_TABLE or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before multiKnobs_table_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to multiKnobs_table_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help multiKnobs_table

% Last Modified by GUIDE v2.5 23-Jun-2009 16:01:44

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @multiKnobs_table_OpeningFcn, ...
                   'gui_OutputFcn',  @multiKnobs_table_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before multiKnobs_table is made visible.
function multiKnobs_table_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to multiKnobs_table (see VARARGIN)

% Choose default command line output for multiKnobs_table
handles.output = handles;

% Update data
edit1_Callback(handles.edit1,[],handles);

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes multiKnobs_table wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = multiKnobs_table_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Delete Table
function pushbutton1_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global FL
if ~strcmp(questdlg('Delete this table?','Table Delete Request'),'Yes')
  return
end
if strcmp(get(handles.edit1,'String'),'Default')
  errordlg('Cannot delete the default table','Delete table error')
  return
end
stat=multiKnobsFn('DeleteTable',get(handles.edit1,'String'));
if stat{1}~=1
  errordlg(stat{2},'Delete Table Error')
  return
end
[stat,pars]=multiKnobsFn('GetPars');
set(FL.Gui.multiKnobs_edit.popupmenu2,'String',pars.tableNames);
if isfield(FL.Gui,'multiKnobs_edit') && ishandle(FL.Gui.multiKnobs_edit.figure1)
  set(FL.Gui.multiKnobs_edit.popupmenu2,'Value',1)
end
set(handles.edit1,'String','NewTable')
edit1_Callback(handles.edit1,[],handles)

% --- Quit
function pushbutton6_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton6 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global FL
if isfield(FL.Gui,'multiKnobs_edit') && ishandle(FL.Gui.multiKnobs_edit.figure1)
  multiKnobs_edit('pushbutton11_Callback',FL.Gui.multiKnobs_edit.pushbutton11,[],FL.Gui.multiKnobs_edit)
  [stat,pars]=multiKnobsFn('GetPars');
  set(FL.Gui.multiKnobs_edit.popupmenu2,'String',pars.tableNames);
  tlist=get(FL.Gui.multiKnobs_edit.popupmenu2,'String');
  if any(strcmp(tlist,get(handles.edit1,'String')))
    set(FL.Gui.multiKnobs_edit.popupmenu2,'Value',find(strcmp(tlist,get(handles.edit1,'String'))))
  end
end
guiCloseFn('multiKnobs_table',handles);


% --- Executes on button press in pushbutton7.
function pushbutton7_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton7 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Interpolation method
function popupmenu1_Callback(hObject, eventdata, handles)
% hObject    handle to popupmenu1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

klist=get(hObject,'String');
tStruc.interpMethod=klist{get(hObject,'Value')};
stat=multiKnobsFn('EditTableStruc',get(handles.edit1,'String'),tStruc);
if stat{1}~=1
  errordlg(stat{2},'Error editing table structure')
end
edit1_Callback(handles.edit1,[],handles);


% --- Executes during object creation, after setting all properties.
function popupmenu1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to popupmenu1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Use extrapolation
function checkbox1_Callback(hObject, eventdata, handles)
% hObject    handle to checkbox1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

tStruc.doExtrap=get(hObject,'Value');
stat=multiKnobsFn('EditTableStruc',get(handles.edit1,'String'),tStruc);
if stat{1}~=1
  errordlg(stat{2},'Error editing table structure')
end
edit1_Callback(handles.edit1,[],handles);


function edit2_Callback(hObject, eventdata, handles)
% hObject    handle to edit2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit2 as text
%        str2double(get(hObject,'String')) returns contents of edit2 as a double
if isnan(str2double(get(hObject,'String')))
  errordlg('Bad input','Knob Value Error')
  set(hObject,'String','')
end

% --- Executes during object creation, after setting all properties.
function edit2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit3_Callback(hObject, eventdata, handles)
% hObject    handle to edit3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit3 as text
%        str2double(get(hObject,'String')) returns contents of edit3 as a double
if isnan(str2double(get(hObject,'String')))
  errordlg('Bad input','Lookup Value Error')
  set(hObject,'String','')
end

% --- Executes during object creation, after setting all properties.
function edit3_CreateFcn(hObject, eventdata, handles) %#ok<*INUSD>
% hObject    handle to edit3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- add button
function pushbutton5_Callback(hObject, eventdata, handles) %#ok<*INUSL,*DEFNU>
% hObject    handle to pushbutton5 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
kVal=str2double(get(handles.edit2,'String'));
lVal=str2double(get(handles.edit3,'String'));
if isnan(kVal) || isnan(lVal)
  errordlg('Bad knob value and/or lookup value inputs','Add row error')
  return
end
tStruc.kVal=kVal;
tStruc.lVal=lVal;
klist=get(handles.popupmenu1,'String');
tStruc.interpMethod=klist{get(handles.popupmenu1,'Value')};
tStruc.doExtrap=get(handles.checkbox1,'Value');
stat=multiKnobsFn('MakeTableEntry',get(handles.edit1,'String'),tStruc);
if stat{1}~=1
  errordlg(stat{2},'Make Table Entry Error')
  return
end
edit1_Callback(handles.edit1,[],handles);

% --- Executes on selection change in listbox1.
function listbox1_Callback(hObject, eventdata, handles)
% hObject    handle to listbox1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = get(hObject,'String') returns listbox1 contents as cell array
%        contents{get(hObject,'Value')} returns selected item from listbox1


% --- Executes during object creation, after setting all properties.
function listbox1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to listbox1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: listbox controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Delete row
function pushbutton2_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
if ~strcmp(questdlg('Delete Table entry/entries?','Table entry delete request'),'Yes')
  return
end
dvals=get(handles.listbox1,'Value');
stat=multiKnobsFn('DeleteTableEntry',get(handles.edit1,'String'),dvals);
if stat{1}~=1
  errodlg(stat{2},'Table Entry Deletion error')
end
set(handles.listbox1,'Value',1)
edit1_Callback(handles.edit1,[],handles)

% --- Display data
function pushbutton3_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global FL

if isfield(FL.Gui,'multiKnobs_table_plot1') && ishandle(FL.Gui.multiKnobs_table_plot1)
  figure(FL.Gui.multiKnobs_table_plot1)
else
  FL.Gui.multiKnobs_table_plot1=figure;
end
[stat pars]=multiKnobsFn('GetPars');
tname=get(handles.edit1,'String');
plot(pars.table.(tname).kVal,pars.table.(tname).lVal,'bo')
title('Lookup table values')
xlabel('Knob Value')
ylabel('Lookup Value')
if get(handles.togglebutton1,'Value')
  hold on
  xx=linspace(pars.table.(tname).kVal(1),pars.table.(tname).kVal(end),10000);
  yy=interp1(pars.table.(tname).kVal,pars.table.(tname).lVal,xx,pars.table.(tname).interpMethod);
  plot(xx,yy,'r')
  hold off
end

% --- Update data from figure
function pushbutton4_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global FL
if isfield(FL.Gui,'multiKnobs_table_plot1') && ishandle(FL.Gui.multiKnobs_table_plot1)
  set(handles.listbox1,'Value',1)
  fhan=get(FL.Gui.multiKnobs_table_plot1,'Children');
  ahan=get(fhan,'Children');
  data(1,:)=get(ahan{end},'XData');
  data(2,:)=get(ahan{end},'Ydata');
  newdata(1,:)=data(1,~isnan(data(2,:)));
  newdata(2,:)=data(2,~isnan(data(2,:)));
  stat=multiKnobsFn('SetTableData',get(handles.edit1,'String'),newdata);
  if stat{1}~=1
    errordlg(stat{2},'Table Data Edit Error')
    return
  end
  pushbutton3_Callback(handles.pushbutton3,[],handles);
  [stat, pars]=multiKnobsFn('GetPars');
  tname=get(handles.edit1,'String');
  set(handles.listbox1,'Value',1)
  set(handles.listbox1,'String',pars.table.(tname).text);
end


% --- Show interpolation data
function togglebutton1_Callback(hObject, eventdata, handles)
% hObject    handle to togglebutton1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% global FL
% 
% if get(hObject,'Value') && isfield(FL.Gui,'multiKnobs_table_plot1') && ishandle(FL.Gui.multiKnobs_table_plot1)
%   figure(FL.Gui.multiKnobs_table_plot1)
%   hold on
%   xx=linspace(pars.kVal(1),pars.kVal(end),10000);
%   yy=interp1(pars.kVal,pars.lVal,xx,pars.interpMethod);
%   plot(xx,yy,'r')
%   hold off
% elseif isfield(FL.Gui,'multiKnobs_table_plot1') && ishandle(FL.Gui.multiKnobs_table_plot1)
%   fhan=get(FL.Gui.multiKnobs_table_plot1,'Children');
%   ahan=get(fhan,'Children');
%   if length(ahan)==2
%     delete(ahan(1))
%   end
% end
pushbutton3_Callback(handles.pushbutton3,[],handles);


function edit1_Callback(hObject, eventdata, handles)
% hObject    handle to edit1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
if ~isempty(regexp(get(hObject,'String'),'\W', 'once'))
  errordlg('No spaces or symbols allowed in name')
  return
end
[stat pars]=multiKnobsFn('GetPars');
tableName=get(hObject,'String');
intlist=get(handles.popupmenu1,'String');
if any(strcmp(tableName,pars.tableNames)) && ~strcmp(pars.table.(tableName).text{1},'<none>')
  set(handles.listbox1,'String',pars.table.(tableName).text)
  set(handles.popupmenu1,'Value',find(strcmp(intlist,pars.table.(tableName).interpMethod)))
  set(handles.checkbox1,'Value',pars.table.(tableName).doExtrap)
else
  set(handles.listbox1,'String','<none>')
  set(handles.popupmenu1,'Value',2)
  set(handles.checkbox1,'Value',0)
end

% --- Executes during object creation, after setting all properties.
function edit1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes when user attempts to close figure1.
function figure1_CloseRequestFcn(hObject, eventdata, handles)
% hObject    handle to figure1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global FL
if isfield(FL.Gui,'multiKnobs_edit') && ishandle(FL.Gui.multiKnobs_edit)
  multiKnobs_edit('pushbutton11_Callback',FL.Gui.multiKnobs_edit.pushbutton11,[],FL.Gui.multiKnobs_edit)
  tlist=get(FL.Gui.multiKnobs_edit.popupmenu2,'String');
  if any(strcmp(tlist,get(handles.edit1,'String')))
    set(FL.Gui.multiKnobs_edit.popupmenu2,'Value',find(strcmp(tlist,get(handles.edit1,'String'))))
  end
end
guiCloseFn('multiKnobs_table',handles);

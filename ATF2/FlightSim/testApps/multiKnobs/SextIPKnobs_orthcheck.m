function data = SextIPKnobs_orthcheck
global GIRDER PS FL
ip=2;
% Make knobs
GenerateSextIPKnobs(ip,0,0,0);

% Test Mover alignment effect
malign=[10e-6 50e-6 100e-6 200e-6 500e-6];
sgind=FL.SimModel.Gind.Sext;
for isext=1:length(sgind)
  ginit{isext}=GIRDER{sgind(isext)}.MoverPos;
end
for ia=1:length(malign)
  for itry=1:50
    for isext=1:length(sgind)
      GIRDER{sgind(isext)}.MoverPos=ginit{isext}+[malign(ia)*randn 0 0];
    end
    [stat Knobs orthdata]=GenerateSextIPKnobs(ip,2,0,1);
    odat(itry,:)=orthdata;
  end
  data.xmalign(ia,:)=mean(odat);
  data.xmalign_rms(ia,:)=std(odat);
end
for isext=1:length(sgind)
  GIRDER{sgind(isext)}.MoverPos=ginit{isext};
end
malign=[10e-6 50e-6 100e-6 200e-6 500e-6];
sgind=FL.SimModel.Gind.Sext;
for isext=1:length(sgind)
  ginit{isext}=GIRDER{sgind(isext)}.MoverPos;
end
for ia=1:length(malign)
  for itry=1:50
    for isext=1:length(sgind)
      GIRDER{sgind(isext)}.MoverPos=ginit{isext}+[0 malign(ia)*randn 0];
    end
    [stat Knobs orthdata]=GenerateSextIPKnobs(ip,2,0,1);
    odat(itry,:)=orthdata;
  end
  data.ymalign(ia,:)=mean(odat);
  data.ymalign_rms(ia,:)=std(odat);
end
for isext=1:length(sgind)
  GIRDER{sgind(isext)}.MoverPos=ginit{isext};
end
malign=[0.1e-3 0.2e-3 0.5e-3 1e-3 5e-3];
sgind=FL.SimModel.Gind.Sext;
for isext=1:length(sgind)
  ginit{isext}=GIRDER{sgind(isext)}.MoverPos;
end
for ia=1:length(malign)
  for itry=1:50
    for isext=1:length(sgind)
      GIRDER{sgind(isext)}.MoverPos=ginit{isext}+[0 0 malign(ia)*randn];
    end
    [stat Knobs orthdata]=GenerateSextIPKnobs(ip,2,0,1);
    odat(itry,:)=orthdata;
  end
  data.tmalign(ia,:)=mean(odat);
  data.tmalign_rms(ia,:)=std(odat);
end
for isext=1:length(sgind)
  GIRDER{sgind(isext)}.MoverPos=ginit{isext};
end
% Test Strength errors
strerr=[1e-4 1e-3 5e-3 1e-2 5e-2];
spind=sort([FL.SimModel.PSind.Sext FL.SimModel.PSind.Quad]);
psinit=[PS(spind).Ampl];
for ia=1:length(strerr)
  for itry=1:50
    for isext=1:length(spind)
      PS(spind(isext)).Ampl=psinit(isext)*(1+strerr(ia)*randn);
    end
    [stat Knobs orthdata]=GenerateSextIPKnobs(ip,2,0,1);
    odat(itry,:)=orthdata;
  end
  data.strerr(ia,:)=mean(odat);
  data.strerr_rms(ia,:)=std(odat);
end
for isext=1:length(spind)
  PS(spind(isext)).Ampl=psinit(isext);
end
save
function stat=sreq(newreq,forcereq)
% SREQ handle multiple server requests
persistent reqid
stat{1}=1;

if isempty(reqid) && ~iscell(reqid)
  reqid={};
end

if exist('newreq','var')
  if strcmp(newreq,'clear') && exist('forcereq','var')
    reqid={forcereq};
    return
  elseif strcmp(newreq,'clear')
    reqid={};
    return
  end
  if any(strcmp(newreq,reqid))
    reqid={reqid{~strcmp(newreq,reqid)}};
  else
    reqid{end+1}=newreq;
  end
end

if ~isempty(reqid)
  stat{1}=-1;
  stat{2}=reqid;
end
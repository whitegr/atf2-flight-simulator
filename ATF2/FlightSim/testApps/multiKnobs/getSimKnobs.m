function [stat Knobs]=getSimKnobs %#ok<STOUT>
stat{1}=1; Knobs=[];
resp=questdlg('Calibrate with online model or use pre-configured simulation Knobs?','Cal Choice','Online','Pre-configured','Cancel','Cancel');
switch resp
  case 'Online'
    stat{1}=0;
    return
  case 'Cancel'
    stat{1}=-1;
    stat{2}='User Canceled';
    return
end

[fn,pn]=uigetfile('testApps/multiKnobs/simKnobs_*.mat','Choose Simulated Knobs');
if isequal(fn,0)
  stat{1}=-1;
  stat{2}='User Canceled';
  return
end
if isempty(whos('-file',fullfile(pn,fn),'Knobs'))
  stat{1}=-1;
  stat{2}='No Knobs definition in this file';
  return
end
load(fullfile(pn,fn),'Knobs')
stat{2}=fn;

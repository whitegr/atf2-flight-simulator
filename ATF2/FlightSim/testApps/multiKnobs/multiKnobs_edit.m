function varargout = multiKnobs_edit(varargin)
% MULTIKNOBS_EDIT M-file for multiKnobs_edit.fig
%      MULTIKNOBS_EDIT, by itself, creates a new MULTIKNOBS_EDIT or raises the existing
%      singleton*.
%
%      H = MULTIKNOBS_EDIT returns the handle to a new MULTIKNOBS_EDIT or the handle to
%      the existing singleton*.
%
%      MULTIKNOBS_EDIT('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in MULTIKNOBS_EDIT.M with the given input arguments.
%
%      MULTIKNOBS_EDIT('Property','Value',...) creates a new MULTIKNOBS_EDIT or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before multiKnobs_edit_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to multiKnobs_edit_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help multiKnobs_edit

% Last Modified by GUIDE v2.5 29-Jun-2009 11:33:19

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @multiKnobs_edit_OpeningFcn, ...
                   'gui_OutputFcn',  @multiKnobs_edit_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before multiKnobs_edit is made visible.
function multiKnobs_edit_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to multiKnobs_edit (see VARARGIN)

% Choose default command line output for multiKnobs_edit
handles.output = handles;

% Fill table popupmenu
[stat pars]=multiKnobsFn('GetPars');
set(handles.popupmenu2,'String',pars.tableNames)

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes multiKnobs_edit wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = multiKnobs_edit_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Save and Quit
function pushbutton6_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton6 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global FL
try
  if isfield(FL.Gui,'multiKnobs') && ishandle(FL.Gui.multiKnobs.figure1)
    [stat pars]=multiKnobsFn('GetPars');
    set(FL.Gui.multiKnobs.popupmenu1,'String',pars.knobNames)
    wknob=strcmp(get(handles.edit4,'String'),pars.knobNames);
    if any(wknob)
      set(FL.Gui.multiKnobs.popupmenu1,'Value',find(wknob))
    end
  end
  % Save data to server
  sreq_wait;
  shan=msgbox('Saving data to server...');
  stat=multiKnobsFn('save');
  if ishandle(shan); delete(shan); end;
  if stat{1}~=1 && length(stat)==2 && iscell(stat)
    errordlg(stat{2},'Server save error')
  end
  sreq_wait('restart')
%   if isfield(FL.Gui,'multiKnobs') && ishandle(FL.Gui.multiKnobs.figure1)
%     multiKnobs('popupmenu1_Callback',FL.Gui.multiKnobs.popupmenu1,[],FL.Gui.multiKnobs)
%   end
  guiCloseFn('multiKnobs_edit',handles);
catch
  warning(lasterr)
  if exist('shan','var') && ishandle(shan); delete(shan); end;
  errordlg('Save failed, restart window to retry','save error')
  delete(handles.figure1)
end

% --- Cancel knob edit
function pushbutton7_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton7 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% edit knob name
function edit4_Callback(hObject, eventdata, handles)
% hObject    handle to edit4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
if ~isempty(regexp(get(hObject,'String'),'\W', 'once'))
  errordlg('No spaces or symbols allowed in name','Name error')
  return
end
% Not allowed names
if any(strcmp(get(hObject,'String'),{'table' 'knobNames' 'lastKnob' 'tableNames'}))
  errordlg('This name not allowed- pick another','Name error')
  return
end
pushbutton11_Callback(handles.pushbutton11,[],handles);

% --- Executes during object creation, after setting all properties.
function edit4_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in radiobutton2.
function radiobutton2_Callback(hObject, eventdata, handles)
% hObject    handle to radiobutton2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of radiobutton2


% --- Executes on selection change in popupmenu1.
function popupmenu1_Callback(hObject, eventdata, handles)
% hObject    handle to popupmenu1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = get(hObject,'String') returns popupmenu1 contents as cell array
%        contents{get(hObject,'Value')} returns selected item from popupmenu1


% --- Executes during object creation, after setting all properties.
function popupmenu1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to popupmenu1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit1_Callback(hObject, eventdata, handles)
% hObject    handle to edit1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit1 as text
%        str2double(get(hObject,'String')) returns contents of edit1 as a double
if isnan(str2double(get(hObject,'String')))
  errordlg('Bad input for Type Index','Multiknob edit error')
  set(hObject,'String',[])
  guidata(hObject, handles);
end

% --- Executes during object creation, after setting all properties.
function edit1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Get index from viewer
function pushbutton4_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global FL BEAMLINE GIRDER

ktype=get(handles.popupmenu1,'String');
% Launch viewer if not already there
if ~isfield(FL.Gui,'BeamlineViewer') ||~isfield(FL.Gui.BeamlineViewer,'figure1') ||  ~ishandle(FL.Gui.BeamlineViewer.figure1)
  FL.Gui.BeamlineViewer=BeamlineViewer;
else
  bl=get(FL.Gui.BeamlineViewer.listbox1,'String');
  wbl=get(FL.Gui.BeamlineViewer.listbox1,'Value');
  ibl=str2double(regexp(bl{wbl(1)},'^\d+','match'));
  wk=ktype{get(handles.popupmenu1,'Value')};
  if strcmp(wk,'GIRDER')
    wk='Girder';
    if ~isfield(GIRDER{BEAMLINE{ibl}.(wk)},'Mover')
      errordlg('No Mover on this girder','Add Control Error')
      return
    end
  end
  if isfield(BEAMLINE{ibl},wk)
    set(handles.edit1,'String',num2str(BEAMLINE{ibl}.(wk)))
  else
    errordlg(['No ',ktype{get(handles.popupmenu1,'Value')},' field for this element'],'BeamlineViewer Select Error')
    set(handles.edit1,'String','')
  end
end


function edit2_Callback(hObject, eventdata, handles)
% hObject    handle to edit2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit2 as text
%        str2double(get(hObject,'String')) returns contents of edit2 as a double


% --- Executes during object creation, after setting all properties.
function edit2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit3_Callback(hObject, eventdata, handles)
% hObject    handle to edit3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit3 as text
%        str2double(get(hObject,'String')) returns contents of edit3 as a double


% --- Executes during object creation, after setting all properties.
function edit3_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in pushbutton5.
function pushbutton5_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton5 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- New table
function pushbutton3_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global FL
FL.Gui.multiKnobs_table=multiKnobs_table;

% --- Executes on button press in radiobutton1.
function radiobutton1_Callback(hObject, eventdata, handles)
% hObject    handle to radiobutton1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
if get(hObject,'Value')
  set(handles.radiobutton3,'Value',0)
else
  set(handles.radiobutton3,'Value',1)
end



% --- Executes on selection change in Knob Control list
function listbox1_Callback(hObject, eventdata, handles)
% hObject    handle to listbox1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
[stat pars]=multiKnobsFn('GetPars');
thisKnobName=get(handles.edit4,'String');
if ~any(strcmp(thisKnobName,pars.knobNames))
  return
end
ic=get(hObject,'Value');
klist=get(hObject,'String');
if strcmp(klist{ic},'<none>')
  return
end
if strcmp(pars.(thisKnobName).control(ic).method,'table')
  set(handles.radiobutton1,'Value',1); set(handles.radiobutton3,'Value',0)
  tnames=get(handles.popupmenu2,'String');
  wt=find(strcmp(pars.(thisKnobName).control(ic).table,tnames));
  if isempty(wt)
    errordlg('This table name no longer exists','Table error')
    return
  end
  set(handles.popupmenu2,'Value',wt);
else
  set(handles.radiobutton1,'Value',0); set(handles.radiobutton3,'Value',1)
  set(handles.edit5,'String',num2str(pars.(thisKnobName).control(ic).exponent))
  set(handles.edit6,'String',num2str(pars.(thisKnobName).control(ic).scale))
end
if strcmp(pars.(thisKnobName).control(ic).type,'PS')
  set(handles.popupmenu1,'Value',1)
elseif strcmp(pars.(thisKnobName).control(ic).type,'GIRDER')
  set(handles.popupmenu1,'Value',2)
end
set(handles.edit1,'String',num2str(pars.(thisKnobName).control(ic).index))

% --- Executes during object creation, after setting all properties.
function listbox1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to listbox1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: listbox controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Delete control entry
function pushbutton2_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
klist=get(handles.listbox1,'String');
ic=get(handles.listbox1,'Value');
if length(klist)<2
  errordlg('Cannot delete all controls','Control delete error')
  return
end
stat=multiKnobsFn('DeleteControl',get(handles.edit4,'String'),ic);
if stat{1}~=1
  errordlg(stat{2},'Control delete error')
  return
end
pushbutton11_Callback(handles.pushbutton11,[],handles);

% --- Executes on button press in radiobutton3.
function radiobutton3_Callback(hObject, eventdata, handles)
% hObject    handle to radiobutton3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of radiobutton3
if get(hObject,'Value')
  set(handles.radiobutton1,'Value',0)
else
  set(handles.radiobutton1,'Value',1)
end


function edit5_Callback(hObject, eventdata, handles)
% hObject    handle to edit5 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit5 as text
%        str2double(get(hObject,'String')) returns contents of edit5 as a double
if isnan(str2double(get(hObject,'String')))
  errordlg('Bad input for Type Index','Multiknob edit error')
  set(hObject,'String',[])
  guidata(hObject, handles);
end

% --- Executes during object creation, after setting all properties.
function edit5_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit5 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit6_Callback(hObject, eventdata, handles)
% hObject    handle to edit6 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit6 as text
%        str2double(get(hObject,'String')) returns contents of edit6 as a double
if isnan(str2double(get(hObject,'String')))
  errordlg('Bad input for Type Index','Multiknob edit error')
  set(hObject,'String',[])
  guidata(hObject, handles);
end

% --- Executes during object creation, after setting all properties.
function edit6_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit6 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in popupmenu2.
function popupmenu2_Callback(hObject, eventdata, handles)
% hObject    handle to popupmenu2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = get(hObject,'String') returns popupmenu2 contents as cell array
%        contents{get(hObject,'Value')} returns selected item from popupmenu2


% --- Executes during object creation, after setting all properties.
function popupmenu2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to popupmenu2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Edit table
function pushbutton8_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton8 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global FL
klist=get(handles.popupmenu2,'String');
FL.Gui.multiKnobs_table=multiKnobs_table;
set(FL.Gui.multiKnobs_table.edit1,'String',klist{get(handles.popupmenu2,'Value')})
multiKnobs_table('edit1_Callback',FL.Gui.multiKnobs_table.edit1,[],FL.Gui.multiKnobs_table)

% --- Add new control
function pushbutton9_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton9 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global GIRDER
typeList=get(handles.popupmenu1,'String');
controlStruc.type=typeList{get(handles.popupmenu1,'Value')};
controlStruc.index=str2double(get(handles.edit1,'String'));
if strcmp(controlStruc.type,'GIRDER') && ~isfield(GIRDER{controlStruc.index},'Mover')
  errordlg('No Mover for this girder','Add control error')
  return
end
controlStruc.dof=get(handles.popupmenu3,'Value');
if get(handles.radiobutton1,'Value')
  controlStruc.method='table';
  tableList=get(handles.popupmenu2,'String');
  controlStruc.table=tableList{get(handles.popupmenu2,'Value')};
else
  controlStruc.method='expScale';
  controlStruc.exponent=str2double(get(handles.edit5,'String'));
  controlStruc.scale=str2double(get(handles.edit6,'String'));
end
stat=multiKnobsFn('MakeControl',get(handles.edit4,'String'),controlStruc);
if stat{1}~=1
  errordlg(stat{2},'Make control error')
  return
end
pushbutton11_Callback(handles.pushbutton11,[],handles);

% --- delete knob
function pushbutton10_Callback(hObject, eventdata, handles) %#ok<*INUSD>
% hObject    handle to pushbutton10 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
if ~strcmp(questdlg('Delete this knob?','Knob deletion request'),'Yes')
  return
end
[stat pars]=multiKnobsFn('GetPars');
knobName=get(handles.edit4,'String');
if strcmp(knobName,'Default')
  errordlg('Cannot delete default knob','Knob delete error')
  return
end
if ~any(strcmp(knobName,pars.knobNames))
  errordlg('No store knob names found which match this','Knob delete error')
  return
end
stat=multiKnobsFn('Delete',knobName);
if stat{1}~=1; errordlg(stat{2},'Knob delete error'); return; end;
set(handles.edit4,'String','NewKnob');
pushbutton11_Callback(handles.pushbutton11,[],handles);
if isfield(FL.Gui,'multiKnobs') && ishandle(FL.Gui.multiKnobs.figure1)
  [stat pars]=multiKnobsFn('GetPars');
  set(FL.Gui.multiKnobs.popupmenu1,'Value',1)
  set(FL.Gui.multiKnobs.popupmenu1,'String',pars.knobNames)
  multiKnobs('popupmenu1_Callback',FL.Gui.multiKnobs.popupmenu1,[],FL.Gui.multiKnobs)
end

% --- Reset Knob Settings
function pushbutton11_Callback(hObject, eventdata, handles) %#ok<*INUSL,*DEFNU>
% hObject    handle to pushbutton11 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
if isempty(get(handles.edit4,'String'))
  set(handles.edit4,'String','Default')
end
if ~isempty(regexp(get(handles.edit4,'String'),'\W', 'once'))
  errordlg('No spaces or symbols allowed in name')
  return
end
% Get list of knob names
[stat pars]=multiKnobsFn('GetPars');
thisKnobName=get(handles.edit4,'String');
 % If user defined knob, shouldn't be editing here
if exist(['userMK_',thisKnobName],'file')
  errordlg('This knob is classified as a user defined knob and has an associated m file that should be used for editing, not this panel','Knob Error')
  return
end
% If knob already exists, fill GUI fields
if any(strcmp(thisKnobName,pars.knobNames))
  set(handles.listbox1,'String',pars.(thisKnobName).knobControls_txt)
  set(handles.listbox1,'Value',1)
  listbox1_Callback(handles.listbox1,[],handles)
else
  set(handles.listbox1,'String','<none>')
  set(handles.listbox1,'Value',1)
end
 


% --- Executes when user attempts to close figure1.
function figure1_CloseRequestFcn(hObject, eventdata, handles)
% hObject    handle to figure1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global FL
try
  if isfield(FL.Gui,'multiKnobs') && ishandle(FL.Gui.multiKnobs)
    [stat pars]=multiKnobsFn('GetPars');
    set(FL.Gui.multiKnobs.popupmenu1,'String',pars.knobNames)
    wknob=strcmp(get(handles.edit4,'String'),pars.knobNames);
    if any(wknob)
      set(FL.Gui.multiKnobs.popupmenu1,'Value',find(wknob))
    end
  end
  % Save data to server
  shan=msgbox('Saving data to server...');
  stat=multiKnobsFn('save');
  if ishandle(shan); delete(shan); end;
  if stat{1}~=1
    errordlg(stat{2},'Server save error')
  end
  guiCloseFn('multiKnobs_edit',handles);
catch
  warning(lasterr)
  if exist('shan','var') && ishandle(shan); delete(shan); end;
  delete(handles.figure1)
end

% --- Executes on selection change in popupmenu3.
function popupmenu3_Callback(hObject, eventdata, handles)
% hObject    handle to popupmenu3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = get(hObject,'String') returns popupmenu3 contents as cell array
%        contents{get(hObject,'Value')} returns selected item from popupmenu3


% --- Executes during object creation, after setting all properties.
function popupmenu3_CreateFcn(hObject, eventdata, handles)
% hObject    handle to popupmenu3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes during object creation, after setting all properties.
function figure1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to figure1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

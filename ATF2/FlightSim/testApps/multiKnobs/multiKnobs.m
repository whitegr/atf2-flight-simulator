function varargout = multiKnobs(varargin)
% MULTIKNOBS M-file for multiKnobs.fig
%      MULTIKNOBS, by itself, creates a new MULTIKNOBS or raises the existing
%      singleton*.
%
%      H = MULTIKNOBS returns the handle to a new MULTIKNOBS or the handle to
%      the existing singleton*.
%
%      MULTIKNOBS('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in MULTIKNOBS.M with the given input arguments.
%
%      MULTIKNOBS('Property','Value',...) creates a new MULTIKNOBS or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before multiKnobs_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to multiKnobs_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help multiKnobs

% Last Modified by GUIDE v2.5 19-Jan-2012 00:50:07

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @multiKnobs_OpeningFcn, ...
                   'gui_OutputFcn',  @multiKnobs_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before multiKnobs is made visible.
function multiKnobs_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to multiKnobs (see VARARGIN)
global FL

% Choose default command line output for multiKnobs
handles.output = handles;

% Restore options
restoreList={'edit2','String',0};
guiRestoreFn('multiKnobs',handles,restoreList);

% Get Knobs
[stat pars]=multiKnobsFn('GetPars');
set(handles.popupmenu1,'String',pars.knobNames)
if ismember(pars.lastKnob,pars.knobNames)
  set(handles.popupmenu1,'Value',find(ismember(pars.knobNames,pars.lastKnob)))
else
  set(handles.popupmenu1,'Value',1)
end
popupmenu1_Callback(handles.popupmenu1,[],handles);

% Create and Start readback value monitoring process
if ~isfield(FL,'t_multiKnobs') || ~strcmp(FL.t_multiKnobs,'on')
  FL.t_multiKnobs=timer('TimerFcn','multiKnobs(''timerFcn'')','Period',3,'StopFcn','multiKnobs(''timerStopFcn'')',...
    'TasksToExecute',15e10,'ExecutionMode','fixedSpacing','StartDelay',3);
  start(FL.t_multiKnobs)
end

% Update handles structure
guidata(hObject, handles);

% Timer function
function timerFcn
global FL
try
  stat=sreq; if stat{1}~=1; error('server proccess busy'); end;
  sreq('multiKnobsTimer');
  if isfield(FL.Gui,'multiKnobs') && ishandle(FL.Gui.multiKnobs.figure1)
    knames=get(FL.Gui.multiKnobs.popupmenu1,'String');
    [stat readback]=multiKnobsFn('GetReadback',knames{get(FL.Gui.multiKnobs.popupmenu1,'Value')});
    if stat{1}~=1 || any(isnan(readback))
      if isequal(stat{2},'No Cal Data')
        set(FL.Gui.multiKnobs.text4,'String','---')
      end
      set(FL.Gui.multiKnobs.text2,'BackgroundColor','red');
      set(FL.Gui.multiKnobs.text3,'BackgroundColor','red');
      set(FL.Gui.multiKnobs.text6,'BackgroundColor','red');
    else
      set(FL.Gui.multiKnobs.text2,'BackgroundColor','white');
      set(FL.Gui.multiKnobs.text3,'BackgroundColor','white');
      set(FL.Gui.multiKnobs.text6,'BackgroundColor','white');
      units=get(FL.Gui.multiKnobs.popupmenu2,'String');
      thisUnits=str2double(units{get(FL.Gui.multiKnobs.popupmenu2,'Value')});
      set(FL.Gui.multiKnobs.text2,'String',num2str(readback(1)/thisUnits))
      set(FL.Gui.multiKnobs.text3,'String',num2str(readback(2)/thisUnits))
      if length(readback)>=4
        set(FL.Gui.multiKnobs.text6,'String',[num2str(readback(3)/thisUnits,2) ' : ' num2str(readback(4)/thisUnits,2)]);
      else
        set(FL.Gui.multiKnobs.text6,'String','---')
      end
    end
  end
  sreq('multiKnobsTimer');
  uiresume(FL.Gui.multiKnobs.figure1)
catch
%   warning(lasterr)
  stat=sreq;
  if stat{1}~=1 && any(strcmp(stat{2},'multiKnobsTimer'))
    sreq('multiKnobsTimer');
  end
  timerStopFcn
  uiresume(FL.Gui.multiKnobs.figure1)
end

% check access status
if isempty(strfind(FL.mode,'trusted'))
  [stat data]=multiKnobsFn('GetData');
  knames=get(FL.Gui.multiKnobs.popupmenu1,'String');
  kname=knames{get(FL.Gui.multiKnobs.popupmenu1,'Value')};
  if isfield(data,kname) && isfield(data.(kname),'access') && isempty(data.(kname).access)
    pushbutton9_Callback(FL.Gui.multiKnobs.pushbutton9,[],FL.Gui.multiKnobs);
  end
end

% Function to run in case of timer error
function timerStopFcn
global FL
if isfield(FL.Gui,'multiKnobs') && ishandle(FL.Gui.multiKnobs.figure1)
  set(FL.Gui.multiKnobs.text2,'BackgroundColor','black');
  set(FL.Gui.multiKnobs.text3,'BackgroundColor','black');
end

% --- Outputs from this function are returned to the command line.
function varargout = multiKnobs_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- EXIT button
function pushbutton1_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global FL
stop(FL.t_multiKnobs);
multiKnobsFn('save');
guiCloseFn('multiKnobs',handles);

% --- Make new multiKnob
function pushbutton8_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton8 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global FL
FL.Gui.multiKnobs_edit=multiKnobs_edit;

% --- Edit selected multiKnbob
function pushbutton7_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton7 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global FL
knobNames=get(handles.popupmenu1,'String');
thisKnobName=knobNames{get(handles.popupmenu1,'Value')};
if exist(['userMK_',thisKnobName,'.m'],'file')
  edit(fullfile('testApps','multiKnobs',['userMK_',thisKnobName,'.m']))
else
  FL.Gui.multiKnobs_edit=multiKnobs_edit;
  set(FL.Gui.multiKnobs_edit.edit4,'String',thisKnobName)
  multiKnobs_edit('pushbutton11_Callback',FL.Gui.multiKnobs_edit.pushbutton11,[],FL.Gui.multiKnobs_edit)
end

% --- Select Knob
function popupmenu1_Callback(hObject, eventdata, handles)
% hObject    handle to popupmenu1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = get(hObject,'String') returns popupmenu1 contents as cell array
%        contents{get(hObject,'Value')} returns selected item from popupmenu1
knobNames=get(hObject,'String');
thisKnobName=knobNames{get(hObject,'Value')};
[stat knobData]=multiKnobsFn('GetData',thisKnobName);
if isfield(knobData,'guiUnits')
  storedUnits=knobData.guiUnits;
  unitsel=find(strcmp(get(handles.popupmenu2,'String'),storedUnits));
  if ~isempty(unitsel)
    set(handles.popupmenu2,'Value',unitsel)
  end
end
units=get(handles.popupmenu2,'String');
thisUnits=str2double(units{get(handles.popupmenu2,'Value')});
if stat{1}~=1
  errordlg(stat{2},'Get Data Error')
  return
end
set(handles.edit1,'String',num2str(knobData.val/thisUnits))
set(handles.edit2,'String',num2str(knobData.stepVal/thisUnits))
set(handles.edit3,'String',num2str(knobData.storedVal/thisUnits))

% Reset knob readback
pushbutton10_Callback(handles.pushbutton10,[],handles);

% Get access permissions
% pushbutton9_Callback(handles.pushbutton9,[],handles);

% Get controls reference info and display
[stat refVals reftime]=multiKnobsFn('GetControlsRef',thisKnobName);
if stat{1}==1
  set(handles.text7,'String',datestr(reftime))
end

% Get info about any calibration performed
if isfield(knobData,'caldata') && ~isempty(knobData.caldata)
  set(handles.text4,'String',knobData.caldata)
end
if isfield(knobData,'caltype') && any(strcmp(knobData.caltype,get(handles.popupmenu3,'String')))
  set(handles.popupmenu3,'Value',find(strcmp(knobData.caltype,get(handles.popupmenu3,'String'))))
end

% Display any user comments
[stat comments]=multiKnobsFn('GetComment',thisKnobName);
if stat{1}==1
  set(handles.edit4,'String',comments)
else
  set(handles.edit4,'String',['Error Getting any comments for this knob: ',stat{1,2}])
end

% Set des value to zero
set(handles.edit1,'String','0.0')

% --- Executes during object creation, after setting all properties.
function popupmenu1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to popupmenu1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit3_Callback(hObject, eventdata, handles)
% hObject    handle to edit3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
knobNames=get(handles.popupmenu1,'String'); thisKnobName=knobNames{get(handles.popupmenu1,'Value')};
if isnan(str2double(get(hObject,'String')))
  errordlg('Bad entry','MultiKnob Value Error')
  [stat knobData]=multiKnobsFn('GetData',thisKnobName);
  set(hObject,'String',num2str(knobData.storedVal))
else
  multiKnobsFn('SetStoredVal',thisKnobName,str2double(get(hObject,'String')));
end

% --- Executes during object creation, after setting all properties.
function edit3_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Go to stored value
function pushbutton6_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton6 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
knobNames=get(handles.popupmenu1,'String');
thisKnobName=knobNames{get(handles.popupmenu1,'Value')};
units=get(handles.popupmenu2,'String');
thisUnits=str2double(units{get(handles.popupmenu2,'Value')});
multiKnobsFn('GetControlsRefTemp',thisKnobName);
stat=multiKnobsFn('SetStoredVal',thisKnobName,str2double(get(handles.edit3,'String'))*thisUnits);
if stat{1}~=1
  errordlg(stat{2},'MultiKnob Error')
  return
end
set(handles.edit1,'String',get(handles.edit3,'String'))
pushbutton2_Callback(handles.pushbutton2,[],handles);

function edit2_Callback(hObject, eventdata, handles)
% hObject    handle to edit2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit2 as text
%        str2double(get(hObject,'String')) returns contents of edit2 as a double
if isnan(str2double(get(hObject,'String')))
  errordlg('Bad entry','MultiKnob Value Error')
  knobNames=get(handles.popupmenu1,'String'); thisKnobName=knobNames{get(handles.popupmenu1,'Value')};
  [stat knobData]=multiKnobsFn('GetData',thisKnobName);
  set(hObject,'String',num2str(knobData.stepVal))
end

% --- Executes during object creation, after setting all properties.
function edit2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Step Down
function pushbutton4_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
set(handles.edit1,'String',num2str(str2double(get(handles.edit1,'String'))-str2double(get(handles.edit2,'String'))))
pushbutton2_Callback(handles.pushbutton2,[],handles)

% --- Step Up
function pushbutton5_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton5 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
set(handles.edit1,'String',num2str(str2double(get(handles.edit1,'String'))+str2double(get(handles.edit2,'String'))))
pushbutton2_Callback(handles.pushbutton2,[],handles)


function edit1_Callback(hObject, eventdata, handles)
% hObject    handle to edit1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit1 as text
%        str2double(get(hObject,'String')) returns contents of edit1 as a double
if isnan(str2double(get(hObject,'String')))
  errordlg('Bad entry','MultiKnob Value Error')
  knobNames=get(handles.popupmenu1,'String'); thisKnobName=knobNames{get(handles.popupmenu1,'Value')};
  [stat knobData]=multiKnobsFn('GetData',thisKnobName);
  set(hObject,'String',num2str(knobData.val))
end

% --- Executes during object creation, after setting all properties.
function edit1_CreateFcn(hObject, eventdata, handles) %#ok<*INUSD>
% hObject    handle to edit1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Set Val
function pushbutton2_Callback(hObject, eventdata, handles) %#ok<*INUSL>
% hObject    handle to pushbutton2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
sreq_wait;
knobNames=get(handles.popupmenu1,'String');
units=get(handles.popupmenu2,'String');
thisUnits=str2double(units{get(handles.popupmenu2,'Value')});
thisKnobName=knobNames{get(handles.popupmenu1,'Value')};
% First store current control value settings in temp storage for undo
% option
multiKnobsFn('GetControlsRefTemp',thisKnobName);
stat=multiKnobsFn('SetVal',thisKnobName,str2double(get(handles.edit1,'String'))*thisUnits);
if stat{1}~=1
  errordlg(stat{2},'MultiKnob SetVal Error')
  return
end
sreq_wait('restart');
drawnow('expose')

% --- Executes when user attempts to close figure1.
function figure1_CloseRequestFcn(hObject, eventdata, handles) %#ok<*DEFNU>
% hObject    handle to figure1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global FL
stop(FL.t_multiKnobs);
multiKnobsFn('save');
guiCloseFn('multiKnobs',handles);


% --- Access Request Button
function pushbutton9_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton9 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
sreq_wait;
knames=get(handles.popupmenu1,'String');
knobName=knames{get(handles.popupmenu1,'Value')};
[stat resp]=multiKnobsFn('AccessRequest',knobName,1);
if stat{1}~=1; resp=false; end;
if resp
  set(hObject,'BackgroundColor','green')
  set(hObject,'String','Granted')
else
  set(hObject,'BackgroundColor','red')
  set(hObject,'String','Denied')
end
sreq_wait('restart');

% --- Access Permissions button create function
function pushbutton9_CreateFcn(hObject, eventdata, handles)
% hObject    handle to pushbutton9 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called
global FL

if isfield(FL,'mode') && strcmp(FL.mode,'trusted')
  set(hObject,'BackgroundColor','green')
  set(hObject,'String','Granted')
end


% --- Set knob offset
function pushbutton10_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton10 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
try
  knames=get(handles.popupmenu1,'String');
  stat=multiKnobsFn('SetOffset',knames{get(handles.popupmenu1,'Value')});
  if stat{1}~=1
    errordlg(stat{2},'Zero Set Error')
  end
catch
end


% --- restart reader timer
function pushbutton11_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton11 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global FL
stop(FL.t_multiKnobs)
pause(FL.t_multiKnobs.Period)
sreq('clear');
start(FL.t_multiKnobs)
drawnow('expose')

% --- Unit selection
function popupmenu2_Callback(hObject, eventdata, handles)
% hObject    handle to popupmenu2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
knames=get(handles.popupmenu1,'String');
kname=knames{get(handles.popupmenu1,'Value')};
units=get(hObject,'String');
multiKnobsFn('SetGuiUnits',kname,units{get(hObject,'Value')});
popupmenu1_Callback(handles.popupmenu1,[],handles);

% --- Executes during object creation, after setting all properties.
function popupmenu2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to popupmenu2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Get Calibration data
function pushbutton12_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton12 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
if ~strcmp(questdlg('Start Calibration procedure?','Cal Procedure'),'Yes'); return; end;
mhan=msgbox('Calibration procedure running...');
sreq_wait;
knames=get(handles.popupmenu1,'String');
ipnames=get(handles.popupmenu3,'String');
[stat caldata]=multiKnobsFn('Calibrate',knames{get(handles.popupmenu1,'Value')},ipnames{get(handles.popupmenu3,'Value')},get(handles.checkbox1,'Value'));
if stat{1}~=1
  errordlg(stat{2},'Calibration Error')
  return
end
set(handles.text4,'String',caldata)
sreq_wait('restart');
if ishandle(mhan); delete(mhan); end;

% --- Executes on selection change in popupmenu3.
function popupmenu3_Callback(hObject, eventdata, handles)
% hObject    handle to popupmenu3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = get(hObject,'String') returns popupmenu3 contents as cell array
%        contents{get(hObject,'Value')} returns selected item from popupmenu3


% --- Executes during object creation, after setting all properties.
function popupmenu3_CreateFcn(hObject, eventdata, handles)
% hObject    handle to popupmenu3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in checkbox1.
function checkbox1_Callback(hObject, eventdata, handles)
% hObject    handle to checkbox1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of checkbox1


% --- Set controls ref
function pushbutton13_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton13 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
sreq_wait;
knames=get(handles.popupmenu1,'String');
stat=multiKnobsFn('SetControlsRef',knames{get(handles.popupmenu1,'Value')});
if stat{1}~=1
  errordlg(stat{2},'SetControlsRef Error')
end
sreq_wait('restart');

% --- Executes on button press in pushbutton14.
function pushbutton14_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton14 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Take new controls ref
function pushbutton15_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton15 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

knames=get(handles.popupmenu1,'String');
[stat refVals reftime]=multiKnobsFn('GetControlsRef',knames{get(handles.popupmenu1,'Value')});
if stat{1}~=1
  errordlg(stat{2},'GetControlsRef Error')
else
  set(handles.text7,'String',datestr(reftime))
end


% --- Executes on button press in pushbutton16.
function pushbutton16_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton16 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
knames=get(handles.popupmenu1,'String');
stat=multiKnobsFn('ResetOffset',knames{get(handles.popupmenu1,'Value')});
if stat{1}~=1
  errordlg(stat{2},'Reset Offset Error')
end

% User comment field
function edit4_Callback(hObject, eventdata, handles)
% hObject    handle to edit4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
knobNames=get(handles.popupmenu1,'String');
multiKnobsFn('SetComment',knobNames{get(handles.popupmenu1,'Value')},get(hObject,'String'));



% --- Executes during object creation, after setting all properties.
function edit4_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in pushbutton18.
function pushbutton18_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton18 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
sreq_wait;
knames=get(handles.popupmenu1,'String');
stat=multiKnobsFn('SetControlsRefTemp',knames{get(handles.popupmenu1,'Value')});
if stat{1}~=1
  errordlg(stat{2},'SetControlsRef Error')
end
sreq_wait('restart');
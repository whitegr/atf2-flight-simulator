function [stat varargout]=IncrementSextIPKnob(Knobs,wknob,ksize)
% Units are mm / % coupling units
global BEAMLINE FL GIRDER
persistent reqid offset

% Get Girder indicies
for isext=1:5
  sgi(isext)=BEAMLINE{FL.SimModel.magGroup.Sext.dB.ClusterList(isext).index(1)}.Girder;
end
iqd0=findcells(BEAMLINE,'Name','QD0FF');
sgi(6)=BEAMLINE{iqd0(1)}.Girder;

% Get Access Permissions for movers
if isempty(reqid)
  [stat reqid]=AccessRequest({sgi [] []});
else
  [stat resp]=AccessRequest('status',reqid);
  if ~resp
    [stat reqid]=AccessRequest({sgi [] []});
  end
end
if stat{1}~=1; return; end;
if ~reqid
  stat{1}=0;
  stat{2}='Access request refused';
  return
end

% If just want access status, job done- access is ok if stat good
if isequal(wknob,'access_status')
  return
end

% Get reference mover positions to compare readback to
if isempty(offset)
  offset=zeros(3,6);
end
if isequal(wknob,'setreadbackoffset')
  for isext=1:6
    for idof=1:3
      offset(idof,isext)=GIRDER{sgi(isext)}.MoverPos(idof);
    end
  end
  return
end
if isequal(wknob,'resetreadbackoffset')
  for isext=1:6
    for idof=1:3
      offset(idof,isext)=0;
    end
  end
  return
end

% Return mover positions if requested
if isequal(wknob,'readback_controls')
  varargout{1}={};
  for isext=1:6
    varargout{1}{isext}=GIRDER{sgi(isext)}.MoverSetPt;
  end
  return
end

% Form knob data
ik{1}=reshape(Knobs.DispY.smoves,3,6);
ik{2}=reshape(Knobs.XpY.smoves,3,6);
ik{3}=reshape(Knobs.WaistY.smoves,3,6);
ik{4}=reshape(Knobs.T322.smoves,3,6);
ik{5}=reshape(Knobs.T326.smoves,3,6);
ik{6}=reshape(Knobs.U3122.smoves,3,6);
ik{7}=reshape(Knobs.WaistX.smoves,3,6);
ik{8}=reshape(Knobs.DispX.smoves,3,6);
scales=[Knobs.DispY.scale Knobs.XpY.scale Knobs.WaistY.scale Knobs.T322.scale Knobs.T326.scale Knobs.U3122.scale Knobs.WaistX.scale Knobs.DispX.scale];
uScales=[Knobs.DispY.uScale Knobs.XpY.uScale Knobs.WaistY.uScale Knobs.T322.uScale Knobs.T326.uScale Knobs.U3122.uScale Knobs.WaistX.uScale Knobs.DispX.uScale];

% Return knob positions if requested
if isequal(wknob,'readback')
  varargout{1}=[];
%   FlHwUpdate({sgi [] []});
  for iknob=1:8
    %     rb=[];
    b=[]; A=[]; minmove=[]; maxmove=[];
    for isext=1:6
      for idof=1:3
        h=FL.HwInfo.GIRDER(sgi(isext)).high(idof);
        l=FL.HwInfo.GIRDER(sgi(isext)).low(idof);
        g=GIRDER{sgi(isext)}.MoverPos(idof);
        mim=l-g; %mim(mim<0)=0;
        mam=h-g; %mam(mam<0)=0;
        minmove(end+1)=(mim*scales(iknob))/(ik{iknob}(idof,isext)*uScales(iknob));
        maxmove(end+1)=(mam*scales(iknob))/(ik{iknob}(idof,isext)*uScales(iknob));
%         minmove(end+1)=mim/(ik{iknob}(idof,isext)/scales(iknob));
%         maxmove(end+1)=mam/(ik{iknob}(idof,isext)/scales(iknob));
        b(end+1)=GIRDER{sgi(isext)}.MoverPos(idof)-offset(idof,isext);
        A(end+1)=ik{iknob}(idof,isext).*uScales(iknob)/scales(iknob);
      end
    end
%     minmove(minmove<0)=1e50;
%     maxmove(maxmove<0)=1e50;
%     minmove(isinf(minmove))=1e50;
%     maxmove(isinf(maxmove))=1e50;
    [x,stdx]=lscov(A',b');
%     kv=b./A; kv=kv(~isnan(kv)); kv=kv(~isinf(kv));
%     x=mean(kv(~isnan(kv)));
%     stdx=std(kv(~isnan(kv)));
    if ~isreal(stdx); stdx=0; end;
    varargout{1}(1,iknob)=x;
    varargout{1}(2,iknob)=stdx; %(stdx/sqrt(1/mse));
    minmove=minmove(~isinf(minmove)); minmove=minmove(minmove<0);
    [val ind]=min(abs(minmove)); m1=minmove(ind); m2=x+minmove(ind);
    if abs(m1)<abs(m2)
      varargout{1}(3,iknob)=m2;
    else
      varargout{1}(3,iknob)=m2;
    end
    maxmove=maxmove(~isinf(maxmove)); maxmove=maxmove(maxmove>0);
    [val ind]=min(abs(maxmove));
    m1=maxmove(ind); m2=x+maxmove(ind);
    if abs(m1)<abs(m2)
      varargout{1}(4,iknob)=m2;
    else
      varargout{1}(4,iknob)=m2;
    end
  end
  return
end

% Move knobs by requested amount
for isext=1:6
  GIRDER{sgi(isext)}.MoverSetPt=GIRDER{sgi(isext)}.MoverSetPt+[ik{wknob}(1,isext) ik{wknob}(2,isext) ik{wknob}(3,isext)].*ksize.*uScales(wknob)/scales(wknob);
end
stat=MoverTrim(sgi,3);
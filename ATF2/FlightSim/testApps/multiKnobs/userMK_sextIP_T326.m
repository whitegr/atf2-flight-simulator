function [stat varargout]=userMK_sextIP_T326(cmd,varargin)
% USERMK_SEXTIP_T326 - IP T326 knob using mover system
global GIRDER BEAMLINE FL
persistent Knobs caldata loadtry sgi
stat{1}=1; varargout{1}=[];

% Load knobs or ask to generate for first call
if isempty(Knobs) && isempty(loadtry)
  [stat Knobs caldata]=loadKnobs;
  loadtry=true;
end
if isempty(Knobs) && ~strcmpi(cmd,'getcaldata') 
  if isempty(Knobs) && ~strcmpi(cmd,'getcaldata') && ~strcmpi(cmd,'calibrate')
    resp=questdlg('No calibration done yet, do now?','Calibration','target=IP','target=MW1IP','Cancel','Cancel');
    if strcmp(resp,'target=IP')
      [stat Knobs]=GenerateSextIPKnobs(2,0,0,0,0); if stat{1}~=1; return; end;
    elseif strcmp(resp,'target=MW1IP')
      [stat Knobs]=GenerateSextIPKnobs(1,0,0,0,0); if stat{1}~=1; return; end;
    else
      return
    end
  end
end

% Get Girder indicies
if isempty(sgi)
  for isext=1:5
    sgi(isext)=BEAMLINE{FL.SimModel.magGroup.Sext.dB.ClusterList(isext).index(1)}.Girder;
  end
  iqd0=findcells(BEAMLINE,'Name','QD0FF');
  sgi(6)=BEAMLINE{iqd0(1)}.Girder;
end

% Process commands
switch lower(cmd)
  case 'getcaldata'
    varargout{1}=caldata;
  case 'move' % rel, current
    varargout{1}=0;
    if nargin<2 || ~isnumeric(varargin{1})
      stat{1}=-1; stat{2}='Bad input arguments';
      return
    end
    stat=IncrementSextIPKnob(Knobs,5,varargin{1});
    varargout{1}=varargin{1};
  case 'calibrate'
    if nargin<3 || (~islogical(varargin{2}) && ~isnumeric(varargin{2})) || ~ischar(varargin{1})
      stat{1}=-1; stat{2}='Bad input arguments';
      return
    end
    % 1 = target (1=MW1IP 2=IP)
    % 2 = Check knobs (true/false)
    caltarget={'MW1IP' 'IP'};
    itarget=find(strcmp(varargin{1},caltarget));
    [stat caldata]=sextmkcal(itarget,5,varargin);
    varargout{1}=caldata;
  case 'access'
    astat=IncrementSextIPKnob(Knobs,'access_status');
    if astat{1}<0; stat=astat; return; end;
    if astat{1}~=1
      varargout{1}=0;
    else
      varargout{1}=1;
    end
  case 'readback_controls'
    [stat rdbk]=IncrementSextIPKnob(Knobs,'readback_controls');
    varargout{1}=[];
    for isext=1:6
      for idof=1:3
        varargout{1}(end+1)=rdbk{isext}(idof);
      end
    end
  case 'setcontrols'
    if nargin<2 || ~isnumeric(varargin{1})
      stat{1}=-1; stat{2}='Bad input arguments';
      return
    end
    rind=0;
    for isext=1:6
      for idof=1:3
        rind=rind+1;
        GIRDER{sgi(isext)}.MoverSetPt(idof)=varargin{1}(rind);
      end
    end
    stat=MoverTrim(sgi,1);
  case 'readback'
    [stat rdbk]=IncrementSextIPKnob(Knobs,'readback');
    varargout{1}=[rdbk(1,5) rdbk(2,5) rdbk(3,5) rdbk(4,5)];
  case 'setreadbackoffset'
    stat=IncrementSextIPKnob(Knobs,'setreadbackoffset');
  case 'resetreadbackoffset'
    stat=IncrementSextIPKnob(Knobs,'resetreadbackoffset');
  otherwise
    stat{1}=-1; stat{2}='Unknown command';
    return
end

function [stat Knobs caldata] = loadKnobs()
global FL
Knobs=[]; caldata=[];
[stat data]=FlDataStore('userMK_sextIP');
if stat{1}~=1 || isempty(data) || ~isfield(FL,'DataStore') || ~isfield(FL.DataStore,'userMK_sextIP'); return; end;
fn=fieldnames(FL.DataStore.userMK_sextIP);
for ifn=1:length(fn)
  if strcmp(fn{ifn},'caldata')
    caldata=FL.DataStore.userMK_sextIP.caldata;
  else
    t=regexp(fn{ifn},'(\w+)_(\w+)','tokens');
    if ~isempty(t)
      Knobs.(t{1}{2}).(t{1}{1})=FL.DataStore.userMK_sextIP.(fn{ifn});
    end
  end
end
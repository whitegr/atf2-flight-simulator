function [stat caldata]=sextmkcal(itarget,thisKnob,varargin)
% calibration routine for sextupole IP multiknobs
global FL
[stat Knobs]=getSimKnobs;
if stat{1}==0
  [stat Knobs]=GenerateSextIPKnobs(FL.SimBeam{1},itarget,varargin{1}{2},0,0,0);
end
if stat{1}==1
  if length(stat)==2
    caldata=regexprep(regexprep(stat{2},'simKnobs_',''),'.mat','');
  else
    caldata=[varargin{1}{1},' ',datestr(now,1)];
  end
  stat=saveKnobs(Knobs,caldata);
elseif stat{1}<0 && ~strcmp(stat{2},'User Canceled')
  errordlg(sprintf('Calibration Failed:\n%s',stat{2}),'Cal Failure');
  return
end
% Set info in comment
knames={'sextIP_wy' 'sextIP_dy' 'sextIP_xpy' 'sextIP_T322' 'sextIP_T326' 'sextIP_U3122' 'sextIP_wx' 'sextIP_dx'};
kfields={'WaistY' 'DispY' 'XpY' 'T322' 'T326' 'U3122' 'WaistX' 'DispX'};
for iknob=1:length(knames)
  knobVals=reshape(Knobs.(kfields{iknob}).smoves.*(Knobs.(kfields{iknob}).uScale./Knobs.(kfields{iknob}).scale),3,6);
  str=num2str(knobVals.*1e6,2);
  multiKnobsFn('SetComment',knames{iknob},str);
  if iknob==thisKnob && isfield(FL.Gui,'multiKnobs') && ishandle(FL.Gui.multiKnobs.figure1)
    set(FL.Gui.multiKnobs.edit4,'String',str)
    drawnow('expose')
  end
end

function stat = saveKnobs(Knobs,caldata)
stat = FlDataStore('userMK_sextIP',...
  'smoves_WaistX',Knobs.WaistX.smoves,'scale_WaistX',Knobs.WaistX.scale,'units_WaistX',Knobs.WaistX.units,'uScale_WaistX',Knobs.WaistX.uScale,...
  'smoves_WaistY',Knobs.WaistY.smoves,'scale_WaistY',Knobs.WaistY.scale,'units_WaistY',Knobs.WaistY.units,'uScale_WaistY',Knobs.WaistY.uScale,...
  'smoves_DispX',Knobs.DispX.smoves,'scale_DispX',Knobs.DispX.scale,'units_DispX',Knobs.DispX.units,'uScale_DispX',Knobs.DispX.uScale,...
  'smoves_DispY',Knobs.DispY.smoves,'scale_DispY',Knobs.DispY.scale,'units_DispY',Knobs.DispY.units,'uScale_DispY',Knobs.DispY.uScale,...
  'smoves_XpY',Knobs.XpY.smoves,'scale_XpY',Knobs.XpY.scale,'units_XpY',Knobs.XpY.units,'uScale_XpY',Knobs.XpY.uScale,...
  'smoves_T322',Knobs.T322.smoves,'scale_T322',Knobs.T322.scale,'units_T322',Knobs.T322.units,'uScale_T322',Knobs.T322.uScale,...
  'smoves_T326',Knobs.T326.smoves,'scale_T326',Knobs.T326.scale,'units_T326',Knobs.T326.units,'uScale_T326',Knobs.T326.uScale,...
  'smoves_U3122',Knobs.U3122.smoves,'scale_U3122',Knobs.U3122.scale,'units_U3122',Knobs.U3122.units,'uScale_U3122',Knobs.U3122.uScale,...
  'caldata',caldata);
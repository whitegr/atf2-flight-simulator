function varargout = dispersion_measurement(varargin)
% DISPERSION_MEASUREMENT M-file for dispersion_measurement.fig
%      DISPERSION_MEASUREMENT, by itself, creates a new DISPERSION_MEASUREMENT or raises the existing
%      singleton*.
%
%      H = DISPERSION_MEASUREMENT returns the handle to a new DISPERSION_MEASUREMENT or the handle to
%      the existing singleton*.
%
%      DISPERSION_MEASUREMENT('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in DISPERSION_MEASUREMENT.M with the given input arguments.
%
%      DISPERSION_MEASUREMENT('Property','Value',...) creates a new DISPERSION_MEASUREMENT or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before dispersion_measurement_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to dispersion_measurement_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help dispersion_measurement

% Last Modified by GUIDE v2.5 12-Jun-2012 11:58:43

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @dispersion_measurement_OpeningFcn, ...
                   'gui_OutputFcn',  @dispersion_measurement_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before dispersion_measurement is made visible.
function dispersion_measurement_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to dispersion_measurement (see VARARGIN)

% Choose default command line output for dispersion_measurement
handles.output = hObject;
handles.out='';
handles=display_output(handles,'loading ...');

rmpath /usr/local/MATLAB/R2010b/toolbox/curvefit/curvefit

global BEAMLINE INSTR

IEX=findcells(BEAMLINE,'Name','IEX');
IEX_s=BEAMLINE{IEX}.S;
handles.IEX=IEX;
handles.IEX_s=IEX_s;
IP=findcells(BEAMLINE,'Name','IP');
IP_s=BEAMLINE{IP}.S;
handles.IP_s=IP_s;
handles.IP=IP;

handles.fit_point='IEX';
set(handles.fit_point_txt,'String',handles.fit_point);
handles.fit_point_index=findcells(BEAMLINE,'Name',handles.fit_point);
handles.fit_point_s=BEAMLINE{handles.fit_point_index}.S;

handles.E_by_EXT=1;
handles.jitter_check=1;
handles.timestamp_check=0;
handles.nbpm_read_tot=200;
handles.wait=true;
handles.filename_save=datestr(now(), 30);
set(handles.filename_save_txt,'String',handles.filename_save);
handles.auto_plot=1;

bpm=[findcells(BEAMLINE,'Name','MB*R') ...
    findcells(BEAMLINE,'Name','MB*X') ...
    findcells(BEAMLINE,'Name','MQ*X') ...
    findcells(BEAMLINE,'Name','MQ*FF') ...
    findcells(BEAMLINE,'Name','MS*FF') ...
    findcells(BEAMLINE,'Name','MFB*FF') ...
    findcells(BEAMLINE,'Name','MPREIP')...
    findcells(BEAMLINE,'Name','IPBPMA')...
    findcells(BEAMLINE,'Name','IPBPMB')...
    findcells(BEAMLINE,'Name','IP')...
    findcells(BEAMLINE,'Name','MPIP')...
    findcells(BEAMLINE,'Name','MDUMP')];
bpm_name='';
bpm_type='';
bpm_s=[];
bpm_index=[];
for i=bpm
    bpm_name=strvcat(bpm_name,BEAMLINE{i}.Name);
    bpm_s(end+1)=BEAMLINE{i}.S;
    bpm_index(end+1)=findcells(INSTR,'Index',i);
    bpm_type=strvcat(bpm_type,INSTR{bpm_index(end)}.Type);
end
button=find(strncmp('MB',cellstr(bpm_name),2)).';
stripline=find(strcmp('stripline',cellstr(bpm_type))).';
cband=find(strcmp('ccav',cellstr(bpm_type))).';
sband=find(strcmp('scav',cellstr(bpm_type))).';
bpm=bpm([button stripline cband sband]);
[handles.bpm,bpm_sort]=unique(bpm);
bpm_name=bpm_name([button stripline cband sband],:);
handles.bpm_name=bpm_name(bpm_sort,:);
bpm_s=bpm_s([button stripline cband sband]);
handles.bpm_s=bpm_s(bpm_sort);
bpm_index=bpm_index([button stripline cband sband]);
handles.bpm_index=bpm_index(bpm_sort);
bpm_type=bpm_type([button stripline cband sband],:);
handles.bpm_type=bpm_type(bpm_sort,:);
handles.nbpm=length(handles.bpm);
handles.bpm_range=1:handles.nbpm;
%handles.bpm_range=[3:5 7:10 12:19 24:35];
%handles.bpm_range_disp=[12:19 24:35];
%handles.bpm_range=[12:14 18:43];
%handles.bpm_range_disp=[12:14 18:43];
handles.bpm_range_disp=1:handles.nbpm;
handles.xlim=[0 handles.bpm_s(end)]-IEX_s;
handles.ylim=[-1 1];
set(handles.xlim_txt,'String',sprintf('[%.3g %.3g]',handles.xlim(1),handles.xlim(2)));
set(handles.ylim_txt,'String',sprintf('[%.3g %.3g]',handles.ylim(1),handles.ylim(2)));

handles.param_range=[1 2 3 4 6];
handles.nparam=length(handles.param_range);
handles.additional_kick_point_index=findcells(BEAMLINE,'Name','KEX2B');
handles.additional_kick_point_index=[];
handles.nadditional_kick_point=length(handles.additional_kick_point_index);

handles.use_sext=true;

axes(handles.plot)
xlim(handles.xlim+IEX_s);
ylim(handles.ylim);
set(gca,'XTick',handles.bpm_s);
set(gca,'XTickLabel',handles.bpm_name);
handles.hText=xticklabel_rotate90();

plot_magnets_external(BEAMLINE,handles.lattice,handles.xlim(1)+IEX_s,handles.xlim(2)+IEX_s);
axes(handles.lattice);
xlim(handles.xlim+IEX_s);

handles=display_output(handles,'done');
% Update handles structure
guidata(hObject, handles);

% UIWAIT makes dispersion_measurement wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = dispersion_measurement_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;



function freq_range_txt_Callback(hObject, eventdata, handles)
% hObject    handle to freq_range_txt (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of freq_range_txt as text
%        str2double(get(hObject,'String')) returns contents of
%        freq_range_txt as a double
guidata(hObject, handles);

% --- Executes during object creation, after setting all properties.
function freq_range_txt_CreateFcn(hObject, eventdata, handles)
% hObject    handle to freq_range_txt (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function nbpm_read_tot_txt_Callback(hObject, eventdata, handles)
% hObject    handle to nbpm_read_tot_txt (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of nbpm_read_tot_txt as text
%        str2double(get(hObject,'String')) returns contents of nbpm_read_tot_txt as a double
handles.nbpm_read_tot=str2double(get(hObject,'String'));
guidata(hObject, handles);

% --- Executes during object creation, after setting all properties.
function nbpm_read_tot_txt_CreateFcn(hObject, eventdata, handles)
% hObject    handle to nbpm_read_tot_txt (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function nbpm_read_per_step_txt_Callback(hObject, eventdata, handles)
% hObject    handle to nbpm_read_per_step_txt (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of nbpm_read_per_step_txt as text
%        str2double(get(hObject,'String')) returns contents of nbpm_read_per_step_txt as a double
guidata(hObject, handles);

% --- Executes during object creation, after setting all properties.
function nbpm_read_per_step_txt_CreateFcn(hObject, eventdata, handles)
% hObject    handle to nbpm_read_per_step_txt (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function filename_save_txt_Callback(hObject, eventdata, handles)
% hObject    handle to filename_save_txt (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of filename_save_txt as text
%        str2double(get(hObject,'String')) returns contents of filename_save_txt as a double
handles.filename_save=get(hObject,'String');
guidata(hObject, handles);

% --- Executes during object creation, after setting all properties.
function filename_save_txt_CreateFcn(hObject, eventdata, handles)
% hObject    handle to filename_save_txt (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in auto_plot_chk.
function auto_plot_chk_Callback(hObject, eventdata, handles)
% hObject    handle to auto_plot_chk (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of auto_plot_chk
handles.auto_plot=get(hObject,'Value');
guidata(hObject, handles);

% --- Executes on button press in measure_btn.
function measure_btn_Callback(hObject, eventdata, handles)
% hObject    handle to measure_btn (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

global BEAMLINE INSTR PS GIRDER FL;

FlHwUpdate();
[s,pulsenum]=FlHwUpdate('getpulsenum');
if (handles.wait)
    pulse0=pulsenum;
else
    pulse0=1;
end
while (pulsenum-pulse0<handles.nbpm_read_tot)
    pause(.6);
    FlHwUpdate;
    [s,pulsenum]=FlHwUpdate('getpulsenum');
    display_output(handles,sprintf('%i/%i pulse measured',pulsenum-pulse0,handles.nbpm_read_tot));
end
handles=display_output(handles,'buffer full, reading it ...');
[stat,handles.raw]=FlHwUpdate('readbuffer',pulsenum-pulse0);
[stat,handles.timestamp]=FlHwUpdate('readtsbuffer');

save(handles.filename_save,'handles','BEAMLINE','INSTR','PS','GIRDER','FL');

if(handles.auto_plot)
    handles.filename_load=handles.filename_save;
    set(handles.filename_load_txt,'String',handles.filename_load);
    guidata(hObject, handles);
    handles=load_btn_Callback(hObject, eventdata, handles);
end

guidata(hObject, handles);


function filename_load_txt_Callback(hObject, eventdata, handles)
% hObject    handle to filename_load_txt (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of filename_load_txt as text
%        str2double(get(hObject,'String')) returns contents of filename_load_txt as a double
handles.filename_load=get(hObject,'String');
guidata(hObject, handles);

% --- Executes during object creation, after setting all properties.
function filename_load_txt_CreateFcn(hObject, eventdata, handles)
% hObject    handle to filename_load_txt (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function xlim_txt_Callback(hObject, eventdata, handles)
% hObject    handle to xlim_txt (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of xlim_txt as text
%        str2double(get(hObject,'String')) returns contents of xlim_txt as a double

handles=display_output(handles,'resizing plot');
handles.xlim=eval(get(hObject,'String'));
axes(handles.plot);
xlim(handles.xlim+handles.IEX_s);
axes(handles.lattice);
xlim(handles.xlim+handles.IEX_s);
guidata(hObject, handles);

% --- Executes during object creation, after setting all properties.
function xlim_txt_CreateFcn(hObject, eventdata, handles)
% hObject    handle to xlim_txt (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function ylim_txt_Callback(hObject, eventdata, handles)
% hObject    handle to ylim_txt (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of ylim_txt as text
%        str2double(get(hObject,'String')) returns contents of ylim_txt as a double


%loaded=load(handles.filename_load);
%bpm=loaded.handles.bpm([loaded.handles.stripline loaded.handles.cband]);
%[bpm_ordered,bpm_order]=unique(bpm);
%bpm_s=loaded.handles.bpm_s([loaded.handles.stripline loaded.handles.cband]);
%bpm_name=loaded.handles.bpm_name([loaded.handles.stripline loaded.handles.cband],:);

handles=display_output(handles,'resizing plot');
handles.ylim=eval(get(hObject,'String'));
axes(handles.plot);
%delete(handles.hText);
ylim(handles.ylim);
delete(handles.hText);
set(gca,'XTick',handles.bpm_s);
set(gca,'XTickLabel',handles.bpm_name);
handles.hText=xticklabel_rotate90();

guidata(hObject, handles);

% --- Executes during object creation, after setting all properties.
function ylim_txt_CreateFcn(hObject, eventdata, handles)
% hObject    handle to ylim_txt (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in chose_bpm_btn.
function chose_bpm_btn_Callback(hObject, eventdata, handles)
% hObject    handle to chose_bpm_btn (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


[selection,ok]=listdlg('PromptString',{'Select bpms :' '' '(Shift+clik = continuous selection)' '(Crtl+clik = uncontinuous selection)'},...
            'ListString',handles.loaded.handles.bpm_name,'ListSize',[250 min(handles.loaded.handles.nbpm*15,500)],...
            'InitialValue',handles.bpm_range,'Name','Select BPM');
 if(ok)
     handles.bpm_range=selection;
 end

guidata(hObject, handles);

function handles=get_R_matrix(handles)
handles.R=zeros(handles.loaded.handles.nbpm*2,handles.nparam+handles.nadditional_kick_point);
handles.Rtot=zeros(handles.loaded.handles.nbpm,6,handles.nparam+handles.nadditional_kick_point);
for i=1:handles.loaded.handles.nbpm;
    if (handles.loaded.handles.bpm(i)<=handles.fit_point_index)
        [s,r]=RmatAtoB(handles.loaded.handles.bpm(i),handles.fit_point_index);
        r=inv(r);
    else
        [s,r]=RmatAtoB(handles.fit_point_index,handles.loaded.handles.bpm(i));
    end
    handles.R(i,1:handles.nparam)=r(1,handles.param_range);
    handles.R(handles.loaded.handles.nbpm+i,1:handles.nparam)=r(3,handles.param_range);
    handles.Rtot(i,:,1:handles.nparam)=r(:,handles.param_range);
    for j=1:handles.nadditional_kick_point
        if (handles.loaded.handles.bpm(i)<=handles.additional_kick_point_index(j))
            r=zeros(6,6);
        else
            [s,r]=RmatAtoB(handles.additional_kick_point_index(j),handles.loaded.handles.bpm(i));
        end
%         handles.R(i,handles.nparam+2*(j-1)+(1:2))=r(1,[2 4]);
%         handles.R(handles.loaded.handles.nbpm+i,handles.nparam+2*(j-1)+(1:2))=r(3,[2 4]);
%         handles.Rtot(i,:,handles.nparam+2*(j-1)+(1:2))=r(:,[2 4]);
        handles.R(i,handles.nparam+j)=r(1,2);
        handles.R(handles.loaded.handles.nbpm+i,handles.nparam+j)=r(3,2);
        handles.Rtot(i,:,handles.nparam+j)=r(:,2);
    end 
end

function handles=get_R_with_sext_matrix(handles)

global BEAMLINE GIRDER PS;

sext_index= findcells(BEAMLINE,'Name','S[FD]*FF');
nsext=length(sext_index);
L_sext=zeros(1,nsext);
m_sext=zeros(1,nsext);
tilt_sext=zeros(1,nsext);
sext_bpm_prev=zeros(1,nsext);
sext_bpm_next=zeros(1,nsext);
sext_name=[];
pos_in_sext=zeros(2,nsext);
for i=1:nsext
     sext_name=strvcat(sext_name,BEAMLINE{sext_index(i)}.Name);
     [sorted,indexes]=sort([handles.bpm sext_index(i)]);
     sext_bpm_prev=sorted(find(indexes==length(sorted))-1);
     sext_bpm_prev_index=find(handles.bpm==sext_bpm_prev);
     sext_bpm_next=sorted(find(indexes==length(sorted))+1);
     sext_bpm_next_index=find(handles.bpm==sext_bpm_next);
     pos_in_sext(1,i)=interp1(  [BEAMLINE{sext_bpm_prev}.S BEAMLINE{sext_bpm_next}.S],...
                                [handles.x_mean(sext_bpm_prev_index) handles.x_mean(sext_bpm_next_index)],...
                                BEAMLINE{sext_index(i)}.S);
     pos_in_sext(2,i)=interp1(  [BEAMLINE{sext_bpm_prev}.S BEAMLINE{sext_bpm_next}.S],...
                                [handles.y_mean(sext_bpm_prev_index) handles.y_mean(sext_bpm_next_index)],...
                                BEAMLINE{sext_index(i)}.S);
%      sext_bpm(i)=findcells(BEAMLINE,'Name',['M' sext_name(i,:)]);
%      pos_in_sext_old(:,i)=[handles.x_mean(sext_bpm(i)==handles.bpm); handles.y_mean(sext_bpm(i)==handles.bpm)];
     L_sext(i)=BEAMLINE{sext_index(i)}.L;
     ps=BEAMLINE{sext_index(i)}.PS;
     m_sext(i)=BEAMLINE{sext_index(i)}.B+BEAMLINE{sext_index(i)}.dB;
     m_sext(i)=m_sext(i)*(PS(ps).Ampl+PS(ps).dAmpl);
     girder_sext=BEAMLINE{sext_index(i)}.Girder;
     tilt_sext(i)=GIRDER{girder_sext}.MoverPos(3);
end
E=BEAMLINE{sext_index(i)}.P;
[s,Rs]=GetRmats(handles.IEX,size(BEAMLINE,1));
Rs_with_sext={Rs.RMAT};
for i=1:nsext
    Rs_with_sext{sext_index(i)-handles.IEX+1}=...
        get_R_sext(L_sext(i),m_sext(i),pos_in_sext(1,i),pos_in_sext(2,i),tilt_sext(i),E);
end
for bpm_index=1:handles.nbpm
    bpm=handles.bpm(bpm_index);
    r=eye(6,6);
    if(handles.bpm>handles.fit_point_index)    
        for i=(bpm-handles.IEX+1):-1:(handles.fit_point_index-handles.IEX+1)
            r=r*Rs_with_sext{i};
        end
    elseif (bpm<handles.fit_point_index)
        for i=(handles.fit_point_index-handles.IEX+1):-1:(bpm-handles.IEX+1)
            r=r*Rs_with_sext{i};
        end
        r=inv(r);
    end
    handles.R(bpm_index,1:handles.nparam)=r(1,handles.param_range);
    handles.R(handles.loaded.handles.nbpm+bpm_index,1:handles.nparam)=r(3,handles.param_range);
    handles.Rtot(bpm_index,:,1:handles.nparam)=r(:,handles.param_range);
    for j=1:handles.nadditional_kick_point
        if (bpm<=handles.additional_kick_point_index(j))
            r=zeros(6,6);
        else
            for i=(handles.additional_kick_point_index(j)-handles.IEX+1):-1:(bpm-handles.IEX+1)
                r=r*Rs_with_sext{i};
            end
        end
        handles.R(bpm_index,handles.nparam+j)=r(1,2);
        handles.R(handles.loaded.handles.nbpm+bpm_index,handles.nparam+j)=r(3,2);
        handles.Rtot(bpm_index,:,handles.nparam+j)=r(:,2);
    end 
end

% --- Executes on button press in fit_btn.
function handles=fit_btn_Callback(hObject, eventdata, handles)
% hObject    handle to fit_btn (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

global BEAMLINE
handles=display_output(handles,'fintting data ...');

handles.raw=handles.loaded.handles.raw(handles.read_range,:);
%ICT=findcells(BEAMLINE,'Name','ICTDUMP');
MQF9X=findcells(BEAMLINE,'Name','MQF9X');
%ICT_index=findcells(INSTR,'Index',ICT);
%handles.iread=handles.raw(:,ICT_index*3+1);
if(~isfield(handles.loaded.handles,'timestamp') || isempty(handles.loaded.handles.timestamp))
    handles=display_output(handles,'timestamp empty, disable check');
else
    ts=handles.loaded.handles.timestamp(:,[1 handles.loaded.handles.bpm_index+1]);
    ts_bpm_ok=find(std(ts(:,2:end))>1e-8);
    if (isempty(ts_bpm_ok))
        handles=display_output(handles,'timestamp pb, disable check');
    else
        pulse_ok=ts(find(std(ts(:,ts_bpm_ok+1),1,2)<1e-8),1);
        pulse_not_ok=ts(find(std(ts(:,ts_bpm_ok+1),1,2)>=1e-8),1);
        row=[];
        for ok= pulse_ok';
          row=[row find(ok==handles.raw(:,1))];
        end
        if(handles.timestamp_check)
          if(~isempty(pulse_not_ok))
              handles=display_output(handles,['pulses ' reshape(sprintf(' %i',pulse_not_ok)',1,numel(sprintf(' %i',pulse_not_ok))) ' has bad timestamp']);
          end
          handles.raw=handles.raw(row,:);
          handles.noise_range=handles.read_range(row);
        end
    end
end
    
x_read=handles.raw(:,handles.loaded.handles.bpm_index*3-1);
y_read=handles.raw(:,handles.loaded.handles.bpm_index*3);
nread=size(handles.raw,1);
handles.dx_read=x_read-(ones(nread,1)*sum(x_read,1)/nread);
handles.dy_read=y_read-(ones(nread,1)*sum(y_read,1)/nread);
rms_ok=std(handles.dx_read,1,2)<mean(std(handles.dx_read,1,2))*5;
rms_ok=rms_ok.*(std(handles.dy_read,1,2)<mean(std(handles.dy_read,1,2))*5);
if(handles.jitter_check)
    if(~isempty(find(~rms_ok)))
        handles=display_output(handles,['pulses ' reshape(sprintf(' %i',find(~rms_ok))',1,numel(sprintf(' %i',find(~rms_ok)))) ' has too much spread']);
    end
    handles.raw=handles.raw(find(rms_ok),:);
    handles.noise_range=handles.read_range(find(rms_ok));
end

nread=size(handles.raw,1);
x_read=handles.raw(:,handles.loaded.handles.bpm_index*3-1);
y_read=handles.raw(:,handles.loaded.handles.bpm_index*3);
x_read(:,1:96)=-x_read(:,1:96);
handles.dx_read=x_read-(ones(nread,1)*sum(x_read,1)/nread);
handles.dy_read=y_read-(ones(nread,1)*sum(y_read,1)/nread);
handles.x_mean=sum(x_read,1)/nread;
handles.y_mean=sum(y_read,1)/nread;
handles.B=[handles.dx_read.'; handles.dy_read.'];
handles.matrix_range=[handles.bpm_range handles.loaded.handles.nbpm+handles.bpm_range];
%skiped=find(handles.resol_nice(handles.matrix_range)<1e-8);
skiped=find(handles.resol(handles.matrix_range)<1e-8);
xskiped=handles.loaded.handles.bpm_name(handles.matrix_range(skiped(handles.matrix_range(skiped)<=handles.loaded.handles.nbpm)),:);
yskiped=handles.loaded.handles.bpm_name(handles.matrix_range(skiped(handles.matrix_range(skiped)>handles.loaded.handles.nbpm))-handles.loaded.handles.nbpm,:);
if(~isempty(xskiped))
    handles=display_output(handles,sprintf(['xBPM skiped :' reshape(sprintf(' %s',xskiped')',1,numel(sprintf(' %s',xskiped)))]));
end
if(~isempty(yskiped))
    handles=display_output(handles,sprintf(['yBPM skiped :' reshape(sprintf(' %s',yskiped')',1,numel(sprintf(' %s',yskiped)))]));
end
handles.matrix_range(find(handles.resol(handles.matrix_range)<1e-8))=[];
range_bpm_E=handles.matrix_range(handles.bpm(handles.matrix_range(handles.matrix_range<=handles.nbpm))<=MQF9X);
handles.matrix_range_E=[range_bpm_E handles.loaded.handles.nbpm+range_bpm_E];

handles.B_noise=handles.noise(handles.noise_range,:).';
handles.weigth=zeros(2*handles.nbpm,1);
handles.weigth(handles.matrix_range)=1./(handles.resol_nice(handles.matrix_range)).^2;
handles.param=[];
handles.param_err=[];

if(handles.use_sext)
    handles=get_R_with_sext_matrix(handles);
else
    handles=get_R_matrix(handles);
end

if(handles.E_by_EXT)
    [handles.param(1:5,:),handles.param_err(1:5,:)]=lscov(handles.R(handles.matrix_range_E,1:5),handles.B(handles.matrix_range_E,:),handles.weigth(handles.matrix_range_E));
    if(handles.nadditional_kick_point>0)
        [handles.param([1:4 (1:handles.nadditional_kick_point)+5],:),handles.param_err([1:4 (1:handles.nadditional_kick_point)+5],:)]=lscov(handles.R(handles.matrix_range,[1:4 (1:handles.nadditional_kick_point)+5]),handles.B(handles.matrix_range,:)-handles.R(handles.matrix_range,5)*handles.param(5,:),handles.weigth(handles.matrix_range));
    else
        [handles.param(1:4,:),handles.param_err(1:4,:)]=lscov(handles.R(handles.matrix_range,1:4),handles.B(handles.matrix_range,:)-handles.R(handles.matrix_range,5)*handles.param(5,:),handles.weigth(handles.matrix_range));
    end
else
    if(handles.nadditional_kick_point>0)
        [handles.param([1:5 (1:handles.nadditional_kick_point)+5],:),handles.param_err([1:5 (1:handles.nadditional_kick_point)+5],:)]=lscov(handles.R(handles.matrix_range,[1:5 (1:handles.nadditional_kick_point)+5]),handles.B(handles.matrix_range,:),handles.weigth(handles.matrix_range));
    else
        [handles.param(1:5,:),handles.param_err(1:5,:)]=lscov(handles.R(handles.matrix_range,1:5),handles.B(handles.matrix_range,:),handles.weigth(handles.matrix_range));
    end
end

% for pulse=1:10;
% figure(9)
% clf
% plot(handles.bpm_s,handles.R(1:handles.nbpm,:)*handles.param(:,pulse));
% hold on
% plot(handles.bpm_s,handles.dx_read(pulse,:),'o');
% plot(handles.bpm_s,handles.R(1:handles.nbpm,:)*diag(handles.param(:,pulse)),'--');
% hold off
% legend('reco','meas','X effect','X'' effect','Y effect','Y'' effect','E effect')
% pause(1);
% end

if (~any(handles.param(5,:)))
    handles=display_output(handles,'1 BPM in dispersive region needed !');
    return;
end

handles.dx_read_rec=(handles.R(1:handles.loaded.handles.nbpm,:)*handles.param).';
handles.dx_read_rec_err=(handles.R(1:handles.loaded.handles.nbpm,:)*handles.param_err).';
handles.dy_read_rec=(handles.R(handles.loaded.handles.nbpm+1:handles.loaded.handles.nbpm*2,:)*handles.param).';
handles.dy_read_rec_err=(handles.R(handles.loaded.handles.nbpm+1:handles.loaded.handles.nbpm*2,:)*handles.param_err).';

handles.dxp_read_rec=(reshape(handles.Rtot(1:handles.loaded.handles.nbpm,2,:),handles.loaded.handles.nbpm,handles.nparam+handles.nadditional_kick_point)*handles.param).';
handles.dxp_read_rec_err=(reshape(handles.Rtot(1:handles.loaded.handles.nbpm,2,:),handles.loaded.handles.nbpm,handles.nparam+handles.nadditional_kick_point)*handles.param).';
handles.dyp_read_rec=(reshape(handles.Rtot(1:handles.loaded.handles.nbpm,4,:),handles.loaded.handles.nbpm,handles.nparam+handles.nadditional_kick_point)*handles.param).';
handles.dyp_read_rec_err=(reshape(handles.Rtot(1:handles.loaded.handles.nbpm,4,:),handles.loaded.handles.nbpm,handles.nparam+handles.nadditional_kick_point)*handles.param).';
for i=1:handles.loaded.handles.nbpm
%     [handles.Dx_meas(i),handles.Dx_meas_err(i)]=noplot_polyfit(handles.param(5,:),handles.dx_read(:,i)-(handles.R(i,[2 3 4])*handles.param([2 3 4],:))',1,[0 1]);
%     [handles.Dy_meas(i),handles.Dy_meas_err(i)]=noplot_polyfit(handles.param(5,:),handles.dy_read(:,i)-(handles.R(handles.nbpm+i,[1 2 4])*handles.param([1 2 4],:))',1,[0 1]);
    [handles.Dx_meas(i),handles.Dx_meas_err(i)]=noplot_polyfit(handles.param(5,:),handles.dx_read(:,i),1,[0 1]);
    [handles.Dy_meas(i),handles.Dy_meas_err(i)]=noplot_polyfit(handles.param(5,:),handles.dy_read(:,i),1,[0 1]);
%    [handles.Coupl_meas(i),handles.Coupl_meas_err(i)]=noplot_polyfit(handles.dx_read(:,i),handles.dy_read(:,i),1,[0 1]);
end
handles.D=[handles.Dx_meas.'; handles.Dy_meas.'];
handles.D_err=[handles.Dx_meas_err.'; handles.Dy_meas_err.'];

handles.matrix_range_disp=[handles.bpm_range_disp handles.loaded.handles.nbpm+handles.bpm_range_disp];
handles.matrix_range_disp(find(handles.resol_nice(handles.matrix_range_disp)<1e-8))=[];

handles.D_design(1:handles.loaded.handles.nbpm)=handles.SimModel.Twiss.etax(handles.loaded.handles.bpm);
handles.D_design(handles.loaded.handles.nbpm+1:2*handles.loaded.handles.nbpm)=handles.SimModel.Twiss.etay(handles.loaded.handles.bpm);
%handles.D_design=reshape(handles.R(:,5),handles.loaded.handles.nbpm,2);


[handles.D_inj,handles.D_inj_err,mse]=lscov(handles.R(handles.matrix_range_disp,1:4),handles.D(handles.matrix_range_disp,:)-handles.R(handles.matrix_range_disp,5),1./handles.D_err(handles.matrix_range_disp,:).^2);
handles.D_rec=(handles.R(:,1:4)*handles.D_inj+handles.R(:,5)).';
handles.D_rec_err=abs(handles.R(:,1:4)*handles.D_inj_err).';
handles.Dx_rec=handles.D_rec(:,1:handles.loaded.handles.nbpm);
handles.Dx_rec_err=handles.D_rec_err(:,1:handles.loaded.handles.nbpm);
handles.Dy_rec=handles.D_rec(:,handles.loaded.handles.nbpm+1:handles.loaded.handles.nbpm*2);
handles.Dy_rec_err=handles.D_rec_err(:,handles.loaded.handles.nbpm+1:handles.loaded.handles.nbpm*2);

[handles.D_param_fit(1),handles.D_param_fit_err(1)]=noplot_polyfit(handles.param(5,:),handles.param(1,:),handles.param_err(1,:),[0 1]);
[handles.D_param_fit(2),handles.D_param_fit_err(2)]=noplot_polyfit(handles.param(5,:),handles.param(2,:),handles.param_err(2,:),[0 1]);
[handles.D_param_fit(3),handles.D_param_fit_err(3)]=noplot_polyfit(handles.param(5,:),handles.param(3,:),handles.param_err(3,:),[0 1]);
[handles.D_param_fit(4),handles.D_param_fit_err(4)]=noplot_polyfit(handles.param(5,:),handles.param(4,:),handles.param_err(4,:),[0 1]);
% handles.matrix_range_disp=[handles.bpm_range_disp];
% [handles.Dx_inj,handles.Dx_inj_err]=lscov(handles.R(handles.matrix_range_disp,1:2),handles.D(handles.matrix_range_disp,:)-handles.R(handles.matrix_range_disp,5),handles.D_err(handles.matrix_range_disp,:));
% handles.Dx_rec=(handles.R(1:handles.loaded.handles.nbpm,1:2)*handles.Dx_inj+handles.R(1:handles.loaded.handles.nbpm,5)).';
% handles.Dx_rec_err=(handles.R(1:handles.loaded.handles.nbpm,1:2)*handles.Dx_inj_err).';
% 
% handles.matrix_range_disp=[handles.nbpm+handles.bpm_range_disp];
% [handles.Dy_inj,handles.Dy_inj_err]=lscov(handles.R(handles.matrix_range_disp,3:4),handles.D(handles.matrix_range_disp,:)-handles.R(handles.matrix_range_disp,5),handles.D_err(handles.matrix_range_disp,:));
% handles.Dy_rec=(handles.R(handles.loaded.handles.nbpm+1:end,3:4)*handles.Dy_inj+handles.R(handles.loaded.handles.nbpm+1:end,5)).';
% handles.Dy_rec_err=(handles.R(handles.loaded.handles.nbpm+1:end,3:4)*handles.Dy_inj_err).';
% 
% handles.D_inj=[handles.Dx_inj handles.Dy_inj];
% handles.D_inj_err=[handles.Dx_inj_err handles.Dy_inj_err];

set(handles.bpm_corr,'Enable','on');
%set(handles.bpm_corr,'String',handles.loaded.handles.bpm_name(handles.bpm_range,:));
set(handles.bpm_corr,'String',handles.loaded.handles.bpm_name);
set(handles.histo_param,'Enable','on');
set(handles.histo_param,'String',strvcat('X','X''','Y','Y''','dE/E','X''(KEX2B)','Y''(KEX2B)'));
set(handles.param_evol_btn,'Enable','on');
set(handles.plot_dispertion,'Enable','on');
set(handles.plot_resol_fit,'Enable','on');
set(handles.correct_btn,'Enable','on');
set(handles.scale_factor_btn,'Enable','on');
set(handles.fluctuation_size,'Enable','on');
set(handles.weigth_btn,'Enable','on');

result_display1=sprintf('@%s : Dx=%3.3g +-%3.3g mm Dx''=%3.3g +-%3.3g mrad Dy=%3.3g +-%3.3g mm Dy''=%3.3g +-%3.3g mrad',...
    handles.fit_point,handles.D_inj(1)*1e3,abs(handles.D_inj_err(1))*1e3,handles.D_inj(2)*1e3,abs(handles.D_inj_err(2))*1e3,handles.D_inj(3)*1e3,abs(handles.D_inj_err(3))*1e3,handles.D_inj(4)*1e3,abs(handles.D_inj_err(4))*1e3);
%result_display2=sprintf('Dx=%3.3g +-%3.3g mm Dx''=%3.3g +-%3.3g mrad Dy=%3.3g +-%3.3g mm Dy''=%3.3g +-%3.3g mrad',...
%    handles.D_param_fit(1)*1e3,abs(handles.D_param_fit_err(1))*1e3,handles.D_param_fit(2)*1e3,abs(handles.D_param_fit_err(2))*1e3,handles.D_param_fit(3)*1e3,abs(handles.D_param_fit_err(3))*1e3,handles.D_param_fit(4)*1e3,abs(handles.D_param_fit_err(4))*1e3);
result_display2='';
set(handles.result_txt,'String',strvcat(result_display1,result_display2));

handles=display_output(handles,'done');

%handles=compute_cor_Dx(handles);
%handles=compute_cor_Dy(handles);

handles=plot_dispertion_Callback(handles.plot_dispertion, eventdata, handles);

guidata(hObject, handles);


% --- Executes on selection change in bpm_corr.
function bpm_corr_Callback(hObject, eventdata, handles)
% hObject    handle to bpm_corr (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = get(hObject,'String') returns bpm_corr contents as cell array
%        contents{get(hObject,'Value')} returns selected item from bpm_corr
bpm=get(hObject,'Value');
bpms_name=get(hObject,'String');
bpm_name=bpms_name(bpm,:);
range_fit=[min(handles.param(5,:)) max(handles.param(5,:))];
figure(2)
subplot(2,1,1)
%plot(handles.param(5,:),handles.dx_read(:,bpm)-handles.noise(handles.noise_range,bpm),'b+');
plot(handles.param(5,:),handles.dx_read(:,bpm),'b+');
hold on
%plot(handles.param(5,:),handles.dx_read(:,bpm)-(handles.R(bpm,[2 3 4])*handles.param([2 3 4],:))'-handles.noise(handles.noise_range,bpm),'k+') 
%plot(handles.dE_alpha,handles.dx_read(19:179,bpm),'c+');
plot(range_fit,handles.Dx_meas(bpm)*range_fit,'b-');
%[D_poor,D_poor_err]=noplot_polyfit(handles.dE_alpha,handles.dx_read(19:179,bpm),1,1);
%plot(range_fit,D_poor(2)*range_fit,'c-');
plot(range_fit,handles.Dx_rec(bpm)*range_fit,'b--');
hold off
subplot(2,1,2)
%plot(handles.param(5,:),handles.dy_read(:,bpm)-handles.noise(handles.noise_range,handles.nbpm+bpm),'r+');
plot(handles.param(5,:),handles.dy_read(:,bpm),'r+');
hold on
%plot(handles.param(5,:),handles.dy_read(:,bpm)-(handles.R(handles.nbpm+bpm,[1 2 4])*handles.param([1 2 4],:))'-handles.noise(handles.noise_range,handles.nbpm+bpm),'k+') 
plot(range_fit,handles.Dy_meas(bpm)*range_fit,'r-');
plot(range_fit,handles.Dy_rec(bpm)*range_fit,'r--');
hold off
%h=legend('X measurement with $$\frac{\Delta E}{E}_{rec}$$','X measurement using $$\alpha_{c~fit}$$',...
%    ['$$D_x=' sprintf('%.3g \\pm %.1g m',handles.Dx_meas(bpm),handles.Dx_meas_err(bpm)) '$$ with $$\frac{\Delta E}{E}_{rec}$$'],...
%    ['$$D_x=' sprintf('%.3g \\pm %.1g m',D_poor(2),D_poor_err(2)) '$$ with  $$\alpha_{c~fit}$$']);
%set(h,'Interpreter','latex');
xlabel('$$\frac{\Delta E}{E}$$','Interpreter','latex');
ylabel('BPM Read [m]');
title(sprintf('%s Disperssion fit',bpm_name))
enhance_plot
guidata(hObject, handles);

% --- Executes during object creation, after setting all properties.
function bpm_corr_CreateFcn(hObject, eventdata, handles)
% hObject    handle to bpm_corr (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% --- Executes on button press in param_evol_btn.
function param_evol_btn_Callback(hObject, eventdata, handles)
% hObject    handle to param_evol_btn (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
%dE=handles.param(5,19:179);
%df=[zeros(1,11) -3*ones(1,13) -2*ones(1,17) -1*ones(1,16) zeros(1,8) ones(1,12) 2*ones(1,7) 3*ones(1,11) 2*ones(1,20) ones(1,13) zeros(1,8) -ones(1,8) -2*ones(1,8) -3*ones(1,9)]*10^3;
%figure(6)
%[p,perr]=noplot_polyfit(dE,-df/714e6,1,1);
%handles.dE_alpha=(-df/714e6-p(1))/p(2);
%enhance_plot();
figure(3)
clf
errorbar(handles.param.',handles.param_err.','.');
hold on;
%plot(19:(18+length(df)),handles.dE_alpha,'c-.');%disp 10 dec avec cav
hold off
legend_txt=strvcat(['X(' handles.fit_point ')[m]'],['X''(' handles.fit_point ')[m]'],['Y(' handles.fit_point ')[m]'],['Y''(' handles.fit_point ')[m]'],' ','$$\frac{\Delta E}{E}$$','X''(KEX2B)','Y''(KEX2B)');
h=legend(legend_txt([handles.param_range],:));
set(h,'Interpreter','latex')
xlabel('pulse'); ylabel('parameter value');
grid on
xlim([0 length(handles.read_range)]);
enhance_plot();
% figure(4)
% clf
% subplot(4,4,1)
% plot(handles.param(1,:).',handles.param(2,:).','.');
% subplot(4,4,2)
% plot(handles.param(1,:).',handles.param(3,:).','.');
% subplot(4,4,3)
% plot(handles.param(1,:).',handles.param(4,:).','.');
% subplot(4,4,4)
% plot(handles.param(1,:).',handles.param(5,:).','.');
% subplot(4,4,6)
% plot(handles.param(2,:).',handles.param(3,:).','.');
% subplot(4,4,7)
% plot(handles.param(2,:).',handles.param(4,:).','.');
% subplot(4,4,8)
% plot(handles.param(2,:).',handles.param(5,:).','.');
% subplot(4,4,11)
% plot(handles.param(3,:).',handles.param(4,:).','.');
% subplot(4,4,12)
% plot(handles.param(3,:).',handles.param(5,:).','.');
% subplot(4,4,16)
% plot(handles.param(4,:).',handles.param(5,:).','.');
guidata(hObject, handles);




function output_txt_Callback(hObject, eventdata, handles)
% hObject    handle to output_txt (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of output_txt as text
%        str2double(get(hObject,'String')) returns contents of output_txt as a double


% --- Executes during object creation, after setting all properties.
function output_txt_CreateFcn(hObject, eventdata, handles)
% hObject    handle to output_txt (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in histo_param.
function histo_param_Callback(hObject, eventdata, handles)
% hObject    handle to histo_param (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = get(hObject,'String') returns histo_param contents as cell array
%        contents{get(hObject,'Value')} returns selected item from histo_param
figure(2)
clf;
param_chosen=get(hObject,'Value');
legend_txt=strvcat('X','X''','Y','Y''','$$\frac{\Delta E}{E}$$','X''','Y''');
legend_txt2=strvcat('um','urad','um','urad',' ','urad','urad');
if(param_chosen~=5)
    rms=std(handles.param(param_chosen,:))*1e6;
    drms=mean(handles.param_err(param_chosen,:))*1e6;
    hist(handles.param(param_chosen,:),(min(handles.param(param_chosen,:))-drms*1e-6):drms*1e-6:(max(handles.param(param_chosen,:))+drms*1e-6));
else
    rms=std(handles.param(param_chosen,:));
    drms=mean(handles.param_err(param_chosen,:));
    hist(handles.param(param_chosen,:),(min(handles.param(param_chosen,:))-drms):drms:(max(handles.param(param_chosen,:))+drms));
end
if(param_chosen<6)
    h=legend( sprintf('%s(%s) rms=%.3g +/- %.3g %s',legend_txt(param_chosen,:),handles.fit_point,rms,drms,legend_txt2(param_chosen,:)));
else
    h=legend( sprintf('%s(%s) rms=%.3g +/- %.3g %s',legend_txt(param_chosen,:),'KEX2',rms,drms,legend_txt2(param_chosen,:)));
end
set(h,'Interpreter','latex')
enhance_plot();
guidata(hObject, handles);


% --- Executes during object creation, after setting all properties.
function histo_param_CreateFcn(hObject, eventdata, handles)
% hObject    handle to histo_param (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in load_btn.
function handles=load_btn_Callback(hObject, eventdata, handles)

% hObject    handle to load_btn (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
handles=display_output(handles,sprintf('loading %s',handles.filename_load));
handles.loaded=load(handles.filename_load);

global BEAMLINE PS GIRDER INSTR

BEAMLINE=handles.loaded.BEAMLINE;
PS=handles.loaded.PS;
GIRDER=handles.loaded.GIRDER;
INSTR=handles.loaded.INSTR;
handles.SimModel=handles.loaded.FL.SimModel;

set(handles.chose_bpm_btn,'Enable','on');
set(handles.bpm_disp_btn,'Enable','on');
set(handles.fit_btn,'Enable','on');
set(handles.fit_point_txt,'Enable','on');
set(handles.read_range_txt,'Enable','on');
set(handles.read_range_txt,'String',sprintf('[%i:%i]',1,size(handles.loaded.handles.raw,1)));
handles.read_range=1:size(handles.loaded.handles.raw,1);

handles=good_bad_bpm(handles);

handles=display_output(handles,'done');
handles=fit_btn_Callback(handles.fit_btn, eventdata, handles);

guidata(hObject, handles);

function handles=good_bad_bpm(handles)
handles.raw=handles.loaded.handles.raw(handles.read_range,:);
nread=size(handles.raw,1);
x_read=handles.raw(:,handles.loaded.handles.bpm_index*3-1);
y_read=handles.raw(:,handles.loaded.handles.bpm_index*3);
nbpm=size(x_read,2);
dx_read=x_read-(ones(nread,1)*sum(x_read,1)/nread);
dy_read=y_read-(ones(nread,1)*sum(y_read,1)/nread);
B=[dx_read dy_read ];
B_strip=zeros(nread,nbpm*2);
B_cbpm=zeros(nread,nbpm*2);
for i=1:nbpm
    if(~isempty(strfind(handles.bpm_type(i,:),'stripline')) || ~isempty(strfind(handles.bpm_type(i,:),'button')) )
        B_strip(:,[i nbpm+i])=B(:,[i nbpm+i]);
    end
    if(~isempty(strfind(handles.bpm_type(i,:),'ccav')) || ~isempty(strfind(handles.bpm_type(i,:),'scav')) )
        B_cbpm(:,[i nbpm+i])=B(:,[i nbpm+i]);
    end
end

[U,S,V]=svd(B);
S_cor=zeros(size(S));
S_cor(1:20,1:20)=diag(diag(S(1:20,1:20)));
B_cor=U*S_cor*V';
handles.resol_old=std(B-B_cor);

[x,y]=find(abs(V(:,1:20))>0.65);
mode_ok=1:20;
for i=unique(x).'
    B(:,i)=zeros(nread,1);
end
[U,S,V]=svd(B);
S_cor=zeros(size(S));
S_cor(mode_ok,mode_ok)=diag(diag(S(mode_ok,mode_ok)));
B_cor=U*S_cor*V';

handles.resol=std(B-B_cor);

for i=[1:11 15:17]
    B(:,i)=zeros(nread,1);
end
[U,S,V]=svd(B);
S_cor=zeros(size(S));
S_cor(mode_ok,mode_ok)=diag(diag(S(mode_ok,mode_ok)));
B_cor=U*S_cor*V';

handles.resol_new=std(B-B_cor);
handles.noise=zeros(nread,nbpm*2);
handles.resol_nice=zeros(nbpm,2);
B_resol=[];
for i=1:nbpm
    if(~isempty(strfind(handles.bpm_type(i,:),'stripline')) || ~isempty(strfind(handles.bpm_type(i,:),'button')) )
        B_resol=[B_strip ones(nread,1)];
    end
    if(~isempty(strfind(handles.bpm_type(i,:),'ccav')) || ~isempty(strfind(handles.bpm_type(i,:),'scav')) )
        B_resol=[B_cbpm ones(nread,1)];
    end
    handles.noise(:,[i nbpm+i])=B_resol(:,[i nbpm+i])-  B_resol(:,[1:i-1 i+1:nbpm nbpm+1:nbpm+i-1 nbpm+i+1:2*nbpm+1])*lscov(B_resol(:,[1:i-1 i+1:nbpm nbpm+1:nbpm+i-1 nbpm+i+1:2*nbpm+1]),B_resol(:,[i nbpm+i]));
    handles.resol_nice(i,:)=std(handles.noise(:,[i nbpm+i]));
end

handles.spread=std([dx_read dy_read]);


function fit_point_txt_Callback(hObject, eventdata, handles)
% hObject    handle to fit_point_txt (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of fit_point_txt as text
%        str2double(get(hObject,'String')) returns contents of fit_point_txt as a double

global BEAMLINE

handles.fit_point=get(hObject,'String');
handles.fit_point_index=findcells(BEAMLINE,'Name',handles.fit_point);
handles.fit_point_index=handles.fit_point_index(1);
handles.fit_point_s=BEAMLINE{handles.fit_point_index}.S;
guidata(hObject, handles);


% --- Executes during object creation, after setting all properties.
function fit_point_txt_CreateFcn(hObject, eventdata, handles)
% hObject    handle to fit_point_txt (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function read_range_txt_Callback(hObject, eventdata, handles)
% hObject    handle to read_range_txt (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of read_range_txt as text
%        str2double(get(hObject,'String')) returns contents of read_range_txt as a double
handles.read_range=eval(get(hObject,'String'));
guidata(hObject, handles);


% --- Executes during object creation, after setting all properties.
function read_range_txt_CreateFcn(hObject, eventdata, handles)
% hObject    handle to read_range_txt (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in plot_resol_fit.
function plot_resol_fit_Callback(hObject, eventdata, handles)
% hObject    handle to plot_resol_fit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = get(hObject,'String') returns plot_resol_fit contents as cell array
%        contents{get(hObject,'Value')} returns selected item from plot_resol_fit
axe=get(hObject,'Value');
global BEAMLINE
IEX=findcells(BEAMLINE,'Name','IEX');
IEX_s=BEAMLINE{IEX}.S;
% h0=figure(1);
% clf
% set(h0,'Position',[0 0 1600 600]);
% h1=axes('Position',[0.1 0.8 0.8 .2]);
% plot_magnets_external(BEAMLINE,h1,handles.xlim(1)+IEX_s,handles.xlim(2)+IEX_s);
% gcf;
% h2=axes('Position',[0.1 0.3 0.8 .5]);
axes(handles.plot)
if(axe==1)
    plot(handles.loaded.handles.bpm_s,std(handles.dx_read-handles.noise(handles.noise_range,1:handles.nbpm)-handles.dx_read_rec,0,1),'r')
    hold on
%    plot(handles.loaded.handles.bpm_s,handles.resol(1:handles.loaded.handles.nbpm),'b') 
%    plot(handles.loaded.handles.bpm_s,handles.resol_old(1:handles.loaded.handles.nbpm),'b--') 
%    plot(handles.loaded.handles.bpm_s,handles.resol_new(1:handles.loaded.handles.nbpm),'b-.') 
%    plot(handles.loaded.handles.bpm_s,std(handles.dx2_read_rec,0,1),'y')    
    plot(handles.loaded.handles.bpm_s,handles.resol_nice(1:handles.loaded.handles.nbpm,1),'b') 
%    plot(handles.loaded.handles.bpm_s,handles.spread(1:handles.loaded.handles.nbpm),'g') 
    hold off
else
    plot(handles.loaded.handles.bpm_s,std(handles.dy_read-handles.noise(handles.noise_range,handles.nbpm+1:2*handles.nbpm)-handles.dy_read_rec,0,1),'r')
    hold on
%    plot(handles.loaded.handles.bpm_s,handles.resol(handles.loaded.handles.nbpm+1:end),'b')    
%    plot(handles.loaded.handles.bpm_s,handles.resol_old(handles.loaded.handles.nbpm+1:end),'b--') 
%    plot(handles.loaded.handles.bpm_s,handles.resol_new(1:handles.loaded.handles.nbpm),'b-.') 
%    plot(handles.loaded.handles.bpm_s,std(handles.dy2_read_rec,0,1),'y')
    plot(handles.loaded.handles.bpm_s,handles.resol_nice(1:handles.loaded.handles.nbpm,2),'b') 
%    plot(handles.loaded.handles.bpm_s,handles.spread(handles.loaded.handles.nbpm+1:end),'g') 
    hold off
end
legend('fit precision','bpm resol');
% ylabel('rms(read-reconstruction)');
xlim(handles.xlim+handles.IEX_s);
bpms=find((handles.bpm_s>(handles.xlim(1)+handles.IEX_s)) .* (handles.bpm_s<(handles.xlim(2)+handles.IEX_s)));
set(gca,'XTick',handles.bpm_s(bpms));
set(gca,'XTickLabel',handles.bpm_name(bpms,:));
handles.hText=xticklabel_rotate90();
yautoscale=get(gca,'Ylim');
set(handles.ylim_txt,'String',sprintf('[%.3g %.3g]',yautoscale));
handles.ylim=yautoscale;
ylim(handles.ylim);
grid on
% enhance_plot()
guidata(hObject, handles);

% --- Executes during object creation, after setting all properties.
function plot_resol_fit_CreateFcn(hObject, eventdata, handles)
% hObject    handle to plot_resol_fit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in plot_dispertion.
function handles=plot_dispertion_Callback(hObject, eventdata, handles)
% hObject    handle to plot_dispertion (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = get(hObject,'String') returns plot_dispertion contents as cell array
%        contents{get(hObject,'Value')} returns selected item from plot_dispertion
handles=display_output(handles,'plotting...');
axe=get(hObject,'Value');
axes(handles.plot)
if(axe==1)
    errorbar(handles.loaded.handles.bpm_s,handles.Dx_meas,handles.Dx_meas_err,'k+')
    hold on
    plot(handles.loaded.handles.bpm_s,handles.Dx_rec,'r-')
%    plot(handles.loaded.handles.bpm_s,handles.Dx_rec.'-handles.Dx_sensibility(1:handles.loaded.handles.nbpm,:)*handles.dPS_Dx,'b')
    plot(handles.loaded.handles.bpm_s,handles.D_design(1:handles.loaded.handles.nbpm),'g--')
%     errorbar(handles.loaded.handles.bpm_s(handles.bpm_range_disp),handles.Dx_rec(handles.bpm_range_disp),handles.Dx_rec_err(handles.bpm_range_disp),'r.')
    hold off
    ylabel('Dx [m]');
%    handles=display_output(handles,sprintf('dPS(QF1X)=%3.3f+-%3.3f dPS(QF6X)=%3.3f+-%3.3f',-handles.dPS_Dx(1),handles.dPS_Dx_err(1),-handles.dPS_Dx(2),handles.dPS_Dx_err(2)));
end
if(axe==2)
    errorbar(handles.loaded.handles.bpm_s,handles.Dy_meas,handles.Dy_meas_err,'k+')
    hold on
    plot(handles.loaded.handles.bpm_s,handles.Dy_rec,'r')
%    plot(handles.loaded.handles.bpm_s,handles.Dy_rec.'-handles.Dy_sensibility(handles.loaded.handles.nbpm+1:end,:)*handles.dPS_Dy,'b')
    plot(handles.loaded.handles.bpm_s,handles.D_design(handles.loaded.handles.nbpm+1:end),'g--')
%     errorbar(handles.loaded.handles.bpm_s(handles.bpm_range_disp),handles.Dy_rec(handles.bpm_range_disp),handles.Dy_rec_err(handles.bpm_range_disp),'r.')
    hold off
    ylabel('Dy [m]');
%    handles=display_output(handles,sprintf('dPS(QS1X)=%3.3f+-%3.3f dPS(QS2X)=%3.3f+-%3.3f',-handles.dPS_Dy(1),handles.dPS_Dy_err(1),-handles.dPS_Dy(2),handles.dPS_Dy_err(2)));
end
% if(axe==3)
%     errorbar(handles.loaded.handles.bpm_s,handles.Coupl_meas,handles.Coupl_meas_err,'k+');
%     grid on;
% end
yautoscale=get(gca,'Ylim');
set(handles.ylim_txt,'String',sprintf('[%.3g %.3g]',yautoscale));
handles.ylim=yautoscale;
ylim(handles.ylim);
xlim(handles.xlim+handles.IEX_s);
if(axe==3)
    legend('measurement');
else
    legend('measurement','reconstruction','D_{design}');
end
set(gca,'XTick',handles.bpm_s);
set(gca,'XTickLabel',handles.bpm_name);
handles.hText=xticklabel_rotate90();
handles=display_output(handles,'done');

guidata(hObject, handles);


% --- Executes during object creation, after setting all properties.
function plot_dispertion_CreateFcn(hObject, eventdata, handles)
% hObject    handle to plot_dispertion (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end




function handles=display_output(handles,string)
    [nline,ncarac]=size(string);
    string_resized='';
    for line=1:nline
        string_resized=strvcat(string_resized,reshape([string(line,:) blanks(ceil(ncarac/38)*38-nline*ncarac)],38,ceil(ncarac/38))');
    end
    handles.out=strvcat(string_resized,handles.out);
    set(handles.output_txt,'String',handles.out);



function result_txt_Callback(hObject, eventdata, handles)
% hObject    handle to result_txt (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of result_txt as text
%        str2double(get(hObject,'String')) returns contents of result_txt as a double


% --- Executes during object creation, after setting all properties.
function result_txt_CreateFcn(hObject, eventdata, handles)
% hObject    handle to result_txt (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in bpm_disp_btn.
function bpm_disp_btn_Callback(hObject, eventdata, handles)
% hObject    handle to bpm_disp_btn (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
[selection,ok]=listdlg('PromptString',{'Select bpms :' '' '(Shift+clik = continuous selection)' '(Crtl+clik = uncontinuous selection)'},...
            'ListString',handles.loaded.handles.bpm_name,'ListSize',[250 min(handles.loaded.handles.nbpm*15,500)],...
            'InitialValue',handles.bpm_range_disp,'Name','Select BPM');
 if(ok)
     handles.bpm_range_disp=selection;
 end

guidata(hObject, handles);

function disp=get_D(handles)
global FL BEAMLINE

beam_E0=FL.SimBeam{1};
beam_E1=FL.SimBeam{1};
beam_E1.Bunch.x(6,:)=(1+3e-3)*beam_E1.Bunch.x(6,:);

[s,beam_mat,instdata]=TrackThru(handles.IEX,handles.IP,beam_E0,1,1);
bpm_read_E0=zeros(2,handles.nbpm);
for i=1:length(instdata{1})
    bpm_i=find(instdata{1}(i).Index==handles.bpm);
    if(~isempty(bpm_i))
        bpm_read_E0(1,bpm_i)=instdata{1}(i).x;
        bpm_read_E0(2,bpm_i)=instdata{1}(i).y;
    end
end
[s,beam_mat,instdata]=TrackThru(handles.IEX,handles.IP,beam_E1,1,1);
bpm_read_E1=zeros(2,handles.nbpm);
for i=1:length(instdata{1})
    bpm_i=find(instdata{1}(i).Index==handles.bpm);
    if(~isempty(bpm_i))
        bpm_read_E1(1,bpm_i)=instdata{1}(i).x;
        bpm_read_E1(2,bpm_i)=instdata{1}(i).y;
    end
end
disp=(bpm_read_E1-bpm_read_E0)/3e-3;


function handles=compute_cor_Dx(handles)
global BEAMLINE PS FL

handles=display_output(handles,'compute Dx correction...');

QF1X=findcells(BEAMLINE,'Name','QF1X');
QF6X=findcells(BEAMLINE,'Name','QF6X');
handles.Dx_magnets=[QF1X QF6X];
handles.Dx_ps=[];
for i=handles.Dx_magnets
    handles.Dx_ps(end+1)=BEAMLINE{i}.PS;
end
[handles.Dx_ps,order]=unique(handles.Dx_ps);
nps=length(handles.Dx_ps);
ps_read_old=zeros(1,nps);
for j=1:nps
    ps_read_old(j)=PS(handles.Dx_ps(j)).Ampl;
end
D_read_ref=get_D(handles);

handles.Dx_sensibility=zeros(handles.loaded.handles.nbpm*2,nps);
for j=1:nps
    PS(handles.Dx_ps(j)).Ampl=ps_read_old(j)+0.1;
    D_read_ps=get_D(handles);

    handles.Dx_sensibility(1:handles.loaded.handles.nbpm,j)=(D_read_ps(1,:)-D_read_ref(1,:))/0.1;
    handles.Dx_sensibility(handles.loaded.handles.nbpm+1:2*handles.loaded.handles.nbpm,j)=(D_read_ps(2,:)-D_read_ref(2,:))/0.1;
    PS(handles.Dx_ps(j)).Ampl=ps_read_old(j);
end

QF1X=findcells(BEAMLINE,'Name','QF1X');
QF6X=findcells(BEAMLINE,'Name','QF6X');
quads=[QF1X QF6X];
M=zeros(handles.nbpm,length(quads));
for i=1:handles.nbpm
    for j=1:length(quads)
        if(quads(j)<handles.bpm(i)) 
            [s,r]=RmatAtoB(quads(j),handles.bpm(i));
            b=BEAMLINE{quads(j)}.B;
            ps=PS(BEAMLINE{quads(j)}.PS).Ampl;
            M(i,j)=r(1,2);
        else
            M(i,j)=0;
        end
    end
end


handles.D_design(1:handles.loaded.handles.nbpm)=handles.SimModel.Twiss.etax(handles.loaded.handles.bpm);
handles.D_design(handles.loaded.handles.nbpm+1:2*handles.loaded.handles.nbpm)=handles.SimModel.Twiss.etay(handles.loaded.handles.bpm);

[handles.dPS_Dx,handles.dPS_Dx_err]=lscov(handles.Dx_sensibility(handles.bpm_range_disp,:),handles.D_rec(handles.bpm_range_disp).'-handles.D_design(handles.bpm_range_disp).',1./(handles.D_rec_err(handles.bpm_range_disp).').^2);
handles=display_output(handles,'done');

function handles=compute_cor_Dy(handles)
global BEAMLINE PS FL

handles=display_output(handles,'compute Dy correction...');
QS1X=findcells(BEAMLINE,'Name','QS1X');
QS2X=findcells(BEAMLINE,'Name','QS2X');
handles.Dy_magnets=[QS1X QS2X];
handles.Dy_ps=[];
for i=handles.Dy_magnets
    handles.Dy_ps(end+1)=BEAMLINE{i}.PS;
end
[handles.Dy_ps,order]=unique(handles.Dy_ps);
nps=length(handles.Dy_ps);
for j=1:nps
    ps_read_old(j)=PS(handles.Dy_ps(j)).Ampl;
end
D_read_ref=get_D(handles);

handles.Dy_sensibility=zeros(handles.loaded.handles.nbpm*2,nps);
for j=1:nps
    PS(handles.Dy_ps(j)).Ampl=ps_read_old(j)+0.1;
    D_read_ps=get_D(handles);

    handles.Dy_sensibility(1:handles.loaded.handles.nbpm,j)=(D_read_ps(1,:)-D_read_ref(1,:))/0.1;
    handles.Dy_sensibility(handles.loaded.handles.nbpm+1:2*handles.loaded.handles.nbpm,j)=(D_read_ps(2,:)-D_read_ref(2,:))/0.1;
    PS(handles.Dy_ps(j)).Ampl=ps_read_old(j);
end
handles.D_design(1:handles.loaded.handles.nbpm)=handles.SimModel.Twiss.etax(handles.loaded.handles.bpm);
handles.D_design(handles.loaded.handles.nbpm+1:2*handles.loaded.handles.nbpm)=handles.SimModel.Twiss.etay(handles.loaded.handles.bpm);

[handles.dPS_Dy,handles.dPS_Dy_err]=lscov(handles.Dy_sensibility(handles.bpm_range_disp+handles.nbpm,:),handles.D_rec(handles.bpm_range_disp+handles.nbpm).'-handles.D_design(handles.bpm_range_disp+handles.nbpm).',1./(handles.D_rec_err(handles.bpm_range_disp+handles.nbpm).').^2);
handles=display_output(handles,'done');

% function handles=compute_cor_Dx_old(handles)
% global BEAMLINE PS FL
% 
% QF1X=findcells(BEAMLINE,'Name','QF1X');
% QF6X=findcells(BEAMLINE,'Name','QF6X');
% nquad=2;
% handles.RQF=zeros(handles.loaded.handles.nbpm*2,nquad);
% QF_KL=zeros(nquad,1);
% handles.QF_PS=zeros(nquad,1);
% QF_B=zeros(nquad,1);
% q_parts=[QF1X; QF6X];
% handles.D_design=zeros(1,2*handles.loaded.handles.nbpm);
% for i=1:handles.loaded.handles.nbpm
%     handles.D_design(i)=FL.SimModel.Twiss.etax(handles.loaded.handles.bpm(i));
%     handles.D_design(handles.loaded.handles.nbpm+i)=FL.SimModel.Twiss.etay(handles.loaded.handles.bpm(i));
%     for q_part=[QF1X QF6X]
%         if (handles.loaded.handles.bpm(i)<=q_part)
%             r=zeros(6,6);
%         else
%             [s,r]=RmatAtoB(q_part+1,handles.loaded.handles.bpm(i));
%         end
%         [quad,part]=find(q_part==q_parts);
%         handles.QF_PS(quad)=PS(BEAMLINE{q_part}.PS).Ampl;
%         QF_B(quad)=QF_B(quad)+BEAMLINE{q_part}.B;
%         handles.P=BEAMLINE{q_part}.P;
%         QF_KL(quad)=QF_KL(quad)+QF_B(quad)*handles.QF_PS(quad)*0.2998/handles.P;
%         handles.RQF(i,quad)=handles.RQF(i,quad)+r(1,2);        
%         handles.RQF(handles.loaded.handles.nbpm+i,quad)=handles.RQF(handles.loaded.handles.nbpm+i,quad)+r(3,4);
%     end
% end
% [handles.QF_dDxp,handles.QF_dDxp_err]=lscov(handles.RQF(handles.bpm_range_disp,:),handles.D_rec(handles.bpm_range_disp).'-handles.D_design(handles.bpm_range_disp).');
% Dx_QF1X=mean(FL.SimModel.Twiss.etax(QF1X));
% Dx_QF6X=mean(FL.SimModel.Twiss.etax(QF6X));
% Dxp_QF1X=mean(FL.SimModel.Twiss.etapx(QF1X));
% Dxp_QF6X=mean(FL.SimModel.Twiss.etapx(QF6X));
% L_QF1X=BEAMLINE{QF1X(1)}.L*2;
% L_QF6X=BEAMLINE{QF6X(2)}.L*2;
% 
% handles.dPS_QF1X=handles.QF_dDxp(1)/(Dx_QF1X+L_QF1X*Dxp_QF1X)*2;
% handles.dPS_QF6X=handles.QF_dDxp(2)/(Dx_QF6X+L_QF6X*Dxp_QF6X)*2;
% 
% 
% function handles=compute_cor_Dy_old(handles)
% global BEAMLINE PS GIRDER INSTR FL
% 
% QK1X=findcells(BEAMLINE,'Name','QK1X');
% QK4X=findcells(BEAMLINE,'Name','QK4X');
% nquad=2;
% handles.RQK=zeros(handles.loaded.handles.nbpm*2,nquad);
% QK_KL=zeros(nquad,1);
% QK_PS=zeros(nquad,1);
% QK_B=zeros(nquad,1);
% q_parts=[QK1X; QK4X];
% handles.D_design=zeros(1,2*handles.loaded.handles.nbpm);
% for i=1:handles.loaded.handles.nbpm
%     handles.D_design(i)=FL.SimModel.Twiss.etax(handles.loaded.handles.bpm(i));
%     handles.D_design(handles.loaded.handles.nbpm+i)=FL.SimModel.Twiss.etay(handles.loaded.handles.bpm(i));
%     for q_part=[QK1X QK4X]
%         Dx_QK=FL.SimModel.Twiss.etax(q_part);
%         Dxp_QK=FL.SimModel.Twiss.etapx(q_part);
%         Dy_QK=FL.SimModel.Twiss.etay(q_part);
%         Dyp_QK=FL.SimModel.Twiss.etapy(q_part);
%         if (handles.loaded.handles.bpm(i)<=q_part)
%             r=zeros(6,6);
%         else
%             [s,r]=RmatAtoB(q_part+1,handles.loaded.handles.bpm(i));
%             r=r*[1 0 0 0 0 0;0 1 1 0 0 0;0 0 1 0 0 0;1 0 0 1 0 0; zeros(2,6)];
%         end
%         [quad,part]=find(q_part==q_parts);
%         handles.QK_PS(quad)=PS(BEAMLINE{q_part}.PS).Ampl;
%         QK_B(quad)=QK_B(quad)+BEAMLINE{q_part}.B;
%         handles.P=BEAMLINE{q_part}.P;
%         QK_KL(quad)=QK_KL(quad)+QK_B(quad)*QK_PS(quad)*0.2998/handles.P;
%         handles.RQK(i,quad)=handles.RQF(i,quad)+r(1,1:4)*[Dx_QK;Dxp_QK; Dy_QK;Dyp_QK];
%         handles.RQK(handles.loaded.handles.nbpm+i,quad)=handles.RQK(handles.loaded.handles.nbpm+i,quad)+r(3,1:4)*[Dx_QK;Dxp_QK; Dy_QK;Dyp_QK]*-1;
%     end
% end
% 
% [handles.cor_Dy,handles.cor_Dy_err]=lscov(handles.RQK(handles.loaded.handles.nbpm+handles.bpm_range_disp,:),handles.D_rec(handles.loaded.handles.nbpm+handles.bpm_range_disp).'-handles.D_design(handles.loaded.handles.nbpm+handles.bpm_range_disp).');

% --- Executes on button press in correct_btn.
function correct_btn_Callback(hObject, eventdata, handles)
% hObject    handle to correct_btn (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global PS
ButtonName = questdlg('What correction to apply ?', ...
                         'Correction', ...
                         'Dx', 'Dy','Abort','Dx');
switch ButtonName
    case 'Dx'
        handles=display_output(handles,'apply Dx correction');
        [s]=AccessRequest({[] [handles.Dx_ps] []});
        if(~s{1})
            handles=display_output(handles,['error in AccesRequest :' s{2}]);
            return;
        end
        for i=1:length(handles.Dx_ps)
            PS(handles.Dx_ps(i)).SetPt=PS(handles.Dx_ps(i)).Ampl-handles.dPS_Dx(i);
        end
        s=PSTrim(handles.Dx_ps,1);
        if(~s{1})
            handles=display_output(handles,['error in PSTrim :' s{2}]);
            return;
        end
        handles=display_output(handles,'done');
    case 'Dy'
        handles=display_output(handles,'apply Dx correction');
        s=AccessRequest({[] [handles.Dy_ps] []});
        if(~s{1})
            handles=display_output(handles,['error in AccesRequest :' s{2}]);
            return;
        end
        for i=1:length(handles.Dy_ps)
            PS(handles.Dy_ps(i)).SetPt=PS(handles.Dy_ps(i)).Ampl-handles.dPS_Dy(i);
        end
        s=PSTrim(handles.Dy_ps,1);
        if(~s{1})
            handles=display_output(handles,['error in PSTrim :' s{2}]);
            return;
        end
        handles=display_output(handles,'done');
end
guidata(hObject, handles);


% --- Executes on button press in wait_chk.
function wait_chk_Callback(hObject, eventdata, handles)
% hObject    handle to wait_chk (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of wait_chk
handles.wait=get(hObject,'Value');
guidata(hObject, handles);


% --- Executes on button press in scale_factor_btn.
function handles=scale_factor_btn_Callback(hObject, eventdata, handles)
% hObject    handle to scale_factor_btn (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
for i=1:handles.loaded.handles.nbpm
    [handles.xscale(i),handles.xscale_err(i)]=noplot_polyfit(handles.dx_read(:,i)-handles.noise(handles.noise_range,i),handles.dx_read_rec(:,i),handles.dx_read_rec_err(:,i),[0 1]);
    [handles.yscale(i),handles.yscale_err(i)]=noplot_polyfit(handles.dy_read(:,i)-handles.noise(handles.noise_range,handles.nbpm+i),handles.dy_read_rec(:,i),handles.dx_read_rec_err(:,i),[0 1]);
end
axes(handles.plot)
errorbar(handles.loaded.handles.bpm_s,handles.xscale,handles.xscale_err,'b')
hold on
errorbar(handles.loaded.handles.bpm_s,handles.yscale,handles.yscale_err,'r')
hold off

yautoscale=[.5 1.5];
set(handles.ylim_txt,'String',sprintf('[%.3g %.3g]',yautoscale));
handles.ylim=yautoscale;
ylim(handles.ylim);
xlim(handles.xlim+handles.IEX_s);
legend('xscale','yscale');
set(gca,'XTick',handles.bpm_s);
set(gca,'XTickLabel',handles.bpm_name);
set(gca,'YTick',yautoscale(1):.1:yautoscale(2));
hor_line(1,'k-');
handles.hText=xticklabel_rotate90();
handles=display_output(handles,'done');
grid on

x_nplot=round(sqrt(4*(handles.loaded.handles.nbpm/3)));
y_nplot=ceil(handles.loaded.handles.nbpm/x_nplot);
% for i=1:handles.loaded.handles.nbpm
%     figure(4)
%     subplot(x_nplot,y_nplot,i)
%     cla
%     rangex=[min(handles.dx_read(:,i)*1e3) max(handles.dx_read(:,i)*1e3)];
%     rangex=[-1 1];
%     plot(handles.dx_read(:,i)*1e3,handles.dx_read_rec(:,i)*1e3,'b.','MarkerSize',1)
%     hold on
%     plot(rangex,rangex*handles.xscale(i),'b-','LineWidth',1)
%     hold off
%     xlim([-1 1])
%     ylim([-1 1])
% end
% for i=1:handles.loaded.handles.nbpm
%     figure(5)
%     subplot(x_nplot,y_nplot,i)
%     cla
%     rangey=[min(handles.dy_read(:,i)*1e3) max(handles.dy_read(:,i)*1e3)];
%     rangey=[-.1 .1];
%     plot(handles.dy_read(:,i)*1e3,handles.dy_read_rec(:,i)*1e3,'r.','MarkerSize',1)
%     hold on
%     plot(rangey,rangey*handles.yscale(i),'r-','LineWidth',.3)
%     hold off
%     xlim([-.1 .1])
%     ylim([-.1 .1])
% end
guidata(hObject, handles);


% --- Executes on button press in fluctuation_size.
function fluctuation_size_Callback(hObject, eventdata, handles)
% hObject    handle to fluctuation_size (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
handles=display_output(handles,'plotting...');
axes(handles.plot)
plot(handles.loaded.handles.bpm_s,std(handles.dx_read_rec)./sqrt(handles.loaded.FL.SimModel.Twiss.betax(handles.loaded.handles.bpm)*2e-9),'b-')
hold on
plot(handles.loaded.handles.bpm_s,std(handles.dxp_read_rec)./sqrt((1+handles.loaded.FL.SimModel.Twiss.alphax(handles.loaded.handles.bpm).^2)./handles.loaded.FL.SimModel.Twiss.betax(handles.loaded.handles.bpm)*2e-9),'g-')
plot(handles.loaded.handles.bpm_s,std(handles.dy_read_rec)./sqrt(handles.loaded.FL.SimModel.Twiss.betay(handles.loaded.handles.bpm)*30e-12),'r-')
plot(handles.loaded.handles.bpm_s,std(handles.dyp_read_rec)./sqrt((1+handles.loaded.FL.SimModel.Twiss.alphay(handles.loaded.handles.bpm).^2)./handles.loaded.FL.SimModel.Twiss.betay(handles.loaded.handles.bpm)*30e-12),'k-')
hold off
yautoscale=get(gca,'Ylim');
set(handles.ylim_txt,'String',sprintf('[%.3g %.3g]',yautoscale));
handles.ylim=yautoscale;
ylim(handles.ylim);
xlim(handles.xlim+handles.IEX_s);
legend('rms(x)/sigma x','rms(xp)/sigma xp','rms(y)/sigma y','rms(yp)/sigma yp');
set(gca,'XTick',handles.bpm_s);
set(gca,'XTickLabel',handles.bpm_name);
handles.hText=xticklabel_rotate90();
handles=display_output(handles,'done');

guidata(hObject, handles);


% --- Executes on button press in timestamp_check_chk.
function timestamp_check_chk_Callback(hObject, eventdata, handles)
% hObject    handle to timestamp_check_chk (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of timestamp_check_chk
handles.timestamp_check=get(hObject,'Value');
guidata(hObject, handles);


% --- Executes on button press in jitter_check_chk.
function jitter_check_chk_Callback(hObject, eventdata, handles)
% hObject    handle to jitter_check_chk (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of jitter_check_chk
handles.jitter_check=get(hObject,'Value');
guidata(hObject, handles);


% --- Executes on button press in weigth_btn.
function weigth_btn_Callback(hObject, eventdata, handles)
% hObject    handle to weigth_btn (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
handles=display_output(handles,'plot weigths used in the parameter fit');

axes(handles.plot)
plot(handles.loaded.handles.bpm_s,handles.weigth(1:handles.nbpm),'b-+')
hold on
plot(handles.loaded.handles.bpm_s,handles.weigth(handles.nbpm+1:end),'r-+')
hold off

xlim(handles.xlim+handles.IEX_s);
legend('xscale','yscale');
set(gca,'XTick',handles.bpm_s);
set(gca,'XTickLabel',handles.bpm_name);
handles.hText=xticklabel_rotate90();
handles=display_output(handles,'done');
guidata(hObject, handles);


% --- Executes on button press in E_by_EXT_chk.
function E_by_EXT_chk_Callback(hObject, eventdata, handles)
% hObject    handle to E_by_EXT_chk (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of E_by_EXT_chk
handles.E_by_EXT=get(hObject,'Value');
guidata(hObject, handles);


% --- Executes on button press in use_sext_chk.
function use_sext_chk_Callback(hObject, eventdata, handles)
% hObject    handle to use_sext_chk (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of use_sext_chk
handles.use_sext=get(hObject,'Value');
guidata(hObject, handles);

function dispMeasUpdate(~, ~, varargin)
% timer function for online dispersion measurement tool
global FL

switch varargin{1}
  case 'start'
    useApp('dispersion_measurement');
    clear online_dispersion_measurement
    set(FL.Gui.main.pushbutton8,'BackgroundColor','green')
  case 'stop'
    set(FL.Gui.main.pushbutton8,'BackgroundColor','red')
  case 'run'
    online_dispersion_measurement(1);
end
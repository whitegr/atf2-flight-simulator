function R=get_R_sext(L,m,xmean,ymean,Tilt,E)
    GEV2TM=3.335640952;
    if (m==0)
        R=eye(6,6);
        R(1,2)=L;
        R(3,4)=L;
        return
    end
    B=m*sqrt(xmean^2+ymean^2);
%     if(xmean~=0)
%         Tilt=Tilt-atan(ymean/xmean)/2+pi/2;
%     else
%         Tilt=Tilt-sign(ymean)*pi/4+pi/2;
%     end
    Tilt=Tilt-atan(ymean/xmean)/2;
    if(B>0 && xmean*B<0)
        Tilt=Tilt+pi/2;
    elseif (B<0 && xmean*B<=0 )
        Tilt=Tilt+pi/2;
    end
    Kmod = abs(B)/L/E/GEV2TM ;
    rootK = sqrt(Kmod) ;
	LrK = L * rootK ;
	xcos = cos(LrK) ;
	xsin = sin(LrK)/rootK ;
	ycos = cosh(LrK) ;
	ysin = sinh(LrK)/rootK ;
    if (abs(Tilt) == pi/2)
		costilt = 0 ;
		sintilt = 1 ;
	else
		costilt = cos(Tilt) ;
		sintilt = sin(Tilt) ;
    end
	cstilt = costilt*sintilt ;
	c2tilt = costilt*costilt ;
	s2tilt = sintilt*sintilt ;
    
	R(1,1) = xcos * c2tilt + ycos*s2tilt ;
	R(1,2) = xsin * c2tilt + ysin*s2tilt ; 
	R(1,3) = (xcos-ycos) * cstilt ;
	R(1,4) = (xsin-ysin) * cstilt ;

	R(2,1) = Kmod * (ysin*s2tilt - xsin*c2tilt) ;
	R(2,2) = R(1,1) ;
	R(2,3) = -Kmod * cstilt * ( xsin + ysin ) ;
	R(2,4) = R(1,3) ;


	R(3,1) = R(1,3) ;
	R(3,2) = R(1,4) ;
	R(3,3) = ycos * c2tilt + xcos * s2tilt ;
	R(3,4) = ysin * c2tilt + xsin * s2tilt ;

	R(4,1) = R(2,3) ;
	R(4,2) = R(1,3) ;
	R(4,3) = Kmod * (ysin * c2tilt - xsin * s2tilt) ;
	R(4,4) = ycos * c2tilt + xcos * s2tilt ;

    R(1:4,5:6) = zeros(4,2) ;
    R(5:6,:)=zeros(2,6);
	R(5,5) = 1.;
	R(6,6) = 1.;

end
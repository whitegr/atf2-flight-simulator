function varargout = bpmgui(varargin)
% BPMGUI M-file for bpmgui.fig
%      BPMGUI, by itself, creates a new BPMGUI or raises the existing
%      singleton*.
%
%      H = BPMGUI returns the handle to a new BPMGUI or the handle to
%      the existing singleton*.
%
%      BPMGUI('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in BPMGUI.M with the given input arguments.
%
%      BPMGUI('Property','Value',...) creates a new BPMGUI or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before bpmgui_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to bpmgui_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help bpmgui

% Last Modified by GUIDE v2.5 07-Apr-2009 16:39:37

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
    'gui_Singleton',  gui_Singleton, ...
    'gui_OpeningFcn', @bpmgui_OpeningFcn, ...
    'gui_OutputFcn',  @bpmgui_OutputFcn, ...
    'gui_LayoutFcn',  [] , ...
    'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before bpmgui is made visible.
function bpmgui_OpeningFcn(hObject, eventdata, handles, varargin) %#ok<*INUSL>
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to bpmgui (see VARARGIN)

% Choose default command line output for bpmgui
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes bpmgui wait for user response (see UIRESUME)
% uiwait(handles.figure1);
set(handles.uipanel1,'Title','Paused')
set(handles.uipanel1,'ForegroundColor',[1 0 0])

global GUI_DATA %grabdata_t

% grabdata_t = timer('TimerFcn',{@grabdata,handles},'Period',0.64,...
%   'ExecutionMode','fixedSpacing');
% start(grabdata_t);
% drawnow

GUI_DATA.ave = 0;
GUI_DATA.avval = 10;
GUI_DATA.plotrms = 0;
GUI_DATA.type = 'abs';
GUI_DATA.counter = 0;
GUI_DATA.fsimmode = true;



% --- Outputs from this function are returned to the command line.
function varargout = bpmgui_OutputFcn(hObject, eventdata, handles)
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on selection change in popupmenu1.
function popupmenu1_Callback(hObject, eventdata, handles)
% hObject    handle to popupmenu1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
%
% Hints: contents = get(hObject,'String') returns popupmenu1 contents as cell array
%        contents{get(hObject,'Value')} returns selected item from popupmenu1


% --- Executes during object creation, after setting all properties.
function popupmenu1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to popupmenu1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), ...
        get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in togglebutton1.
function togglebutton1_Callback(hObject, eventdata, handles)
% hObject    handle to togglebutton1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
%
% Hint: get(hObject,'Value') returns toggle state of togglebutton1

global BEAM_DATA GUI_DATA BEAMLINE INSTR %grabdata_t

if isempty(BEAM_DATA)
    BEAM_DATA.x = nan;
    BEAM_DATA.y = nan;
end

buttonval = get(hObject,'Value');

if buttonval
    set(hObject,'String','CLICK TO PAUSE')
    set(handles.uipanel1,'Title','Running')
    set(handles.uipanel1,'ForegroundColor',[0 1 0])
    set(hObject,'BackgroundColor',[1 0 0])
else
    set(hObject,'String','CLICK TO RUN')
    set(handles.uipanel1,'Title','Paused')
    set(handles.uipanel1,'ForegroundColor',[1 0 0])
    set(hObject,'BackgroundColor',[0 1 0])
end

[stat GUI_DATA.counter] = FlHwUpdate('getpulsenum');
while buttonval
    [stat newnum] = FlHwUpdate('getpulsenum');
    while GUI_DATA.counter == newnum
        pause(0.2)
        [stat newnum] = FlHwUpdate('getpulsenum');
    end
    GUI_DATA.counter = newnum;
    
    try
        axes1as = get(handles.radiobutton3,'Value');
        axes1man = get(handles.radiobutton4,'Value');
        axes1minval = str2double(get(handles.edit1,'String'));
        axes1maxval = str2double(get(handles.edit2,'String'));
        axes1currval = ylim(handles.axes1);
        
        axes2as = get(handles.radiobutton7,'Value');
        axes2man = get(handles.radiobutton8,'Value');
        axes2minval = str2double(get(handles.edit5,'String'));
        axes2maxval = str2double(get(handles.edit6,'String'));
        axes2currval = ylim(handles.axes2);
        
        axes3as = get(handles.radiobutton9,'Value');
        axes3man = get(handles.radiobutton10,'Value');
        axes3minval = str2double(get(handles.edit7,'String'));
        axes3maxval = str2double(get(handles.edit8,'String'));
        axes3currval = ylim(handles.axes3);
    catch %#ok<*CTCH>
        close all
        buttonval = 0; %#ok<*NASGU>
        continue
    end
    
    if axes1minval >= axes1maxval
        axes1minval = axes1currval(1);
        axes1maxval = axes1currval(2);
    end
    if axes2minval >= axes2maxval
        axes2minval = axes2currval(1);
        axes2maxval = axes2currval(2);
    end
    if axes3minval >= axes3maxval
        axes3minval = axes3currval(1);
        axes3maxval = axes3currval(2);
    end
    
    if buttonval == 0; break; end
    
    if GUI_DATA.ave
        [stat output] = FlHwUpdate('bpmave',GUI_DATA.avval);
        BEAM_DATA.x    = output{1}(1,:);
        BEAM_DATA.y    = output{1}(2,:);
        BEAM_DATA.rmsx = output{1}(4,:);
        BEAM_DATA.rmsy = output{1}(5,:);
        BEAM_DATA.q    = cellfun(@(x) x.Data(3),INSTR);
        BEAM_DATA.s    = zeros(1,length(INSTR));
        for bpmnum=1:length(INSTR)
            BEAM_DATA.s(bpmnum) = BEAMLINE{INSTR{bpmnum}.Index}.S;
        end
    else
        FlHwUpdate;
        BEAM_DATA.x    = cellfun(@(x) x.Data(1),INSTR);
        BEAM_DATA.rmsx = zeros(size(BEAM_DATA.x));
        BEAM_DATA.y    = cellfun(@(x) x.Data(2),INSTR);
        BEAM_DATA.rmsy = zeros(size(BEAM_DATA.y));
        BEAM_DATA.q    = cellfun(@(x) x.Data(3),INSTR);
        BEAM_DATA.s    = zeros(1,length(INSTR));
        for bpmnum=1:length(INSTR)
            BEAM_DATA.s(bpmnum) = BEAMLINE{INSTR{bpmnum}.Index}.S;
        end
    end
    
    set(handles.pushbutton1,'Enable','on');
    
    if strcmp(GUI_DATA.type,'abs')
        x = BEAM_DATA.x;
        y = BEAM_DATA.y;
        s = BEAM_DATA.s;
        q = BEAM_DATA.q;
    elseif strcmp(GUI_DATA.type,'dif')
        x = BEAM_DATA.x - GUI_DATA.reference(1,:);
        y = BEAM_DATA.y - GUI_DATA.reference(2,:);
        s = BEAM_DATA.s;
        q = BEAM_DATA.q;
    end
    % order with s
    [s,sortI]=sort(s);
    x=x(sortI); y=y(sortI); q=q(sortI);
    
    if GUI_DATA.ave && GUI_DATA.plotrms
        hold(handles.axes1,'off')
        plot(handles.axes1,s,x*1e3,'bx')
        xlim(handles.axes1,[0 s(end)])
        hold(handles.axes1,'on')
        plot(handles.axes1,s,x*1e3+BEAM_DATA.rmsx*1e3,'xr')
        plot(handles.axes1,s,x*1e3-BEAM_DATA.rmsx*1e3,'xr')
    else
        hold(handles.axes1,'off')
        plot(handles.axes1,s,x*1e3,'bx')
        xlim(handles.axes1,[0 s(end)])
    end
    ylabel(handles.axes1,'x / mm')
    if axes1as
        ylim(handles.axes1,'auto')
    elseif axes1man
        %     ylim('manual')
        minval = axes1minval;
        maxval = axes1maxval;
        if isnan(minval)
            temp = ylim;
            minval = temp(1);
        end
        if isnan(maxval)
            temp = ylim;
            maxval = temp(2);
        end
        ylim(handles.axes1,[minval maxval])
    end
    
    if GUI_DATA.ave && GUI_DATA.plotrms
        hold(handles.axes2,'off')
        plot(handles.axes2,s,y*1e3,'bx')
        xlim(handles.axes2,[0 BEAM_DATA.s(end)])
        hold(handles.axes2,'on')
        plot(handles.axes2,s,y*1e3+BEAM_DATA.rmsy*1e3,'xr')
        plot(handles.axes2,s,y*1e3-BEAM_DATA.rmsy*1e3,'xr')
    else
        hold(handles.axes2,'off')
        plot(handles.axes2,s,y*1e3,'bx')
        xlim(handles.axes2,[0 BEAM_DATA.s(end)])
    end
    ylabel(handles.axes2,'y / mm')
    if axes2as
        ylim(handles.axes2,'auto')
    elseif axes2man
        %     ylim('manual')
        minval = axes2minval;
        maxval = axes2maxval;
        if isnan(minval)
            temp = ylim;
            minval = temp(1);
        end
        if isnan(maxval)
            temp = ylim;
            maxval = temp(2);
        end
        ylim(handles.axes2,[minval maxval])
    end
    
    [s_plot i_plot]=unique(s);
    bar(handles.axes3,s_plot,q(i_plot))
    xlim(handles.axes3,[0 BEAM_DATA.s(end)])
    ylabel(handles.axes3,'# of e-')
    if axes3as
        ylim(handles.axes3,'auto')
    elseif axes3man
        %     ylim('manual')
        minval = axes3minval;
        maxval = axes3maxval;
        if isnan(minval)
            temp = ylim;
            minval = temp(1);
        end
        if isnan(maxval)
            temp = ylim;
            maxval = temp(2);
        end
        ylim(handles.axes3,[minval maxval])
    end
    
    try
        buttonval = get(hObject,'Value');
    catch
        close all
        buttonval=0;
        continue
    end
end



% --- Executes on button press in pushbutton1.
function pushbutton1_Callback(hObject, eventdata, handles) %#ok<*DEFNU,*INUSD>
% hObject    handle to pushbutton1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% This function switches off the timer (if it's on), calculates a reference
% waveform, and switches it back on (if it was on originally).
%
% Need to acquire a reference.  Stop the GUI updating (if necessary), then
% grab the ref from FlHwUpdate('bpmave'), and restart (if it was running
% originally).

% global GUI_DATA INSTR %grabdata_t
%
% tdata = get(grabdata_t);
%
% if strcmp(tdata.Running,'off')
%   if GUI_DATA.ave
%     [stat output] = FlHwUpdate('bpmave',GUI_DATA.avval);
%     GUI_DATA.reference(1,:) = output{1}(1,:);
%     GUI_DATA.reference(2,:) = output{1}(2,:);
%   else
%     FlHwUpdate;
%     GUI_DATA.reference(1,:) = cellfun(@(x) x.Data(1),INSTR);
%     GUI_DATA.reference(2,:) = cellfun(@(x) x.Data(2),INSTR);
%   end
%
% else
%   if GUI_DATA.ave
%     stop(grabdata_t)
%     while strcmp(tdata.Running,'on')
%       pause(0.1)
%       tdata = get(grabdata_t);
%     end
%     [stat output] = FlHwUpdate('bpmave',GUI_DATA.avval);
%     GUI_DATA.reference(1,:) = output{1}(1,:);
%     GUI_DATA.reference(2,:) = output{1}(2,:);
%     start(grabdata_t)
%   else
%     stop(grabdata_t)
%     while strcmp(tdata.Running,'on')
%       pause(0.1)
%       tdata = get(grabdata_t);
%     end
%     FlHwUpdate;
%     GUI_DATA.reference(1,:) = cellfun(@(x) x.Data(1),INSTR);
%     GUI_DATA.reference(2,:) = cellfun(@(x) x.Data(2),INSTR);
%     start(grabdata_t)
%   end
% end

global GUI_DATA BEAM_DATA

GUI_DATA.reference(1,:) = BEAM_DATA.x;
GUI_DATA.reference(2,:) = BEAM_DATA.y;


% --- Executes on button press in checkbox1.
function checkbox1_Callback(hObject, eventdata, handles)
% hObject    handle to checkbox1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
%
% Hint: get(hObject,'Value') returns toggle state of checkbox1

global GUI_DATA

GUI_DATA.plotrms = get(hObject,'Value');


% --- Executes during object deletion, before destroying properties.
function figure1_DeleteFcn(hObject, eventdata, handles)
% hObject    handle to figure1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
%
% global grabdata_t
%
% state = get(handles.togglebutton1,'Value');
% if state
%   togglebutton1_Callback(hObject, eventdata, handles)
%   pause(0.5)
% end

clear global GUI_DATA BEAM_DATA


% --- Executes on button press in radiobutton12.
function radiobutton12_Callback(hObject, eventdata, handles)
% hObject    handle to radiobutton12 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of radiobutton12

global GUI_DATA

GUI_DATA.ave = get(hObject,'Value');


% --- Executes on button press in radiobutton11.
function radiobutton11_Callback(hObject, eventdata, handles)
% hObject    handle to radiobutton11 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of radiobutton11

global GUI_DATA

GUI_DATA.ave = ~get(hObject,'Value');


% --- Executes on button press in radiobutton1.
function radiobutton1_Callback(hObject, eventdata, handles)
% hObject    handle to radiobutton1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of radiobutton1

global GUI_DATA

GUI_DATA.type = 'abs';


% --- Executes on button press in radiobutton2.
function radiobutton2_Callback(hObject, eventdata, handles)
% hObject    handle to radiobutton2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of radiobutton2

global GUI_DATA

GUI_DATA.type = 'dif';
if ~isfield(GUI_DATA,'reference')
    GUI_DATA.reference = zeros(2,numbpms);
end
if ~isnumeric(GUI_DATA.reference) && ~ndims(GUI_DATA.reference)==2
    GUI_DATA.reference = zeros(2,numbpms);
end

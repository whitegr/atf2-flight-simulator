function varargout = manualEXT(varargin)
% MANUALEXT M-file for manualEXT.fig
%      MANUALEXT, by itself, creates a new MANUALEXT or raises the existing
%      singleton*.
%
%      H = MANUALEXT returns the handle to a new MANUALEXT or the handle to
%      the existing singleton*.
%
%      MANUALEXT('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in MANUALEXT.M with the given input arguments.
%
%      MANUALEXT('Property','Value',...) creates a new MANUALEXT or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before manualEXT_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to manualEXT_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help manualEXT

% Last Modified by GUIDE v2.5 15-Apr-2009 11:38:31

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @manualEXT_OpeningFcn, ...
                   'gui_OutputFcn',  @manualEXT_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before manualEXT is made visible.
function manualEXT_OpeningFcn(hObject, eventdata, handles, varargin)
global FL BEAMLINE PS
if FL.OCEXT.overwrite==1
    delete(FL.OCEXT.savefilename)
end
FlHwUpdate();
openfile=open('thermatsEXT.mat');
FL.OCEXT.xmat=openfile.xmat;
FL.OCEXT.ymat=openfile.ymat;
for R2=1:length(FL.OCEXT.bpmweights(:,1))
FL.OCEXT.xmat(:,R2)=FL.OCEXT.xmat(:,R2)*FL.OCEXT.bpmweights(R2,1);
end
for R2=1:length(FL.OCEXT.hcorrweights(:,1))
FL.OCEXT.xmat(R2,:)=FL.OCEXT.xmat(R2,:)*FL.OCEXT.hcorrweights(R2,1);
end
for R2=1:length(FL.OCEXT.bpmweights(:,2))
FL.OCEXT.ymat(:,R2)=FL.OCEXT.ymat(:,R2)*FL.OCEXT.bpmweights(R2,2);
end
for R2=1:length(FL.OCEXT.vcorrweights(:,1))
FL.OCEXT.ymat(R2,:)=FL.OCEXT.ymat(R2,:)*FL.OCEXT.vcorrweights(R2,1);
end
FL.OCEXT.iexlist=findcells(BEAMLINE,'Name','IEX');
FL.OCEXT.extstart=findcells(BEAMLINE,'Name','QF1X');
FL.OCEXT.fflist=findcells(BEAMLINE,'Name','QM16FF');
zv1xpos=findcells(BEAMLINE,'Name','ZV1X')-1;
FL.OCEXT.bpmno=length(findcells({BEAMLINE{zv1xpos(end):FL.OCEXT.fflist(1)}},'Class','MONI'));
list = findcells(BEAMLINE,'Class','XCOR');
FL.OCEXT.xcorpos=list(find(list>zv1xpos(end),1):length(list)-1);
FL.OCEXT.xcorlist=zeros(1, length(FL.OCEXT.xcorpos));
for R=1:length(FL.OCEXT.xcorpos)
FL.OCEXT.xcorlist(R)=BEAMLINE{FL.OCEXT.xcorpos(R)}.PS;
end
list = findcells(BEAMLINE,'Class','YCOR');
FL.OCEXT.ycorpos=list(find(list>zv1xpos(end),1):length(list)-1);
FL.OCEXT.ycorlist=zeros(1, length(FL.OCEXT.ycorpos));
for R=1:length(FL.OCEXT.ycorpos)
FL.OCEXT.ycorlist(R)=BEAMLINE{FL.OCEXT.ycorpos(R)}.PS;
end
FL.OCEXT.fullcorlist=[FL.OCEXT.xcorlist,FL.OCEXT.ycorlist];
FL.OCEXT.fullcorpos=[FL.OCEXT.xcorpos,FL.OCEXT.ycorpos];
FL.OCEXT.iternum=0;
request{1}=[];
request{2}=FL.OCEXT.fullcorlist;
request{3}=[];
[stat FL.OCEXT.resp] = AccessRequest(request);
disp('access request')
disp(stat)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to manualEXT (see VARARGIN)

% Choose default command line output for manualEXT
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes manualEXT wait for user response (see UIRESUME)
 %uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = manualEXT_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on button press in pushbutton1.
function pushbutton1_Callback(hObject, eventdata, handles)
global PS FL BEAMLINE
set(handles.text3, 'String', 'Busy')
pause on
pause(1);
pause off
disp('update')
stat=FlHwUpdate();
disp(stat)
disp('get orbit')
stat=FlHwUpdate('wait',10);
[stat,output]=FlHwUpdate('bpmave',10);
disp(stat)
[stat instdata] = FlTrackThru(FL.OCEXT.iexlist(end),FL.OCEXT.fflist(end),output);
FL.OCEXT.bpmxavg=zeros(FL.OCEXT.bpmno,1);
FL.OCEXT.bpmyavg=zeros(FL.OCEXT.bpmno,1);
for R2=1:FL.OCEXT.bpmno
if isnan(instdata{1}(R2).x) == 1
FL.OCEXT.bpmxavg(R2,1)=FL.OCEXT.bpmtarget(R2,1);
else
FL.OCEXT.bpmxavg(R2,1)=instdata{1}(R2).x;
end
if isnan(instdata{1}(R2).y) == 1
FL.OCEXT.bpmyavg(R2,1)=FL.OCEXT.bpmtarget(R2,2);
else
FL.OCEXT.bpmyavg(R2,1)=instdata{1}(R2).y;
end
end
disp('set undo point to current FS Ampl')
FL.OCEXT.corrundo=[PS(FL.OCEXT.fullcorlist).Ampl];
FL.OCEXT.iternum=FL.OCEXT.iternum+1;
dlmwrite(FL.OCEXT.savefilename,'====================== new track ==================','delimiter', ' ','roffset', 2,'-append');
dlmwrite(FL.OCEXT.savefilename,'iternum','delimiter', ' ','roffset', 2,'-append');
dlmwrite(FL.OCEXT.savefilename,FL.OCEXT.iternum,'delimiter', ' ','roffset', 2,'-append');
dlmwrite(FL.OCEXT.savefilename,'xrms','delimiter', ' ','roffset', 2,'-append');
dlmwrite(FL.OCEXT.savefilename,sqrt(mean(FL.OCEXT.bpmxavg.^2)),'delimiter', ' ','roffset', 2,'-append');
dlmwrite(FL.OCEXT.savefilename,'yrms','delimiter', ' ','roffset', 2,'-append');
dlmwrite(FL.OCEXT.savefilename,sqrt(mean(FL.OCEXT.bpmyavg.^2)),'delimiter', ' ','roffset', 2,'-append')
dlmwrite(FL.OCEXT.savefilename,'hor','delimiter', ' ','roffset', 2,'-append');
dlmwrite(FL.OCEXT.savefilename,FL.OCEXT.bpmxavg,'delimiter', ' ','roffset', 2,'-append');
dlmwrite(FL.OCEXT.savefilename,'vert','delimiter', ' ','roffset', 2,'-append');
dlmwrite(FL.OCEXT.savefilename,FL.OCEXT.bpmyavg,'delimiter', ' ','roffset', 2,'-append');
dlmwrite(FL.OCEXT.savefilename,'---------------------------','delimiter', ' ','roffset', 2,'-append')
disp('Calc correction')
FL.OCEXT.horcorr=transpose(pinv(FL.OCEXT.xmat))*(FL.OCEXT.bpmxavg-FL.OCEXT.bpmtarget(:,1))*10^-5;
FL.OCEXT.vertcorr=transpose(pinv(FL.OCEXT.ymat))*(FL.OCEXT.bpmyavg-FL.OCEXT.bpmtarget(:,2))*10^-5;
FL.OCEXT.midhorcorr=transpose(pinv(FL.OCEXT.xmat))*((FL.OCEXT.bpmxavg-FL.OCEXT.bpmtarget(:,1))/2)*10^-5;
FL.OCEXT.midvertcorr=transpose(pinv(FL.OCEXT.ymat))*((FL.OCEXT.bpmyavg-FL.OCEXT.bpmtarget(:,2))/2)*10^-5;
disp('horizontal')
disp(FL.OCEXT.horcorr)
disp('vertical')
disp(FL.OCEXT.vertcorr)
dlmwrite(FL.OCEXT.savefilename,'horcorr','delimiter', ' ','roffset', 2,'-append');
dlmwrite(FL.OCEXT.savefilename,FL.OCEXT.horcorr,'delimiter', ' ','roffset', 2,'-append');
dlmwrite(FL.OCEXT.savefilename,'vertcorr','delimiter', ' ','roffset', 2,'-append');
dlmwrite(FL.OCEXT.savefilename,FL.OCEXT.vertcorr,'delimiter', ' ','roffset', 2,'-append');
dlmwrite(FL.OCEXT.savefilename,'mid-point horcorr','delimiter', ' ','roffset', 2,'-append');
dlmwrite(FL.OCEXT.savefilename,FL.OCEXT.midhorcorr,'delimiter', ' ','roffset', 2,'-append');
dlmwrite(FL.OCEXT.savefilename,'mid-point vertcorr','delimiter', ' ','roffset', 2,'-append');
dlmwrite(FL.OCEXT.savefilename,FL.OCEXT.midvertcorr,'delimiter', ' ','roffset', 2,'-append');
FL.OCEXT.fullsetcorr=[transpose(FL.OCEXT.horcorr),transpose(FL.OCEXT.vertcorr)];
FL.OCEXT.midsetcorr=[transpose(FL.OCEXT.midhorcorr),transpose(FL.OCEXT.midvertcorr)];
disp('fill in table')
for r=1:length(FL.OCEXT.fullcorpos)
if length(FL.HwInfo.PS(FL.OCEXT.fullcorlist(r)).conv)==1
tabledata(1,r)=PS(FL.OCEXT.fullcorlist(r)).Ampl*BEAMLINE{FL.OCEXT.fullcorpos(r)}.B/FL.HwInfo.PS(FL.OCEXT.fullcorlist(r)).conv;
tabledata(2,r)=FL.OCEXT.fullsetcorr(r)*BEAMLINE{FL.OCEXT.fullcorpos(r)}.B/FL.HwInfo.PS(FL.OCEXT.fullcorlist(r)).conv;
tabledata(3,r)=FL.OCEXT.midsetcorr(r)*BEAMLINE{FL.OCEXT.fullcorpos(r)}.B/FL.HwInfo.PS(FL.OCEXT.fullcorlist(r)).conv;
else
tabledata(1,r)=interp1(FL.HwInfo.PS(FL.OCEXT.fullcorlist(r)).conv(1,:),FL.HwInfo.PS(FL.OCEXT.fullcorlist(r)).conv(2,:),BEAMLINE{FL.OCEXT.fullcorpos(r)}.B)*PS(FL.OCEXT.fullcorlist(r)).Ampl;
tabledata(2,r)=interp1(FL.HwInfo.PS(FL.OCEXT.fullcorlist(r)).conv(1,:),FL.HwInfo.PS(FL.OCEXT.fullcorlist(r)).conv(2,:),BEAMLINE{FL.OCEXT.fullcorpos(r)}.B)*FL.OCEXT.fullsetcorr(r);
tabledata(3,r)=interp1(FL.HwInfo.PS(FL.OCEXT.fullcorlist(r)).conv(1,:),FL.HwInfo.PS(FL.OCEXT.fullcorlist(r)).conv(2,:),BEAMLINE{FL.OCEXT.fullcorpos(r)}.B)*FL.OCEXT.midsetcorr(r);
end
end
set(handles.uitable2,'Data',tabledata)
zv1xpos=findcells(BEAMLINE,'Name','ZV1X')-1;
list=findcells({BEAMLINE{zv1xpos(end):FL.OCEXT.fflist(1)}},'Class','MONI')+FL.OCEXT.iexlist(end)-1;
bpmslist=[];
for r=1:length(list)
bpmslist=[bpmslist,BEAMLINE{list(r)}.S];
end
disp('plot graph')
plot(bpmslist,FL.OCEXT.bpmxavg*10^6,'Color','blue')
hold on
plot(bpmslist,FL.OCEXT.bpmyavg*10^6,'Color','red')
hold off
xlabel('S (m)')
ylabel('BPM Reading (microns)')
title('Current Extraction Line Orbit')
h = legend('Horizontal','Vertical',1);
set(h,'Interpreter','none')
set(handles.text3, 'String', 'Ready')
% hObject    handle to pushbutton1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on button press in pushbutton2.
function pushbutton2_Callback(hObject, eventdata, handles)
global FL PS BEAMLINE
        pause on
        pause(1);
        pause off
        disp('update')
        stat=FlHwUpdate();
        disp(stat)
set(handles.text3, 'String', 'Busy')
disp('old ampl')
disp('horizontal')
disp([PS(FL.OCEXT.xcorlist).Ampl])
disp('vertical')
disp([PS(FL.OCEXT.ycorlist).Ampl])
disp('planned correction')
disp('horizontal')
disp(transpose(FL.OCEXT.horcorr))
disp('vertical')
disp(transpose(FL.OCEXT.vertcorr))
disp('set SetPts')
for R1=1:length(FL.OCEXT.xcorlist)
PS(FL.OCEXT.xcorlist(R1)).SetPt=PS(FL.OCEXT.xcorlist(R1)).Ampl+FL.OCEXT.horcorr(R1);
end
        for R1=1:length(FL.OCEXT.ycorlist)
PS(FL.OCEXT.ycorlist(R1)).SetPt=PS(FL.OCEXT.ycorlist(R1)).Ampl+FL.OCEXT.vertcorr(R1);
        end
        disp('horizontal')
disp([PS(FL.OCEXT.xcorlist).SetPt])
disp('vertical')
disp([PS(FL.OCEXT.ycorlist).SetPt])
        dlmwrite(FL.OCEXT.savefilename,'full correction','delimiter', ' ','roffset', 2,'-append');
dlmwrite(FL.OCEXT.savefilename,'horsetpt','delimiter', ' ','roffset', 2,'-append');
dlmwrite(FL.OCEXT.savefilename,[PS(FL.OCEXT.xcorlist).SetPt],'delimiter', ' ','roffset', 2,'-append');
dlmwrite(FL.OCEXT.savefilename,'vertsetpt','delimiter', ' ','roffset', 2,'-append');
dlmwrite(FL.OCEXT.savefilename,[PS(FL.OCEXT.ycorlist).SetPt],'delimiter', ' ','roffset', 2,'-append');
disp('PSTrim')        
stat=PSTrim(FL.OCEXT.fullcorlist, 1);
disp(stat)
        pause on
        pause(1);
        pause off
        disp('update')
        stat=FlHwUpdate();
        disp(stat)
        disp('new ampl')
disp('horizontal')
disp([PS(FL.OCEXT.xcorlist).Ampl])
disp('vertical')
disp([PS(FL.OCEXT.ycorlist).Ampl])
FL.OCEXT.bpmxavgprev=FL.OCEXT.bpmxavg;
FL.OCEXT.bpmyavgprev=FL.OCEXT.bpmyavg;
disp('Get BPM readings')
FlHwUpdate('wait',10);
[stat,output]=FlHwUpdate('bpmave',10);
disp(stat)
[stat instdata] = FlTrackThru(FL.OCEXT.iexlist(end),FL.OCEXT.fflist(end),output);
FL.OCEXT.bpmxavg=zeros(FL.OCEXT.bpmno,1);
FL.OCEXT.bpmyavg=zeros(FL.OCEXT.bpmno,1);
for R2=1:FL.OCEXT.bpmno
if isnan(instdata{1}(R2).x) == 1
FL.OCEXT.bpmxavg(R2,1)=FL.OCEXT.bpmtarget(R2,1);
else
FL.OCEXT.bpmxavg(R2,1)=instdata{1}(R2).x;
end
if isnan(instdata{1}(R2).y) == 1
FL.OCEXT.bpmyavg(R2,1)=FL.OCEXT.bpmtarget(R2,2);
else
FL.OCEXT.bpmyavg(R2,1)=instdata{1}(R2).y;
end
end
FL.OCEXT.iternum=FL.OCEXT.iternum+1;
zv1xpos=findcells(BEAMLINE,'Name','ZV1X')-1;
list=findcells({BEAMLINE{zv1xpos(end):FL.OCEXT.fflist(1)}},'Class','MONI')+FL.OCEXT.iexlist(end)-1;
bpmslist=[];
for r=1:length(list)
bpmslist=[bpmslist,BEAMLINE{list(r)}.S];
end
disp('Plot Graph')
plot(bpmslist,FL.OCEXT.bpmxavg*10^6,'Color','blue')
hold on
plot(bpmslist,FL.OCEXT.bpmyavg*10^6,'Color','red')
hold off
xlabel('S (m)')
ylabel('BPM Reading (microns)')
title('Current Extraction Line Orbit')
h = legend('Horizontal','Vertical',1);
set(h,'Interpreter','none')
set(handles.text3, 'String', 'Ready')
set(handles.text3, 'String', 'Ready')
% hObject    handle to pushbutton2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on button press in pushbutton3.
function pushbutton3_Callback(hObject, eventdata, handles)
global FL PS BEAMLINE
        pause on
        pause(1);
        pause off
        FlHwUpdate();
set(handles.text3, 'String', 'Busy')
for R1=1:length(FL.OCEXT.xcorlist)
PS(FL.OCEXT.xcorlist(R1)).SetPt=PS(FL.OCEXT.xcorlist(R1)).Ampl+FL.OCEXT.midhorcorr(R1);
end
        for R1=1:length(FL.OCEXT.ycorlist)
PS(FL.OCEXT.ycorlist(R1)).SetPt=PS(FL.OCEXT.ycorlist(R1)).Ampl+FL.OCEXT.midvertcorr(R1);
        end
        dlmwrite(FL.OCEXT.savefilename,'mid-point correction','delimiter', ' ','roffset', 2,'-append');
dlmwrite(FL.OCEXT.savefilename,'horsetpt','delimiter', ' ','roffset', 2,'-append');
dlmwrite(FL.OCEXT.savefilename,[PS(FL.OCEXT.xcorlist).SetPt],'delimiter', ' ','roffset', 2,'-append');
dlmwrite(FL.OCEXT.savefilename,'vertsetpt','delimiter', ' ','roffset', 2,'-append');
dlmwrite(FL.OCEXT.savefilename,[PS(FL.OCEXT.ycorlist).SetPt],'delimiter', ' ','roffset', 2,'-append');
        PSTrim(FL.OCEXT.fullcorlist, 1);
        pause on
        pause(1);
        pause off
        FlHwUpdate();
FL.OCEXT.bpmxavgprev=FL.OCEXT.bpmxavg;
FL.OCEXT.bpmyavgprev=FL.OCEXT.bpmyavg;
FlHwUpdate('wait',10);
[stat,output]=FlHwUpdate('bpmave',10);
[stat instdata] = FlTrackThru(FL.OCEXT.iexlist(end),FL.OCEXT.fflist(end),output);
FL.OCEXT.bpmxavg=zeros(FL.OCEXT.bpmno,1);
FL.OCEXT.bpmyavg=zeros(FL.OCEXT.bpmno,1);
for R2=1:FL.OCEXT.bpmno
if isnan(instdata{1}(R2).x) == 1
FL.OCEXT.bpmxavg(R2,1)=FL.OCEXT.bpmtarget(R2,1);
else
FL.OCEXT.bpmxavg(R2,1)=instdata{1}(R2).x;
end
if isnan(instdata{1}(R2).y) == 1
FL.OCEXT.bpmyavg(R2,1)=FL.OCEXT.bpmtarget(R2,2);
else
FL.OCEXT.bpmyavg(R2,1)=instdata{1}(R2).y;
end
end
FL.OCEXT.iternum=FL.OCEXT.iternum+1;
zv1xpos=findcells(BEAMLINE,'Name','ZV1X')-1;
list=findcells({BEAMLINE{zv1xpos(end):FL.OCEXT.fflist(1)}},'Class','MONI')+FL.OCEXT.iexlist(end)-1;
bpmslist=[];
for r=1:length(list)
bpmslist=[bpmslist,BEAMLINE{list(r)}.S];
end
plot(bpmslist,FL.OCEXT.bpmxavg*10^6,'Color','blue')
hold on
plot(bpmslist,FL.OCEXT.bpmyavg*10^6,'Color','red')
hold off
xlabel('S (m)')
ylabel('BPM Reading (microns)')
title('Current Extraction Line Orbit')
h = legend('Horizontal','Vertical',1);
set(h,'Interpreter','none')
set(handles.text3, 'String', 'Ready')
set(handles.text3, 'String', 'Ready')
% hObject    handle to pushbutton3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on button press in pushbutton4.
function pushbutton4_Callback(hObject, eventdata, handles)
global FL PS BEAMLINE
        pause on
        pause(1);
        pause off
        FlHwUpdate();
set(handles.text3, 'String', 'Busy')
for R1=1:length(FL.OCEXT.fullcorlist)
PS(FL.OCEXT.fullcorlist(R1)).SetPt=FL.OCEXT.corrundo(R1);
end
        dlmwrite(FL.OCEXT.savefilename,'undo correction','delimiter', ' ','roffset', 2,'-append');
dlmwrite(FL.OCEXT.savefilename,'horsetpt','delimiter', ' ','roffset', 2,'-append');
dlmwrite(FL.OCEXT.savefilename,[PS(FL.OCEXT.xcorlist).SetPt],'delimiter', ' ','roffset', 2,'-append');
dlmwrite(FL.OCEXT.savefilename,'vertsetpt','delimiter', ' ','roffset', 2,'-append');
dlmwrite(FL.OCEXT.savefilename,[PS(FL.OCEXT.ycorlist).SetPt],'delimiter', ' ','roffset', 2,'-append');
        PSTrim(FL.OCEXT.fullcorlist, 1);
        pause on
        pause(1);
        pause off
        FlHwUpdate();
FL.OCEXT.bpmxavgprev=FL.OCEXT.bpmxavg;
FL.OCEXT.bpmyavgprev=FL.OCEXT.bpmyavg;
FlHwUpdate('wait',10);
[stat,output]=FlHwUpdate('bpmave',10);
[stat instdata] = FlTrackThru(FL.OCEXT.iexlist(end),FL.OCEXT.fflist(end),output);
FL.OCEXT.bpmxavg=zeros(FL.OCEXT.bpmno,1);
FL.OCEXT.bpmyavg=zeros(FL.OCEXT.bpmno,1);
for R2=1:FL.OCEXT.bpmno
if isnan(instdata{1}(R2).x) == 1
FL.OCEXT.bpmxavg(R2,1)=FL.OCEXT.bpmtarget(R2,1);
else
FL.OCEXT.bpmxavg(R2,1)=instdata{1}(R2).x;
end
if isnan(instdata{1}(R2).y) == 1
FL.OCEXT.bpmyavg(R2,1)=FL.OCEXT.bpmtarget(R2,2);
else
FL.OCEXT.bpmyavg(R2,1)=instdata{1}(R2).y;
end
end
zv1xpos=findcells(BEAMLINE,'Name','ZV1X')-1;
list=findcells({BEAMLINE{zv1xpos(end):FL.OCEXT.fflist(1)}},'Class','MONI')+FL.OCEXT.iexlist(end)-1;
bpmslist=[];
for r=1:length(list)
bpmslist=[bpmslist,BEAMLINE{list(r)}.S];
end
plot(bpmslist,FL.OCEXT.bpmxavg*10^6,'Color','blue')
hold on
plot(bpmslist,FL.OCEXT.bpmyavg*10^6,'Color','red')
hold off
xlabel('S (m)')
ylabel('BPM Reading (microns)')
title('Current Extraction Line Orbit')
h = legend('Horizontal','Vertical',1);
set(h,'Interpreter','none')
set(handles.text3, 'String', 'Ready')
set(handles.text3, 'String', 'Ready')
% hObject    handle to pushbutton4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on button press in pushbutton5.
function pushbutton5_Callback(hObject, eventdata, handles)
global PS FL
AccessRequest('release',FL.OCEXT.resp);
close(handles.figure1);
% hObject    handle to pushbutton5 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on selection change in popupmenu1.
function popupmenu1_Callback(hObject, eventdata, handles)
% hObject    handle to popupmenu1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = get(hObject,'String') returns popupmenu1 contents as cell array
%        contents{get(hObject,'Value')} returns selected item from popupmenu1


% --- Executes during object creation, after setting all properties.
function popupmenu1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to popupmenu1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes during object creation, after setting all properties.
function text3_CreateFcn(hObject, eventdata, handles)
% hObject    handle to text3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

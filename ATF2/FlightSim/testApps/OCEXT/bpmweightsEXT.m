function varargout = bpmweightsEXT(varargin)
% BPMWEIGHTSEXT M-file for bpmweightsEXT.fig
%      BPMWEIGHTSEXT, by itself, creates a new BPMWEIGHTSEXT or raises the existing
%      singleton*.
%
%      H = BPMWEIGHTSEXT returns the handle to a new BPMWEIGHTSEXT or the handle to
%      the existing singleton*.
%
%      BPMWEIGHTSEXT('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in BPMWEIGHTSEXT.M with the given input arguments.
%
%      BPMWEIGHTSEXT('Property','Value',...) creates a new BPMWEIGHTSEXT or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before bpmweightsEXT_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to bpmweightsEXT_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help bpmweightsEXT

% Last Modified by GUIDE v2.5 17-Apr-2009 05:41:09

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @bpmweightsEXT_OpeningFcn, ...
                   'gui_OutputFcn',  @bpmweightsEXT_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before bpmweightsEXT is made visible.
function bpmweightsEXT_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to bpmweightsEXT (see VARARGIN)

% Choose default command line output for bpmweightsEXT
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes bpmweightsEXT wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = bpmweightsEXT_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on button press in pushbutton1.
function pushbutton1_Callback(hObject, eventdata, handles)
close(handles.figure1);
% hObject    handle to pushbutton1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes when entered data in editable cell(s) in uitable1.
function uitable1_CellEditCallback(hObject, eventdata, handles)
global FL
FL.OCEXT.bpmweights=get(hObject,'data');
% hObject    handle to uitable1 (see GCBO)
% eventdata  structure with the following fields (see UITABLE)
%	Indices: row and column indices of the cell(s) edited
%	PreviousData: previous data for the cell(s) edited
%	EditData: string(s) entered by the user
%	NewData: EditData or its converted form set on the Data property. Empty if Data was not changed
%	Error: error string when failed to convert EditData to appropriate value for Data
% handles    structure with handles and user data (see GUIDATA)


% --- Executes during object creation, after setting all properties.
function uitable1_CreateFcn(hObject, eventdata, handles)
global FL
bpmweights=FL.OCEXT.bpmweights;
set(hObject,'Data',bpmweights)
% hObject    handle to uitable1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called


% --- Executes on button press in pushbutton2.
function pushbutton2_Callback(hObject, eventdata, handles)
global FL
bpmweights=FL.OCEXT.bpmweights;
save(FL.OCEXT.tabledatafile,'-append','bpmweights')
% hObject    handle to pushbutton2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on button press in pushbutton3.
function pushbutton3_Callback(hObject, eventdata, handles)
global FL
load(FL.OCEXT.tabledatafile)
FL.OCEXT.bpmweights=bpmweights;
set(handles.uitable1,'Data',bpmweights)
% hObject    handle to pushbutton3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

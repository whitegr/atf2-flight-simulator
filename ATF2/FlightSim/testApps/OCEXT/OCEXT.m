function varargout = OCEXT(varargin)
% OCEXT M-file for OCEXT.fig
%      OCEXT, by itself, creates a new OCEXT or raises the existing
%      singleton*.
%
%      H = OCEXT returns the handle to a new OCEXT or the handle to
%      the existing singleton*.
%
%      OCEXT('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in OCEXT.M with the given input arguments.
%
%      OCEXT('Property','Value',...) creates a new OCEXT or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before OCEXT_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to OCEXT_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help OCEXT

% Last Modified by GUIDE v2.5 15-Apr-2009 14:13:43 

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @OCEXT_OpeningFcn, ...
                   'gui_OutputFcn',  @OCEXT_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before OCEXT is made visible.
function OCEXT_OpeningFcn(hObject, eventdata, handles, varargin)
global FL BEAMLINE PS
FlHwUpdate();
iexlist=findcells(BEAMLINE,'Name','IEX');
extstart=findcells(BEAMLINE,'Name','QF1X');
fflist=findcells(BEAMLINE,'Name','QM16FF');
zv1xpos=findcells(BEAMLINE,'Name','ZV1X')-1;
bpmno=length(findcells({BEAMLINE{zv1xpos(end):fflist(1)}},'Class','MONI'));
list = findcells(BEAMLINE,'Class','XCOR');
xcorpos=list(find(list>zv1xpos(end),1):length(list)-1);
xcorlist=zeros(1, length(xcorpos));
for R=1:length(xcorpos)
xcorlist(R)=BEAMLINE{xcorpos(R)}.PS;
end
list = findcells(BEAMLINE,'Class','YCOR');
ycorpos=list(find(list>zv1xpos(end),1):length(list)-1);
ycorlist=zeros(1, length(ycorpos));
for R=1:length(ycorpos)
ycorlist(R)=BEAMLINE{ycorpos(R)}.PS;
end
fullcorlist=[xcorlist,ycorlist];
FL.OCEXT.corrreset=[PS(fullcorlist).Ampl];
FL.OCEXT.savefilename='testApps/OCEXT/EXTresults.txt';
FL.OCEXT.overwrite=1;
FL.OCEXT.bpmweights=ones(bpmno,2);
FL.OCEXT.bpmtarget=zeros(bpmno,2);
FL.OCEXT.hcorrweights=ones(length(xcorpos),2);
FL.OCEXT.vcorrweights=ones(length(ycorpos),2);
FL.OCEXT.maxiter=10;
FL.OCEXT.tabledatafile='testApps/OCEXT/defaults.mat';
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to OCEXT (see VARARGIN)

% Choose default command line output for OCEXT
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes OCEXT wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = OCEXT_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on button press in pushbutton1.
function pushbutton1_Callback(hObject, eventdata, handles)
bpmtargetEXT
% hObject    handle to pushbutton1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on button press in pushbutton2.
function pushbutton2_Callback(hObject, eventdata, handles)
bpmweightsEXT
% hObject    handle to pushbutton2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on button press in pushbutton3.
function pushbutton3_Callback(hObject, eventdata, handles)
correctorweightsEXT
% hObject    handle to pushbutton3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on button press in pushbutton4.
function pushbutton4_Callback(hObject, eventdata, handles)
global FL PS BEAMLINE
set(handles.text5, 'String', 'Auto-run')
if FL.OCEXT.overwrite==1
    delete(FL.OCEXT.savefilename)
end
FlHwUpdate();
openfile=open('thermatsEXT.mat');
FL.OCEXT.xmat=openfile.xmat;
FL.OCEXT.ymat=openfile.ymat;
for R2=1:length(FL.OCEXT.bpmweights(:,1))
FL.OCEXT.xmat(:,R2)=FL.OCEXT.xmat(:,R2)*FL.OCEXT.bpmweights(R2,1);
end
for R2=1:length(FL.OCEXT.hcorrweights(:,1))
FL.OCEXT.xmat(R2,:)=FL.OCEXT.xmat(R2,:)*FL.OCEXT.hcorrweights(R2,1);
end
for R2=1:length(FL.OCEXT.bpmweights(:,2))
FL.OCEXT.ymat(:,R2)=FL.OCEXT.ymat(:,R2)*FL.OCEXT.bpmweights(R2,2);
end
for R2=1:length(FL.OCEXT.vcorrweights(:,1))
FL.OCEXT.ymat(R2,:)=FL.OCEXT.ymat(R2,:)*FL.OCEXT.vcorrweights(R2,1);
end
FL.OCEXT.iexlist=findcells(BEAMLINE,'Name','IEX');
FL.OCEXT.extstart=findcells(BEAMLINE,'Name','QF1X');
FL.OCEXT.fflist=findcells(BEAMLINE,'Name','QM16FF');
zv1xpos=findcells(BEAMLINE,'Name','ZV1X')-1;
FL.OCEXT.bpmno=length(findcells({BEAMLINE{zv1xpos(end):FL.OCEXT.fflist(1)}},'Class','MONI'));
list = findcells(BEAMLINE,'Class','XCOR');
FL.OCEXT.xcorpos=list(find(list>zv1xpos(end),1):length(list)-1);
FL.OCEXT.xcorlist=zeros(1, length(FL.OCEXT.xcorpos));
for R=1:length(FL.OCEXT.xcorpos)
FL.OCEXT.xcorlist(R)=BEAMLINE{FL.OCEXT.xcorpos(R)}.PS;
end
list = findcells(BEAMLINE,'Class','YCOR');
FL.OCEXT.ycorpos=list(find(list>zv1xpos(end),1):length(list)-1);
FL.OCEXT.ycorlist=zeros(1, length(FL.OCEXT.ycorpos));
for R=1:length(FL.OCEXT.ycorpos)
FL.OCEXT.ycorlist(R)=BEAMLINE{FL.OCEXT.ycorpos(R)}.PS;
end
FL.OCEXT.fullcorlist=[FL.OCEXT.xcorlist,FL.OCEXT.ycorlist];
FL.OCEXT.iternum=0;
request{1}=[];
request{2}=FL.OCEXT.fullcorlist;
request{3}=[];
[stat FL.OCEXT.resp] = AccessRequest(request);
FL.OCEXT.stop=0;
while FL.OCEXT.stop==0
pause on
pause(1);
pause off
FlHwUpdate();
stat=FlHwUpdate('wait',10);
[stat,output]=FlHwUpdate('bpmave',10);
[stat instdata] = FlTrackThru(FL.OCEXT.iexlist(end),FL.OCEXT.fflist(end),output);
FL.OCEXT.bpmxavg=zeros(FL.OCEXT.bpmno,1);
FL.OCEXT.bpmyavg=zeros(FL.OCEXT.bpmno,1);
for R2=1:FL.OCEXT.bpmno
if isnan(instdata{1}(R2).x) == 1
FL.OCEXT.bpmxavg(R2,1)=FL.OCEXT.bpmtarget(R2,1);
else
FL.OCEXT.bpmxavg(R2,1)=instdata{1}(R2).x;
end
if isnan(instdata{1}(R2).y) == 1
FL.OCEXT.bpmyavg(R2,1)=FL.OCEXT.bpmtarget(R2,2);
else
FL.OCEXT.bpmyavg(R2,1)=instdata{1}(R2).y;
end
end
FL.OCEXT.corrundo=[PS(FL.OCEXT.fullcorlist).Ampl];
FL.OCEXT.iternum=FL.OCEXT.iternum+1;
zv1xpos=findcells(BEAMLINE,'Name','ZV1X')-1;
list=findcells({BEAMLINE{zv1xpos(end):FL.OCEXT.fflist(1)}},'Class','MONI')+FL.OCEXT.iexlist(end)-1;
bpmslist=[];
for r=1:length(list)
bpmslist=[bpmslist,BEAMLINE{list(r)}.S];
end
plot(bpmslist,FL.OCEXT.bpmxavg*10^6,'Color','blue')
hold on
plot(bpmslist,FL.OCEXT.bpmyavg*10^6,'Color','red')
hold off
xlabel('S (m)')
ylabel('BPM Reading (microns)')
title('Current Extraction Line Orbit')
h = legend('Horizontal','Vertical',1);
set(h,'Interpreter','none')
dlmwrite(FL.OCEXT.savefilename,'====================== new track ==================','delimiter', ' ','roffset', 2,'-append');
dlmwrite(FL.OCEXT.savefilename,'iternum','delimiter', ' ','roffset', 2,'-append');
dlmwrite(FL.OCEXT.savefilename,FL.OCEXT.iternum,'delimiter', ' ','roffset', 2,'-append');
dlmwrite(FL.OCEXT.savefilename,'xrms','delimiter', ' ','roffset', 2,'-append');
dlmwrite(FL.OCEXT.savefilename,sqrt(mean(FL.OCEXT.bpmxavg.^2)),'delimiter', ' ','roffset', 2,'-append');
dlmwrite(FL.OCEXT.savefilename,'yrms','delimiter', ' ','roffset', 2,'-append');
dlmwrite(FL.OCEXT.savefilename,sqrt(mean(FL.OCEXT.bpmyavg.^2)),'delimiter', ' ','roffset', 2,'-append')
dlmwrite(FL.OCEXT.savefilename,'hor','delimiter', ' ','roffset', 2,'-append');
dlmwrite(FL.OCEXT.savefilename,FL.OCEXT.bpmxavg,'delimiter', ' ','roffset', 2,'-append');
dlmwrite(FL.OCEXT.savefilename,'vert','delimiter', ' ','roffset', 2,'-append');
dlmwrite(FL.OCEXT.savefilename,FL.OCEXT.bpmyavg,'delimiter', ' ','roffset', 2,'-append');
dlmwrite(FL.OCEXT.savefilename,'---------------------------','delimiter', ' ','roffset', 2,'-append')
FL.OCEXT.horcorr=transpose(pinv(FL.OCEXT.xmat))*(FL.OCEXT.bpmxavg-FL.OCEXT.bpmtarget(:,1))*10^-5;
FL.OCEXT.vertcorr=transpose(pinv(FL.OCEXT.ymat))*(FL.OCEXT.bpmyavg-FL.OCEXT.bpmtarget(:,2))*10^-5;
FL.OCEXT.midhorcorr=transpose(pinv(FL.OCEXT.xmat))*((FL.OCEXT.bpmxavg-FL.OCEXT.bpmtarget(:,1))/2)*10^-5;
FL.OCEXT.midvertcorr=transpose(pinv(FL.OCEXT.ymat))*((FL.OCEXT.bpmyavg-FL.OCEXT.bpmtarget(:,2))/2)*10^-5;
dlmwrite(FL.OCEXT.savefilename,'horcorr','delimiter', ' ','roffset', 2,'-append');
dlmwrite(FL.OCEXT.savefilename,FL.OCEXT.horcorr,'delimiter', ' ','roffset', 2,'-append');
dlmwrite(FL.OCEXT.savefilename,'vertcorr','delimiter', ' ','roffset', 2,'-append');
dlmwrite(FL.OCEXT.savefilename,FL.OCEXT.vertcorr,'delimiter', ' ','roffset', 2,'-append');
dlmwrite(FL.OCEXT.savefilename,'mid-point horcorr','delimiter', ' ','roffset', 2,'-append');
dlmwrite(FL.OCEXT.savefilename,FL.OCEXT.midhorcorr,'delimiter', ' ','roffset', 2,'-append');
dlmwrite(FL.OCEXT.savefilename,'mid-point vertcorr','delimiter', ' ','roffset', 2,'-append');
dlmwrite(FL.OCEXT.savefilename,FL.OCEXT.midvertcorr,'delimiter', ' ','roffset', 2,'-append');
for R1=1:length(FL.OCEXT.xcorlist)
PS(FL.OCEXT.xcorlist(R1)).SetPt=PS(FL.OCEXT.xcorlist(R1)).Ampl+FL.OCEXT.horcorr(R1);
end
        for R1=1:length(FL.OCEXT.ycorlist)
PS(FL.OCEXT.ycorlist(R1)).SetPt=PS(FL.OCEXT.ycorlist(R1)).Ampl+FL.OCEXT.vertcorr(R1);
        end
        dlmwrite(FL.OCEXT.savefilename,'full correction','delimiter', ' ','roffset', 2,'-append');
dlmwrite(FL.OCEXT.savefilename,'horsetpt','delimiter', ' ','roffset', 2,'-append');
dlmwrite(FL.OCEXT.savefilename,[PS(FL.OCEXT.xcorlist).SetPt],'delimiter', ' ','roffset', 2,'-append');
dlmwrite(FL.OCEXT.savefilename,'vertsetpt','delimiter', ' ','roffset', 2,'-append');
dlmwrite(FL.OCEXT.savefilename,[PS(FL.OCEXT.ycorlist).SetPt],'delimiter', ' ','roffset', 2,'-append');
        PSTrim(FL.OCEXT.fullcorlist, 1);
        pause on
        pause(1);
        pause off
        FlHwUpdate();
FL.OCEXT.bpmxavgprev=FL.OCEXT.bpmxavg;
FL.OCEXT.bpmyavgprev=FL.OCEXT.bpmyavg;
FlHwUpdate('wait',10);
[stat,output]=FlHwUpdate('bpmave',10);
[stat instdata] = FlTrackThru(FL.OCEXT.iexlist(end),FL.OCEXT.fflist(end),output);
FL.OCEXT.bpmxavg=zeros(FL.OCEXT.bpmno,1);
FL.OCEXT.bpmyavg=zeros(FL.OCEXT.bpmno,1);
for R2=1:FL.OCEXT.bpmno
if isnan(instdata{1}(R2).x) == 1
FL.OCEXT.bpmxavg(R2,1)=FL.OCEXT.bpmtarget(R2,1);
else
FL.OCEXT.bpmxavg(R2,1)=instdata{1}(R2).x;
end
if isnan(instdata{1}(R2).y) == 1
FL.OCEXT.bpmyavg(R2,1)=FL.OCEXT.bpmtarget(R2,2);
else
FL.OCEXT.bpmyavg(R2,1)=instdata{1}(R2).y;
end
end
FL.OCEXT.iternum=FL.OCEXT.iternum+1;
list=findcells({BEAMLINE{FL.OCEXT.iexlist(end):FL.OCEXT.fflist(1)}},'Class','MONI')+FL.OCEXT.iexlist(end)-1;
bpmslist=[];
for r=1:length(list)
bpmslist=[bpmslist,BEAMLINE{list(r)}.S];
end
plot(bpmslist,FL.OCEXT.bpmxavg*10^6,'Color','blue')
hold on
plot(bpmslist,FL.OCEXT.bpmyavg*10^6,'Color','red')
hold off
xlabel('S (m)')
ylabel('BPM Reading (microns)')
title('Current Extraction Line Orbit')
h = legend('Horizontal','Vertical',1);
set(h,'Interpreter','none')
dlmwrite(FL.OCEXT.savefilename,'iternum','delimiter', ' ','roffset', 2,'-append');
dlmwrite(FL.OCEXT.savefilename,FL.OCEXT.iternum,'delimiter', ' ','roffset', 2,'-append');
dlmwrite(FL.OCEXT.savefilename,'xrms','delimiter', ' ','roffset', 2,'-append');
dlmwrite(FL.OCEXT.savefilename,sqrt(mean(FL.OCEXT.bpmxavg.^2)),'delimiter', ' ','roffset', 2,'-append');
dlmwrite(FL.OCEXT.savefilename,'yrms','delimiter', ' ','roffset', 2,'-append');
dlmwrite(FL.OCEXT.savefilename,sqrt(mean(FL.OCEXT.bpmyavg.^2)),'delimiter', ' ','roffset', 2,'-append')
dlmwrite(FL.OCEXT.savefilename,'hor','delimiter', ' ','roffset', 2,'-append');
dlmwrite(FL.OCEXT.savefilename,FL.OCEXT.bpmxavg,'delimiter', ' ','roffset', 2,'-append');
dlmwrite(FL.OCEXT.savefilename,'vert','delimiter', ' ','roffset', 2,'-append');
dlmwrite(FL.OCEXT.savefilename,FL.OCEXT.bpmyavg,'delimiter', ' ','roffset', 2,'-append');
dlmwrite(FL.OCEXT.savefilename,'---------------------------','delimiter', ' ','roffset', 2,'-append')
if sqrt(mean(FL.OCEXT.bpmxavg.^2))/sqrt(mean(FL.OCEXT.bpmxavgprev.^2))>=1
    if sqrt(mean(FL.OCEXT.bpmyavg.^2))/sqrt(mean(FL.OCEXT.bpmyavgprev.^2))>=1
        FL.OCEXT.stop=1;
    end
end
if FL.OCEXT.iternum>=FL.OCEXT.maxiter
    FL.OCEXT.stop=1;
end
end
for R1=1:length(FL.OCEXT.xcorlist)
PS(FL.OCEXT.xcorlist(R1)).SetPt=PS(FL.OCEXT.xcorlist(R1)).Ampl-FL.OCEXT.horcorr(R1);
end
        for R1=1:length(FL.OCEXT.ycorlist)
PS(FL.OCEXT.ycorlist(R1)).SetPt=PS(FL.OCEXT.ycorlist(R1)).Ampl-FL.OCEXT.vertcorr(R1);
        end
        dlmwrite(FL.OCEXT.savefilename,'full correction','delimiter', ' ','roffset', 2,'-append');
dlmwrite(FL.OCEXT.savefilename,'horsetpt','delimiter', ' ','roffset', 2,'-append');
dlmwrite(FL.OCEXT.savefilename,[PS(FL.OCEXT.xcorlist).SetPt],'delimiter', ' ','roffset', 2,'-append');
dlmwrite(FL.OCEXT.savefilename,'vertsetpt','delimiter', ' ','roffset', 2,'-append');
dlmwrite(FL.OCEXT.savefilename,[PS(FL.OCEXT.ycorlist).SetPt],'delimiter', ' ','roffset', 2,'-append');
        PSTrim(FL.OCEXT.fullcorlist, 1);
        pause on
        pause(1);
        pause off
        FlHwUpdate();
FL.OCEXT.bpmxavgprev=FL.OCEXT.bpmxavg;
FL.OCEXT.bpmyavgprev=FL.OCEXT.bpmyavg;
FlHwUpdate('wait',10);
[stat,output]=FlHwUpdate('bpmave',10);
[stat instdata] = FlTrackThru(FL.OCEXT.iexlist(end),FL.OCEXT.fflist(end),output);
FL.OCEXT.bpmxavg=zeros(FL.OCEXT.bpmno,1);
FL.OCEXT.bpmyavg=zeros(FL.OCEXT.bpmno,1);
for R2=1:FL.OCEXT.bpmno
if isnan(instdata{1}(R2).x) == 1
FL.OCEXT.bpmxavg(R2,1)=FL.OCEXT.bpmtarget(R2,1);
else
FL.OCEXT.bpmxavg(R2,1)=instdata{1}(R2).x;
end
if isnan(instdata{1}(R2).y) == 1
FL.OCEXT.bpmyavg(R2,1)=FL.OCEXT.bpmtarget(R2,2);
else
FL.OCEXT.bpmyavg(R2,1)=instdata{1}(R2).y;
end
end
FL.OCEXT.iternum=FL.OCEXT.iternum+1;
list=findcells({BEAMLINE{FL.OCEXT.iexlist(end):FL.OCEXT.fflist(1)}},'Class','MONI')+FL.OCEXT.iexlist(end)-1;
bpmslist=[];
for r=1:length(list)
bpmslist=[bpmslist,BEAMLINE{list(r)}.S];
end
plot(bpmslist,FL.OCEXT.bpmxavg*10^6,'Color','blue')
hold on
plot(bpmslist,FL.OCEXT.bpmyavg*10^6,'Color','red')
hold off
xlabel('S (m)')
ylabel('BPM Reading (microns)')
title('Current Extraction Line Orbit')
h = legend('Horizontal','Vertical',1);
set(h,'Interpreter','none')
dlmwrite(FL.OCEXT.savefilename,'iternum','delimiter', ' ','roffset', 2,'-append');
dlmwrite(FL.OCEXT.savefilename,FL.OCEXT.iternum,'delimiter', ' ','roffset', 2,'-append');
dlmwrite(FL.OCEXT.savefilename,'xrms','delimiter', ' ','roffset', 2,'-append');
dlmwrite(FL.OCEXT.savefilename,sqrt(mean(FL.OCEXT.bpmxavg.^2)),'delimiter', ' ','roffset', 2,'-append');
dlmwrite(FL.OCEXT.savefilename,'yrms','delimiter', ' ','roffset', 2,'-append');
dlmwrite(FL.OCEXT.savefilename,sqrt(mean(FL.OCEXT.bpmyavg.^2)),'delimiter', ' ','roffset', 2,'-append')
dlmwrite(FL.OCEXT.savefilename,'hor','delimiter', ' ','roffset', 2,'-append');
dlmwrite(FL.OCEXT.savefilename,FL.OCEXT.bpmxavg,'delimiter', ' ','roffset', 2,'-append');
dlmwrite(FL.OCEXT.savefilename,'vert','delimiter', ' ','roffset', 2,'-append');
dlmwrite(FL.OCEXT.savefilename,FL.OCEXT.bpmyavg,'delimiter', ' ','roffset', 2,'-append');
dlmwrite(FL.OCEXT.savefilename,'---------------------------','delimiter', ' ','roffset', 2,'-append')
AccessRequest('release',FL.OCEXT.resp);
if FL.OCEXT.stop==2
   set(handles.text5, 'String', 'Aborted') 
else
set(handles.text5, 'String', 'Ready')
end

% hObject    handle to pushbutton4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on button press in pushbutton5.
function pushbutton5_Callback(hObject, eventdata, handles)
FL.OCEXT.stop=2;
% hObject    handle to pushbutton5 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on button press in pushbutton6.
function pushbutton6_Callback(hObject, eventdata, handles)
manualEXT
% hObject    handle to pushbutton6 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on button press in pushbutton7.
function pushbutton7_Callback(hObject, eventdata, handles)
global FL BEAMLINE PS
set(handles.text5, 'String', 'Busy')
iexlist=findcells(BEAMLINE,'Name','IEX');
extstart=findcells(BEAMLINE,'Name','QF1X');
fflist=findcells(BEAMLINE,'Name','QM16FF');
zv1xpos=findcells(BEAMLINE,'Name','ZV1X')-1;
bpmno=length(findcells({BEAMLINE{zv1xpos(end):fflist(1)}},'Class','MONI'));
list = findcells(BEAMLINE,'Class','XCOR');
xcorpos=list(find(list>zv1xpos(end),1):length(list)-1);
xcorlist=zeros(1, length(xcorpos));
for R=1:length(xcorpos)
xcorlist(R)=BEAMLINE{xcorpos(R)}.PS;
end
list = findcells(BEAMLINE,'Class','YCOR');
ycorpos=list(find(list>zv1xpos(end),1):length(list)-1);
ycorlist=zeros(1, length(ycorpos));
for R=1:length(ycorpos)
ycorlist(R)=BEAMLINE{ycorpos(R)}.PS;
end
fullcorlist=[xcorlist,ycorlist];
request{1}=[];
request{2}=fullcorlist;
request{3}=[];
[stat resp] = AccessRequest(request);
for R1=1:length(fullcorlist)
PS(fullcorlist(R1)).SetPt=FL.OCEXT.corrreset(R1);
end
PSTrim(fullcorlist, 1);
pause on
pause(1);
pause off
FlHwUpdate();
AccessRequest('release',resp);
set(handles.text5, 'String', 'Ready')
% hObject    handle to pushbutton7 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on button press in pushbutton8.
function pushbutton8_Callback(hObject, eventdata, handles)
global FL BEAMLINE PS
set(handles.text5, 'String', 'Busy')
pause on
pause(1)
pause off
FlHwUpdate();
iexlist=findcells(BEAMLINE,'Name','IEX');
extstart=findcells(BEAMLINE,'Name','QF1X');
fflist=findcells(BEAMLINE,'Name','QM16FF');
zv1xpos=findcells(BEAMLINE,'Name','ZV1X')-1;
bpmno=length(findcells({BEAMLINE{zv1xpos(end):fflist(1)}},'Class','MONI'));
list = findcells(BEAMLINE,'Class','XCOR');
xcorpos=list(find(list>zv1xpos(end),1):length(list)-1);
xcorlist=zeros(1, length(xcorpos));
for R=1:length(xcorpos)
xcorlist(R)=BEAMLINE{xcorpos(R)}.PS;
end
list = findcells(BEAMLINE,'Class','YCOR');
ycorpos=list(find(list>zv1xpos(end),1):length(list)-1);
ycorlist=zeros(1, length(ycorpos));
for R=1:length(ycorpos)
ycorlist(R)=BEAMLINE{ycorpos(R)}.PS;
end
fullcorlist=[xcorlist,ycorlist];
FL.OCEXT.corrreset=[PS(fullcorlist).Ampl];
set(handles.text5, 'String', 'Ready')
% hObject    handle to pushbutton8 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on button press in pushbutton9.
function pushbutton9_Callback(hObject, eventdata, handles)
close(handles.figure1);
% hObject    handle to pushbutton9 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on button press in pushbutton10.
function pushbutton10_Callback(hObject, eventdata, handles)
outputfileEXT
% hObject    handle to pushbutton10 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on slider movement.
function slider1_Callback(hObject, eventdata, handles)
% hObject    handle to slider1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider


% --- Executes during object creation, after setting all properties.
function slider1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to slider1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end



function edit3_Callback(hObject, eventdata, handles)
global FL
user_entry = str2double(get(hObject,'string'));
if isnan(user_entry)
  errordlg('You must enter a numeric value','Bad Input','modal')
  uicontrol(hObject)
	return
else
    FL.OCEXT.maxiter=user_entry;
end
% hObject    handle to edit3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit3 as text
%        str2double(get(hObject,'String')) returns contents of edit3 as a double


% --- Executes during object creation, after setting all properties.
function edit3_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

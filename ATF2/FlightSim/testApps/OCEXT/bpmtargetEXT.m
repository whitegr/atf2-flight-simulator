function varargout = bpmtargetEXT(varargin)
% BPMTARGETEXT M-file for bpmtargetEXT.fig
%      BPMTARGETEXT, by itself, creates a new BPMTARGETEXT or raises the existing
%      singleton*.
%
%      H = BPMTARGETEXT returns the handle to a new BPMTARGETEXT or the handle to
%      the existing singleton*.
%
%      BPMTARGETEXT('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in BPMTARGETEXT.M with the given input arguments.
%
%      BPMTARGETEXT('Property','Value',...) creates a new BPMTARGETEXT or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before bpmtargetEXT_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to bpmtargetEXT_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help bpmtargetEXT

% Last Modified by GUIDE v2.5 17-Apr-2009 04:32:32

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @bpmtargetEXT_OpeningFcn, ...
                   'gui_OutputFcn',  @bpmtargetEXT_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before bpmtargetEXT is made visible.
function bpmtargetEXT_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to bpmtargetEXT (see VARARGIN)

% Choose default command line output for bpmtargetEXT
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes bpmtargetEXT wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = bpmtargetEXT_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on button press in pushbutton1.
function pushbutton1_Callback(hObject, eventdata, handles)
close(handles.figure1);
% hObject    handle to pushbutton1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes when entered data in editable cell(s) in uitable1.
function uitable1_CellEditCallback(hObject, eventdata, handles)
global FL
FL.OCEXT.bpmtarget=get(hObject,'data')*10^-6;
% hObject    handle to uitable1 (see GCBO)
% eventdata  structure with the following fields (see UITABLE)
%	Indices: row and column indices of the cell(s) edited
%	PreviousData: previous data for the cell(s) edited
%	EditData: string(s) entered by the user
%	NewData: EditData or its converted form set on the Data property. Empty if Data was not changed
%	Error: error string when failed to convert EditData to appropriate value for Data
% handles    structure with handles and user data (see GUIDATA)


% --- Executes during object creation, after setting all properties.
function uitable1_CreateFcn(hObject, eventdata, handles)
global FL
bpmtarget=FL.OCEXT.bpmtarget;
set(hObject,'Data',bpmtarget*10^6)
% hObject    handle to uitable1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called


% --- Executes on button press in pushbutton2.
function pushbutton2_Callback(hObject, eventdata, handles)
global FL
bpmtarget=FL.OCEXT.bpmtarget;
save(FL.OCEXT.tabledatafile,'-append','bpmtarget')
% hObject    handle to pushbutton2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on button press in pushbutton3.
function pushbutton3_Callback(hObject, eventdata, handles)
global FL
load(FL.OCEXT.tabledatafile)
FL.OCEXT.bpmtarget=bpmtarget;
set(handles.uitable1,'Data',bpmtarget*10^6)
% hObject    handle to pushbutton3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on button press in pushbutton4.
function pushbutton4_Callback(hObject, eventdata, handles)
global FL BEAMLINE
set(handles.text5, 'String', 'Busy')
pause on
pause(1);
pause off
FlHwUpdate();
FL.OCEXT.iexlist=findcells(BEAMLINE,'Name','IEX');
FL.OCEXT.extstart=findcells(BEAMLINE,'Name','QF1X');
FL.OCEXT.fflist=findcells(BEAMLINE,'Name','QM16FF');
FL.OCEXT.zv1xpos=findcells(BEAMLINE,'Name','ZV1X');
FL.OCEXT.bpmno=length(findcells({BEAMLINE{FL.OCEXT.zv1xpos(end):FL.OCEXT.fflist(1)}},'Class','MONI'));
stat=FlHwUpdate('wait',10);
[stat,output]=FlHwUpdate('bpmave',10);
[stat instdata] = FlTrackThru(FL.OCEXT.iexlist(end),FL.OCEXT.fflist(end),output);
FL.OCEXT.bpmxavg=zeros(FL.OCEXT.bpmno,1);
FL.OCEXT.bpmyavg=zeros(FL.OCEXT.bpmno,1);
for R2=1:FL.OCEXT.bpmno
if isnan(instdata{1}(R2).x) == 1
FL.OCEXT.bpmxavg(R2,1)=FL.OCEXT.bpmtarget(R2,1);
else
FL.OCEXT.bpmxavg(R2,1)=instdata{1}(R2).x;
end
if isnan(instdata{1}(R2).y) == 1
FL.OCEXT.bpmyavg(R2,1)=FL.OCEXT.bpmtarget(R2,2);
else
FL.OCEXT.bpmyavg(R2,1)=instdata{1}(R2).y;
end
end
FL.OCEXT.bpmtarget(:,1)=FL.OCEXT.bpmxavg(:,1);
FL.OCEXT.bpmtarget(:,2)=FL.OCEXT.bpmyavg(:,1);
set(handles.uitable1,'Data',FL.OCEXT.bpmtarget*10^6)
set(handles.text5, 'String', 'Ready')
% hObject    handle to pushbutton4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)



function edit3_Callback(hObject, eventdata, handles)
global FL
FL.OCEXT.tabledatafile = get(hObject,'String');
% hObject    handle to edit3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit3 as text
%        str2double(get(hObject,'String')) returns contents of edit3 as a double


% --- Executes during object creation, after setting all properties.
function edit3_CreateFcn(hObject, eventdata, handles)
global FL
FL.OCEXT.tabledatafile='testApps/OCEXT/defaults.mat';
% hObject    handle to edit3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

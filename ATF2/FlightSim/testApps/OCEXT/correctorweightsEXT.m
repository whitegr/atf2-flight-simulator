function varargout = correctorweightsEXT(varargin)
% CORRECTORWEIGHTSEXT M-file for correctorweightsEXT.fig
%      CORRECTORWEIGHTSEXT, by itself, creates a new CORRECTORWEIGHTSEXT or raises the existing
%      singleton*.
%
%      H = CORRECTORWEIGHTSEXT returns the handle to a new CORRECTORWEIGHTSEXT or the handle to
%      the existing singleton*.
%
%      CORRECTORWEIGHTSEXT('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in CORRECTORWEIGHTSEXT.M with the given input arguments.
%
%      CORRECTORWEIGHTSEXT('Property','Value',...) creates a new CORRECTORWEIGHTSEXT or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before correctorweightsEXT_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to correctorweightsEXT_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help correctorweightsEXT

% Last Modified by GUIDE v2.5 17-Apr-2009 05:30:37

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @correctorweightsEXT_OpeningFcn, ...
                   'gui_OutputFcn',  @correctorweightsEXT_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before correctorweightsEXT is made visible.
function correctorweightsEXT_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to correctorweightsEXT (see VARARGIN)

% Choose default command line output for correctorweightsEXT
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes correctorweightsEXT wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = correctorweightsEXT_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on button press in pushbutton1.
function pushbutton1_Callback(hObject, eventdata, handles)
close(handles.figure1);
% hObject    handle to pushbutton1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes when entered data in editable cell(s) in uitable1.
function uitable1_CellEditCallback(hObject, eventdata, handles)
global FL
FL.OCEXT.hcorrweights=get(hObject,'data');
% hObject    handle to uitable1 (see GCBO)
% eventdata  structure with the following fields (see UITABLE)
%	Indices: row and column indices of the cell(s) edited
%	PreviousData: previous data for the cell(s) edited
%	EditData: string(s) entered by the user
%	NewData: EditData or its converted form set on the Data property. Empty if Data was not changed
%	Error: error string when failed to convert EditData to appropriate value for Data
% handles    structure with handles and user data (see GUIDATA)


% --- Executes during object creation, after setting all properties.
function uitable1_CreateFcn(hObject, eventdata, handles)
global FL
hcorrweights=FL.OCEXT.hcorrweights;
set(hObject,'Data',hcorrweights)
% hObject    handle to uitable1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called
% --- Executes when entered data in editable cell(s) in uitable1.


% --- Executes when entered data in editable cell(s) in uitable6.
function uitable6_CellEditCallback(hObject, eventdata, handles)
global FL
FL.OCEXT.vcorrweights=get(hObject,'data');
% hObject    handle to uitable6 (see GCBO)
% eventdata  structure with the following fields (see UITABLE)
%	Indices: row and column indices of the cell(s) edited
%	PreviousData: previous data for the cell(s) edited
%	EditData: string(s) entered by the user
%	NewData: EditData or its converted form set on the Data property. Empty if Data was not changed
%	Error: error string when failed to convert EditData to appropriate value for Data
% handles    structure with handles and user data (see GUIDATA)


% --- Executes during object creation, after setting all properties.
function uitable6_CreateFcn(hObject, eventdata, handles)
global FL
vcorrweights=FL.OCEXT.vcorrweights;
set(hObject,'Data',vcorrweights)
% hObject    handle to uitable6 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called


% --- Executes on button press in pushbutton2.
function pushbutton2_Callback(hObject, eventdata, handles)
global FL
hcorrweights=FL.OCEXT.hcorrweights;
save(FL.OCEXT.tabledatafile,'-append','hcorrweights')
vcorrweights=FL.OCEXT.vcorrweights;
save(FL.OCEXT.tabledatafile,'-append','vcorrweights')
% hObject    handle to pushbutton2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on button press in pushbutton3.
function pushbutton3_Callback(hObject, eventdata, handles)
global FL
load(FL.OCEXT.tabledatafile)
FL.OCEXT.hcorrweights=hcorrweights;
set(handles.uitable1,'Data',hcorrweights)
FL.OCEXT.vcorrweights=vcorrweights;
set(handles.uitable6,'Data',vcorrweights)
% hObject    handle to pushbutton3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

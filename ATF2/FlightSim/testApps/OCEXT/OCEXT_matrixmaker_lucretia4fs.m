global BEAMLINE PS
iexlist=findcells(BEAMLINE,'Name','IEX');
extstart=findcells(BEAMLINE,'Name','QF1X');
fflist=findcells(BEAMLINE,'Name','QM16FF');
zv1xpos=findcells(BEAMLINE,'Name','ZV1X')-1;
bpmno=length(findcells({BEAMLINE{zv1xpos(end):fflist(1)}},'Class','MONI'));
list = findcells(BEAMLINE,'Class','XCOR');
xcorpos=list(find(list>zv1xpos(end),1):length(list)-1);
xcorlist=zeros(1, length(xcorpos));
for R=1:length(xcorpos)
xcorlist(R)=BEAMLINE{xcorpos(R)}.PS;
end
list = findcells(BEAMLINE,'Class','YCOR');
ycorpos=list(find(list>zv1xpos(end),1):length(list)-1);
ycorlist=zeros(1, length(ycorpos));
for R=1:length(ycorpos)
ycorlist(R)=BEAMLINE{ycorpos(R)}.PS;
end
fullcorlist=[xcorlist,ycorlist];
xmat=cell(length(xcorlist),1);
for RR=1:length(xcorlist)
bpmx=[];
bpmy=[];
for R=1:10
  beam=Beam0_IEX;
        beam.Bunch.x(1)=0;
        beam.Bunch.x(2)=0;
        beam.Bunch.x(3)=0;
        beam.Bunch.x(4)=0;
[stat,beamout,instdata]=TrackThru(iexlist(end),length(BEAMLINE),beam,1,1,0);
bpmx1=zeros(1, bpmno);
bpmy1=zeros(1, bpmno);
for R1=1:bpmno
bpmx1(R1)=instdata{1}(R1).x;
bpmy1(R1)=instdata{1}(R1).y;
end
bpmx=[bpmx,bpmx1];
bpmy=[bpmy,bpmy1];
end
bpmxavg=zeros(1, bpmno);
bpmyavg=zeros(1, bpmno);
for R=1:bpmno
    bpmxavg(R)=mean(bpmx(R:bpmno:length(bpmx)));
    bpmyavg(R)=mean(bpmy(R:bpmno:length(bpmy)));
end
bpmxavgprev=bpmxavg;
bpmyavgprev=bpmyavg;
horcorr=zeros(1, length(xcorlist));
vertcorr=zeros(1, length(ycorlist));
horcorr(RR)=1e-5;
    for R=1:length(xcorlist)
PS(xcorlist(R)).SetPt=min(max(PS(xcorlist(R)).Ampl-horcorr(R),-1612.8e-6),1612.8e6);
    end
        for R=1:length(ycorlist)
PS(ycorlist(R)).SetPt=min(max(PS(ycorlist(R)).Ampl-vertcorr(R),-1485.5e-6),1485.5e6);
        end
        PSTrim(fullcorlist);
bpmx=[];
bpmy=[];
for R=1:10
  beam=Beam0_IEX;
        beam.Bunch.x(1)=0;
        beam.Bunch.x(2)=0;
        beam.Bunch.x(3)=0;
        beam.Bunch.x(4)=0;
[stat,beamout,instdata]=TrackThru(iexlist(end),length(BEAMLINE),beam,1,1,0);
bpmx1=zeros(1, bpmno);
bpmy1=zeros(1, bpmno);
for R1=1:bpmno
bpmx1(R1)=instdata{1}(R1).x;
bpmy1(R1)=instdata{1}(R1).y;
end
bpmx=[bpmx,bpmx1];
bpmy=[bpmy,bpmy1];
end
bpmxavg=zeros(1, bpmno);
bpmyavg=zeros(1, bpmno);
for R=1:bpmno
    bpmxavg(R)=mean(bpmx(R:bpmno:length(bpmx)));
    bpmyavg(R)=mean(bpmy(R:bpmno:length(bpmy)));
end
horcorr(RR)=-1e-5;
    for R=1:length(xcorlist)
PS(xcorlist(R)).SetPt=min(max(PS(xcorlist(R)).Ampl-horcorr(R),-1612.8e-6),1612.8e6);
    end
        for R=1:length(ycorlist)
PS(ycorlist(R)).SetPt=min(max(PS(ycorlist(R)).Ampl-vertcorr(R),-1485.5e-6),1485.5e6);
        end
        PSTrim(fullcorlist);
        xdiff=bpmxavg-bpmxavgprev;
        ydiff=bpmyavg-bpmyavgprev;
       xmat{RR}=xdiff;
end
xmat0=xmat;
%for R=1:length(xmat)
%    xmat{R}(6)=0;
%end
%xmat{16}=zeros(1, bpmno);
ymat=cell(length(ycorlist),1);
for RR=1:length(ycorlist)
bpmx=[];
bpmy=[];
for R=1:10
  beam=Beam0_IEX;
        beam.Bunch.x(1)=0;
        beam.Bunch.x(2)=0;
        beam.Bunch.x(3)=0;
        beam.Bunch.x(4)=0;
[stat,beamout,instdata]=TrackThru(iexlist(end),length(BEAMLINE),beam,1,1,0);
bpmx1=zeros(1, bpmno);
bpmy1=zeros(1, bpmno);
for R1=1:bpmno
bpmx1(R1)=instdata{1}(R1).x;
bpmy1(R1)=instdata{1}(R1).y;
end
bpmx=[bpmx,bpmx1];
bpmy=[bpmy,bpmy1];
end
bpmxavg=zeros(1, bpmno);
bpmyavg=zeros(1, bpmno);
for R=1:bpmno
    bpmxavg(R)=mean(bpmx(R:bpmno:length(bpmx)));
    bpmyavg(R)=mean(bpmy(R:bpmno:length(bpmy)));
end
bpmxavgprev=bpmxavg;
bpmyavgprev=bpmyavg;
horcorr=zeros(1, length(xcorlist));
vertcorr=zeros(1, length(ycorlist));
vertcorr(RR)=1e-5;
    for R=1:length(xcorlist)
PS(xcorlist(R)).SetPt=min(max(PS(xcorlist(R)).Ampl-horcorr(R),-1612.8e-6),1612.8e6);
    end
        for R=1:length(ycorlist)
PS(ycorlist(R)).SetPt=min(max(PS(ycorlist(R)).Ampl-vertcorr(R),-1485.5e-6),1485.5e6);
        end
PSTrim(fullcorlist);
bpmx=[];
bpmy=[];
for R=1:10
  beam=Beam0_IEX;
        beam.Bunch.x(1)=0;
        beam.Bunch.x(2)=0;
        beam.Bunch.x(3)=0;
        beam.Bunch.x(4)=0;
[stat,beamout,instdata]=TrackThru(iexlist(end),length(BEAMLINE),beam,1,1,0);
bpmx1=zeros(1, bpmno);
bpmy1=zeros(1, bpmno);
for R1=1:bpmno
bpmx1(R1)=instdata{1}(R1).x;
bpmy1(R1)=instdata{1}(R1).y;
end
bpmx=[bpmx,bpmx1];
bpmy=[bpmy,bpmy1];
end
bpmxavg=zeros(1, bpmno);
bpmyavg=zeros(1, bpmno);
for R=1:bpmno
    bpmxavg(R)=mean(bpmx(R:bpmno:length(bpmx)));
    bpmyavg(R)=mean(bpmy(R:bpmno:length(bpmy)));
end
        vertcorr(RR)=-1e-5;
    for R=1:length(xcorlist)
PS(xcorlist(R)).SetPt=min(max(PS(xcorlist(R)).Ampl-horcorr(R),-1612.8e-6),1612.8e6);
    end
        for R=1:length(ycorlist)
PS(ycorlist(R)).SetPt=min(max(PS(ycorlist(R)).Ampl-vertcorr(R),-1485.5e-6),1485.5e6);
        end
PSTrim(fullcorlist);
        xdiff=bpmxavg-bpmxavgprev;
        ydiff=bpmyavg-bpmyavgprev;
       ymat{RR}=ydiff;
end
ymat0=ymat;
%for R=1:length(ymat)
%    ymat{R}(6)=0;
%end
xmat=cell2mat(xmat);
ymat=cell2mat(ymat);
[U,S,V]=svd(transpose(xmat));
S2=svds(S,length(svd(transpose(xmat))));
%S2=svds(S);
S3=zeros(1,length(S2));
 for R=1:length(S2)
if S2(R)==0
S3(R)=0;
else
S3(R)=1/S2(R);
end
 end
ST=diag(S3);
STadd=zeros(length(U)-length(V),length(ST));
ST=transpose([transpose(ST),transpose(STadd)]);
horresp=V*transpose(ST)*U';
[U,S,V]=svd(transpose(ymat));
S2=svds(S,length(svd(transpose(ymat))));
%S2=svds(S); 
S3=zeros(1,length(S2));
for R=1:length(S2)
if S2(R)==0
S3(R)=0;
else
S3(R)=1/S2(R);
end
 end
ST=diag(S3);
STadd=zeros(length(U)-length(V),length(ST));
ST=transpose([transpose(ST),transpose(STadd)]);
vertresp=V*transpose(ST)*U';
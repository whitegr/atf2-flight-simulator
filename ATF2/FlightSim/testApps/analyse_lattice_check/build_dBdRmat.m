function M = build_dBdRmat(corinds, bpminds, psinds)

global BEAMLINE PS

count = 0;
rmat_halflength = length(corinds) * length(bpminds);
M = zeros(rmat_halflength*2, length(psinds));
for cornum=1:length(corinds)
    for bpmnum=1:length(bpminds)
        count = count + 1;
        
        for psnum=1:length(psinds)
            if corinds(cornum)<bpminds(bpmnum)
                [stat rmat] = RmatAtoB_cor(corinds(cornum),bpminds(bpmnum));
                if stat{1}~=1; error(stat{2}); end
            else
                rmat = zeros(6);
            end
            
            if strcmpi(BEAMLINE{corinds(cornum)}.Class,'XCOR')
                before_x = rmat(1,2);
                before_y = rmat(3,2);
            elseif strcmpi(BEAMLINE{corinds(cornum)}.Class,'YCOR')
                before_x = rmat(1,4);
                before_y = rmat(3,4);
            end
            
            orig_Ampl = PS(psinds(psnum)).Ampl;
            PS(psinds(psnum)).Ampl = orig_Ampl + 0.1;
            
            if corinds(cornum)<bpminds(bpmnum)
                [stat rmat] = RmatAtoB_cor(corinds(cornum),bpminds(bpmnum));
                if stat{1}~=1; error(stat{2}); end
            else
                rmat = zeros(6);
            end
            
            if strcmpi(BEAMLINE{corinds(cornum)}.Class,'XCOR')
                after_x = rmat(1,2);
                after_y = rmat(3,2);
            elseif strcmpi(BEAMLINE{corinds(cornum)}.Class,'YCOR')
                after_x = rmat(1,4);
                after_y = rmat(3,4);
            end
            
            if abs(after_x-before_x)>1e6 || abs(after_y-before_y)>1e6
                disp('!!')
            end
            M(count,psnum) = (after_x - before_x) / 0.1;
            M(count+rmat_halflength,psnum) = (after_y - before_y) / 0.1;
            
            PS(psinds(psnum)).Ampl = orig_Ampl;
        end
    end
end


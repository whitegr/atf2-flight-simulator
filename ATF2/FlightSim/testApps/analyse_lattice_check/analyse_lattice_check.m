global BEAMLINE PS
% persistent BigM

load ATF2lat

BL = BEAMLINE;
PS_start = PS;

filename = uigetfile(...
    '~/Lucretia/src/Floodland/testApps/lattice_check/*.mat',...
    'Load data');

if filename==0; return; end

data = load(['~/Lucretia/src/Floodland/testApps/lattice_check/' filename]);
fnames = fieldnames(data);

ringext = findcells(BEAMLINE,'Name','IEX');
moniinds = findcells(BEAMLINE,'Class','MONI');
moniinds = moniinds(moniinds>ringext);

S_1st = inf;
close_bumps = false;
for datapt=1:length(fnames)
    if eval(['data.' fnames{datapt} '.movers']); continue; end
    corind = findcells(BEAMLINE,'Name',fnames{datapt});
    if BEAMLINE{corind}.S < S_1st
        S_1st = BEAMLINE{corind}.S;
        beg_ind = corind;
    end
    if ~close_bumps && eval(['data.' fnames{datapt} '.close_bumps']); close_bumps = true; end
end

quadinds = findcells(BEAMLINE,'Class','QUAD');
if close_bumps
    quadinds = quadinds(quadinds>beg_ind & quadinds<1464);
else
    quadinds = quadinds(quadinds>beg_inds);
end

psinds = zeros(1,length(quadinds));
for ind = 1:length(quadinds)
    psinds(ind) = BEAMLINE{quadinds(ind)}.PS;
end

psinds = unique(sort(psinds));

eval(['good_bpms = ~isnan(data.' fnames{datapt} '.xdiff) & moniinds<1464;'])

corinds = zeros(1,length(fnames));
Rmatx_meas = [];
Rmaty_meas = [];
for datapt=1:length(fnames)
    if eval(['data.' fnames{datapt} '.movers'])
        continue
    else
        eval(['Rmatx_meas = [Rmatx_meas (data.' fnames{datapt} '.xdiff(good_bpms) / '...
            'data.' fnames{datapt} '.kick)];']);
        eval(['Rmaty_meas = [Rmaty_meas (data.' fnames{datapt} '.ydiff(good_bpms) / '...
            'data.' fnames{datapt} '.kick)];']);
        corinds(datapt) = findcells(BEAMLINE,'Name',fnames{datapt});
        if isempty(corinds(datapt)); error(['Can''t find ' fnames{datapt} ' :(']); end
    end
end

tic; BigM = build_dBdRmat(corinds,moniinds(good_bpms),psinds); toc
% tic; BigM = [BigM build_dEdRvec(corinds,moniinds(good_bpms))]; toc

[Rmatx_calc Rmaty_calc] = build_testRmat(quadinds, corinds, moniinds(good_bpms));

Rmatxc = reshape(Rmatx_calc',1,numel(Rmatx_calc));
Rmatyc = reshape(Rmaty_calc',1,numel(Rmaty_calc));

delta_PS = BigM \ ([Rmatxc Rmatyc] -[Rmatx_meas Rmaty_meas])';

for ind=1:length(psinds)
    PS(psinds(ind)).Ampl = PS(psinds(ind)).Ampl - delta_PS(ind);
end

[Rmatx_new Rmaty_new] = build_testRmat(quadinds, corinds, moniinds(good_bpms));

Rmatxn = reshape(Rmatx_new',1,numel(Rmatx_new));
Rmatyn = reshape(Rmaty_new',1,numel(Rmaty_new));

% figure;plot(delta_PS,'-x')

disp(['RMS difference of theoretical and measured R matrices (original) = ' ...
    num2str(std([Rmatxc Rmatyc] -[Rmatx_meas Rmaty_meas]))])

disp(['RMS difference of theoretical and measured R matrices (new)      = ' ...
    num2str(std([Rmatxn Rmatyn] -[Rmatx_meas Rmaty_meas]))])

BEAMLINE = BL;
PS = PS_start;

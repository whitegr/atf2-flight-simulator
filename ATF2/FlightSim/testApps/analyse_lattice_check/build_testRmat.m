function [Rmatx_calc Rmaty_calc] = build_testRmat(quadinds, corinds, bpminds)

global BEAMLINE

Rmatx_calc = zeros(length(corinds),length(bpminds));
Rmaty_calc = Rmatx_calc;
for cornum=1:length(corinds)
    for bpmnum=1:length(bpminds)
        if corinds(cornum)<bpminds(bpmnum)
            [stat rmat] = RmatAtoB_cor(corinds(cornum), bpminds(bpmnum));
            if stat{1}~=1; error(stat{2}); end
        else
            rmat = zeros(6);
        end
        
        if strcmpi(BEAMLINE{corinds(cornum)}.Class,'XCOR')
            Rmatx_calc(cornum,bpmnum) = rmat(1,2);
            Rmaty_calc(cornum,bpmnum) = rmat(3,2);
        elseif strcmpi(BEAMLINE{corinds(cornum)}.Class,'YCOR')
            Rmatx_calc(cornum,bpmnum) = rmat(1,4);
            Rmaty_calc(cornum,bpmnum) = rmat(3,4);
        else
            error('Should never get here')
        end
    end
end

return
function chi_2 = chi2_rmat(B, Rmatx_meas, Rmaty_meas, quadinds, corinds, bpminds)

[Rmatx_calc Rmaty_calc] = build_testRmat(quadinds, B, corinds, bpminds);

diffx = Rmatx_calc - Rmatx_meas;
diffy = Rmaty_calc - Rmaty_meas;

x_sq = diffx.^2;
y_sq = diffy.^2;

chi_2 = sum(sum(x_sq)) + sum(sum(y_sq));

return
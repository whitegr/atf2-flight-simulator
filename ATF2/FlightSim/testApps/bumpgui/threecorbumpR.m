function bumpcoefs = threecorbump(bumpcorinds, bumppos, plane)

global BEAMLINE

L = BEAMLINE{bumpcorinds(1)}.L/2;
cormat1 = ...
    [1 L 0 0 0 0;
    0 1 0 0 0 0;
    0 0 1 L 0 0;
    0 0 0 1 0 0;
    0 0 0 0 1 0;
    0 0 0 0 0 1;];
L = BEAMLINE{bumpcorinds(2)}.L/2;
cormat2 = ...
    [1 L 0 0 0 0;
    0 1 0 0 0 0;
    0 0 1 L 0 0;
    0 0 0 1 0 0;
    0 0 0 0 1 0;
    0 0 0 0 0 1;];
L = BEAMLINE{bumpcorinds(3)}.L/2;
cormat3 = ...
    [1 L 0 0 0 0;
    0 1 0 0 0 0;
    0 0 1 L 0 0;
    0 0 0 1 0 0;
    0 0 0 0 1 0;
    0 0 0 0 0 1;];

% Correct the cor matrices
[stat Amat_wrong] = RmatAtoB(bumpcorinds(1),bumppos);
if stat{1}~=1; error(stat{2}); end
Amat = inv(cormat1) * Amat_wrong; %#ok<*MINV>

[stat Bmat_wrong] = RmatAtoB(bumpcorinds(1),bumpcorinds(3));
if stat{1}~=1; error(stat{2}); end
Bmat = inv(cormat1) * Bmat_wrong * inv(cormat3);

[stat Cmat_wrong] = RmatAtoB(bumpcorinds(2),bumpcorinds(3));
if stat{1}~=1; error(stat{2}); end
Cmat = inv(cormat2) * Cmat_wrong * inv(cormat3);

switch plane
    case 1
        bumpmat = [...
            Amat(1,2) 0         0;
            Bmat(1,2) Cmat(1,2) 0;
            Bmat(2,2) Cmat(2,2) 1];
    case 2
        bumpmat = [...
            Amat(2,2) 0         0;
            Bmat(1,2) Cmat(1,2) 0;
            Bmat(2,2) Cmat(2,2) 1];
    case 3
        bumpmat = [...
            Amat(3,4) 0         0;
            Bmat(3,4) Cmat(3,4) 0;
            Bmat(4,4) Cmat(4,4) 1];
    case 4
        bumpmat = [...
            Amat(4,4) 0         0;
            Bmat(3,4) Cmat(3,4) 0;
            Bmat(4,4) Cmat(4,4) 1];
end

bumpcoefs = bumpmat \ [1; 0; 0];
% global FL PS
%
% load testApps/bumpgui/savedbumps/fourcorxMQF4X.mat
%
% [stat outbeam] = TrackThru(1,1321,FL.SimBeam{2},1,1);
% if stat{1}~=1; error(stat{2}); end
% origpos = outbeam.Bunch.x(1:2);
%
% psnum=1;
% eval([knob.Channel(psnum).Parameter '=' num2str(knob.Channel(psnum).Coefficient*1e-3)...
%      ';PSTrim(' num2str(knob.Channel(psnum).Unit) ',true)'])
%
% [stat outbeam inst] = TrackThru(1,1321,FL.SimBeam{2},1,1);
% if stat{1}~=1; error(stat{2}); end
% newpos = outbeam.Bunch.x(1:2);
%
% for psnum=1:4
% eval([knob.Channel(psnum).Parameter '=' num2str(knob.Channel(psnum).Coefficient*0)...
%      ';PSTrim(' num2str(knob.Channel(psnum).Unit) ',true)'])
% end
%
% disp('Done')

close all ; clear all global

global BEAMLINE PS

load latticeFiles/ATF2lat

[stat R] = RmatAtoB(1362,1415);

[stat beamout] = TrackThru(1,1415,Beam0,1,1);
origx = beamout.Bunch.x(1);

for count=1:25

    PS(BEAMLINE{1362}.PS).SetPt = PS(BEAMLINE{1362}.PS).SetPt + 1e-3;
    stat = PSTrim(BEAMLINE{1362}.PS)

    [stat beamout] = TrackThru(1,1415,Beam0,1,1);
    newx = beamout.Bunch.x(1);

    pred(count) = R(1,2)*1e-3*count;
    act(count) = newx - origx;
end

figure;plot((1:25)*1e-3,((pred-act)./pred)*1e2,'x')
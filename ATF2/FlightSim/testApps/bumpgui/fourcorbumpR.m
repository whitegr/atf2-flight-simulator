function bumpcoefs = fourcorbump(bumpcorinds, bumppos, plane)

global BEAMLINE

L = BEAMLINE{bumpcorinds(1)}.L/2;
cormat1 = ...
    [1 L 0 0 0 0;
    0 1 0 0 0 0;
    0 0 1 L 0 0;
    0 0 0 1 0 0;
    0 0 0 0 1 0;
    0 0 0 0 0 1;];
L = BEAMLINE{bumpcorinds(2)}.L/2;
cormat2 = ...
    [1 L 0 0 0 0;
    0 1 0 0 0 0;
    0 0 1 L 0 0;
    0 0 0 1 0 0;
    0 0 0 0 1 0;
    0 0 0 0 0 1;];
L = BEAMLINE{bumpcorinds(3)}.L/2;
cormat3 = ...
    [1 L 0 0 0 0;
    0 1 0 0 0 0;
    0 0 1 L 0 0;
    0 0 0 1 0 0;
    0 0 0 0 1 0;
    0 0 0 0 0 1;];
L = BEAMLINE{bumpcorinds(4)}.L/2;
cormat4 = ...
    [1 L 0 0 0 0;
    0 1 0 0 0 0;
    0 0 1 L 0 0;
    0 0 0 1 0 0;
    0 0 0 0 1 0;
    0 0 0 0 0 1;];

[stat Amat_wrong] = RmatAtoB(bumpcorinds(1),bumppos);
if stat{1}~=1; error(stat{2}); end
Amat = inv(cormat1) * Amat_wrong; %#ok<*MINV>

[stat Bmat_wrong] = RmatAtoB(bumpcorinds(2),bumppos);
if stat{1}~=1; error(stat{2}); end
Bmat = inv(cormat2) * Bmat_wrong;

[stat Cmat_wrong] = RmatAtoB(bumpcorinds(1),bumpcorinds(4));
if stat{1}~=1; error(stat{2}); end
Cmat = inv(cormat1) * Cmat_wrong * inv(cormat4);

[stat Dmat_wrong] = RmatAtoB(bumpcorinds(2),bumpcorinds(4));
if stat{1}~=1; error(stat{2}); end
Dmat = inv(cormat2) * Dmat_wrong * inv(cormat4);

[stat Emat_wrong] = RmatAtoB(bumpcorinds(3),bumpcorinds(4));
if stat{1}~=1; error(stat{2}); end
Emat = inv(cormat3) * Emat_wrong * inv(cormat4);

switch plane
    case 1
        bumpmat = [...
            Amat(1,2) Bmat(1,2) 0         0;
            Amat(2,2) Bmat(2,2) 0         0;
            Cmat(1,2) Dmat(1,2) Emat(1,2) 0;
            Cmat(2,2) Dmat(2,2) Emat(2,2) 1];
        bumpcoefs = bumpmat \ [1; 0; 0; 0];
    case 2
        bumpmat = [...
            Amat(1,2) Bmat(1,2) 0         0;
            Amat(2,2) Bmat(2,2) 0         0;
            Cmat(1,2) Dmat(1,2) Emat(1,2) 0;
            Cmat(2,2) Dmat(2,2) Emat(2,2) 1];
        bumpcoefs = bumpmat \ [0; 1; 0; 0];
    case 3
        bumpmat = [...
            Amat(3,4) Bmat(3,4) 0         0;
            Amat(4,4) Bmat(4,4) 0         0;
            Cmat(3,4) Dmat(3,4) Emat(3,4) 0;
            Cmat(4,4) Dmat(4,4) Emat(4,4) 1];
        bumpcoefs = bumpmat \ [1; 0; 0; 0];
    case 4
        bumpmat = [...
            Amat(3,4) Bmat(3,4) 0         0;
            Amat(4,4) Bmat(4,4) 0         0;
            Cmat(3,4) Dmat(3,4) Emat(3,4) 0;
            Cmat(4,4) Dmat(4,4) Emat(4,4) 1];
        bumpcoefs = bumpmat \ [0; 1; 0; 0];
end


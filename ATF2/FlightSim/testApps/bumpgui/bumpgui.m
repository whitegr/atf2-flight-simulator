function knob = bumpgui(plane,type,targ,cors)
% knob = bumpgui(plane,type,targ,cors)
% plane = # (1:4) (x,x',y,y')
% type = 3 | 4 (# corrector bump)
% targ = Lucretia BEAMLINE element number of bump target
% cors = corrector or quad with mover list (BEAMLINE element number)
%====================
% 11/10/2009 GW
%   Add ability to choose quads on movers in addition to correctors
%
global BEAMLINE GIRDER

% Check the number of input arguments -- should be 0 or 4
if nargin==0 % GUI mode, auto=true
  auto = true;
elseif nargin==4 % CLI mode, auto=false
  auto = false;
else
  error('Incorrect inputs to bumpgui')
end

% If in CLI mode, then check the inputs
if ~auto
  if ~(plane==1 || plane==2 || plane==3 || plane==4)
    error('First input to bumpgui must be 1, 2, 3, or 4')
  end
  if ~(type==3 || type==4)
    error('Second input to bumpgui must be 3 or 4')
  end
  if ~isnumeric(targ) || ~(targ<=length(BEAMLINE))
    error('Third input to bumpgui must be a number less than length(BEAMLINE)')
  end
  if ~isnumeric(cors) || ~(max(cors)<=length(BEAMLINE))
    error(['Fourth input to bumpgui must be an array of numbers, ' ...
      'each less than length(BEAMLINE)'])
  end
  if length(cors)~=type
    error([num2str(type) ' correctors should be specified for a ' ...
      num2str(type) '-corrector bump!'])
  end
  if plane==1 || plane==2
    for cornum=1:type
      if ~strcmpi('xcor',BEAMLINE{cors(cornum)}.Class)  && ~strcmpi('mark',BEAMLINE{cors(cornum)}.Class) && ...
          ~(strcmp(BEAMLINE{cors(cornum)}.Class,'QUAD') && isfield(BEAMLINE{cors(cornum)},'Girder') &&...
          ~isempty(BEAMLINE{cors(cornum)}.Girder) && BEAMLINE{cors(cornum)}.Girder)
        error(['BEAMLINE element #' num2str(cors(cornum)) ...
          ' is not an xcor or quad with mover'])
      end
    end
  elseif plane==3 || plane==4
    for cornum=1:type
      if ~strcmpi('ycor',BEAMLINE{cors(cornum)}.Class) && ~strcmpi('ycor',BEAMLINE{cors(cornum)}.Class) &&...
          ~(strcmp(BEAMLINE{cors(cornum)}.Class,'QUAD') && isfield(BEAMLINE{cors(cornum)},'Girder') &&...
          ~isempty(BEAMLINE{cors(cornum)}.Girder) && BEAMLINE{cors(cornum)}.Girder)
        error(['BEAMLINE element #' num2str(cors(cornum)) ...
          ' is not an ycor or quad with mover'])
      end
    end
  end
  if type==3
    if cors(2)<=cors(1) || cors(3)<=cors(2)
      error('4th input should be an ordered list of 3 BEAMLINE elements')
    end
    if cors(1)>targ || cors(3)<targ
      error('The target must be between the correctors!')
    end
  end
  if type==4
    if cors(2)<=cors(1) || cors(3)<=cors(2) || cors(4)<=cors(3)
      error('4th input should be an ordered list of 4 BEAMLINE elements')
    end
    if cors(2)>targ || cors(3)<targ
      error('The target must be between the 2nd and 3rd elements')
    end
  end
end

% In GUI mode, we need the location of the start/end of the ext and ff
if auto
  ringext = findcells(BEAMLINE,'Name','IEX');
  extff = findcells(BEAMLINE,'Name','BEGFF');
  if isempty(ringext)
    error('Element Name ''IEX'' cannot be found')
  elseif isempty(extff)
    error('Element Name ''BEGFF'' cannot be found')
  end
end

% Set up strings for GUI dialog boxes
if auto
  btypeq = ...
    {'3 Corrector (Position OR Angle bump)',...
    '4 Corrector (Position AND Angle bump)'};
  region = {'EXT','FF'};
end

% 3 or 4 cor bump?
if auto
  [bumptype ok] = listdlg('ListString',btypeq,'SelectionMode','Single',...
    'Name','Bump type','PromptString','Please choose a bump type',...
    'ListSize',[250 100]);
  if ~ok; return; end
else
  if type==3
    bumptype=1;
  elseif type==4
    bumptype=2;
  end
end

% Which region? ext or ff?
if auto
  [roi ok] = listdlg('ListString',region,'SelectionMode','Single',...
    'Name','Region','PromptString','Choose the region for the bump',...
    'ListSize',[200 100]);
  if ~ok; return; end
end

% Define start and end of region of interest
if auto
  switch roi
    case 1 % EXT
      start = ringext;
      stop = extff;
    case 2 % FF
      start = extff;
      stop = length(BEAMLINE);
  end
  elementlist = {BEAMLINE{start:stop}};
  elenamelist = cell(1,length(elementlist));
  for count = 1:length(elementlist)
    elenamelist{count} = elementlist{count}.Name;
  end
end

% x or y bump?  Position or angle?
if auto
  xory = questdlg('Bump in x or y?','x or y','x','y','x');
  switch xory
    case 'x'
      plane = 1;
      corinds = findcells(BEAMLINE,'Class','XCOR');
    case 'y'
      plane = 3;
      corinds = findcells(BEAMLINE,'Class','YCOR');
  end
  if bumptype==1
    posorang = questdlg('Bump in position or angle?',...
      [xory ' or ' xory ''''],xory,[xory ''''],xory);
    switch posorang
      case [xory '''']
        plane = plane+1;
    end
  elseif bumptype==2
    posorang = questdlg('Bump in position or angle?',...
      [xory ' or ' xory ''''],xory,[xory ''''],xory);
    switch posorang
      case [xory '''']
        plane = plane+1;
    end
  end
end

% Choose bump position
% Check availability of cors to open/close the bump
if auto
  closecors = false;
  while closecors==false
    [bumppos ok] = listdlg('ListString',elenamelist,'SelectionMode','Single',...
      'Name','Element','PromptString','Choose the bump position',...
      'ListSize',[160 600]);
    if ~ok; return; end
    bumppos = bumppos + start - 1;
    numclosecors = length(find(corinds>bumppos));
    if numclosecors==0
      closecors=false;
      uiwait(errordlg(['There are no correctors after this point, therefore this bump' ...
        ' cannot be closed.  Please select another point or press ''cancel'' at the' ...
        ' next prompt to cancel bump generation.'],'No closing correctors'));
    else
      closecors=true;
    end
    corinds1 = corinds(corinds>ringext & corinds<bumppos);
    if isempty(corinds1)
      closecors=false;
      uiwait(errordlg(['There are no correctors available to open this bump.' ...
        '  Please select another point or press ''cancel'' at the' ...
        ' next prompt to cancel bump generation.'],'No opening correctors'));
    else
      corlist1 = cell(1,length(corinds1));
      for count = 1:length(corinds1)
        corlist1{count} = BEAMLINE{corinds1(count)}.Name;
      end
    end
  end
else
  bumppos=targ;
end

% Choose opening correctors
if auto
  causality = false;
  while ~causality
    [cor1 ok] = listdlg('ListString',corlist1,'SelectionMode','Single',...
      'Name','First corrector','PromptString','Choose the opening corrector',...
      'ListSize',[200 600]);
    if ~ok; return; end
    if BEAMLINE{corinds1(cor1)}.S >= BEAMLINE{bumppos}.S
      causality = false;
      warnh = warndlg(['First corrector must be upstream of the bump position ' ...
        BEAMLINE{bumppos}.Name],'Corrector location error');
      uiwait(warnh);
    else
      causality = true;
    end
    if bumptype==2
      corinds2 = corinds(corinds>corinds1(cor1) & corinds<bumppos);
      if isempty(corinds2)
        causality=false;
        uiwait(errordlg(['Choosing this corrector results in no available corrector to '...
          'correctly open the bump.  Please select another corrector or press ''cancel'' ' ...
          'at the next prompt to cancel bump generation.'],'No opening correctors'));
      else
        corlist2 = cell(1,length(corinds2));
        for count = 1:length(corinds2)
          corlist2{count} = BEAMLINE{corinds2(count)}.Name;
        end
      end
    end
  end
end

% Choose closing correctors
switch bumptype
  case 1 % 3 corr bump
    
    if auto
      corinds2 = corinds(corinds>corinds1(cor1));
      corlist2 = cell(1,length(corinds2));
      for count = 1:length(corinds2)
        corlist2{count} = BEAMLINE{corinds2(count)}.Name;
      end
    end
    
    if auto
      causality = false;
      while ~causality
        [cor2 ok] = listdlg('ListString',corlist2,'SelectionMode','Single',...
          'Name','Second corrector','PromptString','Choose the 2nd corrector',...
          'ListSize',[200 600]);
        if ~ok; return; end
        if BEAMLINE{corinds2(cor2)}.S <= BEAMLINE{corinds1(cor1)}.S
          causality = false;
          warnh = warndlg(...
            ['Second corrector must be upstream of the first corrector ' ...
            BEAMLINE{cor1}.Name],'Corrector location error');
          uiwait(warnh);
        else
          causality = true;
        end
        corinds3 = corinds(corinds>bumppos & corinds>corinds2(cor2));
        if isempty(corinds3)
          causality=false;
          uiwait(errordlg(['Choosing this corrector results in no available corrector to '...
            'close the bump.  Please select another corrector or press ''cancel'' at the' ...
            ' next prompt to cancel bump generation.'],'No closing correctors'));
        else
          corlist3 = cell(1,length(corinds3));
          for count = 1:length(corinds3)
            corlist3{count} = BEAMLINE{corinds3(count)}.Name;
          end
        end
      end
    end
    
    if auto
      causality = false;
      while ~causality
        [cor3 ok] = listdlg('ListString',corlist3,'SelectionMode','Single',...
          'Name','Third corrector','PromptString','Choose the 3rd corrector',...
          'ListSize',[200 600]);
        if ~ok; return; end
        if (BEAMLINE{corinds3(cor3)}.S <= BEAMLINE{corinds2(cor2)}.S) && ...
            (BEAMLINE{corinds3(cor3)}.S <= BEAMLINE{bumppos}.S)
          causality = false;
          warnh = warndlg(...
            ['Third corrector must be upstream of the second corrector ' ...
            BEAMLINE{cor1}.Name ' and the bump position ' ...
            BEAMLINE{bumppos}.Name],...
            'Corrector location error');
          uiwait(warnh);
        else
          causality = true;
        end
      end
    end
    
  case 2 % 4 corr bump
    
    %         if auto
    %             corinds2 = corinds(corinds>corinds1(cor1) & corinds<bumppos);
    %             corlist2 = cell(1,length(corinds2));
    %             for count = 1:length(corinds2)
    %                 corlist2{count} = BEAMLINE{corinds2(count)}.Name;
    %             end
    %         end
    
    if auto
      causality = false;
      while ~causality
        [cor2 ok] = listdlg('ListString',corlist2,'SelectionMode','Single',...
          'Name','Second corrector','PromptString','Choose the 2nd corrector',...
          'ListSize',[200 600]);
        if ~ok; return; end
        if BEAMLINE{corinds2(cor2)}.S <= BEAMLINE{corinds1(cor1)}.S
          causality = false;
          warnh = warndlg(...
            ['Second corrector must be downstream of the first corrector ' ...
            BEAMLINE{cor1}.Name],'Corrector location error');
          uiwait(warnh);
        else
          causality = true;
        end
      end
    end
    
    if auto
      corinds3 = corinds(corinds>bumppos);
      corlist3 = cell(1,length(corinds3));
      for count = 1:length(corinds3)
        corlist3{count} = BEAMLINE{corinds3(count)}.Name;
      end
    end
    
    if auto
      causality = false;
      while ~causality
        [cor3 ok] = listdlg('ListString',corlist3,'SelectionMode','Single',...
          'Name','Third corrector','PromptString','Choose the 3rd corrector',...
          'ListSize',[200 600]);
        if ~ok; return; end
        if (BEAMLINE{corinds3(cor3)}.S <= BEAMLINE{corinds2(cor2)}.S) && ...
            (BEAMLINE{corinds3(cor3)}.S <= BEAMLINE{bumppos}.S)
          causality = false;
          warnh = warndlg(...
            ['Third corrector must be downstream of the second corrector ' ...
            BEAMLINE{cor1}.Name ' and the bump position ' ...
            BEAMLINE{bumppos}.Name],...
            'Corrector location error');
          uiwait(warnh);
        else
          causality = true;
        end
        corinds4 = corinds(corinds>corinds3(cor3));
        if isempty(corinds4)
          causality=false;
          uiwait(errordlg(['Choosing this corrector results in no available corrector to '...
            'close the bump.  Please select another corrector or press ''cancel'' at the' ...
            ' next prompt to cancel bump generation.'],'No closing correctors'));
        else
          corlist4 = cell(1,length(corinds4));
          for count = 1:length(corinds4)
            corlist4{count} = BEAMLINE{corinds4(count)}.Name;
          end
        end
      end
    end
    
    if auto
      causality = false;
      while ~causality
        [cor4 ok] = listdlg('ListString',corlist4,'SelectionMode','Single',...
          'Name','Fourth corrector','PromptString','Choose the 4th corrector',...
          'ListSize',[200 600]);
        if ~ok; return; end
        if (BEAMLINE{corinds4(cor4)}.S <= BEAMLINE{corinds3(cor3)}.S) && ...
            (BEAMLINE{corinds4(cor4)}.S <= BEAMLINE{bumppos}.S)
          causality = false;
          warnh = warndlg(...
            ['Fourth corrector must be downstream of the third corrector ' ...
            BEAMLINE{cor1}.Name ' and the bump position ' ...
            BEAMLINE{bumppos}.Name],...
            'Corrector location error');
          uiwait(warnh);
        else
          causality = true;
        end
      end
    end
    
end

% Find out if any cors are shared with the ring
switch bumptype
  case 1 % 3 cor bump
    if exist('corinds1','var')
      cors = [corinds1(cor1) corinds2(cor2) corinds3(cor3)];
    end
    
    if ( strcmpi(BEAMLINE{cors(1)}.Name,'ZH100RX') || ...
        strcmpi(BEAMLINE{cors(1)}.Name,'ZH101RX') || ...
        strcmpi(BEAMLINE{cors(1)}.Name,'ZV100RX') || ...
        strcmpi(BEAMLINE{cors(2)}.Name,'ZH100RX') || ...
        strcmpi(BEAMLINE{cors(2)}.Name,'ZH101RX') || ...
        strcmpi(BEAMLINE{cors(2)}.Name,'ZV100RX') || ...
        strcmpi(BEAMLINE{cors(3)}.Name,'ZH100RX') || ...
        strcmpi(BEAMLINE{cors(3)}.Name,'ZH101RX') || ...
        strcmpi(BEAMLINE{cors(3)}.Name,'ZV100RX') )
      bumptype = 3;
    end
    
  case 2 % 4 cor bump
    if exist('corinds1','var')
      cors = [corinds1(cor1) corinds2(cor2) corinds3(cor3) corinds4(cor4)];
    end
    
    if ( strcmpi(BEAMLINE{cors(1)}.Name,'ZH100RX') || ...
        strcmpi(BEAMLINE{cors(1)}.Name,'ZH101RX') || ...
        strcmpi(BEAMLINE{cors(1)}.Name,'ZV100RX') || ...
        strcmpi(BEAMLINE{cors(2)}.Name,'ZH100RX') || ...
        strcmpi(BEAMLINE{cors(2)}.Name,'ZH101RX') || ...
        strcmpi(BEAMLINE{cors(2)}.Name,'ZV100RX') || ...
        strcmpi(BEAMLINE{cors(3)}.Name,'ZH100RX') || ...
        strcmpi(BEAMLINE{cors(3)}.Name,'ZH101RX') || ...
        strcmpi(BEAMLINE{cors(3)}.Name,'ZV100RX') || ...
        strcmpi(BEAMLINE{cors(4)}.Name,'ZH100RX') || ...
        strcmpi(BEAMLINE{cors(4)}.Name,'ZH101RX') || ...
        strcmpi(BEAMLINE{cors(4)}.Name,'ZV100RX') )
      bumptype = 4;
    end
    
end

% NOW DO THE CALCULATIONS!!
switch bumptype
  case 1 % 3 cor bump
    
    % Generate matrices for drifts.  Half the corrector length.
    if auto
      bumpcorinds = [corinds1(cor1) corinds2(cor2) corinds3(cor3)];
    else
      bumpcorinds = cors;
    end
    
    bumpcoefs  = threecorbump(bumpcorinds, bumppos, plane);
    
    if auto
      comment = inputdlg('Please enter a comment describing this knob','Comment');
      if isempty(comment)
        return
      end
    else
      comment{1} = ['auto-generated knob.  Target = element #' num2str(targ) ...
        ' ' num2str(type) '-corrector bump'];
    end
    % Adjust bumps for quads on movers
    for ibump=1:3
      if ~isempty(findstr(BEAMLINE{bumpcorinds(ibump)}.Class,'COR')) ||...
          ~isempty(findstr(BEAMLINE{bumpcorinds(ibump)}.Class,'MARK'))
        name{ibump}=['PS(' num2str(BEAMLINE{bumpcorinds(ibump)}.PS) ').SetPt'];
      else
        [stat R]=RmatAtoB(GIRDER{BEAMLINE{bumpcorinds(ibump)}.Girder}.Element(1),...
          GIRDER{BEAMLINE{bumpcorinds(ibump)}.Girder}.Element(end));
        if plane<3
          moverdim=1;
          bumpcoefs(ibump)=bumpcoefs(ibump)/R(2,1);
        else
          moverdim=2;
          bumpcoefs(ibump)=bumpcoefs(ibump)/R(4,3);
        end
        name{ibump}=['GIRDER{' num2str(BEAMLINE{bumpcorinds(ibump)}.Girder) '}.MoverSetPt(',num2str(moverdim),')'];
      end
    end
    
    [stat knob] = MakeMultiKnob(comment{1},...
      name{1},bumpcoefs(1),name{2},bumpcoefs(2),name{3},bumpcoefs(3));
    if stat{1}~=1; error(stat{2}); end
    
    if auto
      filename = uiputfile(...
        '~/Lucretia/src/Floodland/testApps/bumpgui/savedbumps/*.mat',...
        'Save knob file');
      save(['~/Lucretia/src/Floodland/testApps/bumpgui/savedbumps/' filename],'knob')
    end
    
    return
    
  case 2 % 4 cor bump
    
    if auto
      bumpcorinds = [corinds1(cor1) corinds2(cor2) corinds3(cor3) corinds4(cor4)];
    else
      bumpcorinds = cors;
    end
    
    bumpcoefs = fourcorbump(bumpcorinds, bumppos, plane);
    
    if auto
      comment = inputdlg('Please enter a comment describing this knob','Comment');
    else
      comment{1} = ['auto-generated knob.  Target = element #' num2str(targ) ...
        ' ' num2str(type) '-corrector bump'];
    end
    
    % Adjust bumps for quads on movers
    for ibump=1:4
      if ~isempty(findstr(BEAMLINE{bumpcorinds(ibump)}.Class,'COR')) ||...
          ~isempty(findstr(BEAMLINE{bumpcorinds(ibump)}.Class,'MARK'))
        name{ibump}=['PS(' num2str(BEAMLINE{bumpcorinds(ibump)}.PS) ').SetPt'];
      else
        [stat R]=RmatAtoB(GIRDER{BEAMLINE{bumpcorinds(ibump)}.Girder}.Element(1),...
          GIRDER{BEAMLINE{bumpcorinds(ibump)}.Girder}.Element(end));
        if plane<3
          moverdim=1;
          bumpcoefs(ibump)=bumpcoefs(ibump)/R(2,1);
        else
          moverdim=2;
          bumpcoefs(ibump)=bumpcoefs(ibump)/R(4,3);
        end
        name{ibump}=['GIRDER{' num2str(BEAMLINE{bumpcorinds(ibump)}.Girder) '}.MoverSetPt(',num2str(moverdim),')'];
      end
    end
    
    [stat knob] = MakeMultiKnob(comment{1},name{1},bumpcoefs(1),name{2},bumpcoefs(2),...
      name{3},bumpcoefs(3),name{4},bumpcoefs(4));
    if stat{1}~=1; error(stat{2}); end
    
    if auto
      filename = uiputfile(...
        '~/Lucretia/src/Floodland/testApps/bumpgui/savedbumps/*.mat',...
        'Save knob file');
      save(['~/Lucretia/src/Floodland/testApps/bumpgui/savedbumps/' filename],'knob')
    end
    
    return
    
    
  case 3 % 3 cor bump shared with the ring
    
    % Generate matrices for drifts.  Half the corrector length.
    if auto
      bumpcorinds = [corinds1(cor1) corinds2(cor2) corinds3(cor3)];
    else
      bumpcorinds = cors;
    end
    
    bumpcoefs  = threecorbump(bumpcorinds, bumppos, plane);
    
    % We need to figure out how many cors are shared with the ring and
    % the amount they will kick the beam
    if plane==1 || plane==2
      ZH100RXind = findcells(BEAMLINE,'Name','ZH100RX');
      ZH101RXind = findcells(BEAMLINE,'Name','ZH101RX');
      
      if bumpcorinds(1)==ZH100RXind
        xp_1 = bumpcoefs(1);
      elseif bumpcorinds(2)==ZH100RXind
        xp_1 = bumpcoefs(1);
      elseif bumpcorinds(3)==ZH100RXind
        xp_1 = bumpcoefs(1);
      else
        xp_1 = 0;
      end
      
      if bumpcorinds(1)==ZH101RXind
        xp_2 = bumpcoefs(1);
      elseif bumpcorinds(2)==ZH101RXind
        xp_2 = bumpcoefs(2);
      elseif bumpcorinds(3)==ZH101RXind
        xp_2 = bumpcoefs(3);
      else
        xp_2 = 0;
      end
      
      ZH100Rind = findcells(BEAMLINE,'Name','ZH100R');
      ZH101Rind = findcells(BEAMLINE,'Name','ZH101R');
      xcorinds = findcells(BEAMLINE,'Class','XCOR');
      ringcor(1) = xcorinds( find(xcorinds==ZH101Rind) + 1 );
      ringcor(2) = xcorinds( find(xcorinds==ZH101Rind) + 2 );
      
      [stat R1toE] = RmatAtoB(ZH100Rind,ringcor(2));
      [stat R2toE] = RmatAtoB(ZH101Rind,ringcor(2));
      [stat R3toE] = RmatAtoB(ringcor(1),ringcor(2));
      
      x_Eorig = R1toE(1,2)*xp_1 + R2toE(1,2)*xp_2;
      xp_3 = -(x_Eorig / R3toE(1,2));
      
      xp_4 = -( R1toE(2,2)*xp_1 + R2toE(2,2)*xp_2 + R3toE(2,2)*xp_3 );
      
      rp_3 = xp_3;
      rp_4 = xp_4;
      
    elseif plane==3 || plane==4
      ZV100RXind = findcells(BEAMLINE,'Name','ZV100RX');
      
      if bumpcorinds(1)==ZV100RXind
        yp_1 = bumpcoefs(1);
      elseif bumpcorinds(2)==ZV100RXind
        yp_1 = bumpcoefs(1);
      elseif bumpcorinds(3)==ZV100RXind
        yp_1 = bumpcoefs(1);
      else
        yp_1 = 0;
      end
      
      yp_2 = 0; % There is only 1 shared ycor
      
      ZV100Rind = findcells(BEAMLINE,'Name','ZV100R');
      ycorinds = findcells(BEAMLINE,'Class','YCOR');
      ringcor(1) = ycorinds( find(ycorinds==ZV100Rind) + 1 );
      ringcor(2) = ycorinds( find(ycorinds==ZV100Rind) + 2 );
      
      [stat R1toE] = RmatAtoB(ZV100Rind,ringcor(2));
      R2toE = zeros(6); % There is only 1 shared ycor
      [stat R3toE] = RmatAtoB(ringcor(1),ringcor(2));
      
      y_Eorig = R1toE(3,4)*yp_1 + R2toE(3,4)*yp_2;
      yp_3 = -(y_Eorig / R3toE(3,4));
      
      yp_4 = -( R1toE(4,4)*yp_1 + R2toE(4,4)*yp_2 + R3toE(4,4)*yp_3 );
      
      rp_3 = yp_3;
      rp_4 = yp_4;
      
    else
      error('How the hell did you get here?')
    end
    
    if auto
      comment = inputdlg('Please enter a comment describing this knob','Comment');
      if isempty(comment)
        return
      end
    else
      comment{1} = ['auto-generated knob.  Target = element #' num2str(targ) ...
        ' ' num2str(type) '-corrector bump'];
    end
    
    % Adjust bumps for quads on movers
    for ibump=1:3
      if ~isempty(findstr(BEAMLINE{bumpcorinds(ibump)}.Class,'COR')) ||...
          ~isempty(findstr(BEAMLINE{bumpcorinds(ibump)}.Class,'MARK'))
        name{ibump}=['PS(' num2str(BEAMLINE{bumpcorinds(ibump)}.PS) ').SetPt'];
      else
        [stat R]=RmatAtoB(GIRDER{BEAMLINE{bumpcorinds(ibump)}.Girder}.Element(1),...
          GIRDER{BEAMLINE{bumpcorinds(ibump)}.Girder}.Element(end));
        if plane<3
          moverdim=1;
          bumpcoefs(ibump)=bumpcoefs(ibump)/R(2,1);
        else
          moverdim=2;
          bumpcoefs(ibump)=bumpcoefs(ibump)/R(4,3);
        end
        name{ibump}=['GIRDER{' num2str(BEAMLINE{bumpcorinds(ibump)}.Girder) '}.MoverSetPt(',num2str(moverdim),')'];
      end
    end
    
    [stat knob] = MakeMultiKnob(comment{1},...
      name{1},bumpcoefs(1),name{2},bumpcoefs(2),name{3},bumpcoefs(3),...
      ['PS(' num2str(BEAMLINE{ringcor(1)}.PS) ').SetPt'],rp_3,...
      ['PS(' num2str(BEAMLINE{ringcor(2)}.PS) ').SetPt'],rp_4);
    if stat{1}~=1; error(stat{2}); end
    
    if auto
      filename = uiputfile(...
        '~/Lucretia/src/Floodland/testApps/bumpgui/savedbumps/*.mat',...
        'Save knob file');
      save(['~/Lucretia/src/Floodland/testApps/bumpgui/savedbumps/' filename],'knob')
    end
    
    return
    
  case 4 % 4 cor bump shared with the ring
    
    if auto
      bumpcorinds = [corinds1(cor1) corinds2(cor2) corinds3(cor3) corinds4(cor4)];
    else
      bumpcorinds = cors;
    end
    
    bumpcoefs = fourcorbump(bumpcorinds, bumppos, plane);
    
    % We need to figure out how many cors are shared with the ring and
    % the amount they will kick the beam
    if plane==1 || plane==2
      ZH100RXind = findcells(BEAMLINE,'Name','ZH100RX');
      ZH101RXind = findcells(BEAMLINE,'Name','ZH101RX');
      
      if bumpcorinds(1)==ZH100RXind
        xp_1 = bumpcoefs(1);
      elseif bumpcorinds(2)==ZH100RXind
        xp_1 = bumpcoefs(1);
      elseif bumpcorinds(3)==ZH100RXind
        xp_1 = bumpcoefs(1);
      else
        xp_1 = 0;
      end
      
      if bumpcorinds(1)==ZH101RXind
        xp_2 = bumpcoefs(1);
      elseif bumpcorinds(2)==ZH101RXind
        xp_2 = bumpcoefs(2);
      elseif bumpcorinds(3)==ZH101RXind
        xp_2 = bumpcoefs(3);
      else
        xp_2 = 0;
      end
      
      ZH100Rind = findcells(BEAMLINE,'Name','ZH100R');
      ZH101Rind = findcells(BEAMLINE,'Name','ZH101R');
      xcorinds = findcells(BEAMLINE,'Class','XCOR');
      ringcor(1) = xcorinds( find(xcorinds==ZH101Rind) + 1 );
      ringcor(2) = xcorinds( find(xcorinds==ZH101Rind) + 2 );
      
      [stat R1toE] = RmatAtoB(ZH100Rind,ringcor(2));
      [stat R2toE] = RmatAtoB(ZH101Rind,ringcor(2));
      [stat R3toE] = RmatAtoB(ringcor(1),ringcor(2));
      
      x_Eorig = R1toE(1,2)*xp_1 + R2toE(1,2)*xp_2;
      xp_3 = -(x_Eorig / R3toE(1,2));
      
      xp_4 = -( R1toE(2,2)*xp_1 + R2toE(2,2)*xp_2 + R3toE(2,2)*xp_3 );
      
      rp_3 = xp_3;
      rp_4 = xp_4;
      
    elseif plane==3 || plane==4
      ZV100RXind = findcells(BEAMLINE,'Name','ZV100RX');
      
      if bumpcorinds(1)==ZV100RXind
        yp_1 = bumpcoefs(1);
      elseif bumpcorinds(2)==ZV100RXind
        yp_1 = bumpcoefs(1);
      elseif bumpcorinds(3)==ZV100RXind
        yp_1 = bumpcoefs(1);
      else
        yp_1 = 0;
      end
      
      yp_2 = 0; % There is only 1 shared ycor
      
      ZV100Rind = findcells(BEAMLINE,'Name','ZV100R');
      ycorinds = findcells(BEAMLINE,'Class','YCOR');
      ringcor(1) = ycorinds( find(ycorinds==ZV100Rind) + 1 );
      ringcor(2) = ycorinds( find(ycorinds==ZV100Rind) + 2 );
      
      [stat R1toE] = RmatAtoB(ZV100Rind,ringcor(2));
      R2toE = zeros(6); % There is only 1 shared ycor
      [stat R3toE] = RmatAtoB(ringcor(1),ringcor(2));
      
      y_Eorig = R1toE(3,4)*yp_1 + R2toE(3,4)*yp_2;
      yp_3 = -(y_Eorig / R3toE(3,4));
      
      yp_4 = -( R1toE(4,4)*yp_1 + R2toE(4,4)*yp_2 + R3toE(4,4)*yp_3 );
      
      rp_3 = yp_3;
      rp_4 = yp_4;
      
    else
      error('How the hell did you get here?')
    end
    
    if auto
      comment = inputdlg('Please enter a comment describing this knob','Comment');
      if isempty(comment)
        return
      end
    else
      comment{1} = ['auto-generated knob.  Target = element #' num2str(targ) ...
        ' ' num2str(type) '-corrector bump'];
    end
    % Adjust bumps for quads on movers
    for ibump=1:4
      if ~isempty(findstr(BEAMLINE{bumpcorinds(ibump)}.Class,'COR')) || ...
          ~isempty(findstr(BEAMLINE{bumpcorinds(ibump)}.Class,'MARK'))
        name{ibump}=['PS(' num2str(BEAMLINE{bumpcorinds(ibump)}.PS) ').SetPt'];
      else
        [stat R]=RmatAtoB(GIRDER{BEAMLINE{bumpcorinds(ibump)}.Girder}.Element(1),...
          GIRDER{BEAMLINE{bumpcorinds(ibump)}.Girder}.Element(end));
        if plane<3
          moverdim=1;
          bumpcoefs(ibump)=bumpcoefs(ibump)/R(2,1);
        else
          moverdim=2;
          bumpcoefs(ibump)=bumpcoefs(ibump)/R(4,3);
        end
        name{ibump}=['GIRDER{' num2str(BEAMLINE{bumpcorinds(ibump)}.Girder) '}.MoverSetPt(',num2str(moverdim),')'];
      end
    end
    [stat knob] = MakeMultiKnob(comment{1},...
      name{1},bumpcoefs(1),...
      name{2},bumpcoefs(2),...
      name{3},bumpcoefs(3),...
      name{4},bumpcoefs(4),...
      ['PS(' num2str(BEAMLINE{ringcor(1)}.PS) ').SetPt'],rp_3,...
      ['PS(' num2str(BEAMLINE{ringcor(2)}.PS) ').SetPt'],rp_4);
    if stat{1}~=1; error(stat{2}); end
    
    if auto
      filename = uiputfile(...
        '~/Lucretia/src/Floodland/testApps/bumpgui/savedbumps/*.mat',...
        'Save knob file');
      save(['~/Lucretia/src/Floodland/testApps/bumpgui/savedbumps/' filename],'knob')
    end
    
    return
end

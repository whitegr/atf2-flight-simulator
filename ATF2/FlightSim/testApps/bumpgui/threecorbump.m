function bumpcoefs = threecorbump(bumpcorinds, bumppos, bumptype)

% calculate coefficients for 3-corrector bump
% NOTE - bumppos must be between corrector 1 and 2

global BEAMLINE

% correctors aren't split in half ... use a half-corrector-length drift to get
% (approximately) to each corrector's center
L=BEAMLINE{bumpcorinds(1)}.L; % corrector 1 length
cormat1=eye(6);
if ~isempty(strfind('COR',BEAMLINE{bumpcorinds(1)}.Class))
  cormat1(1,2)=L/2;cormat1(3,4)=L/2; % half-length drift
end
L=BEAMLINE{bumpcorinds(2)}.L; % corrector 2 length
cormat2=eye(6);
if ~isempty(strfind('COR',BEAMLINE{bumpcorinds(2)}.Class))
  cormat2(1,2)=L/2;cormat2(3,4)=L/2; % half-length drift
end
L=BEAMLINE{bumpcorinds(3)}.L; % corrector 3 length
cormat3=eye(6);
% Don't do this for QUADS
if ~isempty(strfind('COR',BEAMLINE{bumpcorinds(3)}.Class))
  cormat3(1,2)=L/2;cormat3(3,4)=L/2; % half-length drift
end

% corrector 1 to bumppos
[stat,Amat]=RmatAtoB(bumpcorinds(1)+1,bumppos); % corrector 1 exit to bumppos
if (stat{1}~=1),error(stat{2}),end
Amat=Amat*cormat1; % corrector 1 center to bumppos

% corrector 1 to corrector 3
[stat,Bmat]=RmatAtoB(bumpcorinds(1)+1,bumpcorinds(3)-1); % corrector 1 exit to corrector 3 entrance
if (stat{1}~=1),error(stat{2}),end
Bmat=cormat3*Bmat*cormat1; % corrector 1 center to corrector 3 center

% corrector 2 to corrector 3
[stat,Cmat]=RmatAtoB(bumpcorinds(2)+1,bumpcorinds(3)-1); % corrector 2 exit to corrector 3 entrance
if (stat{1}~=1),error(stat{2}),end
Cmat=cormat3*Cmat*cormat2; % corrector 2 center to corrector 3 center

switch bumptype
  case 1
    bumpmat = [...
      Amat(1,2) 0         0;
      Bmat(1,2) Cmat(1,2) 0;
      Bmat(2,2) Cmat(2,2) 1];
  case 2
    bumpmat = [...
      Amat(2,2) 0         0;
      Bmat(1,2) Cmat(1,2) 0;
      Bmat(2,2) Cmat(2,2) 1];
  case 3
    bumpmat = [...
      Amat(3,4) 0         0;
      Bmat(3,4) Cmat(3,4) 0;
      Bmat(4,4) Cmat(4,4) 1];
  case 4
    bumpmat = [...
      Amat(4,4) 0         0;
      Bmat(3,4) Cmat(3,4) 0;
      Bmat(4,4) Cmat(4,4) 1];
end

bumpcoefs = bumpmat \ [1; 0; 0];
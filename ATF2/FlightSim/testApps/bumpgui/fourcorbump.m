function bumpcoefs=fourcorbump(bumpcorinds,bumppos,bumptype)

% NOTE: assumes bumpcorinds(1:2)<bumppos & bumpcorinds(3:4)>bumppos; also
%       assumes that BEAMLINE{bumppos}.L=0

global BEAMLINE

% correctors aren't split in half ... use a half-corrector-length drift to get
% (approximately) to each corrector's center
L=BEAMLINE{bumpcorinds(1)}.L; % corrector 1 length
cormat1=eye(6);
if ~isempty(findstr(BEAMLINE{bumpcorinds(1)}.Class,'COR'))
  cormat1(1,2)=L/2;cormat1(3,4)=L/2; % half-length drift
end
L=BEAMLINE{bumpcorinds(2)}.L; % corrector 2 length
cormat2=eye(6);
if ~isempty(findstr(BEAMLINE{bumpcorinds(2)}.Class,'COR'))
  cormat2(1,2)=L/2;cormat2(3,4)=L/2; % half-length drift
end
L=BEAMLINE{bumpcorinds(3)}.L; % corrector 3 length
cormat3=eye(6);
% Don't do this for QUADS
if ~isempty(findstr(BEAMLINE{bumpcorinds(3)}.Class,'COR'))
  cormat3(1,2)=L/2;cormat3(3,4)=L/2; % half-length drift
end
L=BEAMLINE{bumpcorinds(4)}.L; % corrector 4 length
cormat4=eye(6);
% Don't do this for QUADS
if ~isempty(findstr(BEAMLINE{bumpcorinds(4)}.Class,'COR'))
  cormat4(1,2)=L/2;cormat4(3,4)=L/2; % half-length drift
end

% corrector 1 to bumppos
[stat,Amat]=RmatAtoB(bumpcorinds(1)+1,bumppos); % corrector 1 exit to bumppos
if (stat{1}~=1),error(stat{2}),end
Amat=Amat*cormat1; % corrector 1 center to bumppos

% corrector 2 to bumppos
[stat,Bmat]=RmatAtoB(bumpcorinds(2)+1,bumppos); % corrector 2 exit to bumppos
if (stat{1}~=1),error(stat{2}),end
Bmat=Bmat*cormat2; % corrector 2 center to bumppos

% corrector 1 to corrector 4
[stat,Cmat]=RmatAtoB(bumpcorinds(1)+1,bumpcorinds(4)-1); % corrector 1 exit to corrector 4 entrance
if (stat{1}~=1),error(stat{2}),end
Cmat=cormat4*Cmat*cormat1; % corrector 1 center to corrector 4 center

% corrector 2 to corrector 4
[stat,Dmat]=RmatAtoB(bumpcorinds(2)+1,bumpcorinds(4)-1); % corrector 2 exit to corrector 4 entrance
if (stat{1}~=1),error(stat{2}),end
Dmat=cormat4*Dmat*cormat2; % corrector 2 center to corrector 4 center

% corrector 3 to corrector 4
[stat,Emat]=RmatAtoB(bumpcorinds(3)+1,bumpcorinds(4)-1); % corrector 3 exit to corrector 4 entrance
if (stat{1}~=1),error(stat{2}),end
Emat=cormat4*Emat*cormat3; % corrector 3 center to corrector 4 center

% X and Y response matrices
Rx=[ ...
  Amat(1,2),Bmat(1,2),0        ,0; ...
  Amat(2,2),Bmat(2,2),0        ,0; ...
  Cmat(1,2),Dmat(1,2),Emat(1,2),0; ...
  Cmat(2,2),Dmat(2,2),Emat(2,2),1];
Ry=[ ...
  Amat(3,4),Bmat(3,4),0        ,0; ...
  Amat(4,4),Bmat(4,4),0        ,0; ...
  Cmat(3,4),Dmat(3,4),Emat(3,4),0; ...
  Cmat(4,4),Dmat(4,4),Emat(4,4),1];

% solve for bump coefficients
switch bumptype
  case 1 % X bump
    bumpcoefs=Rx\[1;0;0;0];
  case 2 % X' bump
    bumpcoefs=Rx\[0;1;0;0];
  case 3 % Y bump
    bumpcoefs=Ry\[1;0;0;0];
  case 4 % Y' bump
    bumpcoefs=Ry\[0;1;0;0];
end

end

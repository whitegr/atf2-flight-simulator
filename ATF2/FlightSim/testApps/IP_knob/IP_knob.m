function varargout = IP_knob(varargin)
% IP_KNOB M-file for IP_knob.fig
%      IP_KNOB, by itself, creates a new IP_KNOB or raises the existing
%      singleton*.
%
%      H = IP_KNOB returns the handle to a new IP_KNOB or the handle to
%      the existing singleton*.
%
%      IP_KNOB('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in IP_KNOB.M with the given input arguments.edit55
%
%      IP_KNOB('Property','Value',...) creates a new IP_KNOB or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before IP_knob_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to IP_knob_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help IP_knob

% Last Modified by GUIDE v2.5 15-Dec-2010 09:38:20

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @IP_knob_OpeningFcn, ...
                   'gui_OutputFcn',  @IP_knob_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end
if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before IP_knob is made visible.
function IP_knob_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to IP_knob (see VARARGIN)

% Choose default command line output for IP_knob
handles.output = hObject;
% Update handles structure
guidata(hObject, handles);
% UIWAIT makes IP_knob wait for user response (see UIRESUME)
% uiwait(handles.figure1);

global BEAMLINE PS GIRDER FL IP_KNOBV;

%% for i=1:length(length(FL.HwInfo.GIRDER))
%%     FlSetMoverLimits(i);
%% end

FlSetMoverLimits(50);FlSetMoverLimits(51);
FlSetMoverLimits(52);FlSetMoverLimits(53);
FlSetMoverLimits(54);

FF_MAD_ini;

load ./testApps/IP_knob/KNOB_VALUE;
%% load ./testApps/IP_knob/KNOB_VALUE_FLT;
tname = cellfun(@(x) x.Name, IP_KNOBV, 'UniformOutput',false);
set(handles.popupmenu1, 'String', tname);

%% QuadName = { 'QD6FF'; 'QF5BFF'; 'QF5AFF'};
%% %% QuadName = { 'QD6FF'; 'QF5BFF'; 'QF5AFF'; 'QD4BFF'; 'QD4AFF'; 'QF3FF';...
%% %%     'QD2BFF'; 'QD2AFF'; 'QF1FF'; 'QD0FF'};
%% SextName = {'SF6FF'; 'SF5FF'; 'SD4FF'; 'SF1FF'; 'SD0FF'};
handles.MagnetName = {'QD6FF'; 'QF5BFF'; 'QF5AFF'; 'QD4BFF'; 'QD4AFF'; 'QF3FF';...
              'QD2BFF'; 'QD2AFF'; 'QF1FF'; 'QD0FF';...
              'SF6FF'; 'SF5FF'; 'SD4FF'; 'SF1FF'; 'SD0FF'};
handles.Mag_ps = zeros(1, length(handles.MagnetName));
handles.Mag_Gir = zeros(1, length(handles.MagnetName));
handles.ps_value_ini = zeros(1,length(handles.MagnetName));
handles.mover_value_ini = zeros(length(handles.MagnetName),3);

for i = 1:length(handles.MagnetName)
    ind = findcells(BEAMLINE,'Name',handles.MagnetName{i});
    handles.Mag_ps(i) = BEAMLINE{ind(1)}.PS;
    handles.Mag_Gir(i) = BEAMLINE{ind(1)}.Girder;
    handles.ps_value_ini(i) = PS(handles.Mag_ps(i)).SetPt ;
    handles.mover_value_ini(i,:)=GIRDER{handles.Mag_Gir(i)}.MoverSetPt;
%%  ps_value_ini(i) = PS(Mag_ps(i)).Ampl ;
%%  mover_ini(i,:)=GIRDER{Mag_Gir(i)}.MoverPos;
end

request={[] handles.Mag_ps []};
        [stat handles.respMag] = AccessRequest(request);
        if(stat{1}==-1)
            display(sprintf('AccessRequest returned error :%s',stat{2}));
        else
            display(sprintf('Access granted, ID of request : %s',handles.respMag));
        end

request={handles.Mag_Gir []  []};
        [stat handles.resmMag] = AccessRequest(request);
        if(stat{1}==-1)
            display(sprintf('AccessRequest returned error :%s',stat{2}));
        else
            display(sprintf('Access granted, ID of request : %s',handles.resmMag));
        end
%% handles.MagnetName = MagnetName;
%% handles.Mag_ps = Mag_ps;    %% all componets 
%% handles.Mag_Gir = Mag_Gir ;
%% handles.ps_value_ini = ps_value_ini;
%% handles.mover_value_ini = mover_value_ini;
%% %% handles.ps_value = ps_value_ini;
%% %% handles.mover_value = mover_value_ini; 
%% handles.respMag = respMag; 
%% handles.resmMag = resmMag;
guidata(hObject,handles);
IP_ind = findcells(BEAMLINE,'Name','IP'); %% 1717 not equal FL.SimModel.ip_ind =1735, %% 1703 not equal FL.SimModel.ip_ind = 1721
handles.IP_ind = IP_ind;
%% [stat,beamout] = TrackThru( 1, handles.IP_ind, FL.SimBeam{1}, 1, 1, 0 );
[data txt beamout] = GetIPSimData(FL.SimBeam{1},FL.SimBeam{2},1, handles.IP_ind);
[stat,Twissd]=GetTwiss(1,length(BEAMLINE), ...
  FL.SimModel.Initial.x.Twiss,FL.SimModel.Initial.y.Twiss);
%% [ nx,xout] = hist(beamout.Bunch.x(1,:),100);
%% [ ny,yout] = hist(beamout.Bunch.x(3,:),100);
plot(handles.axes6, beamout.Bunch.x(1,:)*1e6, beamout.Bunch.x(3,:)*1e6,'b.');
%% data.xwaist = Twissd.betax(handles.IP_ind)*Twissd.alphax(handles.IP_ind)/...
%%     (1+Twissd.alphax(handles.IP_ind)^2);
%% data.xdisp = Twissd.etax(handles.IP_ind);

%% data.ywaist = Twissd.betay(handles.IP_ind)*Twissd.alphay(handles.IP_ind)/...
%%    (1+Twissd.alphay(handles.IP_ind)^2);
%% data.ydisp = Twissd.etay(handles.IP_ind);

set(handles.edit4,'String',num2str(data.xwaist,'%5.2e'));
set(handles.edit10,'String',num2str(data.xdisp,'%5.2e'));
set(handles.edit5,'String',num2str(data.xsize,'%5.2e'));
set(handles.edit6,'String',num2str(data.emit_x,'%5.2e'));
set(handles.edit7,'String',num2str(data.ywaist,'%5.2e'));
set(handles.edit11,'String',num2str(data.ydisp,'%5.2e'));
set(handles.edit8,'String',num2str(data.ysize,'%5.2e'));
set(handles.edit9,'String',num2str(data.emit_y,'%5.2e'));

bar(handles.axes10, handles.ps_value_ini, 0.8)
set(handles.axes10,'XTickLabel',handles.MagnetName)
set(handles.axes10,'xlim',[0.5 length(handles.ps_value_ini)+0.5])
%% axes(handles.axes11)
bar(handles.axes11, handles.mover_value_ini(:,1), 0.8)
set(handles.axes11,'XTickLabel',handles.MagnetName);
set(handles.axes11,'xlim',[0.5 length(handles.mover_value_ini)+0.5])
%% axes(handles.axes12)
bar(handles.axes12, handles.mover_value_ini(:,2), 0.8)
set(handles.axes12,'XTickLabel',handles.MagnetName)
set(handles.axes12,'xlim',[0.5 length(handles.mover_value_ini)+0.5]);
for ii = 12:44
    eval(['set(handles.edit',num2str(ii),',''String'',''0'')']);
end

handles.data(1)= data.xwaist;
handles.data(2)= data.xdisp;
handles.data(3)= data.xsize;
handles.data(4)= data.emit_x;

handles.data(5)= data.ywaist;
handles.data(6)= data.ydisp;
handles.data(7)= data.ysize;
handles.data(8)= data.emit_y;

for i=1:length(IP_KNOBV)
    handles.Knob_v(i) = 0;  
end
 handles.Knob_his(1,1) = 1;  
 handles.Knob_his(1,2) = 0;  
%% val8 = get(handles.popupmenu8,'Value');
%% handles.plot = get(handles.popupmenu8,'Value');
guidata(hObject,handles);

% --- Outputs from this function are returned to the command line.
function varargout = IP_knob_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on selection change in popupmenu1.
function popupmenu1_Callback(hObject, eventdata, handles)
% hObject    handle to popupmenu1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns popupmenu1 contents as cell array
%        contents{get(hObject,'Value')} returns selected item from popupmenu1
global BEAMLINE FL PS GIRDER IP_KNOBV;
str = get(handles.popupmenu1, 'String');
val = get(handles.popupmenu1, 'Value');

mover_U = zeros(length(IP_KNOBV{val}.Ind(:,2)),3);
mover_L = zeros(length(IP_KNOBV{val}.Ind(:,2)),3);

%% set all mover up-low bound in FL.HwInfo.GIRDER.high low 
for i=1:length(FL.HwInfo.GIRDER)
    FL.HwInfo.GIRDER(i).high = FL.HwInfo.GIRDER(50).high ;
    FL.HwInfo.GIRDER(i).low = FL.HwInfo.GIRDER(50).low ;
end

mover_U = cat(1, FL.HwInfo.GIRDER(IP_KNOBV{val}.Ind(:,2)).high);
mover_L = cat(1, FL.HwInfo.GIRDER(IP_KNOBV{val}.Ind(:,2)).low);
dps_p = cat(1,FL.HwInfo.PS(IP_KNOBV{val}.Ind(:,1)).high) - cat(1,PS(IP_KNOBV{val}.Ind(:,1)).SetPt) ;
dps_n = cat(1,PS(IP_KNOBV{val}.Ind(:,1)).SetPt)- cat(1,FL.HwInfo.PS(IP_KNOBV{val}.Ind(:,1)).low) ;
      set(handles.edit12,'String',num2str(IP_KNOBV{val}.Unit,'%5.2e'));      
      us = dps_p./IP_KNOBV{val}.Ratio(:,1);
      ls = dps_n./IP_KNOBV{val}.Ratio(:,1);  
      ux = mover_U(:,1)./IP_KNOBV{val}.Ratio(:,2);
      lx = mover_L(:,1)./IP_KNOBV{val}.Ratio(:,2);
      uy = mover_U(:,2)./IP_KNOBV{val}.Ratio(:,3);
      ly = mover_L(:,2)./IP_KNOBV{val}.Ratio(:,3);
      ur = mover_U(:,3)./IP_KNOBV{val}.Ratio(:,4);
      lr = mover_L(:,3)./IP_KNOBV{val}.Ratio(:,4);
      
   set(handles.edit56,'String', num2str(floor(min([min(us(us>0)), -max(ls(ls<0)),...
                                                  min(ux(ux>0)), -max(lx(lx<0)),...
                                                  min(uy(uy>0)), -max(ly(ly<0)),...
                                                  min(ur(ur>0)), -max(lr(lr<0))]) ),'%d')); 
    
   set(handles.edit58,'String', num2str(-floor(min([-max(us(us<0)),min(ls(ls>0)),...
                                                   -max(ux(ux<0)),min(lx(lx>0)),...
                                                   -max(uy(uy<0)),min(ly(ly>0)),...
                                                   -max(ur(ur<0)),min(lr(lr>0))]) ),'%d'));                                                  

% --- Executes during object creation, after setting all properties.
function popupmenu1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to popupmenu1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% --- Executes on selection change in popupmenu4.
function popupmenu4_Callback(hObject, eventdata, handles)
% hObject    handle to popupmenu4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns popupmenu4 contents as cell array
%        contents{get(hObject,'Value')} returns selected item from popupmenu4


% --- Executes during object creation, after setting all properties.
function popupmenu4_CreateFcn(hObject, eventdata, handles)
% hObject    handle to popupmenu4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% --- Executes on selection change in popupmenu5.
function popupmenu5_Callback(hObject, eventdata, handles)
% hObject    handle to popupmenu5 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns popupmenu5 contents as cell array
%        contents{get(hObject,'Value')} returns selected item from popupmenu5

% --- Executes during object creation, after setting all properties.
function popupmenu5_CreateFcn(hObject, eventdata, handles)
% hObject    handle to popupmenu5 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% --- Executes on button press in pushbutton4.
function pushbutton4_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
resp=questdlg('Are you sure?','Exit Request','Yes','No','No');
if ~strcmp(resp,'Yes'); return; end;
try  
  guiCloseFn('IP_knob',handles);
  [stat resp] = AccessRequest('release',handles.respMag);
  [stat resp] = AccessRequest('release',handles.resmMag);
catch
 %% exit
end

% --- Executes on button press in pushbutton6.
function pushbutton6_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton6 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global BEAMLINE IP_KNOBV GIRDER PS;

str = get(handles.popupmenu1, 'String');
val = get(handles.popupmenu1, 'Value');

handles.hTableFigure = figure(...       % The main GUI figure
                    'Name', IP_KNOBV{val}.Name, ...
                    'MenuBar','none', ...
                    'Toolbar','none', ...
                    'Position',[600 400 780 400],...
                    'NumberTitle', 'off',... % Do not show figure number
                    'HandleVisibility','callback', ...
                    'Color', get(0,...
                             'defaultuicontrolbackgroundcolor')); 
handles.hExitButton = uicontrol(... % Button for quitting selected knob
                   'Parent', handles.hTableFigure, ...
                   'Units','normalized',...
                   'HandleVisibility', 'callback',...
                   'Position',[0.025 0.9 0.15 0.07],...
                   'BackgroundColor', [0.961 0.922 0.922],...
                   'FontSize',14,...
                   'String','Exit',...
                   'Style','pushbutton',...
                   'BackgroundColor', 'r' ); 
               uicontrol(... % Knob type 
                   'Parent', handles.hTableFigure, ...
                   'Units','normalized','callback',...
                   'HandleVisibility', ...
                   'Position',[0.185 0.9 0.18 0.07],...
                   'BackgroundColor', [0.855 0.702 1],...
                   'FontSize',14,...
                   'String','Knob Type', ...
                   'Style','Text'); 
handles.hKTpopmenu = uicontrol(... % Knob type 
                   'Parent', handles.hTableFigure, ...
                   'Units','normalized',...
                   'HandleVisibility', 'callback',...
                   'Position',[0.375 0.9 0.125 0.07],...
                   'BackgroundColor', [0.961 0.922 0.922],...
                   'FontSize',14,...
                   'String',{'Mover Knob' 'Strength Knob'  'Both'}, ...
                   'Style','popupmenu');  
               uicontrol(... % Knob type 
                   'Parent', handles.hTableFigure, ...
                   'Units','normalized',...
                   'HandleVisibility', 'callback',...
                   'Position',[0.525 0.9 0.125 0.07],...
                   'BackgroundColor', [0.855 0.702 1],...
                   'FontSize',14,...
                   'String','Step', ...
                   'Style','Text');  
handles.hStepedit = uicontrol(... % Knob type 
                   'Parent', handles.hTableFigure, ...
                   'Units','normalized',...
                   'HandleVisibility', 'callback',...
                   'Position',[0.675 0.9 0.125 0.07],...
                   'BackgroundColor', [0.961 0.922 0.922],...
                   'FontSize',14,...
                   'String','10', ...
                   'Style','edit');  
handles.hSetButton = uicontrol(... % Button for quitting selected knob
                   'Parent', handles.hTableFigure, ...
                   'BackgroundColor', 'g', ...
                   'Units','normalized',...
                   'HandleVisibility', 'callback',...
                   'Position',[0.825 0.9 0.15 0.07],...
                   'BackgroundColor', [0.961 0.922 0.922],...
                   'FontSize',14,...
                   'String','Set',...
                   'Style','pushbutton');               
% set([handles.hExitButton,handles.hKTpopmenu,handles.hStepedit,handles.hSetButton],...
%%     {'callback'},{{@hExitButton_Callback,handles};{@hKTpopmenu_Callback,handles};...
%%    {@hStepedit_Callback,handles};{@hSetButton_Callback,handles}}) % Set callbacks.  
l_knob = length(IP_KNOBV{val}.Ind(:,1));
for i = 1:l_knob
    handles.knobmname{i} = BEAMLINE{PS(IP_KNOBV{val}.Ind(i,1)).Element(1)}.Name;
    handles.kbps_value(i) = PS(IP_KNOBV{val}.Ind(i,1)).SetPt ;
    handles.kbmover_value(i,:) = GIRDER{IP_KNOBV{val}.Ind(i,2)}.MoverSetPt;
end
if (length(handles.knobmname) > l_knob)
    handles.knobmname(l_knob+1:end)=[];
    handles.kbps_value(l_knob+1:end) = [] ;
    handles.kbmover_value((l_knob+1:end),:)= [];
end
%% tname = cellfun(@(x) x.Name, BEAMLINE, 'UniformOutput',false);
[c, handles.ia, ib]  = intersect(handles.MagnetName, handles.knobmname);
%% handles.kbps_value_ini = handles.ps_value_ini(handles.ia);
%% handles.kbmover_value_ini = handles.mover_value_ini(ia);

% Create a figure that will have a uitable or axes or checkboxes

%% pknob = uipanel('Parent',hTableFigure,'Title',str{val},'FontSize',12,...
%%         'BackgroundColor','white','Position',[100, 100, 600, 800]);
%% tknob = uitable('Parent',pknob,'Data',IP_KNOBV{val}.Ratio,'ColumnName',{'strength','x-mover','y-mover','rotation'},... 
%% 'RowName',knobmname,'Position',[0.1 .78 .8 .2]);

% Define parameters for a uitable (col headers are fictional)
colnames = {'Strenth', 'xmover', 'ymover', 'rotation'};
% All column contain numeric data (integers, actually)
colfmt = {'numeric', 'numeric', 'numeric', 'numeric'};
% Disallow editing values (but this can be changed)
coledit = [false false false false];
% Set columns all the same width (must be in pixels)
colwdt = {120 120 120 120};
% Create a uitable on the left side of the figure
handles.htable = uitable('Parent',handles.hTableFigure, 'Units', 'normalized',...
                 'Position', [0.025 0.5 0.975 0.38],...
                 'BackgroundColor', [0.961 0.922 0.922],...
                 'Data',  IP_KNOBV{val}.Ratio,... 
                 'ColumnName', colnames,...
                 'ColumnFormat', colfmt,...
                 'ColumnWidth', colwdt,...
                 'ColumnEditable', coledit,...
                 'RowName',  handles.knobmname);
rcolnames = {'Strenth','read', 'xmover','read','ymover','read', 'rotation','read'};             
% All column contain numeric data (integers, actually)
rcolfmt = {'numeric', 'numeric', 'numeric', 'numeric'};
% Disallow editing values (but this can be changed)
rcoledit = [false false false false false false false false ];
           %% [true true true true true true true true ];
% Set columns all the same width (must be in pixels)
rcolwdt = {80 80 80 80 80 80 80 80 };
%% data = {'set', 'read','set', 'read','set', 'read','set', 'read'; ...
%%     [IP_KNOBV{val}.Ratio, IP_KNOBV{val}.Ratio]};
ttdata = {[IP_KNOBV{val}.Ratio, IP_KNOBV{val}.Ratio]};
uitable('Parent',handles.hTableFigure, 'Units', 'normalized',...
                 'Position', [0.025 0.435 0.975 0.065],...
                 'BackgroundColor', [0.961 0.922 0.922],...
                 'ColumnName', [{''}, colnames],...
                 'ColumnWidth', {80 160 160 160 160 },...
                 'ColumnEditable', coledit,...
                 'RowName',[]); 
        kbdata = [IP_KNOBV{val}.Ratio(:,1),IP_KNOBV{val}.Ratio(:,1),...
                 IP_KNOBV{val}.Ratio(:,2),IP_KNOBV{val}.Ratio(:,2),...
                 IP_KNOBV{val}.Ratio(:,3),IP_KNOBV{val}.Ratio(:,3),...
                 IP_KNOBV{val}.Ratio(:,4),IP_KNOBV{val}.Ratio(:,4)];
handles.htableread = uitable('Parent',handles.hTableFigure, 'Units', 'normalized',...
                 'Position', [0.025 0.025 0.975 0.41],...
                 'ColumnName',{'set','read', 'set','read','set','read', 'set','read'},...
                 'Data',  kbdata,... 
                 'ColumnWidth', rcolwdt,...
                 'RowName',  handles.knobmname); 

%% set([handles.hExitButton, handles.hStepedit, handles.hSetButton],...
%%     {'callback'},{{@hExitButton_Callback,handles};{@hStepedit_Callback,handles};...
%%                   {@hSetButton_Callback,handles}}) % Set callbacks. 
set([handles.hExitButton, handles.hSetButton],...
    {'callback'},{{@hExitButton_Callback,handles};{@hSetButton_Callback,handles}}) % Set callbacks.                
set([handles.htable, handles.htableread],...
   {'CellSelectionCallback'},{{@htable_Callback,handles};{@htableread_Callback,handles}}) % Set callbacks.  
guidata(hObject,handles);
 
  
function hExitButton_Callback(varargin) 
     handles=varargin{3}; 
     delete(handles.hTableFigure);
%% function hStepedit_Callback(varargin) 
%%      handles=varargin{3}; 
%%      guidata(hObject,handles);
   
function hSetButton_Callback(hObject, eventdata, handles)

global BEAMLINE FL IP_KNOBV GIRDER PS;

%% handles=varargin{3};
str = get(handles.popupmenu1, 'String');
val = get(handles.popupmenu1, 'Value');
str7 = get(handles.hKTpopmenu, 'String');
val7 = get(handles.hKTpopmenu, 'Value');
knob_value = eval(['[',get(handles.hStepedit,'String'),']']);
switch lower(str7{val7})
    case {'strength knob'}
        [handles.kbps_value] = set_ps(IP_KNOBV{val}.Ind(:,1)',IP_KNOBV{val}.Ratio(:,1)'*knob_value+ handles.kbps_value );
        bar(handles.axes10, handles.kbps_value -handles.ps_value_ini(handles.ia), 0.8);
        set(handles.axes10,'XTickLabel',handles.knobmname);
        set(handles.axes10,'xlim',[0.5 length(handles.knobmname)+0.5]);
    case {'mover knob'}
        [old_value, handles.kbmover_value] = set_mover(IP_KNOBV{val}.Ind(:,2)', IP_KNOBV{val}.Ratio(:,2:4)*knob_value+handles.kbmover_value);
        bar(handles.axes11, handles.kbmover_value(:,1) - handles.mover_value_ini(handles.ia,1), 0.8);
        set(handles.axes11,'XTickLabel',handles.knobmname);
        set(handles.axes11,'xlim',[0.5 length(handles.knobmname)+0.5]);
        bar(handles.axes12, handles.kbmover_value(:,2) - handles.mover_value_ini(handles.ia,2), 0.8);
        set(handles.axes12,'XTickLabel',handles.knobmname);
        set(handles.axes12,'xlim',[0.5 length(handles.knobmname)+0.5]);
    otherwise
        [handles.kbps_value] = set_ps(IP_KNOBV{val}.Ind(:,1)',IP_KNOBV{val}.Ratio(:,1)'*knob_value+ handles.kbps_value );
        bar(handles.axes10, handles.kbps_value -handles.ps_value_ini(handles.ia), 0.8);
        set(handles.axes10,'XTickLabel',handles.knobmname);
        set(handles.axes10,'xlim',[0.5 length(handles.knobmname)+0.5]);
        [old_value, handles.kbmover_value] = set_mover(IP_KNOBV{val}.Ind(:,2)', IP_KNOBV{val}.Ratio(:,2:4)*knob_value+handles.kbmover_value);
        bar(handles.axes11, handles.kbmover_value(:,1) - handles.mover_value_ini(handles.ia,1), 0.8);
        set(handles.axes11,'XTickLabel',handles.knobmname);
        set(handles.axes11,'xlim',[0.5 length(handles.knobmname)+0.5]);
        bar(handles.axes12, handles.kbmover_value(:,2) - handles.mover_value_ini(handles.ia,2), 0.8);
        set(handles.axes12,'XTickLabel',handles.knobmname);
        set(handles.axes12,'xlim',[0.5 length(handles.knobmname)+0.5]);
end
kbdata(:,1) = handles.kbps_value';
kbdata(:,[3 5 7]) = handles.kbmover_value;
for i = 1:length(IP_KNOBV{val}.Ind(:,1))
    kbdata(i,2) = PS(IP_KNOBV{val}.Ind(i,1)).Ampl ;
    kbdata(i,[4 6 8]) = GIRDER{IP_KNOBV{val}.Ind(i,2)}.MoverPos;
end 
set(handles.htableread, 'data', kbdata);
%% [stat,beamout] = TrackThru( 1, handles.IP_ind, FL.SimBeam{1}, 1, 1, 0 );
[data txt beamout] = GetIPSimData(FL.SimBeam{1},FL.SimBeam{2},1, handles.IP_ind);
[stat,Twissd]=GetTwiss(1,length(BEAMLINE), ...
FL.SimModel.Initial.x.Twiss,FL.SimModel.Initial.y.Twiss);
% data.ydisp = Twissd.etay(handles.IP_ind);
set(handles.edit4,'String',num2str(data.xwaist,'%5.2e'));
set(handles.edit10,'String',num2str(data.xdisp,'%5.2e'));
set(handles.edit5,'String',num2str(data.xsize,'%5.2e'));
set(handles.edit6,'String',num2str(data.emit_x,'%5.2e'));
set(handles.edit7,'String',num2str(data.ywaist,'%5.2e'));
set(handles.edit11,'String',num2str(data.ydisp,'%5.2e'));
set(handles.edit8,'String',num2str(data.ysize,'%5.2e'));
set(handles.edit9,'String',num2str(data.emit_y,'%5.2e'));

%% [stat,beamout,instdata] = TrackThru( 1, IP_ind, FL.SimBeam{1}, 1, 1, 0 );
[ nx,xout] = hist(beamout.Bunch.x(1,:),100);
[ ny,yout] = hist(beamout.Bunch.x(3,:),100); 
%% axes(handles.axes9)
%% cla;
plot(handles.axes6, beamout.Bunch.x(1,:)*1e6, beamout.Bunch.x(3,:)*1e6, 'r.'); 
handles.data(end+1)= data.xwaist;
handles.data(end+1)= data.xdisp;
handles.data(end+1)= data.xsize;
handles.data(end+1)= data.emit_x;
handles.data(end+1)= data.ywaist;
handles.data(end+1)= data.ydisp;
handles.data(end+1)= data.ysize;
handles.data(end+1)= data.emit_y;
handles.Knob_v(val) = handles.Knob_v(val) + knob_value ; 
handles.Knob_his(end+1,1) = val;  
handles.Knob_his(end+1,2) = knob_value; 
if (val < 17)
    tt1 = num2str(handles.Knob_v(val));
    eval(['set(handles.edit',num2str(11+2*val),',''String'',',tt1, ');']);
    tt2 = num2str(handles.Knob_v(val)*IP_KNOBV{val}.Unit,'%5.2e');
    eval(['set(handles.edit',num2str(12+2*val),',''String'',',tt2, ');']);
end

val8 = get(handles.popupmenu8,'Value');
plot(handles.axes9, handles.data(val8:8:end), 'r');
%% plot(handles.axes9, handles.data(handles.plot:8:end), 'r');
%% plot(handles.axes9, beamout.Bunch.x(1,:), beamout.Bunch.x(3,:), 'b.'); 
%% guidata(varargin{1},handles);
set(handles.hSetButton,{'callback'},{{@hSetButton_Callback,handles}}) %% important to self pass the parameters
%% Set callbacks.  
%% %%% guidata(hObject,handles);  %% wrong the parameters doesn't pass out 
guidata(handles.output, handles);  %% important to save 


% --- Executes on button press in pushbutton7.
function pushbutton7_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton7 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

function edit1_Callback(hObject, eventdata, handles)
% hObject    handle to edit1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit1 as text
%        str2double(get(hObject,'String')) returns contents of edit1 as a double


% --- Executes during object creation, after setting all properties.
function edit1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% --- Executes on button press in pushbutton8.
function pushbutton8_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton8 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global BEAMLINE FL IP_KNOBV;

%% str7 = get(handles.popupmenu7, 'String');
%% val7 = get(handles.popupmenu7,'Value');

%% switch val7
%%    case 1
       [ps_value] = set_ps(handles.Mag_ps,handles.ps_value_ini);
        bar(handles.axes10, ps_value - handles.ps_value_ini, 0.8);
        set(handles.axes10,'XTickLabel',handles.MagnetName);
        set(handles.axes10,'xlim',[0.5 length(ps_value)+0.5]);     
        [old_value, mover_value] = set_mover(handles.Mag_Gir, handles.mover_value_ini);
        %% axes(handles.axes12)
        bar(handles.axes11, mover_value(:,1) - handles.mover_value_ini(:,1), 0.8);
        set(handles.axes11,'XTickLabel',handles.MagnetName);
        set(handles.axes11,'xlim',[0.5 length(mover_value)+0.5]);
        bar(handles.axes12, mover_value(:,2) - handles.mover_value_ini(:,2), 0.8);
        set(handles.axes12,'XTickLabel',handles.MagnetName);
        set(handles.axes12,'xlim',[0.5 length(mover_value)+0.5]);
        for ii = 12:44
            eval(['set(handles.edit',num2str(ii),',''String'',''0'')']);
        end
         
%% end
[data txt beamout] = GetIPSimData(FL.SimBeam{1},FL.SimBeam{2},1, handles.IP_ind);
[stat,Twissd]=GetTwiss(1,length(BEAMLINE), ...
     FL.SimModel.Initial.x.Twiss,FL.SimModel.Initial.y.Twiss);
  
set(handles.edit4,'String',num2str(data.xwaist,'%5.2e'));
set(handles.edit10,'String',num2str(data.xdisp,'%5.2e'));
set(handles.edit5,'String',num2str(data.xsize,'%5.2e'));
set(handles.edit6,'String',num2str(data.emit_x,'%5.2e'));
set(handles.edit7,'String',num2str(data.ywaist,'%5.2e'));
set(handles.edit11,'String',num2str(data.ydisp,'%5.2e'));
set(handles.edit8,'String',num2str(data.ysize,'%5.2e'));
set(handles.edit9,'String',num2str(data.emit_y,'%5.2e'));
[ nx,xout] = hist(beamout.Bunch.x(1,:),100);
[ ny,yout] = hist(beamout.Bunch.x(3,:),100);
%% axes(handles.axes9)
%% cla
plot(handles.axes6, beamout.Bunch.x(1,:)*1e6, beamout.Bunch.x(3,:)*1e6, 'r.');

handles.data(end+1)= data.xwaist;
handles.data(end+1)= data.xdisp;
handles.data(end+1)= data.xsize;
handles.data(end+1)= data.emit_x;

handles.data(end+1)= data.ywaist;
handles.data(end+1)= data.ydisp;
handles.data(end+1)= data.ysize;
handles.data(end+1)= data.emit_y;

%% handles.plot = get(handles.popupmenu8,'Value');
%% plot(handles.axes9, handles.data(handles.plot:8:end));
for ii=1:length(handles.Knob_v)
    handles.Knob_v(ii) = 0;
end
handles.Knob_his(end+1,1) = 0.0;  
handles.Knob_his(end+1,2) = 0.0;  

guidata(hObject,handles);


function edit2_Callback(hObject, eventdata, handles)
% hObject    handle to edit2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit2 as text
%        str2double(get(hObject,'String')) returns contents of edit2 as a double


% --- Executes during object creation, after setting all properties.
function edit2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function edit3_Callback(hObject, eventdata, handles)
% hObject    handle to edit3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit3 as text
%        str2double(get(hObject,'String')) returns contents of edit3 as a double


% --- Executes during object creation, after setting all properties.
function edit3_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% --- Executes on selection change in popupmenu7.
function popupmenu7_Callback(hObject, eventdata, handles)
% hObject    handle to popupmenu7 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns popupmenu7 contents as cell array
%        contents{get(hObject,'Value')} returns selected item from popupmenu7


% --- Executes during object creation, after setting all properties.
function popupmenu7_CreateFcn(hObject, eventdata, handles)
% hObject    handle to popupmenu7 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% --- Executes on button press in 3.
function pushbutton9_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton9 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
%% range = eval(['[',get(handles.edit2,'String'),']']);
%% step = eval(['[',get(handles.edit1,'String'),']']);
global BEAMLINE IP_KNOBV;

str = get(handles.popupmenu1, 'String');
val = get(handles.popupmenu1, 'Value');
nn = length(str)+1;
%% uitable('Units', 'normalized',...
%%                  'Position', [0.025 0.5 0.975 0.05],...
%%                  'BackgroundColor', [0.961 0.922 0.922],...
%%                 'RowName',handles.MagnetName); 
[sel,ok] = listdlg('PromptString','Select the magnets:',...
                'SelectionMode','multi',...
                'ListString',handles.MagnetName);
         
prompt = {'Enter knob name:','Enter knob magnet name:','Enter knob strenth ratio:',...
   'Enter knob x mover ratio:','Enter knob y mover ratio:','Enter knob rotation ratio'};
dlg_title = 'Add new knob';
num_lines = 1;
def = {'User1_IP_Wx','{ ''SF6FF''; ''SF5FF''; ''SD4FF''; ''SF1FF''; ''SD0FF'' }','1; 0.1 ; 0.05; 1; -0.2',...
    '0; 0; 0; 0; 0 ','0; 0; 0; 0; 0 ','0; 0; 0; 0; 0 ', '0; 0; 0; 0; 0 '};
if(ok)
    def{2} = '{''' ; 
    for ii = 1: length(sel)-1
        def{2} = [def{2}, handles.MagnetName{sel(ii)}, '''; '''];
    end
   def{2} = [def{2}, handles.MagnetName{sel(end)}, '''}'];
end  
options.Resize='on';
answer = inputdlg(prompt,dlg_title,num_lines,def,options);
tname = cellfun(@(x) x.Name, IP_KNOBV, 'UniformOutput',false);
if (find(strmatch(answer{1}, char(tname))))
    choice = questdlg('The knob name is used. Do you want to replace the original one?');
   %%  switch choice
   %%      case 'Yes'
   %%        aa = strmatch(answer{1}, char(tname));         
else
    str{nn} = answer{1};
        MagnetName = eval(answer{2});
        set(handles.popupmenu1, 'String', str);
        handles.Knob_v(nn) = 0;
        IP_KNOBV{nn}.Name=answer{1};
        IP_KNOBV{nn}.Unit=[];
        ipstre = str2num(answer{3});
        xmover = str2num(answer{4});
        ymover = str2num(answer{5});
        rotation = str2num(answer{6});
        IP_KNOBV{nn}.Ratio = zeros(length(MagnetName),4);
        for i = 1:length(MagnetName)
            ind = findcells(BEAMLINE,'Name',MagnetName{i});
            IP_KNOBV{nn}.Ind(i,1) = BEAMLINE{ind(1)}.PS;
            IP_KNOBV{nn}.Ind(i,2) = BEAMLINE{ind(1)}.Girder;
            IP_KNOBV{nn}.Ratio(i,1)= ipstre(i);
            IP_KNOBV{nn}.Ratio(i,2)= xmover(i);
            IP_KNOBV{nn}.Ratio(i,3)= ymover(i);
            IP_KNOBV{nn}.Ratio(i,4)= rotation(i);
        end
end    

%% %% load .\testApps\IP_knob\KNOB_VALUE;

% Proceed with callback...

function edit4_Callback(hObject, eventdata, handles)
% hObject    handle to edit4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit4 as text
%        str2double(get(hObject,'String')) returns contents of edit4 as a double


% --- Executes during object creation, after setting all properties.
function edit4_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function edit5_Callback(hObject, eventdata, handles)
% hObject    handle to edit5 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit5 as text
%        str2double(get(hObject,'String')) returns contents of edit5 as a double


% --- Executes during object creation, after setting all properties.
function edit5_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit5 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function edit6_Callback(hObject, eventdata, handles)
% hObject    handle to edit6 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit6 as text
%        str2double(get(hObject,'String')) returns contents of edit6 as a double


% --- Executes during object creation, after setting all properties.
function edit6_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit6 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function edit7_Callback(hObject, eventdata, handles)
% hObject    handle to edit7 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit7 as text
%        str2double(get(hObject,'String')) returns contents of edit7 as a double


% --- Executes during object creation, after setting all properties.
function edit7_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit7 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function edit8_Callback(hObject, eventdata, handles)
% hObject    handle to edit8 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit8 as text
%        str2double(get(hObject,'String')) returns contents of edit8 as a double

% --- Executes during object creation, after setting all properties.
function edit8_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit8 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function edit9_Callback(hObject, eventdata, handles)
% hObject    handle to edit9 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit9 as text
%        str2double(get(hObject,'String')) returns contents of edit9 as a double


% --- Executes during object creation, after setting all properties.
function edit9_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit9 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function edit10_Callback(hObject, eventdata, handles)
% hObject    handle to edit10 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit10 as text
%        str2double(get(hObject,'String')) returns contents of edit10 as a double


% --- Executes during object creation, after setting all properties.
function edit10_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit10 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function edit11_Callback(hObject, eventdata, handles)
% hObject    handle to edit11 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit11 as text
%        str2double(get(hObject,'String')) returns contents of edit11 as a double


% --- Executes during object creation, after setting all properties.
function edit11_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit11 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function edit12_Callback(hObject, eventdata, handles)
% hObject    handle to edit12 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit12 as text
%        str2double(get(hObject,'String')) returns contents of edit12 as a double


% --- Executes during object creation, after setting all properties.
function edit12_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit12 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit13_Callback(hObject, eventdata, handles)
% hObject    handle to edit13 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit13 as text
%        str2double(get(hObject,'String')) returns contents of edit13 as a double


% --- Executes during object creation, after setting all properties.
function edit13_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit13 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function edit14_Callback(hObject, eventdata, handles)
% hObject    handle to edit14 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit14 as text
%        str2double(get(hObject,'String')) returns contents of edit14 as a double


% --- Executes during object creation, after setting all properties.
function edit14_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit14 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit15_Callback(hObject, eventdata, handles)
% hObject    handle to edit15 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit15 as text
%        str2double(get(hObject,'String')) returns contents of edit15 as a double


% --- Executes during object creation, after setting all properties.
function edit15_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit15 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit16_Callback(hObject, eventdata, handles)
% hObject    handle to edit16 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit16 as text
%        str2double(get(hObject,'String')) returns contents of edit16 as a double


% --- Executes during object creation, after setting all properties.
function edit16_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit16 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit17_Callback(hObject, eventdata, handles)
% hObject    handle to edit17 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit17 as text
%        str2double(get(hObject,'String')) returns contents of edit17 as a double


% --- Executes during object creation, after setting all properties.
function edit17_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit17 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit18_Callback(hObject, eventdata, handles)
% hObject    handle to edit18 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit18 as text
%        str2double(get(hObject,'String')) returns contents of edit18 as a double


% --- Executes during object creation, after setting all properties.
function edit18_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit18 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit19_Callback(hObject, eventdata, handles)
% hObject    handle to edit19 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit19 as text
%        str2double(get(hObject,'String')) returns contents of edit19 as a double


% --- Executes during object creation, after setting all properties.
function edit19_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit19 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit20_Callback(hObject, eventdata, handles)
% hObject    handle to edit20 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit20 as text
%        str2double(get(hObject,'String')) returns contents of edit20 as a double


% --- Executes during object creation, after setting all properties.
function edit20_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit20 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit21_Callback(hObject, eventdata, handles)
% hObject    handle to edit21 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit21 as text
%        str2double(get(hObject,'String')) returns contents of edit21 as a double


% --- Executes during object creation, after setting all properties.
function edit21_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit21 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit22_Callback(hObject, eventdata, handles)
% hObject    handle to edit22 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit22 as text
%        str2double(get(hObject,'String')) returns contents of edit22 as a double


% --- Executes during object creation, after setting all properties.
function edit22_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit22 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function edit23_Callback(hObject, eventdata, handles)
% hObject    handle to edit23 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit23 as text
%        str2double(get(hObject,'String')) returns contents of edit23 as a double


% --- Executes during object creation, after setting all properties.
function edit23_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit23 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit24_Callback(hObject, eventdata, handles)
% hObject    handle to edit24 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit24 as text
%        str2double(get(hObject,'String')) returns contents of edit24 as a double


% --- Executes during object creation, after setting all properties.
function edit24_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit24 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit25_Callback(hObject, eventdata, handles)
% hObject    handle to edit25 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit25 as text
%        str2double(get(hObject,'String')) returns contents of edit25 as a double


% --- Executes during object creation, after setting all properties.
function edit25_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit25 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit26_Callback(hObject, eventdata, handles)
% hObject    handle to edit26 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit26 as text
%        str2double(get(hObject,'String')) returns contents of edit26 as a double


% --- Executes during object creation, after setting all properties.
function edit26_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit26 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit27_Callback(hObject, eventdata, handles)
% hObject    handle to edit27 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit27 as text
%        str2double(get(hObject,'String')) returns contents of edit27 as a double


% --- Executes during object creation, after setting all properties.
function edit27_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit27 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit28_Callback(hObject, eventdata, handles)
% hObject    handle to edit28 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit28 as text
%        str2double(get(hObject,'String')) returns contents of edit28 as a double


% --- Executes during object creation, after setting all properties.
function edit28_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit28 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit29_Callback(hObject, eventdata, handles)
% hObject    handle to edit29 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit29 as text
%        str2double(get(hObject,'String')) returns contents of edit29 as a double


% --- Executes during object creation, after setting all properties.
function edit29_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit29 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit30_Callback(hObject, eventdata, handles)
% hObject    handle to edit30 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit30 as text
%        str2double(get(hObject,'String')) returns contents of edit30 as a double


% --- Executes during object creation, after setting all properties.
function edit30_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit30 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit31_Callback(hObject, eventdata, handles)
% hObject    handle to edit31 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit31 as text
%        str2double(get(hObject,'String')) returns contents of edit31 as a double


% --- Executes during object creation, after setting all properties.
function edit31_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit31 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit32_Callback(hObject, eventdata, handles)
% hObject    handle to edit32 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit32 as text
%        str2double(get(hObject,'String')) returns contents of edit32 as a double


% --- Executes during object creation, after setting all properties.
function edit32_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit32 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit33_Callback(hObject, eventdata, handles)
% hObject    handle to edit33 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit33 as text
%        str2double(get(hObject,'String')) returns contents of edit33 as a double


% --- Executes during object creation, after setting all properties.
function edit33_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit33 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit34_Callback(hObject, eventdata, handles)
% hObject    handle to edit34 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit34 as text
%        str2double(get(hObject,'String')) returns contents of edit34 as a double


% --- Executes during object creation, after setting all properties.
function edit34_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit34 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit35_Callback(hObject, eventdata, handles)
% hObject    handle to edit35 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit35 as text
%        str2double(get(hObject,'String')) returns contents of edit35 as a double


% --- Executes during object creation, after setting all properties.
function edit35_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit35 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit36_Callback(hObject, eventdata, handles)
% hObject    handle to edit36 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit36 as text
%        str2double(get(hObject,'String')) returns contents of edit36 as a double


% --- Executes during object creation, after setting all properties.
function edit36_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit36 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit37_Callback(hObject, eventdata, handles)
% hObject    handle to edit37 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit37 as text
%        str2double(get(hObject,'String')) returns contents of edit37 as a double


% --- Executes during object creation, after setting all properties.
function edit37_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit37 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit38_Callback(hObject, eventdata, handles)
% hObject    handle to edit38 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit38 as text
%        str2double(get(hObject,'String')) returns contents of edit38 as a double


% --- Executes during object creation, after setting all properties.
function edit38_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit38 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit39_Callback(hObject, eventdata, handles)
% hObject    handle to edit39 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit39 as text
%        str2double(get(hObject,'String')) returns contents of edit39 as a double


% --- Executes during object creation, after setting all properties.
function edit39_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit39 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit40_Callback(hObject, eventdata, handles)
% hObject    handle to edit40 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit40 as text
%        str2double(get(hObject,'String')) returns contents of edit40 as a double


% --- Executes during object creation, after setting all properties.
function edit40_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit40 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit41_Callback(hObject, eventdata, handles)
% hObject    handle to edit41 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit41 as text
%        str2double(get(hObject,'String')) returns contents of edit41 as a double


% --- Executes during object creation, after setting all properties.
function edit41_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit41 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit42_Callback(hObject, eventdata, handles)
% hObject    handle to edit42 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit42 as text
%        str2double(get(hObject,'String')) returns contents of edit42 as a double


% --- Executes during object creation, after setting all properties.
function edit42_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit42 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit43_Callback(hObject, eventdata, handles)
% hObject    handle to edit43 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit43 as text
%        str2double(get(hObject,'String')) returns contents of edit43 as a double


% --- Executes during object creation, after setting all properties.
function edit43_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit43 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit44_Callback(hObject, eventdata, handles)
% hObject    handle to edit44 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit44 as text
%        str2double(get(hObject,'String')) returns contents of edit44 as a double


% --- Executes during object creation, after setting all properties.
function edit44_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit44 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in pushbutton10.
function pushbutton10_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton10 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global IP_KNOBV GIRDER PS ;
for i = 1:length(handles.MagnetName)
    ps_value(i) = PS(handles.Mag_ps(i)).SetPt ;
    mover_value(i,:)=GIRDER{handles.Mag_Gir(i)}.MoverSetPt;
%%  ps_value_ini(i) = PS(Mag_ps(i)).Ampl ;
%%  mover_ini(i,:)=GIRDER{Mag_Gir(i)}.MoverPos;
end
file_name = inputdlg('Input file name:','Save file',1,{'testdata'})
beamdata = [handles.data(1:8:end);handles.data(2:8:end);handles.data(3:8:end);handles.data(4:8:end);...
    handles.data(5:8:end);handles.data(6:8:end);handles.data(7:8:end);handles.data(8:8:end)];
eval(['save ./testApps/IP_knob/data/',file_name{1},' ps_value mover_value handles.Mag_ps handles.Mag_Gir beamdata handles.Knob_his handles.Knob_v IP_KNOBV ;']);   
%% eval(['save .\testApps\IP_knob\data\',file_name{1},' Qps_value Sps_value Smover Q_ps S_ps S_Gir beamdata setdata IP_KNOBV ;']);   
%% save .\testApps\IP_knob\data\file_name Qps_value Sps_value Smover Q_ps S_ps S_Gir ;   



function edit55_Callback(hObject, eventdata, handles)
% hObject    handle to edit55 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit55 as text
%        str2double(get(hObject,'String')) returns contents of edit55 as a double


% --- Executes during object creation, after setting all properties.
function edit55_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit55 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in popupmenu8.
function popupmenu8_Callback(hObject, eventdata, handles)
% hObject    handle to popupmenu8 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = get(hObject,'String') returns popupmenu8 contents as cell array
%        contents{get(hObject,'Value')} returns selected item from
%        popupmenu
val8 = get(handles.popupmenu8,'Value');
plot(handles.axes9, handles.data(val8:8:end), 'r');
guidata(hObject,handles);

% --- Executes during object creation, after setting all properties.
function popupmenu8_CreateFcn(hObject, eventdata, handles)
% hObject    handle to popupmenu8 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function edit56_Callback(hObject, eventdata, handles)
% hObject    handle to edit56 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit56 as text
%        str2double(get(hObject,'String')) returns contents of edit56 as a double


% --- Executes during object creation, after setting all properties.
function edit56_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit56 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit58_Callback(hObject, eventdata, handles)
% hObject    handle to edit58 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit58 as text
%        str2double(get(hObject,'String')) returns contents of edit58 as a double


% --- Executes during object creation, after setting all properties.
function edit58_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit58 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes during object creation, after setting all properties.
function axes9_CreateFcn(hObject, eventdata, handles)
% hObject    handle to axes9 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: place code in OpeningFcn to populate axes9

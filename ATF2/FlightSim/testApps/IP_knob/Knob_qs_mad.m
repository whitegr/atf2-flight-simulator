function [IP_KNOBV] = Knob_qs_mad(QuadName, varargin )
%%% 
%% Construct quadrupole strength knob to adjust the beta x, y waist and eta x at IP using MAD. 
%% [IP_KNOBV] = Knob_qs_mad(QuadName, varargin )
%% QuadName: The name of quadrupoles to construct the knob
%% IP_KNOBV: The output of the knob data in cell with three fields.
%%     IP_KNOBV{:}.Name: The name of the knob.
%%     IP_KNOBV{:}.Unit: The unit of the knob value coresponding to the
%%     minimum adjusting step of power supply 10e-4 or Sext mover 10e-6 m. 
%%     IP_KNOBV{:}.Ratio: The ratio of the power supply or mover postion to
%%     change the knob value in unit value. 
%% 
%% Exampe 
%%
%% QuadName = { 'QD6FF'; 'QF5BFF'; 'QF5AFF'};
%% IP_KNOBV{1}.Name = 'Beta_x_Waist';
%% IP_KNOBV{1}.Unit = 3.5658e-005
%% IP_KNOBV{1}.Ratio =[0.1000   -0.2307    0.2346]*1e-3
%% 
%% Auther: Min-Huey Wang SLAC National Accelerator Laboratory
%% Date: Dec 2009 

%%function [MagRatioSet] = Knob_qs_mad(quadname, range, step)

global BEAMLINE 

%% %% load ./testApps/IP_knob/KNOB_VALUE_FLT.mat
load ./testApps/IP_knob/KNOB_VALUE

ps_step = 1e-4;
MADdict='set dict= .\..\..\..\..\..\ATF2\mad8.51\mad8.dict';
MADcmd='.\..\..\..\..\..\ATF2\mad8.51\cygwin-x86\mad8s.exe < .\testApps\IP_knob\knob_simu.mad';

%% MADdict='set dict=${CSOFT}/../../mad8.51/mad8.dict';
%% MADdict='ln -s ${CSOFT}/../../mad8.51/mad8.dict dict';
%% MADcmd='${CSOFT}/../../mad8.51/linux-x86_64/mad8s < ./testApps/IP_knob/knob_simu.mad';

l_QName = length(QuadName); 
Q_Ps = zeros(1, l_QName);
Q_Gir = zeros(1, l_QName);
for i = 1:l_QName
    ind = findcells(BEAMLINE,'Name',QuadName{i});
    Q_Ps(i) = BEAMLINE{ind(1)}.PS;
    Q_Gir(i) = BEAMLINE{ind(1)}.Girder;
end

FF_MAD_ini;

if nargin>=2 
    ds = (-varargin{1}*ps_step :ps_step:varargin{1}*ps_step ); 
else

   %% ds = [1e-4:1e-4:1e-3];
   ds = (-5.0e-3:1e-4:5.0e-3);
end

%% ds = [5e-5:5e-5:1e-3,1.5e-3:5e-4:1e-2]; %% if the ds = [5e-5:5e-5:1e-2];
%% ds = [1e-5:1e-5:1e-4];
l_ds = length(ds);
l_QName = length(QuadName);
BX = zeros(1,l_ds);
ALX = zeros(1,l_ds);
BX = zeros(1,l_ds);
ALX = zeros(1,l_ds);
SXBX = zeros(1,l_ds);
SXBY = zeros(1,l_ds);
SXETX = zeros(1,l_ds);
SXBX_r = zeros(l_QName,2);
SXBY_r = zeros(l_QName,2);
SXETX_r = zeros(l_QName,2);
SXBX_r2 = zeros(l_QName,3);
SXBY_r2 = zeros(l_QName,3);
SXETX_r2 = zeros(l_QName,3);
for ii = 1:l_QName
    for jj = 1:l_ds
        fid=fopen('./testApps/IP_knob/Quad_s.mad','w');
       %% fprintf(fid,'%s \n',['set , ', QuadName{ii}, '[k1], K', QuadName{ii}, '0*(1+ ', num2str(ds(jj)),')']);
        fprintf(fid,'%s \n',['set , ', QuadName{ii}, '[k1], K', QuadName{ii}, '0*(1+ (', num2str(sign(ds(jj))), ')*', num2str(abs(ds(jj))),')']);
        %% set , QD6FF[k1], KQD6FF0*(1+ 0.0004) 
        %% %%set ,QF5BFF[k1], QF5BFF[k1] - QF5BFF[k1]* 0.003332142511724
        %% fprintf(fid,'%s \n',['RERTURN']);
        status = fclose(fid);
        [s,r]=system([MADdict,'&',MADcmd]);
        %% [s,r]=system('grep "Value of expression \"BX\" is:" .\testApps\IP_knob\ATF2s.echo');
        [s,r]=system('grep "Value of expression .BX. is:" ./testApps/IP_knob/ATF2s.echo');
        ic=strfind(r,':');
        BX(jj)=str2double(r(ic+1:end));
        %% [s,r]=system('grep "Value of expression \"ALX\" is:" .\testApps\IP_knob\ATF2s.echo');
        [s,r]=system('grep "Value of expression .ALX. is:" ./testApps/IP_knob/ATF2s.echo');
        ic=strfind(r,':');
        ALX(jj)=str2double(r(ic+1:end));
        %% [s,r]=system('grep "Value of expression \"BY\" is:" .\testApps\IP_knob\ATF2s.echo');
        [s,r]=system('grep "Value of expression .BY. is:" ./testApps/IP_knob/ATF2s.echo');
        ic=strfind(r,':');
        BY(jj)=str2double(r(ic+1:end));
        %% [s,r]=system('grep "Value of expression \"ALY\" is:" .\testApps\IP_knob\ATF2s.echo');
        [s,r]=system('grep "Value of expression .ALY. is:" ./testApps/IP_knob/ATF2s.echo');
        ic=strfind(r,':');
        ALY(jj)=str2double(r(ic+1:end));
        %% [s,r]=system('grep "Value of expression \"SXBX\" is:" .\testApps\IP_knob\ATF2s.echo');
        [s,r]=system('grep "Value of expression .SXBX. is:" ./testApps/IP_knob/ATF2s.echo');
        ic=strfind(r,':');
        SXBX(jj)=str2double(r(ic+1:end));
        %% [s,r]=system('grep "Value of expression \"SXBY\" is:" .\testApps\IP_knob\ATF2s.echo');
        [s,r]=system('grep "Value of expression .SXBY. is:" ./testApps/IP_knob/ATF2s.echo');
        ic=strfind(r,':');
        SXBY(jj)=str2double(r(ic+1:end));
        %% [s,r]=system('grep "Value of expression \"SXETX\" is:" .\testApps\IP_knob\ATF2s.echo');
        [s,r]=system('grep "Value of expression .SXETX. is:" ./testApps/IP_knob/ATF2s.echo');
        ic=strfind(r,':');
        SXETX(jj)=str2double(r(ic+1:end));
    end
    SXBX_r(ii,:) = polyfit(ds,SXBX,1);
    SXBY_r(ii,:) = polyfit(ds,SXBY,1);
    SXETX_r(ii,:) = polyfit(ds,SXETX,1);
    SXBX_r2(ii,:) = polyfit(ds,SXBX,2);
    SXBY_r2(ii,:) = polyfit(ds,SXBY,2);
    SXETX_r2(ii,:) = polyfit(ds,SXETX,2);
end
%% plot(ds, SXBX);
%% hold ;
%% plot(ds, polyval(SXBX_r(end,:), ds),'ro');
%% reset to zero 
fid = fopen('./testApps/IP_knob/Quad_s.mad','w');
fprintf(fid,'%s \n',['set , ', QuadName{end}, '[k1],K', QuadName{end}, '0']);
status = fclose(fid);
AA = [SXBX_r(:,1)'; SXBY_r(:,1)'; SXETX_r(:,1)'];
IP_Beta_x_Waist_ratio = AA\[1;0;0]; %% adjust in unit of m  %% range 
IP_Beta_y_Waist_ratio = AA\[0;1;0]; %% adjust in unit of m
IP_Eta_x_ratio = AA\[0;0;1];        %% adjust in unit of m

%% switch lower(knobname)
%%    case {'ip_beta_x_waist'}
%%        [MagRatioSet] = knob_set(QuadName, IP_Beta_x_Waist', knob_value);
%%    case {'ip_beta_y_waist'}
%%        [MagRatioSet] = knob_set(QuadName, IP_Beta_y_Waist', knob_value);
%%    case {'ip_eta_x'}
%%        [MagRatioSet] = knob_set(QuadName, IP_Eta_x', knob_value);
%%    otherwise
%%        disp('Please use one of the following knob names:');
%%        disp({'IP_Beta_x_Waist';'IP_Beta_y_Waist'; 'IP_Eta_x'}  );
%% end
IP_KNOBV{1}.Name = 'Beta_x_Waist';
IP_KNOBV{2}.Name = 'Beta_y_Waist';
IP_KNOBV{3}.Name = 'Eta_x';
IP_KNOBV{1}.Unit = ps_step/min(abs(IP_Beta_x_Waist_ratio)); 
IP_KNOBV{2}.Unit = ps_step/min(abs(IP_Beta_y_Waist_ratio)); 
IP_KNOBV{3}.Unit = ps_step/min(abs(IP_Eta_x_ratio)); 
IP_KNOBV{1}.Ratio(:,1) = IP_Beta_x_Waist_ratio'*IP_KNOBV{1}.Unit;
IP_KNOBV{2}.Ratio(:,1) = IP_Beta_y_Waist_ratio'*IP_KNOBV{2}.Unit;
IP_KNOBV{3}.Ratio(:,1) = IP_Eta_x_ratio'*IP_KNOBV{3}.Unit;
IP_KNOBV{1}.Ratio(:,2:4) = zeros(length(IP_KNOBV{1}.Ratio(:,1)),3);
IP_KNOBV{2}.Ratio(:,2:4) = zeros(length(IP_KNOBV{2}.Ratio(:,1)),3);
IP_KNOBV{3}.Ratio(:,2:4) = zeros(length(IP_KNOBV{3}.Ratio(:,1)),3);
IP_KNOBV{1}.Ind(:,1) = Q_Ps ;
IP_KNOBV{2}.Ind(:,1) = Q_Ps ;
IP_KNOBV{3}.Ind(:,1) = Q_Ps ;
IP_KNOBV{1}.Ind(:,2) = Q_Gir ;
IP_KNOBV{2}.Ind(:,2) = Q_Gir ;
IP_KNOBV{3}.Ind(:,2) = Q_Gir ;
%% %% save .\testApps\IP_knob\KNOB_VALUE QuadName IP_Beta_x_Waist_ratio IP_Beta_y_Waist_ratio IP_Eta_x_ratio -append;
%% save ./testApps/IP_knob/KNOB_VALUE IP_KNOBV ;
end


          


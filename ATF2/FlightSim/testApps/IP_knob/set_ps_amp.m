
function [ps_value] = set_ps_amp(Q_ps, ps_value )

global PS
for i = 1:length(Q_ps)
  PS{Q_ps(i)}.Aml =  ps_value(i); 
end
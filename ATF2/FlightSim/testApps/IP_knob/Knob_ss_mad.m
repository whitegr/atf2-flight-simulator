function [IP_KNOBV] = Knob_ss_mad(SextName, varargin)
%%%
%% Construct sextupole strength knob to adjust the 2nd order non-liner term using MAD. 
%% [IP_KNOBV] = Knob_ss_mad(SextName, varargin )
%% SextName: The name of quadrupoles to construct the knob
%% IP_KNOBV: The output of the knob data in cell with three fields.
%%     IP_KNOBV{:}.Name: The name of the knob.
%%     IP_KNOBV{:}.Unit: The unit of the knob value coresponding to the
%%     minimum adjusting step of power supply 10e-4 or Sext mover 10e-6 m. 
%%     IP_KNOBV{:}.Ratio: The ratio of the power supply or mover postion to
%%     change the knob value in unit value. 
%% 
%% Exampe 
%%
%% SextName = {'SF6FF'; 'SF5FF'; 'SD4FF'; 'SF1FF'; 'SD0FF'};
%% IP_KNOBV{4}.Name = 'T122';
%% IP_KNOBV{4}.Unit = 0.0453;
%% IP_KNOBV{4}.Ratio = [0.0014   -0.0130    0.0037    0.0006   -0.0001];
%% 
%% Auther: Min-Huey Wang SLAC National Accelerator Laboratory
%% Date: Dec 2009 

global BEAMLINE 

%% load ./testApps/IP_knob/KNOB_VALUE_FLT.mat
load ./testApps/IP_knob/KNOB_VALUE
ps_step = 1e-4;
MADdict='set dict= .\..\..\..\..\..\ATF2\mad8.51\mad8.dict';
MADcmd='.\..\..\..\..\..\ATF2\mad8.51\cygwin-x86\mad8s.exe < .\testApps\IP_knob\knob_simu.mad';

%% %% MADdict='set dict=${CSOFT}/../../mad8.51/mad8.dict';
%% %% MADcmd='${CSOFT}/../../mad8.51/linux-x86_64/mad8s < ./testApps/IP_knob/knob_simu.mad';
%% MADdict='ln -s ${CSOFT}/../../mad8.51/mad8.dict dict';
%% MADcmd='${CSOFT}/../../mad8.51/linux-x86_64/mad8s < ./testApps/IP_knob/knob_simu.mad';

%% FF_MAD_ini;
[k1,k2] = FF_MAD_ini;

l_SName = length(SextName);
S_Ps = zeros(1, l_SName);
S_Gir = zeros(1, l_SName);
for i = 1:l_SName
    ind = findcells(BEAMLINE,'Name',SextName{i});
    S_Ps(i) = BEAMLINE{ind(1)}.PS;
    S_Gir(i) = BEAMLINE{ind(1)}.Girder;
end

[s,r]=system([MADdict,'&',MADcmd]);
fr=fopen('./testApps/IP_knob/ATF2s.print','r');
for ii=1:63
    tline = fgetl(fr);
end
%% T1 = []; T2 = [];T3 = [];T4 = [];T5 = [];T6 = [];
fout1=fopen('./testApps/IP_knob/TTMatrix.mad','w');
% fprintf(fout1,'%s \n',['MFF2A : SUBROUTINE']);
%  fprintf(fout1,'%s \n',['USE, ATF2sA']);
%  fprintf(fout1,'%s \n',[' MATCH, BETA0=TWSS0']);
%  fprintf(fout1,'%s \n',[' VARY, RASF6FF, STEP=1.E-6 ']);
%  fprintf(fout1,'%s \n',[' VARY, RASF5FF, STEP=1.E-6 ']);
%  fprintf(fout1,'%s \n',[' VARY, RASD4FF, STEP=1.E-6 ']);
%  fprintf(fout1,'%s \n',[' VARY, RASF1FF, STEP=1.E-6']);
%  fprintf(fout1,'%s \n',[' VARY, RASD0FF, STEP=1.E-6 ']);
%  fprintf(fout1,'%s \n',[' TMATRIX, #S/#E, &']);
for ii= 1:5%% no energy dependent term
    a = textscan(fr, '%f %f %f %f %f %f %f ', 1);
    for jj=1:6  
       %%  fprintf(fout1,'%s \n',['TM(',num2str(ii),',1,', num2str(jj), ')=', num2str(a{jj+1},'%10.5e'),...
       %%     ' weight(',num2str(ii),',1,', num2str(jj),
       %%     ')=W1_',num2str(ii),'1', num2str(jj)]);
       %% fprintf(fout1,'%s \n',[' TM(',num2str(ii),',1,', num2str(jj), ')= ', num2str(a{jj+1},'%12.4e')]);
       fprintf(fout1,'%s \n',['set TM',num2str(ii),'1', num2str(jj), ',  ', num2str(a{jj+1},'%16.4e')]);
    end
    %% eval(['T', num2str(ii), '=[T',  num2str(ii), '; a{2} a{3} a{4} a{5} a{6} a{7}];' ]);
    for kk = 1:5
        a = textscan(fr, '%f %f %f %f %f %f ', 1);
        for jj=1:6
            %%  fprintf(fout1,'%s \n',['TM(',num2str(ii),',', num2str(kk+1),',', num2str(jj), ')=', ...
            %% num2str(a{jj},'%10.5e'),...
            %% ' weight(',num2str(ii),',', num2str(kk+1),',', num2str(jj), ')=W1_',num2str(ii), num2str(kk+1), num2str(jj)]);
        %% fprintf(fout1,'%s \n',['set TM', num2str(ii),num2str(kk+1),num2str(jj),' , ', num2str(a{jj},12)]);
        fprintf(fout1,'%s \n',['set TM',num2str(ii), num2str(kk+1), num2str(jj), ',  ',num2str(a{jj},'%16.4e')]);
        end
        %%eval(['T', num2str(ii), '=[T',  num2str(ii), '; a{1} a{2} a{3} a{4} a{5} a{6} ];' ]);
    end
end
% fprintf(fout1,'%s \n',[' LMDIF, TOL=1.E-20 ']);
% fprintf(fout1,'%s \n',[' MIGRAD, TOL=1.E-20 ']);
% fprintf(fout1,'%s \n',[' ENDMATCH ']);
% fprintf(fout1,'%s \n',[' VALUE, DKLSF6FF,DKLSF5FF,DKLSD4FF,DKLSF1FF,DKLSD0FF ']);
% fprintf(fout1,'%s \n',['ENDSUBROUTINE']);
status = fclose(fr);
% for ii = 1:length(SextName)
%     fprintf(fout1,'%s \n',['set , ', SextName{ii}, '[k2],', SextName{ii}, '[k2] + RA', SextName{ii}, '*', SextName{ii}, '[k2]' ]);
% end
status = fclose(fout1);
%%
IP_Tknob_Name = {'IP_T122'; 'IP_T162'; 'IP_T166'; 'IP_T342'; 'IP_T364'};
MADcmd='.\..\..\..\..\..\ATF2\mad8.51\cygwin-x86\mad8s.exe < .\testApps\IP_knob\knob_simu_non.mad';
%% MADcmd='${CSOFT}/../../mad8.51/linux-x86_64/mad8s < ./testApps/IP_knob/knob_simu_non.mad';

for ii = 1:length(IP_Tknob_Name)
    fout2=fopen('./testApps/IP_knob/command.mad','w');
    fprintf(fout2,'%s \n',['set , ', IP_Tknob_Name{ii}, ' 1']);
    %% fprintf(fout2,'%s \n',['MFF2A']);
    status = fclose(fout2);
    [s,r]=system([MADdict,'&',MADcmd]);
    %% [s,r]=system('grep "Value of expression \"DKLSF6FF\" is:" .\testApps\IP_knob\ATF2s.echo');
    [s,r]=system('grep "Value of expression .SF6FF.K2.. is:" ./testApps/IP_knob/ATF2s.echo');
    ic=strfind(r,':');
    %% eval([strcat(IP_Tknob_Name{ii}, '_ratio(1)='),]);
    eval([strcat(IP_Tknob_Name{ii}, '_ratio'),'(1) =(',r(ic+1:end-1), '-k2(1))/k2(1);']);%%  end-1 makes the semicolon work
    %% [s,r]=system('grep "Value of expression \"DKLSF5FF\" is:" .\testApps\IP_knob\ATF2s.echo');
    [s,r]=system('grep "Value of expression .SF5FF.K2.. is:" ./testApps/IP_knob/ATF2s.echo');
    ic=strfind(r,':');
    eval([strcat(IP_Tknob_Name{ii}, '_ratio'),'(2) =(', r(ic+1:end-1), '-k2(2))/k2(2);']);
    %% [s,r]=system('grep "Value of expression \"DKLSD4FF\" is:" .\testApps\IP_knob\ATF2s.echo');
    [s,r]=system('grep "Value of expression .SD4FF.K2.. is:" ./testApps/IP_knob/ATF2s.echo');
    ic=strfind(r,':');
    eval([strcat(IP_Tknob_Name{ii}, '_ratio'), '(3) =(', r(ic+1:end-1), '-k2(3))/k2(3);']);
    %% [s,r]=system('grep "Value of expression \"DKLSF1FF\" is:" .\testApps\IP_knob\ATF2s.echo');
    [s,r]=system('grep "Value of expression .SF1FF.K2.. is:" ./testApps/IP_knob/ATF2s.echo');
    ic=strfind(r,':');
    eval([strcat(IP_Tknob_Name{ii}, '_ratio'), '(4) =(', r(ic+1:end-1), '-k2(4))/k2(4);']);
    %% [s,r]=system('grep "Value of expression \"DKLSD0FF\" is:" .\testApps\IP_knob\ATF2s.echo');
    [s,r]=system('grep "Value of expression .SD0FF.K2.. is:" ./testApps/IP_knob/ATF2s.echo');
    ic=strfind(r,':');
    eval([strcat(IP_Tknob_Name{ii}, '_ratio'), '(5) =(', r(ic+1:end-1), ' -k2(5))/k2(5);']);
end

%% for ii = 1:length(IP_Tknob_Name)
%%     eval([strcat(IP_Tknob_Name{ii}, '_ratio'), ' = ',strcat(IP_Tknob_Name{ii}, '_ratio'),'./k2;'])
%% end
%% %% knobname_list = {'IP_T122', 'IP_T162', 'IP_T166', 'IP_T342', 'IP_T364'}
%% disp(knobname)
%% switch upper(knobname)
%%    case 'IP_T122'
%%        [MagRatioSet] = knob_set(SextName, IP_T122_ratio, knob_value);
%%    case 'IP_T162'
%%        [MagRatioSet] = knob_set(SextName, IP_T162_ratio, knob_value);
%%    case 'IP_T166'
%%        [MagRatioSet] = knob_set(SextName, IP_T166_ratio, knob_value);
%%    case 'IP_T342'
%%        [MagRatioSet] = knob_set(SextName, IP_T342_ratio, knob_value);
%%    case 'IP_T364'
%%        [MagRatioSet] = knob_set(SextName, IP_T364_ratio, knob_value);
%%    otherwise
%%        disp('Please use one of the following knob names:');
%%        disp({'IP_T122'; 'IP_T162'; 'IP_T166'; 'IP_T342'; 'IP_T364'});
%% end
IP_KNOBV{4}.Name = 'T122';
IP_KNOBV{5}.Name = 'T162';
IP_KNOBV{6}.Name = 'T166';
IP_KNOBV{7}.Name = 'T342';
IP_KNOBV{8}.Name = 'T364';
IP_KNOBV{4}.Unit = ps_step/min(abs(IP_T122_ratio)); 
IP_KNOBV{5}.Unit = ps_step/min(abs(IP_T162_ratio)); 
IP_KNOBV{6}.Unit = ps_step/min(abs(IP_T166_ratio)); 
IP_KNOBV{7}.Unit = ps_step/min(abs(IP_T342_ratio)); 
IP_KNOBV{8}.Unit = ps_step/min(abs(IP_T364_ratio)); 
IP_KNOBV{4}.Ratio(:,1) = IP_T122_ratio'*IP_KNOBV{4}.Unit;
IP_KNOBV{5}.Ratio(:,1) = IP_T162_ratio'*IP_KNOBV{5}.Unit;
IP_KNOBV{6}.Ratio(:,1) = IP_T166_ratio'*IP_KNOBV{6}.Unit;
IP_KNOBV{7}.Ratio(:,1) = IP_T342_ratio'*IP_KNOBV{7}.Unit;
IP_KNOBV{8}.Ratio(:,1) = IP_T364_ratio'*IP_KNOBV{8}.Unit;
IP_KNOBV{4}.Ratio(:,2:4) = zeros(length(IP_KNOBV{4}.Ratio(:,1)),3);
IP_KNOBV{5}.Ratio(:,2:4) = zeros(length(IP_KNOBV{5}.Ratio(:,1)),3);
IP_KNOBV{6}.Ratio(:,2:4) = zeros(length(IP_KNOBV{6}.Ratio(:,1)),3);
IP_KNOBV{7}.Ratio(:,2:4) = zeros(length(IP_KNOBV{7}.Ratio(:,1)),3);
IP_KNOBV{8}.Ratio(:,2:4) = zeros(length(IP_KNOBV{8}.Ratio(:,1)),3);

IP_KNOBV{4}.Ind(:,1) = S_Ps ;
IP_KNOBV{5}.Ind(:,1) = S_Ps ;
IP_KNOBV{6}.Ind(:,1) = S_Ps ;
IP_KNOBV{7}.Ind(:,1) = S_Ps ;
IP_KNOBV{8}.Ind(:,1) = S_Ps ;
IP_KNOBV{4}.Ind(:,2) = S_Gir ;
IP_KNOBV{5}.Ind(:,2) = S_Gir ;
IP_KNOBV{6}.Ind(:,2) = S_Gir ;
IP_KNOBV{7}.Ind(:,2) = S_Gir ;
IP_KNOBV{8}.Ind(:,2) = S_Gir ;
%% %%  save .\testApps\IP_knob\KNOB_VALUE SextName IP_T122_ratio IP_T162_ratio IP_T166_ratio IP_T342_ratio IP_T364_ratio -append;  
%% save ./testApps/IP_knob/KNOB_VALUE IP_KNOBV;
end




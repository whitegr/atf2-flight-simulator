
function [mover_value mover_value] = set_mover_pos(S_Gir, mover_value )

global GIRDER

for i = 1:length(S_Gir)
  GIRDER(S_Gir(i)).MoverPos = mover_value(i,:); 
end
    
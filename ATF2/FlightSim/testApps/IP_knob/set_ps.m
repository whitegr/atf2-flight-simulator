function [ps_newread, stat] = set_ps(ps,ps_newvalue)
%SET_PS Summary of this function goes here
%   Detailed explanation goes here

global PS BEAMLINE;

%%!! request{1}=[];
%%!! request{2}=ps;
%%!! request{3}=[];
request={[] ps []}; 

%% [stat resp] = AccessRequest(request);
%% if(stat{1}==-1)
%%    disp(sprintf('AccessRequest returned error :%s',stat{2}));
%% else
%%    disp(sprintf('Access granted, ID of request : %s',resp));
%% end
%!!  Request access to hardware attached to Lucretia elements (request sent to
%!!  FS server program, response from operator required before function
%!! returns)
%!! request{1}(...) = read+write privs requested for mover on GIRDER{...}
%!! request{2}(...) = read+write privs requested for power supply PS(...)
%!! request{3}(...) = read privs requested for instrument INSTR{...}
%!! resp = ID of request (reqID)

nps=getcolumn(size(ps),2);
FlHwUpdate; 

for i=1:nps
    PS(ps(i)).SetPt=ps_newvalue(i);
end

[stat, I] = PSTrim(ps, true);
if(stat{1}==-1)
    disp(sprintf('PSTrim returned error :%s',stat{2}));
end
pause(1);
FlHwUpdate(request);
ps_newread=zeros(1,nps);
for i=1:nps
    ps_newread(i)=PS(ps(i)).SetPt;
end
if ( ~isempty( find(abs((ps_newvalue-ps_newread)./ps_newvalue)>=.05,1) ) )
    disp('repeat PSTrim, PS.SetPt and PS.Ampl does not match at 5% level');
    [stat, I] = PSTrim(ps, true);
    if(stat{1}==-1)
        disp(sprintf('PSTrim returned error :%s',stat{2}));
    end
    pause(1);
    FlHwUpdate(request);
    for i=1:nps
        ps_newread(i)=PS(ps(i)).SetPt;
    end
else
    pause(1);
    FlHwUpdate(request);
    for i=1:nps
        ps_newread(i)=PS(ps(i)).SetPt;
    end
end
end
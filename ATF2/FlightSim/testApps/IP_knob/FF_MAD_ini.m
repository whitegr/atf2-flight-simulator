function [k1, k2 ] = FF_MAD_ini
%%% 
%% Read current main power supply setting of Final Focus as inital lattice
%%

global FL BEAMLINE PS
E0=FL.SimModel.Initial.Momentum;
Cb=1e9/299792458; % rigidity constant (T-m/GeV)
brho=Cb*E0; % rigidity (T-m)
%% begff = findcells(BEAMLINE,'Name','BEGFF');

fini=fopen('.r\testApps\P_knob\ATF2_ini.mad','w');
%% fprintf(fini,'  SET, E0, %s\n',madval(E0));
%% fprintf(fini,'  SET,   TBETX  , %s\n', num2str(FL.SimModel.Twiss.betax(begff)));
%% fprintf(fini,'  SET,   TALFX  , %s\n', num2str(FL.SimModel.Twiss.alphax(begff));
%% fprintf(fini,'  SET,   TDX    , %s\n', num2str(FL.SimModel.Twiss.etax(begff)));
%% fprintf(fini,'  SET,   TDPX   , %s\n', num2str(FL.SimModel.Twiss.etapx(begff)));
%% fprintf(fini,'  SET,   TBETY  , %s\n', num2str(FL.SimModel.Twiss.betay(begff)));
%% fprintf(fini,'  SET,   TALFY  , %s\n', num2str(FL.SimModel.Twiss.alphay(begff)));
%% fprintf(fini,'  SET,   TDY    , %s\n', num2str(FL.SimModel.Twiss.etay(begff)));
%% fprintf(fini,'  SET,   TDPY   , %s\n', num2str(FL.SimModel.Twiss.etapy(begff)));
fprintf(fini,'  SET, E0, %s\n',madval(E0));
fprintf(fini,'  SET,   TBETX  , %s\n', num2str(FL.SimModel.Initial_IEX.x.Twiss.beta));
fprintf(fini,'  SET,   TALFX  , %s\n', num2str(FL.SimModel.Initial_IEX.x.Twiss.alpha));
fprintf(fini,'  SET,   TDX    , %s\n', num2str(FL.SimModel.Initial_IEX.x.Twiss.eta));
fprintf(fini,'  SET,   TDPX   , %s\n', num2str(FL.SimModel.Initial_IEX.x.Twiss.etap));
fprintf(fini,'  SET,   TBETY  , %s\n', num2str(FL.SimModel.Initial_IEX.y.Twiss.beta));
fprintf(fini,'  SET,   TALFY  , %s\n', num2str(FL.SimModel.Initial_IEX.y.Twiss.alpha));
fprintf(fini,'  SET,   TDY    , %s\n', num2str(FL.SimModel.Initial_IEX.y.Twiss.eta));
fprintf(fini,'  SET,   TDPY   , %s\n', num2str(FL.SimModel.Initial_IEX.y.Twiss.etap));

Q_ind = findcells(BEAMLINE, 'Class', 'QUAD', FL.SimModel.ffStart.ind,length(BEAMLINE));
S_ind = findcells(BEAMLINE, 'Class', 'SEXT', FL.SimModel.ffStart.ind,length(BEAMLINE));
for ii = 1:length(Q_ind)/2
    k1(ii) = BEAMLINE{Q_ind(2*ii)}.B*PS(BEAMLINE{Q_ind(2*ii)}.PS).Ampl/brho/BEAMLINE{Q_ind(2*ii)}.L;
    fprintf(fini,'%s \n',[BEAMLINE{Q_ind(2*ii)}.Name, ': QUADRUPOLE, L=', num2str(BEAMLINE{Q_ind(2*ii)}.L,8), ', K1=', num2str(k1(ii),12), ', APERTURE=0.016']);
end
%% SD4FF: SEXTUPOLE, L=0.050039016, K2=1.490537423558E2, APERTURE=0.020638
%% QM16FF: QUADRUPOLE, L=0.099245, K1=2.970747102675, APERTURE=0.016
for ii = 1:length(S_ind)/2
    k2(ii) = BEAMLINE{S_ind(2*ii)}.B*PS(BEAMLINE{S_ind(2*ii)}.PS).Ampl/brho/BEAMLINE{S_ind(2*ii)}.L;
    fprintf(fini,'%s \n',[BEAMLINE{S_ind(2*ii)}.Name, ': SEXTUPOLE, L=', num2str(BEAMLINE{S_ind(2*ii)}.L,8), ', K2=', num2str(k2(ii),12), ', APERTURE=0.020638']);
end   
status = fclose(fini);
          

end

    

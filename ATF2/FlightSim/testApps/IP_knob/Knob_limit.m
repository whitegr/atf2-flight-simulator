function [max_pv, max_nv, min_pv, min_nv] = knob_limit(SextName, IP_knob_mover)
%%% knob_limit: Calculate the limits of the sextupole dispalcement knob
%% Input: The sextupole name and a set of knob mover value in unit of 1
%% Output: The maximum positive and negative, the minimum positive and negative 
%% knob value in unit of meter or 1.  
%% The calculation is based on maximum mover plus minus 1.6 mm. 
%% minimum mover step of 1e-6 m. 
%% The original displacement of the sextupole has been substracted.
%% Example: [max_pv, max_nv, min_pv, min_nv]=knob_limit({'SF6FF';'SD4FF'}, (0.05; -0.6))
%% 
%% Auther: Min-Huey Wang SLAC National Accelerator Laboratory
%% Date: April 2009 

global FL BEAMLINE PS GIRDER
for i = 1:length(SextName)
    ind = findcells(BEAMLINE,'Name',SextName{i});
    mover_oldread(i,:)=GIRDER{BEAMLINE{ind(1)}.Girder}.MoverPos;    
end
ind1 = find(IP_knob_mover~=0)
indn1 = find(IP_knob_mover(ind1) < 0);
maxp = (1.6e-3-mover_oldread(ind1,1))./IP_knob_mover(ind1);
maxn = (-1.6e-3-mover_oldread(ind1,1))./IP_knob_mover(ind1);
minp = 1.0e-6./IP_knob_mover(ind1);
minn = -1.0e-6./IP_knob_mover(ind1);
if (~isempty(indn1))
    temp = maxp(indn1);
    maxp(indn1) = maxn(indn1);
    maxn(indn1)= temp;
    [max_pv,a1] = min(maxp);
    [max_nv,b1] = min(abs(maxn));
    
    temp = minp(indn1);
    minp(indn1) = minn(indn1);
    minn(indn1)= temp;
    [min_pv,c1,] = max(minp);
    [min_nv,d1,] = max(abs(minn));
else
    [max_pv,a1] = min(maxp);
    [max_nv,b1] = min(abs(maxn));
    [min_pv,c1,] = max(minp);
    [min_nv,d1,] = max(abs(minn));
end
max_nv = -max_nv;
min_nv = -min_nv;

end
for i = 1:length(SextName)
    ind = findcells(BEAMLINE,'Name',SextName{i});
    mover_oldread(i,:)=GIRDER{BEAMLINE{ind(1)}.Girder}.MoverPos;    
end
%% Check the knob limitation, check the limitation number
%% 0+- 1.6 mm, 0+- 50 mrad
ind1 = find(IP_Beta_x_Waist_mover~=0)
indn1 = find(IP_Beta_x_Waist_mover(ind1) < 0);
maxp = (1.6e-3-mover_oldread(ind1,1))./IP_Beta_x_Waist_mover(ind1);
maxn = (-1.6e-3-mover_oldread(ind1,1))./IP_Beta_x_Waist_mover(ind1);
minp = 1.0e-6./IP_Beta_x_Waist_mover(ind1);
minn = -1.0e-6./IP_Beta_x_Waist_mover(ind1);
if (~isempty(indn1))
    temp = maxp(indn1);
    maxp(indn1) = maxn(indn1);
    maxn(indn1)= temp;
    [max_psx,a1] = min(maxp);
    [max_nsx,b1] = min(abs(maxn));
    
    temp = minp(indn1);
    minp(indn1) = minn(indn1);
    minn(indn1)= temp;
    [min_psx,c1,] = max(minp);
    [min_nsx,d1,] = max(abs(minn));
else
    [max_psx,a1] = min(maxp);
    [max_nsx,b1] = min(abs(maxn));
    [min_psx,c1,] = max(minp);
    [min_nsx,d1,] = max(abs(minn));
end

ind1 = find(IP_Beta_y_Waist_mover~=0);
indn1 = find(IP_Beta_y_Waist_mover(ind1) < 0);
maxp = (1.6e-3-mover_oldread(ind1,2))./IP_Beta_y_Waist_mover(ind1);
maxn = (-1.6e-3-mover_oldread(ind1,2))./IP_Beta_y_Waist_mover(ind1);
minp = 1.0e-6./IP_Beta_y_Waist_mover(ind1);
minn = -1.0e-6./IP_Beta_y_Waist_mover(ind1);

if (~isempty(indn1))
    temp = maxp(indn1);
    maxp(indn1) = maxn(indn1);
    maxn(indn1)= temp;
    [max_psy,a1] = min(maxp);
    [max_nsy,b1] = min(abs(maxn));
    
    temp = minp(indn1);
    minp(indn1) = minn(indn1);
    minn(indn1)= temp;
    [min_psy,c1] = max(minp);
    [min_nsy,d1] = max(abs(minn));
else
    [max_psx,a1] = min(maxp);
    [max_nsx,b1] = min(abs(maxn));
    [min_psx,c1,] = max(minp);
    [min_nsx,d1,] = max(abs(minn));
end
    
ind1 = find(IP_Eta_x_mover~=0);
indn1 = find(IP_Eta_x_mover(ind1) < 0);
maxp = (1.6e-3-mover_oldread(ind1,3))./IP_Eta_x_mover(ind1);
maxn = (-1.6e-3-mover_oldread(ind1,3))./IP_Eta_x_mover(ind1);
minp = 1.0e-6./IP_Eta_x_mover(ind1);
minn = -1.0e-6./IP_Eta_x_mover(ind1);

if (~isempty(indn1))
    temp = maxp(indn1);
    maxp(indn1) = maxn(indn1);
    maxn(indn1)= temp;
    [max_petax,a1] = min(maxp);
    [max_netax,b1] = min(abs(maxn));
    
    temp = minp(indn1);
    minp(indn1) = minn(indn1);
    minn(indn1)= temp;
    [min_petax,c1] = max(minp);
    [min_netax,d1] = max(abs(minn));
else
    [max_psx,a1] = min(maxp);
    [max_nsx,b1] = min(abs(maxn));
    [min_psx,c1,] = max(minp);
    [min_nsx,d1,] = max(abs(minn));
end

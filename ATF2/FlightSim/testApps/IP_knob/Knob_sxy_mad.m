function [IP_KNOBV] = Knob_sxy_mad(SextName, varargin)
%%% 
%%%
%% Construct sextupole mover posotion knob to adjust the beta x, y waist, dispersion x,y and coupling using MAD. 
%% [IP_KNOBV] = Knob_sxy_mad(SextName, varargin )
%% SextName: The name of quadrupoles to construct the knob
%% IP_KNOBV: The output of the knob data in cell with three fields.
%%     IP_KNOBV{:}.Name: The name of the knob.
%%     IP_KNOBV{:}.Unit: The unit of the knob value coresponding to the
%%     minimum adjusting step of power supply 10e-4 or Sext mover 10e-6 m. 
%%     IP_KNOBV{:}.Ratio: The ratio of the power supply or mover postion to
%%     change the knob value in unit value. 
%% 
%% Exampe 
%%
%% SextName = {'SF6FF'; 'SF5FF'; 'SD4FF'; 'SF1FF'; 'SD0FF'};
%% IP_KNOBV{12}.Name = 'Eta_y';
%% IP_KNOBV{12}.Unit = 3.2061e-008
%% IP_KNOBV{12}.Ratio = 
%% 
%% Auther: Min-Huey Wang SLAC National Accelerator Laboratory
%% Date: Dec 2009 

global  BEAMLINE GIRDER

load ./testApps/IP_knob/KNOB_VALUE
mover_step = 1e-6; 
MADdict='set dict= .\..\..\..\..\..\ATF2\mad8.51\mad8.dict';
MADcmd='.\..\..\..\..\..\ATF2\mad8.51\cygwin-x86\mad8s.exe < .\testApps\IP_knob\knob_simu.mad';
%% %% MADdict='set dict= ./../../../../../ATF2/mad8.51/mad8.dict';
%% %% MADcmd='./../../../../../ATF2/mad8.51/linux-x86_64/mad8s.exe < ./testApps/IP_knob/knob_simu.mad';
%% MADdict='ln -s ${CSOFT}/../../mad8.51/mad8.dict dict';
%% MADcmd='${CSOFT}/../../mad8.51/linux-x86_64/mad8s < ./testApps/IP_knob/knob_simu.mad';

[k1,k2] = FF_MAD_ini;

if nargin>=2 
     ds = (-varargin{1}*mover_step : mover_step :varargin{1}*mover_step );
else

   %% ds = (1e-5:5e-6:1.0e-4);
   %% ds = (0:1e-6:5.0e-5);
   ds = (-5.0e-5:1e-6:5.0e-5);
end

%% ds = [5e-5:5e-5:1e-3,1.5e-3:5e-4:1e-2]; %% if the ds = [5e-5:5e-5:1e-2];
l_ds = length(ds);
l_SName = length(SextName); 

Arr0 = zeros(l_SName,1); 
S_Ps = zeros(l_SName, 1);
S_Gir = zeros(l_SName, 1);
Smover_ini = zeros(l_SName,3);
for i = 1:l_SName
    ind = findcells(BEAMLINE,'Name',SextName{i});
    S_Ps(i) = BEAMLINE{ind(1)}.PS;
    S_Gir(i) = BEAMLINE{ind(1)}.Girder;
    Smover_ini(i,:)=GIRDER{S_Gir(i)}.MoverPos;
end
BX = zeros(1,l_ds);
ALX = zeros(1,l_ds);
BX = zeros(1,l_ds);
ALX = zeros(1,l_ds);
SXBX = zeros(1,l_ds);
SXBY = zeros(1,l_ds);
SXETX = zeros(1,l_ds);
SXBX_r = zeros(l_SName,2);
SXBY_r = zeros(l_SName,2);
SXETX_r = zeros(l_SName,2);
SXBX_r2 = zeros(l_SName,3);
SXBY_r2 = zeros(l_SName,3);
SXETX_r2 = zeros(l_SName,3);
for ii = 1:l_SName
    for jj = 1:l_ds
%%        fid=fopen('.\testApps\IP_knob\Sext_ds.mad','w');
        fid=fopen('./testApps/IP_knob/Sext_ds.mad','w');
        fprintf(fid,'%s \n',['SElect ERROR CLEAR; SELect ERROR ',SextName{ii},'[1]; EALIGN, DX= (', num2str(sign(ds(jj)+Smover_ini(ii,1))), ')*',num2str(abs(ds(jj)+Smover_ini(ii,1))) ]);
        fprintf(fid,'%s \n',['SElect ERROR CLEAR; SELect ERROR ',SextName{ii},'[2]; EALIGN, DX= (', num2str(sign(ds(jj)+Smover_ini(ii,1))), ')*',num2str(abs(ds(jj)+Smover_ini(ii,1))) ]);
        %% fprintf(fid,'%s \n',['RERTURN']);
        status = fclose(fid);
        [s,r]=system([MADdict,'&',MADcmd]);
        %% [s,r]=system('grep "Value of expression \"BX\" is:" .\testApps\IP_knob\ATF2s.echo');
        [s,r]=system('grep "Value of expression .BX. is:" ./testApps/IP_knob/ATF2s.echo');
        ic=strfind(r,':');
        BX(jj)=str2double(r(ic+1:end));
       %%  [s,r]=system('grep "Value of expression \"ALX\" is:" .\testApps\IP_knob\ATF2s.echo');
        [s,r]=system('grep "Value of expression .ALX. is:" ./testApps/IP_knob/ATF2s.echo');
        ic=strfind(r,':');
        ALX(jj)=str2double(r(ic+1:end));
       %%  [s,r]=system('grep "Value of expression \"BY\" is:" .\testApps\IP_knob\ATF2s.echo');
        [s,r]=system('grep "Value of expression .BY. is:" ./testApps/IP_knob/ATF2s.echo');
        ic=strfind(r,':');
        BY(jj)=str2double(r(ic+1:end));
       %%  [s,r]=system('grep "Value of expression \"ALY\" is:" .\testApps\IP_knob\ATF2s.echo');
        [s,r]=system('grep "Value of expression .ALY. is:" ./testApps/IP_knob/ATF2s.echo');
        ic=strfind(r,':');
        ALY(jj)=str2double(r(ic+1:end));
       %%  [s,r]=system('grep "Value of expression \"SXBX\" is:" .\testApps\IP_knob\ATF2s.echo');
        [s,r]=system('grep "Value of expression .SXBX. is:" ./testApps/IP_knob/ATF2s.echo');
        ic=strfind(r,':');
        SXBX(jj)=str2double(r(ic+1:end));
       %%  [s,r]=system('grep "Value of expression \"SXBY\" is:" ./testApps/IP_knob/ATF2s.echo');
        [s,r]=system('grep "Value of expression .SXBY. is:" ./testApps/IP_knob/ATF2s.echo');
        ic=strfind(r,':');
        SXBY(jj)=str2double(r(ic+1:end));
       %% [s,r]=system('grep "Value of expression \"SXETX\" is:" ./testApps/IP_knob/ATF2s.echo');
        [s,r]=system('grep "Value of expression .SXETX. is:" ./testApps/IP_knob/ATF2s.echo');
        ic=strfind(r,':');
        SXETX(jj)=str2double(r(ic+1:end));
    end
    SXBX_r(ii,:) = polyfit(ds,SXBX,1);
    SXBY_r(ii,:) = polyfit(ds,SXBY,1);
    SXETX_r(ii,:) = polyfit(ds,SXETX,1);
    SXBX_r2(ii,:) = polyfit(ds,SXBX,2);
    SXBY_r2(ii,:) = polyfit(ds,SXBY,2);
    SXETX_r2(ii,:) = polyfit(ds,SXETX,2);
end
%% test the fitting
%% plot(ds, SXBX)
%% hold on
%% plot(ds, polyval(SXBX_r(end,:), ds),'ro')
%% plot(ds, polyval(SXBX_r2(end,:), ds),'go')
%% plot(ds, polyval(SXBX_r2(end,2:3), ds),'mo')
%% hold off
fid=fopen('./testApps/IP_knob/Sext_ds.mad','w');
fprintf(fid,'%s \n',['SElect ERROR CLEAR; SELect ERROR ',SextName{ii},'[1]; EALIGN, DX=',num2str(Smover_ini(ii,1))]);
fprintf(fid,'%s \n',['SElect ERROR CLEAR; SELect ERROR ',SextName{ii},'[2]; EALIGN, DX=',num2str(Smover_ini(ii,1))]);
status = fclose(fid);

%% testf = @(x1,x2,x3,x4,x5)100*(SXBX_r2(1,1)*x1^2+SXBX_r2(1,2)*x1+SXBX_r2(1,3)+ ...
%%                               SXBX_r2(2,1)*x2^2+SXBX_r2(2,2)*x2+SXBX_r2(2,3)+ ...
%%                               SXBX_r2(3,1)*x3^2+SXBX_r2(3,2)*x3+SXBX_r2(3,3)+ ...
%%                               SXBX_r2(4,1)*x4^2+SXBX_r2(4,2)*x4+SXBX_r2(4,3)+ ...
%%                               SXBX_r2(5,1)*x5^2+SXBX_r2(5,2)*x5+SXBX_r2(5,3)-1);
%% [x,fval] = fminsearch(testf, [-1.2, 1], ...
%%    optimset('TolX',1e-8));
AA = [SXBX_r(:,1)'; SXBY_r(:,1)'; SXETX_r(:,1)'];
%% IP_Beta_x_Waist_mover = AA\[1;0;0];%% range 
%% IP_Beta_y_Waist_mover = AA\[0;1;0];
%% IP_Eta_x_mover = AA\[0;0;1];
IP_Beta_x_Waist_mover = [ AA\[1;0;0],   Arr0, Arr0];
IP_Beta_y_Waist_mover = [ AA\[0;1;0],   Arr0, Arr0];
IP_Eta_x_mover = [AA\[0;0;1],   Arr0, Arr0];
%% ds = (1e-5:1e-5:1.0e-3);
%% l_ds = length(l_ds);
RR13=zeros(1,l_ds); RR14 =zeros(1,l_ds);  RR23=zeros(1,l_ds); RR24=zeros(1,l_ds);
SXETY = zeros(1,l_ds);
SXETY_r = zeros(l_SName,2);
SXETY_r2 = zeros(l_SName,3);
RR13_r = zeros(l_SName,2);
RR13_r2 = zeros(l_SName,3);
RR14_r = zeros(l_SName,2);
RR14_r2 = zeros(l_SName,3);
RR23_r = zeros(l_SName,2);
RR23_r2 = zeros(l_SName,3);
RR24_r = zeros(l_SName,2);
RR24_r2 = zeros(l_SName,3);
for ii = 1:l_SName
    for jj = 1:l_ds
%%        fid=fopen('.\testApps\IP_knob\Sext_ds.mad','w');
        fid=fopen('./testApps/IP_knob/Sext_ds.mad','w');
        fprintf(fid,'%s \n',['SElect ERROR CLEAR; SELect ERROR ',SextName{ii},'[1]; EALIGN, DY= (', num2str(sign(ds(jj)+Smover_ini(ii,1))), ')*',num2str(abs(ds(jj)+Smover_ini(ii,2))) ]);
        fprintf(fid,'%s \n',['SElect ERROR CLEAR; SELect ERROR ',SextName{ii},'[2]; EALIGN, DY= (', num2str(sign(ds(jj)+Smover_ini(ii,1))), ')*',num2str(abs(ds(jj)+Smover_ini(ii,2))) ]);
        %% fprintf(fid,'%s \n',['RERTURN']);
        status = fclose(fid);
        [s,r]=system([MADdict,'&',MADcmd]);
        %% [s,r]=system('grep "Value of expression \"SXETY\" is:" ./testApps/IP_knob/ATF2s.echo');
        [s,r]=system('grep "Value of expression .SXETY. is:" ./testApps/IP_knob/ATF2s.echo');
        ic=strfind(r,':');
        SXETY(jj)=str2double(r(ic+1:end));
        fr=fopen('./testApps/IP_knob/ATF2s.print','r');
        for kk=1:101
            tline = fgetl(fr);
        end
        a = textscan(fr, '%f %f %f %f %f %f', 1);
        RR13(jj) = a{3};
        RR14(jj) = a{4};
        a = textscan(fr, '%f %f %f %f %f %f', 1);
        RR23(jj) = a{3};
        RR24(jj) = a{4};
        status = fclose(fr);
    end
 %%   SXBX_r(ii,:) = polyfit(ds*k2(ii),SXBX,1);
 %%   SXBY_r(ii,:) = polyfit(ds*k2(ii),SXBY,1);
 %%   SXETX_r(ii,:) = polyfit(ds*k2(ii),SXETX,1);
 %%   SXBX_r2(ii,:) = polyfit(ds*k2(ii),SXBX,2);
 %%   SXBY_r2(ii,:) = polyfit(ds*k2(ii),SXBY,2);
 %%   SXETX_r2(ii,:) = polyfit(ds*k2(ii),SXETX,2);
    SXETY_r(ii,:) = polyfit(ds,SXETY,1);
    SXETY_r2(ii,:) = polyfit(ds,SXETY,2);
    RR13_r(ii,:) = polyfit(ds,RR13,1);
    RR13_r2(ii,:) = polyfit(ds,RR13,2);
    RR14_r(ii,:) = polyfit(ds,RR14,1);
    RR14_r2(ii,:) = polyfit(ds,RR14,2);
    RR23_r(ii,:) = polyfit(ds,RR23,1);
    RR23_r2(ii,:) = polyfit(ds,RR23,2);
    RR24_r(ii,:) = polyfit(ds,RR24,1);
    RR24_r2(ii,:) = polyfit(ds,RR24,2);
end
%% test the fitting
%% plot(ds, SXETY)
%% hold on
%% plot(ds, polyval(SXETY_r(end,:), ds),'ro')
%% plot(ds, polyval(SXETY_r2(end,:), ds),'go')
%% plot(ds, polyval(SXETY_r2(end,2:3), ds),'mo')
%% hold off
fid=fopen('.\testApps\IP_knob\Sext_ds.mad','w');
fprintf(fid,'%s \n',['SElect ERROR CLEAR; SELect ERROR ',SextName{ii},'[1]; EALIGN, DY=',num2str(Smover_ini(ii,2))]);
fprintf(fid,'%s \n',['SElect ERROR CLEAR; SELect ERROR ',SextName{ii},'[2]; EALIGN, DY=',num2str(Smover_ini(ii,2))]);
status = fclose(fid);
AA = [SXETY_r(:,1)'; RR13_r(:,1)'; RR14_r(:,1)'; RR23_r(:,1)'; RR24_r(:,1)'];
%% IP_Eta_y_mover = AA\[1;0;0;0;0]; %% range 
%% IP_R13_mover = AA\[0;1;0;0;0];
%% IP_R14_mover = AA\[0;0;1;0;0];
%% IP_R23_mover = AA\[0;0;0;1;0];
%% IP_R24_mover = AA\[0;0;0;0;1];
IP_Eta_y_mover = [Arr0,  AA\[1;0;0;0;0], Arr0];
IP_R13_mover = [Arr0,  AA\[0;1;0;0;0], Arr0];
IP_R14_mover = [Arr0,  AA\[0;0;1;0;0], Arr0];
IP_R23_mover = [Arr0,  AA\[0;0;0;1;0], Arr0];
IP_R24_mover = [Arr0,  AA\[0;0;0;0;1], Arr0]; 
mover_all = [IP_Beta_x_Waist_mover; IP_Beta_y_Waist_mover; IP_Eta_x_mover; ...
    IP_Eta_y_mover; IP_R13_mover; IP_R14_mover; IP_R23_mover; IP_R24_mover];

%%  switch lower(knobname)
%%   case {'ip_beta_x_waist'}
%%       [mover_oldread]=set_mover(SextName, IP_Beta_x_Waist_mover'*knob_value);
%%       [max_pv, max_nv, min_pv, min_nv] = knob_limit(SextName, IP_Beta_x_Waist_mover);
%%   case {'ip_beta_y_waist'}
%%       [mover_oldread]=set_mover(SextName, IP_Beta_y_Waist_mover'*knob_value);
%%       [max_pv, max_nv, min_pv, min_nv] = knob_limit(SextName, IP_Beta_y_Waist_mover);
%%   case {'ip_eta_x'}
%%       [mover_oldread]=set_mover(SextName, IP_Eta_x_mover'*knob_value);
%%       [max_pv, max_nv, min_pv, min_nv] = knob_limit(SextName, IP_Eta_x_mover);
%%   case {'ip_eta_y'}
%%       [mover_oldread]=set_mover(SextName, IP_Eta_y_mover'*knob_value);
%%       [max_pv, max_nv, min_pv, min_nv] = knob_limit(SextName, IP_Eta_y_mover);
%%   case {'ip_r13'}
%%       [mover_oldread]=set_mover(SextName, IP_R13_mover'*knob_value);
%%       [max_pv, max_nv, min_pv, min_nv] = knob_limit(SextName, IP_R13_mover);
%%   case {'ip_r14'}
%%       [mover_oldread]=set_mover(SextName, IP_R14_mover'*knob_value);
%%       [max_pv, max_nv, min_pv, min_nv] = knob_limit(SextName, IP_R14_mover);
%%   case {'ip_r23'}
%%       [mover_oldread]=set_mover(SextName, IP_R23_mover'*knob_value);
%%       [max_pv, max_nv, min_pv, min_nv] = knob_limit(SextName, IP_R23_mover);
%%   case {'ip_r24'}
%%       [mover_oldread]=set_mover(SextName, IP_R24_mover'*knob_value);
%%       [max_pv, max_nv, min_pv, min_nv] = knob_limit(SextName, IP_R24_mover);
%%   otherwise
%%       disp('Please use one of the following knob names:');
%%       disp({'IP_Beta_x_Waist';'IP_Beta_y_Waist'; 'IP_Eta_x'; 'IP_Eta_y'; 'IP_R13' ; 'IP_R14' ; 'IP_R23' ; 'IP_R24'}  );
%% end
IP_KNOBV{9}.Name = 'Beta_x_Waist_m';
IP_KNOBV{10}.Name = 'Beta_y_Waist_m';
IP_KNOBV{11}.Name = 'Eta_x_m';
IP_KNOBV{12}.Name = 'Eta_y';
IP_KNOBV{13}.Name = 'R13';
IP_KNOBV{14}.Name = 'R14';
IP_KNOBV{15}.Name = 'R23';
IP_KNOBV{16}.Name = 'R24';
ind = (IP_Beta_x_Waist_mover(:,1)~=0);
IP_KNOBV{9}.Unit = mover_step/min(abs(IP_Beta_x_Waist_mover(ind,1))); 
ind = (IP_Beta_y_Waist_mover(:,1)~=0);
IP_KNOBV{10}.Unit = mover_step/min(abs(IP_Beta_y_Waist_mover(ind,1))); 
ind = (IP_Eta_x_mover(:,1)~=0);
IP_KNOBV{11}.Unit = mover_step/min(abs(IP_Eta_x_mover(ind,1)));
%% ind = find(IP_KNOBV{12}.Ratio(:,2)~=0)
ind = (IP_Eta_y_mover(:,2)~=0);
IP_KNOBV{12}.Unit = mover_step/min(abs(IP_Eta_y_mover(ind,2)));
%% ind = find(IP_KNOBV{13}.Ratio(:,2)~=0)
ind = (IP_R13_mover(:,2)~=0);
IP_KNOBV{13}.Unit = mover_step/min(abs(IP_R13_mover(ind,2)));
%% ind = find(IP_KNOBV{14}.Ratio(:,2)~=0)
ind = (IP_R14_mover(:,2)~=0);
IP_KNOBV{14}.Unit = mover_step/min(abs(IP_R14_mover(ind,2)));
%% ind = find(IP_KNOBV{15}.Ratio(:,2)~=0)
ind = (IP_R23_mover(:,2)~=0);
IP_KNOBV{15}.Unit = mover_step/min(abs(IP_R23_mover(ind,2))); 
%% ind = find(IP_KNOBV{16}.Ratio(:,2)~=0)
ind = (IP_R24_mover(:,2)~=0);
IP_KNOBV{16}.Unit = mover_step/min(abs(IP_R24_mover(ind,2)));
IP_KNOBV{9}.Ratio(:,2:4) = IP_Beta_x_Waist_mover*IP_KNOBV{9}.Unit;
IP_KNOBV{10}.Ratio(:,2:4) = IP_Beta_y_Waist_mover*IP_KNOBV{10}.Unit;
IP_KNOBV{11}.Ratio(:,2:4) = IP_Eta_x_mover*IP_KNOBV{11}.Unit;
IP_KNOBV{9}.Ratio(:,1) = zeros(length(IP_KNOBV{9}.Ratio(:,2)),1);
IP_KNOBV{10}.Ratio(:,1) = zeros(length(IP_KNOBV{10}.Ratio(:,2)),1);
IP_KNOBV{11}.Ratio(:,1) = zeros(length(IP_KNOBV{11}.Ratio(:,3)),1);
IP_KNOBV{12}.Ratio(:,2:4) = IP_Eta_y_mover*IP_KNOBV{12}.Unit;
IP_KNOBV{13}.Ratio(:,2:4) = IP_R13_mover*IP_KNOBV{13}.Unit;
IP_KNOBV{14}.Ratio(:,2:4) = IP_R14_mover*IP_KNOBV{14}.Unit;
IP_KNOBV{15}.Ratio(:,2:4) = IP_R23_mover*IP_KNOBV{15}.Unit;
IP_KNOBV{16}.Ratio(:,2:4) = IP_R24_mover*IP_KNOBV{16}.Unit;
IP_KNOBV{12}.Ratio(:,1) = zeros(length(IP_KNOBV{12}.Ratio(:,3)),1);
IP_KNOBV{13}.Ratio(:,1) = zeros(length(IP_KNOBV{13}.Ratio(:,3)),1);
IP_KNOBV{14}.Ratio(:,1) = zeros(length(IP_KNOBV{14}.Ratio(:,3)),1);
IP_KNOBV{15}.Ratio(:,1) = zeros(length(IP_KNOBV{15}.Ratio(:,3)),1);
IP_KNOBV{16}.Ratio(:,1) = zeros(length(IP_KNOBV{16}.Ratio(:,3)),1);
IP_KNOBV{9}.Ind(:,1) = S_Ps;
IP_KNOBV{10}.Ind(:,1) = S_Ps;
IP_KNOBV{11}.Ind(:,1) = S_Ps;
IP_KNOBV{12}.Ind(:,1) = S_Ps;
IP_KNOBV{13}.Ind(:,1) = S_Ps;
IP_KNOBV{14}.Ind(:,1) = S_Ps;
IP_KNOBV{15}.Ind(:,1) = S_Ps;
IP_KNOBV{16}.Ind(:,1) = S_Ps;
IP_KNOBV{9}.Ind(:,2) = S_Gir;
IP_KNOBV{10}.Ind(:,2) = S_Gir;
IP_KNOBV{11}.Ind(:,2) = S_Gir;
IP_KNOBV{12}.Ind(:,2) = S_Gir;
IP_KNOBV{13}.Ind(:,2) = S_Gir;
IP_KNOBV{14}.Ind(:,2) = S_Gir;
IP_KNOBV{15}.Ind(:,2) = S_Gir;
IP_KNOBV{16}.Ind(:,2) = S_Gir;

%% %% save .\testApps\mhtest\KNOB_VALUE SextValue IP_Beta_x_Waist_mover IP_Beta_y_Waist_mover IP_Eta_x_mover IP_Eta_y_mover ...
%% %%      IP_R13_mover IP_R14_mover IP_R23_mover IP_R24_mover -append; 
%% save ./testApps/IP_knob/KNOB_VALUE IP_KNOBV ;
end

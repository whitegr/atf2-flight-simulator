function [mover_oldread, mover_newread]=set_mover(girder,mover_newvalue)
%SET_Mover

    global BEAMLINE GIRDER;

    request = {girder' []  []};
    %% ngirder=getcolumn(size(girder),2);
    ngirder = length(girder);
    mover_oldread=zeros(ngirder,3);

    for i=1:ngirder
        GIRDER{girder(i)}.MoverSetPt=mover_newvalue(i,:);
    end

    %ask to change mover value
    stat=MoverTrim(girder, 3);
%%    stat=MoverTrim(girder, 3);  %% for sim mode only to gurantee the mover really moved 
    %% MoverTrim(girder, 3) ; mode 3 means there is block until the mover
    %% finish movment no need to pause. 
    %% stat=MoverTrim(girder, true);
    if(stat{1}==-1)
        disp(sprintf('MoverTrim returned error :%s',stat{2}));
    end
    pause(1);
    
    %update mover value
    FlHwUpdate(request);
    mover_newread=zeros(ngirder,3);
    for i=1:ngirder
        mover_newread(i,:)=GIRDER{girder(i)}.MoverSetPt;
    end

    %if more than 5um difference, ask again
    itteration=0;
    [bad_mover,bad_dim]=find(abs(mover_newvalue-mover_newread)>=5e-6);
%    while( ~isempty( bad_mover ) )
     while( 0 )
        itteration=itteration+1;
        %give up after 5 asks
        if(itteration>5)
            orientation={'x';'y';'roll'};
            for i=bad_mover
               disp(sprintf('Warning : %s of GIRDER %i is set at %f instead of %f.\n',orientation{i},girder(i),mover_newread(i,bad_dim(i)),mover_newvalue(i,bad_dim(i))));
            end
            break;
        end

        %ask to change different mover from what is wanted
        stat=MoverTrim(girder(bad_mover), 3);
%%        stat=MoverTrim(girder(bad_mover), 3);
        %%stat=MoverTrim(girder(bad_mover), true);
        if(stat{1}==-1)
            disp(sprintf('MoverTrim reurned error :%s',stat{2}));
        end
        pause(1);
        
        %update mover values
        request{1}=girder;
        FlHwUpdate(request);
        for i=1:ngirder
            mover_newread(i,:)=GIRDER{girder(i)}.MoverSetPt;
        end
        [bad_mover,bad_dim]=find(abs(mover_newvalue-mover_newread)>=5e-6);
    end
    
end

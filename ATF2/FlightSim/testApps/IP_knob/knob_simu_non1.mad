  ASSIGN, PRINT="./testApps/IP_knob/ATF2s.print"
  ASSIGN, ECHO="./testApps/IP_knob/ATF2s.echo"
  OPTION, -ECHO, -VERIFY, DOUBLE

! ==============================================================================
! initial conditions (entrance to DR extraction kicker)
! (NOTE: Twiss parameters from drdsgn07Dec03disp.txt, per S. Kuroda)
! ------------------------------------------------------------------------------

  E0d    := 1.542282 !design beam energy (GeV)
  E0     := 1.3      !nominal beam energy (GeV)
  EMITX  := 2.0E-9   !geometric horizontal emittance (m)
  EMITYn := 3.0E-8   !normalized vertical emittance (m)
  BLENG  := 8.0E-3   !bunch length (m)
  ESPRD  := 0.8E-3   !energy spread (1)
  TBETX  := 6.54290  !twiss beta x (m)
  TALFX  := 0.99638  !twiss alpha x
  TDX    := 0        !eta x (m)
  TDPX   := 0        !eta' x
  TBETY  := 3.23785  !twiss beta y (m)
  TALFY  := -1.9183  !twiss alpha y
  TDY    := 0        !eta y (m)
  TDPY   := 0        !eta' y

! ==============================================================================
! global parameters
! ------------------------------------------------------------------------------

  Cb      := 1.0E10/CLIGHT
  Brho    := Cb*E0
  betamax := 120

! ==============================================================================
! construct input beam matrix (assumes TDX=TDPX=TDY=TDPY=0)
! ------------------------------------------------------------------------------

  EMITXn := EMITX*(E0/EMASS)
  EMITY  := EMITYn/(E0/EMASS)

  BEAM, PARTICLE=ELECTRON, ENERGY=E0, EX=EMITX, EY=EMITY, &
        SIGT=BLENG, SIGE=ESPRD, NPART=2.0E+10

  TWSS0 : BETA0, BETX=TBETX, ALFX=TALFX, DX=TDX, DPX=TDPX, &
                 BETY=TBETY, ALFY=TALFY, DY=TDY, DPY=TDPY

  TGAMX := (1+TALFX*TALFX)/TBETX
  TGAMY := (1+TALFY*TALFY)/TBETY
  SIG11 := EMITX*TBETX + TDX*TDX*ESPRD*ESPRD
  SIG21 := -EMITX*TALFX + TDX*TDPX*ESPRD*ESPRD
  SIG22 := EMITX*TGAMX + TDPX*TDPX*ESPRD*ESPRD
  C21   := SIG21/SQRT(SIG11*SIG22)
  SIG33 := EMITY*TBETY
  SIG43 := -EMITY*TALFY
  SIG44 := EMITY*TGAMY
  SIG61 :=  TDX*ESPRD*ESPRD
  SIG62 :=  TDPX*ESPRD*ESPRD
  SIG66 :=  ESPRD*ESPRD
  C43   := SIG43/SQRT(SIG33*SIG44)
  SIG0  : SIGMA0, SIGX=SQRT(SIG11), SIGPX=SQRT(SIG22), R21=C21, &
                  SIGY=SQRT(SIG33), SIGPY=SQRT(SIG44), R43=C43, &
                  SIGT=BLENG      , SIGPT=ESPRD
 !! VALUE, TBETX, SIG11, SIG21, SIG22, C21, SIG33, SIG43, SIG44, C43,SIGMA0  
 
 SIGX0  := SQRT(SIG11)
 SIGY0  := SQRT(SIG33)
 SIGPX0 := SQRT(SIG22)
 SIGPY0 := SQRT(SIG44)
 
! IP beta (alpha=0)

 BXIP := 4.0e-3
 BYIP := 0.1e-3
 DPXIP := 1.394317949E-01
 
 SIGXIP  := SQRT(EMITX*BXIP)
 SIGYIP  := SQRT(EMITY*BYIP)
 SIGPXIP := SQRT(EMITX/BXIP + DPXIP*DPXIP*ESPRD*ESPRD)
 SIGPYIP := SQRT(EMITY/BYIP)
 
 !---------------------------------------------------------------------
! Weight factors for matching T-matrix start to IP.

 W1_SCA := 0.1
 
 W1_111 := W1_SCA * (SIGX0 * SIGX0 ) / SIGXIP    !   0.0046
 W1_121 := W1_SCA * (SIGX0 * SIGPX0) / SIGXIP    !   9.9819e-004
 W1_122 := W1_SCA * (SIGPX0* SIGPX0) / SIGXIP    !   2.1536e-004
 W1_133 := W1_SCA * (SIGY0 * SIGY0 ) / SIGXIP    !   1.3499e-005
 W1_143 := W1_SCA * (SIGY0 * SIGPY0) / SIGXIP    !   9.0192e-006
 W1_144 := W1_SCA * (SIGPY0* SIGPY0) / SIGXIP    !   6.0260e-006
 W1_161 := W1_SCA * (SIGX0 * BLENG ) / SIGXIP    !   0.3236
 W1_162 := W1_SCA * (SIGPX0* BLENG ) / SIGXIP    !   0.0698
 W1_166 := W1_SCA * (BLENG * BLENG ) / SIGXIP    !   22.6274

 W1_211 := W1_SCA * (SIGX0 * SIGX0 ) / SIGPXIP   !   1.8280e-005
 W1_221 := W1_SCA * (SIGX0 * SIGPX0) / SIGPXIP   !   3.9440e-006
 W1_222 := W1_SCA * (SIGPX0* SIGPX0) / SIGPXIP   !   8.5093e-007
 W1_233 := W1_SCA * (SIGY0 * SIGY0 ) / SIGPXIP   !   5.3337e-008
 W1_243 := W1_SCA * (SIGY0 * SIGPY0) / SIGPXIP   !   3.5636e-008
 W1_244 := W1_SCA * (SIGPY0* SIGPY0) / SIGPXIP   !   2.3810e-008
 W1_261 := W1_SCA * (SIGX0 * BLENG ) / SIGPXIP   !   0.0013
 W1_262 := W1_SCA * (SIGPX0* BLENG ) / SIGPXIP   !   2.7582e-004
 W1_266 := W1_SCA * (BLENG * BLENG ) / SIGPXIP   !   0.0894

 W1_331 := W1_SCA * (SIGX0 * SIGY0 ) / SIGYIP    !   0.0206
 W1_332 := W1_SCA * (SIGPX0* SIGY0 ) / SIGYIP    !   0.0044
 W1_341 := W1_SCA * (SIGX0 * SIGPY0) / SIGYIP    !   0.0138
 W1_342 := W1_SCA * (SIGPX0* SIGPY0) / SIGYIP    !   0.0030
 W1_363 := W1_SCA * (SIGY0 * BLENG ) / SIGYIP    !   1.4395
 W1_364 := W1_SCA * (SIGPY0* BLENG ) / SIGYIP    !   0.9618

 W1_431 := W1_SCA * (SIGX0 * SIGY0 ) / SIGPYIP   !   2.0584e-006
 W1_432 := W1_SCA * (SIGPX0* SIGY0 ) / SIGPYIP   !   4.4411e-007
 W1_441 := W1_SCA * (SIGX0 * SIGPY0) / SIGPYIP   !   1.3753e-006
 W1_442 := W1_SCA * (SIGPX0* SIGPY0) / SIGPYIP   !   2.9672e-007
 W1_463 := W1_SCA * (SIGY0 * BLENG ) / SIGPYIP   !   1.4395e-004
 W1_464 := W1_SCA * (SIGPY0* BLENG ) / SIGPYIP   !   9.6179e-005


 W1_131 := W1_SCA * (SIGX0 * SIGY0 ) / SIGXIP    !   2.4991e-004
 W1_132 := W1_SCA * (SIGPX0* SIGY0 ) / SIGXIP    !   5.3919e-005
 W1_141 := W1_SCA * (SIGX0 * SIGPY0) / SIGXIP    !   1.6697e-004
 W1_142 := W1_SCA * (SIGPX0* SIGPY0) / SIGXIP    !   3.6025e-005
 W1_163 := W1_SCA * (SIGY0 * BLENG ) / SIGXIP    !   0.0175
 W1_164 := W1_SCA * (SIGPY0* BLENG ) / SIGXIP    !   0.0117

 W1_231 := W1_SCA * (SIGX0 * SIGY0 ) / SIGPXIP   !   9.8743e-007
 W1_232 := W1_SCA * (SIGPX0* SIGY0 ) / SIGPXIP   !   2.1304e-007
 W1_241 := W1_SCA * (SIGX0 * SIGPY0) / SIGPXIP   !   6.5973e-007
 W1_242 := W1_SCA * (SIGPX0* SIGPY0) / SIGPXIP   !   1.4234e-007
 W1_263 := W1_SCA * (SIGY0 * BLENG ) / SIGPXIP   !   6.9055e-005
 W1_264 := W1_SCA * (SIGPY0* BLENG ) / SIGPXIP   !   4.6138e-005

 W1_311 := W1_SCA * (SIGX0 * SIGX0 ) / SIGYIP    !   0.3811
 W1_321 := W1_SCA * (SIGX0 * SIGPX0) / SIGYIP    !   0.0822
 W1_322 := W1_SCA * (SIGPX0* SIGPX0) / SIGYIP    !   0.0177
 W1_333 := W1_SCA * (SIGY0 * SIGY0 ) / SIGYIP    !   0.0011
 W1_343 := W1_SCA * (SIGY0 * SIGPY0) / SIGYIP    !   7.4288e-004
 W1_344 := W1_SCA * (SIGPY0* SIGPY0) / SIGYIP    !   4.9634e-004
 W1_361 := W1_SCA * (SIGX0 * BLENG ) / SIGYIP    !   26.6496
 W1_362 := W1_SCA * (SIGPX0* BLENG ) / SIGYIP    !   5.7498
 W1_366 := W1_SCA * (BLENG * BLENG ) / SIGYIP    !   1.8637e+003225.41

 W1_411 := W1_SCA * (SIGX0 * SIGX0 ) / SIGPYIP   !   3.8107e-005
 W1_421 := W1_SCA * (SIGX0 * SIGPX0) / SIGPYIP   !   8.2217e-006
 W1_422 := W1_SCA * (SIGPX0* SIGPX0) / SIGPYIP   !   1.7739e-006
 W1_433 := W1_SCA * (SIGY0 * SIGY0 ) / SIGPYIP   !   1.1119e-007
 W1_443 := W1_SCA * (SIGY0 * SIGPY0) / SIGPYIP   !   7.4288e-008
 W1_444 := W1_SCA * (SIGPY0* SIGPY0) / SIGPYIP   !   4.9634e-008
 W1_461 := W1_SCA * (SIGX0 * BLENG ) / SIGPYIP   !   0.0027
 W1_462 := W1_SCA * (SIGPX0* BLENG ) / SIGPYIP   !   5.7498e-004
 W1_466 := W1_SCA * (BLENG * BLENG ) / SIGPYIP   !   0.1864

 W1_511  := W1_SCA * (SIGX0 * SIGX0 ) / BLENG   !
 W1_512  := W1_SCA * (SIGX0 * SIGPX0) / BLENG   !
 W1_522  := W1_SCA * (SIGPX0* SIGPX0) / BLENG   !
 W1_533  := W1_SCA * (SIGY0 * SIGY0 ) / BLENG   !
 W1_534  := W1_SCA * (SIGY0 * SIGPY0) / BLENG   !
 W1_544  := W1_SCA * (SIGPY0* SIGPY0) / BLENG   !
 W1_561  := W1_SCA * (SIGX0 * BLENG ) / BLENG   !
 W1_562  := W1_SCA * (SIGPX0* BLENG ) / BLENG   !
 W1_566  := W1_SCA * (BLENG * BLENG ) / BLENG   !

!=====================================================================
! Desired 2nd order matrix terms at IP
! IPTMAT = matrix opposite to desired IP matrix
! In MAD, it should be: j > k in Tijk matrix terms

 IPTMAT: MATRIX, TM(1,2,2)=-IP_T122, TM(1,4,4)=-IP_T144, &
                 TM(1,6,2)=-IP_T162, TM(1,6,6)=-IP_T166, &
                 TM(3,4,2)=-IP_T342, TM(3,6,4)=-IP_T364, &
                 TM(1,4,2)=-IP_T142, TM(1,6,4)=-IP_T164, &
                 TM(3,2,2)=-IP_T322, TM(3,4,4)=-IP_T344, &
                 TM(3,6,2)=-IP_T362, TM(3,6,6)=-IP_T366

! T-terms created by sextupole K2 adjustment

 IP_T122 :=  0.0   ! 75   (possible w/o SOC10)
 IP_T162 :=  0.0   ! 1.0  
 IP_T166 :=  0.0   ! 0.01 
 IP_T342 :=  0.0   ! 1.0  
 IP_T364 :=  0.0   ! 0.01 (possible w/o SOC10)


! T-terms created by sextupole tilt adjustment

 IP_T322 :=  0.0     ! 1.0
 IP_T344 :=  0.0     ! 1.0
 IP_T362 :=  0.0     ! 0.01
 IP_T366 :=  0.0     ! 1e-4

 IP_T142 :=  0.0     ! 75   (won't match)
 IP_T164 :=  0.0     ! 1.0  (won't match)
 IP_T144 :=  0.0
 TWSSff : BETA0
 
  !! CALL, FILENAME="./latticeFiles/src/v4.1/ATF2_BX20BY100.saveline" !! read beamline
  !! CALL, FILENAME="./latticeFiles/src/v4.1/ATF2_BX10BY10.saveline" !! read beamline
  !! CALL, FILENAME="./latticeFiles/src/v4.1/ATF2_BX1BY1.saveline" !! read beamline
  !! CALL, FILENAME="./testApps/IP_knob/ATF2_ini.mad"  !! set initial condition 
  !! !! CALL, FILENAME="./latticeFiles/src/v4.1/ATF2.xsif"
  CALL, FILENAME="./latticeFiles/src/v4.2/ATF2_BX10BY10.saveline" !! read beamline
  !! CALL, FILENAME="./latticeFiles/src/v4.2/ATF2_BX1BY1.saveline" !! read beamline
  CALL, FILENAME="./testApps/IP_knob/ATF2_ini.mad"  !! set initial condition 

  ATF2S  : LINE=(EXT,FF)
  USE, ATF2S
  SAVEBETA, TIP, 
  TWISS, COUPLE, BETA0=TWSS0
value TIP[ALFX]
  
  !! ATF2   : LINE=(EXT,FF)

ATF2SA  : LINE=(EXT,FF,IPTMAT)
MFF2A : SUBROUTINE 
      USE, ATF2SA
!!      TWISS, COUPLE, BETA0=TWSS0 
       
      MATCH, ORBIT
      RMATRIX, #S/#E, RM(5,6)=0
      TMATRIX, #S/#E, TM(5,6,6)=0
      ENDMATCH 

      MATCH, BETA0=TWSS0
      VARY, SD0FF[K2], STEP=1.E-6 !, LOWER=0
      VARY, SF1FF[K2], STEP=1.E-6 !, LOWER=0
      VARY, SD4FF[K2], STEP=1.E-6 !, LOWER=0
      VARY, SF5FF[K2], STEP=1.E-6 !, LOWER=0
      VARY, SF6FF[K2], STEP=1.E-6 !, LOWER=0

 !!      VARY, DKLSF6FF, STEP=1.E-6 !, LOWER=0
 !!      VARY, DKLSF5FF, STEP=1.E-6 !, UPPER=0
 !!      VARY, DKLSD4FF, STEP=1.E-6 !, LOWER=0
 !!      VARY, DKLSF1FF, STEP=1.E-6 !, LOWER=0
 !!      VARY, DKLSD0FF, STEP=1.E-6 !, UPPER=0
      
      TMATRIX, #S/#E, &
      
        TM(1,1,1)=TM111, weight(1,1,1)=W1_111 &
        TM(1,2,1)=TM121, weight(1,2,1)=W1_121 & 
        TM(1,2,2)=TM122, weight(1,2,2)=W1_122 &
        TM(1,3,3)=TM133, weight(1,3,3)=W1_133 &
        TM(1,4,3)=TM143, weight(1,4,3)=W1_143 &  
        TM(1,4,4)=TM144, weight(1,4,4)=W1_144 &
        TM(1,6,1)=TM161, weight(1,6,1)=W1_161 & 
        TM(1,6,2)=TM162, weight(1,6,2)=W1_162 &
        TM(1,6,6)=TM166, weight(1,6,6)=W1_166 & 
        
        TM(2,1,1)=TM211, weight(2,1,1)=W1_211 &
        TM(2,2,1)=TM221, weight(2,2,1)=W1_221 & 
        TM(2,2,2)=TM222, weight(2,2,2)=W1_222 &
        TM(2,3,3)=TM233, weight(2,3,3)=W1_233 & 
        TM(2,4,3)=TM243, weight(2,4,3)=W1_243 &   
        TM(2,4,4)=TM244, weight(2,4,4)=W1_244 &
        TM(2,6,1)=TM261, weight(2,6,1)=W1_261 &
        TM(2,6,2)=TM262, weight(2,6,2)=W1_262 &
        TM(2,6,6)=TM266, weight(2,6,6)=W1_266 & 
        
        TM(3,3,1)=TM331, weight(3,3,1)=W1_331 &
        TM(3,3,2)=TM332, weight(3,3,2)=W1_332 &  
        TM(3,4,1)=TM341, weight(3,4,1)=W1_341 &
        TM(3,6,3)=TM363, weight(3,6,3)=W1_363*10 & 
        
        TM(4,3,1)=TM431, weight(4,3,1)=W1_431 &
        TM(4,3,2)=TM432, weight(4,3,2)=W1_432 &  
        TM(4,4,1)=TM441, weight(4,4,1)=W1_441 &
        TM(4,4,2)=TM442, weight(4,4,2)=W1_442 &
        TM(4,6,3)=TM463, weight(4,6,3)=W1_463 &  
        TM(4,6,4)=TM464, weight(4,6,4)=W1_464 & 
        
        TM(5,1,1)=TM511, weight(5,1,1)=W1_511 &
        TM(5,1,2)=TM512, weight(5,1,2)=W1_512 &
        TM(5,2,2)=TM522, weight(5,2,2)=W1_522 &
        TM(5,3,3)=TM533, weight(5,3,3)=W1_533 & 
        TM(5,3,4)=TM534, weight(5,3,4)=W1_534 &
        TM(5,4,4)=TM544, weight(5,4,4)=W1_544 &
        TM(5,6,1)=TM561, weight(5,6,1)=W1_561 &
        TM(5,6,2)=TM562, weight(5,6,2)=W1_562 & 
        TM(5,6,6)=TM566, weight(5,6,6)=W1_566 

      LMDIF, TOL=1.E-20
      MIGRAD, TOL=1.E-20
    ENDMATCH
  !!  VALUE, DKLSF6FF,DKLSF5FF,DKLSD4FF,DKLSF1FF,DKLSD0FF
    VALUE, SF6FF[K2],SF5FF[K2],SD4FF[K2],SF1FF[K2],SD0FF[K2]
 ENDSUBROUTINE  
CALL, FILENAME="./testApps/IP_knob/TTmatrix.mad"  
CALL, FILENAME="./testApps/IP_knob/command.mad"     
MFF2A 
STOP



  LSFF:=0.050039016*2
  LSFD= 0.05*2
  SF6FF : SEXT, L=LSFF/2, APER=RSFF, K2=KLSF6FF/LSFF, TILT=TSF6
  SF5FF : SEXT, L=LSFF/2, APER=RSFF, K2=KLSF5FF/LSFF, TILT=TSF5
  SD4FF : SEXT, L=LSFF/2, APER=RSFF, K2=KLSD4FF/LSFF, TILT=TSD4
  SF1FF : SEXT, L=LSFD/2, APER=RSFD, K2=KLSF1FF/LSFD, TILT=TSF1
  SD0FF : SEXT, L=LSFD/2, APER=RSFD, K2=KLSD0FF/LSFD, TILT=TSD0

KLSF6FF := 8.564561598566 + DKLSF6FF ! 8.564403447578
KLSF5FF := -0.790868336382+ DKLSF5FF !-0.790062859253
KLSD4FF := 14.909987129435+ DKLSD4FF !14.903435337138
KLSF1FF := -2.578002823698+ DKLSF1FF !-2.577547317457
KLSD0FF := 4.311860665982 + DKLSD0FF ! 4.309837672996  

SF6FF: SEXTUPOLE, L=0.050039016, K2=85.5782108456, APERTURE=0.020638 
SF5FF: SEXTUPOLE, L=0.050039016, K2=-7.91563183059, APERTURE=0.020638 
SD4FF: SEXTUPOLE, L=0.050039016, K2=149.053742355, APERTURE=0.020638 
SF1FF: SEXTUPOLE, L=0.05, K2=-25.7830408714, APERTURE=0.020638 
SD0FF: SEXTUPOLE, L=0.05, K2=43.1306102849, APERTURE=0.020638 

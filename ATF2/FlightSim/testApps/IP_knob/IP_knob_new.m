function varargout = IP_knob(varargin)
% IP_KNOB M-file for IP_knob.fig
%      IP_KNOB, by itself, creates a new IP_KNOB or raises the existing
%      singleton*.
%
%      H = IP_KNOB returns the handle to a new IP_KNOB or the handle to
%      the existing singleton*.
%
%      IP_KNOB('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in IP_KNOB.M with the given input arguments.edit55
%
%      IP_KNOB('Property','Value',...) creates a new IP_KNOB or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before IP_knob_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to IP_knob_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help IP_knob

% Last Modified by GUIDE v2.5 21-Apr-2010 11:33:43

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @IP_knob_OpeningFcn, ...
                   'gui_OutputFcn',  @IP_knob_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before IP_knob is made visible.
function IP_knob_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to IP_knob (see VARARGIN)

% Choose default command line output for IP_knob
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes IP_knob wait for user response (see UIRESUME)
% uiwait(handles.figure1);

global BEAMLINE PS GIRDER FL IP_KNOBV;

FF_MAD_ini;
load ./testApps/IP_knob/KNOB_VALUE;
QuadName = { 'QD6FF'; 'QF5BFF'; 'QF5AFF'};
%% QuadName = { 'QD6FF'; 'QF5BFF'; 'QF5AFF'; 'QD4BFF'; 'QD4AFF'; 'QF3FF';...
%%     'QD2BFF'; 'QD2AFF'; 'QF1FF'; 'QD0FF'};
SextName = {'SF6FF'; 'SF5FF'; 'SD4FF'; 'SF1FF'; 'SD0FF'};
Q_ps = zeros(1, length(QuadName));
S_ps = zeros(1, length(SextName));
S_Gir = zeros(1, length(SextName));
Qps_value_ini = zeros(1,length(QuadName));
Sps_value_ini = zeros(1, length(SextName));
Smover_ini = zeros(length(SextName),3);

for i = 1:length(QuadName)
    ind = findcells(BEAMLINE,'Name',QuadName{i});
    Q_ps(i) = BEAMLINE{ind(1)}.PS;
    Qps_value_ini(i) = PS(Q_ps(i)).Ampl ;
end

for i = 1:length(SextName)
    ind = findcells(BEAMLINE,'Name',SextName{i});
    S_ps(i) = BEAMLINE{ind(1)}.PS;
    S_Gir(i) = BEAMLINE{ind(1)}.Girder; 
    Sps_value_ini(i) = PS(S_ps(i)).Ampl ;
    Smover_ini(i,:)=GIRDER{S_Gir(i)}.MoverPos;
end
request={[] Q_ps []};
        [stat respQ] = AccessRequest(request);
        if(stat{1}==-1)
            display(sprintf('AccessRequest returned error :%s',stat{2}));
        else
            display(sprintf('Access granted, ID of request : %s',respQ));
        end
request={[] S_ps []};
        [stat respS] = AccessRequest(request);
        if(stat{1}==-1)
            display(sprintf('AccessRequest returned error :%s',stat{2}));
        else
            display(sprintf('Access granted, ID of request : %s',respS));
        end
request={S_Gir []  []};
        [stat resmS] = AccessRequest(request);
        if(stat{1}==-1)
            display(sprintf('AccessRequest returned error :%s',stat{2}));
        else
            display(sprintf('Access granted, ID of request : %s',resmS));
        end
handles.QuadName = QuadName;
handles.SextName = SextName;
handles.Q_ps = Q_ps;
handles.S_ps = S_ps;
handles.S_Gir = S_Gir ;
handles.Qps_value_ini = Qps_value_ini;
handles.Sps_value_ini = Sps_value_ini;
handles.Smover_ini = Smover_ini;
handles.Qps_value_new = Qps_value_ini;
handles.Sps_value_new = Sps_value_ini;
handles.Smover_new = Smover_ini; 
handles.respQ = respQ; 
handles.respS = respS; 
handles.resmS = resmS;
guidata(hObject,handles);
IP_ind = findcells(BEAMLINE,'Name','IP'); %% 1717 not equal FL.SimModel.ip_ind =1735, %% 1703 not equal FL.SimModel.ip_ind = 1721
handles.IP_ind = IP_ind;
%% [stat,beamout] = TrackThru( 1, handles.IP_ind, FL.SimBeam{1}, 1, 1, 0 );
[data txt beamout] = GetIPData(FL.SimBeam{1},FL.SimBeam{2},1, handles.IP_ind);
%% [ nx,xout] = hist(beamout.Bunch.x(1,:),100);
%% [ ny,yout] = hist(beamout.Bunch.x(3,:),100);
plot(handles.axes6, beamout.Bunch.x(1,:), beamout.Bunch.x(3,:),'b.');
%% [stat,twiss]=GetTwiss(1,length(BEAMLINE), ...
%%  FL.SimModel.Initial.x.Twiss,FL.SimModel.Initial.y.Twiss);
set(handles.edit4,'String',num2str(data.xwaist,'%5.2e'));
set(handles.edit10,'String',num2str(data.xdisp,'%5.2e'));
set(handles.edit5,'String',num2str(data.xsize,'%5.2e'));
set(handles.edit6,'String',num2str(data.emit_x,'%5.2e'));
set(handles.edit7,'String',num2str(data.ywaist,'%5.2e'));
set(handles.edit11,'String',num2str(data.ydisp,'%5.2e'));
set(handles.edit8,'String',num2str(data.ysize,'%5.2e'));
set(handles.edit9,'String',num2str(data.emit_y,'%5.2e'));

%% axes(handles.axes6)
%% h11 = line(beamout.Bunch.x(1,:), beamout.Bunch.x(3,:),'Marker','.','Color','b','LineStyle','none');
%% %% ax1 = gca;
%% %% set(ax1,'XColor','k','YColor','k')
%% %% ax2 = axes('Position',get(ax1,'Position'),...
%% %%            'XAxisLocation','bottom',...
%% %%            'YAxisLocation','right',...
%% %%            'Color','none',...
%% %%            'XColor','k','YColor','b');
%% %% ax3 = axes('Position',get(ax1,'Position'),...
%% %%            'XAxisLocation','top',...
%% %%            'YAxisLocation','left',...
%% %%            'Color','none',...
%% %%            'XColor','g','YColor','k');
%% %% %% hold on    
%% %% hl2 = line(xout,nx,'Color','b','Marker','.','LineStyle','none','Parent',ax2);
%% %% hl3 = line(ny,yout,'Color','g','Marker','.','LineStyle','none','Parent',ax3);
%% axes(handles.axes10)
bar(handles.axes10, handles.Qps_value_ini, 0.8)
set(handles.axes10,'XTickLabel',QuadName)
set(handles.axes10,'xlim',[0.5 length(handles.Qps_value_ini)+0.5])
%% axes(handles.axes11)
bar(handles.axes11, handles.Sps_value_ini, 0.8)
set(handles.axes11,'XTickLabel',SextName);
set(handles.axes11,'xlim',[0.5 length(handles.Sps_value_ini)+0.5])
%% axes(handles.axes12)
bar(handles.axes12, handles.Smover_ini(:,1:2), 0.8)
set(handles.axes12,'XTickLabel',SextName)
set(handles.axes12,'xlim',[0.5 length(handles.Smover_ini)+0.5]);
for ii = 12:44
    eval(['set(handles.edit',num2str(ii),',''String'',''0'')']);
end

handles.data(1)= data.xwaist;
handles.data(2)= data.xdisp;
handles.data(3)= data.xsize;
handles.data(4)= data.emit_x;

handles.data(5)= data.ywaist;
handles.data(6)= data.ydisp;
handles.data(7)= data.ysize;
handles.data(8)= data.emit_y;

handles.B_x_Wv = 0;
handles.B_y_Wv = 0;
handles.E_xv = 0;
handles.T122v  = 0;
handles.T162v = 0;
handles.T166v = 0;
handles.T342v = 0; 
handles.T364v = 0; 
handles.B_x_Wmv = 0;
handles.B_y_Wmv = 0;
handles.E_xmv = 0;
handles.E_yv = 0;
handles.R13v  = 0;
handles.R14v  = 0;
handles.R23v  = 0;
handles.R24v  = 0;
handles.B_x_Wa = 0;
handles.B_y_Wa = 0;
handles.E_xa = 0;
handles.T122a  = 0;
handles.T162a = 0;
handles.T166a = 0;
handles.T342a = 0; 
handles.T364a = 0; 
handles.B_x_Wma = 0;
handles.B_y_Wma = 0;
handles.E_xma = 0;
handles.E_ya = 0;
handles.R13a  = 0;
handles.R14a  = 0;
handles.R23a  = 0;
handles.R24a  = 0;
%% val8 = get(handles.popupmenu8,'Value');
handles.plot = get(handles.popupmenu8,'Value');
guidata(hObject,handles);

% --- Outputs from this function are returned to the command line.
function varargout = IP_knob_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on selection change in popupmenu1.
function popupmenu1_Callback(hObject, eventdata, handles)
% hObject    handle to popupmenu1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns popupmenu1 contents as cell array
%        contents{get(hObject,'Value')} returns selected item from popupmenu1
global IP_KNOBV;
str = get(handles.popupmenu1, 'String');
val = get(handles.popupmenu1,'Value');

switch lower(str{val})
   case {'beta_x_waist'} %% 1 mm  --> 0.01
       set(handles.edit12,'String',num2str(IP_KNOBV{1}.Unit,'%5.2e'));
       set(handles.edit55,'String',[num2str(IP_KNOBV{1}.Ratio(1)*1e4,'%3.2f'), '  ',....
                                    num2str(IP_KNOBV{1}.Ratio(2)*1e4,'%3.2f'), '  ',....
                                    num2str(IP_KNOBV{1}.Ratio(3)*1e4,'%3.2f')]);
   case {'beta_y_waist'} %% 1 mm --> 0.01
       
       set(handles.edit12,'String',num2str(IP_KNOBV{2}.Unit,'%5.2e'));
       set(handles.edit55,'String',[num2str(IP_KNOBV{2}.Ratio(1)*1e4,'%3.2f'), '  ',....
                                    num2str(IP_KNOBV{2}.Ratio(2)*1e4,'%3.2f'), '  ',....
                                    num2str(IP_KNOBV{2}.Ratio(3)*1e4,'%3.2f')]);
   case {'eta_x'} %% 0.1 mm ----> 0.01 
       
       set(handles.edit12,'String',num2str(IP_KNOBV{3}.Unit,'%5.2e'));
       set(handles.edit55,'String',[num2str(IP_KNOBV{3}.Ratio(1)*1e4,'%3.2f'), '  ',....
                                    num2str(IP_KNOBV{3}.Ratio(2)*1e4,'%3.2f'), '  ',....
                                    num2str(IP_KNOBV{3}.Ratio(3)*1e4,'%3.2f')]);
   case 't122'  %% 0.1 ---> 0.02 
       set(handles.edit12,'String',num2str(IP_KNOBV{4}.Unit,'%5.2e'));
       set(handles.edit55,'String',[num2str(IP_KNOBV{4}.Ratio(1)*1e4,'%3.2f'), '  ',....
                                    num2str(IP_KNOBV{4}.Ratio(2)*1e4,'%3.2f'), '  ',....
                                    num2str(IP_KNOBV{4}.Ratio(3)*1e4,'%3.2f'), '  ',....
                                    num2str(IP_KNOBV{4}.Ratio(4)*1e4,'%3.2f'), '  ',....
                                    num2str(IP_KNOBV{4}.Ratio(5)*1e4,'%3.2f')]);
   case 't162'  %% 0.1 ---> 0.075
       set(handles.edit12,'String',num2str(IP_KNOBV{5}.Unit,'%5.2e'));
       set(handles.edit55,'String',[num2str(IP_KNOBV{5}.Ratio(1)*1e4,'%3.2f'), '  ',....
                                    num2str(IP_KNOBV{5}.Ratio(2)*1e4,'%3.2f'), '  ',....
                                    num2str(IP_KNOBV{5}.Ratio(3)*1e4,'%3.2f'), '  ',....
                                    num2str(IP_KNOBV{5}.Ratio(4)*1e4,'%3.2f'), '  ',....
                                    num2str(IP_KNOBV{5}.Ratio(5),'%3.2f')]);
   case 't166' %% 0.1 ---> 0.2
       set(handles.edit12,'String',num2str(IP_KNOBV{6}.Unit,'%5.2e'));
       set(handles.edit55,'String',[num2str(IP_KNOBV{6}.Ratio(1)*1e4,'%3.2f'), '  ',....
                                    num2str(IP_KNOBV{6}.Ratio(2)*1e4,'%3.2f'), '  ',....
                                    num2str(IP_KNOBV{6}.Ratio(3)*1e4,'%3.2f'), '  ',....
                                    num2str(IP_KNOBV{6}.Ratio(4)*1e4,'%3.2f'), '  ',....
                                    num2str(IP_KNOBV{6}.Ratio(5)*1e4,'%3.2f')]);
   case 't342' %% 0.1 ---> 0.02
       set(handles.edit12,'String',num2str(IP_KNOBV{7}.Unit ,'%5.2e'));
       set(handles.edit55,'String',[num2str(IP_KNOBV{7}.Ratio(1)*1e4,'%3.2f'), '  ',....
                                    num2str(IP_KNOBV{7}.Ratio(2)*1e4,'%3.2f'), '  ',....
                                    num2str(IP_KNOBV{7}.Ratio(3)*1e4,'%3.2f'), '  ',....
                                    num2str(IP_KNOBV{7}.Ratio(4)*1e4,'%3.2f'), '  ',....
                                    num2str(IP_KNOBV{7}.Ratio(5)*1e4,'%3.2f')]);
   case 't364'  %% 0.1 ---> 0.03
       set(handles.edit12,'String',num2str(IP_KNOBV{8}.Unit,'%5.2e')); 
       set(handles.edit55,'String',[num2str(IP_KNOBV{8}.Ratio(1)*1e4,'%3.2f'), '  ',....
                                    num2str(IP_KNOBV{8}.Ratio(2)*1e4,'%3.2f'), '  ',....
                                    num2str(IP_KNOBV{8}.Ratio(3)*1e4,'%3.2f'), '  ',....
                                    num2str(IP_KNOBV{8}.Ratio(4)*1e4,'%3.2f'), '  ',....
                                    num2str(IP_KNOBV{8}.Ratio(5)*1e4,'%3.2f')]);
   case {'beta_x_waist_m'}  %% 0.1 mm  ---> 1 e-5 m 
       set(handles.edit12,'String',num2str(IP_KNOBV{9}.Unit,'%5.2e'));
       set(handles.edit55,'String',[num2str(IP_KNOBV{9}.Ratio(1,1)*1e6,'%3.2f'), '  ',....
                                    num2str(IP_KNOBV{9}.Ratio(2,1)*1e6,'%3.2f'), '  ',....
                                    num2str(IP_KNOBV{9}.Ratio(3,1)*1e6,'%3.2f'), '  ',....
                                    num2str(IP_KNOBV{9}.Ratio(4,1)*1e6,'%3.2f'), '  ',....
                                    num2str(IP_KNOBV{9}.Ratio(5,1)*1e6,'%3.2f')]);
   case {'beta_y_waist_m'} %% 0.1 mm  ---> 15 e-4 m 
       set(handles.edit12,'String',num2str(IP_KNOBV{10}.Unit,'%5.2e'));
       set(handles.edit55,'String',[num2str(IP_KNOBV{10}.Ratio(1,1)*1e6,'%3.2f'), '  ',....
                                    num2str(IP_KNOBV{10}.Ratio(2,1)*1e6,'%3.2f'), '  ',....
                                    num2str(IP_KNOBV{10}.Ratio(3,1)*1e6,'%3.2f'), '  ',....
                                    num2str(IP_KNOBV{10}.Ratio(4,1)*1e6,'%3.2f'), '  ',....
                                    num2str(IP_KNOBV{10}.Ratio(5,1)*1e6,'%3.2f')]);
   case {'eta_x_m'} %% 0.01 mm  ---> 10 e-5 m 
       set(handles.edit12,'String',num2str(IP_KNOBV{11}.Unit,'%5.2e'));
       set(handles.edit55,'String',[num2str(IP_KNOBV{11}.Ratio(1,1)*1e6,'%3.2f'), '  ',....
                                    num2str(IP_KNOBV{11}.Ratio(2,1)*1e6,'%3.2f'), '  ',....
                                    num2str(IP_KNOBV{11}.Ratio(3,1)*1e6,'%3.2f'), '  ',....
                                    num2str(IP_KNOBV{11}.Ratio(4,1)*1e6,'%3.2f'), '  ',....
                                    num2str(IP_KNOBV{11}.Ratio(5,1)*1e6,'%3.2f')]);
   case {'eta_y'} %% 0.01 mm  ---> 20 e-5 m
       set(handles.edit12,'String',num2str(IP_KNOBV{12}.Unit,'%5.2e'));
       set(handles.edit55,'String',[num2str(IP_KNOBV{12}.Ratio(1,1)*1e6,'%3.2f'), '  ',....
                                    num2str(IP_KNOBV{12}.Ratio(2,1)*1e6,'%3.2f'), '  ',....
                                    num2str(IP_KNOBV{12}.Ratio(3,1)*1e6,'%3.2f'), '  ',....
                                    num2str(IP_KNOBV{12}.Ratio(4,1)*1e6,'%3.2f'), '  ',....
                                    num2str(IP_KNOBV{12}.Ratio(5,1)*1e6,'%3.2f')]);
   case {'r13'} %% 1e-6 --->   5 e-4 m 
       set(handles.edit12,'String',num2str(IP_KNOBV{13}.Unit,'%5.2e'));
       set(handles.edit55,'String',[num2str(IP_KNOBV{13}.Ratio(1,2)*1e6,'%3.2f'), '  ',....
                                    num2str(IP_KNOBV{13}.Ratio(2,2)*1e6,'%3.2f'), '  ',....
                                    num2str(IP_KNOBV{13}.Ratio(3,2)*1e6,'%3.2f'), '  ',....
                                    num2str(IP_KNOBV{13}.Ratio(4,2)*1e6,'%3.2f'), '  ',....
                                    num2str(IP_KNOBV{13}.Ratio(5,2)*1e6,'%3.2f')]);
   case {'r14'}   %% 1e-7 --->   2 e-4 m 
       set(handles.edit12,'String',num2str(IP_KNOBV{14}.Unit,'%5.2e'));
       set(handles.edit55,'String',[num2str(IP_KNOBV{14}.Ratio(1,2)*1e6,'%3.2f'), '  ',....
                                    num2str(IP_KNOBV{14}.Ratio(2,2)*1e6,'%3.2f'), '  ',....
                                    num2str(IP_KNOBV{14}.Ratio(3,2)*1e6,'%3.2f'), '  ',....
                                    num2str(IP_KNOBV{14}.Ratio(4,2)*1e6,'%3.2f'), '  ',....
                                    num2str(IP_KNOBV{14}.Ratio(5,2)*1e6,'%3.2f')]);
   case {'r23'}  %% 1e-7 --->   5 e-4 m 
       set(handles.edit12,'String',num2str(IP_KNOBV{15}.Unit,'%5.2e'));
       set(handles.edit55,'String',[num2str(IP_KNOBV{15}.Ratio(1,2)*1e6,'%3.2f'), '  ',....
                                    num2str(IP_KNOBV{15}.Ratio(2,2)*1e6,'%3.2f'), '  ',....
                                    num2str(IP_KNOBV{15}.Ratio(3,2)*1e6,'%3.2f'), '  ',....
                                    num2str(IP_KNOBV{15}.Ratio(4,2)*1e6,'%3.2f'), '  ',....
                                    num2str(IP_KNOBV{15}.Ratio(5,2)*1e6,'%3.2f')]);
   case {'r24'}  %% 1e-8 --->   1 e-4 m 
       set(handles.edit12,'String',num2str(IP_KNOBV{16}.Unit,'%5.2e'));
       set(handles.edit55,'String',[num2str(IP_KNOBV{16}.Ratio(1,2)*1e6,'%3.2f'), '  ',....
                                    num2str(IP_KNOBV{16}.Ratio(2,2)*1e6,'%3.2f'), '  ',....
                                    num2str(IP_KNOBV{16}.Ratio(3,2)*1e6,'%3.2f'), '  ',....
                                    num2str(IP_KNOBV{16}.Ratio(4,2)*1e6,'%3.2f'), '  ',....
                                    num2str(IP_KNOBV{16}.Ratio(5,2)*1e6,'%3.2f')]);
   otherwise
       disp('Please use one of the following knob names:');
       disp({'Beta_x_Waist';'Beta_y_Waist'; 'Eta_x';...
           'T122'; 'T162'; 'T166'; 'T342'; 'T364';...
           'Beta_x_Waist_m';'Beta_y_Waist_m'; 'Eta_x_m'; 'Eta_y';...
           'R13' ; 'R14' ; 'R23' ; 'R24'}  );
end


% --- Executes during object creation, after setting all properties.
function popupmenu1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to popupmenu1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in popupmenu4.
function popupmenu4_Callback(hObject, eventdata, handles)
% hObject    handle to popupmenu4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns popupmenu4 contents as cell array
%        contents{get(hObject,'Value')} returns selected item from popupmenu4


% --- Executes during object creation, after setting all properties.
function popupmenu4_CreateFcn(hObject, eventdata, handles)
% hObject    handle to popupmenu4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in popupmenu5.
function popupmenu5_Callback(hObject, eventdata, handles)
% hObject    handle to popupmenu5 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns popupmenu5 contents as cell array
%        contents{get(hObject,'Value')} returns selected item from popupmenu5


% --- Executes during object creation, after setting all properties.
function popupmenu5_CreateFcn(hObject, eventdata, handles)
% hObject    handle to popupmenu5 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in pushbutton4.
function pushbutton4_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
resp=questdlg('Are you sure?','Exit Request','Yes','No','No');
if ~strcmp(resp,'Yes'); return; end;
try  
  guiCloseFn('IP_knob',handles);
  [stat resp] = AccessRequest('release',handles.respQ);
  [stat resp] = AccessRequest('release',handles.respS);
  [stat resp] = AccessRequest('release',handles.resmS);
catch
 %% exit
end

% --- Executes on button press in pushbutton5.
function pushbutton5_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton5 (see GCBO)str7 = get(handles.popupmenu7, 'String');
val7 = get(handles.popupmenu7,'Value');
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
handles.data(3,:)
data.xsize


% --- Executes on button press in pushbutton6.
function pushbutton6_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton6 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global BEAMLINE FL IP_KNOBV;
%% load .\testApps\IP_knob\KNOB_VALUE;
knob_value = eval(['[',get(handles.edit3,'String'),']']);
str = get(handles.popupmenu1, 'String');
val = get(handles.popupmenu1,'Value');

switch lower(str{val})
   case {'beta_x_waist'} %% 1 mm  --> 0.01
       %% [ps_value_ini, MagRatioSet] = knob_set(knobname,
       %% IP_Beta_x_Waist_ratio', knob_value);
       %% ratio = ps_step/max(IP_Beta_x_Waist_ratio); 
       %% [handles.Qps_value_new] =
       %% set_ps(handles.Q_ps,IP_Beta_x_Waist_ratio'*knob_value*ratio+handles.Qps_value_new);
       [handles.Qps_value_new] = set_ps(handles.Q_ps,IP_KNOBV{1}.Ratio*knob_value+handles.Qps_value_new);
       %% axes(handles.axes10)
       bar(handles.axes10, handles.Qps_value_new - handles.Qps_value_ini, 0.8);
       set(handles.axes10,'XTickLabel',handles.QuadName);
       set(handles.axes10,'xlim',[0.5 length(handles.Qps_value_new)+0.5])
       %% handles.B_x_Wv = handles.B_x_Wv + knob_value*IP_KNOBV{1}.Unit;
       handles.B_x_Wv = handles.B_x_Wv + knob_value;
       set(handles.edit13,'String',num2str(handles.B_x_Wv,'%d'));
       set(handles.edit14,'String',num2str(handles.B_x_Wv*IP_KNOBV{1}.Unit,'%5.2e'));
   
   case {'beta_y_waist'} %% 1 mm --> 0.01
       %% ratio = ps_step/max(IP_Beta_y_Waist_ratio); 
       %% [handles.Qps_value_new] =
       %% set_ps(handles.Q_ps,IP_Beta_y_Waist_ratio'*knob_value*ratio+handles.Qps_value_new);
       [handles.Qps_value_new] = set_ps(handles.Q_ps,IP_KNOBV{2}.Ratio*knob_value+handles.Qps_value_new);
       %% axes(handles.axes10)
       bar(handles.axes10, handles.Qps_value_new - handles.Qps_value_ini, 0.8);
       set(handles.axes10,'XTickLabel',handles.QuadName);
       set(handles.axes10,'xlim',[0.5 length(handles.Qps_value_new)+0.5]);
       handles.B_y_Wv = handles.B_y_Wv + knob_value;
       set(handles.edit15,'String',num2str(handles.B_y_Wv,'%d'));
       set(handles.edit16,'String',num2str(handles.B_y_Wv*IP_KNOBV{2}.Unit,'%5.2e'));
       
   case {'eta_x'} %% 0.1 mm ----> 0.01 
       %% ratio = ps_step/max(IP_Eta_x_ratio); 
       %% [handles.Qps_value_new] = set_ps(handles.Q_ps,IP_Eta_x_ratio'*knob_value*ratio+handles.Qps_value_new);
       [handles.Qps_value_new] = set_ps(handles.Q_ps,IP_KNOBV{3}.Ratio*knob_value+handles.Qps_value_new);
       %% axes(handles.axes10)
       bar(handles.axes10, handles.Qps_value_new - handles.Qps_value_ini, 0.8);
       set(handles.axes10,'XTickLabel',handles.QuadName);
       set(handles.axes10,'xlim',[0.5 length(handles.Qps_value_new)+0.5]);
       handles.E_xv = handles.E_xv + knob_value;
       set(handles.edit17,'String',num2str(handles.E_xv,'%d'));
       set(handles.edit18,'String',num2str(handles.E_xv*IP_KNOBV{3}.Unit,'%5.2e'));
       
   case 't122'  %% 0.1 ---> 0.02 
       %% ratio = ps_step/max(IP_T122_ratio);
       %% [handles.Sps_value_new] = set_ps(handles.S_ps,IP_T122_ratio*knob_value*ratio+handles.Sps_value_new);
       [handles.Sps_value_new] = set_ps(handles.S_ps,IP_KNOBV{4}.Ratio*knob_value+handles.Sps_value_new);
       %% axes(handles.axes11)
       bar(handles.axes11, handles.Sps_value_new - handles.Sps_value_ini, 0.8);
       set(handles.axes11,'XTickLabel',handles.SextName);
       set(handles.axes11,'xlim',[0.5 length(handles.Sps_value_new)+0.5]);
       handles.T122v  = handles.T122v + knob_value;
       set(handles.edit19,'String',num2str(handles.T122v,'%d'));
       set(handles.edit20,'String',num2str(handles.T122v*IP_KNOBV{4}.Unit,'%5.2e'));
       
   case 't162'  %% 0.1 ---> 0.075
       %% ratio = ps_step/max(IP_T162_ratio);
       %% [handles.Sps_value_new] = set_ps(handles.S_ps,IP_T162_ratio*knob_value*ratio+handles.Sps_value_new);
       [handles.Sps_value_new] = set_ps(handles.S_ps,IP_KNOBV{5}.Ratio*knob_value+handles.Sps_value_new);
       %% axes(handles.axes11)
       bar(handles.axes11, handles.Sps_value_new - handles.Sps_value_ini, 0.8);
       set(handles.axes11,'XTickLabel',handles.SextName);
       set(handles.axes11,'xlim',[0.5 length(handles.Sps_value_new)+0.5]);
       handles.T162v = handles.T162v + knob_value;
       set(handles.edit21,'String',num2str(handles.T162v,'%d'));
       set(handles.edit22,'String',num2str(handles.T162v*IP_KNOBV{5}.Unit,'%5.2e'));
       
   case 't166' %% 0.1 ---> 0.2
       %% ratio = ps_step/max(IP_T166_ratio);
       %% [handles.Sps_value_new] = set_ps(handles.S_ps,IP_T166_ratio*knob_value*ratio+handles.Sps_value_new);
       [handles.Sps_value_new] = set_ps(handles.S_ps,IP_KNOBV{6}.Ratio*knob_value+handles.Sps_value_new);
       %% axes(handles.axes11)
       bar(handles.axes11, handles.Sps_value_new - handles.Sps_value_ini, 0.8);
       set(handles.axes11,'XTickLabel',handles.SextName);
       set(handles.axes11,'xlim',[0.5 length(handles.Sps_value_new)+0.5]);
       handles.T166v = handles.T166v + knob_value;
       set(handles.edit23,'String',num2str(handles.T166v,'%d'));
       set(handles.edit24,'String',num2str(handles.T166v*IP_KNOBV{6}.Unit,'%5.2e'));
       
   case 't342' %% 0.1 ---> 0.02
       %% ratio = ps_step/max(IP_T342_ratio);
       %% [handles.Sps_value_new] = set_ps(handles.S_ps,IP_T342_ratio*knob_value*ratio+handles.Sps_value_new);
       [handles.Sps_value_new] = set_ps(handles.S_ps,IP_KNOBV{7}.Ratio*knob_value+handles.Sps_value_new);
       %% axes(handles.axes11)
       bar(handles.axes11, handles.Sps_value_new - handles.Sps_value_ini, 0.8);
       set(handles.axes11,'XTickLabel',handles.SextName);
       set(handles.axes11,'xlim',[0.5 length(handles.Sps_value_new)+0.5]);
       handles.T342v = handles.T342v + knob_value;
       set(handles.edit25,'String',num2str(handles.T342v ,'%d'));
       set(handles.edit26,'String',num2str(handles.T342v*IP_KNOBV{7}.Unit ,'%5.2e'));
       
   case 't364'  %% 0.1 ---> 0.03
       %% ratio = ps_step/max(IP_T364_ratio);
       %% [handles.Sps_value_new] = set_ps(handles.S_ps,IP_T364_ratio*knob_value*ratio+handles.Sps_value_new);
       [handles.Sps_value_new] = set_ps(handles.S_ps,IP_KNOBV{8}.Ratio*knob_value+handles.Sps_value_new);
       %% axes(handles.axes11)
       bar(handles.axes11, handles.Sps_value_new - handles.Sps_value_ini, 0.8);
       set(handles.axes11,'XTickLabel',handles.SextName);
       set(handles.axes11,'xlim',[0.5 length(handles.Sps_value_new)+0.5]);
       handles.T364v = handles.T364v + knob_value;
       set(handles.edit27,'String',num2str(handles.T364v,'%d')); 
       set(handles.edit28,'String',num2str(handles.T364v*IP_KNOBV{8}.Unit,'%5.2e'));
       
   case {'beta_x_waist_m'}  %% 0.1 mm  ---> 1 e-5 m 
       %% ratio = mover_step/max(IP_Beta_x_Waist_mover(:,1));
       %% [old_value, handles.Smover_new]=set_mover(handles.S_Gir, IP_Beta_x_Waist_mover*knob_value*ratio+handles.Smover_new);
       [old_value, handles.Smover_new]=set_mover(handles.S_Gir, IP_KNOBV{9}.Ratio*knob_value+handles.Smover_new);
       %% axes(handles.axes12) 
       bar(handles.axes12, handles.Smover_new(:,1:2) - handles.Smover_ini(:,1:2), 0.8);
       set(handles.axes12,'XTickLabel',handles.SextName);
       set(handles.axes12,'xlim',[0.5 length(handles.Smover_new)+0.5]);
       handles.B_x_Wmv = handles.B_x_Wmv + knob_value;
       set(handles.edit29,'String',num2str(handles.B_x_Wmv,'%3d'));
       set(handles.edit30,'String',num2str(handles.B_x_Wmv*IP_KNOBV{9}.Unit,'%5.2e'));
       %% [mover_oldread, mover_newread]=set_mover(SextName, IP_Beta_x_Waist_mover'*knob_value);
       % [max_pv, max_nv, min_pv, min_nv] = knob_limit(handles.SextName, IP_Beta_x_Waist_mover);
       
   case {'beta_y_waist_m'} %% 0.1 mm  ---> 15 e-4 m 
       %% ratio = mover_step/max(IP_Beta_y_Waist_mover(:,1));
       %% [old_value, handles.Smover_new]=set_mover(handles.S_Gir, IP_Beta_y_Waist_mover*knob_value*ratio+handles.Smover_new);
       [old_value, handles.Smover_new]=set_mover(handles.S_Gir, IP_KNOBV{10}.Ratio*knob_value+handles.Smover_new);
       %% axes(handles.axes12)
       bar(handles.axes12, handles.Smover_new(:,1:2)- handles.Smover_ini(:,1:2), 0.8);
       set(handles.axes12,'XTickLabel',handles.SextName);
       set(handles.axes12,'xlim',[0.5 length(handles.Smover_new)+0.5]);
       handles.B_y_Wmv = handles.B_y_Wmv + knob_value;
       set(handles.edit31,'String',num2str(handles.B_y_Wmv,'%3d'));
       set(handles.edit32,'String',num2str(handles.B_y_Wmv*IP_KNOBV{10}.Unit,'%5.2e'));
       % [max_pv, max_nv, min_pv, min_nv] = knob_limit(handles.SextName, IP_Beta_y_Waist_mover);
       
   case {'eta_x_m'} %% 0.01 mm  ---> 10 e-5 m 
       %% ratio = mover_step/max(IP_Eta_x_mover(:,1));
       %% [old_value, handles.Smover_new]=set_mover(handles.S_Gir, IP_Eta_x_mover*knob_value*ratio+handles.Smover_new);
       [old_value, handles.Smover_new]=set_mover(handles.S_Gir, IP_KNOBV{11}.Ratio*knob_value+handles.Smover_new);
       %% axes(handles.axes12)
       bar(handles.axes12, handles.Smover_new(:,1:2) - handles.Smover_ini(:,1:2), 0.8);
       set(handles.axes12,'XTickLabel',handles.SextName);
       set(handles.axes12,'xlim',[0.5 length(handles.Smover_new)+0.5]);
       handles.E_xmv = handles.E_xmv + knob_value;
       set(handles.edit33,'String',num2str(handles.E_xmv,'%3d'));
       set(handles.edit34,'String',num2str(handles.E_xmv*IP_KNOBV{11}.Unit,'%5.2e'));
       % [max_pv, max_nv, min_pv, min_nv] = knob_limit(handles.SextName, IP_Eta_x_mover);
       
   case {'eta_y'} %% 0.01 mm  ---> 20 e-5 m
       %% ratio = mover_step/max(IP_Eta_y_mover(:,2));
       %% [old_value, handles.Smover_new]=set_mover(handles.S_Gir, IP_Eta_y_mover*knob_value*ratio+handles.Smover_new);
       [old_value, handles.Smover_new]=set_mover(handles.S_Gir, IP_KNOBV{12}.Ratio*knob_value+handles.Smover_new);
       %% axes(handles.axes12)
       bar(handles.axes12, handles.Smover_new(:,1:2) - handles.Smover_ini(:,1:2), 0.8);
       set(handles.axes12,'XTickLabel',handles.SextName);
       set(handles.axes12,'xlim',[0.5 length(handles.Smover_new)+0.5]);
       handles.E_yv = handles.E_yv + knob_value;
       set(handles.edit35,'String',num2str(handles.E_yv,'%3d'));
       set(handles.edit36,'String',num2str(handles.E_yv*IP_KNOBV{12}.Unit,'%5.2e'));
       % [max_pv, max_nv, min_pv, min_nv] = knob_limit(handles.SextName, IP_Eta_y_mover);
       
   case {'r13'} %% 1e-6 --->   5 e-4 m 
       %% ratio = mover_step/max(IP_R13_movers(:,2));
       %% [old_value, handles.Smover_new]=set_mover(handles.S_Gir, IP_R13_mover*knob_value*ratio+handles.Smover_new);
       [old_value, handles.Smover_new]=set_mover(handles.S_Gir, IP_KNOBV{13}.Ratio*knob_value+handles.Smover_new);
       %% axes(handles.axes12)
       bar(handles.axes12, handles.Smover_new(:,1:2) - handles.Smover_ini(:,1:2), 0.8);
       set(handles.axes12,'XTickLabel',handles.SextName);
       set(handles.axes12,'xlim',[0.5 length(handles.Smover_new)+0.5]);
       handles.R13v  = handles.R13v + knob_value;
       set(handles.edit37,'String',num2str(handles.R13v,'%3d'));
       set(handles.edit38,'String',num2str(handles.R13v*IP_KNOBV{13}.Unit,'%5.2e'));
       % [max_pv, max_nv, min_pv, min_nv] = knob_limit(handles.SextName, IP_R13_mover);
       
   case {'r14'}   %% 1e-7 --->   2 e-4 m 
       %% ratio = mover_step/max(IP_R14_movers(:,2));
       %% [old_value, handles.Smover_new]=set_mover(handles.S_Gir, IP_R14_mover*knob_value*ratio+handles.Smover_new);
       [old_value, handles.Smover_new]=set_mover(handles.S_Gir, IP_KNOBV{14}.Ratio*knob_value+handles.Smover_new);
       %% axes(handles.axes12)
       bar(handles.axes12, handles.Smover_new(:,1:2) - handles.Smover_ini(:,1:2), 0.8);
       set(handles.axes12,'XTickLabel',handles.SextName);
       set(handles.axes12,'xlim',[0.5 length(handles.Smover_new)+0.5]);
       handles.R14v  = handles.R14v + knob_value;
       set(handles.edit39,'String',num2str(handles.R14v,'%3d'));
       set(handles.edit40,'String',num2str(handles.R14v*IP_KNOBV{14}.Unit,'%5.2e'));
       % [max_pv, max_nv, min_pv, min_nv] = knob_limit(handles.SextName, IP_R14_mover);
       
   case {'r23'}  %% 1e-7 --->   5 e-4 m 
       %% ratio = mover_step/max(IP_R23_movers(:,2));
       %% [old_value, handles.Smover_new]=set_mover(handles.S_Gir, IP_R23_mover*knob_value*ratio+handles.Smover_new);
       [old_value, handles.Smover_new]=set_mover(handles.S_Gir, IP_KNOBV{15}.Ratio*knob_value+handles.Smover_new);
       %% axes(handles.axes12)
       bar(handles.axes12, handles.Smover_new(:,1:2) - handles.Smover_ini(:,1:2), 0.8);
       set(handles.axes12,'XTickLabel',handles.SextName);
       set(handles.axes12,'xlim',[0.5 length(handles.Smover_new)+0.5]);
       handles.R23v  = handles.R23v + knob_value;
       set(handles.edit41,'String',num2str(handles.R23v,'%3d'));
       set(handles.edit42,'String',num2str(handles.R23v*IP_KNOBV{15}.Unit,'%5.2e'));
       % [max_pv, max_nv, min_pv, min_nv] = knob_limit(handles.SextName, IP_R23_mover);
       
   case {'r24'}  %% 1e-8 --->   1 e-4 m 
       %% ratio = mover_step/max(IP_R24_movers(:,2));
       %% [old_value, handles.Smover_new]=set_mover(handles.S_Gir, IP_R24_mover*knob_value*ratio+handles.Smover_new);
       [old_value, handles.Smover_new]=set_mover(handles.S_Gir, IP_KNOBV{16}.Ratio*knob_value+handles.Smover_new);
       %% axes(handles.axes12)
       bar(handles.axes12, handles.Smover_new(:,1:2) - handles.Smover_ini(:,1:2), 0.8);
       set(handles.axes12,'XTickLabel',handles.SextName);
       set(handles.axes12,'xlim',[0.5 length(handles.Smover_new)+0.5]);
       handles.R24v  = handles.R24v + knob_value;
       set(handles.edit43,'String',num2str(handles.R24v,'%3d'));
       set(handles.edit44,'String',num2str(handles.R24v*IP_KNOBV{16}.Unit,'%5.2e'));
       % [max_pv, max_nv, min_pv, min_nv] = knob_limit(handles.SextName, IP_R24_mover);
       
   otherwise
       disp('Please use one of the following knob names:');
       disp({'Beta_x_Waist';'Beta_y_Waist'; 'Eta_x';...
           'T122'; 'T162'; 'T166'; 'T342'; 'T364';...
           'Beta_x_Waist_m';'Beta_y_Waist_m'; 'Eta_x_m'; 'Eta_y';...
           'R13' ; 'R14' ; 'R23' ; 'R24'}  );
data.xsize

end
%% [stat,beamout] = TrackThru( 1, handles.IP_ind, FL.SimBeam{1}, 1, 1, 0 );
[data txt beamout] = GetIPData(FL.SimBeam{1},FL.SimBeam{2},1, handles.IP_ind);
%% [stat,twiss]=GetTwiss(1,length(BEAMLINE), ...
%%  FL.SimModel.Initial.x.Twiss,FL.SimModel.Initial.y.Twiss);
set(handles.edit4,'String',num2str(data.xwaist,'%5.2e'));
set(handles.edit10,'String',num2str(data.xdisp,'%5.2e'));
set(handles.edit5,'String',num2str(data.xsize,'%5.2e'));
set(handles.edit6,'String',num2str(data.emit_x,'%5.2e'));
set(handles.edit7,'String',num2str(data.ywaist,'%5.2e'));
set(handles.edit11,'String',num2str(data.ydisp,'%5.2e'));
set(handles.edit8,'String',num2str(data.ysize,'%5.2e'));
set(handles.edit9,'String',num2str(data.emit_y,'%5.2e'));
%% [stat,beamout,instdata] = TrackThru( 1, IP_ind, FL.SimBeam{1}, 1, 1, 0 );
[ nx,xout] = hist(beamout.Bunch.x(1,:),100);
[ ny,yout] = hist(beamout.Bunch.x(3,:),100); 
%% axes(handles.axes9)
%% cla;
plot(handles.axes6, beamout.Bunch.x(1,:), beamout.Bunch.x(3,:), 'r.'); 
%% h11 = line(beamout.Bunch.x(1,:), beamout.Bunch.x(3,:),'Marker','.','Color','r','LineStyle','none');
%% %% ax1 = gca;
%% %% set(ax1,'XColor','k','YColor','k')
%% %% ax2 = axes('Position',get(ax1,'Position'),...
%% %%            'XAxisLocation','bottom',...
%% %%            'YAxisLocation','right',...
%% %%            'Color','none',...GetIPData
%% %%            'XColor','k','YColor','b');
%% %% ax3 = axes('Position',get(ax1,'Position'),...
%% %%            'XAxisLocation','top',...
%% %%            'YAxisLocation','left',...
%% %%            'Color','none',...
%% %%            'XColor','g','YColor','k');   
%% %% hl2 = line(xout,nx,'Color','b','Marker','.','LineStyle','none','Parent',ax2);
%% %% hl3 = line(ny,yout,'Color','g','Marker','.','LineStyle','none','Parent',ax3);
handles.data(end+1)= data.xwaist;
handles.data(end+1)= data.xdisp;
handles.data(end+1)= data.xsize;
handles.data(end+1)= data.emit_x;

handles.data(end+1)= data.ywaist;
handles.data(end+1)= data.ydisp;
handles.data(end+1)= data.ysize;
handles.data(end+1)= data.emit_y;

handles.B_x_Wa(end+1) = handles.B_x_Wv*IP_KNOBV{1}.Unit;
handles.B_y_Wa(end+1) = handles.B_y_Wv*IP_KNOBV{2}.Unit;
handles.E_xa(end+1) = handles.E_xv*IP_KNOBV{3}.Unit;
handles.T122a(end+1)  = handles.T122v*IP_KNOBV{4}.Unit;
handles.T162a(end+1) = handles.T162v*IP_KNOBV{5}.Unit;
handles.T166a(end+1) = handles.T166v*IP_KNOBV{6}.Unit;
handles.T342a(end+1) = handles.T342v*IP_KNOBV{7}.Unit; 
handles.T364a(end+1) = handles.T364v*IP_KNOBV{8}.Unit; 
handles.B_x_Wma(end+1) = handles.B_x_Wmv*IP_KNOBV{9}.Unit;
handles.B_y_Wma(end+1) = handles.B_y_Wv*IP_KNOBV{10}.Unit;
handles.E_xma(end+1) = handles.E_xmv*IP_KNOBV{11}.Unit;
handles.E_ya(end+1) = handles.E_yv*IP_KNOBV{12}.Unit;
handles.R13a(end+1)  = handles.R13v*IP_KNOBV{13}.Unit;
handles.R14a(end+1)  = handles.R14v*IP_KNOBV{14}.Unit;
handles.R23a(end+1)  = handles.R23v*IP_KNOBV{15}.Unit;
handles.R24a(end+1)  = handles.R24v*IP_KNOBV{16}.Unit;
plot(handles.axes9, handles.data(:,3))
%% data.xsize
%% handles.data(3,:)

plot(handles.axes9, handles.data(handles.plot:8:end), 'r');
%% plot(handles.axes9, beamout.Bunch.x(1,:), beamout.Bunch.x(3,:), 'b.'); 
guidata(hObject,handles);

% --- Executes on button press in pushbutton7.
function pushbutton7_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton7 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)



function edit1_Callback(hObject, eventdata, handles)
% hObject    handle to edit1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit1 as text
%        str2double(get(hObject,'String')) returns contents of edit1 as a double


% --- Executes during object creation, after setting all properties.
function edit1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in pushbutton8.
function pushbutton8_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton8 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global BEAMLINE FL IP_KNOBV;

str7 = get(handles.popupmenu7, 'String');
val7 = get(handles.popupmenu7,'Value');

switch val7
    case 1
         [handles.Qps_value_new] = set_ps(handles.Q_ps,handles.Qps_value_ini);
         %% axes(handles.axes10)
         bar(handles.axes10, handles.Qps_value_new - handles.Qps_value_ini, 0.8);
         set(handles.axes10,'XTickLabel',handles.QuadName);
         set(handles.axes10,'xlim',[0.5 length(handles.Qps_value_new)+0.5]);
         set(handles.edit12,'String','0');
         for ii = 13:18
             eval(['set(handles.edit',num2str(ii),',''String'',''0'')']);
         end
         handles.B_x_Wv = 0;
         handles.B_y_Wv = 0;
         handles.E_xv = 0;
    case 2
        [handles.Sps_value_new] = set_ps(handles.S_ps,handles.Sps_value_ini);
        %% axes(handles.axes11)
        bar(handles.axes11, handles.Sps_value_new - handles.Sps_value_ini, 0.8);
        set(handles.axes11,'XTickLabel',handles.SextName);
        set(handles.axes11,'xlim',[0.5 length(handles.Sps_value_new)+0.5]);
        set(handles.edit12,'String','0');
        for ii = 19:28
            eval(['set(handles.edit',num2str(ii),',''String'',''0'')']);
        end
        handles.T122v  = 0;
        handles.T162v = 0;
        handles.T166v = 0;
        handles.T342v = 0;
        handles.T364v = 0; 
    otherwise
        [old_value, handles.Smover_new] = set_mover(handles.S_Gir, handles.Smover_ini);
        %% axes(handles.axes12)
        bar(handles.axes12, handles.Smover_new(:,1:2) - handles.Smover_ini(:,1:2), 0.8);
        set(handles.axes12,'XTickLabel',handles.SextName);
        set(handles.axes12,'xlim',[0.5 length(handles.Smover_new)+0.5]);
        set(handles.edit12,'String','0');
        for ii = 29:44
            eval(['set(handles.edit',num2str(ii),',''String'',''0'')']);
        end
        handles.B_x_Wmv = 0;
        handles.B_y_Wmv = 0;
        handles.E_xmv = 0;
        handles.E_yv = 0;
        handles.R13v  = 0;
        handles.R14v  = 0;
        handles.R23v  = 0;
        handles.R24v  = 0;
end
[data txt beamout] = GetIPData(FL.SimBeam{1},FL.SimBeam{2},1, handles.IP_ind);
%% [stat,beamout] = TrackThru( 1, handles.IP_ind, FL.SimBeam{1}, 1, 1, 0 );
%% %% [stat,beamout,instdata] = TrackThru( 1, IP_ind, FL.SimBeam{1}, 1, 1, 0 );
%% [stat,twiss]=GetTwiss(1,length(BEAMLINE), ...
%%   FL.SimModel.Initial.x.Twiss,FL.SimModel.Initial.y.Twiss);
set(handles.edit4,'String',num2str(data.xwaist,'%5.2e'));
set(handles.edit10,'String',num2str(data.xdisp,'%5.2e'));
set(handles.edit5,'String',num2str(data.xsize,'%5.2e'));
set(handles.edit6,'String',num2str(data.emit_x,'%5.2e'));
set(handles.edit7,'String',num2str(data.ywaist,'%5.2e'));
set(handles.edit11,'String',num2str(data.ydisp,'%5.2e'));
set(handles.edit8,'String',num2str(data.ysize,'%5.2e'));
set(handles.edit9,'String',num2str(data.emit_y,'%5.2e'));
[ nx,xout] = hist(beamout.Bunch.x(1,:),100);
[ ny,yout] = hist(beamout.Bunch.x(3,:),100);
%% axes(handles.axes9)
%% cla
plot(handles.axes6, beamout.Bunch.x(1,:), beamout.Bunch.x(3,:), 'r.');
%% h11 = line(beamout.Bunch.x(1,:), beamout.Bunch.x(3,:),'Marker','.','Color','r','LineStyle','none');
%% %% ax1 = gca;
%% %% set(ax1,'XColor','k','YColor','k')
%% %% ax2 = axes('Position',get(ax1,'Position'),...
%% %%            'XAxisLocation','bottom',...
%% %%            'YAxisLocation','right',...
%% %%            'Color','none',...
%% %%            'XColor','k','YColor','b');
%% %% ax3 = axes('Position',get(ax1,'Position'),...
%% %%            'XAxisLocation','top',...
%% %%            'YAxisLocation','left',...
%% %%            'Color','none',...
%% %%            'XColor','g','YColor','k');   
%% %% hl2 = line(xout,nx,'Color','b','Marker','.','LineStyle','none','Parent',ax2);
%% %% hl3 = line(ny,yout,'Color','g','Marker','.','LineStyle','none','Parent',ax3);

handles.data(end+1)= data.xwaist;
handles.data(end+1)= data.xdisp;
handles.data(end+1)= data.xsize;
handles.data(end+1)= data.emit_x;

handles.data(end+1)= data.ywaist;
handles.data(end+1)= data.ydisp;
handles.data(end+1)= data.ysize;
handles.data(end+1)= data.emit_y;

handles.B_x_Wa(end+1) = handles.B_x_Wv*IP_KNOBV{1}.Unit;
handles.B_y_Wa(end+1) = handles.B_y_Wv*IP_KNOBV{2}.Unit;
handles.E_xa(end+1) = handles.E_xv*IP_KNOBV{3}.Unit;
handles.T122a(end+1)  = handles.T122v*IP_KNOBV{4}.Unit;
handles.T162a(end+1) = handles.T162v*IP_KNOBV{5}.Unit;
handles.T166a(end+1) = handles.T166v*IP_KNOBV{6}.Unit;
handles.T342a(end+1) = handles.T342v*IP_KNOBV{7}.Unit; 
handles.T364a(end+1) = handles.T364v*IP_KNOBV{8}.Unit; 
handles.B_x_Wma(end+1) = handles.B_x_Wmv*IP_KNOBV{9}.Unit;
handles.B_y_Wma(end+1) = handles.B_y_Wv*IP_KNOBV{10}.Unit;
handles.E_xma(end+1) = handles.E_xmv*IP_KNOBV{11}.Unit;
handles.E_ya(end+1) = handles.E_yv*IP_KNOBV{12}.Unit;
handles.R13a(end+1)  = handles.R13v*IP_KNOBV{13}.Unit;
handles.R14a(end+1)  = handles.R14v*IP_KNOBV{14}.Unit;
handles.R23a(end+1)  = handles.R23v*IP_KNOBV{15}.Unit;
handles.R24a(end+1)  = handles.R24v*IP_KNOBV{16}.Unit;
guidata(hObject,handles);


function edit2_Callback(hObject, eventdata, handles)
% hObject    handle to edit2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit2 as text
%        str2double(get(hObject,'String')) returns contents of edit2 as a double


% --- Executes during object creation, after setting all properties.
function edit2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit3_Callback(hObject, eventdata, handles)
% hObject    handle to edit3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit3 as text
%        str2double(get(hObject,'String')) returns contents of edit3 as a double


% --- Executes during object creation, after setting all properties.
function edit3_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in popupmenu7.
function popupmenu7_Callback(hObject, eventdata, handles)
% hObject    handle to popupmenu7 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns popupmenu7 contents as cell array
%        contents{get(hObject,'Value')} returns selected item from popupmenu7


% --- Executes during object creation, after setting all properties.
function popupmenu7_CreateFcn(hObject, eventdata, handles)
% hObject    handle to popupmenu7 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in pushbutton9.
function pushbutton9_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton9 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
%% range = eval(['[',get(handles.edit2,'String'),']']);
%% step = eval(['[',get(handles.edit1,'String'),']']);
global IP_KNOBV;

str7 = get(handles.popupmenu7, 'String');
val7 = get(handles.popupmenu7,'Value');

switch val7
    case 1
        [IP_KNOBV] = Knob_qs_mad(handles.QuadName);
        %% IP_KNOBV{1}.Unit
    case 2
        [IP_KNOBV] = Knob_ss_mad(handles.SextName);
        %% IP_KNOBV{4}.Unit
    otherwise
        [IP_KNOBV] = Knob_sxy_mad(handles.SextName);
        %% IP_KNOBV{9}.Unit
end
%% resp=questdlg('Do you want to save file?','Recalculate Request','Yes','No','No');
%% if ~strcmp(resp,'Yes'); return; end;
%% try  
%%   Q_ps = handles.Q_ps ;
%%   S_ps = handles.S_ps ;
%%   S_Gir = handles.S_Gir ;
%%   Qps_value = handles.Qps_value_new ;
%%   Sps_value = handles.Sps_value_new ;
%%   Smover = handles.Smover_new ; 
%%   file_name = inputdlg('Input file name:','Save file',1,{'testdata'})
%%   beamdata = handles.data;
%%   setdata = [handles.B_x_Wa; handles.B_y_Wa; handles.E_xa;...
%%       handles.T122a; handles.T162a; handles.T166a; handles.T342a; handles.T364a;...
%%       handles.B_x_Wma; handles.B_y_Wma; handles.E_xma; handles.E_ya;...
%%       handles.R13a; handles.R14a; handles.R23a; handles.R24a];
%%   
%%   eval(['save .\testApps\IP_knob\data\',file_name{1},' Qps_value Sps_value Smover Q_ps S_ps S_Gir beamdata setdata  ;']);   
%%   %% save .\testApps\IP_knob\data\file_name Qps_value Sps_value Smover Q_ps S_ps S_Gir beamdata setdata ;  
%% catch
%%  %% exit
%%  end

%% %% load .\testApps\IP_knob\KNOB_VALUE;

% Proceed with callback...

function edit4_Callback(hObject, eventdata, handles)
% hObject    handle to edit4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit4 as text
%        str2double(get(hObject,'String')) returns contents of edit4 as a double


% --- Executes during object creation, after setting all properties.
function edit4_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit5_Callback(hObject, eventdata, handles)
% hObject    handle to edit5 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit5 as text
%        str2double(get(hObject,'String')) returns contents of edit5 as a double


% --- Executes during object creation, after setting all properties.
function edit5_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit5 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit6_Callback(hObject, eventdata, handles)
% hObject    handle to edit6 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit6 as text
%        str2double(get(hObject,'String')) returns contents of edit6 as a double


% --- Executes during object creation, after setting all properties.
function edit6_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit6 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit7_Callback(hObject, eventdata, handles)
% hObject    handle to edit7 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit7 as text
%        str2double(get(hObject,'String')) returns contents of edit7 as a double


% --- Executes during object creation, after setting all properties.
function edit7_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit7 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit8_Callback(hObject, eventdata, handles)
% hObject    handle to edit8 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit8 as text
%        str2double(get(hObject,'String')) returns contents of edit8 as a double


% --- Executes during object creation, after setting all properties.
function edit8_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit8 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit9_Callback(hObject, eventdata, handles)
% hObject    handle to edit9 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit9 as text
%        str2double(get(hObject,'String')) returns contents of edit9 as a double


% --- Executes during object creation, after setting all properties.
function edit9_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit9 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit10_Callback(hObject, eventdata, handles)
% hObject    handle to edit10 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit10 as text
%        str2double(get(hObject,'String')) returns contents of edit10 as a double


% --- Executes during object creation, after setting all properties.
function edit10_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit10 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit11_Callback(hObject, eventdata, handles)
% hObject    handle to edit11 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit11 as text
%        str2double(get(hObject,'String')) returns contents of edit11 as a double


% --- Executes during object creation, after setting all properties.
function edit11_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit11 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit12_Callback(hObject, eventdata, handles)
% hObject    handle to edit12 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit12 as text
%        str2double(get(hObject,'String')) returns contents of edit12 as a double


% --- Executes during object creation, after setting all properties.
function edit12_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit12 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit13_Callback(hObject, eventdata, handles)
% hObject    handle to edit13 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit13 as text
%        str2double(get(hObject,'String')) returns contents of edit13 as a double


% --- Executes during object creation, after setting all properties.
function edit13_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit13 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit14_Callback(hObject, eventdata, handles)
% hObject    handle to edit14 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit14 as text
%        str2double(get(hObject,'String')) returns contents of edit14 as a double


% --- Executes during object creation, after setting all properties.
function edit14_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit14 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit15_Callback(hObject, eventdata, handles)
% hObject    handle to edit15 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit15 as text
%        str2double(get(hObject,'String')) returns contents of edit15 as a double


% --- Executes during object creation, after setting all properties.
function edit15_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit15 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit16_Callback(hObject, eventdata, handles)
% hObject    handle to edit16 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit16 as text
%        str2double(get(hObject,'String')) returns contents of edit16 as a double


% --- Executes during object creation, after setting all properties.
function edit16_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit16 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit17_Callback(hObject, eventdata, handles)
% hObject    handle to edit17 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit17 as text
%        str2double(get(hObject,'String')) returns contents of edit17 as a double


% --- Executes during object creation, after setting all properties.
function edit17_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit17 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit18_Callback(hObject, eventdata, handles)
% hObject    handle to edit18 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit18 as text
%        str2double(get(hObject,'String')) returns contents of edit18 as a double


% --- Executes during object creation, after setting all properties.
function edit18_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit18 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit19_Callback(hObject, eventdata, handles)
% hObject    handle to edit19 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit19 as text
%        str2double(get(hObject,'String')) returns contents of edit19 as a double


% --- Executes during object creation, after setting all properties.
function edit19_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit19 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit20_Callback(hObject, eventdata, handles)
% hObject    handle to edit20 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit20 as text
%        str2double(get(hObject,'String')) returns contents of edit20 as a double


% --- Executes during object creation, after setting all properties.
function edit20_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit20 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit21_Callback(hObject, eventdata, handles)
% hObject    handle to edit21 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit21 as text
%        str2double(get(hObject,'String')) returns contents of edit21 as a double


% --- Executes during object creation, after setting all properties.
function edit21_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit21 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit22_Callback(hObject, eventdata, handles)
% hObject    handle to edit22 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit22 as text
%        str2double(get(hObject,'String')) returns contents of edit22 as a double


% --- Executes during object creation, after setting all properties.
function edit22_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit22 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit23_Callback(hObject, eventdata, handles)
% hObject    handle to edit23 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit23 as text
%        str2double(get(hObject,'String')) returns contents of edit23 as a double


% --- Executes during object creation, after setting all properties.
function edit23_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit23 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit24_Callback(hObject, eventdata, handles)
% hObject    handle to edit24 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit24 as text
%        str2double(get(hObject,'String')) returns contents of edit24 as a double


% --- Executes during object creation, after setting all properties.
function edit24_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit24 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit25_Callback(hObject, eventdata, handles)
% hObject    handle to edit25 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit25 as text
%        str2double(get(hObject,'String')) returns contents of edit25 as a double


% --- Executes during object creation, after setting all properties.
function edit25_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit25 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit26_Callback(hObject, eventdata, handles)
% hObject    handle to edit26 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit26 as text
%        str2double(get(hObject,'String')) returns contents of edit26 as a double


% --- Executes during object creation, after setting all properties.
function edit26_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit26 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit27_Callback(hObject, eventdata, handles)
% hObject    handle to edit27 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit27 as text
%        str2double(get(hObject,'String')) returns contents of edit27 as a double


% --- Executes during object creation, after setting all properties.
function edit27_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit27 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit28_Callback(hObject, eventdata, handles)
% hObject    handle to edit28 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit28 as text
%        str2double(get(hObject,'String')) returns contents of edit28 as a double


% --- Executes during object creation, after setting all properties.
function edit28_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit28 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit29_Callback(hObject, eventdata, handles)
% hObject    handle to edit29 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit29 as text
%        str2double(get(hObject,'String')) returns contents of edit29 as a double


% --- Executes during object creation, after setting all properties.
function edit29_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit29 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit30_Callback(hObject, eventdata, handles)
% hObject    handle to edit30 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit30 as text
%        str2double(get(hObject,'String')) returns contents of edit30 as a double


% --- Executes during object creation, after setting all properties.
function edit30_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit30 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit31_Callback(hObject, eventdata, handles)
% hObject    handle to edit31 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit31 as text
%        str2double(get(hObject,'String')) returns contents of edit31 as a double


% --- Executes during object creation, after setting all properties.
function edit31_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit31 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit32_Callback(hObject, eventdata, handles)
% hObject    handle to edit32 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit32 as text
%        str2double(get(hObject,'String')) returns contents of edit32 as a double


% --- Executes during object creation, after setting all properties.
function edit32_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit32 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit33_Callback(hObject, eventdata, handles)
% hObject    handle to edit33 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit33 as text
%        str2double(get(hObject,'String')) returns contents of edit33 as a double


% --- Executes during object creation, after setting all properties.
function edit33_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit33 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit34_Callback(hObject, eventdata, handles)
% hObject    handle to edit34 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit34 as text
%        str2double(get(hObject,'String')) returns contents of edit34 as a double


% --- Executes during object creation, after setting all properties.
function edit34_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit34 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit35_Callback(hObject, eventdata, handles)
% hObject    handle to edit35 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit35 as text
%        str2double(get(hObject,'String')) returns contents of edit35 as a double


% --- Executes during object creation, after setting all properties.
function edit35_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit35 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit36_Callback(hObject, eventdata, handles)
% hObject    handle to edit36 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit36 as text
%        str2double(get(hObject,'String')) returns contents of edit36 as a double


% --- Executes during object creation, after setting all properties.
function edit36_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit36 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit37_Callback(hObject, eventdata, handles)
% hObject    handle to edit37 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit37 as text
%        str2double(get(hObject,'String')) returns contents of edit37 as a double


% --- Executes during object creation, after setting all properties.
function edit37_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit37 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit38_Callback(hObject, eventdata, handles)
% hObject    handle to edit38 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit38 as text
%        str2double(get(hObject,'String')) returns contents of edit38 as a double


% --- Executes during object creation, after setting all properties.
function edit38_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit38 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit39_Callback(hObject, eventdata, handles)
% hObject    handle to edit39 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit39 as text
%        str2double(get(hObject,'String')) returns contents of edit39 as a double


% --- Executes during object creation, after setting all properties.
function edit39_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit39 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit40_Callback(hObject, eventdata, handles)
% hObject    handle to edit40 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit40 as text
%        str2double(get(hObject,'String')) returns contents of edit40 as a double


% --- Executes during object creation, after setting all properties.
function edit40_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit40 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit41_Callback(hObject, eventdata, handles)
% hObject    handle to edit41 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit41 as text
%        str2double(get(hObject,'String')) returns contents of edit41 as a double


% --- Executes during object creation, after setting all properties.
function edit41_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit41 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit42_Callback(hObject, eventdata, handles)
% hObject    handle to edit42 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit42 as text
%        str2double(get(hObject,'String')) returns contents of edit42 as a double


% --- Executes during object creation, after setting all properties.
function edit42_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit42 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit43_Callback(hObject, eventdata, handles)
% hObject    handle to edit43 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit43 as text
%        str2double(get(hObject,'String')) returns contents of edit43 as a double


% --- Executes during object creation, after setting all properties.
function edit43_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit43 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit44_Callback(hObject, eventdata, handles)
% hObject    handle to edit44 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit44 as text
%        str2double(get(hObject,'String')) returns contents of edit44 as a double


% --- Executes during object creation, after setting all properties.
function edit44_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit44 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in pushbutton10.
function pushbutton10_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton10 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
Q_ps = handles.Q_ps ;
S_ps = handles.S_ps ;
S_Gir = handles.S_Gir ;
Qps_value = handles.Qps_value_new ;
Sps_value = handles.Sps_value_new ;
Smover = handles.Smover_new ; 
file_name = inputdlg('Input file name:','Save file',1,{'testdata'})
beamdata = handles.data;
setdata = [handles.B_x_Wa; handles.B_y_Wa; handles.E_xa;...
    handles.T122a; handles.T162a; handles.T166a; handles.T342a; handles.T364a;...
    handles.B_x_Wma; handles.B_y_Wma; handles.E_xma; handles.E_ya;...
    handles.R13a; handles.R14a; handles.R23a; handles.R24a];

eval(['save .\testApps\IP_knob\data\',file_name{1},' Qps_value Sps_value Smover Q_ps S_ps S_Gir beamdata setdata  ;']);   
%% save .\testApps\IP_knob\data\file_name Qps_value Sps_value Smover Q_ps S_ps S_Gir ;   



function edit55_Callback(hObject, eventdata, handles)
% hObject    handle to edit55 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit55 as text
%        str2double(get(hObject,'String')) returns contents of edit55 as a double


% --- Executes during object creation, after setting all properties.
function edit55_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit55 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in popupmenu8.
function popupmenu8_Callback(hObject, eventdata, handles)
% hObject    handle to popupmenu8 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = get(hObject,'String') returns popupmenu8 contents as cell array
%        contents{get(hObject,'Value')} returns selected item from popupmenu8


% --- Executes during object creation, after setting all properties.
function popupmenu8_CreateFcn(hObject, eventdata, handles)
% hObject    handle to popupmenu8 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in pushbutton11.
function pushbutton11_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton11 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
handles.plot = get(handles.popupmenu8,'Value');
plot(handles.axes9, handles.data(handles.plot:8:end), 'r');
guidata(hObject,handles);







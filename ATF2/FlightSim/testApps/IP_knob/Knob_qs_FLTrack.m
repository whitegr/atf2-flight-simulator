function [IP_KNOBV] = Knob_qs_FLTrack(QuadName, varargin )
%%% 
%% Construct quadrupole strength knob to adjust the beta x, y waist and eta x at IP using MAD. 
%% [IP_KNOBV] = Knob_qs_mad(QuadName, varargin )
%% QuadName: The name of quadrupoles to construct the knob
%% IP_KNOBV: The output of the knob data in cell with three fields.
%%     IP_KNOBV{:}.Name: The name of the knob.
%%     IP_KNOBV{:}.Unit: The unit of the knob value coresponding to the
%%     minimum adjusting step of power supply 10e-4 or Sext mover 10e-6 m. 
%%     IP_KNOBV{:}.Ratio: The ratio of the power supply or mover postion to
%%     change the knob value in unit value. 
%% 
%% Exampe 
%%
%% QuadName = { 'QD6FF'; 'QF5BFF'; 'QF5AFF'};
%% IP_KNOBV{1}.Name = 'Beta_x_Waist';
%% IP_KNOBV{1}.Unit = 3.5658e-005
%% IP_KNOBV{1}.Ratio =[0.1000   -0.2307    0.2346]*1e-3
%% 
%% Auther: Min-Huey Wang SLAC National Accelerator Laboratory
%% Date: Dec 2009 

%%function [MagRatioSet] = Knob_qs_mad(quadname, range, step)

global FL BEAMLINE PS

%% load ./testApps/IP_knob/KNOB_VALUE_FLT
%% QuadName = { 'QD6FF'; 'QF5BFF'; 'QF5AFF'};
l_QName = length(QuadName);
Qps_value_ini = zeros(1,l_QName);
Q_Ps = zeros(1, l_QName);
Q_Gir = zeros(1, l_QName);
for i = 1:l_QName
    ind = findcells(BEAMLINE,'Name',QuadName{i});
    Q_Ps(i) = BEAMLINE{ind(1)}.PS;
    Qps_value_ini(i) = PS(Q_Ps(i)).Ampl ;
    Q_Gir(i) = BEAMLINE{ind(1)}.Girder;
end
    IP_ind = findcells(BEAMLINE,'Name','IP');
%% request={[] Q_Ps []};
%%        [stat respQ] = AccessRequest(request);
%%         if(stat{1}==-1)
%%             display(sprintf('AccessRequest returned error :%s',stat{2}));
%%         else
%%             display(sprintf('Access granted, ID of request : %s',respQ));
%%         end

ps_step = 1e-4;

FF_MAD_ini;

if nargin>=2 
    ds = (-varargin{1}*ps_step :ps_step:varargin{1}*ps_step ); 
else
   %% ds = [1e-4:1e-4:1e-3];
   ds = (-1.0e-3:1e-4:1.0e-3);
end

%% ds = [5e-5:5e-5:1e-3,1.5e-3:5e-4:1e-2]; %% if the ds = [5e-5:5e-5:1e-2];
%% ds = [1e-5:1e-5:1e-4];
l_ds = length(ds);
BX = zeros(1,l_ds);
ALX = zeros(1,l_ds);
BX = zeros(1,l_ds);
ALX = zeros(1,l_ds);
SXBX = zeros(1,l_ds);
SXBY = zeros(1,l_ds);
SXETX = zeros(1,l_ds);
SXBX_r = zeros(l_QName,2);
SXBY_r = zeros(l_QName,2);
SXETX_r = zeros(l_QName,2);
SXBX_r2 = zeros(l_QName,3);
SXBY_r2 = zeros(l_QName,3);
SXETX_r2 = zeros(l_QName,3);

[data0 txt beamout0] = GetIPSimData(FL.SimBeam{1},FL.SimBeam{2},1, IP_ind);
for ii = 1:l_QName
    for jj = 1:l_ds
        PS(Q_Ps(ii)).Ampl = Qps_value_ini(ii)+ds(jj);
        [data txt beamout] = GetIPSimData(FL.SimBeam{1},FL.SimBeam{2},1, IP_ind);
        [stat,Twissd]=GetTwiss(1,length(BEAMLINE), ...
        FL.SimModel.Initial.x.Twiss,FL.SimModel.Initial.y.Twiss);

   %%     BX(jj)=Twissd.betax(IP_ind);
   %%     ALX(jj)=Twissd.alphax(IP_ind);
   %%     BY(jj)=Twissd.betay(IP_ind);
   %%     ALY(jj)=Twissd.alphay(IP_ind);
       
        SXBX(jj)=data.xwaist;
        SXBY(jj)=data.ywaist;
        SXETX(jj)=data.xdisp;
    end
    PS(Q_Ps(ii)).Ampl = Qps_value_ini(ii);
    SXBX_r(ii,:) = polyfit(ds,SXBX,1);
    SXBY_r(ii,:) = polyfit(ds,SXBY,1);
    SXETX_r(ii,:) = polyfit(ds,SXETX,1);
    SXBX_r2(ii,:) = polyfit(ds,SXBX,2);
    SXBY_r2(ii,:) = polyfit(ds,SXBY,2);
    SXETX_r2(ii,:) = polyfit(ds,SXETX,2);
end
%% plot(ds, SXBX);
%% hold ;
%% plot(ds, polyval(SXBX_r(end,:), ds),'ro');
%% reset to zero 
AA = [SXBX_r(:,1)'; SXBY_r(:,1)'; SXETX_r(:,1)'];
IP_Beta_x_Waist_ratio = AA\[1;0;0]; %% adjust in unit of m  %% range 
IP_Beta_y_Waist_ratio = AA\[0;1;0]; %% adjust in unit of m
IP_Eta_x_ratio = AA\[0;0;1];        %% adjust in unit of m

%% switch lower(knobname)
%%    case {'ip_beta_x_waist'}
%%        [MagRatioSet] = knob_set(QuadName, IP_Beta_x_Waist', knob_value);
%%    case {'ip_beta_y_waist'}
%%        [MagRatioSet] = knob_set(QuadName, IP_Beta_y_Waist', knob_value);
%%    case {'ip_eta_x'}
%%        [MagRatioSet] = knob_set(QuadName, IP_Eta_x', knob_value);
%%    otherwise
%%        disp('Please use one of the following knob names:');
%%        disp({'IP_Beta_x_Waist';'IP_Beta_y_Waist'; 'IP_Eta_x'}  );
%% end
IP_KNOBV{1}.Name = 'Beta_x_Waist';
IP_KNOBV{2}.Name = 'Beta_y_Waist';
IP_KNOBV{3}.Name = 'Eta_x';
IP_KNOBV{1}.Unit = ps_step/min(abs(IP_Beta_x_Waist_ratio)); 
IP_KNOBV{2}.Unit = ps_step/min(abs(IP_Beta_y_Waist_ratio)); 
IP_KNOBV{3}.Unit = ps_step/min(abs(IP_Eta_x_ratio)); 
IP_KNOBV{1}.Ratio(:,1) = IP_Beta_x_Waist_ratio'*IP_KNOBV{1}.Unit;
IP_KNOBV{2}.Ratio(:,1) = IP_Beta_y_Waist_ratio'*IP_KNOBV{2}.Unit;
IP_KNOBV{3}.Ratio(:,1) = IP_Eta_x_ratio'*IP_KNOBV{3}.Unit;
IP_KNOBV{1}.Ratio(:,2:4) = zeros(length(IP_KNOBV{1}.Ratio(:,1)),3);
IP_KNOBV{2}.Ratio(:,2:4) = zeros(length(IP_KNOBV{2}.Ratio(:,1)),3);
IP_KNOBV{3}.Ratio(:,2:4) = zeros(length(IP_KNOBV{3}.Ratio(:,1)),3);
IP_KNOBV{1}.Ind(:,1) = Q_Ps ;
IP_KNOBV{2}.Ind(:,1) = Q_Ps ;
IP_KNOBV{3}.Ind(:,1) = Q_Ps ;
IP_KNOBV{1}.Ind(:,2) = Q_Gir ;
IP_KNOBV{2}.Ind(:,2) = Q_Gir ;
IP_KNOBV{3}.Ind(:,2) = Q_Gir ;

%% %% save .\testApps\IP_knob\KNOB_VALUE QuadName IP_Beta_x_Waist_ratio IP_Beta_y_Waist_ratio IP_Eta_x_ratio -append;
%% save ./testApps/IP_knob/KNOB_VALUE_FLT IP_KNOBV ;
%% [stat resp] = AccessRequest('release',respQ);
end


          


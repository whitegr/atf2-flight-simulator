function [data txt B1] = GetIPData(beam,beam0,strack,etrack)
%% author Glen White
global BEAMLINE
persistent ip1 ip2 mqf9

if isempty(ip1)
  ip1=findcells(BEAMLINE,'Name','IP');
  ip2=findcells(BEAMLINE,'Name','MW1IP');
  mqf9=findcells(BEAMLINE,'Name','MQF9X');
end

gamma=1.3/0.511e-3;
if strack<mqf9
  [stat,B1]=TrackThru(strack,mqf9,beam,1,1,0);
  [nx,ny] = GetNEmitFromBeam( B1, 1 );
  data.extemit_y=ny/gamma;
  [stat,B1]=TrackThru(mqf9,etrack,B1,1,1,0);
else
  [stat,B1]=TrackThru(strack,etrack,beam,1,1,0);
end
[nx,ny] = GetNEmitFromBeam( B1, 1 );
data.emit_x=nx/gamma; data.emit_y=ny/gamma;
data.xsize=std(B1.Bunch.x(1,:)); data.xpsize=std(B1.Bunch.x(2,:));
data.ysize=std(B1.Bunch.x(3,:)); data.ypsize=std(B1.Bunch.x(4,:));
data.sigma=cov(B1.Bunch.x');
R=diag(ones(1,6));L=zeros(6,6);L(1,2)=1;L(3,4)=1;
data.xwaist=fminsearch(@(x) minWaist(x,R,L,data.sigma,1),0,optimset('Tolx',1e-6,'TolFun',0.1e-6^2));
data.ywaist=fminsearch(@(x) minWaist(x,R,L,data.sigma,3),0,optimset('Tolx',1e-6,'TolFun',0.1e-6^2));
E_nominal=mean(beam.Bunch.x(6,:));
espread=0.5e-2;
ndisp=10;
disp=-espread/2:espread/(ndisp-1):espread/2;
beam1=beam0;
for idisp=1:ndisp
  beam1.Bunch.x(6,:)=beam0.Bunch.x(6,:)+(E_nominal*disp(idisp));
  [stat,B]=TrackThru(strack,ip1,beam1,1,1,0);
  ip1_x(idisp)=mean(B.Bunch.x(1,:)); ip1_y(idisp)=mean(B.Bunch.x(3,:));
  [stat,B]=TrackThru(strack,ip1,beam1,1,1,0);
  ip1_x(idisp)=mean(B.Bunch.x(1,:)); ip1_y(idisp)=mean(B.Bunch.x(3,:));
  [stat,B]=TrackThru(ip1,ip2,beam1,1,1,0);
  ip2_x(idisp)=mean(B.Bunch.x(1,:)); ip2_y(idisp)=mean(B.Bunch.x(3,:));
end
q=noplot_polyfit(disp,ip1_x,1,1);
dx1 = q(2);
q=noplot_polyfit(disp,ip1_y,1,1);
dy1 = q(2);
q=noplot_polyfit(disp,ip2_x,1,1);
dx2 = q(2);
q=noplot_polyfit(disp,ip2_y,1,1);
dy2 = q(2);
if etrack < ip2
  data.xdisp=dx1; data.ydisp=dy1;
else
  data.xdisp=dx2; data.ydisp=dy2;
end
data.xdp=(dx2-dx1)/(BEAMLINE{ip2}.S-BEAMLINE{ip1}.S);
data.ydp=(dy2-dy1)/(BEAMLINE{ip2}.S-BEAMLINE{ip1}.S);
txt=sprintf('\nTarget Spot Sizes: %g / %g (x / x'') %g / %g (y / y'')\nWaist Offsets: %g (x) %g (y)\nDispersion: %g (x) %g (y)\ncoupling (31 / 32): %g / %g',...
  data.xsize,data.xpsize,data.ysize,data.ypsize,data.xwaist,data.ywaist,data.xdisp,data.ydisp,data.sigma(1,3),data.sigma(2,3));

function chi2 = minWaist(x,R,L,sig,dir)

newsig=(R+L.*x(1))*sig*(R+L.*x(1))';
chi2=newsig(dir,dir)^2;

function chi2 = sinFit(x,data,error) %#ok<DEFNU>

chi2=sum( ( data - ( x(1) * sin((1:length(data))/x(2)+2*pi*x(3))+mean(data) ) ).^2 ./ error.^2);
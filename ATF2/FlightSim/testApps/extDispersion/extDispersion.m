function varargout = extDispersion(varargin)
% EXTDISPERSION M-file for extDispersion.fig
%      EXTDISPERSION, by itself, creates a new EXTDISPERSION or raises the existing
%      singleton*.
%
%      H = EXTDISPERSION returns the handle to a new EXTDISPERSION or the
%      handle to
%      the existing singleton*.
%
%      EXTDISPERSION('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in EXTDISPERSION.M with the given input arguments.
%
%      EXTDISPERSION('Property','Value',...) creates a new EXTDISPERSION or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before extDispersion_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to extDispersion_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help extDispersion

% Last Modified by GUIDE v2.5 25-Oct-2012 22:41:06

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @extDispersion_OpeningFcn, ...
                   'gui_OutputFcn',  @extDispersion_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT

% ------------------------------------------------------------------------------

% --- Executes just before extDispersion is made visible.
function extDispersion_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to extDispersion (see VARARGIN)
global FL

% Choose default command line output for extDispersion
handles.output = handles;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes extDispersion wait for user response (see UIRESUME)
% uiwait(handles.figure1);

% Make sure have update model at least once
FlHwUpdate;

% Initialise parameters
initPars(handles)

% Get region info for disp plots
handles=initRegions(handles);

% Create two figures ... make their handles globally accessible via FL
if (~isfield(FL.Gui,'extDispersion_plot1'))
  FL.Gui.extDispersion_plot1=figure; % create first figure ...
  set(FL.Gui.extDispersion_plot1,'Visible','off') % ... and hide it
end
if (~isfield(FL.Gui,'extDispersion_plot2'))
  FL.Gui.extDispersion_plot2=figure; % create first figure ...
  set(FL.Gui.extDispersion_plot2,'Visible','off') % ... and hide it
end
handles.plot1=FL.Gui.extDispersion_plot1; % for local GUI use
handles.plot2=FL.Gui.extDispersion_plot2; % for local GUI use

% fields to restore from last gui run
restoreList={...
  'radiobutton3','Value',1; ...
  'radiobutton4','Value',1; ...
  'popupmenu3','Value',0; ...
  'edit5','String',1; ...
  'edit10','String',1; ...
  'edit11','String',1; ...
  'radiobutton9','Value',0; ...
  'radiobutton10','Value',0};
guiRestoreFn('extDispersion',handles,restoreList);

% Access control request
pushbutton3_Callback(handles.pushbutton3, eventdata, handles);

% Update handles structure
guidata(hObject, handles);

% Need to select BPMs one time
pushbutton7_Callback(handles.pushbutton7, [], handles);

return

% --- Initialise fields from parameters
function initPars(handles)
global BEAMLINE
[stat pars]=extDispersion_run('getpars');
if stat{1}~=1; errordlg(stat{2},'Initialisation error'); return; end;
set(handles.edit2,'String',sprintf('%3.3f',pars.alpha_p*1e3));
if pars.doqcut
  set(handles.checkbox1,'Value',1)
else
  set(handles.checkbox1,'Value',0)
end
set(handles.edit8,'String',num2str(pars.qcut))
set(handles.edit5,'String',sprintf('%d',pars.nbpm_ave));
set(handles.edit11,'String',num2str(pars.maxknob))
if pars.qmeas
  set(handles.radiobutton4,'Value',1)
  set(handles.radiobutton3,'Value',0)
else
  set(handles.radiobutton4,'Value',0)
  set(handles.radiobutton3,'Value',1)
end
set(handles.edit10,'String',num2str(pars.nstep))
set(handles.checkbox3,'Value',pars.wiremeas)
set(handles.radiobutton1,'Value',pars.corset(1))
set(handles.radiobutton5,'Value',pars.corset(2))
set(handles.radiobutton2,'Value',pars.corset(3))
set(handles.radiobutton6,'Value',pars.corset(4))
set(handles.edit12,'String',num2str(pars.fudge(1)))
set(handles.edit13,'String',num2str(pars.fudge(2)))
set(handles.edit14,'String',num2str(pars.fudge(3)))
set(handles.edit15,'String',num2str(pars.fudge(4)))
set(handles.edit16,'String',num2str(pars.YLim(1)))
set(handles.edit17,'String',num2str(pars.YLim(2)))
set(handles.edit18,'String',num2str(pars.YLim(3)))
set(handles.edit19,'String',num2str(pars.YLim(4)))
if pars.doYLim(1)
  set(handles.radiobutton13,'Value',0)
  set(handles.radiobutton14,'Value',1)
  set(handles.edit16,'Enable','on')
  set(handles.edit17,'Enable','on')
else
  set(handles.radiobutton13,'Value',1)
  set(handles.radiobutton14,'Value',0)
  set(handles.edit16,'Enable','of')
  set(handles.edit17,'Enable','of')
end
if pars.doYLim(2)
  set(handles.radiobutton15,'Value',0)
  set(handles.radiobutton16,'Value',1)
  set(handles.edit18,'Enable','on')
  set(handles.edit19,'Enable','on')
else
  set(handles.radiobutton15,'Value',1)
  set(handles.radiobutton16,'Value',0)
  set(handles.edit18,'Enable','off')
  set(handles.edit19,'Enable','off')
end
if pars.docalcknob
  set(handles.radiobutton9,'Value',0)
  set(handles.radiobutton10,'Value',1)
else
  set(handles.radiobutton9,'Value',1)
  set(handles.radiobutton10,'Value',0)
end
if pars.measMethod==1
  set(handles.radiobutton11,'Value',1)
  set(handles.radiobutton12,'Value',0)
else
  set(handles.radiobutton11,'Value',0)
  set(handles.radiobutton12,'Value',1)
end
set(handles.popupmenu1,'String',arrayfun(@(x) BEAMLINE{x}.Name,pars.bpmind,'UniformOutput',false))
set(handles.popupmenu1,'Value',find(pars.bpmind==pars.target.ind))

% initialise plot pulldown menu
diagPlot(handles)

function handles=initRegions(handles)
global BEAMLINE
id=[1; ...                                   % 1
  findcells(BEAMLINE,'Name','IEX'); ...      % 2
  findcells(BEAMLINE,'Name','MDISP'); ...    % 3
  findcells(BEAMLINE,'Name','BEGFF'); ...    % 4
  findcells(BEAMLINE,'Name','B5FFA')-1; ...  % 5
  findcells(BEAMLINE,'Name','MQD0FF')-1; ... % 6
  findcells(BEAMLINE,'Name','IP'); ...       % 7
  findcells(BEAMLINE,'Name','BDUMPA')-1; ... % 8
  length(BEAMLINE); ...                      % 9
];
handles.Region.DR.ind=[id(1),id(2)];        % DR
handles.Region.EXT.ind=[id(2),id(4)];       % EXT
handles.Region.FF.ind=[id(4),id(7)];        % Final Focus (to IP)
handles.Region.FFdump.ind=[id(7),id(9)];    % Final Focus (post IP)
handles.Region.EXTFF.ind=[id(2),id(7)];     % EXT+FF (to IP)
handles.Region.ALL.ind=[id(1),id(9)];       % DR+EXT+FF+FFdump
handles.Region.IP.ind=[id(6),id(8)];        % IP region
handles.Region.FitRegion.ind=[id(3),id(5)]; % residual dispersion fit region

set(handles.popupmenu3,'String',fieldnames(handles.Region))

% ------------------------------------------------------------------------------

% --- Outputs from this function are returned to the command line.
function varargout = extDispersion_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;

% ------------------------------------------------------------------------------

% --- Measure
function pushbutton1_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global FL

FL.Gui.extDispersion.pushbutton1=handles.pushbutton1;
FL.Gui.extDispersion.text6=handles.text6;
FL.Gui.extDispersion.quietMeas=false;
stat=extDispersion_run('measure');
FL.Gui.extDispersion=rmfield(FL.Gui.extDispersion,'text6');
FL.Gui.extDispersion=rmfield(FL.Gui.extDispersion,'quietMeas');
if (stat{1}<0)
  errordlg(stat{2},'Measure error')
  return
end
diagPlot(handles)

% save to model?
pushbutton6_Callback(handles.pushbutton6,[],handles);


% ------------------------------------------------------------------------------

% --- Correct
function pushbutton2_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
stat{1}=1;
if get(handles.radiobutton18,'Value')
  stat=extDispersion_run('verify');
end
if stat{1}==1
  set(handles.text6,'String','Setting Dispersion Correction...');
  set(handles.text6,'ForegroundColor','black');
  stat = extDispersion_run('correct');
  if stat{1}==1
    set(handles.text6,'String','Finished Setting Dispersion Correction');
    set(handles.text6,'ForegroundColor','black');
  else
    set(handles.text6,'String',['Error correcting dispersion:\n',stat{2}]);
    set(handles.text6,'ForegroundColor','red');
  end % if error
else
  set(handles.text6,'String',stat{2});
end

% ------------------------------------------------------------------------------

% --- Executes during object creation, after setting all properties.
function edit1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% ------------------------------------------------------------------------------

% --- Executes during object creation, after setting all properties.
function edit2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function edit2_Callback(hObject, eventdata, handles)
% hObject    handle to edit2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit2 as text
%        str2double(get(hObject,'String')) returns contents of edit2 as a double
pars.alpha_p=str2double(get(hObject,'String'))*1e-3;
extDispersion_run('setpars',pars);

% ------------------------------------------------------------------------------

% --- Executes during object creation, after setting all properties.
function edit3_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function edit3_Callback(hObject, eventdata, handles)
% hObject    handle to edit3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit3 as text
%        str2double(get(hObject,'String')) returns contents of edit3 as a double
pars.dr_ramp=str2double(get(hObject,'String')); % kHz
extDispersion_run('setpars',pars);

% ------------------------------------------------------------------------------

% --- Executes during object creation, after setting all properties.
function popupmenu2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to popupmenu2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% --- Executes on selection change in popupmenu2.
function popupmenu2_Callback(hObject, eventdata, handles)
% hObject    handle to popupmenu2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = get(hObject,'String') returns popupmenu2 contents as cell array
%        contents{get(hObject,'Value')} returns selected item from popupmenu2
contents = get(hObject,'String');
pars.nramp=str2double(contents{get(hObject,'Value')});
extDispersion_run('setpars',pars);

% ------------------------------------------------------------------------------

% --- Executes during object creation, after setting all properties.
function popupmenu1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to popupmenu1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% --- Executes on selection change in popupmenu1.
function popupmenu1_Callback(hObject, eventdata, handles)
% hObject    handle to popupmenu1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = get(hObject,'String') returns popupmenu1 contents as cell array
%        contents{get(hObject,'Value')} returns selected item from popupmenu1
diagPlot(handles)

% ------------------------------------------------------------------------------

% --- Executes on button press in pushbutton3.
function pushbutton3_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
[stat resp] = controls_access;
if stat{1}~=1
  set(handles.text6,'String',['Error running access control program:\n',stat{2}]); set(handles.text6,'ForegroundColor','red');
  return
end % if error
if resp
  set(handles.text6,'String','Access Granted'); set(handles.text6,'ForegroundColor','black');
  set(hObject,'BackgroundColor','green')
else
  set(handles.text6,'String','Access Not Granted'); set(handles.text6,'ForegroundColor','red');
  set(hObject,'BackgroundColor','red')
end % if resp

function diagPlot(handles)
global BEAMLINE
[stat,data]=extDispersion_run('getdata');
if ((stat{1}==1)&&(~isstruct(data)||~isfield(data,'xbpms_all')))
  stat{1}=-1;
  stat{2}='No measured dispersion data';
end
if (stat{1}~=1)
  set(handles.text6,'String',{'Error getting data:',stat{2}},'ForegroundColor','red')
  return
end % if error
[stat,instdata]=FlTrackThru(1,length(BEAMLINE));
if (stat{1}~=1)
  errordlg(stat{2},'FlTrackThru error')
  return
end
[stat,pars]=extDispersion_run('getpars');
bpmList=get(handles.popupmenu1,'String');
ibpm=get(handles.popupmenu1,'Value');
bpm=findcells(BEAMLINE,'Name',bpmList{ibpm});
nbpm=find([instdata{1}.Index]==bpm);
if (pars.measMethod==1)
  xbpm=data.xbpms_all;
  xbpm_rms=data.xbpms_all_rms;
  ybpm=data.ybpms_all;
  ybpm_rms=data.ybpms_all_rms;
else
  xbpm=data.xbpm_e;
  xbpm_rms=data.xbpm_e_rms;
  ybpm=data.ybpm_e;
  ybpm_rms=data.ybpm_e_rms;
end
if (get(handles.radiobutton7,'Value')) % x
  axes(handles.axes1)
  plot_barsc(1e3*data.etaVec',1e6*xbpm(:,nbpm),1e6*xbpm_rms(:,nbpm),'b','o')
  px=linspace(min(data.etaVec),max(data.etaVec),100); % dP/P (1)
  py=polyval(flipud(data.xq(:,nbpm)),px);
  hold on,plot(1e3*px,1e6*py,'b--'),hold off
else % y
  axes(handles.axes1)
  plot_barsc(1e3*data.etaVec',1e6*ybpm(:,nbpm),1e6*ybpm_rms(:,nbpm),'b','o')
  px=linspace(min(data.etaVec),max(data.etaVec),100); % dP/P (1)
  py=polyval(flipud(data.yq(:,nbpm)),px);
  hold on,plot(1e3*px,1e6*py,'b--'),hold off
end

% display measured eta vector values
if (~isfield(data,'etaVector'))
  errordlg('No fitted dispersion values','diagPlot error')
  return
end
set(handles.text7,'String',...
  [sprintf('%4.3f',data.etaVector(1)*1e3),' +/- ',num2str(data.etaVector_err(1)*1e3,2)]);
set(handles.text8,'String',...
  [sprintf('%4.3f',data.etaVector(2)*1e3),' +/- ',num2str(data.etaVector_err(2)*1e3,2)]);
set(handles.text9,'String',...
  [sprintf('%4.3f',data.etaVector(3)*1e3),' +/- ',num2str(data.etaVector_err(3)*1e3,2)]);
set(handles.text10,'String',...
  [sprintf('%4.3f',data.etaVector(4)*1e3),' +/- ',num2str(data.etaVector_err(4)*1e3,2)]);

% ------------------------------------------------------------------------------

% --- Executes during object creation, after setting all properties.
function edit5_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit5 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function edit5_Callback(hObject, eventdata, handles)
% hObject    handle to edit5 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit5 as text
%        str2double(get(hObject,'String')) returns contents of edit5 as a double
[stat pars]=extDispersion_run('getpars');
origval=pars.nbpm_ave;
newval=str2double(get(hObject,'String'));
if isnan(newval) || newval<2
  errordlg('value must be integer >=2')
  set(hObject,'String',num2str(origval));
else
  pars.nbpm_ave=newval;
  extDispersion_run('setpars',pars);
end

% ------------------------------------------------------------------------------

% --- Exit Button
function pushbutton4_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
hassaved=get(handles.pushbutton6,'UserData');
if isempty(hassaved) || ~hassaved
  resp=questdlg('Save data to Model before quitting?','No Data saved');
  if strcmp(resp,'Yes')
    extDispersion_run('publish');
  elseif strcmp(resp,'Cancel')
    return
  end
end
guiCloseFn('extDispersion',handles);

% ------------------------------------------------------------------------------

% --- EXIT
function figure1_CloseRequestFcn(hObject, eventdata, handles)
% hObject    handle to figure1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: delete(hObject) closes the figure
hassaved=get(handles.pushbutton6,'UserData');
if isempty(hassaved) || ~hassaved
  if strcmp(questdlg('Save data before quitting?','No Data saved'),'Yes')
    extDispersion_run('publish');
  end
end
guiCloseFn('extDispersion',handles);

% ------------------------------------------------------------------------------

% --- BPM plots
function pushbutton5_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton5 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global BEAMLINE FL
% Get pars
[stat pars]=extDispersion_run('GetPars');
if stat{1}~=1
  errordlg(stat{2},'Error Getting Pars')
  return
end
% Get data
[stat data]=extDispersion_run('GetData');
if stat{1}~=1
  errordlg(stat{2},'Error Getting Data')
  return
end
% Get bpm inds
bpmind=[pars.bpmind];
bpmlist=cellfun(@(x) x.Name,BEAMLINE(bpmind),'UniformOutput',false)';
pind=find(ismember(data.bpminds,bpmind));
% Get list of BPMs to display
[id,ok]=listdlg( ...
  'Name','BPM Plots', ...
  'ListSize',[200,320], ...
  'ListString',bpmlist, ...
  'PromptString','Select one or more BPMs to display');
if ~ok,return,end % user pressed Cancel or closed selection window
% Plot raw data
x=1e3*data.etaVec; % dP/P (pm)
xf=linspace(min(data.etaVec),max(data.etaVec),100); % dP/P (1)
for m=1:length(id)
  n=id(m);
  figure(handles.plot1)
  y=1e6*data.xbpms_all(:,pind(n)); % dX (um)
  dy=1e6*data.xbpms_all_rms(:,pind(n)); % dXrms (um)
  plot_barsc(x,y,dy,'b','o')
  yf=polyval(flipud(data.xq(:,pind(n))),xf); % Xfit (m)
  hold on,plot(1e3*xf,1e6*yf,'b--'),hold off
  title(bpmlist{n})
  ylabel('X (um)')
  xlabel('dP/P (pm)')
  set(gcf,'Name','Horizontal BPM Response')
  figure(handles.plot2)
  y=1e6*data.ybpms_all(:,pind(n));
  dy=1e6*data.ybpms_all_rms(:,pind(n));
  plot_barsc(x,y,dy,'b','o')
  yf=polyval(flipud(data.yq(:,pind(n))),xf); % Yfit (m)
  hold on,plot(1e3*xf,1e6*yf,'b--'),hold off
  title(bpmlist{n})
  ylabel('Y (um)')
  xlabel('dP/P (pm)')
  set(gcf,'Name','Vertical BPM Response')
  if m<length(id)
    if strcmp(questdlg('Next BPM?','BPM Plots','Yes','Cancel','Yes'),'Cancel')
      break
    end
  end
end

% ------------------------------------------------------------------------------

% --- Executes during object creation, after setting all properties.
function edit6_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit6 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function edit6_Callback(hObject, eventdata, handles)
% hObject    handle to edit6 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit6 as text
%        str2double(get(hObject,'String')) returns contents of edit6 as a double

% ------------------------------------------------------------------------------

% --- save
function pushbutton6_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton6 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
%if ~strcmp(questdlg('Publish data to file?','Save Request'),'Yes')
%  return
%end
stat=extDispersion_run('publish');
if stat{1}~=1
  errordlg(stat{2},'Save Error')
end
set(handles.pushbutton6,'UserData',1);

% ------------------------------------------------------------------------------

% --- doqcut
function checkbox1_Callback(hObject, eventdata, handles)
% hObject    handle to checkbox1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of checkbox1
newpars.doqcut=get(hObject,'Value');
extDispersion_run('SetPars',newpars);

% ------------------------------------------------------------------------------

% --- Executes during object creation, after setting all properties.
function edit7_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit7 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function edit7_Callback(hObject, eventdata, handles)
% hObject    handle to edit7 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit7 as text
%        str2double(get(hObject,'String')) returns contents of edit7 as a double

% ------------------------------------------------------------------------------

% --- Executes on button press in checkbox2.
function checkbox2_Callback(hObject, eventdata, handles)
% hObject    handle to checkbox2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of checkbox2

% ------------------------------------------------------------------------------

% --- wirescanner measure?
function checkbox3_Callback(hObject, eventdata, handles)
% hObject    handle to checkbox3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of checkbox3
newpars.wiremeas=get(hObject,'Value');
extDispersion_run('SetPars',newpars);

% ------------------------------------------------------------------------------

% --- select bpms
function pushbutton7_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton7 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global BEAMLINE
extDispersion_bpmSel;
extDispersion_run('ReInit');
% reload list of BPMs for diagnostic plot
[stat,pars]=extDispersion_run('getpars');
set(handles.popupmenu1,'String', ...
  arrayfun(@(x) BEAMLINE{x}.Name,pars.bpmind,'UniformOutput',false))
set(handles.popupmenu1,'Value',find(pars.bpmind==pars.target.ind))
% refit measured dispersion
[stat,data]=extDispersion_run('GetData');
if isfield(data,'xbpms_all') && ~isempty(data.xbpms_all)
  extDispersion_run('ReMeasure'); % recalc using existing measurement data
end
% update measured dispersion vector
[stat,data]=extDispersion_run('getdata');
if ((stat{1}==1)&&(~isstruct(data)||~isfield(data,'etaVector')))
  return
end
set(handles.text7,'String',[sprintf('%4.3f',data.etaVector(1)*1e3),' +/- ', ...
  num2str(data.etaVector_err(1)*1e3,2)]);
set(handles.text8,'String',[sprintf('%4.3f',data.etaVector(2)*1e3),' +/- ', ...
  num2str(data.etaVector_err(2)*1e3,2)]);
set(handles.text9,'String',[sprintf('%4.3f',data.etaVector(3)*1e3),' +/- ', ...
  num2str(data.etaVector_err(3)*1e3,2)]);
set(handles.text10,'String',[sprintf('%4.3f',data.etaVector(4)*1e3),' +/- ', ...
  num2str(data.etaVector_err(4)*1e3,2)]);

% ------------------------------------------------------------------------------

% --- Executes during object creation, after setting all properties.
function edit8_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit8 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% -- qcut 
function edit8_Callback(hObject, eventdata, handles)
% hObject    handle to edit8 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit8 as text
%        str2double(get(hObject,'String')) returns contents of edit8 as a double
[stat,pars]=extDispersion_run('getpars');
origval=pars.qcut;
newval=str2double(get(hObject,'String'));
if isnan(newval) || newval<0 || newval>1
  errordlg('value must be between 0 and 1')
  set(hObject,'String',num2str(origval));
else
  pars.qcut=newval;
  extDispersion_run('setpars',pars);
end

% ------------------------------------------------------------------------------

% --- Executes on button press in pushbutton8.
function pushbutton8_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton8 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% ------------------------------------------------------------------------------

% --- Executes on button press in pushbutton9.
function pushbutton9_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton9 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% ------------------------------------------------------------------------------

% --- Reset
function pushbutton10_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton10 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
if ~strcmp(questdlg('Reset Correction Knobs?','extDispersion'),'Yes')
  return
end
extDispersion_run('reset');

% ------------------------------------------------------------------------------

% --- x correction set
function radiobutton1_Callback(hObject, eventdata, handles)
% hObject    handle to radiobutton1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of radiobutton1
[stat pars]=extDispersion_run('GetPars');
newpars.corset=pars.corset;
if get(hObject,'Value')
  newpars.corset(1)=1;
else
  newpars.corset(1)=0;
end
extDispersion_run('SetPars',newpars);

% ------------------------------------------------------------------------------

% --- x' correction set
function radiobutton5_Callback(hObject, eventdata, handles)
% hObject    handle to radiobutton5 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of radiobutton5
[stat pars]=extDispersion_run('GetPars');
newpars.corset=pars.corset;
if get(hObject,'Value')
  newpars.corset(2)=1;
else
  newpars.corset(2)=0;
end
extDispersion_run('SetPars',newpars);

% ------------------------------------------------------------------------------

% --- y correction set
function radiobutton2_Callback(hObject, eventdata, handles)
% hObject    handle to radiobutton2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of radiobutton2
[stat pars]=extDispersion_run('GetPars');
newpars.corset=pars.corset;
if get(hObject,'Value')
  newpars.corset(3)=1;
else
  newpars.corset(3)=0;
end
extDispersion_run('SetPars',newpars);

% ------------------------------------------------------------------------------

% --- y' correction set
function radiobutton6_Callback(hObject, eventdata, handles)
% hObject    handle to radiobutton6 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of radiobutton6
[stat pars]=extDispersion_run('GetPars');
newpars.corset=pars.corset;
if get(hObject,'Value')
  newpars.corset(4)=1;
else
  newpars.corset(4)=0;
end
extDispersion_run('SetPars',newpars);

% ------------------------------------------------------------------------------

% --- DR freq range select
function pushbutton11_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton11 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
extDispersion_freqSel;

% ------------------------------------------------------------------------------

% --- ICTDUMP select
function radiobutton3_Callback(hObject, eventdata, handles)
% hObject    handle to radiobutton3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of radiobutton3
if get(hObject,'Value')
  set(handles.radiobutton4,'Value',0)
  newpars.qmeas=0;
  extDispersion_run('SetPars',newpars);
end

% ------------------------------------------------------------------------------

% --- ICT1X select
function radiobutton4_Callback(hObject, eventdata, handles)
% hObject    handle to radiobutton4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of radiobutton4
if get(hObject,'Value')
  set(handles.radiobutton3,'Value',0)
  newpars.qmeas=1;
  extDispersion_run('SetPars',newpars);
end

% ------------------------------------------------------------------------------

% --- restore
function pushbutton12_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton12 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
if ~strcmp(questdlg('Restore data from file?','Restore Request'),'Yes')
  return
end
stat=extDispersion_run('restore');
if stat{1}~=1
  errordlg(stat{2},'Restore Error')
end
initPars(handles);

% ------------------------------------------------------------------------------

% --- Calibrate dispersion knobs
function pushbutton13_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton13 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global FL PS %#ok<NUSED>

% check really want to do this
if (strcmp(questdlg('Scan QF1X/QF6X to determine knob coefficients?','extDispersion'),'Yes'))
  stat=extDispersion_run('response');
end

% ------------------------------------------------------------------------------

% --- x plot select.
function radiobutton7_Callback(hObject, eventdata, handles)
% hObject    handle to radiobutton7 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of radiobutton7
if get(hObject,'Value')
  set(handles.radiobutton8,'Value',0)
end
diagPlot(handles);

% ------------------------------------------------------------------------------

% --- y plot select
function radiobutton8_Callback(hObject, eventdata, handles)
% hObject    handle to radiobutton8 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of radiobutton8
if get(hObject,'Value')
  set(handles.radiobutton7,'Value',0)
end
diagPlot(handles);

% ------------------------------------------------------------------------------

% --- Calculate & Plot
function pushbutton14_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton14 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global FL INSTR BEAMLINE

% Get region to plot, corresponding INSTR indicies, and S-values
% (NOTE: plot measured data at selected BPMs only)
rstr=get(handles.popupmenu3,'String');
rstr=rstr{get(handles.popupmenu3,'Value')};
bind=handles.Region.(rstr).ind;
bpmind=find(cellfun(@(x) x.Index>=bind(1) & x.Index<=bind(2) & strcmp(x.Class,'MONI'),INSTR));
bpmind=intersect(bpmind,FL.extDispersion.bpmSel.Inst);
wsind=findcells(BEAMLINE,'Name','MW*X');
pipind=findcells(BEAMLINE,'Name','MW1IP');
sind=cellfun(@(x) x.S,{BEAMLINE{bind(1):bind(2)}});
sind_instr=cellfun(@(x) BEAMLINE{x.Index}.S,{INSTR{bpmind}});
sind_ws=cellfun(@(x) x.S,{BEAMLINE{wsind}});
sind_pip=BEAMLINE{pipind}.S;
id=(bind(1):bind(2));

% Get data
[stat,data]=extDispersion_run('GetData');
if (stat{1}~=1)
  errordlg('Error getting dispersion data','extDispersion_run error')
  return
end
if (~isfield(data,'xq'))
  errordlg('No dispersion data - measure first','extDispersion_run error')
  return
end
xdisp=1e3*data.xq(2,bpmind);xdisprms=1e3*data.xdq(2,bpmind); % mm
ydisp=1e3*data.yq(2,bpmind);ydisprms=1e3*data.ydq(2,bpmind); % mm

% Get parameters
[stat,pars]=extDispersion_run('GetPars');
if (stat{1}~=1)
  errordlg('Error getting dispersion parameters','extDispersion_run error')
  return
end

% Determine which BPMs were used in the fit and which were not
idt=pars.bpmind;
for n=1:length(idt)
  idt(n)=find(cellfun(@(x) x.Index==idt(n),INSTR)); % INSTR indices for fit BPMs
end
idfit=[];
for n=1:length(idt)
  m=find(bpmind==idt(n));
  if (~isempty(m))
    idfit=[idfit,m]; % bpmind indices for fit BPMs
  end
end
idnofit=setdiff(1:length(bpmind),idfit); % bpmind indices for nofit BPMs

% Get propagated dispersion values
dispall=get(handles.pushbutton1,'UserData');
if (~isempty(dispall))
  xdispfit=dispall{2}(1,id);
  ydispfit=dispall{2}(3,id);
else
  helpdlg('Propagating (?)','extDispersion: "Calculate & Plot"')
  [stat,Df,dDf]=dispfit(1:length(BEAMLINE),data);
  if (stat{1}~=1)
    errordlg('Error propagating dispersion function','dispfit error')
    return
  end
end

% Plot dispersion data
figure(handles.plot1)
if (~isempty(idnofit))
  plot_barsc(sind_instr(idnofit)',xdisp(idnofit)',xdisprms(idnofit)','k','o')
  hold on
end
if (~isempty(idfit))
  plot_barsc(sind_instr(idfit)',xdisp(idfit)',xdisprms(idfit)','r','o')
  hold on
end
set(gca,'XLim',FL.SimModel.Twiss.S(bind))
plot(sind,1e3*xdispfit,'r-')
plot(FL.SimModel.Twiss.S(id),1e3*FL.SimModel.Twiss.etax(id),'b-')
grid
if (isfield(data,'xwdisp')&&length(data.xwdisp)>1)
  if (length(data.xwdisp_rms)==1)
    data.xwdisp_rms=zeros(size(data.xwdisp));
  end
  if (length(data.xpipdisp_rms)==1)
    data.xpipdisp_rms=zeros(size(data.xpipdisp));
  end
  if (sind_ws(1)>=FL.SimModel.Twiss.S(bind(1))&& ...
      sind_ws(end)<=FL.SimModel.Twiss.S(bind(2)))
    plot_barsc(sind_ws',data.xwdisp',data.xwdisp_rms','k','o')
  end
  if (sind_pip(1)>=FL.SimModel.Twiss.S(bind(1))&& ...
      sind_pip(end)<=FL.SimModel.Twiss.S(bind(2)))
    plot_barsc(sind_pip',data.xpipdisp',data.xpipdisp_rms','k','o')
  end
end
hold off
xlabel('S / m')
ylabel('Dx / mm')
if (isfield(pars,'doYLim')&&pars.doYLim(1))
  set(gca,'YLim',pars.YLim(1:2))
end
if (strcmp(rstr,'IP'))
  ver_line(BEAMLINE{findcells(BEAMLINE,'Name','IP')}.S,'g-')
else
  [h0,h1]=plot_magnets_Lucretia(BEAMLINE(id),1,1,8);
end
set(gcf,'Name','Horizontal Dispersion')

figure(handles.plot2)
if (~isempty(idnofit))
  plot_barsc(sind_instr(idnofit)',ydisp(idnofit)',ydisprms(idnofit)','k','o')
  hold on
end
if (~isempty(idfit))
  plot_barsc(sind_instr(idfit)',ydisp(idfit)',ydisprms(idfit)','r','o')
  hold on
end
set(gca,'XLim',FL.SimModel.Twiss.S(bind))
plot(sind,1e3*ydispfit,'r-')
plot(FL.SimModel.Twiss.S(id),1e3*FL.SimModel.Twiss.etay(id),'b-')
grid
if (isfield(data,'ywdisp')&&length(data.ywdisp)>1)
  if (length(data.ywdisp_rms)==1)
    data.ywdisp_rms=zeros(size(data.ywdisp));
  end
  if (length(data.ypipdisp_rms)==1)
    data.ypipdisp_rms=zeros(size(data.ypipdisp));
  end
  if (sind_ws(1)>=FL.SimModel.Twiss.S(bind(1))&& ...
      sind_ws(end)<=FL.SimModel.Twiss.S(bind(2)))
    plot_barsc(sind_ws',data.ywdisp',data.ywdisp_rms','k','o')
  end
  if (sind_pip(1)>=FL.SimModel.Twiss.S(bind(1))&& ...
      sind_pip(end)<=FL.SimModel.Twiss.S(bind(2)))
    plot_barsc(sind_pip',data.ypipdisp',data.ypipdisp_rms','k','o')
  end
end
hold off
xlabel('S / m')
ylabel('Dy / mm')
if (isfield(pars,'doYLim')&&pars.doYLim(2))
  set(gca,'YLim',pars.YLim(3:4))
end
if (strcmp(rstr,'IP'))
  ver_line(BEAMLINE{findcells(BEAMLINE,'Name','IP')}.S,'g-')
else
  [h0,h1]=plot_magnets_Lucretia(BEAMLINE(id),1,1,8);
end
set(gcf,'Name','Vertical Dispersion')

% ------------------------------------------------------------------------------

% --- Executes during object creation, after setting all properties.
function popupmenu3_CreateFcn(hObject, eventdata, handles)
% hObject    handle to popupmenu3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% --- Executes on selection change in popupmenu3.
function popupmenu3_Callback(hObject, eventdata, handles)
% hObject    handle to popupmenu3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = get(hObject,'String') returns popupmenu3 contents as cell array
%        contents{get(hObject,'Value')} returns selected item from popupmenu3

% ------------------------------------------------------------------------------

% --- Get fitted dispersion at given location
function pushbutton15_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton15 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global BEAMLINE INSTR

name=get(handles.edit9,'String');
bind=findcells(BEAMLINE,'Name',name);
if isempty(bind)
  errordlg('Unknown BEAMLINE element name', ...
    'Get Propagated Dispersion Values')
  return
end
dispdata=get(handles.pushbutton1,'UserData');
if isempty(dispdata) || size(dispdata,2)~=3
  errordlg('No dispersion data - measure first', ...
    'Get Propagated Dispersion Values')
  return
end
% userdata: {bind,Df,dDf}
dispind=find((dispdata{1}(1):dispdata{1}(2))==bind(1),1);
if isempty(dispind)
  errordlg('What the F***?', ...
    'Get Propagated Dispersion Values')
  return
end
s=sprintf(['Fitted dispersion values for BEAMLINE element %d (%s):\n', ...
  '  eta x  = %.3g +- %.3g mm\n', ...
  '  eta x'' = %.3g +- %.3g mrad\n', ...
  '  eta y  = %.3g +- %.3g mm\n',...
  '  eta y'' = %.3g +- %.3g mrad'], ...
  bind(1),get(handles.edit9,'String'), ...
  1e3*dispdata{2}(1,dispind),1e3*dispdata{3}(1,dispind), ...
  1e3*dispdata{2}(2,dispind),1e3*dispdata{3}(2,dispind), ...
  1e3*dispdata{2}(3,dispind),1e3*dispdata{3}(3,dispind), ...
  1e3*dispdata{2}(4,dispind),1e3*dispdata{3}(4,dispind));
set(handles.text6,'String',s)
set(handles.text6,'ForegroundColor','black')

% ------------------------------------------------------------------------------

% --- Executes during object creation, after setting all properties.
function edit9_CreateFcn(hObject, eventdata, handles) %#ok<*INUSD>
% hObject    handle to edit9 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function edit9_Callback(hObject, eventdata, handles)
% hObject    handle to edit9 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit9 as text
%        str2double(get(hObject,'String')) returns contents of edit9 as a double

% ------------------------------------------------------------------------------

% --- Get element name from BeamlineViewer
function pushbutton16_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton16 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global BEAMLINE FL

if isfield(FL.Gui,'BeamlineViewer') && ishandle(FL.Gui.BeamlineViewer.figure1)
  bl=get(FL.Gui.BeamlineViewer.listbox1,'String');
  wbl=get(FL.Gui.BeamlineViewer.listbox1,'Value');
  ibl=str2double(regexp(bl{wbl(1)},'^\d+','match'));
  set(handles.edit9,'String',BEAMLINE{ibl}.Name)
else
  FL.Gui.BeamlineViewer=BeamlineViewer;
  msg={'Select an element from the BEAMLINE list,',...
       'then press "Get Name From BeamlineViewer" again'};
  helpdlg(msg,'extDispersion')
end

% ------------------------------------------------------------------------------

% --- use model
function radiobutton9_Callback(hObject, eventdata, handles)
% hObject    handle to radiobutton9 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of radiobutton9
if get(hObject,'Value')
  set(handles.radiobutton10,'Value',0)
  newpars.docalcknob=false;
  extDispersion_run('SetPars',newpars);
end

% ------------------------------------------------------------------------------

% --- Use calculated
function radiobutton10_Callback(hObject, eventdata, handles) %#ok<*INUSL,*DEFNU>
% hObject    handle to radiobutton10 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of radiobutton10
if get(hObject,'Value')
  set(handles.radiobutton9,'Value',0)
  newpars.docalcknob=true;
  extDispersion_run('SetPars',newpars);
end

% ------------------------------------------------------------------------------

% --- Executes during object creation, after setting all properties.
function edit10_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit10 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function edit10_Callback(hObject, eventdata, handles)
% hObject    handle to edit10 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit10 as text
%        str2double(get(hObject,'String')) returns contents of edit10 as a double
newpars.nstep=str2double(get(hObject,'String'));
extDispersion_run('SetPars',newpars);

% ------------------------------------------------------------------------------

% --- Scan knobs
function pushbutton17_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton17 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% check really want to do this
global FL INSTR BEAMLINE

% Select a knob to scan
knames={'EtaX','EtaPX','EtaY','EtaPY','Sum'};
kunits={'mm','mrad','mm','mrad','amp'};  % for plots
listString={'EtaX','EtaPX','Sum'}; % should = knames ... if we had the knobs!
[iknob,ok]=listdlg( ...
  'Name','Knob Scan', ...
  'ListSize',[150,75], ...
  'SelectionMode','single', ...
  'ListString',listString, ...
  'PromptString','Select a knob to scan');
if (~ok),return,end
scanSum=strcmp(listString{iknob},'Sum');

% Make sure no wirescans
set(handles.checkbox3,'Value',0)
extDispersion('checkbox3_Callback',handles.checkbox3,[],handles);

% Set scan parameters
[stat,knobs]=extDispersion_run('GetKnobs');
if (stat{1}~=1),KnobScanEvent(-1,stat),return,end
[stat,pars]=extDispersion_run('GetPars');
if (stat{1}~=1),KnobScanEvent(-1,stat),return,end
if (scanSum)
  iknob=5;
  sfac=1;
  maxknob=2; % amp
  msg=sprintf('NOTE: +-%s amp limits used for Sum knob scan',num2str(maxknob));
  helpdlg(msg,'Knob Scan')
  fudg=1; % no fudge
  id1=3; % EtaY
  id2=4; % EtaPY
  ford=1; % linear
else
  sfac=1e3;
  maxknob=str2double(get(handles.edit11,'String'))/sfac; % m,rad
  fudg=pars.fudge(iknob);
  id1=iknob;
  id2=3-id1; % EtaX->EtaPX, EtaPX->EtaX
  ford=2; % quadratic
end
nscan=round(str2double(get(handles.edit10,'String')));
if (isnan(nscan))
  stat{1}=-1;
  stat{2}='Invalid # Cal/Scan steps','Knob Scan';
  KnobScanEvent(-1,stat)
  return
end
kscan=linspace(-maxknob,maxknob,nscan);

% Localize the scan knob
kname=knames{iknob};
kunit=kunits{iknob};
%eval(['knob=',['knobs.',knames{iknob}],';'])
knob=knobs.(kname);

% Do the scan
h=handles.text6;
kval0=knob.Value;
KnobScanEvent(0,h,kname) % scan start
for iscan=1:nscan
  kval=fudg*kscan(iscan);
  msg=sprintf('Set %s = %s %s (%d of %d)',kname, ...
    num2str(sfac*kval),kunit,iscan,nscan);
  KnobScanEvent(1,h,msg) % step info
  stat=SetMultiKnob('knob',kval,1); % set
  pause(FL.trimPause)
  FlHwUpdate;
  if (stat{1}~=1)
    KnobScanEvent(2,h,kname,stat) % set error
    return
  end
  FL.Gui.extDispersion.quietMeas=true;
  stat=extDispersion_run('Measure');
  FL.Gui.extDispersion=rmfield(FL.Gui.extDispersion,'quietMeas');
  if (stat{1}==0)
    KnobScanEvent(3,h,kname) % abort request
  elseif (stat{1}~=1)
    KnobScanEvent(4,h,stat) % measure error
  end
  if (stat{1}~=1)
    stat=SetMultiKnob('knob',kval0,1); % reset
    pause(FL.trimPause)
    FlHwUpdate;
    if (stat{1}~=1)
      KnobScanEvent(2,h,kname,stat) % set error
    end
    return
  end
  [stat,data]=extDispersion_run('GetData');
  if (stat{1}~=1)
    KnobScanEvent(-1,stat)
    return
  end
  etavec{iscan}=data.etaVector;
  etavec_err{iscan}=data.etaVector_err;
  % Get dispersion values for all bpms for rms scan
  if (pars.measMethod==1)
    [stat,instdata]=FlTrackThru(1,pars.bpmind(end)+1);
    bpms=ismember([instdata{1}.Index],pars.bpmind);
  else
    bpms=cellfun(@(x) ismember(x.Index,pars.bpmind),INSTR);
  end
  xraw=data.xq(2,bpms);dispx(iscan)=std(xraw(~isnan(xraw)));
  yraw=data.yq(2,bpms);dispy(iscan)=std(yraw(~isnan(yraw)));
end
data.dispx_scan=dispx;
data.dispy_scan=dispy;
stat=SetMultiKnob('knob',kval0,1);
pause(FL.trimPause)
FlHwUpdate;
if (stat{1}~=1)
  KnobScanEvent(2,handles.text6,knames{iknob},stat) % set error
  return
end
KnobScanEvent(5,h,kname) % scan end

% Response plot
figure(handles.plot1)
x=kscan';
xi=linspace(min(kscan),max(kscan)); % for plot
y1=arrayfun(@(x) etavec{x}(id1),1:length(kscan))';
dy1=arrayfun(@(x) etavec_err{x}(id1),1:length(kscan))';
q1=noplot_polyfit(x,y1,dy1,ford);
y2=arrayfun(@(x) etavec{x}(id2),1:length(kscan))';
dy2=arrayfun(@(x) etavec_err{x}(id2),1:length(kscan))';
q2=noplot_polyfit(x,y2,dy2,ford);
plot_barsc(sfac*x,1e3*y1,1e3*dy1,'b','o')
hold on
plot_barsc(sfac*x,1e3*y2,1e3*dy2,'r','o')
plot(sfac*xi,1e3*polyval(fliplr(q1),xi),'b--', ...
     sfac*xi,1e3*polyval(fliplr(q2),xi),'r--')
hold off
xtxt=[kname,' Knob (',kunit,')'];
if (scanSum)
  ytxt=['Measured Dispersion [Blue=EtaY(mm),Red=EtaPY(mrad)]'];
else
  ytxt=['Measured Dispersion [Blue=',knames{id1},'(',kunits{id1}, ...
    '),Red=',knames{id2},'(',kunits{id2},')]'];
  title(sprintf('Slope at %s=0: %s',kname,num2str(q1(2))))
end
xlabel(xtxt)
ylabel(ytxt)
set(gcf,'Name',['Fitted Dispersion at ',pars.target.name])
if (~scanSum)
  % Compute correction value
  r2=roots(fliplr(q1));
  r1=roots(fliplr(q1(1:2)));
  [~,id]=min(abs(r2-r1)); % quadratic root that's closest to linear root
  kvalc=r2(id); % m,rad
  ver_line(sfac*kvalc,'g')
  % Compute new fudge factor
  newfudge=pars.fudge;
  newfudge(iknob)=1/(fudg*q1(2));
end

% RMS response plot
figure(handles.plot2)
x=kscan';
y1=(dispx.^2)';
q1=noplot_polyfit(x,y1,1,2);
y2=(dispy.^2)';
q2=noplot_polyfit(x,y2,1,2);
plot(sfac*x,1e6*y1,'bo',sfac*xi,1e6*polyval(fliplr(q1),xi),'b--', ...
     sfac*x,1e6*y2,'ro',sfac*xi,1e6*polyval(fliplr(q2),xi),'r--')
xlabel(xtxt)
ylabel('RMS^2 Fit Region Dispersion (mm^2) [Blue=EtaX,Red=EtaY]')
set(gcf,'Name','RMS Fit Region Dispersion')
if (scanSum)
  % Compute correction value
  q2=noplot_parab(x,y2,1);
  kvalc=q2(2); % amp
  ver_line(kvalc,'g')
end

% Save new fudge factor?
if (~scanSum)
  msg=['New ',kname,' fudge factor: ',num2str(newfudge(iknob))];
  if (strcmp(questdlg([msg,' Apply?'],'Knob Scan'),'Yes'))
    newpars.fudge=newfudge;
    extDispersion_run('SetPars',newpars);
    set(handles.edit12,'String',num2str(newfudge(1),3))
    set(handles.edit13,'String',num2str(newfudge(2),3))
    set(handles.edit14,'String',num2str(newfudge(3),3))
    set(handles.edit15,'String',num2str(newfudge(4),3))
  end
end

% Correct?
msg=['Measured ',kname,' correction value: ',num2str(sfac*kvalc),' (',kunit,')'];
if (~strcmp(questdlg([msg,' Perform correction?'],'Knob Scan'),'Yes'))
  return
end
set(h,'String',msg)
stat=SetMultiKnob('knob',kvalc,1);
pause(FL.trimPause)
FlHwUpdate;
if (stat{1}~=1)
  KnobScanEvent(2,h,kname,stat) % set error
  return
end
if (~scanSum)
  set(handles.(['text',num2str(16+iknob)]),'String',num2str(kvalc))
end

function KnobScanEvent(opcode,varargin)
switch opcode
  case -1 % setup error
    stat=varargin{1};
    errordlg(stat{2},'Knob Scan')
  case  0 % scan begin
    h=varargin{1};
    kname=varargin{2};
    msg={[kname,' scan started']};
    set(h,'String',msg) % start new message buffer
  case  1 % scan step info
    h=varargin{1};
    msg=varargin{2};
    s=get(h,'String');
    s=[s;msg];
    set(h,'String',s)
  case  3 % scan aborted by user
    h=varargin{1};
    kname=varargin{2};
    msg=[varargin{2},' scan aborted'];
    s=get(h,'String');
    s=[s;msg];
    set(h,'String',s)
  case  4 % measurement error
    h=varargin{1};
    stat=varargin{2};
    msg='Dispersion measurement error!';
    s=get(h,'String');
    s=[s;msg];
    set(h,'String',s)
    errordlg(stat{2},'Knob Scan')
  case  5 % scan end
    h=varargin{1};
    kname=varargin{2};
    msg=[kname,' scan completed'];
    s=get(h,'String');
    s=[s;msg];
    set(h,'String',s)
end
if (opcode>=0),drawnow('expose'),end

% ------------------------------------------------------------------------------

% --- Executes during object creation, after setting all properties.
function edit11_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit11 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function edit11_Callback(hObject, eventdata, handles)
% hObject    handle to edit11 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit11 as text
%        str2double(get(hObject,'String')) returns contents of edit11 as a double

newpars.maxknob=str2double(get(hObject,'String')); % mm/mrad
extDispersion_run('SetPars',newpars);

% ------------------------------------------------------------------------------

% --- Executes during object creation, after setting all properties.
function edit12_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit12 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function edit12_Callback(hObject, eventdata, handles)
% hObject    handle to edit12 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit12 as text
%        str2double(get(hObject,'String')) returns contents of edit12 as a double
[stat pars]=extDispersion_run('GetPars');
newpars.fudge=pars.fudge;
newpars.fudge(1)=str2double(get(hObject,'String'));
extDispersion_run('SetPars',newpars);

% ------------------------------------------------------------------------------

% --- Executes during object creation, after setting all properties.
function edit13_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit13 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function edit13_Callback(hObject, eventdata, handles)
% hObject    handle to edit13 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit13 as text
%        str2double(get(hObject,'String')) returns contents of edit13 as a double
[stat pars]=extDispersion_run('GetPars');
newpars.fudge=pars.fudge;
newpars.fudge(2)=str2double(get(hObject,'String'));
extDispersion_run('SetPars',newpars);

% ------------------------------------------------------------------------------

% --- Executes during object creation, after setting all properties.
function edit14_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit14 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function edit14_Callback(hObject, eventdata, handles)
% hObject    handle to edit14 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit14 as text
%        str2double(get(hObject,'String')) returns contents of edit14 as a double
[stat pars]=extDispersion_run('GetPars');
newpars.fudge=pars.fudge;
newpars.fudge(3)=str2double(get(hObject,'String'));
extDispersion_run('SetPars',newpars);

% ------------------------------------------------------------------------------

% --- Executes during object creation, after setting all properties.
function edit15_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit15 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function edit15_Callback(hObject, eventdata, handles)
% hObject    handle to edit15 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit15 as text
%        str2double(get(hObject,'String')) returns contents of edit15 as a double
[stat pars]=extDispersion_run('GetPars');
newpars.fudge=pars.fudge;
newpars.fudge(4)=str2double(get(hObject,'String'));
extDispersion_run('SetPars',newpars);

% ------------------------------------------------------------------------------

% --- Abort
function pushbutton18_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton18 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global FL
FL.abort_extDispersion=true;

% ------------------------------------------------------------------------------

% --- Use normal measurement method
function radiobutton11_Callback(hObject, eventdata, handles)
% hObject    handle to radiobutton11 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of radiobutton11
if get(hObject,'Value')
  newpars.measMethod=1;
  extDispersion_run('SetPars',newpars);
  set(handles.radiobutton12,'Value',0);
end

% ------------------------------------------------------------------------------

% --- Use svd measurement method
function radiobutton12_Callback(hObject, eventdata, handles)
% hObject    handle to radiobutton12 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of radiobutton12
if get(hObject,'Value')
  newpars.measMethod=2;
  extDispersion_run('SetPars',newpars);
  set(handles.radiobutton11,'Value',0);
end

% ------------------------------------------------------------------------------

% --- Re-calc Knobs
function pushbutton19_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton19 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
stat=extDispersion_run('ReCalcKnobs');
if stat{1}~=1
  errordlg(stat{2},'ReCalc Knob Error')
end

% ------------------------------------------------------------------------------

% --- Autoscale Y-axis of Figure 1
function radiobutton13_Callback(hObject, eventdata, handles)
% hObject    handle to radiobutton13 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of radiobutton13
if get(hObject,'Value')
  set(handles.radiobutton14,'Value',0)
  set(handles.edit16,'Enable','off')
  set(handles.edit17,'Enable','off')
  [stat newpars]=extDispersion_run('GetPars');
  newpars.doYLim(1)=false;
  extDispersion_run('SetPars',newpars);
end

% --- Manual scale Y-axis of Figure 1
function radiobutton14_Callback(hObject, eventdata, handles)
% hObject    handle to radiobutton14 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of radiobutton14
if get(hObject,'Value')
  set(handles.radiobutton13,'Value',0)
  set(handles.edit16,'Enable','on')
  set(handles.edit17,'Enable','on')
  [stat newpars]=extDispersion_run('GetPars');
  newpars.doYLim(1)=true;
  extDispersion_run('SetPars',newpars);
end

% ------------------------------------------------------------------------------

% --- Autoscale Y-axis of Figure 2
function radiobutton15_Callback(hObject, eventdata, handles)
% hObject    handle to radiobutton15 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of radiobutton15
if get(hObject,'Value')
  set(handles.radiobutton16,'Value',0)
  set(handles.edit18,'Enable','off')
  set(handles.edit19,'Enable','off')
  [stat newpars]=extDispersion_run('GetPars');
  newpars.doYLim(2)=false;
  extDispersion_run('SetPars',newpars);
end

% --- Manual scale Y-axis of Figure 2
function radiobutton16_Callback(hObject, eventdata, handles)
% hObject    handle to radiobutton16 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of radiobutton16
if get(hObject,'Value')
  set(handles.radiobutton15,'Value',0)
  set(handles.edit18,'Enable','on')
  set(handles.edit19,'Enable','on')
  [stat newpars]=extDispersion_run('GetPars');
  newpars.doYLim(2)=true;
  extDispersion_run('SetPars',newpars);
end

% ------------------------------------------------------------------------------

% --- Executes during object creation, after setting all properties.
function edit16_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit16 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function edit16_Callback(hObject, eventdata, handles)
% hObject    handle to edit16 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit16 as text
%        str2double(get(hObject,'String')) returns contents of edit16 as a double
[stat newpars]=extDispersion_run('GetPars');
newpars.YLim(1)=str2double(get(hObject,'String'));
extDispersion_run('SetPars',newpars);

% ------------------------------------------------------------------------------

% --- Executes during object creation, after setting all properties.
function edit17_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit17 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function edit17_Callback(hObject, eventdata, handles)
% hObject    handle to edit17 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit17 as text
%        str2double(get(hObject,'String')) returns contents of edit17 as a double
[stat newpars]=extDispersion_run('GetPars');
newpars.YLim(2)=str2double(get(hObject,'String'));
extDispersion_run('SetPars',newpars);

% ------------------------------------------------------------------------------

% --- Executes during object creation, after setting all properties.
function edit18_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit18 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function edit18_Callback(hObject, eventdata, handles)
% hObject    handle to edit18 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit18 as text
%        str2double(get(hObject,'String')) returns contents of edit18 as a double
[stat newpars]=extDispersion_run('GetPars');
newpars.YLim(3)=str2double(get(hObject,'String'));
extDispersion_run('SetPars',newpars);

% ------------------------------------------------------------------------------

% --- Executes during object creation, after setting all properties.
function edit19_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit19 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function edit19_Callback(hObject, eventdata, handles)
% hObject    handle to edit19 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit19 as text
%        str2double(get(hObject,'String')) returns contents of edit19 as a double
[stat newpars]=extDispersion_run('GetPars');
newpars.YLim(4)=str2double(get(hObject,'String'));
extDispersion_run('SetPars',newpars);

% ------------------------------------------------------------------------------

function pushbutton20_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton20 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global BEAMLINE INSTR

dispdata=get(handles.pushbutton1,'UserData');
if isempty(dispdata) || size(dispdata,2)~=3
  errordlg('No dispersion data - measure first', ...
    'Get Propagated Dispersion Values')
  return
end

% find OTRs, wirescanners, and laserwires
id=[ ...
  findcells(BEAMLINE,'Name','OTR*'), ...
  findcells(BEAMLINE,'Name','MW*'), ...
  intersect(findcells(BEAMLINE,'Name','MS*'),findcells(BEAMLINE,'Class','PROF')), ...
  findcells(BEAMLINE,'Name','LW1FF'), ...
  ];
fmt='%-6s %9.3f %9.3f %9.3f %9.3f |';
s=cell(0);
s{    1}='Name       DX mm  DXerr mm    DPX mr DPXerr mr |';
s{end+1}='------ --------- --------- --------- --------- |';
for n=1:length(id)
  dispind=find((dispdata{1}(1):dispdata{1}(2))==id(n),1);
  if (isempty(dispind)),continue,end
  DX=dispdata{2}(1,dispind);dDX=dispdata{3}(1,dispind);
  DPX=dispdata{2}(2,dispind);dDPX=dispdata{3}(2,dispind);
  nc=min([length(BEAMLINE{id(n)}.Name),6]);
  s{end+1}=sprintf(fmt,BEAMLINE{id(n)}.Name(1:nc),1e3*[DX,dDX,DPX,dDPX]);
end
s{end+1}=' ';
s{end+1}='Name       DY mm  DYerr mm    DPY mr DPYerr mr |';
s{end+1}='------ --------- --------- --------- --------- |';
for n=1:length(id)
  dispind=find((dispdata{1}(1):dispdata{1}(2))==id(n),1);
  if (isempty(dispind)),continue,end
  DY=dispdata{2}(3,dispind);dDY=dispdata{3}(3,dispind);
  DPY=dispdata{2}(4,dispind);dDPY=dispdata{3}(4,dispind);
  nc=min([length(BEAMLINE{id(n)}.Name),6]);
  s{end+1}=sprintf(fmt,BEAMLINE{id(n)}.Name(1:nc),1e3*[DY,dDY,DPY,dDPY]);
end
h=msgbox(s,'Propagated Dispersion Values');
hc=get(h,'Children');
hgc=get(hc(1),'Children');
set(hgc,'FontName','system')


% --- Executes on button press in radiobutton17.
function radiobutton17_Callback(hObject, eventdata, handles)
% hObject    handle to radiobutton17 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

if get(hObject,'Value')
  set(handles.radiobutton18,'Value',0);
  newpars.dononlincor=true;
  extDispersion_run('SetPars',newpars);
else
  set(handles.radiobutton18,'Value',1);
  newpars.dononlincor=false;
  extDispersion_run('SetPars',newpars);
  radiobutton18_Callback(handles.radiobutton18,[],handles);
end


% --- Executes on button press in radiobutton18.
function radiobutton18_Callback(hObject, eventdata, handles)
% hObject    handle to radiobutton18 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

if get(hObject,'Value')
  set(handles.radiobutton17,'Value',0);
  newpars.dononlincor=false;
  extDispersion_run('SetPars',newpars);
else
  set(handles.radiobutton17,'Value',1);
  radiobutton17_Callback(handles.radiobutton17,[],handles);
end

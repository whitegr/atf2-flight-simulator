function stat=extDispersion_verify(corset,fudge,etaVector,EtaX,EtaPX,EtaY,EtaPY)
global BEAMLINE PS FL

% get list of dispersion correction power supplies
ip=[[EtaX.Channel.Unit],[EtaY.Channel.Unit]];
Np=length(ip);

% compute extant power supply currents
pname=cell(1,Np);
SetPt0=zeros(1,Np);
I0=zeros(1,Np);
for n=1:Np
  id=PS(ip(n)).Element(1);
  pname{n}=BEAMLINE{id}.Name;
  SetPt0(n)=PS(ip(n)).SetPt;
  conv=FL.HwInfo.PS(ip(n)).conv;
  if (length(conv)>1)
   %if (~FL.HwInfo.PS(ip(n)).unipolar)
   %  conv=[-fliplr(conv),conv(:,2:end)];
   %end
    B=BEAMLINE{id}.B*SetPt0(n);
    I0(n)=interp1(conv(2,:),conv(1,:),B);
  else
    I0(n)=SetPt0(n)/conv; % XCOR or YCOR
  end
end

% use knob coefficients and measured dispersion values to compute SetPt
% increments
dSetPt=zeros(4,Np);
if (corset(1))
  for n=1:length(EtaX.Channel)
    id=find(ip==EtaX.Channel(n).Unit);
    dSetPt(1,id)=dSetPt(1,id)-fudge(1)*etaVector(1)*EtaX.Channel(n).Coefficient;
  end
end
if (corset(2))
  for n=1:length(EtaPX.Channel)
    id=find(ip==EtaPX.Channel(n).Unit);
    dSetPt(2,id)=dSetPt(2,id)-fudge(2)*etaVector(2)*EtaPX.Channel(n).Coefficient;
  end
end
if (corset(3))
  for n=1:length(EtaY.Channel)
    id=find(ip==EtaY.Channel(n).Unit);
    dSetPt(3,id)=dSetPt(3,id)-fudge(3)*etaVector(3)*EtaY.Channel(n).Coefficient;
  end
end
if (corset(4))
  for n=1:length(EtaPY.Channel)
    id=find(ip==EtaPY.Channel(n).Unit);
    dSetPt(4,id)=dSetPt(4,id)-fudge(4)*etaVector(4)*EtaPY.Channel(n).Coefficient;
  end
end

% compute new power supply currents
SetPt=zeros(1,Np);
I=zeros(1,Np);
for n=1:Np
  id=PS(ip(n)).Element(1);
  SetPt(n)=SetPt0(n)+sum(dSetPt(:,n));
  conv=FL.HwInfo.PS(ip(n)).conv;
  if (length(conv)>1)
   %if (~FL.HwInfo.PS(ip(n)).unipolar)
   %  conv=[-fliplr(conv),conv(:,2:end)];
   %end
    B=BEAMLINE{id}.B*SetPt(n);
    I(n)=interp1(conv(2,:),conv(1,:),B);
  else
    I(n)=SetPt(n)/conv;
  end
end

% present proposed power supply changes to user and provide abort opportunity
q=cell(Np+4,1);
q{1}='name       I(now)    I(new)      dI';
q{2}='--------  --------  --------  --------';
for n=1:Np
  q{n+2}=sprintf('%-8s  %8.3f  %8.3f  %8.3f',pname{n},I0(n),I(n),I(n)-I0(n));
end
q{end-1}=' ';
q{end}='Continue?';
s=questdlg(q,'extDispersion','No');

% set return status
if (strcmp(s,'Yes'))
  stat{1}=1;
else
  stat{1}=-1;stat{2}='Aborted by user';
end

return
end
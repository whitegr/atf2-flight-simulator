function [stat,allData]=extDispersion_responseMatrixFull(Model,pars)

global BEAMLINE INSTR PS FL

debug=false;

allData=[];

% this code only runs on the server
if (~strcmp(FL.mode,'trusted'))
  stat{1}=-1;
  stat{2}='responseMatrixFull: test mode not allowed';
  return
end
% HW Update rate must be nonzero
if (get(FL.Gui.main.popupmenu1,'Value')==1)
  stat{1}=-1;
  stat{2}='responseMatrixFull: nonzero HW Update rate required';
  return
end

% initialize
nstep=pars.nstep;
nave=pars.nbpm_ave;
df=Model.RampVec; % kHz
dp=Model.DeltaVec; % fraction
nfreq=length(df);
dSetPt=0.02; % +-dSetPt around present SetPt
twait=5; % magnet settle time
maxiter=100; % maximum BPM read attempts
name={'QF1X','QF6X'};
nmag=length(name);
% BPM pointers
iex=findcells(BEAMLINE,'Name','IEX');
idmi=findcells(INSTR,'Class','MONI');
idm=cellfun(@(x) x.Index,INSTR(idmi));
id=find(idm>iex);
idm=idm(id); % pointers into BEAMLINE
idmi=idmi(id); % pointers into INSTR
nbpm=length(idm);
idx=3*(idmi-1)+2;idy=idx+1;idt=idy+1; % pointers into FlHwUpdate output
% ICT pointers
ict=findcells(BEAMLINE,'Name','ICT*'); % pointers into BEAMLINE
nict=length(ict);
icti=zeros(1,nict);
for n=1:nict
  icti(n)=findcells(INSTR,'Index',ict(n)); % pointer into INSTR
end
idi=3*(icti-1)+4; % pointers into FlHwUpdate output

% allData.misc energy,nmag,nstep,nfreq,nave,nbpm,nict,df,dp
% allData.mag  [nmag] name,iel,ips,B0,SetPt0,Iadc0,Iset0,Idac0,SetPt
% allData.id   [nmag*nstep*nfreq*nave,4] mag#,step#,freq#,read#
% allData.Iadc [nmag*nstep*nfreq*nave]
% allData.Iset [nmag*nstep*nfreq*nave]
% allData.Idac [nmag*nstep*nfreq*nave]
% allData.pid  [nmag*nstep*nfreq*nave,nbpm]
% allData.x    [nmag*nstep*nfreq*nave,nbpm]
% allData.y    [nmag*nstep*nfreq*nave,nbpm]
% allData.t    [nmag*nstep*nfreq*nave,nbpm]
% allData.i    [nmag*nstep*nfreq*nave,nbpm]

% store setup values
allData.misc.energy=FL.SimModel.Initial.Momentum;
allData.misc.nmag=nmag;
allData.misc.nstep=nstep;
allData.misc.nfreq=nfreq;
allData.misc.nave=nave;
allData.misc.nbpm=nbpm;
allData.misc.nict=nict;
allData.misc.df=df;
allData.misc.dp=dp;

% loop over magnets
h=FL.Gui.extDispersion.text6; % for progress messages
n=0; % measurement index
pid=0; % pulse id
abort=false; % user abort request
for nm=1:nmag
  % get initial magnet data
  iel=findcells(BEAMLINE,'Name',name{nm});iel=iel(1);
  ips=BEAMLINE{iel}.PS;
  pv1=FL.HwInfo.PS(ips).pvname{1}; % ADC
  pv2=FL.HwInfo.PS(ips).pvname{2}; % set
  pv3=FL.HwInfo.PS(ips).pvname{3}; % DAC
  SetPt0=PS(ips).SetPt; % initial setting
  SetPt=linspace(SetPt0-dSetPt,SetPt0+dSetPt,nstep);
  if (debug)
    B=BEAMLINE{iel}.B;
    conv=FL.HwInfo.PS(ips).conv;
  end
  % store initial magnet data
  allData.mag(nm).name=name{nm};
  allData.mag(nm).iel=iel;
  allData.mag(nm).ips=ips;
  allData.mag(nm).B0=BEAMLINE{iel}.B;
  allData.mag(nm).SetPt0=SetPt0;
  allData.mag(nm).Iadc0=lcaGet(pv1,1);
  allData.mag(nm).Iset0=lcaGet(pv2,1);
  allData.mag(nm).Idac0=lcaGet(pv3,1);
  allData.mag(nm).SetPt=SetPt;
  
  % loop over magnet setpoints
  msg(h,sprintf('%s: SetPt=%f (initial)',name{nm},SetPt0),1)
  for ns=1:nstep
    msg(h,sprintf('%s: SetPt=%f (step %d of %d)',name{nm},SetPt(ns),ns,nstep),0)
    % set the magnet
    if (debug)
      I=interp1(conv(2,:),conv(1,:),B*SetPt(ns),'linear');
      stat{1}=1;
    else
      PS(ips).SetPt=SetPt(ns);
      stat=PSTrim(ips,true);
      if (stat{1}~=1)
        errordlg(stat{2},'extDispersion')
        return
      end
      pause(twait) % wait for magnet to settle
    end
    % turn DR RF-ramp ON
    msg(h,'Turning DR RF-ramp ON',0)
    stat=changeP(0,'on');
    if (stat{1}~=1)
      stat{2}='responseMatrixFull: DR RF-ramp ON failed';
      errordlg('DR RF-ramp ON failed','responseMatrixFull')
      return
    end

    % loop over energy values
    for nf=1:nfreq
      msg(h,sprintf('Set DR RF-ramp to %d kHz',df(nf)),0)
      % change beam momentum
      stat=changeP(dp(nf),df(nf));
      if (stat{1}~=1)
        stat{2}='responseMatrixFull: DR RF-ramp SET failed';
        errordlg('DR RF-ramp SET failed','responseMatrixFull')
        return
      end
      
      % loop over BPM readings
      for nr=1:nave
        n=n+1;
        % read magnet control values
        if (debug)
          allData.Iadc(n)=I;
          allData.Iset(n)=I;
          allData.Idac(n)=I;
        else
          allData.Iadc(n)=lcaGet(pv1,1);
          allData.Iset(n)=lcaGet(pv2,1);
          allData.Idac(n)=lcaGet(pv3,1);
        end
        % read
        stat{1}=1;data(1)=pid;iter=0;
        while (stat{1}==1&&data(1)==pid)
          if (iter==maxiter)
            stat{1}=-1;
            stat{2}='responseMatrixFull: BPM read failure ... maxiter';
          else
            iter=iter+1;
            FlHwUpdate('wait',1,60); % wait for next pulse ... timeout after 60 seconds
            [stat,data]=FlHwUpdate('readbuffer',1);
          end
        end
        if (stat{1}~=1),return,end
        pid=data(1);
        fprintf(1,'BPM read %d OK ... PID=%d\n',n,pid);
        % store data
        allData.id(n,:)=[nm,ns,nf,nr];
        allData.pid(n)=pid;
        allData.x(n,:)=data(idx);
        allData.y(n,:)=data(idy);
        allData.t(n,:)=data(idt);
        allData.i(n,:)=data(idi);
        % check for abort
        if (isfield(FL,'abort_extDispersion'))
          if strcmp(questdlg('Abort Current Operation?','Abort Request'),'Yes')
            abort=true;
            if (isfield(FL,'Gui')&&isfield(FL.Gui,'extDispersion'))
              set(FL.Gui.extDispersion.text6,'String','Abort requested')
            end
            break
          end
        end
      end % loop over BPM readings
      if (abort),break,end
    end % loop over energy values

    % turn DR RF-ramp OFF
    msg(h,'Turning DR RF-ramp OFF',0)
    stat=changeP(0,'off');
    if (stat{1}~=1)
      stat{2}='MeasureDispersionOnBPMs: DR RF-ramp OFF failed';
      errordlg('DR RF-ramp OFF failed','extDispersion')
      return
    end
    if (abort),break,end
  end % loop over magnet setpoints

  % reset the magnet
  if (debug)
    I=interp1(conv(2,:),conv(1,:),B*SetPt0,'linear');
    stat{1}=1;
  else
    PS(ips).SetPt=SetPt0;
    stat=PSTrim(ips,true);
    if (stat{1}~=1)
      errordlg(stat{2},'extDispersion')
      return
    end
    pause(twait) % wait for magnet to settle
  end
  if (abort)
    stat{1}=0;stat{2}='MeasureDispersionOnBPMs aborted by user';
    return
  end
end % loop over magnets

stat{1}=1;

end
function msg(h,t,c)
if (c)
  s={t};
else
  s=get(h,'String');
  s{end+1}=t;
end
set(h,'String',s)
drawnow('expose')
end

function nonlinDispCor(data,fudge)
% AUTODISPCOR
%  Use non-linear fitting to set QF1X/QF6X QS1X/QS2X to cancel dispersion
%  at fit point (horizontally) and to minimise vertical dispersion in FFS
global FL BEAMLINE PS
persistent ind

% Disable auto mode updating
FlHwUpdate(true); % force update
FL.noUpdate=true;

% Get match point and dispersion fitted there
[~,p]=extDispersion_run('GetPars');

% Get indices
if isempty(ind)
  ind.qf1=findcells(BEAMLINE,'Name','QF1X'); ind.qf1=ind.qf1(1);
  ind.ydispbpm=[findcells(BEAMLINE,'Name','MQM12FF') findcells(BEAMLINE,'Name','MQM11FF') ...
    findcells(BEAMLINE,'Name','MQD10BFF') findcells(BEAMLINE,'Name','MQF5BFF') ...
    findcells(BEAMLINE,'Name','MSF5FF') findcells(BEAMLINE,'Name','MQF5AFF')];
  ind.qf6=findcells(BEAMLINE,'Name','QF6X'); ind.qf6=ind.qf6(1);
  ind.qs1=findcells(BEAMLINE,'Name','QS1X'); ind.qs1=ind.qs1(1);
  ind.qs2=findcells(BEAMLINE,'Name','QS2X'); ind.qs2=ind.qs2(1);
  ind.ps=[BEAMLINE{ind.qf1}.PS BEAMLINE{ind.qf6}.PS BEAMLINE{ind.qs1}.PS ...
    BEAMLINE{ind.qs2}.PS];
  ind.id0=p.target.ind;
end

% Get current fitted dispersions at ydispbpm points
[~,Df,dDf]=dispfit([ind.qf1-1 ind.id0 ind.ydispbpm],data);
for idf=1:4
  Df(idf,:)=Df(idf,:).*fudge(idf);
end

% Find dispersion correction setting which generate the negative of these
% fitted values
PS0=arrayfun(@(x) PS(x).Ampl,ind.ps);
corVals=lsqnonlin(@(x) dispmin(x,Df,dDf,PS0,ind),[0 0 0],-0.5,0.5,optimset('Display','iter','TolX',1e-9,'TolFun',1e-9));
corVals(end+1)=corVals(end);
corVals=corVals./[2 2 4 4];

% Resume auto-updating capability
FL.noUpdate=false;
FlHwUpdate(true); % force update

% Set fitted values
% -- first display changes about to be made
i1qf1=interp1(FL.HwInfo.PS(ind.ps(1)).conv(2,:),FL.HwInfo.PS(ind.ps(1)).conv(1,:),abs(BEAMLINE{ind.qf1}.B*PS0(1)));
i1qf6=interp1(FL.HwInfo.PS(ind.ps(2)).conv(2,:),FL.HwInfo.PS(ind.ps(2)).conv(1,:),abs(BEAMLINE{ind.qf6}.B*PS0(2)));
i1qs1=interp1(FL.HwInfo.PS(ind.ps(3)).conv(2,:),FL.HwInfo.PS(ind.ps(3)).conv(1,:),BEAMLINE{ind.qs1}.B*PS0(3));
i1qs2=interp1(FL.HwInfo.PS(ind.ps(4)).conv(2,:),FL.HwInfo.PS(ind.ps(4)).conv(1,:),BEAMLINE{ind.qs2}.B*PS0(4));
iqf1=interp1(FL.HwInfo.PS(ind.ps(1)).conv(2,:),FL.HwInfo.PS(ind.ps(1)).conv(1,:),abs(BEAMLINE{ind.qf1}.B*(PS0(1)+corVals(1))));
iqf6=interp1(FL.HwInfo.PS(ind.ps(2)).conv(2,:),FL.HwInfo.PS(ind.ps(2)).conv(1,:),abs(BEAMLINE{ind.qf6}.B*(PS0(2)+corVals(2))));
if fudge(3)==0 || fudge(4)==0
  iqs1=i1qs1; iqs2=i1qs2;
else
  iqs1=interp1(FL.HwInfo.PS(ind.ps(3)).conv(2,:),FL.HwInfo.PS(ind.ps(3)).conv(1,:),BEAMLINE{ind.qs1}.B*(PS0(3)+corVals(3)));
  iqs2=interp1(FL.HwInfo.PS(ind.ps(4)).conv(2,:),FL.HwInfo.PS(ind.ps(4)).conv(1,:),BEAMLINE{ind.qs2}.B*(PS0(4)+corVals(4)));
end
if isequal(questdlg(sprintf('Apply magnet current changes (A):\nQF1X %.3f -> %.3f\nQF6X: %.3f -> %.3f\nQS1X: %.3f -> %.3f\nQS2X: %.3f -> %.3f',...
    i1qf1,iqf1,i1qf6,iqf6,i1qs1,iqs1,i1qs2,iqs2)),'Yes')
  for ips=1:length(ind.ps)
    PS(ind.ps(ips)).SetPt=PS0(ips)+corVals(ips);
  end
  PSTrim(ind.ps(fudge>0),1);
end



function rfun=dispmin(x,Df,dDf,PS0,ind)
global PS

% Set simulation PS settings
for ips=1:length(x)
  PS(ind.ps(ips)).Ampl=PS0(ips)+x(ips);
end
PS(ind.ps(end)).Ampl=PS(ind.ps(end-1)).Ampl;

% Get projected dispersion function with these settings
id1=ind.qf1-1;
inds=[ind.id0 ind.ydispbpm];
Nelem=length(inds);
Dfm=zeros(4,Nelem);
% disp [Dx D'x Dy D'y]
for n=1:Nelem
  [~,R]=RmatAtoB(id1,inds(n)-1);
  D=R(1:4,6);
  R=R(1:4,1:4);
  Dfm(:,n)=R*Df(:,1)+D;
end

% Return function is fit to desired negative of currently fitted dispersion
% values
rfun=[(abs(-Df(1:2,2)-Dfm(1:2,1))./dDf(1:2,2))' abs(-Df(3,3:end)-Dfm(3,2:end))./dDf(3,3:end)];

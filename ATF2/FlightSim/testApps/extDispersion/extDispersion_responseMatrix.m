function stat=extDispersion_responseMatrix(xonly)

global BEAMLINE PS FL

debug=false;
if (debug)
  helpdlg('NOTE: "debug" flag set in extDispersion_responseMatrix', ...
    'extDispersion: Cal Knobs');
end

% initialize
[stat,pars]=extDispersion_run('getpars');
if (stat{1}~=1),return,end
pars.wiremeas=false; % make sure no wirescans
nstep=pars.nstep;
dSetPt=0.02; % +-dSetPt around present SetPt
h=FL.Gui.extDispersion.text6;

% scan QF1X and QF6X
name={'QF1X','QF6X'};
SetPt0=zeros(1,2);
SetPt=zeros(nstep,2);
DX=zeros(nstep,2);DXerr=zeros(nstep,2);
DPX=zeros(nstep,2);DPXerr=zeros(nstep,2);
abort=false; % user abort request
for n=1:2
  iel=findcells(BEAMLINE,'Name',name{n});iel=iel(1);
  ips=BEAMLINE{iel}.PS;
  SetPt0(n)=PS(ips).SetPt; % initial setting
  msg(h,sprintf('%s: SetPt=%f (initial)',name{n},SetPt0(n)),1)
  SetPt(:,n)=linspace(SetPt0(n)-dSetPt,SetPt0(n)+dSetPt,nstep);
  for m=1:nstep
    msg(h,sprintf('%s: SetPt=%f (step %d of %d)',name{n},SetPt(m,n),m,nstep),0)
    if (debug)
      stat{1}=1;
    else
      PS(ips).SetPt=SetPt(m,n);
      stat=PSTrim(ips,true);
      if (stat{1}~=1)
        errordlg(stat{2},'extDispersion')
        return
      end
    end
    if (debug)
      stat{1}=1;
    else
      FL.Gui.extDispersion.quietMeas=true;
      stat=extDispersion_run('Measure');
      FL.Gui.extDispersion=rmfield(FL.Gui.extDispersion,'quietMeas');
    end
    if (stat{1}<0)
      errordlg(stat{2},'extDispersion')
      return
    elseif (stat{1}==0)
      abort=true;
      msg(h,'Aborted',0)
      break
    end
    [stat,data]=extDispersion_run('GetData');
    if (stat{1}~=1)
      errordlg(stat{2},'extDispersion')
      return
    end
    DX(m,n)=data.etaVector(1);DXerr(m,n)=data.etaVector_err(1);
    DPX(m,n)=data.etaVector(2);DPXerr(m,n)=data.etaVector_err(2);
  end
  msg(h,sprintf('%s: SetPt=%f (reset)',name{n},SetPt0(n)),0)
  if (debug)
    stat{1}=1;
  else
    PS(ips).SetPt=SetPt0(n);
    stat=PSTrim(ips,true);
    if (stat{1}~=1)
      errordlg(stat{2},'extDispersion')
      return
    end
  end
  if (abort)
    stat{1}=0;stat{2}='extDispersion_responseMatrix aborted by user';
    return
  end
end

% plot raw data
h=[FL.Gui.extDispersion_plot1,FL.Gui.extDispersion_plot2];
A=zeros(2,2);
for n=1:2
  figure(h(n))
  [q1,dq1]=noplot_polyfit(SetPt(:,n),DX(:,n),DXerr(:,n),1);
  [q2,dq2]=noplot_polyfit(SetPt(:,n),DPX(:,n),DPXerr(:,n),1);
  A(:,n)=[q1(2);q2(2)];
  plot_barsc(SetPt(:,n),1e3*DX(:,n),1e3*DXerr(:,n),'b','o')
  hold on
  plot(SetPt(:,n),1e3*polyval(fliplr(q1),SetPt(:,n)),'b--')
  plot_barsc(SetPt(:,n),1e3*DPX(:,n),1e3*DPXerr(:,n),'r','o')
  plot(SetPt(:,n),1e3*polyval(fliplr(q2),SetPt(:,n)),'r--')
  hold off
  ylabel('EtaX (mm) [blue] , EtaPX (mrad) [red]')
  xlabel(sprintf('%s SetPt',name{n}))
  set(gcf,'Name',sprintf('%s Response',name{n}))
end

% construct knobs
[stat,knobs]=extDispersion_run('GetKnobs');
b=[1;0];x=A\b; % EtaX knob
knobs.EtaX.Channel(1).Coefficient=x(1);
knobs.EtaX.Channel(2).Coefficient=x(2);
b=[0;1];px=A\b; % EtaPX knob
knobs.EtaPX.Channel(1).Coefficient=px(1);
knobs.EtaPX.Channel(2).Coefficient=px(2);
%extDispersion_run('SetKnobs',knobs);
save testApps/extDispersion/knobCal SetPt DX DXerr DPX DPXerr

% display new knob coefficients
t={sprintf('EtaX: %.3g %.3g',x(1),x(2));sprintf('EtaPX: %.3g %.3g',px(1),px(2))};
set(FL.Gui.extDispersion.text21,'String',t)

end
function msg(h,t,c)
if (c)
  s={t};
else
  s=get(h,'String');
  s{end+1}=t;
end
set(h,'String',s)
drawnow('expose')
end

function [stat output] = extdypknob(cmd,val)
global PS BEAMLINE FL
persistent scale bval reqID
stat{1}=1; output=[];

if FL.SimMode~=2 && (isempty(reqID) || strcmpi(cmd,'accessrequest'))
  zv5x=findcells(BEAMLINE,'Name','ZV5X'); zv5ps=BEAMLINE{zv5x}.PS;
  zv6x=findcells(BEAMLINE,'Name','ZV6X'); zv6ps=BEAMLINE{zv6x}.PS;
  zv7x=findcells(BEAMLINE,'Name','ZV7X'); zv7ps=BEAMLINE{zv7x}.PS;
  [stat reqID]=AccessRequest({[] [zv5ps zv6ps zv7ps] []});
  if stat{1}~=1; reqID=[]; return; end;
  if strcmpi(cmd,'accessrequest'); return; end;
end
  

if isempty(bval); bval=0; end;
switch lower(cmd)
  case {'calibrate','modelcal'}
    if exist('val','var')
      bcal=val;
    else
      bcal=linspace(-5e-3,5e-3,5);
    end
    zv5x=findcells(BEAMLINE,'Name','ZV5X'); zv5ps=BEAMLINE{zv5x}.PS;
    zv6x=findcells(BEAMLINE,'Name','ZV6X'); zv6ps=BEAMLINE{zv6x}.PS;
    zv7x=findcells(BEAMLINE,'Name','ZV7X'); zv7ps=BEAMLINE{zv7x}.PS;
    psinit=[PS(zv5ps).Ampl PS(zv6ps).Ampl PS(zv7ps).Ampl];
    if strcmpi(cmd,'modelcal')
      corbumpflag='UseModel';
      [stat origpars]=extDispersion_run('GetPars');
      newpars.useModel=true;
      extDispersion_run('SetPars',newpars);
    else
      corbumpflag=true;
    end
    for ibump=1:length(bcal)
      fprintf('Calibrating bump setting %d of %d...\n',ibump,length(bcal))
      stat = corbump3(bcal(ibump),'ZV5X','ZV6X','ZV7X',corbumpflag); if stat{1}~=1; return; end;
      stat=extDispersion_run('Measure'); if stat{1}~=1; return; end;
      [stat data]=extDispersion_run('GetData');
      exteta(:,ibump)=data.etaVector;
      if strcmpi(cmd,'modelcal')
        PS(zv5ps).Ampl=psinit(1); PS(zv6ps).Ampl=psinit(2); PS(zv7ps).Ampl=psinit(3);
      else
        PS(zv5ps).SetPt=psinit(1); PS(zv6ps).SetPt=psinit(2); PS(zv7ps).SetPt=psinit(3);
        PSTrim([zv5ps zv6ps zv7ps],FL.SimMode~=2);
      end
    end
    for idim=1:4
      P=noplot_polyfit(bcal,exteta(idim,:),1,1);
      slope(idim)=P(end);
    end
    output{1}=bcal;
    output{2}=exteta;
    scale=slope(4);
    if strcmpi(cmd,'modelcal')
      newpars.useModel=origpars.useModel;
      extDispersion_run('SetPars',newpars);
    end
  case 'increment'
    if isempty(scale); scale=3.16; end;
    stat = corbump3(val/scale,'ZV5X','ZV6X','ZV7X',false);
    bval=bval+val/scale;
  case 'getval'
    output=bval*scale;
  case 'set'
    if isempty(scale); scale=3.16; end;
    bval=val/scale - bval;
    stat = corbump3(bval,'ZV5X','ZV6X','ZV7X',false);
  otherwise
    error('unkown command')
end
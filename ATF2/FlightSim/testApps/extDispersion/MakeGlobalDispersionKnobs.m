function [stat,varargout]=MakeGlobalDispersionKnobs()
% Generate design EXT dispersion knobs
%   [stat,EtaX,EtaPX,EtaY,EtaPY,Sum]=MakeGlobalDispersionKnobs;

global BEAMLINE PS INSTR FL
persistent EtaX EtaPX EtaY EtaPY Sum

% return previously generated knobs
if (nargout>1)
  if (isempty(EtaX)||isempty(EtaPX)||isempty(EtaY)||isempty(EtaPY)||isempty(Sum))
    stat{1}=0;
    stat{2}='MakeGlobalDispersionKnobs: no knobs';
  else
    stat{1}=1;
    varargout={EtaX,EtaPX,EtaY,EtaPY,Sum};
  end
  return
end

% get dispersion measurement/correction setup parameters
fname='extDispersion_run'; % want to run this
if exist(fname,'file')
  p=[];
else
  [iss,r]=system(sprintf('find . -name %s.m',fname)); % find it
  if (iss~=0)
    stat{1}=0;
    stat{2}=sprintf('MakeGlobalDispersionKnobs: could not find %s.m',fname);
    disp(stat{2})
    return
  end
  p=fileparts(r);
  if ~isdeployed
    addpath(p)
  end
end
[stat,pars]=extDispersion_run('GetPars0');
if (~isempty(p)),rmpath(p),end
idt=pars.target.ind; % dispersion correction target point

debug=false;
if (debug)
  helpdlg('NOTE: "debug" flag set in MakeGlobalDispersion Knobs', ...
    'extDispersion');
end

% EtaX,EtaPX -------------------------------------------------------------------

% define devices
magnet1=findcells(BEAMLINE,'Name','QF1X');
magnet2=findcells(BEAMLINE,'Name','QF6X');

% get list of PSs
ps1=BEAMLINE{magnet1(1)}.PS;
ps2=BEAMLINE{magnet2(1)}.PS;

% get initial Twiss
twxi=FL.SimModel.Initial.x.Twiss;
twyi=FL.SimModel.Initial.y.Twiss;

% simulate QF1X response
Ampl0=PS(ps1).Ampl; % save initial Ampl
psSet=(0.98:0.01:1.02);
for ips=1:length(psSet)
  PS(ps1).Ampl=psSet(ips);
  [stat,tw]=GetTwiss(1,length(BEAMLINE),twxi,twyi);
  etax(ips)=tw.etax(idt);
  etapx(ips)=tw.etapx(idt);
end
if (debug)
  figure(1)
  P1a=flipud(plot_polyfit(psSet,etax,1,1))';
  xlabel('QF1X'),ylabel('EtaX')
  figure(2)
  P1b=flipud(plot_polyfit(psSet,etapx,1,1))';
  xlabel('QF1X'),ylabel('EtaPX')
else
  P1a=polyfit(psSet,etax,1);
  P1b=polyfit(psSet,etapx,1);
end
PS(ps1).Ampl=Ampl0; % restore initial Ampl

% simulate QF6X response
Ampl0=PS(ps2).Ampl; % save initial Ampl
for ips=1:length(psSet)
  PS(ps2).Ampl=psSet(ips);
  [stat,tw]=GetTwiss(1,length(BEAMLINE),twxi,twyi) ;
  etax(ips)=tw.etax(idt);
  etapx(ips)=tw.etapx(idt);
end
if (debug)
  figure(3)
  P2a=flipud(plot_polyfit(psSet,etax,1,1))';
  xlabel('QF6X'),ylabel('EtaX')
  figure(4)
  P2b=flipud(plot_polyfit(psSet,etapx,1,1))';
  xlabel('QF6X'),ylabel('EtaPX')
else
  P2a=polyfit(psSet,etax,1);
  P2b=polyfit(psSet,etapx,1);
end
PS(ps2).Ampl=Ampl0; % restore initial Ampl

% response matrix
% R=[dEtaX(m)/dQF1X(1),dEtaX(m)/dQF6X(1); ...
%    dEtaPX(m)/dQF1X(1),dEtaPX(m)/dQF6X(1)]
R=[P1a(1),P2a(1);P1b(1),P2b(1)];

% multiknob coefficients (already in PS units)
knob1=R\[1;0]; % [dQF1X(1)/dEtaX(m);dQF6X(1)/dEtaX(m)]
knob2=R\[0;1]; % [dQF1X(1)/dEtaPX(m);dQF6X(1)/dEtaPX(m)]

% construct the multiknobs
name=sprintf('EtaX at %s [m]',pars.target.name);
[stat,EtaX]=MakeMultiKnob(name, ...
  ['PS(',num2str(ps1),').SetPt'],knob1(1), ...
  ['PS(',num2str(ps2),').SetPt'],knob1(2));
name=sprintf('EtaX'' at %s [rad]',pars.target.name);
[stat,EtaPX]=MakeMultiKnob(name, ...
  ['PS(',num2str(ps1),').SetPt'],knob2(1), ...
  ['PS(',num2str(ps2),').SetPt'],knob2(2));

% EtaY,EtaPY -------------------------------------------------------------------

% the EtaY and EtaPY knobs are linear combinations of the QS1X/QS2X "sum knob"
% (which sets both QS1X and QS2X to the same strength in order to generate
% vertical dispersion without generating x-y coupling) and the ZV5X/ZV6X/ZV7X
% "Kubo" bump knob

% simulate "Kubo" bump response

% define devices
idc1=findcells(BEAMLINE,'Name','ZV5X');  % 1st
id0=findcells(BEAMLINE,'Name','MQD8X');  % 2nd
idc2=findcells(BEAMLINE,'Name','ZV6X');  % 3rd
idc3=findcells(BEAMLINE,'Name','ZV7X');  % 4th

% get INSTR pointers
% id0i=findcells(INSTR,'Index',id0);
% idti=findcells(INSTR,'Index',idt);

% generate drift matrices for mapping to/from corrector centers
L=BEAMLINE{idc1}.L;Dc1=[1,L/2;0,1]; % drift half corrector
L=BEAMLINE{idc2}.L;Dc2=[1,L/2;0,1]; % drift half corrector
L=BEAMLINE{idc3}.L;Dc3=[1,L/2;0,1]; % drift half corrector

% get exit-to-entrance Rmats
[stat,A]=RmatAtoB(idc1+1,id0);    % ZV5X exit to MQD8X
[stat,B]=RmatAtoB(idc1+1,idc3-1); % ZV5X exit to ZV7X entrance
[stat,C]=RmatAtoB(idc2+1,idc3-1); % ZV6X exit to ZV7X entrance

% convert to center-to-center Rmats
A=A(3:4,3:4)*Dc1;     % ZV5X center to MQD8X
B=Dc3*B(3:4,3:4)*Dc1; % ZV5X center to ZV7X center
C=Dc3*C(3:4,3:4)*Dc2; % ZV6X center to ZV7X center

% form response matrix and compute bump coefficients
R=[A(1,2),0,0;B(1,2),C(1,2),0;B(2,2),C(2,2),1];
kick=R\[1;0;0]; % kick(rad)/bump(m)

% get list of PSs
idpsc=[BEAMLINE{idc1}.PS,BEAMLINE{idc2}.PS,BEAMLINE{idc3}.PS];

if (debug)
  % compute and display settings for +1 mm bump
  Izv=zeros(1,3);
  for n=1:3
    eval(sprintf('brho=BEAMLINE{idc%d}.B;',n))
    bvi=FL.HwInfo.PS(idpsc(n)).conv;
    Izv(n)=brho*(0.001*kick(n))/bvi;     %#ok<NODEF>
  end
  disp(' ')
  disp('For a +1 mm Y-bump at MQD8X:')
  disp(' ')
  fprintf(1,'  ZV5X: %+6.3f mrad , %+6.3f amp\n',kick(1),Izv(1));
  fprintf(1,'  ZV6X: %+6.3f mrad , %+6.3f amp\n',kick(2),Izv(2));
  fprintf(1,'  ZV7X: %+6.3f mrad , %+6.3f amp\n',kick(3),Izv(3));
end

% set up a single particle to track
p0=FL.SimModel.Initial.Momentum; % GeV
brho=p0/0.299792458; % rigidity (T-m)
beamIn=FL.SimBeam{2}; % single particle
beamIn.Bunch.x(1:5)=0; % zero out the initial trajectory
beamIn.Bunch.x(6)=p0; % on-energy
iex=FL.SimModel.extStart; % track from start of EXT

% make 2D scan of bump and energy ... observe etay/etapy
bumpSet=1e-3*(-3:3)';Nb=length(bumpSet); % bump range
dpSet=1e-3*(-2:2);Np=length(dpSet); % dP/P range
yb=zeros(Nb,1);
pyb=zeros(Nb,1);
yt=zeros(Nb,Np);
pyt=zeros(Nb,Np);
etay=zeros(Nb,1);
etapy=zeros(Nb,1);
Ampl0=[PS(idpsc).Ampl]; % save initial Ampls
for nb=1:Nb
  for m=1:3,PS(idpsc(m)).Ampl=bumpSet(nb)*kick(m);end
  [stat,beamOut]=TrackThru(iex,id0,beamIn,1,1);
  yb(nb)=beamOut.Bunch.x(3);
  pyb(nb)=beamOut.Bunch.x(4);
  for np=1:Np
    beamIn.Bunch.x(6)=p0*(1+dpSet(np));
    [stat,beamOut]=TrackThru(iex,idt,beamIn,1,1);
    yt(nb,np)=beamOut.Bunch.x(3);
    pyt(nb,np)=beamOut.Bunch.x(4);
  end
  q=noplot_polyfit(dpSet,yt(nb,:),1,1);
  etay(nb)=q(2);
  q=noplot_polyfit(dpSet,pyt(nb,:),1,1);
  etapy(nb)=q(2);
  continue
end
for m=1:3,PS(idpsc(m)).Ampl=Ampl0(m);end % restore initial Ampls

if (debug)
  figure(5)
  plot_polyfit(bumpSet,yb,1,1)
  xlabel('Bump Knob (m)'),ylabel('MQD8X Y (m)')
  figure(6)
  plot_polyfit(bumpSet,pyb,1,1)
  xlabel('Bump Knob (m)'),ylabel('MQD8X PY (rad)')
  figure(7)
  q=plot_polyfit(bumpSet,etay,1,1);
  xlabel('Bump Knob (m)'),ylabel('EtaY (m)')
  P1a=q(2);
  figure(8)
  q=plot_polyfit(bumpSet,etapy,1,1);
  xlabel('Bump Knob (m)'),ylabel('EtaPY (rad)')
  P1b=q(2);
else
  q=noplot_polyfit(bumpSet,etay,1,1);
  P1a=q(2);
  q=noplot_polyfit(bumpSet,etapy,1,1);
  P1b=q(2);
end

% simulate "sum knob" response

% define devices
idq1=findcells(BEAMLINE,'Name','QS1X');
idq2=findcells(BEAMLINE,'Name','QS2X');

% get list of PSs
idpsq=[BEAMLINE{idq1(1)}.PS,BEAMLINE{idq2(1)}.PS];

% scan the knob and the energy and observe etay/etapy at the target point
skewSet=(-0.04:0.02:0.04); % GL/2 per magnet (T)
Ns=length(skewSet);
yt=zeros(Ns,Np);
pyt=zeros(Ns,Np);
etay=zeros(Ns,1);
etapy=zeros(Ns,1);
Ampl0=[PS(idpsq).Ampl]; % save initial Ampls
for ns=1:Ns
  for m=1:2,PS(idpsq(m)).Ampl=skewSet(ns);end
  for np=1:Np
    beamIn.Bunch.x(6)=p0*(1+dpSet(np));
    [stat,beamOut]=TrackThru(iex,idt,beamIn,1,1);
    yt(ns,np)=beamOut.Bunch.x(3);
    pyt(ns,np)=beamOut.Bunch.x(4);
  end
  q=noplot_polyfit(dpSet,yt(ns,:),1,1);
  etay(ns)=q(2);
  q=noplot_polyfit(dpSet,pyt(ns,:),1,1);
  etapy(ns)=q(2);
end
for m=1:2,PS(idpsq(m)).Ampl=Ampl0(m);end % restore initial Ampls

if (debug)
  figure(9)
  q=plot_polyfit(skewSet,etay,1,1);
  xlabel('Sum Knob (T)'),ylabel('EtaY (m)')
  P2a=q(2);
  figure(10)
  q=plot_polyfit(skewSet,etapy,1,1);
  xlabel('Sum Knob (T)'),ylabel('EtaPY (rad)')
  P2b=q(2);
else
  q=noplot_polyfit(skewSet,etay,1,1);
  P2a=q(2);
  q=noplot_polyfit(skewSet,etapy,1,1);
  P2b=q(2);
end

% make etay/etapy knob coefficients

R=[P1a,P2a;P1b,P2b];
knob1=R\[1;0]; % [DY(m)/bumpKnob(m);DY(m)/sumKnob(T)]
knob2=R\[0;1]; % [DPY(m)/bumpKnob(m);DPY(m)/sumKnob(T)]

if (debug)
  v1=knob1*0.01; % [bump(m),GL/2(T)] ... for +10 mm EtaY
  v1(2)=2*v1(2)/brho; % KL(1/m)
  v2=knob2*0.005; % [bump(m),GL/2(T)] ... for +5 mrad EtaPY
  v2(2)=2*v2(2)/brho; % KL(1/m)
  disp(' ')
  disp('For +10 mm EtaY:')
  disp(' ')
  fprintf(1,'  Bump: %+6.3f    mm\n',1e3*v1(1));
  fprintf(1,'  KLQS: %+9.6f 1/m\n',v1(2));
  disp(' ')
  disp('For +5 mrad EtaPY:')
  disp(' ')
  fprintf(1,'  Bump: %+6.3f    mm\n',1e3*v2(1));
  fprintf(1,'  KLQS: %+9.6f 1/m\n',v2(2));
end

% finally, create the EtaY/EtaPY multiknobs

name=sprintf('EtaY at %s [m]',pars.target.name);
[stat,EtaY]=MakeMultiKnob(name, ...
  ['PS(',num2str(idpsc(1)),').SetPt'],knob1(1)*kick(1), ...
  ['PS(',num2str(idpsc(2)),').SetPt'],knob1(1)*kick(2), ...
  ['PS(',num2str(idpsc(3)),').SetPt'],knob1(1)*kick(3), ...
  ['PS(',num2str(idpsq(1)),').SetPt'],knob1(2), ...
  ['PS(',num2str(idpsq(2)),').SetPt'],knob1(2));
name=sprintf('EtaY'' at %s [rad]',pars.target.name);
[stat,EtaPY]=MakeMultiKnob(name, ...
  ['PS(',num2str(idpsc(1)),').SetPt'],knob2(1)*kick(1), ...
  ['PS(',num2str(idpsc(2)),').SetPt'],knob2(1)*kick(2), ...
  ['PS(',num2str(idpsc(3)),').SetPt'],knob2(1)*kick(3), ...
  ['PS(',num2str(idpsq(1)),').SetPt'],knob2(2), ...
  ['PS(',num2str(idpsq(2)),').SetPt'],knob2(2));

if (debug)
  coef1=[EtaY.Channel.Coefficient];
  v1=coef1*0.01; % [kick5(rad),kick6(rad),kick7(rad),GL1/2(T),GL2/2(T)] ... for +10 mm EtaY
  I=zeros(1,5);
  for n=1:3
    eval(sprintf('brho=BEAMLINE{idc%d}.B;',n))
    bvi=FL.HwInfo.PS(idpsc(n)).conv;
    I(n)=brho*v1(n)/bvi;    
  end
  conv=FL.HwInfo.PS(idpsq(1)).conv; % B vs I lookup table (assume both skew quads are identical)
  conv=[conv,-conv]; % include the other polarity ...
  [temp,idi]=unique(conv(1,:)); % ... and manipulate ...
  conv=conv(:,idi); % ... to make the table bipolar
  I(4)=interp1(conv(2,:),conv(1,:),v1(4));
  I(5)=interp1(conv(2,:),conv(1,:),v1(5));
  disp(' ')
  disp('For +10 mm EtaY:')
  disp(' ')
  fprintf(1,'  ZV5X: %+7.3f amp\n',I(1));
  fprintf(1,'  ZV6X: %+7.3f amp\n',I(2));
  fprintf(1,'  ZV7X: %+7.3f amp\n',I(3));
  fprintf(1,'  QS1X: %+7.3f amp\n',I(4));
  fprintf(1,'  QS2X: %+7.3f amp\n',I(5));
  disp(' ')
  coef2=[EtaPY.Channel.Coefficient];
  v2=coef2*0.005; % [kick5(rad),kick6(rad),kick7(rad),GL1/2(T),GL2/2(T)] ... for +5 mrad EtaPY
  for n=1:3
    eval(sprintf('brho=BEAMLINE{idc%d}.B;',n))
    bvi=FL.HwInfo.PS(idpsc(n)).conv;
    I(n)=brho*v2(n)/bvi;    
  end
  I(4)=interp1(conv(2,:),conv(1,:),v2(4));
  I(5)=interp1(conv(2,:),conv(1,:),v2(5));
  disp(' ')
  disp('For +5 mrad EtaPY:')
  disp(' ')
  fprintf(1,'  ZV5X: %+7.3f amp\n',I(1));
  fprintf(1,'  ZV6X: %+7.3f amp\n',I(2));
  fprintf(1,'  ZV7X: %+7.3f amp\n',I(3));
  fprintf(1,'  QS1X: %+7.3f amp\n',I(4));
  fprintf(1,'  QS2X: %+7.3f amp\n',I(5));
  disp(' ')
end

% Sum knob -------------------------------------------------------------------

% define devices
magnet1=findcells(BEAMLINE,'Name','QS1X');
magnet2=findcells(BEAMLINE,'Name','QS2X');

% get list of PSs
ps1=BEAMLINE{magnet1(1)}.PS;
ps2=BEAMLINE{magnet2(1)}.PS;

% compute coefficients (knob value in amperes)
I=FL.HwInfo.PS(ps1).conv(1,:);
GL=FL.HwInfo.PS(ps1).conv(2,:);
coef1=noplot_polyfit(I,GL,1,[0,1]); % dGL/dI slope
I=FL.HwInfo.PS(ps2).conv(1,:);
GL=FL.HwInfo.PS(ps2).conv(2,:);
coef2=noplot_polyfit(I,GL,1,[0,1]); % dGL/dI slope

% construct the multiknobs
name='Sum knob [amp]';
[stat,Sum]=MakeMultiKnob(name, ...
  ['PS(',num2str(ps1),').SetPt'],coef1, ...
  ['PS(',num2str(ps2),').SetPt'],coef2);

end

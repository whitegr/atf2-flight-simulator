function [stat resp] = controls_access(cmd)
% function [stat resp] = controls_access(cmd)
% Deal with access rights for this app
% stat = Lucretia status response
% resp = true if have access to required list
% if cmd='release', then release previous access request
persistent reqID accessList
global BEAMLINE FL

% If in non-Floodland simulation mode, do nothing and return success
if FL.SimMode==2
  stat{1}=1; resp=true; return;
end % if sim mode = 2

% Access requirement list
if isempty(accessList)
  qf1x=findcells(BEAMLINE,'Name','QF1X');
  qf6x=findcells(BEAMLINE,'Name','QF6X');
  qs1x=findcells(BEAMLINE,'Name','QS1X');
  qs2x=findcells(BEAMLINE,'Name','QS2X');
  zv5x=findcells(BEAMLINE,'Name','ZV5X');
  zv6x=findcells(BEAMLINE,'Name','ZV6X');
  zv7x=findcells(BEAMLINE,'Name','ZV7X');
  accessList={[] ...
    [BEAMLINE{qf1x(1)}.PS ...
    BEAMLINE{qf6x(1)}.PS ...
    BEAMLINE{qs1x(1)}.PS ...
    BEAMLINE{qs2x(1)}.PS ...
    BEAMLINE{zv5x}.PS ...
    BEAMLINE{zv6x}.PS ...
    BEAMLINE{zv7x}.PS] ...
  []};
end % if not initialised list yet

% Deal with any requested commands
if exist('cmd','var') && isequal(lower(cmd),'release') && ~isempty(reqID)
  [stat resp] = AccessRequest('release',reqID);
  return
end % if release requested

% Request or return status of access request
if isempty(reqID) % no access- request it
  [stat resp] = AccessRequest(accessList);
  if stat{1}~=1; return; end;
  reqID=resp;
  resp=true;
else % access already requested, check status
  [stat resp] = AccessRequest('status',reqID);
  if stat{1}~=1; return; end;
  if isempty(resp); clear reqID; end;
end % if isempty(reqID)

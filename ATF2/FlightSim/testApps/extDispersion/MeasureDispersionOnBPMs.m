function [stat,data]=MeasureDispersionOnBPMs(Model,pars,data,remeas)
%
% [stat,data]=MeasureDispersionOnBPMs(Model,pars,data,remeas);

% function which measures the dispersion (dOrbit/dEnergy) on several BPMs and
% returns the least-squares fitted [etax;etapx;etay;etapy] at a designated
% target BPM

global FL BEAMLINE INSTR

if (isfield(FL,'Gui')&& ...
    isfield(FL.Gui,'extDispersion')&& ...
    isfield(FL.Gui.extDispersion,'text6')&& ...
    (~isfield(FL.Gui.extDispersion,'quietMeas')|| ...
     ~FL.Gui.extDispersion.quietMeas))
  verbose=true;
  h=FL.Gui.extDispersion.text6;
else
  verbose=false;
end

stat{1}=1;

% remeas ... list of BPMs to use might have changed ... refit existing data
if (exist('remeas','var')&&remeas&&isfield(data,'xbpms_all'))
  if (pars.measMethod==1)
    [data.etaVector,data.etaVector_err,mse,data.cov]=lscov(Model.EtaMatrix,...
    [data.xq(2,Model.EtaBPMs_instdata_index)';data.yq(2,Model.EtaBPMs_instdata_index)'],...
    1./[data.xdq(2,Model.EtaBPMs_instdata_index)'.^2;data.ydq(2,Model.EtaBPMs_instdata_index)'.^2]); 
    data.etaVector_err=data.etaVector_err*sqrt(1/mse);
    data.cov=data.cov*(1/mse);
    data.xbpms=data.xbpms_all(:,Model.EtaBPMs_instdata_index);
    data.ybpms=data.xbpms_all(:,Model.EtaBPMs_instdata_index);
  else
    [data.etaVector,data.etaVector_err,mse,data.cov]=lscov(Model.EtaMatrix,...
    [data.xq(2,Model.EtaBPMs_INSTR_index)';data.yq(2,Model.EtaBPMs_INSTR_index)'],...
    1./[data.xdq(2,Model.EtaBPMs_INSTR_index)'.^2;data.ydq(2,Model.EtaBPMs_INSTR_index).^2']); 
    data.etaVector_err=data.etaVector_err*sqrt(1/mse);
    data.cov=data.cov*(1/mse);
    data.xbpms=data.xbpms_all(:,Model.EtaBPMs_INSTR_index);
    data.ybpms=data.xbpms_all(:,Model.EtaBPMs_INSTR_index);
  end
  return
elseif (exist('remeas','var')&&remeas&&~isfield(data,'xbpms_all'))
  return
end

% initialize data arrays
data.xbpms_all=[];data.xbpms_all_rms=[];data.xbpms_all_e=[];
data.ybpms_all=[];data.ybpms_all_rms=[];data.ybpms_all_e=[];
data.xwdata=[];data.xwdata_rms=[];
data.ywdata=[];data.ywdata_rms=[];
data.xpip=[];data.xpip_rms=[];
data.ypip=[];data.ypip_rms=[];
data.ebpms=[];
data.xbpm_e=[];data.xbpm_e_rms=[];
data.ybpm_e=[];data.ybpm_e_rms=[];
data.etaVecUsed=Model.DeltaVec;

% turn DR RF-ramp ON
if (verbose),MeasDispEvent(0,h),end
stat=changeP(0,'on');
if (stat{1}~=1)
  stat{2}='MeasureDispersionOnBPMs: DR RF-ramp ON failed';
  errordlg('DR RF-ramp ON failed','Error')
  return
end

% measure
abort=false;
for count=1:length(Model.DeltaVec)
  if (verbose)
    msg=sprintf('Requesting Ramp setting change to %d (kHz)',Model.RampVec(count));
    MeasDispEvent(1,h,msg)
  end
  % change beam momentum
  stat=changeP(Model.DeltaVec(count),Model.RampVec(count));
  if (stat{1}~=1)
    stat{2}='MeasureDispersionOnBPMs: DR RF-ramp SET failed';
    errordlg('DR RF-ramp SET failed','Error')
    return
  end
  % Get average BPM readings (wait for requisite number of pulses to exist
  % in server buffers, or wait for pars.nbpm_ave new ones)
  if ((pars.measMethod==1)||pars.useModel)
    if (~pars.useModel)
      stat=FlHwUpdate('wait',pars.nbpm_ave+0,pars.nbpm_ave/FL.accRate+120);
      if (stat{1}~=1)
        if FL.SimMode~=2
          errdlg(stat{2},'Error')
        else
          warning('Lucretia:MeasureDispersionOnBPMs:newBPMData',stat{2})
        end
        return
      end
      if (isfield(pars,'doqcut')&&pars.doqcut&&...
          isfield(pars,'qcut')&&pars.qcut&&isfield(pars,'qmeas'))
        [stat,bpmdata]=FlHwUpdate('bpmave',pars.nbpm_ave,0,[pars.qcut,3,pars.qmeas,0]);
      else
        [stat,bpmdata]=FlHwUpdate('bpmave',pars.nbpm_ave);
      end
      if (stat{1}~=1),return,end
      [stat,instdata]=FlTrackThru(1,length(BEAMLINE),bpmdata);
    else
      [stat,B,instdata]=TrackThru(1,length(BEAMLINE),FL.SimBeam{2},1,1);
    end
    if (stat{1}~=1),return,end
    % Get bpm readings for BPMs we are interested in
    data.xbpms_all(count,:)=[instdata{1}.x];
    data.ybpms_all(count,:)=[instdata{1}.y];
    if (isfield(instdata{1},'xrms'))
      data.xbpms_all_rms(count,:)=[instdata{1}.xrms];
      data.ybpms_all_rms(count,:)=[instdata{1}.yrms];
    else
      data.xbpms_all_rms(count,:)=zeros(1,length(instdata{1}));
      data.ybpms_all_rms(count,:)=data.xbpms_all_rms(count,:);
    end
    data.bpminds=[instdata{1}.Index];
  else
    [stat bpmdata]=FlHwUpdate('readbuffer',pars.nbpm_ave);
    if (stat{1}~=1),return,end
    data.xbpms_all_e=[data.xbpms_all_e;bpmdata(:,2:3:end)];
    data.ybpms_all_e=[data.ybpms_all_e;bpmdata(:,3:3:end)];
    data.ebpms=[data.ebpms;ones(pars.nbpm_ave,1).*Model.DeltaVec(count)];
    data.xbpms_all(count,:)=mean(bpmdata(:,2:3:end));
    data.ybpms_all(count,:)=mean(bpmdata(:,3:3:end));
    data.xbpms_all_rms(count,:)=std(bpmdata(:,2:3:end));
    data.ybpms_all_rms(count,:)=std(bpmdata(:,2:3:end));
    data.bpminds=cellfun(@(x) x.Index,INSTR);
  end
  % Do wirescan position measurement if requested
  if (pars.wiremeas)
    wdata=wiremeasGui;
    wdata(isnan(wdata))=0;
    data.xwdata(count,:)=wdata(1,:)*1e-3;data.xwdata_rms(count,:)=wdata(2,:)*1e-3;
    data.ywdata(count,:)=wdata(3,:)*1e-3;data.ywdata_rms(count,:)=wdata(4,:)*1e-3;
    data.xpip(count)=wdata(5,1)*1e-3;data.xpip_rms(count)=wdata(5,3)*1e-3;
    data.ypip(count)=wdata(5,2)*1e-3;data.ypip_rms(count)=wdata(5,4)*1e-3;
  end
  % Handle abort request
  if (isfield(FL,'abort_extDispersion'))
    FL=rmfield(FL,'abort_extDispersion');
    abort=strcmp(questdlg('Abort dispersion measurement?','MeasureDispersionOnBPMs'),'Yes');
    if (abort)
      MeasDispEvent(2,h)
      break
    end
  end
end

% turn DR RF-ramp OFF
if (verbose),MeasDispEvent(3,h),end
stat=changeP(0,'off');
if (stat{1}~=1)
  stat{2}='MeasureDispersionOnBPMs: DR RF-ramp OFF failed';
  errordlg('DR RF-ramp OFF failed','Error')
  return
end
if (abort)
  stat{1}=0;stat{2}='MeasureDispersionOnBPMs aborted by user';
  return
end
if (verbose),MeasDispEvent(4,h),end

% Get wirescanner dispersion vals if requested
data.xwdisp=[];data.xwdisp_rms=[];
data.ywdisp=[];data.ywdisp_rms=[];
data.xpipdisp=[];data.xpipdisp_rms=[];
data.ypipdisp=[];data.ypipdisp_rms=[];
if (pars.wiremeas)
  for iwire=1:5
    if (any(~data.xwdata_rms))
      [q,dq]=noplot_polyfit(Model.DeltaVec',data.xwdata(:,iwire),1,1);
    else
      [q,dq]=noplot_polyfit(Model.DeltaVec',data.xwdata(:,iwire),data.xwdata_rms(:,iwire),1);
    end
    data.xwdisp(iwire)=q(2);
    data.xwdisp_rms=dq(2);
    if (any(~data.ywdata_rms))
      [q,dq]=noplot_polyfit(Model.DeltaVec',data.ywdata(:,iwire),1,1);
    else
      [q,dq]=noplot_polyfit(Model.DeltaVec',data.ywdata(:,iwire),data.ywdata_rms(:,iwire),1);
    end
    data.ywdisp(iwire)=q(2);
    data.ywdisp_rms=dq(2);
  end
  if (any(~data.xpip_rms))
    [q,dq]=noplot_polyfit(Model.DeltaVec,data.xpip,1,1);
  else
    [q,dq]=noplot_polyfit(Model.DeltaVec,data.xpip,data.xpip_rms,1);
  end
  data.xpipdisp=q(2);
  data.xpipdisp_rms=dq(2);
  if (any(~data.ypip_rms))
    [q,dq]=noplot_polyfit(Model.DeltaVec,data.ypip,1,1);
  else
    [q,dq]=noplot_polyfit(Model.DeltaVec,data.ypip,data.ypip_rms,1);
  end
  data.ypipdisp=q(2);
  data.ypipdisp_rms=dq(2);
end

% Get orbit slopes
fitOrder=1;
data.xq=[];data.xdq=[];
data.yq=[];data.ydq=[];
if (pars.measMethod==1)
  bpmdim=size(data.xbpms_all);
  for count=1:bpmdim(2)
    if (~any(data.xbpms_all_rms(:,count)))
      [data.xq(:,count),data.xdq(:,count),data.xchisq(count),data.xqrms(count)]=...
        noplot_polyfit(Model.DeltaVec',data.xbpms_all(:,count),0,fitOrder);
    else
      [data.xq(:,count),data.xdq(:,count),data.xchisq(count),data.xqrms(count)]=...
        noplot_polyfit(Model.DeltaVec',data.xbpms_all(:,count),data.xbpms_all_rms(:,count),fitOrder);
    end
    if (~any(data.ybpms_all_rms(:,count)))
      [data.yq(:,count),data.ydq(:,count),data.ychisq(count),data.yqrms(count)]=...
        noplot_polyfit(Model.DeltaVec',data.ybpms_all(:,count),0,fitOrder);
    else
      [data.yq(:,count),data.ydq(:,count),data.ychisq(count),data.yqrms(count)]=...
        noplot_polyfit(Model.DeltaVec',data.ybpms_all(:,count),data.ybpms_all_rms(:,count),fitOrder);
    end
  end
  % fit the dispersions [etax;etapx;etay;etapy] 
  [data.etaVector,data.etaVector_err,mse data.cov]=lscov(Model.EtaMatrix,...
    [data.xq(2,Model.EtaBPMs_instdata_index)';data.yq(2,Model.EtaBPMs_instdata_index)'],...
    1./[data.xdq(2,Model.EtaBPMs_instdata_index)'.^2;data.ydq(2,Model.EtaBPMs_instdata_index)'.^2]); 
  data.etaVector_err=data.etaVector_err*sqrt(1/mse);
  data.cov=data.cov*(1/mse);
  data.xbpms=data.xbpms_all(:,Model.EtaBPMs_instdata_index);
  data.ybpms=data.xbpms_all(:,Model.EtaBPMs_instdata_index);
else
  % Do singular value decomposition
  [Ux,Sx,Vtx]=svd(data.xbpms_all_e);Vx=Vtx';
  [Uy,Sy,Vty]=svd(data.ybpms_all_e);Vy=Vty';
  % Correlate modes with Energy
  for imode=1:10
    [r,p]=corrcoef(data.ebpms,data.xbpms_all_e*Vx(imode,:)');
    if (~isempty(find(p<0.1,1)))
      rcorx(imode)=r(1,2);
    else
      rcorx(imode)=0;
    end % if any significant correlations
    [r,p]=corrcoef(data.ebpms,data.ybpms_all_e*Vy(imode,:)');
    if (~isempty(find(p<0.1,1)))
      rcory(imode)=r(1,2);
    else
      rcory(imode)=0;
    end % if any significant correlations
  end % for imode
  if (~any(rcorx)||~any(rcory))
    stat{1}=-1;
    stat{2}='No significant correlations found for both x and y';
    return
  end % if no correlations found, return
  % Get energy contribution to bpm readings using most correlated mode
  Sx_new=zeros(size(Sx));
  Sy_new=zeros(size(Sy));
  [val,xind]=max(rcorx);
  [val,yind]=max(rcory);
  Sx_new(xind,xind)=Sx(xind,xind);
  Sy_new(yind,yind)=Sy(yind,yind);
  xbpm=Ux*Sx_new*Vx;
  ybpm=Uy*Sy_new*Vy;
  % form mean and rms values per ramp value
  ic=0;
  bs=size(xbpm);
  for ipulse=1:pars.nbpm_ave:bs(1)
    ic=ic+1;
    data.xbpm_e(ic,:)=mean(xbpm(ipulse:ipulse+pars.nbpm_ave-1,:));
    data.xbpm_e_rms(ic,:)=std(xbpm(ipulse:ipulse+pars.nbpm_ave-1,:));
    data.ybpm_e(ic,:)=mean(ybpm(ipulse:ipulse+pars.nbpm_ave-1,:));
    data.ybpm_e_rms(ic,:)=std(ybpm(ipulse:ipulse+pars.nbpm_ave-1,:));
  end
  % Fit dispersion curves
  bpmsize=size(data.xbpm_e);
  for ibpm=1:bpmsize(2)
    [data.xq(:,ibpm),data.xdq(:,ibpm),data.xchisq(ibpm),data.xqrms(ibpm)]=...
      noplot_polyfit(Model.DeltaVec',data.xbpm_e(:,ibpm)',data.xbpm_e_rms(:,ibpm)',1);
    [data.yq(:,ibpm),data.ydq(:,ibpm),data.ychisq(ibpm),data.yqrms(ibpm)]=...
      noplot_polyfit(Model.DeltaVec',data.ybpm_e(:,ibpm)',data.ybpm_e_rms(:,ibpm)',1);
  end % for ibpm
  % fit the dispersions [etax;etapx;etay;etapy] 
  [data.etaVector data.etaVector_err mse data.cov]=lscov(Model.EtaMatrix,...
    [data.xq(2,Model.EtaBPMs_INSTR_index)';data.yq(2,Model.EtaBPMs_INSTR_index)'],...
    1./[data.xdq(2,Model.EtaBPMs_INSTR_index)'.^2;data.ydq(2,Model.EtaBPMs_INSTR_index).^2']) ; 
  data.etaVector_err=data.etaVector_err*sqrt(1/mse);
  data.cov=data.cov*(1/mse);
  data.xbpms=data.xbpms_all(:,Model.EtaBPMs_INSTR_index);
  data.ybpms=data.xbpms_all(:,Model.EtaBPMs_INSTR_index);
end

function MeasDispEvent(opcode,varargin)
switch opcode
  case 0 % measurement begin
    h=varargin{1};
    msg={'Dispersion measurement started'; ...
         'Turning DR RF-ramp ON'};
    set(h,'String',msg) % start new message buffer
  case 1 % measurement step info
    h=varargin{1};
    msg=varargin{2};
    s=get(h,'String');
    s=[s;msg];
    set(h,'String',s)
  case 2 % measurement aborted by user
    h=varargin{1};
    msg={'Dispersion measurement aborted by user'};
    s=get(h,'String');
    s=[s;msg];
    set(h,'String',s)
  case 3 % DR RF-ramp OFF
    h=varargin{1};
    msg={'Turning DR RF-ramp OFF'};
    s=get(h,'String');
    s=[s;msg];
    set(h,'String',s)
  case 4 % measurement end
    h=varargin{1};
    msg={'Dispersion measurement finished'};
    s=get(h,'String');
    s=[s;msg];
    set(h,'String',s)
end
drawnow('expose')

function [stat, dataout] = extDispersion_run(cmd,newpars)
% [stat dataout] = extDispersion_run(cmd,newpars)
% Commands for running extraction line dispersion measurement and
% correction routines

% ------------------------------------------------------------------------------
% 03-Mar-2011, M. Woodley
%    After every successful dispersion measurement (or remeasurement),
%    propagate fitted dispersion to all BEAMLINE elements and store the
%    propagated values in "Measure" button's UserData area
% 07-Nov-2010, M. Woodley
%    Assume that target point and default set of BPMs exist in current BEAMLINE,
%    but indices might have changed since data was SAVEd
% ------------------------------------------------------------------------------

global FL BEAMLINE INSTR
persistent data Model pars

stat{1}=1;dataout=[];
if (~exist('cmd','var')),cmd='init';end

% set default parameters
dr_ramp_max=10; % kHz
if (isempty(pars))
  pars.dononlincor=false;
  pars.alpha_p=2.0e-3;
  pars.doqcut=true;
  pars.qcut=0.5;
  pars.nbpm_ave=10;
  pars.maxknob=10;
  pars.qmeas=1; % 0=ICTDUMP,1=ICT1X
  pars.nstep=5;
  pars.wiremeas=false;
  pars.corset=[1 1 0 0];
  pars.fudge=[1 1 1 1];
  pars.doYLim=[0 0];
  pars.YLim=[-10000 10000 -10000 10000]; % [Ymin(top) Ymax(top) Ymin(botton) Ymax(bottom)]
  pars.docalcknob=false;
  pars.measMethod=1;
  pars.target.name='MQD10X'; % dispersion correction point
  pars.target.ind=findcells(BEAMLINE,'Name',pars.target.name);
  pars.bpmlist={ ...
    'MQF9X' 'MQD10X' 'MQF11X' 'MQD12X' 'MQF13X' 'MQD14X' 'MQF15X' 'MQD16X' ...
    'MQF17X' 'MQD18X' 'MQF19X' 'MQD20X' 'MQF21X' 'MQM16FF' 'MQM15FF' ...
    'MQM14FF' 'MQM13FF' 'MQM12FF' 'MQM11FF' 'MQD10BFF' 'MQD10AFF' 'MQF9BFF' ...
    'MSF6FF' 'MQF9AFF' 'MQD8FF' 'MQF7FF' ...
  }; % non-dispersive BPMs (except MFB1FF and MFB2FF)
  pars.bpmind=cellfun(@(x) findcells(BEAMLINE,'Name',x),pars.bpmlist);
  pars.dr_ramp=-3:3; % kHz
  pars.useModel=false; % ???
  % restore most recently saved parameters, if there are any
  % NOTE: BEAMLINE indices might have changed since parameters were saved!
  FlDataStore('extDispersion');
  if (isfield(FL,'DataStore')&&isfield(FL.DataStore,'extDispersion'))
    fn=fieldnames(FL.DataStore.extDispersion);
    for ifn=1:length(fn)
      if (~isempty(regexp(fn{ifn},'^pars','once')))
        fname=regexprep(fn{ifn},'^pars','pars.');
       %fprintf(1,'%s\n',[fname,'=FL.DataStore.extDispersion.',fn{ifn},';']);
        eval([fname,'=FL.DataStore.extDispersion.',fn{ifn},';']);
      end
    end
    if (~iscell(pars.bpmlist))
      eval(['pars.bpmlist={',strrep(pars.bpmlist,'#',''''),'};'])
    end
    pars.bpmind=cellfun(@(x) findcells(BEAMLINE,'Name',x),pars.bpmlist);
    if (isfield(pars,'targetname'))
      pars.target.name=pars.targetname;
      pars=rmfield(pars,'targetname');
      pars=rmfield(pars,'targetind');
    end
    pars.target.ind=findcells(BEAMLINE,'Name',pars.target.name);
    if (any(abs(pars.dr_ramp)>dr_ramp_max))
      helpdlg('Converting saved DR RF-ramp values to kHz','extDispersion')
      pars.dr_ramp=1e-3*pars.dr_ramp;
    end
  end
end % if empty pars

% design knob generation
if (isequal(lower(cmd),'getpars0'))
  dataout=pars;
  return
end

% first call or init request
if (isempty(Model)||isequal(lower(cmd),'init'))
  Model=FL.SimModel;
  Model=init_setup(Model,pars);
end

switch lower(cmd)
  case 'reinit'
    Model=FL.SimModel;
    Model=init_setup(Model,pars);
  case 'restore'
    stat=FlDataStore('extDispersion');
    if ((stat{1}~=1)||~isfield(FL,'DataStore')||~isfield(FL.DataStore,'extDispersion'))
      errordlg('No data to restore','Disp Data Restore Error')      
      return
    end
    fn=fieldnames(FL.DataStore.extDispersion);
    for ifn=1:length(fn)
      fname=regexprep(fn{ifn},'^data','data.');
      fname=regexprep(fname,'^pars','pars.');
      eval([fname,'=FL.DataStore.extDispersion.',fn{ifn},';']);
    end
    if (~iscell(pars.bpmlist))
      eval(['pars.bpmlist={',strrep(pars.bpmlist,'#',''''),'};'])
    end
    pars.bpmind=cellfun(@(x) findcells(BEAMLINE,'Name',x),pars.bpmlist);
    pars.target.ind=findcells(BEAMLINE,'Name',pars.target.name);
    
    % Write fitted dispersion values to each BEAMLINE index
    % Also write INSTR for WIRE and PROF classes
    dispfit(1:length(BEAMLINE),data,'writemodel');
    
  case 'publish'
    bpmlist=cell2mat(strcat('#',pars.bpmlist,'#,'));bpmlist(end)=[];
    if (~isempty(data)&&isfield(data,'etaVector')&&isfield(Model,'DeltaVec'))
      
      % Write fitted dispersion values to each BEAMLINE index
      % Also write INSTR for WIRE and PROF classes
      dispfit(1:length(BEAMLINE),data,'writemodel');
      
      % publish parameters + data
      data.etaVec=Model.DeltaVec;
      if (~isfield(data,'calcknob')),data.calcknob=[];end
      if (~isfield(data,'xwdisp'))
        data.xwdisp=0;data.xwdisp_rms=0;
        data.ywdisp=0;data.ywdisp_rms=0;
      end
      if (~isfield(data,'dispy_scan'))
        data.dispx_scan=[];data.dispy_scan=[];
      end
      stat=FlDataStore('extDispersion',...
        'parsdononlincor',pars.dononlincor,...
        'parsalpha_p',pars.alpha_p,...
        'parsdoqcut',pars.doqcut,...
        'parsqcut',pars.qcut,...
        'parsnbpm_ave',pars.nbpm_ave,...
        'parsmaxknob',pars.maxknob,...
        'parsdr_ramp',pars.dr_ramp,...
        'parsqmeas',pars.qmeas,...
        'parsnstep',pars.nstep,...
        'parswiremeas',pars.wiremeas,...
        'parscorset',pars.corset,...
        'parsfudge',pars.fudge,...
        'parsdocalcknob',pars.docalcknob,...
        'parsmeasMethod',pars.measMethod,...
        'parstargetname',pars.target.name,'parstargetind',pars.target.ind,...
        'parsbpmlist',bpmlist,'parsbpmind',pars.bpmind,...
        'dataetaVector',data.etaVector,'dataetaVector_err',data.etaVector_err,...
        'dataxbpms_all',data.xbpms_all,'dataxbpms_all_rms',data.xbpms_all_rms,...
        'dataybpms_all',data.ybpms_all,'dataybpms_all_rms',data.ybpms_all_rms,...
        'databpminds',data.bpminds,...
        'dataxq',data.xq,'dataxdq',data.xdq,...
        'datayq',data.yq,'dataydq',data.ydq,...
        'datacalcknob',data.calcknob,...
        'dataxwdisp',data.xwdisp,'dataxwdisp_rms',data.xwdisp_rms,...
        'dataywdisp',data.ywdisp,'dataywdisp_rms',data.ywdisp_rms,...
        'dataxbpm_e',data.xbpm_e,'dataxbpm_e_rms',data.xbpm_e_rms,...
        'dataybpm_e',data.ybpm_e,'dataybpm_e_rms',data.ybpm_e_rms,...
        'dataxpipdisp',data.xpipdisp,'dataxpipdisp_rms',data.xpipdisp_rms,...
        'dataypipdisp',data.ypipdisp,'dataypipdisp_rms',data.ypipdisp_rms,...
        'datadispx_scan',data.dispx_scan,...
        'datadispy_scan',data.dispy_scan,...
        'datacov',data.cov);
      % publish etaVector to INSTR array
      % Indicate datetime of measurement in FL structure
      FL.lastDispMeas=clock;
      if (isfield(data,'xq'))
        for ii=1:length(INSTR)
          wind=find(data.bpminds==INSTR{ii}.Index,1);
          if (~isempty(wind))
            INSTR{ii}.dispref=[data.xq(2,wind),0,data.yq(2,wind),0];
            INSTR{ii}.dispreferr=[data.xdq(2,wind),0,data.ydq(2,wind),0];
          end
        end
      end
      if (isfield(data,'xwdisp')&&(length(data.xwdisp)==5))
        for iw=1:5
          iele=findcells(BEAMLINE,'Name',['MW',num2str(iw-1),'X']);
          iind=findcells(INSTR,'Index',iele);
          INSTR{iind}.dispref=[data.xwdisp(iw),0,data.ywdisp(iw),0];
          INSTR{iind}.dispreferr=[data.xwdisp_rms(iw),0,data.ywdisp_rms(iw)];
        end
      end
      if (isfield(data,'xpipdisp')&&~isempty(data.xpipdisp))
        iele=findcells(BEAMLINE,'Name','MW1IP');
        iind=findcells(INSTR,'Index',iele);
        INSTR{iind}.dispref=[data.xpipdisp,0,data.ypipdisp,0];
        INSTR{iind}.dispreferr=[data.xpipdisp_rms,0,data.ypipdisp_rms,0];
      end
    else
      % publish parameters only
      stat=FlDataStore('extDispersion',...
        'parsdononlincor','pars.dononlincor',...
        'parsalpha_p',pars.alpha_p,...
        'parsdoqcut',pars.doqcut,...
        'parsqcut',pars.qcut,...
        'parsnbpm_ave',pars.nbpm_ave,...
        'parsmaxknob',pars.maxknob,...
        'parsdr_ramp',pars.dr_ramp,...
        'parsqmeas',pars.qmeas,...
        'parsnstep',pars.nstep,...
        'parswiremeas',pars.wiremeas,...
        'parscorset',pars.corset,...
        'parsfudge',pars.fudge,...
        'parsdocalcknob',pars.docalcknob,...
        'parsmeasMethod',pars.measMethod,...
        'parstargetname',pars.target.name,'parstargetind',pars.target.ind,...
        'parsbpmlist',bpmlist,'parsbpmind',pars.bpmind);
    end % if ~empty data
    if (stat{1}~=1),return,end
  case {'measure','remeasure'}
    remeas=strcmpi(cmd,'remeasure');
    [stat,data]=MeasureDispersionOnBPMs(Model,pars,data,remeas);
    if ((stat{1}==1)&& ...
        isfield(FL,'Gui')&& ...
        isfield(FL.Gui,'extDispersion')&& ...
        isfield(FL.Gui.extDispersion,'pushbutton1'))
      [stat,Df,dDf]=dispfit(1:length(BEAMLINE),data); % propagate everywhere
      if (stat{1}==1)
        h=FL.Gui.extDispersion.pushbutton1; % save in "Measure" button's UserData
        set(h,'UserData',{[1,length(BEAMLINE)],Df,dDf})
      end
    end
  case 'verify'
    if pars.dononlincor
      return
    end
    if (pars.docalcknob)
      if (~isfield(data.calcknob,'EtaX'))
        stat{1}=-1;stat{2}='No knob calibrations have been made';
        return
      end
      stat=extDispersion_verify(pars.corset,pars.fudge,data.etaVector, ...
        data.calcknob.EtaX,data.calcknob.EtaPX,data.calcknob.EtaY,data.calcknob.EtaPY);
    else
      stat=extDispersion_verify(pars.corset,pars.fudge,data.etaVector, ...
        Model.ext.EtaX,Model.ext.EtaPX,Model.ext.EtaY,Model.ext.EtaPY);
    end
  case 'correct'
    if (pars.docalcknob)
      if (~isfield(data.calcknob,'EtaX'))
        stat{1}=-1;stat{2}='No knob calculation made';
        return
      end
      if (pars.corset(1)),IncrementMultiKnob('data.calcknob.EtaX',-data.etaVector(1)*pars.fudge(1),FL.SimMode~=2);end
      if (pars.corset(2)),IncrementMultiKnob('data.calcknob.EtaPX',-data.etaVector(2)*pars.fudge(2),FL.SimMode~=2);end
      if (pars.corset(3)),IncrementMultiKnob('data.calcknob.EtaY',-data.etaVector(3)*pars.fudge(3),FL.SimMode~=2);end
      if (pars.corset(4)),IncrementMultiKnob('data.calcknob.EtaPY',-data.etaVector(4)*pars.fudge(4),FL.SimMode~=2);end
      if (isfield(FL.Gui,'extDispersion'))
        set(FL.Gui.extDispersion.text17,'String',num2str(data.calcknob.EtaX.Value))
        set(FL.Gui.extDispersion.text18,'String',num2str(data.calcknob.EtaPX.Value))
        set(FL.Gui.extDispersion.text19,'String',num2str(data.calcknob.EtaY.Value))
        set(FL.Gui.extDispersion.text20,'String',num2str(data.calcknob.EtaPY.Value))
      end
    else
      if pars.dononlincor
        nonlinDispCor(data,pars.fudge.*pars.corset);
      else
        if (pars.corset(1)),IncrementMultiKnob('Model.ext.EtaX',-data.etaVector(1)*pars.fudge(1),FL.SimMode~=2);end
        if (pars.corset(2)),IncrementMultiKnob('Model.ext.EtaPX',-data.etaVector(2)*pars.fudge(2),FL.SimMode~=2);end
        if (pars.corset(3)),IncrementMultiKnob('Model.ext.EtaY',-data.etaVector(3)*pars.fudge(3),FL.SimMode~=2);end
        if (pars.corset(4)),IncrementMultiKnob('Model.ext.EtaPY',-data.etaVector(4)*pars.fudge(4),FL.SimMode~=2);end
        if (isfield(FL.Gui,'extDispersion'))
          set(FL.Gui.extDispersion.text17,'String',num2str(Model.ext.EtaX.Value))
          set(FL.Gui.extDispersion.text18,'String',num2str(Model.ext.EtaPX.Value))
          set(FL.Gui.extDispersion.text19,'String',num2str(Model.ext.EtaY.Value))
          set(FL.Gui.extDispersion.text20,'String',num2str(Model.ext.EtaPY.Value))
        end
      end
    end
  case 'getknobs'
    if (pars.docalcknob&&isfield(data,'calcknob')&&~isempty(data.calcknob))
      dataout=data.calcknob;
    else
      dataout=Model.ext;
    end
  case 'setknobs'
    data.calcknob=newpars;
  case 'reset'
    if (pars.corset(1)),SetMultiKnob('Model.ext.EtaX',0,FL.SimMode~=2);end
    if (pars.corset(2)),SetMultiKnob('Model.ext.EtaPX',0,FL.SimMode~=2);end
    if (pars.corset(3)),SetMultiKnob('Model.ext.EtaY',0,FL.SimMode~=2);end
    if (pars.corset(4)),SetMultiKnob('Model.ext.EtaPY',0,FL.SimMode~=2);end
    if (isfield(FL.Gui,'extDispersion'))
      set(FL.Gui.extDispersion.text17,'String',num2str(Model.ext.EtaX.Value))
      set(FL.Gui.extDispersion.text18,'String',num2str(Model.ext.EtaPX.Value))
      set(FL.Gui.extDispersion.text19,'String',num2str(Model.ext.EtaY.Value))
      set(FL.Gui.extDispersion.text20,'String',num2str(Model.ext.EtaPY.Value))
    end
  case 'getpars'
    dataout=pars;
  case 'getdata'
    data.etaVec=Model.DeltaVec;
    dataout=data;
  case 'setpars'
    if (~exist('newpars','var')||~isstruct(newpars))
      stat{1}=-1;stat{2}='Incorrect arguments';return
    end % if newpars wrong
    names=fieldnames(newpars);
    for iname=1:length(names)
      pars.(names{iname})=newpars.(names{iname});
    end % for iname
    Model=init_setup(Model,pars);
  case 'getbpms'
    dataout=Model.EtaBPMs;
  case 'recalcknobs'
    %msg='Model knobs are only generated at FS startup, using the design optics';
    %helpdlg(msg,'extDispersion')
    stat=MakeGlobalDispersionKnobs;
  case 'response'
    doFull=false;
    if (doFull)
      helpdlg('NOTE: using extDispersion_responseMatrixFull', ...
        'extDispersion: Cal Knobs'); %#ok<*UNRCH>
      [stat,allData]=extDispersion_responseMatrixFull(Model,pars);
      if (stat{1}==1)
        fname=['extDispersion_responseMatrixFull_',datestr(now,'yymmmdd_HHMM')];
        eval(['save ',fname,' allData'])
        helpdlg(['Saved ',fullfile(pwd,fname)],'extDispersion: Cal Knobs');
      else
        errordlg(stat{2},'responseMatrixFull')
      end
    else
      xonly=true; % horizontal only
      stat=extDispersion_responseMatrix(xonly);
      if (stat{1}~=1)
        errordlg(stat{2},'responseMatrix')
      end
    end
end % switch cmd

function Model=init_setup(Model,pars)
global BEAMLINE INSTR FL

% Define EXT end-point
Model.ext.endInd=findcells(BEAMLINE,'Name','BEGFF');

% find the beginning and end of the section with misaligned quads, and the
% beginning and end of the BH1-2 section:
Model.ext.msim=Model.extStart;
Model.ext.begff=findcells(BEAMLINE,'Name','BEGFF');
Model.ext.bh12beg=findcells(BEAMLINE,'Name','BH1XA');
Model.ext.bh12end=findcells(BEAMLINE,'Name','BH2XB')+1;

% now construct the data structure: first the vector of deltas
Model.DeltaVec=-(pars.dr_ramp/714e3)/pars.alpha_p; % RF frequency values in kHz
Model.RampVec=pars.dr_ramp;
Model.EtaBPMs=pars.bpmind;
Nbpm=length(Model.EtaBPMs);

% Get instdata indicies for EtaBPMs
[stat,instdata]=FlTrackThru(1,length(BEAMLINE));
Model.EtaBPMs_instdata_index=find(ismember([instdata{1}.Index],pars.bpmind));
Model.EtaBPMs_INSTR_index=ismember(cellfun(@(x) x.Index,INSTR),pars.bpmind);

% Get the matrix which transforms dispersion into dPos/dDelta slope
EtaMatrix=zeros(2*Nbpm,4);
EtaMat6=zeros(2*Nbpm,1);
ida=pars.target.ind; % target BPM index
for n=1:Nbpm
  idb=Model.EtaBPMs(n);
  if (idb>=ida)
    [stat,R]=RmatAtoB(ida,idb);
  else
    [stat,R]=RmatAtoB(idb,ida);
    R=inv(R);
  end
  EtaMatrix(n,1:4)=R(1,1:4);
  EtaMat6(n)=R(1,6);
  EtaMatrix(Nbpm+n,1:4)=R(3,1:4);
  EtaMat6(Nbpm+n)=R(3,6);
end
Model.EtaMatrix=EtaMatrix;
Model.EtaMat6=EtaMat6;
Model.LastElem=Model.EtaBPMs(end);

% get precalculated design dispersion knobs
MakeGlobalDispersionKnobs;
[stat,EtaX,EtaPX,EtaY,EtaPY,Sum]=MakeGlobalDispersionKnobs;
Model.ext.EtaX=EtaX;
Model.ext.EtaPX=EtaPX;
Model.ext.EtaY=EtaY;
Model.ext.EtaPY=EtaPY;
Model.ext.Sum=Sum;

% as an added safety measure, set low and high PS.Ampl limits for dispersion
% correction magnets ... CYA!
if (FL.SimMode~=2)
  mags={'QF1X','QF6X','QS1X','QS2X','ZV5X','ZV6X','ZV7X'};
  magtype=[1,1,2,2,3,3,3];
  maglow=[0,0,-5,-5,-10,-10,-10];
  maghigh=[100,100,5,5,10,10,10];
  for n=1:length(mags)
    id=findcells(BEAMLINE,'Name',mags{n});
    ip=BEAMLINE{id(1)}.PS;
    conv=FL.HwInfo.PS(ip).conv;
    switch magtype(n)
      case 1
        B=BEAMLINE{id(1)}.B; % nominal
        Bmin=interp1(conv(1,:),conv(2,:),maglow(n));
        Bmax=interp1(conv(1,:),conv(2,:),maghigh(n));
        lowval=Bmin/abs(B);
        highval=Bmax/abs(B);
      case 2
        Bmin=interp1(-conv(1,:),-conv(2,:),maglow(n));
        Bmax=interp1(conv(1,:),conv(2,:),maghigh(n));
        lowval=min(Bmin,Bmax);
        highval=max(Bmin,Bmax);
      case 3
        Bmin=conv*maglow(n);
        Bmax=conv*maghigh(n);
        lowval=min(Bmin,Bmax);
        highval=max(Bmin,Bmax);
    end
    FL.HwInfo.PS(ip).low=lowval;   % minimum value of PS(ip).Ampl
    FL.HwInfo.PS(ip).high=highval; % maximum value of PS(ip).Ampl
    %fprintf(1,'%s : low= %.4f , high= %.4f\n',mags{n},lowval,highval);
  end
end

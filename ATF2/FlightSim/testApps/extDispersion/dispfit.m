function [stat,Df,dDf]=dispfit(bind,data,cmd)
global BEAMLINE INSTR
% Propagate dispersion measured at fit point through specified region

% use fit errors and covariance matrix to get errors on propagated values
% (include coupling)

[stat,p]=extDispersion_run('GetPars');
id0=p.target.ind;
D0=data.etaVector;
if (isfield(data,'cov'))
  cov=data.cov;
else
  cov=diag(data.etaVector_err.^2); % uncorrelated errors
end

id=bind;
Nelem=length(id);
Df=zeros(4,Nelem);
dDf=zeros(4,Nelem);
% disp [Dx D'x Dy D'y]
for n=1:Nelem
  if (id(n)<id0)
    [stat,R]=RmatAtoB(id(n),id0);
    R=inv(R);
  else
    [stat,R]=RmatAtoB(id0-1,id(n)-1);
  end
  D=R(1:4,6);
  R=R(1:4,1:4);
  Df(:,n)=R*D0+D;
  for m=1:4
    dDf(m,n)=sqrt(R(m,:)*cov*R(m,:)');
  end
  if exist('cmd','var') && isequal(cmd,'writemodel')
    BEAMLINE{id(n)}.MeasData.disp=Df(:,n);
    BEAMLINE{id(n)}.MeasData.dispErr=dDf(:,n);
    if strcmp(BEAMLINE{id(n)}.Class,'WIRE') || strcmp(BEAMLINE{id(n)}.Class,'PROF')
      instid=findcells(INSTR,'Index',id(n));
      if ~isempty(instid)
        INSTR{instid}.dispref=Df(:,n);
        INSTR{instid}.dispreferr=dDf(:,n);
      end
    end
  end
end
function [stat output] = extdypknob(cmd,val)
global PS BEAMLINE FL
persistent scale bval
stat{1}=1; output=[];
if isempty(bval); bval=0; end;
switch lower(cmd)
  case 'calibrate'
    bcal=linspace(-5e-3,5e-3,20);
    zv5x=findcells(BEAMLINE,'Name','ZV5X'); zv5ps=BEAMLINE{zv5x}.PS;
    zv6x=findcells(BEAMLINE,'Name','ZV6X'); zv6ps=BEAMLINE{zv6x}.PS;
    zv7x=findcells(BEAMLINE,'Name','ZV7X'); zv7ps=BEAMLINE{zv7x}.PS;
    psinit=[PS(zv5ps).Ampl PS(zv6ps).Ampl PS(zv7ps).Ampl];
    for ibump=1:length(bcal)
      stat = corbump3(bcal(ibump),'ZV5X','ZV6X','ZV7X',true); if stat{1}~=1; return; end;
      stat=extDispersion_run('Measure'); if stat{1}~=1; return; end;
      [stat data]=extDispersion_run('GetData');
      exteta(:,ibump)=data.etaVector;
      PS(zv5ps).SetPt=psinit(1); PS(zv6ps).SetPt=psinit(2); PS(zv7ps).SetPt=psinit(3);
      PSTrim([zv5ps zv6ps zv7ps],FL.SimMode~=2);
    end
    for idim=1:4
      P=polyfit(bcal,exteta(idim,:),1);
      output{1}(idim)=P(1);
    end
    output{2}=exteta;
    scale=output{1}(4);
  case 'increment'
    if isempty(scale); scale=3.16; end;
    stat = corbump3(val/scale,'ZV5X','ZV6X','ZV7X',true);
    bval=bval+val/scale;
  case 'getval'
    output=bval;
  otherwise
    error('unkown command')
end
function ret = feedbackFF1(Model,command)
global BEAMLINE PS VERB
persistent R BpmHan PsList PsLast bpmData initKick fbEleLast

ret=[];

% restore functionality
if isequal(Model,'get_restore')
  ret={R BpmHan PsList PsLast bpmData initKick fbEleLast}; return;
elseif isequal(Model,'set_restore')
  [R BpmHan PsList PsLast bpmData initKick fbEleLast]=deal(command{:}); return;
end % if restore

PsThis=[PS(Model.PSind.Quad).Ampl];
fbEleThis=[Model.fbBpmsToUseX Model.fbBpmsToUse Model.fbCorToUseX Model.fbCorToUse Model.fbWeight];

% Filter values
K= -1;
V= [ones(1,4).*0.3285;
    ones(1,4).*0.0091] ;
% K= -1;
% V= [ones(1,18).*0.552;
%     ones(1,18).*-0.0825] ;
fbWeights=ones(20,1);

% fbWeights([10 20])=10;
% Get BPM/Corrector lists and R matricies
% First time subroutine called or Quad PS changed?
if isempty(R) || isempty(PsLast) || (sum(PsThis==PsLast)~=length(PsThis)) || ~isequal(fbEleThis,fbEleLast)
  fbEleLast=fbEleThis;
  if VERB; disp('Recalculating Response Matrix for steering...'); end;
  BpmList.x=Model.fbBpmsToUseX(1);
  BpmList.y=Model.fbBpmsToUse(1);
%   BpmList.x=[58 85 143 154 169 185 281 373];
%   BpmList.y=[];
%   BpmList.x=[Model.magGroup.Sext.Bpm(1).Ind];
%   BpmList.y=[Model.magGroup.Sext.Bpm(1).Ind];
  BpmHan.x=find(ismember(Model.handles.BPM,BpmList.x));
  BpmHan.y=find(ismember(Model.handles.BPM,BpmList.y));
  if length([BpmList.x BpmList.y]) ~= length([BpmHan.x BpmHan.y]); error('BPM location error!'); end;
  PsList.x=Model.fbCorToUseX(1) ;
  PsList.y=Model.fbCorToUse(1) ;
%   PsList.x=Model.fb.cor.x([6 12]);
%   PsList.y=[];
  R=zeros(length([BpmList.x BpmList.y]),length([PsList.x PsList.y]));
  nKik=0;
  for iKik=[PsList.x PsList.y]
    nKik=nKik+1;
    nBpm=0;
    for iBpm=[BpmList.x BpmList.y]
      nBpm=nBpm+1;
      if iBpm>iKik
        [stat, Rm] = RmatAtoB_cor( iKik, iBpm ); if ~stat{1}; error(stat{2}); end;
      else
        Rm=zeros(6);
      end
      if (nBpm <= length(BpmList.x)) && (nKik <= length(PsList.x))
        R(nBpm,nKik)=Rm(1,2);
      elseif (nBpm > length(BpmList.x)) && (nKik <= length(PsList.x))
        R(nBpm,nKik)=Rm(3,2);
      elseif (nBpm <= length(BpmList.x)) && (nKik > length(PsList.x))      
        R(nBpm,nKik)=Rm(1,4);
      else
        R(nBpm,nKik)=Rm(3,4);
      end % if
    end % iBpm
  end % iKik
  R=R.*(R>0.1);
end % is first call?
PsLast=PsThis;

% get FB BPM readings
fbBpm=zeros(length([BpmHan.x BpmHan.y]),1); nBpm=0; W=ones(length([BpmHan.x BpmHan.y]),1).*1e-20;
for iBpm=[BpmHan.x BpmHan.y]
  nBpm=nBpm+1;
  if nBpm<=length(BpmHan.x)
    fbBpm(nBpm)=Model.bpmData.x(iBpm);
    W(nBpm)=sqrt((Model.Twiss.betax(Model.handles.BPM(iBpm))*2.3585e-9)^2 + BEAMLINE{Model.handles.BPM(iBpm)}.Resolution^2);
%     W(nBpm)=BEAMLINE{Model.handles.BPM(iBpm)}.Resolution;
  else
    fbBpm(nBpm)=Model.bpmData.y(iBpm);
    W(nBpm)=sqrt((Model.Twiss.betay(Model.handles.BPM(iBpm))*1.1792e-11)^2 + BEAMLINE{Model.handles.BPM(iBpm)}.Resolution^2);
%     W(nBpm)=BEAMLINE{Model.handles.BPM(iBpm)}.Resolution;
  end % if
end % for iBpm
% W(end)=W(end).*2;
% if isempty(bpmData)
%   bpmData=zeros(size(fbBpm));
%   initKick=[];
%   for iKik=[PsList.x PsList.y]
%     initKick=[initKick PS(BEAMLINE{iKik}.PS).Ampl] ;
%   end
% end
% bpmData=[bpmData fbBpm];
% for iBpm=1:length([BpmHan.x BpmHan.y])
%   filteredData(:,iBpm) = latcfilt(K,V(:,iBpm),bpmData(iBpm,:)') ;
%   filteredData(:,iBpm) = filteredData(:,iBpm)*fbWeights(iBpm) ;
% end
% setKik=initKick'+lscov(R,-filteredData(end,:)',1./(W.^2));
% setKik=lscov(R,-fbBpm.*Model.fbWeight,1./(W.^2));
setKik=lscov(R,-fbBpm.*Model.fbWeight);
% Apply correction
switch upper(command)
  case 'RESET'
    for iKik=[PsList.x PsList.y]
      PS(BEAMLINE{iKik}.PS).SetPt=0; PSTrim(BEAMLINE{iKik}.PS);
    end % for iKik
  case 'CORRECT'
    nKik=0;
    for iKik=[PsList.x PsList.y]
      nKik=nKik+1;
      PS(BEAMLINE{iKik}.PS).SetPt=PS(BEAMLINE{iKik}.PS).Ampl+setKik(nKik)*Model.fbWeight;
      PSTrim(BEAMLINE{iKik}.PS);
    end % for iKik
  otherwise
    error('Unknown Command!');
end % switch command

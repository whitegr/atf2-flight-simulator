function [IPdat Model] = IP_meas( Beam, Model, dobcor )
% Get IP info

global VERB INSTR FL BEAMLINE GIRDER
persistent smresdata instr_ip qd0bpm iqd0 imqf1 ipbsm wsid instr_ws

% restore functionality
if isequal(Beam,'get_restore')
  IPdat={smresdata}; return;
elseif isequal(Beam,'set_restore')
  smresdata=Model{1}; return;
end % if restore

if ~exist('dobcor','var')
  dobcor=false;
end

nAve=1;

FL.SimBeam{1}=Beam{1}; FL.SimBeam{2}=Beam{2};

% load SM resolution data
if Model.doSMres && isempty(smresdata)
  load SM_resdata data2 data8 data30 data174
  smresdata.res.lc2=data2(:,3).*1e-9; smresdata.res.lc8=data8(:,3).*1e-9;
  smresdata.res.lc30=data30(:,3).*1e-9; smresdata.res.lc174=data174(:,3).*1e-9;
  smresdata.bsize.lc2=data2(:,1).*1e-9; smresdata.bsize.lc8=data8(:,1).*1e-9;
  smresdata.bsize.lc30=data30(:,1).*1e-9; smresdata.bsize.lc174=data174(:,1).*1e-9;
end % if doing SM res modeling- load data

ipData.sigma=zeros(6);ipData.sigma1=0;ipData.sigma2=0;
ipData.x=0; ipData.y=0;
ipData.xBunch=zeros(size(Beam{1}.Bunch.x(1,:)));
ipData.yBunch=zeros(size(Beam{1}.Bunch.x(3,:)));

if isempty(instr_ip) || strcmp(INSTR{instr_ip}.Class,'NOHW')
  iqd0=findcells(BEAMLINE,'Name','MQD0FF');
  qd0bpm=findcells(INSTR,'Index',iqd0);
  ipbsm=findcells(BEAMLINE,'Name','MS1IP');
  instr_ip=findcells(INSTR,'Index',ipbsm);
  for iw=0:4
    wsid(iw+1)=findcells(BEAMLINE,'Name',['MW',num2str(iw),'X']);
    instr_ws(iw+1)=findcells(INSTR,'Index',wsid(iw+1));
  end
  if isempty(instr_ip)
    error('IP_SM not found');
  end % if no IP_SM
  if strcmp(INSTR{instr_ip}.Class,'NOHW')
    INSTR{instr_ip}.Class='PROF';
    INSTR{instr_ip}.Type='SM';
    INSTR{instr_ip}.Res=zeros(1,6);
  end
end

if Model.doGM
  if Model.doJitter
    for iTrack=1:Model.nPulseMeas
      if isfield(Model,'Sm')
        Model.Sm.pulseNum=iTrack;
        Model = myTrack(Beam{1},Model);
        IPdat.scan.rms(iTrack)=sqrt(INSTR{instr_ip}.sigma(3,3));
        IPdat.scan.gaussFit(iTrack)=INSTR{instr_ip}.Data(5);
        IPdat.scan.ip_pos(iTrack,:)=[INSTR{instr_ip}.Data(1) INSTR{instr_ip}.Data(2)];
      else
        Model = myTrack(Beam{2},Model);
      end
    end % for iTrack
    if isfield(Model,'Sm')
      signal=Model.Sm.scan.signal;
      [x,chi2]=fminsearch(@(x) sinFit(x,signal,Model.Sm.scan.error),[(max(signal)-mean(signal)) 4 0.25]);
      signal_fit = x(1)*sin((1:length(signal))/x(2)+2*pi*x(3))+mean(signal);
      M = (max(signal_fit)-min(signal_fit)) / (max(signal_fit) + min(signal_fit));
      IPdat.scan.smPhase = x(3) ;
      IPdat.scan.smSize = (Model.Sm.d/(2*pi)) * sqrt( 2 * log(abs(cos(Model.Sm.theta))/M) ) ;
      IPdat.scan.chi2 = chi2;
      IPdat.scan.signal = signal;
      IPdat.scan.signal_fit = signal_fit;
    end % if sm sim
  elseif Model.doSimJitter
     % Apply Ground Motion
    Model.time=Model.time+(Model.nPulseMeas/Model.repRate)*Model.nMeasIter; gmMove(Model);
    % Converge FB
    for iarea=1:2
      for iPulse=1:Model.nFB
        FL.simBeamID=2; FL.SimModel=Model;
        FlHwUpdate; Model=FL.SimModel;
        Model.bpmData.x=cellfun(@(x) x.Data(1),INSTR); Model.bpmData.y=cellfun(@(x) x.Data(2),INSTR);
        if iarea==1
          feedbackEXT(Model,'Correct');
        else
          feedbackFF1(Model,'CORRECT');
          feedbackFF2(Model,'CORRECT');
        end % if iarea=1
      end % for iPulse
    end % for iarea
  end % if Model.doJitter
end % if Model.doGM
FL.simBeamID=1; FL.SimModel=Model;
FL.SimModel.Measure_ipbcor=dobcor;
FlHwUpdate;
FL.SimModel.Measure_ipbcor=false;
Model=FL.SimModel;  
Model.bpmData.x=cellfun(@(x) x.Data(1),INSTR); Model.bpmData.y=cellfun(@(x) x.Data(2),INSTR);

% IP correlation terms
if dobcor
  IPdat.bcor=INSTR{instr_ip}.bcor;
end

% Get beam info at EXT end point?
if isfield(Model,'extMeas') && Model.extMeas
  [stat,extBeam]=TrackThru(Model.tstart,Model.ffStart.ind,Beam{1},1,1,0);
  IPdat.sigmaEXT=cov(extBeam.Bunch.x');
  % Get x and y IP dispersion
  E_nominal=mean(Beam{2}.Bunch.x(6,:));
  disp_spread=0.5e-2;
  ndisp=10;
  disp=-disp_spread/2:disp_spread/(ndisp-1):disp_spread/2;
  beam1=Beam{2};
  ext_x=zeros(ndisp,1); ext_y=zeros(ndisp,1);
  FL.SimModel=Model;
  for idisp=1:ndisp
    beam1.Bunch.x(6,:)=Beam{2}.Bunch.x(6,:)+(E_nominal*disp(idisp));
    x=0; y=0;
    for iStep=1:nAve
      [stat,dextBeam]=TrackThru(Model.tstart,Model.ffStart.ind,beam1,1,1,0);
      x=mean(dextBeam.Bunch.x(1,:));y=mean(dextBeam.Bunch.x(3,:));
    end
    ext_x(idisp)=x./nAve; ext_y(idisp)=y./nAve;
  end
  q=noplot_polyfit(disp,ext_x,1,1);
  IPdat.ExtDisp_x = abs(q(2));
  q=noplot_polyfit(disp,ext_y,1,1);
  IPdat.ExtDisp_y = abs(q(2));
end % if Model.extMeas
ipData.sigma=INSTR{instr_ip}.sigma;
ipData.x=INSTR{instr_ip}.Data(1);
ipData.y=INSTR{instr_ip}.Data(2);
IPdat.xpos=ipData.x;
IPdat.ypos=ipData.y;
IPdat.bx_sig=sqrt(ipData.sigma(1,1)); IPdat.by_sig=sqrt(ipData.sigma(3,3));
if length(Beam{1}.Bunch.x)>10000
  IPdat.sigy_meas=INSTR{instr_ip}.Data(5);
  IPdat.sigx_meas=INSTR{instr_ip}.Data(4);
  if isequal(Model.ipbsm_method,'rms')
    IPdat.xsize_real=IPdat.bx_sig;
    IPdat.ysize_real=IPdat.by_sig;
    IPdat.xsize_alt=IPdat.sigx_meas;
    IPdat.ysize_alt=IPdat.sigy_meas;
  else
    IPdat.xsize_real=IPdat.sigx_meas;
    IPdat.ysize_real=IPdat.sigy_meas;
    IPdat.xsize_alt=IPdat.bx_sig;
    IPdat.ysize_alt=IPdat.by_sig;
  end % if rms method
else
  IPdat.xsize_real=IPdat.bx_sig;
  IPdat.ysize_real=IPdat.by_sig;
end % if length>10000
% --- IP measurement resolution
if ~isempty(smresdata)
  if IPdat.ysize_real<smresdata.bsize.lc174(1)
    yres=smresdata.res.lc174(1);
  elseif IPdat.ysize_real<=smresdata.bsize.lc174(end)
    yres=interp1(smresdata.bsize.lc174,smresdata.res.lc174,IPdat.ysize_real,'spline');
  elseif IPdat.ysize_real<=smresdata.bsize.lc30(end)
    yres=interp1(smresdata.bsize.lc30,smresdata.res.lc30,IPdat.ysize_real,'spline');
  elseif IPdat.ysize_real<=smresdata.bsize.lc8(end)
    yres=interp1(smresdata.bsize.lc8,smresdata.res.lc8,IPdat.ysize_real,'spline');
  elseif IPdat.ysize_real<=smresdata.bsize.lc2(end)
    yres=interp1(smresdata.bsize.lc2,smresdata.res.lc2,IPdat.ysize_real,'spline');
  elseif IPdat.ysize_real>smresdata.bsize.lc2(end)
    yres=smresdata.res.lc2(end);
  else
    error('Shouldn''t be here...')
  end % if ysize...
  Model.ipLaserRes=[yres yres].*Model.smResScale;
end % if smresdata
if IPdat.ysize_real<Model.avePoint && Model.nMeasIter>1
  IPdat.xsize=IPdat.xsize_real+randn*Model.ipLaserRes(1)./sqrt(Model.nMeasIter);
  IPdat.ysize=IPdat.ysize_real+randn*Model.ipLaserRes(2)./sqrt(Model.nMeasIter);
  FL.SimData.trackCount=FL.SimData.trackCount+FL.SimModel.nPulseMeas*(Model.nMeasIter-1);
else
  IPdat.xsize=IPdat.xsize_real+randn*Model.ipLaserRes(1);
  IPdat.ysize=IPdat.ysize_real+randn*Model.ipLaserRes(2);
end % if ysize<model.avepoint

% IP sigma
sigma=ipData.sigma; 
IPdat.sigma=sigma;
if isfield(Model,'genKnobs') && Model.genKnobs
  IPdat.sigma=sigma;
else
%   for j = 1:6
%     IPdat.sigma(j,j) = sqrt(sigma(j,j)) ;
%   end
%   for i = 1:5
%     for j = i+1:6
%       IPdat.sigma(i,j) = sigma(i,j)/IPdat.sigma(i,i)/IPdat.sigma(j,j) ;
%     end
%   end
end % if Model.genKnobs
[IPdat.emit_x,IPdat.emit_y,IPdat.nt] = GetNEmitFromSigmaMatrix( INSTR{instr_ip}.P, sigma );
[IPdat.emit_x_norm,IPdat.emit_y_norm,IPdat.nt_norm] = GetNEmitFromSigmaMatrix( INSTR{instr_ip}.P, sigma, 'normalmode' );


% Waists
R=diag(ones(1,6));L=zeros(6,6);L(1,2)=1;L(3,4)=1;
IPdat.Waist_x=fminsearch(@(x) minWaist(x,R,L,sigma,1),0,optimset('Tolx',1e-6,'TolFun',0.1e-6^2));
IPdat.Waist_y=fminsearch(@(x) minWaist(x,R,L,sigma,3),0,optimset('Tolx',1e-6,'TolFun',0.1e-6^2));

% "Luminosity" representation
ySize=IPdat.ysize;
ySize_real=IPdat.ysize_real;
if isfield(Model,'initTrack')
  ySize_init=Model.initTrack.ysize;
  ySize_real_init=Model.initTrack.ysize_real;
  IPdat.lumi=(1/(ySize/ySize_init))*100;
  IPdat.lumi_real=(1/(ySize_real/ySize_real_init))*100;
else
  IPdat.lumi_real=(1/((IPdat.ysize_real/35e-9)))*100;
  IPdat.lumi=(1/((IPdat.ysize/35e-9)))*100;
end

% Get x and y IP dispersion
dp=linspace(-0.005,0.005,5);
ip_x=zeros(length(dp),1); ip_y=zeros(length(dp),1);
FL.SimModel=Model;
for idisp=1:length(dp)
  x=0; y=0; x1=0; y1=0; wsx=[0 0 0 0 0]; wsy=[0 0 0 0 0]; xdat1=[]; ydat1=[];
  for iStep=1:nAve
    % NB: reality not needed here- disp measurement just for sim info- not
    % done in reality
    FL.simBeamID=2;
    changeP(dp(idisp),dp(idisp));
    FlHwUpdate;
    [stat instdata]=FlTrackThru(Model.tstart,Model.ip_ind);
    xdat1=[xdat1; [instdata{1}.x]];
    ydat1=[ydat1; [instdata{1}.y]];
    x=x+INSTR{instr_ip}.Data(1); y=y+INSTR{instr_ip}.Data(2);
    x1=x1+INSTR{qd0bpm}.Data(1); y1=y1+INSTR{qd0bpm}.Data(2);
    for iw=1:5
      wsx(iw)=wsx(iw)+INSTR{instr_ws(iw)}.Data(1);
      wsy(iw)=wsy(iw)+INSTR{instr_ws(iw)}.Data(2);
    end
  end
  xdat(idisp,:)=mean(xdat1,1); ydat(idisp,:)=mean(ydat1,1);
  ip_x(idisp)=x./nAve; ip_y(idisp)=y./nAve;
  qd0_x(idisp)=x1./nAve; qd0_y(idisp)=y1./nAve;
  ws_x(idisp,:)=wsx./nAve; ws_y(idisp,:)=wsy./nAve;
end
changeP(0,0);
bs=size(xdat);
for ibpm=1:bs(2)
  q=noplot_polyfit(dp,xdat(:,ibpm)',1,1);
  IPdat.bpmdisp_x(ibpm)=q(2);
  q=noplot_polyfit(dp,ydat(:,ibpm)',1,1);
  IPdat.bpmdisp_y(ibpm)=q(2);
end
q=noplot_polyfit(dp,ip_x,1,1);
IPdat.Disp_x = q(2);
q=noplot_polyfit(dp,ip_y,1,1);
IPdat.Disp_y = q(2);
q=noplot_polyfit(dp,qd0_x,1,1);
dqd0_x = q(2);
q=noplot_polyfit(dp,qd0_y,1,1);
dqd0_y = q(2);
IPdat.dpx = atan2((IPdat.Disp_x-dqd0_x),(BEAMLINE{ipbsm}.S-BEAMLINE{iqd0}.S));
IPdat.dpy = atan2((IPdat.Disp_y-dqd0_y),(BEAMLINE{ipbsm}.S-BEAMLINE{iqd0}.S));
for iw=1:5
  q=noplot_polyfit(dp,ws_x(:,iw)',1,1);
  IPdat.DispWS_x(iw)=q(2);
  q=noplot_polyfit(dp,ws_y(:,iw)',1,1);
  IPdat.DispWS_y(iw)=q(2);
end
IPdat.ntrack=FL.SimData.trackCount;

IPdat.dispxmeas=IPdat.Disp_x;
IPdat.dispymeas=IPdat.Disp_y;
% IPdat.Disp_x=sign(IPdat.sigma(1,6))*sqrt(abs(IPdat.sigma(1,6)))/mean(Beam{1}.Bunch.x(6,:));
% IPdat.Disp_y=sign(IPdat.sigma(3,6))*sqrt(abs(IPdat.sigma(3,6)))/mean(Beam{1}.Bunch.x(6,:));

gamma= mean(Beam{1}.Bunch.x(6,:))/0.511e-3;
xdispcor=sqrt(IPdat.xsize^2 - IPdat.Disp_x^2*sigma(6,6));
ydispcor=sqrt(IPdat.ysize^2 - IPdat.Disp_y^2*sigma(6,6));
IPdat.beta_x=IPdat.xsize^2/(IPdat.emit_x/gamma);
IPdat.beta_y=IPdat.ysize^2/(IPdat.emit_y/gamma);

if VERB && ~exist('flag','var')
  fprintf('IP Position: x= %g y= %g\n',IPdat.xpos,IPdat.ypos)
  fprintf('Waist_x: %g Waist_y: %g\n',IPdat.Waist_x,IPdat.Waist_y);
  if isfield(IPdat,'Disp_x'); fprintf('Disp_x: %g Disp_y: %g\n',IPdat.Disp_x,IPdat.Disp_y); end;
  fprintf('dpx: %g dpy: %g\n',IPdat.dpx,IPdat.dpy)
  fprintf('x size: %g y size: %g\n',IPdat.xsize_real,IPdat.ysize_real)
  fprintf('x RMS: %g y RMS: %g\n',IPdat.bx_sig,IPdat.by_sig);
  fprintf('X-Y Coupling: %g %g %g %g\n',IPdat.sigma(1,3),IPdat.sigma(1,4),IPdat.sigma(2,3),IPdat.sigma(2,4))
  fprintf('Horizontal Projected/Normal mode Emittance: %g / %g\n',IPdat.emit_x/gamma,IPdat.emit_x_norm/gamma)
  fprintf('Vertical Projected/Normal mode Emittance: %g / %g\n',IPdat.emit_y/gamma,IPdat.emit_y_norm/gamma)
  fprintf('Normalised Horizontal Projected/Normal mode Emittance: %g / %g\n',IPdat.emit_x,IPdat.emit_x_norm)
  fprintf('Normalised Vertical Projected/Normal mode Emittance: %g / %g\n',IPdat.emit_y,IPdat.emit_y_norm)
  fprintf('Geometric Luminosity (percent nominal): %g\n',IPdat.lumi)
  fprintf('Beta_x: %g Beta_y: %g\n',IPdat.beta_x,IPdat.beta_y)
  fprintf('Disp corrected beam size x: %g y: %g\n',xdispcor,ydispcor)
  fprintf('--------------------------------------------------------\n')
end

% if isfield(Model,'doQF1FB') && Model.doQF1FB
%   if isempty(imqf1)
%     imqf1=findcells(INSTR,'Index',findcells(BEAMLINE,'Name','MQF1FF'));
%   end
%   off=[INSTR{imqf1}.Data(1) INSTR{imqf1}.Data(2)];
%   GIRDER{BEAMLINE{INSTR{imqf1}.Index}.Girder}.MoverSetPt(1:2)=GIRDER{BEAMLINE{INSTR{imqf1}.Index}.Girder}.MoverPos(1:2)+off;
%   MoverTrim(BEAMLINE{INSTR{imqf1}.Index}.Girder,0);
% end

function chi2 = minWaist(x,R,L,sig,dir)

newsig=(R+L.*x(1))*sig*(R+L.*x(1))';
chi2=newsig(dir,dir)^2;

function chi2 = sinFit(x,data,error)

chi2=sum( ( data - ( x(1) * sin((1:length(data))/x(2)+2*pi*x(3))+mean(data) ) ).^2 ./ error.^2);

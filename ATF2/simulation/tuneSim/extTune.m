function [Model IPdat] = extTune(nseed,gmModelToRun)
% ATF2 FFS alignment and tuning
global BEAMLINE PS VERB MDATA INSTR FL %#ok<NUSED>

% Switch off things I'm tired of hearing about
warning off MATLAB:lscov:RankDefDesignMat
warning off MATLAB:PSTrim_online
warning off MATLAB:MoverTrim_online
warning off MATLAB:FlHwUpdate_nonFSsim
warning off MATLAB:polyfit:RepeatedPointsOrRescale

% Verbosity level
VERB = 0;  

Model.nocompile=false; % Set to true if running un-compiled on farm

% Load in lattice and Model structure (created with lucretia.m)
load ATF2lat-EXTsim
id=findcells(BEAMLINE,'Name','MQM7R');
BEAMLINE{id}.B=3.6233e-04;
%BEAMLINE{id}.B=0;
BEAMLINE{id}.PoleIndex=1;
BEAMLINE{id}.Tilt=0.7854;
% BPM initial setup
[stat,beamout,instdata] = TrackThru( 1, Model.ip_ind, Beam0, 1, 1, 0 ); %#ok<NODEF>
[Model,INSTR]=CreateINSTR(Model,instdata); %#ok<NASGU>
Model.Initial=Model.Initial_IEX;
Beam1=Beam1_IEX; Beam2=Beam2_IEX;
[stat, twiss] = GetTwiss(Model.extStart,length(BEAMLINE),Model.Initial_IEX.x.Twiss,Model.Initial_IEX.y.Twiss) ;
if stat{1}~=1; error(stat{2}); end;

% Set online parameter
Model.online = false;

% Some other stuff that needs to be set
Model.doSMres=false;
Model.ipbsm_method='core';

% Start ind for all tracking = extraction point (IEX)
Model.tstart=Model.extStart;

% IP beam measurememt point (end point of tracking)
Model.ip_ind=findcells(BEAMLINE,'Name','MBS1IP');

% Fill Model with twiss data
tf=fieldnames(twiss);
for it=Model.extStart:length(BEAMLINE)
  for itf=1:length(tf)
    Model.Twiss.(tf{itf})(it)=twiss.(tf{itf})(it-Model.extStart+2);
  end % for itf
end % for it

Model.nocompile=false; % Set to true if running un-compiled on farm

% "reality" settings
Model.reality = false; % Set to true to turn on apertures, low charge, etc.
Model.realbeam = Beam1;
MDATA.chargeloss = zeros(1,length(BEAMLINE)); % Cumulative measure of the charge loss wrt Z
Model.camsettings.quad.a = 0.145250;
Model.camsettings.quad.c = 0.123952;
Model.camsettings.quad.R = 0.031;
Model.camsettings.quad.L = 0.0015875;
Model.camsettings.quad.S1 = 0.034925;
Model.camsettings.quad.S2 = 0.2905;
Model.camsettings.quad.b = Model.camsettings.quad.c + ...
  Model.camsettings.quad.S1 - (sqrt(2)*Model.camsettings.quad.R);
Model.camsettings.sext.a = 0.145250;
Model.camsettings.sext.c = 0.123952;
Model.camsettings.sext.R = 0.031;
Model.camsettings.sext.L = 0.0015875;
Model.camsettings.sext.S1 = 0.034925;
Model.camsettings.sext.S2 = 0.2905;
Model.camsettings.sext.b = Model.camsettings.sext.c + ...
Model.camsettings.sext.S1 - (sqrt(2)*Model.camsettings.sext.R);

% Initialise IP scan count (count number of pulses for tuning)
FL.SimData.trackCount=0;

% Simulation switches
Model.doEXT = true;
Model.doFFS = false;
Model.doGMRUN = false;
Model.doJitter = false;
Model.doSimJitter = false;
Model.nMeasIter = 1; % number of times to iterate a beamsize measurement
%Model.Sm = shintakeMonitor; % Use shintake monitor simulation for IP size measurement (Model.doJitter must be true)
Model = setJitterParams(Model, Beam1);

% Random seed (variables passed as strings when compiled)
if ~isdeployed
  seed_in=nseed;
else
  seed_in=str2double(nseed);
  gmModelToRun=str2double(gmModelToRun);
end
Model.nseed=seed_in;
% Load in Kubo simulations for ring extraction conditions
load ringSimData ringSimData
nt=ringSimData(mod(Model.nseed-1,100)+1,:); % normal mode twiss parameters at IEX
[sig4d,newtwiss,Model.Initial_IEX,Beam1] = norm2sig4d(nt(2),nt(3),nt(4),nt(6),nt(5),nt(7),nt(8),nt(9),nt(10),nt(11),Model.Initial_IEX,Beam1); % convert to 4D beam sigma matrix
Beam0 = makeSingleBunch(Beam1);


% Look for Checkpoint (previous run booted from farm e.g. because exceeded
% cpu time limit)
if exist(['data/mksave-ext_',num2str(Model.nseed),'.mat'],'file')
  load(['data/mksave-ext_',num2str(Model.nseed)]);
  MDATA.IPSCANCOUNT=IPdat.mk.nscans(end); %#ok<NODEF>
  % reset random number generator states
  randn('state',Model.randnState); %#ok<*RAND>
  rand('state',Model.randState); %#ok<RAND>
else
  % Initialise random number generators
  randn('state',Model.nseed);
  rand('state',Model.nseed); %#ok<RAND>
end % if checkpoint

% Ground Motion Model and component jitter
gmModels='ABCY';
Model.gmModel=gmModels(gmModelToRun);
Model.nPulseMeas = 90 ; % Number of pulses for IP beamsize measurement
Model.repRate = 1.56 ; % Hz
Model.nFB = 500; % number of pulses to allow FB to converge
Model.doGM = false;
if isfield(Model,'Sm')
  Model.Sm.scanPhases=-360*1.5:(720*1.5)/(Model.nPulseMeas-1):360*1.5;
end % if SM sim
Model.avePoint=1e-3;

% Pulse-pulse jitter sources
if Model.doJitter
  Model = setJitterParams(Model, Beam1);
else
  Model = setJitterParams(Model, Beam1,'zero');
end % if Model.doJitter

% Twiss parameters
[stat,Model.Twiss] = GetTwiss(1,length(BEAMLINE),Model.Initial.x.Twiss,Model.Initial.y.Twiss) ;

% Track beam through perfect lattice for reference
[Model,INSTR]=CreateINSTR(Model,'zero'); %#ok<NASGU> % set perfect bpms
Model.ipLaserRes = [0 0];
[dat Model] = IP_meas( {Beam1 Beam0}, Model);
Model.initTrack = dat ;
BPM_zeroset( Model, Beam0, 'xonly' )

%   IP waist measurement
Model.ipLaserRes = ([2 2].*1e-9) ./  sqrt(Model.nMeasIter) ; % x/y RMS measurement errors (absolute)

%   Set errors, switch off PS to higher-order magnets, set bpm
%   resolutions
AssignErrors( Model);

% Restore standard BPM settings
[Model,INSTR]=CreateINSTR(Model);

%   EXT-line setup
Model = EXTsetup(Model,Beam2);

% initialise model time and GM seed
if Model.doGM
  Model.time=0; 
  gmMove(Model);
end % if doGM

if Model.doEXT

  disp('Tuning EXT');
  % --- Apply feedback - first get beam through EXT
  for iTrack=1:Model.nFB
    FL.simBeamID=2; FL.SimModel=Model; FL.SimModel.ip_ind=Model.ext.begff;
    FlHwUpdate;
    feedbackEXT(Model,'Correct');
  end % for iTrack
  % Now apply feedback in FFS
  for iTrack=1:Model.nFB
    Model = myTrack(Beam0,Model);
  end % for iTrack
  % Get beam data before coupling correction
  [IPdat.preCOUP Model] = IP_meas( {Beam1 Beam0}, Model ) ;
  % --- EXT dispersion-correction
  for niter=1:1
    EtaVector=MeasureDispersionOnBPMs(Model,Model.ext.MeasEtaStruc,Model.ext.EtaBeams);
%     IncrementMultiKnob('Model.ext.EtaX', EtaVector(1));
%     IncrementMultiKnob('Model.ext.EtaPX',EtaVector(2));
    IncrementMultiKnob('Model.ext.EtaY', -EtaVector(3)/2);
  end % for niter
  % EXT coupling correction
  [iss1,Model.ext.Skew1]=FindOptimumSkewQuadSetting(Model,Model.ext.Skew1,Model.ext.Emat,Beam1);
  if (iss1{1}~=1),disp('Skew1 scan failed'),end
  [iss2,Model.ext.Skew2]=FindOptimumSkewQuadSetting(Model,Model.ext.Skew2,Model.ext.Emat,Beam1);
  if (iss2{1}~=1),disp('Skew2 scan failed'),end
  [iss3,Model.ext.Skew3]=FindOptimumSkewQuadSetting(Model,Model.ext.Skew3,Model.ext.Emat,Beam1);
  if (iss3{1}~=1),disp('Skew3 scan failed'),end
  [iss4,Model.ext.Skew4]=FindOptimumSkewQuadSetting(Model,Model.ext.Skew4,Model.ext.Emat,Beam1);
  if (iss4{1}~=1),disp('Skew4 scan failed'),end
  if ((iss1{1}~=1)||(iss2{1}~=1)||(iss3{1}~=1)||(iss4{1}~=1))
    disp('Coupling correction did not converge')
  end
  % --- Apply feedbacks again
  for iTrack=1:Model.nFB
    FL.simBeamID=2; FL.SimModel=Model; FL.SimModel.ip_ind=Model.ext.begff;
    FlHwUpdate;
    feedbackEXT(Model,'Correct');
  end % for iTrack
  for iTrack=1:Model.nFB
    Model = myTrack(Beam0,Model);
  end % for iTrack
  % --- Measure beam
  [IPdat.postEXT Model] = IP_meas( {Beam1 Beam0}, Model ) ;
  IPdat.postEXT.etaVector=EtaVector;
end % if Model.doEXT

% - run long-term stability model
if Model.doGMRUN
  BPM_zeroset( Model, Beam0 );
  Model.time=0; gmMove(Model);
  Model.doGM = true ;
  VERB=false; 
  Model = setJitterParams(Model, Beam1);
  fb_test;
end % if Model.doGMRUN

% Final IP beam measurements including IP beam correlations
Model.Measure_ipbcor=true;
[IPdat.postTune Model] = IP_meas( {Beam1 Beam0}, Model ) ;
ip_instr=findcells(INSTR,'Index',Model.ip_ind);
IPdat.postTune.ipbcor.tsize=INSTR{ip_instr}.bcor.tsize;
IPdat.postTune.ipbcor.vals=INSTR{ip_instr}.bcor.vals;
IPdat.postTune.ipbcor.terms=INSTR{ip_instr}.bcor.terms;

% Save data running on SLAC batch farm
if isdeployed || Model.nocompile
  farmSave('data',['mksave-ext_',num2str(Model.nseed)],Model,IPdat);
  evalc(['!touch data/final-ext_',num2str(Model.nseed),'.mat']);
end

jo=[]; jv={}; xv={};
for ik=1:length(IPdat.mk.lumi)
  jo(ik)=IPdat.mk.jitord{ik}(1);
  jv{ik}=IPdat.mk.ipjitvals{ik}(:,jo(ik))';
  xv{ik}=[IPdat.mk.optim{ik}(2:end-2).x];
  for im=2:length(IPdat.mk.optim{ik})-2
    ys{ik}(im-1)=sqrt(IPdat.mk.optim{ik}(im).vals.fval);
  end
end
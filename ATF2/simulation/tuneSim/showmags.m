function [qn sn mn mn_pole mn_KL K1 K2 K1L K2L]=showmags
global BEAMLINE FL

E0=1.282; %GeV
for iele=findcells(BEAMLINE,'B')
  BEAMLINE{iele}.B=BEAMLINE{iele}.B.*(E0/BEAMLINE{iele}.P);
  BEAMLINE{iele}.P=E0;
end

if exist('FL.mat','file'); load FL_spline; end;
clight=299792458; % speed of light (m/s)
Cb=1e9/clight; % T-m/GeV
brho=Cb*E0;
i1=findcells(BEAMLINE,'Name','QF1X');i1=i1(1);
q=findcells(BEAMLINE,'Class','QUAD',i1-1,findcells(BEAMLINE,'Name','IP'));
s=findcells(BEAMLINE,'Class','SEXT',findcells(BEAMLINE,'Name','BEGFF'),findcells(BEAMLINE,'Name','IP'));
s=[s findcells(BEAMLINE,'Name','SK1FF')];
qn=arrayfun(@(x) BEAMLINE{x}.Name,q,'UniformOutput',false); [qn qi]=unique(qn); q=q(qi);
mqn=arrayfun(@(x) [qn{x} 'MULT'],1:length(qn),'UniformOutput',false);
sn=arrayfun(@(x) BEAMLINE{x}.Name,s,'UniformOutput',false); [sn si]=unique(sn); s=s(si);
sqn=arrayfun(@(x) [sn{x} 'MULT'],1:length(sn),'UniformOutput',false);
mn=[mqn sqn];
disp(' ')
disp('=========')
disp('QUADS:')
disp('=========')
for iq=1:length(qn)
  try
    Lq(iq)=GetTrueStrength(q(iq))*2;
  catch
    Lq(iq)=BEAMLINE{q(iq)}.B(1)*2;
  end
  K1L(iq)=Lq(iq)/brho;
  K1(iq)=K1L(iq)/(BEAMLINE{q(iq)}.L*2);
  fprintf('%10s: Lucretia: %0+11.8f (T) K1: %0+11.8f K1L: %0+11.8f I:',qn{iq},Lq(iq),K1(iq),K1L(iq))
  if exist('FL','var') && isfield(FL,'HwInfo') && BEAMLINE{q(iq)}.PS && ...
      length(FL.HwInfo.PS)>=BEAMLINE{q(iq)}.PS && ~isempty(FL.HwInfo.PS(BEAMLINE{q(iq)}.PS).conv)
    ips=BEAMLINE{q(iq)}.PS;
    i=interp1(FL.HwInfo.PS(ips).conv(2,:),FL.HwInfo.PS(ips).conv(1,:),...
      abs(BEAMLINE{q(iq)}.B(1)),'linear');
    fprintf('%11.8f (A)\n',i)
  else
    fprintf('? (A)\n')
  end
end
disp('=========')
disp('SEXTS:')
disp('=========')
for is=1:length(sn)
  try
    Ls(is)=GetTrueStrength(s(is))*2;
  catch
    Ls(is)=BEAMLINE{s(is)}.B(1)*2;
  end
  K2L(is)=Ls(is)/brho;
  K2(is)=K2L(is)/(BEAMLINE{s(is)}.L*2);
  fprintf('%10s: Lucretia: %0+12.8f (T/m) K2: %0+12.8f K2L: %0+12.8f I: ',sn{is},Ls(is),K2(is),K2L(is))
  if exist('FL','var') && isfield(FL,'HwInfo') && BEAMLINE{s(is)}.PS && ...
      length(FL.HwInfo.PS)>=BEAMLINE{s(is)}.PS && ~isempty(FL.HwInfo.PS(BEAMLINE{s(is)}.PS).conv)
    ips=BEAMLINE{s(is)}.PS;
    i=interp1(FL.HwInfo.PS(ips).conv(2,:),FL.HwInfo.PS(ips).conv(1,:),...
      abs(BEAMLINE{s(is)}.B(1)),'linear');
    fprintf('%11.8f (A)\n',i)
  else
    fprintf('? (A)\n')
  end
end
for im=1:length(mn)
  if ~strcmp(mn{im},'SK1FFMULT') && ~strcmp(mn{im},'SK2FFMULT') && ~strcmp(mn{im},'SK3FFMULT') && ~strcmp(mn{im},'SK4FFMULT')
    bl=findcells(BEAMLINE,'Name',mn{im});
    try
      mn_pole{im}=BEAMLINE{bl(1)}.PoleIndex;
      mn_KL{im}=BEAMLINE{bl(1)}.B/brho;
    catch
      mn_pole{im}=0;
      mn_KL{im}=0;
    end
  end
end

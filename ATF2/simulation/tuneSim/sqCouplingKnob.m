classdef sqCouplingKnob < handle
  properties(Dependent)
    sig13
    sig23
    sig14
    sig24
  end
  properties(Access=private)
    psvals
    psid=[];
    knobs={};
    knobvals=[0 0 0 0];
  end
  properties
    
  end
  %% get/set methods
  methods
    function set.sig13(obj,val)
      setsig(obj,1,val);
    end
    function val=get.sig13(obj)
      val=obj.knobvals(1);
    end
    function set.sig23(obj,val)
      setsig(obj,2,val);
    end
    function val=get.sig23(obj)
      val=obj.knobvals(2);
    end
    function set.sig14(obj,val)
      setsig(obj,3,val);
    end
    function val=get.sig14(obj)
      val=obj.knobvals(3);
    end
    function set.sig24(obj,val)
      setsig(obj,4,val);
    end
    function val=get.sig24(obj)
      val=obj.knobvals(4);
    end
  end
  %% Main public methods
  methods
    function obj=sqCouplingKnob(knobs)
      global BEAMLINE
      if ~exist('knobs','var') || ~isfield(knobs,'sig13') || ~isfield(knobs,'sig23') || ~isfield(knobs,'sig14') || ~isfield(knobs,'sig24')
        error('Must pass properly formatted knobs structure')
      end
      qk=findcells(BEAMLINE,'Name','QK*X');
      for iqk=1:2:length(qk)
        obj.psid(end+1)=BEAMLINE{qk(iqk)}.PS;
      end
      getps(obj);
      obj.knobs{1}=knobs.sig13;
      obj.knobs{2}=knobs.sig23;
      obj.knobs{3}=knobs.sig14;
      obj.knobs{4}=knobs.sig24;
    end
  end
  %% Private methods
  methods(Access=private)
    function setsig(obj,isig,val)
      global PS
      qkval=obj.knobs{isig}.*val;
      for iqk=1:4
        PS(obj.psid(iqk)).SetPt=obj.psvals(iqk)+qkval(iqk);
      end
      stat=PSTrim(obj.psid);
      if stat{1}==1
        obj.knobvals(isig)=val;
      else
        warning('Lucretia:sqCouplingKnob:PSTrimError','PSTrim error: %s',stat{2});
      end
    end
    function getps(obj)
      global PS
      for iqk=1:4
        obj.psvals(iqk)=PS(obj.psid(iqk)).Ampl;
      end
    end
  end
end
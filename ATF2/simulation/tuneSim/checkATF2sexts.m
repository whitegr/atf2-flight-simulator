function camstatus = checkATF2sexts(Model)

global GIRDER BEAMLINE
persistent sg

if isempty(sg)
  isext=findcells(BEAMLINE,'Name','SF6FF'); sg(1)=BEAMLINE{isext(1)}.Girder;
  isext=findcells(BEAMLINE,'Name','SF5FF'); sg(2)=BEAMLINE{isext(1)}.Girder;
  isext=findcells(BEAMLINE,'Name','SD4FF'); sg(3)=BEAMLINE{isext(1)}.Girder;
  isext=findcells(BEAMLINE,'Name','SF1FF'); sg(4)=BEAMLINE{isext(1)}.Girder;
  isext=findcells(BEAMLINE,'Name','SD0FF'); sg(5)=BEAMLINE{isext(1)}.Girder;
end

xpos = GIRDER{sg(1)}.MoverSetPt(1);
ypos = GIRDER{sg(1)}.MoverSetPt(2);
tilt = GIRDER{sg(1)}.MoverSetPt(3);
camangles50 = fftbmover_convert(Model,xpos,ypos,tilt,'sext');

xpos = GIRDER{sg(2)}.MoverSetPt(1);
ypos = GIRDER{sg(2)}.MoverSetPt(2);
tilt = GIRDER{sg(2)}.MoverSetPt(3);
camangles51 = fftbmover_convert(Model,xpos,ypos,tilt,'sext');

xpos = GIRDER{sg(3)}.MoverSetPt(1);
ypos = GIRDER{sg(3)}.MoverSetPt(2);
tilt = GIRDER{sg(3)}.MoverSetPt(3);
camangles52 = fftbmover_convert(Model,xpos,ypos,tilt,'sext');

xpos = GIRDER{sg(4)}.MoverSetPt(1);
ypos = GIRDER{sg(4)}.MoverSetPt(2);
tilt = GIRDER{sg(4)}.MoverSetPt(3);
camangles53 = fftbmover_convert(Model,xpos,ypos,tilt,'sext');

xpos = GIRDER{sg(5)}.MoverSetPt(1);
ypos = GIRDER{sg(5)}.MoverSetPt(2);
tilt = GIRDER{sg(5)}.MoverSetPt(3);
camangles54 = fftbmover_convert(Model,xpos,ypos,tilt,'sext');


if ~isreal(camangles50(1)) || ~isreal(camangles50(2)) || ~isreal(camangles50(3)) || ...
    ~isreal(camangles51(1)) || ~isreal(camangles51(2)) || ~isreal(camangles51(3)) || ...
    ~isreal(camangles52(1)) || ~isreal(camangles52(2)) || ~isreal(camangles52(3)) || ...
    ~isreal(camangles53(1)) || ~isreal(camangles53(2)) || ~isreal(camangles53(3)) || ...
    ~isreal(camangles54(1)) || ~isreal(camangles54(2)) || ~isreal(camangles54(3))
  camstatus = false;
else
  camstatus = true;
end

return
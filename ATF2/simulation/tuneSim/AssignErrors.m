function [] = AssignErrors(Model)
% Assign alignment errors to magnets

global BEAMLINE PS

if Model.doEXT && Model.doFFS
  group={'EXTmagGroup' 'ffmagGroup'};
elseif Model.doEXT
  group={'EXTmagGroup'};
elseif Model.doFFS
  group={'ffmagGroup'};
else
  disp('No alignment errors being added!')
  return
end % misalign just FFS or all?

for iGroup=1:length(group)
  if isequal(group{iGroup},'EXTmagGroup')
    stat = ErrorGroupGaussErrors( Model.(group{iGroup}).Quad.OffUse, zeros(1,6), [Model.ext.align.x 0 Model.ext.align.y 0 Model.align.z Model.ext.align.rot], zeros(1,6), Model.errSig );
    if stat{1}~=1; error(stat{2:end}); end;
    if ~isempty(Model.(group{iGroup}).Sext.Off)
      stat = ErrorGroupGaussErrors( Model.(group{iGroup}).Sext.Off, zeros(1,6), [Model.ext.align.x 0 Model.ext.align.y 0 Model.align.z Model.ext.align.rot], zeros(1,6), Model.errSig );
      if stat{1}~=1; error(stat{2:end}); end;
    end % if sextupoles exist
    stat = ErrorGroupGaussErrors( Model.(group{iGroup}).Quad.dB, 0, Model.ext.align.dB, 0, Model.errSig );
    if stat{1}~=1; error(stat{2:end}); end;
    if ~isempty(Model.(group{iGroup}).Sext.dB)
      stat = ErrorGroupGaussErrors( Model.(group{iGroup}).Sext.dB, 0, Model.ext.align.dB_sext, 0, Model.errSig );
      if stat{1}~=1; error(stat{2:end}); end;
    end % if sextupoles exist
    if ~isempty(Model.(group{iGroup}).SBEN.Off)
      stat = ErrorGroupGaussErrors( Model.(group{iGroup}).SBEN.Off, zeros(1,6), [Model.ext.align.x_bend 0 Model.ext.align.y_bend 0 Model.align.z Model.ext.align.rot_bend], zeros(1,6), Model.errSig );
      if stat{1}~=1; error(stat{2:end}); end;
    end % if sben exist
  else
    stat = ErrorGroupGaussErrors( Model.(group{iGroup}).Quad.OffUse, zeros(1,6), [Model.align.x 0 Model.align.y 0 Model.align.z Model.align.rot], zeros(1,6), Model.errSig );
    if stat{1}~=1; error(stat{2:end}); end;
    if ~isempty(Model.(group{iGroup}).Sext.Off)
      stat = ErrorGroupGaussErrors( Model.(group{iGroup}).Sext.Off, zeros(1,6), [Model.align.x 0 Model.align.y 0 Model.align.z Model.align.rot], zeros(1,6), Model.errSig );
      if stat{1}~=1; error(stat{2:end}); end;
    end % if sextupoles exist
    stat = ErrorGroupGaussErrors( Model.(group{iGroup}).Quad.dB, 0, Model.align.dB, 0, Model.errSig );
    if stat{1}~=1; error(stat{2:end}); end;
    if ~isempty(Model.(group{iGroup}).Sext.dB)
      stat = ErrorGroupGaussErrors( Model.(group{iGroup}).Sext.dB, 0, Model.align.dB_sext, 0, Model.errSig );
      if stat{1}~=1; error(stat{2:end}); end;
    end % if sextupoles exist
    if ~isempty(Model.(group{iGroup}).SBEN.Off)
      stat = ErrorGroupGaussErrors( Model.(group{iGroup}).SBEN.Off, zeros(1,6), [Model.align.x_bend 0 Model.align.y_bend 0 Model.align.z Model.align.rot_bend], zeros(1,6), Model.errSig );
      if stat{1}~=1; error(stat{2:end}); end;
    end % if sben exist
  end % which group?
end % for iGroup

% Misalign BPM's not attached to magnets
if Model.doFFS
  for iBPM=[Model.fbBpmsToUseX Model.fbBpmsToUse]
    BEAMLINE{iBPM}.Offset=BEAMLINE{iBPM}.Offset + [randnt(Model.errSig)*Model.align.x 0 randnt(Model.errSig)*Model.align.y 0 randnt(Model.errSig)*Model.align.z 0];
  end % for iBPM
end % if misaligning ffmagGroup

% Systematic Magnetic error measurements
sysError=Model.align.dB_syst*randnt(Model.errSig);
for iGroup=1:length(group)
  if ~isempty(Model.(group{iGroup}).Sext.dB)
    mags=[Model.(group{iGroup}).Quad.dB.ClusterList.index Model.(group{iGroup}).Sext.dB.ClusterList.index];
  else
    mags=[Model.(group{iGroup}).Quad.dB.ClusterList.index];
  end % if ~isempty sexts
  for iMag=mags
    BEAMLINE{iMag}.dB=BEAMLINE{iMag}.dB+sysError;
  end
end % for iGroup

if Model.doFFS
  % Quads
  for g_id=1:length(Model.magGroup.Quad.dB.ClusterList)
    if ~isempty(Model.magGroup.Quad.Bpm(g_id).Ind) && (Model.magGroup.Quad.Bpm(g_id).Ind>=Model.ffStart.ind)
      BEAMLINE{Model.magGroup.Quad.Bpm(g_id).Ind}.Offset=BEAMLINE{Model.magGroup.Quad.dB.ClusterList(g_id).index(1)}.Offset;
      BEAMLINE{Model.magGroup.Quad.Bpm(g_id).Ind}.Offset(1)=...
        BEAMLINE{Model.magGroup.Quad.Bpm(g_id).Ind}.Offset(1)+(randnt(1)*Model.align.magbpm_x);
      BEAMLINE{Model.magGroup.Quad.Bpm(g_id).Ind}.Offset(3)=...
        BEAMLINE{Model.magGroup.Quad.Bpm(g_id).Ind}.Offset(3)+(randnt(1)*Model.align.magbpm_y);
    elseif ~isempty(Model.magGroup.Quad.Bpm(g_id).Ind) && (Model.magGroup.Quad.Bpm(g_id).Ind<Model.ffStart.ind)
      BEAMLINE{Model.magGroup.Quad.Bpm(g_id).Ind}.Offset=BEAMLINE{Model.magGroup.Quad.dB.ClusterList(g_id).index(1)}.Offset;
      BEAMLINE{Model.magGroup.Quad.Bpm(g_id).Ind}.Offset(1)=...
        BEAMLINE{Model.magGroup.Quad.Bpm(g_id).Ind}.Offset(1)+(randnt(1)*Model.ext.align.magbpm_x);
      BEAMLINE{Model.magGroup.Quad.Bpm(g_id).Ind}.Offset(3)=...
        BEAMLINE{Model.magGroup.Quad.Bpm(g_id).Ind}.Offset(3)+(randnt(1)*Model.ext.align.magbpm_y);
    end
  end
  % Sextupoles
  for g_id=1:length(Model.magGroup.Sext.dB.ClusterList)
    BEAMLINE{Model.magGroup.Sext.Bpm(g_id).Ind}.Offset=BEAMLINE{Model.magGroup.Sext.dB.ClusterList(g_id).index(1)}.Offset;
    BEAMLINE{Model.magGroup.Sext.Bpm(g_id).Ind}.Offset(1)=...
      BEAMLINE{Model.magGroup.Sext.Bpm(g_id).Ind}.Offset(1)+(randnt(1)*Model.align.magbpm_x);
    BEAMLINE{Model.magGroup.Sext.Bpm(g_id).Ind}.Offset(3)=...
      BEAMLINE{Model.magGroup.Sext.Bpm(g_id).Ind}.Offset(3)+(randnt(1)*Model.align.magbpm_y);
    PS(Model.PSind.Sext(g_id)).SetPt = 0;
    stat = PSTrim( Model.PSind.Sext(g_id) ); if stat{1}~=1; error(stat{2:end}); end;
  end
end % if Model.FFS 


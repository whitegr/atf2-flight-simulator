function Model = setJitterParams(Model, Beam, par)
% Set fast jitter parameters
global BEAMLINE PS INSTR

for i_instr=1:length(INSTR)
  if isfield(INSTR{i_instr},'Res') && isfield(BEAMLINE{INSTR{i_instr}.Index},'Resolution')
    BEAMLINE{INSTR{i_instr}.Index}.Resolution=INSTR{i_instr}.Res(1);
  end % if res given
end % for i_instr

if exist('par','var') && isequal(par,'zero')
  Model.fbKikErr=0;
  Model.fbWeight=0.2;
  Model.ext.feedback.weight=0.02;
  Model.nFB=200;
  Model.align.incoming.x = 0;
  Model.align.incoming.xp = 0;
  Model.align.incoming.y = 0;
  Model.align.incoming.yp = 0;
  Model.align.incoming.e = 0;
  Model.dE_set_error = 0;
  Model.jitter.quad=0;
  Model.jitter.sext=0;
  Model.jitter.oct=0;
  for iPS=1:length(PS)
    PS(iPS).dAmpl=0;
    PS(iPS).Step=0;
  end % for iPS
else
  Model.fbKikErr=1e-4; %floor((Model.nseed-1)/100)*1e-4;
  Model.fbWeight=0.2; %0.15+0.01*floor((Model.nseed-1)/100);
  Model.ext.feedback.weight=0.02; % + 0.001*floor((Model.nseed-1)/100);
  Model.nFB=200;
  Model.align.incoming.x = std(Beam.Bunch.x(1,:))*0.1;%*floor((Model.nseed-1)/100)*0.1;
  Model.align.incoming.xp = std(Beam.Bunch.x(2,:))*0.1;%*floor((Model.nseed-1)/100)*0.1;
  Model.align.incoming.y = std(Beam.Bunch.x(3,:))*0.1;%*floor((Model.nseed-1)/100)*0.1;
  Model.align.incoming.yp = std(Beam.Bunch.x(4,:))*0.1;%*floor((Model.nseed-1)/100)*0.1;
  Model.align.incoming.e = 1e-4; %2e-4*floor((Model.nseed-1)/100);
  Model.dE_set_error = 0;
  Model.jitter.quad=10e-9; %5e-9*floor((Model.nseed-1)/100);
  Model.jitter.sext=10e-9; %5e-9*floor((Model.nseed-1)/100);
  Model.jitter.oct=10e-9; %5e-9*floor((Model.nseed-1)/100);
  if isfield(Model,'Sm')
    Model.Sm.fringeJitter=5e-9*floor((Model.nseed-1)/100);
  end % if using shintake monitor sim
  % Correctors
  for ips=[Model.PSind.cor.fbX1 Model.PSind.cor.fbY1]
    PS(ips).Step=Model.align.ps_step;
    PS(ips).dAmpl=Model.align.cor_dAmpl;
  end % for ips
end % if par passed and 'zero'

function chi2 = QuadFit(x,Model,dir,Qinit,qset,Beam,RM,is_squad,g_id,bpm_list)
% Function for finding Quad-Beam offset
global GIRDER PS INSTR

newmove=[0 0 0]; newmove(dir)=x;

% Make move
GIRDER{Model.Gind.Quad(g_id)}.MoverSetPt=Qinit + newmove ;
stat = MoverTrim( Model.Gind.Quad(g_id) );if stat{1}~=1; error(stat{2:end}); end;

% Get orbit changes for quad strength change
PS(Model.PSind.Quad(g_id)).SetPt = 1; stat = PSTrim( Model.PSind.Quad(g_id) );if stat{1}~=1; error(stat{2:end}); end;
if Model.doGM
  Model = myTrack(Beam,Model);
else
  FL.simBeamID=2; FL.SimModel=Model;
  FlHwUpdate;
  Model.bpmData.x=cellfun(@(x) x.Data(1),INSTR); Model.bpmData.y=cellfun(@(x) x.Data(2),INSTR);
end
bpm_x{1} = Model.bpmData.x; bpm_y{1} = Model.bpmData.y;
PS(Model.PSind.Quad(g_id)).SetPt = Model.bpm.DeltaB_qalign; stat = PSTrim( Model.PSind.Quad(g_id) );if stat{1}~=1; error(stat{2:end}); end;
if Model.doGM
  Model = myTrack(Beam,Model);
else
  FL.simBeamID=2; FL.SimModel=Model;
  FlHwUpdate;
  Model.bpmData.x=cellfun(@(x) x.Data(1),INSTR); Model.bpmData.y=cellfun(@(x) x.Data(2),INSTR);
end
bpm_x{2} = Model.bpmData.x; bpm_y{2} = Model.bpmData.y;
xdiff=bpm_x{2}-bpm_x{1}; ydiff=bpm_y{2}-bpm_y{1};

% Restore quad to initial condition
PS(Model.PSind.Quad(g_id)).SetPt = qset; stat = PSTrim( Model.PSind.Quad(g_id) );if stat{1}~=1; error(stat{2:end}); end;
GIRDER{Model.Gind.Quad(g_id)}.MoverSetPt=Qinit;
stat = MoverTrim( Model.Gind.Quad(g_id) );if stat{1}~=1; error(stat{2:end}); end;

% Calculate offset
dx=xdiff(bpm_list.Han); 
dy=ydiff(bpm_list.Han); 
dx_err=sqrt(2).*bpm_list.Res;
dy_err=sqrt(2).*bpm_list.Res;
if ~any(dx_err)
  dx_err=ones(size(dx_err));
else
  dx_err(dx_err==0)=min(dx_err(dx_err~=0));
end
if ~any(dy_err)
  dy_err=ones(size(dy_err));
else
  dy_err(dy_err==0)=min(dy_err(dy_err~=0));
end

dq=0;
if length(dx)>2
  switch dir
    case 1
      if is_squad
        [q,dq]=noplot_polyfit(1:length(bpm_list.Han),-dy./(RM.R33.*RM.dRquad(3,1)+RM.R34.*RM.dRquad(4,1)),dy_err./ ...
          (RM.R33.*RM.dRquad(3,1)+RM.R34.*RM.dRquad(4,1)),0);
      else
        [q,dq]=noplot_polyfit(1:length(bpm_list.Han),-dx./(RM.R11.*RM.dRquad(1,1)+RM.R12.*RM.dRquad(2,1)),dx_err./ ...
          (RM.R11.*RM.dRquad(1,1)+RM.R12.*RM.dRquad(2,1)),0);
      end
    case 2
      if is_squad
        [q,dq]=noplot_polyfit(1:length(bpm_list.Han),-dx./(RM.R11.*RM.dRquad(1,3)+RM.R12.*RM.dRquad(2,3)),dx_err./ ...
          (RM.R11.*RM.dRquad(1,3)+RM.R12.*RM.dRquad(2,3)),0);
      else
        [q,dq]=noplot_polyfit(1:length(bpm_list.Han),-dy./(RM.R33.*RM.dRquad(3,3)+RM.R34.*RM.dRquad(4,3)),dy_err./ ...
          (RM.R33.*RM.dRquad(3,3)+RM.R34.*RM.dRquad(4,3)),0);
      end
    otherwise
      error('Only choose dir=1|2');
  end % switch
elseif (dir==1 && ~is_squad) || (dir==2 && is_squad)
  q=mean(dx);
elseif (dir==2 && ~is_squad) || (dir==1 && is_squad)
  q=mean(dy);
end % if length dx > 2
if dq==0; dq=1; end;
chi2 = (q/dq)^2;

return

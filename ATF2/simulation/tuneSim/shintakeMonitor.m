function Sm = shintakeMonitor(Sm,Beam,fringePhase)
% Simulation of shintake monitor beamsize measurement
% Initial setup:
% --------------
% Sm = shintakeMonitor('setup-default'); or Sm = shintakeMonitor;
% Setup default Sm - 532nm YAG laser, crossing angle= 174 degrees
% Change parameters by editing returned Sm structure
%
% Scan Beam:
% ----------
% Sm = shintake(Sm,Beam)
% Beam = lucretia macroparticle Beam structure
% smPhase = phase of inteference pattern (degrees)
% Sm.signal = returned

% Input arg check
if ~nargin || (nargin==1 && isequal(lower(Sm),'setup-default'))
  mode = 'setup-default';
elseif nargin==3
  mode='run';
else
  error('Incorrect arguments - seek help');
end % arg check

% main code- setup or run
switch mode
  case 'setup-default'
    Sm.laser_type='YAG';
    Sm.lambda=532e-9; % wavelength
    Sm.phi=174; % laser angle
    Sm.signal=0;
    Sm.fringeRange=10; % +/- N fringe spacings
    Sm.fringeJitter=10e-9; % jitter on fringe position
  case 'run'
    Sm.theta=2*(pi/2-((180-Sm.phi)/(360*2))*2*pi);
    ky=((2*pi)/Sm.lambda)*sin(Sm.theta/2);    
    Sm.d=pi/ky; % fringe spacing
    % Get y histogram from bunch
    [N,y] = hist(Beam.Bunch.x(3,:),1e6); N=N.*Beam.Bunch.Q(1);
    % Get fringe function evaluated at histogram bin centres
    y=y+randn*Sm.fringeJitter;
    fringeFn=1+cos(Sm.theta).*cos(2.*ky.*y+(fringePhase/360)*2*pi);
    % Make fringeFn zero outside of fringe range
    fringeFn(abs(y)>Sm.fringeRange*Sm.d)=0;
    % Get overlap integral of bunch charge with fringeFn
    Sm.signal=sum(fringeFn.*N);
    Sm.error=1./sqrt(Sm.signal);
  otherwise
    error('Shouldn''t be here- something gone terribly wrong, call your Mother');
end % switch mode
function [] = farmSave(directory,file,Model,IPdat) %#ok<INUSD>
% save file whilst running on SLAC farm (locks directory whilst saving)
global BEAMLINE PS GIRDER FL INSTR %#ok<NUSED>

% save random number states and any function internals
Model.randState = rand('state');
Model.randnState = randn('state');
Model.funcvals = funcRestore('get');

waitTime = 2 ; % (mins) if file locked for waitTime, assume locking process crashed and delete lockfile
 
% Check for lock file
lockFile_orig=dir([directory,'/lockfile']);
tic; % start clock
if ~isempty(lockFile_orig)
  while 1 % wait until lockfile removed
    lockFile=dir([directory,'/lockfile']);
    if isempty(lockFile); break; end;
    if isequal(lockFile.date,lockFile_orig.date)
      lockTime = datevec(now -datenum(lockFile_orig.date)) ;
      if lockTime(end-1) > waitTime; delete([directory,'/lockfile']); disp('**Deleting lockfile!'); end;
    else
      lockFile_orig=lockFile;
    end % is this the original lockfile?
  end % while locked
end % if lockfile found

% Lock directory
try
  fid=fopen([directory,'/lockfile'],'w'); fclose(fid);
catch
  pause(2);
  try
    fid=fopen([directory,'/lockfile'],'w'); fclose(fid);
  catch
    disp('lockfile creation failed!');
  end % try/catch
end % try/catch

% Execute save command
save([directory,'/',file])

% Remove directory lock
delete([directory,'/lockfile']);

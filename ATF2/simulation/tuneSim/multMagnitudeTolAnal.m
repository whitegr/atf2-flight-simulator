clear all global
jm=findResource;
job=jm.Jobs;
jtag=get(job,'Tag');
job=job(ismember(jtag,'multMagnitudeTol'));
sigx=[]; sigy=[]; tol=[];
mtol=linspace(0,0.1,100);
for ij=1:length(job)
  fprintf('Processing %d of %d\n',ij,length(job));
  try
    ldata=job(ij).getAllOutputArguments;
  catch
    continue
  end
  if isempty(ldata); continue; end;
  tol(end+1)=mtol(ij);
  sigx(end+1)=ldata{1}(1);
  sigy(end+1)=ldata{1}(2);
end
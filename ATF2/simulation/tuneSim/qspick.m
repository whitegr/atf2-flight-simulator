function [xpick,ypick] = qspick(Model,maxoff,mindev)
global INSTR BEAMLINE
usequad=find([Model.magGroup.Quad.Bpm.Ind]>Model.extStart);
usequad=usequad(ismember(usequad,Model.moverList));
xpick=cell(1,max(usequad));
ypick=cell(1,max(usequad));
for iq=1:length(usequad)
  if isempty(Model.magGroup.Quad.Bpm(usequad(iq)).Ind)
    fprintf('No BPM on quad: %s\n',BEAMLINE{Model.magGroup.Quad.dB.ClusterList(usequad(iq)).index(1)}.Name)
    continue
  end % if no bpm on this quad
  for iqbpm=iq+1:length(usequad)+1
    quadind=Model.magGroup.Quad.dB.ClusterList(usequad(iq)).index;
    if iqbpm==length(usequad)+1
      bpmhan=findcells(INSTR,'Index',findcells(BEAMLINE,'Name','MS1IP'));
      bpmind=INSTR{bpmhan}.Index;
    else
      bpmind=Model.magGroup.Quad.Bpm(usequad(iqbpm)).Ind;
      bpmhan=findcells(INSTR,'Index',Model.magGroup.Quad.Bpm(usequad(iqbpm)).Ind);
    end % if ipbpm
    if isempty(bpmhan); continue; end;
    [stat,R]=RmatAtoB(quadind(end)+1,bpmind); if stat{1}~=1; error(stat{2}); end;
    [stat,Rmag]=RmatAtoB(quadind(1),quadind(end)); if stat{1}~=1; error(stat{2}); end;
    if length(INSTR{bpmhan}.Res)>1
      maxdev_x=(maxoff*Rmag(2,1)*R(1,2)+maxoff*Rmag(1,1)*R(1,1))/INSTR{bpmhan}.Res(1);
      maxdev_y=(maxoff*Rmag(4,3)*R(3,4)+maxoff*Rmag(3,3)*R(3,3))/INSTR{bpmhan}.Res(3);
    else
      maxdev_x=(maxoff*Rmag(2,1)*R(1,2)+maxoff*Rmag(1,1)*R(1,1))/INSTR{bpmhan}.Res;
      maxdev_y=(maxoff*Rmag(4,3)*R(3,4)+maxoff*Rmag(3,3)*R(3,3))/INSTR{bpmhan}.Res;
    end % if length res>1
    if abs(maxdev_x)>mindev
      xpick{usequad(iq)}=[xpick{usequad(iq)} bpmhan];
    end % if can use this bpm
    if abs(maxdev_y)>mindev
      ypick{usequad(iq)}=[xpick{usequad(iq)} bpmhan];
    end % if can use this bpm
  end % for iqbpm
end % for iq
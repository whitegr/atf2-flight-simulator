[qn sn mn mn_pole mn_KL]=showmags;
data=[[qroll.bsize] [sroll.bsize] [bendroll.bsize] [soff.bsize]].*1e6;
rh=rectangle('Position',[length(data)-5.5 0 6.5 ax(4)],'FaceColor',[0.1 0.5 0.5]);
hold on
bar(data,'FaceColor','r')
data=[[qroll.xpy] [sroll.xpy] [bendroll.xpy] [soff.xpy]].*1e6;
bar(data,'FaceColor','b')
data=[[qroll.xy] [sroll.xy] [bendroll.xy] [soff.xy]].*1e6;
bar(data,'FaceColor','k')
hold off
names=[qn sn {'B1FF' 'B2FF' 'B5FF'} sn];
grid on
set(gca,'XTick',1:length(names))
set(gca,'XTickLabel',names)
set(gca,'FontWeight','bold')
ylabel('Vertical beam size / um')
legend({'Beamsize' '<x''y> Contribution' '<xy> Contribution'});
ax=axis;
xticklabel_rotate90()
function [terms0 termsize0 mterms mtermsize] =  multAberTerms(file)
global BEAMLINE
load(file)

mults={'QD0FFMULT' 'QF1FFMULT' 'QD2AFFMULT' 'QD2BFFMULT' 'QF3FFMULT' 'QD4AFFMULT' 'QD4BFFMULT' 'QF5AFFMULT' 'QF5BFFMULT' ...
  'QD6FFMULT' 'QF7FFMULT' 'QD8FFMULT' 'QF9AFFMULT' 'QF9BFFMULT' 'QD10AFFMULT' 'QD10BFFMULT' 'QM11FFMULT' 'QM12FFMULT' ...
  'QM13FFMULT' 'QM14FFMULT' 'QM15FFMULT' 'QM16FFMULT' 'SF6FFMULT' 'SF5FFMULT' 'SD4FFMULT' 'SF1FFMULT' 'SD0FFMULT'};

for imult=1:length(mults)
  multind{imult}=findcells(BEAMLINE,'Name',mults{imult});
end
qm16ind=findcells(BEAMLINE,'Name','QM16FFMULT');
[stat beam]=TrackThru(1,qm16ind(1),Beam1,1,1,0);

[terms0 termsize0]=getterms(beam,qm16ind);

for imult=1:length(mults)
  fprintf('MULT strength test for %s (step %d of %d)\n',mults{imult},imult,length(mults))
  B=BEAMLINE{multind{imult}(1)}.B;
  for iele=1:length(multind{imult})
    BEAMLINE{multind{imult}(iele)}.B=BEAMLINE{multind{imult}(iele)}.B.*0;
  end
  [terms termsize]=getterms(beam,qm16ind,termsize0);
  mterms(imult,:)=terms;
  mtermsize(imult,:)=termsize;
  for iele=1:length(multind{imult})
    BEAMLINE{multind{imult}(iele)}.B=B;
  end
end

[a b]=sort(mtermsize(:,1),'descend');
for imult=b'
  fprintf('%s: %g (nm) Term=%d %d %d %d\n',mults{imult},mtermsize(imult,1),terms0(mterms(imult,1),:))
end


function [terms termsize]=getterms(beam,qm16ind,termsize0)
global BEAMLINE
persistent ipind

if isempty(ipind)
  ipind=findcells(BEAMLINE,'Name','IP');
end
[stat bo]=TrackThru(qm16ind(1),ipind,beam,1,1,0);
[terms,fitCoef,bsizecor,termsize] = beamTerms(3,bo);
if exist('termsize0','var')
  termsize=termsize0-termsize;
  [a b]=sort(termsize,'descend');
  terms=b(1:3);
  termsize=a(1:3);
end

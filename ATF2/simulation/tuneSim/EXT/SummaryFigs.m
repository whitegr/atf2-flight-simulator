load result0
SigY=result.SigY;
EmitY=result.EmitY;

load 'state0/results_a'
results0=results;
load 'state1/results_a'
results1=results;
load 'state2/results_a'
results2=results;
load 'state3/results19a'
results3=results;
load 'state4/results19a'
results4=results;

figure(1)
clf,subplot
subplot(221)
semilogy(1e9*[results0.SigY],'b+')
set(gca,'YLim',[1e1,1e5],'YTick',[1e1,1e2,1e3,1e4,1e5])
hor_line(1e9*SigY,'g')
ylabel('\sigma_y* (nm)')
title('no correction')
subplot(222)
semilogy(1e9*[results1.SigY],'b+')
set(gca,'YLim',[1e1,1e5],'YTick',[1e1,1e2,1e3,1e4,1e5])
hor_line(1e9*SigY,'g')
title('launch')
subplot(223)
%plot(1e9*[results1.SigY],'b+')
semilogy(1e9*[results1.SigY],'b+')
set(gca,'YLim',[1e1,1e5],'YTick',[1e1,1e2,1e3,1e4,1e5])
hor_line(1e9*SigY,'g')
ylabel('\sigma_y* (nm)')
xlabel('seed #')
title('steer/launch')
subplot(224)
%plot(1e9*[results2.SigY],'b+')
semilogy(1e9*[results2.SigY],'b+')
set(gca,'YLim',[1e1,1e5],'YTick',[1e1,1e2,1e3,1e4,1e5])
hor_line(1e9*SigY,'g')
xlabel('seed #')
title('steer/launch/dispersion')

disp(sprintf('%.1f +- %.1f um',mean(1e6*[results0.SigY]),std(1e6*[results0.SigY])))
disp(sprintf('%.1f +- %.1f nm',mean(1e9*[results1.SigY]),std(1e9*[results1.SigY])))
disp(sprintf('%.1f +- %.1f nm',mean(1e9*[results2.SigY]),std(1e9*[results2.SigY])))
disp(sprintf('%.1f +- %.1f nm',mean(1e9*[results3.SigY]),std(1e9*[results3.SigY])))

figure(2)
clf,subplot
subplot(221)
semilogy(1e9*[results0.EmitY],'b+')
set(gca,'YLim',[5e0,1e6],'YTick',[1e1,1e2,1e3,1e4,1e5,1e6])
hor_line(1e9*EmitY,'g')
ylabel('\gamma\epsilon_y (nm)')
title('no correction')
subplot(222)
semilogy(1e9*[results1.EmitY],'b+')
set(gca,'YLim',[5e0,1e6],'YTick',[1e1,1e2,1e3,1e4,1e5,1e6])
hor_line(1e9*EmitY,'g')
title('launch')
subplot(223)
semilogy(1e9*[results1.EmitY],'b+')
set(gca,'YLim',[5e0,1e6],'YTick',[1e1,1e2,1e3,1e4,1e5,1e6])
hor_line(1e9*EmitY,'g')
ylabel('\gamma\epsilon_y (nm)')
xlabel('seed #')
title('steer/launch')
subplot(224)
semilogy(1e9*[results2.EmitY],'b+')
set(gca,'YLim',[5e0,1e6],'YTick',[1e1,1e2,1e3,1e4,1e5,1e6])
hor_line(1e9*EmitY,'g')
xlabel('seed #')
title('steer/launch/dispersion')

disp(' ')
disp(sprintf('%.1f +- %.1f um',mean(1e6*[results0.EmitY]),std(1e6*[results0.EmitY])))
disp(sprintf('%.1f +- %.1f nm',mean(1e9*[results1.EmitY]),std(1e9*[results1.EmitY])))
disp(sprintf('%.1f +- %.1f nm',mean(1e9*[results2.EmitY]),std(1e9*[results2.EmitY])))
disp(sprintf('%.1f +- %.1f nm',mean(1e9*[results3.EmitY]),std(1e9*[results3.EmitY])))

figure(3)
clf,subplot
subplot(121)
hist(1e9*[results3.SigY],20)
set(gca,'XLim',[30,60])
ver_line(1e9*SigY,'g')
xlabel('\sigma_y* (nm)')
title('steer/launch/dispersion')
subplot(122)
hist(1e9*[results4.SigY],20)
set(gca,'XLim',[30,60])
ver_line(1e9*SigY,'g')
xlabel('\sigma_y* (nm)')
title('steer/launch/dispersion/coupling')

disp(' ')
disp(sprintf('%.1f +- %.1f um',mean(1e9*[results3.SigY]),std(1e9*[results3.SigY])))
disp(sprintf('%.1f +- %.1f nm',mean(1e9*[results4.SigY]),std(1e9*[results4.SigY])))

figure(4)
clf,subplot
subplot(121)
hist(1e9*[results3.EmitY],20)
set(gca,'XLim',[25,60])
ver_line(1e9*EmitY,'g')
xlabel('\gamma\epsilon_y (nm)')
title('steer/launch/dispersion')
subplot(122)
hist(1e9*[results4.EmitY],20)
set(gca,'XLim',[25,60])
ver_line(1e9*EmitY,'g')
xlabel('\gamma\epsilon_y (nm)')
title('steer/launch/dispersion/coupling')

disp(' ')
disp(sprintf('%.1f +- %.1f um',mean(1e9*[results3.EmitY]),std(1e9*[results3.EmitY])))
disp(sprintf('%.1f +- %.1f nm',mean(1e9*[results4.EmitY]),std(1e9*[results4.EmitY])))

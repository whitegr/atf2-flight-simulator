qL=0.07867;
qI=[-20.0,-16.0,-12.0, -8.0, -4.0,  0.0,  4.0,  8.0, 12.0, 16.0, 20.0]; % amp
qG=[-5.662,-4.522,-3.385,-2.247,-1.115, 0.000, 1.115, 2.247, 3.385, 4.522, 5.662]; % T/m

figure
plot(qI,qG*qL,'o--')
axis([-21,21,-0.5,0.5])
grid
title('ATF$MAG:MAG\_KI\_Q\_IDX\_SKEW.FOR')
ylabel('GL (T)')
xlabel('I (amp)')

figure
Cb=1e9/2.99792458e8; % T-m/GeV
Brho=Cb*1.3; % T-m
qKL=qG*qL/Brho;
plot(qI,qKL,'o--')
axis([-21,21,-0.11,0.11])
grid
title('ATF$MAG:MAG\_KI\_Q\_IDX\_SKEW.FOR')
ylabel('KL @ 1.3 GeV (1/m)')
xlabel('I (amp)')

function K1L=DRxSkew(errv)
%
% Apply uniformly distributed random skew strengths to DR extraction devices

global BEAMLINE PS

Brho=1.3/0.299792458; % Tesla-m

msim=findcells(BEAMLINE,'Name','IEX');
idm=findcells(BEAMLINE,'Class','MULT');
idm=idm(idm<msim);

K1L=zeros(size(idm));
for n=1:length(idm)
  K1L(n)=errv*(2*rand-1);
  m=BEAMLINE{idm(n)}.PS;
  PS(m).Ampl=Brho*K1L(n); % Tesla
end

end

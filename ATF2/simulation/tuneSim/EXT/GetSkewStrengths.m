function SkewK1L=GetSkewStrengths()
%
% return K1L (1/m) for EXT skew quads

global BEAMLINE PS

SkewNames=[ ...
  {'QS1X'}; ...
  {'QS2X'}; ...
  {'QK1X'}; ...
  {'QK2X'}; ...
  {'QK3X'}; ...
  {'QK4X'}; ...
];

SkewK1L=zeros(size(SkewNames));
for n=1:length(SkewNames)
  id=findcells(BEAMLINE,'Name',char(SkewNames(n)));
  BeamEnergy=BEAMLINE{id(1)}.P;
  Brho=BeamEnergy/0.299792458; % T-m
  jd=BEAMLINE{id(1)}.PS;
  SkewK1L(n)=PS(jd).SetPt/Brho;
end

return

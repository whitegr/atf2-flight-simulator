function result=SteerAndTuneATF2(state,seed,PlotFlag)

global BEAMLINE PS

load ATF2Perfect

DoEtaX=any([EtaX.Channel.Coefficient]);
DoEtaPX=any([EtaPX.Channel.Coefficient]);
DoEtaY=any([EtaY.Channel.Coefficient]);
nSteer=2;
nEta=1;
nLaunch=3;

% states:
%
%   -1 = perfect machine
%    0 = misalign only
%    1 = misalign and launch
%    2 = misalign, steer flat, and launch
%    3 = misalign, steer flat, correct dispersion, and launch
%    4 = misalign, steer flat, correct dispersion, correct coupling, and launch

% define the error magnitudes

DRxSkewK1Lv=0; %0.0175; % <normalized vertical emittance> = 60 nm
BendOffs=[0,0,100e-6,0,0,0];
QuadOffs=[50e-6,0,30e-6,0,0,300e-6];

% seed the rng's

rand('state',seed)
randn('state',seed)

% set the errors

if (DRxSkewK1Lv~=0)
  DRxSkewK1L=DRxSkew(DRxSkewK1Lv);
else
  DRxSkewK1L=[];
end

if (state>=0)
  zvec=[0,0,0,0,0,0];
  if (any(BendOffs))
    [iss,errv]=ErrorGroupGaussErrors(BendMisalign,zvec,BendOffs,zvec);
  end
  if (any(QuadOffs))
    [iss,errv]=ErrorGroupGaussErrors(QuadMisalign,zvec,QuadOffs,zvec);
  end
end

switch state
  case 1 % launch into FF
    for n=1:nLaunch
      DoATF2Launch(beamsparse,BPMLaunchStruc);
    end
  case 2 % steer flat and launch into FF
    for n=1:nSteer
      DoATF2Steer(beamsparse,BPMSteerStruc);
    end
    for n=1:nLaunch
      DoATF2Launch(beamsparse,BPMLaunchStruc);
    end
  case 3 % steer flat, correct dispersion, and launch into FF
    for n=1:nSteer
      DoATF2Steer(beamsparse,BPMSteerStruc);
    end
    for n=1:nEta
      EtaVector=MeasureDispersionOnBPMs(MeasEtaStruc,EtaBeams);
      if (DoEtaX),iss=IncrementMultiKnob('EtaX', -EtaVector(1));end
      if (DoEtaPX),iss=IncrementMultiKnob('EtaPX',-EtaVector(2));end
      if (DoEtaY),iss=IncrementMultiKnob('EtaY', -EtaVector(3));end
    end
    for n=1:nLaunch
      DoATF2Launch(beamsparse,BPMLaunchStruc);
    end
  case 4 % steer flat, correct coupling, and launch into FF
    for n=1:nSteer
      DoATF2Steer(beamsparse,BPMSteerStruc);
    end
    for n=1:nEta
      EtaVector=MeasureDispersionOnBPMs(MeasEtaStruc,EtaBeams);
      if (DoEtaX),iss=IncrementMultiKnob('EtaX', -EtaVector(1));end
      if (DoEtaPX),iss=IncrementMultiKnob('EtaPX',-EtaVector(2));end
      if (DoEtaY),iss=IncrementMultiKnob('EtaY', -EtaVector(3));end
    end
    [iss1,opt]=FindOptimumSkewQuadSetting(Skew1,Emat,beamsparse);
    if (iss1{1}~=1),disp('Skew1 scan failed'),end
    iss=SetMultiKnob('Skew1',opt);
    [iss2,opt]=FindOptimumSkewQuadSetting(Skew2,Emat,beamsparse);
    if (iss2{1}~=1),disp('Skew2 scan failed'),end
    iss=SetMultiKnob('Skew2',opt);
    [iss3,opt]=FindOptimumSkewQuadSetting(Skew3,Emat,beamsparse);
    if (iss3{1}~=1),disp('Skew3 scan failed'),end
    iss=SetMultiKnob('Skew3',opt);
    [iss4,opt]=FindOptimumSkewQuadSetting(Skew4,Emat,beamsparse);
    if (iss4{1}~=1),disp('Skew4 scan failed'),end
    iss=SetMultiKnob('Skew4',opt);
    if ((iss1{1}~=1)|(iss2{1}~=1)|(iss3{1}~=1)|(iss4{1}~=1))
      disp('Coupling correction did not converge')
    end
    for n=1:nLaunch
      DoATF2Launch(beamsparse,BPMLaunchStruc);
    end
end
  
% get the orbit and dispersion from tracking

[S,X,Y,DX,DPX,DY,DPY]=GetMyData(1,IP,MyBeams);

% track the dense beam and record beam properties at the IP  
  
[iss,beamout]=TrackThru(1,IP,beamdense,1,1,0);
[cen,sig]=GetBeamPars(beamout,1);
SigX=sqrt(sig(1,1));
SigPX=sqrt(sig(2,2));
r21=sig(2,1)/sqrt(sig(1,1)*sig(2,2));
SigY=sqrt(sig(3,3)) ;
SigPY=sqrt(sig(4,4));
r43=sig(4,3)/sqrt(sig(3,3)*sig(4,4));
[EmitX,EmitY,nt]=GetNEmitFromSigmaMatrix(cen(6),sig);

% create results structure

result.state=state;
result.seed=seed;
result.S=S;
result.X=X;
result.Y=Y;
result.EtaX=DX;
result.EtaPX=DPX;
result.EtaY=DY;
result.EtaPY=DPY;
result.SigX=SigX;
result.SigPX=SigPX;
result.r21=r21;
result.SigY=SigY;
result.SigPY=SigPY;
result.r43=r43;
result.EmitX=EmitX;
result.EmitY=EmitY;
result.SkewK1L=GetSkewStrengths();
result.DRxSkewK1L=DRxSkewK1L;

% make plots

if (PlotFlag)
  MyPlot(S,X,Y,DX,DY,1,IP,1)
  MyPlot(S,X,Y,DX,DY,diagbeg,diagend,1)
end

return

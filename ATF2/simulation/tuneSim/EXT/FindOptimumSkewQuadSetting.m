function [stat,knob] = FindOptimumSkewQuadSetting( Model, knob, Emat, beam, varargin ) 

debug=0;

% scan a skew quad knob, measure emittance, find the optimum emittance
% value

global INSTR FL

stat = InitializeMessageStack ;
nRetryMax = 10 ;

if (nargin > 4)
  ScanStepsOrig = varargin{2} ;
else
  ScanStepsOrig = [-1 -0.5 0 0.5 1] ;
end
ScanSteps = ScanStepsOrig ;
ScanStepsJump = ScanSteps ;
goodscan = 0 ; nrepeat = 0 ; opt = 0 ;

% scan the knob and get the beam sizes on each wire scanner -- here we have
% a row for each measurement and a column for each wire

% Get dispersion values at wirescanners
[ipd Model] = IP_meas( {FL.SimBeam{1} FL.SimBeam{2}}, FL.SimModel ) ; FL.SimModel=Model;

while (goodscan == 0)

  nrepeat = nrepeat + 1 ;
  WireSig33 = [] ; %sy=[];
  for count = ScanSteps
    SetMultiKnob('knob',count) ;
    FL.simBeamID=1; FL.SimModel=Model;
    FlHwUpdate;
    WireSig33 = [WireSig33 ; ...
      INSTR{Model.ext.mw_iind(1)}.Data(5) ...
      INSTR{Model.ext.mw_iind(2)}.Data(5) ...
      INSTR{Model.ext.mw_iind(3)}.Data(5) ...
      INSTR{Model.ext.mw_iind(4)}.Data(5) ...
      INSTR{Model.ext.mw_iind(5)}.Data(5)     ] ;
%     sy=[sy INSTR{end}.Data(5)];
  end

  RestoreMultiKnob('knob') ;

  % remove dispersive beam size component and flip the wire size matrix
%   WireSig33 = WireSig33-FL.SimModel.Initial.SigPUncorrel.^2.*repmat(ipd.DispWS_y,5,1).^2;
  WireSig33 = WireSig33' ;

  % get the least-squares sigma matrix at the first wire scanner on each
  % step of the knob

  sig0 =  Emat \ WireSig33 ;

  % compute the resulting emittance as a function of the knob

  emit2y = sig0(1,:).*sig0(3,:) - sig0(2,:).*sig0(2,:) ;
  emity = sqrt(emit2y) ;

  % convert to normalized emittances in nm, which I'm more used to

  emity = 1e9 * emity * 1.3/0.000511 ;

  % fit a parabola to this thing
%   emity(logical(imag(emity(:))))=0;
  % error if imaginary emittance
  if any(logical(imag(emity(:)))); error('Imaginary emittance calculated'); end;
  [A,B,C] = parabola_fit( ScanSteps, emity, 0 ) ;
  if (debug) %nargin > 3)
    figure
    plot(ScanSteps,emity,'o') ;
    hold on
    dx=(max(ScanSteps)-min(ScanSteps))/100;
    x=min(ScanSteps):dx:max(ScanSteps);
    plot(x,C(1)+A(1)*(x-B(1)).^2,'r--') ;
    ver_line(B(1),'r-')
  end

  % if this was the first scan and the optimum value is within the scan
  % range, accept it.  If the optimum was outside the scan range, double the
  % scan range.  If the optimum was inside the scan range but this scan was
  % not the first one, do a final scan which uses the original range but is
  % centered on the optimum value.
   
  if ( (B(1)>min(ScanSteps)) && (B(1)<max(ScanSteps)) )
    opt = B(1) ;
    if (nrepeat == 1)
      goodscan = 1 ;
    else
      nrepeat = 0 ;
      ScanSteps = ScanStepsOrig + opt ;
      ScanStepsJump = ScanStepsOrig ;
    end
  else
    ScanStepsJump = 2 * ScanStepsJump ;
    ScanSteps = ScanStepsJump + opt ;
  end
  if (nrepeat > nRetryMax)
    stat{1} = 0 ;
    stat = AddMessageToStack(stat,...
      ['Emittance scan failed to converge after ',num2str(nRetryMax), ...
      ' attempts.']) ;
    disp('   *** Emittance scan failed ***')
    return ;
  end

end
SetMultiKnob('knob',opt) ;
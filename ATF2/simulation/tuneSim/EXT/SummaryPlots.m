function SummaryPlots(results)

load ATF2Perfect
ip=findcells(BEAMLINE,'Name','IP');

load result0
SigY0=1e9*result.SigY;
EmitY0=1e9*result.EmitY;
clear result

Nresult=length(results);

% orbit plot

S=[results(1).S];
X=[];
Y=[];
for n=1:Nresult
  X=[X,[results(n).X]];
  Y=[Y,[results(n).Y]];
end

figure
clf,subplot
id=[1:ip]';
subplot(211)
plot(S(id),1e6*X(id,:))
set(gca,'XLim',[0,S(ip)])
hor_line(0,'k:')
ylabel('X Orbit (\mum)')
subplot(212)
plot(S(id),1e6*Y(id,:))
set(gca,'XLim',[0,S(ip)])
hor_line(0,'k:')
ylabel('Y Orbit (\mum)')
xlabel('S (m)')

if (Nresult==1), return, end

% SigY and EmitY plots

SigY=1e9*[results.SigY]';
EmitY=1e9*[results.EmitY]';
figure
clf,subplot
subplot(221)
plot(SigY,'b+')
ylabel('\sigma^*_y (nm)')
xlabel('Seed Number')
set(gca,'XLim',[0,Nresult])
hor_line(SigY0,'g-')
[temp,id]=sort(SigY,'ascend');
disp(sprintf('   SigY(90%%) = %.1f',temp(0.9*Nresult)))
subplot(222)
hist(SigY,20)
ver_line(SigY0,'g-')
xlabel('\sigma^*_y (nm)')
title(sprintf('%.1f \\pm %.1f',mean(SigY),std(SigY)))
subplot(223)
plot(EmitY,'b+')
ylabel('\gamma\epsilon_y (nm)')
xlabel('Seed Number')
set(gca,'XLim',[0,Nresult])
hor_line(EmitY0,'g-')
[temp,id]=sort(EmitY,'ascend');
disp(sprintf('   EmitY(90%%) = %.1f',temp(0.9*Nresult)))
subplot(224)
hist(EmitY,20)
ver_line(EmitY0,'g-')
xlabel('\gamma\epsilon_y (nm)')
title(sprintf('%.1f \\pm %.1f',mean(EmitY),std(EmitY)))

% skew quad current histograms

KL=[results.SkewK1L]';
if (~isempty(find(any(KL)~=0)))

  % MMS data for quad types (I is amp, G is T/m)
  % (from: ATF$MAG:MAG_KI_Q_IDX_SKEW.FOR)

  qL=0.07867;
  qI=[-20.0  ,-16.0  ,-12.0  ,-8.0  ,-4.0  ,0.0  ,4.0  ,8.0  ,12.0  ,16.0  ,20.0  ];
  qG=[ -5.662, -4.522, -3.385,-2.247,-1.115,0.000,1.115,2.247, 3.385, 4.522, 5.662];

  Brho=Initial.Momentum/0.299792458; % Tesla-m
  [C,dq]=noplot_polyfit(qG*qL/Brho,qI,1,[0,1]); % KL-to-I
  qname=['QS1X';'QS2X';'QK1X';'QK2X';'QK3X';'QK4X'];
  Imax=[5;5;20;5;5;20]; % maximum operating currents
  I=C*KL;

  figure
  clf,subplot
  for n=1:6
    subplot(3,2,n)
    hist(I(:,n),20)
    if ((min(I(:,n))<-Imax(n))|(max(I(:,n))>Imax(n)))
      ver_line(Imax(n)*[-1,1],'r-')
    else
      set(gca,'XLim',Imax(n)*[-1,1])
    end
    title(qname(n,:))
    if (n>4),xlabel('Current (amp)'),end
  end
end

% normalized vertical dispersion plots

ml9x=findcells(BEAMLINE,'Name','ML9X');
me=0.51099906e-3;
egamma=Initial.Momentum/me;
emity=Initial.y.NEmit/egamma;
bety=T.betay(ml9x);
alfy=T.alphay(ml9x);
sigdp=Initial.SigPUncorrel;
const=sigdp/sqrt(emity*bety);
x=zeros(Nresult,1);
y=zeros(Nresult,1);
for n=1:Nresult
  dy=results(n).EtaY(ml9x);
  dpy=results(n).EtaPY(ml9x);
  x(n)=const*dy;
  y(n)=const*(bety*dpy+alfy*dy);
end
v=max([max(abs(x)),max(abs(y))]);
coef=polyfit(x,y,1);
xf=[-v;v];
yf=polyval(coef,xf);

load DYKnobScan
xs=const*DY;
ys=const*(bety*DPY+alfy*DY);

figure
plot(x,y,'o',xs,ys,'r-',xf,yf,'b--')
axis([-v,v,-v,v])
axis square
hor_line(0,'k:')
ver_line(0,'k:')
title('ML9X')
ylabel('\sigma_{\delta}(\beta_y\eta^''_y+\alpha_y\eta_y)/(\epsilon_{y0}\beta_y)^{1/2}')
xlabel('\sigma_{\delta}\eta_y/(\epsilon_{y0}\beta_y)^{1/2}')

end
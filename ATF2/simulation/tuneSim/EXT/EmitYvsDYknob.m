load ATF2Perfect
ML9X=findcells(BEAMLINE,'Name','MQF13X');

DYknob=0.001*[-10:10];
N=length(DYknob);

results=[];
for n=1:N
  if (DYknob(n)~=0)
    iss=SetMultiKnob('EtaY',DYknob(n));
  end
  [iss,beamout]=TrackThru(1,ML9X,beamdense,1,1,0);

  x=beamout.Bunch.x(1,:)';
  px=beamout.Bunch.x(2,:)';
  y=beamout.Bunch.x(3,:)';
  py=beamout.Bunch.x(4,:)';
  dp=(beamout.Bunch.x(6,:)'-Initial.Momentum)/Initial.Momentum;
  coef=polyfit(dp,x,1);DX=coef(1);
  coef=polyfit(dp,px,1);DPX=coef(1);
  coef=polyfit(dp,y,1);DY=coef(1);
  coef=polyfit(dp,py,1);DPY=coef(1);
  x0=x-DX*dp;
  px0=px-DPX*dp;
  y0=y-DY*dp;
  py0=py-DPY*dp;

  sig0=cov([x0,px0,y0,py0]);
  J=[0,1;-1,0];
  J=[J,zeros(2,2);zeros(2,2),J];
  ex=sqrt(det(sig0(1:2,1:2)));
  ey=sqrt(det(sig0(3:4,3:4)));
  t2=trace((sig0*J)^2);
  t4=trace((sig0*J)^4);
  e1=sqrt((-t2+sqrt(-t2^2+4*t4))/4);
  e2=sqrt((-t2-sqrt(-t2^2+4*t4))/4);

  result.DYknob=DYknob(n);
  result.DX=DX;
  result.DPX=DPX;
  result.DY=DY;
  result.DPY=DPY;
  result.ex=ex;
  result.ey=ey;
  result.e1=e1;
  result.e2=e2;
  results=[results;result];
  
  if (DYknob(n)==0)
    ex0=ex;
    ey0=ey;
    e10=e1;
    e20=e2;
  end
 
  if (DYknob(n)~=0)
    iss=RestoreMultiKnob('EtaY');
  end
end

figure
subplot(221)
plot(1e3*[results.DYknob]',[results.e1]'/e10,'o--')
axis([-11,11,0.999,1.025])
hor_line(0,'g-')
ver_line(0,'k:')
ylabel('\epsilon_1/\epsilon_{10}')
subplot(222)
plot(1e3*[results.DYknob]',[results.e2]'/e20,'o--')
axis([-11,11,0.999,1.025])
hor_line(0,'g-')
ver_line(0,'k:')
ylabel('\epsilon_2/\epsilon_{20}')
subplot(223)
plot(1e3*[results.DYknob]',[results.ex]'/ex0,'o--')
axis([-11,11,0.999,1.025])
hor_line(0,'g-')
ver_line(0,'k:')
ylabel('\epsilon_x/\epsilon_{x0}')
xlabel('\eta_y knob (mm)')
subplot(224)
plot(1e3*[results.DYknob]',[results.ey]'/ey0,'o--')
axis([-11,11,0.999,1.025])
hor_line(0,'g-')
ver_line(0,'k:')
ylabel('\epsilon_y/\epsilon_{y0}')
xlabel('\eta_y knob (mm)')

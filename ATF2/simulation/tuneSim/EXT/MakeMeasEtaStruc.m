function [beams,MeasEtaStruc] = MakeMeasEtaStruc( DeltaVec, Beam ) 

% construct data structure to manage measurement of the dispersion at BPM ML9X,
% using readings from BPMs ML9X through QBPM9X

  global BEAMLINE ;

% start with the beams which are to be used

  beams = [] ;
  for count = DeltaVec
    beamtemp=Beam;
    beamtemp.Bunch.x(6,:) = mean(Beam.Bunch.x(6,:)).*(1+count) ;
    beams = [beams ; makeSingleBunch(beamtemp)] ;
  end

% out of the BPMs, which ones do we want to measure?

  ml9x  = findcells(BEAMLINE,'Name','MQF13X') ;
  qbpm9x = findcells(BEAMLINE,'Name','MQF21X') ;
  
  SwitchedOnBPMs = findcells(BEAMLINE,'Class','MONI',ml9x,qbpm9x) ;
  firsteta = find(SwitchedOnBPMs == ml9x) ;
  EtaBPMs = linspace(firsteta,length(SwitchedOnBPMs),...
                     length(SwitchedOnBPMs)-firsteta+1) ;
  LastBPMElem = SwitchedOnBPMs(end) ;
  
% now construct the data structure:  first the vector of deltas  
  
  MeasEtaStruc.DeltaVec = DeltaVec ;
  MeasEtaStruc.EtaBPMs  = SwitchedOnBPMs(EtaBPMs) ;
  nEtaBPMs = length(MeasEtaStruc.EtaBPMs) ;
  
% now the matrix which transforms dispersions into dPos/dDelta slopes
  
  EtaMatrix = zeros(2*nEtaBPMs,4) ;
  BPMelem1 = SwitchedOnBPMs(firsteta) ;
  for count = 1:nEtaBPMs
      
      BPMelem2 = SwitchedOnBPMs(EtaBPMs(count)) ;
      [stat,R] = RmatAtoB(BPMelem1,BPMelem2) ;
      EtaMatrix(count,1:4) = R(1,1:4) ;
      EtaMatrix(count+nEtaBPMs,1:4) = R(3,1:4) ;
      
  end
  MeasEtaStruc.EtaMatrix = EtaMatrix ;
  MeasEtaStruc.LastElem = LastBPMElem ;
  
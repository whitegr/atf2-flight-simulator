function MyPlot(S,x,y,etax,etay,istart,iend,BPMonly)

global BEAMLINE

if (BPMonly)
  id=findcells(BEAMLINE,'Class','MONI');
  n=find(id>=istart&id<=iend);
  id=id(n);
  style='bo--';
else
  id=istart:iend;
  style='b-';
end

figure
clf,subplot
subplot(211)
plot(S(id),1e3*x(id),style)
hor_line(0,'k:')
ylabel('X (mm)')
subplot(212)
plot(S(id),1e3*y(id),style)
hor_line(0,'k:')
ylabel('Y (mm)')
xlabel('S (m)')
[h10,h11]=AddMagnetPlot(istart,iend);

figure
clf,subplot
subplot(211)
plot(S(id),etax(id),style)
hor_line(0,'k:')
ylabel('Eta X (m)')
subplot(212)
plot(S(id),etay(id),style)
hor_line(0,'k:')
ylabel('Eta Y (m)')
xlabel('S (m)')
[h20,h21]=AddMagnetPlot(istart,iend);

return

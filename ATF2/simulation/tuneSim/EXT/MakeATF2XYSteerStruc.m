function steerstruc = MakeATF2XYSteerStruc( )
%
% generate structures in support of ATF2 steering.
%

%=======================================================================

  global BEAMLINE PS ;
  
  ml1x   = findcells(BEAMLINE,'Name','MQF7X') ;
  qbpm9x = findcells(BEAMLINE,'Name','MQF9X') ;

  zs3x   = findcells(BEAMLINE,'Name','ZS3X') ;
  zh10x  = findcells(BEAMLINE,'Name','ZH10X') ;
  
  zv1x   = findcells(BEAMLINE,'Name','ZV1X') ;
  zv11x  = findcells(BEAMLINE,'Name','ZV11X') ;
  
  bpms = findcells(BEAMLINE,'Class','MONI',ml1x,qbpm9x) ;
  xcor = findcells(BEAMLINE,'Class','XCOR',zs3x,zh10x) ;
  ycor = findcells(BEAMLINE,'Class','YCOR',zv1x,zv11x) ;
  
  
  steerstruc = [] ;
  brho = 1.3 / 0.299792458 ; % T.m
  nbpm = length(bpms) ; nxcor = length(xcor) ; nycor = length(ycor) ;
    
% generate the appropriate matrix (the one that generates BPM values from
% corrector values).  The matrix will be in the form:
%
%       [xcor->xbpm ycor->xbpm ; xcor->ybpm ycor->ybpm] ;

    xfermat = zeros(2*length(bpms)+4,nxcor+nycor) ;
    
    for bpmcount = 1:nbpm
      bpmelem = bpms(bpmcount) ;
      for xcorcount = 1:nxcor
        xcorelem = xcor(xcorcount) ;
        if (xcorelem < bpmelem)
          [stat,R] = RmatAtoB(xcorelem,bpmelem) ;
          xfermat(bpmcount,xcorcount) = R(1,2) ;
          xfermat(bpmcount+nbpm,xcorcount) = R(3,2) ;
        end
      end
      for ycorcount = 1:nycor
        ycorelem = ycor(ycorcount) ;
        if (ycorelem < bpmelem)
          [stat,R] = RmatAtoB(ycorelem,bpmelem) ;
          xfermat(bpmcount,nxcor+ycorcount) = R(1,4) ;
          xfermat(bpmcount+nbpm,nxcor+ycorcount) = R(3,4) ;
        end
      end
    end    
    
% get the power supplies and the matrix elements to the begff marker

    begff = findcells(BEAMLINE,'Name','BEGFF') ;

    xcorps = [] ; 
    for ccount = 1:nxcor
        count = xcor(ccount) ;
        xcorps = [xcorps BEAMLINE{count}.PS] ;
        [stat,R] = RmatAtoB(count,begff) ;
        xfermat(2*nbpm+1,ccount) = R(1,2) ;
        xfermat(2*nbpm+2,ccount) = R(2,2) ;
        xfermat(2*nbpm+3,ccount) = R(3,2) ;
        xfermat(2*nbpm+4,ccount) = R(4,2) ;
    end
    ycorps = [] ; 
    for ccount = 1:nycor
        count = ycor(ccount) ;
        ycorps = [ycorps BEAMLINE{count}.PS] ;
        [stat,R] = RmatAtoB(count,begff) ;
        xfermat(2*nbpm+1,nxcor+ccount) = R(1,4) ;
        xfermat(2*nbpm+2,nxcor+ccount) = R(2,4) ;
        xfermat(2*nbpm+3,nxcor+ccount) = R(3,4) ;
        xfermat(2*nbpm+4,nxcor+ccount) = R(4,4) ;
    end
        
% convert from meters per radian to meters per T.m

    xfermat = xfermat / brho ;
    
% attach the values to the data structure

    steerstruc.ps = [xcorps ycorps] ;
    steerstruc.xfermat = xfermat ;
    steerstruc.lastelem = begff ;
    
  end
        
        
function [S,x,y,etax,etapx,etay,etapy]=GetMyData(istart,iend,MyBeams)
%
% get orbit and dispersion data from tracking

global BEAMLINE

b0=MyBeams(1);
bm=MyBeams(2);
bp=MyBeams(3);

S=BEAMLINE{istart}.S;
[cen0,sig]=GetBeamPars(b0,1);
[cenm,sig]=GetBeamPars(bm,1);
[cenp,sig]=GetBeamPars(bp,1);
x=cen0(1);
y=cen0(3);
ddp=(cenp(6)-cenm(6))/cen0(6);
etax=(cenp(1)-cenm(1))/ddp;
etapx=(cenp(2)-cenm(2))/ddp;
etay=(cenp(3)-cenm(3))/ddp;
etapy=(cenp(4)-cenm(4))/ddp;
for n=istart:iend
  [iss,b0]=TrackThru(n,n,b0,1,1,0);
  [iss,bm]=TrackThru(n,n,bm,1,1,0);
  [iss,bp]=TrackThru(n,n,bp,1,1,0);
  if (isfield(BEAMLINE{n},'L'))
    S=[S;BEAMLINE{n}.S+BEAMLINE{n}.L];
  else
    S=[S;BEAMLINE{n}.S];
  end
  [cen0,sig]=GetBeamPars(b0,1);
  [cenm,sig]=GetBeamPars(bm,1);
  [cenp,sig]=GetBeamPars(bp,1);
  x=[x;cen0(1)];
  y=[y;cen0(3)];
  etax=[etax;(cenp(1)-cenm(1))/ddp];
  etapx=[etapx;(cenp(2)-cenm(2))/ddp];
  etay=[etay;(cenp(3)-cenm(3))/ddp];
  etapy=[etapy;(cenp(4)-cenm(4))/ddp];
end

return

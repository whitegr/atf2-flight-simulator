function EtaVector = MeasureDispersionOnBPMs( Model, MeasEtaStruc, EtaBeams )

% function which measures the dispersion (dOrbit/dEnergy) on several
% BPMs and returns the least-squares fitted [etax ; etapx ; etay ; etapy]
% on the first of those BPMs.
global INSTR FL

BPMXData = [] ; BPMYData = [] ; bpmind=[];
for ibpm=MeasEtaStruc.EtaBPMs
  bpmind=[bpmind findcells(INSTR,'Index',ibpm)];
end % for ibpm
for count = 1:length(MeasEtaStruc.DeltaVec)
  FL.SimBeam=EtaBeams(count); FL.SimModel=Model;
  FlHwUpdate;
  for ibpm=1:length(bpmind)
    BPMXData(ibpm,count)=INSTR{bpmind(ibpm)}.Data(1);
    BPMYData(ibpm,count)=INSTR{bpmind(ibpm)}.Data(2);
  end % for ibpm
end

Xslope = [] ; Yslope = [] ;
for count = 1:length(MeasEtaStruc.EtaBPMs)
  P = polyfit(MeasEtaStruc.DeltaVec,BPMXData(count,:),1) ;
  Xslope = [Xslope ; P(1)] ;
  P = polyfit(MeasEtaStruc.DeltaVec,BPMYData(count,:),1) ;
  Yslope = [Yslope ; P(1)] ;
end

% fit the dispersions [etax ; etapx ; etay ; etapy] 

EtaVector = MeasEtaStruc.EtaMatrix \ [Xslope ; Yslope] ;

load ATF2Perfect
ML9X=findcells(BEAMLINE,'Name','MQF13X');

seeds=load('xseeds.dat');
Nseed=length(seeds);

DRxSkewK1Lv=0.0175;

results=[];
for n=1:Nseed
 %if (rem(n,100)==0),disp(n),end
  rand('state',seeds(n))
  randn('state',seeds(n))
  DRxSkewK1L=DRxSkew(DRxSkewK1Lv);
  if ~Model.reality
    [iss,beamout]=TrackThru(1,ML9X,beamdense,1,1,0);
  else
    [iss,beamout]=TrackThruaper(Model,1,ML9X,1,1,0);
  end

  x=beamout.Bunch.x(1,:)';
  px=beamout.Bunch.x(2,:)';
  y=beamout.Bunch.x(3,:)';
  py=beamout.Bunch.x(4,:)';
  dp=(beamout.Bunch.x(6,:)'-Initial.Momentum)/Initial.Momentum;
  coef=polyfit(dp,x,1);DX=coef(1);
  coef=polyfit(dp,px,1);DPX=coef(1);
  coef=polyfit(dp,y,1);DY=coef(1);
  coef=polyfit(dp,py,1);DPY=coef(1);
  x0=x-DX*dp;
  px0=px-DPX*dp;
  y0=y-DY*dp;
  py0=py-DPY*dp;

  sig0=cov([x0,px0,y0,py0]);
  J=[0,1;-1,0];
  J=[J,zeros(2,2);zeros(2,2),J];
  ex=sqrt(det(sig0(1:2,1:2)));
  ey=sqrt(det(sig0(3:4,3:4)));
  t2=trace((sig0*J)^2);
  t4=trace((sig0*J)^4);
  e1=sqrt((-t2+sqrt(-t2^2+4*t4))/4);
  e2=sqrt((-t2-sqrt(-t2^2+4*t4))/4);

  result.DRxSkewK1L=DRxSkewK1L;
  result.DX=DX;
  result.DPX=DPX;
  result.DY=DY;
  result.DPY=DPY;
  result.ex=ex;
  result.ey=ey;
  result.e1=e1;
  result.e2=e2;
  results=[results;result];
end

function DoATF2Launch( beam, struc )

% function which performs the launch from EXT into FF

  global PS INSTR

% track the beam to each BPM

  nbpms = length(struc.elements) ;
  bpmdata = zeros(2*nbpms,1) ;
  id2 = struc.elements(n);
  FL.SimBeam=beam; FL.SimModel=Model; FL.SimModel.ip_ind=id2;
  FlHwUpdate;
  Model.bpmData.x=cellfun(@(x) x.Data(1),INSTR); Model.bpmData.y=cellfun(@(x) x.Data(2),INSTR);
  for n = 1:nbpms
    bpmdata(n) = x(1) ;
    bpmdata(nbpms+n) = x(3) ;
    id1 = id2+1 ;
  end
  
% compute the correction (actually, the opposite of the correction, since
% we are solving the matrix BPMs = R * corrs)

  Correctors = struc.xfermat \ bpmdata ;
  
% apply the correction to the power supplies

  for count = 1:length(Correctors)
    PS(struc.ps(count)).SetPt = PS(struc.ps(count)).SetPt - Correctors(count) ;
  end
  
  stat = PSTrim(struc.ps) ;
  

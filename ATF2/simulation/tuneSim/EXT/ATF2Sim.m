
global BEAMLINE PS

seeds=load('seeds.dat');
%seeds=load('xseeds.dat'); % generate 2x vertical emittance blowup (0.0175)
SkewMax=[2.5363e-2;2.5363e-2;10.272e-2;2.5363e-2;2.5363e-2;10.272e-2];

state=4;
Nseed=100;
PlotFlag=0;
verbose=0;

results=[];
disp(' ')
for seednum=1:Nseed
  if (seednum==0)
    seed=1;
  else
    seed=seeds(seednum);
  end

  tic
  result=SteerAndTuneATF2(state,seed,PlotFlag);
  et=toc;

  results=[results;result];
  
  disp(sprintf('   seednum=%3d, state=%2d, seed=%8d, time=%.1f seconds',seednum,state,seed,et))
  if (verbose)
    disp(' ')
    disp(['   SigY(nm)   = ',num2str(1e9*result.SigY)])
    disp(['   EmityY(nm) = ',num2str(1e9*result.EmitY)])
    disp(['   QS1X(K1L)  = ',num2str(result.SkewK1L(1)),' (',num2str(result.SkewK1L(1)/SkewMax(1)),')'])
    disp(['   QS2X(K1L)  = ',num2str(result.SkewK1L(2)),' (',num2str(result.SkewK1L(2)/SkewMax(2)),')'])
    disp(['   QK1X(K1L)  = ',num2str(result.SkewK1L(3)),' (',num2str(result.SkewK1L(3)/SkewMax(3)),')'])
    disp(['   QK2X(K1L)  = ',num2str(result.SkewK1L(4)),' (',num2str(result.SkewK1L(4)/SkewMax(4)),')'])
    disp(['   QK3X(K1L)  = ',num2str(result.SkewK1L(5)),' (',num2str(result.SkewK1L(5)/SkewMax(5)),')'])
    disp(['   QK4X(K1L)  = ',num2str(result.SkewK1L(6)),' (',num2str(result.SkewK1L(6)/SkewMax(6)),')'])
    disp(' ')
  end
end
if (~verbose),disp(' '),end

SummaryPlots(results)

function [S,ex,ey,ez,e1,e2,e3]=GetEmitFromTracking(istart,iend,beam)

global BEAMLINE

S=BEAMLINE{istart}.S;
[x,sig]=GetBeamPars(beam,1);
[ex,ey,ez]=GetNEmitFromSigmaMatrix(x(6),sig);
[e1,e2,e3]=GetNEmitFromSigmaMatrix(x(6),sig,'normalmode');
beamout=beam;
for n=istart:iend
  if ~Model.reality
    [stat,beamout]=TrackThru(n,n,beamout,1,1,0);
  else
    [stat,beamout]=TrackThruaper(Model,n,n,1,1,0);
  end
  if (isfield(BEAMLINE{n},'L'))
    S=[S;BEAMLINE{n}.S+BEAMLINE{n}.L];
  else
    S=[S;BEAMLINE{n}.S];
  end
  [x,sig]=GetBeamPars(beamout,1);
  [exn,eyn,ezn]=GetNEmitFromSigmaMatrix(x(6),sig);
  [e1n,e2n,e3n]=GetNEmitFromSigmaMatrix(x(6),sig,'normalmode');
  ex=[ex;exn];ey=[ey;eyn];ez=[ez;ezn];
  e1=[e1;e1n];e2=[e2;e2n];e3=[e3;e3n];
end

return

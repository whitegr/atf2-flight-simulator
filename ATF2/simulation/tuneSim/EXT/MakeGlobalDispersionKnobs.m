function [EtaX,EtaPX,EtaY]=MakeGlobalDispersionKnobs(Twiss)

% function which generates global dispersion knobs at BPM ML9X

  global BEAMLINE PS

% EtaX,EtaPX -------------------------------------------------------------------

% first find the normal quads of interest

  magnet1=findcells(BEAMLINE,'Name','QF1X');
  magnet2=findcells(BEAMLINE,'Name','QF6X');
  
% find the power supplies of interest

  ps1=BEAMLINE{magnet1(1)}.PS;
  ps2=BEAMLINE{magnet2(1)}.PS;
  
% find the BPM of interest

  BPM=findcells(BEAMLINE,'Name','MQF13X');
  
% construct the matrix which relates the K1L values of the devices to the
% resulting etax and etapx at the BPM

  [stat,R1]=RmatAtoB(magnet1(2),BPM);
  [stat,R2]=RmatAtoB(magnet2(2),BPM);
  
  R=[-R1(1,2)*Twiss.etax(magnet1(2)),-R2(1,2)*Twiss.etax(magnet2(2)); ...
     -R1(2,2)*Twiss.etax(magnet1(2)),-R2(2,2)*Twiss.etax(magnet2(2))];

% compute the knob coefficients in units of K1L per meter (or K1L per
% radian), then convert K1L's to Tesla

  knob1=R\[1;0];
  knob2=R\[0;1];
  brho=Twiss.P(BPM)/0.299792458; % Tesla-m
  knob1=knob1*brho;
  knob2=knob2*brho;
  
% construct the multiknobs
% put in PS units
  knob1(1)=knob1(1).*(BEAMLINE{PS(ps1).Element(1)}.B*2);
  knob1(2)=knob1(2).*(BEAMLINE{PS(ps2).Element(1)}.B*2);
  knob2(1)=knob2(1).*(BEAMLINE{PS(ps1).Element(1)}.B*2);
  knob2(2)=knob2(2).*(BEAMLINE{PS(ps2).Element(1)}.B*2);
  [stat,EtaX]=MakeMultiKnob('EtaX at MQF13X [m]', ...
    ['PS(',num2str(ps1),').SetPt'],knob1(1), ...
    ['PS(',num2str(ps2),').SetPt'],knob1(2));
  [stat,EtaPX]=MakeMultiKnob('EtaX'' at MQF13X [rad]', ...
    ['PS(',num2str(ps1),').SetPt'],knob2(1), ...
    ['PS(',num2str(ps2),').SetPt'],knob2(2));

% EtaY -------------------------------------------------------------------------

% the EtaY knob is a "sum knob" which sets both QS1X and QS2X to the same
% strength in order to generate etay at the BPM without generating x-y
% coupling; etapy is not explicitly constrained, although the EXT optics causes
% it to be tightly correlated to etay ... zeroing etay also zeros etapy

% first find the skew quads of interest

  magnet1=findcells(BEAMLINE,'Name','QS1X');
  magnet2=findcells(BEAMLINE,'Name','QS2X');
  
% find the power supplies of interest

  ps1=BEAMLINE{magnet1(1)}.PS;
  ps2=BEAMLINE{magnet2(1)}.PS;
  
% find the BPM of interest

  BPM=findcells(BEAMLINE,'Name','MQF13X');
  
% construct the value which relates a common K1L value for the devices to etay
% at the BPM

  [stat,R1]=RmatAtoB(magnet1(2),BPM);
  [stat,R2]=RmatAtoB(magnet2(2),BPM);
  
  R=-R1(3,4)*Twiss.etax(magnet1(2))-R2(3,4)*Twiss.etax(magnet2(2));
  
% compute the knob coefficient in units of K1L per meter (or K1L per
% radian)

  knob=1/R;
  
% construct the multiknobs, converting K1L's to Tesla

  brho=Twiss.P(BPM)/0.299792458;
  knob=knob*brho;
  
  [stat,EtaY]=MakeMultiKnob('EtaY at MQF13X [m]', ...
    ['PS(',num2str(ps1),').SetPt'],knob, ...
    ['PS(',num2str(ps2),').SetPt'],knob);

end

global BEAMLINE PS

load ATF2Perfect

% define the error magnitudes

BendOffs=[0,0,100e-6,0,0,0];
QuadOffs=[50e-6,0,30e-6,0,0,300e-6];

% seed the rng's

seed=11729990;
rand('state',seed)
randn('state',seed)

% set the errors

zvec=[0,0,0,0,0,0];
if (any(BendOffs))
  [iss,errv]=ErrorGroupGaussErrors(BendMisalign,zvec,BendOffs,zvec);
end
if (any(QuadOffs))
  [iss,errv]=ErrorGroupGaussErrors(QuadMisalign,zvec,QuadOffs,zvec);
end

% steer flat

nSteer=2;
for n=1:nSteer
  DoATF2Steer(beamsparse,BPMSteerStruc);
end

% get the orbit and dispersion before dispersion correction from tracking

[S,X,Y,DX,DPX,DY,DPY]=GetMyData(1,IP,MyBeams);

nEta=1;
for n=1:nEta
  EtaVector=MeasureDispersionOnBPMs(MeasEtaStruc,EtaBeams);
  iss=IncrementMultiKnob('EtaX', -EtaVector(1));
  iss=IncrementMultiKnob('EtaPX',-EtaVector(2));
  iss=IncrementMultiKnob('EtaY', -EtaVector(3));

% get the orbit and dispersion before dispersion correction from tracking

  [S,Xn,Yn,DXn,DPXn,DYn,DPYn]=GetMyData(1,IP,MyBeams);
  X=[X,Xn];DX=[DX,DXn];DPX=[DPX,DPXn];
  Y=[Y,Yn];DY=[DY,DYn];DPY=[DPY,DPYn];
end

% plot the dispersion

id=[diagbeg:diagend]';
figure(1)
clf,subplot
subplot(211)
plot(S(id),1e3*DX(id,:))
ylabel('DX (mm)')
subplot(212)
plot(S(id),1e3*DY(id,:))
ylabel('DY (mm)')
xlabel('S (m)')

% display the dispersion readings at the target BPM

ml9x=findcells(BEAMLINE,'Name','MQF13X');
for n=1:1+nEta
  disp(sprintf('%10.3f %10.3f %10.3f %10.3f',1e3*[DX(ml9x,n),DPX(ml9x,n),DY(ml9x,n),DPY(ml9x,n)]))
end

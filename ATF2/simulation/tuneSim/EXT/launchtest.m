global BEAMLINE PS

load ATF2Perfect

% define the error magnitudes

BendOffs=[0,0,100e-6,0,0,0];
QuadOffs=[50e-6,0,30e-6,0,0,300e-6];

% seed the rng's

seed=11729990;
rand('state',seed)
randn('state',seed)

% set the errors

zvec=[0,0,0,0,0,0];
if (any(BendOffs))
  [iss,errv]=ErrorGroupGaussErrors(BendMisalign,zvec,BendOffs,zvec);
end
if (any(QuadOffs))
  [iss,errv]=ErrorGroupGaussErrors(QuadMisalign,zvec,QuadOffs,zvec);
end

% get the orbit and dispersion before launch from tracking

[S,X,Y,DX,DPX,DY,DPY]=GetMyData(1,IP,MyBeams);

% launch into the FF
% (NOTE: since the launch correctors have nonzero length, the response matrix
%        isn't exactly correct ... so we iterate)

idc=BPMLaunchStruc.ps;
idm=BPMLaunchStruc.elements';
A=BPMLaunchStruc.xfermat;

NLaunch=3;
for k=1:NLaunch

% compute the corrector settings and trim

  b=[X(idm,end);Y(idm,end)];
  x=A\b;
  for m=1:length(idc)
    n=idc(m);
    PS(n).SetPt=PS(n).SetPt-x(m);
  end
  stat=PSTrim(idc);

% get the orbit and dispersion from tracking

  [S,Xt,Yt,DXt,DPXt,DYt,DPYt]=GetMyData(1,IP,MyBeams);
  X=[X,Xt];DX=[DX,DXt];DPX=[DPX,DPXt];
  Y=[Y,Yt];DY=[DY,DYt];DPY=[DPY,DPYt];
end

% plot the orbits

figure(1)
clf,subplot
subplot(211)
plot(S,1e6*X)
subplot(212)
plot(S,1e6*Y)

% display the orbit readings at the feedback BPMs

for k=1:1+NLaunch
  for m=1:length(idm)
    n=idm(m);
    disp(sprintf('%10.3f %10.3f',1e6*[X(n,k),Y(n,k)]))
  end
  disp(' ')
end

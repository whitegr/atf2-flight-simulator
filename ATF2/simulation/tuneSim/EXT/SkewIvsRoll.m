
% MMS data for quad types (I is amp, G is T/m)
% (from: ATF$MAG:MAG_KI_Q_IDX_SKEW.FOR
%        ATF$MAG:MAG_KI_Q_ECUBE_SKEW)

qL=[0.07867;0.08];
qI=[-20.0,-16.0,-12.0, -8.0, -4.0,  0.0,  4.0,  8.0, 12.0, 16.0, 20.0; ...
    -20.0,-16.0,-12.0, -8.0, -4.0,  0.0,  4.0,  8.0, 12.0, 16.0, 20.0];
qG=[-5.662,-4.522,-3.385,-2.247,-1.115, 0.000, 1.115, 2.247, 3.385, 4.522, 5.662; ...
    -0.015,-0.012,-0.009,-0.006,-0.003, 0.000, 0.003, 0.006, 0.009, 0.012, 0.015];

load ATF2Perfect
Brho=Initial.Momentum/0.299792458; % Tesla-m
C=zeros(2,1); % KL-to-I conversion constants
[C(1),dq]=noplot_polyfit(qG(1,:)*qL(1)/Brho,qI(1,:),1,[0,1]); % IDX
[C(2),dq]=noplot_polyfit(qG(2,:)*qL(2)/Brho,qI(2,:),1,[0,1]); % ECUBE
qname=['QS1X';'QS2X';'QK1X';'QK2X';'QK3X';'QK4X'];
qtype=[1;1;1;1;1;1];
Imax=[2;2;20;5;5;20]; % maximum operating currents

roll=[300;250;200;150;100]; % urad
%fnum=[9;10;11;12;13]; % roll errors only
fnum=[1;3;4;5;6]; % all errors

Ihi=[];
Ilo=[];
for n=1:5
  fname=sprintf('state4/results4_%d',fnum(n));
  load(fname)
  KL=[results.SkewK1L]';
  I=KL*diag(C(qtype));
  Ihi=[Ihi;max(I)];
  Ilo=[Ilo;min(I)];
end

figure
clf,subplot
for n=1:6
  subplot(3,2,n)
  plot(roll,Ihi(:,n),'bo--',roll,Ilo(:,n),'bo--')
  set(gca,'YLim',[-Imax(n),Imax(n)])
  hor_line(0,'k:')
  if (Imax(n)==20),hor_line([-15,-10,-5,5,10,15],'r:'),end
  title(qname(n,:))
  if (rem(n,2)),ylabel('Current (amps)'),end
  if (n>4),xlabel('Quadrupole Roll rms (urad)'),end
end

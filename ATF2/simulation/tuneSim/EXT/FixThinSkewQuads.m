function FixThinSkewQuads()

global BEAMLINE

id=findcells(BEAMLINE,'Name','MKEX1');
id=[id,findcells(BEAMLINE,'Name','MQM6R')];
id=[id,findcells(BEAMLINE,'Name','MQM7R')];
id=[id,findcells(BEAMLINE,'Name','MBS1X')];
id=[id,findcells(BEAMLINE,'Name','MBS2X')];
id=[id,findcells(BEAMLINE,'Name','MBS3X')];

for n=id
  BEAMLINE{n}.Tilt=pi/4;
  BEAMLINE{n}.PoleIndex=1;
end

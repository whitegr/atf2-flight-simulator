function DoATF2Steer( beam, struc )

% function which performs the steering of the front part of ATF2

  global PS ;

% start by getting the BPM readings:

  if ~Model.reality
    [stat,beamout,instdata] = TrackThru(1,struc.lastelem,beam,1,1,0) ;
  else
    [stat,beamout,instdata] = TrackThruaper(Model,1,struc.lastelem,1,1,0) ;
  end
  [S,x,y] = GetBPMvsS(instdata{1}) ;
  bpmdata = [x' ; y'] ;
  
% add the positions and angles of the beam at the end of the beamline

  [x,sig] = GetBeamPars(beamout,1) ;
  bpmdata = [bpmdata ; x(1) ; x(2) ; x(3) ; x(4)] ;
  
% compute the correction (actually, the opposite of the correction, since
% we are solving the matrix BPMs = R * corrs)

  Correctors = struc.xfermat \ bpmdata ;
  
% apply the correction to the power supplies

  for count = 1:length(Correctors)
      PS(struc.ps(count)).SetPt = PS(struc.ps(count)).SetPt - Correctors(count) ;
  end
  
  stat = PSTrim(struc.ps) ;
  


global BEAMLINE

load ATF2Perfect

% MMS data for IDX skew quads (I is amp, G is T/m)
% (from: ATF$MAG:MAG_KI_Q_IDX_SKEW.FOR)

qL=0.07867;
qI=[-20.0  ,-16.0  ,-12.0  ,-8.0  ,-4.0  ,0.0  ,4.0  ,8.0  ,12.0  ,16.0  ,20.0  ];
qG=[ -5.662, -4.522, -3.385,-2.247,-1.115,0.000,1.115,2.247, 3.385, 4.522, 5.662];

% use MMS data and EtaY knob definition to define scan DY values

Brho=Initial.Momentum/0.299792458; % Tesla-m
GL=qG*qL; % Tesla
[C,dq]=noplot_polyfit(qI,GL,1,[0,1]); % I-to-GL
I=[-5:5]'; % scan currents
GL=C*I; % scan GLs
DYscan=GL/EtaY.Channel(1).Coefficient; % scan DY values (m)

% scan the EtaY knob

X=[];DX=[];DPX=[];
Y=[];DY=[];DPY=[];
stat=RestoreMultiKnob('EtaY');
for n=1:length(DYscan)
  stat=SetMultiKnob('EtaY',DYscan(n));
  [S,Xn,Yn,DXn,DPXn,DYn,DPYn]=GetMyData(1,IP,MyBeams);
  X=[X,Xn];DX=[DX,DXn];DPX=[DPX,DPXn];
  Y=[Y,Yn];DY=[DY,DYn];DPY=[DPY,DPYn];
end
stat=RestoreMultiKnob('EtaY');

% find the target BPM

ml9x=findcells(BEAMLINE,'Name','ML9X');

% make some plots

id=[diagbeg:diagend]';
plot(S(id),1e3*DY(id,:),S(ml9x)*ones(size(DYscan)),1e3*DYscan,'bo')
ylabel('DY (mm)')
xlabel('S (m)')

% save DYKnobScan.mat

DY=DY(ml9x,:)';
DPY=DPY(ml9x,:)';
save DYKnobScan DY DPY

return

function steerstruc = MakeAFT2LaunchSteerStruc( )
%
% generate structures in support of ATF2 launch steering (pulse-to-pulse
% feedback)
%

%=======================================================================

  global BEAMLINE PS ;
  
  xcor = findcells(BEAMLINE,'Name','ZH10X') ;
  xcor = [xcor findcells(BEAMLINE,'Name','ZH1FF')] ;
  
  ycor = findcells(BEAMLINE,'Name','ZV11X') ;
  ycor = [ycor findcells(BEAMLINE,'Name','ZV1FF')] ;
  
  bpms = findcells(BEAMLINE,'Name','MFB2FF') ;
  bpms = [bpms findcells(BEAMLINE,'Name','MFB1FF')] ;
  bpms = [bpms findcells(BEAMLINE,'Name','MQD10BFF')] ;
  bpms = [bpms findcells(BEAMLINE,'Name','MQF9BFF')] ;
  
  steerstruc = [] ;
  brho = 1.28 / 0.299792458 ; % T.m
  nbpm = length(bpms) ; nxcor = length(xcor) ; nycor = length(ycor) ;
    
% generate the appropriate matrix (the one that generates BPM values from
% corrector values).  The matrix will be in the form:
%
%       [xcor->xbpm ycor->xbpm ; xcor->ybpm ycor->ybpm] ;

    xfermat = zeros(2*nbpm,nxcor+nycor) ;
    
    for bpmcount = 1:nbpm
      bpmelem = bpms(bpmcount) ;
      for xcorcount = 1:nxcor
        xcorelem = xcor(xcorcount) ;
        if (xcorelem < bpmelem)
          [stat,R] = RmatAtoB(xcorelem,bpmelem) ;
          xfermat(bpmcount,xcorcount) = R(1,2) ;
          xfermat(bpmcount+nbpm,xcorcount) = R(3,2) ;
        end
      end
      for ycorcount = 1:nycor
        ycorelem = ycor(ycorcount) ;
        if (ycorelem < bpmelem)
          [stat,R] = RmatAtoB(ycorelem,bpmelem) ;
          xfermat(bpmcount,nxcor+ycorcount) = R(1,4) ;
          xfermat(bpmcount+nbpm,nxcor+ycorcount) = R(3,4) ;
        end
      end
    end    
    
% get the power supplies 

    xcorps = [] ; 
    for ccount = 1:nxcor
        count = xcor(ccount) ;
        xcorps = [xcorps BEAMLINE{count}.PS] ;
    end
    ycorps = [] ; 
    for ccount = 1:nycor
        count = ycor(ccount) ;
        ycorps = [ycorps BEAMLINE{count}.PS] ;
    end
        
% convert from meters per radian to meters per T.m

    xfermat = xfermat / brho ;
    
% attach the values to the data structure

    steerstruc.ps = [xcorps ycorps] ;
    steerstruc.xfermat = xfermat ;
    steerstruc.elements = bpms ;

% set the BPM resolutions to zero ... they're perfect

    for bpmcount = 1:nbpm
      BEAMLINE{bpms(bpmcount)}.Resolution = 0;
    end

  end

%-------------------------------------------------------------------------------
% load ATF2 lattice and set up for simulations
%-------------------------------------------------------------------------------

% NOTE: remove AT package from Matlab's path!

global BEAMLINE PS

filename='ATF2Master.xsif';

Np=0.5e10;              % particles per bunch
qelec=1.6021764623e-19; % elementary charge (C)
nrays=10000;            % number of rays to track
randn('state',0)        % initialize random number generator
tplot=0;                % Twiss plot?
dp=1e-4;                % energy offset for making eta plots from tracking
BPMres=5e-6;            % BPM resolution (m)
BH1SextOFF=0;           % switch BH1/BH2 sextupole components OFF

stat=InitializeMessageStack();
  
% parse the lattice (use a MAD SAVELINE output)

disp(sprintf('   parsing %s ...',filename))
tic,[iss,Initial]=XSIFToLucretia(filename,'ATF2');et=toc;
disp(sprintf('   ... elapsed time = %.1f seconds',et))
stat=AddStackToStack(stat,iss);
stat{1}=[stat{1},iss{1}];
Initial.Q=Np*qelec; % not sure why I have to do this ...

% some useful things

N=length(BEAMLINE);
diagbeg=findcells(BEAMLINE,'Name','MDISP');
diagend=findcells(BEAMLINE,'Name','BEGFF');
IP=findcells(BEAMLINE,'Name','IP');

% since XSIF foolishly packs out the multipole information for multipoles with
% all-zero coeffs, fix that now for the extraction EXTsexts

%FixThinSkewQuads

% calculate and plot Twiss

[iss,T]=GetTwiss(1,length(BEAMLINE),Initial.x.Twiss,Initial.y.Twiss);
stat=AddStackToStack(stat,iss);
stat{1}=[stat{1},iss{1}];
if (tplot),PlotATF2Twiss,end

% set up block and slice information

iss=SetElementBlocks(1,length(BEAMLINE));
stat=AddStackToStack(stat,iss);
stat{1}=[stat{1},iss{1}];
iss=SetElementSlices(1,length(BEAMLINE));
stat=AddStackToStack(stat,iss);
stat{1}=[stat{1},iss{1}];

% add BPMs to EXT quad blocks

AddBPMsToQuadBlocks

% assign each magnet to its own power supply and move the B values to the power
% supplies
% (NOTE: QF13X and QF15X are powered in series)

iss=SetIndependentPS(1,length(BEAMLINE));
stat=AddStackToStack(stat,iss);
stat{1}=[stat{1},iss{1}];

qf15x=findcells(BEAMLINE,'Name','QF15X');
iss=AssignToPS(qf15x,0);
stat=AddStackToStack(stat,iss);
stat{1}=[stat{1},iss{1}];
qf13x=findcells(BEAMLINE,'Name','QF13X');
iss=AssignToPS(qf15x,BEAMLINE{qf13x(1)}.PS);
stat=AddStackToStack(stat,iss);
stat{1}=[stat{1},iss{1}];
clear qf13x qf15x

iss=MovePhysicsVarsToPS(linspace(1,length(PS),length(PS)));
stat=AddStackToStack(stat,iss);
stat{1}=[stat{1},iss{1}];

% turn on instrument data everywhere

list=SetTrackFlags('GetInstData',1,1,length(BEAMLINE));
  
% turn on BPM data acquisition in the region from ML1X to QBPM9X

ml1x=findcells(BEAMLINE,'Name','MQF7X');
qbpm9x=findcells(BEAMLINE,'Name','MQF21X');
list=SetTrackFlags('GetBPMData',1,ml1x,qbpm9x);
list=SetTrackFlags('GetBPMBeamPars',1,ml1x,qbpm9x);
clear list ml1x qbpm9x
  
% set the BPM resolution

list=findcells(BEAMLINE,'Class','MONI');
for count=list
  BEAMLINE{count}.Resolution=BPMres;
end
clear count list

% define a few beams:  an on-energy beam which is good for steering and tuning
% purposes, and an on-energy dense beam which is good for emittance purposes

beamsparse=MakeBeam6DSparse(Initial,3,11,11);
beamdense=MakeBeam6DGauss(Initial,nrays,5,1);

% MyBeams: 1=on energy; 2=low energy; 3=high energy

MyBeams=beamsparse;
Initial0=Initial;
Initial0.Momentum=Initial.Momentum*(1-dp);
MyBeams=[MyBeams;MakeBeam6DSparse(Initial0,3,11,11)];
Initial0.Momentum=Initial.Momentum*(1+dp);
MyBeams=[MyBeams;MakeBeam6DSparse(Initial0,3,11,11)];
  
% find the beginning and end of the section with misaligned quads, and the
% beginning and end of the BH1-2 section:

msim=findcells(BEAMLINE,'Name','IEX');
begff=findcells(BEAMLINE,'Name','BEGFF');
bh12beg=findcells(BEAMLINE,'Name','BH1XA');
bh12end=findcells(BEAMLINE,'Name','BH2XB')+1;
  
% define error groups for the element misalignments  

[iss,BendMisalign]=MakeErrorGroup( ...
  {'BEAMLINE','SBEN'},[bh12beg,bh12end],'Offset',2,'BH1-2 Misalignments');
stat=AddStackToStack(stat,iss);
stat{1}=[stat{1},iss{1}];
clear bh12beg bh12end
[iss,QuadMisalign]=MakeErrorGroup( ...
  {'BEAMLINE','QUAD'},[msim,begff-1],'Offset',2,'Quad+BPM Misalignments');
stat=AddStackToStack(stat,iss);
stat{1}=[stat{1},iss{1}];

% construct response matrices for both of the steering jobs

BPMSteerStruc=MakeATF2XYSteerStruc;
BPMLaunchStruc=MakeATF2LaunchSteerStruc;

% construct matrices to use for measuring the dispersion, the vector which
% defines the fractional energy changes to use in the energy scan, and the
% knobs which tune the dispersion

EnergyScanVector=[-0.002,-0.001,0,0.001,0.002];
[EtaBeams,MeasEtaStruc]=MakeMeasEtaStruc(EnergyScanVector,Initial);
[EtaX,EtaPX,EtaY]=MakeGlobalDispersionKnobs(T);

% construct the matrix which converts the beam matrix at the first wire into 
% the sig33 at each wire in the wire scanner system

Emat=MakeATF2Emat;

% define the 4 skew quad power supplies to be multiknobs

[Skew1,Skew2,Skew3,Skew4]=MakeCouplingCorrectionMultiKnobs; 

% switch OFF BH1 sextupole components

% if (BH1SextOFF)
%   id=[findcells(BEAMLINE,'Name','BH1XS'),findcells(BEAMLINE,'Name','BH2XS')];
%   for n=id
%     idPS=BEAMLINE{n}.PS;
%     PS(idPS).SetPt=0;
%     PS(idPS).Ampl=0;
%   end
% end

% save the perfect machine

ans=prompt(' Save ATF2Perfect?','yn','y');
if (strcmp(ans,'y'))
  save ATF2Perfect
end

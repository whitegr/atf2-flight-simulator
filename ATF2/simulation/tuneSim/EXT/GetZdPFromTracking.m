function [S,E,sigz,sigd]=GetZdPFromTracking(istart,iend,beam)

global BEAMLINE

[x,sig]=GetBeamPars(beam,1);
S=BEAMLINE{istart}.S;
E=x(6);
sigz=sqrt(sig(5,5));
sigd=sqrt(sig(6,6))/x(6);
beamout=beam;
for count=istart:iend
  if ~Model.reality
    [stat,beamout]=TrackThru(count,count,beamout,1,1,0);
    [x,sig]=GetBeamPars(beamout,1);
  else
    [stat,Model.realbeam]=TrackThruaper(Model,count,count,1,1,0);
    [x,sig]=GetBeamPars(Model.realbeam,1);
  end
  if (isfield(BEAMLINE{count},'L'))
    S=[S;BEAMLINE{count}.S+BEAMLINE{count}.L];
  else
    S=[S;BEAMLINE{count}.S];
  end
  E=[E;x(6)];
  sigz=[sigz;sqrt(sig(5,5))];
  sigd=[sigd;sqrt(sig(6,6))/x(6)];
end

return

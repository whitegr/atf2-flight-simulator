function [S,y,etay]=GetVerticalDispersionFromTracking(istart,iend,beam)

global BEAMLINE

[x,sig]=GetBeamPars(beam,1);
S=BEAMLINE{istart}.S;
etay=sig(3,6)/sig(6,6)*x(6);
y=x(3);

beamout=beam;
temp = Model.realbeam;
Model.realbeam = beamout;
for count=istart:iend
  if ~Model.reality
    [stat,beamout]=TrackThru(count,count,beamout,1,1,0) ;
    [x,sig]=GetBeamPars(beamout,1);
  else
    [stat,Model.realbeam]=TrackThruaper(Model,count,count,1,1,0) ;
    [x,sig]=GetBeamPars(Model.realbeam,1);
  end
  if (isfield(BEAMLINE{count},'L'))
    S=[S;BEAMLINE{count}.S+BEAMLINE{count}.L];
  else
    S=[S;BEAMLINE{count}.S];
  end
  etay=[etay;sig(3,6)/sig(6,6)*x(6)];
  y=[y;x(3)];
end

Model.realbeam = temp;
clear temp

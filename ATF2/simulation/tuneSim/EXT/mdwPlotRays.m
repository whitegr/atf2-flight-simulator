function mdwPlotRays(rays,program,asym)

if (~exist('program','var'))
  program='TURTLE';
end
if (~exist('asym','var'))
  asym=0;
end

xdat=1e6*rays(:,1);  % um
pxdat=1e6*rays(:,2); % urad
ydat=1e6*rays(:,3);  % um
pydat=1e6*rays(:,4); % urad
zdat=1e3*rays(:,5);  % mm
dpdat=1e3*rays(:,6); % pm
sig=cov(rays);
nbin=100;
nsig=3;

% x-px phase space plot

figure
clf,subplot,hold off

subplot(222)
[v,u]=hist(pxdat,nbin);
warning off MATLAB:rankDeficientMatrix
if (asym)
  [pxfit,q,dq,chi2]=agauss_fit(u,v,[],0);
else
  [pxfit,q,dq,chi2]=gauss_fit(u,v,[],0);
end
warning on MATLAB:rankDeficientMatrix
pxg=q(4);
h2=barh(u,v);
set(h2,'EdgeColor',[0,0,1],'FaceColor',[0,0,1])
hold on
plot(pxfit,u,'r-')
hold off
title(['\sigma_{px} = ',num2str(pxg),' urad'])
ylabel('px (urad)')
ax2=axis;

subplot(223)
[v,u]=hist(xdat,nbin);
warning off MATLAB:rankDeficientMatrix
if (asym)
  [xfit,q,dq,chi2]=agauss_fit(u,v,[],0);
else
  [xfit,q,dq,chi2]=gauss_fit(u,v,[],0);
end
warning on MATLAB:rankDeficientMatrix
xg=q(4);
h3=bar(u,v);
set(h3,'EdgeColor',[0,0,1],'FaceColor',[0,0,1])
hold on
plot(u,xfit,'r-')
hold off
title(['\sigma_x = ',num2str(xg),' um'])
xlabel('x (um)')
ax3=axis;

subplot(221)
h1=plot(xdat,pxdat,'.');
axis([ax3(1:2),ax2(3:4)])
hold on
plot_ellipse(inv(nsig*(1e12*sig(1:2,1:2))),mean(xdat),mean(pxdat),'r-')
hold off
title(sprintf('%s rays',deblank(program)))
xlabel('x (um)')
ylabel('px (urad)')

% y-py phase space plot

figure
clf,subplot,hold off

subplot(222)
[v,u]=hist(pydat,nbin);
warning off MATLAB:rankDeficientMatrix
if (asym)
  [pyfit,q,dq,chi2]=agauss_fit(u,v,[],0);
else
  [pyfit,q,dq,chi2]=gauss_fit(u,v,[],0);
end
warning on MATLAB:rankDeficientMatrix
pyg=q(4);
h2=barh(u,v);
set(h2,'EdgeColor',[0,0,1],'FaceColor',[0,0,1])
hold on
plot(pyfit,u,'r-')
hold off
title(['\sigma_{py} = ',num2str(pyg),' urad'])
ylabel('py (urad)')
ax2=axis;

subplot(223)
[v,u]=hist(ydat,nbin);
warning off MATLAB:rankDeficientMatrix
if (asym)
  [yfit,q,dq,chi2]=agauss_fit(u,v,[],0);
else
  [yfit,q,dq,chi2]=gauss_fit(u,v,[],0);
end
warning on MATLAB:rankDeficientMatrix
yg=q(4);
h3=bar(u,v);
set(h3,'EdgeColor',[0,0,1],'FaceColor',[0,0,1])
hold on
plot(u,yfit,'r-')
hold off
title(['\sigma_y = ',num2str(yg),' um'])
xlabel('y (um)')
ax3=axis;

subplot(221)
h1=plot(ydat,pydat,'.');
axis([ax3(1:2),ax2(3:4)])
hold on
plot_ellipse(inv(nsig*(1e12*sig(3:4,3:4))),mean(ydat),mean(pydat),'r-')
hold off
title(sprintf('%s rays',deblank(program)))
xlabel('y (um)')
ylabel('py (urad)')

% z-dp phase space plot

figure
clf,subplot,hold off

subplot(222)
warning off MATLAB:rankDeficientMatrix
[v,u]=hist(dpdat,nbin);
if (asym)
  [dpfit,q,dq,chi2]=agauss_fit(u,v,[],0);
else
  [dpfit,q,dq,chi2]=gauss_fit(u,v,[],0);
end
warning on MATLAB:rankDeficientMatrix
dpg=q(4);
h2=barh(u,v);
set(h2,'EdgeColor',[0,0,1],'FaceColor',[0,0,1])
hold on
plot(dpfit,u,'r-')
hold off
title(['\sigma_{dp} = ',num2str(dpg),' pm'])
ylabel('dp (pm)')
ax2=axis;

subplot(223)
[v,u]=hist(zdat,nbin);
warning off MATLAB:rankDeficientMatrix
if (asym)
  [zfit,q,dq,chi2]=agauss_fit(u,v,[],0);
else
  [zfit,q,dq,chi2]=gauss_fit(u,v,[],0);
end
warning on MATLAB:rankDeficientMatrix
zg=q(4);
h3=bar(u,v);
set(h3,'EdgeColor',[0,0,1],'FaceColor',[0,0,1])
hold on
plot(u,zfit,'r-')
hold off
title(['\sigma_z = ',num2str(zg),' mm'])
xlabel('z (mm)')
ax3=axis;

subplot(221)
h1=plot(zdat,dpdat,'.');
axis([ax3(1:2),ax2(3:4)])
hold on
plot_ellipse(inv(nsig*(1e6*sig(5:6,5:6))),mean(zdat),mean(dpdat),'r-')
hold off
title(sprintf('%s rays',deblank(program)))
xlabel('z (mm)')
ylabel('dp (pm)')

r21=sig(2,1)/sqrt(sig(1,1)*sig(2,2));
r43=sig(4,3)/sqrt(sig(3,3)*sig(4,4));
r65=sig(6,5)/sqrt(sig(5,5)*sig(6,6));

disp(' ')
disp(sprintf('   sigx  = %11.6f um (rms = %11.6f)',xg,std(xdat)))
disp(sprintf('   sigxp = %11.6f ur (rms = %11.6f)',pxg,std(pxdat)))
disp(sprintf('                          (r21 = %11.6f)',r21))
disp(sprintf('   sigy  = %11.6f um (rms = %11.6f)',yg,std(ydat)))
disp(sprintf('   sigyp = %11.6f ur (rms = %11.6f)',pyg,std(pydat)))
disp(sprintf('                          (r43 = %11.6f)',r43))
disp(sprintf('   sigz  = %11.6f mm (rms = %11.6f)',zg,std(zdat)))
disp(sprintf('   sigdp = %11.6f pm (rms = %11.6f)',dpg,std(dpdat)))
disp(sprintf('                          (r65 = %11.6f)',r65))
disp(' ')

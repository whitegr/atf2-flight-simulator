%%
clear all global
jm=findResource;
job=jm.Jobs;
if ~isempty(job)
  jtag=get(job,'Tag');
  job=job(ismember(jtag,'tuneSim'));
  if ~isempty(job)
    job.destroy;
  end
end
clear job
for ijob=1:128
  job(ijob)=createJob(jm);
  job(ijob).Tag='tuneSim';
  createTask(job(ijob),@tuneSim,2,{ijob,4});
end
%%
for ijob=1:128
  submit(job(ijob))
end
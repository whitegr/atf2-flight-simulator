function [] = beam_align( Model, Beam, niter, xfact, corRoutine, dfsWeight )
% BBA routine - either standard 1-1 steering (QBBA) BBA or DFS (corRoutine)
% niter = number of passes through the lattice (BBA iterations)
% xfact = change relative weight to give to restriction of Quad movement
% over requirement to minimise BPM reading (larger value means minimiser
% allowed to move quads more)
% dfsWeight = relative weight for minimisation of dispersive orbit (with
% respect to quad movement + BPM orbit)
global BEAMLINE GIRDER FL INSTR

% Correct up to QD0 from first FFS Quad
qf6=findcells(BEAMLINE,'Name','QM16FF');
if abs(BEAMLINE{qf6(1)}.B(1))>0.1
  qId1=findcells(BEAMLINE,'Name','QM16FF');
else
  qId1=findcells(BEAMLINE,'Name','QM15FF');
end
qId=findcells(BEAMLINE,'Name','QD0FF');
if isequal(corRoutine,'DFS')
%   align_range={[26 36] [30 40] [35 47]};
  align_range = {[BEAMLINE{qId1(1)}.PS BEAMLINE{qId(1)}.PS]};
else
  align_range = {[BEAMLINE{qId1(1)}.PS BEAMLINE{qId(1)}.PS]};
end
iWeight=xfact.*[1 1 1];

% Perform bba
for iter=1:niter
  for irange=1:length(align_range)
    Model.qbba_segment=irange; % Flag which segment we are aligning
    align_length=align_range{irange}(2)-align_range{irange}(1);
    switch upper(corRoutine)
      case 'QBBA'
        qbba(align_range{irange}(1):align_range{irange}(2),...
        {[ones(1,align_length-5).*1e-7 [1e-7 1e-7 1e-7 1e-7 1e-7]] [ones(1,align_length-5).*1e-7 [1e-7 1e-7 1e-7 1e-7 1e-7]]},...
        {[ones(1,align_length-1).*200e-6.*iWeight(irange) 1e-5] [ones(1,align_length-1).*200e-6.*iWeight(irange) 1e-5]}...
        ,0,0,Beam,Model);
      case 'DFS'
        qbba_dfs(align_range{irange}(1):align_range{irange}(2),...
        {[ones(1,align_length-5).*1e-7 [1e-7 1e-7 1e-7 1e-7 1e-7]] [ones(1,align_length-5).*1e-7 [1e-7 1e-7 1e-7 1e-7 1e-7]]},...
        {[ones(1,align_length-1).*200e-6.*iWeight(irange) 1e-8].*0.2 [ones(1,align_length-1).*200e-6.*iWeight(irange)*0.7 1e-8]}...
        ,0,0,Beam,Model,dfsWeight);
      otherwise
        error('unsupported corRoutine!');
    end % switch corRoutine
  end
end

% Move Sexts to measured orbit
FL.SimModel=Model; FL.simBeamID=2;
FlHwUpdate;
bpmData.x=cellfun(@(x) x.Data(1),INSTR); bpmData.y=cellfun(@(x) x.Data(2),INSTR);
for mRef=1:length(Model.Gind.Sext)
  init_ref=GIRDER{Model.Gind.Sext(mRef)}.MoverPos(1:2);
  mag_ref=init_ref+[bpmData.x(Model.magGroup.Sext.Bpm(mRef).Han) bpmData.y(Model.magGroup.Sext.Bpm(mRef).Han)];
  GIRDER{Model.Gind.Sext(mRef)}.MoverSetPt(1:2)=mag_ref;
  if Model.reality
    xpos = GIRDER{Model.Gind.Sext(mRef)}.MoverSetPt(1);
    ypos = GIRDER{Model.Gind.Sext(mRef)}.MoverSetPt(2);
    camangles = fftbmover_convert(Model,xpos,ypos,0,'sext');
    if ~isreal(camangles(1)) || ~isreal(camangles(2)) || ~isreal(camangles(3))
      save(['radpolice_saves/out' num2str(Model.nseed) '.mat'])
      disp(' ')
      error('Attempting to push cams beyond physical limits.')
    end
  end
end % for mRef
stat = MoverTrim( Model.Gind.Sext ); if stat{1}~=1; error(stat{2:end}); end;

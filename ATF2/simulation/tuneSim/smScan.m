function Model = smScan(Model,Beam)
% Scan shintake monitor fringe pattern across beam and calculate spot size

if ~isfield(Model,'Sm')
  error('Need to setup shintakeMonitor first!');
end % ~isfield Model.Sm

% Scan fringe phase across beam
for iPhase=-360:10:360
  Model.Sm = shintakeMonitor(Model.Sm,Beam,iPhase);
  signal(((iPhase+360)/10)+1)=Model.Sm.signal;
end

% Get sine fit
x=fminsearch(@(x) sum((signal-((max(signal)-mean(signal))*sin((1:length(signal))/x(1)+2*pi*x(2))+x(3))).^2),[5 0 0]);

% Get modulation depth and phase of scan
signal_fit = (max(signal)-mean(signal))*sin((1:length(signal))/x(1)+2*pi*x(2))+x(3);
Model.Sm.scan.M = (max(signal_fit)-min(signal_fit)) / (max(signal_fit) + min(signal_fit));
Model.Sm.scan.phase = x(2) ;

% Beam size assuming gaussian beam shape
Model.Sm.scan.gaussSize = (Model.Sm.d/(2*pi)) * sqrt( 2 * log(abs(cos(Model.Sm.theta))/Model.Sm.scan.M) ) ;
function tunePlots(nplots,filename)
TARGETSIZE=35e-9;
load(filename)
%%
knobNames={'Initial Alignment' '<x''y>' '\alpha^*_y' '\eta^*_y' 'T322' 'T326' 'U3122' '35nm'};
moveNames={'XpY' 'WaistY' 'DispY' 'T322' 'T326' 'U3122'};
knobCols='brgkcmy';
docomp=false;
if ishandle(nplots); docomp=true; end;
figure(nplots)
subplot(2,2,1)
nyplot=[preAlignY' ysizeTune_alt]; %#ok<*NODEF>
mtype='.'; msize=25;
if docomp;hold on; mtype='d'; msize=10; end;
errorbar(1,mean(nyplot(:,1)).*1e9,std(nyplot(:,1)).*1e9,[knobCols(1) mtype],'MarkerSize',msize,'LineWidth',2)
hold on
for imove=1:length(moveNames)
  mdata=mean(nyplot(:,[false ismember(IPdat.mk.which_move,moveNames{imove})])).*1e9;
  sdata=std(nyplot(:,[false ismember(IPdat.mk.which_move,moveNames{imove})])).*1e9;
  sdata(sdata>=mdata)=mdata(sdata>=mdata)-1e-9;
  errorbar(1+find(ismember(IPdat.mk.which_move,moveNames{imove})),...
    mdata,...
    sdata,[knobCols(imove+1) mtype],'MarkerSize',msize,'LineWidth',2)
end
ax=axis;
if nplots>ax(2)
  error('Not this many plots')
end
ax(2)=nplots;
axis(ax);
lh=line([ax(1) ax(2)],[35 35]); set(lh,'LineWidth',4); set(lh,'LineStyle',':','Color','r');
hold off
set(gca,'YScale','log')
legend(gca,knobNames,'fontsize',12,'fontweight','bold')
grid on
xlabel('Knob Iteration Step','fontsize',14,'fontweight','b')
ylabel('\sigma_y^* / nm','fontsize',14,'fontweight','b')
set(gca,'fontsize',16,'fontweight','bold')
ax=axis;
ax(3)=30;
axis(ax);
%%
subplot(2,2,2)
if nplots<3
  hist(preAlignY.*1e9,12);
  thisydata=preAlignY;
end
if nplots==2
  hold on
  hist(ysizeTune_alt(:,1).*1e9,12);
  thisydata=ysizeTune_alt(:,1);
  h = findobj(gca,'Type','patch');
  set(h(1),'FaceColor',knobCols(1+find(ismember(moveNames,IPdat.mk.which_move{1}))))
  hold off
elseif nplots>2
  hist(ysizeTune_alt(:,nplots-2).*1e9,12);
  hold on
  hist(ysizeTune_alt(:,nplots-1).*1e9,12);
  thisydata=ysizeTune_alt(:,nplots-1);
  h = findobj(gca,'Type','patch');
  set(h(1),'FaceColor',knobCols(1+find(ismember(moveNames,IPdat.mk.which_move{nplots-1}))))
  set(h(2),'FaceColor',knobCols(1+find(ismember(moveNames,IPdat.mk.which_move{nplots-2}))))
  hold off
end
ax=axis;
text(diff(ax(1:2))*0.65+ax(1),diff(ax(3:4))*0.9+ax(3),sprintf('\\mu = %.1f nm\n\\sigma = %.1f nm',mean(thisydata).*1e9,std(thisydata).*1e9),'BackGroundColor','y','EdgeColor','k','fontsize',14,'fontweight','bold')
xlabel('\sigma_y^* / nm','fontsize',14,'fontweight','b')
grid on
set(gca,'fontsize',16,'fontweight','bold')

%%
subplot(2,2,3)
terms=[1 2 4 6]; type={'\Sigma' 'T' 'U' 'V' 'W' 'X' 'Y' 'Z'}; pos=[0.9 0.7 0.5 0.3 0.1];
if nplots>2
  fcol(1)=knobCols(1+find(ismember(moveNames,IPdat.mk.which_move{nplots-2})));
  fcol(2)=knobCols(1+find(ismember(moveNames,IPdat.mk.which_move{nplots-1})));
else
  fcol(1)='b';
end
if nplots>1
  fcol(2)=knobCols(1+find(ismember(moveNames,IPdat.mk.which_move{nplots-1})));
end
sz=size(maxAber);
if nplots>1
  A=zeros(2,sz(2));
%   A1=accumarray(maxAber(:,nplots-1),(1/length(maxAber(:,nplots-1))).*tsize(:,nplots-1).*double(tsize(:,nplots-1)>TARGETSIZE)).*1e9;
%   A2=accumarray(maxAber(:,nplots),(1/length(maxAber(:,nplots))).*tsize(:,nplots).*double(tsize(:,nplots)>TARGETSIZE)).*1e9;
  A1=accumarray(maxAber(:,nplots-1),double(ysizeTune_alt(:,nplots-1)>TARGETSIZE).*(1/length(maxAber(:,nplots-1))).*tsize(:,nplots-1)).*1e9;
  A2=accumarray(maxAber(:,nplots),double(ysizeTune_alt(:,nplots)>TARGETSIZE).*(1/length(maxAber(:,nplots))).*tsize(:,nplots)).*1e9;
  A(1,1:length(A1))=A1;
  A(2,1:length(A2))=A2;
else
  A=zeros(1,sz(2));
%   A1=accumarray(maxAber(:,nplots),(1/length(maxAber(:,nplots))).*tsize(:,nplots).*double(tsize(:,nplots)>TARGETSIZE)).*1e9;
  A1=accumarray(maxAber(:,nplots),double(ysizeTune_alt(:,nplots)>TARGETSIZE).*(1/length(maxAber(:,nplots))).*tsize(:,nplots)).*1e9;
  A(1,1:length(A1))=A1;
end
bh=bar(A');
for ibar=1:length(bh)
  set(bh(ibar),'FaceColor',fcol(ibar))
end
[X I]=sort(get(bh(end),'YData')); wterm=0;
for iterm=length(I):-1:length(I)-2
  wterm=wterm+1;
  atype=aberTerms(I(iterm),:);
  tstring=[type{sum(atype)} '3'];
  for it=1:length(atype)
    if atype(it)
      for numt=1:atype(it)
        tstring=[tstring num2str(terms(it))];
      end
    end
  end
  text(I(iterm),X(iterm).*pos(wterm),tstring,'FontSize',18,'BackGroundColor','y','EdgeColor','k','fontsize',14,'fontweight','bold')
end
grid on
ax=axis; ax(4)=max(get(bh(end),'Ydata'))*1.1; ax(3)=0; axis(ax);
xlabel('Aberration #','fontsize',14,'fontweight','b')
ylabel('(1/N).\Sigma(Aberration Cont. to \sigma_y^* / nm)','fontsize',14,'fontweight','b')
set(gca,'fontsize',16,'fontweight','bold')

%%
subplot(2,2,4)
if nplots==1
  ydat1=preAlignY;
else
  ydat1=ysizeTune(:,nplots-1);
  ydat2=ysizeTune_alt(:,nplots-1);
end
ind=0;
for ysize=min(ydat1):(max(ydat1)-min(ydat1))/1000:max(ydat1)
  ind=ind+1;
  yplot_x1(ind)=ysize;
  yplot1(ind)=sum(ydat1>ysize)./length(ydat1);
end
if nplots>1
  ah=area(yplot_x1.*1e9,yplot1.*100); set(ah,'FaceColor',knobCols(1+find(ismember(moveNames,IPdat.mk.which_move{nplots-1}))))
  hold on
  ind=0;
  for ysize=min(ydat2):(max(ydat2)-min(ydat2))/1000:max(ydat2)
    ind=ind+1;
    yplot_x2(ind)=ysize;
    yplot2(ind)=sum(ydat2>ysize)./length(ydat2);
  end
  ah=area(yplot_x2.*1e9,yplot2.*100); set(ah,'FaceColor','w')
else
  plot(yplot_x1.*1e9,yplot1.*100,'LineWidth',2)
end
hold off
grid on
set(gca,'Layer','top')
ax=axis;
if nplots>1
  text(diff(ax(1:2))*0.6+ax(1),diff(ax(3:4))*0.85+ax(3),sprintf('50%% CL = %.1f nm\n90%% CL = %.1f nm',yplot_x2(find(yplot2<0.5,1))*1e9,yplot_x2(find(yplot2<0.1,1))*1e9),'BackGroundColor','y','EdgeColor','k','fontsize',14,'fontweight','bold')
else
  text(diff(ax(1:2))*0.6+ax(1),diff(ax(3:4))*0.85+ax(3),sprintf('50%% CL = %.1f nm\n90%% CL = %.1f nm',yplot_x1(find(yplot1<0.5,1))*1e9,yplot_x1(find(yplot1<0.1,1))*1e9),'BackGroundColor','y','EdgeColor','k','fontsize',14,'fontweight','bold')
end
xlabel('\sigma_y^* / nm','fontsize',14,'fontweight','b')
ylabel('% Seeds Converge with \sigma_y^* > x-axis value','fontsize',14,'fontweight','b')
set(gca,'fontsize',16,'fontweight','bold')
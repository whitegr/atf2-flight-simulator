function bsize=multMagnitudeTol(file,mtol)
global BEAMLINE PS GIRDER

load(file,'BEAMLINE','PS','GIRDER','Beam1')

ip=findcells(BEAMLINE,'Name','IP');
mults=findcells(BEAMLINE,'Name','*MULT');
multNames=unique(arrayfun(@(x) BEAMLINE{x}.Name,mults,'UniformOutput',false));
for imult=1:length(multNames)
  ele{imult}=findcells(BEAMLINE,'Name',multNames{imult});
  B{imult}=BEAMLINE{ele{imult}(1)}.B;
end
for itrack=1:100
  for imult=1:length(multNames)
    errfac=randn.*mtol;
    for iele=ele{imult}
      BEAMLINE{iele}.B=B{imult}.*(1+errfac);
    end
  end
  [stat bo]=TrackThru(1,ip,Beam1,1,1,0);
  bsize(itrack,:)=[std(bo.Bunch.x(1,:)) std(bo.Bunch.x(3,:))];
end
bsize=std(bsize);
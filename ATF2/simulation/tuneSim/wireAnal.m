function [C_x C_y]=wireAnal(beamsigma,wirediam,doplot)
warning('off','MATLAB:nearlySingularMatrix');
if ~exist('doplot','var'); doplot=false; end;

if doplot; close all; end;
N=10000;
[x,y]=cylinder(wirediam/2,N*2);
W_x=x(1,1:N);
W_y=2.*y(1,1:N);W_y=W_y./max(W_y);
G_x=linspace(-beamsigma*5,beamsigma*5,N);
G_y=gauss(G_x,0,beamsigma);
if doplot
  figure
  plot(G_x.*1e6,G_y./max(G_y),W_x.*1e6,W_y); grid on
  xlabel('Transverse Position / um')
  ylabel('Normalised charge/material density')
end
W_y=interp1(W_x,W_y,linspace(min(G_x),max(G_x),length(G_y))); W_y(isnan(W_y))=0;
C_y=conv(W_y,G_y);
dx=max(G_x)-min(G_x);
C_x=linspace(-dx,dx,length(C_y));
if doplot
  hold on
  plot(C_x.*1e6,C_y./max(C_y),'m')
  hold off
end

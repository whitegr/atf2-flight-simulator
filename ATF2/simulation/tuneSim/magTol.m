function [qroll sroll bendroll soff]=magTol(file)
global BEAMLINE PS GIRDER

ld=load(file);
BEAMLINE=ld.BEAMLINE;
PS=ld.PS;
GIRDER=ld.GIRDER;
Beam1=ld.Beam1;

rs=300e-6;
os=150e-6;
qroll=[];
[qn sn]=showmags;
for iq=1:length(qn)
  ind=findcells(BEAMLINE,'Name',qn{iq});
  for i_ind=ind
    BEAMLINE{i_ind}.Offset(6)=rs;
  end
  [stat bo]=TrackThru(1,findcells(BEAMLINE,'Name','IP'),Beam1,1,1,0);
  [fitTerm,fitCoef,bsize_corrected,bsize] = beamTerms(3,bo);
  qroll(iq).xy=bsize(15);
  qroll(iq).xpy=bsize(25);
  qroll(iq).bsize=std(bo.Bunch.x(3,:));
  for i_ind=ind
    BEAMLINE{i_ind}.Offset(6)=0;
  end
  fprintf('%s: <xy> %g <x''y> %g sigy= %g\n',qn{iq},qroll(iq).xy,qroll(iq).xpy,std(bo.Bunch.x(3,:)))
end
indb{1}=findcells(BEAMLINE,'Name','B1FF*');
indb{2}=findcells(BEAMLINE,'Name','B2FF*');
indb{3}=findcells(BEAMLINE,'Name','B5FF*');
bname={'B1FF' 'B2FF' 'B5FF'};
for ib=1:3
  for i_ind=indb{ib}
    BEAMLINE{i_ind}.Offset(6)=rs;
  end
  [stat bo]=TrackThru(1,findcells(BEAMLINE,'Name','IP'),Beam1,1,1,0);
  [fitTerm,fitCoef,bsize_corrected,bsize] = beamTerms(3,bo);
  bendroll(ib).xy=bsize(15);
  bendroll(ib).xpy=bsize(25);
  bendroll(ib).bsize=std(bo.Bunch.x(3,:));
  for i_ind=indb{ib}
    BEAMLINE{i_ind}.Offset(6)=0;
  end
  fprintf('%s: <xy> %g <x''y> %g sigy= %g\n',bname{ib},bendroll(ib).xy,bendroll(ib).xpy,std(bo.Bunch.x(3,:)))
end
for is=1:length(sn)
  ind=findcells(BEAMLINE,'Name',sn{is});
  for i_ind=ind
    BEAMLINE{i_ind}.Offset(6)=rs;
  end
  [stat bo]=TrackThru(1,findcells(BEAMLINE,'Name','IP'),Beam1,1,1,0);
  [fitTerm,fitCoef,bsize_corrected,bsize] = beamTerms(3,bo);
  sroll(is).xy=bsize(15);
  sroll(is).xpy=bsize(25);
  sroll(is).bsize=std(bo.Bunch.x(3,:));
  for i_ind=ind
    BEAMLINE{i_ind}.Offset(6)=0;
  end
  fprintf('%s: <xy> %g <x''y> %g sigy= %g\n',sn{is},sroll(is).xy,sroll(is).xpy,std(bo.Bunch.x(3,:)))
end
for is=1:length(sn)
  ind=findcells(BEAMLINE,'Name',sn{is});
  for i_ind=ind
    BEAMLINE{i_ind}.Offset(3)=os;
  end
  [stat bo]=TrackThru(1,findcells(BEAMLINE,'Name','IP'),Beam1,1,1,0);
  [fitTerm,fitCoef,bsize_corrected,bsize] = beamTerms(3,bo);
  soff(is).xy=bsize(15);
  soff(is).xpy=bsize(25);
  soff(is).bsize=std(bo.Bunch.x(3,:));
  for i_ind=ind
    BEAMLINE{i_ind}.Offset(3)=0;
  end
  fprintf('%s (off): <xy> %g <x''y> %g sigy= %g\n',sn{is},soff(is).xy,soff(is).xpy,std(bo.Bunch.x(3,:)))
end
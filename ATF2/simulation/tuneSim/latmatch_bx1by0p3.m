% function latmatch
global PS BEAMLINE
% Load lattice
load ../../../Lucretia/src/Floodland/latticeFiles/src/v4.5/ATF2lat_BX1BY1.mat

% Make match object
[beam Istruc]=initTransport(Model.Initial,Beam1,1,findcells(BEAMLINE,'Name','BEGFF'));
i1=findcells(BEAMLINE,'Name','BEGFF');
% beam=Beam1_IEX; Istruc=Model.Initial_IEX; i1=findcells(BEAMLINE,'Name','IEX');
M=Match;
M.beam=beam;
M.iInitial=i1;
M.initStruc=Istruc;
M.verbose=true; % see optimizer output or not
M.optim='lsqnonlin';

% Variables
% 6 matching quads, skew sextupole and 5 ffs sextupole strengths
% matching quads
mq=findcells(BEAMLINE,'Name','QM*FF'); mq=mq(1:2:end);
mqps=arrayfun(@(x) BEAMLINE{x}.PS,mq);
for ips=1:length(mqps)
  M=addVariable(M,'PS',mqps(ips),'Ampl',-2.5,2.5);
end
% QD0FF
qd0=findcells(BEAMLINE,'Name','QD0FF');
qd0ps=BEAMLINE{qd0(1)}.PS;
M=addVariable(M,'PS',qd0ps,'Ampl',0.8,1.2);
% QF1FF
qf1=findcells(BEAMLINE,'Name','QF1FF');
qf1ps=BEAMLINE{qf1(1)}.PS;
M=addVariable(M,'PS',qf1ps,'Ampl',0.8,1.2);
% SK1FF
sk1=findcells(BEAMLINE,'Name','SK1FF');
sk1ps=BEAMLINE{sk1(1)}.PS;
if ~sk1ps
  AssignToPS(sk1,length(PS)+1);
  sk1ps=length(PS);
end
% 
ss=findcells(BEAMLINE,'Name','S*FF'); ss=ss(~ismember(ss,sk1)); ss=ss(1:2:end);
ssps=arrayfun(@(x) BEAMLINE{x}.PS,ss);
% All quads
aq=findcells(BEAMLINE,'Name','Q*FF'); aq=aq(1:2:end);
aqps=arrayfun(@(x) BEAMLINE{x}.PS,aq(~ismember(aq,[mq qd0(1) qf1(1)])));

% Constraints
% Min IP vertical spot size + alpha's & betas at IP and vertical waist at
%   MQF2FF IP vertical image point
i_ip=findcells(BEAMLINE,'Name','IP');
i_image=findcells(BEAMLINE,'Name','MFB2FF');
i_mfb1=findcells(BEAMLINE,'Name','MFB1FF');
M=addMatch(M,i_ip,'alpha_x',0,1e-4);
M=addMatch(M,i_ip,'alpha_y',0,1e-4);
M=addMatch(M,i_ip,'beta_x',0.004,0.0001);
M=addMatch(M,i_ip,'beta_y',3.3333e-05,1e-6);

% See initial problem
display(M)

% Run matching procedure (linear)
M=M.doMatch;

% See end results
display(M)

M=addMatch(M,i_image,'alpha_y',0,1e-4);
% M=addMatch(M,i_image,'beta_y',0.014,0.001);
M=addMatch(M,i_mfb1,'alpha_x',0,1e-4);
% M=addMatch(M,i_mfb1,'beta_x',1e-2,1e-3;
M=M.doMatch;
display(M);

% Run matching procedure (non-linear)
M2=Match;
M2.beam=beam;
M2.iInitial=i1;
M2.initStruc=Istruc;
M2.verbose=false;
M2.optimDisplay='iter';
M2.optim='fminsearch';
M2.useParallel=false;
M2=addVariable(M2,'PS',sk1ps,'Ampl',-1,1);
for ips=1:length(mqps)
  M2=addVariable(M2,'PS',mqps(ips),'Ampl',-2.5,2.5);
end
M2=addVariable(M2,'PS',qd0ps,'Ampl',0.8,1.2);
M2=addVariable(M2,'PS',qf1ps,'Ampl',0.8,1.2);
for ips=1:length(aqps)
  M2=addVariable(M2,'PS',aqps(ips),'Ampl',0.7,1.2);
end
for ips=1:length(ss)
  M2=addVariable(M2,'PS',ssps(ips),'Ampl',0,2);
end
M2=addMatch(M2,i_ip,'Sigma',20e-9^2,8e-17,'33');
M2=addMatch(M2,i_image,'alpha_y',0,1e-4);
M2=addMatch(M2,i_mfb1,'alpha_x',0,1e-4);
M2=addMatch(M2,i_ip,'Sigma',2.83e-6^2,1.5e-12,'11');
% starting non-linear match condition
display(M2);
M2=M2.doMatch;

% See end results
display(M2)

% Renormalise Power supplies to BEAMLINE
for ips=1:length(PS)
  if any(PS(ips).Element) && isempty(regexp(BEAMLINE{PS(ips).Element(1)}.Class,'COR$','once'))
    RenormalizePS(ips);
    PS(ips).SetPt=PS(ips).Ampl;
  end
end

% Remove skew sext PS
for isk=1:length(sk1)
  BEAMLINE{sk1(isk)}.PS=0;
end

% Check tracking
[stat b]=TrackThru(1,i_ip,Beam1,1,1,0);
fprintf('IP x = %g y = %g\n',std(b.Bunch.x(1,:)),std(b.Bunch.x(3,:)))

save endmatch


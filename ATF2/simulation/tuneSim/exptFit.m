function [fitdata minfunc]=exptFit(fname,knobs,esteps,iprot)
% EXPTFIT - fit model to experimental data
%  fname = filename of model
%  edata = vector of vertical beamsize experimental data
%  esteps = cell array of strings of tuning steps that the data corresponds
%           to
global BEAMLINE PS VERB MDATA INSTR FL GIRDER
warning('off','MATLAB:FlHwUpdate_nonFSsim')
ld=load(fname);
BEAMLINE=ld.BEAMLINE;
PS=ld.PS;
VERB=false;
MDATA=ld.MDATA;
INSTR=ld.INSTR;
FL=ld.FL;
GIRDER=ld.GIRDER;
Model=ld.Model;
Beam={ld.Beam1 ld.Beam0};

SQ=sqCouplingKnob(knobs);

[dat Model]=IP_meas( Beam, Model, true );

ipbsm=findcells(BEAMLINE,'Name','MS1IP');

BEAMLINE{ipbsm}.Offset(6)=iprot;
stepNames={'XpY' 'WaistY' 'DispY' 'sig13'};
ranges=[3e-9 10 1 1e-14];
tols=[1e-12 0.01 2e-3 1e-16];
for ifit=1:length(esteps)
  istep=ismember(stepNames,esteps{ifit});
  curpos=setSextKnob('getmoves');
  if ~istep; error('Unknown fit name supplied in ''esteps'''); end;
  min_move = fminbnd(@(x) minFunc(x,esteps{ifit},curpos,Model,SQ,Beam),-ranges(istep),ranges(istep),...
    optimset('TolX',tols(istep),'TolFun',1e-18,'MaxIter',20,'Display','iter'));
  minFunc(min_move,esteps{ifit},curpos,Model,SQ,Beam);
  fitdata{ifit}=IP_meas( Beam, Model, true );
  minfunc(ifit)=min_move;
  fprintf('%s: sigy=%g\n',esteps{ifit},fitdata{ifit}.ysize_real)
end

function chi2=minFunc(x,mName,curpos,Model,SQ,Beam)
persistent dat

if isequal(x,'getdata')
  chi2=dat;
  return
end

switch mName
  case {'DispY' 'WaistY' 'XpY'}
    moves=curpos;
    moves(1:3,:)=moves(1:3,:)+reshape(Model.knob.(mName).smoves.*x.*(Model.knob.(mName).uScale./Model.knob.(mName).scale),3,6);
    setSextKnob(Model,Beam{2},moves);
  otherwise
    SQ.(mName)=x;
end

dat = IP_meas( Beam, Model, true );
chi2 = dat.ysize^2;
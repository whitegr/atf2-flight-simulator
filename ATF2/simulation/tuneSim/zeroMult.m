% nmult=findcells(BEAMLINE,'Name','*MULT');
% goodlist={'QD6FFMULT' 'QF7FFMULT' 'QF5AFFMULT' 'QF5BFFMULT'};
% iff=findcells(BEAMLINE,'Name','BEGFF');
% for imult=nmult
% %   if imult<iff; continue; end;
%   BEAMLINE{imult}.B=BEAMLINE{imult}.B.*0;
% %   if ismember(BEAMLINE{imult}.Name,goodlist)
% %     %BEAMLINE{imult}.B=BEAMLINE{imult}.B.*0;
% %     for iT=1:length(BEAMLINE{imult}.Tilt)
% %       BEAMLINE{imult}.Tilt(iT)=pi*(rand*2-1);
% %     end
% %   end
% end

idm=setxor(findcells(BEAMLINE,'Class','MULT'),findcells(BEAMLINE,'Name','SQS*'));
for m=1:length(idm)
  n=idm(m);
  if (strcmp(BEAMLINE{n}.Name(1:3),'KEX')&&(BEAMLINE{n}.B(1)~=0))
    BEAMLINE{n}.B=BEAMLINE{n}.B(1);
    BEAMLINE{n}.Tilt=BEAMLINE{n}.Tilt(1);
    BEAMLINE{n}.PoleIndex=BEAMLINE{n}.PoleIndex(1);
  else
    BEAMLINE{n}.B=0;
    BEAMLINE{n}.Tilt=0;
    BEAMLINE{n}.PoleIndex=0;
  end
end
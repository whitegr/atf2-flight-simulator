#!/bin/tcsh
setenv LD_LIBRARY_PATH ${MATLAB}/sys/os/glnx86:${MATLAB}/bin/glnx86:${MATLAB}/sys/java/jre/glnx86/jre1.5.0/lib/i386/native_threads
setenv LD_LIBRARY_PATH ${LD_LIBRARY_PATH}:${MATLAB}/sys/java/jre/glnx86/jre1.5.0/lib/i386/client:${MATLAB}/sys/java/jre/glnx86/jre1.5.0/lib/i386
setenv LD_LIBRARY_PATH ${LD_LIBRARY_PATH}:/afs/slac/u/ey/whitegr/u01/whitegr/xerces-c-src_2_8_0/lib
./tuneSim $1 $2
#unsetenv DISPLAY
#matlab_linux >&! jobOutputs/tuneSim_$1.out << EOF
#tuneSim($1,$2);
#exit
#EOF

function [dX, dY] = gmMove(Model,vals)
% Apply ground motion and component jitter to lattice
global BEAMLINE GIRDER %#ok<NUSED>
persistent origPos lastRMS


% restore functionality
if isequal(Model,'get_restore')
  dX={origPos lastRMS}; return;
elseif isequal(Model,'set_restore')
  [origPos lastRMS]=deal(vals{:}); return;
end % if restore

% Save internal state if requested
if isequal(Model,'saveState')
  dX = origPos;
  dY = lastRMS;
  return
end

% Restore or reset last saved conditions
if isfield(Model,'gmInitPos') && isempty(origPos)
  origPos=Model.gmInitPos;
end
if isfield(Model,'gmRandSave') && isempty(lastRMS)
  lastRMS=Model.gmRandSave;
end
if ~Model.time
  origPos={}; lastRMS=[];
end

% Calculate ground motion
% -----------------------------------------------------------------------
%-- Uncomment when using Transfer functions to describe vibration transer --
%-- through IP doublet stabilisation table --
% % Use Table supports TF for SF1-QD0 table
% sVals=[Model.gmData.sCoords{:}]';
% TFx=zeros(size(sVals));
% TFy=zeros(size(sVals));
% TFx(sVals==Model.vibTable.S(1))=1; TFx(sVals==Model.vibTable.S(2))=1;
% TFy(sVals==Model.vibTable.S(1))=1; TFy(sVals==Model.vibTable.S(2))=1;
% [X, Y] = matgm( sVals, Model.time, Model.gmModel, TFx, TFy, 1);
%------------------------------------------------------------------------
[X, Y] = matgm( [Model.gmData.sCoords]', Model.time, Model.gmModel);
dX=X-X(1); dY=Y-Y(1);

% Apply ground motion
iS=0;
for iGM=1:length(Model.gmData)
  if isfield(Model.gmData(iGM),'girder') && ~isempty(Model.gmData(iGM).girder)
    object='girder'; whichStruc='GIRDER'; %#ok<NASGU>
  elseif isfield(Model.gmData(iGM),'element') && ~isempty(Model.gmData(iGM).element)
    object='element'; whichStruc='BEAMLINE'; %#ok<NASGU>
  else
    error(['Model.gmData(',num2str(iGM),') has no element or girder field!']);
  end % find if element or girder
  if length(Model.gmData(iGM).sCoords)==1
    iS=iS+1;
    if isempty(origPos) || length(origPos)<iGM || isempty(origPos{iGM})
      evalc(['origPos{iGM}=',whichStruc,'{Model.gmData(iGM).(object)}.Offset;']);
    end % if no original position cell element for this object
    evalc([whichStruc,'{Model.gmData(iGM).(object)}.Offset=origPos{iGM} + [dX(iS) 0 dY(iS) 0 0 0];']);
  elseif length(Model.gmData(iGM).sCoords)==2 % long element/girder
    x1=dX(iS+1); x2=dX(iS+2); y1=dY(iS+1); y2=dY(iS+2);
    if Model.gmData(iGM).sCoords(1) == Model.gmData(iGM).sCoords(2)
      angle=[0 0];
    else
      angle=[ (x2-x1)/(Model.gmData(iGM).sCoords(2)-Model.gmData(iGM).sCoords(1)) (y2-y1)/...
        (Model.gmData(iGM).sCoords(2)-Model.gmData(iGM).sCoords(1)) ]; %#ok<NASGU>
    end % if s1 and s2 the same place
    if isequal(object,'girder') || (isequal(object,'element') && length(Model.gmData(iGM).element)==1)
      if isempty(origPos) || length(origPos)<iGM || isempty(origPos{iGM})
        origPos{iGM}=GIRDER{Model.gmData(iGM).girder}.Offset;
      end % if no original position cell element for this object
      GIRDER{Model.gmData(iGM).girder}.Offset= origPos{iGM} + [mean([x1 x2]) angle(1) mean([y1 y2]) angle(2) 0 0];
    elseif isequal(object,'element') && length(Model.gmData(iGM).element)==2
      gInd=0;
      for gEle=Model.gmData(iGM).element(1):Model.gmData(iGM).element(2)
        gInd=gInd+1;
        if isempty(origPos) || length(origPos)<iGM || isempty(origPos{iGM}) || (numel(origPos{iGM})/6)<gInd
          if isfield(BEAMLINE{gEle},'Offset')
            origPos{iGM}(gInd,:)=BEAMLINE{gEle}.Offset;
          else
            origPos{iGM}(gInd,:)=zeros(1,6);
          end % if Offset field
        end % if no original position cell element for this object
        if isfield(BEAMLINE{gEle},'Offset')
          xoff=x1+(BEAMLINE{gEle}.S-Model.gmData(iGM).sCoords(1))*tan(angle(1));
          yoff=y1+(BEAMLINE{gEle}.S-Model.gmData(iGM).sCoords(1))*tan(angle(2));
          BEAMLINE{gEle}.Offset=origPos{iGM}(gInd,:) + [xoff angle(1) yoff angle(2) 0 0];
        end % if exist offset field
      end % for gEle
    end % if girder or not
    iS=iS+2;
  else % sCoords ~= 1 or 2
    error(['Model.gmData(',num2str(iGM),').sCoords should have length 1 or 2!']);
  end % if length sCoords 1 or 2
end % for iGM
% Random Magnet vibration (with respect to girder- no random walk)
for iMag=1:length(Model.magGroup.AllMag)
  magType=Model.magGroup.AllMag{iMag}.Type;
  magPointer=Model.magGroup.AllMag{iMag}.Pointer;
  if isfield(BEAMLINE{Model.magGroup.(magType).Off.ClusterList(magPointer).index(1)},'Block')
    ele1=BEAMLINE{Model.magGroup.(magType).Off.ClusterList(magPointer).index(1)}.Block(1);
    ele2=BEAMLINE{Model.magGroup.(magType).Off.ClusterList(magPointer).index(1)}.Block(2);
  else
    ele1=Model.magGroup.(magType).Off.ClusterList(magPointer).index(1);
    ele2=Model.magGroup.(magType).Off.ClusterList(magPointer).index(end);
  end
  thisRMS=randn(1,2).*Model.jitter.(lower(magType));
  for iEle=ele1:ele2
    if isfield(BEAMLINE{iEle},'Offset') && isfield(Model.jitter,lower(magType))
      if ~isempty(lastRMS) && length(lastRMS)>=iMag
        BEAMLINE{iEle}.Offset([1 3]) = BEAMLINE{iEle}.Offset([1 3]) - lastRMS{iMag} + thisRMS ;
      else
        BEAMLINE{iEle}.Offset([1 3]) = BEAMLINE{iEle}.Offset([1 3]) + thisRMS ;
      end % if lastRMS exists for this magnet
    end % if there is an Offset field
  end % magnet element loop
  lastRMS{iMag} = thisRMS;
end % iMag loop

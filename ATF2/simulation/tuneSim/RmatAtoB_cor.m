function [stat, R] = RmatAtoB_cor(ele1,ele2)
% Modified version of RmatAtoB to go from centre of corrector element
% (point of actual kick)

global BEAMLINE

[stat, R] = RmatAtoB(ele1,ele2);
if BEAMLINE{ele1}.L>0
  [stat, Rcor] = GetRmats(ele1,ele1);
  Rcor.RMAT(1,2)=Rcor.RMAT(1,2)/2;
  Rcor.RMAT(3,4)=Rcor.RMAT(3,4)/2;
  R=inv(Rcor.RMAT)*R;
end % if L>0
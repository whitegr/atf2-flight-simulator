function chi2 = mk_findmin(x,iknob,Beam,Model,curpos,SQ)
% chi2 = mk_findmin(x,iknob,Beam,Model)
% minimisation function for sextupole multi-knobs
global FL GIRDER
persistent keep
chi2=[];
if isequal(x,'reset') || isempty(keep)
  keep=[];
  ssext=[];
  if isequal(x,'reset'); return; end;
elseif isequal(x,'getkeep')
  chi2=keep;
  return
end

if isempty(x); x=0; end;

% Set Sextupole move up (relative to curpos)
% moves=curpos;
moves=zeros(3,6);
switch iknob
  case {'DispY' 'WaistY' 'XpY' 'T322' 'T326' 'U3122'}
    moves(1:3,:)=moves(1:3,:)+reshape(Model.knob.(iknob).smoves.*x.*(Model.knob.(iknob).uScale./Model.knob.(iknob).scale),3,6);
  case {'roll1' 'roll2' 'roll3' 'roll4' 'roll5'}
    moves(3,str2double(iknob(end)))=moves(3,str2double(iknob(end)))+x;
  case {'dk1' 'dk2' 'dk3' 'dk4' 'dk5'}
    moves(4,str2double(iknob(end)))=moves(3,str2double(iknob(end)))+x;
  case 'sig13'
    SQ.sig13=x;
  otherwise
    error('unkown knob option')
end % switch iknob

% Make sextupole move and track to get IP size and form chi2 statistic
% if ~strcmp(iknob,'sig13')
%   setSextKnob(Model,Beam{2},moves);
% end
imag=0;
for isext=curpos
  imag=imag+1;
  GIRDER{isext}.MoverSetPt=GIRDER{isext}.MoverSetPt+moves(1:3,imag)';
end
MoverTrim(curpos);
dat = IP_meas( Beam, Model, true );
chi2 = dat.ysize^2;
keep(end+1).optim=FL.optimizer;
keep(end).dat=dat;
imag=0;
for isext=curpos
  imag=imag+1;
  GIRDER{isext}.MoverSetPt=GIRDER{isext}.MoverSetPt-moves(1:3,imag)';
end
MoverTrim(curpos);

% Get IP jitter mode analysis
% [stat f data]=FlIPJitData;
% keep(end).ipjitdata=data;
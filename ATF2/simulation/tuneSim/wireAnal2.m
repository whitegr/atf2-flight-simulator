doplot=false;
bs=linspace(0.485e-6,2e-6,50);
ws=[3.75 5 6.25].*1e-6;
gfit=[];
for iw=1:length(ws)
  for ib=1:length(bs)
    beamsigma=bs(ib);
    [C_x C_y]=wireAnal(beamsigma,ws(iw),doplot);
    % resample to +/- 3sigma
    xvals=C_x(C_x>-beamsigma*5 & C_x<beamsigma*5);
    xvals=xvals(1:ceil(length(xvals)/1000):end);
    C_y=C_y(ismember(C_x,xvals));
    C_x=C_x(ismember(C_x,xvals));
    [yfit,q] = gauss_fit(C_x,C_y);
    gfit(iw,ib)=q(4);
    if doplot
      hold on
      plot(C_x.*1e6,yfit./max(yfit),'k--')
      hold off
    end
    fprintf('Beamsize = %g (step %d of %d)\n',beamsigma,ib,length(bs))
    fprintf('Gaussian fit to convoluted function, sigma = %g\n',q(4))
  end
end

plot(repmat(bs,3,1)'.*1e6,gfit'.*1e6)
ax=axis; ax(4)=3; axis(ax);
xlabel('Input Beam size / um')
ylabel('Width of gaussian fit to convoluted signal / um')
legend('Wire diameter = 3.75um','Wire diameter = 5um','Wire diameter = 6.25um')
grid on
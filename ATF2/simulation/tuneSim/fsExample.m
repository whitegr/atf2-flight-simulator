% This is an example scripts to show some things that you can do with
% Lucretia in the Flight Simulator simulated ATF2 environment

% First load the global variables we need
global BEAMLINE PS INSTR FL GIRDER

% Load the ATF2 lattice file
% This contains the damping ring, extraction line and final focus lattice
% plus a lot of pre-defined stuff (mainly found in the "Model" structure)
load ATF2lat

% have 2 pre-defined beams- Beam0 and Beam1, defined for the start of the
% DR (element 1) Beam0 is a single particle, Beam1 is a macro-particle beam
% (1e4 particles)

% Track beam through whole lattice using Lucretia tracking function to get
% readout on all bpms and beamline instruments
[stat,beamout,instdata] = TrackThru( 1, length(BEAMLINE), Beam0, 1, 1, 0 ); 

% Create INSTR data structure - this is specific to Floodland (Flight
% Simulator environment), once created, future calls to the subroutine
% "FlHwUpdate" will automatically track the beam and fill the bpm,
% wirescanner etc entries in INSTR. In the online version of Floodland,
% instead of tracking - INSTR is filled with real data using the same
% update command
[Model,INSTR]=CreateINSTR(Model,instdata); 

% define some useful marker points
iex=findcells(BEAMLINE,'Name','IEX'); % start of extraction line
ffs=findcells(BEAMLINE,'Name','BEGFF'); % start of final focus system

% Need this to keep track of number of pulses
FL.SimData.trackCount=0;

% indicate that this is simulated environment
Model.reality=false;

% Calculate twiss parameters starting from beginning of the extraction line
% Model.Initial_IEX contains the beam parameters corresponding to the point
% iex (Model.Initial contains those to element 1)- these are the twiss
% parameters, momentum etc at that point
[stat, twiss] = GetTwiss(iex,length(BEAMLINE),Model.Initial_IEX.x.Twiss,Model.Initial_IEX.y.Twiss) ;
if stat{1}~=1; error(stat{2}); end;

% Make a beam that is matched to the start of the extraction line
Beam1_IEX = MakeBeam6DGauss(Model.Initial_IEX,10001,5,0); % macro particle beam
Beam0_IEX = makeSingleBunch(Beam1_IEX); % single ray with mean values from generated macro particle beam

% Load model and beam data into FL data structure, if
% meaningful bunch size etc information required- use a macro-particle
% beam, if just position information needed, single particle beam is ok
% Also say where to start and finish tracking- in this case, only care
% about EXT + FFS, so start at iex- end at the end of the lattice (IP)
Model.tstart=iex; Model.ip_ind=length(BEAMLINE);
FL.simBeamID=2; FL.SimModel=Model;

% Update HW (track through lattice)
FlHwUpdate;

% Plot orbit reported by BPMs (plot EXT + FFS BPMs only)
s=cellfun(@(x) BEAMLINE{x.Index}.S,INSTR);
bpmx=cellfun(@(x) x.Data(1),INSTR); bpmy=cellfun(@(x) x.Data(2),INSTR);
inds=cellfun(@(x) x.Index>iex & isequal(x.Class,'MONI'),INSTR);
figure(1); plot(s(inds),bpmx(inds),'b.',s(inds),bpmy(inds),'r.');

% Make a change to a corrector magnet
% Note- PS units are in radian units for correctors, and multiples of
% design magnet strength for all other magnets, except skew quads where PS
% units are T
ycor_element=findcells(BEAMLINE,'Name','ZV7X');
ycor_ps=BEAMLINE{ycor_element}.PS;
PS(ycor_ps).SetPt = 10e-6; % 10 urad kick
 % issue command to set power supply - Note if this were to be applied on
 % the real accelerator, an additional parameter of 1 would have to be
 % passed [ i.e. PSTrim( ycor_ps, 1) ]
stat = PSTrim( ycor_ps );
if stat{1}~=1; error(stat{2}); end;

% move a magnet mover (GIRDER position)
mag_element=findcells(BEAMLINE,'Name','QD6FF');
mag_girder=BEAMLINE{mag_element(1)}.Girder;
GIRDER{mag_girder}.MoverSetPt(1)=GIRDER{mag_girder}.MoverPos(1)+1e-3; % move QD6 in x by 1 mm
% Issue command to move mover- again in the real accelerator, issue 1 as
% and extra parameter as in the PSTrim case
stat = MoverTrim( mag_girder );
if stat{1}~=1; error(stat{2}); end;

% Plot the new orbit
FlHwUpdate; % Update the bpm readings first
bpmx=cellfun(@(x) x.Data(1),INSTR); bpmy=cellfun(@(x) x.Data(2),INSTR);
figure(2); plot(s(inds),bpmx(inds),'b.',s(inds),bpmy(inds),'r.')

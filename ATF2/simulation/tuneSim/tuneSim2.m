function [Model IPdat] = tuneSim2(nseed,gmModelToRun,loadSeed)
% ATF2 FFS alignment and tuning
global BEAMLINE PS VERB MDATA INSTR FL GIRDER

% Switch off things I'm tired of hearing about
warning off MATLAB:lscov:RankDefDesignMat
warning off MATLAB:PSTrim_online
warning off MATLAB:MoverTrim_online
warning off MATLAB:FlHwUpdate_nonFSsim
warning off MATLAB:polyfit:RepeatedPointsOrRescale
warning off MATLAB:illConditionedMatrix

% Verbosity level
VERB = 0;

% Display compile date if deployed
if isdeployed
  fprintf('Compile Date: %s\n','jan-12-2011')
end

Model.nocompile=true; % Set to true if running un-compiled on farm

% Load in lattice and Model structure (created with lucretia.m)
if ~isdeployed
  load ../../../Lucretia/src/Floodland/latticeFiles/src/v4.5/ATF2lat_BX2p5BY1_bcons
%   load ../../../Lucretia/src/Floodland/latticeFiles/src/v4.5/ATF2lat_BX1BY1
%   load ../../../Lucretia/src/Floodland/latticeFiles/src/v4.3/ATF2lat_BX2p5BY1_opt
%   load ATF2lat_BX1BY1
else
%   load ATF2lat_BX2.5BY1_opt.mat
  load ATF2lat
end
% Remove multipole fields
% zeroMult;

% Floodland specific flags
FL.SimMode=2; % flag that we are running in nono-FS sim mode for FS code
FL.mode='trusted'; % don't try and use server functions for FS code
FL.isthread=false;
FL.simBeamID=1;
FL.accRate=1e10; % make rep rate arbitarily fast
FL.Gui=[];
FL.noerrors=false;
FL.expt='ATF2';
FL.atfDir=regexprep(pwd,'ATF2.*','ATF2');
FL.Region.DR.ind=[1 Model.extStart-1];
FL.Region.FFS.ind=[Model.ffStart.ind length(BEAMLINE)];
FL.Region.EXT.ind=[Model.extStart Model.ffStart.ind-1];
FL.Region.ALL.ind=[1 length(BEAMLINE)];
FL.Region.EXTFFS.ind=[Model.extStart length(BEAMLINE)];
FL.HwInfo=FlAssignHW(Model);

% Random seed (variables passed as strings when compiled)
if ~isdeployed
  seed_in=nseed;
else
  seed_in=str2double(nseed);
  gmModelToRun=str2double(gmModelToRun);
end
Model.nseed=seed_in;

% Look for Checkpoint (previous run booted from farm e.g. because exceeded
% cpu time limit)
if exist(['data/mksave_',num2str(Model.nseed),'.mat'],'file')
  load(['data/mksave_',num2str(Model.nseed)]);
  % reset random number generator states and function internals
  randn('state',Model.randnState); %#ok<RAND>
  rand('state',Model.randState); %#ok<RAND>
  Model.funcvals = funcRestore('set',Model.funcvals);
else
  % Initialise random number generators
  randn('state',Model.nseed); %#ok<RAND>
  rand('state',Model.nseed); %#ok<RAND>
end % if checkpoint

%randn('state',39);
%rand('state',39);

% BPM initial setup
Beam0.Bunch.x(1:5)=zeros(1,5);
[stat,beamout,instdata] = TrackThru( 1, length(BEAMLINE), Beam0, 1, 1, 0 ); 
if stat{1}~=1; error(stat{2}); end;
[Model,INSTR]=CreateINSTR(Model,instdata);
% Design lattice input for IEX point
Model.Initial=Model.Initial_IEX;
Beam2=Beam2_IEX;
Model.Initial_IEX.y.NEmit=3e-8;
Model.Initial_IEX.x.NEmit=5e-6;
Beam1=MakeBeam6DGauss(Model.Initial_IEX,1.1e4,5,0);
% Beam1=Beam1_IEX;
[stat, twiss] = GetTwiss(Model.extStart,length(BEAMLINE),Model.Initial_IEX.x.Twiss,Model.Initial_IEX.y.Twiss) ;
if stat{1}~=1; error(stat{2}); end;

% Load in Kubo simulations for ring extraction conditions SIG,Twiss,BMAG,Initial,Beam
% load ringSimData ringSimData
% nt=ringSimData(mod(Model.nseed-1,100)+1,:); % normal mode twiss parameters at IEX
%rand('state',Model.nseed)
%Model.Initial_IEX.y.Twiss.eta=0.02*rand*2-0.02;
%Model.Initial_IEX.y.Twiss.etap=0.02*rand*2-0.02;
%rand('state',39)
% [sig4d,newtwiss,bmag,Model.Initial_IEX,Beam1] = norm2sig4d(nt(2),nt(3),nt(4),nt(6),nt(5),nt(7),nt(8),nt(9),nt(10),nt(11),Model.Initial_IEX,Beam1); % convert to 4D beam sigma matrix
Beam0 = makeSingleBunch(Beam1);
FL.SimBeam={Beam1 Beam0 Beam2};
Model.Initial=Model.Initial_IEX;
FL.SimModel=Model;

% Set online parameter
Model.online = false;

% Load multi-knobs
% load 10umknobs Knobs
% Model.knob=Knobs; clear Knobs;
% load sextKnobData; knobgen;

% Start ind for all tracking = extraction point (IEX)
Model.tstart=Model.extStart;

% IP beam measurememt point (end point of tracking)
Model.ip_ind=findcells(BEAMLINE,'Name','MBS1IP');

tf=fieldnames(twiss);
for it=Model.extStart:length(BEAMLINE)
  for itf=1:length(tf)
    Model.Twiss.(tf{itf})(it)=twiss.(tf{itf})(it-Model.extStart+2);
  end % for itf
end % for it
if exist('loadSeed','var') && ~isempty(loadSeed)
  seed_preserve=nseed; gm_preserve=gmModelToRun;
  load(loadSeed); clear IPdat
  nseed=seed_preserve; gmModelToRun=gm_preserve;  %#ok<NASGU>
end

Model.nocompile=false; % Set to true if running un-compiled on farm

% "reality" settings
MDATA=struct;
Model.reality = false; % Set to true to turn on apertures, low charge, etc.
Model.realbeam = Beam1;
MDATA.chargeloss = zeros(1,length(BEAMLINE)); % Cumulative measure of the charge loss wrt Z
Model.camsettings.quad.a = 0.145250;
Model.camsettings.quad.c = 0.123952;
Model.camsettings.quad.R = 0.031;
Model.camsettings.quad.L = 0.0015875;
Model.camsettings.quad.S1 = 0.034925;
Model.camsettings.quad.S2 = 0.2905;
Model.camsettings.quad.b = Model.camsettings.quad.c + ...
  Model.camsettings.quad.S1 - (sqrt(2)*Model.camsettings.quad.R);
Model.camsettings.sext.a = 0.145250;
Model.camsettings.sext.c = 0.123952;
Model.camsettings.sext.R = 0.031;
Model.camsettings.sext.L = 0.0015875;
Model.camsettings.sext.S1 = 0.034925;
Model.camsettings.sext.S2 = 0.2905;
Model.camsettings.sext.b = Model.camsettings.sext.c + ...
Model.camsettings.sext.S1 - (sqrt(2)*Model.camsettings.sext.R);

% Errors
% Model.align.dB=0; Model.align.dB_sext=0; Model.align.dB_syst=0;

% Initialise IP scan count (count number of pulses for tuning)
MDATA.IPSCANCOUNT=0;
FL.SimData.trackCount=0;
FL.SimData.moverTime=0;

% Simulation switches
Model.doEXT = true;
Model.doFFS = true;
Model.doTune = true;
Model.doGMRUN = false;
Model.doJitter = false;
Model.doSimJitter = false;
Model.nMeasIter = 1; % number of times to iterate a beamsize measurement
Model.avePoint = 100e-1; % beamsize at which to start beamsize measurement averaging
% Model.Sm = shintakeMonitor; % Use shintake monitor simulation for IP size measurement (Model.doJitter must be true)
Model = setJitterParams(Model, Beam1);
Model.ipbsm_method = 'rms'; % method for measuring IP spot size
Model.doSMres = true; % use shintake BSM resolution data?
%reschoice=[0.5 1 1.5 2 2.5 3];
Model.smResScale = 1; %reschoice(ceil(nseed/100));
Model.extMeas = true; % measure sigma matrix + dispersion at EXT end-point
Model.align.mover_step=1e-6;
Model.ext.align.dB=1e-3;
Model.ext.align.dB_sext=1e-3;
Model.align.dB=1e-3;
Model.align.dB_sext=1e-3;

% weight parameter / niter for BBA
% wscan=0.6:0.02:0.8;
% ww=wscan(ceil(Model.nseed/100));
ww=0.74;
% bba_iter=ceil(Model.nseed/100);
bba_iter=10;


% FL.SimBeam=Beam0; FL.SimModel=Model;
% FlHwUpdate;
% farmSave('data',['tracktunedbeam_',num2str(Model.nseed)],Model,IPdat);
% [stat,beamout] = TrackThru( FL.SimModel.tstart, FL.SimModel.ip_ind, Beam1, 1, 1, 0 );
%  beam=beamout.Bunch.x;
%  save(['data/tunedbeam_',num2str(Model.nseed),'.txt'],'beam','-ASCII')
%  evalc(['!touch data/final_',num2str(Model.nseed),'.mat']);
%  return

if ~exist('IPdat','var') % no checkpoint loaded

  IPdat=[];
  
  % Initialise random number generators
  randn('state',Model.nseed); %#ok<RAND>
  rand('state',Model.nseed); %#ok<RAND>

  % Ground Motion Model and component jitter
  gmModels='ABCY';
  Model.gmModel=gmModels(gmModelToRun);
  Model.nPulseMeas = 90 ; % Number of pulses for IP beamsize measurement
  Model.repRate = 1.56 ; % Hz
  Model.nFB = 500; % number of pulses to allow FB to converge
  Model.doGM = false;
  if isfield(Model,'Sm')
    Model.Sm.scanPhases=-360*1.5:(720*1.5)/(Model.nPulseMeas-1):360*1.5;
  end % if SM sim

  % Pulse-pulse jitter sources
  % Extra IP measurement res degredation due to simulated pulse-pulse jiter
  Model.jitSimRes = 1.2643e-9 ;
  if Model.doJitter
    Model = setJitterParams(Model, Beam1);
  else
    Model = setJitterParams(Model, Beam1,'zero');
  end % if Model.doJitter
  
  % Twiss parameters
  [stat,Model.Twiss] = GetTwiss(1,length(BEAMLINE),Model.Initial.x.Twiss,Model.Initial.y.Twiss) ;
  
  % Track beam through perfect lattice for reference
  [Model,INSTR]=CreateINSTR(Model,'zero'); % set perfect bpms
  Model.ipLaserRes = [0 0];
  gm=Model.doGM; Model.doGM=false;
  [dat Model] = IP_meas( {Beam1 Beam0}, Model, true);
  Model.doGM=gm;
  Model.initTrack = dat ;
  FL.simBeamID=2; FL.SimModel=Model;
%   BPM_zeroset( Model, Beam0, 'xonly' )

  % Multiknobs generation / load
  if ~isdeployed
   load userData/Knobs_bx2p5by1_bcons Knobs
%     [stat Knobs]=GenerateKnobs(2,1,1,1); if stat{1}~=1; error(stat{2}); end;
  else
%     load userData/KEKknobs Knobs
    load userData/GenerateSextIPKnobs_save Knobs
%    [stat Knobs]=GenerateKnobs(2,0,0,1); if stat{1}~=1; error(stat{2}); end;
  end
  Model.knob=Knobs; clear Knobs;
  
  % IP waist measurement
  Model.ipLaserRes = ([2 2].*1e-9) ./  sqrt(Model.nMeasIter) ; % x/y RMS measurement errors (absolute)
  if Model.doSimJitter
    Model.ipLaserRes = sqrt(Model.ipLaserRes.^2 + Model.jitSimRes^2);
  end % if Model.do

  
%   Set errors, switch off PS to higher-order magnets, set bpm
%   resolutions
  if ~FL.noerrors
    AssignErrors( Model);
%     setAlignData; % load in measured misalignment data
    % Restore standard BPM settings
    [Model,INSTR]=CreateINSTR(Model);
  end
  
%   EXT-line setup
%   Model = EXTsetup(Model,Beam2);
  % define the 4 skew quad power supplies to be multiknobs
  [Skew1,Skew2,Skew3,Skew4]=MakeCouplingCorrectionMultiKnobs;
  Model.ext.Skew1=Skew1; Model.ext.Skew2=Skew2;
  Model.ext.Skew3=Skew3; Model.ext.Skew4=Skew4;
  % EXT feedback parameters
  % Model.ext.feedback.bpms_x = findcells(BEAMLINE,'Class','MONI',1,findcells(BEAMLINE,'Name','QBPM6FF'));
  % Model.ext.feedback.bpms_y = findcells(BEAMLINE,'Class','MONI',1,findcells(BEAMLINE,'Name','QBPM6FF'));
  Model.ext.feedback.bpms_x = findcells(BEAMLINE,'Class','MONI',FL.Region.EXT.ind(1),FL.Region.EXT.ind(end));
  Model.ext.feedback.bpms_y = findcells(BEAMLINE,'Class','MONI',FL.Region.EXT.ind(1),FL.Region.EXT.ind(end));
  Model.ext.feedback.cor_x = find(cellfun(@(x) ~isempty(regexp(x.Name,'ZH\d\d?X', 'once')),BEAMLINE))';
  Model.ext.feedback.cor_y = find(cellfun(@(x) ~isempty(regexp(x.Name,'ZV\d\d?X', 'once')),BEAMLINE))';
  Model.ext.feedback.weight = 0.1 ;
  Model.ext.Emat=MakeATF2Emat;
  % Wirescanners indicies
  Model.ext.mw_ind = findcells(BEAMLINE,'Name','MW*X');
  Model.ext.mw_iind = find(cellfun(@(x) ismember(x.Index,Model.ext.mw_ind),INSTR));
  if length(Model.ext.mw_ind)~=length(Model.ext.mw_iind)
    error('INSTR indexing error');
  end % if length INSTR,mw_ind not match


  % initialise model time and GM seed
  if Model.doGM
    Model.time=0; 
    gmMove(Model);
  end % if doGM
  
  if Model.doEXT
    
    disp('Tuning EXT');
    % Apply feedback - first get beam through EXT
    FL.simBeamID=2; FL.SimModel=Model; FL.SimModel.ip_ind=findcells(BEAMLINE,'Name','BEGFF');
    for iTrack=1:Model.nFB
      stat=FlHwUpdate; if stat{1}~=1; error(stat{2}); end;
      feedbackEXT(Model,'Correct');
    end % for iTrack
    FL.SimModel.ip_ind=Model.ip_ind;
    for iTrack=1:Model.nFB
      Model = myTrack(Beam0,Model);
    end % for iTrack
    stat=FlHwUpdate; if stat{1}~=1; error(stat{2}); end;
    [IPdat.preEXT Model] = IP_meas( {Beam1 Beam0}, Model, true ) ;
    % EXT dispersion-correction
    firstiter=0;
    for itry=1:10
      for idim=1:4
        newpars.corset=[0 0 0 0]; newpars.corset(idim)=1;
        extDispersion_run('SetPars',newpars);
        stat=extDispersion_run('Measure');
        if stat{1}~=1
          error(stat{2})
        end
        [stat data]=extDispersion_run('GetData');
        if ~firstiter; IPdat.exteta(:,1)=data.etaVector; firstiter=1; end;
        stat=extDispersion_run('Correct');
        if stat{1}~=1
          error(stat{2})
        end
        if idim==4
          stat=extDispersion_run('Measure'); if stat{1}~=1;error(stat{2});end;
          [stat data]=extDispersion_run('GetData'); if stat{1}~=1;error(stat{2});end;
          IPdat.exteta(:,itry)=data.etaVector;
        end
      end
    end
    stat=extDispersion_run('Measure');
    if stat{1}~=1
      error(stat{2})
    end
    [stat data]=extDispersion_run('GetData');
    IPdat.exteta(:,2)=data.etaVector;
    [stat IPdat.exteta_ypknob]=extdypknob('GetVal');
    % EXT coupling correction
    for itry=1:1
      [iss1,Model.ext.Skew1]=FindOptimumSkewQuadSetting(Model,Model.ext.Skew1,Model.ext.Emat,Beam2);
      if (iss1{1}~=1),disp('Skew1 scan failed'),end
      [iss2,Model.ext.Skew2]=FindOptimumSkewQuadSetting(Model,Model.ext.Skew2,Model.ext.Emat,Beam2);
      if (iss2{1}~=1),disp('Skew2 scan failed'),end
      [iss3,Model.ext.Skew3]=FindOptimumSkewQuadSetting(Model,Model.ext.Skew3,Model.ext.Emat,Beam2);
      if (iss3{1}~=1),disp('Skew3 scan failed'),end
      [iss4,Model.ext.Skew4]=FindOptimumSkewQuadSetting(Model,Model.ext.Skew4,Model.ext.Emat,Beam2);
      if (iss4{1}~=1),disp('Skew4 scan failed'),end
      if ((iss1{1}~=1)||(iss2{1}~=1)||(iss3{1}~=1)||(iss4{1}~=1))
        disp('Coupling correction did not converge')
      end % end if fail
    end % for itry
    for iTrack=1:Model.nFB
      FL.simBeamID=2; FL.SimModel=Model; FL.SimModel.ip_ind=findcells(BEAMLINE,'Name','BEGFF');
      stat=FlHwUpdate; if stat{1}~=1; error(stat{2}); end;
      feedbackEXT(Model,'Correct');
    end % for iTrack
    FL.SimModel.ip_ind=Model.ip_ind;
    for iTrack=1:Model.nFB*10
      Model = myTrack(Beam0,Model);
    end % for iTrack
    [IPdat.postEXT Model] = IP_meas( {Beam1 Beam0}, Model, true ) ;
  end % if Model.doEXT
  
  if Model.doFFS
  
    % Perform initial FFS steering
    Model.gInitRef=GIRDER;
%     disp('Performing initial bba...   ') 
    BPM_zeroset( 'remove' );
    for iter=1:bba_iter
      beam_align( Model, Beam0, 1, 2e-4*ww, 'DFS',1e12 );
    end % for iter
    % 
    % Perform FFS Quad BPM alignment
    
    disp('Quad Alignment.....')
    Model = BPM_align(Model,Beam0,[1 0 0 0 0]);
    % 
    % FFS Beam Based Alignment of Quads
    disp('Quad bba...')
    for iter=1:bba_iter
      beam_align( Model, Beam0, 1, 4.5e-4*ww, 'DFS',1e12 );
    end % for iter
    % 
    % Align FFS Sextupole BPMs and Sextupoles
    disp('Sextupole Alignment...')
    Model = BPM_align(Model, Beam0, [0 1 0 0 0]);
    % 
    % Activate FF sextupoles
    for g_id=1:length(Model.PSind.Sext)
      PS(Model.PSind.Sext(g_id)).SetPt = 1;
      stat = PSTrim( Model.PSind.Sext(g_id) ); if stat{1}~=1; error(stat{2:end}); end;
    end

    % Get IP params after alignment
    [IPdat.postAlign Model] = IP_meas( {Beam1 Beam0}, Model, true ) ;
  end % if Model.doFFS

  % Set gold orbit (zero BPM readings for this orbit)
  FL.simBeamID=2; FL.SimModel=Model;
  BPM_zeroset( Model, Beam0 );
  
end % use checkpoint data

% IP dispersion correction with EXT knobs
%FL.SimModel=Model; vals=[Model.ext.EtaX.Value Model.ext.EtaPX.Value Model.ext.EtaY.Value];
%ymin = fminbnd(@(x) ipmindisp(x,3,vals),-100e-3,100e-3,optimset('TolX',0.1e-3,'TolFun',0.1e-3^2,'Display','none'));
%ipmindisp(ymin,3,vals);
%Model=FL.SimModel;
% [IPdat.ipmindisp Model]=IP_meas( {Beam1 Beam0}, Model ) ;

Model.doGM=false;
if Model.doTune
  % Set GM
  if Model.doGM
    Model.time=0;
    gmMove(Model);
  end % if doGM
  % Apply sextupole tuning knobs for FFS tuning
%   Model.doQF1FB = true;
  IPdat = MK_correct(Model,{Beam1 Beam0},1,IPdat);
end % if Model.doTune

% - run long-term stability model
if Model.doGMRUN
  BPM_zeroset( Model, Beam0 );
  Model.time=0; gmMove(Model);
  Model.doGM = true ;
  VERB=false;
  Model = setJitterParams(Model, Beam1);
  fb_test;
end % if Model.doGMRUN

Model.doGM=false;
if Model.doGM
  Model.time=0;
  gmMove(Model);
  Model = setJitterParams(Model, Beam1);
end % if doGM
% Model.Measure_ipbcor=true;
% [IPdat.postTune Model] = IP_meas( {Beam1 Beam0}, Model ) ;
% ip_instr=findcells(INSTR,'Index',Model.ip_ind);
% IPdat.postTune.ipbcor.tsize=INSTR{ip_instr}.bcor.tsize;
% IPdat.postTune.ipbcor.vals=INSTR{ip_instr}.bcor.vals;
% IPdat.postTune.ipbcor.terms=INSTR{ip_instr}.bcor.terms;

% Get corrector strengths of interest etc
IPdat.corstr.zv5x=PS(BEAMLINE{findcells(BEAMLINE,'Name','ZV5X')}.PS).Ampl;
IPdat.corstr.zv6x=PS(BEAMLINE{findcells(BEAMLINE,'Name','ZV6X')}.PS).Ampl;
IPdat.corstr.zv7x=PS(BEAMLINE{findcells(BEAMLINE,'Name','ZV7X')}.PS).Ampl;
qs1=findcells(BEAMLINE,'Name','QS1X'); IPdat.sq.qs1x=PS(BEAMLINE{qs1(1)}.PS).Ampl;
qs2=findcells(BEAMLINE,'Name','QS2X'); IPdat.sq.qs2x=PS(BEAMLINE{qs2(1)}.PS).Ampl;
qk1x=findcells(BEAMLINE,'Name','QK1X'); IPdat.sq.qk1x=PS(BEAMLINE{qk1x(1)}.PS).Ampl;
qk2x=findcells(BEAMLINE,'Name','QK2X'); IPdat.sq.qk2x=PS(BEAMLINE{qk2x(1)}.PS).Ampl;
qk3x=findcells(BEAMLINE,'Name','QK3X'); IPdat.sq.qk3x=PS(BEAMLINE{qk3x(1)}.PS).Ampl;
qk4x=findcells(BEAMLINE,'Name','QK4X'); IPdat.sq.qk4x=PS(BEAMLINE{qk4x(1)}.PS).Ampl;

if isdeployed || Model.nocompile
  farmSave('data',['final_',num2str(Model.nseed)],Model,IPdat);
%   evalc(['!touch data/final_',num2str(Model.nseed),'.mat']);
%   Lucretia2AML('output',['data/mksave_',num2str(Model.nseed),'.aml']);
else % save beamline elements
  Model.GIRDER=GIRDER;
  Model.BEAMLINE=BEAMLINE;
  Model.PS=PS;
end

% ==========================================
function chi2=ipmindisp(x,dim,vals) %#ok<DEFNU>
global FL

if any(dim==1); SetMultiKnob('FL.SimModel.ext.EtaX', x+vals(1)); end;
if any(dim==2); SetMultiKnob('FL.SimModel.ext.EtaPX', x+vals(2)); end;
if any(dim==3); SetMultiKnob('FL.SimModel.ext.EtaY', x+vals(3)); end;
[ipd Model] = IP_meas( {FL.SimBeam{1} FL.SimBeam{2}}, FL.SimModel ) ; FL.SimModel=Model;
chi2=0;
if any(dim==1); chi2=chi2+ipd.Disp_x^2; end;
if any(dim==2); chi2=chi2+ipd.dpx^2; end;
if any(dim==3); chi2=chi2+ipd.Disp_y^2; end;

% function ssextInsert
% global BEAMLINE PS

% Remove tilt from Sextupoles
% isext=findcells(BEAMLINE,'Name','S*FF');
% for ii=isext
%   BEAMLINE{ii}.Offset(6)=0;
% end


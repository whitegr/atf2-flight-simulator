function Model = BPM_align(Model, Beam, alist)
% Perform bpm->magnet beam-based alignment

global BEAMLINE PS GIRDER INSTR VERB

Select.quad=alist(1); Select.sext=alist(2); Select.oct=alist(3); Select.mult=alist(4);
qmove=500e-6;
% Pick bpms to use for quad shunting
[quad_xpick,quad_ypick] = qspick(Model,qmove,10);

%------- BPM - Quad alignment
if Select.quad
  for g_id=1:length(quad_xpick)
    if (isempty(quad_xpick{g_id}) && isempty(quad_ypick{g_id})) || ...
        isequal(BEAMLINE{Model.magGroup.Quad.dB.ClusterList(g_id).index(1)}.Name,'QM16FF')
      continue
    end % if no bpms to measure alignment or first quad
    is_squad=0; qset=PS(Model.PSind.Quad(g_id)).Ampl;
    
    % Flag skew quads
    for ind=Model.magGroup.Quad.dB.ClusterList(g_id).index(1):Model.magGroup.Quad.dB.ClusterList(g_id).index(end)
      if find(ind==Model.handles.SQuad)
        is_squad=1; qset=0;
      end
    end
    
    Quad_startingPos=GIRDER{Model.Gind.Quad(g_id)}.MoverPos;
    
    % Quads not to align
    doAlign.x=true; doAlign.y=true;
    xList=[]; yList=[];
%     xList=[1,5,11,20]-1+firstMag;
%     yList=[1]-1+firstMag;
    if ~isempty(find(xList==g_id,1)); doAlign.x=false; end;
    if ~isempty(find(yList==g_id,1)); doAlign.y=false; end;
    
    % Initially move quad to 0 BPM reading
    if Model.doGM
      Model = myTrack(Beam,Model);
    else
      FL.SimModel=Model;FL.simBeamID=2;
      FlHwUpdate;
      Model.bpmData.x=cellfun(@(x) x.Data(1),INSTR); Model.bpmData.y=cellfun(@(x) x.Data(2),INSTR);
    end
    Qoff(1) = Model.bpmData.x(Model.magGroup.Quad.Bpm(g_id).Han);
    Qoff(2) = Model.bpmData.y(Model.magGroup.Quad.Bpm(g_id).Han); Qoff(3)=0;
    GIRDER{Model.Gind.Quad(g_id)}.MoverSetPt=GIRDER{Model.Gind.Quad(g_id)}.MoverPos + Qoff ;
    if Model.reality
      xpos = GIRDER{Model.Gind.Quad(g_id)}.MoverSetPt(1);
      ypos = GIRDER{Model.Gind.Quad(g_id)}.MoverSetPt(2);
      camangles = fftbmover_convert(Model,xpos,ypos,0,'quad');
      if ~isreal(camangles(1)) || ~isreal(camangles(2)) || ~isreal(camangles(3))
        save(['radpolice_saves/out' num2str(Model.nseed) '.mat'])
        disp(' ')
        error('Attempting to push cams beyond physical limits.')
      end
    end
    stat = MoverTrim( Model.Gind.Quad(g_id) ); if stat{1}~=1; error(stat{2:end}); end;
    Quad_initialpos=GIRDER{Model.Gind.Quad(g_id)}.MoverPos;
    Qoff_min_x=0; Qoff_min_y=0;
    
    if doAlign.x || doAlign.y
      % Get Quad R matrix for k change
      PS(Model.PSind.Quad(g_id)).SetPt = 1; stat = PSTrim( Model.PSind.Quad(g_id) );
      if stat{1}~=1; error(stat{2:end}); end;
      [stat,Rquad1]=RmatAtoB(Model.magGroup.Quad.dB.ClusterList(g_id).index(1),Model.magGroup.Quad.dB.ClusterList(g_id).index(end));
      PS(Model.PSind.Quad(g_id)).SetPt = Model.bpm.DeltaB_qalign ; stat = PSTrim( Model.PSind.Quad(g_id) );
      if stat{1}~=1; error(stat{2:end}); end;
      [stat,Rquad2]=RmatAtoB(Model.magGroup.Quad.dB.ClusterList(g_id).index(1),Model.magGroup.Quad.dB.ClusterList(g_id).index(end));
      RM.dRquad=Rquad2-Rquad1;
    end
    
    if doAlign.x && ~isempty(quad_xpick{g_id})
      % Get BPM list to fit to and required transfer matrix elements (x)
      RM.R11=[]; RM.R12=[]; RM.R33=[]; RM.R34=[]; bpm_list=[];
      bpm_list.Han=quad_xpick{g_id};
      for ibpm=1:length(bpm_list.Han)
        bpm_list.Ind(ibpm)=INSTR{bpm_list.Han(ibpm)}.Index;
        if length(INSTR{bpm_list.Han(ibpm)}.Res)>1
          bpm_list.Res(ibpm)=INSTR{bpm_list.Han(ibpm)}.Res(1);
        else
          bpm_list.Res(ibpm)=INSTR{bpm_list.Han(ibpm)}.Res;
        end % if len res > 1
        [stat,R] = RmatAtoB(Model.magGroup.Quad.dB.ClusterList(g_id).index(end)+1,bpm_list.Ind(ibpm));
        if stat{1}~=1; error(stat{2:end}); end;
        RM.R11(1,ibpm)=R(1,1); RM.R12(1,ibpm)=R(1,2); RM.R33(1,ibpm)=R(3,3); RM.R34(1,ibpm)=R(3,4);
      end
      % Find Quad offset to align field center with beam (x)
      dir = 1;
      Qoff_min_x = fminbnd(@(x) QuadFit(x,Model,dir,Quad_initialpos,qset,Beam,RM,is_squad,g_id,bpm_list),...
            -qmove,qmove,optimset('TolX',1e-9,'Display','off'));
    end % doAlign.x
      
    if doAlign.y && ~isempty(quad_ypick{g_id})
      % Get BPM list to fit to and required transfer matrix elements (y)
      RM.R11=[]; RM.R12=[]; RM.R33=[]; RM.R34=[]; bpm_list=[];
      bpm_list.Han=quad_ypick{g_id};
      for ibpm=1:length(bpm_list.Han)
        bpm_list.Ind(ibpm)=INSTR{bpm_list.Han(ibpm)}.Index;
        if length(INSTR{bpm_list.Han(ibpm)}.Res)>1
          bpm_list.Res(ibpm)=INSTR{bpm_list.Han(ibpm)}.Res(2);
        else
          bpm_list.Res(ibpm)=INSTR{bpm_list.Han(ibpm)}.Res;
        end % if len res > 1
        [stat,R] = RmatAtoB(Model.magGroup.Quad.dB.ClusterList(g_id).index(end)+1,bpm_list.Ind(ibpm));
        if stat{1}~=1; error(stat{2:end}); end;
        RM.R11(1,ibpm)=R(1,1); RM.R12(1,ibpm)=R(1,2); RM.R33(1,ibpm)=R(3,3); RM.R34(1,ibpm)=R(3,4);
      end
      % Find Quad offset to align field center with beam (x)
      dir = 2;
      Qoff_min_y = fminbnd(@(x) QuadFit(x,Model,dir,Quad_initialpos,qset,Beam,RM,is_squad,g_id,bpm_list),...
            -qmove,qmove,optimset('TolX',1e-9,'Display','off'));
    end % doAlign.y
    
    % Move Quad to beam
    GIRDER{Model.Gind.Quad(g_id)}.MoverSetPt=Quad_initialpos + [Qoff_min_x Qoff_min_y 0] ;
    if Model.reality
      xpos = GIRDER{Model.Gind.Quad(g_id)}.MoverSetPt(1);
      ypos = GIRDER{Model.Gind.Quad(g_id)}.MoverSetPt(2);
      camangles = fftbmover_convert(Model,xpos,ypos,0,'quad');
      if ~isreal(camangles(1)) || ~isreal(camangles(2)) || ~isreal(camangles(3))
        save(['radpolice_saves/out' num2str(Model.nseed) '.mat'])
        disp(' ')
        error('Attempting to push cams beyond physical limits.')
      end
    end
    stat = MoverTrim( Model.Gind.Quad(g_id) );if stat{1}~=1; error(stat{2:end}); end;
    
    % Align BPM in group
    if Model.doGM
      Model = myTrack(Beam,Model);
    else
      FL.SimModel=Model;FL.simBeamID=2;
      FlHwUpdate;
      Model.bpmData.x=cellfun(@(x) x.Data(1),INSTR); Model.bpmData.y=cellfun(@(x) x.Data(2),INSTR);
    end
    BeamPos_x=Model.bpmData.x(Model.magGroup.Quad.Bpm(g_id).Han);
    BeamPos_y=Model.bpmData.y(Model.magGroup.Quad.Bpm(g_id).Han);
    if doAlign.x
      BEAMLINE{Model.magGroup.Quad.Bpm(g_id).Ind}.ElecOffset(1)=-(BeamPos_x-BEAMLINE{Model.magGroup.Quad.Bpm(g_id).Ind}.ElecOffset(1));
    end
    if doAlign.y
      BEAMLINE{Model.magGroup.Quad.Bpm(g_id).Ind}.ElecOffset(2)=-(BeamPos_y-BEAMLINE{Model.magGroup.Quad.Bpm(g_id).Ind}.ElecOffset(2));
    end
    Model.bpm.QuadActual(g_id).x=(BEAMLINE{Model.magGroup.Quad.Bpm(g_id).Ind}.Offset(1)-BEAMLINE{Model.magGroup.Quad.Bpm(g_id).Ind}.ElecOffset(1))-...
      BEAMLINE{Model.magGroup.Quad.dB.ClusterList(g_id).index(1)}.Offset(1);
    Model.bpm.QuadActual(g_id).y=(BEAMLINE{Model.magGroup.Quad.Bpm(g_id).Ind}.Offset(3)-BEAMLINE{Model.magGroup.Quad.Bpm(g_id).Ind}.ElecOffset(2))-...
      BEAMLINE{Model.magGroup.Quad.dB.ClusterList(g_id).index(1)}.Offset(3);
    Model.bpm.QuadPos(g_id).x=GIRDER{Model.Gind.Quad(g_id)}.MoverPos(1);
    Model.bpm.QuadPos(g_id).y=GIRDER{Model.Gind.Quad(g_id)}.MoverPos(2);
    if VERB; fprintf('gid: %d x actual: %g y actual: %g\n',g_id,Model.bpm.QuadActual(g_id).x,Model.bpm.QuadActual(g_id).y); end;
    % Return Quad
    GIRDER{Model.Gind.Quad(g_id)}.MoverSetPt=Quad_startingPos;
    if Model.reality
      xpos = GIRDER{Model.Gind.Quad(g_id)}.MoverSetPt(1);
      ypos = GIRDER{Model.Gind.Quad(g_id)}.MoverSetPt(2);
      camangles = fftbmover_convert(Model,xpos,ypos,0,'quad');
      if ~isreal(camangles(1)) || ~isreal(camangles(2)) || ~isreal(camangles(3))
        save(['radpolice_saves/out' num2str(Model.nseed) '.mat'])
        disp(' ')
        error('Attempting to push cams beyond physical limits.')
      end
    end
    stat = MoverTrim( Model.Gind.Quad(g_id) );if stat{1}~=1; error(stat{2:end}); end;
    
    % Display progress
%     fprintf('\b\b\b%2.0f%%',(g_id/length(quad_xpick))*100)
    
  end % g_id loop
end % if select

% --- bpm - Sextupole Alignment
if Select.sext

  N_moves=50; S_min=-500e-6; S_max=500e-6;
  S_moves=S_min:(abs(S_min)+abs(S_max))/N_moves:S_max;
  sbpm=findcells(INSTR,'Index',findcells(BEAMLINE,'Name','MS1IP'));
  for g_id=length(Model.magGroup.Sext.Off.ClusterList)-4:length(Model.magGroup.Sext.Off.ClusterList)
    % Get bpm reading and move magnet to 0 reading
    if Model.doGM
      Model = myTrack(Beam,Model);
    else
      FL.SimModel=Model;FL.simBeamID=2;
      FlHwUpdate;
      Model.bpmData.x=cellfun(@(x) x.Data(1),INSTR); Model.bpmData.y=cellfun(@(x) x.Data(2),INSTR);
    end
    mRef = g_id ;
    mName = 'Sext' ;
    mSpan = [1 2] ;
    init_ref=GIRDER{Model.Gind.(mName)(mRef)}.MoverPos(mSpan);
    mag_ref=init_ref+[Model.bpmData.x(Model.magGroup.Sext.Bpm(g_id).Han) Model.bpmData.y(Model.magGroup.Sext.Bpm(g_id).Han)];
    GIRDER{Model.Gind.(mName)(mRef)}.MoverSetPt(mSpan)=mag_ref;
    if Model.reality
      xpos = GIRDER{Model.Gind.(mName)(mRef)}.MoverSetPt(1);
      ypos = GIRDER{Model.Gind.(mName)(mRef)}.MoverSetPt(2);
      camangles = fftbmover_convert(Model,xpos,ypos,0,'sext');
      if ~isreal(camangles(1)) || ~isreal(camangles(2)) || ~isreal(camangles(3))
        save(['radpolice_saves/out' num2str(Model.nseed) '.mat'])
        disp(' ')
        error('Attempting to push cams beyond physical limits.')
      end
    end
    stat = MoverTrim( Model.Gind.(mName)(mRef) ); if stat{1}~=1; error(stat{2:end}); end;
    % Turn magnet on
    PS(Model.PSind.Sext(g_id)).SetPt = 1; stat = PSTrim( Model.PSind.Sext(g_id) );
    if stat{1}~=1; error(stat{2:end}); end;
    bpm_x=[];bpm_y=[];
    % move through x settings and record Model.bpm's
    for smove=1:length(S_moves)
      GIRDER{Model.Gind.(mName)(mRef)}.MoverSetPt(mSpan)=mag_ref+[S_moves(smove) 0];
      if Model.reality
        xpos = GIRDER{Model.Gind.(mName)(mRef)}.MoverSetPt(1);
        ypos = GIRDER{Model.Gind.(mName)(mRef)}.MoverSetPt(2);
        camangles = fftbmover_convert(Model,xpos,ypos,0,'sext');
        if ~isreal(camangles(1)) || ~isreal(camangles(2)) || ~isreal(camangles(3))
          save(['radpolice_saves/out' num2str(Model.nseed) '.mat'])
          disp(' ')
          error('Attempting to push cams beyond physical limits.')
        end
      end
      stat = MoverTrim( Model.Gind.(mName)(mRef) );if stat{1}~=1; error(stat{2:end}); end;
      if Model.doGM; Model.time=Model.time+(1/Model.repRate); gmMove(Model); end;
      FL.SimModel=Model;FL.simBeamID=2;
      FlHwUpdate;
      bpm_x(smove)=INSTR{sbpm}.Data(1);
    end
    % move through y settings and record bpms
    for smove=1:length(S_moves)
      GIRDER{Model.Gind.(mName)(mRef)}.MoverSetPt(mSpan)=mag_ref+[0 S_moves(smove)];
      if Model.reality
        xpos = GIRDER{Model.Gind.(mName)(mRef)}.MoverSetPt(1);
        ypos = GIRDER{Model.Gind.(mName)(mRef)}.MoverSetPt(2);
        camangles = fftbmover_convert(Model,xpos,ypos,0,'sext');
        if ~isreal(camangles(1)) || ~isreal(camangles(2)) || ~isreal(camangles(3))
          save(['radpolice_saves/out' num2str(Model.nseed) '.mat'])
          disp(' ')
          error('Attempting to push cams beyond physical limits.')
        end
      end
      stat = MoverTrim( Model.Gind.(mName)(mRef) );if stat{1}~=1; error(stat{2:end}); end;
      if Model.doGM; Model.time=Model.time+(1/Model.repRate); gmMove(Model); end;
      FL.SimModel=Model;FL.simBeamID=2;
      FlHwUpdate;
      bpm_y(smove)=INSTR{sbpm}.Data(1); % (deliberately x- y scan deviates orbit in x-plane)
    end
    GIRDER{Model.Gind.(mName)(mRef)}.MoverSetPt(mSpan)=mag_ref;
    if Model.reality
      xpos = GIRDER{Model.Gind.(mName)(mRef)}.MoverSetPt(1);
      ypos = GIRDER{Model.Gind.(mName)(mRef)}.MoverSetPt(2);
      camangles = fftbmover_convert(Model,xpos,ypos,0,'sext');
      if ~isreal(camangles(1)) || ~isreal(camangles(2)) || ~isreal(camangles(3))
        save(['radpolice_saves/out' num2str(Model.nseed) '.mat'])
        disp(' ')
        error('Attempting to push cams beyond physical limits.')
      end
    end
    stat = MoverTrim( Model.Gind.(mName)(mRef) );if stat{1}~=1; error(stat{2:end}); end;
    % Fit 2nd order polynomial and use min gradient as offset
    P=polyfit(S_moves,bpm_x,2);
    Fitvals=S_min:(S_max-S_min)/100000:S_max;
    Pvals=polyval(P,Fitvals);
    [m ind]=min(abs(gradient(Pvals)));
    Model.bpm.SextOffsets(g_id).x=-Fitvals(ind);
    P=polyfit(S_moves,bpm_y,2);
    Fitvals=S_min:(S_max-S_min)/100000:S_max;
    Pvals=polyval(P,Fitvals);
    [m ind]=min(abs(gradient(Pvals)));
    Model.bpm.SextOffsets(g_id).y=-Fitvals(ind);
    % Align BPMs in group
    if Model.doGM
      Model = myTrack(Beam,Model);
    else
      FL.SimModel=Model; FL.simBeamID=2;
      FlHwUpdate;
      Model.bpmData.x=cellfun(@(x) x.Data(1),INSTR); Model.bpmData.y=cellfun(@(x) x.Data(2),INSTR);
    end
    BeamPos_x=Model.bpmData.x(Model.magGroup.Sext.Bpm(g_id).Han);
    BeamPos_y=Model.bpmData.y(Model.magGroup.Sext.Bpm(g_id).Han);
    BEAMLINE{Model.magGroup.Sext.Bpm(g_id).Ind}.ElecOffset(1)=-(BeamPos_x+Model.bpm.SextOffsets(g_id).x-BEAMLINE{Model.magGroup.Sext.Bpm(g_id).Ind}.ElecOffset(1));
    BEAMLINE{Model.magGroup.Sext.Bpm(g_id).Ind}.ElecOffset(2)=-(BeamPos_y+Model.bpm.SextOffsets(g_id).y-BEAMLINE{Model.magGroup.Sext.Bpm(g_id).Ind}.ElecOffset(2));
    Model.bpm.SextActual(g_id).x=(BEAMLINE{Model.magGroup.Sext.Bpm(g_id).Ind}.Offset(1)-BEAMLINE{Model.magGroup.Sext.Bpm(g_id).Ind}.ElecOffset(1))-...
      BEAMLINE{Model.magGroup.Sext.dB.ClusterList(g_id).index(1)}.Offset(1);
    Model.bpm.SextActual(g_id).y=(BEAMLINE{Model.magGroup.Sext.Bpm(g_id).Ind}.Offset(3)-BEAMLINE{Model.magGroup.Sext.Bpm(g_id).Ind}.ElecOffset(2))-...
      BEAMLINE{Model.magGroup.Sext.dB.ClusterList(g_id).index(1)}.Offset(3);
    if VERB; fprintf('SEXT gid: %d x actual: %g y actual: %g\n',g_id,Model.bpm.SextActual(g_id).x,Model.bpm.SextActual(g_id).y); end;
    % De-activate Sextupole
    PS(Model.PSind.Sext(g_id)).SetPt = 0; stat = PSTrim( Model.PSind.Sext(g_id) );
    if stat{1}~=1; error(stat{2:end}); end;
    % Move Sextupole
    GIRDER{Model.Gind.Sext(g_id)}.MoverSetPt(1:2)=mag_ref+[-Model.bpm.SextOffsets(g_id).x -Model.bpm.SextOffsets(g_id).y];
    if Model.reality
      xpos = GIRDER{Model.Gind.Sext(g_id)}.MoverSetPt(1);
      ypos = GIRDER{Model.Gind.Sext(g_id)}.MoverSetPt(2);
      camangles = fftbmover_convert(Model,xpos,ypos,0,'sext');
      if ~isreal(camangles(1)) || ~isreal(camangles(2)) || ~isreal(camangles(3))
        save(['radpolice_saves/out' num2str(Model.nseed) '.mat'])
        disp(' ')
        error('Attempting to push cams beyond physical limits.')
      end
    end
    stat = MoverTrim( Model.Gind.Sext(g_id) );if stat{1}~=1; error(stat{2:end}); end;
    Model.bpm.SextPos(g_id).x=GIRDER{Model.Gind.Sext(g_id)}.Offset(1);
    Model.bpm.SextPos(g_id).y=GIRDER{Model.Gind.Sext(g_id)}.Offset(2);
  end
end


function camangles = fftbmover_convert(Model,x,y,rot,magtype)

if strcmpi(magtype,'quad')

  x_1 = x + (Model.camsettings.quad.a*cos(rot)) + (Model.camsettings.quad.b*sin(rot)) - Model.camsettings.quad.a;
  y_1 = y - (Model.camsettings.quad.b*cos(rot)) + (Model.camsettings.quad.a*sin(rot)) + Model.camsettings.quad.c;
  betaminus = pi/4 - rot;
  betaplus  = pi/4 + rot;

  camangles(1) = rot - asin((1/Model.camsettings.quad.L) * ...
    ((x_1+Model.camsettings.quad.S2)*sin(rot) - y_1*cos(rot) + (Model.camsettings.quad.c - Model.camsettings.quad.b)));

  camangles(2) = rot - asin((1/Model.camsettings.quad.L) * ...
    ((x_1+Model.camsettings.quad.S1)*sin(betaminus) + y_1*cos(betaminus) - Model.camsettings.quad.R));

  camangles(3) = rot - asin((1/Model.camsettings.quad.L) * ...
    ((x_1-Model.camsettings.quad.S1)*sin(betaplus) - y_1*cos(betaplus) + Model.camsettings.quad.R));

  if ~isreal(camangles(1)) || ~isreal(camangles(2)) || ~isreal(camangles(3))
    camangles = [NaN NaN NaN];
    return
  end

  return
  
elseif strcmpi(magtype,'sext')

  x_1 = x + (Model.camsettings.sext.a*cos(rot)) + (Model.camsettings.sext.b*sin(rot)) - Model.camsettings.sext.a;
  y_1 = y - (Model.camsettings.sext.b*cos(rot)) + (Model.camsettings.sext.a*sin(rot)) + Model.camsettings.sext.c;
  betaminus = pi/4 - rot;
  betaplus  = pi/4 + rot;

  camangles(1) = rot - asin((1/Model.camsettings.sext.L) * ...
    ((x_1+Model.camsettings.sext.S2)*sin(rot) - y_1*cos(rot) + (Model.camsettings.sext.c - Model.camsettings.sext.b)));

  camangles(2) = rot - asin((1/Model.camsettings.sext.L) * ...
    ((x_1+Model.camsettings.sext.S1)*sin(betaminus) + y_1*cos(betaminus) - Model.camsettings.sext.R));

  camangles(3) = rot - asin((1/Model.camsettings.sext.L) * ...
    ((x_1-Model.camsettings.sext.S1)*sin(betaplus) - y_1*cos(betaplus) + Model.camsettings.sext.R));

  if ~isreal(camangles(1)) || ~isreal(camangles(2)) || ~isreal(camangles(3))
    camangles = [NaN NaN NaN];
    return
  end

  return
  
end
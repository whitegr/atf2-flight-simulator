function ret = setSextKnob(Model,Beam,newmoves)
% ret = setSextKnob(Model,Beam,newmoves)
% Set beamoffsets in sextupoles to perform IP spotsize tuning
% Moves Sextupoles such that beam passes through given x/y co-ordinates
% and given roll values
% Beam : lucretia beam
% 2 move modes, relative and absolute:
% For absolute:
% -------------
% moves : 4*6 matrix rows = x,y,roll,dk cols = sext # (1-5) + QD0 (6)
% For relative:
% -------------
% moves : 1*3 vector [x,y,z], moves(x,y) = moves(x,y) + z
% ( dk is in % change +/- from Model value, others are um/urad units )
% ret =[]
% -----------------
% -----------------
% ret = setSextKnob('getmoves')
% Get internal stored moves matrix
global GIRDER INSTR PS FL BEAMLINE
persistent moves


% restore functionality
if isequal(Model,'get_restore')
  ret={moves}; return;
elseif isequal(Model,'set_restore')
  moves=Beam{1}; return;
end % if restore

% initialise moves matrix
if isempty(moves); moves=zeros(4,6); end;

% Return stored moves if requested
if isequal(Model,'getmoves')
  ret=moves;
  return
else
  ret=[];
end % if getmoves required

% Check args etc
[rw col]=size(newmoves);
if (rw~=4 || col~=6) && (rw~=1 || col~=3)
  error('Moves should be 4*6 matrix: rows=x,y,roll,dk col=sext #');
end % if length moves ~=5

% Deal with relative or absolute move command
if length(newmoves)<5
  moves(newmoves(1),newmoves(2))=moves(newmoves(1),newmoves(2))+newmoves(3);
else
  moves=newmoves;
end % if relative move command

% Get sextupole/qd0 indicies
for isext=1:5
  sgi(isext)=BEAMLINE{FL.SimModel.magGroup.Sext.dB.ClusterList(isext).index(1)}.Girder;
  ips(isext)=BEAMLINE{FL.SimModel.magGroup.Sext.dB.ClusterList(isext).index(1)}.PS;
end
iqd0=findcells(BEAMLINE,'Name','QD0FF');
sgi(6)=BEAMLINE{iqd0(1)}.Girder;
ips(6)=BEAMLINE{iqd0(1)}.PS;
sbpm.ind=[Model.magGroup.Sext.Bpm.Ind];
sbpm.han=arrayfun(@(x) findcells(INSTR,'Index',x), [Model.magGroup.Sext.Bpm.Ind]);
if length(sbpm.ind)~=5 || length(sbpm.han)~=5
  error('Sext model indices wrong length!');
end % check Model.Sext length

% Average readings
if isfield(Model,'genKnobs') && Model.genKnobs
  nave=1;
else
  nave=10;
end % if Model.genKnobs

% Put required dk in first
for iMove=1:6
  PS(ips(iMove)).SetPt = 1 + moves(4,iMove);
end % for iMove
stat = PSTrim(ips, Model.online) ; if stat{1}~=1; error(stat{2:end}); end;

% Put in any desired tilt next
for iMove=1:6
  GIRDER{sgi(iMove)}.MoverSetPt(3) = moves(3,iMove) ;
end % for iMove
FL.SimData.moverTime=FL.SimData.moverTime+max([0 (ceil((max(abs(moves(3,:)))/Model.align.mover_speed(3))*Model.repRate)-1)/Model.repRate]);
stat = MoverTrim(sgi, Model.online) ; if stat{1}~=1; error(stat{2:end}); end;

% % Change x and y moves to be in the Sextupole/QD0 ref frame (take account of
% % any tilts)
% for iSext=1:6
%   theta=GIRDER{sgi(iSext)}.MoverSetPt(3);
%   rotvals=[cos(theta) sin(theta); -sin(theta) cos(theta)]*[moves(1,iSext);moves(2,iSext)];
%   moves(1,iSext)=rotvals(1); moves(2,iSext)=rotvals(2);
% end % for iMove

% Initial tracking to get Sext BPM readings
for iTrack=1:nave
  if Model.doGM
    Model = myTrack(Beam,Model);
    xdata(iTrack,:)=Model.bpmData.x; %#ok<AGROW>
    ydata(iTrack,:)=Model.bpmData.y; %#ok<AGROW>
  else
    FL.simBeamID=2; FL.SimModel=Model;
    FlHwUpdate;
    xdata(iTrack,:)=cellfun(@(x) x.Data(1),INSTR); %#ok<AGROW>
    ydata(iTrack,:)=cellfun(@(x) x.Data(2),INSTR); %#ok<AGROW>
  end % if Model.doGM
end % for iTrack
xdataMean=mean(xdata,1);
ydataMean=mean(ydata,1);
sbpm.vals=[xdataMean(sbpm.han); ydataMean(sbpm.han)];

% Get desired new orbit through Sexts
% if (isfield(Model,'genKnobs') && Model.genKnobs) || ~any(Model.align.mover_step)
%   sRes=1e-8;
% else
%   sRes=Model.align.mover_step*2;
% end % if perfect step size
% if length(sRes)==1; sRes=[sRes sRes]; end;
% ntries=0;
% while any(abs(sbpm.vals(1,:)+moves(1,1:5))>sRes(1)) || any(abs(sbpm.vals(2,:)+moves(2,1:5))>sRes(2))
%   ntries=ntries+1;
for iMove=1:5
  GIRDER{Model.Gind.Sext(iMove)}.MoverSetPt = GIRDER{Model.Gind.Sext(iMove)}.MoverPos + ...
    [sbpm.vals(1,iMove) sbpm.vals(2,iMove) 0] + [moves(1,iMove) moves(2,iMove) 0] ;
  maxmovetime=max(abs([sbpm.vals(1,iMove)/Model.align.mover_speed(1) sbpm.vals(2,iMove)/Model.align.mover_speed(2) 0]) + ...
    abs([moves(1,iMove)/Model.align.mover_speed(1) moves(2,iMove)/Model.align.mover_speed(2) 0]));
  stat = MoverTrim(Model.Gind.Sext(iMove)) ; if stat{1}~=1; error(stat{2:end}); end;
  FL.SimData.moverTime=FL.SimData.moverTime+(ceil(maxmovetime*Model.repRate)-1)/Model.repRate;
  for iTrack=1:nave
    if Model.doGM
      Model = myTrack(Beam,Model);
      xdata(iTrack,:)=Model.bpmData.x; %#ok<AGROW>
      ydata(iTrack,:)=Model.bpmData.y; %#ok<AGROW>
    else
      FL.simBeamID=2; FL.SimModel=Model;
      FlHwUpdate;
      xdata(iTrack,:)=cellfun(@(x) x.Data(1),INSTR); %#ok<AGROW>
      ydata(iTrack,:)=cellfun(@(x) x.Data(2),INSTR); %#ok<AGROW>
    end % if Model.doGM
  end % for iTrack
  xdataMean=mean(xdata,1);
  ydataMean=mean(ydata,1);
  sbpm.vals=[xdataMean(sbpm.han); ydataMean(sbpm.han)];
  FL.SimData.moverTime=FL.SimData.moverTime+max([0 (1-ceil(max(maxmovetime)*Model.repRate))/Model.repRate]);
end % for iMove
%   if ntries>20
%     save userData/setSextKnob_err
%     error('ntries exceeded in setSextKnob')
%   end
% end % while sexts orbit incorrect

% Make QD0 move
GIRDER{sgi(6)}.MoverSetPt(1) = moves(1,6) ;
GIRDER{sgi(6)}.MoverSetPt(2) = moves(2,6) ;
FL.SimData.moverTime=FL.SimData.moverTime+max([0 (ceil((max(abs(moves(1:2,6)))/Model.align.mover_speed(1))*Model.repRate)-1)/Model.repRate]);
stat = MoverTrim(sgi(6), Model.online) ; if stat{1}~=1; error(stat{2:end}); end;

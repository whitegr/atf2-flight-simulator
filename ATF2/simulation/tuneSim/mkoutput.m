function stop = mkoutput(x, optimValues, state)
global FL
stop=false;
if ~isfield(FL,'optimizer') || ~isfield(FL.optimizer,'state') || isequal(FL.optimizer(end).state,'done')
  FL.optimizer=[];
end
FL.optimizer(end+1).state=state;
FL.optimizer(end).vals=optimValues;
FL.optimizer(end).x=x;
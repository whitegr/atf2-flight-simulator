function sy=wakeRun()
global BEAMLINE PS GIRDER INSTR FL %#ok<NUSED>

BL=9; % mm

% Load in-use optics file
load('~/ATF2/FlightSim/latticeFiles/ATF2lat')
% Load BPM offset data
load userData/cavewake/archiveGetBpmsAndBBA bpmname read_x read_y

% Make beam
Model.Initial.sigz=BL*1e-3;
Model.Initial.y.NEmit=6e-8;
Model.Initial.Q=Model.Initial.Q;
Beam1=MakeBeam6DGauss(Model.Initial,2e4,5,1);

% Loop over BPM readings (pulses)
for ipulse=1:length(read_y)
  fprintf('Pulse: %d\n',ipulse)
  offset.name=bpmname;
  offset.x=read_x(ipulse,:); offset.y=read_y(ipulse,:);
  sy(ipulse)=wakeTrack(BL,offset,Beam1,Model);
end

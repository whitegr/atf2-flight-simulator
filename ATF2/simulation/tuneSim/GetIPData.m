function [data txt] = GetIPData(beam,strack,etrack)
global BEAMLINE
persistent ip1 mqf9


if isempty(ip1)
  ip1=findcells(BEAMLINE,'Name','IP');
  mqf9=findcells(BEAMLINE,'Name','MQF9X');
end

gamma=1.28/0.511e-3;
[nx1,ny1] = GetNEmitFromBeam( beam, 1 );
if strack<mqf9
  [stat,B1]=TrackThru(strack,mqf9,beam,1,1,0);
  [nx,ny] = GetNEmitFromBeam( B1, 1 );
  data.extemit_y=ny/gamma;
  [stat,B1]=TrackThru(mqf9,etrack,B1,1,1,0);
else
  [stat,B1]=TrackThru(strack,etrack,beam,1,1,0);
end
[Tx Ty]=GetUncoupledTwissFromBeamPars(B1,1);
[fitTerm,fitCoef,bsize_corrected,bsize,p] = beamTerms(3,B1);
data.bterms=fitTerm;
data.btsize=bsize;
data.bvals_global=p.Coefficients;
data.bvals=fitCoef;
data.bsize_corrected=bsize_corrected;
[nx,ny] = GetNEmitFromBeam( B1, 1 );
data.emit_x=nx/gamma; data.emit_y=ny/gamma;
data.xpos=mean(B1.Bunch.x(1,:));
data.ypos=mean(B1.Bunch.x(3,:));
data.xsize=std(B1.Bunch.x(1,:)); data.xpsize=std(B1.Bunch.x(2,:));
data.ysize=std(B1.Bunch.x(3,:)); data.ypsize=std(B1.Bunch.x(4,:));
% Gaussian fits to IP dists
rays0=[B1.Bunch.x]';
P=mean(rays0(:,6)); % GeV/c
dp=(rays0(:,6)-P)/P;
rays0(:,6)=dp;
[rays,xv,sigm,sigv]=AnalyzeRays(rays0,P,0,0);
data.xfit=abs(sigv(1));
data.yfit=abs(sigv(3));
data.sigma=cov(B1.Bunch.x');
R=diag(ones(1,6));L=zeros(6,6);L(1,2)=1;L(3,4)=1;
data.xwaist=fminsearch(@(x) minWaist(x,R,L,data.sigma,1),0,optimset('Tolx',1e-6,'TolFun',0.1e-6^2));
data.ywaist=fminsearch(@(x) minWaist(x,R,L,data.sigma,3),0,optimset('Tolx',1e-6,'TolFun',0.1e-6^2));

data.xdisp=Tx.eta;
data.ydisp=Ty.eta;
data.xdp=Tx.etap;
data.ydp=Ty.etap;
data.betax=Tx.beta;
data.betay=data.bsize_corrected(end)^2/(ny1/gamma);
txt=sprintf('\nTarget Spot Sizes: %g / %g (x / x'') %g / %g (y / y'')\nWaist Offsets: %g (x) %g (y)\nDispersion: %g (x) %g (y)\ncoupling (31 / 32): %g / %g',...
  data.xsize,data.xpsize,data.ysize,data.ypsize,data.xwaist,data.ywaist,data.xdisp,data.ydisp,data.sigma(1,3),data.sigma(2,3));


function chi2 = minWaist(x,R,L,sig,dir)

newsig=(R+L.*x(1))*sig*(R+L.*x(1))';
chi2=newsig(dir,dir)^2;

function chi2 = sinFit(x,data,error) %#ok<DEFNU>

chi2=sum( ( data - ( x(1) * sin((1:length(data))/x(2)+2*pi*x(3))+mean(data) ) ).^2 ./ error.^2);
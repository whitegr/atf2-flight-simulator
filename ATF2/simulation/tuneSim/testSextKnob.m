global BEAMLINE PS VERB MDATA INSTR FL %#ok<NUSED>
warning off MATLAB:lscov:RankDefDesignMat
warning off MATLAB:PSTrim_online
warning off MATLAB:MoverTrim_online
warning off MATLAB:FlHwUpdate_nonFSsim
doplot=true;
Model.Measure_ipbcor=false;
Model.genKnobs=false;
setKnobVals=0:1:10;
testTerms={'WaistY'};
FL.SimModel=Model;
ip_instr=findcells(INSTR,'Index',Model.ip_ind);
for iterm=1:length(testTerms)
  for iknob=1:length(setKnobVals)
    fprintf('term: %s size: %g\n',testTerms{iterm},setKnobVals(iknob))
    moves=zeros(4,5);
    moves(1:2,:)=reshape(Model.knob.(testTerms{iterm}).smoves.*setKnobVals(iknob),2,5);
    setSextKnob(Model,Beam0,moves);
    [IPdat Model] = IP_meas( {Beam1 Beam0}, Model );
    knob1(iterm,iknob)=IPdat.Waist_y./Model.knob.WaistY.scale;
    knob2(iterm,iknob)=IPdat.Disp_y./Model.knob.DispY.scale;
    knob3(iterm,iknob)=IPdat.sigma(1,3);%./Model.knob.XpY.scale;
    knob4(iterm,iknob)=IPdat.sigma(2,3);%./Model.knob.XpY.scale;
%     tsize{iterm,iknob}=INSTR{ip_instr}.bcor.tsize;
%     [val,Ind]=sort(abs(tsize{iterm,iknob}),'descend');
%     fprintf('thisterm= %g t1= %g t2=%g t3=%g\n',tsize{iterm,iknob}(testTerms(iterm)),tsize{iterm,iknob}(Ind(1)),...
%       tsize{iterm,iknob}(Ind(2)),tsize{iterm,iknob}(Ind(3)))
%     save testSextKnob tsize
  end % for iknob
end % for iterm
if doplot
  plot(knob1-knob1(1,1),'r'); hold on; plot(knob2-knob2(1,1),'g'); plot(knob3-knob3(1,1),'b'); plot(knob4-knob4(1,1),'k'); hold off;
end % doplot
legend('WaistY','DispY','XY','XpY')
save testSextKnob
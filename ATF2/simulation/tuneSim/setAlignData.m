function setAlignData
global BEAMLINE
load survey_data survey_names survey_y
for idata=1:length(survey_names)
  magind=findcells(BEAMLINE,'Name',survey_names{idata});
  if ~isempty(magind)
    for ind=1:length(magind)
      BEAMLINE{magind(ind)}.Offset(3)=survey_y(idata);
    end % for ind
  end % if magind data
end % for idata
function [IPdat] = MK_correct(Model,Beam,iCP,IPdat)
% Apply first- and second-order sextupole multi-knobs to minimise IP spot
% sizes
%#function mkoutput
global VERB GIRDER PS BEAMLINE FL INSTR %#ok<NUSED>
persistent minSize sgi ips

% indices
if isempty(sgi)
  for isext=1:5
    sgi(isext)=BEAMLINE{FL.SimModel.magGroup.Sext.dB.ClusterList(isext).index(1)}.Girder;
    ips(isext)=BEAMLINE{FL.SimModel.magGroup.Sext.dB.ClusterList(isext).index(1)}.PS;
  end
  iqd0=findcells(BEAMLINE,'Name','QD0FF');
  sgi(6)=BEAMLINE{iqd0(1)}.Girder;
  ips(6)=BEAMLINE{iqd0(1)}.PS;
end

% restore functionality
if isequal(Model,'get_restore')
  IPdat={minSize}; return;
elseif isequal(Model,'set_restore')
  minSize=Beam{1}; return;
end % if restore

oseq={  'XpY' 'WaistY' 'DispY' 'XpY' 'WaistY' 'T322' 'DispY' 'XpY' 'T322' 'WaistY' 'DispY' 'XpY' 'WaistY' 'T322' 'DispY' 'XpY' 'WaistY' 'T322' 'DispY' 'XpY' 'WaistY' 'T322' 'DispY' 'XpY' 'WaistY' 'T322' 'DispY' 'XpY' 'WaistY' 'T322' 'DispY' 'XpY' 'WaistY' 'T322' 'T326'};
% rseq1=[  3e-9   1e-14     10       1   0.5e-9     1     1e-14     1     0.5  0.5e-9    1      1      1e-14     1    0.5e-9   1      1e-14   0.5      1    0.5e-9   1      1e-14    1      0.5  0.5e-9    1      1e-14    1      0.5  0.5e-9   0.1     1e-14   0.05    0.5  0.5e-9    0.1     1e-14  0.05    0.5  0.5e-9    0.1     1e-14  0.05   0.5];
rseq1=[  3e-10   1e-14     10       1   0.5e-10     1     1e-14     1     0.5  0.5e-10    1      1      1e-14     1    0.5e-10   1      1e-14   0.5      1    0.5e-10   1      1e-14    1      0.5  0.5e-10    1      1e-14    1      0.5  0.5e-10   0.1     1e-14   0.05    0.5  0.5e-10    0.1     1e-14  0.05    0.5  0.5e-10    0.1     1e-14  0.05   0.5];
tseq1=[  1e-12  1e-17   0.001    2e-3  1e-12   0.001    1e-17  0.001   2e-3  1e-12   0.001  0.001    1e-17   2e-3   1e-12 0.0001    1e-17  0.0001  2e-3   1e-12  0.0001   1e-17  0.0001  2e-3  1e-12  0.0001    1e-17  0.0001  2e-3  1e-12   0.0001   1e-17  0.0001  2e-3  1e-12    0.0001   1e-17  0.0001  2e-3 1e-12   0.0001    1e-17 0.0001  0.0001];
moves={oseq(:)};
ranges={rseq1};
tolx={tseq1};
tolfun=1e-18;
fprintf('MK_correct Sequence: %d of %d ...\n',iCP,length(moves));

if VERB==2
  displayVar='iter';
else
  displayVar='off';
end

if ~isfield(IPdat,'mk') || iCP>=3
  iFirst=1;
else
  iFirst=length(IPdat.mk.which_move)+1;
  if iFirst>length(moves{iCP}); return; end;
end

% Loop over multiknobs
iterSteps=20;
SQ=sqCouplingKnob(Model.knob.sq);
for im=iFirst:length(moves{iCP})
  %--- Find knob setting that minimises IP vertical spot size
%   curpos=setSextKnob('getmoves');
  mk_findmin('reset');
%   min_move = fminbnd(@(x) mk_findmin(x,moves{iCP}{im},Beam,Model,curpos,SQ),-ranges{iCP}(im),ranges{iCP}(im),...
%     optimset('TolX',tolx{iCP}(im),'TolFun',tolfun,'MaxIter',iterSteps,'Display',displayVar,'OutputFcn',{'mkoutput'}));
  min_move = fminbnd(@(x) mk_findmin(x,moves{iCP}{im},Beam,Model,ips,SQ),-ranges{iCP}(im),ranges{iCP}(im),...
    optimset('TolX',tolx{iCP}(im),'TolFun',tolfun,'MaxIter',iterSteps,'Display',displayVar,'OutputFcn',{'mkoutput'}));
  mkKeep=mk_findmin('getkeep');
%   [jitvals jitord]=doIPJitAnal(mkKeep);
  %--- Make sextupole move based on optimal knob setting
  if ~isfield(IPdat,'mk') || mkKeep(end).dat.ysize_real < IPdat.mk.sigy(end)
    switch moves{iCP}{im}
      case {'DispY' 'WaistY' 'XpY' 'T322' 'T326' 'U3122'} 
%         curpos(1:3,:)=curpos(1:3,:)+reshape(Model.knob.(moves{iCP}{im}).smoves.*min_move*(Model.knob.(moves{iCP}{im}).uScale./Model.knob.(moves{iCP}{im}).scale),3,6);
        curpos=reshape(Model.knob.(moves{iCP}{im}).smoves.*min_move*(Model.knob.(moves{iCP}{im}).uScale./Model.knob.(moves{iCP}{im}).scale),3,6);
      case {'roll1' 'roll2' 'roll3' 'roll4' 'roll5'}
        curpos(3,str2double(moves{iCP}{im}(end)))=curpos(3,str2double(moves{iCP}{im}(end)))+min_move;
      case {'dk1' 'dk2' 'dk3' 'dk4' 'dk5'}
        curpos(4,str2double(moves{iCP}{im}(end)))=curpos(3,str2double(moves{iCP}{im}(end)))+min_move;
      case 'sig13'
        SQ.sig13=min_move;
      otherwise
        error('unkown knob option')
    end % switch iknob
  end
%   if ~strcmp(moves{iCP}{im},'sig13')
%     setSextKnob(Model,Beam{2},curpos);
%   end
  imag=0;
  for isext=ips
    imag=imag+1;
    GIRDER{isext}.MoverSetPt=GIRDER{isext}.MoverSetPt+curpos(1:3,imag)';
  end
  MoverTrim(ips);
  %--- track and save data
  [dat Model]=IP_meas( Beam, Model, true );
  if ~isfield(IPdat,'mk')
    IPdat.mk.lumi(1)=dat.lumi_real;
    IPdat.mk.sigy(1)=dat.ysize_real;
    IPdat.mk.sigy_alt(1)=dat.ysize_alt;
    IPdat.mk.which_move{1}=moves{iCP}{im};
    IPdat.mk.nscans(1)=FL.SimData.trackCount;
    IPdat.mk.moverTime(1)=FL.SimData.moverTime;
    IPdat.mk.minmove=min_move;
    IPdat.mk.dat{1}=dat;
    IPdat.mk.optim{1}=FL.optimizer;
    IPdat.mk.optimMin{1}=mkKeep;
    [X IPdat.mk.maxAber(1)]=max(dat.bcor.tsize);
    IPdat.mk.moverLimits(1)=checkATF2sexts(Model);
%     IPdat.mk.ipjitvals{1}=jitvals;
%     IPdat.mk.jitord{1}=jitord;
  else
    IPdat.mk.lumi=[IPdat.mk.lumi dat.lumi_real];
    IPdat.mk.sigy=[IPdat.mk.sigy dat.ysize_real];
    IPdat.mk.sigy_alt=[IPdat.mk.sigy_alt dat.ysize_alt];
    IPdat.mk.which_move={IPdat.mk.which_move{:} moves{iCP}{im}}; %#ok<CCAT>
    IPdat.mk.nscans=[IPdat.mk.nscans FL.SimData.trackCount];
    IPdat.mk.moverTime=[IPdat.mk.moverTime FL.SimData.moverTime];
    IPdat.mk.minmove=[IPdat.mk.minmove min_move];
    IPdat.mk.dat{end+1}=dat;
    IPdat.mk.optim{end+1}=FL.optimizer;
    IPdat.mk.optimMin{end+1}=mkKeep;
    [X IPdat.mk.maxAber(end+1)]=max(dat.bcor.tsize);
    IPdat.mk.moverLimits(end+1)=checkATF2sexts(Model);
%     IPdat.mk.ipjitvals{end+1}=jitvals;
%     IPdat.mk.jitord{end+1}=jitord;
  end
  if isfield(Model,'Sm')
    fprintf('***** [Knob: %s ] Sig (y): %g SM: %g\n',moves{iCP}{im},dat.ysize_real,dat.scan.smSize)
  else
    fprintf('***** [Knob: %s ] Sig (y): %g\n',moves{iCP}{im},dat.ysize_real)
  end
  if isdeployed || Model.nocompile
    if isempty(minSize) || dat.ysize_real<minSize
      farmSave('data',['mksave_',num2str(Model.nseed)],Model,IPdat);
%       Lucretia2AML('output',['data/mksave_',num2str(Model.nseed),'.aml']);
      minSize = dat.ysize_real;
    end
  end
end % for im


% function [jitvals sortOrder]=doIPJitAnal(keep)
% 
% % Form matrix of jitter freqmodes and covariance elements
% jitvals=zeros(length(keep)-1,length(keep(1).ipjitdata)+10);
% for ik=1:length(keep)-1
%   jitvals(ik,1:length(keep(1).ipjitdata))=keep(ik+1).ipjitdata;
%   xval(ik)=keep(ik+1).optim(ik+1).x;
% end
% 
% % Do singular value decomposition and store top 10 svd modes in jitter
% % matrix
% [U,S,Vt]=svd(jitvals(:,1:length(keep(1).ipjitdata)));
% V=Vt';
% for imode=1:10
%   jitvals(:,length(keep(1).ipjitdata)+imode)=jitvals(:,1:length(keep(1).ipjitdata))*V(imode,:)';
% end
% 
% % Correlate modes with beam size and return correlation strength order
% sz=size(jitvals);
% for imode=1:sz(2)
%   [r,p] = corrcoef(xval,jitvals(:,imode));
%   if ~isempty(find(p<0.1, 1))
%     rcor(imode)=r(1,2);
%   else
%     rcor(imode)=0;
%   end
% end % for imode
% [X sortOrder]=sort(rcor,'descend');

function [ ] = BPM_zeroset( Model, Beam, cmd )
% Set BPM offsets to zero readings
global BEAMLINE INSTR

if isequal(Model,'remove')
  for iBpm=findcells(INSTR,'Class','MONI')
    BEAMLINE{INSTR{iBpm}.Index}.ElecOffset=[0 0];
  end % for iBpm
  return
end % if remove request

% Average readings
nave=10;

for iTrack=1:nave
  if Model.doGM
    Model = myTrack(Beam,Model);
    xdata(iTrack,:)=Model.bpmData.x; %#ok<AGROW>
    ydata(iTrack,:)=Model.bpmData.y; %#ok<AGROW>
  else
    FlHwUpdate;
    xdata(iTrack,:)=cellfun(@(x) x.Data(1),INSTR); %#ok<AGROW>
    ydata(iTrack,:)=cellfun(@(x) x.Data(2),INSTR); %#ok<AGROW>
  end % if Model.doGM
end % for iTrack
xdataMean=mean(xdata,1);
ydataMean=mean(ydata,1);
if exist('cmd','var') && isequal(cmd,'xonly')
  ydataMean=ydataMean.*0;
end % if xonly
for iBpm=findcells(INSTR,'Class','MONI')
  % Set bpm -> 0
  BEAMLINE{INSTR{iBpm}.Index}.ElecOffset=BEAMLINE{INSTR{iBpm}.Index}.ElecOffset-...
    [xdataMean(iBpm) ydataMean(iBpm)];
end

function [beam, mang, mstd] = wakeConv(beam,S,twf_short,yoff)

z=beam.Bunch.x(5,:);
q=beam.Bunch.Q;
[~,bin] = histc(z,S) ; bin(bin==0)=1;
dq = accumarray(bin',q,size(S)) ;
L=1; %L=(4/3)*0.5; % To make mean wake = 0.4 V / mm
% twf_short=twf_short.*(10/cavRadius);
V=[0 yoff.*L.*arrayfun(@(ibin) sum(dq(1:ibin-1).*twf_short(1:ibin-1)) + ...
  (twf_short(1)*dq(ibin))/2,2:length(dq))] ;
beam.Bunch.x(4,bin)=beam.Bunch.x(4,bin)+V(bin)/(1e9*mean(beam.Bunch.x(6,:)));
mang=mean(beam.Bunch.x(4,:));
mstd=std(beam.Bunch.x(4,:));
% beam.Bunch.x(4,:)=beam.Bunch.x(4,:)-mean(beam.Bunch.x(4,:));

% z<0 in head of bunch (arrive first)
function ret = bpmSextOffset(Model,command)
global BEAMLINE GIRDER
persistent bpmRef

ret=[];

% restore functionality
if isequal(Model,'get_restore')
  ret={bpmRef}; return;
elseif isequal(Model,'set_restore')
  bpmRef=command{1}; return;
end % if restore

switch upper(command)
  case 'GET'
    for iSext=length(Model.Gind.Sext)-4:length(Model.Gind.Sext)
      bpmRef{iSext}=GIRDER{Model.Gind.Sext(iSext)}.MoverPos(1:2);
    end
  case 'SET'
    if isempty(bpmRef); error('Use ''get'' before ''set''!'); end;
    for iSext=length(Model.Gind.Sext)-4:length(Model.Gind.Sext)
      bpmNewRef{iSext}=GIRDER{Model.Gind.Sext(iSext)}.MoverPos(1:2); %#ok<AGROW>
      BEAMLINE{Model.magGroup.Sext.Bpm(iSext).Ind}.ElecOffset=BEAMLINE{Model.magGroup.Sext.Bpm(iSext).Ind}.ElecOffset+...
        bpmNewRef{iSext}-bpmRef{iSext};
    end
  otherwise
    error('Unknown command!');
end % switch
function Model = EXTsetup( Model, Beam )
% Set up errors and model parameters for the EXT part of the lattice
% From Woodley files

global BEAMLINE INSTR

% Define EXT end-point
Model.ext.endInd = findcells(BEAMLINE,'Name','BEGFF') ;

% Number of tuning iterations
Model.ext.nSteer=50;
Model.ext.nEta=1;

% EXT BPM resolution
Model.ext.bpm_res = 5e-6;

% EXT feedback parameters
% Model.ext.feedback.bpms_x = findcells(BEAMLINE,'Class','MONI',1,findcells(BEAMLINE,'Name','QBPM6FF'));
% Model.ext.feedback.bpms_y = findcells(BEAMLINE,'Class','MONI',1,findcells(BEAMLINE,'Name','QBPM6FF'));
Model.ext.feedback.bpms_x = findcells(BEAMLINE,'Class','MONI',Model.extStart,Model.ext.endInd);
Model.ext.feedback.bpms_y = findcells(BEAMLINE,'Class','MONI',Model.extStart,Model.ext.endInd);
Model.ext.feedback.cor_x = find(cellfun(@(x) ~isempty(regexp(x.Name,'ZH\d\d?X', 'once')),BEAMLINE))';
Model.ext.feedback.cor_y = find(cellfun(@(x) ~isempty(regexp(x.Name,'ZV\d\d?X', 'once')),BEAMLINE))';
Model.ext.feedback.weight = 0.1 ;

% Wirescanners indicies
Model.ext.mw_ind = findcells(BEAMLINE,'Name','MW*X');
Model.ext.mw_iind = find(cellfun(@(x) ismember(x.Index,Model.ext.mw_ind),INSTR));
if length(Model.ext.mw_ind)~=length(Model.ext.mw_iind)
  error('INSTR indexing error');
end % if length INSTR,mw_ind not match

% Move physics vals to PS's for skew quads
tval=BEAMLINE{Model.magGroup.Quad.dB.ClusterList(Model.magGroup.SQuad_id(1)).index(1)}.Tilt;
squads=findcells(BEAMLINE,'Tilt',tval);
for isq=1:2:length(squads)
  MovePhysicsVarsToPS(squads(isq));
end % for isq

% Beams for EXT tuning: 1=on energy; 2=low energy; 3=high energy
dp=1e-4; % energy offset for making eta plots from tracking
Model.ext.beams=Beam;
Initial0=Model.Initial;
Initial0.Momentum=Model.Initial.Momentum*(1-dp);
Model.ext.beams=[Model.ext.beams;MakeBeam6DSparse(Initial0,3,11,11)];
Initial0.Momentum=Model.Initial.Momentum*(1+dp);
Model.ext.beams=[Model.ext.beams;MakeBeam6DSparse(Initial0,3,11,11)];

% turn on BPM data acquisition in the region from ML1X to QBPM9X
ml1x=findcells(BEAMLINE,'Name','MQF7X');
qbpm9x=findcells(BEAMLINE,'Name','MQF9X');
SetTrackFlags('GetBPMData',1,ml1x,qbpm9x);
SetTrackFlags('GetBPMBeamPars',1,ml1x,qbpm9x);

% find the beginning and end of the section with misaligned quads, and the
% beginning and end of the BH1-2 section:
Model.ext.msim=Model.extStart;
Model.ext.begff=findcells(BEAMLINE,'Name','BEGFF');
Model.ext.bh12beg=findcells(BEAMLINE,'Name','BH1XA');
Model.ext.bh12end=findcells(BEAMLINE,'Name','BH2XB')+1;

% construct response matrices for both of the steering jobs
% Model.ext.BPMSteerStruc=MakeATF2XYSteerStruc;
% Model.ext.BPMLaunchStruc=MakeATF2LaunchSteerStruc;

% construct matrices to use for measuring the dispersion, the vector which
% defines the fractional energy changes to use in the energy scan, and the
% knobs which tune the dispersion
% EnergyScanVector=[-0.002,-0.001,0,0.001,0.002];
% [EtaBeams,MeasEtaStruc]=MakeMeasEtaStruc(EnergyScanVector,Beam);
% MakeGlobalDispersionKnobs;
% [stat ex exp ey eyp]=MakeGlobalDispersionKnobs;
% Model.ext.EtaBeams=EtaBeams; Model.ext.MeasEtaStruc=MeasEtaStruc;
% Model.ext.EtaX=ex; Model.ext.EtaPX=exp;
% Model.ext.EtaY=ey; Model.ext.EtaPY=eyp;

% construct the matrix which converts the beam matrix at the first wire into 
% the sig33 at each wire in the wire scanner system
Model.ext.Emat=MakeATF2Emat;

% define the 4 skew quad power supplies to be multiknobs
[Skew1,Skew2,Skew3,Skew4]=MakeCouplingCorrectionMultiKnobs;
Model.ext.Skew1=Skew1; Model.ext.Skew2=Skew2;
Model.ext.Skew3=Skew3; Model.ext.Skew4=Skew4;
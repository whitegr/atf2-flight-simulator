 clear all global
 jm=findResource;
 job=jm.Jobs;
 jtag=get(job,'Tag');
 job=job(ismember(jtag,'tuneSim'));
% load init_state
sStrCh=[]; ysizes=[]; bpmOffset=[];
ind=0;
for ij=1:length(job)
  fprintf('Processing %d of %d\n',ij,length(job));
  ldata=job(ij).getAllOutputArguments;
  if isempty(ldata); continue; end;
  ind=ind+1;
  Model=ldata{1}; IPdat=ldata{2}; 
  %terms(ind,:)=IPdat.postTune.ipbcor.tsize;
  % disp stuff
  exteta1(:,ind)=IPdat.exteta(:,1);
  exteta2(:,ind)=IPdat.exteta(:,2);
  extyp_ksize(ind)=IPdat.exteta_ypknob;
  %   zv5str(ind)=IPdat.corstr.zv5x/3.2949e-4;
  %   zv6str(ind)=IPdat.corstr.zv6x/3.2949e-4;
  %   zv7str(ind)=IPdat.corstr.zv7x/3.2949e-4;
  %   qs1str(ind)=IPdat.sq.qs1x*-90;
  %   qs2str(ind)=IPdat.sq.qs2x*-90;
  init_dy(ind)=Model.Initial.y.Twiss.eta;
  init_dpy(ind)=Model.Initial.y.Twiss.etap;
  %   if isfield(IPdat.sq,'qk1x')
  %     qk1str(ind)=IPdat.sq.qk1x*-90;
  %     qk2str(ind)=IPdat.sq.qk2x*-90;
  %     qk3str(ind)=IPdat.sq.qk3x*-90;
  %     qk4str(ind)=IPdat.sq.qk4x*-90;
  %   end
  initY(ind)=Model.initTrack.ysize_real;
  preAlignY(ind)=IPdat.postAlign.ysize_real;
  ysizeTune(ind,1:length(IPdat.mk.sigy))=IPdat.mk.sigy;
  ysizeTune_alt(ind,1:length(IPdat.mk.sigy))=IPdat.mk.sigy_alt;
  tuneTime(ind,1:length(IPdat.mk.sigy))=IPdat.mk.nscans.*(1/1.56)/3600;
  totalTuneTime(ind)=IPdat.mk.nscans(end).*(1/1.56)/3600;
  [minTune(ind) I]=min(IPdat.mk.sigy);
  minTune_alt(ind)=IPdat.mk.sigy_alt(I);
  minTuneComp(ind)=(minTune(ind)-initY(ind))./initY(ind);
  minTuneComp_alt(ind)=(minTune_alt(ind)-initY(ind))./initY(ind);
  minTuneTime(ind)=tuneTime(ind,I);
  moverTime(ind,1:length(IPdat.mk.moverTime))=IPdat.mk.moverTime;
  minMoverTime(ind)=IPdat.mk.moverTime(I);
  disp1(ind)=IPdat.preEXT.Disp_y;
  disp2(ind)=IPdat.postEXT.Disp_y;
  c1_1(ind)=IPdat.preEXT.sigma(1,3);
  c2_1(ind)=IPdat.preEXT.sigma(1,4);
  c3_1(ind)=IPdat.preEXT.sigma(2,3);
  c4_1(ind)=IPdat.preEXT.sigma(2,4);
  c1_2(ind)=IPdat.postEXT.sigma(1,3);
  c2_2(ind)=IPdat.postEXT.sigma(1,4);
  c3_2(ind)=IPdat.postEXT.sigma(2,3);
  c4_2(ind)=IPdat.postEXT.sigma(2,4);
  for j = 1:6
    IPdat.preEXT.sigmaEXT(j,j) = sqrt(IPdat.preEXT.sigmaEXT(j,j)) ;
    IPdat.postEXT.sigmaEXT(j,j) = sqrt(IPdat.postEXT.sigmaEXT(j,j)) ;
  end
  for i = 1:5
    for j = i+1:6
      IPdat.postEXT.sigmaEXT(i,j) = IPdat.postEXT.sigmaEXT(i,j)/IPdat.postEXT.sigmaEXT(i,i)/IPdat.postEXT.sigmaEXT(j,j) ;
      IPdat.preEXT.sigmaEXT(i,j) = IPdat.preEXT.sigmaEXT(i,j)/IPdat.preEXT.sigmaEXT(i,i)/IPdat.preEXT.sigmaEXT(j,j) ;
    end
  end
  extc1_1(ind)=IPdat.preEXT.sigmaEXT(1,3);
  extc2_1(ind)=IPdat.preEXT.sigmaEXT(1,4);
  extc3_1(ind)=IPdat.preEXT.sigmaEXT(2,3);
  extc4_1(ind)=IPdat.preEXT.sigmaEXT(2,4);
  extc1_2(ind)=IPdat.postEXT.sigmaEXT(1,3);
  extc2_2(ind)=IPdat.postEXT.sigmaEXT(1,4);
  extc3_2(ind)=IPdat.postEXT.sigmaEXT(2,3);
  extc4_2(ind)=IPdat.postEXT.sigmaEXT(2,4);
  extDisp1_x(ind)=IPdat.preEXT.ExtDisp_x;
  extDisp2_x(ind)=IPdat.postEXT.ExtDisp_x;
  extDisp1_y(ind)=IPdat.preEXT.ExtDisp_y;
  extDisp2_y(ind)=IPdat.postEXT.ExtDisp_y;
  if isfield(IPdat,'mk')
    for ti=1:length(IPdat.mk.sigy)
      if IPdat.mk.sigy(ti)<(minTune(ind)*1.1)
        timeInd=ti;
        break;
      end
    end
    minTuneTime10(ind)=tuneTime(ind,timeInd);
    minMoverTime10(ind)=IPdat.mk.moverTime(timeInd)./3600;
    totalTime(ind)=minTuneTime10(ind)+minMoverTime10(ind);
    minTuneInd(ind)=I;
    minmove(ind,1:length(IPdat.mk.minmove))=IPdat.mk.minmove;
    for iscan=1:length(IPdat.mk.sigy)
      minscan(iscan,ind)=min(IPdat.mk.sigy(1:iscan));
    end
    if length(IPdat.mk.sigy)>=38
      sStrCh=[sStrCh IPdat.mk.sigy(38)-IPdat.mk.sigy(37)];
    end
    qbpm_x(ind,:)=[Model.bpm.QuadActual.x];
    sbpm_x(ind,:)=[Model.bpm.SextActual.x];
    qbpm_y(ind,:)=[Model.bpm.QuadActual.y];
    sbpm_y(ind,:)=[Model.bpm.SextActual.y];
  end
  moverLimits(ind,:)=IPdat.mk.moverLimits;
  bsizeCorrected(ind,:)=IPdat.mk.dat{end}.bcor.bsize_corrected;
  eyn(ind)=IPdat.postAlign.emit_y_norm;
  wy(ind)=IPdat.postAlign.Waist_y;
  dy(ind)=IPdat.postAlign.Disp_y;
  cp1(ind)=IPdat.postAlign.sigma(2,3);
  cp2(ind)=IPdat.postAlign.sigma(1,3);
  ey1(ind)=IPdat.postAlign.emit_y;
  eyn1(ind)=IPdat.postAlign.emit_y_norm;
  y1(ind)=IPdat.postAlign.ysize_real;
  ey2(ind)=IPdat.postAlign.emit_y;
  eyn2(ind)=IPdat.postAlign.emit_y_norm;
  y2(ind)=IPdat.postAlign.ysize_real;
  b1(ind)=(y1(ind)^2)/ey1(ind);
  b2(ind)=(y2(ind)^2)/ey2(ind);
  [X I]=max(IPdat.postAlign.bcor.tsize-Model.initTrack.bcor.tsize);
  maxAber(ind,1)=I;
  tsize(ind,1)=X;
  ysizeCor1(ind,1)=IPdat.postAlign.bcor.bsize_corrected(1);
  ysizeCor2(ind,1)=IPdat.postAlign.bcor.bsize_corrected(2);
  ysizeCor3(ind,1)=IPdat.postAlign.bcor.bsize_corrected(3);
  emity=Model.Initial.y.NEmit/(Model.Initial.Momentum/0.511e-3);
  betay(ind,1)=ysizeCor3(ind,1)^2/emity;
  for iaber=1:length(IPdat.mk.maxAber)
    maxAber(ind,iaber+1)=IPdat.mk.maxAber(iaber);
    tsize(ind,iaber+1)=IPdat.mk.dat{iaber}.bcor.tsize(maxAber(ind,iaber))-Model.initTrack.bcor.tsize(maxAber(ind,iaber));
    ysizeCor1(ind,iaber+1)=IPdat.mk.dat{iaber}.bcor.bsize_corrected(1);
    ysizeCor2(ind,iaber+1)=IPdat.mk.dat{iaber}.bcor.bsize_corrected(2);
    ysizeCor3(ind,iaber+1)=IPdat.mk.dat{iaber}.bcor.bsize_corrected(3);
    betay(ind,iaber+1)=ysizeCor3(ind,iaber+1)^2/emity;
  end
  for imk=1:length(IPdat.mk.optimMin)
    for imin=2:length(IPdat.mk.optimMin{imk})-1
      stepvals{ind,imk}(imin-1,:)=IPdat.mk.optimMin{imk}(imin).dat.bcor.tsize'-Model.initTrack.bcor.tsize';
      stepvals_val{ind,imk}(imin-1)=IPdat.mk.optimMin{imk}(imin).optim(imin).vals.fval;
      stepvals_x{ind,imk}(imin-1)=IPdat.mk.optimMin{imk}(imin).optim(imin).x;
    end
  end
  aberTerms=IPdat.postAlign.bcor.terms;
end % file loop
if strcmp(Model.ipbsm_method,'core')
  temp=ysizeTune;
  ysizeTune=ysizeTune_alt;
  ysizeTune_alt=temp;
  temp=minTune;
  minTune=minTune_alt;
  minTune_alt=temp;
  temp=minTuneComp;
  minTuneComp=minTuneComp_alt;
  minTuneComp_alt=temp;
  clear temp
end
if exist('minTune','var')
  ind=0;
  for ysize=min(minTune):(max(minTune)-min(minTune))/1000:max(minTune)
    ind=ind+1;
    yplot_x(ind)=ysize;
    yplot(ind)=sum(minTune>ysize)./length(minTune);
  end
  ind=0;
  for ysize=min(minTune_alt):(max(minTune_alt)-min(minTune_alt))/1000:max(minTune_alt)
    ind=ind+1;
    yplot_x_alt(ind)=ysize;
    yplot_alt(ind)=sum(minTune_alt>ysize)./length(minTune_alt);
  end
  ind=0;
  for ysize=min(minTuneComp):(max(minTuneComp)-min(minTuneComp))/1000:max(minTuneComp)
    ind=ind+1;
    yplot_x_comp(ind)=ysize;
    yplot_comp(ind)=sum(minTuneComp>ysize)./length(minTuneComp);
  end
  ind=0;
  for ysize=min(minTuneComp_alt):(max(minTuneComp_alt)-min(minTuneComp_alt))/1000:max(minTuneComp_alt)
    ind=ind+1;
    yplot_x_comp_alt(ind)=ysize;
    yplot_comp_alt(ind)=sum(minTuneComp_alt>ysize)./length(minTuneComp_alt);
  end

  ind=0;
  for ytime=min(minTuneTime10):(max(minTuneTime10)-min(minTuneTime10))/1000:max(minTuneTime10)
    ind=ind+1;
    timeplot_x(ind)=ytime;
    timeplot(ind)=sum(minTuneTime10>ytime)./length(minTuneTime10);
  end

  ind=0;
  for ytime=min(totalTime):(max(totalTime)-min(totalTime))/1000:max(totalTime)
    ind=ind+1;
    totalTimeplot_x(ind)=ytime;
    totalTimeplot(ind)=sum(totalTime>ytime)./length(totalTime);
  end
end

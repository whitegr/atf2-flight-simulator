% figure
% hist(preAlignY.*1e6,15)
% hold on
% hist(ysizeTune(:,1).*1e6,15)
% hist(ysizeTune(:,2).*1e6,15)
% hist(ysizeTune(:,3).*1e6,15)
% hist(ysizeTune(:,4).*1e6,15)
% figure
% plot([median(preAlignY) median(ysizeTune(:,1:10))])
errval=[0.3 0.2 0.4 0.6 0.8 1 1.2 1.4 1.6];
[val po]=sort(errval);
yalign=[]; ysize1=[]; ysize2=[]; ysize3=[]; ysize4=[];
for ifile=1:100
  load(sprintf('data/bx10by10/mksave_%d',ifile),'IPdat');
  yalign(end+1)=IPdat.postAlign.ysize_real;
  ysize1(end+1)=IPdat.mk.sigy_alt(1);
  if numel(IPdat.mk.sigy_alt)>1
    ysize2(end+1)=IPdat.mk.sigy_alt(2);
  end
  if numel(IPdat.mk.sigy_alt)>2
    ysize3(end+1)=IPdat.mk.sigy_alt(3);
  end
  if numel(IPdat.mk.sigy_alt)>3
    ysize4(end+1)=IPdat.mk.sigy_alt(4);
  end
end
y1=median(yalign);
ys=sort(yalign); y1_err25=ys(floor(length(ys)/4)); y1_err75=ys(3*floor(length(ys)/4));
y2=median(ysize1);
ys=sort(ysize1); y2_err25=ys(floor(length(ys)/4)); y2_err75=ys(3*floor(length(ys)/4));
y3=median(ysize2);
ys=sort(ysize2); y3_err25=ys(floor(length(ys)/4)); y3_err75=ys(3*floor(length(ys)/4));
y4=median(ysize3);
ys=sort(ysize3); y4_err25=ys(floor(length(ys)/4)); y4_err75=ys(3*floor(length(ys)/4));
y5=median(ysize4);
ys=sort(ysize4); y5_err25=ys(floor(length(ys)/4)); y5_err75=ys(3*floor(length(ys)/4));

for ierr=1:8
  yalign=[]; ysize1=[]; ysize2=[]; ysize3=[]; ysize4=[];
  for ifile=(ierr-1)*100+1:ierr*100
    if exist(sprintf('data/multiRoll_qf1tiltmin/mksave_%d.mat',ifile),'file')
      try
        load(sprintf('data/multiRoll_qf1tiltmin/mksave_%d',ifile),'IPdat');
      catch
        continue
      end
    else
      continue
    end
    yalign(end+1)=IPdat.postAlign.ysize_real;
    ysize1(end+1)=IPdat.mk.sigy_alt(1);
    if numel(IPdat.mk.sigy_alt)>1
      ysize2(end+1)=IPdat.mk.sigy_alt(2);
    end
    if numel(IPdat.mk.sigy_alt)>2
      ysize3(end+1)=IPdat.mk.sigy_alt(3);
    end
    if numel(IPdat.mk.sigy_alt)>3
      ysize4(end+1)=IPdat.mk.sigy_alt(4);
    end
  end
  y1=[y1 median(yalign)];
  ys=sort(yalign); y1_err25=[y1_err25 ys(floor(length(ys)/4))]; y1_err75=[y1_err75 ys(3*floor(length(ys)/4))];
  y2=[y2 median(ysize1)];
  ys=sort(ysize1); y2_err25=[y2_err25 ys(floor(length(ys)/4))]; y2_err75=[y2_err75 ys(3*floor(length(ys)/4))];
  y3=[y3 median(ysize2)];
  ys=sort(ysize2); y3_err25=[y3_err25 ys(floor(length(ys)/4))]; y3_err75=[y3_err75 ys(3*floor(length(ys)/4))];
  y4=[y4 median(ysize3)];
  ys=sort(ysize3); y4_err25=[y4_err25 ys(floor(length(ys)/4))]; y4_err75=[y4_err75 ys(3*floor(length(ys)/4))];
  y5=[y5 median(ysize4)];
  ys=sort(ysize4); y5_err25=[y5_err25 ys(floor(length(ys)/4))]; y5_err75=[y5_err75 ys(3*floor(length(ys)/4))];
end
plot(errval(po),y1_err75(po).*1e6,'b')
hold on
ax=axis;
line([ax(1) ax(2)],[0.31 0.31])
line([ax(1) ax(2)],[0.85 0.85])
plot(errval(po),y1(po).*1e6,'*')
plot(errval(po),y1_err25(po).*1e6,'b')
plot(errval(po),y2(po).*1e6,'r*')
plot(errval(po),y2_err25(po).*1e6,'r')
plot(errval(po),y2_err75(po).*1e6,'r')
plot(errval(po),y3_err25(po).*1e6,'k')
plot(errval(po),y3_err75(po).*1e6,'k')
plot(errval(po),y3(po).*1e6,'k*')
plot(errval(po),y4(po).*1e6,'m*')
plot(errval(po),y4_err75(po).*1e6,'m')
plot(errval(po),y4_err25(po).*1e6,'m')
plot(errval(po),y5(po).*1e6,'g*')
plot(errval(po),y5_err75(po).*1e6,'g')
plot(errval(po),y5_err25(po).*1e6,'g')
xlabel('RMS FFS Magnet Roll Error / mrad')
ylabel('Vertical IP waist size / um')
grid on


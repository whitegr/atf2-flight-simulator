function writelat(fname)
[qn sn mn mn_pole mn_KL K1 K2]=showmags;
% Update quad K1 vals
fid=fopen(fname);
fw=[];
while 1
  tline=fgets(fid);
  if ~ischar(tline), break, end
  for iq=1:length(qn)
    if ~isempty(regexp(tline,sprintf('^%s:',qn{iq}), 'once'))
      regexprep(tline,'K1=-?\d*\.?\d+E?-?\d*',sprintf('K1=%.10g',K1(iq)))
      tline=regexprep(tline,'K1=-?\d*\.?\d+E?-?\d*',sprintf('K1=%.10g',K1(iq)));
    end
  end
  fw=[fw tline];
end
fclose(fid);
fid=fopen(fname,'w');
fprintf(fid,fw);
fclose(fid);
% Update sext K2 vals

fid=fopen(fname);
fw=[];
while 1
  tline=fgets(fid);
  if ~ischar(tline), break, end
  for is=1:length(sn)
    if ~isempty(regexp(tline,sprintf('^%s:',sn{is}), 'once'))
      regexprep(tline,'K2=-?\d*\.?\d+E?-?\d*',sprintf('K2=%.10g',K2(is)))
      tline=regexprep(tline,'K2=-?\d*\.?\d+E?-?\d*',sprintf('K2=%.10g',K2(is)));
    end
  end
  fw=[fw tline];
end
fclose(fid);
fid=fopen(fname,'w');
fprintf(fid,fw);
fclose(fid);

fid=fopen(fname);
fw=[];
while 1
  tline=fgets(fid);
  if ~ischar(tline), break, end
  for im=1:length(mn)
    if ~isempty(regexp(tline,sprintf('^%s:',mn{im}), 'once'))
      for ip=1:length(mn_pole{im})
        if ~mn_pole{im}; continue; end;
        if ~isempty(regexp(tline,sprintf('K%dL=&',mn_pole{im}(ip)),'once'))
          fw=[fw tline];
          tline=fgets(fid);
          regexprep(tline,'-?\\d*\\.?\\d+E?-?\\d*',sprintf('%.10G',mn_KL{im}(ip)))
          tline=regexprep(tline,'-?\\d*\\.?\\d+E?-?\\d*',sprintf('%.10G',mn_KL{im}(ip)));
        else
          while isempty(regexp(tline,sprintf('K%dL=-?\\d*\\.?\\d+E?-?\\d*',mn_pole{im}(ip)),'once'))
            fw=[fw tline];
            tline=fgets(fid);
          end
          regexprep(tline,sprintf('K%dL=-?\\d*\\.?\\d+E?-?\\d*',mn_pole{im}(ip)),sprintf('K%dL=%.10G',mn_pole{im}(ip),mn_KL{im}(ip)))
          tline=regexprep(tline,sprintf('K%dL=-?\\d*\\.?\\d+E?-?\\d*',mn_pole{im}(ip)),sprintf('K%dL=%.10G',mn_pole{im}(ip),mn_KL{im}(ip)));
        end
      end
    end
  end
  fw=[fw tline];
end
fclose(fid);
fid=fopen(fname,'w');
fprintf(fid,fw);
fclose(fid);
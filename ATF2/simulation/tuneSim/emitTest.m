function emitTest(correctMethod)
global BEAMLINE PS GIRDER FL KLYSTRON %#ok<NUSED>
% Correction method:
% 1 = Javier's method
% 2 = lsqnonlin method
% 3 = quad scan method

% ================
% Load beamline data
% ================
load ../../FlightSim/latticeFiles/src/v4.5/ATF2lat_BX2p5BY5
FL.SimModel=Model;
FL.SimModel.opticsVersion=4.5;
FL.SimModel.opticsName='BX2p5BY5';

% ================
% Add transverse deflecting cavities
% ================
addTC;

% =============
% Introduce coupling terms into beam
% =============
% beam = beamCouple(beam,[<xy> <x'y> <xy'> <x'y'> <yz>])
%  coupling terms are applied as a rotation matrix between the coupled
%  dimensions (rotation angle in degrees passed by this function)
CoupledBeam=beamCouple(Beam1,[[-6 0 0 10] 0]);

% ==============
% KEX2 quad field
% ==============
kexmult=findcells(BEAMLINE,'Name','KEX2MULT');
for ik=1:length(kexmult)
  BEAMLINE{kexmult(ik)}.dB=0;
end

% ====================
% Plot beam diagnostics and correct coupling and wake effects
% ====================
TCcor=[0 0]; bc=[0 0 0 0];
% == First display beam diagnostics
for ntry=1:1
  data=beamDiag(CoupledBeam,Model,1); drawnow;
  % --- Javier's routine (also applies correction)
  if correctMethod==1
    for itry=1:3
      [stat bc]=correctcoupling(Beam1,data.otr.sig13);
      if itry<3; data=beamDiag(CoupledBeam,Model,0); end;
      emitTry(itry)=data.otr.emity;
    end
    fprintf('Emittance for all iterations:\n')
    disp(emitTry)
    % ---
    % --- lsqnonlin method (also applies correction)
  elseif correctMethod==2
    [stat bc]=extCouplingCorrect(Beam1,data.otr.sig13,bc,Model);
    [stat bc]=ccorScan(CoupledBeam,Model,bc);
    % ---
    % --- sequential quad scan (of emit_y) (also applies correction)
  elseif correctMethod==3
    for itry=1:3
      [stat bc]=ccorScan(CoupledBeam,Model,bc);
      data=beamDiag(CoupledBeam,Model,0);
      emitTry(itry)=data.otr.emity;
    end
    fprintf('Emittance for all iterations:\n')
    disp(emitTry)
  end
  beamDiag(CoupledBeam,Model,1); drawnow;
  % == correct for wakefield effects with TCAVS
%   TCcor=fminsearch(@(x) minfuntc(x,CoupledBeam,Model),TCcor,optimset('Display','iter'));
%   minfuntc(TCcor,CoupledBeam,Model);
%   fprintf('TC0X Ampl: %g kV\nTC1X Ampl: %g kV\n',TCcor.*[1e3 1e3]);
  % TCcor=fminsearch(@(x) minfuntcff(x,CoupledBeam,Model),[0 0],optimset('Display','iter'));
  % fprintf('TC0FF Ampl: %g kV Phase: %g rad\n',TCcor(1)*1e3,TCcor(2));
  % minfuntcff(TCcor,CoupledBeam,Model);
  % == Re-display beam diagnostics post-correction
end
fprintf('QK strengths: %g %g %g %g\n',bc)
beamDiag(CoupledBeam,Model,1);


function [stat Bcorrect]=ccor(ModelBeam,sig13,bc,Model)
stat{1}=1;
% first model coupling parameters of incoming beam
cpar=lsqnonlin(@(x) minfun1(x,ModelBeam,sig13),bc,[],[],optimset('Display','iter'));
CoupledBeam=beamCouple(ModelBeam,cpar);
% find QK / TCAV solution that removes fitted coupled beam
Bcorrect=lsqnonlin(@(x) minfun2(x,CoupledBeam,Model),bc,-0.22,0.22,optimset('Display','iter'));
minfun2(Bcorrect,CoupledBeam,Model);

function [stat Bcorrect]=ccorScan(CoupledBeam,Model,bc)
stat{1}=1;
%  scan QK magnets on coupled beam
Bcorrect=bc;
cval=1e9;
for iqk=1:4
  Bcorrect(iqk)=fminbnd(@(x) minfun2(x,CoupledBeam,Model,iqk),-0.22,0.22,optimset('Display','iter'));
  cvalNew=minfun2(Bcorrect,CoupledBeam,Model);
  if cvalNew>cval
    Bcorrect(iqk)=bc(iqk);
    minfun2(Bcorrect,CoupledBeam,Model);
  else
    cval=cvalNew;
  end
end

function F=minfun1(x,beam,sig13)
beam=beamCouple(beam,x);
F=(getCoup(beam)-sig13)./1e-15;

function F=minfuntc(x,beam,Model)
global BEAMLINE KLYSTRON
persistent tc
if isempty(tc)
  tc=findcells(BEAMLINE,'Name','TC*X');
  tc=[BEAMLINE{tc(1)}.Klystron BEAMLINE{tc(2)}.Klystron];
end
KLYSTRON(tc(1)).Ampl=x(1);% KLYSTRON(tc(1)).Phase=x(2);
KLYSTRON(tc(2)).Ampl=x(2);% KLYSTRON(tc(2)).Phase=x(4);
data=beamDiag(beam,Model,0);
F=data.otr.emity;

function F=minfuntcff(x,beam,Model)
global KLYSTRON BEAMLINE
persistent tc
if isempty(tc)
  tc=findcells(BEAMLINE,'Name','TC0FF');
  tc=BEAMLINE{tc}.Klystron;
end
KLYSTRON(tc).Ampl=x(1);
KLYSTRON(tc).Phase=x(2);
[stat bo]=TrackThru(1,Model.ip_ind,beam,1,1,0);
F=std(bo.Bunch.x(3,:));

function F=minfun2(x,beam,Model,iqkscan)
global BEAMLINE PS
persistent qk
if isempty(qk)
  qkind=findcells(BEAMLINE,'Name','QK*X');
  qk=[];
  for iqk=1:2:length(qkind)
    qk(end+1)=BEAMLINE{qkind(iqk)}.PS;
  end
end
if exist('iqkscan','var')
  iqind=iqkscan; x=ones(1,4).*x;
else
  iqind=1:4;
end
for iq=iqind
  PS(qk(iq)).Ampl=x(iq);
end
data=beamDiag(beam,Model,0);
F=data.otr.emity;


function [stat Bcorrect]=correctcoupling(ModelBeam,sig13)
global PS BEAMLINE FL
persistent otrind skewind skewps

if isempty(otrind)
  for ind = 0:3
    otrind(ind+1) = findcells(BEAMLINE,'Name',['OTR',num2str(ind),'X']);
  end
  for ind = 1:4
    temp = findcells(BEAMLINE,'Name',['QK',num2str(ind),'X']);
    skewind(ind)=temp(2);
  end
  for n=1:4
    skewps(n)=BEAMLINE{skewind(n)}.PS;
  end
end
for n=1:4
  psinit(n)=PS(skewps(n)).Ampl;
end

otruse=ones(1,4);
intensities=[-18 -10 -5 0 5 10 18];
IBlookup=[-20 -16 -12 -8 -4 0 4 8 12 16 20;
  -0.2227 -0.1779 -0.1331 -0.0884 -0.0439 0 0.0439 0.0884 0.1331 0.1779 0.2227];
B = interp1(IBlookup(1,:),IBlookup(2,:),intensities);


[stat,beam_EXT]=TrackThru(1,FL.SimModel.extStart,ModelBeam,1,1,0);

%GET for different B for each of the 4 skews the coupling terms in each OTR
for ind=1:4%for the different skews
  for j=1:length(B)%for the different B
    
    %all skews off except i
    for n=1:4
      PS(skewps(n)).Ampl=0;
      PS(skewps(n)).SetPt=0;
    end
    PS(skewps(ind)).Ampl=1;
    PS(skewps(ind)).SetPt=1;
    
    %put B in the skew
    BEAMLINE{skewind(ind)-1}.B=B(j);
    BEAMLINE{skewind(ind)}.B=B(j);
    
    %track the beam from EXT to all 4 OTRs
    [stat,beam_OTR0]=TrackThru(FL.SimModel.extStart,otrind(1),beam_EXT,1,1,0);
    [x,sigma_OTR0] = GetBeamPars(beam_OTR0,1);sigma_OTR0=sigma_OTR0(1:4,1:4);
    [stat,beam_OTR1]=TrackThru(otrind(1),otrind(2),beam_OTR0,1,1,0);
    [x,sigma_OTR1] = GetBeamPars(beam_OTR1,1);sigma_OTR1=sigma_OTR1(1:4,1:4);
    [stat,beam_OTR2]=TrackThru(otrind(2),otrind(3),beam_OTR1,1,1,0);
    [x,sigma_OTR2] = GetBeamPars(beam_OTR2,1);sigma_OTR2=sigma_OTR2(1:4,1:4);
    [stat,beam_OTR3]=TrackThru(otrind(3),otrind(4),beam_OTR2,1,1,0);
    [x,sigma_OTR3] = GetBeamPars(beam_OTR3,1);sigma_OTR3=sigma_OTR3(1:4,1:4);
    
    coup(ind,1,j)=sigma_OTR0(1,3);
    coup(ind,2,j)=sigma_OTR1(1,3);
    coup(ind,3,j)=sigma_OTR2(1,3);
    coup(ind,4,j)=sigma_OTR3(1,3);
    
  end
end
%Do linear fit and build response matrix
for ind=1:4 %for each skew quad
  for k=1:4
    temp(1:length(B))=coup(ind,k,:);
    c=polyfit(B(:),temp(:),1);
    R(k,ind)=c(1);
  end
end

%Calculate INT to correct
Bcorrect=lscov(R(find(otruse),:),sig13');

%Calculate the final set value
for ind=1:4
  Bcorrect(ind)=psinit(ind)-Bcorrect(ind);
end

% %Change the skew strengths
for ind=1:4
  PS(skewps(ind)).SetPt=Bcorrect(ind);
  stat=PSTrim(skewps(ind));
end

function addTC
global BEAMLINE KLYSTRON
% tcx0 = RFStruc( 0.1, 1, 0.25, 2856, 0, 0, 0, 4e-2, 'TC0X', 1 );
tcx0 = RFStruc( 0.1, 1, 0.25, 2142, 0, 0, 0, 4e-2, 'TC0X', 1 );
% tcx0 = RFStruc( 0.1, 1, 0.25, 5700, 0, 0, 0, 4e-2, 'TC0X', 1 );
tcx0.Tilt=pi/2;
tcx1=tcx0; tcx1.Name='TC1X';
tc0ff=tcx0; tc0ff.Name='TC0FF';
% d1=findcells(BEAMLINE,'Name','L009B'); d2=findcells(BEAMLINE,'Name','L010E');
d1=findcells(BEAMLINE,'Name','L105B'); d2=findcells(BEAMLINE,'Name','L109B');
d3=findcells(BEAMLINE,'Name','L212B');
d1=d1(1); d2=d2(1); d3=d3(1);
BEAMLINE{d1}.L=BEAMLINE{d1}.L-0.1;
BEAMLINE{d2}.L=BEAMLINE{d2}.L-0.1;
BEAMLINE{d3}.L=BEAMLINE{d3}.L-0.1;
BEAMLINE=[BEAMLINE(1:d1); tcx0; BEAMLINE(d1+1:d2); tcx1; BEAMLINE(d2+1:d3); tc0ff; BEAMLINE(d3+1:end)];
SetSPositions( 1, length(BEAMLINE), 0 );
tc=findcells(BEAMLINE,'Name','TC*');
for itc=1:length(tc)
  BEAMLINE{tc(itc)}.P=BEAMLINE{d1}.P;
  AssignToKlystron( tc(itc), length(KLYSTRON)+1 );
  KLYSTRON(end).Ampl=0;
end


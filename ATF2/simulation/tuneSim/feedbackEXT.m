function [] = feedbackEXT(Model,command)
global BEAMLINE PS VERB INSTR FL
persistent R BpmHan PsList PsLast fbEleLast

PsThis=[PS(Model.PSind.Quad).Ampl];
fbEleThis=[Model.ext.feedback.bpms_x Model.ext.feedback.bpms_y Model.ext.feedback.cor_x Model.ext.feedback.cor_y Model.ext.feedback.weight];

% Get BPM/Corrector lists and R matricies
% First time subroutine called or Quad PS changed?
if isempty(R) || isempty(PsLast) || (sum(PsThis==PsLast)~=length(PsThis)) || ~isequal(fbEleThis,fbEleLast)
  fbEleLast=fbEleThis;
  if VERB; disp('Recalculating Response Matrix for steering...'); end;
  BpmList.x=[]; BpmList.y=[]; BpmHan.x=[]; BpmHan.y=[];
  for xlist=Model.ext.feedback.bpms_x
    if ~isequal(INSTR{findcells(INSTR,'Index',xlist)}.Class,'NOHW')
      BpmList.x=[BpmList.x xlist];
      BpmHan.x=[BpmHan.x findcells(INSTR,'Index',xlist)];
    end % if hw
  end % for xlist
  for ylist=Model.ext.feedback.bpms_y
    if ~isequal(INSTR{findcells(INSTR,'Index',ylist)}.Class,'NOHW')
      BpmList.y=[BpmList.y ylist];
      BpmHan.y=[BpmHan.y findcells(INSTR,'Index',ylist)];
    end % if hw
  end % for ylist
  if length([BpmList.x BpmList.y]) ~= length([BpmHan.x BpmHan.y]); error('BPM location error!'); end;
  PsList.x=Model.ext.feedback.cor_x ;
  PsList.y=Model.ext.feedback.cor_y ;
  R=zeros(length([BpmList.x BpmList.y]),length([PsList.x PsList.y]));
  nKik=0;
  for iKik=[PsList.x PsList.y]
    nKik=nKik+1;
    nBpm=0;
    for iBpm=[BpmList.x BpmList.y]
      nBpm=nBpm+1;
      if iBpm>iKik
        [stat, Rm] = RmatAtoB_cor( iKik, iBpm ); if ~stat{1}; error(stat{2}); end;
      else
        Rm=zeros(6);
      end
      if (nBpm <= length(BpmList.x)) && (nKik <= length(PsList.x))
        R(nBpm,nKik)=Rm(1,2);
      elseif (nBpm > length(BpmList.x)) && (nKik <= length(PsList.x))
        R(nBpm,nKik)=Rm(3,2);
      elseif (nBpm <= length(BpmList.x)) && (nKik > length(PsList.x))      
        R(nBpm,nKik)=Rm(1,4);
      else
        R(nBpm,nKik)=Rm(3,4);
      end % if
    end % iBpm
  end % iKik
  R=R.*(R>0.1);
end % is first call?
PsLast=PsThis;

% get FB BPM readings
fbBpm=zeros(length([BpmHan.x BpmHan.y]),1); nBpm=0; W=ones(length([BpmHan.x BpmHan.y]),1).*1e-20;
for iBpm=[BpmHan.x BpmHan.y]
  nBpm=nBpm+1;
  if nBpm<=length(BpmHan.x)
    fbBpm(nBpm)=INSTR{iBpm}.Data(1);
    W(nBpm)=INSTR{iBpm}.Res(1);
  else
    fbBpm(nBpm)=INSTR{iBpm}.Data(2);
    W(nBpm)=INSTR{iBpm}.Res(1);
  end % if
  if INSTR{iBpm}.Index>FL.Region.EXT.ind(end)
    W(nBpm)=W(nBpm).*0.9;
  end % if ffs bpm
end % for iBpm
W(~W)=max(W);

% compute correction
setKik=lscov(R,-fbBpm.*Model.ext.feedback.weight);

% Apply correction
switch upper(command)
  case 'RESET'
    for iKik=[PsList.x PsList.y]
      PS(BEAMLINE{iKik}.PS).SetPt=0; PSTrim(BEAMLINE{iKik}.PS);
    end % for iKik
  case 'CORRECT'
    nKik=0;
    for iKik=[PsList.x PsList.y]
      nKik=nKik+1;
%       ps1=PS(BEAMLINE{iKik}.PS).Ampl;
      PS(BEAMLINE{iKik}.PS).SetPt = (PS(BEAMLINE{iKik}.PS).Ampl + setKik(nKik));
      PSTrim(BEAMLINE{iKik}.PS);
%       fprintf('(%d) Before: %g After: %g\n',BEAMLINE{iKik}.PS,ps1,PS(BEAMLINE{iKik}.PS).Ampl)
    end % for iKik
    pause(0.01)
  otherwise
    error('Unknown Command!');
end % switch command

function vals = funcRestore(cmd,newvals)
% vals = funcRestore(cmd,newvals)
% Get/Restore persistent values from functions for tuneSim environment use
% for saving/restoring store points for farm jobs

%#function beamTerms bpmSextOffset fb_5Hz feedbackFF1 feedbackFF2 gmMove IP_meas MK_correct myTrack qbba_dfs setSextKnob SextMultiKnob


vals=[];
funcList={...
  'bpmSextOffset';
  'fb_5Hz';
  'feedbackFF1';
  'feedbackFF2';
  'gmMove';
  'IP_meas';
  'MK_correct';
  'myTrack';
  'qbba_dfs';
  'setSextKnob';
  'SextMultiKnob'};

if exist('newvals','var')
  if length(newvals)~=length(funcList)
    error('Length of newvals cell must == length of funcList vector!')
  end % if newvals length wrong
end % if newvals passed

for ifunc=1:length(funcList)
  if isequal(cmd,'get')
    evalc(['vals{ifunc}=',funcList{ifunc},'(''get_restore'')']);
  elseif isequal(cmd,'set')
    evalc([funcList{ifunc},'(''set_restore'',newvals{ifunc})']);
  else
    error('unknown cmd')
  end % if get/set
end % for ifunc

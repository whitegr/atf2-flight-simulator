function [stat Knobs_out orthret skdata]=GenerateKnobs(Beam1,target,docheck,doskewquad,notrackcond,dosumknob)
global GIRDER BEAMLINE PS FL
persistent Knobs qs1ps qs2ps

orthret=[];
warning off MATLAB:lscov:RankDefDesignMat
warning off MATLAB:PSTrim_online
warning off MATLAB:MoverTrim_online
warning off MATLAB:FlHwUpdate_nonFSsim
warning off MATLAB:rankDeficientMatrix

stat{1}=1; Knobs_out=[];

if exist('docheck','var') && docheck==2
  load userData/GenerateSextIPKnobs_save
  %   load userData/KEKknobs
else
  % Get track points
  strack=findcells(BEAMLINE,'Name','BEGFF');
  if target==1
    etrack=findcells(BEAMLINE,'Name','MW1IP');
  elseif target==2
    etrack=findcells(BEAMLINE,'Name','IP');
  else
    etrack=[];
  end
  
  % sum knob
  if ~exist('dosumknob','var')
    dosumknob=false;
  end
  
  if isempty(etrack)
    stat{1}=-1; stat{2}='Unknown requested target';
    return
  end
  
  % Check Sextupoles powered on and near proper strengths
  for isext=1:5
    sgi(isext)=BEAMLINE{FL.SimModel.magGroup.Sext.dB.ClusterList(isext).index(1)}.Girder;
    ips(isext)=BEAMLINE{FL.SimModel.magGroup.Sext.dB.ClusterList(isext).index(1)}.PS;
    if PS(ips(isext)).Ampl<0.95 || PS(ips(isext)).Ampl>1.05
      warning('Lucretia:Floodland:GenerateSextIPKnobs_sextStr','%s deviates from design value by >5%',...
        BEAMLINE{FL.SimModel.magGroup.Sext.dB.ClusterList(isext).index(1)}.Name);
    end
  end
  iqd0=findcells(BEAMLINE,'Name','QD0FF');
  sgi(6)=BEAMLINE{iqd0(1)}.Girder;
  ips(6)=BEAMLINE{iqd0(1)}.PS;
  
  % Parameters for Sextupole knob determination
  nmoves=5; move_range=100e-6;
  Smoves=0:move_range/(nmoves-1):move_range;
  maxSEXT=max(abs(Smoves));
  T=zeros(8,length(sgi)*3);
  sig3_x=zeros(length(sgi),nmoves);
  sig3_y=zeros(length(sgi),nmoves);
  
  % Switch off correctors, skew quads, zero girders, zero offsets for tracking purposes
  if ~exist('notrackcond','var') || ~notrackcond
    soind=[findcells(BEAMLINE,'Class','XCOR',strack,etrack) findcells(BEAMLINE,'Class','YCOR',strack,etrack)];
    tind=findcells(BEAMLINE,'Tilt',[],strack,etrack);
    soind=[soind tind(cellfun(@(x) x.Tilt(1)~=0,{BEAMLINE{tind}}))]; %#ok<*CCAT1>
    for ind=soind
      if isfield(BEAMLINE{ind},'PS')
        PS(BEAMLINE{ind}.PS).Ampl=0;
      end
    end
    for igir=1:length(GIRDER)
      if isfield(GIRDER{igir},'MoverPos')
        GIRDER{igir}.MoverPos=[0 0 0];
      end
    end
    offEle=findcells(BEAMLINE,'Offset',[],strack,etrack);
    initOffset=cell(1,length(offEle));
    for iele=1:length(offEle)
      initOffset{iele}=BEAMLINE{offEle(iele)}.Offset;
      BEAMLINE{offEle(iele)}.Offset=BEAMLINE{offEle(iele)}.Offset.*0;
    end
  end
  
  % Track beam to start point
  [stat Beam1]=TrackThru(findcells(BEAMLINE,'Name','IEX'),strack,Beam1,1,1,0);
  if stat{1}~=1; error(stat{2}); end;
  Beam0 = Beam1;
  Beam0.Bunch.x=mean(Beam1.Bunch.x,2);
  Beam0.Bunch.Q=mean(Beam1.Bunch.Q,2);
  Beam0.Bunch.stop=mean(Beam1.Bunch.stop,2);
  
  % Get Target IP beam data
  [data_init txt] = GetIPData(Beam1,strack,etrack);
  fprintf('Initial data: %s\n',txt)
  corterms=data_init.bterms;
  
  % Just run checks of knobs if requested
  if docheck==2 && ~isempty(Knobs)
    orthret=runDoCheck(Knobs,sgi,Beam1,strack,etrack);
    return
  elseif docheck==2 && isempty(Knobs)
    stat{1}=0; stat{2}='Knobs not yet generated';
    return
  end
  if isempty(qs1ps)
    qs1=findcells(BEAMLINE,'Name','QS1X'); qs1ps=BEAMLINE{qs1(1)}.PS;
    qs2=findcells(BEAMLINE,'Name','QS2X'); qs2ps=BEAMLINE{qs2(1)}.PS;
  end
  xWaist_x=zeros(length(sgi),length(Smoves));
  xWaist_y=xWaist_x; xWaist_tilt=xWaist_x; yWaist_x=xWaist_x; yWaist_y=xWaist_x; yWaist_tilt=xWaist_x; sig16_x=xWaist_x;
  sig16_y=xWaist_x; sig16_tilt=xWaist_x; sig36_x=xWaist_x; sig36_y=xWaist_x; sig36_tilt=xWaist_x;
  sig3_x=xWaist_x; sig3_y=xWaist_x; sig3_tilt=xWaist_x; T326_x=xWaist_x; T326_y=xWaist_x; T326_tilt=xWaist_x;
  T324_x=xWaist_x; T324_y=xWaist_x; T324_tilt=xWaist_x; T322_x=xWaist_x; T322_y=xWaist_x; T322_tilt=xWaist_x;
  % make knobs
  for iSext=1:length(sgi)
    GIRDER{sgi(iSext)}.MoverPos=[0 0 0];
    fprintf('Scanning Sext %d (x)\n',iSext)
    % move in x
    for imove=1:length(Smoves)
      if iSext~=6
        GIRDER{sgi(iSext)}.MoverPos=[Smoves(imove) 0 0];
      end
      % Get IP params
      data = GetIPData(Beam1,strack,etrack);
      sig3_x(iSext,imove) = data.sigma(2,3);
      xWaist_x(iSext,imove)=data.xwaist;
      yWaist_x(iSext,imove)=data.ywaist;
      GIRDER{sgi(iSext)}.MoverPos=[0 0 0];
      sig16_x(iSext,imove) = data.xdisp; %sigma(1,6);
      sig26_x(iSext,imove) = data.xdp; %sigma(2,6);
      sig36_x(iSext,imove) = data.ydisp; %sigma(3,6);
      sig46_x(iSext,imove) = data.ydp; %sigma(4,6);
      T322_x(iSext,imove) = data.bvals(arrayfun(@(x) isequal(data.bterms(x,:),[0 2 0 0 0 0]),1:length(data.bterms)));
      T326_x(iSext,imove) = data.bvals(arrayfun(@(x) isequal(data.bterms(x,:),[0 1 0 0 0 1]),1:length(data.bterms)));
      T324_x(iSext,imove) = data.bvals(arrayfun(@(x) isequal(data.bterms(x,:),[1 2 0 0 0 0]),1:length(data.bterms)));
      T312_x(iSext,imove) = data.bvals(arrayfun(@(x) isequal(data.bterms(x,:),[1 1 0 0 0 0]),1:length(data.bterms)));
      T311_x(iSext,imove) = data.bvals(arrayfun(@(x) isequal(data.bterms(x,:),[2 0 0 0 0 0]),1:length(data.bterms)));
      U3112_x(iSext,imove) = data.bvals(arrayfun(@(x) isequal(data.bterms(x,:),[2 1 0 0 0 0]),1:length(data.bterms)));
      T324_x(iSext,imove) = data.bvals(arrayfun(@(x) isequal(data.bterms(x,:),[0 1 0 1 0 0]),1:length(data.bterms)));
      T322_x(iSext,imove) = data.bvals(arrayfun(@(x) isequal(data.bterms(x,:),[0 2 0 0 0 0]),1:length(data.bterms)));
      U3111_x(iSext,imove) = data.bvals(arrayfun(@(x) isequal(data.bterms(x,:),[3 0 0 0 0 0]),1:length(data.bterms)));
      U3222_x(iSext,imove) = data.bvals(arrayfun(@(x) isequal(data.bterms(x,:),[0 3 0 0 0 0]),1:length(data.bterms)));
      U3224_x(iSext,imove) = data.bvals(arrayfun(@(x) isequal(data.bterms(x,:),[0 2 0 1 0 0]),1:length(data.bterms)));
      S31_x(iSext,imove) = data.bvals(arrayfun(@(x) isequal(data.bterms(x,:),[1 0 0 0 0 0]),1:length(data.bterms)));
      corvals_x{iSext,imove} = data.bvals;
      cortsize_x{iSext,imove} = data.btsize;
      betay_x(iSext,imove) = data.betay;
    end
    % Move in y
    fprintf('Scanning Sext %d (y)\n',iSext)
    for imove=1:length(Smoves)
      if iSext~=6
        GIRDER{sgi(iSext)}.MoverPos=[0 Smoves(imove) 0];
      end
      % Get IP params
      data = GetIPData(Beam1,strack,etrack);
      sig3_y(iSext,imove) = data.sigma(2,3);
      xWaist_y(iSext,imove)=data.xwaist;
      yWaist_y(iSext,imove)=data.ywaist;
      GIRDER{sgi(iSext)}.MoverPos=[0 0 0];
      sig16_y(iSext,imove) = data.xdisp; %sigma(1,6);
      sig26_y(iSext,imove) = data.xdp; %sigma(2,6);
      sig36_y(iSext,imove) = data.ydisp; %sigma(3,6);
      sig46_y(iSext,imove) = data.ydp; %sigma(4,6);
      T322_y(iSext,imove) = data.bvals(arrayfun(@(x) isequal(data.bterms(x,:),[0 2 0 0 0 0]),1:length(data.bterms)));
      T326_y(iSext,imove) = data.bvals(arrayfun(@(x) isequal(data.bterms(x,:),[0 1 0 0 0 1]),1:length(data.bterms)));
      T324_y(iSext,imove) = data.bvals(arrayfun(@(x) isequal(data.bterms(x,:),[1 2 0 0 0 0]),1:length(data.bterms)));
      T312_y(iSext,imove) = data.bvals(arrayfun(@(x) isequal(data.bterms(x,:),[1 1 0 0 0 0]),1:length(data.bterms)));
      T311_y(iSext,imove) = data.bvals(arrayfun(@(x) isequal(data.bterms(x,:),[2 0 0 0 0 0]),1:length(data.bterms)));
      U3112_y(iSext,imove) = data.bvals(arrayfun(@(x) isequal(data.bterms(x,:),[2 1 0 0 0 0]),1:length(data.bterms)));
      T324_y(iSext,imove) = data.bvals(arrayfun(@(x) isequal(data.bterms(x,:),[0 1 0 1 0 0]),1:length(data.bterms)));
      T322_y(iSext,imove) = data.bvals(arrayfun(@(x) isequal(data.bterms(x,:),[0 2 0 0 0 0]),1:length(data.bterms)));
      U3111_y(iSext,imove) = data.bvals(arrayfun(@(x) isequal(data.bterms(x,:),[3 0 0 0 0 0]),1:length(data.bterms)));
      U3222_y(iSext,imove) = data.bvals(arrayfun(@(x) isequal(data.bterms(x,:),[0 3 0 0 0 0]),1:length(data.bterms)));
      U3224_y(iSext,imove) = data.bvals(arrayfun(@(x) isequal(data.bterms(x,:),[0 2 0 1 0 0]),1:length(data.bterms)));
      S31_y(iSext,imove) = data.bvals(arrayfun(@(x) isequal(data.bterms(x,:),[1 0 0 0 0 0]),1:length(data.bterms)));
      corvals_y{iSext,imove} = data.bvals;
      cortsize_y{iSext,imove} = data.btsize;
      betay_y(iSext,imove) = data.betay;
    end
    % Move in tilt
    %     fprintf('Scanning Sext %d (tilt)\n',iSext)
    %     for imove=1:length(Smoves)
    %       GIRDER{sgi(iSext)}.MoverPos=[0 0 Smoves(imove)*10];
    %       % Get IP params
    %       data = GetIPData(Beam1,strack,etrack);
    %       sig3_tilt(iSext,imove) = data.sigma(2,3);
    %       xWaist_tilt(iSext,imove)=data.xwaist;
    %       yWaist_tilt(iSext,imove)=data.ywaist;
    %       GIRDER{sgi(iSext)}.MoverPos=[0 0 0];
    %       sig16_tilt(iSext,imove) = data.xdisp; %sigma(1,6);
    %       sig26_tilt(iSext,imove) = data.xdp; %sigma(2,6);
    %       sig36_tilt(iSext,imove) = data.ydisp; %sigma(3,6);
    %       sig46_tilt(iSext,imove) = data.ydp; %sigma(4,6);
    %       T322_tilt(iSext,imove) = data.bvals(arrayfun(@(x) isequal(data.bterms(x,:),[0 2 0 0]),1:length(data.bterms)));
    %       T326_tilt(iSext,imove) = data.bvals(arrayfun(@(x) isequal(data.bterms(x,:),[0 1 0 1]),1:length(data.bterms)));
    %       T324_tilt(iSext,imove) = data.bvals(arrayfun(@(x) isequal(data.bterms(x,:),[1 2 0 0]),1:length(data.bterms)));
    %       T312_tilt(iSext,imove) = data.bvals(arrayfun(@(x) isequal(data.bterms(x,:),[1 1 0 0]),1:length(data.bterms)));
    %       T311_tilt(iSext,imove) = data.bvals(arrayfun(@(x) isequal(data.bterms(x,:),[2 0 0 0]),1:length(data.bterms)));
    %       U3112_tilt(iSext,imove) = data.bvals(arrayfun(@(x) isequal(data.bterms(x,:),[2 1 0 0]),1:length(data.bterms)));
    %       T324_tilt(iSext,imove) = data.bvals(arrayfun(@(x) isequal(data.bterms(x,:),[0 1 1 0]),1:length(data.bterms)));
    %       T322_tilt(iSext,imove) = data.bvals(arrayfun(@(x) isequal(data.bterms(x,:),[0 2 0 0]),1:length(data.bterms)));
    %       U3111_tilt(iSext,imove) = data.bvals(arrayfun(@(x) isequal(data.bterms(x,:),[3 0 0 0]),1:length(data.bterms)));
    %       U3222_tilt(iSext,imove) = data.bvals(arrayfun(@(x) isequal(data.bterms(x,:),[0 3 0 0]),1:length(data.bterms)));
    %       U3224_tilt(iSext,imove) = data.bvals(arrayfun(@(x) isequal(data.bterms(x,:),[0 2 1 0]),1:length(data.bterms)));
    %       S31_tilt(iSext,imove) = data.bvals(arrayfun(@(x) isequal(data.bterms(x,:),[1 0 0 0]),1:length(data.bterms)));
    %       corvals_tilt{iSext,imove} = data.bvals;
    %       cortsize_tilt{iSext,imove} = data.btsize;
    %       betay_tilt(iSext,imove) = data.betay;
    %     end
    %     fprintf('Scanning Sext %d (dK)\n',iSext)
    %     PSinit=PS(ips(iSext)).Ampl;
    %     for imove=1:length(Smoves)
    %       PS(ips(iSext)).Ampl=PSinit*(1+Smoves(imove)*2000);
    %       % Get IP params
    %       data = GetIPData(Beam1,strack,etrack);
    %       sig3_dk(iSext,imove) = data.sigma(2,3);
    %       xWaist_dk(iSext,imove)=data.xwaist;
    %       yWaist_dk(iSext,imove)=data.ywaist;
    %       GIRDER{sgi(iSext)}.MoverPos=[0 0 0];
    %       sig16_dk(iSext,imove) = data.xdisp; %sigma(1,6);
    %       sig26_dk(iSext,imove) = data.xdp; %sigma(2,6);
    %       sig36_dk(iSext,imove) = data.ydisp; %sigma(3,6);
    %       sig46_dk(iSext,imove) = data.ydp; %sigma(4,6);
    %       T322_dk(iSext,imove) = data.bvals(arrayfun(@(x) isequal(data.bterms(x,:),[0 2 0 0 0 0]),1:length(data.bterms)));
    %       T326_dk(iSext,imove) = data.bvals(arrayfun(@(x) isequal(data.bterms(x,:),[0 1 0 0 0 1]),1:length(data.bterms)));
    %       T324_dk(iSext,imove) = data.bvals(arrayfun(@(x) isequal(data.bterms(x,:),[1 2 0 0 0 0]),1:length(data.bterms)));
    %       T312_dk(iSext,imove) = data.bvals(arrayfun(@(x) isequal(data.bterms(x,:),[1 1 0 0 0 0]),1:length(data.bterms)));
    %       T311_dk(iSext,imove) = data.bvals(arrayfun(@(x) isequal(data.bterms(x,:),[2 0 0 0 0 0]),1:length(data.bterms)));
    %       U3112_dk(iSext,imove) = data.bvals(arrayfun(@(x) isequal(data.bterms(x,:),[2 1 0 0 0 0]),1:length(data.bterms)));
    %       T324_dk(iSext,imove) = data.bvals(arrayfun(@(x) isequal(data.bterms(x,:),[0 1 0 1 0 0]),1:length(data.bterms)));
    %       T322_dk(iSext,imove) = data.bvals(arrayfun(@(x) isequal(data.bterms(x,:),[0 2 0 0 0 0]),1:length(data.bterms)));
    %       U3111_dk(iSext,imove) = data.bvals(arrayfun(@(x) isequal(data.bterms(x,:),[3 0 0 0 0 0]),1:length(data.bterms)));
    %       U3222_dk(iSext,imove) = data.bvals(arrayfun(@(x) isequal(data.bterms(x,:),[0 3 0 0 0 0]),1:length(data.bterms)));
    %       U3224_dk(iSext,imove) = data.bvals(arrayfun(@(x) isequal(data.bterms(x,:),[0 2 0 1 0 0]),1:length(data.bterms)));
    %       S31_dk(iSext,imove) = data.bvals(arrayfun(@(x) isequal(data.bterms(x,:),[1 0 0 0 0 0]),1:length(data.bterms)));
    %       corvals_dk{iSext,imove} = data.bvals;
    %       cortsize_dk{iSext,imove} = data.btsize;
    %       betay_dk(iSext,imove) = data.betay;
    %     end
    %     PS(ips(iSext)).Ampl=PSinit;
  end
  
  %   Linear fits
  emitx = FL.SimModel.Initial.x.NEmit/(1.3/0.511e-3);
  emity = FL.SimModel.Initial.y.NEmit/(1.3/0.511e-3);
  sigx = sqrt(emitx * FL.SimModel.Twiss.betax(etrack));
  sigy = sqrt(emity * FL.SimModel.Twiss.betay(etrack));
  esigx =  sqrt(sigx^2 / FL.SimModel.Initial.SigPUncorrel^2);
  esigy =  sqrt(sigy^2 / FL.SimModel.Initial.SigPUncorrel^2);
  epsigx=sigx/FL.SimModel.Twiss.betax(etrack);
  epsigy=sigy/FL.SimModel.Twiss.betay(etrack);
  for iSext=1:length(sgi)
    q=noplot_polyfit((Smoves./maxSEXT),xWaist_x(iSext,:)./FL.SimModel.Twiss.betax(etrack),1,1); T(1,(iSext-1)*3+1)=q(2);
    q=noplot_polyfit((Smoves./maxSEXT),xWaist_y(iSext,:)./FL.SimModel.Twiss.betax(etrack),1,1); T(1,(iSext-1)*3+2)=q(2);
    q=noplot_polyfit((Smoves.*10./maxSEXT),xWaist_tilt(iSext,:)./FL.SimModel.Twiss.betax(etrack),1,1); T(1,(iSext-1)*3+3)=q(2);
    q=noplot_polyfit((Smoves./maxSEXT),yWaist_x(iSext,:)./(1*FL.SimModel.Twiss.betay(etrack)),1,1); T(2,(iSext-1)*3+1)=q(2);
    q=noplot_polyfit((Smoves./maxSEXT),yWaist_y(iSext,:)./(1*FL.SimModel.Twiss.betay(etrack)),1,1); T(2,(iSext-1)*3+2)=q(2);
    q=noplot_polyfit((Smoves.*10./maxSEXT),yWaist_tilt(iSext,:)./(10*FL.SimModel.Twiss.betay(etrack)),1,1); T(2,(iSext-1)*3+3)=q(2);
    q=noplot_polyfit((Smoves./maxSEXT),sig16_x(iSext,:)./esigx,1,1); T(3,(iSext-1)*3+1)=q(2);
    q=noplot_polyfit((Smoves./maxSEXT),sig16_y(iSext,:)./esigx,1,1); T(3,(iSext-1)*3+2)=q(2);
    q=noplot_polyfit((Smoves.*10./maxSEXT),sig16_tilt(iSext,:)./esigx,1,1); T(3,(iSext-1)*3+3)=q(2);
    q=noplot_polyfit((Smoves./maxSEXT),sig36_x(iSext,:)./esigy,1,1); T(4,(iSext-1)*3+1)=q(2);
    q=noplot_polyfit((Smoves./maxSEXT),sig36_y(iSext,:)./esigy,1,1); T(4,(iSext-1)*3+2)=q(2);
    q=noplot_polyfit((Smoves.*10./maxSEXT),sig36_tilt(iSext,:)./esigy,1,1); T(4,(iSext-1)*3+3)=q(2);
    q=noplot_polyfit((Smoves./maxSEXT),sig3_x(iSext,:)./(1*emity),1,1); T(5,(iSext-1)*3+1)=q(2);
    q=noplot_polyfit((Smoves./maxSEXT),sig3_y(iSext,:)./(1*emity),1,1); T(5,(iSext-1)*3+2)=q(2);
    q=noplot_polyfit((Smoves.*10./maxSEXT),sig3_tilt(iSext,:)./(1*emity),1,1); T(5,(iSext-1)*3+3)=q(2);
    q=noplot_polyfit((Smoves./maxSEXT),T322_x(iSext,:)./0.1,1,1); T(6,(iSext-1)*3+1)=q(2);
    q=noplot_polyfit((Smoves./maxSEXT),T322_y(iSext,:)./0.1,1,1); T(6,(iSext-1)*3+2)=q(2);
    q=noplot_polyfit((Smoves.*10./maxSEXT),T322_tilt(iSext,:)./0.1,1,1); T(6,(iSext-1)*3+3)=q(2);
    q=noplot_polyfit((Smoves./maxSEXT),T326_x(iSext,:)./0.1,1,1); T(7,(iSext-1)*3+1)=q(2);
    q=noplot_polyfit((Smoves./maxSEXT),T326_y(iSext,:)./0.1,1,1); T(7,(iSext-1)*3+2)=q(2);
    q=noplot_polyfit((Smoves.*10./maxSEXT),T326_tilt(iSext,:)./0.1,1,1); T(7,(iSext-1)*3+3)=q(2);
    q=noplot_polyfit((Smoves./maxSEXT),T324_x(iSext,:)./0.1,1,1); T(8,(iSext-1)*3+1)=q(2);
    q=noplot_polyfit((Smoves./maxSEXT),T324_y(iSext,:)./0.1,1,1); T(8,(iSext-1)*3+2)=q(2);
    q=noplot_polyfit((Smoves.*10./maxSEXT),T324_tilt(iSext,:)./0.1,1,1); T(8,(iSext-1)*3+3)=q(2);
  end % for iSext
  
  % Make knobs
  % inds=[1 2 4 5 7 8 10 11 13 14];
  inds=1:length(sgi)*3; tinds=1:8;
%   Knobs.WaistX.smoves = lscov(T(1:7,inds),[1 0 0 0 0 0 0]').*maxSEXT;
  Knobs.WaistX.smoves = (pinv(T(1:7,inds))*[1 0 0 0 0 0 0]').*maxSEXT;
  Knobs.WaistX.scale = FL.SimModel.Twiss.betax(etrack);
  Knobs.WaistX.units = 'mm'; Knobs.WaistX.uScale = 1e-3;
%   Knobs.WaistY.smoves = lscov(T(1:5,inds),[0 1 0 0 0 0 0]').*maxSEXT;
  Knobs.WaistY.smoves = (pinv(T(1:7,inds))*[0 1 0 0 0 0 0]').*maxSEXT;
  Knobs.WaistY.scale = 1*FL.SimModel.Twiss.betay(etrack);
  Knobs.WaistY.units = 'mm'; Knobs.WaistY.uScale = 1e-3;
%   Knobs.DispX.smoves = lscov(T(1:7,inds),[0 0 1 0 0 0 0]').*maxSEXT;
  Knobs.DispX.smoves = (pinv(T(1:7,inds))*[0 0 1 0 0 0 0]').*maxSEXT;
  Knobs.DispX.scale = esigx;
  Knobs.DispX.units = 'mm'; Knobs.DispX.uScale = 1e-3;
%   Knobs.DispY.smoves = lscov(T([1:5 7],inds),[0 0 0 1 0 0]').*maxSEXT;
  Knobs.DispY.smoves = (pinv(T(1:7,inds))*[0 0 0 1 0 0 0]').*maxSEXT;
  Knobs.DispY.scale = esigy;
  Knobs.DispY.units = 'mm'; Knobs.DispY.uScale = 1e-3;
%   Knobs.XpY.smoves = lscov(T(1:7,inds),[0 0 0 0 1 0 0]').*maxSEXT;
  Knobs.XpY.smoves = (pinv(T(1:7,inds))*[0 0 0 0 1 0 0]').*maxSEXT;
  Knobs.XpY.scale = 1*emity;
  Knobs.XpY.units = 'raw'; Knobs.XpY.uScale = 1;
%   Knobs.T322.smoves = lscov(T([1 2 4 5 6 7],inds),[0 0 0 0 1 0]').*maxSEXT;
  Knobs.T322.smoves = (pinv(T(1:7,inds))*[0 0 0 0 0 1 0]').*maxSEXT;
  Knobs.T322.scale = 0.01;
  Knobs.T322.units = 'units'; Knobs.T322.uScale = 1;
%   Knobs.T326.smoves = lscov(T([1 2 4 5 6 7],inds),[0 0 0 0 0 1]').*maxSEXT;
  Knobs.T326.smoves = (pinv(T(1:7,inds))*[0 0 0 0 0 0 1]').*maxSEXT;
  Knobs.T326.scale = 0.01;
  Knobs.T326.units = 'units'; Knobs.T326.uScale = 1;
%   Knobs.T324.smoves = lscov(T([1 2 3 4 5 6 7 8],inds),[0 0 0 0 0 0 0 1]').*maxSEXT;
  Knobs.T324.smoves = (pinv(T(1:8,inds))*[0 0 0 0 0 0 0 1]').*maxSEXT;
  Knobs.T324.scale = 0.01;
  Knobs.T324.units = 'units'; Knobs.T324.uScale = 1;
  
  
  % Skew quad
  if doskewquad
    moves=-0.03:0.005:0.03;
    ind=0;
    for iQuad=FL.SimModel.magGroup.SQuad_id(3:end)
      fprintf('Generating knobs for squad: %d\n',iQuad)
      ind=ind+1;
      ind2=0;
      for iDB=moves
        ind2=ind2+1;
        PS(FL.SimModel.PSind.Quad(iQuad)).Ampl=iDB;
        data = GetIPData(Beam1,FL.Region.EXT.ind(1),etrack);
        sig1(ind,ind2) = data.sigma(1,3);
        sig2(ind,ind2) = data.sigma(1,4);
        sig3(ind,ind2) = data.sigma(2,3);
        sig4(ind,ind2) = data.sigma(2,4);
      end
      q=noplot_polyfit(moves,sig1(ind,:),1,1); Tsq(1,ind)=q(2);
      q=noplot_polyfit(moves,sig2(ind,:),1,1); Tsq(2,ind)=q(2);
      q=noplot_polyfit(moves,sig3(ind,:),1,1); Tsq(3,ind)=q(2);
      q=noplot_polyfit(moves,sig4(ind,:),1,1); Tsq(4,ind)=q(2);
      PS(FL.SimModel.PSind.Quad(iQuad)).Ampl=0;
    end
    Knobs.sq.sig13=Tsq\[1 0 0 0]';
    Knobs.sq.sig14=Tsq\[0 1 0 0]';
    Knobs.sq.sig23=Tsq\[0 0 1 0]';
    Knobs.sq.sig24=Tsq\[0 0 0 1]';
  end
  save userData/GenerateSextIPKnobs_save
  Knobs_out=Knobs;
end
%%
% test
% load userData/KEKknobs
if docheck || dosumknob
  [orthret maxknobs]=runDoCheck(Knobs,sgi,Beam1,strack,etrack);
end

% --- Dispersion knobs
% Scan sum knob and look at IP effect
skdata=[];
if dosumknob
  if isempty(qs1ps)
    qs1=findcells(BEAMLINE,'Name','QS1X'); qs1ps=BEAMLINE{qs1(1)}.PS;
    qs2=findcells(BEAMLINE,'Name','QS2X'); qs2ps=BEAMLINE{qs2(1)}.PS;
  end
  psmax=0.0557;
  ps_scan=linspace(-psmax,psmax,41);
  qs1orig=PS(qs1ps).Ampl; qs2orig=PS(qs2ps).Ampl;
  for ips=1:length(ps_scan)
    PS(qs1ps).Ampl=ps_scan(ips);
    PS(qs2ps).Ampl=ps_scan(ips);
    data = GetIPData(Beam1,FL.Region.EXT.ind(1),etrack);
    skdata.ydisp(ips)=data.ydisp;
    skdata.ydp(ips)=data.ydp;
    skdata.sknob(ips)=ps_scan(ips);
    skdata.ysize(ips)=data.ysize;
    skdata.sig23(ips)=data.sigma(2,3);
    skdata.sig13(ips)=data.sigma(1,3);
    skdata.wy(ips)=data.ywaist;
    skdata.emit_x(ips)=data.emit_x;
    skdata.emit_y(ips)=data.emit_y;
    skdata.extemit_y(ips)=data.extemit_y;
  end
  PS(qs1ps).Ampl=qs1orig;
  PS(qs2ps).Ampl=qs2orig;
  if isfield(FL.Gui,'GenerateSextIPKnobs_plot3') && ishandle(FL.Gui.GenerateSextIPKnobs_plot3)
    figure(FL.Gui.GenerateSextIPKnobs_plot3)
  else
    FL.Gui.GenerateSextIPKnobs_plot3=figure;
  end
  plotyy(skdata.sknob,skdata.sigma36,skdata.sknob,skdata.sigma46)
  if isfield(FL.Gui,'GenerateSextIPKnobs_plot4') && ishandle(FL.Gui.GenerateSextIPKnobs_plot4)
    figure(FL.Gui.GenerateSextIPKnobs_plot4)
  else
    FL.Gui.GenerateSextIPKnobs_plot4=figure;
  end
  plot(skdata.sknob,skdata.ysize)
  if isfield(FL.Gui,'GenerateSextIPKnobs_plot5') && ishandle(FL.Gui.GenerateSextIPKnobs_plot5)
    figure(FL.Gui.GenerateSextIPKnobs_plot5)
  else
    FL.Gui.GenerateSextIPKnobs_plot5=figure;
  end
  plot(skdata.sknob,skdata.sig23./maxknobs(2),skdata.sknob,skdata.sigma36./maxknobs(1),skdata.sknob,skdata.sigma46./maxknobs(4),...
    skdata.sknob,skdata.wy./maxknobs(3))
  legend('sig23','DispY','DispYP','WaistY')
  % find min beam size from sum knob scan and set
  [P,~,MU] = POLYFIT(skdata.sknob,skdata.ysize,5);
  xinterp=linspace(skdata.sknob(1),skdata.sknob(end),10000);
  Y = POLYVAL(P,xinterp,[],MU);
  [~, Imin]=min(Y); minknob=xinterp(Imin); fprintf('Min Sum Knob: %g\n',minknob)
  PS(qs1ps).Ampl=minknob;
  PS(qs2ps).Ampl=minknob;
end

% Put any offsets back
if ~exist('notrackcond','var') || ~notrackcond
  for iele=1:length(offEle)
    BEAMLINE{offEle(iele)}.Offset=initOffset{iele};
  end
end

function [orthret maxknobs]=runDoCheck(Knobs,sgi,Beam1,strack,etrack)
global GIRDER FL BEAMLINE PS
%==-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-0
% Okugi Knobs
% load ../../FlightSim/latticeFiles/src/v4.5/ATF2lat_BX10BY1 BEAMLINE GIRDER PS Beam1
% zeroMult;
% Knobs.WaistY.smoves=[507 0 0 47 0 0 -543 0 0 -282 0 0 -593 0 0 0 0 0]';
% Knobs.DispY.smoves=[0 76 0 0 -67 0 0 -367 0 0 6 0 0 375 0 0 0 0]';
% Knobs.XpY.smoves=[0 123 0 0 0 0 0 -505 0 0 230 0 0 -242 0 0 0 0]';
% names={'QM16FF' 'QM15FF' 'QM14FF' 'QM13FF' 'QM12FF' 'QM11FF' 'QF7FF' 'SF5FF' 'QD2BFF' 'QD2AFF' 'SF1FF'};
% diffPS=[0.728 2.12 0.782 0.881 4.63 0 1.86 40.4 0.807 1.26 1.22];
% for ibl=1:length(names)
%   ele=findcells(BEAMLINE,'Name',names{ibl});
%   PS(BEAMLINE{ele(1)}.PS).Ampl=diffPS(ibl);
% end
% qf1=findcells(BEAMLINE,'Name','QF1FF'); qf1=BEAMLINE{qf1(1)}.PS;
% qd0=findcells(BEAMLINE,'Name','QD0FF'); qd0=BEAMLINE{qd0(1)}.PS;
% PS(qf1).Ampl=1.0047; PS(qd0).Ampl=1.0041;
% [~, Beam1]=TrackThru(1,strack-1,Beam1,1,1,0);
%==-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-0
nmag=length(sgi);
ik1{1}=reshape(Knobs.DispY.smoves,3,nmag)./max(max(abs(Knobs.DispY.smoves)));
ik1{2}=reshape(Knobs.XpY.smoves,3,nmag)./max(max(abs(Knobs.XpY.smoves)));
ik1{3}=reshape(Knobs.WaistY.smoves,3,nmag)./max(max(abs(Knobs.WaistY.smoves)));
ik1{4}=reshape(Knobs.T322.smoves,3,nmag)./max(max(abs(Knobs.T322.smoves)));
ik1{5}=reshape(Knobs.T326.smoves,3,nmag)./max(max(abs(Knobs.T326.smoves)));
ik1{6}=reshape(Knobs.T324.smoves,3,nmag)./max(max(abs(Knobs.T324.smoves)));
scales=[Knobs.DispY.scale Knobs.XpY.scale Knobs.WaistY.scale Knobs.T322.scale Knobs.T326.scale Knobs.T324.scale];
uScales=[Knobs.DispY.uScale Knobs.XpY.uScale Knobs.WaistY.uScale Knobs.T322.uScale Knobs.T326.uScale Knobs.T324.uScale];
knames={'Dispersion' 'Coupling' 'Waist' 'T322' 'T326' 'T324'};
dofmax=[2.25 1.6 5]'.*1e-3;
maxrange=zeros(size(ik1));
data0 = GetIPData(Beam1,strack,etrack);
for iknob=1:length(ik1)
  maxrange(iknob)=min(dofmax./max(abs(ik1{iknob}),[],2));
end
for isext=1:nmag
  ginit{isext}=GIRDER{sgi(isext)}.MoverPos;
end
for iknob=1:length(ik1)
  fprintf('Testing Knob: %s\n',knames{iknob})
  nk=0; npoints=10;
  for ik=linspace(0,maxrange(iknob),npoints)
    for isext=1:nmag
      GIRDER{sgi(isext)}.MoverPos=ginit{isext}+[ik1{iknob}(1,isext) ik1{iknob}(2,isext) ik1{iknob}(3,isext)].*ik;
    end
    nk=nk+1;
    data = GetIPData(Beam1,strack,etrack);
    ysize(iknob,nk)=data.ysize;
    sig3(iknob,nk) = data.sigma(2,3)-data0.sigma(2,3);
    xwaist(iknob,nk) = data.xwaist-data0.xwaist;
    ywaist(iknob,nk) = data.ywaist-data0.ywaist;
    dispx(iknob,nk) = data.xdisp-data0.xdisp; %data.sigma(1,6);
    dispy(iknob,nk) = data.ydisp-data0.ydisp; %data.sigma(3,6);
    dispxp(iknob,nk) = data.xdp-data0.xdp; %data.sigma(2,6);
    dispyp(iknob,nk) = data.ydp-data0.ydp; %data.ydp(4,6);
    knobVal(iknob,nk) = ik; %/scales(iknob)/uScales(iknob);
    btsize{iknob,nk} = data.btsize;
    bterms{iknob,nk} = data.bterms;
    bvals{iknob,nk} = data.bvals; %#ok<*NASGU>
    T322(iknob,nk) = data.bvals(arrayfun(@(x) isequal(data.bterms(x,:),[0 2 0 0 0 0]),1:length(data.bterms)))-...
      data0.bvals(arrayfun(@(x) isequal(data.bterms(x,:),[0 2 0 0 0 0]),1:length(data.bterms)));
    T326(iknob,nk) = data.bvals(arrayfun(@(x) isequal(data.bterms(x,:),[0 1 0 0 0 1]),1:length(data.bterms)))-...
      data0.bvals(arrayfun(@(x) isequal(data.bterms(x,:),[0 1 0 0 0 1]),1:length(data.bterms)));
    T324(iknob,nk) = data.bvals(arrayfun(@(x) isequal(data.bterms(x,:),[0 1 0 1 0 0]),1:length(data.bterms)))-...
      data0.bvals(arrayfun(@(x) isequal(data.bterms(x,:),[0 1 0 1 0 0]),1:length(data.bterms)));
    T322_bs(iknob,nk) = data.btsize(arrayfun(@(x) isequal(data.bterms(x,:),[0 2 0 0 0 0]),1:length(data.bterms)))-...
      data0.btsize(arrayfun(@(x) isequal(data.bterms(x,:),[0 2 0 0 0 0]),1:length(data.bterms)));
    T326_bs(iknob,nk) = data.btsize(arrayfun(@(x) isequal(data.bterms(x,:),[0 1 0 0 0 1]),1:length(data.bterms)))-...
      data0.btsize(arrayfun(@(x) isequal(data.bterms(x,:),[0 1 0 0 0 1]),1:length(data.bterms)));
    T324_bs(iknob,nk) = data.btsize(arrayfun(@(x) isequal(data.bterms(x,:),[0 1 0 1 0 0]),1:length(data.bterms)))-...
      data0.btsize(arrayfun(@(x) isequal(data.bterms(x,:),[0 1 0 1 0 0]),1:length(data.bterms)));
  end
end
for isext=1:nmag
  GIRDER{sgi(isext)}.MoverPos=ginit{isext};
end
if isfield(FL.Gui,'GenerateSextIPKnobs_plot1') && ishandle(FL.Gui.GenerateSextIPKnobs_plot1)
  figure(FL.Gui.GenerateSextIPKnobs_plot1)
else
  FL.Gui.GenerateSextIPKnobs_plot1=figure;
end
semilogy(linspace(0,1,length(knobVal(1,:))),ysize'./1e-6), axis tight, title('Sextupole Knobs Beamsize effect / um')
if isfield(FL.Gui,'GenerateSextIPKnobs_plot2') && ishandle(FL.Gui.GenerateSextIPKnobs_plot2)
  figure(FL.Gui.GenerateSextIPKnobs_plot2)
else
  FL.Gui.GenerateSextIPKnobs_plot2=figure;
end
maxknobs=[max(max(abs(dispy))) max(max(abs(sig3))) max(max(abs(ywaist))) max(max(abs(T322)))];
subplot(4,2,1), plot(linspace(0,1,length(knobVal(1,:))),dispx'./uScales(1)), title('dispx / mm'), axis tight
subplot(4,2,2), plot(linspace(0,1,length(knobVal(1,:))),dispy'./uScales(1)), title('dispy / mm'), axis tight
subplot(4,2,4), plot(linspace(0,1,length(knobVal(1,:))),sig3'/scales(2)), title('sig23 / emit_y'), axis tight
subplot(4,2,5), plot(linspace(0,1,length(knobVal(1,:))),xwaist'./uScales(3)), title('xwaist / mm'), axis tight
subplot(4,2,6), plot(linspace(0,1,length(knobVal(1,:))),ywaist'./uScales(3)), title('ywaist / mm'), axis tight
subplot(4,2,7), plot(linspace(0,1,length(knobVal(1,:))),T326'./uScales(5)), title('T326'), axis tight
subplot(4,2,8), plot(linspace(0,1,length(knobVal(1,:))),T322'./uScales(4)), title('T322'), axis tight
subplot(4,2,3), plot(linspace(0,1,length(knobVal(1,:))),T324'./uScales(6)), title('T324'), axis tight
figure
subplot(3,1,1), plot(linspace(0,1,length(knobVal(1,:))),T326_bs'), title('T326'), axis tight
subplot(3,1,2), plot(linspace(0,1,length(knobVal(1,:))),T322_bs'), title('T322'), axis tight
subplot(3,1,3), plot(linspace(0,1,length(knobVal(1,:))),T324_bs'), title('T324'), axis tight
dy1=max(abs(dispy(1,:)-dispy(1,4)));
dy2=max(max(abs(dispy(2:6,:)-mean(dispy(2:6,4)))));
dyorth=(1-abs(dy1-dy2)/abs(dy1))*100;
wy1=max(abs(ywaist(3,:)-ywaist(3,4)));
wy2=max(max(abs(ywaist([1 2 4 5 6],:)-mean(ywaist([1 2 4 5 6],4)))));
wyorth=(1-abs(wy1-wy2)/abs(wy1))*100;
sy1=max(abs(sig3(2,:)-sig3(2,4)));
sy2=max(max(abs(sig3([1 3 4 5 6],:)-mean(sig3([1 3 4 5 6],4)))));
syorth=(1-abs(sy1-sy2)/abs(sy1))*100;
dpy1=max(abs(T322(4,:)-T322(4,4)));
dpy2=max(max(abs(T322([1:3 5 6],:)-mean(T322([1:3 5 6],4)))));
dpyorth=(1-abs(dpy1-dpy2)/abs(dpy1))*100;
dt1=max(abs(T326(5,:)-T326(5,4)));
dt2=max(max(abs(T326([1:4 6],:)-mean(T326([1:4 6],4)))));
dtorth=(1-abs(dt1-dt2)/abs(dt1))*100;
dt1=max(abs(T324(6,:)-T324(6,4)));
dt2=max(max(abs(T324(1:5,:)-mean(T324(1:5,4)))));
duorth=(1-abs(dt1-dt2)/abs(dt1))*100;
fprintf('Knob non-orthogonality: DispY: %g XpY: %g WaistY: %g T322: %g T326: %g T324: %g (%%)\n',dyorth,syorth,wyorth,dpyorth,dtorth,duorth)
orthret=[dyorth syorth wyorth dpyorth dtorth duorth];
if isfield(Knobs,'sq')
  qk=findcells(BEAMLINE,'Name','QK*X'); qkps=[];
  for iqk=1:2:length(qk)
    qkps(end+1)=BEAMLINE{qk(iqk)}.PS;
  end
  Knobs.sq.sig13=Knobs.sq.sig13./max(abs(Knobs.sq.sig13));
  Knobs.sq.sig23=Knobs.sq.sig23./max(abs(Knobs.sq.sig23));
  Knobs.sq.sig14=Knobs.sq.sig14./max(abs(Knobs.sq.sig14));
  Knobs.sq.sig24=Knobs.sq.sig24./max(abs(Knobs.sq.sig24));
  ks=[-0.2 -0.1 -0.05 0 0.05 0.1 0.2];
  kname={'sig13' 'sig14' 'sig23' 'sig24'}; sig3=[];
  sig3=[];
  for iks=1:length(ks)
    for iknob=1:4
      qkval=Knobs.sq.(kname{iknob}).*ks(iks);
      for iqk=1:4
        PS(qkps(iqk)).Ampl=qkval(iqk);
      end
      data = GetIPData(Beam1,FL.Region.EXT.ind(1),etrack);
      sig1(iknob,iks)=data.sigma(1,3);
      sig2(iknob,iks)=data.sigma(1,4);
      sig3(iknob,iks)=data.sigma(2,3);
      sig4(iknob,iks)=data.sigma(2,4);
      ysize_ks(iknob,iks)=data.ysize;
    end
  end
  if isfield(FL.Gui,'GenerateSextIPKnobs_plot3') && ishandle(FL.Gui.GenerateSextIPKnobs_plot3)
    figure(FL.Gui.GenerateSextIPKnobs_plot3)
  else
    FL.Gui.GenerateSextIPKnobs_plot3=figure;
  end
  subplot(2,2,1), plot(ks,sig1'), title('<xy>')
  subplot(2,2,2), plot(ks,sig2'), title('<xy''>')
  subplot(2,2,3), plot(ks,sig3(1:4,1:7)'), title('<x''y>')
  subplot(2,2,4), plot(ks,sig4'), title('<x''y''>')
  if isfield(FL.Gui,'GenerateSextIPKnobs_plot4') && ishandle(FL.Gui.GenerateSextIPKnobs_plot4)
    figure(FL.Gui.GenerateSextIPKnobs_plot4)
  else
    FL.Gui.GenerateSextIPKnobs_plot4=figure;
  end
  plot(ks,ysize_ks'), axis tight, title('Skew Quad Knob Beamsize Effect')
  d1=max(abs(sig1(1,:)-sig1(1,4)));
  d2=max(max(abs(sig1(2:4,:)-mean(sig1(2:4,4)))));
  orth1=(1-abs(d1-d2)/abs(d1))*100;
  d1=max(abs(sig2(2,:)-sig1(2,4)));
  d2=max(max(abs(sig2([1 3 4],:)-mean(sig2([1 3 4],4)))));
  orth2=(1-abs(d1-d2)/abs(d1))*100;
  d1=max(abs(sig3(3,:)-sig3(3,4)));
  d2=max(max(abs(sig3([1 2 4],:)-mean(sig3([1 2 4],4)))));
  orth3=(1-abs(d1-d2)/abs(d1))*100;
  d1=max(abs(sig4(4,:)-sig4(4,4)));
  d2=max(max(abs(sig4(1:3,:)-mean(sig4(1:3,4)))));
  orth4=(1-abs(d1-d2)/abs(d1))*100;
  fprintf('SQ Knob non-orthogonality: sig13: %g sig14: %g sig23: %g sig24: %g(%%)\n',orth1,orth2,orth3,orth4)
  orthret=[orthret orth1 orth2 orth3 orth4];
end
save userData/sextKnobTest
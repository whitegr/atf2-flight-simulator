function [SIG,Twiss,BMAG,Initial,Beam] = norm2sig4d(emit_u,emit_v,alpha_u,alpha_v,beta_u,beta_v,R1,R2,R3,R4,Initial,Beam)
% Generate 4-d sigma matrix from normal-mode twiss quantities
% Optionally generate macro particles in physical reference frame if
% Initial (standard lucretia Initial structure) and Beam (macro-particle
% Lucretia Beam format) supplied

BMAG.x=0; BMAG.y=0;

u=emit_u*beta_u;
up=emit_u*(1+alpha_u^2)/beta_u;
uup=-emit_u*alpha_u;
v=emit_v*beta_v;
vp=emit_v*(1+alpha_v^2)/beta_v;
vvp=-emit_v*alpha_v;
r=[R1 R2;
   R3 R4];
mu=sqrt(1-det(r));

sx =  R2^2*vp + R4^2*v - 2*R2*R4*vvp + mu^2*u ;
sxp = R1^2*vp + R3^2*v - 2*R1*R3*vvp + mu^2*up ;
sy =  R1^2*u + R2^2*up + mu^2*v + 2*R1*R2*uup ;
syp = R3^2*u + R4^2*up + 2*R3*R4*uup + mu^2*vp ;
sxxp = mu^2*uup - R3*R4*v - R1*R2*vp + (R1*R4+R2*R3)*vvp ;
syyp = R1*R3*u + R2*R4*up + mu^2*vvp + (R2*R3+R1*R4)*uup ;
sxy = -R1*mu*u - R2*mu*uup + R4*mu*v - R2*mu*vvp ;
sxyp = -R3*mu*u - R4*mu*uup + R4*mu*vvp - R2*mu*vp ;
sxpy = -R1*mu*uup - R2*mu*u - R3*mu*v + R1*mu*vvp ;
sxpyp = -R3*mu*uup - R4*mu*up - R3*mu*vvp + R1*mu*v ;

SIG=[sx   sxxp  sxy  sxyp ;
   sxxp sxp   sxpy sxpyp;
   sxy  sxpy  sy   syyp ;
   sxyp sxpyp syyp syp  ];

gamma=1.28/0.511e-3;
Twiss.emit_x=sqrt(sx*sxp-sxxp^2);
Twiss.beta_x=sx/Twiss.emit_x;
Twiss.alpha_x=sqrt(((sxp*Twiss.beta_x)/Twiss.emit_x)-1);
Twiss.emit_y=sqrt(sy*syp-syyp^2);
Twiss.beta_y=sy/Twiss.emit_y;
Twiss.alpha_y=sqrt(((syp*Twiss.beta_y)/Twiss.emit_y)-1);
tx=Initial.x.Twiss; ty=Initial.y.Twiss;
BMAG.x=0.5*( ((Twiss.beta_x/tx.beta)+(tx.beta/Twiss.beta_x)) + ...
             ((Twiss.alpha_x*sqrt(tx.alpha/Twiss.alpha_x))-(tx.alpha*sqrt(Twiss.beta_x/tx.beta)))^2);
BMAG.y=0.5*( ((Twiss.beta_y/ty.beta)+(ty.beta/Twiss.beta_y)) + ...
             ((Twiss.alpha_y*sqrt(ty.alpha/Twiss.alpha_y))-(ty.alpha*sqrt(Twiss.beta_y/ty.beta)))^2);
if exist('Initial','var') && exist('Beam','var')
  Initial.x.NEmit=gamma*emit_u;
  Initial.x.Twiss.beta=beta_u;
  Initial.x.Twiss.alpha=alpha_u;
  Initial.y.NEmit=gamma*emit_v;
  Initial.y.Twiss.beta=beta_v;
  Initial.y.Twiss.alpha=alpha_v;
  newBeam=MakeBeam6DGauss( Initial, length(Beam.Bunch.x), 5, 0 );
  T=[ mu   0  R4 -R2;
       0  mu -R3  R1;
     -R1 -R2  mu   0;
     -R3 -R4   0  mu];
  Beam.Bunch.x(1:4,:)=T*newBeam.Bunch.x(1:4,:);
  Beam.Bunch.x(5:6,:)=newBeam.Bunch.x(5:6,:);
else
  Initial=[]; Beam=[];
end % if Initial and Beam given
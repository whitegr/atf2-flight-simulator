function Model = myTrack(Beam,Model)
% Track a bunch through to the IP after adding 1 pulse of
% ground motion and applying input jitter
global FL INSTR BEAMLINE
persistent s isext ismoni isffs

% restore functionality
if isequal(Beam,'get_restore')
  Model={s isext ismoni isffs}; return;
elseif isequal(Beam,'set_restore')
  [s isext ismoni isffs]=deal(Model{:}); return;
end % if restore

if Model.doGM
  % Apply 1 pulse of Ground Motion
  if ~(isfield(Model,'nofeedback') && Model.nofeedback)
    Model.time=Model.time+(1/Model.repRate); gmMove(Model);
  end
  % Apply input jitter
  if Model.doJitter
    eError=Model.align.incoming.e*randn;
    Beam.Bunch.x(1,:)=Beam.Bunch.x(1,:)+Model.align.incoming.x.*randn;
    Beam.Bunch.x(2,:)=Beam.Bunch.x(2,:)+Model.align.incoming.xp.*randn;
    Beam.Bunch.x(3,:)=Beam.Bunch.x(3,:)+Model.align.incoming.y.*randn;
    Beam.Bunch.x(4,:)=Beam.Bunch.x(4,:)+Model.align.incoming.yp.*randn;
    Beam.Bunch.x(6,:)=Beam.Bunch.x(6,:)+(mean(Beam.Bunch.x(6,:))*eError);
  end % if Model.doJitter
end % if doGM
% Track pulse
FL.simBeamID=2; FL.SimModel=Model;
stat=FlHwUpdate; if stat{1}~=1; error(stat{2}); end;
Model=FL.SimModel;
Model.bpmData.x=cellfun(@(x) x.Data(1),INSTR); Model.bpmData.y=cellfun(@(x) x.Data(2),INSTR);
if isempty(s)
  Model.ext.endInd = findcells(BEAMLINE,'Name','BEGFF') ;
  s=cellfun(@(x) BEAMLINE{x.Index}.S,INSTR); ismoni=cellfun(@(x) isequal(x.Class,'MONI'),INSTR);
  isext=cellfun(@(x) x.Index>Model.tstart & x.Index<Model.ext.endInd,INSTR);
  isffs=cellfun(@(x) x.Index>Model.ext.endInd & x.Index<Model.ip_ind,INSTR);
end % if empty s
Model.bpmData.s=s; Model.bpmData.isext=isext; Model.bpmData.ismoni=ismoni; Model.bpmData.isffs=isffs;

% Apply 5-Hz feedback
if ~(isfield(Model,'nofeedback') && Model.nofeedback)
%   feedbackEXT(Model,'Correct');
%   feedbackFF1(Model,'CORRECT');
%   feedbackFF2(Model,'CORRECT');
  % fb_5Hz(Model,'CORRECT');
  [stat pars]=FlFB('GetPars');
  if ~pars.active
    newpars.active=true;
    FlFB('SetPars',newpars);
  end
  stat = FlFB('Run'); if stat{1}~=1; warning(stat{2}); end;
end % do feedback?
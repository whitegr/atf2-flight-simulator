%--- Reduce data set to one with interesting terms only
% sexts={'sext1' 'sext2' 'sext3' 'sext4' 'sext5'};
sexts={'sext1' 'sext2' 'sext3' 'sext4' 'sext5'};
for is=1:length(sexts)
  for ie=1:69
    ran=abs(tsize_x.(sexts{is})(:,ie).*sign(vals_x.(sexts{is})(:,ie)));
    xran(is,ie)=max(ran)-min(ran);
    ran=abs(tsize_y.(sexts{is})(:,ie).*sign(vals_y.(sexts{is})(:,ie)));
    yran(is,ie)=max(ran)-min(ran);
    ran=abs(tsize_tilt.(sexts{is})(:,ie).*sign(vals_tilt.(sexts{is})(:,ie)));
    tran(is,ie)=max(ran)-min(ran);
    ran=abs(tsize_dk.(sexts{is})(:,ie).*sign(vals_dk.(sexts{is})(:,ie)));
    dran(is,ie)=max(ran)-min(ran);
  end % for ie
end % for is
[ns1 nt1]=find(xran>4e-9); [ns2 nt2]=find(yran>4e-9); [ns3 nt3]=find(tran>4e-9); [ns4 nt4]=find(dran>4e-9); 
ikeep=unique([nt1' nt2' nt3' nt4']);
sextResponse.terms={terms{ikeep'}}; %#ok<USENS>
sextResponse.x.move=-6e-4:10e-6:6e-4;
sextResponse.y.move=-6e-4:10e-6:6e-4;
sextResponse.tilt.move=-5e-3:50e-5:5e-3;
sextResponse.dk.move=-0.04:0.002:0.04;
for is=1:length(sexts)
  for ie=1:length(ikeep)
    sextResponse.x.tsize{is,ie}=tsize_x.(sexts{is})(:,ikeep(ie)).*sign(vals_x.(sexts{is})(:,ikeep(ie)));
    sextResponse.y.tsize{is,ie}=tsize_y.(sexts{is})(:,ikeep(ie)).*sign(vals_y.(sexts{is})(:,ikeep(ie)));
    sextResponse.tilt.tsize{is,ie}=tsize_tilt.(sexts{is})(:,ikeep(ie)).*sign(vals_tilt.(sexts{is})(:,ikeep(ie)));
    sextResponse.dk.tsize{is,ie}=tsize_dk.(sexts{is})(:,ikeep(ie)).*sign(vals_dk.(sexts{is})(:,ikeep(ie)));
  end % for ie
end % for is
% Polynomial fits to data
is=1;ie=1;
xorder=ones(length(sexts),length(ikeep)).*3;yorder=xorder;torder=xorder;dorder=xorder;
xorder(1,3)=10; xorder(2,3)=10; xorder(3,7)=10; xorder(4,3)=10; xorder(4,13)=10;
yorder(1,6)=10; yorder(1,7)=10; yorder(1,12)=10; yorder(1,14)=4;
yorder(1,15)=10;yorder(2,4)=10;yorder(3,13)=10;yorder(3,14)=7;yorder(3,15)=5;
yorder(3,16)=10;yorder(4,1)=3;yorder(4,3)=10;yorder(4,5)=5;yorder(4,6)=4;
yorder(4,10)=10;yorder(5,1)=10;yorder(5,3)=4;yorder(5,13)=10;yorder(5,16)=10;yorder(5,18)=10;
torder(1,7)=5;torder(1,9)=10;torder(4,10)=7;
dorder(1,8)=10;dorder(3,9)=9;dorder(5,9)=10;
for is=1:length(sexts)
  for ie=1:length(ikeep)
    [P,S,MU]=polyfit(sextResponse.x.move,sextResponse.x.tsize{is,ie}',xorder(is,ie));
    a=sextResponse.x.move(1); b=sextResponse.x.move(end);
    sextResponse.x.tsize_fit{is,ie} = polyval(P,a:(b-a)/10000:b,[],MU);
    sextResponse.x.move_fit=a:(b-a)/10000:b;
    sextResponse.x.tsize_fit{is,ie}=sextResponse.x.tsize_fit{is,ie}-polyval(P,0,[],MU);
    sextResponse.x.tsize_grad{is,ie}=gradient(sextResponse.x.tsize_fit{is,ie},(sextResponse.x.move_fit(2)-sextResponse.x.move_fit(1))./...
      max(abs(sextResponse.x.move_fit)));
    [P,S,MU]=polyfit(sextResponse.y.move,sextResponse.y.tsize{is,ie}',yorder(is,ie));
    a=sextResponse.y.move(1); b=sextResponse.y.move(end);
    sextResponse.y.tsize_fit{is,ie} = polyval(P,a:(b-a)/10000:b,[],MU);
    sextResponse.y.move_fit=a:(b-a)/10000:b;
    sextResponse.y.tsize_fit{is,ie}=sextResponse.y.tsize_fit{is,ie}-polyval(P,0,[],MU);
    sextResponse.y.tsize_grad{is,ie}=gradient(sextResponse.y.tsize_fit{is,ie},(sextResponse.y.move_fit(2)-sextResponse.y.move_fit(1))./...
      max(abs(sextResponse.y.move_fit)));
    [P,S,MU]=polyfit(sextResponse.tilt.move,sextResponse.tilt.tsize{is,ie}',torder(is,ie));
    a=sextResponse.tilt.move(1); b=sextResponse.tilt.move(end);
    sextResponse.tilt.tsize_fit{is,ie} = polyval(P,a:(b-a)/10000:b,[],MU);
    sextResponse.tilt.move_fit=a:(b-a)/10000:b;
    sextResponse.tilt.tsize_fit{is,ie}=sextResponse.tilt.tsize_fit{is,ie}-polyval(P,0,[],MU);
    sextResponse.tilt.tsize_grad{is,ie}=gradient(sextResponse.tilt.tsize_fit{is,ie},(sextResponse.tilt.move_fit(2)-sextResponse.tilt.move_fit(1))./...
      max(abs(sextResponse.tilt.move_fit)));
    [P,S,MU]=polyfit(sextResponse.dk.move,sextResponse.dk.tsize{is,ie}',dorder(is,ie));
    a=sextResponse.dk.move(1); b=sextResponse.dk.move(end);
    sextResponse.dk.move_fit=a:(b-a)/10000:b;
    sextResponse.dk.tsize_fit{is,ie} = polyval(P,a:(b-a)/10000:b,[],MU);
    sextResponse.dk.tsize_fit{is,ie}=sextResponse.dk.tsize_fit{is,ie}-polyval(P,0,[],MU);
    sextResponse.dk.tsize_grad{is,ie}=gradient(sextResponse.dk.tsize_fit{is,ie},(sextResponse.dk.move_fit(2)-sextResponse.dk.move_fit(1))./...
      max(abs(sextResponse.dk.move_fit)));
  end % for ie
end % for % is
clear all global
%%
file='/afs/slac/u/ey/whitegr/atf2fs/Lucretia/src/Floodland/latticeFiles/src/v4.5/ATF2lat_BX10BY1_bcons';
mtol=linspace(0,0.1,100);
jm=findResource;
for ijob=1:100
  job(ijob)=createJob(jm);
  job(ijob).Tag='multMagnitudeTol';
  createTask(job(ijob),@multMagnitudeTol,1,{file,mtol(ijob)});
end
%%
for ijob=1:100
  submit(job(ijob))
end
%%

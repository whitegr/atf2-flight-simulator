function [sens bmult bmult_skew bmult_rank tol]=multTol(eval)

BEAMLINE=Composite();
PS=Composite();
GIRDER=Composite();
Model=Composite();
Beam1_IEX=Composite();
sens=Composite();
% load ~/Lucretia/src/Floodland/latticeFiles/src/v4.3/ATF2lat_BX1BY1s BEAMLINE PS GIRDER Model Beam1_IEX
load ~mdw/public_html/ATF2/FlightSimulator/ATF2lat_BX1BY1s2 BEAMLINE PS GIRDER Model Beam1_IEX
% load ~/Lucretia/src/Floodland/latticeFiles/src/v4.3/ATF2lat_BX1BY1k.mat
% load ~/Lucretia/src/Floodland/latticeFiles/src/v4.3/ATF2lat_BX2.5BY1k_opt.mat

%% 
% Switch off all MULTs to start with
% make sure PS's assigned to MULTS, make groupings etc
indMult=findcells(BEAMLINE,'Name','*MULT');
indMult=indMult(~ismember(indMult,findcells(BEAMLINE,'Name','KEX*MULT')));
bmult={}; bmult_skew={};
for imult=1:length(indMult)
  bmult{imult}=BEAMLINE{indMult(imult)}.B;
  tiltmult{imult}=BEAMLINE{indMult(imult)}.Tilt;
  bmult_skew{imult}=getSkewComponent(BEAMLINE{indMult(imult)});
  bmult_normal{imult}=getNormComponent(BEAMLINE{indMult(imult)});
  mname{imult}=regexprep(BEAMLINE{indMult(imult)}.Name,'MULT','');
  relErr=getBerr(mname{imult},eval);
  if ~isempty(relErr) && length(bmult{imult})>=4
    berr{imult}=relErr.*bmult{imult}(1:4);
    tilterr{imult}=[2e-4 2e-4 2e-4 2e-4];
  end
  if BEAMLINE{indMult(imult)}.Name(1)=='B' || BEAMLINE{indMult(imult)}.Name(1)=='K'
    mname{imult}=[mname{imult} 'A'];
  end
  imag=findcells(BEAMLINE,'Name',mname{imult});
  bmult_rank(imult)=bmult_skew{imult}(1)*(1/BEAMLINE{imag(1)}.B(1));
%   BEAMLINE{indMult(imult)}.B=BEAMLINE{indMult(imult)}.B.*0;
  for igrp=BEAMLINE{indMult(imult)}.Block(1):BEAMLINE{indMult(imult)}.Block(end)
    if isfield(BEAMLINE{igrp},'PS') && BEAMLINE{igrp}.PS
      BEAMLINE{indMult(imult)}.PS=BEAMLINE{igrp}.PS;
      break
    end
  end
  if sum(ismember(mname,mname{imult}))==1
    if length(BEAMLINE{indMult(imult)}.PoleIndex)>3
      fprintf('%s (norm): (%d %d %d %d) %.2f %.2f %.2f %.2f\n',mname{imult},BEAMLINE{indMult(imult)}.PoleIndex(1:4),bmult_normal{imult}(1:4).*4)
    end
    if length(BEAMLINE{indMult(imult)}.PoleIndex)>3
      fprintf('%s (skew): (%d %d %d %d) %.2f %.2f %.2f %.2f\n',mname{imult},BEAMLINE{indMult(imult)}.PoleIndex(1:4),bmult_skew{imult}(1:4).*4)
    end
  end
end

  % mname=mname(1:20);
%% Initial tracking results with no errors
[beamout sigv]=trackBeam(Model,Beam1_IEX,BEAMLINE,PS,GIRDER); %#ok<*NODEF>
fprintf('Initial beamsize: %g nm (y rms) %g nm (y fit)\n',std(beamout.Bunch.x(3,:))*1e9,sigv(3)*1e9)
Model.syinit=std(beamout.Bunch.x(3,:));
Model.syinitFit=sigv(3);
mnames=unique(mname);

for imag=1:length(mnames)
  basemag=findcells(BEAMLINE,'Name',mnames{imag});
  magind{imag}=[basemag(1)-1 basemag(1)+1];
  if length(basemag)>1
    magind{imag}=[magind{imag} basemag(end)-1 basemag(end)+1];
  end
end

spmd
  %% dK sensitivities
  % dk=linspace(-1e-2,0,10);
  % for imag=1:length(mnames)
  %   magind=findcells(BEAMLINE,'Name',mnames{imag});
  %   if (BEAMLINE{magind(1)}.B(1)*PS(BEAMLINE{magind(1)}.PS).Ampl)==0
  %     continue
  %   end
  %   psinit=PS(BEAMLINE{magind(1)}.PS).Ampl;
  %   for idk=1:length(dk)
  %     PS(BEAMLINE{magind(1)}.PS).Ampl=psinit*(1+dk(idk));
  %     [beamout sigv]=trackBeam(Model,Beam1_IEX);
  %     ysize(idk)=std(beamout.Bunch.x(3,:));
  %     ysizeFit(idk)=sigv(3);
  %     [~,~,bsize_corrected] = beamTerms(3,beamout);
  %     ysizeCor(idk,:)=bsize_corrected;
  %   end
  %   PS(BEAMLINE{magind(1)}.PS).Ampl=psinit;
  %   sens.dk.magName{imag}=mnames{imag};
  %   sens.dk.ysize(imag)=interp1(ysize,dk,Model.syinit+1e-9,'spline');
  %   sens.dk.ysizeFit(imag)=interp1(ysizeFit,dk,Model.syinitFit+1e-9,'spline');
  %   sens.dk.ysizeCor1(imag)=interp1(ysizeCor(:,1)',dk,Model.syinit+1e-9,'spline');
  %   sens.dk.ysizeCor2(imag)=interp1(ysizeCor(:,2)',dk,Model.syinit+1e-9,'spline');
  %   sens.dk.ysizeCor3(imag)=interp1(ysizeCor(:,3)',dk,Model.syinit+1e-9,'spline');
  %   fprintf('%s dk : %g / %g\n',mnames{imag},sens.dk.ysize(imag),sens.dk.ysizeFit(imag))
  % end

  %% Sext. multipole sensitivities
%   mulSize=[0.2 20 2e4 2e7];
%   if matlabpool('size')
%     disp('Running parallel...')
%     sens=getMultSens(Model,Beam1_IEX,sens,mnames,mulSize(labindex),labindex+1,false,BEAMLINE,PS,GIRDER);
%     sens=getMultSens(Model,Beam1_IEX,sens,mnames,mulSize(labindex),labindex+1,true,BEAMLINE,PS,GIRDER);
%   else
%     disp('Running serially...')
%     for ind=1:4
%       sens=getMultSens(Model,Beam1_IEX,sens,mnames,mulSize(ind),ind+1,false,BEAMLINE,PS,GIRDER);
%       sens=getMultSens(Model,Beam1_IEX,sens,mnames,mulSize(ind),ind+1,true,BEAMLINE,PS,GIRDER);
%     end
%   end

  %% Existing multipole magnitude and angle measurement tolerances
  for imult=1:length(indMult)
    if length(berr)>=imult && ~isempty(berr{imult}) && length(bmult{imult})>=4
      BEAMLINE{indMult(imult)}.B(1:length(berr{imult}))=bmult{imult}(1:length(berr{imult}))+...
        randn(1,length(berr{imult})).*berr{imult};
      BEAMLINE{indMult(imult)}.Tilt(1:length(tilterr{imult}))=tiltmult{imult}(1:length(tilterr{imult}))+...
        randn(1,length(tilterr{imult})).*tilterr{imult};
    end
  end
  [~, beamout]=TrackThru(Model.extStart,Model.ip_ind,Beam1_IEX,1,1,0);
  rays0=[beamout.Bunch.x]';
  P=mean(rays0(:,6)); % GeV/c
  dp=(rays0(:,6)-P)/P;
  rays0(:,6)=dp;
  [~,~,~,sigv]=AnalyzeRays(rays0,P,0,false);
%   [beamout sigv]=trackBeam(Model,Beam1_IEX,BEAMLINE,PS,GIRDER);
  ysize=std(beamout.Bunch.x(3,:))-Model.syinit;
  ysizeFit=sigv(3)-Model.syinitFit;
  [~,~,bsize_corrected] = beamTerms(3,beamout);
  ysizeCor1=bsize_corrected(1)-Model.syinit;
  ysizeCor2=bsize_corrected(2)-Model.syinit;
  ysizeCor3=bsize_corrected(3)-Model.syinit;
  for imult=1:length(indMult)
    if length(berr)>=imult && ~isempty(berr{imult}) && length(bmult{imult})>=4
      BEAMLINE{indMult(imult)}.B(1:length(berr{imult}))=bmult{imult}(1:length(berr{imult}));
      BEAMLINE{indMult(imult)}.Tilt(1:length(tilterr{imult}))=tiltmult{imult}(1:length(tilterr{imult}));
    end
  end
end
fprintf('dY = %g +/- %g (Fit) %g +/- %g (RMS)\n',mean([ysizeFit{:}]),std([ysizeFit{:}]),...
  mean([ysize{:}]),std([ysize{:}]))
tol.ysize=ysize;
tol.ysizeFit=ysizeFit;
tol.ysizeCor1=ysizeCor1;
tol.ysizeCor2=ysizeCor2;
tol.ysizeCor3=ysizeCor3;

%% Internal functions
function relErr=getBerr(mname,eval)
% Relative error on multipole field strength assuming 1e-5 error on
% Bn/Bquad @ 1cm
mnameLookup={'QM14FF' 'QM15FF' 'QD16X' 'QM11FF' 'QM13FF' 'QF17X' 'QD18X' 'QM16FF' 'QF19X' 'QF11X' 'QD10X' 'QM12FF' 'QD12X' 'QD8FF' ...
  'QD10AFF' 'QF9BFF' 'QF9AFF' 'QD10BFF' 'QD6FF' 'QF7FF' 'QF5AFF' 'QF5BFF' 'QD4AFF' 'QF3FF' 'QD2AFF' 'QD4BFF' 'QD2BFF'};
relerrLookup=[0.03 0.04 0.09 0.01;
  0.1 0.08 0.05 0.02;
  0.02 0.06 0.05 0.01;
  0.04 0.16 0.11 0.01; 
  0.08 0.03 0.08 0.01;
  0.11 0.03 0.11 0.01;
  0.05 0.08 0.2 0.01;
  0.03 0.03 0.13 0.01;
  0.01 0.08 0.19 0.01;
  0.07 0.02 0.28 0.01;
  0.01 0.03 0.04 0.01;
  0.04 0.1 0.27 0.01;
  0.01 0.03 0.04 0.01;
  0.01 0.01 0.03 0.02;
  0.01 0.07 0.19 0.02;
  0.02 0.11 0.07 0.01;
  0.01 0.02 0.04 0.01;
  0.01 0.03 0.4 0.01;
  4.90E-3 1.08E-2 2.60E-2 1.63E-2;
  121.2E-4 417.5E-4 141.8E-4 138.0E-3;
  155.1E-4 104.1E-4 554.2E-4 138.9E-4;
  840.5E-5 126.4E-4 451.7E-4 108.6E-4;
  336.8E-5 167.2E-4 544.8E-4 162.9E-4;
  886.9E-5 985.4E-5 526.3E-4 155.7E-4;
  107.6E-4 905.1E-5 404.6E-4 153.3E-4;
  315.4E-5 151.8E-4 268.8E-4 123.5E-4;
  423.2E-5 233.7E-3 124.6E-3 141.5E-4].*eval;

werr=find(ismember(mname,mnameLookup));
if ~isempty(werr)
 relErr=relerrLookup(werr,:);
else
  relErr=[];
end

function sens=getMultSens(Model,Beam,sens,mnames,magnitude,pind,skew,BEAMLINE,PS,GIRDER)
dmult=linspace(0,magnitude,10);
sensNames={'sext' 'oct' 'dec' 'dodec'};
if skew
  sensName=['skew' sensNames{pind-1}];
else
  sensName=sensNames{pind-1};
end
sens.(sensName).ysize=zeros(1,length(mnames));
sens.(sensName).ysizeFit=zeros(1,length(mnames));
for imag=1:length(mnames)
  magind=[];
  basemag=findcells(BEAMLINE,'Name',mnames{imag});
  magind(1)=basemag(1)-1;
  magind(2)=basemag(1)+1;
  if length(basemag)>1
    magind(3)=basemag(end)-1;
    magind(4)=basemag(end)+1;
  end
  if ~ismember(pind,BEAMLINE{magind(1)}.PoleIndex)
    continue
  else
    wind=BEAMLINE{magind(1)}.PoleIndex==pind;
  end
  binit=BEAMLINE{magind(1)}.B(wind);
  tinit=BEAMLINE{magind(1)}.Tilt(wind);
  tiltSel=pi.*(360./(BEAMLINE{magind(1)}.PoleIndex+1)/4)./180;
  for imc=1:length(magind)
    if skew
      BEAMLINE{magind(imc)}.Tilt(wind)=tiltSel(pind-1);
    else
      BEAMLINE{magind(imc)}.Tilt(wind)=0;
    end
  end
  for idmult=1:length(dmult)
    for imc=1:length(magind)
      BEAMLINE{magind(imc)}.B(wind)=binit+dmult(idmult);
    end
    [beamout sigv]=trackBeam(Model,Beam,BEAMLINE,PS,GIRDER);
    ysize(idmult)=std(beamout.Bunch.x(3,:));
    ysizeFit(idmult)=sigv(3);
    [~,~,bsize_corrected] = beamTerms(3,beamout);
    ysizeCor(idmult,:)=bsize_corrected;
  end
  for imc=1:length(magind)
    BEAMLINE{magind(imc)}.B(wind)=binit;
    BEAMLINE{magind(imc)}.Tilt(wind)=tinit;
  end
  sens.(sensName).magName{imag}=mnames{imag};
  try
    sens.(sensName).ysize(imag)=interp1(ysize,dmult,Model.syinit+1e-9,'spline')*length(magind);
    sens.(sensName).ysizeFit(imag)=interp1(ysizeFit,dmult,Model.syinitFit+1e-9,'spline')*length(magind);
    sens.(sensName).ysizeCor1(imag)=interp1(ysizeCor(:,1)',dmult,Model.syinit+1e-9,'spline')*length(magind);
    sens.(sensName).ysizeCor2(imag)=interp1(ysizeCor(:,2)',dmult,Model.syinit+1e-9,'spline')*length(magind);
    sens.(sensName).ysizeCor3(imag)=interp1(ysizeCor(:,3)',dmult,Model.syinit+1e-9,'spline')*length(magind);
  catch
    sens.(sensName).ysize(imag)=1e30;
    sens.(sensName).ysizeFit(imag)=1e30;
    sens.(sensName).ysizeCor1(imag)=1e30;
    sens.(sensName).ysizeCor2(imag)=1e30;
    sens.(sensName).ysizeCor3(imag)=1e30;
  end
  fprintf('%s %s : %g / %g\n',mnames{imag},sensName,sens.(sensName).ysize(imag),sens.(sensName).ysizeFit(imag))
end
    

function Bskew=getSkewComponent(BL)
stilt=pi.*(360./(BL.PoleIndex+1)/4)./180;
BL.Tilt=abs(BL.Tilt);
Bskew=zeros(size(stilt));
% put tilt in 0 -> 2* stilt range and get magnitude of skew component
for iskew=1:length(BL.PoleIndex)
  while BL.Tilt(iskew) > stilt(iskew)*2
    BL.Tilt(iskew) = BL.Tilt(iskew)-stilt(iskew)*2 ;
  end
  if BL.Tilt(iskew)>stilt(iskew)
    Bskew(iskew)=(abs(BL.B(iskew))*cos((pi/2)-BL.Tilt(iskew)))/cos(stilt(iskew));
  else
    Bskew(iskew)=(abs(BL.B(iskew))*cos(BL.Tilt(iskew)))/cos(stilt(iskew));
  end
end

function Bnorm=getNormComponent(BL)
stilt=pi.*(360./(BL.PoleIndex+1)/4)./180;
BL.Tilt=abs(BL.Tilt);
Bnorm=zeros(size(stilt));
% put tilt in 0 -> 2* stilt range and get magnitude of normal component
for iskew=1:length(BL.PoleIndex)
  while BL.Tilt(iskew) > stilt(iskew)*2
    BL.Tilt(iskew) = BL.Tilt(iskew)-stilt(iskew)*2 ;
  end
  Bnorm(iskew)=abs(BL.B(iskew))*cos(BL.Tilt(iskew));
end

function [beamout sigv]=trackBeam(Model,Beam,BEAMLINE,PS,GIRDER) %#ok<INUSD>
[~, beamout]=TrackThru(Model.extStart,Model.ip_ind,Beam,1,1,0);
rays0=[beamout.Bunch.x]';
P=mean(rays0(:,6)); % GeV/c
dp=(rays0(:,6)-P)/P;
rays0(:,6)=dp;
[~,~,~,sigv]=AnalyzeRays(rays0,P,0,false);
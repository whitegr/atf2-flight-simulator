function bs=wirefit(inputGauss,inputSize)

% bs=fminsearch(@(x) minfunc(x,inputGauss),[1e-6 5e-6],optimset('Display','iter','TolX',1e-9,'TolFun',1e-9^2));

bs=fminsearch(@(x) minfunc2(x,inputGauss,inputSize),5e-6,optimset('Display','iter','TolX',1e-9,'TolFun',1e-9^2));


function chi2=minfunc2(x,inputGauss,inputSize)

[C_x C_y]=wireAnal(inputSize,x(1),0);
% resample to +/- 5sigma
xvals=C_x(C_x>-x(1)*5 & C_x<x(1)*5);
xvals=xvals(1:ceil(length(xvals)/1000):end);
C_y=C_y(ismember(C_x,xvals));
C_x=C_x(ismember(C_x,xvals));
[yfit,q] = gauss_fit(C_x,C_y);
chi2=(q(4)-inputGauss).^2;

function chi2=minfunc(x,inputGauss)

[C_x C_y]=wireAnal(x(1),x(2),0);
% resample to +/- 5sigma
xvals=C_x(C_x>-x(1)*5 & C_x<x(1)*5);
xvals=xvals(1:ceil(length(xvals)/1000):end);
C_y=C_y(ismember(C_x,xvals));
C_x=C_x(ismember(C_x,xvals));
[yfit,q] = gauss_fit(C_x,C_y);
chi2=(q(4)-inputGauss).^2;
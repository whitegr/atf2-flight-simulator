function stat = SextMultiKnob(resp,knobval,beam)
% set a sextupole-based multiknob (relative move)
% Must have FL.SimBeam / FL.SimModel defined

%% Main routine
global FL
persistent moves

% restore functionality
if isequal(resp,'get_restore')
  stat={moves}; return;
elseif isequal(resp,'set_restore')
  moves=knobval{1}; return;
end % if restore

stat{1}=1; stat{2}=[];

% init moves array
if isempty(moves); moves=zeros(20,1); end;

% args check
if ~exist('resp','var') || ~isfield(resp,'terms') || ~isfield(resp,'x') || ...
    ~isfield(resp,'y') || ~isfield(resp,'tilt') || ~isfield(resp,'dk')
  error('Incorrect resp structure passed')
end % if resp struc error
if length(knobval)~=length(resp.terms)
  error('Must provide a vector of length %d for knobval',length(resp.terms))
end % if wrong knobval length
[r c]=size(knobval); if r>c; knobval=knobval'; end;

% Move normalisation
mnorm=[max(abs(resp.x.move)).*ones(1,5);
  max(abs(resp.y.move)).*ones(1,5);
  max(abs(resp.tilt.move)).*ones(1,5);
  max(abs(resp.dk.move)).*ones(1,5); ];

% Where are we in knob-space?
origknob=knobcalc(reshape(moves./mnorm(:),4,5),resp);

% Calculate move based on gradient vectors
gain=0.01;
newmoves=moves;
goalknob=origknob+knobval; curknob=goalknob;
nch=abs(goalknob)>0;
T=knobmat(reshape(newmoves,4,5),resp);
curknob=knobcalc(reshape(newmoves./mnorm(:),4,5),resp);
while abs(sum( curknob(nch)-goalknob(nch).^2 )/sum(goalknob(nch).^2))>1e-4
  newmoves=newmoves+lscov(T,(goalknob'-curknob')*gain).*mnorm(:);
  T=knobmat(reshape(newmoves,4,5),resp);
  curknob=knobcalc(reshape(newmoves./mnorm(:),4,5),resp);
  sum((curknob(nch)-goalknob(nch)).^2 )/sum(goalknob(nch).^2)
end % for nmove

% Use simplex to find moves
% moves = fminsearch(@(x) knobmin(x,origknob(1:end)+knobval(1:end),resp),moves./mnorm(:),...
%   optimset('Display','iter','TolFun',1e-3,'TolX',1e-4,'MaxFunEvals',10000,'MaxIter',10000)).*mnorm(:);

% Make moves
setSextKnob(FL.SimModel,beam,reshape(moves,4,5));

%% Minimisatation functions
function chi2 = knobmin(x,reqknob,resp)
% minimising function for achieving sext arrangement to get desired knob
% setting (Nelder-mead fminsearch method)
moves=reshape(x,4,5);
theseknobs=knobcalc(moves,resp);
chi2 = sum( (theseknobs(1:end)-reqknob(1:end)).^2 )./sum(reqknob(1:end).^2);


%% Knob calculation
function knobvals=knobcalc(moves,resp)
% Estimate knobs given sextupole configuration
persistent dims

% Check moves are within calculated response curves
if isempty(dims); dims={'x' 'y' 'tilt' 'dk'}; end;
% for iMove=1:4
%   if any(moves(iMove,:)>max(resp.(dims{iMove}).move)) || any(moves(iMove,:)<min(resp.(dims{iMove}).move))
%     knobvals=1e99.*ones(size(resp.terms));
%     return
%   end % if moves outside calculated response curves
% end % for iMove
if any(any(abs(moves)>1))
  knobvals=1e99.*ones(size(resp.terms));
  return
end % if any moves > 1

% lookup move vs tsize using 1-d spline interp
knobvals=zeros(size(resp.terms));
for is=1:5
  for im=1:4
    for iterm=1:length(knobvals)
      knobvals(1,iterm)=knobvals(iterm)+interp1(resp.(dims{im}).move_fit,resp.(dims{im}).tsize_fit{is,iterm},moves(im,is)*max(abs(resp.(dims{im}).move_fit)),'linear');
    end % for iterm
  end % for im
end % for is

function T = knobmat(moves,resp)
% Get response matrix for current sextupole configuration
persistent dims
if isempty(dims); dims={'x' 'y' 'tilt' 'dk'}; end;
for is=1:5
  for im=1:4
    for iterm=1:length(resp.(dims{im}).tsize_grad)
      T(iterm,(is-1)*4+im)=interp1(resp.(dims{im}).move_fit,resp.(dims{im}).tsize_grad{is,iterm},moves(im,is),'linear');
    end % for iterm
  end % for im
end % for is

% function latmatch
global PS BEAMLINE
% Load lattice
load  ~/ATF2/FlightSim/latticeFiles/src/v5.0/ATF2lat_BX10BY1
% load tempmatch
% zeroMult;

% Make match object
i1=findcells(BEAMLINE,'Name','QD20X'); i1=i1(1);
[beam Istruc]=initTransport(Model.Initial,Beam1,1,i1-1);

M=Match;
M.beam=beam;
M.iInitial=i1;
M.initStruc=Istruc;
M.verbose=true; % see optimizer output or not
M.optim='lsqnonlin';
% 
% Variables
mq=findcells(BEAMLINE,'Name','QM*FF'); mq=mq(1:2:end);
mq=mq(1:end-1); mq=mq(2:end); % do not include QM11FF or QM16FF (keep 0)
mqps=arrayfun(@(x) BEAMLINE{x}.PS,mq);
for ips=1:length(mqps)
  MovePhysicsVarsToPS(mqps(ips));
  M.addVariable('PS',mqps(ips),'Ampl',-10,10);
end
mq=findcells(BEAMLINE,'Name','QD20X'); mqps=BEAMLINE{mq(1)}.PS;
mq=findcells(BEAMLINE,'Name','QF21X'); mqps(2)=BEAMLINE{mq(1)}.PS;
for ips=1:length(mqps)
  MovePhysicsVarsToPS(mqps(ips));
  M.addVariable('PS',mqps(ips),'Ampl',-10,10);
end
% % QD0FF
qd0=findcells(BEAMLINE,'Name','QD0FF');
qd0ps=BEAMLINE{qd0(1)}.PS;
M.addVariable('PS',qd0ps,'Ampl',0.8,1.2);
% % QF1FF
qf1=findcells(BEAMLINE,'Name','QF1FF');
qf1ps=BEAMLINE{qf1(1)}.PS;
M.addVariable('PS',qf1ps,'Ampl',0.8,1.2);
% % Skew sexts
sk=findcells(BEAMLINE,'Name','SK*FF'); sk=sk(1:2:end);
skps=arrayfun(@(x) BEAMLINE{x}.PS,sk);
% 
ss=findcells(BEAMLINE,'Name','S*FF'); ss=ss(1:2:end); ss=ss(~ismember(ss,sk));
ssps=arrayfun(@(x) BEAMLINE{x}.PS,ss);
% % All quads
% aq=findcells(BEAMLINE,'Name','Q*FF'); aq=aq(1:2:end);
% aqps=arrayfun(@(x) BEAMLINE{x}.PS,aq(~ismember(aq,[mq qd0(1) qf1(1)])));
% 
% % Constraints
% % Min IP vertical spot size + alpha's & betas at IP and vertical waist at
% %   MQF2FF IP vertical image point
i_ip=findcells(BEAMLINE,'Name','IP');
i_image=findcells(BEAMLINE,'Name','MFB2FF');
i_mfb1=findcells(BEAMLINE,'Name','MFB1FF');
M.addMatch(i_image,'nu_y',0,1e-3,num2str(Model.ip_ind));
M.addMatch(i_mfb1,'nu_x',0,1e-3,num2str(Model.ip_ind));
M.addMatch(findcells(BEAMLINE,'Name','ZV1FF'),'nu_y',1,1e-3,num2str(i_image));
M.addMatch(findcells(BEAMLINE,'Name','ZH1FF'),'nu_x',1,1e-3,num2str(i_mfb1));
M.addMatch(i_ip,'alpha_x',0,1e-4);
M.addMatch(i_ip,'alpha_y',0,1e-4);
M.addMatch(i_ip,'beta_x',0.04,0.0001);
M.addMatch(i_ip,'beta_y',1e-4,1e-5);
% 
% % See initial problem
display(M)
% 
% Run matching procedure (linear)
M.doMatch;
% 
% % Add image point constraints and rematch
% M.addMatch(i_image,'alpha_y',0,1e-4);
% % M=addMatch(M,i_image,'beta_y',0.014,0.001);
% M.addMatch(i_mfb1,'alpha_x',0,1e-4);
% % M=addMatch(M,i_mfb1,'beta_x',1e-2,1e-3;
% 
% % See end previous results
% display(M)
% 
% M.doMatch;
display(M)

% Run matching procedure (non-linear)
M2=Match;
M2.beam=beam;
M2.iInitial=i1;
M2.initStruc=Istruc;
M2.verbose=false;
M2.optimDisplay='iter';
M2.optim='fmincon';
M2.useParallel=false;
% for ips=1:length(mqps)
%   M2.addVariable('PS',mqps(ips),'Ampl',-2.5,2.5);
% end
% M2.addVariable('PS',qd0ps,'Ampl',0.8,1.2);
% M2.addVariable('PS',qf1ps,'Ampl',0.8,1.2);
% for ips=1:length(aqps)
%   M2=addVariable(M2,'PS',aqps(ips),'Ampl',0.7,1.2);
% end
% PS(53).Ampl=0;
for ips=1:length(ss)
%   if ssps(ips)==53; continue; end; % exclude sf5ff
  if ssps(ips)==53;
    M2.addVariable('PS',ssps(ips),'Ampl',1,1.5);
  else
    M2.addVariable('PS',ssps(ips),'Ampl',0.5,1.5);
  end
end
% for ips=1:length(skps)
%   M2.addVariable('PS',skps(ips),'Ampl',-50,50);
% end
M2.addMatch(i_ip,'Sigma',35e-9^2,3.5e-17,'33');
% M2.addMatch(i_ip,'SigmaGauss',35e-9,0.5e-9,'3');
% M2.addMatch(i_image,'alpha_y',0,1e-4);
% M2.addMatch(i_mfb1,'alpha_x',0,1e-4);
% M2.addMatch(i_ip,'Sigma',8.9e-6^2,2e-12,'11');

% starting non-linear match condition
display(M2);
M2.doMatch;

% See end results
display(M2)

% Renormalise Power supplies to BEAMLINE
for ips=1:length(PS)
  if any(PS(ips).Element) && isempty(regexp(BEAMLINE{PS(ips).Element(1)}.Class,'COR$','once'))
    RenormalizePS(ips);
    PS(ips).SetPt=PS(ips).Ampl;
  end
end

% Check tracking
[stat b]=TrackThru(1,i_ip,Beam1,1,1,0);
fprintf('IP x = %g y = %g\n',std(b.Bunch.x(1,:)),std(b.Bunch.x(3,:)))

save endmatch


function [sy, syp, mang, mstd, emit] = wakeTrack(BL,offset,Beam1,Model,varargin)
% BL: bunch length in mm
% offset.name : cell array of BEAMLINE Names
% offset.x : horizontal offset
% offset.y : vertical offset
global BEAMLINE
load userData/atfWakeCalc tw9 tw12
switch BL
  case 9
    S=tw9(:,1); tw=2.*tw9(:,2).*1e12.*1e3;
  case 12
    S=tw12(:,1); tw=2.*tw12(:,2).*1e12.*1e3;
  otherwise
    error('No wake data for this bunch length')
end

i1=findcells(BEAMLINE,'Name','MQM16FF');
emit=[];
getemit=false;
if nargin>=5 && strcmpi(varargin{1},'getemit')
  getemit=true;
  gamma=mean(Beam1.Bunch.x(6,:))/0.511e-3;
end

[~, b]=TrackThru(1,i1-1,Beam1,1,1,0);

for iele=i1+1:Model.ip_ind
  [~, b]=TrackThru(iele,iele,b,1,1,0);
  if ismember(BEAMLINE{iele}.Name,offset.name)
    [b,mang,mstd]=wakeConv(b,S,tw,offset.y(ismember(offset.name,BEAMLINE{iele}.Name)));
  end
  if getemit
    [~,ny] = GetNEmitFromBeam( b, 1 );
    emit(iele-i1)=ny/gamma;
  end
end
sy=std(b.Bunch.x(3,:))*1e9;
syp=std(b.Bunch.x(4,:));
fprintf('SIGMA_Y (IP) = %g nm\n',sy)
function ret = qbba_dfs(quad_list, sig_bpm, sig_mover, bpm_set, mover_set, Beam, Model, dispWeight)
% Quad DFS for BDS using quad movers
global GIRDER INSTR
persistent R

ret=[];
% restore functionality
if isequal(quad_list,'get_restore')
  ret={R}; return;
elseif isequal(quad_list,'set_restore')
  R=sig_bpm{1}; return;
end % if restore

% Check bpm_set, mover_set format
if ~iscell(bpm_set)
  bpm_set={};
  bpm_set{1}=0; bpm_set{2}=0;
elseif ~length(bpm_set)==2 || ~(length(bpm_set{1})==(length(quad_list)-1)) || ~(length(bpm_set{2})==(length(quad_list)-1))
  error('Incorrectly formatted bpm_set cell array!');
end
if ~iscell(mover_set)
  mover_set={};
  mover_set{1}=0; mover_set{2}=0;
elseif ~length(mover_set)==2 || ~(length(mover_set{1})==(length(quad_list)-1)) || ~(length(mover_set{2})==(length(quad_list)-1))
  error('Incorrectly formatted mover_set cell array!');
end

% Energy Delta
dE_set = 0.013 ; % GeV

% Get Quad mover and bpm list
qmovers = quad_list(2:end-1);
qbpms = quad_list(2:end);

% Check weight format
if ~iscell(sig_bpm) && length(sig_bpm)==1
  sbpm=sig_bpm; sig_bpm={};
  sig_bpm{1}=ones(length(quad_list)-1,1).*sbpm;
  sig_bpm{2}=ones(length(quad_list)-1,1).*sbpm;
elseif ~iscell(sig_bpm) && length(sig_bpm)==2
  sbpm=sig_bpm; sig_bpm={};
  sig_bpm{1}=ones(length(quad_list)-1,1).*sbpm(1);
  sig_bpm{2}=ones(length(quad_list)-1,1).*sbpm(2);
elseif ~iscell(sig_bpm) || ( length(sig_bpm{1})~=length(quad_list)-1 ) || ( length(sig_bpm{2})~=length(quad_list)-1 )
  error('Incorrect sig_bpm format!');
end
if ~iscell(sig_mover) && length(sig_mover)==1
  smover=sig_mover; sig_mover={};
  sig_mover{1}=ones(length(quad_list)-1,1).*smover;
  sig_mover{2}=ones(length(quad_list)-1,1).*smover;
elseif ~iscell(sig_mover) && length(sig_mover)==2
  smover=sig_mover; sig_mover={};
  sig_mover{1}=ones(length(quad_list)-1,1).*smover(1);
  sig_mover{2}=ones(length(quad_list)-1,1).*smover(2);
elseif ~iscell(sig_bpm) || ( length(sig_bpm{1})~=length(quad_list)-1 ) || ( length(sig_bpm{2})~=length(quad_list)-1 )
  error('Incorrect sig_bpm format!');
end
[rw,cl]=size(sig_bpm{1}); if cl>rw; sig_bpm{1}=sig_bpm{1}'; end;
[rw,cl]=size(sig_bpm{2}); if cl>rw; sig_bpm{2}=sig_bpm{2}'; end;
[rw,cl]=size(sig_mover{1}); if cl>rw; sig_mover{1}=sig_mover{1}'; end;
[rw,cl]=size(sig_mover{2}); if cl>rw; sig_mover{2}=sig_mover{2}'; end;

% bba constraint vector (bpm's + dispersive bpm's + movers + initial trajectory correction)
% [x & y]
b = zeros( (length(qbpms) + length(qmovers) + 1)*2 , 1);

% bba correction vector (quad moves + 1st quad dipole steer)
c = zeros( 2*(length(qmovers)+1), 1 );

% Initialise R Matrix Cell on first call
if isempty(R)
  R=cell(3,1);
end

% Which alignment segment is being run
aSeg=Model.qbba_segment;

% DFS Response Matrix 
pMoves=[dE_set+randn*Model.dE_set_error -dE_set+randn*Model.dE_set_error];
if isempty(R{aSeg}) % Only calculate on first run of subroutine for given R segment
  R_ret1 = getRmat(Model,b,c,qmovers,qbpms,Model.magGroup.Quad.dB.ClusterList(quad_list(1)).index(end)+1);
  rSize=size(R_ret1);
  pShift(pMoves(1));
  R_ret2 = getRmat(Model,b,c,qmovers,qbpms,Model.magGroup.Quad.dB.ClusterList(quad_list(1)).index(end)+1);
  pShift(-pMoves(1)+pMoves(2));
  R_ret3 = getRmat(Model,b,c,qmovers,qbpms,Model.magGroup.Quad.dB.ClusterList(quad_list(1)).index(end)+1);
  pShift(-pMoves(2));
  R{aSeg} = [ R_ret1(1:rSize(1)/2,:);
              R_ret1(1:rSize(1)/2,:)-R_ret2(1:rSize(1)/2,:);
              R_ret1(1:rSize(1)/2,:)-R_ret3(1:rSize(1)/2,:);
              R_ret1((rSize(1)/2)+1:end,:) ];
end % first run

% Get BPM readings and fill B vector
pMoves=[dE_set+randn*Model.dE_set_error -dE_set+randn*Model.dE_set_error];
BpmHan = [Model.magGroup.Quad.Bpm(quad_list).Han];
if Model.doGM
  Model = myTrack(Beam,Model);
else
  FL.simBeamID=2; FL.SimModel=Model;
  FlHwUpdate;
  Model.bpmData.x=cellfun(@(x) x.Data(1),INSTR); Model.bpmData.y=cellfun(@(x) x.Data(2),INSTR);
end
bpm_x = Model.bpmData.x(BpmHan) ;
bpm_y = Model.bpmData.y(BpmHan) ;
pShift(pMoves(1));
if Model.doGM
  Model = myTrack(Beam,Model);
else
  FL.simBeamID=2; FL.SimModel=Model;
  FlHwUpdate;
  Model.bpmData.x=cellfun(@(x) x.Data(1),INSTR); Model.bpmData.y=cellfun(@(x) x.Data(2),INSTR);
end
bpm_dx1 = bpm_x-Model.bpmData.x(BpmHan);
bpm_dy1 = bpm_y-Model.bpmData.y(BpmHan);
pShift(-pMoves(1)+pMoves(2));
if Model.doGM
  Model = myTrack(Beam,Model);
else
  FL.simBeamID=2; FL.SimModel=Model; %#ok<STRNU>
  FlHwUpdate;
  Model.bpmData.x=cellfun(@(x) x.Data(1),INSTR); Model.bpmData.y=cellfun(@(x) x.Data(2),INSTR);
end
bpm_dx2 = bpm_x-Model.bpmData.x(BpmHan);
bpm_dy2 = bpm_y-Model.bpmData.y(BpmHan);
pShift(-pMoves(2));
bpm_result.initial = [std(bpm_x(2:end)) std(bpm_y(2:end))]; %#ok<STRNU>
B=zeros(length(b)*2,1);
B(1:length(b)/4) = -bpm_x(2:end) + bpm_set{1} ;
B(length(b)/4+1:length(b)/2) = -bpm_y(2:end) + bpm_set{2} ;
B(1+length(b)/2:length(b)/2+length(b)/4) = -bpm_dx1(2:end) ;
B(length(b)/2+length(b)/4+1:length(b)/2+length(b)/2) = -bpm_dy1(2:end) ;
B(1+length(b):length(b)+length(b)/4) = -bpm_dx2(2:end) ;
B(length(b)+length(b)/4+1:length(b)+length(b)/2) = -bpm_dy2(2:end) ;
B(length(b)*(3/2)+1:length(b)*(3/2)+length(b)/4) = mover_set{1} ;
B(length(b)*(3/2)+length(b)/4+1:end) = mover_set{2} ;

% Generate weight vector
w=[sig_bpm{1}; sig_bpm{2}; sig_bpm{1}.*dispWeight; sig_bpm{2}.*dispWeight;
   sig_bpm{1}.*dispWeight; sig_bpm{2}.*dispWeight; sig_mover{1}; sig_mover{2}];

% Calcuate c vector
c = lscov(R{aSeg},B,1./(w.^2)) ;

% Apply correction
ind=0;
for iquad=quad_list(1:end-1)
  ind=ind+1;
  if ind==1
    [stat,Rquad]=RmatAtoB(GIRDER{Model.Gind.Quad(iquad)}.Element(1),GIRDER{Model.Gind.Quad(iquad)}.Element(end));
    if stat{1}~=1; error(stat{2:end}); end;
    gcorval=GIRDER{Model.Gind.Quad(iquad)}.MoverPos(1:2)-[c(length(c)/2) c(end)]./[Rquad(2,1) Rquad(4,3)];
    if ~any(isinf(gcorval)) && ~any(isnan(gcorval))
      GIRDER{Model.Gind.Quad(iquad)}.MoverSetPt(1:2)=gcorval;
    end
%     BEAMLINE{Model.magGroup.Quad.Bpm(iquad).Ind}.ElecOffset=BEAMLINE{Model.magGroup.Quad.Bpm(iquad).Ind}.ElecOffset+[c(length(c)/2) c(end)]./[Rquad(2,1) Rquad(4,3)];
    if Model.reality
      xpos = GIRDER{Model.Gind.Quad(iquad)}.MoverSetPt(1);
      ypos = GIRDER{Model.Gind.Quad(iquad)}.MoverSetPt(2);
      camangles = fftbmover_convert(Model,xpos,ypos,0,'quad');
      if ~isreal(camangles(1)) || ~isreal(camangles(2)) || ~isreal(camangles(3))
        save(['radpolice_saves/out' num2str(Model.nseed) '.mat'])
        disp(' ')
        error('Attempting to push cams beyond physical limits.')
      end
    end
  else
    GIRDER{Model.Gind.Quad(iquad)}.MoverSetPt = GIRDER{Model.Gind.Quad(iquad)}.MoverPos;
    gcorval=[ c(ind-1) c(ind-1+length(c)/2) ] ;
    if ~any(isinf(gcorval)) && ~any(isnan(gcorval))
      GIRDER{Model.Gind.Quad(iquad)}.MoverSetPt(1:2) = GIRDER{Model.Gind.Quad(iquad)}.MoverSetPt(1:2) + gcorval;
    end
    if Model.reality
      xpos = GIRDER{Model.Gind.Quad(iquad)}.MoverSetPt(1);
      ypos = GIRDER{Model.Gind.Quad(iquad)}.MoverSetPt(2);
      camangles = fftbmover_convert(Model,xpos,ypos,0,'quad');
      if ~isreal(camangles(1)) || ~isreal(camangles(2)) || ~isreal(camangles(3))
        save(['radpolice_saves/out' num2str(Model.nseed) '.mat'])
        disp(' ')
        error('Attempting to push cams beyond physical limits.')
      end
    end
  end
end
stat = MoverTrim( Model.Gind.Quad(quad_list(1:end-1)) );if stat{1}~=1; error(stat{2:end}); end;

% ----------------------------------------------

function R = getRmat(Model,b,c,qmovers,qbpms,firstQuad)
R = zeros( length(b) , length(c) );
for ic=1:length(c)
  for ib=1:length(b)
    if (ib==ic) && (ib<(length(b)/4)) || ( ib>(length(b)/4) && (ib<length(b)/2) && (ib==ic) )
      R(ib,ic) = -1 ;
    elseif (ic==(length(c)/2)) && (ib<=(length(b)/4))
      [stat, R1] = RmatAtoB( firstQuad, Model.magGroup.Quad.Bpm(qbpms(ib)).Ind );
      if stat{1}~=1; error(stat{2:end}); end;
      R(ib,ic) = R1(1,2) ;
    elseif (ic==length(c)) && (ib<=(length(b)/4))
      [stat, R1] = RmatAtoB( firstQuad, Model.magGroup.Quad.Bpm(qbpms(ib)).Ind );
      if stat{1}~=1; error(stat{2:end}); end;
      R(ib,ic) = R1(1,4) ;
    elseif (ic==length(c)) && ( (ib<=(length(b)/2)) && ib>(length(b)/4) )
      [stat, R1] = RmatAtoB( firstQuad, Model.magGroup.Quad.Bpm(qbpms(ib-(length(b)/4))).Ind );
      if stat{1}~=1; error(stat{2:end}); end;
      R(ib,ic) = R1(3,4) ;
    elseif (ic==length(c)/2) && ( (ib<=(length(b)/2)) && ib>(length(b)/4) )
      [stat, R1] = RmatAtoB( firstQuad, Model.magGroup.Quad.Bpm(qbpms(ib-(length(b)/4))).Ind );
      if stat{1}~=1; error(stat{2:end}); end;
      R(ib,ic) = R1(3,2) ;
    elseif ( ib>ic ) && ( ic<(length(c)/2) ) && (ib<=(length(b)/4))
      [stat,RQ]=RmatAtoB(Model.magGroup.Quad.Off.ClusterList(qmovers(ic)).index(1), ...
                         Model.magGroup.Quad.Off.ClusterList(qmovers(ic)).index(end));
      if stat{1}~=1; error(stat{2:end}); end;
      [stat,R1]=RmatAtoB(Model.magGroup.Quad.Off.ClusterList(qmovers(ic)).index(end)+1, ...
                         Model.magGroup.Quad.Bpm(qbpms(ib)).Ind);
      if stat{1}~=1; error('mover= %g bpm= %g ic= %d ib= %d ind1= %g ind2= %g : error: %s',ic,ib,qmovers(ic),qbpms(ib),Model.magGroup.Quad.Off.ClusterList(qmovers(ic)).index(end)+1,Model.magGroup.Quad.Bpm(qbpms(ib)).Ind,stat{2:end}); end;
      R(ib,ic) = RQ_func('XX',RQ,R1);
    elseif ( (ib>(ic-length(c)/2) ) ) && ( ic>(length(c)/2) ) && (ib<=(length(b)/4))
      [stat,RQ]=RmatAtoB(Model.magGroup.Quad.Off.ClusterList(qmovers(ic-length(c)/2)).index(1), ...
                         Model.magGroup.Quad.Off.ClusterList(qmovers(ic-length(c)/2)).index(end));
      if stat{1}~=1; error(stat{2:end}); end;
      [stat,R1]=RmatAtoB(Model.magGroup.Quad.Off.ClusterList(qmovers(ic-length(c)/2)).index(end)+1, ...
                         Model.magGroup.Quad.Bpm(qbpms(ib)).Ind);
      if stat{1}~=1; error(stat{2:end}); end;
      R(ib,ic) = RQ_func('XY',RQ,R1);
    elseif ( (ib>ic ) && ( ic<(length(c)/2) ) ) && ( (ib<=(length(b)/2)) && ib>(length(b)/4) && (ib-length(b)/4)>ic )
      [stat,RQ]=RmatAtoB(Model.magGroup.Quad.Off.ClusterList(qmovers(ic)).index(1), ...
                         Model.magGroup.Quad.Off.ClusterList(qmovers(ic)).index(end));
      if stat{1}~=1; error(stat{2:end}); end;
      [stat,R1]=RmatAtoB(Model.magGroup.Quad.Off.ClusterList(qmovers(ic)).index(end)+1, ...
                         Model.magGroup.Quad.Bpm(qbpms(ib-length(b)/4)).Ind);
      if stat{1}~=1; error(stat{2:end}); end;
      R(ib,ic) = RQ_func('YX',RQ,R1);
    elseif ( (ib>ic ) && ( ic>(length(c)/2) ) ) && ( (ib<=(length(b)/2)) && ib>(length(b)/4) && (ib-length(b)/4)>(ic-length(c)/2) ) 
      [stat,RQ]=RmatAtoB(Model.magGroup.Quad.Off.ClusterList(qmovers(ic-length(c)/2)).index(1), ...
                         Model.magGroup.Quad.Off.ClusterList(qmovers(ic-length(c)/2)).index(end));
      if stat{1}~=1; error(stat{2:end}); end;
      [stat,R1]=RmatAtoB(Model.magGroup.Quad.Off.ClusterList(qmovers(ic-length(c)/2)).index(end)+1, ...
                         Model.magGroup.Quad.Bpm(qbpms(ib-length(b)/4)).Ind);
      if stat{1}~=1; error(stat{2:end}); end;
      R(ib,ic) = RQ_func('YY',RQ,R1);
    elseif ( ib > length(b)/2 ) && ( ( ib-length(b)/2 ) == ic )
      R(ib,ic) = 1 ;
    end % if
  end % for M_id
end % for Mb_id

function R_ret = RQ_func(type, Rq, Rij)
if isempty(Rq) || isempty(Rij)
  R_ret = 0;
  return
end
type=upper(type);
switch type
  case 'XX'
    R_ret = (Rq(1,1)-1)*Rij(1,1) + Rq(2,1)*Rij(1,2) + Rq(3,1)*Rij(1,3) + Rq(4,1)*Rij(1,4) ;
  case 'XY'
    R_ret = Rq(1,3)*Rij(1,1) + Rq(2,3)*Rij(1,2) + (Rq(3,3)-1)*Rij(1,3) + Rq(4,3)*Rij(1,4) ;
  case 'YY'
    R_ret = Rq(1,3)*Rij(3,1) + Rq(2,3)*Rij(3,2) + (Rq(3,3)-1)*Rij(3,3) + Rq(4,3)*Rij(3,4) ;
  case 'YX'
    R_ret = (Rq(1,1)-1)*Rij(3,1) + Rq(2,1)*Rij(3,2) + Rq(3,1)*Rij(3,3) + Rq(4,1)*Rij(3,4) ;
  otherwise
    error('Wrong Type argument in RQ_func!');
end
R_ret = -R_ret;

function [] = pShift(pShiftVal)
% Change BEAMLINE P values throughout BDS
global BEAMLINE
for iEle=1:length(BEAMLINE)
 if isfield(BEAMLINE{iEle},'P')
   BEAMLINE{iEle}.P=BEAMLINE{iEle}.P+pShiftVal;
 end
end

function [] = kShift(Model,kShiftVal) %#ok<DEFNU>
% Scale BDS lattice
global PS
for iPS=Model.PSind.Quad
  PS(iPS).SetPt = PS(iPS).Ampl + kShiftVal;
end
stat = PSTrim( Model.PSind.Quad ); if stat{1}~=1; error(stat{2:end}); end;

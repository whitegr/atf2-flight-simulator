function [stat,beamout,data,Model] = TrackThruaper(Model, E1, E2, B1, B2, flag)

% Overloaded version of TrackThru to get around the apertures bug.
% Tracks a beam one element at a time, hopefully giving an identical output to TrackThru.

global BEAMLINE MDATA %VERB PS

% 'Inter_beam' is the intermediate beam generated at each step.  For the
% first step, we set this equal to the initial beam.
inter_beam=Model.realbeam;
stat{1}=1;
data=cell(1,3);
counter1=0; counter2=0; counter3=0;
% disp('...Tracking...')
xbeampos=[];
ybeampos=[];

if ~isfield(MDATA,'pulsecounter')
  MDATA.pulsecounter = 1;
  MDATA.beamlosses = 0;
elseif MDATA.pulsecounter == 5616 % number of pulses in an hour @1.56 Hz
  disp('5616 pulses reached.  Resetting beamloss monitor to zero.')
  MDATA.pulsecounter = 1;
  MDATA.beamlosses = 0;
else
  MDATA.pulsecounter = MDATA.pulsecounter + 1;
end

for niter=E1:E2
  if niter==E1
    for bpm_ind=Model.handles.BPM
      Model.BPMrescorrfact=sum(inter_beam.Bunch.Q) / (1.6021773e-19*2e10);

      if bpm_ind==Model.handles.BPM(Model.ip_bpm)
        BEAMLINE{bpm_ind}.Resolution = 2e-9/Model.BPMrescorrfact;
      elseif sum(bpm_ind==[Model.magGroup.Sext.Bpm(:).Ind])
        BEAMLINE{bpm_ind}.Resolution = Model.bpm.sres/Model.BPMrescorrfact;
      elseif bpm_ind==Model.fbBpmsToUse(1) || bpm_ind==Model.fbBpmsToUse(2)
        BEAMLINE{bpm_ind}.Resolution = 10e-9/Model.BPMrescorrfact;
      elseif isfield(Model.ext,'endInd') && bpm_ind<Model.ext.endInd
        BEAMLINE{bpm_ind}.Resolution = Model.ext.bpm_res/Model.BPMrescorrfact;
      else
        BEAMLINE{bpm_ind}.Resolution = Model.bpm.res/Model.BPMrescorrfact;
      end
    end
  end
  [inter_stat,inter_beam,inter_data] = TrackThru(niter,niter,inter_beam,B1,B2,flag);
  xbeampos=[xbeampos mean(inter_beam.Bunch.x(1,:))]; %#ok<AGROW>
  ybeampos=[ybeampos mean(inter_beam.Bunch.x(3,:))]; %#ok<AGROW>
  if inter_stat{1}~=1
    stat=inter_stat;
  end
  if ( strcmp(BEAMLINE{niter}.Class,'MONI') || strcmp(BEAMLINE{niter}.Class,'HMON') || ...
      strcmp(BEAMLINE{niter}.Class,'VMON') || strcmp(BEAMLINE{niter}.Class,'INST') || ...
      strcmp(BEAMLINE{niter}.Class,'PROF') || strcmp(BEAMLINE{niter}.Class,'WIRE') || ...
      strcmp(BEAMLINE{niter}.Class,'BLMO') || strcmp(BEAMLINE{niter}.Class,'SLMO') || ...
      strcmp(BEAMLINE{niter}.Class,'IMON') )
    if size(inter_data{1},2)
      counter1=counter1+1;
      data{1}(counter1)=inter_data{1};
    end
    if size(inter_data{2},2)
      counter2=counter2+1;
      data{2}(counter2)=inter_data{2};
    end
    if size(inter_data{3},2)
      counter3=counter3+1;
      data{3}(counter3)=inter_data{3};
    end
  end

  % Determine if any particles have hit an aperture.
  if isfield(BEAMLINE{niter},'aper')
    maxdev=sqrt((inter_beam.Bunch.x(1,:).^2) + (inter_beam.Bunch.x(3,:).^2));
    lostparts_index=find(maxdev>BEAMLINE{niter}.aper);
    notlost_index=find(maxdev<BEAMLINE{niter}.aper);
    if ~isempty(lostparts_index)
      lostcharge=length(lostparts_index)*mean(inter_beam.Bunch.Q(lostparts_index))*1e9;
      MDATA.chargeloss(niter) = MDATA.chargeloss(niter) + lostcharge;
      if size(inter_beam.Bunch.x,2)~=1
        if length(lostparts_index)>1
          disp([num2str(length(lostparts_index)) ' rays / ' ...
            num2str(size(inter_beam.Bunch.x,2)) ' lost (' num2str(lostcharge) ...
            ' nC) at element ' num2str(niter) '(' BEAMLINE{niter}.Name ')'])
        else
          disp([num2str(length(lostparts_index)) ' rays / ' ...
            num2str(size(inter_beam.Bunch.x,2)) ' lost (' num2str(lostcharge) ...
            ' nC) at element ' num2str(niter) '(' BEAMLINE{niter}.Name ')'])
        end
      end
    else
      lostcharge=0;
    end
    MDATA.beamlosses = MDATA.beamlosses + lostcharge;
    if MDATA.beamlosses>2883.9178
      save(['radpolice_saves/out' num2str(Model.nseed) '.mat'])
      disp(' ')
      disp('You have exceeded the allowed beam loss for this hour.')
      error('The radiation police have been called.')
    end
    if isempty(notlost_index)
      disp(['Beam entirely stopped by element ' num2str(niter) ...
        ' (' BEAMLINE{niter}.Name ').'])
      bpmcounter=0;
      for bpm_ind=Model.handles.BPM
        bpmcounter=bpmcounter+1;
        if bpm_ind<niter
          continue
        end
        data{1}(bpmcounter).Index=bpm_ind;
        data{1}(bpmcounter).S=BEAMLINE{bpm_ind}.S;
        data{1}(bpmcounter).Pmod=BEAMLINE{bpm_ind}.P;
        data{1}(bpmcounter).x=0;
        data{1}(bpmcounter).y=0;
        data{1}(bpmcounter).z=0;
        data{1}(bpmcounter).P=0;
        data{1}(bpmcounter).sigma=zeros(6,6);
      end
      beamout=inter_beam;
      return
    end
    new_beam.Bunch.x=inter_beam.Bunch.x(:,notlost_index);
    new_beam.Bunch.Q=inter_beam.Bunch.Q(notlost_index);
    new_beam.Bunch.stop=inter_beam.Bunch.stop(notlost_index);
    new_beam.BunchInterval=inter_beam.BunchInterval;
    clear inter_beam
    inter_beam=new_beam;
    clear new_beam
  end
  if niter<E2 && strcmp(BEAMLINE{niter+1}.Class,'MONI')
    %         Model.BPMrescorrfact=(mean(inter_beam.Bunch.Q)*length(inter_beam.Bunch.Q))/...
    %             (1.6021773e-19*2e10);
    Model.BPMrescorrfact=1;
    BEAMLINE{niter+1}.Resolution=BEAMLINE{niter+1}.Resolution/Model.BPMrescorrfact;
  end
end

bpm_ind=findcells(BEAMLINE,'Class','MONI');
if size(data{1},2)<length(bpm_ind)
  for bpmcounter=(size(data{1},2)+1):length(bpm_ind)
    data{1}(bpmcounter).Index=bpm_ind(bpmcounter);
    data{1}(bpmcounter).S=BEAMLINE{bpm_ind(bpmcounter)}.S;
    data{1}(bpmcounter).Pmod=BEAMLINE{bpm_ind(bpmcounter)}.P;
    data{1}(bpmcounter).x=0;
    data{1}(bpmcounter).y=0;
    data{1}(bpmcounter).z=0;
    data{1}(bpmcounter).P=0;
    data{1}(bpmcounter).sigma=zeros(6,6);
  end
end

beamout=inter_beam;

clear xbeampos ybeampos

return

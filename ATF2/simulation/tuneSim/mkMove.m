function Model = mkMove( Model, move_vec )
% Apply multi-knob move to correct IP abberations
global GIRDER PS

% Ensure move_vec is row-wise and correct length
[r c]=size(move_vec);
if c>r; move_vec=move_vec'; end;
if length(move_vec)~=8 && length(move_vec)~=17; error('move_vec must have 8 or 17 elements!'); end;

% Use SQ3FF for coupling compensation for sigma(2,3) term correction
% if move_vec(7)
%   PS(Model.PSind.Quad(Model.magGroup.SQuad_id(5))).SetPt = PS(Model.PSind.Quad(Model.magGroup.SQuad_id(5))).Ampl + move_vec(7);
%   PSTrim( Model.PSind.Quad(Model.magGroup.SQuad_id(5)) );
% end
% Sextupole multikobs
knobNames={'WaistX' 'WaistY' 'DispX' 'DispY' 'SIG13'};
iMoveVec=[1 2 3 4 7];
for iKnob=1:5
  if move_vec(iMoveVec(iKnob))
    bpmSextOffset(Model,'get');
    IncrementMultiKnob( ['Model.knob.',knobNames{iKnob}] , move_vec(iMoveVec(iKnob)) );
    bpmSextOffset(Model,'set');
  end
end
% SQuad strength changes
if move_vec(5)
  PS(Model.PSind.Quad(Model.magGroup.SQuad_id(Model.sqMove))).SetPt=...
    PS(Model.PSind.Quad(Model.magGroup.SQuad_id(Model.sqMove))).Ampl + move_vec(5);
    PSTrim( Model.PSind.Quad(Model.magGroup.SQuad_id(Model.sqMove)) );
end
if length(move_vec)>8
  % Fix higher-order terms using Sextupole dK and Tilt
  if sum(move_vec(9:12))
    moveAxis=[3 3 3 6];
    for iSext=1:4
      if move_vec(8+iSext)
        GIRDER{Model.Gind.Sext(iSext)}.MoverSetPt(moveAxis(iSext))=GIRDER{Model.Gind.Sext(iSext)}.MoverPos(moveAxis(iSext))+move_vec(8+iSext);
        MoverTrim( Model.Gind.Sext(iSext) );
      end
    end
  end
  if sum(move_vec(13:17))
    for iSext=1:5
      if move_vec(12+iSext)
        PS(Model.PSind.Sext(iSext)).SetPt=PS(Model.PSind.Sext(iSext)).Ampl+move_vec(12+iSext);
        PSTrim(Model.PSind.Sext(iSext));
      end
    end
  end
end